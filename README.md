# White Rabbit Compliance Tests

This project hosts compliance tests dedicated for WR devices and based on the ATTEST framework available from Veryx Technologies 
(http://www.veryxtech.com). To use the material available in this project, the ATTEST framework needs to be purchased.