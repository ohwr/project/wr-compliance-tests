#! /usr/bin/tcl
########################################################################################
#####                     Command Array for Cisco 2950                             #####
#####                     No of SET commands       : 26                            #####
#####                     No of GET commands       :  8                            #####
#####                     No of CALLBACK functions : 35                            #####
########################################################################################

########################################################################################
# File Name           :     vlan_array.tcl                                             #
# File Version        :     1.28                                                       #
# Component Name      :     NET-O2 ATTEST DUT Command Line Interface (CLI)             #
# Module Name         :     VLAN                                                       #
#                                                                                      #
########################################################################################
#                                                                                      #
#     SET ARRAY INDEX                                 GET ARRAY INDEX                  #
#                                                                                      #
# 1a.DUT_VLAN_ADD_STP_BRIDGE                       1.DUT_VLAN_GET_BRIDGE_ID            #
# 1b.DUT_VLAN_ADD_RSTP_BRIDGE                      2.DUT_VLAN_READ_PORT_STATE          #
# 1c.DUT_VLAN_ADD_MSTP_BRIDGE                      3.DUT_VLAN_READ_PORT_ROLE           #
# 2 .DUT_VLAN_ADD_VLAN                             4.DUT_VLAN_CHECK_PORT_ENTRY         #
# 3 .DUT_VLAN_REMOVE_VLAN                          5.DUT_VLAN_CHECK_PORT_MCAST_ENTRY   #
# 4 .DUT_VLAN_ADD_PORT                             6.DUT_VLAN_CHECK_REGISTRATION_ENTRY #
# 5a.DUT_VLAN_ENABLE_STP_PORT                      7.DUT_VLAN_CHECK_GVRP_PORT_STATE    #
# 5b.DUT_VLAN_ENABLE_RSTP_PORT                     8.DUT_VLAN_GET_PGD_GROUP_ID         #
# 5c.DUT_VLAN_ENABLE_MSTP_PORT                                                         #
# 6 .DUT_VLAN_DISABLE_PORT                                                             #
# 7 .DUT_VLAN_ADD_STATIC_ENTRY                                                         #
# 8 .DUT_VLAN_REMOVE_STATIC_ENTRY                                                      #
# 9 .DUT_VLAN_ADD_STATIC_MCAST_ENTRY                                                   #
#10 .DUT_VLAN_REMOVE_STATIC_MCAST_ENTRY                                                #
#11 .DUT_VLAN_SET_INGRESS_FILTER                                                       #
#12 .DUT_VLAN_RESET_INGRESS_FILTER                                                     #
#13 .DUT_VLAN_ADMIT_ALL_FRAMES                                                         #
#14 .DUT_VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES                                            #
#15 .DUT_VLAN_SET_TAGGED_PORT_VLAN_MEMBERSHIP                                          #
#16 .DUT_VLAN_RESET_TAGGED_PORT_VLAN_MEMBERSHIP                                        #
#17 .DUT_VLAN_SET_PVID_TO_UNTAGGED_PORT                                                #
#18 .DUT_VLAN_RESET_PVID_TO_UNTAGGED_PORT                                              #
#19 .DUT_VLAN_ENABLE_GVRP                                                              #
#20 .DUT_VLAN_DISABLE_GVRP                                                             #
#21 .DUT_VLAN_SET_REGISTRATION_TYPE_FORBIDDEN                                          #
#22 .DUT_VLAN_RESET_REGISTRATION_TYPE_FORBIDDEN                                        #
#23 .DUT_VLAN_ADD_PGD_ENTRY                                                            #
#24 .DUT_VLAN_REMOVE_PGD_ENTRY                                                         #
#25 .DUT_VLAN_ADD_VID_SET                                                              #
#26 .DUT_VLAN_REMOVE_VID_SET                                                           #
#27 .DUT_VLAN_CREATE_STATIC_MCAST_ENTRY                                                #
#                                                                                      #
#      ENV VARIABLES                                                                   #
#                                                                                      #
#  1.dut_VLAN_DESIGNATED_PORT_ROLE                                                     #
#  2.dut_VLAN_ROOT_PORT_ROLE                                                           #
#  3.dut_VLAN_ALTERNATE_PORT_ROLE                                                      #
#  4.dut_VLAN_BLOCKING_STATE                                                           #
#  5.dut_VLAN_LISTENING_STATE                                                          #
#  6.dut_VLAN_LEARNING_STATE                                                           #
#  7.dut_VLAN_FORWARDING_STATE                                                         #
#                                                                                      #
########################################################################################
# History       Date     Author         Addition/ Alteration                           #
#                                                                                      #
#    1.0      Apr/2003   NET-O2         Initial                                        #
#    1.1      Jun/2003   NET-O2         Added new functions                            #
#    1.2      Nov/2004   NET-O2         Removed an additional array index              #
#                                       in the DUT_VLAN_ADD_MSTP_BRIDGE                #
#                                       command                                        #
#    <1.3 - 1.22> logs not captured.                                                   #
#    1.23     Mar/2005   NET-O2         Added Command DUT_VLAN_CREATE_STATIC           #
#                                       _MCAST_ENTRY.                                  #
#    1.24     Jun/2005   NET-O2         Added "no switchport nonegotiate"              #
#                                       in "DUT_VLAN_DISABLE_PORT".                    #
#                                       (Refer vlanRfc012.txt)                         #
#    1.25     Feb/2006   NET-O2         Added "DUT_VLAN_SET_PORT_PATHCOST" and         #
#                                       callback_vlan_set_port_path_cost functions     #
#    1.26     Feb/2006   NET-O2         Commented some commands to use the default     #
#                                       values from the DUT.                           #
#                                       Removed "DUT_VLAN_SET_PORT_PATHCOST" and       #
#                                       callback_vlan_set_port_path_cost functions     #
#    1.27     Feb/2008   NET-O2         Updated function name                          #
#                                       convert_macaddress_to_user_dut to              #
#                                       vlan_convert_macaddress_to_user_dut            #
#    1.28     Apr/2020   NET-O2         Updated calculation of VLAN mask (bug fix)     #
########################################################################################

########################################################################################
# Copyright (C) 2003-08 Net-O2 Technologies (P) Ltd.                                   #
#                                                                                      #
# Net-O2 Technologies (P) Ltd. grants the Source and Binary licenses of                #
# Net-O2 ATTEST Test Suite with the permission to modify and use the                   #
# contents of this file, solely to interface ATTEST with the Device Under              #
# Test.                                                                                #
#                                                                                      #
######################################################################################## 


########################################################################################
#                       SET Commands                                                   #
########################################################################################

global listofPorts
global listofPorts_admitall
global listofPorts_admitvlan
global listofPorts_tpvlan
set listofPorts ""
set listofPorts_admitall ""
set listofPorts_admitvlan ""
set listofPorts_tpvlan ""
proc  tee_set_dut_vlan_cli_from_file {} {
global cli_set_record
global env
global vlan_config_digest 


  global ATTEST_CORE tc_name  ovc_created ovc_removed

  set PktFileName $ATTEST_CORE(-pktFileName)
  set log_instance [lindex [split $PktFileName "."] 0]
  set tc_name_list [split $log_instance "_"]
  set group_name  [lindex $tc_name_list 3]
  set case_id     [join [lrange $tc_name_list 4 6] "_"]
  set case_num [lindex $tc_name_list 4]
  set tc_name_wo_date [lreplace $tc_name_list end end]
  set tc_name [join $tc_name_wo_date _]
  set ovc_created 0
  set ovc_removed 0


#################################################
######## Cisco no vlan msti map digest ##########
#################################################

 set vlan_config_digest "bb3b6c15ef8d089bb55ed10d24df44de"

### Set the Port Role ###

set env(dut_VLAN_DESIGNATED_PORT_ROLE) "Desg"
set env(dut_VLAN_ROOT_PORT_ROLE)       "Root"
set env(dut_VLAN_ALTERNATE_PORT_ROLE)  "Altn"

### Set the Port State ###
# For RSTP Listening State will also be Blocking

set env(dut_VLAN_BLOCKING_STATE)   "BLK"
set env(dut_VLAN_LISTENING_STATE)  "BLK"
set env(dut_VLAN_LEARNING_STATE)   "LRN"
set env(dut_VLAN_FORWARDING_STATE) "FWD"

#############################################################################
#1a.                                                                        #
#  COMMAND          : Add Bridge                                            #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_ADD_STP_BRIDGE                               #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = bridge_instance (if supported)                           #
#  ATTEST_PARAM2 = vlan_id (range 1-4094)                                   #
#  ATTEST_PARAM3 = priority (range 0 - 65535)                               #
#  ATTEST_PARAM4 = HelloTime (range 1.0 - 10.0)                             #
#  ATTEST_PARAM5 = ForwardDelay (range 4.0 - 30.0)                          #
#  ATTEST_PARAM6 = MaxAge (range 6.0 - 40.0)                                #
#                                                                           #
#  DEFINITION       : Adds a Bridge at DUT in STP mode.                     #
#                     The last command should be "NONE".                    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################

   set cli_set_record(DUT_VLAN_ADD_STP_BRIDGE,ATTEST_SET,1)   "NONE"

#############################################################################
#1b.                                                                        #
#  COMMAND          : Add Bridge                                            #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_ADD_RSTP_BRIDGE                              #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = bridge_instance (if supported)                           #
#  ATTEST_PARAM2 = vlan_id (range 1-4094)                                   #
#  ATTEST_PARAM3 = priority (range 0 - 61440 in steps of 4096)              #
#  ATTEST_PARAM4 = HelloTime (range 1.0 - 10.0)                             #
#  ATTEST_PARAM5 = ForwardDelay (range 4.0 - 30.0)                          #
#  ATTEST_PARAM6 = MaxAge (range 6.0 - 40.0)                                #
#                                                                           #
#  DEFINITION       : Adds a Bridge at DUT in RSTP mode.                    #
#                     The last command should be "NONE".                    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################

   set cli_set_record(DUT_VLAN_ADD_RSTP_BRIDGE,ATTEST_SET,1)   "NONE"

#############################################################################
#1c.                                                                        #
#  COMMAND          : Add Bridge                                            #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_ADD_MSTP_BRIDGE                              #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = bridge_instance (if supported)                           #
#  ATTEST_PARAM2 = vlan_id (range 1-4094)                                   #
#  ATTEST_PARAM3 = priority (range 0 - 61440 in steps of 4096)              #
#  ATTEST_PARAM4 = HelloTime (range 1.0 - 10.0)                             #
#  ATTEST_PARAM5 = ForwardDelay (range 4.0 - 30.0)                          #
#  ATTEST_PARAM6 = MaxAge (range 6.0 - 40.0)                                #
#                                                                           #
#  DEFINITION       : Add a Bridge on the DUT.                              #
#                     The last command should be "NONE".                    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################

   set cli_set_record(DUT_VLAN_ADD_MSTP_BRIDGE,ATTEST_SET,1)   "NONE"

#############################################################################
#2.                                                                         #
#  COMMAND          : Add Vlan                                              #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_ADD_VLAN                                     #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = port (in Hex) Eg:for port 2 -> Hex(2) -> 0x2             #
#  ATTEST_PARAM2 = vlan_id (range 1-4094)                                   #
#                                                                           #
#  DEFINITION       : Adds a Vlan to a Bridge at DUT .                      #
#                     The last command should be "NONE".                    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################

   set cli_set_record(DUT_VLAN_ADD_VLAN,ATTEST_SET,1)   "wrs_vlans \-\–rvid ATTEST_PARAM2 --rmask 0x0"

   set cli_set_record(DUT_VLAN_ADD_VLAN,ATTEST_SET,1)   "NONE"


#############################################################################
#3.                                                                         #
#  COMMAND          : Remove Vlan                                           #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_REMOVE_VLAN                                  #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = port (in Hex)Eg:for port 2 -> Hex(2) -> 0x2              #
#  ATTEST_PARAM2 = vlan_id (range 1-4094)                                   #
#                                                                           #
#  DEFINITION       : Removes a Vlan from a Bridge at DUT .                 #
#                     The last command should be "NONE".                    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################

   set cli_set_record(DUT_VLAN_REMOVE_VLAN,ATTEST_SET,1)   "wrs_vlans --rvid ATTEST_PARAM2 --del"

   set cli_set_record(DUT_VLAN_REMOVE_VLAN,ATTEST_SET,2)   "wrs_vlans --clear"

   set cli_set_record(DUT_VLAN_REMOVE_VLAN,ATTEST_SET,3)   "wrs_vlans --plist"

   set cli_set_record(DUT_VLAN_REMOVE_VLAN,ATTEST_SET,4)   "wrs_vlans --list"

   set cli_set_record(DUT_VLAN_REMOVE_VLAN,ATTEST_SET,5)   "NONE"

#############################################################################
#4.                                                                         #
#  COMMAND          : Add Port                                              #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_ADD_PORT                                     #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = bridge_instance (if supported)                           #
#  ATTEST_PARAM2 = vlan_id (range 1-4094)                                   #
#  ATTEST_PARAM3 = port (in Hex) Eg:for port 2 -> Hex(2) -> 0x2             #
#                                                                           #
#  DEFINITION       : Adds a Port to the Bridge at DUT.                     #
#                     The last command should be "NONE".                    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################

   set cli_set_record(DUT_VLAN_ADD_PORT,ATTEST_SET,1)   "ifconfig ATTEST_PARAM3 up"

   set cli_set_record(DUT_VLAN_ADD_PORT,ATTEST_SET,2)   "NONE"

#############################################################################
#5a.                                                                        #
#  COMMAND          : Enable Port                                           #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_ENABLE_STP_PORT                              #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = bridge_instance (if supported)                           #
#  ATTEST_PARAM2 = vlan_id (range 1-4094)                                   #
#  ATTEST_PARAM3 = port_num                                                 #
#  ATTEST_PARAM4 = priority                                                 #
#  ATTEST_PARAM5 = pathcost                                                 #
#                                                                           #
#  DEFINITION       : Adds a Port to the Bridge with the specified pathcost #
#                     and priority.                                         #
#                     The last command should be "NONE".                    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
# Note: AdminPointToPointMAC set as False for the specified Port.           #
#                                                                           #
#############################################################################

   set cli_set_record(DUT_VLAN_ENABLE_STP_PORT,ATTEST_SET,1)  "ifconfig ATTEST_PARAM3 up"

   set cli_set_record(DUT_VLAN_ENABLE_STP_PORT,ATTEST_SET,2)  "NONE"
#############################################################################
#5b.                                                                        #
#  COMMAND          : Enable Port                                           #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_ENABLE_RSTP_PORT                             #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = bridge_instance (if supported)                           #
#  ATTEST_PARAM2 = vlan_id (range 1-4094)                                   #
#  ATTEST_PARAM3 = port_num                                                 #
#  ATTEST_PARAM4 = priority                                                 #
#  ATTEST_PARAM5 = pathcost                                                 #
#                                                                           #
#  DEFINITION       : Adds a Port to the Bridge with the specified pathcost #
#                     and priority.                                         #
#                     The last command should be "NONE".                    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
# Note: AdminPointToPointMAC set as False for the specified Port.           #
#                                                                           #
#############################################################################

   set cli_set_record(DUT_VLAN_ENABLE_RSTP_PORT,ATTEST_SET,1)  "ifconfig ATTEST_PARAM3 up"

   set cli_set_record(DUT_VLAN_ENABLE_RSTP_PORT,ATTEST_SET,2)  "NONE"
#############################################################################
#5c.                                                                        #
#  COMMAND          : Enable Port                                           #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_ENABLE_MSTP_PORT                             #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = bridge_instance (if supported)                           #
#  ATTEST_PARAM2 = vlan_id (range 1-4094)                                   #
#  ATTEST_PARAM3 = port_num                                                 #
#  ATTEST_PARAM4 = priority                                                 #
#  ATTEST_PARAM5 = pathcost                                                 #
#                                                                           #
#  DEFINITION       : Adds a Port to the Bridge with the specified pathcost #
#                     and priority.                                         #
#                     The last command should be "NONE".                    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
# Note: AdminPointToPointMAC set as False for the specified Port.           #
#                                                                           #
#############################################################################

   set cli_set_record(DUT_VLAN_ENABLE_MSTP_PORT,ATTEST_SET,1)  "ifconfig ATTEST_PARAM3 up"

   set cli_set_record(DUT_VLAN_ENABLE_MSTP_PORT,ATTEST_SET,2)  "NONE"
#############################################################################
#6.                                                                         #
#  COMMAND          : Disable Port                                          #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_DISABLE_PORT                                 #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM2 = port name (eg: wri1)                                     #
#                                                                           #
#  DEFINITION      : Makes the specified port down,i.e disables the port.   #
#                    The last command should be "NONE".                     #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################
#Attest_param1 is assigned for bridge instance 
   set cli_set_record(DUT_VLAN_DISABLE_PORT,ATTEST_SET,1)   "ifconfig ATTEST_PARAM2 down"

   set cli_set_record(DUT_VLAN_DISABLE_PORT,ATTEST_SET,2)   "NONE"


#############################################################################
#7.                                                                         #
#  COMMAND          : Add Static Entry                                      #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_ADD_STATIC_ENTRY                             #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = bridge_instance (if supported)                           #
#  ATTEST_PARAM2 = port_num                                                 #
#  ATTEST_PARAM3 = vlan_id (range 1-4094)                                   #
#  ATTEST_PARAM4 = mac_address                                              #  
#                                                                           #
#  DEFINITION       : Adds a static mac-address entry at DUT for the        #
#                     specified Port and specified vlan id.                 #
#                     The last command should be "NONE".                    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################

  set cli_set_record(DUT_VLAN_ADD_STATIC_ENTRY,ATTEST_SET,1)  "rtu_stat add ATTEST_PARAM4 ATTEST_PARAM2 0 ATTEST_PARAM3"

  set cli_set_record(DUT_VLAN_ADD_STATIC_ENTRY,ATTEST_SET,2)  "NONE"

#############################################################################
#8.                                                                         #
#  COMMAND          : Remove Static Entry                                   #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_REMOVE_STATIC_ENTRY                          #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = bridge_instance (if supported)                           #
#  ATTEST_PARAM2 = port_num                                                 #
#  ATTEST_PARAM3 = vlan_id (range 1-4094)                                   #
#  ATTEST_PARAM4 = mac_address                                              #  
#                                                                           #
#  DEFINITION       : Removes the static mcast mac-address entry at DUT for #
#                     the specified Port and specified vlan id.             #
#                     The last command should be "NONE".                    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################

  set cli_set_record(DUT_VLAN_REMOVE_STATIC_ENTRY,ATTEST_SET,1)  "rtu_stat remove mask ATTEST_PARAM4 ATTEST_PARAM2"

  set cli_set_record(DUT_VLAN_REMOVE_STATIC_ENTRY,ATTEST_SET,2)  "NONE"

#############################################################################
#9.                                                                         #
#  COMMAND          : Add Static Mcast Entry                                #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_ADD_STATIC_MCAST_ENTRY                       #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = bridge_instance (if supported)                           #
#  ATTEST_PARAM2 = port_num                                                 #
#  ATTEST_PARAM3 = vlan_id (range 1-4094)                                   #
#  ATTEST_PARAM4 = mac_address                                              #  
#                                                                           #
#  DEFINITION       : Adds the static mcast mac-address entry at DUT for the#
#                     specified Port and specified vlan id.                 #
#                     The last command should be "NONE".                    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################

  set cli_set_record(DUT_VLAN_ADD_STATIC_MCAST_ENTRY,ATTEST_SET,1)  "rtu_stat add mask ATTEST_PARAM4 ATTEST_PARAM2 0 ATTEST_PARAM3"

  set cli_set_record(DUT_VLAN_ADD_STATIC_MCAST_ENTRY,ATTEST_SET,2)  "NONE"


#############################################################################
#10.                                                                        #
#  COMMAND          : Remove Static Mcast Entry                             #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_REMOVE_STATIC_MCAST_ENTRY                    #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = bridge_instance (if supported)                           #
#  ATTEST_PARAM2 = port_num                                                 #
#  ATTEST_PARAM3 = vlan_id (range 1-4094)                                   #
#  ATTEST_PARAM4 = mac_address                                              #  
#                                                                           #
#  DEFINITION       : Removes static mcast mac-address entry at DUT for the #
#                     specified Port and specified vlan id.                 #
#                     The last command should be "NONE".                    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################

  set cli_set_record(DUT_VLAN_REMOVE_STATIC_MCAST_ENTRY,ATTEST_SET,1)  "rtu_stat remove mask ATTEST_PARAM4 ATTEST_PARAM2"

  set cli_set_record(DUT_VLAN_REMOVE_STATIC_MCAST_ENTRY,ATTEST_SET,2)  "NONE"



#############################################################################
#11.                                                                        #
#  COMMAND          : Set Ingress Filter                                    #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_SET_INGRESS_FILTER                           #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = port_num (in hex) Eg:for port 2 -> Hex(2) -> 0x2         #
#  ATTEST_PARAM2 = filter id                                                # 
#                                                                           #
#  DEFINITION      : Enables the Ingress Filter parameter at DUT.           #
#                    The last command should be "NONE".                     #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################

# set cli_set_record(DUT_VLAN_SET_INGRESS_FILTER,ATTEST_SET,1) "wrs_vlans --rvid ATTEST_PARAM1 --rfid ATTEST_PARAM1"

 set cli_set_record(DUT_VLAN_SET_INGRESS_FILTER,ATTEST_SET,1) "NONE"

#############################################################################
#12.                                                                        #
#  COMMAND          : Reset Ingress Filter                                  #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_RESET_INGRESS_FILTER                         #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = port_num (in hex) Eg:for port 2 -> Hex(2) -> 0x2         #
#  ATTEST_PARAM2 = filter id                                                # 
#                                                                           #
#  DEFINITION      : Disables Ingress Filter parameter at DUT.              #
#                    The last command should be "NONE".                     #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################

# set cli_set_record(DUT_VLAN_RESET_INGRESS_FILTER,ATTEST_SET,1) "wrs_vlans --rvid ATTEST_PARAM1 --rfid ATTEST_PARAM1 --del" 

 set cli_set_record(DUT_VLAN_RESET_INGRESS_FILTER,ATTEST_SET,1) "NONE"


#############################################################################
#13.                                                                        #
#  COMMAND          : Admit all frames                                      #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_ADMIT_ALL_FRAMES                             #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = port_num (in hex) Eg:for port 2 -> Hex(2) -> 0x2         #
#  ATTEST_PARAM2 = port_num (in Dec) Eg: for port 2 -> 2                    #
#  ATTEST_PARAM3 = vlan_id (range 0-4094)                                   #
#  ATTEST_PARAM4 = Priority (-1|0-7)                                        #
#                                                                           #
#  DEFINITION      : Sets the specified port to accept VLAN-tagged, priority#
#                    tagged and untagged frames.                            #
#                    The last command should be "NONE".                     #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################

# set cli_set_record(DUT_VLAN_ADMIT_ALL_FRAMES,ATTEST_SET,1) "wrs_vlans --rvid ATTEST_PARAM3 --rmask ATTEST_PARAM4"
#Newchange: Changing pmode 2 to pmode 3
 set cli_set_record(DUT_VLAN_ADMIT_ALL_FRAMES,ATTEST_SET,1) "wrs_vlans --port ATTEST_PARAM2 --pmode 3"

 set cli_set_record(DUT_VLAN_ADMIT_ALL_FRAMES,ATTEST_SET,2) "wrs_vlans --plist"

 set cli_set_record(DUT_VLAN_ADMIT_ALL_FRAMES,ATTEST_SET,4) "wrs_vlans --list"
# set cli_set_record(DUT_VLAN_ADMIT_ALL_FRAMES,ATTEST_SET,2) "wrs_vlans --port ATTEST_PARAM2 --pmode 2"

 set cli_set_record(DUT_VLAN_ADMIT_ALL_FRAMES,ATTEST_SET,5) "NONE"

#############################################################################
#14.                                                                        #
#  COMMAND          : Admit Only Vlan Tagged                                #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES                #
#                                                                           #
#								   	    #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = port_num (in hex) Eg:for port 2 -> Hex(2) -> 0x2         #
#  ATTEST_PARAM2 = port_num (in Dec) Eg: for port 2 -> 2                    #
#  ATTEST_PARAM3 = vlan_id (range 1-4094)                                   #
#                                                                           #
#  DEFINITION       : Allows only vlan tagged frames on the specified port. #
#                    The last command should be "NONE".                     #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################

 set cli_set_record(DUT_VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES,ATTEST_SET,1) "wrs_vlans --rvid ATTEST_PARAM3 --rmask ATTEST_PARAM4"

 set cli_set_record(DUT_VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES,ATTEST_SET,2) "wrs_vlans --port ATTEST_PARAM2 --pmode 1 --puntag 0"

 set cli_set_record(DUT_VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES,ATTEST_SET,3) "wrs_vlans --plist"

 set cli_set_record(DUT_VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES,ATTEST_SET,4) "wrs_vlans --list"

 set cli_set_record(DUT_VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES,ATTEST_SET,5) "NONE"


#############################################################################
#15.                                                                        #
#  COMMAND          : Set Tagged Port Vlan Membership                       #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_SET_TAGGED_PORT_VLAN_MEMBERSHIP              #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = port_num (in hex) Eg:for port 2 -> Hex(2) -> 0x2         #
#  ATTEST_PARAM2 = port_num (in Dec) Eg: for port 2 -> 2                    #
#  ATTEST_PARAM3 = vlan_id (range 1-4094)                                   #
#                                                                           #
#  DEFINITION       : Associates the specified vlan id with the specified   #
#                     tagged port.                                          #
#                    The last command should be "NONE".                     #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################

 set cli_set_record(DUT_VLAN_SET_TAGGED_PORT_VLAN_MEMBERSHIP,ATTEST_SET,1) "wrs_vlans --rvid ATTEST_PARAM3 --rmask ATTEST_PARAM4"

 set cli_set_record(DUT_VLAN_SET_TAGGED_PORT_VLAN_MEMBERSHIP,ATTEST_SET,2) "wrs_vlans --port ATTEST_PARAM2 --pmode 1 --puntag 0"

 set cli_set_record(DUT_VLAN_SET_TAGGED_PORT_VLAN_MEMBERSHIP,ATTEST_SET,3) "wrs_vlans --plist"

 set cli_set_record(DUT_VLAN_SET_TAGGED_PORT_VLAN_MEMBERSHIP,ATTEST_SET,4) "wrs_vlans --list"

 set cli_set_record(DUT_VLAN_SET_TAGGED_PORT_VLAN_MEMBERSHIP,ATTEST_SET,5) "NONE"

#############################################################################
#16.                                                                        #
#  COMMAND          : Reset Tagged Port Vlan Membership                     #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_RESET_TAGGED_PORT_VLAN_MEMBERSHIP            #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = bridge_instance (if supported)                           #
#  ATTEST_PARAM2 = port_num                                                 #
#  ATTEST_PARAM3 = vlan_id (range 1-4094)                                   #
#                                                                           #
#  DEFINITION       : Removes the specified vlan id from the specified      #
#                     tagged port.                                          #
#                     The last command should be "NONE".                    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################

 set cli_set_record(DUT_VLAN_RESET_TAGGED_PORT_VLAN_MEMBERSHIP,ATTEST_SET,1) "wrs_vlans --clear"

 set cli_set_record(DUT_VLAN_RESET_TAGGED_PORT_VLAN_MEMBERSHIP,ATTEST_SET,2) "NONE"


#############################################################################
#17.                                                                        #
#  COMMAND          : Set Vlan Membership on Access Port                    #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_SET_PVID_TO_UNTAGGED_PORT                    #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = port_num (in hex) Eg:for port 2 -> Hex(2) -> 0x2         #
#  ATTEST_PARAM2 = port_num (in Dec) Eg: for port 2 -> 2                    #
#  ATTEST_PARAM3 = vlan_id (range 1-4094)                                   #
#                                                                           #
#  DEFINITION       : Associates a permanent vlan id to the specified       #
#                     untagged port.                                        #
#                    The last command should be "NONE".                     #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################

 set cli_set_record(DUT_VLAN_SET_PVID_TO_UNTAGGED_PORT,ATTEST_SET,1) "wrs_vlans --rvid ATTEST_PARAM3 --rmask ATTEST_PARAM4"
 if {($group_name == "bmg") && ($case_num == "002")} {

 set cli_set_record(DUT_VLAN_SET_PVID_TO_UNTAGGED_PORT,ATTEST_SET,2) "wrs_vlans --port ATTEST_PARAM2  --pvid ATTEST_PARAM3 "

 set cli_set_record(DUT_VLAN_SET_PVID_TO_UNTAGGED_PORT,ATTEST_SET,3) "wrs_vlans --plist"

 set cli_set_record(DUT_VLAN_SET_PVID_TO_UNTAGGED_PORT,ATTEST_SET,4) "wrs_vlans --list"

 set cli_set_record(DUT_VLAN_SET_PVID_TO_UNTAGGED_PORT,ATTEST_SET,5) "NONE"

} else {

 set cli_set_record(DUT_VLAN_SET_PVID_TO_UNTAGGED_PORT,ATTEST_SET,2) "wrs_vlans --port ATTEST_PARAM2 --pmode 0 --pvid ATTEST_PARAM3 "

 set cli_set_record(DUT_VLAN_SET_PVID_TO_UNTAGGED_PORT,ATTEST_SET,3) "wrs_vlans --plist"

 set cli_set_record(DUT_VLAN_SET_PVID_TO_UNTAGGED_PORT,ATTEST_SET,4) "wrs_vlans --list"

 set cli_set_record(DUT_VLAN_SET_PVID_TO_UNTAGGED_PORT,ATTEST_SET,5) "NONE"

}

#############################################################################
#18.                                                                        #
#  COMMAND          : Reset Vlan Membership on Access Port                  #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_RESET_PVID_TO_UNTAGGED_PORT                  #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = bridge_instance (if supported)                           #
#  ATTEST_PARAM2 = port_num                                                 #
#  ATTEST_PARAM3 = vlan_id (range 1-4094)                                   #
#                                                                           #
#  DEFINITION       : Removes the specified permanent vlan id from the      #
#                     specified untagged port.                              #
#                     The last command should be "NONE".                    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################

 set cli_set_record(DUT_VLAN_RESET_PVID_TO_UNTAGGED_PORT,ATTEST_SET,1) "wrs_vlans --clear"

 set cli_set_record(DUT_VLAN_RESET_PVID_TO_UNTAGGED_PORT,ATTEST_SET,2) "NONE"


#############################################################################
#19.                                                                        #
#  COMMAND          : Enable GVRP                                           #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_ENABLE_GVRP                                  #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = bridge_instance (if supported)                           #
#                                                                           #
#  DEFINITION       : Enables GVRP at  DUT.                                 #
#                     The last command should be "NONE".                    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################

 set cli_set_record(DUT_VLAN_ENABLE_GVRP,ATTEST_SET,1) "NONE"



#############################################################################
#20.                                                                        #
#  COMMAND          : Disable GVRP                                          #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_DISABLE_GVRP                                 #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = bridge_instance (if supported)                           #
#                                                                           #
#  DEFINITION       : Disables GVRP at  DUT.                                #
#                     The last command should be "NONE".                    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################

 set cli_set_record(DUT_VLAN_DISABLE_GVRP,ATTEST_SET,1) "NONE"


#############################################################################
#21.                                                                        #
#  COMMAND          : Sets the Registration Type as Forbidden               #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_SET_REGISTRATION_TYPE_FORBIDDEN              #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = bridge_instance (if supported)                           #
#  ATTEST_PARAM2 = port_num                                                 #
#  ATTEST_PARAM3 = vlan_id (range 1-4094)                                   #
#                                                                           #
#  DEFINITION      : Sets the Registration Type for the specified port in   #
#                    as"forbidden" for the specified Vlan Id.               #
#                    The last command should be "NONE".                     #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################


 set cli_set_record(DUT_VLAN_SET_REGISTRATION_TYPE_FORBIDDEN,ATTEST_SET,1) "wrs_vlans --rvid ATTEST_PARAM3 --rdrop 1"

 set cli_set_record(DUT_VLAN_SET_REGISTRATION_TYPE_FORBIDDEN,ATTEST_SET,2) "NONE"

#############################################################################
#22.                                                                        #
#  COMMAND          : Resets the Registration Type as Forbidden             #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_RESET_REGISTRATION_TYPE_FORBIDDEN            #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = bridge_instance (if supported)                           #
#  ATTEST_PARAM2 = port_num                                                 #
#  ATTEST_PARAM3 = vlan_id (range 1-4094)                                   #
#                                                                           #
#  DEFINITION      : Resets the Registration Type for the specified port.   #
#                    The last command should be "NONE".                     #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################


 set cli_set_record(DUT_VLAN_RESET_REGISTRATION_TYPE_FORBIDDEN,ATTEST_SET,1) "NONE"


#############################################################################
#23.                                                                        #
#  COMMAND          : Add Protocol to Group ID                              #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_ADD_PGD_ENTRY                                #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = bridge_instance (if supported)                           #
#  ATTEST_PARAM2 = frame_type                                               #
#  ATTEST_PARAM3 = protocol_type                                            #
#  ATTEST_PARAM4 = group_id                                                 #
#                                                                           #
#  DEFINITION       : Adds a Protocol to Group ID.                          #
#                     The last command should be "NONE".                    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################


   set cli_set_record(DUT_VLAN_ADD_PGD_ENTRY,ATTEST_SET,1)   "NONE"

#############################################################################
#24.                                                                        #
#  COMMAND          : Remove Protocol to Group ID                           #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_REMOVE_PGD_ENTRY                             #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = bridge_instance (if supported)                           #
#  ATTEST_PARAM2 = frame_type                                               #
#  ATTEST_PARAM3 = protocol_type                                            #
#  ATTEST_PARAM4 = group_id                                                 #
#                                                                           #
#  DEFINITION       : Removes a Protocol from a Group ID.                   #
#                     The last command should be "NONE".                    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################

   set cli_set_record(DUT_VLAN_REMOVE_PGD_ENTRY,ATTEST_SET,1)   "NONE"

#############################################################################
#25.                                                                        #
#  COMMAND          : Add VID set to Group ID                               #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_ADD_VID_SET                                  #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = bridge_instance (if supported)                           #
#  ATTEST_PARAM2 = vlan_id (range 1-4094)                                   #
#  ATTEST_PARAM3 = port_num                                                 #
#  ATTEST_PARAM4 = group_id (range 1-4094)                                  #
#                                                                           #
#  DEFINITION       : Associates a VID set to Group ID.                     #
#                     The last command should be "NONE".                    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################

 set cli_set_record(DUT_VLAN_ADD_VID_SET,ATTEST_SET,1) "NONE"


#############################################################################
#26.                                                                        #
#  COMMAND          : Remove VID set from Group ID                          #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_REMOVE_VID_SET                               #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = bridge_instance (if supported)                           #
#  ATTEST_PARAM2 = vlan_id (range 1-4094)                                   #
#  ATTEST_PARAM3 = port_num                                                 #
#  ATTEST_PARAM4 = group_id (range 1-4094)                                  #
#                                                                           #
#  DEFINITION       : Removes VID set from Group ID.                        #
#                     The last command should be "NONE".                    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################

 set cli_set_record(DUT_VLAN_REMOVE_VID_SET,ATTEST_SET,1) "NONE"

#############################################################################
#27.                                                                        #
#  COMMAND          : Create  Static Mcast Entry                            #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_CREATE_STATIC_MCAST_ENTRY                    #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = bridge_instance (if supported)                           #
#  ATTEST_PARAM2 = Port mask                                                #
#  ATTEST_PARAM3 = vlan id                                                  #
#  ATTEST_PARAM4 = Mac address                                              #
#                                                                           #
#  DEFINITION       : Adds the static mcast mac-address entry at DUT for the#
#                     specified Port and specified vlan id.                 #
#                     The last command should be "NONE".                    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################

# NOTE: Cisco does not have support for mapping this command. Secondly in 
# Cisco an explicit creation of mcast entry without any port is not required.

  set cli_set_record(DUT_VLAN_CREATE_STATIC_MCAST_ENTRY,ATTEST_SET,1)   "rtu_stat add mask ATTEST_PARAM4 ATTEST_PARAM2 0 ATTEST_PARAM3"

  set cli_set_record(DUT_VLAN_CREATE_STATIC_MCAST_ENTRY,ATTEST_SET,2)   "NONE"
}

#############################################################################
# 			GET Commands					    #
#############################################################################


proc  tee_get_dut_vlan_cli_from_file {} {
global cli_get_record

#############################################################################
#1.                                                                         #
#  COMMAND          : Get Bridge Id                                         #
#								   	    #
#  ARRAY INDEX      : DUT_VLAN_GET_BRIDGE_ID                                #
#								   	    #
#  INPUT PARAMETERS : 							    #
#								   	    #
#  ATTEST_PARAM1 = bridge_instance (if supported)          		    #
#  ATTEST_PARAM2 = vlan_id (range 1-4094)                                   #  
#                                                                           #
#  DEFINITION       : Read Bridge Id.                                       #
#		      The last command should be "NONE".     		    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#		      None.                                                 #
#									    #
#############################################################################

   set cli_get_record(DUT_VLAN_GET_BRIDGE_ID,ATTEST_OUTPUT,1)   "show spanning-tree vlan ATTEST_PARAM2 \
                                                                 bridge id"

   set cli_get_record(DUT_VLAN_GET_BRIDGE_ID,ATTEST_GET,2)      "NONE"


#############################################################################
#2.                                                                         #
#  COMMAND          : Read Port State                                       #
#								   	    #
#  ARRAY INDEX      : DUT_VLAN_READ_PORT_STATE                              #
#								   	    #
#  INPUT PARAMETERS : 							    #
#								   	    #
#  ATTEST_PARAM1 = bridge_instance (if supported)          		    #
#  ATTEST_PARAM2 = port_num Port Number                                     #  
#  ATTEST_PARAM3 = vlan_id (range 1-4094)                                   #  
#  ATTEST_PARAM4 = port_state                                               #  
#                                                                           #
#  DEFINITION       : Read Port State.                                      #
#		      The last command should be "NONE".     		    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#		      None.                                                 #
#									    #
#############################################################################
 
  set cli_get_record(DUT_VLAN_READ_PORT_STATE,ATTEST_OUTPUT,1)  "rtu_stat"
                                                                 

  set cli_get_record(DUT_VLAN_READ_PORT_STATE,ATTEST_GET,2)     "NONE"

#############################################################################
#3.                                                                         #
#  COMMAND          : Read Port Role                                        #
#								   	    #
#  ARRAY INDEX      : DUT_VLAN_READ_PORT_ROLE                               #
#								   	    #
#  INPUT PARAMETERS : 							    #
#								   	    #
#  ATTEST_PARAM1 = bridge_instance (if supported)          		    #
#  ATTEST_PARAM2 = port_num Port Number                                     #  
#  ATTEST_PARAM3 = vlan_id (range 1-4094)                                   #  
#  ATTEST_PARAM4 = port_role                                                #  
#                                                                           #
#  DEFINITION       : Read Port State.                                      #
#		      The last command should be "NONE".     		    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#		      None.                                                 #
#									    #
#############################################################################
 
  set cli_get_record(DUT_VLAN_READ_PORT_ROLE,ATTEST_OUTPUT,1)  "show spanning-tree vlan ATTEST_PARAM3\
                                                                interface ATTEST_PARAM2"

  set cli_get_record(DUT_VLAN_READ_PORT_ROLE,ATTEST_GET,2)     "NONE"

#############################################################################
#4.                                                                         #
#  COMMAND          : Check Port Entry for MAC Address                      #
#								   	    #
#  ARRAY INDEX      : DUT_VLAN_CHECK_PORT_ENTRY                             #
#								   	    #
#  INPUT PARAMETERS : 							    #
#								   	    #
#  ATTEST_PARAM1 = bridge_instance (if supported)          		    #
#  ATTEST_PARAM2 = Mac Address                                 		    #
#  ATTEST_PARAM3 = port_num Port Number                                     #  
#  ATTEST_PARAM4 = vlan_id (range 1-4094)                                   #  
#                                                                           #
#  DEFINITION       : Checks for the specified port entry to be present for #
#                     the specified MAC address and vlan id.                #
#		      The last command should be "NONE".     		    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#		      None.                                                 #
#									    #
#############################################################################
 
  set cli_get_record(DUT_VLAN_CHECK_PORT_ENTRY,ATTEST_OUTPUT,1) "rtu_stat"

  set cli_get_record(DUT_VLAN_CHECK_PORT_ENTRY,ATTEST_GET,2)   "NONE"

#############################################################################
#5.                                                                         #
#  COMMAND          : Check Port Entry for Mcast MAC Address                #
#								   	    #
#  ARRAY INDEX      : DUT_VLAN_CHECK_PORT_MCAST_ENTRY                       #
#								   	    #
#  INPUT PARAMETERS : 							    #
#								   	    #
#  ATTEST_PARAM1 = bridge_instance (if supported)          		    #
#  ATTEST_PARAM2 = Mac Address                                 		    #
#  ATTEST_PARAM3 = port_num Port Number                                     #  
#  ATTEST_PARAM4 = vlan_id (range 1-4094)                                   #  
#                                                                           #
#  DEFINITION       : Checks for the specified port entry to be present for #
#                     the specified Mcast MAC address and vlan id.          #
#		      The last command should be "NONE".     		    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#		      None.                                                 #
#									    #
#############################################################################
 
  set cli_get_record(DUT_VLAN_CHECK_PORT_MCAST_ENTRY,ATTEST_OUTPUT,1) "rtu_stat"

  set cli_get_record(DUT_VLAN_CHECK_PORT_MCAST_ENTRY,ATTEST_GET,2)   "NONE"

#############################################################################
#6.                                                                         #
#  COMMAND          : Read Dynamic Vlan Registration Entry                  #
#								   	    #
#  ARRAY INDEX      : DUT_VLAN_CHECK_REGISTRATION_ENTRY                     #
#								   	    #
#  INPUT PARAMETERS : 							    #
#								   	    #
#  ATTEST_PARAM1 = bridge_instance (if supported)          		    #
#  ATTEST_PARAM2 = port_num Port Number                                     #  
#  ATTEST_PARAM3 = vlan_id (range 1-4094)                                   #  
#                                                                           #
#  DEFINITION       : Checks the Dynamic Vlan Registration Entry for the    #
#                     specified Port and vlan id.                           #
#		      The last command should be "NONE".     		    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#		      None.                                                 #
#									    #
#############################################################################

  set cli_get_record(DUT_VLAN_CHECK_REGISTRATION_ENTRY,ATTEST_OUTPUT,1)  "show mac address-table dynamic interface ATTEST_PARAM2"

  set cli_get_record(DUT_VLAN_CHECK_REGISTRATION_ENTRY,ATTEST_OUTPUT,2)  "NONE"

#############################################################################
#7.                                                                         #
#  COMMAND          : Read GVRP Port State.                                 #
#								   	    #
#  ARRAY INDEX      : DUT_VLAN_CHECK_GVRP_PORT_STATE                        #
#								   	    #
#  INPUT PARAMETERS : 							    #
#								   	    #
#  ATTEST_PARAM1 = bridge_instance (if supported)          		    #
#  ATTEST_PARAM2 = port_num Port Number                                     #  
#  ATTEST_PARAM3 = port_state                                               #  
#                                                                           #
#  DEFINITION       : Reads the GVRP state for the specified Port.          #
#		      The last command should be "NONE".     		    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#		      None.                                                 #
#									    #
#############################################################################

  set cli_get_record(DUT_VLAN_CHECK_GVRP_PORT_STATE,ATTEST_OUTPUT,1)  "NONE"

#############################################################################
#8.                                                                         #
#  COMMAND          : Get Protocol Group ID                                 #
#                                                                           #
#  ARRAY INDEX      : DUT_VLAN_GET_PGD_GROUP_ID                             #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  ATTEST_PARAM1 = bridge_instance (if supported)                           #
#  ATTEST_PARAM2 = frame_type                                               #
#  ATTEST_PARAM3 = protocol_type                                            #
#                                                                           #
#  DEFINITION       : Adds a Protocol to Group ID.                          #
#                     The last command should be "NONE".                    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                     None.                                                 #
#                                                                           #
#############################################################################

   set cli_get_record(DUT_VLAN_GET_PGD_GROUP_ID,ATTEST_OUTPUT,1)   "NONE"

}

### NOTE : This function has been OBSOLETED
proc  tee_vlan_get_port_base_data {} {
global cli_vlan_get_protocols_array
global vlan_frames_list
global vlan_protocols_list
set vlan_frames_list [list DUT_VLAN_ETHERNET_FRAME DUT_VLAN_LLC_OTHER_FRAME   \
                     DUT_VLAN_RFC_1042_FRAME DUT_VLAN_SNAP_OTHER_FRAME  \
                     DUT_VLAN_SNAP_8021H_FRAME DUT_VLAN_SAP_FRAME]

set vlan_protocols_list [list DUT_VLAN_IPV4 DUT_VLAN_IPV6 \
                        DUT_VLAN_ARP_PKT DUT_VLAN_APPLETALK DUT_VLAN_SNA  \
                        DUT_VLAN_IPX DUT_VLAN_LAT DUT_VLAN_NBS DUT_VLAN_RARP_PKT DUT_VLAN_PPPOE]

### 1a. Type Ethernet ###
   set cli_vlan_get_protocols_array(DUT_VLAN_ETHERNET_FRAME,DUT_VLAN_IPV4,SUPPORT)    "YES"
   set cli_vlan_get_protocols_array(DUT_VLAN_ETHERNET_FRAME,DUT_VLAN_IPV4,FRAME)      "Ethernet_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_ETHERNET_FRAME,DUT_VLAN_IPV4,PROTOCOL)   "ipv4"
  
## 1b. 
   set cli_vlan_get_protocols_array(DUT_VLAN_ETHERNET_FRAME,DUT_VLAN_IPV6,SUPPORT)    "YES"
   set cli_vlan_get_protocols_array(DUT_VLAN_ETHERNET_FRAME,DUT_VLAN_IPV6,FRAME)      "Ethernet_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_ETHERNET_FRAME,DUT_VLAN_IPV6,PROTOCOL)   "ipv6"
   
## 1c. 
   set cli_vlan_get_protocols_array(DUT_VLAN_ETHERNET_FRAME,DUT_VLAN_ARP_PKT,SUPPORT)    "YES"
   set cli_vlan_get_protocols_array(DUT_VLAN_ETHERNET_FRAME,DUT_VLAN_ARP_PKT,FRAME)      "Ethernet_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_ETHERNET_FRAME,DUT_VLAN_ARP_PKT,PROTOCOL)   "arp"
   
## 1c.a 
   set cli_vlan_get_protocols_array(DUT_VLAN_ETHERNET_FRAME,DUT_VLAN_RARP_PKT,SUPPORT)    "YES"
   set cli_vlan_get_protocols_array(DUT_VLAN_ETHERNET_FRAME,DUT_VLAN_RARP_PKT,FRAME)      "Ethernet_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_ETHERNET_FRAME,DUT_VLAN_RARP_PKT,PROTOCOL)   "rarp"
## 1d. 
   set cli_vlan_get_protocols_array(DUT_VLAN_ETHERNET_FRAME,DUT_VLAN_APPLETALK,SUPPORT)    "YES"
   set cli_vlan_get_protocols_array(DUT_VLAN_ETHERNET_FRAME,DUT_VLAN_APPLETALK,FRAME)      "Ethernet_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_ETHERNET_FRAME,DUT_VLAN_APPLETALK,PROTOCOL)   "appletalk"
   
## 1e. 
   set cli_vlan_get_protocols_array(DUT_VLAN_ETHERNET_FRAME,DUT_VLAN_SNA,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_ETHERNET_FRAME,DUT_VLAN_SNA,FRAME)      "Ethernet_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_ETHERNET_FRAME,DUT_VLAN_SNA,PROTOCOL)   "sna"
   
## 1f. 
   set cli_vlan_get_protocols_array(DUT_VLAN_ETHERNET_FRAME,DUT_VLAN_IPX,SUPPORT)    "YES"
   set cli_vlan_get_protocols_array(DUT_VLAN_ETHERNET_FRAME,DUT_VLAN_IPX,FRAME)      "Ethernet_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_ETHERNET_FRAME,DUT_VLAN_IPX,PROTOCOL)   "ipx"
   
## 1g. 
   set cli_vlan_get_protocols_array(DUT_VLAN_ETHERNET_FRAME,DUT_VLAN_LAT,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_ETHERNET_FRAME,DUT_VLAN_LAT,FRAME)      "Ethernet_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_ETHERNET_FRAME,DUT_VLAN_LAT,PROTOCOL)   "decnet"
   
## 1h. 
   set cli_vlan_get_protocols_array(DUT_VLAN_ETHERNET_FRAME,DUT_VLAN_NBS,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_ETHERNET_FRAME,DUT_VLAN_NBS,FRAME)      "Ethernet_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_ETHERNET_FRAME,DUT_VLAN_NBS,PROTOCOL)   "netbeui"
   
### 2a. LLC_OTHER FRAME ###
   set cli_vlan_get_protocols_array(DUT_VLAN_LLC_OTHER_FRAME,DUT_VLAN_IPV4,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_LLC_OTHER_FRAME,DUT_VLAN_IPV4,FRAME)      "llc_others_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_LLC_OTHER_FRAME,DUT_VLAN_IPV4,PROTOCOL)   "IpV4"
  
## 2b. 
   set cli_vlan_get_protocols_array(DUT_VLAN_LLC_OTHER_FRAME,DUT_VLAN_IPV6,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_LLC_OTHER_FRAME,DUT_VLAN_IPV6,FRAME)      "llc_others_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_LLC_OTHER_FRAME,DUT_VLAN_IPV6,PROTOCOL)   "IpV6"
   
## 2c. 
   set cli_vlan_get_protocols_array(DUT_VLAN_LLC_OTHER_FRAME,DUT_VLAN_ARP_PKT,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_LLC_OTHER_FRAME,DUT_VLAN_ARP_PKT,FRAME)      "llc_others_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_LLC_OTHER_FRAME,DUT_VLAN_ARP_PKT,PROTOCOL)   "arp"
   
## 2c.a 
   set cli_vlan_get_protocols_array(DUT_VLAN_LLC_OTHER_FRAME,DUT_VLAN_RARP_PKT,SUPPORT)    "YES"
   set cli_vlan_get_protocols_array(DUT_VLAN_LLC_OTHER_FRAME,DUT_VLAN_RARP_PKT,FRAME)      "llc_others_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_LLC_OTHER_FRAME,DUT_VLAN_RARP_PKT,PROTOCOL)   "rarp"
## 2d. 
   set cli_vlan_get_protocols_array(DUT_VLAN_LLC_OTHER_FRAME,DUT_VLAN_APPLETALK,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_LLC_OTHER_FRAME,DUT_VLAN_APPLETALK,FRAME)      "llc_others_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_LLC_OTHER_FRAME,DUT_VLAN_APPLETALK,PROTOCOL)   "appletalk"
   
## 2e. 
   set cli_vlan_get_protocols_array(DUT_VLAN_LLC_OTHER_FRAME,DUT_VLAN_SNA,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_LLC_OTHER_FRAME,DUT_VLAN_SNA,FRAME)      "llc_others_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_LLC_OTHER_FRAME,DUT_VLAN_SNA,PROTOCOL)   "sna"
   
## 2f. 
   set cli_vlan_get_protocols_array(DUT_VLAN_LLC_OTHER_FRAME,DUT_VLAN_IPX,SUPPORT)    "YES"
   set cli_vlan_get_protocols_array(DUT_VLAN_LLC_OTHER_FRAME,DUT_VLAN_IPX,FRAME)      "llc_others_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_LLC_OTHER_FRAME,DUT_VLAN_IPX,PROTOCOL)   "ipx"
   
## 2g. 
   set cli_vlan_get_protocols_array(DUT_VLAN_LLC_OTHER_FRAME,DUT_VLAN_LAT,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_LLC_OTHER_FRAME,DUT_VLAN_LAT,FRAME)      "llc_others_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_LLC_OTHER_FRAME,DUT_VLAN_LAT,PROTOCOL)   "decnet"
   
## 2h. 
   set cli_vlan_get_protocols_array(DUT_VLAN_LLC_OTHER_FRAME,DUT_VLAN_NBS,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_LLC_OTHER_FRAME,DUT_VLAN_NBS,FRAME)      "llc_others_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_LLC_OTHER_FRAME,DUT_VLAN_NBS,PROTOCOL)   "netbeui"
   
### 3a. RFC 1042 FRAME###
   set cli_vlan_get_protocols_array(DUT_VLAN_RFC_1042_FRAME,DUT_VLAN_IPV4,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_RFC_1042_FRAME,DUT_VLAN_IPV4,FRAME)      "1042_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_RFC_1042_FRAME,DUT_VLAN_IPV4,PROTOCOL)   "IpV4"
  
## 3b. 
   set cli_vlan_get_protocols_array(DUT_VLAN_RFC_1042_FRAME,DUT_VLAN_IPV6,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_RFC_1042_FRAME,DUT_VLAN_IPV6,FRAME)      "1042_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_RFC_1042_FRAME,DUT_VLAN_IPV6,PROTOCOL)   "IpV6"
   
## 3c. 
   set cli_vlan_get_protocols_array(DUT_VLAN_RFC_1042_FRAME,DUT_VLAN_ARP_PKT,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_RFC_1042_FRAME,DUT_VLAN_ARP_PKT,FRAME)      "1042_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_RFC_1042_FRAME,DUT_VLAN_ARP_PKT,PROTOCOL)   "arp"
   
## 3c.a 
   set cli_vlan_get_protocols_array(DUT_VLAN_RFC_1042_FRAME,DUT_VLAN_RARP_PKT,SUPPORT)    "YES"
   set cli_vlan_get_protocols_array(DUT_VLAN_RFC_1042_FRAME,DUT_VLAN_RARP_PKT,FRAME)      "1042_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_RFC_1042_FRAME,DUT_VLAN_RARP_PKT,PROTOCOL)   "rarp"
## 3d. 
   set cli_vlan_get_protocols_array(DUT_VLAN_RFC_1042_FRAME,DUT_VLAN_APPLETALK,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_RFC_1042_FRAME,DUT_VLAN_APPLETALK,FRAME)      "1042_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_RFC_1042_FRAME,DUT_VLAN_APPLETALK,PROTOCOL)   "appletalk"
   
## 3e. 
   set cli_vlan_get_protocols_array(DUT_VLAN_RFC_1042_FRAME,DUT_VLAN_SNA,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_RFC_1042_FRAME,DUT_VLAN_SNA,FRAME)      "1042_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_RFC_1042_FRAME,DUT_VLAN_SNA,PROTOCOL)   "sna"
   
## 3f. 
   set cli_vlan_get_protocols_array(DUT_VLAN_RFC_1042_FRAME,DUT_VLAN_IPX,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_RFC_1042_FRAME,DUT_VLAN_IPX,FRAME)      "1042_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_RFC_1042_FRAME,DUT_VLAN_IPX,PROTOCOL)   "ipx"
   
## 3g. 
   set cli_vlan_get_protocols_array(DUT_VLAN_RFC_1042_FRAME,DUT_VLAN_LAT,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_RFC_1042_FRAME,DUT_VLAN_LAT,FRAME)      "1042_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_RFC_1042_FRAME,DUT_VLAN_LAT,PROTOCOL)   "decnet"
   
## 3h. 
   set cli_vlan_get_protocols_array(DUT_VLAN_RFC_1042_FRAME,DUT_VLAN_NBS,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_RFC_1042_FRAME,DUT_VLAN_NBS,FRAME)      "1042_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_RFC_1042_FRAME,DUT_VLAN_NBS,PROTOCOL)   "netbeui"
 
### 4a. SNAP_OTHER_FRAME###
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_OTHER_FRAME,DUT_VLAN_IPV4,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_OTHER_FRAME,DUT_VLAN_IPV4,FRAME)      "snap_other_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_OTHER_FRAME,DUT_VLAN_IPV4,PROTOCOL)   "IpV4"
  
## 4b. 
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_OTHER_FRAME,DUT_VLAN_IPV6,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_OTHER_FRAME,DUT_VLAN_IPV6,FRAME)      "snap_other_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_OTHER_FRAME,DUT_VLAN_IPV6,PROTOCOL)   "IpV6"
   
## 4c. 
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_OTHER_FRAME,DUT_VLAN_ARP_PKT,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_OTHER_FRAME,DUT_VLAN_ARP_PKT,FRAME)      "snap_other_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_OTHER_FRAME,DUT_VLAN_ARP_PKT,PROTOCOL)   "arp"
   
## 4c.a 
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_OTHER_FRAME,DUT_VLAN_RARP_PKT,SUPPORT)    "YES"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_OTHER_FRAME,DUT_VLAN_RARP_PKT,FRAME)      "snap_other_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_OTHER_FRAME,DUT_VLAN_RARP_PKT,PROTOCOL)   "rarp"
## 4d. 
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_OTHER_FRAME,DUT_VLAN_APPLETALK,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_OTHER_FRAME,DUT_VLAN_APPLETALK,FRAME)      "snap_other_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_OTHER_FRAME,DUT_VLAN_APPLETALK,PROTOCOL)   "appletalk"
   
## 4e. 
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_OTHER_FRAME,DUT_VLAN_SNA,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_OTHER_FRAME,DUT_VLAN_SNA,FRAME)      "snap_other_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_OTHER_FRAME,DUT_VLAN_SNA,PROTOCOL)   "sna"
   
## 4f. 
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_OTHER_FRAME,DUT_VLAN_IPX,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_OTHER_FRAME,DUT_VLAN_IPX,FRAME)      "snap_other_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_OTHER_FRAME,DUT_VLAN_IPX,PROTOCOL)   "ipx"
   
## 4g. 
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_OTHER_FRAME,DUT_VLAN_LAT,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_OTHER_FRAME,DUT_VLAN_LAT,FRAME)      "snap_other_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_OTHER_FRAME,DUT_VLAN_LAT,PROTOCOL)   "decnet"
   
## 4h. 
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_OTHER_FRAME,DUT_VLAN_NBS,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_OTHER_FRAME,DUT_VLAN_NBS,FRAME)      "snap_other_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_OTHER_FRAME,DUT_VLAN_NBS,PROTOCOL)   "netbeui"
 
### 5a. SNAP_8021H_FRAME###
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_8021H_FRAME,DUT_VLAN_IPV4,SUPPORT)    "YES"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_8021H_FRAME,DUT_VLAN_IPV4,FRAME)      "8021h_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_8021H_FRAME,DUT_VLAN_IPV4,PROTOCOL)   "IpV4"
  
## 5b. 
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_8021H_FRAME,DUT_VLAN_IPV6,SUPPORT)    "YES"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_8021H_FRAME,DUT_VLAN_IPV6,FRAME)      "8021h_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_8021H_FRAME,DUT_VLAN_IPV6,PROTOCOL)   "IpV6"
   
## 5c. 
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_8021H_FRAME,DUT_VLAN_ARP_PKT,SUPPORT)    "YES"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_8021H_FRAME,DUT_VLAN_ARP_PKT,FRAME)      "8021h_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_8021H_FRAME,DUT_VLAN_ARP_PKT,PROTOCOL)   "arp"
   
## 5c.a 
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_8021H_FRAME,DUT_VLAN_RARP_PKT,SUPPORT)    "YES"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_8021H_FRAME,DUT_VLAN_RARP_PKT,FRAME)      "8021h_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_8021H_FRAME,DUT_VLAN_RARP_PKT,PROTOCOL)   "rarp"
   
## 5d. 
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_8021H_FRAME,DUT_VLAN_APPLETALK,SUPPORT)    "YES"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_8021H_FRAME,DUT_VLAN_APPLETALK,FRAME)      "8021h_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_8021H_FRAME,DUT_VLAN_APPLETALK,PROTOCOL)   "appletalk"
   
## 5e. 
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_8021H_FRAME,DUT_VLAN_SNA,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_8021H_FRAME,DUT_VLAN_SNA,FRAME)      "8021h_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_8021H_FRAME,DUT_VLAN_SNA,PROTOCOL)   "sna"
   
## 5f. 
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_8021H_FRAME,DUT_VLAN_IPX,SUPPORT)    "YES"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_8021H_FRAME,DUT_VLAN_IPX,FRAME)      "8021h_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_8021H_FRAME,DUT_VLAN_IPX,PROTOCOL)   "ipx"
   
## 5g. 
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_8021H_FRAME,DUT_VLAN_LAT,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_8021H_FRAME,DUT_VLAN_LAT,FRAME)      "8021h_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_8021H_FRAME,DUT_VLAN_LAT,PROTOCOL)   "decnet"
   
## 5h. 
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_8021H_FRAME,DUT_VLAN_NBS,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_8021H_FRAME,DUT_VLAN_NBS,FRAME)      "8021h_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_SNAP_8021H_FRAME,DUT_VLAN_NBS,PROTOCOL)   "netbeui"
 
### 6a. SAP_FRAME###
   set cli_vlan_get_protocols_array(DUT_VLAN_SAP_FRAME,DUT_VLAN_IPV4,SUPPORT)    "YES"
   set cli_vlan_get_protocols_array(DUT_VLAN_SAP_FRAME,DUT_VLAN_IPV4,FRAME)      "sap_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_SAP_FRAME,DUT_VLAN_IPV4,PROTOCOL)   "IpV4"
  
## 6b. 
   set cli_vlan_get_protocols_array(DUT_VLAN_SAP_FRAME,DUT_VLAN_IPV6,SUPPORT)    "YES"
   set cli_vlan_get_protocols_array(DUT_VLAN_SAP_FRAME,DUT_VLAN_IPV6,FRAME)      "sap_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_SAP_FRAME,DUT_VLAN_IPV6,PROTOCOL)   "IpV6"
   
## 6c. 
   set cli_vlan_get_protocols_array(DUT_VLAN_SAP_FRAME,DUT_VLAN_ARP_PKT,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_SAP_FRAME,DUT_VLAN_ARP_PKT,FRAME)      "sap_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_SAP_FRAME,DUT_VLAN_ARP_PKT,PROTOCOL)   "arp"
   
## 6c.a 
   set cli_vlan_get_protocols_array(DUT_VLAN_SAP_FRAME,DUT_VLAN_RARP_PKT,SUPPORT)    "YES"
   set cli_vlan_get_protocols_array(DUT_VLAN_SAP_FRAME,DUT_VLAN_RARP_PKT,FRAME)      "sap_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_SAP_FRAME,DUT_VLAN_RARP_PKT,PROTOCOL)   "rarp"
## 6d. 
   set cli_vlan_get_protocols_array(DUT_VLAN_SAP_FRAME,DUT_VLAN_APPLETALK,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_SAP_FRAME,DUT_VLAN_APPLETALK,FRAME)      "sap_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_SAP_FRAME,DUT_VLAN_APPLETALK,PROTOCOL)   "appletalk"
   
## 6e. 
   set cli_vlan_get_protocols_array(DUT_VLAN_SAP_FRAME,DUT_VLAN_SNA,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_SAP_FRAME,DUT_VLAN_SNA,FRAME)      "sap_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_SAP_FRAME,DUT_VLAN_SNA,PROTOCOL)   "sna"
   
## 6f. 
   set cli_vlan_get_protocols_array(DUT_VLAN_SAP_FRAME,DUT_VLAN_IPX,SUPPORT)    "YES"
   set cli_vlan_get_protocols_array(DUT_VLAN_SAP_FRAME,DUT_VLAN_IPX,FRAME)      "sap_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_SAP_FRAME,DUT_VLAN_IPX,PROTOCOL)   "ipx"
   
## 6g. 
   set cli_vlan_get_protocols_array(DUT_VLAN_SAP_FRAME,DUT_VLAN_LAT,SUPPORT)    "NO"
   set cli_vlan_get_protocols_array(DUT_VLAN_SAP_FRAME,DUT_VLAN_LAT,FRAME)      "sap_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_SAP_FRAME,DUT_VLAN_LAT,PROTOCOL)   "decnet"
   
## 6h. 
   set cli_vlan_get_protocols_array(DUT_VLAN_SAP_FRAME,DUT_VLAN_NBS,SUPPORT)    "YES"
   set cli_vlan_get_protocols_array(DUT_VLAN_SAP_FRAME,DUT_VLAN_NBS,FRAME)      "sap_frame"
   set cli_vlan_get_protocols_array(DUT_VLAN_SAP_FRAME,DUT_VLAN_NBS,PROTOCOL)   "netbeui"

}

#############################################################################
#9.                                                                         #
#  COMMAND          : Read WRS_VLANS associated port list.                  #
#								   	    #
#  ARRAY INDEX      : DUT_VLAN_CHECK_ASSOCIATED_PORTS                       #
#								   	    #
#  INPUT PARAMETERS : 							    #
#								   	    #
#  ATTEST_PARAM1 = bridge_instance (if supported)          		    #
#  ATTEST_PARAM2 = port_num Port Number                                     #  
#  ATTEST_PARAM3 = port_state                                               #  
#                                                                           #
#  DEFINITION       : Reads the GVRP state for the specified Port.          #
#		      The last command should be "NONE".     		    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#		      None.                                                 #
#									    #
#############################################################################

  set cli_get_record(DUT_VLAN_CHECK_ASSOCIATED_PORTS,ATTEST_OUTPUT,1)  "wrs_vlans --list"

  set cli_get_record(DUT_VLAN_CHECK_ASSOCIATED_PORTS,ATTEST_OUTPUT,2)  "NONE"

#############################################################################
#	         CALLBACK Functions For GET Operation			    #
#############################################################################



#############################################################################
#1a.  							   	            #
#  PROCEDURE NAME   : callback_vlan_get_bridge_id_stp_mode                  #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  bridge_instance  : (Optional)  Bridge Instance.                          #
#  vlan_id          : (Optional)  Vlan-ID (Default Value 1)                 #
#  buffer           : (Mandatory) Output buffer of show commands.           #
#                                                                           #
#  DEFINITION       : Adds a Bridge at DUT in STP mode.                     #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                                                                           #
#  dut_bridgeID     : (Mandatory) Bridge ID                                 #
#                                                                           #
#  RETURNS          : 1 on success                                          #
#                     0 on failure                                          #
#                     2 if the ATTEST has to do the parsing                 #
#                                                                           #
#############################################################################

proc callback_vlan_get_bridgeID_stp_mode {args} {

  array set param $args
  global env

  set bridge_instance $param(-bridge_instance)
  set vlan_id         $param(-vlan_id)
  set priority        $param(-priority)
  set buffer          $param(-buffer)
  upvar $param(-dut_bridge_id) dut_bridge_id


#The dut_bridge_id is the concatenation of priority and mac address of the DUT.#
#Priority is an input parameter from the testcase and mac address is an        #
#environmentel varaiable from the tee_env_set.tcl.                             #

#  set dut_bridge_id $priority/$env(DUT_MAC_ADDRESS)
#  return 1

  return 2
}

#############################################################################
#1b. 							   	            #
#  PROCEDURE NAME   : callback_vlan_get_bridge_id_rstp_mode                 #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  bridge_instance  : (Optional)  Bridge Instance.                          #
#  vlan_id          : (Optional)  Vlan-ID (Default Value 1)                 #
#  buffer           : (Mandatory) Output buffer of show commands.           #
#                                                                           #
#  DEFINITION       : Adds a Bridge at DUT in RSTP mode.                    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                                                                           #
#  dut_bridgeID     : (Mandatory) Bridge ID                                 #
#                                                                           #
#  RETURNS          : 1 on success                                          #
#                     0 on failure                                          #
#                     2 if the ATTEST has to do the parsing                 #
#                                                                           #
#############################################################################

proc callback_vlan_get_bridgeID_rstp_mode {args} {

  array set param $args
  global env

  set bridge_instance $param(-bridge_instance)
  set vlan_id         $param(-vlan_id)
  set priority        $param(-priority)
  set buffer          $param(-buffer)
  upvar $param(-dut_bridge_id) dut_bridge_id

#The dut_bridge_id is the concatenation of priority and mac address of the DUT.#
#Priority is an input parameter from the testcase and mac address is an        #
#environmentel varaiable from the tee_env_set.tcl.                             #

#  set dut_bridge_id $priority/$env(DUT_MAC_ADDRESS)
#  return 1

  return 2

}

#############################################################################
#1c.							   	            #
#  PROCEDURE NAME   : callback_vlan_get_bridge_id_mstp_mode                 #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  bridge_instance  : (Optional)  Bridge Instance.                          #
#  vlan_id          : (Optional)  Vlan-ID (Default Value 1)                 #
#  buffer           : (Mandatory) Output buffer of show commands.           #
#                                                                           #
#  DEFINITION       : Add a Bridge on the DUT in Mstp mode.                 #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                                                                           #
#  dut_bridgeID     : (Mandatory) Bridge ID                                 #
#                                                                           #
#  RETURNS          : 1 on success                                          #
#                     0 on failure                                          #
#                     2 if the ATTEST has to do the parsing                 #
#                                                                           #
#############################################################################

proc callback_vlan_get_bridgeID_mstp_mode {args} {

  array set param $args
  global env

  set bridge_instance $param(-bridge_instance)
  set vlan_id         $param(-vlan_id)
  set priority        $param(-priority)
  set buffer          $param(-buffer)
  upvar $param(-dut_bridge_id) dut_bridge_id

#The dut_bridge_id is the concatenation of priority and mac address of the DUT.#
#Priority is an input parameter from the testcase and mac address is an        #
#environmentel varaiable from the tee_env_set.tcl.                             #

#  set dut_bridge_id $priority/$env(DUT_MAC_ADDRESS)
#  return 1
            set result \
            [regexp {(MST00)([\s]+)([0-9a-zA-Z]+)(\.)(([0-9a-zA-Z]+)(\.)([0-9a-zA-Z]+)(\.)([0-9a-zA-Z]+))}\
                        $buffer ful mst spa priority dot mac_addr]
            if {$result == 1} {
#               puts "cli_rstp_get_bridge_id  BRIDGE instance present"
                set dut_bridge_id [format "%d" 0x[string trim $priority]]
               set mac1  [lindex [split $mac_addr . ] 0]
               set mac2  [lindex [split $mac_addr . ] 1]
               set mac3  [lindex [split $mac_addr . ] 2]
               append dut_bridge_id "/"
               append dut_bridge_id [string range $mac1 0 1]
               append dut_bridge_id ":"
               append dut_bridge_id [string range $mac1 2 3]
               append dut_bridge_id ":"
               append dut_bridge_id [string range $mac2 0 1]
               append dut_bridge_id ":"
               append dut_bridge_id [string range $mac2 2 3]
               append dut_bridge_id ":"
               append dut_bridge_id [string range $mac3 0 1]
               append dut_bridge_id ":"
               append dut_bridge_id [string range $mac3 2 3]

               return 1 
            } else  {
                    return 0
            }

}

#############################################################################
#2.								   	    #
#  PROCEDURE NAME   : callback_vlan_read_port_state                         #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  bridge_instance  : (Optional)  Bridge Instance.                          #
#  vlan_id          : (Optional)  VLAN Identifier (Default Value 1).        #
#                                 When not specified default PVID=1 is      #
#                                 assumed.                                  #
#  port_num         : (Mandatory) Port Number.                              #
#  port_state       : (Mandatory) Port State.                               #
#  buffer           : (Mandatory) Output buffer of show commands.           #
#                                                                           #
#  DEFINITION       : Read Port State.                                      #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                                                                           #
#  NIL                                                                      #
#                                                                           #
#  RETURNS          : 1 on success                                          #
#                     0 on failure                                          #
#                     2 if the ATTEST has to do the parsing                 #
#                                                                           #
#############################################################################

proc callback_vlan_read_port_state {args} {

  array set param $args
  set bridge_instance $param(-bridge_instance)
  set buffer          $param(-buffer)
  set port_num        $param(-port_num)
  set port_state      $param(-port_state)
  set vlan_id         $param(-vlan_id)
 
  return 1 
}

#############################################################################
#3.								   	    #
#  PROCEDURE NAME   : callback_vlan_read_port_role            		    #
#								   	    #
#  INPUT PARAMETERS : 							    #
#                                                                           #
#  bridge_instance     : (Optional)  Bridge Instance (Default Value 0)      #
#  vlan_id             : (Optional)  VLAN Identifier (Default Value 1).     #
#  port_num            : (Mandatory) Port Number.                           #
#  port_role           : (Mandatory) Port Role.                             #
#  buffer              : (Mandatory) Output buffer of show commands.        #
#                                                                           #
#  DEFINITION       : Read Port State.                                      #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                                                                           #
#     NIL                                                                   #
#                                                                           #
#  RETURNS          : 1 on success     			     		    #
#                     0 on failure     			     		    #
#                     2 if the ATTEST has to do the parsing                 #
#									    #
#############################################################################

proc callback_vlan_read_port_role {args} {

  array set param $args
  set bridge_instance $param(-bridge_instance)
  set vlan_id         $param(-vlan_id)
  set port_num        $param(-port_num)
  set port_role       $param(-port_role)
  set buffer          $param(-buffer)
 
  return 2
}

#############################################################################
#4.								   	    #
#  PROCEDURE NAME   : callback_vlan_check_portentry_for_macaddress          #
#								   	    #
#  INPUT PARAMETERS : 							    #
#                                                                           #
#  bridge_instance    : (Optional)  Bridge Instance (Default Value 0)       #
#  port_num           : (Mandatory) Port Number                             #
#  vlan_id            : (Mandatory  VLAN Identifier                         #
#  mac_address        : (Mandatory) Mac Address                             #
#  buffer             : (Mandatory) Output buffer of show commands.         #
#                                                                           #
#  DEFINITION       : Checks for the specified port entry to be present for #
#                     the specified MAC address and vlan id.                #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                                                                           #
#     NIL                                                                   #
#                                                                           #
#  RETURNS          : 1 on success     			     		    #
#                     0 on failure     			     		    #
#                     2 if the ATTEST has to do the parsing                 #
#									    #
#############################################################################

proc callback_vlan_check_portentry_for_macaddress {args} {

  array set param $args
  set bridge_instance $param(-bridge_instance)
  set port_num    $param(-port_num)
  set vlan_id     $param(-vlan_id)
  set mac_address $param(-mac_address)
  set buffer      $param(-buffer)

  set port [regexp -all {[0-9]+} $port_num Port]
  set port_num $Port
  set mac_address [vlan_convert_dutmac_to_userMac -macaddress $mac_address]
  set bridge_instance 0
  set splitbuffer [split $buffer \n]
  set ReqLines ""
  set chanceToPass ""
  set flag 0
  foreach line $splitbuffer {
      if {[regexp $mac_address $line]} {
          lappend ReqLines $line
      }
  }
  foreach line $ReqLines {
    set flag 1
     if {([lindex $line 1] == $port_num) && ([lindex $line 2] == $vlan_id)} {
       lappend chanceToPass 1
     } else {
       lappend chanceToPass 0
     }
  }
  if {$flag == 1} {
     if {[lsearch $chanceToPass 1] != -1} {
        return 1
     } else {
        return 0
     }
  }

return 0
}



#############################################################################
#5.								   	    #
#  PROCEDURE NAME   : callback_vlan_check_portentry_for_mcast_macaddress    #
#								   	    #
#  INPUT PARAMETERS : 							    #
#                                                                           #
#  bridge_instance  : (Optional)  Bridge Instance (Default Value 0)         #
#  port_num         : (Mandatory) Port Number                               #
#  vlan_id          : (Mandatory) Vlan ID                                   #
#  mac_address      : (Mandatory) Mac Address                               #
#  buffer           : (Mandatory) Output buffer of show commands.           #
#                                                                           #
#  DEFINITION       : Checks for the specified port entry to be present for #
#                     the specified Mcast MAC address and vlan id.          #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                                                                           #
#     NIL                                                                   #
#                                                                           #
#                                                                           #
#  RETURNS          : 1 on success     			     		    #
#                     0 on failure     			     		    #
#                     2 if the ATTEST has to do the parsing                 #
#									    #
#############################################################################

proc callback_vlan_check_portentry_for_mcast_macaddress {args} {

  array set param $args
  
   set bridge_instance $param(-bridge_instance)
   set port_num        $param(-port_num)
   set vlan_id         $param(-vlan_id)
   set mac_address     $param(-mac_address)
   set buffer          $param(-buffer)

   return 2
}


#############################################################################
#6.								   	    #
#  PROCEDURE NAME   : callback_vlan_check_gvrp_port_state     		    #
#								   	    #
#  INPUT PARAMETERS : 							    #
#  bridge_instance  : (Optional) Bridge Instance (Default Value 0)          #
#  port_num         : (Mandatory)Port Number                                #
#  state            : (Mandatory)GVRP state                                 #
#  buffer           : (Mandatory) Output buffer of show commands.           #
#                                                                           #
#  DEFINITION       : Reads the GVRP state for the specified Port.          #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                                                                           #
#    NIL                                                                    #
#                                                                           #
#  RETURNS          : 1 on success     			     		    #
#                     0 on failure     			     		    #
#                     2 if the ATTEST has to do the parsing                 #
#									    #
#############################################################################

proc callback_vlan_check_gvrp_port_state {args} {

  array set param $args
  set bridge_instance $param(-bridge_instance)
  set port_num        $param(-port_num)
  set state           $param(-state)
  set buffer          $param(-buffer)

  return 2
  if [regexp $gvrp_state $buffer ] {
	return 1
  } else {
	return 0
  }
}


#############################################################################
#7.								   	    #
#  PROCEDURE NAME   : callback_vlan_check_vlan_registration_entry           #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#  bridge_instance  : (Optional) Bridge Instance (Default Value 0)          #
#  port_num         : (Mandatory)Port Number                                #
#  vlan_id          : (Mandatory) VLAN ID.                                  #
#  buffer           : (Mandatory) Output buffer of show commands            #
#                                                                           #
#  DEFINITION       : Checks the Dynamic Vlan Registration Entry for the    #
#                     specified Port and vlan id.                           #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                                                                           #
#                     NIL.                                                  #
#                                                                           #
#  RETURNS          : 1 on success                                          #
#                     0 on failure                                          #
#                     2 if the ATTEST has to do the parsing                 #
#                                                                           #
#############################################################################

proc callback_vlan_check_vlan_registration_entry {args} {

  array set param $args
  set bridge_instance $param(-bridge_instance)
  set buffer $param(-buffer)
  set port_num $param(-port_num)
  set vlan_id $param(-vlan_id)

  return 2
  if [regexp $vlan_id $buffer ] {
	return 1
  } else {
	return 0
  }
}

###############################################################################
#8.                                                                           #
#  PROCEDURE NAME     : callback_vlan_get_pgd_group_id                        #
#                                                                             #
#  INPUT PARAMETERS   :                                                       #
#                                                                             #
#  session_handler   : (Mandatory) Session-ID of the DUT                      #
#  bridge_instance   : (Mandatory) Bridge Instance (Default Value 0)          #
#  frame_type        : (Mandatory) Frames Type.                               #
#  protocol_type     : (Mandatory) Protocol Type.                             #
#                                                                             #
#  DEFINITION       : Adds a Protocol to Group ID.                            #
#                                                                             #
#  OUTPUT PARAMETERS:                                                         #
#  group_id          : Group Id.                                              #
#                                                                             #
#  RETURNS          : 1 on success     			     		      #
#                     0 on failure     			     		      #
#                     2 if the ATTEST has to do the parsing.                  #
#                                                                             #
###############################################################################

proc callback_vlan_get_pgd_group_id {args} {

  array set param $args

       set session_handler    $param(-session_handler)
       set bridge_instance    $param(-bridge_instance)
       set frame_type         $param(-frame_type)
       set protocol_type      $param(-protocol_type)

return 2

}
#############################################################################
#	         CALLBACK Functions For SET Operation			    #
#############################################################################


#############################################################################
#1.            						   	            #
#  PROCEDURE NAME   : callback_vlan_add_bridge                              #
#								   	    #
#  INPUT PARAMETERS : 							    #
#                                                                           #
# session_handler   : Session handler. Used by send_cli_command             #
# bridgeInstance   : (Mandatory) Bridge Instance                            #
# cmd_type          : Command Type. Used by send_cli_command                #
# priority          : Bridge priority.                                      #
# hello_time        : Hello time.                                           #
# forward_delay     : forward delay                                         #
# max_age           : Maximum Age                                           #
#                                                                           #
#                                                                           #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                                                                           #
#                     NIL.                                                  #
#                                                                           #
#  RETURNS          : 1 on success     			     		    #
#                     0 on failure     			     		    #
#                     2 if the ATTEST has to send the cli command           #
#									    #
#############################################################################

proc callback_vlan_add_bridge {args} {

  array set param $args
  global env
  set session_handler $param(-session_handler)
  set bridge_instance $param(-bridge_instance)
  set cmd_type        $param(-cmd_type)
  set priority        $param(-priority)
  set hello_time      $param(-hello_time)
  set forward_delay   $param(-forward_delay)
  set max_age         $param(-max_age)
  set vlan_id         $param(-vlan_id)
  set config_name     $env(dut_VLAN_MSTP_RG1_CONFIG_NAME)
  set revision_level  $env(dut_VLAN_MSTP_RG1_REVISION_LEVEL)


  set result [Send_cli_command -session_handler   $session_handler \
   			  -cmd_type          $cmd_type \
                          -parameter1        $bridge_instance \
                          -parameter2        $vlan_id \
                          -parameter3        $priority \
                          -parameter4        $hello_time\
                          -parameter5        $forward_delay\
                          -parameter6        $max_age\
                          -parameter7        $config_name\
                          -parameter8        $revision_level]
  return $result

}



#############################################################################
#2.                                                                         #
# PROCEDURE NAME   : callback_vlan_add_vlan                                 #
#                                                                           #
# INPUT PARAMETERS :                                                        #
#  session_handler : (Mandatory) session Id of the DUT session opened       #
#  bridge_instance : (Optional)  Bridge Instance (Default Value 0)          #
#  vlan_id         : (Mandatory) Vlan-ID                                    #
#                                                                           #
#  DEFINITION       : Adds a Vlan to a Bridge at DUT .                      #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                                                                           #
#                     NIL.                                                  #
#                                                                           #
#  RETURNS          : 1 on success     			     		    #
#                     0 on failure     			     		    #
#                     2 if the ATTEST has to send the cli command           #
#                                                                           #
#############################################################################

proc callback_vlan_add_vlan {args} {

  array set param $args
  set session_handler $param(-session_handler)
  set bridge_instance $param(-bridge_instance)
  set vlan_id         $param(-vlan_id)
  set cmd_type        $param(-cmd_type)

#    if {![Send_cli_command -session_handler   $session_handler \
#                             -cmd_type        $cmd_type\
#                             -parameter1      $bridge_instance\
#                             -parameter2      $vlan_id]} {
#
#           puts "cli_vlan_add_vlan: Error while adding vlan"
#           return 0
#     }
#     return 1

  return 2
}



#############################################################################
# 3.                                                                        #
# PROCEDURE NAME   : callback_vlan_remove_vlan                              #
#                                                                           #
# INPUT PARAMETERS :                                                        #
#  session_handler : (Mandatory) session Id of the DUT session opened       #
#  bridge_instance : (Optional)  Bridge Instance (Default Value 0)          #
#  vlan_id         : (Mandatory) Vlan-ID                                    #
#                                                                           #
#  DEFINITION       : Removes a Vlan from a Bridge at DUT .                 #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                                                                           #
#                     NIL.                                                  #
#                                                                           #
#  RETURNS          : 1 on success     			     		    #
#                     0 on failure     			     		    #
#                     2 if the ATTEST has to send the cli command           #
#                                                                           #
#############################################################################

proc callback_vlan_remove_vlan {args} {

  array set param $args
  set session_handler $param(-session_handler)
  set bridge_instance $param(-bridge_instance)
  set vlan_id         $param(-vlan_id)
  set cmd_type        $param(-cmd_type)

#    if {![Send_cli_command -session_handler   $session_handler \
#                             -cmd_type        $cmd_type\
#                             -parameter1      $bridge_instance\
#                             -parameter2      $vlan_id]} {
#
#           puts "cli_vlan_remove_vlan: Error while adding vlan"
#           return 0
#     }
#     return 1
  return 2
}




#############################################################################
# 4.                                                                        #
# PROCEDURE NAME  : callback_vlan_add_one_port                              #
#                                                                           #
# INPUT PARAMETERS:                                                         #
#                                                                           #
#  session_handler : (Mandatory) session-ID of the DUT session              #
#  bridge_instance : (Optional)  Bridge Instance (Default Value 0)          #
#  port            : (Mandatory) The port to be added on bridge             #
#  vlan_id         : (Mandatory) Vlan-ID (Default Value 1)                  #
#                                                                           #
#  DEFINITION       : Adds a Port to the Bridge at DUT.                     #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                                                                           #
#                     NIL.                                                  #
#                                                                           #
#  RETURNS          : 1 on success     			     		    #
#                     0 on failure     			     		    #
#                     2 if the ATTEST has to do the parsing                 #
#                                                                           #
#############################################################################

proc callback_vlan_add_one_port {args} {

  array set param $args
  set session_handler $param(-session_handler)
  set bridge_instance $param(-bridge_instance)
  set vlan_id         $param(-vlan_id)
  set portnum         $param(-portnum)
  set cmd_type        $param(-cmd_type)

  set bridgeInstance 0

 if {![Send_cli_command -session_handler   $session_handler \
                        -cmd_type          $cmd_type\
                        -parameter1        $bridgeInstance \
                        -parameter2        $vlan_id \
                        -parameter3        $portnum]} {

     puts "cli_vlan_add_one_port: Error while adding port"
     return 0
 }
 return 1

#  return 2
}


#############################################################################
#5a.							   	            #
#  PROCEDURE NAME   : callback_vlan_enable_one_port_mstp_mode  		    #
#								   	    #
#  INPUT PARAMETERS : 							    #
#  session_handler  : (Mandatory) Sesion-ID of the DUT session              #
#  bridge_instance  : (Optional)  bridge-Instance (Default Value 0)         #
#  vlan_id          : (optional)  Vlan-ID      (Default Value 1)            #
#  portnum          : (Mandatory) Port Number                               #
#  pathcost         : (optional)  Path-cost to be associated with thePort.  #
#  priority         : (optional)  Priority to be associated with the Port.  #
#  cmd_type         : Command Type. Used by send_cli_command                #
#                                                                           #
#  DEFINITION       : Adds a Port to the Bridge with the specified pathcost #
#                     and priority.                                         #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                                                                           #
#    NIL                                                                    #
#                                                                           #
#  RETURNS          : 1 on success     			     		    #
#                     0 on failure     			     		    #
#                     2 if the ATTEST has to do the parsing                 #
#									    #
#############################################################################

proc callback_vlan_enable_one_port_mstp_mode {args} {

  array set param $args

  set session_handler $param(-session_handler)
  set bridge_instance $param(-bridge_instance)
  set vlan_id $param(-vlan_id)
  set portnum $param(-portnum)
  set pathcost $param(-pathcost)
  set priority $param(-priority)
  set cmd_type $param(-cmd_type)

#    if {![Send_cli_command -session_handler $session_handler \
#                         -cmd_type          $cmd_type\
#                         -parameter1        $bridgeInstance \
#                         -parameter2        $vlan_id \
#                         -parameter3        $portnum \
#                         -parameter4        $priority \
#                         -parameter5        $pathcost]} {
#
#      puts "cli_vlan_enable_one_port: Error while adding port"
#      return 0
#    }
#  return 1

  return 2

}

#############################################################################
#5b.								   	    #
#  PROCEDURE NAME   : callback_vlan_enable_one_port_stp_mode  		    #
#								   	    #
#  INPUT PARAMETERS : 							    #
#  session_handler  : (Mandatory) Sesion-ID of the DUT session              #
#  bridge_instance  : (Optional)  bridge-Instance (Default Value 0)         #
#  vlan_id          : (optional)  Vlan-ID      (Default Value 1)            #
#  portnum          : (Mandatory) Port Number                               #
#  pathcost         : (optional)  Path-cost to be associated with thePort.  #
#  priority         : (optional)  Priority to be associated with the Port.  #
#  cmd_type         : Command Type. Used by send_cli_command                #
#                                                                           #
#  DEFINITION       : Adds a Port to the Bridge with the specified pathcost #
#                     and priority.                                         #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                                                                           #
#    NIL                                                                    #
#                                                                           #
#  RETURNS          : 1 on success     			     		    #
#                     0 on failure     			     		    #
#                     2 if the ATTEST has to do the parsing                 #
#									    #
#############################################################################

proc callback_vlan_enable_one_port_stp_mode {args} {

  array set param $args

  set session_handler $param(-session_handler)
  set bridge_instance $param(-bridge_instance)
  set vlan_id $param(-vlan_id)
  set portnum $param(-portnum)
  set pathcost $param(-pathcost)
  set priority $param(-priority)
  set cmd_type $param(-cmd_type)

#    if {![Send_cli_command -session_handler $session_handler \
#                         -cmd_type          $cmd_type\
#                         -parameter1        $bridgeInstance \
#                         -parameter2        $vlan_id \
#                         -parameter3        $portnum \
#                         -parameter4        $priority \
#                         -parameter5        $pathcost]} {
#
#      puts "cli_vlan_enable_one_port: Error while adding port"
#      return 0
#    }
#  return 1

  return 2

}

#############################################################################
#5c.							   	            #
#  PROCEDURE NAME   : callback_vlan_enable_one_port_rstp_mode  		    #
#								   	    #
#  INPUT PARAMETERS : 							    #
#  session_handler  : (Mandatory) Sesion-ID of the DUT session              #
#  bridge_instance  : (Optional)  bridge-Instance (Default Value 0)         #
#  vlan_id          : (optional)  Vlan-ID      (Default Value 1)            #
#  portnum          : (Mandatory) Port Number                               #
#  pathcost         : (optional)  Path-cost to be associated with thePort.  #
#  priority         : (optional)  Priority to be associated with the Port.  #
#  cmd_type         : Command Type. Used by send_cli_command                #
#                                                                           #
#  DEFINITION       : Adds a Port to the Bridge with the specified pathcost #
#                     and priority.                                         #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                                                                           #
#    NIL                                                                    #
#                                                                           #
#  RETURNS          : 1 on success     			     		    #
#                     0 on failure     			     		    #
#                     2 if the ATTEST has to do the parsing                 #
#									    #
#############################################################################

proc callback_vlan_enable_one_port_rstp_mode {args} {

  array set param $args

  set session_handler $param(-session_handler)
  set bridge_instance $param(-bridge_instance)
  set vlan_id $param(-vlan_id)
  set portnum $param(-portnum)
  set pathcost $param(-pathcost)
  set priority $param(-priority)
  set cmd_type $param(-cmd_type)

#    if {![Send_cli_command -session_handler $session_handler \
#                         -cmd_type          $cmd_type\
#                         -parameter1        $bridgeInstance \
#                         -parameter2        $vlan_id \
#                         -parameter3        $portnum \
#                         -parameter4        $priority \
#                         -parameter5        $pathcost]} {
#
#      puts "cli_vlan_enable_one_port: Error while adding port"
#      return 0
#    }
#  return 1

  return 2

}

#############################################################################
#6.								   	    #
# PROCEDURE NAME  : callback_vlan_set_port_disable                          #
#                                                                           #
# INPUT PARAMETERS:                                                         #
#                                                                           #
#  session_handler: (Mandatory)Session-ID of the DUT                        #
#  bridge_instance: (Optional) Bridge Instance (Default Value 0)            #
#  port_num       : (Mandatory)Port location                                #
#                                                                           #
#  DEFINITION      : Makes the specified port down,i.e disables the port.   #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                                                                           #
#                     NIL.                                                  #
#                                                                           #
#  RETURNS          : 1 on success     			     		    #
#                     0 on failure     			     		    #
#                     2 if the ATTEST has to do the parsing.                #
#                                                                           #
#                                                                           #
#############################################################################

proc callback_vlan_set_port_disable {args} {

  array set param $args

  set session_handler $param(-session_handler)
  set bridge_instance $param(-bridge_instance)
  set port_num         $param(-port_num)
  set cmd_type        $param(-cmd_type)

#   if {![Send_cli_command  -session_handler   $session_handler \
#                          -cmd_type           $cmd_type\
#                          -parameter1         $bridge_instance\
#                          -parameter2         $port_num]} {
#
#        puts "cli_vlan_set_port_disable: Error while disabling port"
#        return 0
#     }
#     return 1
  return 2
}



###############################################################################
#7.       							   	      #
# PROCEDURE NAME    : callback_vlan_add_static_fwd_entry                      #
#                                                                             #
# INPUT PARAMETERS:                                                           #
#  session_handler    : (Mandatory) Sesion-ID of the DUT session              #
#  bridge_instance    : (Mandatory) Bridge Instance (Default Value 0)         #
#  vlan_id            : (Mandatory) VLAN Identifier (Default Value 1).        #
#  port_num           : (Mandatory) Port Number                               #
#  mac_address        : (Mandatory) Mac Address                               #
#                                                                             #
#  DEFINITION       : Adds a static mac-address entry at DUT for the          #
#                     specified Port and specified vlan id.                   #
#                                                                             #
#  OUTPUT PARAMETERS:                                                         #
#                                                                             #
#                     NIL.                                                    #
#                                                                             #
#  RETURNS          : 1 on success     			     		      #
#                     0 on failure     			     		      #
#                     2 if the ATTEST has to do the parsing.                  #
#                                                                             #
###############################################################################

proc callback_vlan_add_static_fwd_entry {args} {

  array set param $args

  set session_handler $param(-session_handler)
  set bridge_instance $param(-bridge_instance)
  set port_num        $param(-port_num)
  set vlan_id         $param(-vlan_id)
  set mac_address     $param(-mac_address)
  set cmd_type        $param(-cmd_type)

  set mac_address [vlan_convert_dutmac_to_userMac -macaddress $mac_address]
       set port [regexp -all {[0-9]+} $port_num Port]
       set port_num $Port
       set bridge_instance 0

      if {![Send_cli_command  -session_handler   $session_handler \
                              -cmd_type          $cmd_type\
                              -parameter1        $bridge_instance\
                              -parameter2        $port_num \
                              -parameter3        $vlan_id \
                              -parameter4        $mac_address]} {

      puts "cli_vlan_add_static_fwd_entry: Error adding static MAC forwarding entry"
      return 0
     }

     return 1

#  return 2
}



###############################################################################
#8.       							   	      #
# PROCEDURE NAME    : callback_vlan_remove_static_fwd_entry                   #
#                                                                             #
# INPUT PARAMETERS:                                                           #
#  session_handler    : (Mandatory) Sesion-ID of the DUT session              #
#  bridge_instance    : (Mandatory) Bridge Instance                           #
#  vlan_id            : (Mandatory) VLAN ID                                   #
#  port_num           : (Mandatory) Port Number                               #
#  mac_address        : (Mandatory) Mac Address                               #
#                                                                             #
#  DEFINITION       : Removes the static mcast mac-address entry at DUT for   #
#                     the specified Port and specified vlan id.               #
#                                                                             #
#  OUTPUT PARAMETERS:                                                         #
#                                                                             #
#                     NIL.                                                    #
#                                                                             #
#  RETURNS          : 1 on success     			     		      #
#                     0 on failure     			     		      #
#                     2 if the ATTEST has to do the parsing.                  #
#                                                                             #
###############################################################################

proc callback_vlan_remove_static_fwd_entry {args} {

  array set param $args

  set session_handler $param(-session_handler)
  set bridge_instance $param(-bridge_instance)
  set port_num        $param(-port_num)
  set vlan_id         $param(-vlan_id)
  set mac_address     $param(-mac_address)
  set cmd_type        $param(-cmd_type)

#      set mac_address [vlan_convert_dutmac_to_userMac -macaddress $mac_address]
#      set port [regexp -all {[0-9]+} $port_num Port]
#      set port_num $Port
#      set bridge_instance 0

     set mac_address [vlan_convert_dutmac_to_userMac -macaddress $mac_address]
     set port [regexp -all {[0-9]+} $port_num Port]
     set portslist ""
     set port_num $Port
     set bridge_instance 0
     if {$Port != 1} {
          set portInBin [string replace  [string repeat 0 [expr $Port-1]] 0 0 10]
     } else {
          set portInBin 1
     }

      Send_cli_command_to_read -session_handler   $session_handler \
                           -cmd_type          "DUT_VLAN_READ_PORT_STATE"\
                           -parameter1        $bridge_instance \
                           -parameter2        $vlan_id \
                           -output            buffer

     set reqline ""
     set spliting [split $buffer \n]
     foreach line $spliting {
        if {[regexp $mac_address $line]} {
           lappend reqline $line
        }
     }    
            if {$reqline != ""} {
            foreach line $reqline {
            if {([llength $line] == 7) && ($mac_address == [lindex $line 0])} {
            if {([lindex $line 2] == $vlan_id) && ($mac_address == [lindex $line 0])} {
               set portToDec [wrsbin2dec $portInBin]
            }
            } else {
               if {$mac_address == [lindex $line 0]} {
                    set linelength [llength $line]
                    set subActualLength [expr $linelength-7]
                    if {([lindex $line [expr $subActualLength+2]]) == $vlan_id} {
                       for {set i 1} {$i <= [expr $subActualLength+1]} {incr i} {
                          lappend portslist [lindex $line $i]
                       }
                       set appendedlist 0
                       foreach plist $portslist {
                       if {$plist != 1} {
                            set plistInBin [wrsbin2dec  [string replace  [string repeat 0 [expr $plist-1]] 0 0 10]]
                       } else {
                            set plistInBin 1
                       }
                            set appendedlist [expr $appendedlist | $plistInBin] 
                            set portToDec $appendedlist
                            puts " appendedlist $appendedlist , "
                       }
                    }
               }
            }
           }
         } else {
             set portToDec [wrsbin2dec $portInBin]

         }
     
     set port_num   "0x[string toupper [format %01x $portToDec]]"
     if {![Send_cli_command  -session_handler   $session_handler \
                             -cmd_type          $cmd_type\
                             -parameter1        $bridge_instance\
                             -parameter2        $port_num \
                             -parameter3        $vlan_id \
                             -parameter4        $mac_address]} {

      puts "cli_vlan_remove_static_fwd_entry: Error removing static MAC forwarding entry"
      return 0
     }

     return 1

#  return 2
}


###############################################################################
#9.                                                                           #
# PROCEDURE NAME    : callback_vlan_add_static_mcast_fwd_entry                #
#                                                                             #
# INPUT PARAMETERS:                                                           #
#  session_handler    : (Mandatory) Sesion-ID of the DUT session              #
#  bridge_instance    : (Mandatory) Bridge Instance (Default Value 0)         #
#  vlan_id            : (Mandatory) VLAN Identifier (Default Value 1).        #
#  port_num           : (Mandatory) Port Number                               #
#  mac_address        : (Mandatory) Mac Address                               #
#                                                                             #
#  DEFINITION       : Adds the static mcast mac-address entry at DUT for the  #
#                     specified Port and specified vlan id.                   #
#                                                                             #
#  OUTPUT PARAMETERS:                                                         #
#                                                                             #
#                     NIL.                                                    #
#                                                                             #
#  RETURNS          : 1 on success     			     		      #
#                     0 on failure     			     		      #
#                     2 if the ATTEST has to do the parsing.                  #
#                                                                             #
###############################################################################

proc callback_vlan_add_static_mcast_fwd_entry {args} {

  array set param $args

  set session_handler $param(-session_handler)
  set bridge_instance $param(-bridge_instance)
  set port_num        $param(-port_num)
  set vlan_id         $param(-vlan_id)
  set mac_address     $param(-mac_address)
  set cmd_type        $param(-cmd_type)

       set port [regexp -all {[0-9]+} $port_num Port]
       set port_num $Port
       if {$Port != 1} {
           set portInBin [string replace  [string repeat 0 [expr $Port-1]] 0 0 10]
       } else {
           set portInBin 1
       }

           set mac_address [vlan_convert_dutmac_to_userMac -macaddress $mac_address]
           set portToDec [wrsbin2dec $portInBin]
           set port_num   "0x[string toupper [format %01x $portToDec]]"
       set bridge_instance 0
      Send_cli_command_to_read -session_handler   $session_handler \
                           -cmd_type          "DUT_VLAN_READ_PORT_STATE"\
                           -parameter1        $bridge_instance \
                           -parameter2        $vlan_id \
                           -output            buffer
     
     puts "Debug--->###########################"
     puts "Debug---> buffer $buffer"
     puts "Debug--->###########################"
     set previousvlanindec ""
     set addedports ""
     set spliting [split $buffer \n]
     foreach line $spliting {
         if {[regexp $vlan_id $line]} {
            puts "Debug---> line 2 [lindex $line 2] , vlan_id $vlan_id"
            if {([lindex $line 2] == $vlan_id) && ($mac_address == [lindex $line 0])} {
                set previousport [lindex $line 1]
                if {$previousport != 1} {
                    set portInBinRTU [string replace  [string repeat 0 [expr $previousport-1]] 0 0 10]
                } else {
                    set portInBinRTU 1
                }
                set previousPorttoDec [wrsbin2dec $portInBinRTU]
                set addedports [expr $previousPorttoDec | $portToDec]
            }
         }
      }
               if {$addedports != ""} {
                set port_num   "0x[string toupper [format %01x $addedports]]"

               }
           
puts "DEbug---> Final added ports $port_num"
           Send_cli_command   -session_handler   $session_handler \
                              -cmd_type          "DUT_VLAN_CREATE_STATIC_MCAST_ENTRY"\
                              -parameter1        $bridge_instance\
                              -parameter2        $port_num \
                              -parameter3        $vlan_id \
                              -parameter4        $mac_address



     if {![Send_cli_command   -session_handler   $session_handler \
                              -cmd_type          "DUT_VLAN_ADD_STATIC_MCAST_ENTRY"\
                              -parameter1        $bridge_instance\
                              -parameter2        $port_num \
                              -parameter3        $vlan_id \
                              -parameter4        $mac_address]} {

      puts "cli_vlan_add_static_mcast_fwd_entry: Error adding static MAC forwarding entry"
      return 0
     }

     return 1

}



###############################################################################
#10.                                                                          #
# PROCEDURE NAME    : callback_vlan_remove_static_mcast_fwd_entry             #
#                                                                             #
# INPUT PARAMETERS:                                                           #
#  session_handler    : (Mandatory) Sesion-ID of the DUT session              #
#  bridge_instance    : (Mandatory) Bridge Instance (Default Value 0)         #
#  vlan_id            : (Mandatory) VLAN Identifier (Default Value 1).        #
#  port_num           : (Mandatory) Port Number                               #
#  mac_address        : (Mandatory) Mac Address                               #
#                                                                             #
#  DEFINITION       : Removes static mcast mac-address entry at DUT for the   #
#                     specified Port and specified vlan id.                   #
#                                                                             #
#  OUTPUT PARAMETERS:                                                         #
#                                                                             #
#                     NIL.                                                    #
#                                                                             #
#  RETURNS          : 1 on success     			     		      #
#                     0 on failure     			     		      #
#                     2 if the ATTEST has to do the parsing.                  #
#                                                                             #
###############################################################################

proc callback_vlan_remove_static_mcast_fwd_entry {args} {

  array set param $args

  set session_handler $param(-session_handler)
  set bridge_instance $param(-bridge_instance)
  set port_num        $param(-port_num)
  set vlan_id         $param(-vlan_id)
  set mac_address     $param(-mac_address)
  set cmd_type        $param(-cmd_type)

     set port [regexp -all {[0-9]+} $port_num Port]
     set port_num $Port
     set bridge_instance 0
      if {$Port != 1} {
           set portInBin [string replace  [string repeat 0 [expr $Port-1]] 0 0 10]
       } else {
           set portInBin 1
       }

     set portToDec [wrsbin2dec $portInBin]
     set port_num   "0x[string toupper [format %01x $portToDec]]"
     set mac_address [vlan_convert_dutmac_to_userMac -macaddress $mac_address]
     if {![Send_cli_command   -session_handler   $session_handler \
                              -cmd_type          $cmd_type \
                              -parameter1        $bridge_instance\
                              -parameter2        $port_num \
                              -parameter3        $vlan_id \
                              -parameter4        $mac_address]} {

      puts "cli_vlan_remove_static_mcast_fwd_entry: Error removing static MAC forwarding entry"
      return 0
     }

     return 1

#  return 2
}



###############################################################################
#11.                                                                          #
# PROCEDURE NAME   : callback_vlan_set_ingress_filter                         #
#                                                                             #
# INPUT PARAMETERS :                                                          #
#  session_handler : (Mandatory) Session-ID of the DUT                        #
#  bridge_instance : (Mandatory) Bridge Instance                              #
#  port_num        : (Mandatory) Port Number                                  #
#                                                                             #
#  DEFINITION      : Enables the Ingress Filter parameter at DUT.             #
#                                                                             #
#  OUTPUT PARAMETERS:                                                         #
#                                                                             #
#                     NIL.                                                    #
#                                                                             #
#  RETURNS          : 1 on success     			     		      #
#                     0 on failure     			     		      #
#                     2 if the ATTEST has to do the parsing.                  #
#                                                                             #
###############################################################################

proc callback_vlan_set_ingress_filter {args} {

  array set param $args

       set session_handler $param(-session_handler)
       set bridge_instance $param(-bridge_instance)
       set port_num        $param(-port_num)
       set cmd_type        $param(-cmd_type)

#      if {![Send_cli_command  -session_handler   $session_handler \
#                              -cmd_type          $cmd_type\
#                              -parameter1        $bridge_instance\
#                              -parameter2        $port_num]} {
#
#            puts "cli_vlan_set_ingress_filter: Error! Unable to set ingress filter parameter"
#            return 0
#       }
#
#       return 1

 return 2

}



###############################################################################
#12.                                                                          #
# PROCEDURE NAME   : callback_vlan_reset_ingress_filter                       #
#                                                                             #
# INPUT PARAMETERS :                                                          #
# session_handler  : (Mandatory)Session-ID of the DUT                         #
# bridge_instance  : (Mandatory) Bridge Instance                              #
# port_num         : (Mandatory)Port Number                                   #
#                                                                             #
#  DEFINITION      : Disables Ingress Filter parameter at DUT.                #
#                                                                             #
#  OUTPUT PARAMETERS:                                                         #
#                                                                             #
#                     NIL.                                                    #
#                                                                             #
#  RETURNS          : 1 on success     			     		      #
#                     0 on failure     			     		      #
#                     2 if the ATTEST has to do the parsing.                  #
#                                                                             #
###############################################################################

proc callback_vlan_reset_ingress_filter {args} {

  array set param $args

       set session_handler $param(-session_handler)
       set bridge_instance $param(-bridge_instance)
       set port_num        $param(-port_num)
       set cmd_type        $param(-cmd_type)

#      if {![Send_cli_command  -session_handler   $session_handler \
#                              -cmd_type          $cmd_type\
#                              -parameter1        $bridge_instance\
#                              -parameter2        $port_num]} {
#
#            puts "cli_vlan_reset_ingress_filter: Error! Unable to reset ingress filter"
#            return 0
#       }
#
#       return 1
#
 return 2

}



###############################################################################
#13.                                                                          #
# PROCEDURE NAME  : callback_vlan_admit_all_frames                            #
#                                                                             #
# INPUT PARAMETERS:                                                           #
#  session_handler: (Mandatory)Session-ID of the DUT                          #
#  bridge_instance: (Mandatory) Bridge Instance                               #
#  port_num       : (Mandatory)Port location                                  #
#  vlan_id        : (Optional) VLAN Identifier (DEFAULT VLAUE 0)              #
#                                                                             #
#  DEFINITION      : Sets the specified port to accept VLAN-tagged, priority  #
#                    tagged and untagged frames.                              #
#                                                                             #
#  OUTPUT PARAMETERS:                                                         #
#                                                                             #
#                     NIL.                                                    #
#                                                                             #
#  RETURNS          : 1 on success     			     		      #
#                     0 on failure     			     		      #
#                     2 if the ATTEST has to do the parsing.                  #
#                                                                             #
###############################################################################

proc callback_vlan_admit_all_frames {args} {

  array set param $args
  global listofPorts_admitall
  set session_handler $param(-session_handler)
  set bridge_instance $param(-bridge_instance)
  set port_num        $param(-port_num)
  set vlan_id         $param(-vlan_id)
  set cmd_type        $param(-cmd_type)
  set bridgeInstance 0
  set addedports_$vlan_id 0

  set port [regexp -all {[0-9]+} $port_num Port]
  set PortNum $Port
  if {$Port != 1} {
     set portInBin [string replace  [string repeat 0 [expr $Port-1]] 0 0 10]
  } else {
     set portInBin 1
  }
  Send_cli_command_to_read -session_handler   $session_handler \
                            -cmd_type          "DUT_VLAN_CHECK_ASSOCIATED_PORTS"\
                            -parameter1        $bridge_instance \
                            -parameter2        $vlan_id \
                            -output            buffer

      set previousvlanindec ""
      set spliting [split $buffer \n]
      #NEW
      if {[llength $spliting] == 6} {
             set listofPorts_admitall ""
      }
      foreach line $spliting {
          if {[regexp $vlan_id $line]} {
             if {[lindex $line 0] == $vlan_id} {
                 set previousvlan [string map {"0x0004" ""} [lindex $line 2]]
				 ## Take into account only the 18 ports
				 set previousvlan [format 0x%x [expr {$previousvlan & 0x3FFFF}]] 
                 set previousvlaninbin [string trimleft [HextoBin $previousvlan] 0]
                 set previousvlanindec [wrsbin2dec $previousvlaninbin]
                 set checkportsavail [Portfinder $previousvlaninbin]
             }
          }
       }
  lappend listofPorts_admitall "$vlan_id-$portInBin"

     foreach LPorts $listofPorts_admitall {
        set Req [split $LPorts -]
        if {$vlan_id == [lindex $Req 0]} {
            set LPorts_[lindex $Req 0] [wrsbin2dec [lindex $Req 1]]
            set addedports_[lindex $Req 0] [expr [set addedports_[lindex $Req 0]] | [set LPorts_[lindex $Req 0]]]
        }
      }
      set portToDec [set addedports_$vlan_id]
      set port_num   "0x[string toupper [format %01x $portToDec]]"

      if {$previousvlanindec != ""} {
         if {[lsearch $checkportsavail $PortNum] != -1} {
             set port_num "0x[string toupper [format %01x $previousvlanindec]]"
         } else {
            set maskindec [wrsbin2dec [string trimleft [HextoBin [string map {"0x" ""} $port_num]] 0]]
            set finaladdedports [expr ($previousvlanindec | $maskindec)]
            set port_num   "0x[string toupper [format %01x $finaladdedports]]"
         }
      }

       if {![Send_cli_command  -session_handler   $session_handler \
                               -cmd_type          $cmd_type\
                               -parameter1        $bridge_instance\
                               -parameter2        $PortNum \
                               -parameter3        $vlan_id \
                               -parameter4        $port_num]} {

            puts "cli_vlan_admit_all_frames: Error! Unable to set the port to accept all frames"
            return 0
       }
      return 1

#return 2
}



###############################################################################
#14.                                                                          #
# PROCEDURE NAME  : callback_vlan_admit_only_vlan_tagged_frames               #
#                                                                             #
# INPUT PARAMETERS:                                                           #
#  session_handler: (Mandatory) Session-ID of the DUT                         #
#  bridge_instance: (Mandatory) Bridge Instance                               #
#  port_num       : (Mandatory) Port Number                                   #
#  vlan_id        : (Mandatory) VLAN Identifier (Default Value 1).            #
#                                                                             #
#  DEFINITION       : Allows only vlan tagged frames on the specified port.   #
#                                                                             #
#  OUTPUT PARAMETERS:                                                         #
#                                                                             #
#                     NIL.                                                    #
#                                                                             #
#  RETURNS          : 1 on success     			     		      #
#                     0 on failure     			     		      #
#                     2 if the ATTEST has to do the parsing.                  #
#                                                                             #
###############################################################################

proc callback_vlan_admit_only_vlan_tagged_frames {args} {

  array set param $args
       global listofPorts_admitvlan
       set session_handler $param(-session_handler)
       set bridge_instance $param(-bridge_instance)
       set port_num        $param(-port_num)
       set vlan_id         $param(-vlan_id)
       set cmd_type        $param(-cmd_type)
       set addedports_$vlan_id 0
       set bridgeInstance 0

       set port [regexp -all {[0-9]+} $port_num Port]
       set PortNum $Port
      if {$Port != 1} {
         set portInBin [string replace  [string repeat 0 [expr $Port-1]] 0 0 10]
      } else {
         set portInBin 1
      }

      Send_cli_command_to_read -session_handler   $session_handler \
                               -cmd_type          "DUT_VLAN_CHECK_ASSOCIATED_PORTS"\
                               -parameter1        $bridge_instance \
                               -parameter2        $vlan_id \
                               -output            buffer

      set previousvlanindec ""
      set spliting [split $buffer \n]
      foreach line $spliting {
          if {[regexp $vlan_id $line]} {
             if {[lindex $line 0] == $vlan_id} {
                 set previousvlan [string map {"0x0004" ""} [lindex $line 2]]
				 ## Take into account only the 18 ports
				 set previousvlan [format 0x%x [expr {$previousvlan & 0x3FFFF}]] 
                 set previousvlaninbin [string trimleft [HextoBin $previousvlan] 0]
                 set previousvlanindec [wrsbin2dec $previousvlaninbin]
                 set checkportsavail [Portfinder $previousvlaninbin]
             }
          }
       }

      lappend listofPorts_admitvlan "$vlan_id-$portInBin"
      foreach LPorts $listofPorts_admitvlan {
        set Req [split $LPorts -]
        if {$vlan_id == [lindex $Req 0]} {
            set LPorts_[lindex $Req 0] [wrsbin2dec [lindex $Req 1]]
            set addedports_[lindex $Req 0] [expr [set addedports_[lindex $Req 0]] | [set LPorts_[lindex $Req 0]]]
        }
      }
      set portToDec [set addedports_$vlan_id]
      set port_num   "0x[string toupper [format %01x $portToDec]]"

      if {$previousvlanindec != ""} {
         if {[lsearch $checkportsavail $PortNum] != -1} {
             set port_num "0x[string toupper [format %01x $previousvlanindec]]"
         } else {
            set maskindec [wrsbin2dec [string trimleft [HextoBin [string map {"0x" ""} $port_num]] 0]]
            set finaladdedports [expr ($previousvlanindec | $maskindec)]
            set port_num   "0x[string toupper [format %01x $finaladdedports]]"
         }
      }
       if {![Send_cli_command  -session_handler   $session_handler \
                               -cmd_type          $cmd_type\
                               -parameter1        $bridge_instance\
                               -parameter2        $PortNum \
                               -parameter3        $vlan_id \
                               -parameter4        $port_num]} {

            puts "cli_vlan_admit_only_vlan_tagged_frames: Error! Unable to set the port to accept\
                  only VLAN tagged frames"
            return 0
       }

  return 1

#  return 2

}



###############################################################################
#15.                                                                          #
# PROCEDURE NAME   : callback_vlan_set_tagged_port_vlan_membership            #
#                                                                             #
# INPUT PARAMETERS :                                                          #
#  session_handler : (Mandatory)Session-ID of the DUT                         #
#  bridge_instance : (Mandatory) Bridge Instance                              #
#  port_num        : (Mandatory)Port Number                                   #
#  vlan_id         : (Mandatory)Vlan ID                                       #
#                                                                             #
#  DEFINITION       : Associates the specified vlan id with the specified     #
#                     tagged port.                                            #
#                                                                             #
#  OUTPUT PARAMETERS:                                                         #
#                                                                             #
#                     NIL.                                                    #
#                                                                             #
#  RETURNS          : 1 on success     			     		      #
#                     0 on failure     			     		      #
#                     2 if the ATTEST has to do the parsing.                  #
#                                                                             #
###############################################################################

proc callback_vlan_set_tagged_port_vlan_membership {args} {

  array set param $args
       global listofPorts_tpvlan
       set session_handler $param(-session_handler)
       set bridge_instance $param(-bridge_instance)
       set port_num        $param(-port_num)
       set vlan_id         $param(-vlan_id)
       set cmd_type        $param(-cmd_type)
       set bridgeInstance 0
       set addedports_$vlan_id 0

       set port [regexp -all {[0-9]+} $port_num Port]
       set PortNum $Port
      if {$Port != 1} {
         set portInBin [string replace  [string repeat 0 [expr $Port-1]] 0 0 10]
      } else {
      set portInBin 1
      }
      ##########################  NEW  ############################
      Send_cli_command_to_read -session_handler   $session_handler \
                               -cmd_type          "DUT_VLAN_CHECK_ASSOCIATED_PORTS"\
                               -parameter1        $bridge_instance \
                               -parameter2        $vlan_id \
                               -output            buffer

      set previousvlanindec ""
      set spliting [split $buffer \n]
      foreach line $spliting {
          if {[regexp $vlan_id $line]} {
             if {[lindex $line 0] == $vlan_id} {
                 set previousvlan [string map {"0x0004" ""} [lindex $line 2]]
				 ## Take into account only the 18 ports
				 set previousvlan [format 0x%x [expr {$previousvlan & 0x3FFFF}]] 
				 set previousvlaninbin [string trimleft [HextoBin $previousvlan] 0]
                 set previousvlanindec [wrsbin2dec $previousvlaninbin]
                 set checkportsavail [Portfinder $previousvlaninbin]
             }
          }
       }
      ##########################  NEW  ############################

      lappend listofPorts_tpvlan "$vlan_id-$portInBin"
      foreach LPorts $listofPorts_tpvlan {
        set Req [split $LPorts -]
        if {$vlan_id == [lindex $Req 0]} {
            set LPorts_[lindex $Req 0] [wrsbin2dec [lindex $Req 1]]
            set addedports_[lindex $Req 0] [expr [set addedports_[lindex $Req 0]] | [set LPorts_[lindex $Req 0]]]
        }
      }
      set portToDec [set addedports_$vlan_id]
      set port_num   "0x[string toupper [format %01x $portToDec]]"

      ##########################  NEW  ############################
      if {$previousvlanindec != ""} {
         if {[lsearch $checkportsavail $PortNum] != -1} {
             set port_num "0x[string toupper [format %01x $previousvlanindec]]"
         } else {
            set maskindec [wrsbin2dec [string trimleft [HextoBin [string map {"0x" ""} $port_num]] 0]]
            set finaladdedports [expr ($previousvlanindec | $maskindec)]
            set port_num   "0x[string toupper [format %01x $finaladdedports]]"
         }
      }
	  ##########################  NEW  ############################

       if {![Send_cli_command  -session_handler   $session_handler \
                               -cmd_type          $cmd_type\
                               -parameter1        $bridge_instance\
                               -parameter2        $PortNum \
                               -parameter3        $vlan_id \
                               -parameter4        $port_num]} {

            puts "cli_vlan_set_tagged_port_vlan_membership: Error! Unable to\
                  set Tagged port vlan membership"
            return 0
       }
    return 1

#return 2
}



###############################################################################
#16.                                                                          #
# PROCEDURE NAME  : callback_vlan_reset_tagged_port_vlan_membership           #
#                                                                             #
# INPUT PARAMETERS:                                                           #
#  session_handler: (Mandatory)Session-ID of the DUT                          #
#  bridge_instance: (Mandatory)Bridge Instance                                #
#  port_num       : (Mandatory)Port Number                                    #
#  vlan_id        : (Mandatory)Vlan ID                                        #
#                                                                             #
#  DEFINITION       : Removes the specified vlan id from the specified        #
#                     tagged port.                                            #
#                                                                             #
#  OUTPUT PARAMETERS:                                                         #
#                                                                             #
#                     NIL.                                                    #
#                                                                             #
#  RETURNS          : 1 on success     			     		      #
#                     0 on failure     			     		      #
#                     2 if the ATTEST has to do the parsing.                  #
#                                                                             #
###############################################################################

proc callback_vlan_reset_tagged_port_vlan_membership {args} {

  array set param $args

       set session_handler $param(-session_handler)
       set bridge_instance $param(-bridge_instance)
       set port_num $param(-port_num)
       set vlan_id $param(-vlan_id)
       set cmd_type        $param(-cmd_type)

#       if {![Send_cli_command  -session_handler   $session_handler \
#                               -cmd_type          $cmd_type\
#                               -parameter1        $bridge_instance\
#                               -parameter2        $port_num \
#                               -parameter3        $vlan_id]} {
#
#        puts "cli_vlan_reset_tagged_port_vlan_membership: Error! Unable to reset Tagged port vlan membership"
#        return 0
#       }
#       return 1
#
return 2
}



###############################################################################
#17.                                                                          #
# PROCEDURE NAME  : callback_vlan_set_pvid_to_untagged_port                   #
#                                                                             #
# INPUT PARAMETERS:                                                           #
#  session_handler: (Mandatory) Sesion-ID of the DUT session                  #
#  bridge_instance: (Mandatory) Bridge Instance                               #
#  port_num       : (Mandatory) Port Number                                   #
#  vlan_id        : (Mandatory) Vlan ID                                       #
#                                                                             #
#  DEFINITION       : Associates a permanent vlan id to the specified         #
#                     untagged port.                                          #
#                                                                             #
#  OUTPUT PARAMETERS:                                                         #
#                                                                             #
#                     NIL.                                                    #
#                                                                             #
#  RETURNS          : 1 on success     			     		      #
#                     0 on failure     			     		      #
#                     2 if the ATTEST has to do the parsing.                  #
#                                                                             #
#                                                                             #
###############################################################################

proc callback_vlan_set_pvid_to_untagged_port {args} {

  array set param $args
       global listofPorts
       set session_handler $param(-session_handler)
       set bridge_instance $param(-bridge_instance)
       set port_num        $param(-port_num)
       set vlan_id         $param(-vlan_id)
       set cmd_type        $param(-cmd_type)
       set addedports_$vlan_id 0
       set bridgeInstance 0

       set port [regexp -all {[0-9]+} $port_num Port]
       set PortNum $Port
      if {$Port != 1} {
         set portInBin [string replace  [string repeat 0 [expr $Port-1]] 0 0 10]
      } else {
         set portInBin 1
      }

      Send_cli_command_to_read -session_handler   $session_handler \
                               -cmd_type          "DUT_VLAN_CHECK_ASSOCIATED_PORTS"\
                               -parameter1        $bridge_instance \
                               -parameter2        $vlan_id \
                               -output            buffer 
      set previousvlanindec ""
      set spliting [split $buffer \n]
      foreach line $spliting {
          if {[regexp $vlan_id $line]} {
             if {[lindex $line 0] == $vlan_id} {
                 set previousvlan [string map {"0x0004" ""} [lindex $line 2]]
				 ## Take into account only the 18 ports
				 set previousvlan [format 0x%x [expr {$previousvlan & 0x3FFFF}]] 
                 set previousvlaninbin [string trimleft [HextoBin $previousvlan] 0]
                 set previousvlanindec [wrsbin2dec $previousvlaninbin]
                 set checkportsavail [Portfinder $previousvlaninbin]
             }
          }
       }

      lappend listofPorts "$vlan_id-$portInBin"
      foreach LPorts $listofPorts {
        set Req [split $LPorts -]
        if {$vlan_id == [lindex $Req 0]} {
            set LPorts_[lindex $Req 0] [wrsbin2dec [lindex $Req 1]]
            set addedports_[lindex $Req 0] [expr [set addedports_[lindex $Req 0]] | [set LPorts_[lindex $Req 0]]]
        }
      }
      set portToDec [set addedports_$vlan_id]
      set port_num   "0x[string toupper [format %01x $portToDec]]"
      if {$previousvlanindec != ""} {
         if {[lsearch $checkportsavail $PortNum] != -1} {
             set port_num "0x[string toupper [format %01x $previousvlanindec]]"
         } else {
            set maskindec [wrsbin2dec [string trimleft [HextoBin [string map {"0x" ""} $port_num]] 0]]
            set finaladdedports [expr ($previousvlanindec | $maskindec)]
            set port_num   "0x[string toupper [format %01x $finaladdedports]]"
         }
      }

      if {![Send_cli_command  -session_handler   $session_handler \
                               -cmd_type          $cmd_type\
                               -parameter1        $bridge_instance\
                               -parameter2        $PortNum \
                               -parameter3        $vlan_id \
                               -parameter4        $port_num]} {

            puts "cli_vlan_set_pvid_to_untagged_port: Error! Unable to set PVID to Untagged port"
            return 0
       }
       return 1

#return 2
}




###############################################################################
#18.                                                                          #
# PROCEDURE NAME  : callback_vlan_reset_pvid_to_untagged_port                 #
#                                                                             #
# INPUT PARAMETERS:                                                           #
# session_handler : (Mandatory)Session-ID of the DUT                          #
# bridge_instance : (Mandatory)Bridge Instance                                #
# port_num        : (Mandatory)Port Number                                    #
# vlan_id         : (Mandatory)Vlan ID                                        #
#                                                                             #
#  DEFINITION       : Removes the specified permanent vlan id from the        #
#                     specified untagged port.                                #
#                                                                             #
#  OUTPUT PARAMETERS:                                                         #
#                                                                             #
#                     NIL.                                                    #
#                                                                             #
#  RETURNS          : 1 on success     			     		      #
#                     0 on failure     			     		      #
#                     2 if the ATTEST has to do the parsing.                  #
#                                                                             #
###############################################################################


proc callback_vlan_reset_pvid_to_untagged_port {args} {

  array set param $args

       set session_handler $param(-session_handler)
       set bridge_instance $param(-bridge_instance)
       set port_num        $param(-port_num)
       set vlan_id         $param(-vlan_id)
       set cmd_type        $param(-cmd_type)

       set port [regexp -all {[0-9]+} $port_num Port]
       set port_num $Port
       set bridge_instance 0

       if {![Send_cli_command  -session_handler   $session_handler \
                               -cmd_type          $cmd_type\
                               -parameter1        $bridge_instance\
                               -parameter2        $port_num \
                               -parameter3        $vlan_id]} {

         puts "cli_vlan_reset_tagged_port_vlan_membership: Error! Unable to reset PVID to Untagged port"
          return 0
       }
   return 1

#return 2
}



###############################################################################
#19.                                                                          #
# PROCEDURE NAME  : callback_vlan_enable_gvrp                                 #
#                                                                             #
# INPUT PARAMETERS:                                                           #
#                                                                             #
#  session_handler: (Mandatory) Sesion-ID of the DUT session                  #
#  bridge_instance: (Mandatory) Bridge Instance                               #
#                                                                             #
#  DEFINITION       : Enables GVRP at  DUT.                                   #
#                                                                             #
#  OUTPUT PARAMETERS:                                                         #
#                                                                             #
#                     NIL.                                                    #
#                                                                             #
#  RETURNS          : 1 on success     			     		      #
#                     0 on failure     			     		      #
#                     2 if the ATTEST has to do the parsing.                  #
#                                                                             #
###############################################################################

proc callback_vlan_enable_gvrp {args} {

  array set param $args

       set session_handler $param(-session_handler)
       set bridge_instance $param(-bridge_instance)
       set cmd_type        $param(-cmd_type)


#        if {![Send_cli_command  -session_handler   $session_handler \
#                                -cmd_type          $cmd_type\
#                                -parameter1        $bridge_instance]} {
#
#            puts "cli_vlan_enable_gvrp: Error while Enabling GVRP"
#            return 0
#         }
#          return 1

return 2
}

###############################################################################
#20.                                                                          #
# PROCEDURE NAME  : callback_vlan_disable_gvrp                                #
#                                                                             #
# INPUT PARAMETERS:                                                           #
#                                                                             #
#  session_handler: (Mandatory) Sesion-ID of the DUT session                  #
#  bridge_instance: (Mandatory) Bridge Instance                               #
#                                                                             #
#  DEFINITION       : Disables GVRP at  DUT.                                  #
#                                                                             #
#  OUTPUT PARAMETERS:                                                         #
#                                                                             #
#                     NIL.                                                    #
#                                                                             #
#  RETURNS          : 1 on success     			     		      #
#                     0 on failure     			     		      #
#                     2 if the ATTEST has to do the parsing.                  #
#                                                                             #
###############################################################################

proc callback_vlan_disable_gvrp {args} {

  array set param $args

       set session_handler $param(-session_handler)
       set bridge_instance $param(-bridge_instance)
       set cmd_type        $param(-cmd_type)

#        if {![Send_cli_command  -session_handler   $session_handler \
#                                -cmd_type          $cmd_type\
#                                -parameter1        $bridge_instance]} {
#
#            puts "cli_vlan_disable_gvrp: Error while Disabling GVRP"
#            return 0
#         }
#          return 1

return 2
}


###############################################################################
#21.                                                                          #
# PROCEDURE NAME  : callback_vlan_set_registration_type_forbidden             #
#                                                                             #
# INPUT PARAMETERS:                                                           #
# session_handler : (Mandatory)Session-ID of the DUT                          #
# bridge_instance : (Mandatory)Bridge Instance                                #
# port_num        : (Mandatory)Port Number                                    #
# vlan_id         : (Mandatory)Vlan ID                                        #
#                                                                             #
#  DEFINITION      : Sets the Registration Type for the specified port in     #
#                    as"forbidden" for the specified Vlan Id.                 #
#                                                                             #
#  OUTPUT PARAMETERS:                                                         #
#                                                                             #
#                     NIL.                                                    #
#                                                                             #
#  RETURNS          : 1 on success     			     		      #
#                     0 on failure     			     		      #
#                     2 if the ATTEST has to do the parsing.                  #
#                                                                             #
###############################################################################


proc callback_vlan_set_registration_type_forbidden {args} {

  array set param $args

       set session_handler $param(-session_handler)
       set bridge_instance $param(-bridge_instance)
       set port_num        $param(-port_num)
       set vlan_id         $param(-vlan_id)
       set cmd_type        $param(-cmd_type)

#       if {![Send_cli_command  -session_handler   $session_handler \
#                               -cmd_type          $cmd_type\
#                               -parameter1        $bridge_instance\
#                               -parameter2        $port_num \
#                               -parameter3        $vlan_id]} {
#
#         puts "callback_vlan_set_registration_type_forbidden: Error! Unable to set Reg Type"
#          return 0
#       }
#   return 1

return 2
}


###############################################################################
#22.                                                                          #
# PROCEDURE NAME  : callback_vlan_reset_registration_type_forbidden           #
#                                                                             #
# INPUT PARAMETERS:                                                           #
# session_handler : (Mandatory)Session-ID of the DUT                          #
# bridge_instance : (Mandatory)Bridge Instance                                #
# port_num        : (Mandatory)Port Number                                    #
# vlan_id         : (Mandatory)Vlan ID                                        #
#                                                                             #
#  DEFINITION      : Resets the Registration Type for the specified port.     #
#                                                                             #
#  OUTPUT PARAMETERS:                                                         #
#                                                                             #
#                     NIL.                                                    #
#                                                                             #
#  RETURNS          : 1 on success     			     		      #
#                     0 on failure     			     		      #
#                     2 if the ATTEST has to do the parsing.                  #
#                                                                             #
###############################################################################


proc callback_vlan_reset_registration_type_forbidden {args} {

  array set param $args

       set session_handler $param(-session_handler)
       set bridge_instance $param(-bridge_instance)
       set port_num        $param(-port_num)
       set vlan_id         $param(-vlan_id)
       set cmd_type        $param(-cmd_type)

#       if {![Send_cli_command  -session_handler   $session_handler \
#                               -cmd_type          $cmd_type\
#                               -parameter1        $bridge_instance\
#                               -parameter2        $port_num \
#                               -parameter3        $vlan_id]} {
#
#         puts "callback_vlan_reset_registration_type_forbidden: Error! Unable to set Reg Type"
#          return 0
#       }
#   return 1

return 2
}


###############################################################################
#23.                                                                          #
#  PROCEDURE NAME     : callback_vlan_add_pgd_entry                           #
#                                                                             #
#  INPUT PARAMETERS   :                                                       #
#                                                                             #
#  session_handler   : (Mandatory) Session-ID of the DUT                      #
#  bridge_instance   : (Mandatory) Bridge Instance (Default Value 0)          #
#  frame_type        : (Mandatory) Frames Type.                               #
#  protocol_type     : (Mandatory) Protocol Type.                             #
#  group_id          :(Mandatory)  Group Id.                                  #
#                                                                             #
#  DEFINITION       : Adds a Protocol to Group ID.                            #
#                                                                             #
#  OUTPUT PARAMETERS:                                                         #
#                      NONE                                                   #
#                                                                             #
#  RETURNS          : 1 on success     			     		      #
#                     0 on failure     			     		      #
#                     2 if the ATTEST has to do the parsing.                  #
#                                                                             #
###############################################################################

proc callback_vlan_add_pgd_entry {args} {

  array set param $args

       set session_handler    $param(-session_handler)
       set bridge_instance    $param(-bridge_instance)
       set frame_type         $param(-frame_type)
       set protocol_type      $param(-protocol_type)
       set group_id           $param(-group_id)
       set cmd_type           $param(-cmd_type)

#       if {![Send_cli_command  -session_handler   $session_handler \
#                               -cmd_type          $cmd_type\
#                               -parameter1        $bridge_instance\
#                               -parameter2        $frame_type\
#                               -parameter3        $protocol_type\
#                               -parameter4        $group_id]} {
#
#         puts "callback_vlan_set_protocol_group_database: Error! Unable to add Protocol to Group"
#          return 0
#       }
#   return 1

return 2
}


###############################################################################
#24.                                                                          #
#  PROCEDURE NAME     : callback_vlan_remove_pgd_entry                        #
#                                                                             #
#  INPUT PARAMETERS   :                                                       #
#                                                                             #
#  session_handler   : (Mandatory) Session-ID of the DUT                      #
#  bridge_instance   : (Mandatory) Bridge Instance (Default Value 0)          #
#  frame_type        : (Mandatory) Frames Types.                              #
#  Protocol_type     : (Mandatory) Protocol Types.                            #
#  group_id          : (Mandatory) Group ID.                                  #
#                                                                             #
#  DEFINITION       : Removes a Protocol from a Group ID.                     #
#                                                                             #
#  OUTPUT PARAMETERS:                                                         #
#                                                                             #
#                     NIL.                                                    #
#                                                                             #
#  RETURNS          : 1 on success     			     		      #
#                     0 on failure     			     		      #
#                     2 if the ATTEST has to do the parsing.                  #
#                                                                             #
###############################################################################

proc callback_vlan_remove_pgd_entry {args} {

  array set param $args

       set session_handler    $param(-session_handler)
       set bridge_instance    $param(-bridge_instance)
       set frame_type         $param(-frame_type)
       set protocol_type      $param(-protocol_type)
       set group_id           $param(-group_id)
       set cmd_type           $param(-cmd_type)

#       if {![Send_cli_command  -session_handler   $session_handler \
#                               -cmd_type          $cmd_type\
#                               -parameter1        $bridge_instance\
#                               -parameter2        $frame_type\
#                               -parameter3        $protocol_type\
#                               -parameter4        $group_id]} {
#
#         puts "callback_vlan_remove_protocol_group: Error! Unable to remove Protocol from Group"
#          return 0
#       }
#   return 1

return 2
}


###############################################################################
#25.                                                                          #
# PROCEDURE NAME  : callback_vlan_add_vid_set                                 # 
#                                                                             #
# INPUT PARAMETERS:                                                           #
# session_handler : (Mandatory) Session-ID of the DUT                         #
# bridge_instance : (Mandatory) Bridge Instance                               #
# group_id        : (Mandatory) Group-ID of protocol                          #
# vlan_id         : (Mandatory) Vlan ID                                       #
# port_num        : (Mandatory) Port Number                                   #
#                                                                             #
#  DEFINITION       : Associates a VID set to Group ID.                       #
#                                                                             #
#  OUTPUT PARAMETERS:                                                         #
#                                                                             #
#                     NIL.                                                    #
#                                                                             #
#  RETURNS          : 1 on success     			     		      #
#                     0 on failure     			     		      #
#                     2 if the ATTEST has to do the parsing.                  #
#                                                                             #
###############################################################################


proc callback_vlan_add_vid_set {args} {

  array set param $args

       set session_handler $param(-session_handler)
       set bridge_instance $param(-bridge_instance)
       set group_id        $param(-group_id)
       set vlan_id         $param(-vlan_id)
       set port_num        $param(-port_num)
       set cmd_type        $param(-cmd_type)

#       if {![Send_cli_command  -session_handler   $session_handler \
#                               -cmd_type          $cmd_type\
#                               -parameter1        $bridge_instance\
#                               -parameter2        $vlan_id \
#                               -parameter3        $port_num\
#                               -parameter4        $group_id]} {
#
#         puts "callback_vlan_add_vid_set: Error! Unable to add VID set to Group ID"
#          return 0
#       }
#   return 1

return 2
}


###############################################################################
#26.                                                                          #
# PROCEDURE NAME  : callback_vlan_remove_vid_set                              # 
#                                                                             #
# INPUT PARAMETERS:                                                           #
# session_handler : (Mandatory) Session-ID of the DUT                         #
# bridge_instance : (Mandatory) Bridge Instance                               #
# group_id        : (Mandatory) Group-ID of protocol                          #
# vlan_id         : (Mandatory) Vlan ID                                       #
# port_num        : (Mandatory) Port Number                                   #
#                                                                             #
#  DEFINITION       : Removes VID set from Group ID.                          #
#                                                                             #
#  OUTPUT PARAMETERS:                                                         #
#                                                                             #
#                     NIL.                                                    #
#                                                                             #
#  RETURNS          : 1 on success     			     		      #
#                     0 on failure     			     		      #
#                     2 if the ATTEST has to do the parsing.                  #
#                                                                             #
###############################################################################


proc callback_vlan_remove_vid_set {args} {

  array set param $args

       set session_handler $param(-session_handler)
       set bridge_instance $param(-bridge_instance)
       set group_id        $param(-group_id)
       set vlan_id         $param(-vlan_id)
       set port_num        $param(-port_num)
       set cmd_type        $param(-cmd_type)

#       if {![Send_cli_command  -session_handler   $session_handler \
#                               -cmd_type          $cmd_type\
#                               -parameter1        $bridge_instance\
#                               -parameter2        $vlan_id \
#                               -parameter3        $port_num\
#                               -parameter4        $group_id]} {
#
#         puts "callback_vlan_remove_vid_set: Error! Unable to remove VID set from Group ID"
#          return 0
#       }
#   return 1

return 2
}



#############################################################################
#27.                                                                        #
#  								   	    #
#  PROCEDURE NAME   : vlan_convert_macaddress_to_user_dut                   #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  macaddress       : (Mandatory) MAC Address to be converted.              #
#                                                                           #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                                                                           #
#  mac_address      : (Mandatory) Mac Address                               #
#                                                                           #
#  RETURNS          : 1 on success                                          #
#                     0 on failure                                          #
#                     2 if the ATTEST has to do the parsing                 #
#                                                                           #
#                                                                           #
#############################################################################

proc vlan_convert_macaddress_to_user_dut {args} {

  array set param $args
  set macaddress $param(-macaddress)

  set i 0
  foreach index [split $macaddress :] {
  set mac$i $index
  incr i
  }

  set macaddress $mac0$mac1.$mac2$mac3.$mac4$mac5

  return $macaddress
}

#############################################################################
#28.                                                                        #
#  								   	    #
#  PROCEDURE NAME   : wrsbin2dec                                            #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  num              : (Mandatory) Binary value (Eg: 10000)                  #
#                                                                           #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                                                                           #
#  ans              : (Mandatory) Decimal value (Eg :16)                    #
#                                                                           #
#  RETURNS          : 1 on success                                          #
#                     0 on failure                                          #
#                     2 if the ATTEST has to do the parsing                 #
#                                                                           #
#                                                                           #
#############################################################################
proc wrsbin2dec num {
   set num h[string map {1 i 0 o} $num]
   while {[regexp {[io]} $num]} {
      set num\
         [string map {0o 0 0i 1 1o 2 1i 3 2o 4 2i 5 3o 6 3i 7 4o 8 4i 9 ho h hi h1}\
            [string map {0 o0 1 o1 2 o2 3 o3 4 o4 5 i0 6 i1 7 i2 8 i3 9 i4} $num]]
   }
   set ans [string range $num 1 end]
   return $ans
}

#############################################################################
#27.                                                                        #
#  								   	    #
#  PROCEDURE NAME   : vlan_convert_dutmac_to_userMac                        #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  macaddress       : (Mandatory) MAC Address to be converted.              #
#                                (Eg : 0222.2222.2222)                      #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                                                                           #
#  macaddress       : (Mandatory) Mac Address (Eg: 00:22:22:22:22:22)       #
#                                                                           #
#  RETURNS          : 1 on success                                          #
#                     0 on failure                                          #
#                     2 if the ATTEST has to do the parsing                 #
#                                                                           #
#                                                                           #
#############################################################################

proc vlan_convert_dutmac_to_userMac {args} {

  array set param $args
  set macaddress $param(-macaddress)

  set i 0
  foreach index [split $macaddress .] {
  set mac$i [string range $index 0 1]
  incr i
  set mac$i [string range $index 2 3]
  incr i
  }

  set macaddress $mac0:$mac1:$mac2:$mac3:$mac4:$mac5

  return $macaddress
}
#############################################################################
#28.                                                                        #
#  								   	    #
#  PROCEDURE NAME   : HextoBin                                              #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  hexvalue         : (Mandatory) Hexadecimal value to be given (Eg:10)     #
#                                                                           #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                                                                           #
#  bits       : (Mandatory) Binary value of the input(Eg : 10000            #
#                                                                           #
#  RETURNS          : 1 on success                                          #
#                     0 on failure                                          #
#                     2 if the ATTEST has to do the parsing                 #
#                                                                           #
#                                                                           #
#############################################################################
proc HextoBin hex {
    set t [list 0 0000 1 0001 2 0010 3 0011 4 0100 \
            5 0101 6 0110 7 0111 8 1000 9 1001 \
            a 1010 b 1011 c 1100 d 1101 e 1110 f 1111 \
            A 1010 B 1011 C 1100 D 1101 E 1110 F 1111]
    regsub {^0[xX]} $hex {} hex
    return [string map -nocase $t $hex]
}


#############################################################################
#29.                                                                        #
#  								   	    #
#  PROCEDURE NAME   : Portfinder                                            #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#  Binary value     : (Mandatory) Binary value to be given (Eg: 11000)      #
#                                                                           #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                                                                           #
#  myports          : (Mandatory)List of ports (Eg: 5 4)                    #
#                                                                           #
#  RETURNS          : 1 on success                                          #
#                     0 on failure                                          #
#                     2 if the ATTEST has to do the parsing                 #
#                                                                           #
#                                                                           #
#############################################################################
# For Binary value 11000(Means the port 4 and 5) , Portfinder helps to find 
# the port 4 and 5 and give the output in list
proc Portfinder value {
    set lengthofvalue [string length $value]
    set myports ""
    for {set i 0} {$i <= $lengthofvalue} {incr i} {
        set initial [string first 1 $value $i]
        if {$initial != -1} {
            lappend myports [expr $lengthofvalue-$initial]
            set i $initial
        }
    }
    return $myports
}

