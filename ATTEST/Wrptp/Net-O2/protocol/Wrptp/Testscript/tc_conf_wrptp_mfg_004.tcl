#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_wrptp_mfg_004                                   #
# Test Case Version : 1.1                                                     #
# Component Name    : ATTEST WRPTP CONFORMANCE TEST SUITE                     #
# Module Name       : Message Format Group (MFG)                              #
#                                                                             #
# Title             : WRPTP Announce message - transport is over IPv4/UDP     #
#                                                                             #
# Purpose           : To verify that a WRPTP enabled device sends WRPTP       #
#                     Announce message in correct format when transport is    #
#                     over IPv4/UDP.                                          #
#                                                                             #
# Reference         : White Rabbit Specification v2.0 July 2011, Clause 6.5.2 #
#                     Pages 27 and 28                                         #
#                                                                             #
# Conformance Type  : MUST                                                    #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 001                                                     #
# Test Topology     : 1                                                       #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode = One-step/Two-step> |           #
#           |                     <Transport Protocol = IPv4/UDP> |           #
#           |            <Configure Priority1 = X, Priority2 = Y> | P1        #
#           |                                      <Enable WRPTP> | P1        #
#           |                   <Configure wrConfig = WR_M_AND_S> | P1        #
#           |      <Configure default values for knownDeltaTx and | P1        #
#           |                                       knownDeltaRx> |           #
#           |                                                     |           #
#           |                 WRPTP ANNOUNCE [messageType = 0x0B, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x2000] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     TEE       = Test Execution Engine                                       #
#     DUT       = Device Under Test                                           #
#     T1        = TEE's port 1                                                #
#     P1        = DUT's port 1                                                #
#     PTP       = Precision Time Protocol                                     #
#     WRPTP     = White Rabbit Precision Time Protocol                        #
#     OC        = Ordinary Clock                                              #
#     BC        = Boundary Clock                                              #
#     DN1       = Domain Number 1                                             #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure Network Transport Protocol as IPv4/UDP.                  #
#    vi.   Configure default values for Priority1 = X and Priority2 = Y.      #
#   vii.   Enable WRPTP on port P1.                                           #
#  viii.   Configure wrConfig = WR_M_AND_S.                                   #
#    ix.   Configure default values for knownDeltaTx and knownDeltaRx on P1.  #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Verify that DUT transmits WRPTP ANNOUNCE message on port P1 with   #
#          following parameters.                                              #
#                                                                             #
#               Ethernet Header                                               #
#                 1) Source MAC                = Unicast MAC                  #
#                 2) Destination MAC           = Unicast MAC or               #
#                                                01:00:5E:00:01:81            #
#                 3) EtherType                 = IPv4 (0x800)                 #
#                                                                             #
#               IPv4 Fields                                                   #
#                 4) IP Protocol = 17 (UDP)                                   #
#                 5) Destination IP = 224.0.1.129                             #
#                 6) Source IP = Unicast IP                                   #
#                 7) Checksum = Valid                                         #
#                                                                             #
#               UDP Fields                                                    #
#                 8) UDP Destination Port = 320 (General Message)             #
#                 9) Checksum = Valid                                         #
#                                                                             #
#               PTP Header                                                    #
#                10) transportSpecific         = 0 or 1, Reserved: 2-F        #
#                                                (4 bits)                     #
#                11) messageType               = 0x0B (4 bits)                #
#                12) Reserved Bits (1)         = 0 (4 bits)                   #
#                13) versionPTP                = 2 (4 bits)                   #
#                14) messageLength             = 64 (2 octets)                #
#                15) domainNumber              = 0 - 255, Reserved: 128 - 255 #
#                16) Reserved Bits (2)         = 0 (8 bits)                   #
#                17) flagField                 = 0x0000 - 0xFFFF              #
#                18) correctionField           = 0 (8 octets)                 #
#                19) Reserved Bits (3)         = 0 (32 bits)                  #
#                20) sourcePortIdentity                                       #
#                     a)clockIdentity          = 8 octets                     #
#                     b)portNumber             = 2 octets                     #
#                21) sequenceId                = 0 - 65535                    #
#                22) controlField              = 5                            #
#                23) logMessageInterval        = 0 - 4                        #
#                24) originTimestamp                                          #
#                     a)secondsField           = 6 octets                     #
#                     a)nanosecondsField       = 4 octets                     #
#                25) currentUtcOffset          = -32768 to 32767              #
#                26) Reserved Bits (4)         = 0 (8 bits)                   #
#                27) grandmasterPriority1      = 0 - 255                      #
#                28) grandmasterClockQuality                                  #
#                    a) clockClass             = 0 - 255                      #
#                    b) clockAccuracy          = 0x00 - 0xFF                  #
#                    c) clockVariance          = 0 - 65535                    #
#                29) grandmasterPriority2      = 0 - 255                      #
#                30) grandmasterIdentity       = 0x0000000000000000 -         #
#                                                0xFFFFFFFFFFFFFFFF           #
#                31) stepsRemoved              = 0 - 65535                    #
#                32) timeSource                = 0x00 - 0xFF                  #
#               TLV Header                                                    #
#                33) tlvType                   = 0x0003                       #
#                34) lengthField               = 10 (2 octets)                #
#                35) OrganizationId            = 0x080030                     #
#                36) magicNumber               = 0xDEAD                       #
#                37) versionNumber             = 0x01                         #
#                38) wrMessageId               = 0x2000                       #
#                39) wrFlags                   = 0x0000 - 0xFFFF              #
#                    a) wrConfig               = 0x3                          #
#                    b) calibrated             = 1                            #
#                    c) wrModeOn               = 0                            #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          Aug/2018      CERN          Initial                            #
# 1.1          Apr/2019      JC.BAU/CERN   Improve execution time             #
#                                                                             #
###############################################################################
# Copyright (c) 2018 CERN                                                     #
###############################################################################

global dut_log_msg
global tee_log_msg
global session_id
global step

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def    "To verify that a WRPTP enabled device sends WRPTP Announce\
			   message in correct format when transport is over IPv4/UDP."

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set dut_port_1                     $::dut_port_num_1
set vlan_id                        $::wrptp_vlan_id
set dut_ip                         $::dut_test_port_ip
set dut_dest_ip                    $::dut_test_port_ip
set tee_dest_ip                    $::tee_test_port_ip
set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT

set domain                         $::DEFAULT_DOMAIN
set wrptp_tlv                      $::ORG_EXT_TLV
set wrmsg_id                       $::wrMessageID(ANN_SUFIX)
set infinity                       $::INFINITY
set idle                           $::WRPTP_PORT_STATE(IDLE)
set clock_step                     $::ptp_dut_clock_step
set announce_timeout               [expr $::wrptp_announce_timeout + 60]
set step                           0

########################### END - INITIALIZATION ##############################

if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IEEE) } {

	TC_ABORT -reason "This test case is applicable only for transport type IPv4/UDP"\
		-tc_def  $tc_def

}

wrptp::display_interface_details

wrptp::dut_Initialization

proc cleanup {} {
	global step
	if { $step != 0 } {
		wrptp::dut_cleanup_setup_001
		wrptp::dut_commit_changes
	}
}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

### (Initial Part) ###

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure Network Transport Protocol as IPv4/UDP.                  #
#    vi.   Configure default values for Priority1 = X and Priority2 = Y.      #
#   vii.   Enable WRPTP on port P1.                                           #
#  viii.   Configure wrConfig = WR_M_AND_S.                                   #
#    ix.   Configure default values for knownDeltaTx and knownDeltaRx on P1.  #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP1)"

if {!([wrptp::dut_configure_setup_001] && [wrptp::dut_commit_changes])} {

	LOG -msg "$dut_log_msg(INIT_SETUP1_F)"

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP1_F)"\
		-tc_def  $tc_def
	return 0
}

###############################################################################
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(INIT)"

wrptp::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

	if {![WRPTP_UNICAST_PRE_REQUISITE_CONFIGURATION\
						-port_num       $tee_port_1\
						-tee_mac        $tee_mac\
						-tee_ip         $tee_ip\
						-dut_ip         $dut_ip\
						-vlan_id        $vlan_id\
						-dut_mac        tee_dest_mac\
						-timeout        $arp_timeout]} {

		TEE_CLEANUP

		TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
			-tc_def  $tc_def

		return 0
	}

} else {
	set tee_dest_ip             $::PTP_E2E_IP
	set tee_dest_mac            $::PTP_E2E_MAC
}

if {![pltLib::create_filter_wrptp_pkt\
			  -port_num                $tee_port_1\
			  -ether_type              $ptp_ethtype\
			  -vlan_id                 $vlan_id\
			  -ids                     id1]} {

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
		-tc_def  $tc_def

	return 0
}

set filter_id [lindex $id1 end]

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 3 : Verify that DUT transmits WRPTP ANNOUNCE message on port P1 with   #
#          following parameters.                                              #
#                                                                             #
#               Ethernet Header                                               #
#                 1) Source MAC                = Unicast MAC                  #
#                 2) Destination MAC           = Unicast MAC or               #
#                                                01:1B:19:00:00:00            #
#                 3) EtherType                 = IPv4 (0x800)                 #
#                                                                             #
#               IPv4 Fields                                                   #
#                 4) IP Protocol = 17 (UDP)                                   #
#                 5) Destination IP = 224.0.1.129                             #
#                 6) Source IP = Unicast IP                                   #
#                 7) Checksum = Valid                                         #
#                                                                             #
#               UDP Fields                                                    #
#                 8) UDP Destination Port = 320 (General Message)             #
#                 9) Checksum = Valid                                         #
#                                                                             #
#               PTP Header                                                    #
#                10) transportSpecific         = 0 or 1, Reserved: 2-F        #
#                                                (4 bits)                     #
#                11) messageType               = 0x0B (4 bits)                #
#                12) Reserved Bits (1)         = 0 (4 bits)                   #
#                13) versionPTP                = 2 (4 bits)                   #
#                14) messageLength             = 64 (2 octets)                #
#                15) domainNumber              = 0 - 255, Reserved: 128 - 255 #
#                16) Reserved Bits (2)         = 0 (8 bits)                   #
#                17) flagField                 = 0x0000 - 0xFFFF              #
#                18) correctionField           = 0 (8 octets)                 #
#                19) Reserved Bits (3)         = 0 (32 bits)                  #
#                20) sourcePortIdentity                                       #
#                     a)clockIdentity          = 8 octets                     #
#                     b)portNumber             = 2 octets                     #
#                21) sequenceId                = 0 - 65535                    #
#                22) controlField              = 5                            #
#                23) logMessageInterval        = 0 - 4                        #
#                24) originTimestamp                                          #
#                     a)secondsField           = 6 octets                     #
#                     a)nanosecondsField       = 4 octets                     #
#                25) currentUtcOffset          = -32768 to 32767              #
#                26) Reserved Bits (4)         = 0 (8 bits)                   #
#                27) grandmasterPriority1      = 0 - 255                      #
#                28) grandmasterClockQuality                                  #
#                    a) clockClass             = 0 - 255                      #
#                    b) clockAccuracy          = 0x00 - 0xFF                  #
#                    c) clockVariance          = 0 - 65535                    #
#                29) grandmasterPriority2      = 0 - 255                      #
#                30) grandmasterIdentity       = 0x0000000000000000 -         #
#                                                0xFFFFFFFFFFFFFFFF           #
#                31) stepsRemoved              = 0 - 65535                    #
#                32) timeSource                = 0x00 - 0xFF                  #
#               TLV Header                                                    #
#                33) tlvType                   = 0x0003                       #
#                34) lengthField               = 10 (2 octets)                #
#                35) OrganizationId            = 0x080030                     #
#                36) magicNumber               = 0xDEAD                       #
#                37) versionNumber             = 0x01                         #
#                38) wrMessageId               = 0x2000                       #
#                39) wrFlags                   = 0x0000 - 0xFFFF              #
#                    a) wrConfig               = 0x3                          #
#                    b) calibrated             = 1                            #
#                    c) wrModeOn               = 0                            #
#                                                                             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_V)"

if {![wrptp::check_announce \
		   -port_num                $tee_port_1\
		   -domain_number           $domain\
		   -timeout                 $announce_timeout\
		   -tlv_type                $wrptp_tlv\
		   -message_id              $wrmsg_id\
		   -filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL   -reason "DUT does not send WRPTP Announce message in\
								 correct format when transport is over IPv4/UDP."\
		-tc_def  $tc_def
	return 0
}

TEE_CLEANUP

TC_CLEAN_AND_PASS   -reason "DUT sends WRPTP Announce message in correct format when\
							 transport is over IPv4/UDP."\
	-tc_def  $tc_def

return 1
############################ END OF TEST CASE #################################
