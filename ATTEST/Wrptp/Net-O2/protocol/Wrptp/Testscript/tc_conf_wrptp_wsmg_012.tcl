#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_wrptp_wsmg_012                                  #
# Test Case Version : 1.3                                                     #
# Component Name    : ATTEST WRPTP CONFORMANCE TEST SUITE                     #
# Module Name       : WRPTP State Machine Group (WSMG)                        #
#                                                                             #
# Title             : WR Master transition of WRPTP portState from            #
#                     RESP_CALIB_REQ to WR_LINK_ON before EXC_TIMEOUT_RETRY   #
#                     occurs - otherPortCalPeriod and otherPortCalRetry       #
#                     are 0x0                                                 #
#                                                                             #
# Purpose           : To verify that a WRPTP enabled device with it's port in #
#                     PTP Master sends WR_MODE_ON message and transitions     #
#                     it's port state from RESP_CALIB_REQ to WR_LINK_ON state #
#                     on the reception of CALIBRATED message before           #
#                     EXC_TIMEOUT_RETRY occurs when otherPortCalPeriod and    #
#                     otherPortCalRetry are 0x0.                              #
#                                                                             #
# Reference         : White Rabbit Specification v2.0 July 2011, Clause 6.7.3 #
#                     Page 35, Clause 6.7.4 Page 37, Figure 27 Page 62        #
#                                                                             #
# Conformance Type  : MUST                                                    #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 001                                                     #
# Test Topology     : 1                                                       #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode = One-step/Two-step> |           #
#           |          <Transport Protocol = IEEE 802.3/Ethernet> |           #
#           |            <Configure Priority1 = X, Priority2 = Y> | P1        #
#           |                                      <Enable WRPTP> | P1        #
#           |                   <Configure wrConfig = WR_M_AND_S> | P1        #
#           |      <Configure default values for knownDeltaTx and | P1        #
#           |                                       knownDeltaRx> |           #
#           |                                                     |           #
#           |                 WRPTP ANNOUNCE [messageType = 0x0B, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x2000] |           #
#           |                         {sourcePortIdentity = SPI1} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP SLAVE_PRESENT [messageType = 0x0C,            |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1000]             |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                     WRPTP LOCK [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1001] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP LOCKED [messageType = 0x0C,                   |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1002]             |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                WRPTP CALIBRATE [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1003] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |               WRPTP CALIBRATED [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1004] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP CALIBRATE [messageType = 0x0C,                |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1003,             |           #
#           | calSendPattern = FALSE, calRetry = 0,               |           #
#           | calPeriod = 0]                                      |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |      <Wait for 300ms to complete 1 x                |           #
#           |                     WR_RESP_CALIB_REQ_TIMEOUT>      |           #
#           |                                                     |           #
#           | WRPTP CALIBRATED [messageType = 0x0C,               |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1004,             |           #
#           | deltaTx = 0, deltaRx = 0]                           |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                  WRPTP WR_MODE_ON [MSG_TYPE = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1005] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     TEE       = Test Execution Engine                                       #
#     DUT       = Device Under Test                                           #
#     T1        = TEE's port 1                                                #
#     P1        = DUT's port 1                                                #
#     PTP       = Precision Time Protocol                                     #
#     WRPTP     = White Rabbit Precision Time Protocol                        #
#     OC        = Ordinary Clock                                              #
#     BC        = Boundary Clock                                              #
#     DN1       = Domain Number 1                                             #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure Network Transport Protocol as IEEE 802.3/Ethernet.       #
#    vi.   Configure default values for Priority1 = X and Priority2 = Y.      #
#   vii.   Enable WRPTP on port P1.                                           #
#  viii.   Configure wrConfig = WR_M_AND_S.                                   #
#    ix.   Configure default values for knownDeltaTx and knownDeltaRx on P1.  #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Observe that DUT transmits WRPTP ANNOUNCE message on the port P1   #
#          with following parameters and store sourcePortIdentity as SPI1.    #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                                                                             #
# Step 4 : Send WRPTP SLAVE_PRESENT message on port T1 with following         #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
#                                                                             #
# Step 5 : Observe that DUT transmits WRPTP LOCK message on port P1 with      #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
#                                                                             #
# Step 6 : Send WRPTP LOCKED message on port T1 with following parameters.    #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1002                              #
#                                                                             #
# Step 7 : Observe that DUT transmits WRPTP CALIBRATE message on port P1 with #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                                                                             #
# Step 8 : Observe that DUT transmits WRPTP CALIBRATED message on port P1 with#
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                                                                             #
# Step 9 : Send WRPTP CALIBRATE message on port T1 with following parameters. #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                  calSendPattern       = FALSE                               #
#                  calRetry             = 0                                   #
#                  calPeriod            = 0                                   #
#                                                                             #
# Step 10: Wait for 300ms to complete 1 x WR_RESP_CALIB_REQ_TIMEOUT.          #
#                                                                             #
# Step 11: Send WRPTP CALIBRATED message on port T1 with following parameters #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                  deltaTx              = 0                                   #
#                  deltaRx              = 0                                   #
#                                                                             #
# Step 12: Verify that DUT transmits WRPTP WR_MODE_ON message on the port P1  #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1005                              #
#                                                                             #
# Note :                                                                      #
#                                                                             #
#   Values mentioned in above steps are examples based on default values.     #
#   However, the test will be executed using the values given in ATTEST GUI   #
#   (Selected configuration in ATTEST Configuration Manager > Protocol        #
#   Options > WRPTP > WRPTP Attributes). Hence, the values displayed in test  #
#   logs may differ from those mentioned in the steps.                        #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          Jul/2018      CERN          Initial                            #
# 1.1          Dec/2018      CERN          a) Modified waiting timeout to     #
#                                             receive LOCK, CALIBRATE,        #
#                                             CALIBRATED and WR_MODE_ON       #
#                                             messages.                       #
#                                          b) Replaced wrStateTimeout of      #
#                                             RESP_CALIB_REQ to               #
#                                             WR_RESP_CALIB_REQ_TIMEOUT.      #
# 1.2          Jan/2019      CERN          a) Changed default value of        #
#                                             RESP_CALIB_REQ_TIMEOUT = 300ms. #
#                                          b) Resolved non-transmission of    #
#                                             messages at background while    #
#                                             waiting for state transitions.  #
# 1.3          Apr/2019      JC.BAU/CERN   Improve execution time             #
#                                                                             #
###############################################################################
# Copyright (c) 2018 - 2019 CERN                                              #
###############################################################################

global dut_log_msg
global tee_log_msg
global tee_rx_errlog
global session_id
global step

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def    "To verify that a WRPTP enabled device with it's port in\
			   PTP Master sends WR_MODE_ON message and transitions\
			   it's port state from RESP_CALIB_REQ to WR_LINK_ON state\
			   on the reception of CALIBRATED message before\
			   EXC_TIMEOUT_RETRY occurs when otherPortCalPeriod and\
			   otherPortCalRetry are 0x0."

########################### START - INITIALIZATION ############################

set tee_port_1               $::tee_port_num_1
set dut_port_1               $::dut_port_num_1
set dut_ip                   $::dut_test_port_ip
set vlan_id                  $::wrptp_vlan_id
set dut_dest_ip              $::dut_test_port_ip
set tee_dest_ip              $::tee_test_port_ip
set ptp_comm_model           $::ptp_comm_model
set arp_timeout              $::ARP_TIMEOUT
set ptp_ethtype              $::PTP_ETHTYPE

set announce_timeout         [expr $::wrptp_announce_timeout + 60]
set signal_timeout           $::wrptp_signal_timeout

set wrptp_tlv                $::ORG_EXT_TLV
set domain                   $::DEFAULT_DOMAIN

set ann_msg_id               $::wrMessageID(ANN_SUFIX)
set slave_present_msg_id     $::wrMessageID(SLAVE_PRESENT)
set lock_msg_id              $::wrMessageID(LOCK)
set locked_msg_id            $::wrMessageID(LOCKED)
set calibrate_msg_id         $::wrMessageID(CALIBRATE)
set calibrated_msg_id        $::wrMessageID(CALIBRATED)
set wr_mode_on_msg_id        $::wrMessageID(WR_MODE_ON)

set step 0

########################### END - INITIALIZATION ##############################

wrptp::display_interface_details

wrptp::dut_Initialization

proc cleanup {} {
global step
if { $step != 0 } {
	wrptp::dut_cleanup_setup_001
	wrptp::dut_commit_changes
}
}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

### (Initial Part) ###

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure Network Transport Protocol as IEEE 802.3/Ethernet.       #
#    vi.   Configure default values for Priority1 = X and Priority2 = Y.      #
#   vii.   Enable WRPTP on port P1.                                           #
#  viii.   Configure wrConfig = WR_M_AND_S.                                   #
#    ix.   Configure default values for knownDeltaTx and knownDeltaRx on P1.  #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP1)"

if {!( [wrptp::dut_configure_setup_001] && [wrptp::dut_commit_changes]) } {

	LOG -msg "$dut_log_msg(INIT_SETUP1_F)"

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP1_F)"\
		-tc_def  $tc_def
	return 0
}



###############################################################################
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(INIT)"

wrptp::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

	if {![WRPTP_UNICAST_PRE_REQUISITE_CONFIGURATION\
						-port_num       $tee_port_1\
						-tee_mac        $tee_mac\
						-tee_ip         $tee_ip\
						-dut_ip         $dut_ip\
						-vlan_id        $vlan_id\
						-dut_mac        tee_dest_mac\
						-timeout        $arp_timeout]} {

		TEE_CLEANUP

		TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
			-tc_def  $tc_def

		return 0
	}

} else {
	set tee_dest_ip             $::PTP_E2E_IP
	set tee_dest_mac            $::PTP_E2E_MAC
}

if {![pltLib::create_filter_wrptp_pkt\
			  -port_num                $tee_port_1\
			  -ether_type              $ptp_ethtype\
			  -vlan_id                 $vlan_id\
			  -ids                     id1]} {

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
		-tc_def  $tc_def

	return 0
}

set filter_id [lindex $id1 end]

wrptp::reset_capture_stats\
	-port_num           $tee_port_1



###############################################################################
# Step 3 : Observe that DUT transmits WRPTP ANNOUNCE message on the port P1   #
#          with following parameters and store sourcePortIdentity as SPI1.    #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![wrptp::recv_announce\
			-port_num                   $tee_port_1\
			-filter_id                  $filter_id\
			-domain_number              $domain\
			-tlv_type                   $wrptp_tlv\
			-message_id                 $ann_msg_id\
			-timeout                    $announce_timeout\
			-error_reason               error_reason\
			-recvd_src_port_number      recvd_src_port_number\
			-recvd_src_clock_identity   recvd_src_clock_identity]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0

}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1



###############################################################################
# Step 4 : Send WRPTP SLAVE_PRESENT message on port T1 with following         #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(SLAVE_PRESENT_TX_P1)"

if {![wrptp::send_signal\
			-port_num               $tee_port_1\
			-dest_mac               $tee_dest_mac\
			-src_mac                $tee_mac\
			-src_ip                 $tee_ip\
			-dest_ip                $tee_dest_ip\
			-domain_number          $domain\
			-target_port_number     $recvd_src_port_number\
			-target_clock_identity  $recvd_src_clock_identity\
			-tlv_type               $wrptp_tlv\
			-message_id             $slave_present_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SLAVE_PRESENT_TX_P1_F)"\
		-tc_def  $tc_def

	return 0

}



###############################################################################
# Step 5 : Observe that DUT transmits WRPTP LOCK message on port P1 with      #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCK_RX_P1_O)"

if {![wrptp::recv_signal\
			-port_num        $tee_port_1\
			-filter_id       $filter_id\
			-domain_number   $domain\
			-error_reason    error_reason\
			-tlv_type        $wrptp_tlv\
			-message_id      $lock_msg_id\
			-timeout         $::wrptp_m_lock_timeout_recv]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(LOCK_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0

}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1



###############################################################################
# Step 6 : Send WRPTP LOCKED message on port T1 with following parameters.    #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1002                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCKED_TX_P1)"

if {![wrptp::send_signal\
			 -port_num               $tee_port_1\
			 -dest_mac               $tee_dest_mac\
			 -src_mac                $tee_mac\
			 -src_ip                 $tee_ip\
			 -dest_ip                $tee_dest_ip\
			 -domain_number          $domain\
			 -target_port_number     $recvd_src_port_number\
			 -target_clock_identity  $recvd_src_clock_identity\
			 -tlv_type               $wrptp_tlv\
			 -message_id             $locked_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(LOCKED_TX_P1_F)"\
		-tc_def  $tc_def

	return 0
}



###############################################################################
# Step 7 : Observe that DUT transmits WRPTP CALIBRATE message on port P1 with #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATE_RX_P1_O)"

if {![wrptp::recv_signal\
			-port_num           $tee_port_1\
			-filter_id          $filter_id\
			-domain_number      $domain\
			-timeout            $::wrptp_calibration_timeout_recv\
			-error_reason       error_reason\
			-tlv_type           $wrptp_tlv\
			-message_id         $calibrate_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(CALIBRATE_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0

}



###############################################################################
# Step 8 : Observe that DUT transmits WRPTP CALIBRATED message on port P1 with#
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATED_RX_P1_O)"

if {![wrptp::recv_signal\
			-port_num            $tee_port_1\
			-filter_id           $filter_id\
			-domain_number       $domain\
			-timeout             $::wrptp_calibrated_timeout_recv\
			-error_reason        error_reason\
			-tlv_type            $wrptp_tlv\
			-message_id          $calibrated_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(CALIBRATED_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1



###############################################################################
# Step 9 : Send WRPTP CALIBRATE message on port T1 with following parameters. #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                  calSendPattern       = FALSE                               #
#                  calRetry             = 0                                   #
#                  calPeriod            = 0                                   #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATE_TX_P1)"

set cal_retry    0
set cal_period   0

if {![wrptp::send_signal\
		   -port_num                 $tee_port_1\
		   -dest_mac                 $tee_dest_mac\
		   -src_mac                  $tee_mac\
		   -src_ip                   $tee_ip\
		   -dest_ip                  $tee_dest_ip\
		   -domain_number            $domain\
		   -target_port_number       $recvd_src_port_number\
		   -target_clock_identity    $recvd_src_clock_identity\
		   -tlv_type                 $wrptp_tlv\
		   -cal_retry                $cal_retry\
		   -cal_period               $cal_period\
		   -message_id               $calibrate_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(CALIBRATE_TX_P1_F)"\
		-tc_def  $tc_def

	return 0

}



wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 10: Wait for 300ms to complete 1 x WR_RESP_CALIB_REQ_TIMEOUT.          #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(WAIT_ONE_RESP_CALIB_REQ_TIMEOUT)"

wrptp::wait_msec $::wrptp_resp_calib_req_timeout



###############################################################################
# Step 11: Send WRPTP CALIBRATED message on port T1 with following parameters #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                  deltaTx              = 0                                   #
#                  deltaRx              = 0                                   #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATED_TX_P1)"

set delta_tx    0
set delta_rx    0

if {![wrptp::send_signal\
			 -port_num               $tee_port_1\
			 -dest_mac               $tee_dest_mac\
			 -src_mac                $tee_mac\
			 -src_ip                 $tee_ip\
			 -dest_ip                $tee_dest_ip\
			 -domain_number          $domain\
			 -target_port_number     $recvd_src_port_number\
			 -target_clock_identity  $recvd_src_clock_identity\
			 -tlv_type               $wrptp_tlv\
			 -delta_tx               $delta_tx\
			 -delta_rx               $delta_rx\
			 -message_id             $calibrated_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(CALIBRATED_TX_P1_F)"
	-tc_def  $tc_def

	return 0

}



###############################################################################
# Step 12: Verify that DUT transmits WRPTP WR_MODE_ON message on the port P1  #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1005                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(WR_MODE_ON_RX_P1_V)"

if {![wrptp::recv_signal\
			-port_num          $tee_port_1\
			-filter_id         $filter_id\
			-domain_number     $domain\
			-tlv_type          $wrptp_tlv\
			-error_reason      error_reason\
			-message_id        $wr_mode_on_msg_id\
			-timeout           $::wrptp_wr_link_on_timeout_recv]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL  -reason "DUT does not send WR_MODE_ON message on the\
								 reception of CALIBRATED message before\
								 EXC_TIMEOUT_RETRY occurs. $error_reason"\
		-tc_def  $tc_def

	return 0

}

TEE_CLEANUP

TC_CLEAN_AND_PASS -reason "DUT sends WR_MODE_ON message and transitions\
						   it's port state from RESP_CALIB_REQ to WR_LINK_ON state\
						   on the reception of CALIBRATED message before\
						   EXC_TIMEOUT_RETRY occurs."\
	-tc_def  $tc_def

return 1

################################# END OF TESTCASE #############################
