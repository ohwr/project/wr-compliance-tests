#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_wrptp_wcg_001                                   #
# Test Case Version : 1.6                                                     #
# Component Name    : ATTEST WRPTP CONFORMANCE TEST SUITE                     #
# Module Name       : WRPTP Configuration Group (WCG)                         #
#                                                                             #
# Title             : Default initialization values for WRPTP attributes      #
#                                                                             #
# Purpose           : To verify that a WRPTP enabled device stores all        #
#                     attributes with default initialization values. Checking #
#                     that the following attributes have correct default      #
#                     values.                                                 #
#                       1) defaultDS.domainNumber = 0                         #
#                       2) defaultDS.priority1 = 64                           #
#                       3) defaultDS.priority2 = 128                          #
#                       4) portDS.logSyncInterval = 0                         #
#                       5) portDS.knownDeltaTx = Default value                #
#                       6) portDS.knownDeltaRx = Default value                #
#                       7) portDS.wrConfig = WR_M_AND_S                       #
#                       8) portDS.calPeriod = 3000us                          #
#                       9) portDS.calRetry = 0                                #
#                      10) WR_PRESENT_TIMEOUT = 1000ms                        #
#                      11) WR_M_LOCK_TIMEOUT = 15000ms                        #
#                      12) WR_LOCKED_TIMEOUT = 300ms                          #
#                      13) WR_RESP_CALIB_REQ_TIMEOUT = 300ms                  #
#                      14) WR_CALIBRATED_TIMEOUT = 300ms                      #
#                      15) WR_STATE_RETRY = 3                                 #
#                     Note: The default values of these attributes can be     #
#                     changed through ATTEST GUI (Go to Configuration Manager #
#                     and select desired configuration, go to                 #
#                     Protocol Options > WRPTP > WRPTP Attributes).           #
#                                                                             #
# Reference         : White Rabbit Specification v2.0 July 2011, Clause 6.3   #
#                     Page 16                                                 #
#                                                                             #
# Conformance Type  : MUST                                                    #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 003                                                     #
# Test Topology     : 1                                                       #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode = One-step/Two-step> |           #
#           |          <Transport Protocol = IEEE 802.3/Ethernet> |           #
#           |                                      <Enable WRPTP> | P1        #
#           |                                                     |           #
#           |                 WRPTP ANNOUNCE [messageType = 0x0B, |           #
#           |        domainNumber = 0, grandmasterPriority1 = 64, |           #
#           |       grandmasterPriority2 = 128, tlvType = 0x0003, |           #
#           |               wrMessageId = 0x2000, wrConfig = 0x3] |           #
#           |                         {sourcePortIdentity = SPI1} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP SLAVE_PRESENT [messageType = 0x0C,            |           #
#           | domainNumber = 0, targetPortIdentity = SPI1,        |           #
#           | tlvType = 0x0003, wrMessageId = 0x1000]             |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                     WRPTP LOCK [messageType = 0x0C, |           #
#           |                 domainNumber = 0, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1001] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP LOCKED [messageType = 0x0C,                   |           #
#           | domainNumber = 0, targetPortIdentity = SPI1,        |           #
#           | tlvType = 0x0003, wrMessageId = 0x1002]             |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                WRPTP CALIBRATE [messageType = 0x0C, |           #
#           |                 domainNumber = 0, tlvType = 0x0003, |           #
#           |       wrMessageId = 0x1003, calSendPattern = FALSE, |           #
#           |                   calRetry = 0, calPeriod = 3000us] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |               WRPTP CALIBRATED [messageType = 0x0C, |           #
#           |                 domainNumber = 0, tlvType = 0x0003, |           #
#           |      wrMessageId = 0x1004, deltaTx = Default value, |           #
#           |            deltaRx' = knownDeltaRx + {0 - 16000ps}] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP CALIBRATE [messageType = 0x0C,                |           #
#           | domainNumber = 0, targetPortIdentity = SPI1,        |           #
#           | tlvType = 0x0003, wrMessageId = 0x1003,             |           #
#           | calSendPattern = FALSE, calRetry = 0,               |           #
#           | calPeriod = 3000us]                                 |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           | WRPTP CALIBRATED [messageType = 0x0C,               |           #
#           | domainNumber = 0, targetPortIdentity = SPI1,        |           #
#           | tlvType = 0x0003, wrMessageId = 0x1004,             |           #
#           | deltaTx = 0, deltaRx = 0]                           |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                  WRPTP WR_MODE_ON [MSG_TYPE = 0x0C, |           #
#           |                 domainNumber = 0, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1005] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                           SYNC [messageType = 0x00, |           #
#           |           domainNumber = 0, logMessageInterval = 0] |           #
#           |                           {receivedTimestamp = TS1} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                           SYNC [messageType = 0x00, |           #
#           |           domainNumber = 0, logMessageInterval = 0] |           #
#           |                           {receivedTimestamp = TS2} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                           SYNC [messageType = 0x00, |           #
#           |           domainNumber = 0, logMessageInterval = 0] |           #
#           |                           {receivedTimestamp = TS3} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |          <Check ((TS2 - TS1) + (TS3 - TS2))/2 = 1s> |           #
#           |                                                     |           #
#           |                                     <Disable WRPTP> | P1        #
#           |                                                     |           #
#           |                                      <Enable WRPTP> | P1        #
#           |                                                     |           #
#           |           <Wait for WRPTP to be enabled>            |           #
#           |                                                     |           #
#           |                 WRPTP ANNOUNCE [messageType = 0x0B, |           #
#           |        domainNumber = 0, grandmasterPriority1 = 64, |           #
#           |       grandmasterPriority2 = 128, tlvType = 0x0003, |           #
#           |               wrMessageId = 0x2000, wrConfig = 0x3] |           #
#           |                         {sourcePortIdentity = SPI1} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP SLAVE_PRESENT [messageType = 0x0C,            |           #
#           | domainNumber = 0, targetPortIdentity = SPI1,        |           #
#           | tlvType = 0x0003, wrMessageId = 0x1000]             |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                     WRPTP LOCK [messageType = 0x0C, |           #
#           |                 domainNumber = 0, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1001] |           #
#           |                           {receivedTimestamp = TS1} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                     WRPTP LOCK [messageType = 0x0C, |           #
#           |                 domainNumber = 0, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1001] |           #
#           |                           {receivedTimestamp = TS2} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                     WRPTP LOCK [messageType = 0x0C, |           #
#           |                 domainNumber = 0, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1001] |           #
#           |                           {receivedTimestamp = TS3} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |     <Check ((TS2 - TS1) + (TS3 - TS2))/2 = 15000ms> |           #
#           |                                                     |           #
#           |                                     <Disable WRPTP> | P1        #
#           |                                                     |           #
#           |                                      <Enable WRPTP> | P1        #
#           |                                                     |           #
#           |           <Wait for WRPTP to be enabled>            |           #
#           |                                                     |           #
#           |                 WRPTP ANNOUNCE [messageType = 0x0B, |           #
#           |        domainNumber = 0, grandmasterPriority1 = 64, |           #
#           |       grandmasterPriority2 = 128, tlvType = 0x0003, |           #
#           |               wrMessageId = 0x2000, wrConfig = 0x3] |           #
#           |                         {sourcePortIdentity = SPI1} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP SLAVE_PRESENT [messageType = 0x0C,            |           #
#           | domainNumber = 0, targetPortIdentity = SPI1,        |           #
#           | tlvType = 0x0003, wrMessageId = 0x1000]             |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                     WRPTP LOCK [messageType = 0x0C, |           #
#           |                 domainNumber = 0, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1001] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP LOCKED [messageType = 0x0C,                   |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1002]             |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                WRPTP CALIBRATE [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1003] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |               WRPTP CALIBRATED [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1004] |           #
#           |                           {receivedTimestamp = TS1} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |               WRPTP CALIBRATED [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1004] |           #
#           |                           {receivedTimestamp = TS2} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |               WRPTP CALIBRATED [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1004] |           #
#           |                           {receivedTimestamp = TS3} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |       <Check ((TS2 - TS1) + (TS3 - TS2))/2 = 300ms> |           #
#           |                                                     |           #
#           |                                     <Disable WRPTP> | P1        #
#           |                                                     |           #
#           |                                      <Enable WRPTP> | P1        #
#           |                                                     |           #
#           |           <Wait for WRPTP to be enabled>            |           #
#           |                                                     |           #
#           |                 WRPTP ANNOUNCE [messageType = 0x0B, |           #
#           |        domainNumber = 0, grandmasterPriority1 = 64, |           #
#           |       grandmasterPriority2 = 128, tlvType = 0x0003, |           #
#           |               wrMessageId = 0x2000, wrConfig = 0x3] |           #
#           |                         {sourcePortIdentity = SPI1} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP SLAVE_PRESENT [messageType = 0x0C,            |           #
#           | domainNumber = 0, targetPortIdentity = SPI1,        |           #
#           | tlvType = 0x0003, wrMessageId = 0x1000]             |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                     WRPTP LOCK [messageType = 0x0C, |           #
#           |                 domainNumber = 0, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1001] |           #
#           |                           {receivedTimestamp = TS1} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP LOCKED [messageType = 0x0C,                   |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1002]             |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                WRPTP CALIBRATE [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1003] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |               WRPTP CALIBRATED [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1004] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP CALIBRATE [messageType = 0x0C,                |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1003,             |           #
#           | calSendPattern = FALSE, calRetry = 0,               |           #
#           | calPeriod = 3000us]                                 |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |      <Wait for 150ms to complete 0.5 x              |           #
#           |           WR_RESP_CALIB_REQ_TIMEOUT and check       |           #
#           |                   WRPTP portState = RESP_CALIB_REQ> | P1        #
#           |                                                     |           #
#           |      <Wait for 600ms to complete 2 x                |           #
#           |                      WR_RESP_CALIB_REQ_TIMEOUT>     |           #
#           |                                                     |           #
#           |            <Check WRPTP portState = RESP_CALIB_REQ> | P1        #
#           |                                                     |           #
#           |      <Wait for 630ms to complete 2 x                |           #
#           |             WR_RESP_CALIB_REQ_TIMEOUT +             |           #
#           |              10% of WR_RESP_CALIB_REQ_TIMEOUT>      |           #
#           |                                                     |           #
#           |                      <Check WRPTP portState = IDLE> | P1        #
#           |                                                     |           #
#           |                                     <Disable WRPTP> | P1        #
#           |                                                     |           #
#           |                                      <Enable WRPTP> | P1        #
#           |                                                     |           #
#           | WRPTP ANNOUNCE [messageType = 0x0B,                 |           #
#           | domainNumber = DN1, grandmasterPriority1 = X - 1,   |           #
#           | tlvType = 0x0003, wrMessageId = 0x2000,             |           #
#           | wrConfig = 0x3, calibrated = 1, wrModeOn = 0]       |           #
#           | (sendCount = INFINITY)                              |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |            WRPTP SLAVE_PRESENT [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1000] |           #
#           |                           {receivedTimestamp = TS1} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |            WRPTP SLAVE_PRESENT [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1000] |           #
#           |                           {receivedTimestamp = TS2} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |            WRPTP SLAVE_PRESENT [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1000] |           #
#           |                           {receivedTimestamp = TS3} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |      <Check ((TS2 - TS1) + (TS3 - TS2))/2 = 1000ms> |           #
#           |                                                     |           #
#           |                                     <Disable WRPTP> | P1        #
#           |                                                     |           #
#           |                                      <Enable WRPTP> | P1        #
#           |                                                     |           #
#           | WRPTP ANNOUNCE [messageType = 0x0B,                 |           #
#           | domainNumber = DN1, grandmasterPriority1 = X - 1,   |           #
#           | tlvType = 0x0003, wrMessageId = 0x2000,             |           #
#           | wrConfig = 0x3, calibrated = 1, wrModeOn = 0]       |           #
#           | (sendCount = INFINITY)                              |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |            WRPTP SLAVE_PRESENT [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1000] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP LOCK [messageType = 0x0C,                     |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1001]             |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                   WRPTP LOCKED [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1002] |           #
#           |                           {receivedTimestamp = TS1} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                   WRPTP LOCKED [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1002] |           #
#           |                           {receivedTimestamp = TS2} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                   WRPTP LOCKED [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1002] |           #
#           |                           {receivedTimestamp = TS3} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |       <Check ((TS2 - TS1) + (TS3 - TS2))/2 = 300ms> |           #
#           |                                                     |           #
#           |                                     <Disable WRPTP> | P1        #
#           |                                                     |           #
#           |                                      <Enable WRPTP> | P1        #
#           |                                                     |           #
#           | WRPTP ANNOUNCE [messageType = 0x0B,                 |           #
#           | domainNumber = DN1, grandmasterPriority1 = X - 1,   |           #
#           | tlvType = 0x0003, wrMessageId = 0x2000,             |           #
#           | wrConfig = 0x3, calibrated = 1, wrModeOn = 0]       |           #
#           | (sendCount = INFINITY)                              |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |            WRPTP SLAVE_PRESENT [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1000] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP LOCK [messageType = 0x0C,                     |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1001]             |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                   WRPTP LOCKED [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1002] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP CALIBRATE [messageType = 0x0C,                |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1003,             |           #
#           | calSendPattern = FALSE, calRetry = 0,               |           #
#           | calPeriod = 3000us]                                 |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |      <Wait for 150ms to complete 0.5 x              |           #
#           |           WR_RESP_CALIB_REQ_TIMEOUT and check       |           #
#           |                   WRPTP portState = RESP_CALIB_REQ> | P1        #
#           |                                                     |           #
#           |      <Wait for 600ms to complete 2 x                |           #
#           |                      WR_RESP_CALIB_REQ_TIMEOUT>     |           #
#           |                                                     |           #
#           |            <Check WRPTP portState = RESP_CALIB_REQ> | P1        #
#           |                                                     |           #
#           |      <Wait for 630ms to complete 2 x                |           #
#           |             WR_RESP_CALIB_REQ_TIMEOUT +             |           #
#           |              10% of WR_RESP_CALIB_REQ_TIMEOUT>      |           #
#           |                                                     |           #
#           |                      <Check WRPTP portState = IDLE> | P1        #
#           |                                                     |           #
#           |                                     <Disable WRPTP> | P1        #
#           |                                                     |           #
#           |                                      <Enable WRPTP> | P1        #
#           |                                                     |           #
#           | WRPTP ANNOUNCE [messageType = 0x0B,                 |           #
#           | domainNumber = DN1, grandmasterPriority1 = X - 1,   |           #
#           | tlvType = 0x0003, wrMessageId = 0x2000,             |           #
#           | wrConfig = 0x3, calibrated = 1, wrModeOn = 0]       |           #
#           | (sendCount = INFINITY)                              |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |            WRPTP SLAVE_PRESENT [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1000] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP LOCK [messageType = 0x0C,                     |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1001]             |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                   WRPTP LOCKED [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1002] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP CALIBRATE [messageType = 0x0C,                |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1003,             |           #
#           | calSendPattern = FALSE, calRetry = 0,               |           #
#           | calPeriod = 3000us]                                 |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           | WRPTP CALIBRATED [messageType = 0x0C,               |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1004,             |           #
#           | deltaTx = 0, deltaRx = 0]                           |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                WRPTP CALIBRATE [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1003] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |               WRPTP CALIBRATED [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1004] |           #
#           |                           {receivedTimestamp = TS1} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |               WRPTP CALIBRATED [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1004] |           #
#           |                           {receivedTimestamp = TS2} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |               WRPTP CALIBRATED [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1004] |           #
#           |                           {receivedTimestamp = TS3} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |       <Check ((TS2 - TS1) + (TS3 - TS2))/2 = 300ms> |           #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     TEE       = Test Execution Engine                                       #
#     DUT       = Device Under Test                                           #
#     T1        = TEE's port 1                                                #
#     P1        = DUT's port 1                                                #
#     PTP       = Precision Time Protocol                                     #
#     WRPTP     = White Rabbit Precision Time Protocol                        #
#     OC        = Ordinary Clock                                              #
#     BC        = Boundary Clock                                              #
#     TS1 - TS3 = Timestamps 1 - 3                                            #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure Network Transport Protocol as IEEE 802.3/Ethernet.       #
#    vi.   Enable WRPTP on port P1.                                           #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Observe that DUT transmits WRPTP ANNOUNCE message on the port P1   #
#          with following parameters and store sourcePortIdentity as SPI1.    #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = 0                                   #
#                  grandmasterPriority1 = 64                                  #
#                  grandmasterPriority2 = 128                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                                                                             #
# Step 4 : Send WRPTP SLAVE_PRESENT message on port T1 with following         #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
#                                                                             #
# Step 5 : Observe that DUT transmits WRPTP LOCK message on port P1 with      #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
#                                                                             #
# Step 6 : Send WRPTP LOCKED message on port T1 with following parameters.    #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1002                              #
#                                                                             #
# Step 7 : Verify that DUT transmits WRPTP CALIBRATE message on port P1 with  #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                  calSendPattern       = FALSE                               #
#                  calRetry             = 0                                   #
#                  calPeriod            = 3000us                              #
#                                                                             #
# Step 8 : Observe that DUT transmits WRPTP CALIBRATED message on port P1 with#
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                  deltaTx              = Default value                       #
#                  deltaRx'             = knownDeltaRx + {0 - 16000ps}        #
#                                                                             #
# Step 9 : Send WRPTP CALIBRATE message on port T1 with following parameters. #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                  calSendPattern       = FALSE                               #
#                  calRetry             = 0                                   #
#                  calPeriod            = 3000us                              #
#                                                                             #
# Step 10: Send WRPTP CALIBRATED message on port T1 with following parameters #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                  deltaTx              = 0                                   #
#                  deltaRx              = 0                                   #
#                                                                             #
# Step 11 : Observe that DUT transmits WRPTP WR_MODE_ON message on the port P1#
#           with following parameters.                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1005                              #
#                                                                             #
# Step 12: Observe that DUT transmits SYNC message on port P1 with following  #
#          parameters and store the received timestamp as TS1.                #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x00                                #
#                  domainNumber         = 0                                   #
#                  logMessageInterval   = 0                                   #
#                                                                             #
# Step 13: Observe that DUT transmits SYNC message on port P1 with following  #
#          parameters and store the received timestamp as TS2.                #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x00                                #
#                  domainNumber         = 0                                   #
#                  logMessageInterval   = 0                                   #
#                                                                             #
# Step 14: Observe that DUT transmits SYNC message on port P1 with following  #
#          parameters and store the received timestamp as TS3.                #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x00                                #
#                  domainNumber         = 0                                   #
#                  logMessageInterval   = 0                                   #
#                                                                             #
# Step 15: Verify ((TS2 - TS1) + (TS3 - TS2))/2 = 1s.                         #
#                                                                             #
# Step 16: Disable WRPTP on port P1.                                          #
#                                                                             #
# Step 17: Enable WRPTP on port P1.                                           #
#                                                                             #
# Step 18: Wait for WRPTP to be enabled.                                      #
#                                                                             #
# Step 19: Verify that DUT transmits WRPTP ANNOUNCE message on the port P1    #
#          with following parameters and store sourcePortIdentity as SPI1.    #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = 0                                   #
#                  grandmasterPriority1 = 64                                  #
#                  grandmasterPriority2 = 128                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                                                                             #
# Step 20: Send WRPTP SLAVE_PRESENT message on port T1 with following         #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
#                                                                             #
# Step 21: Observe that DUT transmits WRPTP LOCK message on port P1 with      #
#          following parameters and store the received timestamp as TS1.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
#                                                                             #
# Step 22: Verify that DUT transmits WRPTP LOCK message on port P1 with       #
#          following parameters and store the received timestamp as TS2.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
#                                                                             #
# Step 23: Verify that DUT transmits WRPTP LOCK message on port P1 with       #
#          following parameters and store the received timestamp as TS3.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
#                                                                             #
# Step 24: Verify ((TS2 - TS1) + (TS3 - TS2))/2 = 15000ms (WR_M_LOCK_TIMEOUT).#
#                                                                             #
# Step 25: Disable WRPTP on port P1.                                          #
#                                                                             #
# Step 26: Enable WRPTP on port P1.                                           #
#                                                                             #
# Step 27: Wait for WRPTP to be enabled.                                      #
#                                                                             #
# Step 28: Verify that DUT transmits WRPTP ANNOUNCE message on the port P1    #
#          with following parameters and store sourcePortIdentity as SPI1.    #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = 0                                   #
#                  grandmasterPriority1 = 64                                  #
#                  grandmasterPriority2 = 128                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                                                                             #
# Step 29: Send WRPTP SLAVE_PRESENT message on port T1 with following         #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
#                                                                             #
# Step 30: Observe that DUT transmits WRPTP LOCK message on port P1 with      #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
#                                                                             #
# Step 31: Send WRPTP LOCKED message on port T1 with following parameters.    #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1002                              #
#                                                                             #
# Step 32: Observe that DUT transmits WRPTP CALIBRATE message on port P1 with #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                                                                             #
# Step 33: Observe that DUT transmits WRPTP CALIBRATED message on port P1 with#
#          following parameters and store the received timestamp as TS1.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                                                                             #
# Step 34: Verify that DUT transmits WRPTP CALIBRATED message on port P1 with #
#          following parameters and store the received timestamp as TS2.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                                                                             #
# Step 35: Verify that DUT transmits WRPTP CALIBRATED message on port P1 with #
#          following parameters and store the received timestamp as TS3.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                                                                             #
# Step 36: Verify ((TS2 - TS1) + (TS3 - TS2))/2 = 300ms                       #
#          (WR_CALIBRATED_TIMEOUT).                                           #
#                                                                             #
# Step 37: Disable WRPTP on port P1.                                          #
#                                                                             #
# Step 38: Enable WRPTP on port P1.                                           #
#                                                                             #
# Step 39: Wait for WRPTP to be enabled.                                      #
#                                                                             #
# Step 40: Verify that DUT transmits WRPTP ANNOUNCE message on the port P1    #
#          with following parameters and store sourcePortIdentity as SPI1.    #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = 0                                   #
#                  grandmasterPriority1 = 64                                  #
#                  grandmasterPriority2 = 128                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                                                                             #
# Step 41: Send WRPTP SLAVE_PRESENT message on port T1 with following         #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
#                                                                             #
# Step 42: Observe that DUT transmits WRPTP LOCK message on port P1 with      #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
#                                                                             #
# Step 43: Send WRPTP LOCKED message on port T1 with following parameters.    #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1002                              #
#                                                                             #
# Step 44: Observe that DUT transmits WRPTP CALIBRATE message on port P1 with #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                                                                             #
# Step 45: Observe that DUT transmits WRPTP CALIBRATED message on port P1 with#
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                                                                             #
# Step 46: Send WRPTP CALIBRATE message on port T1 with following parameters. #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                  calSendPattern       = FALSE                               #
#                  calRetry             = 0                                   #
#                  calPeriod            = 3000us                              #
#                                                                             #
# Step 47: Wait for 150ms (0.5 x WR_RESP_CALIB_REQ_TIMEOUT) and observe that  #
#          WRPTP portState of port P1 is in RESP_CALIB_REQ state.             #
#                                                                             #
# Step 48: Wait for 600ms to complete 2 x WR_RESP_CALIB_REQ_TIMEOUT.          #
#                                                                             #
# Step 49: Verify that WRPTP portState of port P1 is in RESP_CALIB_REQ state. #
#                                                                             #
# Step 50: Wait for 630ms to complete 2 x WR_RESP_CALIB_REQ_TIMEOUT + 10% of  #
#          WR_RESP_CALIB_REQ_TIMEOUT.                                         #
#                                                                             #
# Step 51: Verify that WRPTP portState of port P1 is in IDLE state.           #
#                                                                             #
# Step 52: Disable WRPTP on port P1.                                          #
#                                                                             #
# Step 53: Enable WRPTP on port P1.                                           #
#                                                                             #
# Step 54: Send periodic WRPTP ANNOUNCE messages on the port T1 with following#
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#                  grandmasterPriority1 = X - 1                               #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                  calibrated           = 1                                   #
#                  wrModeOn             = 0                                   #
#                                                                             #
# Step 55: Wait until completion of BMCA and observe that DUT transmits WRPTP #
#          SLAVE_PRESENT message on port P1 with following parameters. Store  #
#          the received timestamp as TS1.                                     #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
#                                                                             #
# Step 56: Verify that the DUT transmits WRPTP SLAVE_PRESENT message on port  #
#          P1 with following parameters and store the received timestamp as   #
#          TS2.                                                               #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
#                                                                             #
# Step 57: Verify that the DUT transmits WRPTP SLAVE_PRESENT message on port  #
#          P1 with following parameters and store the received timestamp as   #
#          TS3.                                                               #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
#                                                                             #
# Step 58: Verify ((TS2 - TS1) + (TS3 - TS2))/2 = 1000ms (WR_PRESENT_TIMEOUT).#
#                                                                             #
# Step 59: Disable WRPTP on port P1.                                          #
#                                                                             #
# Step 60: Enable WRPTP on port P1.                                           #
#                                                                             #
# Step 61: Send periodic WRPTP ANNOUNCE messages on the port T1 with following#
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#                  grandmasterPriority1 = X - 1                               #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                  calibrated           = 1                                   #
#                  wrModeOn             = 0                                   #
#                                                                             #
# Step 62: Wait until completion of BMCA and observe that DUT transmits WRPTP #
#          SLAVE_PRESENT message on port P1 with following parameters.        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
#                                                                             #
# Step 63: Send WRPTP LOCK message on port T1 with following parameters.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
#                                                                             #
# Step 64: Observe that the DUT transmits WRPTP LOCKED message on port P1 with#
#          following parameters and store the received timestamp as TS1.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1002                              #
#                                                                             #
# Step 65: Verify that the DUT transmits WRPTP LOCKED message on port P1 with #
#          following parameters and store the received timestamp as TS2.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1002                              #
#                                                                             #
# Step 66: Verify that the DUT transmits WRPTP LOCKED message on port P1 with #
#          following parameters and store the received timestamp as TS3.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1002                              #
#                                                                             #
# Step 67: Verify ((TS2 - TS1) + (TS3 - TS2))/2 = 300ms (WR_LOCKED_TIMEOUT).  #
#                                                                             #
# Step 68: Disable WRPTP on port P1.                                          #
#                                                                             #
# Step 69: Enable WRPTP on port P1.                                           #
#                                                                             #
# Step 70: Send periodic WRPTP ANNOUNCE messages on the port T1 with following#
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#                  grandmasterPriority1 = X - 1                               #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                  calibrated           = 1                                   #
#                  wrModeOn             = 0                                   #
#                                                                             #
# Step 71: Wait until completion of BMCA and observe that DUT transmits WRPTP #
#          SLAVE_PRESENT message on port P1 with following parameters.        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
#                                                                             #
# Step 72: Send WRPTP LOCK message on port T1 with following parameters.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
#                                                                             #
# Step 73: Observe that the DUT transmits WRPTP LOCKED message on port P1 with#
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1002                              #
#                                                                             #
# Step 74: Send WRPTP CALIBRATE message on port T1 with following parameters. #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                  calSendPattern       = FALSE                               #
#                  calRetry             = 0                                   #
#                  calPeriod            = 3000us                              #
#                                                                             #
# Step 75: Wait for 150ms (0.5 x WR_RESP_CALIB_REQ_TIMEOUT) and observe that  #
#          WRPTP portState of port P1 is in RESP_CALIB_REQ state.             #
#                                                                             #
# Step 76: Wait for 600ms to complete 2 x WR_RESP_CALIB_REQ_TIMEOUT.          #
#                                                                             #
# Step 77: Verify that WRPTP portState of port P1 is in RESP_CALIB_REQ state. #
#                                                                             #
# Step 78: Wait for 630ms to complete 2 x WR_RESP_CALIB_REQ_TIMEOUT + 10% of  #
#          WR_RESP_CALIB_REQ_TIMEOUT.                                         #
#                                                                             #
# Step 79: Verify that WRPTP portState of port P1 is in IDLE state.           #
#                                                                             #
# Step 80: Disable WRPTP on port P1.                                          #
#                                                                             #
# Step 81: Enable WRPTP on port P1.                                           #
#                                                                             #
# Step 82: Send periodic WRPTP ANNOUNCE messages on the port T1 with following#
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#                  grandmasterPriority1 = X - 1                               #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                  calibrated           = 1                                   #
#                  wrModeOn             = 0                                   #
#                                                                             #
# Step 83: Wait until completion of BMCA and observe that DUT transmits WRPTP #
#          SLAVE_PRESENT message on port P1 with following parameters.        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
#                                                                             #
# Step 84: Send WRPTP LOCK message on port T1 with following parameters.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
#                                                                             #
# Step 85: Observe that the DUT transmits WRPTP LOCKED message on port P1 with#
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1002                              #
#                                                                             #
# Step 86: Send WRPTP CALIBRATE message on port T1 with following parameters. #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                  calSendPattern       = FALSE                               #
#                  calRetry             = 0                                   #
#                  calPeriod            = 3000us                              #
#                                                                             #
# Step 87: Send WRPTP CALIBRATED message on port T1 with following parameters.#
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                  deltaTx              = 0                                   #
#                  deltaRx              = 0                                   #
#                                                                             #
# Step 88: Observe that the DUT transmits WRPTP CALIBRATE message on port P1  #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                                                                             #
# Step 89: Observe that the DUT transmits WRPTP CALIBRATED message on port P1 #
#          with following parameters and store the received timestamp as TS1. #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                                                                             #
# Step 90: Verify that the DUT transmits WRPTP CALIBRATED message on port P1  #
#          with following parameters and store the received timestamp as TS2. #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                                                                             #
# Step 91: Verify that the DUT transmits WRPTP CALIBRATED message on port P1  #
#          with following parameters and store the received timestamp as TS3. #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                                                                             #
# Step 92: Verify ((TS2 - TS1) + (TS3 - TS2))/2 = 300ms                       #
#          (WR_CALIBRATED_TIMEOUT).                                           #
#                                                                             #
# Note :                                                                      #
#                                                                             #
#   Values mentioned in above steps are examples based on default values.     #
#   However, the test will be executed using the values given in ATTEST GUI   #
#   (Selected configuration in ATTEST Configuration Manager > Protocol        #
#   Options > WRPTP > WRPTP Attributes). Hence, the values displayed in test  #
#   logs may differ from those mentioned in the steps.                        #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          Jul/2018      CERN          Initial                            #
# 1.1          Aug/2018      CERN          Changed default transmission       #
#                                          interval of Sync messages according#
#                                          to portDS.logSyncInterval to 0     #
# 1.2          Sep/2018      CERN          Reset capture to avoid processing  #
#                                          buffered messages                  #
# 1.3          Sep/2018      CERN          Changed default value of           #
#                                          defaultDS.priority1 = 64           #
# 1.4          Jan/2019      CERN          Added verification for timers in   #
#                                          each state of WRPTP state machine. #
# 1.5          Jan/2019      CERN          a) Changed default value of        #
#                                             RESP_CALIB_REQ_TIMEOUT = 300ms. #
#                                          b) Resolved non-transmission of    #
#                                             messages at background while    #
#                                             waiting for state transitions.  #
#                                          c) Added delay before checking     #
#                                             RESP_CALIB_REQ state.           #
# 1.6          Apr/2019      JC.BAU/CERN   Improve execution time             #
#                                                                             #
###############################################################################
# Copyright (c) 2018 - 2019 CERN                                              #
###############################################################################

global dut_log_msg
global tee_log_msg
global tee_rx_errlog
global session_id
global step

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def    "To verify that a WRPTP enabled device stores all attributes with\
			   default initialization values. Checking that the following\
			   attributes have correct default values.\
			   1) defaultDS.domainNumber = 0\
			   2) defaultDS.priority1 = 64\
			   3) defaultDS.priority2 = 128\
			   4) portDS.logSyncInterval = 0\
			   5) portDS.knownDeltaTx = Default value\
			   6) portDS.knownDeltaRx = Default value\
			   7) portDS.wrConfig = WR_M_AND_S\
			   8) portDS.calPeriod = 3000us\
			   9) portDS.calRetry = 0\
			   10) WR_PRESENT_TIMEOUT = 1000ms\
			   11) WR_M_LOCK_TIMEOUT = 15000ms\
			   12) WR_LOCKED_TIMEOUT = 300ms\
			   13) WR_RESP_CALIB_REQ_TIMEOUT = 300ms\
			   14) WR_CALIBRATED_TIMEOUT = 300ms\
			   15) WR_STATE_RETRY = 3"

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set dut_port_1                     $::dut_port_num_1
set vlan_id                        $::wrptp_vlan_id
set dut_ip                         $::dut_test_port_ip
set dut_dest_ip                    $::dut_test_port_ip
set tee_dest_ip                    $::tee_test_port_ip
set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT

set domain                         $::DEFAULT_DOMAIN
set wrptp_tlv                      $::ORG_EXT_TLV

set ann_msg_id                     $::wrMessageID(ANN_SUFIX)
set slave_present_msg_id           $::wrMessageID(SLAVE_PRESENT)
set lock_msg_id                    $::wrMessageID(LOCK)
set locked_msg_id                  $::wrMessageID(LOCKED)
set calibrate_msg_id               $::wrMessageID(CALIBRATE)
set calibrated_msg_id              $::wrMessageID(CALIBRATED)
set wr_mode_on_msg_id              $::wrMessageID(WR_MODE_ON)

set gm_priority1                   $::wrptp_priority1
set gm_priority2                   $::wrptp_priority2
set wrconfig                       [set ::WRPTP_WRCONFIG($::wrptp_wrconfig)]

set infinity                       $::INFINITY
set resp_calib_req                 $::WRPTP_PORT_STATE(RESP_CALIB_REQ)
set idle                           $::WRPTP_PORT_STATE(IDLE)
set clock_step                     $::ptp_dut_clock_step
set announce_timeout               [expr $::wrptp_announce_timeout + 60]
set bmca_time                      $::wrptp_announce_timeout
set signal_timeout                 $::wrptp_signal_timeout
set delta_tx                       $::wrptp_known_delta_tx
set delta_rx                       $::wrptp_known_delta_rx

set cal_send_pattern               0
set cal_retry                      $::wrptp_cal_retry
set cal_period                     $::wrptp_cal_period
set log_message_interval           0

set calibrated                     1
set step                           0

########################### END - INITIALIZATION ##############################

wrptp::display_interface_details

wrptp::dut_Initialization

proc cleanup {} {
	global step
	if { $step != 0 } {
		wrptp::dut_cleanup_setup_003
		wrptp::dut_commit_changes
	}
}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

### (Initial Part) ###

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure Network Transport Protocol as IEEE 802.3/Ethernet.       #
#    vi.   Enable WRPTP on port P1.                                           #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP3)"

if {!([wrptp::dut_configure_setup_003] && [wrptp::dut_commit_changes])} {

	LOG -msg "$dut_log_msg(INIT_SETUP3_F)"

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP3_F)"\
		-tc_def  $tc_def
	return 0
}

###############################################################################
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(INIT)"

wrptp::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

	if {![WRPTP_UNICAST_PRE_REQUISITE_CONFIGURATION\
						-port_num       $tee_port_1\
						-tee_mac        $tee_mac\
						-tee_ip         $tee_ip\
						-dut_ip         $dut_ip\
						-vlan_id        $vlan_id\
						-dut_mac        tee_dest_mac\
						-timeout        $arp_timeout]} {

		TEE_CLEANUP

		TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
			-tc_def  $tc_def

		return 0
	}

} else {
	set tee_dest_ip             $::PTP_E2E_IP
	set tee_dest_mac            $::PTP_E2E_MAC
}

if {![pltLib::create_filter_wrptp_pkt\
			  -port_num                $tee_port_1\
			  -ether_type              $ptp_ethtype\
			  -vlan_id                 $vlan_id\
			  -ids                     id1]} {

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
		-tc_def  $tc_def

	return 0
}

set filter_id [lindex $id1 end]

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 3 : Observe that DUT transmits WRPTP ANNOUNCE message on the port P1   #
#          with following parameters and store sourcePortIdentity as SPI1.    #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = 0                                   #
#                  grandmasterPriority1 = 64                                  #
#                  grandmasterPriority2 = 128                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                                                                             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![wrptp::recv_announce \
		   -port_num                  $tee_port_1\
		   -domain_number             $domain\
		   -gm_priority1              $gm_priority1\
		   -gm_priority2              $gm_priority2\
		   -tlv_type                  $wrptp_tlv\
		   -message_id                $ann_msg_id\
		   -wrconfig                  $wrconfig\
		   -recvd_src_port_number     src_port_number\
		   -recvd_src_clock_identity  src_clock_identity\
		   -error_reason              error_reason\
		   -timeout                   $announce_timeout\
		   -filter_id                 $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANNOUNCE_RX_P1_F) $error_reason"\
		-tc_def $tc_def

	return 0
}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 4 : Send WRPTP SLAVE_PRESENT message on port T1 with following         #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
#                                                                             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(SLAVE_PRESENT_TX_P1)"

if {![wrptp::send_signal \
		   -port_num                $tee_port_1\
		   -src_mac                 $tee_mac\
		   -dest_mac                $tee_dest_mac\
		   -src_ip                  $tee_ip\
		   -dest_ip                 $tee_dest_ip\
		   -domain_number           $domain\
		   -target_port_number      $src_port_number\
		   -target_clock_identity   $src_clock_identity\
		   -tlv_type                $wrptp_tlv\
		   -message_id              $slave_present_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SLAVE_PRESENT_TX_P1_F)"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 5 : Observe that DUT transmits WRPTP LOCK message on port P1 with      #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
#                                                                             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCK_RX_P1_O)"

if {![wrptp::recv_signal \
		   -port_num                $tee_port_1\
		   -domain_number           $domain\
		   -reset_count             $::reset_count_off\
		   -tlv_type                $wrptp_tlv\
		   -message_id              $lock_msg_id\
		   -timeout                 $::wrptp_m_lock_timeout_recv\
		   -error_reason            error_reason\
		   -filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(LOCK_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 6 : Send WRPTP LOCKED message on port T1 with following parameters.    #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1002                              #
#                                                                             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCKED_TX_P1)"

if {![wrptp::send_signal \
		   -port_num                $tee_port_1\
		   -src_mac                 $tee_mac\
		   -dest_mac                $tee_dest_mac\
		   -src_ip                  $tee_ip\
		   -dest_ip                 $tee_dest_ip\
		   -domain_number           $domain\
		   -target_port_number      $src_port_number\
		   -target_clock_identity   $src_clock_identity\
		   -tlv_type                $wrptp_tlv\
		   -message_id              $locked_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(LOCKED_TX_P1_F)"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 7 : Verify that DUT transmits WRPTP CALIBRATE message on port P1 with  #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                  calSendPattern       = FALSE                               #
#                  calRetry             = 0                                   #
#                  calPeriod            = 3000us                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATE_RX_P1_V)"

if {![wrptp::recv_signal \
		   -port_num                $tee_port_1\
		   -domain_number           $domain\
		   -tlv_type                $wrptp_tlv\
		   -message_id              $calibrate_msg_id\
		   -cal_send_pattern        $cal_send_pattern\
		   -cal_retry               $cal_retry\
		   -cal_period              $cal_period\
		   -timeout                 $::wrptp_calibration_timeout_recv\
		   -error_reason            error_reason\
		   -filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL  -reason "$tee_log_msg(CALIBRATE_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 8 : Observe that DUT transmits WRPTP CALIBRATED message on port P1 with#
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                  deltaTx              = Default value                       #
#                  deltaRx'             = knownDeltaRx + {0 - 16000ps}        #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATED_RX_P1_O)"

if {![wrptp::recv_signal \
		   -port_num                $tee_port_1\
		   -domain_number           $domain\
		   -tlv_type                $wrptp_tlv\
		   -message_id              $calibrated_msg_id\
		   -delta_tx                $delta_tx\
		   -recvd_delta_rx          recvd_delta_rx\
		   -timeout                 $::wrptp_calibrated_timeout_recv\
		   -error_reason            error_reason\
		   -filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(CALIBRATED_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

if {!($recvd_delta_rx >= $delta_rx && $recvd_delta_rx  <= [expr $delta_rx + 16000])} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(CALIBRATED_RX_P1_F) (Reason: deltaRx is not matching)"\
		-tc_def  $tc_def

	return 0

}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 9 : Send WRPTP CALIBRATE message on port T1 with following parameters. #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                  calSendPattern       = FALSE                               #
#                  calRetry             = 0                                   #
#                  calPeriod            = 3000us                              #
#                                                                             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATE_TX_P1)"

if {![wrptp::send_signal \
		   -port_num                $tee_port_1\
		   -src_mac                 $tee_mac\
		   -dest_mac                $tee_dest_mac\
		   -src_ip                  $tee_ip\
		   -dest_ip                 $tee_dest_ip\
		   -domain_number           $domain\
		   -target_port_number      $src_port_number\
		   -target_clock_identity   $src_clock_identity\
		   -tlv_type                $wrptp_tlv\
		   -message_id              $calibrate_msg_id\
		   -cal_send_pattern        $cal_send_pattern\
		   -cal_retry               $cal_retry\
		   -cal_period              $cal_period]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(CALIBRATE_TX_P1_F)"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 10: Send WRPTP CALIBRATED message on port T1 with following parameters #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                  deltaTx              = 0                                   #
#                  deltaRx              = 0                                   #
#                                                                             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATED_TX_P1)"

set delta_tx    0
set delta_rx    0

if {![wrptp::send_signal \
		   -port_num                $tee_port_1\
		   -src_mac                 $tee_mac\
		   -dest_mac                $tee_dest_mac\
		   -src_ip                  $tee_ip\
		   -dest_ip                 $tee_dest_ip\
		   -domain_number           $domain\
		   -target_port_number      $src_port_number\
		   -target_clock_identity   $src_clock_identity\
		   -tlv_type                $wrptp_tlv\
		   -delta_tx                $delta_tx\
		   -delta_rx                $delta_rx\
		   -message_id              $calibrated_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(CALIBRATED_TX_P1_F)"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 11 : Observe that DUT transmits WRPTP WR_MODE_ON message on the port P1#
#           with following parameters.                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1005                              #
#                                                                             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(WR_MODE_ON_RX_P1_O)"

if {![wrptp::recv_signal \
		   -port_num                $tee_port_1\
		   -domain_number           $domain\
		   -tlv_type                $wrptp_tlv\
		   -message_id              $wr_mode_on_msg_id\
		   -timeout                 $::wrptp_wr_link_on_timeout_recv\
		   -error_reason            error_reason\
		   -filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(WR_MODE_ON_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 12: Observe that DUT transmits SYNC message on port P1 with following  #
#          parameters and store the received timestamp as TS1.                #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x00                                #
#                  domainNumber         = 0                                   #
#                  logMessageInterval   = 0                                   #
#                                                                             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(SYNC_RX_P1_O)"

if {![wrptp::recv_sync \
		   -port_num                $tee_port_1\
		   -domain_number           $domain\
		   -log_message_interval    $log_message_interval\
		   -timeout                 $::ptp_sync_timeout\
		   -reset_count             $::reset_count_off\
		   -rx_timestamp_ns         TS1\
		   -error_reason            error_reason\
		   -filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 13: Observe that DUT transmits SYNC message on port P1 with following  #
#          parameters and store the received timestamp as TS2.                #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x00                                #
#                  domainNumber         = 0                                   #
#                  logMessageInterval   = 0                                   #
#                                                                             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(SYNC_RX_P1_O)"

if {![wrptp::recv_sync \
		   -port_num                $tee_port_1\
		   -domain_number           $domain\
		   -log_message_interval    $log_message_interval\
		   -timeout                 $::ptp_sync_timeout\
		   -reset_count             $::reset_count_off\
		   -rx_timestamp_ns         TS2\
		   -error_reason            error_reason\
		   -filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_RX_2_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 14: Observe that DUT transmits SYNC message on port P1 with following  #
#          parameters and store the received timestamp as TS3.                #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x00                                #
#                  domainNumber         = 0                                   #
#                  logMessageInterval   = 0                                   #
#                                                                             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(SYNC_RX_P1_O)"

if {![wrptp::recv_sync \
		   -port_num                $tee_port_1\
		   -domain_number           $domain\
		   -log_message_interval    $log_message_interval\
		   -timeout                 $::ptp_sync_timeout\
		   -reset_count             $::reset_count_off\
		   -rx_timestamp_ns         TS3\
		   -error_reason            error_reason\
		   -filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_RX_2_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 15: Verify ((TS2 - TS1) + (TS3 - TS2))/2 = 1s.                         #
###############################################################################

STEP [incr step]

set b [ expr (((($TS2-$TS1) + ($TS3-$TS2))/2)/1000000000.0) ]
set b [format %.1f $b]

set min_intvl [expr $::wrptp_sync_interval - ($::wrptp_sync_interval * 0.3)]
set max_intvl [expr $::wrptp_sync_interval + ($::wrptp_sync_interval * 0.3)]

LOG -level 0 -msg "$tee_log_msg(CHECK_INTVL_EXPR_V)"
LOG -level 0 -msg "\nSYNC message transmission interval:"
LOG -level 0 -msg "               Expected = $::wrptp_sync_interval s (Allowable range: $min_intvl s - $max_intvl s)"
LOG -level 0 -msg "               Observed = $b s"

if {!($b >= $min_intvl && $b <= $max_intvl)} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL   -reason "DUT does not send SYNC message in the correct\
								 interval"\
		-tc_def  $tc_def
	return 0
}

###############################################################################
# Step 16: Disable WRPTP on port P1.                                          #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(DISABLE_WRPTP_PORT_P1)"

if {![wrptp::dut_port_wrptp_disable\
			-port_num          $dut_port_1]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(DISABLE_WRPTP_PORT_P1_F)"\
		-tc_def  $tc_def

	reason 0

}

###############################################################################
# Step 17: Enable WRPTP on port P1.                                           #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(ENABLE_WRPTP_PORT_P1)"

if {!( [wrptp::dut_port_wrptp_enable -port_num  $dut_port_1] &&
	[wrptp::dut_commit_changes]) } {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(ENABLE_WRPTP_PORT_P1_F)"\
		-tc_def  $tc_def

	reason 0

}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 18: Wait for WRPTP to be enabled.                                      #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(WAIT_WRPTP_ENABLE)"
LOG -level 0 -msg "Waiting for $::wrptp_enable_time secs"

wrptp::wait_msec [expr $::wrptp_enable_time * 1000]

###############################################################################
# Step 19: Verify that DUT transmits WRPTP ANNOUNCE message on the port P1    #
#          with following parameters and store sourcePortIdentity as SPI1.    #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = 0                                   #
#                  grandmasterPriority1 = 64                                  #
#                  grandmasterPriority2 = 128                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                                                                             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_V)"

if {![wrptp::recv_announce \
-port_num                  $tee_port_1\
-domain_number             $domain\
-gm_priority1              $gm_priority1\
-gm_priority2              $gm_priority2\
-tlv_type                  $wrptp_tlv\
-message_id                $ann_msg_id\
-wrconfig                  $wrconfig\
-recvd_src_port_number     src_port_number\
-recvd_src_clock_identity  src_clock_identity\
-error_reason              error_reason\
-timeout                   $announce_timeout\
-filter_id                 $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL  -reason "$tee_log_msg(ANNOUNCE_RX_P1_F) $error_reason"\
		-tc_def $tc_def

	return 0
}

###############################################################################
# Step 20: Send WRPTP SLAVE_PRESENT message on port T1 with following         #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
#                                                                             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(SLAVE_PRESENT_TX_P1)"

if {![wrptp::send_signal \
-port_num                $tee_port_1\
-src_mac                 $tee_mac\
-dest_mac                $tee_dest_mac\
-src_ip                  $tee_ip\
-dest_ip                 $tee_dest_ip\
-domain_number           $domain\
-target_port_number      $src_port_number\
-target_clock_identity   $src_clock_identity\
-tlv_type                $wrptp_tlv\
-message_id              $slave_present_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SLAVE_PRESENT_TX_P1_F)"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 21: Observe that DUT transmits WRPTP LOCK message on port P1 with      #
#          following parameters and store the received timestamp as TS1.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
#                                                                             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCK_RX_P1_O)"

if {![wrptp::recv_signal \
-port_num                $tee_port_1\
-domain_number           $domain\
-tlv_type                $wrptp_tlv\
-message_id              $lock_msg_id\
-reset_count             $::reset_count_off\
-rx_timestamp_ns         TS1\
-error_reason            error_reason\
-timeout                 $::wrptp_m_lock_timeout_recv\
-filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(LOCK_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 22: Verify that DUT transmits WRPTP LOCK message on port P1 with       #
#          following parameters and store the received timestamp as TS2.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
#                                                                             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCK_RX_P1_O)"

if {![wrptp::recv_signal \
-port_num                $tee_port_1\
-domain_number           $domain\
-tlv_type                $wrptp_tlv\
-message_id              $lock_msg_id\
-reset_count             $::reset_count_off\
-rx_timestamp_ns         TS2\
-error_reason            error_reason\
-timeout                 $::wrptp_m_lock_timeout_recv\
-filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL  -reason "$tee_log_msg(LOCK_RX_2_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 23: Verify that DUT transmits WRPTP LOCK message on port P1 with       #
#          following parameters and store the received timestamp as TS3.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
#                                                                             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCK_RX_P1_O)"

if {![wrptp::recv_signal \
-port_num                $tee_port_1\
-domain_number           $domain\
-tlv_type                $wrptp_tlv\
-message_id              $lock_msg_id\
-reset_count             $::reset_count_off\
-rx_timestamp_ns         TS3\
-error_reason            error_reason\
-timeout                 $::wrptp_m_lock_timeout_recv\
-filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL  -reason "$tee_log_msg(LOCK_RX_2_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 24: Verify ((TS2 - TS1) + (TS3 - TS2))/2 = 300ms.                      #
###############################################################################

STEP [incr step]

set b [ expr (((($TS2-$TS1) + ($TS3-$TS2))/2)/1000000.0) ]
set b [format %.1f $b]

set min_intvl [expr $::wrptp_m_lock_timeout - ($::wrptp_m_lock_timeout * 0.3)]
set max_intvl [expr $::wrptp_m_lock_timeout + ($::wrptp_m_lock_timeout * 0.3)]

LOG -level 0 -msg "$tee_log_msg(CHECK_INTVL_EXPR_V)"
LOG -level 0 -msg "\nLOCK message re-transmission interval:"
LOG -level 0 -msg "               Expected = $::wrptp_m_lock_timeout ms (Allowable range: $min_intvl ms - $max_intvl ms)"
LOG -level 0 -msg "               Observed = $b ms"

if {!($b >= $min_intvl && $b <= $max_intvl)} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL   -reason "DUT does not transmit LOCK message in the\
correct interval."\
		-tc_def $tc_def
	return 0
}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 25: Disable WRPTP on port P1.                                          #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(DISABLE_WRPTP_PORT_P1)"

if {![wrptp::dut_port_wrptp_disable\
-port_num          $dut_port_1]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(DISABLE_WRPTP_PORT_P1_F)"\
		-tc_def  $tc_def

	reason 0

}

###############################################################################
# Step 26: Enable WRPTP on port P1.                                           #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(ENABLE_WRPTP_PORT_P1)"

if {!( [wrptp::dut_port_wrptp_enable -port_num  $dut_port_1] &&
	[wrptp::dut_commit_changes]) } {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(ENABLE_WRPTP_PORT_P1_F)"\
		-tc_def  $tc_def

	reason 0

}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 27: Wait for WRPTP to be enabled.                                      #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(WAIT_WRPTP_ENABLE)"
LOG -level 0 -msg "Waiting for $::wrptp_enable_time secs"

wrptp::wait_msec [expr $::wrptp_enable_time * 1000]

###############################################################################
# Step 28: Verify that DUT transmits WRPTP ANNOUNCE message on the port P1    #
#          with following parameters and store sourcePortIdentity as SPI1.    #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = 0                                   #
#                  grandmasterPriority1 = 64                                  #
#                  grandmasterPriority2 = 128                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                                                                             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_V)"

if {![wrptp::recv_announce \
-port_num                  $tee_port_1\
-domain_number             $domain\
-gm_priority1              $gm_priority1\
-gm_priority2              $gm_priority2\
-tlv_type                  $wrptp_tlv\
-message_id                $ann_msg_id\
-wrconfig                  $wrconfig\
-recvd_src_port_number     src_port_number\
-recvd_src_clock_identity  src_clock_identity\
-error_reason              error_reason\
-timeout                   $announce_timeout\
-filter_id                 $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL  -reason "$tee_log_msg(ANNOUNCE_RX_P1_F) $error_reason"\
		-tc_def $tc_def

	return 0
}

###############################################################################
# Step 29: Send WRPTP SLAVE_PRESENT message on port T1 with following         #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
#                                                                             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(SLAVE_PRESENT_TX_P1)"

if {![wrptp::send_signal \
-port_num                $tee_port_1\
-src_mac                 $tee_mac\
-dest_mac                $tee_dest_mac\
-src_ip                  $tee_ip\
-dest_ip                 $tee_dest_ip\
-domain_number           $domain\
-target_port_number      $src_port_number\
-target_clock_identity   $src_clock_identity\
-tlv_type                $wrptp_tlv\
-message_id              $slave_present_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SLAVE_PRESENT_TX_P1_F)"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 30: Observe that DUT transmits WRPTP LOCK message on port P1 with      #
#          following parameters and store the received timestamp as TS1.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
#                                                                             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCK_RX_P1_O)"

if {![wrptp::recv_signal \
-port_num                $tee_port_1\
-domain_number           $domain\
-tlv_type                $wrptp_tlv\
-message_id              $lock_msg_id\
-reset_count             $::reset_count_off\
-rx_timestamp_ns         TS1\
-error_reason            error_reason\
-timeout                 $::wrptp_m_lock_timeout_recv\
-filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(LOCK_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 31: Send WRPTP LOCKED message on port T1 with following parameters.    #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1002                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCKED_TX_P1)"

if {![wrptp::send_signal\
-port_num               $tee_port_1\
-dest_mac               $tee_dest_mac\
-src_mac                $tee_mac\
-dest_ip                $tee_dest_ip\
-src_ip                 $tee_ip\
-domain_number          $domain\
-target_port_number     $src_port_number\
-target_clock_identity  $src_clock_identity\
-tlv_type               $wrptp_tlv\
-message_id             $locked_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(LOCKED_TX_P1_F)"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 32: Observe that DUT transmits WRPTP CALIBRATE message on port P1 with #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATE_RX_P1_O)"

if {![wrptp::recv_signal\
-port_num           $tee_port_1\
-filter_id          $filter_id\
-domain_number      $domain\
-timeout            $::wrptp_calibration_timeout_recv\
-tlv_type           $wrptp_tlv\
-error_reason       error_reason\
-message_id         $calibrate_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(CALIBRATE_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 33: Observe that DUT transmits WRPTP CALIBRATED message on port P1 with#
#          following parameters and store the received timestamp as TS1.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATED_RX_P1_O)"

if {![wrptp::recv_signal\
-port_num            $tee_port_1\
-filter_id           $filter_id\
-domain_number       $domain\
-timeout             $::wrptp_calibrated_timeout_recv\
-tlv_type            $wrptp_tlv\
-message_id          $calibrated_msg_id\
-error_reason        error_reason\
-reset_count         $::reset_count_off\
-rx_timestamp_ns     TS1]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(CALIBRATED_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 34: Verify that DUT transmits WRPTP CALIBRATED message on port P1 with #
#          following parameters and store the received timestamp as TS2.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATED_RX_P1_O)"

if {![wrptp::recv_signal\
-port_num            $tee_port_1\
-filter_id           $filter_id\
-domain_number       $domain\
-timeout             $::wrptp_calibrated_timeout_recv\
-tlv_type            $wrptp_tlv\
-error_reason        error_reason\
-message_id          $calibrated_msg_id\
-reset_count         $::reset_count_off\
-rx_timestamp_ns     TS2]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL  -reason "$tee_log_msg(CALIBRATED_RX_2_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 35: Verify that DUT transmits WRPTP CALIBRATED message on port P1 with #
#          following parameters and store the received timestamp as TS3.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATED_RX_P1_O)"

if {![wrptp::recv_signal\
-port_num            $tee_port_1\
-filter_id           $filter_id\
-domain_number       $domain\
-timeout             $::wrptp_calibrated_timeout_recv\
-tlv_type            $wrptp_tlv\
-error_reason        error_reason\
-message_id          $calibrated_msg_id\
-reset_count         $::reset_count_off\
-rx_timestamp_ns     TS3]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL  -reason "$tee_log_msg(CALIBRATED_RX_2_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 36: Verify ((TS2 - TS1) + (TS3 - TS2))/2 = 300ms                       #
#          (WR_CALIBRATED_TIMEOUT).                                           #
###############################################################################

STEP [incr step]

set b [ expr (((($TS2-$TS1) + ($TS3-$TS2))/2)/1000000.0) ]
set b [format %.1f $b]
set min_intvl [expr $::wrptp_calibrated_timeout - ($::wrptp_calibrated_timeout * 0.3)]
set max_intvl [expr $::wrptp_calibrated_timeout + ($::wrptp_calibrated_timeout * 0.3)]

LOG -level 0 -msg "$tee_log_msg(CHECK_INTVL_EXPR_V)"
LOG -level 0 -msg "\nCALIBRATED message re-transmission Interval:"
LOG -level 0 -msg "               Expected = $::wrptp_calibrated_timeout ms (Acceptable range: $min_intvl ms - $max_intvl ms)"
LOG -level 0 -msg "               Observed = $b ms"

if {!($b >= $min_intvl && $b <= $max_intvl)} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL   -reason "DUT does not send CALIBRATED message and re-enter\
to it's port WR state CALIBRATED on expiry of\
CALIBRATED_TIMEOUT"\
		-tc_def  $tc_def
	return 0
}

###############################################################################
# Step 37: Disable WRPTP on port P1.                                          #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(DISABLE_WRPTP_PORT_P1)"

if {![wrptp::dut_port_wrptp_disable\
-port_num          $dut_port_1]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(DISABLE_WRPTP_PORT_P1_F)"\
		-tc_def  $tc_def

	reason 0

}

###############################################################################
# Step 38: Enable WRPTP on port P1.                                           #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(ENABLE_WRPTP_PORT_P1)"

if {!( [wrptp::dut_port_wrptp_enable -port_num  $dut_port_1] &&
	[wrptp::dut_commit_changes]) } {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(ENABLE_WRPTP_PORT_P1_F)"\
		-tc_def  $tc_def

	reason 0

}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 39: Wait for WRPTP to be enabled.                                      #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(WAIT_WRPTP_ENABLE)"
LOG -level 0 -msg "Waiting for $::wrptp_enable_time secs"

wrptp::wait_msec [expr $::wrptp_enable_time * 1000]

###############################################################################
# Step 40: Verify that DUT transmits WRPTP ANNOUNCE message on the port P1    #
#          with following parameters and store sourcePortIdentity as SPI1.    #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = 0                                   #
#                  grandmasterPriority1 = 64                                  #
#                  grandmasterPriority2 = 128                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                                                                             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_V)"

if {![wrptp::recv_announce \
-port_num                  $tee_port_1\
-domain_number             $domain\
-gm_priority1              $gm_priority1\
-gm_priority2              $gm_priority2\
-tlv_type                  $wrptp_tlv\
-message_id                $ann_msg_id\
-wrconfig                  $wrconfig\
-recvd_src_port_number     src_port_number\
-recvd_src_clock_identity  src_clock_identity\
-error_reason              error_reason\
-timeout                   $announce_timeout\
-filter_id                 $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL  -reason "$tee_log_msg(ANNOUNCE_RX_P1_F) $error_reason"\
		-tc_def $tc_def

	return 0
}

###############################################################################
# Step 41: Send WRPTP SLAVE_PRESENT message on port T1 with following         #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
#                                                                             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(SLAVE_PRESENT_TX_P1)"

if {![wrptp::send_signal \
-port_num                $tee_port_1\
-src_mac                 $tee_mac\
-dest_mac                $tee_dest_mac\
-src_ip                  $tee_ip\
-dest_ip                 $tee_dest_ip\
-domain_number           $domain\
-target_port_number      $src_port_number\
-target_clock_identity   $src_clock_identity\
-tlv_type                $wrptp_tlv\
-message_id              $slave_present_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SLAVE_PRESENT_TX_P1_F)"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 42: Observe that DUT transmits WRPTP LOCK message on port P1 with      #
#          following parameters and store the received timestamp as TS1.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
#                                                                             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCK_RX_P1_O)"

if {![wrptp::recv_signal \
-port_num                $tee_port_1\
-domain_number           $domain\
-tlv_type                $wrptp_tlv\
-message_id              $lock_msg_id\
-reset_count             $::reset_count_off\
-rx_timestamp_ns         TS1\
-error_reason            error_reason\
-timeout                 $::wrptp_m_lock_timeout_recv\
-filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(LOCK_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 43: Send WRPTP LOCKED message on port T1 with following parameters.    #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1002                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCKED_TX_P1)"

if {![wrptp::send_signal\
-port_num               $tee_port_1\
-dest_mac               $tee_dest_mac\
-src_mac                $tee_mac\
-dest_ip                $tee_dest_ip\
-src_ip                 $tee_ip\
-domain_number          $domain\
-target_port_number     $src_port_number\
-target_clock_identity  $src_clock_identity\
-tlv_type               $wrptp_tlv\
-message_id             $locked_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(LOCKED_TX_P1_F)"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 44: Observe that DUT transmits WRPTP CALIBRATE message on port P1 with #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATE_RX_P1_O)"

if {![wrptp::recv_signal\
-port_num           $tee_port_1\
-filter_id          $filter_id\
-domain_number      $domain\
-timeout            $::wrptp_calibration_timeout_recv\
-tlv_type           $wrptp_tlv\
-error_reason       error_reason\
-message_id         $calibrate_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(CALIBRATE_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 45: Observe that DUT transmits WRPTP CALIBRATED message on port P1 with#
#          following parameters and store the received timestamp as TS1.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATED_RX_P1_O)"

if {![wrptp::recv_signal\
-port_num            $tee_port_1\
-filter_id           $filter_id\
-domain_number       $domain\
-timeout             $::wrptp_calibrated_timeout_recv\
-tlv_type            $wrptp_tlv\
-message_id          $calibrated_msg_id\
-error_reason        error_reason\
-reset_count         $::reset_count_off\
-rx_timestamp_ns     TS1]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(CALIBRATED_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 46: Send WRPTP CALIBRATE message on port T1 with following parameters. #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                  calSendPattern       = FALSE                               #
#                  calRetry             = 0                                   #
#                  calPeriod            = 3000us                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATE_TX_P1)"

if {![wrptp::send_signal\
-port_num                 $tee_port_1\
-dest_mac                 $tee_dest_mac\
-src_mac                  $tee_mac\
-dest_ip                  $tee_dest_ip\
-src_ip                   $tee_ip\
-domain_number            $domain\
-target_port_number       $src_port_number\
-target_clock_identity    $src_clock_identity\
-tlv_type                 $wrptp_tlv\
-message_id               $calibrate_msg_id\
-cal_retry                $cal_retry\
-cal_period               $cal_period]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(CALIBRATE_TX_P1_F)"\
		-tc_def  $tc_def

	return 0

}

if { $::SENT_CALIBRATE <= 0 } {

	vwait ::SENT_CALIBRATE

	set ::SENT_CALIBRATE    0
}

# Used to substract the execution time from the timer in next steps
set msgTimeMs [clock milliseconds]

###############################################################################
# Step 47: Wait for 150ms (0.5 x WR_RESP_CALIB_REQ_TIMEOUT) and observe that  #
#          WRPTP portState of port P1 is in RESP_CALIB_REQ state.             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(WAIT_CHECK_STATE_RESP_CALIB_REQ_P1_O)"

# Just wait 10 ms for the DUT to treat the received message
wrptp::wait_msec 10

if {![wrptp::dut_check_wr_port_state\
-port_num            $dut_port_1\
-wr_port_state       $resp_calib_req]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_RESP_CALIB_REQ_P1_F)"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 48: Wait for 600ms to complete 2 x WR_RESP_CALIB_REQ_TIMEOUT.          #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(WAIT_TWICE_RESP_CALIB_REQ_TIMEOUT)"

set procTime [ expr [clock milliseconds] - $msgTimeMs ]

wrptp::wait_msec [ expr $::wrptp_resp_calib_req_timeout_2 - $procTime ]

###############################################################################
# Step 49: Verify that WRPTP portState of port P1 is in RESP_CALIB_REQ state. #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_RESP_CALIB_REQ_P1_V)"

if {![wrptp::dut_check_wr_port_state\
-port_num            $dut_port_1\
-wr_port_state       $resp_calib_req]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_RESP_CALIB_REQ_P1_F)"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 50: Wait for 630ms to complete 2 x WR_RESP_CALIB_REQ_TIMEOUT + 10% of  #
#          WR_RESP_CALIB_REQ_TIMEOUT.                                         #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(WAIT_TWICE10_RESP_CALIB_REQ_TIMEOUT)"

wrptp::wait_msec $::wrptp_resp_calib_req_timeout_210

###############################################################################
# Step 51: Verify that WRPTP portState of port P1 is in IDLE state.           #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_IDLE_P1_V)"

if {![wrptp::dut_check_wr_port_state\
-port_num            $dut_port_1\
-wr_port_state       $idle]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_IDLE_P1_F)"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 52: Disable WRPTP on port P1.                                          #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(DISABLE_WRPTP_PORT_P1)"

if {![wrptp::dut_port_wrptp_disable\
-port_num          $dut_port_1]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(DISABLE_WRPTP_PORT_P1_F)"\
		-tc_def  $tc_def

	reason 0

}

###############################################################################
# Step 53: Enable WRPTP on port P1.                                           #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(ENABLE_WRPTP_PORT_P1)"

if {!( [wrptp::dut_port_wrptp_enable -port_num  $dut_port_1] &&
	[wrptp::dut_commit_changes]) } {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(ENABLE_WRPTP_PORT_P1_F)"\
		-tc_def  $tc_def

	reason 0

}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 54: Send periodic WRPTP ANNOUNCE messages on the port T1 with following#
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#                  grandmasterPriority1 = X - 1                               #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                  calibrated           = 1                                   #
#                  wrModeOn             = 0                                   #
###############################################################################

STEP [incr step]

set gm_priority1 [expr $::wrptp_priority1 - 1]

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_TX_P1)"

if {![wrptp::send_announce\
-port_num         $tee_port_1\
-dest_mac         $tee_dest_mac\
-src_mac          $tee_mac\
-src_ip           $tee_ip\
-dest_ip          $tee_dest_ip\
-domain_number    $domain\
-gm_priority1     $gm_priority1\
-tlv_type         $wrptp_tlv\
-message_id       $ann_msg_id\
-wrconfig         $wrconfig\
-count            $infinity\
-calibrated       $calibrated]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_TX_P1_F)"\
		-tc_def  $tc_def
	return 0

}

###############################################################################
# Step 55: Wait until completion of BMCA and observe that DUT transmits WRPTP #
#          SLAVE_PRESENT message on port P1 with following parameters.Store   #
#          the received timestamp as TS1.                                     #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
###############################################################################

STEP [incr step]

set bmca_signal_timeout  [expr $::wrptp_present_timeout_recv + $bmca_time]

LOG -level 0 -msg "$tee_log_msg(BMCA_SLAVE_PRESENT_RX_P1_O)"

if {![wrptp::recv_signal\
-port_num          $tee_port_1\
-filter_id         $filter_id\
-domain_number     $domain\
-timeout           $bmca_signal_timeout\
-error_reason      error_reason\
-tlv_type          $wrptp_tlv\
-reset_count       $::reset_count_off\
-rx_timestamp_ns   TS1\
-message_id        $slave_present_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SLAVE_PRESENT_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 56: Verify that the DUT transmits WRPTP SLAVE_PRESENT message on port  #
#          P1 with following parameters and store the received timestamp as   #
#          TS2.                                                               #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(SLAVE_PRESENT_RX_P1_O)"

if {![wrptp::recv_signal\
-port_num          $tee_port_1\
-filter_id         $filter_id\
-domain_number     $domain\
-tlv_type          $wrptp_tlv\
-error_reason      error_reason\
-reset_count       $::reset_count_off\
-rx_timestamp_ns   TS2\
-timeout           $::wrptp_present_timeout_recv\
-message_id        $slave_present_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL  -reason "$tee_log_msg(SLAVE_PRESENT_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 57: Verify that the DUT transmits WRPTP SLAVE_PRESENT message on port  #
#          P1 with following parameters and store the received timestamp as   #
#          TS3.                                                               #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(SLAVE_PRESENT_RX_P1_V)"

if {![wrptp::recv_signal\
-port_num          $tee_port_1\
-filter_id         $filter_id\
-domain_number     $domain\
-tlv_type          $wrptp_tlv\
-error_reason      error_reason\
-reset_count       $::reset_count_off\
-rx_timestamp_ns   TS3\
-timeout           $::wrptp_present_timeout_recv\
-message_id        $slave_present_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL  -reason "WRPTP enabled device with it's port in\
PTP Slave does not send SLAVE_PRESENT message\
continuously. $error_reason"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 58: Verify ((TS2 - TS1) + (TS3 - TS2))/2 = 1000ms (WR_PRESENT_TIMEOUT).#
###############################################################################

STEP [incr step]

set b [ expr (((($TS2-$TS1) + ($TS3-$TS2))/2)/1000000.0) ]
set b [format %.1f $b]

set min_intvl [expr $::wrptp_present_timeout - ($::wrptp_present_timeout * 0.3)]
set max_intvl [expr $::wrptp_present_timeout + ($::wrptp_present_timeout * 0.3)]

LOG -level 0 -msg "$tee_log_msg(CHECK_INTVL_EXPR_V)"
LOG -level 0 -msg "\nSLAVE_PRESENT message re-transmission interval:"
LOG -level 0 -msg "               Expected = $::wrptp_present_timeout ms (Acceptable range: $min_intvl ms - $max_intvl ms)"
LOG -level 0 -msg "               Observed = $b ms"

if {!($b >= $min_intvl && $b <= $max_intvl)} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL   -reason "DUT does not send SLAVE_PRESENT message at the expected\
re-transmission interval."\
		-tc_def  $tc_def
	return 0
}

###############################################################################
# Step 59: Disable WRPTP on port P1.                                          #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(DISABLE_WRPTP_PORT_P1)"

if {![wrptp::dut_port_wrptp_disable\
-port_num          $dut_port_1]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(DISABLE_WRPTP_PORT_P1_F)"\
		-tc_def  $tc_def

	reason 0

}

###############################################################################
# Step 60: Enable WRPTP on port P1.                                           #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(ENABLE_WRPTP_PORT_P1)"

if {!( [wrptp::dut_port_wrptp_enable -port_num  $dut_port_1] &&
	[wrptp::dut_commit_changes]) } {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(ENABLE_WRPTP_PORT_P1_F)"\
		-tc_def  $tc_def

	reason 0

}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 61: Send periodic WRPTP ANNOUNCE messages on the port T1 with following#
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#                  grandmasterPriority1 = X - 1                               #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                  calibrated           = 1                                   #
#                  wrModeOn             = 0                                   #
###############################################################################

STEP [incr step]

set gm_priority1 [expr $::wrptp_priority1 - 1]

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_TX_P1)"

if {![wrptp::send_announce\
-port_num         $tee_port_1\
-dest_mac         $tee_dest_mac\
-src_mac          $tee_mac\
-src_ip           $tee_ip\
-dest_ip          $tee_dest_ip\
-domain_number    $domain\
-gm_priority1     $gm_priority1\
-tlv_type         $wrptp_tlv\
-message_id       $ann_msg_id\
-wrconfig         $wrconfig\
-count            $infinity\
-calibrated       $calibrated]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_TX_P1_F)"\
		-tc_def  $tc_def
	return 0

}

###############################################################################
# Step 62: Wait until completion of BMCA and observe that DUT transmits WRPTP #
#          SLAVE_PRESENT message on port P1 with following parameters.Store   #
#          the received timestamp as TS1.                                     #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
###############################################################################

STEP [incr step]

set bmca_signal_timeout  [expr $::wrptp_present_timeout_recv + $bmca_time]

LOG -level 0 -msg "$tee_log_msg(BMCA_SLAVE_PRESENT_RX_P1_O)"

if {![wrptp::recv_signal\
-port_num          $tee_port_1\
-filter_id         $filter_id\
-domain_number     $domain\
-timeout           $bmca_signal_timeout\
-error_reason      error_reason\
-tlv_type          $wrptp_tlv\
-reset_count       $::reset_count_off\
-rx_timestamp_ns   TS1\
-message_id        $slave_present_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SLAVE_PRESENT_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 63: Send WRPTP LOCK message on port T1 with following parameters.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCK_TX_P1)"

if {![wrptp::send_signal\
-port_num               $tee_port_1\
-src_mac                $tee_mac\
-dest_mac               $tee_dest_mac\
-src_ip                 $tee_ip\
-dest_ip                $tee_dest_ip\
-domain_number          $domain\
-target_port_number     $src_port_number\
-target_clock_identity  $src_clock_identity\
-tlv_type               $wrptp_tlv\
-message_id             $lock_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(LOCK_TX_P1_F)"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 64: Observe that the DUT transmits WRPTP LOCKED message on port P1 with#
#          following parameters and store the received timestamp as TS1.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1002                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCKED_RX_P1_O)"

if {![wrptp::recv_signal \
-port_num                $tee_port_1\
-domain_number           $domain\
-tlv_type                $wrptp_tlv\
-message_id              $locked_msg_id\
-reset_count             $::reset_count_off\
-rx_timestamp_ns         TS1\
-error_reason            error_reason\
-timeout                 $::wrptp_locked_timeout_recv\
-filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(LOCKED_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 65: Observe that the DUT transmits WRPTP LOCKED message on port P1 with#
#          following parameters and store the received timestamp as TS2.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1002                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCKED_RX_P1_O)"

if {![wrptp::recv_signal \
-port_num                $tee_port_1\
-domain_number           $domain\
-tlv_type                $wrptp_tlv\
-message_id              $locked_msg_id\
-reset_count             $::reset_count_off\
-rx_timestamp_ns         TS2\
-error_reason            error_reason\
-timeout                 $::wrptp_locked_timeout_recv\
-filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(LOCKED_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 66: Observe that the DUT transmits WRPTP LOCKED message on port P1 with#
#          following parameters and store the received timestamp as TS3.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1002                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCKED_RX_P1_O)"

if {![wrptp::recv_signal \
-port_num                $tee_port_1\
-domain_number           $domain\
-tlv_type                $wrptp_tlv\
-message_id              $locked_msg_id\
-reset_count             $::reset_count_off\
-error_reason            error_reason\
-rx_timestamp_ns         TS3\
-timeout                 $::wrptp_locked_timeout_recv\
-filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(LOCKED_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 67: Verify ((TS2 - TS1) + (TS3 - TS2))/2 = 300ms (WR_LOCKED_TIMEOUT).  #
###############################################################################

STEP [incr step]

set b [ expr (((($TS2-$TS1) + ($TS3-$TS2))/2)/1000000.0) ]
set b [format %.1f $b]

set min_intvl [expr $::wrptp_locked_timeout - ($::wrptp_locked_timeout * 0.3)]
set max_intvl [expr $::wrptp_locked_timeout + ($::wrptp_locked_timeout * 0.3)]

LOG -level 0 -msg "$tee_log_msg(CHECK_INTVL_EXPR_V)"
LOG -level 0 -msg "\nLOCKED message re-transmission interval:"
LOG -level 0 -msg "               Expected = $::wrptp_locked_timeout ms (Acceptable range: $min_intvl ms - $max_intvl ms)"
LOG -level 0 -msg "               Observed = $b ms"

if {!($b >= $min_intvl && $b <= $max_intvl)} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL   -reason "DUT in PTP Slave does not send LOCKED message and\
does not re-enter to it's port WR state LOCKED on\
expiry of WR_LOCKED_TIMEOUT."\
		-tc_def  $tc_def
	return 0
}

###############################################################################
# Step 68: Disable WRPTP on port P1.                                          #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(DISABLE_WRPTP_PORT_P1)"

if {![wrptp::dut_port_wrptp_disable\
-port_num          $dut_port_1]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(DISABLE_WRPTP_PORT_P1_F)"\
		-tc_def  $tc_def

	reason 0

}

###############################################################################
# Step 69: Enable WRPTP on port P1.                                           #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(ENABLE_WRPTP_PORT_P1)"

if {!( [wrptp::dut_port_wrptp_enable -port_num  $dut_port_1] &&
	[wrptp::dut_commit_changes]) } {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(ENABLE_WRPTP_PORT_P1_F)"\
		-tc_def  $tc_def

	reason 0

}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 70: Send periodic WRPTP ANNOUNCE messages on the port T1 with following#
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#                  grandmasterPriority1 = X - 1                               #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                  calibrated           = 1                                   #
#                  wrModeOn             = 0                                   #
###############################################################################

STEP [incr step]

set gm_priority1 [expr $::wrptp_priority1 - 1]

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_TX_P1)"

if {![wrptp::send_announce\
-port_num         $tee_port_1\
-dest_mac         $tee_dest_mac\
-src_mac          $tee_mac\
-src_ip           $tee_ip\
-dest_ip          $tee_dest_ip\
-domain_number    $domain\
-gm_priority1     $gm_priority1\
-tlv_type         $wrptp_tlv\
-message_id       $ann_msg_id\
-wrconfig         $wrconfig\
-count            $infinity\
-calibrated       $calibrated]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_TX_P1_F)"\
		-tc_def  $tc_def
	return 0

}

###############################################################################
# Step 71: Wait until completion of BMCA and observe that DUT transmits WRPTP #
#          SLAVE_PRESENT message on port P1 with following parameters.Store   #
#          the received timestamp as TS1.                                     #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
###############################################################################

STEP [incr step]

set bmca_signal_timeout  [expr $::wrptp_present_timeout_recv + $bmca_time]

LOG -level 0 -msg "$tee_log_msg(BMCA_SLAVE_PRESENT_RX_P1_O)"

if {![wrptp::recv_signal\
-port_num          $tee_port_1\
-filter_id         $filter_id\
-domain_number     $domain\
-timeout           $bmca_signal_timeout\
-error_reason      error_reason\
-tlv_type          $wrptp_tlv\
-reset_count       $::reset_count_off\
-rx_timestamp_ns   TS1\
-message_id        $slave_present_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SLAVE_PRESENT_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 72: Send WRPTP LOCK message on port T1 with following parameters.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCK_TX_P1)"

if {![wrptp::send_signal\
-port_num               $tee_port_1\
-src_mac                $tee_mac\
-dest_mac               $tee_dest_mac\
-src_ip                 $tee_ip\
-dest_ip                $tee_dest_ip\
-domain_number          $domain\
-target_port_number     $src_port_number\
-target_clock_identity  $src_clock_identity\
-tlv_type               $wrptp_tlv\
-message_id             $lock_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(LOCK_TX_P1_F)"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 73: Observe that the DUT transmits WRPTP LOCKED message on port P1 with#
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1002                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCKED_RX_P1_O)"

if {![wrptp::recv_signal \
-port_num                $tee_port_1\
-domain_number           $domain\
-tlv_type                $wrptp_tlv\
-message_id              $locked_msg_id\
-reset_count             $::reset_count_off\
-error_reason            error_reason\
-timeout                 $::wrptp_locked_timeout_recv\
-filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(LOCKED_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 74: Send WRPTP CALIBRATE message on port T1 with following parameters. #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                  calSendPattern       = FALSE                               #
#                  calRetry             = 0                                   #
#                  calPeriod            = 3000us                              #
#                                                                             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATE_TX_P1)"

if {![wrptp::send_signal \
-port_num                $tee_port_1\
-src_mac                 $tee_mac\
-dest_mac                $tee_dest_mac\
-src_ip                  $tee_ip\
-dest_ip                 $tee_dest_ip\
-domain_number           $domain\
-target_port_number      $src_port_number\
-target_clock_identity   $src_clock_identity\
-tlv_type                $wrptp_tlv\
-message_id              $calibrate_msg_id\
-cal_send_pattern        $cal_send_pattern\
-cal_retry               $cal_retry\
-cal_period              $cal_period]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(CALIBRATE_TX_P1_F)"\
		-tc_def  $tc_def

	return 0
}

if { $::SENT_CALIBRATE <= 0 } {

	vwait ::SENT_CALIBRATE

	set ::SENT_CALIBRATE    0
}

# Used to substract the execution time from the timer in next steps
set msgTimeMs [clock milliseconds]

###############################################################################
# Step 75: Wait for 150ms (0.5 x WR_RESP_CALIB_REQ_TIMEOUT) and observe that  #
#          WRPTP portState of port P1 is in RESP_CALIB_REQ state.             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(WAIT_CHECK_STATE_RESP_CALIB_REQ_P1_O)"

wrptp::wait_msec 10 
#$::wrptp_resp_calib_req_timeout_5

if {![wrptp::dut_check_wr_port_state\
-port_num            $dut_port_1\
-wr_port_state       $resp_calib_req]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_RESP_CALIB_REQ_P1_F)"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 76: Wait for 600ms to complete 2 x WR_RESP_CALIB_REQ_TIMEOUT.          #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(WAIT_TWICE_RESP_CALIB_REQ_TIMEOUT)"

set procTime [ expr [clock milliseconds] - $msgTimeMs ]

wrptp::wait_msec [ expr $::wrptp_resp_calib_req_timeout_2 - $procTime ]

###############################################################################
# Step 77: Verify that WRPTP portState of port P1 is in RESP_CALIB_REQ state. #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_RESP_CALIB_REQ_P1_V)"

if {![wrptp::dut_check_wr_port_state\
-port_num            $dut_port_1\
-wr_port_state       $resp_calib_req]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_RESP_CALIB_REQ_P1_F)"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 78: Wait for 630ms to complete 2 x WR_RESP_CALIB_REQ_TIMEOUT + 10% of  #
#          WR_RESP_CALIB_REQ_TIMEOUT.                                         #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(WAIT_TWICE10_RESP_CALIB_REQ_TIMEOUT)"

wrptp::wait_msec $::wrptp_resp_calib_req_timeout_210

###############################################################################
# Step 79: Verify that WRPTP portState of port P1 is in IDLE state.           #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_IDLE_P1_V)"

if {![wrptp::dut_check_wr_port_state\
-port_num            $dut_port_1\
-wr_port_state       $idle]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_IDLE_P1_F)"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 80: Disable WRPTP on port P1.                                          #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(DISABLE_WRPTP_PORT_P1)"

if {![wrptp::dut_port_wrptp_disable\
-port_num          $dut_port_1]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(DISABLE_WRPTP_PORT_P1_F)"\
		-tc_def  $tc_def

	reason 0

}

###############################################################################
# Step 81: Enable WRPTP on port P1.                                           #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(ENABLE_WRPTP_PORT_P1)"

if {!( [wrptp::dut_port_wrptp_enable -port_num  $dut_port_1] &&
	[wrptp::dut_commit_changes]) } {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(ENABLE_WRPTP_PORT_P1_F)"\
		-tc_def  $tc_def

	reason 0

}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 82: Send periodic WRPTP ANNOUNCE messages on the port T1 with following#
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#                  grandmasterPriority1 = X - 1                               #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                  calibrated           = 1                                   #
#                  wrModeOn             = 0                                   #
###############################################################################

STEP [incr step]

set gm_priority1 [expr $::wrptp_priority1 - 1]

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_TX_P1)"

if {![wrptp::send_announce\
-port_num         $tee_port_1\
-dest_mac         $tee_dest_mac\
-src_mac          $tee_mac\
-src_ip           $tee_ip\
-dest_ip          $tee_dest_ip\
-domain_number    $domain\
-gm_priority1     $gm_priority1\
-tlv_type         $wrptp_tlv\
-message_id       $ann_msg_id\
-wrconfig         $wrconfig\
-count            $infinity\
-calibrated       $calibrated]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_TX_P1_F)"\
		-tc_def  $tc_def
	return 0

}

###############################################################################
# Step 83: Wait until completion of BMCA and observe that DUT transmits WRPTP #
#          SLAVE_PRESENT message on port P1 with following parameters.Store   #
#          the received timestamp as TS1.                                     #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
###############################################################################

STEP [incr step]

set bmca_signal_timeout  [expr $::wrptp_present_timeout_recv + $bmca_time]

LOG -level 0 -msg "$tee_log_msg(BMCA_SLAVE_PRESENT_RX_P1_O)"

if {![wrptp::recv_signal\
-port_num          $tee_port_1\
-filter_id         $filter_id\
-domain_number     $domain\
-timeout           $bmca_signal_timeout\
-error_reason      error_reason\
-tlv_type          $wrptp_tlv\
-reset_count       $::reset_count_off\
-rx_timestamp_ns   TS1\
-message_id        $slave_present_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SLAVE_PRESENT_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 84: Send WRPTP LOCK message on port T1 with following parameters.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCK_TX_P1)"

if {![wrptp::send_signal\
-port_num               $tee_port_1\
-src_mac                $tee_mac\
-dest_mac               $tee_dest_mac\
-src_ip                 $tee_ip\
-dest_ip                $tee_dest_ip\
-domain_number          $domain\
-target_port_number     $src_port_number\
-target_clock_identity  $src_clock_identity\
-tlv_type               $wrptp_tlv\
-message_id             $lock_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(LOCK_TX_P1_F)"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 85: Observe that the DUT transmits WRPTP LOCKED message on port P1 with#
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1002                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCKED_RX_P1_O)"

if {![wrptp::recv_signal \
-port_num                $tee_port_1\
-domain_number           $domain\
-tlv_type                $wrptp_tlv\
-message_id              $locked_msg_id\
-reset_count             $::reset_count_off\
-error_reason            error_reason\
-timeout                 $::wrptp_locked_timeout_recv\
-filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(LOCKED_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 86: Send WRPTP CALIBRATE message on port T1 with following parameters. #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                  calSendPattern       = FALSE                               #
#                  calRetry             = 0                                   #
#                  calPeriod            = 3000us                              #
#                                                                             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATE_TX_P1)"

if {![wrptp::send_signal \
-port_num                $tee_port_1\
-src_mac                 $tee_mac\
-dest_mac                $tee_dest_mac\
-src_ip                  $tee_ip\
-dest_ip                 $tee_dest_ip\
-domain_number           $domain\
-target_port_number      $src_port_number\
-target_clock_identity   $src_clock_identity\
-tlv_type                $wrptp_tlv\
-message_id              $calibrate_msg_id\
-cal_send_pattern        $cal_send_pattern\
-cal_retry               $cal_retry\
-cal_period              $cal_period]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(CALIBRATE_TX_P1_F)"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 87: Send WRPTP CALIBRATED message on port T1 with following parameters #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = 0                                   #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                  deltaTx              = 0                                   #
#                  deltaRx              = 0                                   #
#                                                                             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATED_TX_P1)"

set delta_tx    0
set delta_rx    0

if {![wrptp::send_signal \
-port_num                $tee_port_1\
-src_mac                 $tee_mac\
-dest_mac                $tee_dest_mac\
-src_ip                  $tee_ip\
-dest_ip                 $tee_dest_ip\
-domain_number           $domain\
-target_port_number      $src_port_number\
-target_clock_identity   $src_clock_identity\
-tlv_type                $wrptp_tlv\
-delta_tx                $delta_tx\
-delta_rx                $delta_rx\
-message_id              $calibrated_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(CALIBRATED_TX_P1_F)"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 88: Observe that the DUT transmits WRPTP CALIBRATE message on port P1  #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATE_RX_P1_O)"

if {![wrptp::recv_signal\
-port_num                $tee_port_1\
-domain_number           $domain\
-tlv_type                $wrptp_tlv\
-message_id              $calibrate_msg_id\
-timeout                 $::wrptp_calibration_timeout_recv\
-error_reason            error_reason\
-filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(CALIBRATE_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 89: Observe that the DUT transmits WRPTP CALIBRATED message on port P1 #
#          with following parameters and store the received timestamp as TS1. #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATED_RX_P1_O)"

if {![wrptp::recv_signal\
-port_num                $tee_port_1\
-domain_number           $domain\
-tlv_type                $wrptp_tlv\
-message_id              $calibrated_msg_id\
-rx_timestamp_ns         TS1\
-error_reason            error_reason\
-reset_count             $::reset_count_off\
-timeout                 $::wrptp_calibrated_timeout_recv\
-filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(CALIBRATED_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 90: Verify that the DUT transmits WRPTP CALIBRATED message on port P1  #
#          with following parameters and store the received timestamp as TS2. #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATED_RX_P1_O)"

if {![wrptp::recv_signal\
-port_num                $tee_port_1\
-domain_number           $domain\
-tlv_type                $wrptp_tlv\
-message_id              $calibrated_msg_id\
-rx_timestamp_ns         TS2\
-error_reason            error_reason\
-reset_count             $::reset_count_off\
-timeout                 $::wrptp_calibrated_timeout_recv\
-filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL -reason "$tee_log_msg(CALIBRATED_RX_2_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 91: Verify that the DUT transmits WRPTP CALIBRATED message on port P1  #
#          with following parameters and store the received timestamp as TS3. #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATED_RX_P1_O)"

if {![wrptp::recv_signal\
-port_num                $tee_port_1\
-domain_number           $domain\
-tlv_type                $wrptp_tlv\
-message_id              $calibrated_msg_id\
-rx_timestamp_ns         TS3\
-error_reason            error_reason\
-reset_count             $::reset_count_off\
-timeout                 $::wrptp_calibrated_timeout_recv\
-filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL -reason "$tee_log_msg(CALIBRATED_RX_2_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 92: Verify ((TS2 - TS1) + (TS3 - TS2))/2 = 300ms                       #
#          (WR_CALIBRATED_TIMEOUT).                                           #
###############################################################################

STEP [incr step]

set b [ expr (((($TS2-$TS1) + ($TS3-$TS2))/2)/1000000.0) ]
set b [format %.1f $b]

set min_intvl [expr $::wrptp_calibrated_timeout - ($::wrptp_calibrated_timeout * 0.3)]
set max_intvl [expr $::wrptp_calibrated_timeout + ($::wrptp_calibrated_timeout * 0.3)]

LOG -level 0 -msg "$tee_log_msg(CHECK_INTVL_EXPR_V)"
LOG -level 0 -msg "\nCALIBRATED message re-transmission interval:"
LOG -level 0 -msg "               Expected = $::wrptp_calibrated_timeout ms (Acceptable range: $min_intvl ms - $max_intvl ms)"
LOG -level 0 -msg "               Observed = $b ms"

if {!($b >= $min_intvl && $b <= $max_intvl)} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL   -reason "DUT does not send CALIBRATED message and\
does not re-enter to it's port WR state CALIBRATED\
on expiry of WR_CALIBRATED_TIMEOUT."\
		-tc_def  $tc_def
	return 0
}

TEE_CLEANUP

TC_CLEAN_AND_PASS   -reason "DUT stores all attributes with default initialization values."\
	-tc_def  $tc_def

return 1

############################ END OF TEST CASE #################################

