#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_wrptp_iog_001                                   #
# Test Case Version : 1.1                                                     #
# Component Name    : ATTEST WRPTP CONFORMANCE TEST SUITE                     #
# Module Name       : Inter-operability Group (IOG)                           #
#                                                                             #
# Title             : Working of WR Master with non-WR device                 #
#                                                                             #
# Purpose           : To verify that a WRPTP enabled device in WR_MASTER mode #
#                     moves to standard PTP Master mode when it is connected  #
#                     to non-WR device.                                       #
#                                                                             #
# Reference         : White Rabbit Specification v2.0 July 2011, Clause 6.1   #
#                     Page 13, Figure 28 Page 63                              #
#                                                                             #
# Conformance Type  : MUST                                                    #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 001                                                     #
# Test Topology     : 1                                                       #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode = One-step/Two-step> |           #
#           |          <Transport Protocol = IEEE 802.3/Ethernet> |           #
#           |            <Configure Priority1 = X, Priority2 = Y> | P1        #
#           |                                      <Enable WRPTP> | P1        #
#           |                   <Configure wrConfig = WR_M_AND_S> | P1        #
#           |      <Configure default values for knownDeltaTx and | P1        #
#           |                                       knownDeltaRx> |           #
#           |                                                     |           #
#           |                 WRPTP ANNOUNCE [messageType = 0x0B, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x2000] |           #
#           |                          {grandmasterPriority1 = X} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | ANNOUNCE [messageType = 0x0B,                       |           #
#           | domainNumber = DN1, correctionField = 0,            |           #
#           | controlField = 0x05, logMessageInterval = 1,        |           #
#           | grandmasterPriority1 = X + 1]                       |           #
#           | (sendCount = INFINITY)                              |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                           SYNC [messageType = 0x00, |           #
#           |                                 domainNumber = DN1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | DELAY_REQ [messageType = 0x01,                      |           #
#           | domainNumber = DN1, correctionField = 0,            |           #
#           | controlField = 0x01, logMessageInterval = 0x7F]     |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                     DELAY_RESP [messageType = 0x09, |           #
#           |                                 domainNumber = DN1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     TEE       = Test Execution Engine                                       #
#     DUT       = Device Under Test                                           #
#     T1        = TEE's port 1                                                #
#     P1        = DUT's port 1                                                #
#     PTP       = Precision Time Protocol                                     #
#     WRPTP     = White Rabbit Precision Time Protocol                        #
#     OC        = Ordinary Clock                                              #
#     BC        = Boundary Clock                                              #
#     DN1       = Domain Number 1                                             #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure Network Transport Protocol as IEEE 802.3/Ethernet.       #
#    vi.   Configure default values for Priority1 = X and Priority2 = Y.      #
#   vii.   Enable WRPTP on port P1.                                           #
#  viii.   Configure wrConfig = WR_M_AND_S.                                   #
#    ix.   Configure default values for knownDeltaTx and knownDeltaRx on P1.  #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Observe that DUT transmits WRPTP ANNOUNCE message on the port P1   #
#          with following parameters and store grandmasterPriority1 as X.     #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                                                                             #
# Step 4 : Send periodic ANNOUNCE messages on the port T1 with following      #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#                  correctionField      = 0                                   #
#                  controlField         = 0x05                                #
#                  logMessageInterval   = 1                                   #
#                  grandmasterPriority1 = X + 1                               #
#                                                                             #
# Step 5 : Verify that DUT transmits SYNC message on port P1 with following   #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x00                                #
#                  domainNumber         = DN1                                 #
#                                                                             #
# Step 6 : Send DELAY_REQ message on the port T1 with following parameters.   #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x01                                #
#                  domainNumber         = DN1                                 #
#                  correctionField      = 0                                   #
#                  controlField         = 0x01                                #
#                  logMessageInterval   = 0x7F                                #
#                                                                             #
# Step 7 : Verify that DUT transmits DELAY_RESP message on port P1 with       #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x09                                #
#                  domainNumber         = DN1                                 #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          Jul/2018      CERN          Initial                            #
# 1.1          Apr/2019      JC.BAU/CERN   Improve execution time             #
#                                                                             #
###############################################################################
# Copyright (c) 2018 CERN                                                     #
###############################################################################

global dut_log_msg
global tee_log_msg
global tee_rx_errlog
global session_id
global step

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def    "To verify that a WRPTP enabled device in WR_MASTER mode\
			   moves to standard PTP Master mode when it is connected\
			   to non-WR device."

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set dut_port_1                     $::dut_port_num_1
set vlan_id                        $::wrptp_vlan_id
set dut_ip                         $::dut_test_port_ip
set dut_dest_ip                    $::dut_test_port_ip
set tee_dest_ip                    $::tee_test_port_ip
set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT

set domain                         $::DEFAULT_DOMAIN
set wrptp_tlv                      $::ORG_EXT_TLV
set infinity                       $::INFINITY
set announce_timeout               [expr $::wrptp_announce_timeout + 60]
set ann_msg_id                     $::wrMessageID(ANN_SUFIX)
set log_message_interval           1
set step                           0

########################### END - INITIALIZATION ##############################

wrptp::display_interface_details

wrptp::dut_Initialization

proc cleanup {} {
global step
if { $step != 0 } {
	wrptp::dut_cleanup_setup_001
	wrptp::dut_commit_changes
}
}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

### (Initial Part) ###

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure Network Transport Protocol as IEEE 802.3/Ethernet.       #
#    vi.   Configure default values for Priority1 = X and Priority2 = Y.      #
#   vii.   Enable WRPTP on port P1.                                           #
#  viii.   Configure wrConfig = WR_M_AND_S.                                   #
#    ix.   Configure default values for knownDeltaTx and knownDeltaRx on P1.  #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP1)"

if {!( [wrptp::dut_configure_setup_001] && [wrptp::dut_commit_changes]) } {

	LOG -msg "$dut_log_msg(INIT_SETUP1_F)"

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP1_F)"\
		-tc_def  $tc_def
	return 0
}



###############################################################################
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(INIT)"

wrptp::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

	if {![WRPTP_UNICAST_PRE_REQUISITE_CONFIGURATION\
						-port_num       $tee_port_1\
						-tee_mac        $tee_mac\
						-tee_ip         $tee_ip\
						-dut_ip         $dut_ip\
						-vlan_id        $vlan_id\
						-dut_mac        tee_dest_mac\
						-timeout        $arp_timeout]} {

		TEE_CLEANUP

		TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
			-tc_def  $tc_def

		return 0
	}

} else {
	set tee_dest_ip             $::PTP_E2E_IP
	set tee_dest_mac            $::PTP_E2E_MAC
}

if {![pltLib::create_filter_wrptp_pkt\
			  -port_num                $tee_port_1\
			  -ether_type              $ptp_ethtype\
			  -vlan_id                 $vlan_id\
			  -ids                     id1]} {

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
		-tc_def  $tc_def

	return 0
}

set filter_id [lindex $id1 end]

wrptp::reset_capture_stats\
	-port_num           $tee_port_1



###############################################################################
# Step 3 : Observe that DUT transmits WRPTP ANNOUNCE message on the port P1   #
#          with following parameters and store grandmasterPriority1 as X.     #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![wrptp::recv_announce\
		   -port_num            $tee_port_1\
		   -filter_id           $filter_id\
		   -domain_number       $domain\
		   -tlv_type            $wrptp_tlv\
		   -timeout             $announce_timeout\
		   -error_reason        error_reason\
		   -recvd_gm_priority1  recvd_gm_priority1\
		   -message_id          $ann_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0

}



###############################################################################
# Step 4 : Send periodic ANNOUNCE messages on the port T1 with following      #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#                  correctionField      = 0                                   #
#                  controlField         = 0x05                                #
#                  logMessageInterval   = 1                                   #
#                  grandmasterPriority1 = X + 1                               #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_TX_P1)"

set gm_priority1  [expr $recvd_gm_priority1 + 1]
set control_field 5

if {![wrptp::send_announce\
			-port_num              $tee_port_1\
			-src_mac               $tee_mac\
			-dest_mac              $tee_dest_mac\
			-src_ip                $tee_ip\
			-dest_ip               $tee_dest_ip\
			-domain_number         $domain\
			-count                 $infinity\
			-control_field         $control_field\
			-log_message_interval  $log_message_interval\
			-gm_priority1          $gm_priority1]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_TX_P1_F)"\
		-tc_def  $tc_def

	return 0

}



###############################################################################
# Step 5 : Verify that DUT transmits SYNC message on port P1 with following   #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x00                                #
#                  domainNumber         = DN1                                 #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(SYNC_RX_P1_V)"

if {![wrptp::recv_sync\
		   -port_num       $tee_port_1\
		   -filter_id      $filter_id\
		   -domain_number  $domain\
		   -error_reason   error_reason\
		   -timeout        $::ptp_sync_timeout]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL -reason "DUT does not transmit SYNC message on port P1 $error_reason"\
		-tc_def  $tc_def

	return 0

}



###############################################################################
# Step 6 : Send DELAY_REQ message on the port T1 with following parameters.   #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x01                                #
#                  domainNumber         = DN1                                 #
#                  correctionField      = 0                                   #
#                  controlField         = 0x01                                #
#                  logMessageInterval   = 0x7F                                #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(DELAY_REQ_TX_P1)"

set control_field  1

if {![wrptp::send_delay_req\
			-port_num              $tee_port_1\
			-src_mac               $tee_mac\
			-dest_mac              $tee_dest_mac\
			-src_ip                $tee_ip\
			-dest_ip               $tee_dest_ip\
			-control_field         $control_field\
			-count                 $infinity\
			-domain_number         $domain]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(DELAY_REQ_TX_P1_F)"\
		-tc_def  $tc_def

	return 0

}



###############################################################################
# Step 7 : Verify that DUT transmits DELAY_RESP message on port P1 with       #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x09                                #
#                  domainNumber         = DN1                                 #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(DELAY_RESP_RX_P1_V)"

if {![wrptp::recv_delay_resp\
			-port_num          $tee_port_1\
			-filter_id         $filter_id\
			-domain_number     $domain\
			-error_reason      error_reason\
			-timeout           $::ptp_delay_resp_timeout]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL -reason "DUT does not transmit DELAY_RESP message\
								on port P1. $error_reason"\
		-tc_def  $tc_def

	return 0

}

TEE_CLEANUP

TC_CLEAN_AND_PASS -reason "DUT in WR_MASTER mode moves to standard\
						   PTP Master mode when it is connected\
						   to non-WR device."\
	-tc_def  $tc_def

return 1

############################# END OF TESTCASE #################################

