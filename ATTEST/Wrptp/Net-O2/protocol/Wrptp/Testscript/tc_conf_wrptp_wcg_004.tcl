#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_wrptp_wcg_004                                   #
# Test Case Version : 1.4                                                     #
# Component Name    : ATTEST WRPTP CONFORMANCE TEST SUITE                     #
# Module Name       : WRPTP Configuration Group (WCG)                         #
#                                                                             #
# Title             : knownDeltaTx in WR Slave                                #
#                                                                             #
# Purpose           : To verify that a WRPTP enabled device with it's port in #
#                     PTP Slave sends CALIBRATED message with configured      #
#                     knownDeltaTx (allowed range: UInteger64).               #
#                                                                             #
# Reference         : White Rabbit Specification v2.0 July 2011, Clause 6.3   #
#                     Page 16                                                 #
#                                                                             #
# Conformance Type  : MUST                                                    #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 001                                                     #
# Test Topology     : 2                                                       #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode = One-step/Two-step> |           #
#           |          <Transport Protocol = IEEE 802.3/Ethernet> |           #
#           |            <Configure Priority1 = X, Priority2 = Y> | P1        #
#           |                                      <Enable WRPTP> | P1        #
#           |                   <Configure wrConfig = WR_M_AND_S> | P1        #
#           |      <Configure default values for knownDeltaTx and | P1        #
#           |                                       knownDeltaRx> |           #
#           |                                                     |           #
#           | WRPTP ANNOUNCE [messageType = 0x0B,                 |           #
#           | domainNumber = DN1, grandmasterPriority1 = X - 1,   |           #
#           | tlvType = 0x0003, wrMessageId = 0x2000,             |           #
#           | wrConfig = 0x3, calibrated = 1, wrModeOn = 0]       |           #
#           | (sendCount = INFINITY)                              |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |            WRPTP SLAVE_PRESENT [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1000] |           #
#           |                         {sourcePortIdentity = SPI1} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP LOCK [messageType = 0x0C,                     |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1001]             |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                   WRPTP LOCKED [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1002] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP CALIBRATE [messageType = 0x0C,                |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1003,             |           #
#           | calSendPattern = FALSE, calRetry = 0,               |           #
#           | calPeriod = 0]                                      |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           | WRPTP CALIBRATED [messageType = 0x0C,               |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1004,             |           #
#           | deltaTx = 0, deltaRx = 0]                           |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                WRPTP CALIBRATE [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1003] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |               WRPTP CALIBRATED [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |      wrMessageId = 0x1004, deltaTx = Default value] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                                     <Disable WRPTP> | P1        #
#           |                                                     |           #
#           |                                      <Enable WRPTP> | P1        #
#           |                                                     |           #
#           |                        <Configure knownDeltaTx = 0> | P1        #
#           |                                                     |           #
#           |           <Wait for WRPTP to be enabled>            |           #
#           |                                                     |           #
#           | WRPTP ANNOUNCE [messageType = 0x0B,                 |           #
#           | domainNumber = DN1, grandmasterPriority1 = X - 1,   |           #
#           | tlvType = 0x0003, wrMessageId = 0x2000,             |           #
#           | wrConfig = 0x3, calibrated = 1, wrModeOn = 0]       |           #
#           | (sendCount = INFINITY)                              |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |            WRPTP SLAVE_PRESENT [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1000] |           #
#           |                         {sourcePortIdentity = SPI1} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP LOCK [messageType = 0x0C,                     |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1001]             |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                   WRPTP LOCKED [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1002] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP CALIBRATE [messageType = 0x0C,                |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1003,             |           #
#           | calSendPattern = FALSE, calRetry = 0,               |           #
#           | calPeriod = 0]                                      |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           | WRPTP CALIBRATED [messageType = 0x0C,               |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1004,             |           #
#           | deltaTx = 0, deltaRx = 0]                           |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                WRPTP CALIBRATE [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1003] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |               WRPTP CALIBRATED [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                  wrMessageId = 0x1004, deltaTx = 0] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                                     <Disable WRPTP> | P1        #
#           |                                                     |           #
#           |                                      <Enable WRPTP> | P1        #
#           |                                                     |           #
#           |            <Configure knownDeltaTx = Default value> | P1        #
#           |                                                     |           #
#           |           <Wait for WRPTP to be enabled>            |           #
#           |                                                     |           #
#           | WRPTP ANNOUNCE [messageType = 0x0B,                 |           #
#           | domainNumber = DN1, grandmasterPriority1 = X - 1,   |           #
#           | tlvType = 0x0003, wrMessageId = 0x2000,             |           #
#           | wrConfig = 0x3, calibrated = 1, wrModeOn = 0]       |           #
#           | (sendCount = INFINITY)                              |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |            WRPTP SLAVE_PRESENT [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1000] |           #
#           |                         {sourcePortIdentity = SPI1} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP LOCK [messageType = 0x0C,                     |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1001]             |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                   WRPTP LOCKED [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1002] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP CALIBRATE [messageType = 0x0C,                |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1003,             |           #
#           | calSendPattern = FALSE, calRetry = 0,               |           #
#           | calPeriod = 0]                                      |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           | WRPTP CALIBRATED [messageType = 0x0C,               |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1004,             |           #
#           | deltaTx = 0, deltaRx = 0]                           |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                WRPTP CALIBRATE [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1003] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |               WRPTP CALIBRATED [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |      wrMessageId = 0x1004, deltaTx = Default value] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     TEE       = Test Execution Engine                                       #
#     DUT       = Device Under Test                                           #
#     T1        = TEE's port 1                                                #
#     P1        = DUT's port 1                                                #
#     PTP       = Precision Time Protocol                                     #
#     WRPTP     = White Rabbit Precision Time Protocol                        #
#     OC        = Ordinary Clock                                              #
#     BC        = Boundary Clock                                              #
#     DN1       = Domain Number 1                                             #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure Network Transport Protocol as IEEE 802.3/Ethernet.       #
#    vi.   Configure default values for Priority1 = X and Priority2 = Y.      #
#   vii.   Enable WRPTP on port P1.                                           #
#  viii.   Configure wrConfig = WR_M_AND_S.                                   #
#    ix.   Configure default values for knownDeltaTx and knownDeltaRx on P1.  #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Send periodic WRPTP ANNOUNCE messages on the port T1 with following#
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#                  grandmasterPriority1 = X - 1                               #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                  calibrated           = 1                                   #
#                  wrModeOn             = 0                                   #
#                                                                             #
# Step 4 : Wait until completion of BMCA and observe that DUT transmits WRPTP #
#          SLAVE_PRESENT message on port P1 with following parameters. Store  #
#          sourcePortIdentity as SPI1.                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
#                                                                             #
# Step 5 : Send WRPTP LOCK message on port T1 with following parameters.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
#                                                                             #
# Step 6 : Observe that the DUT transmits WRPTP LOCKED message on port P1 with#
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1002                              #
#                                                                             #
# Step 7 : Send WRPTP CALIBRATE message on port T1 with following parameters. #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                  calSendPattern       = FALSE                               #
#                  calRetry             = 0                                   #
#                  calPeriod            = 0                                   #
#                                                                             #
# Step 8 : Send WRPTP CALIBRATED message on port T1 with following parameters.#
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                  deltaTx              = 0                                   #
#                  deltaRx              = 0                                   #
#                                                                             #
# Step 9 : Observe that the DUT transmits WRPTP CALIBRATE message on port P1  #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                                                                             #
# Step 10: Verify that the DUT transmits WRPTP CALIBRATED message on port P1  #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                  deltaTx              = Default value                       #
#                                                                             #
# (Part 2)                                                                    #
#                                                                             #
# Step 11: Disable WRPTP on port P1.                                          #
#                                                                             #
# Step 12: Configure knownDeltaTx = 0 on port P1.                             #
#                                                                             #
# Step 13: Enable WRPTP on port P1.                                           #
#                                                                             #
# Step 14: Wait for WRPTP te be enabled.                                      #
#                                                                             #
# Step 15: Send periodic WRPTP ANNOUNCE messages on the port T1 with following#
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#                  grandmasterPriority1 = X - 1                               #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                  calibrated           = 1                                   #
#                  wrModeOn             = 0                                   #
#                                                                             #
# Step 16: Wait until completion of BMCA and observe that DUT transmits WRPTP #
#          SLAVE_PRESENT message on port P1 with following parameters. Store  #
#          sourcePortIdentity as SPI1.                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
#                                                                             #
# Step 17: Send WRPTP LOCK message on port T1 with following parameters.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
#                                                                             #
# Step 18: Observe that the DUT transmits WRPTP LOCKED message on port P1 with#
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1002                              #
#                                                                             #
# Step 19: Send WRPTP CALIBRATE message on port T1 with following parameters. #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                  calSendPattern       = FALSE                               #
#                  calRetry             = 0                                   #
#                  calPeriod            = 0                                   #
#                                                                             #
# Step 20: Send WRPTP CALIBRATED message on port T1 with following parameters.#
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                  deltaTx              = 0                                   #
#                  deltaRx              = 0                                   #
#                                                                             #
# Step 21: Observe that the DUT transmits WRPTP CALIBRATE message on port P1  #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                                                                             #
# Step 22: Verify that the DUT transmits WRPTP CALIBRATED message on port P1  #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                  deltaTx              = 0                                   #
#                                                                             #
# (Part 3)                                                                    #
#                                                                             #
# Step 23: Disable WRPTP on port P1.                                          #
#                                                                             #
# Step 24: Configure knownDeltaTx = Default value on port P1.                 #
#                                                                             #
# Step 25: Enable WRPTP on port P1.                                           #
#                                                                             #
# Step 26: Wait for WRPTP te be enabled.                                      #
#                                                                             #
# Step 27: Send periodic WRPTP ANNOUNCE messages on the port T1 with following#
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#                  grandmasterPriority1 = X - 1                               #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                  calibrated           = 1                                   #
#                  wrModeOn             = 0                                   #
#                                                                             #
# Step 28: Wait until completion of BMCA and observe that DUT transmits WRPTP #
#          SLAVE_PRESENT message on port P1 with following parameters. Store  #
#          sourcePortIdentity as SPI1.                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
#                                                                             #
# Step 29: Send WRPTP LOCK message on port T1 with following parameters.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
#                                                                             #
# Step 30: Observe that the DUT transmits WRPTP LOCKED message on port P1 with#
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1002                              #
#                                                                             #
# Step 31: Send WRPTP CALIBRATE message on port T1 with following parameters. #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                  calSendPattern       = FALSE                               #
#                  calRetry             = 0                                   #
#                  calPeriod            = 0                                   #
#                                                                             #
# Step 32: Send WRPTP CALIBRATED message on port T1 with following parameters.#
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                  deltaTx              = 0                                   #
#                  deltaRx              = 0                                   #
#                                                                             #
# Step 33: Observe that the DUT transmits WRPTP CALIBRATE message on port P1  #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                                                                             #
# Step 34: Verify that the DUT transmits WRPTP CALIBRATED message on port P1  #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                  deltaTx              = Default value                       #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          Jul/2018      CERN          Initial                            #
# 1.1          Aug/2018      CERN          Updated timeout for completing BMCA#
# 1.2          Jan/2019      CERN          Modified waiting timeout to receive#
#                                          SLAVE_PRESENT, LOCKED, CALIBRATE   #
#                                          and CALIBRATED messages.           #
# 1.3          Jan/2019      CERN          a) Resolved non-transmission of    #
#                                             messages at background while    #
#                                             waiting for state transitions.  #
# 1.4          Apr/2019      JC.BAU/CERN   Improve execution time             #
#                                                                             #
###############################################################################
# Copyright (c) 2018 - 2019 CERN                                              #
###############################################################################

global dut_log_msg
global tee_log_msg
global tee_rx_errlog
global session_id
global step

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def    "To verify that a WRPTP enabled device with it's port in\
			   PTP Slave sends CALIBRATED message with configured\
			   knownDeltaTx (allowed range: UInteger64)."

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set dut_port_1                     $::dut_port_num_1
set vlan_id                        $::wrptp_vlan_id
set dut_ip                         $::dut_test_port_ip
set dut_dest_ip                    $::dut_test_port_ip
set tee_dest_ip                    $::tee_test_port_ip
set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT

set domain                         $::DEFAULT_DOMAIN
set wrptp_tlv                      $::ORG_EXT_TLV

set ann_msg_id                     $::wrMessageID(ANN_SUFIX)
set slave_present_msg_id           $::wrMessageID(SLAVE_PRESENT)
set lock_msg_id                    $::wrMessageID(LOCK)
set locked_msg_id                  $::wrMessageID(LOCKED)
set calibrate_msg_id               $::wrMessageID(CALIBRATE)
set calibrated_msg_id              $::wrMessageID(CALIBRATED)
set wr_mode_on_msg_id              $::wrMessageID(WR_MODE_ON)
set default_delta_tx               $::wrptp_known_delta_tx
set default_delta_rx               $::wrptp_known_delta_rx

set infinity                       $::INFINITY
set wrconfig                       $::WRCONFIG(WR_MASTER_AND_SLAVE)
set idle                           $::WRPTP_PORT_STATE(IDLE)

set announce_timeout               [expr $::wrptp_announce_timeout + 60]
set exc_timeout_retry              [expr $::wrptp_state_retry * $::wrptp_state_timeout]
set signal_timeout                 $::wrptp_signal_timeout

set bmca_time                      $::wrptp_announce_timeout

set calibrated                     1
set step                           0

########################### END - INITIALIZATION ##############################

wrptp::display_interface_details

wrptp::dut_Initialization

proc cleanup {} {
	global step
	if { $step != 0 } {
		wrptp::dut_cleanup_setup_001
		wrptp::dut_commit_changes
	}
}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

### (Initial Part) ###

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure Network Transport Protocol as IEEE 802.3/Ethernet.       #
#    vi.   Configure default values for Priority1 = X and Priority2 = Y.      #
#   vii.   Enable WRPTP on port P1.                                           #
#  viii.   Configure wrConfig = WR_M_AND_S.                                   #
#    ix.   Configure default values for knownDeltaTx and knownDeltaRx on P1.  #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP1)"

if {!([wrptp::dut_configure_setup_001] && [wrptp::dut_commit_changes])} {

	LOG -msg "$dut_log_msg(INIT_SETUP1_F)"

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP1_F)"\
		-tc_def   $tc_def
	return 0
}

###############################################################################
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(INIT)"

wrptp::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

	if {![WRPTP_UNICAST_PRE_REQUISITE_CONFIGURATION\
						-port_num       $tee_port_1\
						-tee_mac        $tee_mac\
						-tee_ip         $tee_ip\
						-dut_ip         $dut_ip\
						-vlan_id        $vlan_id\
						-dut_mac        tee_dest_mac\
						-timeout        $arp_timeout]} {

		TEE_CLEANUP

		TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
			-tc_def  $tc_def

		return 0
	}

} else {
	set tee_dest_ip             $::PTP_E2E_IP
	set tee_dest_mac            $::PTP_E2E_MAC
}

if {![pltLib::create_filter_wrptp_pkt\
			  -port_num                $tee_port_1\
			  -ether_type              $ptp_ethtype\
			  -vlan_id                 $vlan_id\
			  -ids                     id1]} {

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
		-tc_def  $tc_def

	return 0
}

set filter_id [lindex $id1 end]

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 3 : Send periodic WRPTP ANNOUNCE messages on the port T1 with following#
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#                  grandmasterPriority1 = X - 1                               #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                  calibrated           = 1                                   #
#                  wrModeOn             = 0                                   #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_TX_P1)"

set gm_priority1 [expr $::wrptp_priority1 - 1]

if {![wrptp::send_announce\
			-port_num         $tee_port_1\
			-dest_mac         $tee_dest_mac\
			-src_mac          $tee_mac\
			-count            $infinity\
			-src_ip           $tee_ip\
			-dest_ip          $tee_dest_ip\
			-domain_number    $domain\
			-gm_priority1     $gm_priority1\
			-tlv_type         $wrptp_tlv\
			-message_id       $ann_msg_id\
			-wrconfig         $wrconfig\
			-calibrated       $calibrated]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_TX_P1_F)"\
		-tc_def  $tc_def
	return 0

}

###############################################################################
# Step 4 : Wait until completion of BMCA and observe that DUT transmits WRPTP #
#          SLAVE_PRESENT message on port P1 with following parameters. Store  #
#          sourcePortIdentity as SPI1.                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(BMCA_SLAVE_PRESENT_RX_P1_O)"

set bmca_signal_timeout  [expr $::wrptp_present_timeout_recv + $bmca_time]

if {![wrptp::recv_signal\
		   -port_num                  $tee_port_1\
		   -filter_id                 $filter_id\
		   -domain_number             $domain\
		   -tlv_type                  $wrptp_tlv\
		   -timeout                   $bmca_signal_timeout\
		   -error_reason              error_reason\
		   -recvd_src_port_number     recvd_src_port_number\
		   -recvd_src_clock_identity  recvd_src_clock_identity\
		   -message_id                $slave_present_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SLAVE_PRESENT_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0

}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 5 : Send WRPTP LOCK message on port T1 with following parameters.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCK_TX_P1)"

if {![wrptp::send_signal\
		   -port_num               $tee_port_1\
		   -src_mac                $tee_mac\
		   -dest_mac               $tee_dest_mac\
		   -src_ip                 $tee_ip\
		   -dest_ip                $tee_dest_ip\
		   -domain_number          $domain\
		   -target_port_number     $recvd_src_port_number\
		   -target_clock_identity  $recvd_src_clock_identity\
		   -tlv_type               $wrptp_tlv\
		   -message_id             $lock_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(LOCK_TX_P1_F)"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 6 : Observe that the DUT transmits WRPTP LOCKED message on port P1 with#
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1002                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCKED_RX_P1_O)"

if {![wrptp::recv_signal\
		   -port_num                $tee_port_1\
		   -domain_number           $domain\
		   -tlv_type                $wrptp_tlv\
		   -message_id              $locked_msg_id\
		   -error_reason            error_reason\
		   -timeout                 $::wrptp_locked_timeout_recv\
		   -filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(LOCKED_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 7 : Send WRPTP CALIBRATE message on port T1 with following parameters. #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                  calSendPattern       = FALSE                               #
#                  calRetry             = 0                                   #
#                  calPeriod            = 0                                   #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATE_TX_P1)"

set cal_retry    0
set cal_period   0

if {![wrptp::send_signal\
			 -port_num               $tee_port_1\
			 -src_ip                 $tee_ip\
			 -dest_ip                $tee_dest_ip\
			 -src_mac                $tee_mac\
			 -dest_mac               $tee_dest_mac\
			 -domain_number          $domain\
			 -target_port_number     $recvd_src_port_number\
			 -target_clock_identity  $recvd_src_clock_identity\
			 -tlv_type               $wrptp_tlv\
			 -cal_retry              $cal_retry\
			 -cal_period             $cal_period\
			 -message_id             $calibrate_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(CALIBRATE_TX_P1_F)"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 8 : Send WRPTP CALIBRATED message on port T1 with following parameters.#
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                  deltaTx              = 0                                   #
#                  deltaRx              = 0                                   #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATED_TX_P1)"

set delta_tx    0
set delta_rx    0

if {![wrptp::send_signal\
			 -port_num               $tee_port_1\
			 -src_ip                 $tee_ip\
			 -dest_ip                $tee_dest_ip\
			 -src_mac                $tee_mac\
			 -dest_mac               $tee_dest_mac\
			 -domain_number          $domain\
			 -target_port_number     $recvd_src_port_number\
			 -target_clock_identity  $recvd_src_clock_identity\
			 -tlv_type               $wrptp_tlv\
			 -delta_tx               $delta_tx\
			 -delta_rx               $delta_rx\
			 -message_id             $calibrated_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(CALIBRATED_TX_P1_F)"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 9 : Observe that the DUT transmits WRPTP CALIBRATE message on port P1  #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATE_RX_P1_O)"

if {![wrptp::recv_signal\
		   -port_num                $tee_port_1\
		   -domain_number           $domain\
		   -tlv_type                $wrptp_tlv\
		   -message_id              $calibrate_msg_id\
		   -error_reason            error_reason\
		   -timeout                 $::wrptp_calibration_timeout_recv\
		   -filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(CALIBRATE_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 10: Verify that the DUT transmits WRPTP CALIBRATED message on port P1  #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                  deltaTx              = Default value                       #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATED_RX_P1_V)"

if {![wrptp::recv_signal\
		   -port_num                $tee_port_1\
		   -domain_number           $domain\
		   -tlv_type                $wrptp_tlv\
		   -message_id              $calibrated_msg_id\
		   -timeout                 $::wrptp_calibrated_timeout_recv\
		   -error_reason            error_reason\
		   -delta_tx                $default_delta_tx\
		   -filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL  -reason "$tee_log_msg(CALIBRATED_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

wrptp::stop_announce

# (Part 2)

###############################################################################
# Step 11 : Disable WRPTP on port P1.                                         #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(DISABLE_WRPTP_PORT_P1)"

if {![wrptp::dut_port_wrptp_disable\
			-port_num          $dut_port_1]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(DISABLE_WRPTP_PORT_P1_F)"\
		-tc_def  $tc_def

	reason 0

}

###############################################################################
# Step 12: Configure knownDeltaTx = 0 on port P1.                             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(SET_KNOWN_DELTA_TX_0)"

set known_delta_tx  0

if {![wrptp::dut_set_known_delta_tx\
			-port_num            $dut_port_1\
			-known_delta_tx      $known_delta_tx]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(SET_KNOWN_DELTA_TX_F)"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 13: Enable WRPTP on port P1.                                           #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(ENABLE_WRPTP_PORT_P1)"

if {!([wrptp::dut_port_wrptp_enable\
			-port_num          $dut_port_1] && [wrptp::dut_commit_changes])} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(ENABLE_WRPTP_PORT_P1_F)"\
		-tc_def  $tc_def

	reason 0

}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 14: Wait for WRPTP te be enabled.                                      #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(WAIT_WRPTP_ENABLE)"
LOG -level 0 -msg "Waiting for $::wrptp_enable_time secs"

wrptp::wait_msec [expr $::wrptp_enable_time * 1000]

###############################################################################
# Step 15: Send periodic WRPTP ANNOUNCE messages on the port T1 with following#
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#                  grandmasterPriority1 = X - 1                               #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                  calibrated           = 1                                   #
#                  wrModeOn             = 0                                   #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_TX_P1)"

set gm_priority1 [expr $::wrptp_priority1 - 1]

if {![wrptp::send_announce\
			-port_num         $tee_port_1\
			-dest_mac         $tee_dest_mac\
			-src_mac          $tee_mac\
			-count            $infinity\
			-src_ip           $tee_ip\
			-dest_ip          $tee_dest_ip\
			-domain_number    $domain\
			-gm_priority1     $gm_priority1\
			-tlv_type         $wrptp_tlv\
			-message_id       $ann_msg_id\
			-wrconfig         $wrconfig\
			-calibrated       $calibrated]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_TX_P1_F)"\
		-tc_def  $tc_def
	return 0

}

###############################################################################
# Step 16: Wait until completion of BMCA and observe that DUT transmits WRPTP #
#          SLAVE_PRESENT message on port P1 with following parameters. Store  #
#          sourcePortIdentity as SPI1.                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(BMCA_SLAVE_PRESENT_RX_P1_O)"

set bmca_signal_timeout  [expr $::wrptp_present_timeout_recv + $bmca_time]

if {![wrptp::recv_signal\
		   -port_num                  $tee_port_1\
		   -filter_id                 $filter_id\
		   -domain_number             $domain\
		   -tlv_type                  $wrptp_tlv\
		   -timeout                   $bmca_signal_timeout\
		   -error_reason              error_reason\
		   -recvd_src_port_number     recvd_src_port_number\
		   -recvd_src_clock_identity  recvd_src_clock_identity\
		   -message_id                $slave_present_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SLAVE_PRESENT_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0

}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 17: Send WRPTP LOCK message on port T1 with following parameters.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCK_TX_P1)"

if {![wrptp::send_signal\
		   -port_num               $tee_port_1\
		   -src_mac                $tee_mac\
		   -dest_mac               $tee_dest_mac\
		   -src_ip                 $tee_ip\
		   -dest_ip                $tee_dest_ip\
		   -domain_number          $domain\
		   -target_port_number     $recvd_src_port_number\
		   -target_clock_identity  $recvd_src_clock_identity\
		   -tlv_type               $wrptp_tlv\
		   -message_id             $lock_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(LOCK_TX_P1_F)"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 18: Observe that the DUT transmits WRPTP LOCKED message on port P1 with#
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1002                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCKED_RX_P1_O)"

if {![wrptp::recv_signal\
		   -port_num                $tee_port_1\
		   -domain_number           $domain\
		   -tlv_type                $wrptp_tlv\
		   -message_id              $locked_msg_id\
		   -error_reason            error_reason\
		   -timeout                 $::wrptp_locked_timeout_recv\
		   -filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(LOCKED_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 19: Send WRPTP CALIBRATE message on port T1 with following parameters. #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                  calSendPattern       = FALSE                               #
#                  calRetry             = 0                                   #
#                  calPeriod            = 0                                   #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATE_TX_P1)"

set cal_retry    0
set cal_period   0

if {![wrptp::send_signal\
			 -port_num               $tee_port_1\
			 -src_ip                 $tee_ip\
			 -dest_ip                $tee_dest_ip\
			 -src_mac                $tee_mac\
			 -dest_mac               $tee_dest_mac\
			 -domain_number          $domain\
			 -target_port_number     $recvd_src_port_number\
			 -target_clock_identity  $recvd_src_clock_identity\
			 -tlv_type               $wrptp_tlv\
			 -cal_retry              $cal_retry\
			 -cal_period             $cal_period\
			 -message_id             $calibrate_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(CALIBRATE_TX_P1_F)"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 20: Send WRPTP CALIBRATED message on port T1 with following parameters.#
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                  deltaTx              = 0                                   #
#                  deltaRx              = 0                                   #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATED_TX_P1)"

set delta_tx    0
set delta_rx    0

if {![wrptp::send_signal\
			 -port_num               $tee_port_1\
			 -src_ip                 $tee_ip\
			 -dest_ip                $tee_dest_ip\
			 -src_mac                $tee_mac\
			 -dest_mac               $tee_dest_mac\
			 -domain_number          $domain\
			 -target_port_number     $recvd_src_port_number\
			 -target_clock_identity  $recvd_src_clock_identity\
			 -tlv_type               $wrptp_tlv\
			 -delta_tx               $delta_tx\
			 -delta_rx               $delta_rx\
			 -message_id             $calibrated_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(CALIBRATED_TX_P1_F)"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 21: Observe that the DUT transmits WRPTP CALIBRATE message on port P1  #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATE_RX_P1_O)"

if {![wrptp::recv_signal\
		   -port_num                $tee_port_1\
		   -domain_number           $domain\
		   -tlv_type                $wrptp_tlv\
		   -message_id              $calibrate_msg_id\
		   -error_reason            error_reason\
		   -timeout                 $::wrptp_calibration_timeout_recv\
		   -filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(CALIBRATE_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 22: Verify that the DUT transmits WRPTP CALIBRATED message on port P1  #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                  deltaTx              = 0                                   #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATED_RX_P1_V)"

set deltaTx  0

if {![wrptp::recv_signal\
		   -port_num                $tee_port_1\
		   -domain_number           $domain\
		   -tlv_type                $wrptp_tlv\
		   -message_id              $calibrated_msg_id\
		   -timeout                 $::wrptp_calibrated_timeout_recv\
		   -error_reason            error_reason\
		   -delta_tx                $deltaTx\
		   -filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL  -reason "$tee_log_msg(CALIBRATED_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

wrptp::stop_announce

# (Part 3)

###############################################################################
# Step 23 : Disable WRPTP on port P1.                                         #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(DISABLE_WRPTP_PORT_P1)"

if {![wrptp::dut_port_wrptp_disable\
			-port_num          $dut_port_1]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(DISABLE_WRPTP_PORT_P1_F)"\
		-tc_def  $tc_def

	reason 0

}

###############################################################################
# Step 24: Configure knownDeltaTx = Default value on port P1.                 #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(SET_KNOWN_DELTA_TX_DEFAULT)"

if {![wrptp::dut_set_known_delta_tx\
			-port_num            $dut_port_1\
			-known_delta_tx      $default_delta_tx]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(SET_KNOWN_DELTA_TX_F)"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 25: Enable WRPTP on port P1.                                           #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(ENABLE_WRPTP_PORT_P1)"

if {!([wrptp::dut_port_wrptp_enable\
			-port_num          $dut_port_1] && [wrptp::dut_commit_changes])} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(ENABLE_WRPTP_PORT_P1_F)"\
		-tc_def  $tc_def

	reason 0

}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 26: Wait for WRPTP te be enabled.                                      #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(WAIT_WRPTP_ENABLE)"
LOG -level 0 -msg "Waiting for $::wrptp_enable_time secs"

wrptp::wait_msec [expr $::wrptp_enable_time * 1000]

###############################################################################
# Step 27: Send periodic WRPTP ANNOUNCE messages on the port T1 with following#
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#                  grandmasterPriority1 = X - 1                               #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                  calibrated           = 1                                   #
#                  wrModeOn             = 0                                   #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_TX_P1)"

set gm_priority1 [expr $::wrptp_priority1 - 1]

if {![wrptp::send_announce\
			-port_num         $tee_port_1\
			-dest_mac         $tee_dest_mac\
			-src_mac          $tee_mac\
			-count            $infinity\
			-src_ip           $tee_ip\
			-dest_ip          $tee_dest_ip\
			-domain_number    $domain\
			-gm_priority1     $gm_priority1\
			-tlv_type         $wrptp_tlv\
			-message_id       $ann_msg_id\
			-wrconfig         $wrconfig\
			-calibrated       $calibrated]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_TX_P1_F)"\
		-tc_def  $tc_def
	return 0

}

###############################################################################
# Step 28: Wait until completion of BMCA and observe that DUT transmits WRPTP #
#          SLAVE_PRESENT message on port P1 with following parameters. Store  #
#          sourcePortIdentity as SPI1.                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(BMCA_SLAVE_PRESENT_RX_P1_O)"

set bmca_signal_timeout  [expr $::wrptp_present_timeout_recv  + $bmca_time]

if {![wrptp::recv_signal\
		   -port_num                  $tee_port_1\
		   -filter_id                 $filter_id\
		   -domain_number             $domain\
		   -tlv_type                  $wrptp_tlv\
		   -timeout                   $bmca_signal_timeout\
		   -error_reason              error_reason\
		   -recvd_src_port_number     recvd_src_port_number\
		   -recvd_src_clock_identity  recvd_src_clock_identity\
		   -message_id                $slave_present_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SLAVE_PRESENT_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0

}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 29: Send WRPTP LOCK message on port T1 with following parameters.      #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCK_TX_P1)"

if {![wrptp::send_signal\
		   -port_num               $tee_port_1\
		   -src_mac                $tee_mac\
		   -dest_mac               $tee_dest_mac\
		   -src_ip                 $tee_ip\
		   -dest_ip                $tee_dest_ip\
		   -domain_number          $domain\
		   -target_port_number     $recvd_src_port_number\
		   -target_clock_identity  $recvd_src_clock_identity\
		   -tlv_type               $wrptp_tlv\
		   -message_id             $lock_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(LOCK_TX_P1_F)"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 30: Observe that the DUT transmits WRPTP LOCKED message on port P1 with#
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1002                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCKED_RX_P1_O)"

if {![wrptp::recv_signal\
		   -port_num                $tee_port_1\
		   -domain_number           $domain\
		   -tlv_type                $wrptp_tlv\
		   -message_id              $locked_msg_id\
		   -error_reason            error_reason\
		   -timeout                 $::wrptp_locked_timeout_recv\
		   -filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(LOCKED_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 31: Send WRPTP CALIBRATE message on port T1 with following parameters. #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                  calSendPattern       = FALSE                               #
#                  calRetry             = 0                                   #
#                  calPeriod            = 0                                   #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATE_TX_P1)"

set cal_retry    0
set cal_period   0

if {![wrptp::send_signal\
			 -port_num               $tee_port_1\
			 -src_ip                 $tee_ip\
			 -dest_ip                $tee_dest_ip\
			 -src_mac                $tee_mac\
			 -dest_mac               $tee_dest_mac\
			 -domain_number          $domain\
			 -target_port_number     $recvd_src_port_number\
			 -target_clock_identity  $recvd_src_clock_identity\
			 -tlv_type               $wrptp_tlv\
			 -cal_retry              $cal_retry\
			 -cal_period             $cal_period\
			 -message_id             $calibrate_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(CALIBRATE_TX_P1_F)"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 32: Send WRPTP CALIBRATED message on port T1 with following parameters.#
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                  deltaTx              = 0                                   #
#                  deltaRx              = 0                                   #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATED_TX_P1)"

set delta_tx    0
set delta_rx    0

if {![wrptp::send_signal\
			 -port_num               $tee_port_1\
			 -src_ip                 $tee_ip\
			 -dest_ip                $tee_dest_ip\
			 -src_mac                $tee_mac\
			 -dest_mac               $tee_dest_mac\
			 -domain_number          $domain\
			 -target_port_number     $recvd_src_port_number\
			 -target_clock_identity  $recvd_src_clock_identity\
			 -tlv_type               $wrptp_tlv\
			 -delta_tx               $delta_tx\
			 -delta_rx               $delta_rx\
			 -message_id             $calibrated_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(CALIBRATED_TX_P1_F)"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 33: Observe that the DUT transmits WRPTP CALIBRATE message on port P1  #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATE_RX_P1_O)"

if {![wrptp::recv_signal\
		   -port_num                $tee_port_1\
		   -domain_number           $domain\
		   -tlv_type                $wrptp_tlv\
		   -message_id              $calibrate_msg_id\
		   -error_reason            error_reason\
		   -timeout                 $::wrptp_calibration_timeout_recv\
		   -filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(CALIBRATE_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 34: Verify that the DUT transmits WRPTP CALIBRATED message on port P1  #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                  deltaTx              = Default value                       #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATED_RX_P1_V)"

if {![wrptp::recv_signal\
		   -port_num                $tee_port_1\
		   -domain_number           $domain\
		   -tlv_type                $wrptp_tlv\
		   -message_id              $calibrated_msg_id\
		   -timeout                 $::wrptp_calibrated_timeout_recv\
		   -error_reason            error_reason\
		   -delta_tx                $default_delta_tx\
		   -filter_id               $filter_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL  -reason "$tee_log_msg(CALIBRATED_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0
}

TEE_CLEANUP

TC_CLEAN_AND_PASS -reason "DUT in PTP Slave mode sends CALIBRATED message with configured\
						   knownDeltaTx."\
	-tc_def  $tc_def

return 1

############################### END OF TESTCASE ################################
