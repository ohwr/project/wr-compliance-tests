#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_wrptp_wsmg_017                                  #
# Test Case Version : 1.3                                                     #
# Component Name    : ATTEST WRPTP CONFORMANCE TEST SUITE                     #
# Module Name       : WRPTP State Machine Group (WSMG)                        #
#                                                                             #
# Title             : Storing of otherPortDeltaTx, otherPortDeltaRx,          #
#                     otherPortCalPeriod, otherPortCalRetry and               #
#                     otherPortCalSendPattern in WR Master                    #
#                                                                             #
# Purpose           : To verify that a WRPTP enabled device with it's port in #
#                     PTP Master stores otherPortDeltaTx, otherPortDeltaRx,   #
#                     otherPortCalPeriod, otherPortCalRetry and               #
#                     otherPortCalSendPattern received in CALIBRATE message.  #
#                                                                             #
# Reference         : White Rabbit Specification v2.0 July 2011,              #
#                     Clause 6.3.1.2.20 - 6.3.1.2.24 Pages 22                 #
#                                                                             #
# Conformance Type  : MUST                                                    #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 001                                                     #
# Test Topology     : 1                                                       #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode = One-step/Two-step> |           #
#           |          <Transport Protocol = IEEE 802.3/Ethernet> |           #
#           |            <Configure Priority1 = X, Priority2 = Y> | P1        #
#           |                                      <Enable WRPTP> | P1        #
#           |                   <Configure wrConfig = WR_M_AND_S> | P1        #
#           |      <Configure default values for knownDeltaTx and | P1        #
#           |                                       knownDeltaRx> |           #
#           |                                                     |           #
#           |                 WRPTP ANNOUNCE [messageType = 0x0B, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x2000] |           #
#           |                         {sourcePortIdentity = SPI1} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP SLAVE_PRESENT [messageType = 0x0C,            |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1000]             |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                     WRPTP LOCK [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1001] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP LOCKED [messageType = 0x0C,                   |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1002]             |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                WRPTP CALIBRATE [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1003] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |               WRPTP CALIBRATED [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1004] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP CALIBRATE [messageType = 0x0C,                |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1003,             |           #
#           | calSendPattern = FALSE, calRetry = 0,               |           #
#           | calPeriod = 0]                                      |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           | WRPTP CALIBRATED [messageType = 0x0C,               |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1004,             |           #
#           | deltaTx = 0, deltaRx = 0]                           |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                        <Check otherPortDeltaTx = 0> | P1        #
#           |                                                     |           #
#           |                        <Check otherPortDeltaRx = 0> | P1        #
#           |                                                     |           #
#           |             <Check otherPortCalSendPattern = FALSE> | P1        #
#           |                                                     |           #
#           |                      <Check otherPortCalPeriod = 0> | P1        #
#           |                                                     |           #
#           |                       <Check otherPortCalRetry = 0> | P1        #
#           |                                                     |           #
#           |                                     <Disable WRPTP> | P1        #
#           |                                                     |           #
#           |                                      <Enable WRPTP> | P1        #
#           |                                                     |           #
#           |           <Wait for WRPTP to be enabled>            |           #
#           |                                                     |           #
#           |                 WRPTP ANNOUNCE [messageType = 0x0B, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x2000] |           #
#           |                         {sourcePortIdentity = SPI1} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP SLAVE_PRESENT [messageType = 0x0C,            |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1000]             |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                     WRPTP LOCK [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1001] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP LOCKED [messageType = 0x0C,                   |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1002]             |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                WRPTP CALIBRATE [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1003] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |               WRPTP CALIBRATED [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1004] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP CALIBRATE [messageType = 0x0C,                |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1003,             |           #
#           | calSendPattern = TRUE, calRetry = 3,                |           #
#           | calPeriod = 3000us]                                 |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           | WRPTP CALIBRATED [messageType = 0x0C,               |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1004,             |           #
#           | deltaTx = 226214, deltaRx = 226758]                 |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                   <Check otherPortDeltaTx = 226214> | P1        #
#           |                                                     |           #
#           |                   <Check otherPortDeltaRx = 226758> | P1        #
#           |                                                     |           #
#           |              <Check otherPortCalSendPattern = TRUE> | P1        #
#           |                                                     |           #
#           |                 <Check otherPortCalPeriod = 3000us> | P1        #
#           |                                                     |           #
#           |                       <Check otherPortCalRetry = 3> | P1        #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     TEE       = Test Execution Engine                                       #
#     DUT       = Device Under Test                                           #
#     T1        = TEE's port 1                                                #
#     P1        = DUT's port 1                                                #
#     PTP       = Precision Time Protocol                                     #
#     WRPTP     = White Rabbit Precision Time Protocol                        #
#     OC        = Ordinary Clock                                              #
#     BC        = Boundary Clock                                              #
#     DN1       = Domain Number 1                                             #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure Network Transport Protocol as IEEE 802.3/Ethernet.       #
#    vi.   Configure default values for Priority1 = X and Priority2 = Y.      #
#   vii.   Enable WRPTP on port P1.                                           #
#  viii.   Configure wrConfig = WR_M_AND_S.                                   #
#    ix.   Configure default values for knownDeltaTx and knownDeltaRx on P1.  #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Observe that DUT transmits WRPTP ANNOUNCE message on the port P1   #
#          with following parameters and store sourcePortIdentity as SPI1.    #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                                                                             #
# Step 4 : Send WRPTP SLAVE_PRESENT message on port T1 with following         #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
#                                                                             #
# Step 5 : Observe that DUT transmits WRPTP LOCK message on port P1 with      #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
#                                                                             #
# Step 6 : Send WRPTP LOCKED message on port T1 with following parameters.    #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1002                              #
#                                                                             #
# Step 7 : Observe that DUT transmits WRPTP CALIBRATE message on port P1 with #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                                                                             #
# Step 8 : Observe that DUT transmits WRPTP CALIBRATED message on port P1 with#
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                                                                             #
# Step 9 : Send WRPTP CALIBRATE message on port T1 with following parameters. #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                  calSendPattern       = FALSE                               #
#                  calRetry             = 0                                   #
#                  calPeriod            = 0                                   #
#                                                                             #
# Step 10: Send WRPTP CALIBRATED message on port T1 with following parameters #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                  deltaTx              = 0                                   #
#                  deltaRx              = 0                                   #
#                                                                             #
# Step 11: Verify otherPortDeltaTx = 0 on port P1.                            #
#                                                                             #
# Step 12: Verify otherPortDeltaRx = 0 on port P1.                            #
#                                                                             #
# Step 13: Verify otherPortCalSendPattern = FALSE on port P1.                 #
#                                                                             #
# Step 14: Verify otherPortCalPeriod = 0 on port P1.                          #
#                                                                             #
# Step 15: Verify otherPortCalRetry = 0 on port P1.                           #
#                                                                             #
# (Part 2)                                                                    #
#                                                                             #
# Step 16: Disable WRPTP on port P1.                                          #
#                                                                             #
# Step 17: Enable WRPTP on port P1.                                           #
#                                                                             #
# Step 18: Wait for WRPTP te be enabled.                                      #
#                                                                             #
# Step 19: Observe that DUT transmits WRPTP ANNOUNCE message on the port P1   #
#          with following parameters and store sourcePortIdentity as SPI1.    #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                                                                             #
# Step 20: Send WRPTP SLAVE_PRESENT message on port T1 with following         #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
#                                                                             #
# Step 21: Observe that DUT transmits WRPTP LOCK message on port P1 with      #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
#                                                                             #
# Step 22: Send WRPTP LOCKED message on port T1 with following parameters.    #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1002                              #
#                                                                             #
# Step 23: Observe that DUT transmits WRPTP CALIBRATE message on port P1 with #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                                                                             #
# Step 24: Observe that DUT transmits WRPTP CALIBRATED message on port P1 with#
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                                                                             #
# Step 25: Send WRPTP CALIBRATE message on port T1 with following parameters. #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                  calSendPattern       = TRUE                                #
#                  calRetry             = 3                                   #
#                  calPeriod            = 3000us                              #
#                                                                             #
# Step 26: Send WRPTP CALIBRATED message on port T1 with following parameters #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                  deltaTx              = 226214                              #
#                  deltaRx              = 226758                              #
#                                                                             #
# Step 27: Verify otherPortDeltaTx = 226214 on port P1.                       #
#                                                                             #
# Step 28: Verify otherPortDeltaRx = 226758 on port P1.                       #
#                                                                             #
# Step 29: Verify otherPortCalSendPattern = TRUE on port P1.                  #
#                                                                             #
# Step 30: Verify otherPortCalPeriod = 3000us on port P1.                     #
#                                                                             #
# Step 31: Verify otherPortCalRetry = 3 on port P1.                           #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          Jul/2018      CERN          Initial                            #
# 1.1          Sep/2018      CERN          Added steps to verify              #
#                                          otherPortCalSendPattern,           #
#                                          otherPortCalPeriod and             #
#                                          otherPortCalRetry                  #
# 1.2          Dec/2018      CERN          Modified waiting timeout to receive#
#                                          LOCK, CALIBRATE and CALIBRATED     #
#                                          messages.                          #
# 1.3          Jan/2019      CERN          a) Resolved non-transmission of    #
#                                             messages at background while    #
#                                             waiting for state transitions.  #
# 1.4          Apr/2019      JC.BAU/CERN   Improve execution time             #
#                                                                             #
###############################################################################
# Copyright (c) 2018 - 2019 CERN                                              #
###############################################################################

global dut_log_msg
global tee_log_msg
global tee_rx_errlog
global session_id
global step

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def    "To verify that a WRPTP enabled device with it's port in\
			   PTP Master stores otherPortDeltaTx, otherPortDeltaRx,\
			   otherPortCalPeriod, otherPortCalRetry and\
			   otherPortCalSendPattern received in CALIBRATE message."

########################### START - INITIALIZATION ############################

set tee_port_1               $::tee_port_num_1
set dut_port_1               $::dut_port_num_1
set dut_ip                   $::dut_test_port_ip
set vlan_id                  $::wrptp_vlan_id
set dut_dest_ip              $::dut_test_port_ip
set tee_dest_ip              $::tee_test_port_ip
set ptp_comm_model           $::ptp_comm_model
set arp_timeout              $::ARP_TIMEOUT
set ptp_ethtype              $::PTP_ETHTYPE

set announce_timeout         [expr $::wrptp_announce_timeout + 60]
set signal_timeout           $::wrptp_signal_timeout

set wrptp_tlv                $::ORG_EXT_TLV
set domain                   $::DEFAULT_DOMAIN

set ann_msg_id               $::wrMessageID(ANN_SUFIX)
set slave_present_msg_id     $::wrMessageID(SLAVE_PRESENT)
set lock_msg_id              $::wrMessageID(LOCK)
set locked_msg_id            $::wrMessageID(LOCKED)
set calibrate_msg_id         $::wrMessageID(CALIBRATE)
set calibrated_msg_id        $::wrMessageID(CALIBRATED)

set cal_send_pattern_true    1
set cal_send_pattern_false   0

set dut_cal_send_pattern_true    TRUE
set dut_cal_send_pattern_false   FALSE

set step                     0
global step

########################### END - INITIALIZATION ##############################

wrptp::display_interface_details

wrptp::dut_Initialization

proc cleanup {} {
	global step
	if { $step != 0 } {
		wrptp::dut_cleanup_setup_001
		wrptp::dut_commit_changes
	}
}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

### (Initial Part) ###

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure Network Transport Protocol as IEEE 802.3/Ethernet.       #
#    vi.   Configure default values for Priority1 = X and Priority2 = Y.      #
#   vii.   Enable WRPTP on port P1.                                           #
#  viii.   Configure wrConfig = WR_M_AND_S.                                   #
#    ix.   Configure default values for knownDeltaTx and knownDeltaRx on P1.  #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP1)"

if {!( [wrptp::dut_configure_setup_001] && [wrptp::dut_commit_changes]) } {

	LOG -msg "$dut_log_msg(INIT_SETUP1_F)"

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP1_F)"\
		-tc_def  $tc_def
	return 0
}

###############################################################################
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(INIT)"

wrptp::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

	if {![WRPTP_UNICAST_PRE_REQUISITE_CONFIGURATION\
						-port_num       $tee_port_1\
						-tee_mac        $tee_mac\
						-tee_ip         $tee_ip\
						-dut_ip         $dut_ip\
						-vlan_id        $vlan_id\
						-dut_mac        tee_dest_mac\
						-timeout        $arp_timeout]} {

		TEE_CLEANUP

		TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
			-tc_def  $tc_def

		return 0
	}

} else {
	set tee_dest_ip             $::PTP_E2E_IP
	set tee_dest_mac            $::PTP_E2E_MAC
}

if {![pltLib::create_filter_wrptp_pkt\
			  -port_num                $tee_port_1\
			  -ether_type              $ptp_ethtype\
			  -vlan_id                 $vlan_id\
			  -ids                     id1]} {

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
		-tc_def  $tc_def

	return 0
}

set filter_id [lindex $id1 end]

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 3 : Observe that DUT transmits WRPTP ANNOUNCE message on the port P1   #
#          with following parameters and store sourcePortIdentity as SPI1.    #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![wrptp::recv_announce\
		   -port_num                  $tee_port_1\
		   -filter_id                 $filter_id\
		   -domain_number             $domain\
		   -timeout                   $announce_timeout\
		   -tlv_type                  $wrptp_tlv\
		   -message_id                $ann_msg_id\
		   -error_reason              error_reason\
		   -recvd_src_port_number     recvd_src_port_number\
		   -recvd_src_clock_identity  recvd_src_clock_identity]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANNOUNCE_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0

}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 4 : Send WRPTP SLAVE_PRESENT message on port T1 with following         #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(SLAVE_PRESENT_TX_P1)"

if {![wrptp::send_signal\
		   -port_num               $tee_port_1\
		   -dest_mac               $tee_dest_mac\
		   -src_mac                $tee_mac\
		   -src_ip                 $tee_ip\
		   -dest_ip                $tee_dest_ip\
		   -domain_number          $domain\
		   -target_port_number     $recvd_src_port_number\
		   -target_clock_identity  $recvd_src_clock_identity\
		   -tlv_type               $wrptp_tlv\
		   -message_id             $slave_present_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(SLAVE_PRESENT_TX_P1_F)"\
		-tc_def  $tc_def
	return 0

}

###############################################################################
# Step 5 : Observe that DUT transmits WRPTP LOCK message on port P1 with      #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCK_RX_P1_O)"

if {![wrptp::recv_signal\
			-port_num         $tee_port_1\
			-filter_id        $filter_id\
			-domain_number    $domain\
			-timeout          $::wrptp_m_lock_timeout_recv\
			-error_reason     error_reason\
			-tlv_type         $wrptp_tlv\
			-message_id       $lock_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(LOCK_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0

}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 6 : Send WRPTP LOCKED message on port T1 with following parameters.    #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1002                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCKED_TX_P1)"

if {![wrptp::send_signal\
			 -port_num               $tee_port_1\
			 -dest_mac               $tee_dest_mac\
			 -src_mac                $tee_mac\
			 -src_ip                 $tee_ip\
			 -dest_ip                $tee_dest_ip\
			 -domain_number          $domain\
			 -target_port_number     $recvd_src_port_number\
			 -target_clock_identity  $recvd_src_clock_identity\
			 -tlv_type               $wrptp_tlv\
			 -message_id             $locked_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(LOCKED_TX_P1_F)"\
		-tc_def  $tc_def

	return 0
}

###############################################################################
# Step 7 : Observe that DUT transmits WRPTP CALIBRATE message on port P1 with #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATE_RX_P1_O)"

if {![wrptp::recv_signal\
			-port_num           $tee_port_1\
			-filter_id          $filter_id\
			-domain_number      $domain\
			-timeout            $::wrptp_calibration_timeout_recv\
			-error_reason       error_reason\
			-tlv_type           $wrptp_tlv\
			-message_id         $calibrate_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(CALIBRATE_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 8 : Observe that DUT transmits WRPTP CALIBRATED message on port P1 with#
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                                                                             #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATED_RX_P1_O)"

if {![wrptp::recv_signal\
			-port_num          $tee_port_1\
			-filter_id         $filter_id\
			-domain_number     $domain\
			-timeout           $::wrptp_calibrated_timeout_recv\
			-error_reason      error_reason\
			-tlv_type          $wrptp_tlv\
			-message_id        $calibrated_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(CALIBRATED_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0

}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 9 : Send WRPTP CALIBRATE message on port T1 with following parameters. #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                  calSendPattern       = FALSE                               #
#                  calRetry             = 0                                   #
#                  calPeriod            = 0                                   #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATE_TX_P1)"

set cal_retry    0
set cal_period   0

if {![wrptp::send_signal\
			-port_num               $tee_port_1\
			-src_mac                $tee_mac\
			-dest_mac               $tee_dest_mac\
			-src_ip                 $tee_ip\
			-dest_ip                $tee_dest_ip\
			-domain_number          $domain\
			-target_port_number     $recvd_src_port_number\
			-target_clock_identity  $recvd_src_clock_identity\
			-tlv_type               $wrptp_tlv\
			-cal_retry              $cal_retry\
			-cal_period             $cal_period\
			-message_id             $calibrate_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(CALIBRATE_TX_P1_F)"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 10: Send WRPTP CALIBRATED message on port T1 with following parameters #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                  deltaTx              = 0                                   #
#                  deltaRx              = 0                                   #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATED_TX_P1)"

set delta_tx    0
set delta_rx    0

if {![wrptp::send_signal\
		   -port_num               $tee_port_1\
		   -src_mac                $tee_mac\
		   -dest_mac               $tee_dest_mac\
		   -src_ip                 $tee_ip\
		   -dest_ip                $tee_dest_ip\
		   -domain_number          $domain\
		   -target_port_number     $recvd_src_port_number\
		   -target_clock_identity  $recvd_src_clock_identity\
		   -tlv_type               $wrptp_tlv\
		   -delta_tx               $delta_tx\
		   -delta_rx               $delta_rx\
		   -message_id             $calibrated_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(CALIBRATED_TX_P1_F)"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 11: Verify otherPortDeltaTx = 0 on port P1.                            #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(CHECK_OTHER_PORT_DELTA_TX_P1_V)"

if {![wrptp::dut_check_other_port_delta_tx\
			 -port_num               $dut_port_1\
			 -other_port_delta_tx    $delta_tx]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL  -reason "DUT does not store otherPortDeltaTx\
								received in CALIBRATE message."\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 12: Verify otherPortDeltaRx = 0 on port P1.                            #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(CHECK_OTHER_PORT_DELTA_RX_P1_V)"

if {![wrptp::dut_check_other_port_delta_rx\
			 -port_num               $dut_port_1\
			 -other_port_delta_rx    $delta_rx]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL  -reason "DUT does not store otherPortDeltaRx received\
								in CALIBRATE message."\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 13: Verify otherPortCalSendPattern = FALSE on port P1.                 #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(CHECK_OTHER_PORT_CAL_SEND_PATTERN_P1_V)"

if {![wrptp::dut_check_other_port_cal_send_pattern\
			 -port_num                      $dut_port_1\
			 -other_port_cal_send_pattern   $dut_cal_send_pattern_false]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL  -reason "DUT does not store otherPortCalSendPattern received\
								in CALIBRATE message."\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 14: Verify otherPortCalPeriod = 0 on port P1.                          #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(CHECK_OTHER_PORT_CAL_PERIOD_P1_V)"

if {![wrptp::dut_check_other_port_cal_period\
			 -port_num               $dut_port_1\
			 -other_port_cal_period  $cal_period]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL  -reason "DUT does not store otherPortCalPeriod received\
								in CALIBRATE message."\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 15: Verify otherPortCalRetry = 0 on port P1.                           #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(CHECK_OTHER_PORT_CAL_RETRY_P1_V)"

if {![wrptp::dut_check_other_port_cal_retry\
			 -port_num               $dut_port_1\
			 -other_port_cal_retry   $cal_retry]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL  -reason "DUT does not store otherPortCalRetry received\
								in CALIBRATE message."\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 16: Disable WRPTP on port P1.                                          #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(DISABLE_WRPTP_PORT_P1)"

if {![wrptp::dut_port_wrptp_disable\
			-port_num          $dut_port_1]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(DISABLE_WRPTP_PORT_P1_F)"\
		-tc_def  $tc_def

	reason 0

}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 17: Enable WRPTP on port P1.                                           #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(ENABLE_WRPTP_PORT_P1)"

if {!([wrptp::dut_port_wrptp_enable\
			-port_num          $dut_port_1] && [wrptp::dut_commit_changes])} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$dut_log_msg(ENABLE_WRPTP_PORT_P1_F)"\
		-tc_def  $tc_def

	reason 0

}

###############################################################################
# Step 18: Wait for WRPTP te be enabled.                                      #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(WAIT_WRPTP_ENABLE)"
LOG -level 0 -msg "Waiting for $::wrptp_enable_time secs"

wrptp::wait_msec [expr $::wrptp_enable_time * 1000]

###############################################################################
# Step 19: Observe that DUT transmits WRPTP ANNOUNCE message on the port P1   #
#          with following parameters and store sourcePortIdentity as SPI1.    #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![wrptp::recv_announce\
			-port_num                    $tee_port_1\
			-filter_id                   $filter_id\
			-domain_number               $domain\
			-tlv_type                    $wrptp_tlv\
			-timeout                     $announce_timeout\
			-message_id                  $ann_msg_id\
			-error_reason                error_reason\
			-recvd_src_port_number       recvd_src_port_number\
			-recvd_src_clock_identity    recvd_src_clock_identity\
			-timeout                     $announce_timeout]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	reason 0

}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 20: Send WRPTP SLAVE_PRESENT message on port T1 with following         #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(SLAVE_PRESENT_TX_P1)"

if {![wrptp::send_signal\
		   -port_num               $tee_port_1\
		   -src_mac                $tee_mac\
		   -dest_mac               $tee_dest_mac\
		   -src_ip                 $tee_ip\
		   -dest_ip                $tee_dest_ip\
		   -domain_number          $domain\
		   -target_port_number     $recvd_src_port_number\
		   -target_clock_identity  $recvd_src_clock_identity\
		   -tlv_type               $wrptp_tlv\
		   -message_id             $slave_present_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SLAVE_PRESENT_TX_P1_F)"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 21: Observe that DUT transmits WRPTP LOCK message on port P1 with      #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCK_RX_P1_O)"

if {![wrptp::recv_signal\
			-port_num          $tee_port_1\
			-filter_id         $filter_id\
			-domain_number     $domain\
			-timeout           $::wrptp_m_lock_timeout_recv\
			-error_reason      error_reason\
			-tlv_type          $wrptp_tlv\
			-message_id        $lock_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(LOCK_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0

}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 22: Send WRPTP LOCKED message on port T1 with following parameters.    #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1002                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCKED_TX_P1)"

if {![wrptp::send_signal\
		   -port_num               $tee_port_1\
		   -src_mac                $tee_mac\
		   -dest_mac               $tee_dest_mac\
		   -src_ip                 $tee_ip\
		   -dest_ip                $tee_dest_ip\
		   -domain_number          $domain\
		   -target_port_number     $recvd_src_port_number\
		   -target_clock_identity  $recvd_src_clock_identity\
		   -tlv_type               $wrptp_tlv\
		   -message_id             $locked_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(LOCKED_TX_P1_F)"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 23: Observe that DUT transmits WRPTP CALIBRATE message on port P1 with #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATE_RX_P1_O)"

if {![wrptp::recv_signal\
			-port_num          $tee_port_1\
			-filter_id         $filter_id\
			-domain_number     $domain\
			-timeout           $::wrptp_calibration_timeout_recv\
			-error_reason      error_reason\
			-tlv_type          $wrptp_tlv\
			-message_id        $calibrate_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(CALIBRATE_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 24: Observe that DUT transmits WRPTP CALIBRATED message on port P1 with#
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATED_RX_P1_O)"

if {![wrptp::recv_signal\
			-port_num          $tee_port_1\
			-filter_id         $filter_id\
			-domain_number     $domain\
			-timeout           $::wrptp_calibrated_timeout_recv\
			-error_reason      error_reason\
			-tlv_type          $wrptp_tlv\
			-message_id        $calibrated_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT -reason "$tee_log_msg(CALIBRATED_RX_P1_F) $error_reason"\
		-tc_def  $tc_def

	return 0

}

wrptp::reset_capture_stats\
	-port_num           $tee_port_1

###############################################################################
# Step 25: Send WRPTP CALIBRATE message on port T1 with following parameters. #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1003                              #
#                  calSendPattern       = TRUE                                #
#                  calRetry             = 3                                   #
#                  calPeriod            = 3000us                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATE_TX_P1)"

set cal_retry    3
set cal_period   3000

if {![wrptp::send_signal\
		   -port_num               $tee_port_1\
		   -src_mac                $tee_mac\
		   -dest_mac               $tee_dest_mac\
		   -src_ip                 $tee_ip\
		   -dest_ip                $tee_dest_ip\
		   -domain_number          $domain\
		   -target_port_number     $recvd_src_port_number\
		   -target_clock_identity  $recvd_src_clock_identity\
		   -tlv_type               $wrptp_tlv\
		   -cal_send_pattern       $cal_send_pattern_true\
		   -cal_retry              $cal_retry\
		   -cal_period             $cal_period\
		   -message_id             $calibrate_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(CALIBRATE_TX_P1_F)"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 26: Send WRPTP CALIBRATED message on port T1 with following parameters #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1004                              #
#                  deltaTx              = 226214                              #
#                  deltaRx              = 226758                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(CALIBRATED_TX_P1)"

set delta_tx    226214
set delta_rx    226758

if {![wrptp::send_signal\
		   -port_num               $tee_port_1\
		   -src_mac                $tee_mac\
		   -dest_mac               $tee_dest_mac\
		   -src_ip                 $tee_ip\
		   -dest_ip                $tee_dest_ip\
		   -domain_number          $domain\
		   -target_port_number     $recvd_src_port_number\
		   -target_clock_identity  $recvd_src_clock_identity\
		   -tlv_type               $wrptp_tlv\
		   -delta_tx               $delta_tx\
		   -delta_rx               $delta_rx\
		   -message_id             $calibrated_msg_id]} {

	TEE_CLEANUP

	TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(CALIBRATED_TX_P1_F)"\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 27: Verify otherPortDeltaTx = 226214 on port P1.                       #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(CHECK_OTHER_PORT_DELTA_TX_P1_V)"

if {![wrptp::dut_check_other_port_delta_tx\
			  -port_num                 $dut_port_1\
			  -other_port_delta_tx      $delta_tx]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL  -reason "DUT does not store otherPortDeltaTx\
								 received in CALIBRATE message."\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 28: Verify otherPortDeltaRx = 226758 on port P1.                       #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(CHECK_OTHER_PORT_DELTA_RX_P1_V)"

if {![wrptp::dut_check_other_port_delta_rx\
			  -port_num                 $dut_port_1\
			  -other_port_delta_rx      $delta_rx]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL  -reason "DUT does not store otherPortDeltaRx\
								 received in CALIBRATE message."\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 29: Verify otherPortCalSendPattern = TRUE on port P1.                  #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(CHECK_OTHER_PORT_CAL_SEND_PATTERN_P1_V)"

if {![wrptp::dut_check_other_port_cal_send_pattern\
			 -port_num                      $dut_port_1\
			 -other_port_cal_send_pattern   $dut_cal_send_pattern_true]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL  -reason "DUT does not store otherPortCalSendPattern received\
								in CALIBRATE message."\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 30: Verify otherPortCalPeriod = 3000us on port P1.                     #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(CHECK_OTHER_PORT_CAL_PERIOD_P1_V)"

if {![wrptp::dut_check_other_port_cal_period\
			 -port_num               $dut_port_1\
			 -other_port_cal_period  $cal_period]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL  -reason "DUT does not store otherPortCalPeriod received\
								in CALIBRATE message."\
		-tc_def  $tc_def

	return 0

}

###############################################################################
# Step 31: Verify otherPortCalRetry = 3 on port P1.                           #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(CHECK_OTHER_PORT_CAL_RETRY_P1_V)"

if {![wrptp::dut_check_other_port_cal_retry\
			 -port_num               $dut_port_1\
			 -other_port_cal_retry   $cal_retry]} {

	TEE_CLEANUP

	TC_CLEAN_AND_FAIL  -reason "DUT does not store otherPortCalRetry received\
								in CALIBRATE message."\
		-tc_def  $tc_def

	return 0

}

TEE_CLEANUP

TC_CLEAN_AND_PASS -reason "DUT stores otherPortDeltaTx,\
						   otherPortDeltaRx,otherPortCalPeriod,\
						   otherPortCalRetry and otherPortCalSendPattern\
						   received in CALIBRATE message."\
	-tc_def  $tc_def

return 1

################################ END OF TESTCASE ##############################
