#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_wrptp_wcg_002                                   #
# Test Case Version : 1.5                                                     #
# Component Name    : ATTEST WRPTP CONFORMANCE TEST SUITE                     #
# Module Name       : WRPTP Configuration Group (WCG)                         #
#                                                                             #
# Title             : wrConfig                                                #
#                                                                             #
# Purpose           : To verify that a WRPTP enabled device supports to       #
#                     configure wrConfig data set member (allowable values:   #
#                     WR_S_ONLY, WR_M_ONLY and WR_M_AND_S).                   #
#                                                                             #
# Reference         : White Rabbit Specification v2.0 July 2011, Clause 6.3   #
#                     Page 16                                                 #
#                                                                             #
# Conformance Type  : MUST                                                    #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 001                                                     #
# Test Topology     : 3                                                       #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode = One-step/Two-step> |           #
#           |          <Transport Protocol = IEEE 802.3/Ethernet> |           #
#           |            <Configure Priority1 = X, Priority2 = Y> | P1        #
#           |                                      <Enable WRPTP> | P1        #
#           |                   <Configure wrConfig = WR_M_AND_S> | P1        #
#           |      <Configure default values for knownDeltaTx and | P1        #
#           |                                       knownDeltaRx> |           #
#           |                                                     |           #
#           |                 WRPTP ANNOUNCE [messageType = 0x0B, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |               wrMessageId = 0x2000, wrConfig = 0x3] |           #
#           |                          {grandmasterPriority1 = X} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP ANNOUNCE [messageType = 0x0B,                 |           #
#           | domainNumber = DN1,  tlvType = 0x0003,              |           #
#           | wrMessageId = 0x2000, grandmasterPriority1 = X - 1, |           #
#           | wrConfig = 0x3, calibrated = 1, wrModeOn = 0]       |           #
#           | (sendCount = INFINITY)                              |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |            WRPTP SLAVE_PRESENT [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1000] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                    <Configure wrConfig = WR_M_ONLY> | P1        #
#           |                                                     |           #
#           |                 WRPTP ANNOUNCE [messageType = 0x0B, |           #
#           |                 domainNumber = 0, tlvType = 0x0003, |           #
#           |               wrMessageId = 0x2000, wrConfig = 0x2] |           #
#           |                          {grandmasterPriority1 = X} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP ANNOUNCE [messageType = 0x0B,                 |           #
#           | domainNumber = DN1,  tlvType = 0x0003,              |           #
#           | wrMessageId = 0x2000, grandmasterPriority1 = X - 1, |           #
#           | wrConfig = 0x3, calibrated = 1, wrModeOn = 0]       |           #
#           | (sendCount = INFINITY)                              |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |            WRPTP SLAVE_PRESENT [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1000] |           #
#        T1 |                          XX---------------------<<--| P1        #
#           |                                                     |           #
#           |                      <Check PTP portState = MASTER> | P1        #
#           |                                                     |           #
#           |                    <Configure wrConfig = WR_S_ONLY> | P1        #
#           |                                                     |           #
#           | WRPTP ANNOUNCE [messageType = 0x0B,                 |           #
#           | domainNumber = DN1,  tlvType = 0x0003,              |           #
#           | wrMessageId = 0x2000, grandmasterPriority1 = X + 1, |           #
#           | wrConfig = 0x3, calibrated = 1, wrModeOn = 0]       |           #
#           | (sendCount = INFINITY)                              |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |         < Wait for 6s to complete BMCA >            |           #
#           |                                                     |           #
#           | WRPTP SLAVE_PRESENT [messageType = 0x0C,            |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1000]             |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                     WRPTP LOCK [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1001] |           #
#        T1 |                          XX---------------------<<--| P1        #
#           |                                                     |           #
#           |                       <Check PTP portState = SLAVE> | P1        #
#           |                                                     |           #
#           |                   <Configure wrConfig = WR_M_AND_S> | P1        #
#           |                                                     |           #
#           |                 WRPTP ANNOUNCE [messageType = 0x0B, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |               wrMessageId = 0x2000, wrConfig = 0x3] |           #
#           |                          {grandmasterPriority1 = X} |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | WRPTP ANNOUNCE [messageType = 0x0B,                 |           #
#           | domainNumber = DN1,  tlvType = 0x0003,              |           #
#           | wrMessageId = 0x2000, grandmasterPriority1 = X + 1, |           #
#           | wrConfig = 0x3, calibrated = 1, wrModeOn = 0]       |           #
#           | (sendCount = INFINITY)                              |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |         < Wait for 6s to complete BMCA >            |           #
#           |                                                     |           #
#           | WRPTP SLAVE_PRESENT [messageType = 0x0C,            |           #
#           | domainNumber = DN1, targetPortIdentity = SPI1,      |           #
#           | tlvType = 0x0003, wrMessageId = 0x1000]             |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                     WRPTP LOCK [messageType = 0x0C, |           #
#           |               domainNumber = DN1, tlvType = 0x0003, |           #
#           |                               wrMessageId = 0x1001] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                      <Check PTP portState = M_LOCK> | P1        #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     TEE       = Test Execution Engine                                       #
#     DUT       = Device Under Test                                           #
#     T1        = TEE's port 1                                                #
#     P1        = DUT's port 1                                                #
#     PTP       = Precision Time Protocol                                     #
#     WRPTP     = White Rabbit Precision Time Protocol                        #
#     OC        = Ordinary Clock                                              #
#     BC        = Boundary Clock                                              #
#     DN1       = Domain Number 1                                             #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure Network Transport Protocol as IEEE 802.3/Ethernet.       #
#    vi.   Configure default values for Priority1 = X and Priority2 = Y.      #
#   vii.   Enable WRPTP on port P1.                                           #
#  viii.   Configure wrConfig = WR_M_AND_S.                                   #
#    ix.   Configure default values for knownDeltaTx and knownDeltaRx on P1.  #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Observe that DUT transmits WRPTP ANNOUNCE message on the port P1   #
#          with following parameters and store grandmasterPriority1 as X.     #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                                                                             #
# Step 4 : Send periodic WRPTP ANNOUNCE messages on the port T1 with following#
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#                  grandmasterPriority1 = X - 1                               #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                  calibrated           = 1                                   #
#                  wrModeOn             = 0                                   #
#                                                                             #
# Step 5 : Wait until completion of BMCA and verify that DUT transmits WRPTP  #
#          SLAVE_PRESENT message on port P1 with following parameters.        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
#                                                                             #
# (Part 2)                                                                    #
#                                                                             #
# Step 6 : Configure wrConfig = WR_M_ONLY.                                    #
#                                                                             #
# Step 7 : Observe that DUT transmits WRPTP ANNOUNCE message on the port P1   #
#          with following parameters and store grandmasterPriority1 as X.     #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x2                                 #
#                                                                             #
# Step 8 : Send periodic WRPTP ANNOUNCE messages on the port T1 with following#
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#                  grandmasterPriority1 = X - 1                               #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                  calibrated           = 1                                   #
#                  wrModeOn             = 0                                   #
#                                                                             #
# Step 9 : Wait until completion of BMCA and verify that DUT does not transmit#
#          WRPTP SLAVE_PRESENT message on port P1 with following parameters.  #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
#                                                                             #
# Step 10: Verify that PTP portState of port P1 is MASTER.                    #
#                                                                             #
# (Part 3)                                                                    #
#                                                                             #
# Step 11: Configure wrConfig = WR_S_ONLY.                                    #
#                                                                             #
# Step 12: Send periodic WRPTP ANNOUNCE messages on the port T1 with following#
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#                  grandmasterPriority1 = X + 1                               #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                  calibrated           = 1                                   #
#                  wrModeOn             = 0                                   #
#                                                                             #
# Step 13: Wait for 6s to complete BMCA.                                      #
#                                                                             #
# Step 14: Send WRPTP SLAVE_PRESENT message on port T1 with following         #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
#                                                                             #
# Step 15: Verify that DUT does not transmit WRPTP LOCK message on port P1    #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
#                                                                             #
# Step 16: Verify that PTP portState of port P1 is SLAVE.                     #
#                                                                             #
# (Part 4)                                                                    #
#                                                                             #
# Step 17: Configure wrConfig = WR_M_AND_S.                                   #
#                                                                             #
# Step 18: Observe that DUT transmits WRPTP ANNOUNCE message on the port P1   #
#          with following parameters and store grandmasterPriority1 as X.     #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                                                                             #
# Step 19: Send periodic WRPTP ANNOUNCE messages on the port T1 with following#
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#                  grandmasterPriority1 = X + 1                               #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                  calibrated           = 1                                   #
#                  wrModeOn             = 0                                   #
#                                                                             #
# Step 20: Wait for 6s to complete BMCA.                                      #
#                                                                             #
# Step 21: Send WRPTP SLAVE_PRESENT message on port T1 with following         #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
#                                                                             #
# Step 22: Verify that DUT transmits WRPTP LOCK message on port P1 with       #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
#                                                                             #
# Step 23: Verify that PTP portState of port P1 is M_LOCK.                    #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          Jul/2018      CERN          Initial                            #
# 1.1          Aug/2018      CERN          Updated timeout for completing BMCA#
# 1.2          Jan/2019      CERN          Modified waiting timeout to receive#
#                                          SLAVE_PRESENT and LOCKED messages. #
# 1.3          Jan/2019      CERN          a) Resolved error in priority1     #
#                                             value.                          #
#                                          b) Resolved non-transmission of    #
#                                             messages at background while    #
#                                             waiting to complete BMCA.       #
# 1.4          Mar/2019      CERN          a) Added verification of WR_M_AND_S#
#                                          b) Removed verification of NON_WR. #
# 1.5          Apr/2019      JC.BAU/CERN   Improve execution time             #
#                                                                             #
#                                                                             #
###############################################################################
# Copyright (c) 2018 - 2019 CERN                                              #
###############################################################################

global dut_log_msg
global tee_log_msg
global tee_rx_errlog
global session_id
global step

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def    "To verify that a WRPTP enabled device supports to configure wrConfig\
               data set member (allowable values: NON_WR, WR_S_ONLY, WR_M_ONLY and\
               WR_M_AND_S)."

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set dut_port_1                     $::dut_port_num_1
set vlan_id                        $::wrptp_vlan_id
set dut_ip                         $::dut_test_port_ip
set dut_dest_ip                    $::dut_test_port_ip
set tee_dest_ip                    $::tee_test_port_ip
set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT

set domain                         $::DEFAULT_DOMAIN
set wrptp_tlv                      $::ORG_EXT_TLV

set ann_msg_id                     $::wrMessageID(ANN_SUFIX)
set slave_present_msg_id           $::wrMessageID(SLAVE_PRESENT)
set lock_msg_id                    $::wrMessageID(LOCK)

set infinity                       $::INFINITY
set idle                           $::WRPTP_PORT_STATE(IDLE)

set wrconfig_0                     $::WRCONFIG(NON_WR)
set wrconfig_1                     $::WRCONFIG(WR_SLAVE_ONLY)
set wrconfig_2                     $::WRCONFIG(WR_MASTER_ONLY)
set wrconfig_3                     $::WRCONFIG(WR_MASTER_AND_SLAVE)

set dut_wrconfig_0                 "NON_WR"
set dut_wrconfig_1                 "WR_S_ONLY"
set dut_wrconfig_2                 "WR_M_ONLY"
set dut_wrconfig_3                 "WR_M_AND_S"

set dut_ptp_state_master           "MASTER"
set dut_ptp_state_slave            "SLAVE"
set dut_wr_state_m_lock            "M_LOCK"

set announce_timeout               [expr $::wrptp_announce_timeout + 60]
set signal_timeout                 $::wrptp_signal_timeout
set exc_timeout_retry              [expr $::wrptp_state_retry * $::wrptp_state_timeout]

set bmca_time                      $::wrptp_announce_timeout

set calibrated                     1
set step                           0

########################### END - INITIALIZATION ##############################

wrptp::display_interface_details

wrptp::dut_Initialization

proc cleanup {} {
	global step
	if { $step != 0 } {
		wrptp::dut_cleanup_setup_001
		wrptp::dut_commit_changes
	}
}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

### (Initial Part) ###

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure Network Transport Protocol as IEEE 802.3/Ethernet.       #
#    vi.   Configure default values for Priority1 = X and Priority2 = Y.      #
#   vii.   Enable WRPTP on port P1.                                           #
#  viii.   Configure wrConfig = WR_M_AND_S.                                   #
#    ix.   Configure default values for knownDeltaTx and knownDeltaRx on P1.  #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP1)"

if {!([wrptp::dut_configure_setup_001] && [wrptp::dut_commit_changes])} {

    LOG -msg "$dut_log_msg(INIT_SETUP1_F)"

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP1_F)"\
                      -tc_def   $tc_def
    return 0
}

###############################################################################
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(INIT)"

wrptp::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

    if {![WRPTP_UNICAST_PRE_REQUISITE_CONFIGURATION\
                        -port_num       $tee_port_1\
                        -tee_mac        $tee_mac\
                        -tee_ip         $tee_ip\
                        -dut_ip         $dut_ip\
                        -vlan_id        $vlan_id\
                        -dut_mac        tee_dest_mac\
                        -timeout        $arp_timeout]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
                           -tc_def  $tc_def

        return 0
    }

} else {
    set tee_dest_ip             $::PTP_E2E_IP
    set tee_dest_mac            $::PTP_E2E_MAC
}

if {![pltLib::create_filter_wrptp_pkt\
              -port_num                $tee_port_1\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def  $tc_def

    return 0
}

set filter_id [lindex $id1 end]

wrptp::reset_capture_stats\
        -port_num           $tee_port_1

###############################################################################
# Step 3 : Observe that DUT transmits WRPTP ANNOUNCE message on the port P1   #
#          with following parameters and store grandmasterPriority1 as X.     #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![wrptp::recv_announce \
           -port_num                  $tee_port_1\
           -domain_number             $domain\
           -tlv_type                  $wrptp_tlv\
           -message_id                $ann_msg_id\
           -recvd_src_port_number     src_port_number\
           -recvd_src_clock_identity  src_clock_identity\
           -recvd_gm_priority1        gm_priority1\
           -error_reason              error_reason\
           -wrconfig                  $wrconfig_3\
           -timeout                   $announce_timeout\
           -filter_id                 $filter_id]} {

      TEE_CLEANUP

      TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANNOUNCE_RX_P1_F) $error_reason"\
                          -tc_def $tc_def
  
      return 0
}

wrptp::reset_capture_stats\
        -port_num           $tee_port_1

###############################################################################
# Step 4 : Send periodic WRPTP ANNOUNCE messages on the port T1 with following#
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#                  grandmasterPriority1 = X - 1                               #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                  calibrated           = 1                                   #
#                  wrModeOn             = 0                                   #
###############################################################################

STEP [incr step]

set gm_priority1 [expr $gm_priority1 - 1]

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_TX_P1)"

if {![wrptp::send_announce\
            -port_num         $tee_port_1\
            -dest_mac         $tee_dest_mac\
            -src_mac          $tee_mac\
            -src_ip           $tee_ip\
            -dest_ip          $tee_dest_ip\
            -domain_number    $domain\
            -gm_priority1     $gm_priority1\
            -tlv_type         $wrptp_tlv\
            -message_id       $ann_msg_id\
            -wrconfig         $wrconfig_3\
            -count            $infinity\
            -calibrated       $calibrated]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_TX_P1_F)"\
                        -tc_def  $tc_def
     return 0

}

###############################################################################
# Step 5 : Wait until completion of BMCA and verify that DUT transmits WRPTP  #
#          SLAVE_PRESENT message on port P1 with following parameters.        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(BMCA_SLAVE_PRESENT_RX_P1_V)"

set bmca_signal_timeout  [expr $::wrptp_present_timeout_recv + $bmca_time]

if {![wrptp::recv_signal\
           -port_num                  $tee_port_1\
           -filter_id                 $filter_id\
           -timeout                   $bmca_signal_timeout\
           -domain_number             $domain\
           -tlv_type                  $wrptp_tlv\
           -error_reason              error_reason\
           -message_id                $slave_present_msg_id]} {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL  -reason "$tee_log_msg(SLAVE_PRESENT_RX_P1_F) $error_reason"\
                       -tc_def  $tc_def

    return 0

}

wrptp::reset_capture_stats\
        -port_num           $tee_port_1

#(Part 2)  

###############################################################################
# Step 6 : Configure wrConfig = WR_M_ONLY.                                    #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(SET_WRCONFIG_M_ONLY)"

if {!([wrptp::dut_set_wr_config\
            -port_num            $dut_port_1\
            -value               $dut_wrconfig_2] && [wrptp::dut_commit_changes])} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(SET_WRCONFIG_M_ONLY_F)"\
                        -tc_def  $tc_def

      return 0

}

###############################################################################
# Step 7 : Observe that DUT transmits WRPTP ANNOUNCE message on the port P1   #
#          with following parameters and store grandmasterPriority1 as X.     #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x2                                 #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![wrptp::recv_announce \
           -port_num                  $tee_port_1\
           -domain_number             $domain\
           -tlv_type                  $wrptp_tlv\
           -message_id                $ann_msg_id\
           -recvd_gm_priority1        gm_priority1\
           -wrconfig                  $wrconfig_2\
           -timeout                   $announce_timeout\
           -error_reason              error_reason\
           -filter_id                 $filter_id]} {

      TEE_CLEANUP

      TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANNOUNCE_RX_P1_F) $error_reason"\
                          -tc_def $tc_def
  
      return 0
}

wrptp::reset_capture_stats\
        -port_num           $tee_port_1

###############################################################################
# Step 8 : Send periodic WRPTP ANNOUNCE messages on the port T1 with following#
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#                  grandmasterPriority1 = X - 1                               #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                  calibrated           = 1                                   #
#                  wrModeOn             = 0                                   #
###############################################################################

STEP [incr step]

set gm_priority1 [expr $gm_priority1 - 1]

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_TX_P1)"

if {![wrptp::send_announce\
            -port_num         $tee_port_1\
            -dest_mac         $tee_dest_mac\
            -src_mac          $tee_mac\
            -src_ip           $tee_ip\
            -dest_ip          $tee_dest_ip\
            -domain_number    $domain\
            -gm_priority1     $gm_priority1\
            -tlv_type         $wrptp_tlv\
            -message_id       $ann_msg_id\
            -wrconfig         $wrconfig_3\
            -count            $infinity\
            -calibrated       $calibrated]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_TX_P1_F)"\
                        -tc_def  $tc_def
     return 0

}

###############################################################################
# Step 9 : Wait until completion of BMCA and verify that DUT does not transmit#
#          WRPTP SLAVE_PRESENT message on port P1 with following parameters.  #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(BMCA_SLAVE_PRESENT_NRX_P1_V)"

set bmca_signal_timeout  [expr $::wrptp_present_timeout_recv + $bmca_time]

if {[wrptp::recv_signal\
           -port_num                  $tee_port_1\
           -filter_id                 $filter_id\
           -timeout                   $bmca_signal_timeout\
           -domain_number             $domain\
           -tlv_type                  $wrptp_tlv\
           -message_id                $slave_present_msg_id]} {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL  -reason "$tee_log_msg(SLAVE_PRESENT_NRX_P1_F)"\
                       -tc_def  $tc_def

    return 0

}

###############################################################################
# Step 10: Verify that PTP portState of port P1 is MASTER.                    #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(CHECK_PTP_STATE_MASTER_P1_V)"

if {![wrptp::dut_check_ptp_port_state\
            -port_num            $dut_port_1\
            -state               $dut_ptp_state_master]} {

     TEE_CLEANUP

     TC_CLEAN_AND_FAIL  -reason "$dut_log_msg(CHECK_PTP_STATE_MASTER_P1_F)"\
                        -tc_def  $tc_def

      return 0

}

wrptp::reset_capture_stats\
        -port_num           $tee_port_1

# (Part 3)

###############################################################################
# Step 11: Configure wrConfig = WR_S_ONLY.                                    #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(SET_WRCONFIG_S_ONLY)"

if {!([wrptp::dut_set_wr_config\
            -port_num            $dut_port_1\
            -value               $dut_wrconfig_1] && [wrptp::dut_commit_changes])} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(SET_WRCONFIG_S_ONLY_F)"\
                        -tc_def  $tc_def

      return 0

}

###############################################################################
# Step 12: Send periodic WRPTP ANNOUNCE messages on the port T1 with following#
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#                  grandmasterPriority1 = X + 1                               #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                  calibrated           = 1                                   #
#                  wrModeOn             = 0                                   #
###############################################################################

STEP [incr step]

set gm_priority1 [expr $gm_priority1 + 2]

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_TX_P1)"

if {![wrptp::send_announce\
            -port_num         $tee_port_1\
            -dest_mac         $tee_dest_mac\
            -src_mac          $tee_mac\
            -src_ip           $tee_ip\
            -dest_ip          $tee_dest_ip\
            -domain_number    $domain\
            -gm_priority1     $gm_priority1\
            -tlv_type         $wrptp_tlv\
            -message_id       $ann_msg_id\
            -wrconfig         $wrconfig_3\
            -count            $infinity\
            -calibrated       $calibrated]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_TX_P1_F)"\
                        -tc_def  $tc_def
     return 0

}

###############################################################################
# Step 13: Wait for 6s to complete BMCA.                                      #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(WAIT_BMCA)"
LOG -level 0 -msg "Waiting for $bmca_time secs"

wrptp::wait_msec [expr int($bmca_time * 1000)]

###############################################################################
# Step 14: Send WRPTP SLAVE_PRESENT message on port T1 with following         #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(SLAVE_PRESENT_TX_P1)"

if {![wrptp::send_signal \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -dest_mac                $tee_dest_mac\
           -src_ip                  $tee_ip\
           -dest_ip                 $tee_dest_ip\
           -domain_number           $domain\
           -target_port_number      $src_port_number\
           -target_clock_identity   $src_clock_identity\
           -tlv_type                $wrptp_tlv\
           -message_id              $slave_present_msg_id]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SLAVE_PRESENT_TX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 15: Verify that DUT does not transmit WRPTP LOCK message on port P1    #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCK_NRX_P1_V)"

if {[wrptp::recv_signal \
           -port_num                $tee_port_1\
           -domain_number           $domain\
           -tlv_type                $wrptp_tlv\
           -message_id              $lock_msg_id\
           -timeout                 $::wrptp_m_lock_timeout_recv\
           -filter_id               $filter_id]} {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL  -reason "$tee_log_msg(LOCK_NRX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 16: Verify that PTP portState of port P1 is SLAVE.                     #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(CHECK_PTP_STATE_SLAVE_P1_V)"

if {![wrptp::dut_check_ptp_port_state\
            -port_num            $dut_port_1\
            -state               $dut_ptp_state_slave]} {

     TEE_CLEANUP

     TC_CLEAN_AND_FAIL  -reason "$dut_log_msg(CHECK_PTP_STATE_SLAVE_P1_F)"\
                        -tc_def  $tc_def

      return 0

}

wrptp::reset_capture_stats\
        -port_num           $tee_port_1

# (Part 4)

###############################################################################
# Step 17: Configure wrConfig = WR_M_AND_S.                                   #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(SET_WRCONFIG_M_AND_S)"

if {!([wrptp::dut_set_wr_config\
            -port_num            $dut_port_1\
            -value               $dut_wrconfig_3] && [wrptp::dut_commit_changes])} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(SET_WRCONFIG_M_AND_S_F)"\
                        -tc_def  $tc_def

      return 0

}

###############################################################################
# Step 18: Observe that DUT transmits WRPTP ANNOUNCE message on the port P1   #
#          with following parameters and store grandmasterPriority1 as X.     #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![wrptp::recv_announce \
           -port_num                  $tee_port_1\
           -domain_number             $domain\
           -tlv_type                  $wrptp_tlv\
           -message_id                $ann_msg_id\
           -recvd_src_port_number     src_port_number\
           -recvd_src_clock_identity  src_clock_identity\
           -recvd_gm_priority1        gm_priority1\
           -error_reason              error_reason\
           -wrconfig                  $wrconfig_3\
           -timeout                   $announce_timeout\
           -filter_id                 $filter_id]} {

      TEE_CLEANUP

      TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANNOUNCE_RX_P1_F) $error_reason"\
                          -tc_def $tc_def
  
      return 0
}

wrptp::reset_capture_stats\
        -port_num           $tee_port_1

# (Part 5)

###############################################################################
# Step 19: Send periodic WRPTP ANNOUNCE messages on the port T1 with following#
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0B                                #
#                  domainNumber         = DN1                                 #
#                  grandmasterPriority1 = X + 1                               #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x2000                              #
#                  wrConfig             = 0x3                                 #
#                  calibrated           = 1                                   #
#                  wrModeOn             = 0                                   #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_TX_P1)"

set gm_priority1 [expr $gm_priority1 + 1]

if {![wrptp::send_announce\
            -port_num         $tee_port_1\
            -dest_mac         $tee_dest_mac\
            -src_mac          $tee_mac\
            -src_ip           $tee_ip\
            -dest_ip          $tee_dest_ip\
            -domain_number    $domain\
            -gm_priority1     $gm_priority1\
            -tlv_type         $wrptp_tlv\
            -message_id       $ann_msg_id\
            -wrconfig         $wrconfig_3\
            -count            $infinity\
            -calibrated       $calibrated]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_TX_P1_F)"\
                        -tc_def  $tc_def
     return 0

}

###############################################################################
# Step 20: Wait for 6s to complete BMCA.                                      #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(WAIT_BMCA)"
LOG -level 0 -msg "Waiting for $bmca_time secs"

wrptp::wait_msec [expr int($bmca_time * 1000)]

###############################################################################
# Step 21: Send WRPTP SLAVE_PRESENT message on port T1 with following         #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#                  targetPortIdentity   = SPI1                                #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageID          = 0x1000                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(SLAVE_PRESENT_TX_P1)"

if {![wrptp::send_signal \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -dest_mac                $tee_dest_mac\
           -src_ip                  $tee_ip\
           -dest_ip                 $tee_dest_ip\
           -domain_number           $domain\
           -target_port_number      $src_port_number\
           -target_clock_identity   $src_clock_identity\
           -tlv_type                $wrptp_tlv\
           -message_id              $slave_present_msg_id]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SLAVE_PRESENT_TX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 22: Verify that DUT transmits WRPTP LOCK message on port P1 with       #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  messageType          = 0x0C                                #
#                  domainNumber         = DN1                                 #
#              TLV                                                            #
#                  tlvType              = 0x0003                              #
#                  wrMessageId          = 0x1001                              #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$tee_log_msg(LOCK_RX_P1_V)"

if {![wrptp::recv_signal \
           -port_num                $tee_port_1\
           -domain_number           $domain\
           -tlv_type                $wrptp_tlv\
           -message_id              $lock_msg_id\
           -timeout                 $::wrptp_m_lock_timeout_recv\
           -filter_id               $filter_id]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(LOCK_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 23: Verify that PTP portState of port P1 is M_LOCK.                    #
###############################################################################

STEP [incr step]

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_M_LOCK_P1_V)"

if {![wrptp::dut_check_wr_port_state\
            -port_num            $dut_port_1\
            -wr_port_state       $dut_wr_state_m_lock]} {

     TEE_CLEANUP

     TC_CLEAN_AND_FAIL  -reason "$dut_log_msg(CHECK_STATE_M_LOCK_P1_F)"\
                        -tc_def  $tc_def

      return 0

}


TC_CLEAN_AND_PASS   -reason "DUT supports to configure wrConfig data set member as\
                             WR_S_ONLY, WR_M_ONLY and WR_M_AND_S."\
                    -tc_def  $tc_def
return 1

############################ END OF TEST CASE #################################

