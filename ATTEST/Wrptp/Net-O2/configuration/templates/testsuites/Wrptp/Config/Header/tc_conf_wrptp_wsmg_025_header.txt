 Test Case         : tc_conf_wrptp_wsmg_025                                  
 Test Case Version : 1.3                                                     
 Component Name    : ATTEST WRPTP CONFORMANCE TEST SUITE                     
 Module Name       : WRPTP State Machine Group (WSMG)                        
                                                                             
 Title             : WR Slave re-entering of WRPTP portState - RESP_CALIB_REQ
                     - on expiry of WR_RESP_CALIB_REQ_TIMEOUT                
                                                                             
 Purpose           : To verify that a WRPTP enabled device with it's port in 
                     PTP Slave re-enters to it's port WR state RESP_CALIB_REQ
                     on expiry of WR_RESP_CALIB_REQ_TIMEOUT when             
                     otherPortCalPeriod is 0x0.                              
                                                                             
 Reference         : White Rabbit Specification v2.0 July 2011, Clause 6.7.3 
                     Pages 35, Clause 6.7.4 Page 37, Figure 27 Page 62       
                                                                             
 Conformance Type  : MUST                                                    
                                                                             
