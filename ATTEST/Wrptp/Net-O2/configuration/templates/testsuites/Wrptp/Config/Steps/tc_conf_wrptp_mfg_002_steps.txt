                                                                             
 Procedure         :                                                         
                                                                             
 (Initial Part)                                                              
                                                                             
 Step 1 : Initialization of DUT                                              
     i.   Enable DUT's port P1.                                              
    ii.   Enable PTP on port P1.                                             
   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   
    iv.   Configure clock mode as One-step/Two-step.                         
     v.   Configure Network Transport Protocol as IEEE 802.3/Ethernet.       
    vi.   Configure default values for Priority1 = X and Priority2 = Y.      
   vii.   Enable WRPTP on port P1.                                           
  viii.   Configure wrConfig = WR_M_AND_S.                                   
    ix.   Configure default values for knownDeltaTx and knownDeltaRx on P1.  
                                                                             
 Step 2 : Initialization of TEE                                              
     i.   Add port T1 at TEE.                                                
                                                                             
 (Part 1)                                                                    
                                                                             
 Step 3 : Observe that DUT transmits WRPTP ANNOUNCE message on the port P1   
          with following parameters and store sourcePortIdentity as SPI1.    
                                                                             
              PTP Header                                                     
                  messageType          = 0x0B                                
                  domainNumber         = DN1                                 
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageId          = 0x2000                              
                                                                             
 Step 4 : Send WRPTP SLAVE_PRESENT message on port T1 with following         
          parameters.                                                        
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
                  targetPortIdentity   = SPI1                                
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageID          = 0x1000                              
                                                                             
 Step 5 : Verify that DUT transmits WRPTP LOCK message on the port P1 with   
          following parameters.                                              
                                                                             
               Ethernet Header                                               
                 1) Source MAC                = Unicast MAC                  
                 2) Destination MAC           = Unicast MAC or               
                                                01:1B:19:00:00:00            
                 3) EtherType                 = PTPv2 over Ethernet (0x88F7) 
               PTP Header                                                    
                 4) transportSpecific         = 0 or 1, Reserved: 2-F        
                                                (4 bits)                     
                 5) messageType               = 0x0C (4 bits)                
                 6) Reserved Bits (1)         = 0 (4 bits)                   
                 7) Version                   = 2 (4 bits)                   
                 8) messageLength             = 56 (2 octets)                
                 9) domainNumber              = 0 - 255, Reserved: 128 - 255 
                10) Reserved Bits (2)         = 0 (8 bits)                   
                11) flagField                 = 0x0000 - 0xFFFF              
                12) correctionField           = 0 (8 octets)                 
                13) Reserved Bits (3)         = 0 (32 bits)                  
                14) sourcePortIdentity                                       
                     a)clockIdentity          = 8 octets                     
                     b)portNumber             = 2 octets                     
                15) sequenceId                = 0 - 65535                    
                16) controlField              = 5                            
                17) logMessageInterval        = 127                          
                18) targetPortIdentity        = non-zero (10 octets)         
               TLV Header                                                    
                19) tlvType                   = 0x0003                       
                20) lengthField               = 8 (2 octets)                 
                21) OrganizationId            = 0x080030                     
                22) magicNumber               = 0xDEAD                       
                23) versionNumber             = 0x01                         
                24) wrMessageId               = 0x1001                       
                                                                             
 Step 6 : Send WRPTP LOCKED message on port T1 with following parameters.    
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
                  targetPortIdentity   = SPI1                                
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageId          = 0x1002                              
                                                                             
 Step 7 : Verify that DUT transmits WRPTP CALIBRATE message on the port P1   
          with following parameters.                                         
                                                                             
               Ethernet Header                                               
                 1) Source MAC                = Unicast MAC                  
                 2) Destination MAC           = Unicast MAC or               
                                                01:1B:19:00:00:00            
                 3) EtherType                 = PTPv2 over Ethernet (0x88F7) 
               PTP Header                                                    
                 4) transportSpecific         = 0 or 1, Reserved: 2-F        
                                                (4 bits)                     
                 5) messageType               = 0x0C (4 bits)                
                 6) Reserved Bits (1)         = 0 (4 bits)                   
                 7) Version                   = 2 (4 bits)                   
                 8) messageLength             = 62 (2 octets)                
                 9) domainNumber              = 0 - 255, Reserved: 128 - 255 
                10) Reserved Bits (2)         = 0 (8 bits)                   
                11) flagField                 = 0x0000 - 0xFFFF              
                12) correctionField           = 0 (8 octets)                 
                13) Reserved Bits (3)         = 0 (32 bits)                  
                14) sourcePortIdentity                                       
                     a)clockIdentity          = 8 octets                     
                     b)portNumber             = 2 octets                     
                15) sequenceId                = 0 - 65535                    
                16) controlField              = 5                            
                17) logMessageInterval        = 127                          
                18) targetPortIdentity        = non-zero (10 octets)         
               TLV Header                                                    
                19) tlvType                   = 0x0003                       
                20) lengthField               = 14 (2 octets)                
                21) OrganizationId            = 0x080030                     
                22) magicNumber               = 0xDEAD                       
                23) versionNumber             = 0x01                         
                24) wrMessageId               = 0x1003                       
                25) calSendPattern            = 0x0 (Warning, if 0x1)        
                26) calRetry                  = 1 octet                      
                26) calPeriod                 = 4 octets                     
                                                                             
 Step 8 : Verify that DUT transmits WRPTP CALIBRATED message on the port P1  
          with following parameters.                                         
                                                                             
               Ethernet Header                                               
                 1) Source MAC                = Unicast MAC                  
                 2) Destination MAC           = Unicast MAC or               
                                                01:1B:19:00:00:00            
                 3) EtherType                 = PTPv2 over Ethernet (0x88F7) 
               PTP Header                                                    
                 4) transportSpecific         = 0 or 1, Reserved: 2-F        
                                                (4 bits)                     
                 5) messageType               = 0x0C (4 bits)                
                 6) Reserved Bits (1)         = 0 (4 bits)                   
                 7) Version                   = 2 (4 bits)                   
                 8) messageLength             = 72 (2 octets)                
                 9) domainNumber              = 0 - 255, Reserved: 128 - 255 
                10) Reserved Bits (2)         = 0 (8 bits)                   
                11) flagField                 = 0x0000 - 0xFFFF              
                12) correctionField           = 0 (8 octets)                 
                13) Reserved Bits (3)         = 0 (32 bits)                  
                14) sourcePortIdentity                                       
                     a)clockIdentity          = 8 octets                     
                     b)portNumber             = 2 octets                     
                15) sequenceId                = 0 - 65535                    
                16) controlField              = 5                            
                17) logMessageInterval        = 127                          
                18) targetPortIdentity        = non-zero (10 octets)         
               TLV Header                                                    
                19) tlvType                   = 0x0003                       
                20) lengthField               = 24 (2 octets)                
                21) OrganizationId            = 0x080030                     
                22) magicNumber               = 0xDEAD                       
                23) versionNumber             = 0x01                         
                24) wrMessageId               = 0x1004                       
                25) deltaTx                   = 8 octets                     
                26) deltaRx                   = 8 octets                     
                                                                             
 Step 9 : Send WRPTP CALIBRATE message on port T1 with following parameters. 
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
                  targetPortIdentity   = SPI1                                
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageId          = 0x1003                              
                  calSendPattern       = FALSE                               
                  calRetry             = 0                                   
                  calPeriod            = 3000us                              
                                                                             
 Step 10 : Send WRPTP CALIBRATED message on port T1 with following parameters
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
                  targetPortIdentity   = SPI1                                
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageId          = 0x1004                              
                  deltaTx              = 0                                   
                  deltaRx              = 0                                   
                                                                             
 Step 11 : Verify that DUT transmits WRPTP WR_MODE_ON message on the port P1 
           with following parameters.                                        
                                                                             
               Ethernet Header                                               
                 1) Source MAC                = Unicast MAC                  
                 2) Destination MAC           = Unicast MAC or               
                                                01:1B:19:00:00:00            
                 3) EtherType                 = PTPv2 over Ethernet (0x88F7) 
               PTP Header                                                    
                 4) transportSpecific         = 0 or 1, Reserved: 2-F        
                                                (4 bits)                     
                 5) messageType               = 0x0C (4 bits)                
                 6) Reserved Bits (1)         = 0 (4 bits)                   
                 7) Version                   = 2 (4 bits)                   
                 8) messageLength             = 56 (2 octets)                
                 9) domainNumber              = 0 - 255, Reserved: 128 - 255 
                10) Reserved Bits (2)         = 0 (8 bits)                   
                11) flagField                 = 0x0000 - 0xFFFF              
                12) correctionField           = 0 (8 octets)                 
                13) Reserved Bits (3)         = 0 (32 bits)                  
                14) sourcePortIdentity                                       
                     a)clockIdentity          = 8 octets                     
                     b)portNumber             = 2 octets                     
                15) sequenceId                = 0 - 65535                    
                16) controlField              = 5                            
                17) logMessageInterval        = 127                          
                18) targetPortIdentity        = non-zero (10 octets)         
               TLV Header                                                    
                19) tlvType                   = 0x0003                       
                20) lengthField               = 8 (2 octets)                 
                21) OrganizationId            = 0x080030                     
                22) magicNumber               = 0xDEAD                       
                23) versionNumber             = 0x01                         
                24) wrMessageId               = 0x1005                       
                                                                             
