                                                                             
 Ladder Diagram    :                                                         
                                                                             
          TEE                                                   DUT          
           |                                                     |           
           |                                        <Enable PTP> | P1        
           |                       <Enable PTP with BC/OC clock> |           
           |                    <Clock mode = One-step/Two-step> |           
           |                     <Transport Protocol = IPv4/UDP> |           
           |            <Configure Priority1 = X, Priority2 = Y> | P1        
           |                                      <Enable WRPTP> | P1        
           |                   <Configure wrConfig = WR_M_AND_S> | P1        
           |      <Configure default values for knownDeltaTx and | P1        
           |                                       knownDeltaRx> |           
           |                                                     |           
           |                 WRPTP ANNOUNCE [messageType = 0x0B, |           
           |               domainNumber = DN1, tlvType = 0x0003, |           
           |                               wrMessageId = 0x2000] |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
                                                                             
 Legends           :                                                         
                                                                             
     TEE       = Test Execution Engine                                       
     DUT       = Device Under Test                                           
     T1        = TEE's port 1                                                
     P1        = DUT's port 1                                                
     PTP       = Precision Time Protocol                                     
     WRPTP     = White Rabbit Precision Time Protocol                        
     OC        = Ordinary Clock                                              
     BC        = Boundary Clock                                              
     DN1       = Domain Number 1                                             
                                                                             
