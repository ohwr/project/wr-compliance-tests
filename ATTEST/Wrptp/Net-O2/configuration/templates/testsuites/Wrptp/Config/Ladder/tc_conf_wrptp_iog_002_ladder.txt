                                                                             
 Ladder Diagram    :                                                         
                                                                             
          TEE                                                   DUT          
           |                                                     |           
           |                                        <Enable PTP> | P1        
           |                       <Enable PTP with BC/OC clock> |           
           |                    <Clock mode = One-step/Two-step> |           
           |          <Transport Protocol = IEEE 802.3/Ethernet> |           
           |            <Configure Priority1 = X, Priority2 = Y> | P1        
           |                                      <Enable WRPTP> | P1        
           |                   <Configure wrConfig = WR_M_AND_S> | P1        
           |      <Configure default values for knownDeltaTx and | P1        
           |                                       knownDeltaRx> |           
           |                                                     |           
           |                 WRPTP ANNOUNCE [messageType = 0x0B, |           
           |               domainNumber = DN1, tlvType = 0x0003, |           
           |                               wrMessageId = 0x2000] |           
           |                          {grandmasterPriority1 = X} |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           | ANNOUNCE [messageType = 0x0B,                       |           
           | domainNumber = DN1, correctionField = 0,            |           
           | controlField = 0x05, logMessageInterval = 1,        |           
           | grandmasterPriority1 = X - 1]                       |           
           | (sendCount = INFINITY)                              |           
        T1 |-->>-------------------------------------------------| P1        
           |                                                     |           
           | SYNC [messageType = 0x00,                           |           
           | domainNumber = DN1, correctionField = 0,            |           
           | controlField = 0x00, logMessageInterval = 0]        |           
           | (sendCount = INFINITY)                              |           
        T1 |-->>-------------------------------------------------| P1        
           |                                                     |           
           | If Two-step clock, FOLLOW_UP                        |           
           | [messageType = 0x08, domainNumber = DN1]            |           
           | correctionField = 0, controlField = 0x02,           |           
           | logMessageInterval = 0]                             |           
           | (sendCount = INFINITY)                              |           
        T1 |-->>-------------------------------------------------| P1        
           |                                                     |           
           |         < Wait for 6s to complete BMCA >            |           
           |                                                     |           
           |                      DELAY_REQ [messageType = 0x01, |           
           |                                 domainNumber = DN1] |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
                                                                             
 Legends           :                                                         
                                                                             
     TEE       = Test Execution Engine                                       
     DUT       = Device Under Test                                           
     T1        = TEE's port 1                                                
     P1        = DUT's port 1                                                
     PTP       = Precision Time Protocol                                     
     WRPTP     = White Rabbit Precision Time Protocol                        
     OC        = Ordinary Clock                                              
     BC        = Boundary Clock                                              
     DN1       = Domain Number 1                                             
                                                                             
