 Test Case         : tc_conf_wrptp_wsmg_030                                  
 Test Case Version : 1.3                                                     
 Component Name    : ATTEST WRPTP CONFORMANCE TEST SUITE                     
 Module Name       : WRPTP State Machine Group (WSMG)                        
                                                                             
 Title             : WR Slave transition of WRPTP portState from             
                     RESP_CALIB_REQ to IDLE after EXC_TIMEOUT_RETRY occurs - 
                     otherPortCalPeriod and otherPortCalRetry > 0x0          
                                                                             
 Purpose           : To verify that a WRPTP enabled device with it's port in 
                     PTP Slave transitions it's port WR state from           
                     RESP_CALIB_REQ to IDLE state after EXC_TIMEOUT_RETRY    
                     occurs when otherPortCalPeriod and otherPortCalRetry are
                     greater than 0x0 (otherPortCalPeriod and                
                     otherPortCalRetry should be ignored by DUT).            
                                                                             
 Reference         : White Rabbit Specification v2.0 July 2011, Clause 6.7.3 
                     Pages 35, Clause 6.7.4 Page 37, Figure 27 Page 62       
                                                                             
 Conformance Type  : MUST                                                    
                                                                             
