                                                                             
 Procedure         :                                                         
                                                                             
 (Initial Part)                                                              
                                                                             
 Step 1 : Initialization of DUT                                              
     i.   Enable DUT's port P1.                                              
    ii.   Enable PTP on port P1.                                             
   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   
    iv.   Configure clock mode as One-step/Two-step.                         
     v.   Configure Network Transport Protocol as IEEE 802.3/Ethernet.       
    vi.   Configure default values for Priority1 = X and Priority2 = Y.      
   vii.   Enable WRPTP on port P1.                                           
  viii.   Configure wrConfig = WR_M_AND_S.                                   
    ix.   Configure default values for knownDeltaTx and knownDeltaRx on P1.  
                                                                             
 Step 2 : Initialization of TEE                                              
     i.   Add port T1 at TEE.                                                
                                                                             
 (Part 1)                                                                    
                                                                             
 Step 3 : Send periodic WRPTP ANNOUNCE messages on the port T1 with following
          parameters.                                                        
                                                                             
              PTP Header                                                     
                  messageType          = 0x0B                                
                  domainNumber         = DN1                                 
                  grandmasterPriority1 = X - 1                               
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageId          = 0x2000                              
                  wrConfig             = 0x3                                 
                  calibrated           = 1                                   
                  wrModeOn             = 0                                   
                                                                             
 Step 4 : Wait until completion of BMCA and observe that DUT transmits WRPTP 
          SLAVE_PRESENT message on port P1 with following parameters. Store  
          sourcePortIdentity as SPI1.                                        
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageID          = 0x1000                              
                                                                             
 Step 5 : Send WRPTP LOCK message on port T1 with following parameters.      
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
                  targetPortIdentity   = SPI1                                
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageId          = 0x1001                              
                                                                             
 Step 6 : Observe that the DUT transmits WRPTP LOCKED message on port P1 with
          following parameters.                                              
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageID          = 0x1002                              
                                                                             
 Step 7 : Send WRPTP CALIBRATE message on port T1 with following parameters. 
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
                  targetPortIdentity   = SPI1                                
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageId          = 0x1003                              
                  calSendPattern       = FALSE                               
                  calRetry             = 0                                   
                  calPeriod            = 0                                   
                                                                             
 Step 8 : Send WRPTP CALIBRATED message on port T1 with following parameters.
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
                  targetPortIdentity   = SPI1                                
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageId          = 0x1004                              
                  deltaTx              = 0                                   
                  deltaRx              = 0                                   
                                                                             
 Step 9 : Observe that the DUT transmits WRPTP CALIBRATE message on port P1  
          with following parameters.                                         
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageId          = 0x1003                              
                                                                             
 Step 10: Verify that the DUT transmits WRPTP CALIBRATED message on port P1  
          with following parameters.                                         
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageId          = 0x1004                              
                  deltaTx              = Default value                       
                                                                             
 (Part 2)                                                                    
                                                                             
 Step 11: Disable WRPTP on port P1.                                          
                                                                             
 Step 12: Configure knownDeltaTx = 0 on port P1.                             
                                                                             
 Step 13: Enable WRPTP on port P1.                                           
                                                                             
 Step 14: Wait for WRPTP te be enabled.                                      
                                                                             
 Step 15: Send periodic WRPTP ANNOUNCE messages on the port T1 with following
          parameters.                                                        
                                                                             
              PTP Header                                                     
                  messageType          = 0x0B                                
                  domainNumber         = DN1                                 
                  grandmasterPriority1 = X - 1                               
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageId          = 0x2000                              
                  wrConfig             = 0x3                                 
                  calibrated           = 1                                   
                  wrModeOn             = 0                                   
                                                                             
 Step 16: Wait until completion of BMCA and observe that DUT transmits WRPTP 
          SLAVE_PRESENT message on port P1 with following parameters. Store  
          sourcePortIdentity as SPI1.                                        
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageID          = 0x1000                              
                                                                             
 Step 17: Send WRPTP LOCK message on port T1 with following parameters.      
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
                  targetPortIdentity   = SPI1                                
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageId          = 0x1001                              
                                                                             
 Step 18: Observe that the DUT transmits WRPTP LOCKED message on port P1 with
          following parameters.                                              
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageID          = 0x1002                              
                                                                             
 Step 19: Send WRPTP CALIBRATE message on port T1 with following parameters. 
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
                  targetPortIdentity   = SPI1                                
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageId          = 0x1003                              
                  calSendPattern       = FALSE                               
                  calRetry             = 0                                   
                  calPeriod            = 0                                   
                                                                             
 Step 20: Send WRPTP CALIBRATED message on port T1 with following parameters.
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
                  targetPortIdentity   = SPI1                                
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageId          = 0x1004                              
                  deltaTx              = 0                                   
                  deltaRx              = 0                                   
                                                                             
 Step 21: Observe that the DUT transmits WRPTP CALIBRATE message on port P1  
          with following parameters.                                         
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageId          = 0x1003                              
                                                                             
 Step 22: Verify that the DUT transmits WRPTP CALIBRATED message on port P1  
          with following parameters.                                         
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageId          = 0x1004                              
                  deltaTx              = 0                                   
                                                                             
 (Part 3)                                                                    
                                                                             
 Step 23: Disable WRPTP on port P1.                                          
                                                                             
 Step 24: Configure knownDeltaTx = Default value on port P1.                 
                                                                             
 Step 25: Enable WRPTP on port P1.                                           
                                                                             
 Step 26: Wait for WRPTP te be enabled.                                      
                                                                             
 Step 27: Send periodic WRPTP ANNOUNCE messages on the port T1 with following
          parameters.                                                        
                                                                             
              PTP Header                                                     
                  messageType          = 0x0B                                
                  domainNumber         = DN1                                 
                  grandmasterPriority1 = X - 1                               
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageId          = 0x2000                              
                  wrConfig             = 0x3                                 
                  calibrated           = 1                                   
                  wrModeOn             = 0                                   
                                                                             
 Step 28: Wait until completion of BMCA and observe that DUT transmits WRPTP 
          SLAVE_PRESENT message on port P1 with following parameters. Store  
          sourcePortIdentity as SPI1.                                        
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageID          = 0x1000                              
                                                                             
 Step 29: Send WRPTP LOCK message on port T1 with following parameters.      
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
                  targetPortIdentity   = SPI1                                
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageId          = 0x1001                              
                                                                             
 Step 30: Observe that the DUT transmits WRPTP LOCKED message on port P1 with
          following parameters.                                              
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageID          = 0x1002                              
                                                                             
 Step 31: Send WRPTP CALIBRATE message on port T1 with following parameters. 
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
                  targetPortIdentity   = SPI1                                
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageId          = 0x1003                              
                  calSendPattern       = FALSE                               
                  calRetry             = 0                                   
                  calPeriod            = 0                                   
                                                                             
 Step 32: Send WRPTP CALIBRATED message on port T1 with following parameters.
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
                  targetPortIdentity   = SPI1                                
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageId          = 0x1004                              
                  deltaTx              = 0                                   
                  deltaRx              = 0                                   
                                                                             
 Step 33: Observe that the DUT transmits WRPTP CALIBRATE message on port P1  
          with following parameters.                                         
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageId          = 0x1003                              
                                                                             
 Step 34: Verify that the DUT transmits WRPTP CALIBRATED message on port P1  
          with following parameters.                                         
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageId          = 0x1004                              
                  deltaTx              = Default value                       
                                                                             
