                                                                             
 Ladder Diagram    :                                                         
                                                                             
          TEE                                                   DUT          
           |                                                     |           
           |                                        <Enable PTP> | P1        
           |                       <Enable PTP with BC/OC clock> |           
           |                    <Clock mode = One-step/Two-step> |           
           |          <Transport Protocol = IEEE 802.3/Ethernet> |           
           |            <Configure Priority1 = X, Priority2 = Y> | P1        
           |                                      <Enable WRPTP> | P1        
           |                   <Configure wrConfig = WR_M_AND_S> | P1        
           |      <Configure default values for knownDeltaTx and | P1        
           |                                       knownDeltaRx> |           
           |                                                     |           
           | WRPTP ANNOUNCE [messageType = 0x0B,                 |           
           | domainNumber = DN1, grandmasterPriority1 = X - 1,   |           
           | tlvType = 0x0003, wrMessageId = 0x2000,             |           
           | wrConfig = 0x3, calibrated = 1, wrModeOn = 0]       |           
           | (sendCount = INFINITY)                              |           
        T1 |-->>-------------------------------------------------| P1        
           |                                                     |           
           |            WRPTP SLAVE_PRESENT [messageType = 0x0C, |           
           |               domainNumber = DN1, tlvType = 0x0003, |           
           |                               wrMessageId = 0x1000] |           
           |                         {sourcePortIdentity = SPI1} |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           | WRPTP LOCK [messageType = 0x0C,                     |           
           | domainNumber = DN1, targetPortIdentity = SPI1,      |           
           | tlvType = 0x0003, wrMessageId = 0x1001]             |           
        T1 |-->>-------------------------------------------------| P1        
           |                                                     |           
           |                   WRPTP LOCKED [messageType = 0x0C, |           
           |               domainNumber = DN1, tlvType = 0x0003, |           
           |                               wrMessageId = 0x1002] |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           |  <Wait for 1230ms for EXC_TIMEOUT_RETRY to occur>   |           
           |                                                     |           
           | WRPTP CALIBRATE [messageType = 0x0C,                |           
           | domainNumber = DN1, targetPortIdentity = SPI1,      |           
           | tlvType = 0x0003, wrMessageId = 0x1003,             |           
           | calSendPattern = FALSE, calRetry = 0,               |           
           | calPeriod = 3000us]                                 |           
        T1 |-->>-------------------------------------------------| P1        
           |                                                     |           
           |                      <Check WRPTP portState = IDLE> | P1        
           |                                                     |           
                                                                             
 Legends           :                                                         
                                                                             
     TEE       = Test Execution Engine                                       
     DUT       = Device Under Test                                           
     T1        = TEE's port 1                                                
     P1        = DUT's port 1                                                
     PTP       = Precision Time Protocol                                     
     WRPTP     = White Rabbit Precision Time Protocol                        
     OC        = Ordinary Clock                                              
     BC        = Boundary Clock                                              
     DN1       = Domain Number 1                                             
                                                                             
