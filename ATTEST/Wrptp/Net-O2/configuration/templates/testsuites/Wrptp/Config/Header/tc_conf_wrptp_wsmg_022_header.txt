 Test Case         : tc_conf_wrptp_wsmg_022                                  
 Test Case Version : 1.2                                                     
 Component Name    : ATTEST WRPTP CONFORMANCE TEST SUITE                     
 Module Name       : WRPTP State Machine Group (WSMG)                        
                                                                             
 Title             : WR Slave re-entering of WRPTP portState - LOCKED - on   
                     expiry of WR_LOCKED_TIMEOUT                             
                                                                             
 Purpose           : To verify that a WRPTP enabled device with it's port in 
                     PTP Slave sends LOCKED message and re-enters to it's    
                     port WR state LOCKED on expiry of WR_LOCKED_TIMEOUT.    
                                                                             
 Reference         : White Rabbit Specification v2.0 July 2011, Clause 6.7.3 
                     Pages 35, Figure 27 Page 62                             
                                                                             
 Conformance Type  : MUST                                                    
                                                                             
