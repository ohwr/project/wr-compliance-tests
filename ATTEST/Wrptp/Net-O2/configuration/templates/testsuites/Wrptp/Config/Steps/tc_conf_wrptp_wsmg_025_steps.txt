                                                                             
 Procedure         :                                                         
                                                                             
 (Initial Part)                                                              
                                                                             
 Step 1 : Initialization of DUT                                              
     i.   Enable DUT's port P1.                                              
    ii.   Enable PTP on port P1.                                             
   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   
    iv.   Configure clock mode as One-step/Two-step.                         
     v.   Configure Network Transport Protocol as IEEE 802.3/Ethernet.       
    vi.   Configure default values for Priority1 = X and Priority2 = Y.      
   vii.   Enable WRPTP on port P1.                                           
  viii.   Configure wrConfig = WR_M_AND_S.                                   
    ix.   Configure default values for knownDeltaTx and knownDeltaRx on P1.  
                                                                             
 Step 2 : Initialization of TEE                                              
     i.   Add port T1 at TEE.                                                
                                                                             
 (Part 1)                                                                    
                                                                             
 Step 3 : Send periodic WRPTP ANNOUNCE messages on the port T1 with following
          parameters.                                                        
                                                                             
              PTP Header                                                     
                  messageType          = 0x0B                                
                  domainNumber         = DN1                                 
                  grandmasterPriority1 = X - 1                               
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageId          = 0x2000                              
                  wrConfig             = 0x3                                 
                  calibrated           = 1                                   
                  wrModeOn             = 0                                   
                                                                             
 Step 4 : Wait until completion of BMCA and observe that DUT transmits WRPTP 
          SLAVE_PRESENT message on port P1 with following parameters. Store  
          sourcePortIdentity as SPI1.                                        
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageID          = 0x1000                              
                                                                             
 Step 5 : Send WRPTP LOCK message on port T1 with following parameters.      
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
                  targetPortIdentity   = SPI1                                
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageId          = 0x1001                              
                                                                             
 Step 6 : Observe that the DUT transmits WRPTP LOCKED message on port P1 with
          following parameters.                                              
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageID          = 0x1002                              
                                                                             
 Step 7 : Send WRPTP CALIBRATE message on port T1 with following parameters. 
                                                                             
              PTP Header                                                     
                  messageType          = 0x0C                                
                  domainNumber         = DN1                                 
                  targetPortIdentity   = SPI1                                
              TLV                                                            
                  tlvType              = 0x0003                              
                  wrMessageId          = 0x1003                              
                  calSendPattern       = FALSE                               
                  calRetry             = 0                                   
                  calPeriod            = 0                                   
                                                                             
 Step 8 : Wait for 150ms (0.5 x WR_RESP_CALIB_REQ_TIMEOUT) and observe that  
          WRPTP portState of port P1 is in RESP_CALIB_REQ state.             
                                                                             
 Step 9 : Wait for 300ms to complete 1 x WR_RESP_CALIB_REQ_TIMEOUT.          
                                                                             
 Step 10: Observe that WRPTP portState of port P1 is in RESP_CALIB_REQ state.
                                                                             
 Note :                                                                      
                                                                             
   Values mentioned in above steps are examples based on default values.     
   However, the test will be executed using the values given in ATTEST GUI   
   (Selected configuration in ATTEST Configuration Manager > Protocol        
   Options > WRPTP > WRPTP Attributes). Hence, the values displayed in test  
   logs may differ from those mentioned in the steps.                        
                                                                             
