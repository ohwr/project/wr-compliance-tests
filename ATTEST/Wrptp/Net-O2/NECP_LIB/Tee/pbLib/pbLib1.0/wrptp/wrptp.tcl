################################################################################
# File Name           : wrptp.tcl                                              #
# File Version        : 1.0                                                    #
# Component Name      : ATTEST Packet Library                                  #
# Module Name         : White Rabbit Precision Time Protocol                   #
################################################################################
# History    Date       Author     Addition/ Alteration                        #
#                                                                              #
#  1.0       Jun/2018   CERN       Initial                                     #
#                                                                              #
################################################################################
# Copyright (C) 2018 CERN                                                      #
################################################################################

############################# LIST OF PROCEDURES ###############################
#                                                                              #
#  1. pbLib::encode_eth_header                                                 #
#  2. pbLib::decode_eth_header                                                 #
#  3. pbLib::encode_vlan_header                                                #
#  4. pbLib::decode_vlan_header                                                #
#  5. pbLib::encode_arp_header                                                 #
#  6. pbLib::decode_arp_header                                                 #
#  7. pbLib::encode_ipv4_header                                                #
#  8. pbLib::decode_ipv4_header                                                #
#  9. pbLib::encode_udp_header                                                 #
# 10. pbLib::decode_udp_header                                                 #
# 11. pbLib::encode_wrptp_common_header                                        #
# 12. pbLib::decode_wrptp_common_header                                        #
# 13. pbLib::encode_wrptp_sync_header                                          #
# 14. pbLib::decode_wrptp_sync_header                                          #
# 15. pbLib::encode_wrptp_announce_header                                      #
# 16. pbLib::decode_wrptp_announce_header                                      #
# 17. pbLib::encode_wrptp_delay_req_header                                     #
# 18. pbLib::decode_wrptp_delay_req_header                                     #
# 19. pbLib::encode_wrptp_delay_resp_header                                    #
# 20. pbLib::decode_wrptp_delay_resp_header                                    #
# 21. pbLib::encode_wrptp_followup_header                                      #
# 22. pbLib::decode_wrptp_followup_header                                      #
# 23. pbLib::encode_wrptp_pdelay_req_header                                    #
# 24. pbLib::decode_wrptp_pdelay_req_header                                    #
# 25. pbLib::encode_wrptp_pdelay_resp_header                                   #
# 26. pbLib::decode_wrptp_pdelay_resp_header                                   #
# 27. pbLib::encode_wrptp_pdelay_resp_followup_header                          #
# 28. pbLib::decode_wrptp_pdelay_resp_followup_header                          #
# 29. pbLib::encode_wrptp_signal_header                                        #
# 30. pbLib::decode_wrptp_signal_header                                        #
# 31. pbLib::encode_wrptp_mgmt_header                                          #
# 32. pbLib::decode_wrptp_mgmt_header                                          #
# 33. pbLib::encode_wrptp_tlv                                                  #
# 34. pbLib::decode_wrptp_tlv                                                  #
#                                                                              #
# 35. pbLib::encode_arp_pkt                                                    #
# 36. pbLib::decode_arp_pkt                                                    #
# 37. pbLib::encode_wrptp_pkt                                                  #
# 38. pbLib::decode_wrptp_pkt                                                  #
#                                                                              #
# 39. pbLib::encode_port_identity                                              #
# 40. pbLib::push_vlan_tag                                                     #
#                                                                              #
################################################################################

namespace eval pbLib {}

################################################################################
#  PROCEDURE NAME       : pbLib::encode_eth_header                             #
#                                                                              #
#  DEFINITION           : This function encodes ethernet header.               #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  dest_mac             : (Optional) Destination MAC address of eth header     #
#                                    ( Default : 00:00:00:00:00:AA )           #
#                                                                              #
#  src_mac              : (Optional) Source MAC address of eth header          #
#                                    ( Default : 00:00:00:00:00:BB )           #
#                                                                              #
#  eth_type             : (Optional) Ethernet type/length of eth header        #
#                                    ( Default : ffff )                        #
#                                                                              #
#  OUTPUT PARAMETERS    : NONE                                                 #
#                                                                              #
#  RETURNS              : Ethernet header hex dump                             #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#             pbLib::encode_eth_header\                                        #
#                                      -dest_mac      $dest_mac \              #
#                                      -src_mac       $src_mac  \              #
#                                      -eth_type      $eth_type                #
#                                      -hexdump       hexdump                  #
#                                                                              #
################################################################################

proc pbLib::encode_eth_header {args} {

    array set param $args

    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)

    } else {

        set dest_mac "00:00:00:00:00:AA"
    }

    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)

    } else {

        set src_mac "00:00:00:00:00:BB"
    }

    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)

    } else {

        set eth_type FFFF
    }

    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    ethernet eth

    set eth::dmac     [mac2hex $dest_mac]
    set eth::smac     [mac2hex $src_mac]
    set eth::type     $eth_type

    set hexdump [eth::pkt]

    return $hexdump

}


################################################################################
#  PROCEDURE NAME       : pbLib::encode_vlan_header                            #
#                                                                              #
#  DEFINITION           : This function encodes vlan header.                   #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  vlan_id              : (Optional) Vlan id of vlan header                    #
#                                    ( Default : 0 )                           #
#                                                                              #
#  priority             : (Optional) Priority of vlan header                   #
#                                    ( Default : 1 )                           #
#                                                                              #
#  dei                  : (Optional) Eligible indicator of vlan header         #
#                                    ( Default : 0 )                           #
#                                                                              #
#  next_protocol        : (Optional) Next protocol of vlan header              #
#                                    ( Default : ffff )                        #
#                                                                              #
#  OUTPUT PARAMETERS    : NONE                                                 #
#                                                                              #
#  RETURNS              : Vlan header hex dump                                 #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#          pbLib::encode_vlan_header\                                          #
#                                    -vlan_id         $vlan_id  \              #
#                                    -priority        $priority \              #
#                                    -dei             $dei      \              #
#                                    -next_protocol   $next_protocol           #
#                                    -hexdump         hexdump                  #
#                                                                              #
################################################################################

proc pbLib::encode_vlan_header {args} {

    array set param $args

    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)

    } else {

        set vlan_id 0
    }

    if {[info exists param(-priority)]} {

        set priority $param(-priority)

    } else {

        set priority 1
    }


    if {[info exists param(-dei)]} {

        set dei $param(-dei)

    } else {

        set dei 0
    }

    if {[info exists param(-next_protocol)]} {

        set next_protocol $param(-next_protocol)

    } else {

        set next_protocol "FFFF"
    }

    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }


    vlan virtual

    set virtual::priority [dec2hex $priority 2]
    set virtual::cfi      [dec2hex $dei 2]
    set virtual::vlan     [dec2hex $vlan_id 4]
    set virtual::typelen  $next_protocol

    set hexdump [virtual::pkt]

    return $hexdump

}


################################################################################
#  PROCEDURE NAME       : pbLib::encode_arp_header                             #
#                                                                              #
#  DEFINITION           : This function encodes arp header.                    #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  hardware_type        : (Optional) Specifies the network protocol type       #
#                                    ( Default : 1 )                           #
#                                                                              #
#  protocol_type        : (Optional) Specifies the internetwork protocol for   #
#                                    which the ARP request is intended.        #
#                                    ( Default : 0800 )                        #
#                                                                              #
#  hardware_size        : (Optional) Length of a hardware address              #
#                                    ( Default : 6 )                           #
#                                                                              #
#  protocol_size        : (Optional) Length addresses used in the upper layer  #
#                                    protocol. ( Default : 4 )                 #
#                                                                              #
#  message_type         : (Optional) Specifies the operation that the sender   #
#                                    is performing: 1 - request, 2 - reply.    #
#                                    ( Default : 1 )                           #
#                                                                              #
#  sender_mac           : (Optional) Sender hardware address                   #
#                                    ( Default : 00:00:00:00:00:00 )           #
#                                                                              #
#  sender_ip            : (Optional) Sender protocol address                   #
#                                    ( Default : 0.0.0.0 )                     #
#                                                                              #
#  target_mac           : (Optional) Target hardware address                   #
#                                    ( Default : 00:00:00:00:00:00 )           #
#                                                                              #
#  target_ip            : (Optional) Target protocol address                   #
#                                    ( Default : 0.0.0.0 )                     #
#                                                                              #
#  OUTPUT PARAMETERS    : NONE                                                 #
#                                                                              #
#  RETURNS              : Arp header hex dump                                  #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#             pbLib::encode_arp_header      -hardware_type   $hardware_typ  \  #
#                                    -protocol_type   $protocol_type \         #
#                                    -hardware_size   $hardware_size \         #
#                                    -protocol_size   $protocol_size \         #
#                                    -message_type    $message_type \          #
#                                    -sender_mac      $sender_mac \            #
#                                    -sender_ip       $sender_ip \             #
#                                    -target_mac      $target_mac \            #
#                                    -target_ip       $target_ip               #
#                                    -hexdump         hexdump                  #
#                                                                              #
################################################################################

proc pbLib::encode_arp_header {args} {

    array set param $args

    if {[info exists param(-hardware_type)]} {

        set hardware_type $param(-hardware_type)

    } else {

        set hardware_type 1

    }

    if {[info exists param(-protocol_type)]} {

        set protocol_type $param(-protocol_type)

    } else {

        set protocol_type 0800

    }

    if {[info exists param(-hardware_size)]} {

        set hardware_size $param(-hardware_size)

    } else {

        set hardware_size 06

    }

    if {[info exists param(-protocol_size)]} {

        set protocol_size $param(-protocol_size)

    } else {

        set protocol_size 04
    }

    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)

    } else {

        set message_type 1
    }

    if {[info exists param(-sender_mac)]} {

        set sender_mac $param(-sender_mac)

    } else {

        set sender_mac "00:00:00:00:00:00"
    }


    if {[info exists param(-sender_ip)]} {

        set sender_ip $param(-sender_ip)

    } else {

        set sender_ip "0.0.0.0"
    }


    if {[info exists param(-target_mac)]} {

        set target_mac $param(-target_mac)

    } else {

        set target_mac "00:00:00:00:00:00"
    }


    if {[info exists param(-target_ip)]} {

        set target_ip $param(-target_ip)

    } else {

        set target_ip "0.0.0.0"
    }

    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    arp Arp

    set Arp::hw_type     [dec2hex $hardware_type 2]
    set Arp::proto_type  $protocol_type
    set Arp::hw_size     [dec2hex $hardware_size 2]
    set Arp::proto_size  [dec2hex $protocol_size 2]
    set Arp::opcode      [dec2hex $message_type 2]
    set Arp::src_mac     [mac2hex $sender_mac]
    set Arp::src_ip      [ip2hex $sender_ip]
    set Arp::dest_mac    [mac2hex $target_mac]
    set Arp::dest_ip     [ip2hex $target_ip]

    set hexdump [Arp::pkt]

    return $hexdump

}


################################################################################
#  PROCEDURE NAME       : pbLib::encode_ipv4_header                            #
#                                                                              #
#  DEFINITION           : This function encodes ipv4 header.                   #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  version              : (Optional) Version of ip                             #
#                                    ( Default : 4 )                           #
#                                                                              #
#  header_length        : (Optional) Header length  of ipv4                    #
#                                    ( Default : 20)                           #
#                                                                              #
#  tos                  : (Optional) Type of service (hex value)               #
#                                    ( Default : 00 )                          #
#                                                                              #
#  total_length         : (Optional) Total length  of ipv4                     #
#                                    ( Default : 20 )                          #
#                                                                              #
#  identification       : (Optional) Unique id of ipv4                         #
#                                    ( Default : 0 )                           #
#                                                                              #
#  flags                : (Optional) Flags represents the control or identify  #
#                                    fragments. ( Default - 0 )                #
#                                    ( 0 - Reserved , 1 - Don't fragmetn ,     #
#                                      2 - More fragment )                     #
#                                                                              #
#  offset               : (Optional) Fragment offset                           #
#                                    ( Default - 0 )                           #
#                                                                              #
#  ttl                  : (Optional) Time to live                              #
#                                    ( Default - 64 )                          #
#                                                                              #
#  checksum             : (Optional) Error-checking of header(16-bit checksum) #
#                                    ( Default - auto )                        #
#                                                                              #
#  next_protocol        : (Optional) Protocol in the data portion of the       #
#                                    IP datagram. ( Default - ff )             #
#                                                                              #
#  src_ip               : (Optional) IPv4 address of the sender of the packet  #
#                                    ( Default - 0.0.0.0 )                     #
#                                                                              #
#  dest_ip              : (Optional) IPv4 address of the reciever of the packet#
#                                    ( Default - 0.0.0.0 )                     #
#                                                                              #
#  OUTPUT PARAMETERS    : NONE                                                 #
#                                                                              #
#  RETURNS              : Ipv4 header hex dump                                 #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#             pbLib::encode_ipv4_header     -version         $version  \       #
#                                    -header_length   $header_length \         #
#                                    -tos             $tos \                   #
#                                    -total_length    $total_length \          #
#                                    -identification  $id \                    #
#                                    -flags           $flags \                 #
#                                    -ttl             $ttl \                   #
#                                    -offset          $offset \                #
#                                    -checksum        $checksum \              #
#                                    -next_protocol   $next_protocol \         #
#                                    -src_ip          $src_ip \                #
#                                    -dest_ip         $dest_ip                 #
#                                    -hexdump         hexdump                  #
#                                                                              #
################################################################################

proc pbLib::encode_ipv4_header {args} {

    array set param $args

    if {[info exists param(-version)]} {

        set version $param(-version)

    } else {

        set version 4

    }

    if {[info exists param(-header_length)]} {

        set header_length $param(-header_length)

    } else {

        set header_length 20
    }

    if {[info exists param(-tos)]} {

        set tos $param(-tos)

    } else {

        set tos "00"
    }

    if {[info exists param(-total_length)]} {

        set total_length $param(-total_length)

    } else {

        set total_length 20

    }

    if {[info exists param(-identification)]} {

        set identification $param(-identification)

    } else {

        set identification 0
    }

    if {[info exists param(-flags)]} {

        set flags $param(-flags)

    } else {

        set flags 0

    }

    if {[info exists param(-offset)]} {

        set offset $param(-offset)

    } else {

        set offset 0
    }

    if {[info exists param(-ttl)]} {

        set ttl $param(-ttl)

    } else {

        set ttl 64

    }


    if {[info exists param(-checksum)]} {

        set checksum $param(-checksum)

    } else {

        set checksum "auto"
    }

    if {[info exists param(-next_protocol)]} {

        set next_protocol $param(-next_protocol)

    } else {

        set next_protocol "ff"

    }

    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)

    } else {

        set src_ip 0.0.0.0
    }

    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)

    } else {

        set dest_ip 0.0.0.0

    }

    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    set auto_checksum 0
    if { [string match -nocase "auto" $checksum] } {
        set checksum 0000
        set auto_checksum 1
    }

    set hdr_len [expr $header_length/4]

    ip ipv4

    set ipv4::version        [dec2hex $version 1]
    set ipv4::hdrlen         [dec2hex $hdr_len 1]
    set ipv4::tos            $tos
    set ipv4::len            [dec2hex $total_length 4]
    set ipv4::id             [dec2hex $identification 2]
    set ipv4::flags          [dec2hex $flags 2]
    set ipv4::offset         [dec2hex $offset 2]
    set ipv4::ttl            [dec2hex $ttl 2]
    set ipv4::proto          [dec2hex $next_protocol 2]
    set ipv4::checksum       $checksum
    set ipv4::srcip          [ip2hex  $src_ip]
    set ipv4::dstip          [ip2hex  $dest_ip]

    set hexdump [ipv4::pkt]

    if {$auto_checksum} {

        #Hexdump with calculating checksum
        set ipv4::checksum [calculate_checksum $hexdump]
        set hexdump [ipv4::pkt]
    }

    return $hexdump

}

################################################################################
#  PROCEDURE NAME       : pbLib::encode_udp_header                             #
#                                                                              #
#  DEFINITION           : This function encodes udp header.                    #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  src_port             : (Optional) Sender's port number                      #
#                                    ( Default - 0 )                           #
#                                                                              #
#  dest_port            : (Optional) Receiver's port number                    #
#                                    ( Default - 0 )                           #
#                                                                              #
#  payload_length       : (Optional) Length in bytes of UDP header and data    #
#                                    ( Default - 8 )                           #
#                                                                              #
#  checksum             : (Optional) Error-checking of the header              #
#                                    ( Default - auto )                        #
#                                                                              #
#  pseudo_header        : (Optional) Pseudo header for checksum calculation    #
#                                                                              #
#  OUTPUT PARAMETERS    : NONE                                                 #
#                                                                              #
#  RETURNS              : Udp header hex dump                                  #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#             pbLib::encode_udp_header\                                        #
#                                    -src_port       $src_port \               #
#                                    -dest_port      $dest_port \              #
#                                    -payload_length $length \                 #
#                                    -checksum       $checksum                 #
#                                    -pseudo_header  $pseudo_header\           #
#                                    -hexdump        hexdump                   #
#                                                                              #
################################################################################

proc pbLib::encode_udp_header {args} {

    array set param $args

    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)

    } else {

        set src_port 0
    }

    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)

    } else {

        set dest_port 0
    }

    if {[info exists param(-payload_length)]} {

        set payload_length $param(-payload_length)

    } else {

        set payload_length 8
    }

    if {[info exists param(-checksum)]} {

        set checksum $param(-checksum)

    } else {

        set checksum "auto"
    }

    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    if {[info exists param(-pseudo_header)]} {

        set pseudo_header $param(-pseudo_header)
    } else {
        set pseudo_header ""
    }

    if {[info exists param(-data)]} {

        set data $param(-data)
    } else {
        set data ""
    }

    set auto_checksum 0
    if { [string match -nocase "auto" $checksum] } {
        set checksum 0000
        set auto_checksum 1
    }

    if {$pseudo_header == ""} {
        set auto_checksum 0
    }

    udp Udp

    set Udp::srcport  [dec2hex $src_port 2]
    set Udp::dstport  [dec2hex $dest_port 2]
    set Udp::length   [dec2hex $payload_length 4]
    set Udp::checksum $checksum

    set hexdump [Udp::pkt]

    if {$auto_checksum} {
        append pseudo_header $Udp::length
        append pseudo_header $hexdump
        append pseudo_header $data

        set Udp::checksum [calculate_checksum $pseudo_header]
        set hexdump [Udp::pkt]
    }

    return $hexdump

}

################################################################################
#  PROCEDURE NAME       : pbLib::decode_eth_header                             #
#                                                                              #
#  DEFINITION           : This function decodes ethernet header.               #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  hex                  : (Mandatory) Ethernet hex dump                        #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  dest_mac             : (Optional) Destination MAC address of eth header     #
#                                                                              #
#  src_mac              : (Optional) Source MAC address of eth header          #
#                                                                              #
#  eth_type/next_protocol : (Optional) Ethernet type/length of eth header      #
#                                                                              #
#  RETURNS              : None                                                 #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#             pbLib::decode_eth_header        -hex           $hex \            #
#                                      -dest_mac      dest_mac \               #
#                                      -src_mac       src_mac  \               #
#                                      -eth_type      eth_type                 #
#                                                                              #
################################################################################

proc pbLib::decode_eth_header {args} {

    array set param $args

    if {[info exists param(-hex)]} {

        set hex $param(-hex)

    } else {

        return -code error "Mandatory parameter missing - hex"
    }

    if {[info exists param(-dest_mac)]} {

        upvar $param(-dest_mac) dmac
    }

    if {[info exists param(-src_mac)]} {
        upvar $param(-src_mac) smac
    }

    if {[info exists param(-next_protocol)]} {
        upvar $param(-next_protocol) next
    }

    if {[info exists param(-eth_type)]} {
        upvar $param(-eth_type) next
    }

    ethernet eth = $hex

    set dmac [hex2mac $eth::dmac]
    set smac [hex2mac $eth::smac]
    set next $eth::type

}

################################################################################
#  PROCEDURE NAME       : pbLib::decode_vlan_header                            #
#                                                                              #
#  DEFINITION           : This function decodes vlan header.                   #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  hex                  : (Mandatory) vlan hex dump                            #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  vlan_id              : (Optional) Vlan id of vlan header                    #
#                                                                              #
#  priority             : (Optional) Priority of vlan header                   #
#                                                                              #
#  dei                  : (Optional) Eligible indicator of vlan header         #
#                                                                              #
#  next_protocol        : (Optional) Next protocol of vlan header              #
#                                                                              #
#  RETURNS              : None                                                 #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#          pbLib::decode_vlan_header        -hex             $hex \            #
#                                    -vlan_id         vlan_id  \               #
#                                    -priority        priority \               #
#                                    -dei             dei      \               #
#                                    -next_protocol   next_protocol            #
#                                                                              #
################################################################################

proc pbLib::decode_vlan_header {args} {

    array set param $args

    if {[info exists param(-hex)]} {

        set hex $param(-hex)

    } else {

        return -code error "Mandatory parameter missing - hex"
    }


    if {[info exists param(-priority)]} {

        upvar $param(-priority) priority
    }

    if {[info exists param(-dei)]} {

        upvar $param(-dei) dei
    }

    if {[info exists param(-vlan_id)]} {

        upvar $param(-vlan_id) vlan_id
    }

    if {[info exists param(-next_protocol)]} {

        upvar $param(-next_protocol) next_protocol
    }


    vlan virtual = $hex

    set priority      [hex2dec $virtual::priority]
    set dei           [hex2dec $virtual::cfi]
    set vlan_id       [hex2dec $virtual::vlan]
    set next_protocol $virtual::typelen

}


################################################################################
#  PROCEDURE NAME       : pbLib::decode_arp_header                             #
#                                                                              #
#  DEFINITION           : This function decodes arp header.                    #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  hex                  : (Mandatory) Arp header hex dump                      #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  hardware_type        : (Optional) Specifies the network protocol type       #
#                                                                              #
#  protocol_type        : (Optional) Specifies the internetwork protocol for   #
#                         which the ARP request is intended                    #
#                                                                              #
#  hardware_size        : (Optional) Length of a hardware address              #
#                                                                              #
#  protocol_size        : (Optional) Length addresses used in the upper layer  #
#                                    protocol                                  #
#                                                                              #
#  message_type         : (Optional) Specifies the operation that the sender   #
#                                    is performing: 1 - request, 2 - reply.    #
#                                                                              #
#  sender_mac           : (Optional) Sender hardware address                   #
#                                                                              #
#  sender_ip            : (Optional) Sender protocol address                   #
#                                                                              #
#  target_mac           : (Optional) Target hardware address                   #
#                                                                              #
#  target_ip            : (Optional) Target protocol address                   #
#                                                                              #
#  RETURNS              : None                                                 #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#             pbLib::decode_arp_header\                                        #
#                                    -hex             $hex \                   #
#                                    -hardware_type   hardware_typ  \          #
#                                    -protocol_type   protocol_type \          #
#                                    -hardware_size   hardware_size \          #
#                                    -protocol_size   protocol_size \          #
#                                    -message_type    message_type \           #
#                                    -sender_mac      sender_mac \             #
#                                    -sender_ip       sender_ip \              #
#                                    -target_mac      target_mac \             #
#                                    -target_ip       target_ip                #
#                                                                              #
################################################################################

proc pbLib::decode_arp_header {args} {

    array set param $args

    if {[info exists param(-hex)]} {

        set hex $param(-hex)

    } else {

        return -code error "Mandatory parameter missing - hex"
    }


    if {[info exists param(-hardware_type)]} {

        upvar $param(-hardware_type) hardware_type
        set hardware_type ""
    }

    if {[info exists param(-protocol_type)]} {

        upvar $param(-protocol_type) protocol_type
        set protocol_type ""
    }

    if {[info exists param(-hardware_size)]} {

        upvar $param(-hardware_size) hardware_size
        set hardware_size ""
    }

    if {[info exists param(-protocol_size)]} {

        upvar $param(-protocol_size) protocol_size
        set protocol_size ""
    }

    if {[info exists param(-message_type)]} {

        upvar $param(-message_type) message_type
        set message_type ""
    }

    if {[info exists param(-sender_mac)]} {

        upvar $param(-sender_mac) sender_mac
        set sender_mac ""
    }

    if {[info exists param(-sender_ip)]} {

        upvar $param(-sender_ip) sender_ip
        set sender_ip ""
    }

    if {[info exists param(-target_mac)]} {

        upvar $param(-target_mac) target_mac
        set target_mac ""
    }

    if {[info exists param(-target_ip)]} {

        upvar $param(-target_ip) target_ip
        set target_ip ""
    }

    arp Arp = $hex

    set hardware_type  [hex2dec $Arp::hw_type]
    set protocol_type  $Arp::proto_type
    set hardware_size  [hex2dec $Arp::hw_size]
    set protocol_size  [hex2dec $Arp::proto_size]
    set message_type   [hex2dec $Arp::opcode]
    set sender_mac     [hex2mac $Arp::src_mac]
    set sender_ip      [hex2ip $Arp::src_ip]
    set target_mac     [hex2mac $Arp::dest_mac]
    set target_ip      [hex2ip $Arp::dest_ip]
}

################################################################################
#  PROCEDURE NAME       : pbLib::decode_ipv4_header                            #
#                                                                              #
#  DEFINITION           : This function decodes ipv4 header.                   #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  hex                  : (Mandatory) Ipv4 hex dump                            #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  version              : (Optional) Version of ip                             #
#                                                                              #
#  header_length        : (Optional) Header length  of ipv4                    #
#                                                                              #
#  tos                  : (Optional) Type of service                           #
#                                                                              #
#  total_length         : (Optional) Total length  of ipv4                     #
#                                                                              #
#  identification       : (Optional) Unique id of ipv4                         #
#                                                                              #
#  flags                : (Optional) Flags represents the control or identify  #
#                                    fragments.                                #
#                                    ( 0 - Reserved , 1 - Don't fragmetn ,     #
#                                      2 - More fragment )                     #
#                                                                              #
#  offset               : (Optional) Fragment offset                           #
#                                                                              #
#  ttl                  : (Optional) Time to live                              #
#                                                                              #
#  checksum             : (Optional) Error-checking of the header              #
#                                                                              #
#  next_protocol        : (Optional) Protocol in the data portion of the       #
#                                    IP datagram                               #
#                                                                              #
#  src_ip               : (Optional) IPv4 address of the sender of the packet  #
#                                                                              #
#  dest_ip              : (Optional) IPv4 address of the reciever of the packet#
#                                                                              #
#  RETURNS              : None                                                 #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#             pbLib::decode_ipv4_header\                                       #
#                            -hex                     $hex\                    #
#                            -version                 version\                 #
#                            -header_length           header_length\           #
#                            -tos                     tos\                     #
#                            -total_length            total_length\            #
#                            -identification          id\                      #
#                            -flags                   flags\                   #
#                            -ttl                     ttl\                     #
#                            -offset                  offset\                  #
#                            -checksum                checksum\                #
#                            -next_protocol           next_protocol\           #
#                            -src_ip                  src_ip\                  #
#                            -dest_ip                 dest_ip\                 #
#                            -checksum_status         checksum_status\         #
#                            -checksum_expected       checksum_expected        #
#                                                                              #
################################################################################

proc pbLib::decode_ipv4_header {args} {

    array set param $args

    if {[info exists param(-hex)]} {

        set hex $param(-hex)

    } else {

        return -code error "Mandatory parameter missing - hex"
    }


    if {[info exists param(-type)]} {

        upvar $param(-type) type
        set type ""
    }

    if {[info exists param(-version)]} {

        upvar $param(-version) version
        set version ""
    }


    if {[info exists param(-header_length)]} {

        upvar $param(-header_length) header_length
        set header_length ""
    }

    if {[info exists param(-tos)]} {

        upvar $param(-tos) tos
        set tos ""
    }

    if {[info exists param(-total_length)]} {

        upvar $param(-total_length) total_length
        set total_length ""
    }

    if {[info exists param(-identification)]} {

        upvar $param(-identification) identification
        set identification ""
    }

    if {[info exists param(-flags)]} {

        upvar $param(-flags) flags
        set flags ""
    }

    if {[info exists param(-offset)]} {

        upvar $param(-offset) offset
        set offset ""
    }

    if {[info exists param(-ttl)]} {

        upvar $param(-ttl) ttl
        set ttl ""
    }


    if {[info exists param(-checksum)]} {

        upvar $param(-checksum) checksum
        set checksum ""
    }

    if {[info exists param(-next_protocol)]} {

        upvar $param(-next_protocol) next_protocol
        set next_protocol ""
    }

    if {[info exists param(-src_ip)]} {

        upvar $param(-src_ip) src_ip
        set src_ip ""
    }

    if {[info exists param(-dest_ip)]} {

        upvar $param(-dest_ip) dest_ip
        set dest_ip ""
    }

    if {[info exists param(-checksum_status)]} {

        upvar $param(-checksum_status) checksum_status
        set checksum_status ""
    }

    if {[info exists param(-checksum_expected)]} {

        upvar $param(-checksum_expected) checksum_expected
        set checksum_expected ""
    }

    ip ipv4 = $hex

    set version         [hex2dec $ipv4::version]
    set header_length   [expr [hex2dec $ipv4::hdrlen] * 4]
    set tos             $ipv4::tos
    set total_length    [hex2dec $ipv4::len]
    set identification  [hex2dec $ipv4::id]
    set flags           [hex2dec $ipv4::flags]
    set offset          [hex2dec $ipv4::offset]
    set ttl             [hex2dec $ipv4::ttl]
    set next_protocol   [hex2dec $ipv4::proto]
    set checksum        $ipv4::checksum
    set src_ip          [hex2ip $ipv4::srcip]
    set dest_ip         [hex2ip $ipv4::dstip]

    #Checksum validation
    set ipv4::checksum      0000
    set checksum_status     [expr !0x[calculate_checksum $hex]]
    set checksum_expected   [calculate_checksum [ipv4::pkt]]

}

################################################################################
#  PROCEDURE NAME       : pbLib::decode_udp_header                             #
#                                                                              #
#  DEFINITION           : This function decodes udp header.                    #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  hex                  : Udp hex dump                                         #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  src_port             : (Optional) Sender's port number                      #
#                                                                              #
#  dest_port            : (Optional) Receiver's port number                    #
#                                                                              #
#  payload_length       : (Optional) Length in bytes of UDP header and data    #
#                                                                              #
#  checksum             : (Optional) Error-checking of the header              #
#                                                                              #
#  pseudo_header        : (Optional) Pseudo header for checksum calculation    #
#                                                                              #
#  checksum_status      : (Optional) Status of UDP Checksum                    #
#                                                                              #
#  checksum_expected    : (Optional) Expected UDP Checksum                     #
#                                                                              #
#  RETURNS              : None                                                 #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#             pbLib::decode_udp_header\                                        #
#                            -hex                    $hex\                     #
#                            -src_port               src_port\                 #
#                            -dest_port              dest_port\                #
#                            -payload_length         length\                   #
#                            -checksum               checksum\                 #
#                            -pseudo_header          $pseudo_header\           #
#                            -checksum_status        checksum_status\          #
#                            -checksum_expected      checksum_expected         #
#                                                                              #
################################################################################

proc pbLib::decode_udp_header {args} {

    array set param $args

    if {[info exists param(-hex)]} {

        set hex $param(-hex)

    } else {

        return -code error "Mandatory parameter missing - hex"
    }

    if {[info exists param(-src_port)]} {

        upvar $param(-src_port) src_port
        set src_port ""
    }

    if {[info exists param(-dest_port)]} {

        upvar $param(-dest_port) dest_port
        set dest_port ""
    }

    if {[info exists param(-payload_length)]} {

        upvar $param(-payload_length) payload_length
        set payload_length ""
    }

    if {[info exists param(-checksum)]} {

        upvar $param(-checksum) checksum
        set checksum ""
    }

    if {[info exists param(-pseudo_header)]} {

        set pseudo_header $param(-pseudo_header)
    } else {
        set pseudo_header ""
    }

    if {[info exists param(-data)]} {

        set data $param(-data)
    } else {
        set data ""
    }

    if {[info exists param(-checksum_status)]} {

        upvar $param(-checksum_status) checksum_status
        set checksum_status ""
    }

    if {[info exists param(-checksum_expected)]} {

        upvar $param(-checksum_expected) checksum_expected
        set checksum_expected ""
    }

    udp Udp = $hex

    set src_port         [hex2dec $Udp::srcport]
    set dest_port        [hex2dec $Udp::dstport]
    set payload_length   [hex2dec $Udp::length]
    set checksum         $Udp::checksum

    #Checksum validation
    if {$checksum == "0000"} {
        set checksum_status 1
    } else {
        append pseudo_header $Udp::length
        set checksum_status [expr !0x[calculate_checksum $pseudo_header[Udp::pkt]$data]]
    }

    set Udp::checksum       0000
    set checksum_expected   [calculate_checksum $pseudo_header[Udp::pkt]$data]

}

################################################################################
#  PROCEDURE NAME       : push_vlan_tag                                        #
#                                                                              #
#  DEFINITION           : This function used to push vlan in given packet      #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  packet               : (Mandatory) packet hex dump                          #
#                                                                              #
#  vlan_id              : (Optional) Vlan id of vlan header                    #
#                                    ( Default : 0 )                           #
#                                                                              #
#  priority             : (Optional) Priority of vlan header                   #
#                                    ( Default : 1 )                           #
#                                                                              #
#  dei                  : (Optional) Eligible indicator of vlan header         #
#                                    ( Default : 0 )                           #
#                                                                              #
#  RETURNS              : Vlan tagged packet hex dump                          #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#          push_vlan_tag             -packet          $packet \                #
#                                    -vlan_id         $vlan_id  \              #
#                                    -priority        $priority \              #
#                                    -dei             $dei \                   #
#                                    -next_protocol   $next_protocol           #
#                                                                              #
################################################################################

proc pbLib::push_vlan_tag {args} {

    array set param $args

    if {[info exists param(-packet)]} {

        set hex $param(-packet)

    } else {

        return -code error "Mandatory parameter missing - packet"
    }


    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)

    } else {

        set vlan_id 0
    }

    if {[info exists param(-type)]} {

        set type $param(-type)

    } else {

        set type "8100"
    }

    if {[info exists param(-priority)]} {

        set priority $param(-priority)

    } else {

        set priority 1
    }


    if {[info exists param(-dei)]} {

        set dei $param(-dei)

    } else {

        set dei 0
    }

    if {[info exists param(-next_protocol)]} {

        set next_protocol $param(-next_protocol)

    } else {

        set next_protocol "FFFF"
    }


    set vlan_hex [pbLib::encode_vlan_header \
                        -vlan_id        $vlan_id\
                        -next_protocol  $next_protocol \
                        -priority       $priority\
                        -dei            $dei]

    set vlan_hex [string range $type$vlan_hex 0 7]

    set eth_dump [string range $hex 0 23]
    set rem_dump [string range $hex 24 end]

    return $eth_dump$vlan_hex$rem_dump

}

################################################################################
#  PROCEDURE NAME       : pbLib::encode_arp_pkt                                #
#                                                                              #
#  DEFINITION           : This function encodes arp packet                     #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  ( ethernet )                                                                #
#                                                                              #
#  dest_mac             : (Optional) Destination MAC address of eth header     #
#                                    ( Default : 00:00:00:00:00:AA )           #
#                                                                              #
#  src_mac              : (Optional) Source MAC address of eth header          #
#                                    ( Default : 00:00:00:00:00:BB )           #
#                                                                              #
#  eth_type             : (Optional) Ethernet type/length of eth header        #
#                                    ( Default : 0806 )                        #
#                                                                              #
#  ( arp )                                                                     #
#                                                                              #
#  hardware_type        : (Optional) Specifies the network protocol type       #
#                                    ( Default : 1 )                           #
#                                                                              #
#  protocol_type        : (Optional) Specifies the internetwork protocol for   #
#                                    which the ARP request is intended.        #
#                                    ( Default : 0800 )                        #
#                                                                              #
#  hardware_size        : (Optional) Length of a hardware address              #
#                                    ( Default : 6 )                           #
#                                                                              #
#  protocol_size        : (Optional) Length addresses used in the upper layer  #
#                                    protocol. ( Default : 4 )                 #
#                                                                              #
#  message_type         : (Optional) Specifies the operation that the sender   #
#                                    is performing: 1 - request, 2 - reply.    #
#                                    ( Default : 1 )                           #
#                                                                              #
#  sender_mac           : (Optional) Sender hardware address                   #
#                                    ( Default : 00:00:00:00:00:00 )           #
#                                                                              #
#  sender_ip            : (Optional) Sender protocol address                   #
#                                    ( Default : 0.0.0.0 )                     #
#                                                                              #
#  target_mac           : (Optional) Target hardware address                   #
#                                    ( Default : 00:00:00:00:00:00 )           #
#                                                                              #
#  target_ip            : (Optional) Target protocol address                   #
#                                    ( Default : 0.0.0.0 )                     #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  hexdump               :  (Optional)  Hex dump of ARP packet                 #
#                                                                              #
#  RETURNS              : Arp packet hex dump                                  #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#                   pbLib::encode_arp_pkt\                                     #
#                                    -dest_mac        $dest_mac \              #
#                                    -src_mac         $src_mac  \              #
#                                    -eth_type        $eth_type \              #
#                                    -hardware_type   $hardware_typ  \         #
#                                    -protocol_type   $protocol_type \         #
#                                    -hardware_size   $hardware_size \         #
#                                    -protocol_size   $protocol_size \         #
#                                    -message_type    $message_type \          #
#                                    -sender_mac      $sender_mac \            #
#                                    -sender_ip       $sender_ip \             #
#                                    -target_mac      $target_mac \            #
#                                    -target_ip       $target_ip               #
#                                                                              #
################################################################################

proc pbLib::encode_arp_pkt {args} {

    array set param $args

    #ETHERNET PARAMETERS
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)

    } else {

        set dest_mac "00:00:00:00:00:AA"
    }

    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)

    } else {

        set src_mac "00:00:00:00:00:BB"
    }

    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)

    } else {

        set eth_type "0806"
    }


    #ETHERNET CONSTRUCT
    set eth_hex [pbLib::encode_eth_header \
                -dest_mac  $dest_mac\
                -src_mac   $src_mac\
                -eth_type  $eth_type]


    #ARP PARAMETERS
    if {[info exists param(-hardware_type)]} {

        set hardware_type $param(-hardware_type)

    } else {

        set hardware_type 1

    }


    if {[info exists param(-protocol_type)]} {

        set protocol_type $param(-protocol_type)

    } else {

        set protocol_type 0800

    }

    if {[info exists param(-hardware_size)]} {

        set hardware_size $param(-hardware_size)

    } else {

        set hardware_size 06

    }

    if {[info exists param(-protocol_size)]} {

        set protocol_size $param(-protocol_size)

    } else {

        set protocol_size 04
    }

    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)

    } else {

        set message_type 1
    }

    if {[info exists param(-sender_mac)]} {

        set sender_mac $param(-sender_mac)

    } else {

        set sender_mac "00:00:00:00:00:00"
    }


    if {[info exists param(-sender_ip)]} {

        set sender_ip $param(-sender_ip)

    } else {

        set sender_ip "0.0.0.0"
    }


    if {[info exists param(-target_mac)]} {

        set target_mac $param(-target_mac)

    } else {

        set target_mac "00:00:00:00:00:00"
    }


    if {[info exists param(-target_ip)]} {

        set target_ip $param(-target_ip)

    } else {

        set target_ip "0.0.0.0"
    }

    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    set arp_hex [pbLib::encode_arp_header\
                -hardware_type $hardware_type\
                -protocol_type $protocol_type\
                -hardware_size $hardware_size\
                -protocol_size $protocol_size\
                -message_type  $message_type\
                -sender_mac    $sender_mac\
                -sender_ip     $sender_ip\
                -target_mac    $target_mac\
                -target_ip     $target_ip]

    set hexdump $eth_hex$arp_hex
    return 1

}

################################################################################
#  PROCEDURE NAME       : pbLib::decode_arp_pkt                                #
#                                                                              #
#  DEFINITION           : This function decodes arp packet                     #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  packet               : (Mandatory) arp packet hex dump                      #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  ( ethernet )                                                                #
#                                                                              #
#  dest_mac             : (Optional) Destination MAC address of eth header     #
#                                                                              #
#  src_mac              : (Optional) Source MAC address of eth header          #
#                                                                              #
#  eth_type             : (Optional) Ethernet type/length of eth header        #
#                                                                              #
#  ( if packet is single tagged )                                              #
#                                                                              #
#  vlan_id              : (Optional) Vlan id of vlan header                    #
#                                                                              #
#  vlan_priority        : (Optional) Priority of vlan header                   #
#                                                                              #
#  vlan_ dei            : (Optional) Eligible indicator of vlan header         #
#                                                                              #
#  vlan_next            : (Optional) Next protocol of vlan header              #
#                                                                              #
#  ( if packet is double tagged )                                              #
#                                                                              #
#  outer_vlan_id        : (Optional) Vlan id of outer vlan                     #
#                                                                              #
#  outer_vlan_priority  : (Optional) Priority of outer vlan                    #
#                                                                              #
#  outer_vlan_ dei      : (Optional) Eligible indicator of outer vlan          #
#                                                                              #
#  outer_vlan_next      : (Optional) Next protocol of outer vlan header        #
#                                                                              #
#  inner_vlan_id        : (Optional) Vlan id of inner vlan                     #
#                                                                              #
#  inner_vlan_priority  : (Optional) Priority of inner vlan                    #
#                                                                              #
#  inner_vlan_ dei      : (Optional) Eligible indicator of inner vlan          #
#                                                                              #
#  inner_vlan_next      : (Optional) Next protocol of inner vlan header        #
#                                                                              #
#  ( arp )                                                                     #
#                                                                              #
#  hardware_type        : (Optional) Specifies the network protocol type       #
#                                                                              #
#  protocol_type        : (Optional) Specifies the internetwork protocol for   #
#                         which the ARP request is intended                    #
#                                                                              #
#  hardware_size        : (Optional) Length of a hardware address              #
#                                                                              #
#  protocol_size        : (Optional) Length addresses used in the upper layer  #
#                                    protocol                                  #
#                                                                              #
#  message_type         : (Optional) Specifies the operation that the sender   #
#                                    is performing: 1 - request, 2 - reply.    #
#                                                                              #
#  sender_mac           : (Optional) Sender hardware address                   #
#                                                                              #
#  sender_ip            : (Optional) Sender protocol address                   #
#                                                                              #
#  target_mac           : (Optional) Target hardware address                   #
#                                                                              #
#  target_ip            : (Optional) Target protocol address                   #
#                                                                              #
#  RETURNS              : None                                                 #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#             pbLib::decode_arp_pkt\                                           #
#                                    -packet           $packet                 #
#                                    -dest_mac         dest_mac \              #
#                                    -src_mac          src_mac  \              #
#                                    -eth_type         eth_type \              #
#                                    -version          version  \              #
#                                    -hex              hex \                   #
#                                    -hardware_type    hardware_typ  \         #
#                                    -protocol_type    protocol_type \         #
#                                    -hardware_size    hardware_size \         #
#                                    -protocol_size    protocol_size \         #
#                                    -message_type     message_type \          #
#                                    -sender_mac       sender_mac \            #
#                                    -sender_ip        sender_ip \             #
#                                    -target_mac       target_mac \            #
#                                    -target_ip        target_ip               #
#                                                                              #
################################################################################

proc pbLib::decode_arp_pkt {args} {

    array set param $args

    if {[info exists param(-packet)]} {

        set packet $param(-packet)

    } else {

        return -code error "Mandatory parameter missing - packet"
    }

    # ETHERNET HEADER
    if {[info exists param(-dest_mac)]} {

        upvar $param(-dest_mac) dest_mac
        set dest_mac ""
    }

    if {[info exists param(-src_mac)]} {

        upvar $param(-src_mac) src_mac
        set src_mac ""
    }

    if {[info exists param(-eth_type)]} {

        upvar $param(-eth_type) eth_type
        set eth_type ""
    }

    # VLAN HEADER
    if { [info exists param(-vlan_priority)] } {

        upvar $param(-vlan_priority) vlan_priority
        set vlan_priority ""
    }

    if { [info exists param(-vlan_dei)] } {

        upvar $param(-vlan_dei) vlan_dei
        set vlan_dei ""
    }

    if {[info exists param(-vlan_id)]} {

        upvar $param(-vlan_id) vlan_id
        set vlan_id ""
    }

    if {[info exists param(-vlan_next)]} {

        upvar $param(-vlan_next) vlan_next
        set vlan_next ""
    }

    #upvaring ARP
    if {[info exists param(-hardware_type)]} {

        upvar $param(-hardware_type) hardware_type
        set hardware_type ""
    }

    if {[info exists param(-protocol_type)]} {

        upvar $param(-protocol_type) protocol_type
        set protocol_type ""
    }

    if {[info exists param(-hardware_size)]} {

        upvar $param(-hardware_size) hardware_size
        set hardware_size ""
    }

    if {[info exists param(-protocol_size)]} {

        upvar $param(-protocol_size) protocol_size
        set protocol_size ""
    }

    if {[info exists param(-message_type)]} {

        upvar $param(-message_type) message_type
        set message_type ""
    }

    if {[info exists param(-sender_mac)]} {

        upvar $param(-sender_mac) sender_mac
        set sender_mac ""
    }

    if {[info exists param(-sender_ip)]} {

        upvar $param(-sender_ip) sender_ip
        set sender_ip ""
    }

    if {[info exists param(-target_mac)]} {

        upvar $param(-target_mac) target_mac
        set target_mac ""
    }

    if {[info exists param(-target_ip)]} {

        upvar $param(-target_ip) target_ip
        set target_ip ""
    }

    set hex [string tolower $packet]

    #DECODING ETHERNET
    pbLib::decode_eth_header\
                 -hex               $hex\
                 -dest_mac          dest_mac\
                 -src_mac           src_mac\
                 -next_protocol     eth_type

    set next_protocol $eth_type
    set hex [string range $hex 28 end]

    while 1 {
        switch $next_protocol {

            "8100" {
                pbLib::decode_vlan_header\
                            -hex            $hex\
                            -priority       vlan_priority\
                            -dei            vlan_dei\
                            -vlan_id        vlan_id\
                            -next_protocol  vlan_next

                set next_protocol $vlan_next
                set hex [string range $hex 8 end]
                continue
            }

            "0806" {
                pbLib::decode_arp_header\
                        -hex            $hex\
                        -hardware_type  hardware_type\
                        -protocol_type  protocol_type\
                        -hardware_size  hardware_size\
                        -protocol_size  protocol_size\
                        -message_type   message_type\
                        -sender_mac     sender_mac\
                        -sender_ip      sender_ip\
                        -target_mac     target_mac\
                        -target_ip      target_ip
                break
            }

            default {
                return 0
            }
        }
    }

    return 1
}

################################################################################
#  PROCEDURE NAME       : pbLib::encode_wrptp_common_header                    #
#                                                                              #
#  DEFINITION           : This function encodes PTP common header.             #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  transport_specific    :  (Optional)  Transport specific                     #
#                                       (Default : 0)                          #
#                                                                              #
#  message_type          :  (Optional)  Message Type                           #
#                                       (Default : 0)                          #
#                                                                              #
#  version_ptp           :  (Optional)  PTP version number                     #
#                                       (Default : 2)                          #
#                                                                              #
#  message_length        :  (Optional)  PTP message length                     #
#                                       (Default : 44)                         #
#                                                                              #
#  domain_number         :  (Optional)  Domain number                          #
#                                       (Default : 0)                          #
#                                                                              #
#  alternate_master_flag :  (Optional)  Alternate Master flag                  #
#                                       (Default : 0)                          #
#                                                                              #
#  two_step_flag         :  (Optional)  Two Step Flag                          #
#                                       (Default : 0)                          #
#                                                                              #
#  unicast_flag          :  (Optional)  Unicast Flag                           #
#                                       (Default : 0)                          #
#                                                                              #
#  profile_specific1     :  (Optional)  Profile specific1 Flag                 #
#                                       (Default : 0)                          #
#                                                                              #
#  profile_specific2     :  (Optional)  Profile specific2 Flag                 #
#                                       (Default : 0)                          #
#                                                                              #
#  leap61                :  (Optional)  Leap61 Flag                            #
#                                       (Default : 0)                          #
#                                                                              #
#  leap59                :  (Optional)  Leap59 Flag                            #
#                                       (Default : 0)                          #
#                                                                              #
#  current_utc_offset_valid:(Optional)  Current UTC Offset Flag                #
#                                       (Default : 0)                          #
#                                                                              #
#  ptp_timescale         :  (Optional)  PTP TimeSclae Flag                     #
#                                       (Default : 0)                          #
#                                                                              #
#  time_traceable        :  (Optional)  Time Traceable Flag                    #
#                                       (Default : 0)                          #
#                                                                              #
#  freq_traceable        :  (Optional)  Frequency Traceable Flag               #
#                                       (Default : 0)                          #
#                                                                              #
#  correction_field      :  (Optional)  Correction Field value                 #
#                                       (Default : 0)                          #
#                                                                              #
#  src_port_number       :  (Optional)  Source port identity                   #
#                                       (Default : 0)                          #
#                                                                              #
#  src_clock_identity    :  (Optional)  Source Clock identity                  #
#                                       (Default : 0)                          #
#                                                                              #
#  sequence_id           :  (Optional)  Sequence ID                            #
#                                       (Default : 0)                          #
#                                                                              #
#  control_field         :  (Optional)  Control field                          #
#                                       (Default : 0)                          #
#                                                                              #
#  log_message_interval  :  (Optional)  Log message interval                   #
#                                       (Default : 0)                          #
#                                                                              #
#  OUTPUT PARAMETERS    : NONE                                                 #
#                                                                              #
#  hexdump               :  (Optional)  Hex dump of PTP Commom Header          #
#                                       (Default : 0)                          #
#                                                                              #
#                                                                              #
#  RETURNS              : PTP header hex dump                                  #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#     pbLib::encode_wrptp_common_header\                                       #
#                 -transport_specific                $transport_specific\      #
#                 -message_type                      $message_type\            #
#                 -version_ptp                       $version_ptp\             #
#                 -message_length                    $message_length\          #
#                 -domain_number                     $domain_number\           #
#                 -alternate_master_flag             $alternate_master_flag\   #
#                 -two_step_flag                     $two_step_flag\           #
#                 -unicast_flag                      $unicast_flag\            #
#                 -profile_specific1                 $profile_specific1\       #
#                 -profile_specific2                 $profile_specific2\       #
#                 -leap61                            $leap61\                  #
#                 -leap59                            $leap59\                  #
#                 -current_utc_offset_valid          $current_utc_offset_valid\#
#                 -ptp_timescale                     $ptp_timescale\           #
#                 -time_traceable                    $time_traceable\          #
#                 -freq_traceable                    $freq_traceable\          #
#                 -correction_field                  $correction_field\        #
#                 -src_port_number                   $src_port_number\         #
#                 -src_clock_identity                $src_clock_identity\      #
#                 -sequence_id                       $sequence_id\             #
#                 -control_field                     $control_field\           #
#                 -log_message_interval              $log_message_interval\    #
#                 -hexdump                           hexdump                   #
#                                                                              #
################################################################################

proc pbLib::encode_wrptp_common_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #Transport specific
    if {[info exists param(-transport_specific)]} {

        set transport_specific $param(-transport_specific)
    } else {

        set transport_specific 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
    } else {

        set message_type 0
    }

    #PTP version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
    } else {

        set version_ptp 2
    }

    #PTP message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
    } else {

        set message_length 44
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
    } else {

        set domain_number 0
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
    } else {

        set unicast_flag 0
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
    } else {

        set profile_specific2 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
    } else {

        set freq_traceable 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
    } else {

        set correction_field 0
    }

    #Source port number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
    } else {

        set src_port_number 0
    }

    #Source clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
    } else {

        set src_clock_identity 0
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
    } else {

        set sequence_id 0
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
    } else {

        set control_field 0
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
    } else {

        set log_message_interval 0
    }

    #OUTPUT PARAMETERS

    #Hex dump of PTP Commom Header
    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    #LOGIC GOES HERE
    #FLAGS
    wrptp_flags flags

    set flags::alternateMasterFlag         $alternate_master_flag
    set flags::twoStepFlag                 $two_step_flag
    set flags::unicastFlag                 $unicast_flag
    set flags::reserved1                   0
    set flags::profileSpecific1            $profile_specific1
    set flags::profileSpecific2            $profile_specific2
    set flags::reserved2                   0
    set flags::leap61                      $leap61
    set flags::leap59                      $leap59
    set flags::currentUtcOffsetValid       $current_utc_offset_valid
    set flags::ptpTimescale                $ptp_timescale
    set flags::timeTraceable               $time_traceable
    set flags::frequencyTraceable          $freq_traceable
    set flags::reserved3                   0

    set flag_field [flags::pkt]

    #Source port Identity
    wrptp_port_identity port_identity

    set port_identity::clockIdentity   [dec2hex $src_clock_identity 16]
    set port_identity::portNumber      [dec2hex $src_port_number 4]

    set source_port_identity [port_identity::pkt]


    #Header
    wrptp_common hdr

    set hdr::transportSpecific    [dec2hex $transport_specific 1]
    set hdr::messageType          [dec2hex $message_type 1]
    set hdr::reserved1            [dec2hex 0 1]
    set hdr::versionPTP           [dec2hex $version_ptp 1]
    set hdr::messageLength        [dec2hex $message_length 4]
    set hdr::domainNumber         [dec2hex $domain_number 2]
    set hdr::reserved2            [dec2hex 0 2]
    set hdr::flagField            $flag_field
    set hdr::correctionField      [dec2hex $correction_field 16]
    set hdr::reserved3            [dec2hex 0 8]
    set hdr::sourcePortIdentity   $source_port_identity
    set hdr::sequenceId           [dec2hex $sequence_id 4]
    set hdr::controlField         [dec2hex $control_field 2]
    set hdr::logMessageInterval   [dec2hex $log_message_interval 2]

    set hexdump [hdr::pkt]

    return $hexdump

}

################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::encode_wrptp_sync_header                      #
#                                                                              #
#  DEFINITION           : This function encodes PTP sync header.               #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  origin_timestamp_sec :  (Optional)  Origin timestamp in seconds             #
#                                      (Default : 0)                           #
#                                                                              #
#  origin_timestamp_ns  :  (Optional)  Origin timestamp in nanoseconds         #
#                                      (Default : 0)                           #
#                                                                              #
#  OUTPUT PARAMETERS    : NONE                                                 #
#                                                                              #
#  hexdump              :  (Optional)  Hex dump of PTP Sync Header             #
#                                                                              #
#                                                                              #
#  RETURNS              : PTP header hex dump                                  #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#    pbLib::encode_wrptp_sync_header\                                          #
#                -origin_timestamp_sec              $origin_timestamp_sec\     #
#                -origin_timestamp_ns               $origin_timestamp_ns\      #
#                -hexdump                           hexdump                    #
#                                                                              #
################################################################################

proc pbLib::encode_wrptp_sync_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #Origin timestamp in seconds
    if {[info exists param(-origin_timestamp_sec)]} {

        set origin_timestamp_sec $param(-origin_timestamp_sec)
    } else {

        set origin_timestamp_sec 0
    }

    #Origin timestamp in nanoseconds
    if {[info exists param(-origin_timestamp_ns)]} {

        set origin_timestamp_ns $param(-origin_timestamp_ns)
    } else {

        set origin_timestamp_ns 0
    }

    #OUTPUT PARAMETERS

    #Hex dump of PTP Sync Header
    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    #LOGIC GOES HERE

    #Timestamp
    wrptp_timestamp timestamp

    set timestamp::secondsField     [dec2hex $origin_timestamp_sec 12]
    set timestamp::nanosecondsField [dec2hex $origin_timestamp_ns 8]

    set origin_timestamp [timestamp::pkt]


    #Header
    wrptp_sync hdr

    set hdr::originTimestamp $origin_timestamp

    set hexdump [hdr::pkt]

    return $hexdump

}

################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::encode_wrptp_announce_header                  #
#                                                                              #
#  DEFINITION           : This function encodes PTP announce header            #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  origin_timestamp_sec :  (Optional)  Origin timestamp in seconds             #
#                                      (Default : 0)                           #
#                                                                              #
#  origin_timestamp_ns  :  (Optional)  Origin timestamp in nanoseconds         #
#                                      (Default : 0)                           #
#                                                                              #
#  current_utc_offset   :  (Optional)  Current UTC Offset                      #
#                                      (Default : 0)                           #
#                                                                              #
#  gm_priority1         :  (Optional)  Grand Master Priority 1                 #
#                                      (Default : 0)                           #
#                                                                              #
#  gm_priority2         :  (Optional)  Grand Master Priority 2                 #
#                                      (Default : 128)                         #
#                                                                              #
#  gm_identity          :  (Optional)  Grand Master Identity                   #
#                                      (Default : 0)                           #
#                                                                              #
#  steps_removed        :  (Optional)  Steps Removed                           #
#                                      (Default : 0)                           #
#                                                                              #
#  time_source          :  (Optional)  Time Source                             #
#                                      (Default : 0)                           #
#                                                                              #
#  gm_clock_class       :  (Optional)  Grand Master Clock class                #
#                                      (Default : 0)                           #
#                                                                              #
#  gm_clock_accuracy    :  (Optional)  Grand Master Clock accuracy             #
#                                      (Default : 0)                           #
#                                                                              #
#  gm_clock_variance    :  (Optional)  Grand Master Clock offset scaled        #
#                                        log variance (Default : 0)            #
#                                                                              #
#  OUTPUT PARAMETERS    : NONE                                                 #
#                                                                              #
#  hexdump              :  (Optional)  PTP Announce header hex dump            #
#                                                                              #
#  RETURNS              : PTP packet hex dump                                  #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#   pbLib::encode_wrptp_announce_header\                                       #
#               -origin_timestamp_sec              $origin_timestamp_sec\      #
#               -origin_timestamp_ns               $origin_timestamp_ns\       #
#               -current_utc_offset                $current_utc_offset\        #
#               -gm_priority1                      $gm_priority1\              #
#               -gm_priority2                      $gm_priority2\              #
#               -gm_identity                       $gm_identity\               #
#               -steps_removed                     $steps_removed\             #
#               -time_source                       $time_source\               #
#               -gm_clock_class                    $gm_clock_class\            #
#               -gm_clock_accuracy                 $gm_clock_accuracy\         #
#               -gm_clock_variance                 $gm_clock_variance\         #
#               -hexdump                           hexdump                     #
#                                                                              #
################################################################################

proc pbLib::encode_wrptp_announce_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #Origin timestamp in seconds
    if {[info exists param(-origin_timestamp_sec)]} {

        set origin_timestamp_sec $param(-origin_timestamp_sec)
    } else {

        set origin_timestamp_sec 0
    }

    #Origin timestamp in nanoseconds
    if {[info exists param(-origin_timestamp_ns)]} {

        set origin_timestamp_ns $param(-origin_timestamp_ns)
    } else {

        set origin_timestamp_ns 0
    }

    #Current UTC Offset
    if {[info exists param(-current_utc_offset)]} {

        set current_utc_offset $param(-current_utc_offset)
    } else {

        set current_utc_offset 0
    }

    #Grand Master Priority 1
    if {[info exists param(-gm_priority1)]} {

        set gm_priority1 $param(-gm_priority1)
    } else {

        set gm_priority1 0
    }

    #Grand Master Priority 2
    if {[info exists param(-gm_priority2)]} {

        set gm_priority2 $param(-gm_priority2)
    } else {

        set gm_priority2 128
    }

    #Grand Master Identity
    if {[info exists param(-gm_identity)]} {

        set gm_identity $param(-gm_identity)
    } else {

        set gm_identity 0
    }

    #Steps Removed
    if {[info exists param(-steps_removed)]} {

        set steps_removed $param(-steps_removed)
    } else {

        set steps_removed 0
    }

    #Time Source
    if {[info exists param(-time_source)]} {

        set time_source $param(-time_source)
    } else {

        set time_source 0
    }

    #Grand Master Clock class
    if {[info exists param(-gm_clock_class)]} {

        set gm_clock_class $param(-gm_clock_class)
    } else {

        set gm_clock_class 0
    }

    #Grand Master Clock accuracy
    if {[info exists param(-gm_clock_accuracy)]} {

        set gm_clock_accuracy $param(-gm_clock_accuracy)
    } else {

        set gm_clock_accuracy 0
    }

    #Grand Master Clock offset scaled
    if {[info exists param(-gm_clock_variance)]} {

        set gm_clock_variance $param(-gm_clock_variance)
    } else {

        set gm_clock_variance 0
    }

    #OUTPUT PARAMETERS

    #PTP Announce header hex dump
    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    #LOGIC GOES HERE

    #Timestamp
    wrptp_timestamp timestamp

    set timestamp::secondsField     [dec2hex $origin_timestamp_sec 12]
    set timestamp::nanosecondsField [dec2hex $origin_timestamp_ns 8]

    set originTimestamp [timestamp::pkt]


    #Clock Quality
    wrptp_clock_quality clock_quality

    set clock_quality::clockClass              [dec2hex $gm_clock_class 2]
    set clock_quality::clockAccuracy           [dec2hex $gm_clock_accuracy 2]
    set clock_quality::offsetScaledLogVariance [dec2hex $gm_clock_variance 4]

    set grandmasterClockQuality [clock_quality::pkt]


    #Header
    wrptp_announce hdr

    set hdr::originTimestamp         $originTimestamp
    set hdr::currentUtcOffset        [dec2hex $current_utc_offset 4]
    set hdr::reserved                [dec2hex 0 2]
    set hdr::grandmasterPriority1    [dec2hex $gm_priority1 2]
    set hdr::grandmasterClockQuality $grandmasterClockQuality
    set hdr::grandmasterPriority2    [dec2hex $gm_priority2 2]
    set hdr::grandmasterIdentity     [dec2hex $gm_identity 16]
    set hdr::stepsRemoved            [dec2hex $steps_removed 4]
    set hdr::timeSource              [dec2hex $time_source 2]

    set hexdump [hdr::pkt]

    return $hexdump

}

################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::encode_wrptp_delay_req_header                 #
#                                                                              #
#  DEFINITION           : This function encodes PTP delay req header.          #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  origin_timestamp_sec :  (Optional)  Origin timestamp in seconds             #
#                                      (Default : 0)                           #
#                                                                              #
#  origin_timestamp_ns  :  (Optional)  Origin timestamp in nanoseconds         #
#                                      (Default : 0)                           #
#                                                                              #
#  OUTPUT PARAMETERS    : NONE                                                 #
#                                                                              #
#  hexdump              :  (Optional)  Hex dump of PTP Delay request Header    #
#                                                                              #
#                                                                              #
#  RETURNS              : PTP delay request header hex dump                    #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#    pbLib::encode_wrptp_delay_req_header\                                     #
#                -origin_timestamp_sec              $origin_timestamp_sec\     #
#                -origin_timestamp_ns               $origin_timestamp_ns\      #
#                -hexdump                           hexdump                    #
#                                                                              #
################################################################################

proc pbLib::encode_wrptp_delay_req_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #Origin timestamp in seconds
    if {[info exists param(-origin_timestamp_sec)]} {

        set origin_timestamp_sec $param(-origin_timestamp_sec)
    } else {

        set origin_timestamp_sec 0
    }

    #Origin timestamp in nanoseconds
    if {[info exists param(-origin_timestamp_ns)]} {

        set origin_timestamp_ns $param(-origin_timestamp_ns)
    } else {

        set origin_timestamp_ns 0
    }

    #OUTPUT PARAMETERS

    #Hex dump of PTP Sync Header
    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    #LOGIC GOES HERE

    #Timestamp
    wrptp_timestamp timestamp

    set timestamp::secondsField     [dec2hex $origin_timestamp_sec 12]
    set timestamp::nanosecondsField [dec2hex $origin_timestamp_ns 8]

    set origin_timestamp [timestamp::pkt]


    #Header
    wrptp_delay_req hdr

    set hdr::originTimestamp $origin_timestamp

    set hexdump [hdr::pkt]

    return $hexdump

}

################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::encode_wrptp_delay_resp_header                #
#                                                                              #
#  DEFINITION           : This function encodes PTP delay response header.     #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  receive_timestamp_sec         :  (Optional)  Receive Timestamp              #
#                                     (Default : 0)                            #
#                                                                              #
#  receive_timestamp_ns          :  (Optional)  Receive Timestamp              #
#                                     (Default : 0)                            #
#                                                                              #
#  requesting_port_number        :  (Optional)  Requesting Port Number         #
#                                       (Default : 0)                          #
#                                                                              #
#  requesting_clock_identity     :  (Optional)  Requesting Clock Identity      #
#                                       (Default : 0)                          #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  hexdump              :  (Optional) Hex dump of PTP Delay response header    #
#                                                                              #
#                                                                              #
#  RETURNS              : PTP delay reponse header hex dump                    #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#   pbLib::encode_wrptp_delay_resp_header\                                     #
#               -receive_timestamp_sec         $receive_timestamp_sec\         #
#               -receive_timestamp_ns          $receive_timestamp_ns\          #
#               -requesting_port_number        $requesting_port_number\        #
#               -requesting_clock_identity     $requesting_clock_identity\     #
#               -hexdump                       hexdump                         #
#                                                                              #
################################################################################

proc pbLib::encode_wrptp_delay_resp_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #Receive Timestamp
    if {[info exists param(-receive_timestamp_sec)]} {

        set receive_timestamp_sec $param(-receive_timestamp_sec)
    } else {

        set receive_timestamp_sec 0
    }

    #Receive Timestamp
    if {[info exists param(-receive_timestamp_ns)]} {

        set receive_timestamp_ns $param(-receive_timestamp_ns)
    } else {

        set receive_timestamp_ns 0
    }

    #Requesting Port Number
    if {[info exists param(-requesting_port_number)]} {

        set requesting_port_number $param(-requesting_port_number)
    } else {

        set requesting_port_number 0
    }

    #Requesting Clock Identity
    if {[info exists param(-requesting_clock_identity)]} {

        set requesting_clock_identity $param(-requesting_clock_identity)
    } else {

        set requesting_clock_identity 0
    }

    #OUTPUT PARAMETERS

    #Hex dump of PTP Delay response Header
    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }


    #LOGIC GOES HERE

    #Receive Timestamp
    wrptp_timestamp timestamp

    set timestamp::secondsField     [dec2hex $receive_timestamp_sec 12]
    set timestamp::nanosecondsField [dec2hex $receive_timestamp_ns 8]

    set receive_timestamp [timestamp::pkt]

    #Header
    wrptp_delay_resp hdr

    set hdr::receiveTimestamp        $receive_timestamp
    set hdr::requestingPortIdentity  [pbLib::encode_port_identity $requesting_clock_identity $requesting_port_number]

    set hexdump [hdr::pkt]

    return $hexdump

}

################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::encode_wrptp_followup_header                  #
#                                                                              #
#  DEFINITION           : This function encodes PTP follow up header.          #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  precise_origin_timestamp_sec :  (Optional)  Precise Origin timestamp in sec #
#                                      (Default : 0)                           #
#                                                                              #
#  precise_origin_timestamp_ns  :  (Optional)  Precise Origin timestamp in ns  #
#                                      (Default : 0)                           #
#                                                                              #
#  OUTPUT PARAMETERS    : NONE                                                 #
#                                                                              #
#  hexdump              :  (Optional)  Hex dump of PTP Follow up Header        #
#                                                                              #
#                                                                              #
#  RETURNS              : PTP follow up header hex dump                        #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#    pbLib::encode_wrptp_followup_header\                                      #
#                -precise_origin_timestamp_sec  $precise_origin_timestamp_sec\ #
#                -precise_origin_timestamp_ns   $precise_origin_timestamp_ns\  #
#                -hexdump                       hexdump                        #
#                                                                              #
################################################################################

proc pbLib::encode_wrptp_followup_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #Precise Origin timestamp in seconds
    if {[info exists param(-precise_origin_timestamp_sec)]} {

        set precise_origin_timestamp_sec $param(-precise_origin_timestamp_sec)
    } else {

        set precise_origin_timestamp_sec 0
    }

    #Precise Origin timestamp in nanoseconds
    if {[info exists param(-precise_origin_timestamp_ns)]} {

        set precise_origin_timestamp_ns $param(-precise_origin_timestamp_ns)
    } else {

        set precise_origin_timestamp_ns 0
    }

    #OUTPUT PARAMETERS

    #Hex dump of PTP Sync Header
    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    #LOGIC GOES HERE

    #Timestamp
    wrptp_timestamp timestamp

    set timestamp::secondsField     [dec2hex $precise_origin_timestamp_sec 12]
    set timestamp::nanosecondsField [dec2hex $precise_origin_timestamp_ns 8]

    set precise_origin_timestamp [timestamp::pkt]


    #Header
    wrptp_followup hdr

    set hdr::preciseOriginTimestamp $precise_origin_timestamp

    set hexdump [hdr::pkt]

    return $hexdump

}

################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::encode_wrptp_pdelay_req_header                #
#                                                                              #
#  DEFINITION           : This function encodes PTP peer delay request         #
#                         header                                               #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  origin_timestamp_sec :  (Optional)  Origin timestamp in seconds             #
#                                      (Default : 0)                           #
#                                                                              #
#  origin_timestamp_ns  :  (Optional)  Origin timestamp in nanoseconds         #
#                                            (Default : 0)                     #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  hexdump              :  (Optional)  PTP peer delay request header           #
#                                      hex dump                                #
#                                                                              #
#  RETURNS              : PTP peer delay request header hex dump               #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#   pbLib::encode_wrptp_pdelay_req_header\                                     #
#               -origin_timestamp_sec              $origin_timestamp_sec\      #
#               -origin_timestamp_ns               $origin_timestamp_ns\       #
#               -hexdump                           hexdump                     #
#                                                                              #
################################################################################

proc pbLib::encode_wrptp_pdelay_req_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #Origin timestamp in seconds
    if {[info exists param(-origin_timestamp_sec)]} {

        set origin_timestamp_sec $param(-origin_timestamp_sec)
    } else {

        set origin_timestamp_sec 0
    }

    #Origin timestamp in nanoseconds
    if {[info exists param(-origin_timestamp_ns)]} {

        set origin_timestamp_ns $param(-origin_timestamp_ns)
    } else {

        set origin_timestamp_ns 0
    }

    #OUTPUT PARAMETERS

    #PTP peer delay request header
    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    #LOGIC GOES HERE

    #Orgin Timestamp
    wrptp_timestamp timestamp

    set timestamp::secondsField     [dec2hex $origin_timestamp_sec 12]
    set timestamp::nanosecondsField [dec2hex $origin_timestamp_ns 8]

    set originTimestamp [timestamp::pkt]


    #Header
    wrptp_pdelay_req hdr

    set hdr::originTimestamp  $originTimestamp
    set hdr::reserved         [dec2hex 0 20]

    set hexdump [hdr::pkt]

    return $hexdump

}

################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::encode_wrptp_pdelay_resp_header               #
#                                                                              #
#  DEFINITION           : This function encodes PTP peer delay response        #
#                         header                                               #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  requesting_port_number        :  (Optional)  Requesting Port Number         #
#                                       (Default : 0)                          #
#                                                                              #
#  requesting_clock_identity     :  (Optional)  Requesting Clock Identity      #
#                                       (Default : 0)                          #
#                                                                              #
#  request_receipt_timestamp_sec :  (Optional)  Request Receipt Timestamp      #
#                                       (Default : 0)                          #
#                                                                              #
#  request_receipt_timestamp_ns  :  (Optional)  Request Receipt Timestamp      #
#                                       (Default : 0)                          #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  hexdump              :  (Optional)  PTP peer delay response header          #
#                                      hex dump                                #
#                                                                              #
#  RETURNS              : PTP peer delay response header hex dump              #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#   pbLib::encode_wrptp_pdelay_resp_header\                                    #
#           -requesting_port_number         $requesting_port_number\           #
#           -requesting_clock_identity      $requesting_clock_identity\        #
#           -request_receipt_timestamp_sec  $request_receipt_timestamp_sec\    #
#           -request_receipt_timestamp_ns   $request_receipt_timestamp_ns\     #
#           -hexdump                        hexdump                            #
#                                                                              #
################################################################################

proc pbLib::encode_wrptp_pdelay_resp_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #Requesting Port Number
    if {[info exists param(-requesting_port_number)]} {

        set requesting_port_number $param(-requesting_port_number)
    } else {

        set requesting_port_number 0
    }

    #Requesting Clock Identity
    if {[info exists param(-requesting_clock_identity)]} {

        set requesting_clock_identity $param(-requesting_clock_identity)
    } else {

        set requesting_clock_identity 0
    }

    #Request Receipt Timestamp
    if {[info exists param(-request_receipt_timestamp_sec)]} {

        set request_receipt_timestamp_sec $param(-request_receipt_timestamp_sec)
    } else {

        set request_receipt_timestamp_sec 0
    }

    #Request Receipt Timestamp
    if {[info exists param(-request_receipt_timestamp_ns)]} {

        set request_receipt_timestamp_ns $param(-request_receipt_timestamp_ns)
    } else {

        set request_receipt_timestamp_ns 0
    }

    #OUTPUT PARAMETERS

    #PTP peer delay response header
    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    #LOGIC GOES HERE

    #Request Receipt Timestamp
    wrptp_timestamp timestamp

    set timestamp::secondsField     [dec2hex $request_receipt_timestamp_sec 12]
    set timestamp::nanosecondsField [dec2hex $request_receipt_timestamp_ns 8]

    set requestReceiptTimestamp [timestamp::pkt]


    #Requesting port Identity
    wrptp_port_identity port_identity

    set port_identity::clockIdentity   [dec2hex $requesting_clock_identity 16]
    set port_identity::portNumber      [dec2hex $requesting_port_number 4]

    set requestingPortIdentity [port_identity::pkt]


    #Header
    wrptp_pdelay_resp hdr

    set hdr::requestReceiptTimestamp $requestReceiptTimestamp
    set hdr::requestingPortIdentity  $requestingPortIdentity

    set hexdump [hdr::pkt]

    return $hexdump

}

################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::encode_wrptp_pdelay_resp_followup_header      #
#                                                                              #
#  DEFINITION           : This function encodes PTP peer delay response        #
#                         followup header                                      #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  requesting_port_number        :  (Optional)  Requesting Port Number         #
#                                       (Default : 0)                          #
#                                                                              #
#  requesting_clock_identity     :  (Optional)  Requesting Clock Identity      #
#                                       (Default : 0)                          #
#                                                                              #
#  response_origin_timestamp_sec :  (Optional)  Response Origin Timestamp      #
#                                       (Default : 0)                          #
#                                                                              #
#  response_origin_timestamp_ns  :  (Optional)  Response Origin Timestamp      #
#                                      (Default : 0)                           #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  hexdump              :  (Optional)  PTP Peer delay response followup        #
#                                      header hex dump                         #
#                                                                              #
#  RETURNS              : PTP Peer delay response followup header hexdump      #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#   pbLib::encode_wrptp_pdelay_resp_followup_header\                           #
#           -requesting_port_number            $requesting_port_number\        #
#           -requesting_clock_identity         $requesting_clock_identity\     #
#           -response_origin_timestamp_sec     $response_origin_timestamp_sec\ #
#           -response_origin_timestamp_ns      $response_origin_timestamp_ns\  #
#           -hexdump                           hexdump                         #
#                                                                              #
################################################################################

proc pbLib::encode_wrptp_pdelay_resp_followup_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #Requesting Port Number
    if {[info exists param(-requesting_port_number)]} {

        set requesting_port_number $param(-requesting_port_number)
    } else {

        set requesting_port_number 0
    }

    #Requesting Clock Identity
    if {[info exists param(-requesting_clock_identity)]} {

        set requesting_clock_identity $param(-requesting_clock_identity)
    } else {

        set requesting_clock_identity 0
    }

    #Response Origin Timestamp
    if {[info exists param(-response_origin_timestamp_sec)]} {

        set response_origin_timestamp_sec $param(-response_origin_timestamp_sec)
    } else {

        set response_origin_timestamp_sec 0
    }

    #Response Origin Timestamp
    if {[info exists param(-response_origin_timestamp_ns)]} {

        set response_origin_timestamp_ns $param(-response_origin_timestamp_ns)
    } else {

        set response_origin_timestamp_ns 0
    }

    #OUTPUT PARAMETERS

    #PTP Peer delay response followup
    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    #LOGIC GOES HERE

    #Response Origin Timestamp
    wrptp_timestamp timestamp

    set timestamp::secondsField     [dec2hex $response_origin_timestamp_sec 12]
    set timestamp::nanosecondsField [dec2hex $response_origin_timestamp_ns 8]

    set responseOriginTimestamp [timestamp::pkt]


    #Requesting port Identity
    wrptp_port_identity port_identity

    set port_identity::clockIdentity   [dec2hex $requesting_clock_identity 16]
    set port_identity::portNumber      [dec2hex $requesting_port_number 4]

    set requestingPortIdentity [port_identity::pkt]


    #Header
    wrptp_pdelay_resp_followup hdr

    set hdr::responseOriginTimestamp $responseOriginTimestamp
    set hdr::requestingPortIdentity  $requestingPortIdentity

    set hexdump [hdr::pkt]

    return $hexdump

}

################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::encode_wrptp_signal_header                    #
#                                                                              #
#  DEFINITION           : This function encodes PTP signal header              #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  target_port_number            :  (Optional)  Target Port Number             #
#                                       (Default : 0)                          #
#                                                                              #
#  target_clock_identity         :  (Optional)  Target Clock Identity          #
#                                       (Default : 0)                          #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  hexdump              :  (Optional)  PTP Signal header hex dump              #
#                                                                              #
#  RETURNS              : PTP Signal header hex dump                           #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#   pbLib::encode_wrptp_signal_header\                                         #
#              -target_port_number             $target_port_number\            #
#              -target_clock_identity          $target_clock_identity\         #
#              -hexdump                        hexdump                         #
#                                                                              #
################################################################################

proc pbLib::encode_wrptp_signal_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #Target Port Number
    if {[info exists param(-target_port_number)]} {

        set target_port_number $param(-target_port_number)
    } else {

        set target_port_number 0
    }

    #Target Clock Identity
    if {[info exists param(-target_clock_identity)]} {

        set target_clock_identity $param(-target_clock_identity)
    } else {

        set target_clock_identity 0
    }

    #OUTPUT PARAMETERS

    #PTP Signal header hex dump
    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    #LOGIC GOES HERE

    #Target Port Identity
    wrptp_port_identity port_identity

    set port_identity::clockIdentity   [dec2hex $target_clock_identity 16]
    set port_identity::portNumber      [dec2hex $target_port_number 4]

    set targetPortIdentity [port_identity::pkt]

    #Header
    wrptp_signal hdr

    set hdr::targetPortIdentity $targetPortIdentity
    set hexdump [hdr::pkt]

    return $hexdump

}

################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::encode_wrptp_mgmt_header                      #
#                                                                              #
#  DEFINITION           : This function encodes PTP management header          #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  target_port_number        :  (Optional)  Target Port Number                 #
#                                   (Default : 0)                              #
#                                                                              #
#  target_clock_identity     :  (Optional)  Target Clock Identity              #
#                                   (Default : 0)                              #
#                                                                              #
#  starting_boundary_hops    :  (Optional)  Starting Boundary Hops             #
#                                   (Default : 0)                              #
#                                                                              #
#  boundary_hops             :  (Optional)  Boundary Hops                      #
#                                   (Default : 0)                              #
#                                                                              #
#  action_field              :  (Optional)  Action Field                       #
#                                   (Default : 0)                              #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  hexdump              :  (Optional)  PTP Management header hex dump          #
#                                                                              #
#  RETURNS              : PTP Management header hex dump                       #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#   pbLib::encode_wrptp_mgmt_header\                                           #
#               -target_port_number                $target_port_number\        #
#               -target_clock_identity             $target_clock_identity\     #
#               -starting_boundary_hops            $starting_boundary_hops\    #
#               -boundary_hops                     $boundary_hops\             #
#               -action_field                      $action_field\              #
#               -hexdump                           hexdump                     #
#                                                                              #
################################################################################

proc pbLib::encode_wrptp_mgmt_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #Target Port Number
    if {[info exists param(-target_port_number)]} {

        set target_port_number $param(-target_port_number)
    } else {

        set target_port_number 0
    }

    #Target Clock Identity
    if {[info exists param(-target_clock_identity)]} {

        set target_clock_identity $param(-target_clock_identity)
    } else {

        set target_clock_identity 0
    }

    #Starting Boundary Hops
    if {[info exists param(-starting_boundary_hops)]} {

        set starting_boundary_hops $param(-starting_boundary_hops)
    } else {

        set starting_boundary_hops 0
    }

    #Boundary Hops
    if {[info exists param(-boundary_hops)]} {

        set boundary_hops $param(-boundary_hops)
    } else {

        set boundary_hops 0
    }

    #Action Field
    if {[info exists param(-action_field)]} {

        set action_field $param(-action_field)
    } else {

        set action_field 0
    }

    #OUTPUT PARAMETERS

    #PTP Management header hex dump
    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    #LOGIC GOES HERE

    #Target Port Identity
    wrptp_port_identity target_port_identity

    set target_port_identity::clockIdentity   [dec2hex $target_clock_identity 16]
    set target_port_identity::portNumber      [dec2hex $target_port_number 4]

    set targetPortIdentity [target_port_identity::pkt]


    #Header
    wrptp_mgmt hdr

    set hdr::targetPortIdentity      $targetPortIdentity

    set hdr::startingBoundaryHops    [dec2hex $starting_boundary_hops 2]
    set hdr::boundaryHops            [dec2hex $boundary_hops 2]
    set hdr::reserved1               [dec2hex 0 1]
    set hdr::actionField             [dec2hex $action_field 1]
    set hdr::reserved2               [dec2hex 0 1]

    set hexdump [hdr::pkt]

    return $hexdump

}

################################################################################
#  PROCEDURE NAME       : pbLib::decode_wrptp_pkt                              #
#                                                                              #
#  DEFINITION           : This function decodes PTP packet                     #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  packet               : (Mandatory) PTP packet hex dump                      #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  (Ethernet HEADER)                                                           #
#                                                                              #
#  dest_mac             : (Optional) Destination MAC address of eth header     #
#                                                                              #
#  src_mac              : (Optional) Source MAC address of eth header          #
#                                                                              #
#  eth_type             : (Optional) Ethernet type/length of eth header        #
#                                                                              #
#                                                                              #
#  (VLAN HEADER)                                                               #
#                                                                              #
#  vlan_id              : (Optional) Vlan id of vlan header                    #
#                                                                              #
#  vlan_priority        : (Optional) Priority of vlan header                   #
#                                                                              #
#  vlan_dei             : (Optional) Eligible indicator of vlan header         #
#                                                                              #
#                                                                              #
#  (IPv4 HEADER)                                                               #
#                                                                              #
#  ip_version           : (Optional) Version of ip                             #
#                                                                              #
#  ip_header_length     : (Optional) Header length  of ipv4                    #
#                                                                              #
#  ip_tos               : (Optional) Type of service (hex value)               #
#                                                                              #
#  ip_total_length      : (Optional) Total length  of ipv4                     #
#                                                                              #
#  ip_identification    : (Optional) Unique id of ipv4                         #
#                                                                              #
#  ip_flags             : (Optional) Flags represents the control or identify  #
#                                 fragments.                                   #
#                                                                              #
#  ip_offset            : (Optional) Fragment offset                           #
#                                                                              #
#  ip_ttl               : (Optional) Time to live                              #
#                                                                              #
#  ip_checksum          : (Optional) IP Checksum                               #
#                                                                              #
#  ip_protocol          : (Optional) Protocol in the data portion of the       #
#                                    IP datagram.                              #
#                                                                              #
#  src_ip               : (Optional) IPv4 address of the sender of the packet  #
#                                                                              #
#  dest_ip              : (Optional) IPv4 address of the reciever of the packet#
#                                                                              #
#                                                                              #
#  (UDP HEADER)                                                                #
#                                                                              #
#  src_port             : (Optional) Sender's port number                      #
#                                                                              #
#  dest_port            : (Optional) Receiver's port number                    #
#                                                                              #
#  udp_length           : (Optional) Length in bytes of UDP header and data    #
#                                                                              #
#  udp_checksum         : (Optional) UDP Checksum                              #
#                                                                              #
#                                                                              #
#  (PTP COMMON HEADER)                                                         #
#                                                                              #
#  transport_specific    :  (Optional)  Transport specific                     #
#                                                                              #
#  message_type          :  (Optional)  Message Type                           #
#                                                                              #
#  version_ptp           :  (Optional)  PTP version number                     #
#                                                                              #
#  message_length        :  (Optional)  PTP Sync message length                #
#                                                                              #
#  domain_number         :  (Optional)  Domain number                          #
#                                                                              #
#  alternate_master_flag :  (Optional)  Alternate Master flag                  #
#                                                                              #
#  two_step_flag         :  (Optional)  Two Step Flag                          #
#                                                                              #
#  unicast_flag          :  (Optional)  Unicast Flag                           #
#                                                                              #
#  profile_specific1     :  (Optional)  Profile specific1 Flag                 #
#                                                                              #
#  profile_specific2     :  (Optional)  Profile specific2 Flag                 #
#                                                                              #
#  leap61                :  (Optional)  Leap61 Flag                            #
#                                                                              #
#  leap59                :  (Optional)  Leap59 Flag                            #
#                                                                              #
#  current_utc_offset_valid:(Optional)  Current UTC Offset Flag                #
#                                                                              #
#  ptp_timescale         :  (Optional)  PTP TimeSclae Flag                     #
#                                                                              #
#  time_traceable        :  (Optional)  Time Traceable Flag                    #
#                                                                              #
#  freq_traceable        :  (Optional)  Frequency Traceable Flag               #
#                                                                              #
#  correction_field      :  (Optional)  Correction Field value                 #
#                                                                              #
#  src_port_number       :  (Optional)  Source port number                     #
#                                                                              #
#  src_clock_identity    :  (Optional)  Source Clock identity                  #
#                                                                              #
#  sequence_id           :  (Optional)  Sequence ID                            #
#                                                                              #
#  control_field         :  (Optional)  Control field                          #
#                                                                              #
#  log_message_interval  :  (Optional)  Log message interval                   #
#                                                                              #
#                                                                              #
#  (PTP SYNC/DELAY REQUEST HEADER)                                             #
#                                                                              #
#  origin_timestamp_sec       :  (Optional)  Origin timestamp in seconds       #
#                                                                              #
#  origin_timestamp_ns        :  (Optional)  Origin timestamp in nanoseconds   #
#                                                                              #
#                                                                              #
#  (PTP ANNOUNCE HEADER)                                                       #
#                                                                              #
#  current_utc_offset         :  (Optional)  Current UTC Offset                #
#                                                                              #
#  gm_priority1               :  (Optional)  Grand Master Priority 1           #
#                                                                              #
#  gm_priority2               :  (Optional)  Grand Master Priority 2           #
#                                                                              #
#  gm_identity                :  (Optional)  Grand Master Identity             #
#                                                                              #
#  steps_removed              :  (Optional)  Steps Removed                     #
#                                                                              #
#  time_source                :  (Optional)  Time Source                       #
#                                                                              #
#  gm_clock_class             :  (Optional)  Grand Master Clock class          #
#                                                                              #
#  gm_clock_accuracy          :  (Optional)  Grand Master Clock accuracy       #
#                                                                              #
#  gm_clock_variance          :  (Optional)  Grand Master Clock offset scaled  #
#                                                                              #
#                                                                              #
#  (PTP OTHER HEADERS)                                                         #
#                                                                              #
#  precise_origin_timestamp_sec  :  (Optional)  Precise Origin Timestamp       #
#                                                                              #
#  precise_origin_timestamp_ns   :  (Optional)  Precise Origin Timestamp       #
#                                                                              #
#  receive_timestamp_sec         :  (Optional)  Receive Timestamp              #
#                                                                              #
#  receive_timestamp_ns          :  (Optional)  Receive Timestamp              #
#                                                                              #
#  requesting_port_number        :  (Optional)  Requesting Port Number         #
#                                                                              #
#  requesting_clock_identity     :  (Optional)  Requesting Clock Identity      #
#                                                                              #
#  request_receipt_timestamp_sec :  (Optional)  Request Receipt Timestamp      #
#                                                                              #
#  request_receipt_timestamp_ns  :  (Optional)  Request Receipt Timestamp      #
#                                                                              #
#  response_origin_timestamp_sec :  (Optional)  Response Origin Timestamp      #
#                                                                              #
#  response_origin_timestamp_ns  :  (Optional)  Response Origin Timestamp      #
#                                                                              #
#  target_port_number            :  (Optional)  Target Port Number             #
#                                                                              #
#  target_clock_identity         :  (Optional)  Target Clock Identity          #
#                                                                              #
#  starting_boundary_hops        :  (Optional)  Starting Boundary Hops         #
#                                                                              #
#  boundary_hops                 :  (Optional)  Boundary Hops                  #
#                                                                              #
#  action_field                  :  (Optional)  Action Field                   #
#                                                                              #
#                                                                              #
#  (PTP TLVs)                                                                  #
#                                                                              #
#  tlv_type                      :  (Optional)  TLV Type                       #
#                                                                              #
#  tlv_length                    :  (Optional)  TLV length                     #
#                                                                              #
#  organization_id               :  (Optional) Organization id                 #
#                                                                              #
#  magic_number                  :  (Optional) Magic number                    #
#                                                                              #
#  version_number                :  (Optional) Version number                  #
#                                                                              #
#  message_id                    :  (Optional) Message id                      #
#                                                                              #
#  wrconfig                      :  (Optional) Wrconfig                        #
#                                                                              #
#  calibrated                    :  (Optional) Calibrated                      #
#                                                                              #
#  wrmode_on                     :  (Optional) WR Mode ON                      #
#                                                                              #
#  cal_send_pattern              :  (Optional) Cal send pattern                #
#                                                                              #
#  cal_retry                     :  (Optional) Cal retry                       #
#                                                                              #
#  cal_period                    :  (Optional) Cal period                      #
#                                                                              #
#  delta_tx                      :  (Optional) Delta tx                        #
#                                                                              #
#  delta_rx                      :  (Optional) Delta rx                        #
#                                                                              #
#                                                                              #
#  (MISC)                                                                      #
#                                                                              #
#  ip_checksum_expected          :  (Optional)  Expected IP Checksum           #
#                                                                              #
#  ip_checksum_status            :  (Optional)  Status of IP Checksum          #
#                                                                              #
#  udp_checksum_expected         :  (Optional)  Expected UDP Checksum          #
#                                                                              #
#  udp_checksum_status           :  (Optional)  Status of UDP Checksum         #
#                                                                              #
#  RETURNS              : 0 on failure (If the packet is not PTP packet)       #
#                                                                              #
#                         1 on success (If the packet is PTP packet)           #
#                                                                              #
#  USAGE                :                                                      #
#    pbLib::decode_wrptp_pkt\                                                  #
#             -dest_mac                       dest_mac\                        #
#             -src_mac                        src_mac\                         #
#             -eth_type                       eth_type\                        #
#             -vlan_id                        vlan_id\                         #
#             -vlan_priority                  vlan_priority\                   #
#             -vlan_dei                       vlan_dei\                        #
#             -ip_version                     ip_version\                      #
#             -ip_header_length               ip_header_length\                #
#             -ip_tos                         ip_tos\                          #
#             -ip_total_length                ip_total_length\                 #
#             -ip_identification              ip_identification\               #
#             -ip_flags                       ip_flags\                        #
#             -ip_offset                      ip_offset\                       #
#             -ip_ttl                         ip_ttl\                          #
#             -ip_checksum                    ip_checksum\                     #
#             -ip_protocol                    ip_protocol\                     #
#             -src_ip                         src_ip\                          #
#             -dest_ip                        dest_ip\                         #
#             -src_port                       src_port\                        #
#             -dest_port                      dest_port\                       #
#             -udp_length                     udp_length\                      #
#             -udp_checksum                   udp_checksum\                    #
#             -transport_specific             transport_specific\              #
#             -message_type                   message_type\                    #
#             -version_ptp                    version_ptp\                     #
#             -message_length                 message_length\                  #
#             -domain_number                  domain_number\                   #
#             -alternate_master_flag          alternate_master_flag\           #
#             -two_step_flag                  two_step_flag\                   #
#             -unicast_flag                   unicast_flag\                    #
#             -profile_specific1              profile_specific1\               #
#             -profile_specific2              profile_specific2\               #
#             -leap61                         leap61\                          #
#             -leap59                         leap59\                          #
#             -current_utc_offset_valid       current_utc_offset_valid\        #
#             -ptp_timescale                  ptp_timescale\                   #
#             -time_traceable                 time_traceable\                  #
#             -freq_traceable                 freq_traceable\                  #
#             -correction_field               correction_field\                #
#             -src_port_number                src_port_number\                 #
#             -src_clock_identity             src_clock_identity\              #
#             -sequence_id                    sequence_id\                     #
#             -control_field                  control_field\                   #
#             -log_message_interval           log_message_interval\            #
#             -origin_timestamp_sec           origin_timestamp_sec\            #
#             -origin_timestamp_ns            origin_timestamp_ns\             #
#             -current_utc_offset             current_utc_offset\              #
#             -gm_priority1                   gm_priority1\                    #
#             -gm_priority2                   gm_priority2\                    #
#             -gm_identity                    gm_identity\                     #
#             -steps_removed                  steps_removed\                   #
#             -time_source                    time_source\                     #
#             -gm_clock_class                 gm_clock_class\                  #
#             -gm_clock_accuracy              gm_clock_accuracy\               #
#             -gm_clock_variance              gm_clock_variance\               #
#             -precise_origin_timestamp_sec   precise_origin_timestamp_sec\    #
#             -precise_origin_timestamp_ns    precise_origin_timestamp_ns\     #
#             -receive_timestamp_sec          receive_timestamp_sec            #
#             -receive_timestamp_ns           receive_timestamp_ns\            #
#             -requesting_port_number         requesting_port_number\          #
#             -requesting_clock_identity      requesting_clock_identity\       #
#             -request_receipt_timestamp_sec  request_receipt_timestamp_sec\   #
#             -request_receipt_timestamp_ns   request_receipt_timestamp_ns\    #
#             -response_origin_timestamp_sec  response_origin_timestamp_sec\   #
#             -response_origin_timestamp_ns   response_origin_timestamp_ns\    #
#             -target_port_number             target_port_number\              #
#             -target_clock_identity          target_clock_identity\           #
#             -starting_boundary_hops         starting_boundary_hops\          #
#             -boundary_hops                  boundary_hops\                   #
#             -action_field                   action_field\                    #
#             -tlv_type                       tlv_type\                        #
#             -tlv_length                     tlv_length\                      #
#             -organization_id                organization_id\                 #
#             -magic_number                   magic_number\                    #
#             -version_number                 version_number\                  #
#             -message_id                     message_id\                      #
#             -wrconfig                       wrconfig\                        #
#             -calibrated                     calibrated\                      #
#             -wrmode_on                      wrmode_on\                       #
#             -cal_send_pattern               cal_send_pattern\                #
#             -cal_retry                      cal_retry\                       #
#             -cal_period                     cal_period\                      #
#             -delta_tx                       delta_tx\                        #
#             -delta_rx                       delta_rx                         #
#             -ip_checksum_expected           ip_checksum_expected\            #
#             -ip_checksum_status             ip_checksum_status\              #
#             -udp_checksum_expected          udp_checksum_expected\           #
#             -udp_checksum_status            udp_checksum_status              #
#                                                                              #
################################################################################

proc pbLib::decode_wrptp_pkt {args} {

    array set param $args

    #INPUT PARAMETERS

    #PTP packet hex dump
    if {[info exists param(-packet)]} {

        set packet $param(-packet)
    } else {

        return -code error "pbLib::decode_wrptp_pkt: Missing Argument -> packet"
    }

    #OUTPUT PARAMETERS

    #Destination MAC address of eth header
    if {[info exists param(-dest_mac)]} {

        upvar $param(-dest_mac) dest_mac
        set dest_mac ""
    }

    #Source MAC address of eth header
    if {[info exists param(-src_mac)]} {

        upvar $param(-src_mac) src_mac
        set src_mac ""
    }

    #Ethernet type
    if {[info exists param(-eth_type)]} {

        upvar $param(-eth_type) eth_type
        set eth_type ""
    }

    #Vlan id of vlan header
    if {[info exists param(-vlan_id)]} {

        upvar $param(-vlan_id) vlan_id
        set vlan_id ""
    }

    #Priority of vlan header
    if {[info exists param(-vlan_priority)]} {

        upvar $param(-vlan_priority) vlan_priority
        set vlan_priority ""
    }

    #Eligible indicator of vlan header
    if {[info exists param(-vlan_dei)]} {

        upvar $param(-vlan_dei) vlan_dei
        set vlan_dei ""
    }

    #Version of ip
    if {[info exists param(-ip_version)]} {

        upvar $param(-ip_version) ip_version
        set ip_version ""
    }

    #Header length  of ipv4
    if {[info exists param(-ip_header_length)]} {

        upvar $param(-ip_header_length) ip_header_length
        set ip_header_length ""
    }

    #Type of service
    if {[info exists param(-ip_tos)]} {

        upvar $param(-ip_tos) ip_tos
        set ip_tos ""
    }

    #Total length  of ipv4
    if {[info exists param(-ip_total_length)]} {

        upvar $param(-ip_total_length) ip_total_length
        set ip_total_length ""
    }

    #Unique id of ipv4
    if {[info exists param(-ip_identification)]} {

        upvar $param(-ip_identification) ip_identification
        set ip_identification ""
    }

    #Flags represents the control or identify
    if {[info exists param(-ip_flags)]} {

        upvar $param(-ip_flags) ip_flags
        set ip_flags ""
    }

    #Fragment offset
    if {[info exists param(-ip_offset)]} {

        upvar $param(-ip_offset) ip_offset
        set ip_offset ""
    }

    #Time to live
    if {[info exists param(-ip_ttl)]} {

        upvar $param(-ip_ttl) ip_ttl
        set ip_ttl ""
    }

    #IP Checksum
    if {[info exists param(-ip_checksum)]} {

        upvar $param(-ip_checksum) ip_checksum
        set ip_checksum ""
    }

    #Protocol in the data portion of the
    if {[info exists param(-ip_protocol)]} {

        upvar $param(-ip_protocol) ip_protocol
        set ip_protocol ""
    }

    #IPv4 address of the sender of the packet
    if {[info exists param(-src_ip)]} {

        upvar $param(-src_ip) src_ip
        set src_ip ""
    }

    #IPv4 address of the reciever of the packet
    if {[info exists param(-dest_ip)]} {

        upvar $param(-dest_ip) dest_ip
        set dest_ip ""
    }

    #Sender
    if {[info exists param(-src_port)]} {

        upvar $param(-src_port) src_port
        set src_port ""
    }

    #Receiver
    if {[info exists param(-dest_port)]} {

        upvar $param(-dest_port) dest_port
        set dest_port ""
    }

    #Length in bytes of UDP header and data
    if {[info exists param(-udp_length)]} {

        upvar $param(-udp_length) udp_length
        set udp_length ""
    }

    #UDP Checksum
    if {[info exists param(-udp_checksum)]} {

        upvar $param(-udp_checksum) udp_checksum
        set udp_checksum ""
    }

    #Transport specific
    if {[info exists param(-transport_specific)]} {

        upvar $param(-transport_specific) transport_specific
        set transport_specific ""
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        upvar $param(-message_type) message_type
        set message_type ""
    }

    #PTP version number
    if {[info exists param(-version_ptp)]} {

        upvar $param(-version_ptp) version_ptp
        set version_ptp ""
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        upvar $param(-message_length) message_length
        set message_length ""
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        upvar $param(-domain_number) domain_number
        set domain_number ""
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        upvar $param(-alternate_master_flag) alternate_master_flag
        set alternate_master_flag ""
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        upvar $param(-two_step_flag) two_step_flag
        set two_step_flag ""
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        upvar $param(-unicast_flag) unicast_flag
        set unicast_flag ""
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        upvar $param(-profile_specific1) profile_specific1
        set profile_specific1 ""
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        upvar $param(-profile_specific2) profile_specific2
        set profile_specific2 ""
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        upvar $param(-leap61) leap61
        set leap61 ""
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        upvar $param(-leap59) leap59
        set leap59 ""
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        upvar $param(-current_utc_offset_valid) current_utc_offset_valid
        set current_utc_offset_valid ""
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        upvar $param(-ptp_timescale) ptp_timescale
        set ptp_timescale ""
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        upvar $param(-time_traceable) time_traceable
        set time_traceable ""
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        upvar $param(-freq_traceable) freq_traceable
        set freq_traceable ""
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        upvar $param(-correction_field) correction_field
        set correction_field ""
    }

    #Source port number
    if {[info exists param(-src_port_number)]} {

        upvar $param(-src_port_number) src_port_number
        set src_port_number ""
    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        upvar $param(-src_clock_identity) src_clock_identity
        set src_clock_identity ""
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        upvar $param(-sequence_id) sequence_id
        set sequence_id ""
    }

    #Control field
    if {[info exists param(-control_field)]} {

        upvar $param(-control_field) control_field
        set control_field ""
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        upvar $param(-log_message_interval) log_message_interval
        set log_message_interval ""
    }

    #Origin timestamp in seconds
    if {[info exists param(-origin_timestamp_sec)]} {

        upvar $param(-origin_timestamp_sec) origin_timestamp_sec
        set origin_timestamp_sec ""
    }

    #Origin timestamp in nanoseconds
    if {[info exists param(-origin_timestamp_ns)]} {

        upvar $param(-origin_timestamp_ns) origin_timestamp_ns
        set origin_timestamp_ns ""
    }

    #Current UTC Offset
    if {[info exists param(-current_utc_offset)]} {

        upvar $param(-current_utc_offset) current_utc_offset
        set current_utc_offset ""
    }

    #Grand Master Priority 1
    if {[info exists param(-gm_priority1)]} {

        upvar $param(-gm_priority1) gm_priority1
        set gm_priority1 ""
    }

    #Grand Master Priority 2
    if {[info exists param(-gm_priority2)]} {

        upvar $param(-gm_priority2) gm_priority2
        set gm_priority2 ""
    }

    #Grand Master Identity
    if {[info exists param(-gm_identity)]} {

        upvar $param(-gm_identity) gm_identity
        set gm_identity ""
    }

    #Steps Removed
    if {[info exists param(-steps_removed)]} {

        upvar $param(-steps_removed) steps_removed
        set steps_removed ""
    }

    #Time Source
    if {[info exists param(-time_source)]} {

        upvar $param(-time_source) time_source
        set time_source ""
    }

    #Grand Master Clock class
    if {[info exists param(-gm_clock_class)]} {

        upvar $param(-gm_clock_class) gm_clock_class
        set gm_clock_class ""
    }

    #Grand Master Clock accuracy
    if {[info exists param(-gm_clock_accuracy)]} {

        upvar $param(-gm_clock_accuracy) gm_clock_accuracy
        set gm_clock_accuracy ""
    }

    #Grand Master Clock offset scaled
    if {[info exists param(-gm_clock_variance)]} {

        upvar $param(-gm_clock_variance) gm_clock_variance
        set gm_clock_variance ""
    }

    #Precise Origin Timestamp
    if {[info exists param(-precise_origin_timestamp_sec)]} {

        upvar $param(-precise_origin_timestamp_sec) precise_origin_timestamp_sec
        set precise_origin_timestamp_sec ""
    }

    #Precise Origin Timestamp
    if {[info exists param(-precise_origin_timestamp_ns)]} {

        upvar $param(-precise_origin_timestamp_ns) precise_origin_timestamp_ns
        set precise_origin_timestamp_ns ""
    }

    #Receive Timestamp
    if {[info exists param(-receive_timestamp_sec)]} {

        upvar $param(-receive_timestamp_sec) receive_timestamp_sec
        set receive_timestamp_sec ""
    }

    #Receive Timestamp
    if {[info exists param(-receive_timestamp_ns)]} {

        upvar $param(-receive_timestamp_ns) receive_timestamp_ns
        set receive_timestamp_ns ""
    }

    #Requesting Port Number
    if {[info exists param(-requesting_port_number)]} {

        upvar $param(-requesting_port_number) requesting_port_number
        set requesting_port_number ""
    }

    #Requesting Clock Identity
    if {[info exists param(-requesting_clock_identity)]} {

        upvar $param(-requesting_clock_identity) requesting_clock_identity
        set requesting_clock_identity ""
    }

    #Request Receipt Timestamp
    if {[info exists param(-request_receipt_timestamp_sec)]} {

        upvar $param(-request_receipt_timestamp_sec) request_receipt_timestamp_sec
        set request_receipt_timestamp_sec ""
    }

    #Request Receipt Timestamp
    if {[info exists param(-request_receipt_timestamp_ns)]} {

        upvar $param(-request_receipt_timestamp_ns) request_receipt_timestamp_ns
        set request_receipt_timestamp_ns ""
    }

    #Response Origin Timestamp
    if {[info exists param(-response_origin_timestamp_sec)]} {

        upvar $param(-response_origin_timestamp_sec) response_origin_timestamp_sec
        set response_origin_timestamp_sec ""
    }

    #Response Origin Timestamp
    if {[info exists param(-response_origin_timestamp_ns)]} {

        upvar $param(-response_origin_timestamp_ns) response_origin_timestamp_ns
        set response_origin_timestamp_ns ""
    }

    #Target Port Number
    if {[info exists param(-target_port_number)]} {

        upvar $param(-target_port_number) target_port_number
        set target_port_number ""
    }

    #Target Clock Identity
    if {[info exists param(-target_clock_identity)]} {

        upvar $param(-target_clock_identity) target_clock_identity
        set target_clock_identity ""
    }

    #Starting Boundary Hops
    if {[info exists param(-starting_boundary_hops)]} {

        upvar $param(-starting_boundary_hops) starting_boundary_hops
        set starting_boundary_hops ""
    }

    #Boundary Hops
    if {[info exists param(-boundary_hops)]} {

        upvar $param(-boundary_hops) boundary_hops
        set boundary_hops ""
    }

    #Action Field
    if {[info exists param(-action_field)]} {

        upvar $param(-action_field) action_field
        set action_field ""
    }

    #TLV

    #TLV Type
    if {[info exists param(-tlv_type)]} {

        upvar $param(-tlv_type) tlv_type
        set tlv_type ""
    }

    #TLV length
    if {[info exists param(-tlv_length)]} {

        upvar $param(-tlv_length) tlv_length
        set tlv_length ""
    }

    #Organisation ID
    if {[info exists param(-organization_id)]} {

        upvar $param(-organization_id) organization_id
        set organization_id ""
    }

    #Magic Number
    if {[info exists param(-magic_number)]} {

        upvar $param(-magic_number) magic_number
        set magic_number ""
    }

    #Version Number
    if {[info exists param(-version_number)]} {

        upvar $param(-version_number) version_number
        set version_number ""
    }

    #Message ID
    if {[info exists param(-message_id)]} {

        upvar $param(-message_id) message_id
        set message_id ""
    }

    #WR Config
    if {[info exists param(-wrconfig)]} {

        upvar $param(-wrconfig) wrconfig
        set wrconfig ""
    }

    #Calibrated
    if {[info exists param(-calibrated)]} {

        upvar $param(-calibrated) calibrated
        set calibrated ""
    }

    #WR Mode ON
    if {[info exists param(-wrmode_on)]} {

        upvar $param(-wrmode_on) wrmode_on
        set wrmode_on ""
    }

    #Cal Send Pattern
    if {[info exists param(-cal_send_pattern)]} {

        upvar $param(-cal_send_pattern) cal_send_pattern
        set cal_send_pattern ""
    }

    #Cal Retry
    if {[info exists param(-cal_retry)]} {

        upvar $param(-cal_retry) cal_retry
        set cal_retry ""
    }

    #Cal Period
    if {[info exists param(-cal_period)]} {

        upvar $param(-cal_period) cal_period
        set cal_period ""
    }

    #Delta Tx
    if {[info exists param(-delta_tx)]} {

        upvar $param(-delta_tx) delta_tx
        set delta_tx ""
    }

    #Delta Rx
    if {[info exists param(-delta_rx)]} {

        upvar $param(-delta_rx) delta_rx
        set delta_rx ""
    }

    #MISC

    #IP Checksum status
    if {[info exists param(-ip_checksum_status)]} {

        upvar $param(-ip_checksum_status) ip_checksum_status
        set ip_checksum_status ""
    }

    #Expected IP Checksum
    if {[info exists param(-ip_checksum_expected)]} {

        upvar $param(-ip_checksum_expected) ip_checksum_expected
        set ip_checksum_expected ""
    }

    #IP Checksum status
    if {[info exists param(-udp_checksum_status)]} {

        upvar $param(-udp_checksum_status) udp_checksum_status
        set udp_checksum_status ""
    }

    #Expected UDP Checksum
    if {[info exists param(-udp_checksum_expected)]} {

        upvar $param(-udp_checksum_expected) udp_checksum_expected
        set udp_checksum_expected ""
    }

    #LOGIC GOES HERE
    set hex [string tolower $packet]
    set decode_ptp 0

    #DECODE ETH HEADER
    pbLib::decode_eth_header\
                -hex            $hex\
                -dest_mac       dest_mac\
                -src_mac        src_mac\
                -next_protocol  ethtype

    set eth_type $ethtype
    set hex [string range $hex 28 end]

    while 1 {
        switch $ethtype {

            "8100" {
                #DECODE VLAN HEADER
                pbLib::decode_vlan_header\
                             -hex           $hex\
                             -priority      vlan_priority\
                             -dei           vlan_dei\
                             -vlan_id       vlan_id\
                             -next_protocol ethtype

                set hex [string range $hex 8 end]
                continue
            }

            "0800" {

                #DECODE IPv4 HEADER
                pbLib::decode_ipv4_header\
                             -hex                     $hex\
                             -version                 ip_version\
                             -header_length           ip_header_length\
                             -tos                     ip_tos\
                             -total_length            ip_total_length\
                             -identification          ip_identification\
                             -flags                   ip_flags\
                             -offset                  ip_offset\
                             -ttl                     ip_ttl\
                             -next_protocol           ip_protocol\
                             -checksum                ip_checksum\
                             -src_ip                  src_ip\
                             -dest_ip                 dest_ip\
                             -checksum_status         ip_checksum_status\
                             -checksum_expected       ip_checksum_expected

                set hex [string range $hex [expr $ip_header_length*2] end]

                if {$ip_protocol == 17} {

                    set ip_pseudo_header ""
                    append ip_pseudo_header [dec2hex $ip_protocol 4]
                    append ip_pseudo_header [ip2hex $src_ip]
                    append ip_pseudo_header [ip2hex $dest_ip]

                    set data [string range $hex 16 end]

                    #DECODE UDP HEADER
                    pbLib::decode_udp_header\
                                      -hex                 $hex\
                                      -src_port            src_port\
                                      -dest_port           dest_port\
                                      -payload_length      udp_length\
                                      -checksum            udp_checksum\
                                      -pseudo_header       $ip_pseudo_header\
                                      -data                $data\
                                      -checksum_status     udp_checksum_status\
                                      -checksum_expected   udp_checksum_expected

                    set hex $data
                    set decode_ptp 1
                }

                break
            }

            "88f7" {

                set decode_ptp 1
                break

            }

            default {
                break
            }
        }
    }

    #Not a PTP packet
    if {!$decode_ptp} {
        return 0
    }

    #DECODE PTP COMMON HEADER
    pbLib::decode_wrptp_common_header\
                -hex                               $hex\
                -transport_specific                transport_specific\
                -message_type                      message_type\
                -version_ptp                       version_ptp\
                -message_length                    message_length\
                -domain_number                     domain_number\
                -correction_field                  correction_field\
                -sequence_id                       sequence_id\
                -control_field                     control_field\
                -log_message_interval              log_message_interval\
                -src_clock_identity                src_clock_identity\
                -src_port_number                   src_port_number\
                -alternate_master_flag             alternate_master_flag\
                -two_step_flag                     two_step_flag\
                -unicast_flag                      unicast_flag\
                -profile_specific1                 profile_specific1\
                -profile_specific2                 profile_specific2\
                -leap61                            leap61\
                -leap59                            leap59\
                -current_utc_offset_valid          current_utc_offset_valid\
                -ptp_timescale                     ptp_timescale\
                -time_traceable                    time_traceable\
                -freq_traceable                    freq_traceable

    set hex [string range $hex 68 end]

    switch $message_type {
        0 {
            #Sync
            pbLib::decode_wrptp_sync_header\
                                         -hex                        $hex\
                                         -origin_timestamp_sec       origin_timestamp_sec\
                                         -origin_timestamp_ns        origin_timestamp_ns

            set hex [string range $hex 20 end]

        }

        1 {
            #Delay Request
            pbLib::decode_wrptp_delay_req_header\
                                         -hex                        $hex\
                                         -origin_timestamp_sec       origin_timestamp_sec\
                                         -origin_timestamp_ns        origin_timestamp_ns

            set hex [string range $hex 20 end]

        }

        2 {
            #Peer Delay Request
            pbLib::decode_wrptp_pdelay_req_header\
                                         -hex                        $hex\
                                         -origin_timestamp_sec       origin_timestamp_sec\
                                         -origin_timestamp_ns        origin_timestamp_ns

            set hex [string range $hex 40 end]

        }

        3 {
            #Peer Delay Response
            pbLib::decode_wrptp_pdelay_resp_header\
                                         -hex                            $hex\
                                         -request_receipt_timestamp_sec  request_receipt_timestamp_sec\
                                         -request_receipt_timestamp_ns   request_receipt_timestamp_ns\
                                         -requesting_port_number         requesting_port_number\
                                         -requesting_clock_identity      requesting_clock_identity

            set hex [string range $hex 40 end]

        }

        8 {
            #Follow up
            pbLib::decode_wrptp_followup_header\
                                         -hex                           $hex\
                                         -precise_origin_timestamp_sec  precise_origin_timestamp_sec\
                                         -precise_origin_timestamp_ns   precise_origin_timestamp_ns\

            set hex [string range $hex 20 end]

        }

        9 {
            #Delay Response
            pbLib::decode_wrptp_delay_resp_header\
                                         -hex                          $hex\
                                         -receive_timestamp_sec        receive_timestamp_sec\
                                         -receive_timestamp_ns         receive_timestamp_ns\
                                         -requesting_port_number       requesting_port_number\
                                         -requesting_clock_identity    requesting_clock_identity

            set hex [string range $hex 40 end]

        }

        10 {
            #Peer Delay Response Follow up
            pbLib::decode_wrptp_pdelay_resp_followup_header\
                                         -hex                           $hex\
                                         -response_origin_timestamp_sec response_origin_timestamp_sec\
                                         -response_origin_timestamp_ns  response_origin_timestamp_ns\
                                         -requesting_port_number        requesting_port_number\
                                         -requesting_clock_identity     requesting_clock_identity

            set hex [string range $hex 40 end]

        }

        11 {
            #Announce
            pbLib::decode_wrptp_announce_header\
                                         -hex                        $hex\
                                         -origin_timestamp_sec       origin_timestamp_sec\
                                         -origin_timestamp_ns        origin_timestamp_ns\
                                         -current_utc_offset         current_utc_offset\
                                         -gm_priority1               gm_priority1\
                                         -gm_priority2               gm_priority2\
                                         -gm_identity                gm_identity\
                                         -steps_removed              steps_removed\
                                         -time_source                time_source\
                                         -gm_clock_class             gm_clock_class\
                                         -gm_clock_accuracy          gm_clock_accuracy\
                                         -gm_clock_variance          gm_clock_variance

            set hex [string range $hex 60 end]

        }

        12 {
            #Signalling
            pbLib::decode_wrptp_signal_header\
                                         -hex                               $hex\
                                         -target_port_number                target_port_number\
                                         -target_clock_identity             target_clock_identity

            set hex [string range $hex 20 end]

        }

        13 {
            #Management
            pbLib::decode_wrptp_mgmt_header\
                                      -hex                        $hex\
                                      -target_port_number         target_port_number\
                                      -target_clock_identity      target_clock_identity\
                                      -starting_boundary_hops     starting_boundary_hops\
                                      -boundary_hops              boundary_hops\
                                      -action_field               action_field

            set hex [string range $hex 28 end]

        }

        default {
            #RESERVED
        }
    }

    pbLib::decode_wrptp_tlv\
                -hex                      $hex\
                -type                     tlv_type\
                -length                   tlv_length\
                -organization_id          organization_id\
                -magic_number             magic_number\
                -version_number           version_number\
                -message_id               message_id\
                -wrconfig                 wrconfig\
                -calibrated               calibrated\
                -wrmode_on                wrmode_on\
                -cal_send_pattern         cal_send_pattern\
                -cal_retry                cal_retry\
                -cal_period               cal_period\
                -delta_tx                 delta_tx\
                -delta_rx                 delta_rx\

    return 1

}

################################################################################
#  PROCEDURE NAME        : pbLib::decode_wrptp_common_header                   #
#                                                                              #
#  DEFINITION            : This function decodes PTP header.                   #
#                                                                              #
#  INPUT PARAMETERS      : NONE                                                #
#                                                                              #
#  hex                   :  (Mandatory)  Hex dump of PTP Commom Header         #
#                                                                              #
#  OUTPUT PARAMETERS     :                                                     #
#                                                                              #
#  transport_specific    :  (Optional)  Transport specific                     #
#                                                                              #
#  message_type          :  (Optional)  Message Type                           #
#                                                                              #
#  version_ptp           :  (Optional)  PTP version number                     #
#                                                                              #
#  message_length        :  (Optional)  PTP message length                     #
#                                                                              #
#  domain_number         :  (Optional)  Domain number                          #
#                                                                              #
#  alternate_master_flag :  (Optional)  Alternate Master flag                  #
#                                                                              #
#  two_step_flag         :  (Optional)  Two Step Flag                          #
#                                                                              #
#  unicast_flag          :  (Optional)  Unicast Flag                           #
#                                                                              #
#  profile_specific1     :  (Optional)  Profile specific1 Flag                 #
#                                                                              #
#  profile_specific2     :  (Optional)  Profile specific2 Flag                 #
#                                                                              #
#  leap61                :  (Optional)  Leap61 Flag                            #
#                                                                              #
#  leap59                :  (Optional)  Leap59 Flag                            #
#                                                                              #
#  current_utc_offset_valid:(Optional)  Current UTC Offset Flag                #
#                                                                              #
#  ptp_timescale         :  (Optional)  PTP TimeSclae Flag                     #
#                                                                              #
#  time_traceable        :  (Optional)  Time Traceable Flag                    #
#                                                                              #
#  freq_traceable        :  (Optional)  Frequency Traceable Flag               #
#                                                                              #
#  correction_field      :  (Optional)  Correction Field value                 #
#                                                                              #
#  src_port_number       :  (Optional)  Source port number                     #
#                                                                              #
#  src_clock_identity    :  (Optional)  Source clock identity                  #
#                                                                              #
#  sequence_id           :  (Optional)  Sequence ID                            #
#                                                                              #
#  control_field         :  (Optional)  Control field                          #
#                                                                              #
#  log_message_interval  :  (Optional)  Log message interval                   #
#                                                                              #
#                                                                              #
#  RETURNS              : PTP common header hex dump                           #
#                                                                              #
#  USAGE                :                                                      #
#   pbLib::decode_wrptp_common_header\                                         #
#              -hexdump                           $hexdump\                    #
#              -transport_specific                transport_specific\          #
#              -message_type                      message_type\                #
#              -version_ptp                       version_ptp\                 #
#              -message_length                    message_length\              #
#              -domain_number                     domain_number\               #
#              -alternate_master_flag             alternate_master_flag\       #
#              -two_step_flag                     two_step_flag\               #
#              -unicast_flag                      unicast_flag\                #
#              -profile_specific1                 profile_specific1\           #
#              -profile_specific2                 profile_specific2\           #
#              -leap61                            leap61\                      #
#              -leap59                            leap59\                      #
#              -current_utc_offset_valid          current_utc_offset_valid\    #
#              -ptp_timescale                     ptp_timescale\               #
#              -time_traceable                    time_traceable\              #
#              -freq_traceable                    freq_traceable\              #
#              -correction_field                  correction_field\            #
#              -src_port_number                   src_port_number\             #
#              -src_clock_identity                src_clock_identity           #
#              -sequence_id                       sequence_id\                 #
#              -control_field                     control_field\               #
#              -log_message_interval              log_message_interval         #
#                                                                              #
################################################################################

proc pbLib::decode_wrptp_common_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #Hex dump of PTP Commom Header
    if {[info exists param(-hex)]} {

        set hex $param(-hex)
    } else {

        return -code error "pbLib::decode_wrptp_common_header: Missing Argument -> hex"
    }

    #OUTPUT PARAMETERS

    #Transport specific
    if {[info exists param(-transport_specific)]} {

        upvar $param(-transport_specific) transport_specific
        set transport_specific ""
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        upvar $param(-message_type) message_type
        set message_type ""
    }

    #PTP version number
    if {[info exists param(-version_ptp)]} {

        upvar $param(-version_ptp) version_ptp
        set version_ptp ""
    }

    #PTP message length
    if {[info exists param(-message_length)]} {

        upvar $param(-message_length) message_length
        set message_length ""
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        upvar $param(-domain_number) domain_number
        set domain_number ""
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        upvar $param(-alternate_master_flag) alternate_master_flag
        set alternate_master_flag ""
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        upvar $param(-two_step_flag) two_step_flag
        set two_step_flag ""
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        upvar $param(-unicast_flag) unicast_flag
        set unicast_flag ""
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        upvar $param(-profile_specific1) profile_specific1
        set profile_specific1 ""
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        upvar $param(-profile_specific2) profile_specific2
        set profile_specific2 ""
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        upvar $param(-leap61) leap61
        set leap61 ""
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        upvar $param(-leap59) leap59
        set leap59 ""
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        upvar $param(-current_utc_offset_valid) current_utc_offset_valid
        set current_utc_offset_valid ""
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        upvar $param(-ptp_timescale) ptp_timescale
        set ptp_timescale ""
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        upvar $param(-time_traceable) time_traceable
        set time_traceable ""
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        upvar $param(-freq_traceable) freq_traceable
        set freq_traceable ""
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        upvar $param(-correction_field) correction_field
        set correction_field ""
    }

    #Source port number
    if {[info exists param(-src_port_number)]} {

        upvar $param(-src_port_number) src_port_number
        set src_port_number ""
    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        upvar $param(-src_clock_identity) src_clock_identity
        set src_clock_identity ""
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        upvar $param(-sequence_id) sequence_id
        set sequence_id ""
    }

    #Control field
    if {[info exists param(-control_field)]} {

        upvar $param(-control_field) control_field
        set control_field ""
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        upvar $param(-log_message_interval) log_message_interval
        set log_message_interval ""
    }

    #LOGIC GOES HERE
    wrptp_common hdr = $hex

    set transport_specific               [hex2dec $hdr::transportSpecific]
    set message_type                     [hex2dec $hdr::messageType]
    set version_ptp                      [hex2dec $hdr::versionPTP]
    set message_length                   [hex2dec $hdr::messageLength]
    set domain_number                    [hex2dec $hdr::domainNumber]

    wrptp_flags flag = $hdr::flagField

    set alternate_master_flag            $flag::alternateMasterFlag
    set two_step_flag                    $flag::twoStepFlag
    set unicast_flag                     $flag::unicastFlag
    set profile_specific1                $flag::profileSpecific1
    set profile_specific2                $flag::profileSpecific2
    set leap61                           $flag::leap61
    set leap59                           $flag::leap59
    set current_utc_offset_valid         $flag::currentUtcOffsetValid
    set ptp_timescale                    $flag::ptpTimescale
    set time_traceable                   $flag::timeTraceable
    set freq_traceable                   $flag::frequencyTraceable

    set correction_field                 [hex2dec $hdr::correctionField]

    wrptp_port_identity port_identity = $hdr::sourcePortIdentity

    set src_port_number                  [hex2dec $port_identity::portNumber]
    set src_clock_identity               [hex2dec $port_identity::clockIdentity]

    set sequence_id                      [hex2dec $hdr::sequenceId]
    set control_field                    [hex2dec $hdr::controlField]
    set log_message_interval             [hex2dec $hdr::logMessageInterval]

    return 1

}

################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::decode_wrptp_sync_header                      #
#                                                                              #
#  DEFINITION           : This function decodes PTP sync header                #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  hex                  : (Mandatory) hex dump                                 #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  origin_timestamp_sec :  (Optional)  Origin timestamp in seconds             #
#                                                                              #
#  origin_timestamp_ns  :  (Optional)  Origin timestamp in nanoseconds         #
#                                                                              #
#  RETURNS              : 0 on failure (If the hexdump is not PTP SYNC)        #
#                                                                              #
#                         1 on success (If the hexdump is PTP SYNC)            #
#                                                                              #
#  USAGE                :                                                      #
#    pbLib::decode_wrptp_sync_header\                                          #
#               -hex                               $hex\                       #
#               -origin_timestamp_sec              origin_timestamp_sec\       #
#               -origin_timestamp_ns               origin_timestamp_ns         #
#                                                                              #
################################################################################

proc pbLib::decode_wrptp_sync_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #hex dump
    if {[info exists param(-hex)]} {

        set hex $param(-hex)
    } else {

        return -code error "pbLib::decode_wrptp_sync_header: Missing Argument -> hex"
    }

    #OUTPUT PARAMETERS

    #Origin timestamp in seconds
    if {[info exists param(-origin_timestamp_sec)]} {

        upvar $param(-origin_timestamp_sec) origin_timestamp_sec
        set origin_timestamp_sec ""
    }

    #Origin timestamp in nanoseconds
    if {[info exists param(-origin_timestamp_ns)]} {

        upvar $param(-origin_timestamp_ns) origin_timestamp_ns
        set origin_timestamp_ns ""
    }


    #LOGIC GOES HERE
    wrptp_sync hdr = $hex

    wrptp_timestamp timestamp =  $hdr::originTimestamp

    set origin_timestamp_sec [hex2dec $timestamp::secondsField]
    set origin_timestamp_ns  [hex2dec $timestamp::nanosecondsField]

    return 1

}


################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::decode_wrptp_announce_header                  #
#                                                                              #
#  DEFINITION           : This function decodes PTP announce header            #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  hex                  : (Mandatory) hex dump                                 #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  origin_timestamp_sec :  (Optional)  Origin timestamp in seconds             #
#                                                                              #
#  origin_timestamp_ns  :  (Optional)  Origin timestamp in nanoseconds         #
#                                                                              #
#  current_utc_offset   :  (Optional)  Current UTC Offset                      #
#                                                                              #
#  gm_priority1         :  (Optional)  Grand Master Priority 1                 #
#                                                                              #
#  gm_priority2         :  (Optional)  Grand Master Priority 2                 #
#                                                                              #
#  gm_identity          :  (Optional)  Grand Master Identity                   #
#                                                                              #
#  steps_removed        :  (Optional)  Steps Removed                           #
#                                                                              #
#  time_source          :  (Optional)  Time Source                             #
#                                                                              #
#  gm_clock_class       :  (Optional)  Grand Master Clock class                #
#                                                                              #
#  gm_clock_accuracy    :  (Optional)  Grand Master Clock accuracy             #
#                                                                              #
#  gm_clock_variance    :  (Optional)  Grand Master Clock offset scaled        #
#                                                                              #
#  RETURNS              : 0 on failure (If the hexdump is not PTP ANNOUNCE)    #
#                                                                              #
#                         1 on success (If the hexdump is PTP ANNOUNCE)        #
#                                                                              #
#  #USAGE                                                                      #
#  pbLib::decode_wrptp_announce_header\                                        #
#             -hex                               $hex\                         #
#             -origin_timestamp_sec              origin_timestamp_sec\         #
#             -origin_timestamp_ns               origin_timestamp_ns\          #
#             -current_utc_offset                current_utc_offset\           #
#             -gm_priority1                      gm_priority1\                 #
#             -gm_priority2                      gm_priority2\                 #
#             -gm_identity                       gm_identity\                  #
#             -steps_removed                     steps_removed\                #
#             -time_source                       time_source\                  #
#             -gm_clock_class                    gm_clock_class\               #
#             -gm_clock_accuracy                 gm_clock_accuracy\            #
#             -gm_clock_variance                 gm_clock_variance             #
#                                                                              #
################################################################################

proc pbLib::decode_wrptp_announce_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #hex dump
    if {[info exists param(-hex)]} {

        set hex $param(-hex)
    } else {

        return -code error "pbLib::decode_wrptp_announce_header: Missing Argument -> hex"
    }

    #OUTPUT PARAMETERS

    #Origin timestamp in seconds
    if {[info exists param(-origin_timestamp_sec)]} {

        upvar $param(-origin_timestamp_sec) origin_timestamp_sec
        set origin_timestamp_sec ""
    }

    #Origin timestamp in nanoseconds
    if {[info exists param(-origin_timestamp_ns)]} {

        upvar $param(-origin_timestamp_ns) origin_timestamp_ns
        set origin_timestamp_ns ""
    }

    #Current UTC Offset
    if {[info exists param(-current_utc_offset)]} {

        upvar $param(-current_utc_offset) current_utc_offset
        set current_utc_offset ""
    }

    #Grand Master Priority 1
    if {[info exists param(-gm_priority1)]} {

        upvar $param(-gm_priority1) gm_priority1
        set gm_priority1 ""
    }

    #Grand Master Priority 2
    if {[info exists param(-gm_priority2)]} {

        upvar $param(-gm_priority2) gm_priority2
        set gm_priority2 ""
    }

    #Grand Master Identity
    if {[info exists param(-gm_identity)]} {

        upvar $param(-gm_identity) gm_identity
        set gm_identity ""
    }

    #Steps Removed
    if {[info exists param(-steps_removed)]} {

        upvar $param(-steps_removed) steps_removed
        set steps_removed ""
    }

    #Time Source
    if {[info exists param(-time_source)]} {

        upvar $param(-time_source) time_source
        set time_source ""
    }

    #Grand Master Clock class
    if {[info exists param(-gm_clock_class)]} {

        upvar $param(-gm_clock_class) gm_clock_class
        set gm_clock_class ""
    }

    #Grand Master Clock accuracy
    if {[info exists param(-gm_clock_accuracy)]} {

        upvar $param(-gm_clock_accuracy) gm_clock_accuracy
        set gm_clock_accuracy ""
    }

    #Grand Master Clock offset scaled
    if {[info exists param(-gm_clock_variance)]} {

        upvar $param(-gm_clock_variance) gm_clock_variance
        set gm_clock_variance ""
    }

    #LOGIC GOES HERE

    wrptp_announce hdr = $hex

    wrptp_timestamp timestamp =  $hdr::originTimestamp

    set origin_timestamp_sec [hex2dec $timestamp::secondsField]
    set origin_timestamp_ns  [hex2dec $timestamp::nanosecondsField]

    set current_utc_offset   [hex2dec $hdr::currentUtcOffset]
    set gm_priority1         [hex2dec $hdr::grandmasterPriority1]
    set gm_priority2         [hex2dec $hdr::grandmasterPriority2]
    set gm_identity          [hex2dec $hdr::grandmasterIdentity]
    set steps_removed        [hex2dec $hdr::stepsRemoved]
    set time_source          [hex2dec $hdr::timeSource]

    wrptp_clock_quality clock_quality = $hdr::grandmasterClockQuality

    set gm_clock_class       [hex2dec $clock_quality::clockClass]
    set gm_clock_accuracy    [hex2dec $clock_quality::clockAccuracy]
    set gm_clock_variance    [hex2dec $clock_quality::offsetScaledLogVariance]

    return 1

}


################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::decode_wrptp_delay_req_header                 #
#                                                                              #
#  DEFINITION           : This function decodes PTP delay request header       #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  hex                  : (Mandatory) hex dump                                 #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  origin_timestamp_sec :  (Optional)  Origin timestamp in seconds             #
#                                                                              #
#  origin_timestamp_ns  :  (Optional)  Origin timestamp in nanoseconds         #
#                                                                              #
#  RETURNS              : 0 on failure (If the hexdump is not PTP DELAY        #
#                                       REQUEST)                               #
#                                                                              #
#                         1 on success (If the hexdump is PTP DELAY REQUEST)   #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#   pbLib::decode_wrptp_delay_req_header\                                      #
#              -hex                               $hex\                        #
#              -origin_timestamp_sec              origin_timestamp_sec\        #
#              -origin_timestamp_ns               origin_timestamp_ns          #
#                                                                              #
################################################################################

proc pbLib::decode_wrptp_delay_req_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #hex dump
    if {[info exists param(-hex)]} {

        set hex $param(-hex)
    } else {

        return -code error "pbLib::decode_wrptp_delay_req_header: Missing Argument -> hex"
    }

    #OUTPUT PARAMETERS

    #Origin timestamp in seconds
    if {[info exists param(-origin_timestamp_sec)]} {

        upvar $param(-origin_timestamp_sec) origin_timestamp_sec
        set origin_timestamp_sec ""
    }

    #Origin timestamp in nanoseconds
    if {[info exists param(-origin_timestamp_ns)]} {

        upvar $param(-origin_timestamp_ns) origin_timestamp_ns
        set origin_timestamp_ns ""
    }

    #LOGIC GOES HERE
    wrptp_delay_req hdr = $hex

    wrptp_timestamp timestamp =  $hdr::originTimestamp

    set origin_timestamp_sec [hex2dec $timestamp::secondsField]
    set origin_timestamp_ns  [hex2dec $timestamp::nanosecondsField]

    return 1

}


####################################################################################
#                                                                                  #
#  PROCEDURE NAME           : pbLib::decode_wrptp_delay_resp_header                #
#                                                                                  #
#  DEFINITION               : This function decodes PTP delay response header      #
#                                                                                  #
#  INPUT PARAMETERS         :                                                      #
#                                                                                  #
#  hex                      : (Mandatory) hex dump                                 #
#                                                                                  #
#  OUTPUT PARAMETERS        :                                                      #
#                                                                                  #
#  receive_timestamp_sec    : (Optional)  Receive Timestamp                        #
#                                                                                  #
#  receive_timestamp_ns     : (Optional)  Receive Timestamp                        #
#                                                                                  #
#  requesting_port_number   : (Optional)  Requesting Port Number                   #
#                                                                                  #
#  requesting_clock_identity: (Optional)  Requesting Clock Identity                #
#                                                                                  #
#  RETURNS                  : 0 on failure (If the hexdump is not PTP DELAY        #
#                             REQUEST)                                             #
#                                                                                  #
#                             1 on success (If the hexdump is PTP DELAY            #
#                             REQUEST)                                             #
#                                                                                  #
#  USAGE                    :                                                      #
#                                                                                  #
#   pbLib::decode_wrptp_delay_resp_header\                                         #
#              -hex                               $hex\                            #
#              -receive_timestamp_sec             receive_timestamp_sec\           #
#              -receive_timestamp_ns              receive_timestamp_ns\            #
#              -requesting_port_number            requesting_port_number\          #
#              -requesting_clock_identity         requesting_clock_identity        #
#                                                                                  #
####################################################################################

proc pbLib::decode_wrptp_delay_resp_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #hex dump
    if {[info exists param(-hex)]} {

        set hex $param(-hex)
    } else {

        return -code error "pbLib::decode_wrptp_delay_resp_header: Missing Argument -> hex"
    }

    #OUTPUT PARAMETERS

    #Receive Timestamp
    if {[info exists param(-receive_timestamp_sec)]} {

        upvar $param(-receive_timestamp_sec) receive_timestamp_sec
        set receive_timestamp_sec ""
    }

    #Receive Timestamp
    if {[info exists param(-receive_timestamp_ns)]} {

        upvar $param(-receive_timestamp_ns) receive_timestamp_ns
        set receive_timestamp_ns ""
    }

    #Requesting Port Number
    if {[info exists param(-requesting_port_number)]} {

        upvar $param(-requesting_port_number) requesting_port_number
        set requesting_port_number ""
    }

    #Requesting Port Identity
    if {[info exists param(-requesting_clock_identity)]} {

        upvar $param(-requesting_clock_identity) requesting_clock_identity
        set requesting_clock_identity ""
    }

    #LOGIC GOES HERE
    wrptp_delay_resp hdr = $hex

    wrptp_timestamp timestamp =  $hdr::receiveTimestamp

    set receive_timestamp_sec [hex2dec $timestamp::secondsField]
    set receive_timestamp_ns  [hex2dec $timestamp::nanosecondsField]

    wrptp_port_identity port_identity = $hdr::requestingPortIdentity

    set requesting_port_number     [hex2dec $port_identity::portNumber]
    set requesting_clock_identity  [hex2dec $port_identity::clockIdentity]


    return 1

}


#########################################################################################
#                                                                                       #
#  PROCEDURE NAME                : pbLib::decode_wrptp_followup_header                  #
#                                                                                       #
#  DEFINITION                    : This function decodes PTP followup header            #
#                                                                                       #
#  INPUT PARAMETERS              :                                                      #
#                                                                                       #
#  hex                           : (Mandatory) hex dump                                 #
#                                                                                       #
#  OUTPUT PARAMETERS             :                                                      #
#                                                                                       #
#  precise_origin_timestamp_sec  : (Optional)  Precise Origin Timestamp                 #
#                                                                                       #
#  precise_origin_timestamp_ns   : (Optional)  Precise Origin Timestamp                 #
#                                                                                       #
#  RETURNS                       : 0 on failure (If the hexdump is not PTP FOLLOWUP     #
#                                                header)                                #
#                                                                                       #
#                                  1 on success (If the hexdump is PTP FOLLOWUP         #
#                                                header)                                #
#                                                                                       #
#  USAGE                         :                                                      #
#                                                                                       #
#   pbLib::decode_wrptp_followup_header\                                                #
#              -hex                               $hex\                                 #
#              -precise_origin_timestamp_sec      precise_origin_timestamp_sec\         #
#              -precise_origin_timestamp_ns       precise_origin_timestamp_ns           #
#                                                                                       #
#########################################################################################

proc pbLib::decode_wrptp_followup_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #hex dump
    if {[info exists param(-hex)]} {

        set hex $param(-hex)
    } else {

        return -code error "pbLib::decode_wrptp_followup_header: Missing Argument -> hex"
    }

    #OUTPUT PARAMETERS

    #Precise Origin Timestamp
    if {[info exists param(-precise_origin_timestamp_sec)]} {

        upvar $param(-precise_origin_timestamp_sec) precise_origin_timestamp_sec
        set precise_origin_timestamp_sec ""
    }

    #Precise Origin Timestamp
    if {[info exists param(-precise_origin_timestamp_ns)]} {

        upvar $param(-precise_origin_timestamp_ns) precise_origin_timestamp_ns
        set precise_origin_timestamp_ns ""
    }

    #LOGIC GOES HERE
    wrptp_followup hdr = $hex

    wrptp_timestamp timestamp =  $hdr::preciseOriginTimestamp

    set precise_origin_timestamp_sec [hex2dec $timestamp::secondsField]
    set precise_origin_timestamp_ns  [hex2dec $timestamp::nanosecondsField]


    return 1

}


################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::decode_wrptp_pdelay_req_header                #
#                                                                              #
#  DEFINITION           : This function decodes PTP peer delay request hdr     #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  hex                  : (Mandatory) hex dump                                 #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  origin_timestamp_sec : (Optional)  Origin timestamp in seconds              #
#                                                                              #
#  origin_timestamp_ns  : (Optional)  Origin timestamp in nanoseconds          #
#                                                                              #
#  RETURNS              : 0 on failure (If the hexdump is not PTP PDELAY       #
#                                       REQUEST)                               #
#                                                                              #
#                         1 on success (If the hexdump is PTP PDELAY REQUEST   #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#   pbLib::decode_wrptp_pdelay_req_header\                                     #
#              -hex                               $hex\                        #
#              -origin_timestamp_sec              origin_timestamp_sec\        #
#              -origin_timestamp_ns               origin_timestamp_ns          #
#                                                                              #
################################################################################

proc pbLib::decode_wrptp_pdelay_req_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #hex dump
    if {[info exists param(-hex)]} {

        set hex $param(-hex)
    } else {

        return -code error "pbLib::decode_wrptp_pdelay_req_header: Missing Argument -> hex"
    }

    #OUTPUT PARAMETERS

    #Origin timestamp in seconds
    if {[info exists param(-origin_timestamp_sec)]} {

        upvar $param(-origin_timestamp_sec) origin_timestamp_sec
        set origin_timestamp_sec ""
    }

    #Origin timestamp in nanoseconds
    if {[info exists param(-origin_timestamp_ns)]} {

        upvar $param(-origin_timestamp_ns) origin_timestamp_ns
        set origin_timestamp_ns ""
    }

    #LOGIC GOES HERE
    wrptp_pdelay_req hdr = $hex

    wrptp_timestamp timestamp =  $hdr::originTimestamp

    set origin_timestamp_sec [hex2dec $timestamp::secondsField]
    set origin_timestamp_ns  [hex2dec $timestamp::nanosecondsField]

    return 1

}


#########################################################################################
#                                                                                       #
#  PROCEDURE NAME                : pbLib::decode_wrptp_pdelay_resp_header               #
#                                                                                       #
#  DEFINITION                    : This function decodes PTP peer delay response hdr    #
#                                                                                       #
#  INPUT PARAMETERS              :                                                      #
#                                                                                       #
#  hex                           : (Mandatory) hex dump                                 #
#                                                                                       #
#  OUTPUT PARAMETERS             :                                                      #
#                                                                                       #
#  request_receipt_timestamp_sec : (Optional)  Request Receipt Timestamp                #
#                                                                                       #
#  request_receipt_timestamp_ns  : (Optional)  Request Receipt Timestamp                #
#                                                                                       #
#  requesting_port_number        : (Optional)  Requesting Port Number                   #
#                                                                                       #
#  requesting_clock_identity     : (Optional)  Requesting Clock Identity                #
#                                                                                       #
#                                                                                       #
#  RETURNS                       : 0 on failure (If the hexdump is not PTP PDELAY       #
#                                       RESPONSE)                                       #
#                                                                                       #
#                                  1 on success (If the hexdump is PTP PDELAY           #
#                                       RESPONSE)                                       #
#                                                                                       #
#  USAGE                         :                                                      #
#                                                                                       #
#   pbLib::decode_wrptp_pdelay_resp_header\                                             #
#              -hex                               $hex\                                 #
#              -request_receipt_timestamp_sec     request_receipt_timestamp_sec\        #
#              -request_receipt_timestamp_ns      request_receipt_timestamp_ns\         #
#              -requesting_port_number            requesting_port_number\               #
#              -requesting_clock_identity         requesting_clock_identity             #
#                                                                                       #
#########################################################################################

proc pbLib::decode_wrptp_pdelay_resp_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #hex dump
    if {[info exists param(-hex)]} {

        set hex $param(-hex)
    } else {

        return -code error "pbLib::decode_wrptp_pdelay_resp_header: Missing Argument -> hex"
    }

    #OUTPUT PARAMETERS

    #Request Receipt Timestamp
    if {[info exists param(-request_receipt_timestamp_sec)]} {

        upvar $param(-request_receipt_timestamp_sec) request_receipt_timestamp_sec
        set request_receipt_timestamp_sec ""
    }

    #Request Receipt Timestamp
    if {[info exists param(-request_receipt_timestamp_ns)]} {

        upvar $param(-request_receipt_timestamp_ns) request_receipt_timestamp_ns
        set request_receipt_timestamp_ns ""
    }

    #Requesting Port Number
    if {[info exists param(-requesting_port_number)]} {

        upvar $param(-requesting_port_number) requesting_port_number
        set requesting_port_number ""
    }

    #Requesting Clock Identity
    if {[info exists param(-requesting_clock_identity)]} {

        upvar $param(-requesting_clock_identity) requesting_clock_identity
        set requesting_clock_identity ""
    }

    #LOGIC GOES HERE
    wrptp_pdelay_resp hdr = $hex

    wrptp_timestamp timestamp =  $hdr::requestReceiptTimestamp

    set request_receipt_timestamp_sec  [hex2dec $timestamp::secondsField]
    set request_receipt_timestamp_ns   [hex2dec $timestamp::nanosecondsField]

    wrptp_port_identity port_identity =  $hdr::requestingPortIdentity

    set requesting_port_number       [hex2dec $port_identity::portNumber]
    set requesting_clock_identity    [hex2dec $port_identity::clockIdentity]


    return 1

}


##########################################################################################
#                                                                                        #
#  PROCEDURE NAME                : pbLib::decode_wrptp_pdelay_resp_followup_header       #
#                                                                                        #
#  DEFINITION                    : This function decodes PTP peer delay response         #
#                                  followup header                                       #
#                                                                                        #
#  INPUT PARAMETERS              :                                                       #
#                                                                                        #
#  hex                           : (Mandatory) hex dump                                  #
#                                                                                        #
#  OUTPUT PARAMETERS             :                                                       #
#                                                                                        #
#  response_origin_timestamp_sec :  (Optional)  Response Origin Timestamp                #
#                                                                                        #
#  response_origin_timestamp_ns  :  (Optional)  Response Origin Timestamp                #
#                                                                                        #
#  requesting_port_number        :  (Optional)  Requesting Port Number                   #
#                                                                                        #
#  requesting_clock_identity     :  (Optional)  Requesting Clock Identity                #
#                                                                                        #
#  RETURNS                       : 0 on failure                                          #
#                                                                                        #
#                                                                                        #
#                                  1 on success                                          #
#                                                                                        #
#                                                                                        #
#  USAGE                         :                                                       #
#                                                                                        #
#   pbLib::decode_wrptp_pdelay_resp_followup_header\                                     #
#              -hex                               $hex\                                  #
#              -response_origin_timestamp_sec     response_origin_timestamp_sec\         #
#              -response_origin_timestamp_ns      response_origin_timestamp_ns\          #
#              -requesting_port_number            requesting_port_number\                #
#              -requesting_clock_identity         requesting_clock_identity              #
#                                                                                        #
##########################################################################################
#
proc pbLib::decode_wrptp_pdelay_resp_followup_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #hex dump
    if {[info exists param(-hex)]} {

        set hex $param(-hex)
    } else {

        return -code error "pbLib::decode_wrptp_pdelay_resp_followup_header: Missing Argument -> hex"
    }

    #OUTPUT PARAMETERS

    #Response Origin Timestamp
    if {[info exists param(-response_origin_timestamp_sec)]} {

        upvar $param(-response_origin_timestamp_sec) response_origin_timestamp_sec
        set response_origin_timestamp_sec ""
    }

    #Response Origin Timestamp
    if {[info exists param(-response_origin_timestamp_ns)]} {

        upvar $param(-response_origin_timestamp_ns) response_origin_timestamp_ns
        set response_origin_timestamp_ns ""
    }

    #Requesting Port Number
    if {[info exists param(-requesting_port_number)]} {

        upvar $param(-requesting_port_number) requesting_port_number
        set requesting_port_number ""
    }

    #Requesting Clock Identity
    if {[info exists param(-requesting_clock_identity)]} {

        upvar $param(-requesting_clock_identity) requesting_clock_identity
        set requesting_clock_identity ""
    }

    #LOGIC GOES HERE

    wrptp_pdelay_resp_followup hdr = $hex

    wrptp_timestamp timestamp = $hdr::responseOriginTimestamp

    set response_origin_timestamp_sec [hex2dec $timestamp::secondsField]
    set response_origin_timestamp_ns  [hex2dec $timestamp::nanosecondsField]

    wrptp_port_identity port_identity = $hdr::requestingPortIdentity

    set requesting_clock_identity [hex2dec $port_identity::clockIdentity]
    set requesting_port_number    [hex2dec $port_identity::portNumber]

    return 1

}


#################################################################################
#                                                                               #
#  PROCEDURE NAME                : pbLib::decode_wrptp_signal_header            #
#                                                                               #
#  DEFINITION                    : This function decodes PTP signal header      #
#                                                                               #
#  INPUT PARAMETERS              :                                              #
#                                                                               #
#  hex                           : (Mandatory) hex dump                         #
#                                                                               #
#  OUTPUT PARAMETERS             :                                              #
#                                                                               #
#  target_port_number            : (Optional)  Target Port Number               #
#                                                                               #
#  target_clock_identity         : (Optional)  Target Clock Identity            #
#                                                                               #
#  RETURNS                       : 0 on failure (If the hexdump is not PTP      #
#                                                SIGNAL)                        #
#                                                                               #
#                                  1 on success (If the hexdump is PTP SIGNAL   #
#                                                                               #
#   #USAGE                                                                      #
#   pbLib::decode_wrptp_signal_header\                                          #
#              -hex                               $hex\                         #
#              -target_port_number                target_port_number\           #
#              -target_clock_identity             target_clock_identity         #
#                                                                               #
#################################################################################

proc pbLib::decode_wrptp_signal_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #hex dump
    if {[info exists param(-hex)]} {

        set hex $param(-hex)
    } else {

        return -code error "pbLib::decode_wrptp_signal_header: Missing Argument -> hex"
    }

    #OUTPUT PARAMETERS

    #Target Port Number
    if {[info exists param(-target_port_number)]} {

        upvar $param(-target_port_number) target_port_number
        set target_port_number ""
    }

    #Target Clock Identity
    if {[info exists param(-target_clock_identity)]} {

        upvar $param(-target_clock_identity) target_clock_identity
        set target_clock_identity ""
    }

    #LOGIC GOES HERE
    wrptp_signal hdr = $hex

    wrptp_port_identity port_identity =  $hdr::targetPortIdentity

    set target_clock_identity [hex2dec $port_identity::clockIdentity]
    set target_port_number    [hex2dec $port_identity::portNumber]

    return 1

}


####################################################################################
#                                                                                  #
#  PROCEDURE NAME           : pbLib::decode_wrptp_mgmt_header                      #
#                                                                                  #
#  DEFINITION               : This function decodes PTP management header          #
#                                                                                  #
#  INPUT PARAMETERS         :                                                      #
#                                                                                  #
#  hex                      : (Mandatory) hex dump                                 #
#                                                                                  #
#  OUTPUT PARAMETERS        :                                                      #
#                                                                                  #
#  target_port_number       : (Optional)  Target Port Number                       #
#                                                                                  #
#  target_clock_identity    : (Optional)  Target Clock Identity                    #
#                                                                                  #
#  starting_boundary_hops   : (Optional)  Starting Boundary Hops                   #
#                                                                                  #
#  boundary_hops            : (Optional)  Boundary Hops                            #
#                                                                                  #
#  action_field             : (Optional)  Action Field                             #
#                                                                                  #
#  RETURNS                  : 0 on failure (If the hexdump is not PTP MANAGEMENT   #
#                                                                                  #
#                             1 on success (If the hexdump is PTP MANAGEMENT       #
#                                                                                  #
#  USAGE                    :                                                      #
#                                                                                  #
#   pbLib::decode_wrptp_mgmt_header\                                               #
#              -hex                               $hex\                            #
#              -target_port_number                target_port_number\              #
#              -target_clock_identity             target_clock_identity\           #
#              -starting_boundary_hops            starting_boundary_hops\          #
#              -boundary_hops                     boundary_hops\                   #
#              -action_field                      action_field                     #
#                                                                                  #
####################################################################################

proc pbLib::decode_wrptp_mgmt_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #hex dump
    if {[info exists param(-hex)]} {

        set hex $param(-hex)
    } else {

        return -code error "pbLib::decode_wrptp_mgmt_header: Missing Argument -> hex"
    }

    #OUTPUT PARAMETERS

    #Target Port Number
    if {[info exists param(-target_port_number)]} {

        upvar $param(-target_port_number) target_port_number
        set target_port_number ""
    }

    #Target Clock Identity
    if {[info exists param(-target_clock_identity)]} {

        upvar $param(-target_clock_identity) target_clock_identity
        set target_clock_identity ""
    }

    #Starting Boundary Hops
    if {[info exists param(-starting_boundary_hops)]} {

        upvar $param(-starting_boundary_hops) starting_boundary_hops
        set starting_boundary_hops ""
    }

    #Boundary Hops
    if {[info exists param(-boundary_hops)]} {

        upvar $param(-boundary_hops) boundary_hops
        set boundary_hops ""
    }

    #Action Field
    if {[info exists param(-action_field)]} {

        upvar $param(-action_field) action_field
        set action_field ""
    }

    #LOGIC GOES HERE

    #Management Header
    wrptp_mgmt hdr = $hex

    set target_port_identity     $hdr::targetPortIdentity
    set starting_boundary_hops   [hex2dec $hdr::startingBoundaryHops]
    set boundary_hops            [hex2dec $hdr::boundaryHops]
    set action_field             [hex2dec $hdr::actionField]

    #Target Port Identity
    wrptp_port_identity port_identity = $target_port_identity

    set target_clock_identity [hex2dec $port_identity::clockIdentity]
    set target_port_number    [hex2dec $port_identity::portNumber]

    return 1

}

################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::encode_wrptp_pkt                              #
#                                                                              #
#  DEFINITION           : This function constructs PTP packet                  #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  (Ethernet HEADER)                                                           #
#                                                                              #
#  dest_mac             : (Optional) Destination MAC address of eth header     #
#                                    ( Default : 00:00:00:00:00:AA )           #
#                                                                              #
#  src_mac              : (Optional) Source MAC address of eth header          #
#                                    ( Default : 00:00:00:00:00:BB )           #
#                                                                              #
#  eth_type             : (Optional) Ethernet type/length of eth header        #
#                                    ( Default : 0800 )                        #
#                                                                              #
#                                                                              #
#  (VLAN HEADER)                                                               #
#                                                                              #
#  vlan_id              : (Optional) Vlan id of vlan header                    #
#                                    ( Default : -1 )                          #
#                                                                              #
#  vlan_priority        : (Optional) Priority of vlan header                   #
#                                    ( Default : 1 )                           #
#                                                                              #
#  vlan_dei             : (Optional) Eligible indicator of vlan header         #
#                                    ( Default : 0 )                           #
#                                                                              #
#                                                                              #
#  (IPv4 HEADER)                                                               #
#                                                                              #
#  ip_version           : (Optional) Version of ip                             #
#                                    ( Default : 4 )                           #
#                                                                              #
#  ip_header_length     : (Optional) Header length  of ipv4                    #
#                                    ( Default : 20)                           #
#                                                                              #
#  ip_tos               : (Optional) Type of service (hex value)               #
#                                    ( Default : 00 )                          #
#                                                                              #
#  ip_total_length      : (Optional) Total length  of ipv4                     #
#                                    ( Default : 72 )                          #
#                                                                              #
#  ip_identification    : (Optional) Unique id of ipv4                         #
#                                    ( Default : 0 )                           #
#                                                                              #
#  ip_flags             : (Optional) Flags represents the control or identify  #
#                                    fragments. ( Default : 0 )                #
#                                    ( 0 - Reserved , 1 - Don't fragment ,     #
#                                      2 - More fragment )                     #
#                                                                              #
#  ip_offset            : (Optional) Fragment offset                           #
#                                    ( Default : 0 )                           #
#                                                                              #
#  ip_ttl               : (Optional) Time to live                              #
#                                    ( Default : 64 )                          #
#                                                                              #
#  ip_checksum          : (Optional) Error-checking of header(16-bit checksum) #
#                                    ( Default : auto )                        #
#                                                                              #
#  ip_protocol          : (Optional) Protocol in the data portion of the       #
#                                    IP datagram. ( Default : 17 )             #
#                                                                              #
#  src_ip               : (Optional) IPv4 address of the sender of the packet  #
#                                    ( Default : 0.0.0.0 )                     #
#                                                                              #
#  dest_ip              : (Optional) IPv4 address of the reciever of the packet#
#                                    ( Default : 0.0.0.0 )                     #
#                                                                              #
#                                                                              #
#  (UDP HEADER)                                                                #
#                                                                              #
#  src_port             : (Optional) Sender's port number                      #
#                                    ( Default : 319 )                         #
#                                                                              #
#  dest_port            : (Optional) Receiver's port number                    #
#                                    ( Default : 319 )                         #
#                                                                              #
#  udp_length           : (Optional) Length in bytes of UDP header and data    #
#                                    ( Default : 52 )                          #
#                                                                              #
#  udp_checksum         : (Optional) Error-checking of the header              #
#                                    ( Default : auto )                        #
#                                                                              #
#                                                                              #
#  (PTP COMMON HEADER)                                                         #
#                                                                              #
#  transport_specific    :  (Optional)  Transport specific                     #
#                                       (Default : 0)                          #
#                                                                              #
#  message_type          :  (Optional)  Message Type                           #
#                                       (Default : 0)                          #
#                                                                              #
#  version_ptp           :  (Optional)  PTP version number                     #
#                                       (Default : 2)                          #
#                                                                              #
#  message_length        :  (Optional)  PTP Sync message length                #
#                                       (Default : 44)                         #
#                                                                              #
#  domain_number         :  (Optional)  Domain number                          #
#                                       (Default : 0)                          #
#                                                                              #
#  alternate_master_flag :  (Optional)  Alternate Master flag                  #
#                                       (Default : 0)                          #
#                                                                              #
#  two_step_flag         :  (Optional)  Two Step Flag                          #
#                                       (Default : 0)                          #
#                                                                              #
#  unicast_flag          :  (Optional)  Unicast Flag                           #
#                                       (Default : 0)                          #
#                                                                              #
#  profile_specific1     :  (Optional)  Profile specific1 Flag                 #
#                                       (Default : 0)                          #
#                                                                              #
#  profile_specific2     :  (Optional)  Profile specific2 Flag                 #
#                                       (Default : 0)                          #
#                                                                              #
#  leap61                :  (Optional)  Leap61 Flag                            #
#                                       (Default : 0)                          #
#                                                                              #
#  leap59                :  (Optional)  Leap59 Flag                            #
#                                       (Default : 0)                          #
#                                                                              #
#  current_utc_offset_valid:(Optional)  Current UTC Offset Flag                #
#                                       (Default : 0)                          #
#                                                                              #
#  ptp_timescale         :  (Optional)  PTP Timescale Flag                     #
#                                       (Default : 0)                          #
#                                                                              #
#  time_traceable        :  (Optional)  Time Traceable Flag                    #
#                                       (Default : 0)                          #
#                                                                              #
#  freq_traceable        :  (Optional)  Frequency Traceable Flag               #
#                                       (Default : 0)                          #
#                                                                              #
#  correction_field      :  (Optional)  Correction Field value                 #
#                                       (Default : 0)                          #
#                                                                              #
#  src_port_number       :  (Optional)  Source port number                     #
#                                       (Default : 0)                          #
#                                                                              #
#  src_clock_identity    :  (Optional)  Source Clock identity                  #
#                                       (Default : 0)                          #
#                                                                              #
#  sequence_id           :  (Optional)  Sequence ID                            #
#                                       (Default : 0)                          #
#                                                                              #
#  control_field         :  (Optional)  Control field                          #
#                                       (Default : 0)                          #
#                                                                              #
#  log_message_interval  :  (Optional)  Log message interval                   #
#                                       (Default : 127)                        #
#                                                                              #
#                                                                              #
#  (PTP SYNC/DELAY REQUEST HEADER)                                             #
#                                                                              #
#  origin_timestamp_sec       :  (Optional)  Origin timestamp in seconds       #
#                                            (Default : 0)                     #
#                                                                              #
#  origin_timestamp_ns        :  (Optional)  Origin timestamp in nanoseconds   #
#                                            (Default : 0)                     #
#                                                                              #
#                                                                              #
#  (PTP ANNOUNCE HEADER)                                                       #
#                                                                              #
#  current_utc_offset         :  (Optional)  Current UTC Offset                #
#                                            (Default : 0)                     #
#                                                                              #
#  gm_priority1               :  (Optional)  Grand Master Priority 1           #
#                                            (Default : 0)                     #
#                                                                              #
#  gm_priority2               :  (Optional)  Grand Master Priority 2           #
#                                            (Default : 128)                   #
#                                                                              #
#  gm_identity                :  (Optional)  Grand Master Identity             #
#                                            (Default : 0)                     #
#                                                                              #
#  steps_removed              :  (Optional)  Steps Removed                     #
#                                            (Default : 0)                     #
#                                                                              #
#  time_source                :  (Optional)  Time Source                       #
#                                            (Default : 0)                     #
#                                                                              #
#  gm_clock_class             :  (Optional)  Grand Master Clock class          #
#                                            (Default : 0)                     #
#                                                                              #
#  gm_clock_accuracy          :  (Optional)  Grand Master Clock accuracy       #
#                                            (Default : 0)                     #
#                                                                              #
#  gm_clock_variance          :  (Optional)  Grand Master Clock offset scaled  #
#                                        log variance (Default : 0)            #
#                                                                              #
#                                                                              #
#  (PTP OTHER HEADERS)                                                         #
#                                                                              #
#  precise_origin_timestamp_sec  :  (Optional)  Precise Origin Timestamp       #
#                                     (Default : 0)                            #
#                                                                              #
#  precise_origin_timestamp_ns   :  (Optional)  Precise Origin Timestamp       #
#                                     (Default : 0)                            #
#                                                                              #
#  receive_timestamp_sec         :  (Optional)  Receive Timestamp              #
#                                     (Default : 0)                            #
#                                                                              #
#  receive_timestamp_ns          :  (Optional)  Receive Timestamp              #
#                                     (Default : 0)                            #
#                                                                              #
#  requesting_port_number        :  (Optional)  Requesting Port Number         #
#                                       (Default : 0)                          #
#                                                                              #
#  requesting_clock_identity     :  (Optional)  Requesting Clock Identity      #
#                                       (Default : 0)                          #
#                                                                              #
#  request_receipt_timestamp_sec :  (Optional)  Request Receipt Timestamp      #
#                                       (Default : 0)                          #
#                                                                              #
#  request_receipt_timestamp_ns  :  (Optional)  Request Receipt Timestamp      #
#                                       (Default : 0)                          #
#                                                                              #
#  response_origin_timestamp_sec :  (Optional)  Response Origin Timestamp      #
#                                       (Default : 0)                          #
#                                                                              #
#  response_origin_timestamp_ns  :  (Optional)  Response Origin Timestamp      #
#                                      (Default : 0)                           #
#                                                                              #
#  target_port_number            :  (Optional)  Target Port number             #
#                                       (Default : 0)                          #
#                                                                              #
#  target_clock_identity         :  (Optional)  Target clock identity          #
#                                       (Default : 0)                          #
#                                                                              #
#  starting_boundary_hops        :  (Optional)  Starting Boundary Hops         #
#                                       (Default : 0)                          #
#                                                                              #
#  boundary_hops                 :  (Optional)  Boundary Hops                  #
#                                       (Default : 0)                          #
#                                                                              #
#  action_field                  :  (Optional)  Action Field                   #
#                                       (Default : 0)                          #
#                                                                              #
#                                                                              #
#  (PTP TLVs)                                                                  #
#                                                                              #
#  tlv_type                      :  (Optional)  TLV Type                       #
#                                       (Default : -1)                         #
#                                                                              #
#  tlv_length                    :  (Optional)  TLV length                     #
#                                       (Default : 8)                          #
#                                                                              #
#  organization_id               :  (Optional) Organization id                 #
#                                       (Default: 0x080030)                    #
#                                                                              #
#  magic_number                  :  (Optional) Magic number                    #
#                                       (Default: 0xDEAD)                      #
#                                                                              #
#  version_number                :  (Optional) Version number                  #
#                                       (Default: 1)                           #
#                                                                              #
#  message_id                    :  (Optional) Message id                      #
#                                       (Default: 0x2000)                      #
#                                                                              #
#  wrconfig                      :  (Optional) Wrconfig                        #
#                                       (Default: 0)                           #
#                                                                              #
#  calibrated                    :  (Optional) Calibrated                      #
#                                       (Default: 0)                           #
#                                                                              #
#  wrmode_on                     :  (Optional) WR Mode ON                      #
#                                       (Default: 0)                           #
#                                                                              #
#  cal_send_pattern              :  (Optional) Cal send pattern                #
#                                       (Default: 0)                           #
#                                                                              #
#  cal_retry                     :  (Optional) Cal retry                       #
#                                       (Default: 0)                           #
#                                                                              #
#  cal_period                    :  (Optional) Cal period                      #
#                                       (Default: 0)                           #
#                                                                              #
#  delta_tx                      :  (Optional) Delta tx                        #
#                                       (Default: 0)                           #
#                                                                              #
#  delta_rx                      :  (Optional) Delta rx                        #
#                                       (Default: 0)                           #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  hexdump                       :  (Optional)  PTP packet hex dump            #
#                                                                              #
#  RETURNS              : 1 on success, 0 on failure                           #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#   pbLib::encode_wrptp_pkt\                                                   #
#              -dest_mac                       $dest_mac\                      #
#              -src_mac                        $src_mac\                       #
#              -eth_type                       $eth_type\                      #
#              -vlan_id                        $vlan_id\                       #
#              -vlan_priority                  $vlan_priority\                 #
#              -vlan_dei                       $vlan_dei\                      #
#              -ip_version                     $ip_version\                    #
#              -ip_header_length               $ip_header_length\              #
#              -ip_tos                         $ip_tos\                        #
#              -ip_total_length                $ip_total_length\               #
#              -ip_identification              $ip_identification\             #
#              -ip_flags                       $ip_flags\                      #
#              -ip_offset                      $ip_offset\                     #
#              -ip_ttl                         $ip_ttl\                        #
#              -ip_checksum                    $ip_checksum\                   #
#              -ip_protocol                    $ip_protocol\                   #
#              -src_ip                         $src_ip\                        #
#              -dest_ip                        $dest_ip\                       #
#              -src_port                       $src_port\                      #
#              -dest_port                      $dest_port\                     #
#              -udp_length                     $udp_length\                    #
#              -udp_checksum                   $udp_checksum\                  #
#              -transport_specific             $transport_specific\            #
#              -message_type                   $message_type\                  #
#              -version_ptp                    $version_ptp\                   #
#              -message_length                 $message_length\                #
#              -domain_number                  $domain_number\                 #
#              -alternate_master_flag          $alternate_master_flag\         #
#              -two_step_flag                  $two_step_flag\                 #
#              -unicast_flag                   $unicast_flag\                  #
#              -profile_specific1              $profile_specific1\             #
#              -profile_specific2              $profile_specific2\             #
#              -leap61                         $leap61\                        #
#              -leap59                         $leap59\                        #
#              -current_utc_offset_valid       $current_utc_offset_valid\      #
#              -ptp_timescale                  $ptp_timescale\                 #
#              -time_traceable                 $time_traceable\                #
#              -freq_traceable                 $freq_traceable\                #
#              -correction_field               $correction_field\              #
#              -src_port_number                $src_port_number\               #
#              -src_clock_identity             $src_clock_identity\            #
#              -sequence_id                    $sequence_id\                   #
#              -control_field                  $control_field\                 #
#              -log_message_interval           $log_message_interval\          #
#              -origin_timestamp_sec           $origin_timestamp_sec\          #
#              -origin_timestamp_ns            $origin_timestamp_ns\           #
#              -current_utc_offset             $current_utc_offset\            #
#              -gm_priority1                   $gm_priority1\                  #
#              -gm_priority2                   $gm_priority2\                  #
#              -gm_identity                    $gm_identity\                   #
#              -steps_removed                  $steps_removed\                 #
#              -time_source                    $time_source\                   #
#              -gm_clock_class                 $gm_clock_class\                #
#              -gm_clock_accuracy              $gm_clock_accuracy\             #
#              -gm_clock_variance              $gm_clock_variance\             #
#              -precise_origin_timestamp_sec   $precise_origin_timestamp_sec\  #
#              -precise_origin_timestamp_ns    $precise_origin_timestamp_ns\   #
#              -receive_timestamp_sec          $receive_timestamp_sec\         #
#              -receive_timestamp_ns           $receive_timestamp_ns\          #
#              -requesting_port_number         $requesting_port_number\        #
#              -requesting_clock_identity      $requesting_clock_identity\     #
#              -request_receipt_timestamp_sec  $request_receipt_timestamp_sec\ #
#              -request_receipt_timestamp_ns   $request_receipt_timestamp_ns\  #
#              -response_origin_timestamp_sec  $response_origin_timestamp_sec\ #
#              -response_origin_timestamp_ns   $response_origin_timestamp_ns\  #
#              -target_port_number             $target_port_number\            #
#              -target_clock_identity          $target_clock_identity\         #
#              -starting_boundary_hops         $starting_boundary_hops\        #
#              -boundary_hops                  $boundary_hops\                 #
#              -action_field                   $action_field\                  #
#              -tlv_type                       $tlv_type\                      #
#              -tlv_length                     $tlv_length\                    #
#              -organization_id                $organization_id\               #
#              -magic_number                   $magic_number\                  #
#              -version_number                 $version_number\                #
#              -message_id                     $message_id\                    #
#              -wrconfig                       $wrconfig\                      #
#              -calibrated                     $calibrated\                    #
#              -wrmode_on                      $wrmode_on\                     #
#              -cal_send_pattern               $cal_send_pattern\              #
#              -cal_retry                      $cal_retry\                     #
#              -cal_period                     $cal_period\                    #
#              -delta_tx                       $delta_tx\                      #
#              -delta_rx                       $delta_rx\                      #
#              -hexdump                        hexdump                         #
#                                                                              #
################################################################################
#
proc pbLib::encode_wrptp_pkt {args} {

    array set param $args

    #INPUT PARAMETERS

    #Destination MAC address of eth header
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
    } else {

        set dest_mac 00:00:00:00:00:AA
    }

    #Source MAC address of eth header
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
    } else {

        set src_mac 00:00:00:00:00:BB
    }

    #Ethernet type
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
    } else {

        set eth_type 0800
    }

    #Vlan id of vlan header
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
    } else {

        set vlan_id -1
    }

    #Priority of vlan header
    if {[info exists param(-vlan_priority)]} {

        set vlan_priority $param(-vlan_priority)
    } else {

        set vlan_priority 1
    }

    #Eligible indicator of vlan header
    if {[info exists param(-vlan_dei)]} {

        set vlan_dei $param(-vlan_dei)
    } else {

        set vlan_dei 0
    }

    #Version of ip
    if {[info exists param(-ip_version)]} {

        set ip_version $param(-ip_version)
    } else {

        set ip_version 4
    }

    #Header length  of ipv4
    if {[info exists param(-ip_header_length)]} {

        set ip_header_length $param(-ip_header_length)
    } else {

        set ip_header_length 20
    }

    #Type of service
    if {[info exists param(-ip_tos)]} {

        set ip_tos $param(-ip_tos)
    } else {

        set ip_tos 00
    }

    #Total length  of ipv4
    if {[info exists param(-ip_total_length)]} {

        set ip_total_length $param(-ip_total_length)
    } else {

        set ip_total_length 72
    }

    #Unique id of ipv4
    if {[info exists param(-ip_identification)]} {

        set ip_identification $param(-ip_identification)
    } else {

        set ip_identification 0
    }

    #Flags represents the control or identify
    if {[info exists param(-ip_flags)]} {

        set ip_flags $param(-ip_flags)
    } else {

        set ip_flags 0
    }

    #Fragment offset
    if {[info exists param(-ip_offset)]} {

        set ip_offset $param(-ip_offset)
    } else {

        set ip_offset 0
    }

    #Time to live
    if {[info exists param(-ip_ttl)]} {

        set ip_ttl $param(-ip_ttl)
    } else {

        set ip_ttl 64
    }

    #Error
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
    } else {

        set ip_checksum "auto"
    }

    #Protocol in the data portion of the
    if {[info exists param(-ip_protocol)]} {

        set ip_protocol $param(-ip_protocol)
    } else {

        set ip_protocol 17
    }

    #IPv4 address of the sender of the packet
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
    } else {

        set src_ip 0.0.0.0
    }

    #IPv4 address of the reciever of the packet
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
    } else {

        set dest_ip 0.0.0.0
    }

    #Sender
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
    } else {

        set src_port 319
    }

    #Receiver
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
    } else {

        set dest_port 319
    }

    #Length in bytes of UDP header and data
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
    } else {

        set udp_length 52
    }

    #Error
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
    } else {

        set udp_checksum auto
    }

    #Transport specific
    if {[info exists param(-transport_specific)]} {

        set transport_specific $param(-transport_specific)
    } else {

        set transport_specific 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
    } else {

        set message_type 0
    }

    #PTP version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
    } else {

        set message_length 44
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
    } else {

        set domain_number 0
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
    } else {

        set unicast_flag 0
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
    } else {

        set profile_specific2 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
    } else {

        set current_utc_offset_valid 0
    }

    #PTP Timescale Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
    } else {

        set freq_traceable 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
    } else {

        set correction_field 0
    }

    #Source port number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
    } else {

        set src_port_number 0
    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
    } else {

        set src_clock_identity 0
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
    } else {

        set sequence_id 0
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
    } else {

        set control_field 0
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
    } else {

        set log_message_interval 127
    }

    #Origin timestamp in seconds
    if {[info exists param(-origin_timestamp_sec)]} {

        set origin_timestamp_sec $param(-origin_timestamp_sec)
    } else {

        set origin_timestamp_sec 0
    }

    #Origin timestamp in nanoseconds
    if {[info exists param(-origin_timestamp_ns)]} {

        set origin_timestamp_ns $param(-origin_timestamp_ns)
    } else {

        set origin_timestamp_ns 0
    }

    #Current UTC Offset
    if {[info exists param(-current_utc_offset)]} {

        set current_utc_offset $param(-current_utc_offset)
    } else {

        set current_utc_offset 0
    }

    #Grand Master Priority 1
    if {[info exists param(-gm_priority1)]} {

        set gm_priority1 $param(-gm_priority1)
    } else {

        set gm_priority1 0
    }

    #Grand Master Priority 2
    if {[info exists param(-gm_priority2)]} {

        set gm_priority2 $param(-gm_priority2)
    } else {

        set gm_priority2 128
    }

    #Grand Master Identity
    if {[info exists param(-gm_identity)]} {

        set gm_identity $param(-gm_identity)
    } else {

        set gm_identity 0
    }

    #Steps Removed
    if {[info exists param(-steps_removed)]} {

        set steps_removed $param(-steps_removed)
    } else {

        set steps_removed 0
    }

    #Time Source
    if {[info exists param(-time_source)]} {

        set time_source $param(-time_source)
    } else {

        set time_source 0
    }

    #Grand Master Clock class
    if {[info exists param(-gm_clock_class)]} {

        set gm_clock_class $param(-gm_clock_class)
    } else {

        set gm_clock_class 0
    }

    #Grand Master Clock accuracy
    if {[info exists param(-gm_clock_accuracy)]} {

        set gm_clock_accuracy $param(-gm_clock_accuracy)
    } else {

        set gm_clock_accuracy 0
    }

    #Grand Master Clock offset scaled
    if {[info exists param(-gm_clock_variance)]} {

        set gm_clock_variance $param(-gm_clock_variance)
    } else {

        set gm_clock_variance 0
    }

    #Precise Origin Timestamp
    if {[info exists param(-precise_origin_timestamp_sec)]} {

        set precise_origin_timestamp_sec $param(-precise_origin_timestamp_sec)
    } else {

        set precise_origin_timestamp_sec 0
    }

    #Precise Origin Timestamp
    if {[info exists param(-precise_origin_timestamp_ns)]} {

        set precise_origin_timestamp_ns $param(-precise_origin_timestamp_ns)
    } else {

        set precise_origin_timestamp_ns 0
    }

    #Receive Timestamp
    if {[info exists param(-receive_timestamp_sec)]} {

        set receive_timestamp_sec $param(-receive_timestamp_sec)
    } else {

        set receive_timestamp_sec 0
    }

    #Receive Timestamp
    if {[info exists param(-receive_timestamp_ns)]} {

        set receive_timestamp_ns $param(-receive_timestamp_ns)
    } else {

        set receive_timestamp_ns 0
    }

    #Requesting Port Number
    if {[info exists param(-requesting_port_number)]} {

        set requesting_port_number $param(-requesting_port_number)
    } else {

        set requesting_port_number 0
    }

    #Requesting Clock Identity
    if {[info exists param(-requesting_clock_identity)]} {

        set requesting_clock_identity $param(-requesting_clock_identity)
    } else {

        set requesting_clock_identity 0
    }

    #Request Receipt Timestamp
    if {[info exists param(-request_receipt_timestamp_sec)]} {

        set request_receipt_timestamp_sec $param(-request_receipt_timestamp_sec)
    } else {

        set request_receipt_timestamp_sec 0
    }

    #Request Receipt Timestamp
    if {[info exists param(-request_receipt_timestamp_ns)]} {

        set request_receipt_timestamp_ns $param(-request_receipt_timestamp_ns)
    } else {

        set request_receipt_timestamp_ns 0
    }

    #Response Origin Timestamp
    if {[info exists param(-response_origin_timestamp_sec)]} {

        set response_origin_timestamp_sec $param(-response_origin_timestamp_sec)
    } else {

        set response_origin_timestamp_sec 0
    }

    #Response Origin Timestamp
    if {[info exists param(-response_origin_timestamp_ns)]} {

        set response_origin_timestamp_ns $param(-response_origin_timestamp_ns)
    } else {

        set response_origin_timestamp_ns 0
    }

    #Target Port number
    if {[info exists param(-target_port_number)]} {

        set target_port_number $param(-target_port_number)
    } else {

        set target_port_number 0
    }

    #Target clock identity
    if {[info exists param(-target_clock_identity)]} {

        set target_clock_identity $param(-target_clock_identity)
    } else {

        set target_clock_identity 0
    }

    #Starting Boundary Hops
    if {[info exists param(-starting_boundary_hops)]} {

        set starting_boundary_hops $param(-starting_boundary_hops)
    } else {

        set starting_boundary_hops 0
    }

    #Boundary Hops
    if {[info exists param(-boundary_hops)]} {

        set boundary_hops $param(-boundary_hops)
    } else {

        set boundary_hops 0
    }

    #Action Field
    if {[info exists param(-action_field)]} {

        set action_field $param(-action_field)
    } else {

        set action_field 0
    }

    #TLV Type
    if {[info exists param(-tlv_type)]} {

        set tlv_type $param(-tlv_type)
    } else {

        set tlv_type -1
    }

    #TLV length
    if {[info exists param(-tlv_length)]} {

        set tlv_length $param(-tlv_length)
    } else {

        set tlv_length 8
    }

    #Organisation ID
    if {[info exists param(-organization_id)]} {

        set organization_id $param(-organization_id)
    } else {

        set organization_id [expr 0x080030]
    }

    #Magic Number
    if {[info exists param(-magic_number)]} {

        set magic_number $param(-magic_number)
    } else {

        set magic_number [expr 0xDEAD]
    }

    #Version Number
    if {[info exists param(-version_number)]} {

        set version_number $param(-version_number)
    } else {

        set version_number 1
    }

    #Message ID
    if {[info exists param(-message_id)]} {

        set message_id $param(-message_id)
    } else {

        set message_id [expr 0x2000]
    }

    if {[info exists param(-wrconfig)]} {

        set wrconfig $param(-wrconfig)
    } else {

        set wrconfig 0
    }

    if {[info exists param(-calibrated)]} {

        set calibrated $param(-calibrated)
    } else {

        set calibrated 0
    }

    if {[info exists param(-wrmode_on)]} {

        set wrmode_on $param(-wrmode_on)
    } else {

        set wrmode_on 0
    }

    if {[info exists param(-cal_send_pattern)]} {

        set cal_send_pattern $param(-cal_send_pattern)
    } else {

        set cal_send_pattern 0
    }

    if {[info exists param(-cal_retry)]} {

        set cal_retry $param(-cal_retry)
    } else {

        set cal_retry 0
    }

    if {[info exists param(-cal_period)]} {

        set cal_period $param(-cal_period)
    } else {

        set cal_period 0
    }

    if {[info exists param(-delta_tx)]} {

        set delta_tx $param(-delta_tx)
    } else {

        set delta_tx 0
    }

    if {[info exists param(-delta_rx)]} {

        set delta_rx $param(-delta_rx)
    } else {

        set delta_rx 0
    }

    #OUTPUT PARAMETERS

    #PTP packet hex dump
    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }


    #LOGIC GOES HERE
    set packet ""

    if {(($src_ip == "0.0.0.0") && ($dest_ip == "0.0.0.0")) || $src_ip == "" || $dest_ip == ""} {
        set transport_type "Ethernet"
        set eth_type 88F7
    } else {
        set transport_type "IPv4/UDP"
        set eth_type 0800
    }

    #TLV
    pbLib::encode_wrptp_tlv\
                -type                     $tlv_type\
                -length                   $tlv_length\
                -organization_id          $organization_id\
                -magic_number             $magic_number\
                -version_number           $version_number\
                -message_id               $message_id\
                -wrconfig                 $wrconfig\
                -calibrated               $calibrated\
                -wrmode_on                $wrmode_on\
                -cal_send_pattern         $cal_send_pattern\
                -cal_retry                $cal_retry\
                -cal_period               $cal_period\
                -delta_tx                 $delta_tx\
                -delta_rx                 $delta_rx\
                -hexdump                  tlv

    prepend packet $tlv

    #PTP Message Header
    switch $message_type {
        0 {
            #Sync
            pbLib::encode_wrptp_sync_header\
                                         -origin_timestamp_sec       $origin_timestamp_sec\
                                         -origin_timestamp_ns        $origin_timestamp_ns\
                                         -hexdump                    header
        }
        1 {
            #Delay Request
            pbLib::encode_wrptp_delay_req_header\
                                         -origin_timestamp_sec       $origin_timestamp_sec\
                                         -origin_timestamp_ns        $origin_timestamp_ns\
                                         -hexdump                    header
        }
        2 {
            #Peer Delay Request
            pbLib::encode_wrptp_pdelay_req_header\
                                         -origin_timestamp_sec       $origin_timestamp_sec\
                                         -origin_timestamp_ns        $origin_timestamp_ns\
                                         -hexdump                    header
        }
        3 {
            #Peer Delay Response
            pbLib::encode_wrptp_pdelay_resp_header\
                                         -request_receipt_timestamp_sec  $request_receipt_timestamp_sec\
                                         -request_receipt_timestamp_ns   $request_receipt_timestamp_ns\
                                         -requesting_port_number         $requesting_port_number\
                                         -requesting_clock_identity      $requesting_clock_identity\
                                         -hexdump                        header
        }
        8 {
            #Follow up
            pbLib::encode_wrptp_followup_header\
                                         -precise_origin_timestamp_sec  $precise_origin_timestamp_sec\
                                         -precise_origin_timestamp_ns   $precise_origin_timestamp_ns\
                                         -hexdump                       header
        }
        9 {
            #Delay Response
            pbLib::encode_wrptp_delay_resp_header\
                                         -receive_timestamp_sec        $receive_timestamp_sec\
                                         -receive_timestamp_ns         $receive_timestamp_ns\
                                         -requesting_port_number       $requesting_port_number\
                                         -requesting_clock_identity    $requesting_clock_identity\
                                         -hexdump                      header
        }
       10 {
            #Peer Delay Response Follow up
            pbLib::encode_wrptp_pdelay_resp_followup_header\
                                         -response_origin_timestamp_sec $response_origin_timestamp_sec\
                                         -response_origin_timestamp_ns  $response_origin_timestamp_ns\
                                         -requesting_port_number        $requesting_port_number\
                                         -requesting_clock_identity     $requesting_clock_identity\
                                         -hexdump                       header
        }
       11 {
            #Announce
            pbLib::encode_wrptp_announce_header\
                                         -origin_timestamp_sec       $origin_timestamp_sec\
                                         -origin_timestamp_ns        $origin_timestamp_ns\
                                         -current_utc_offset         $current_utc_offset\
                                         -gm_priority1               $gm_priority1\
                                         -gm_priority2               $gm_priority2\
                                         -gm_identity                $gm_identity\
                                         -steps_removed              $steps_removed\
                                         -time_source                $time_source\
                                         -gm_clock_class             $gm_clock_class\
                                         -gm_clock_accuracy          $gm_clock_accuracy\
                                         -gm_clock_variance          $gm_clock_variance\
                                         -hexdump                    header
        }
       12 {
            #Signalling
            pbLib::encode_wrptp_signal_header\
                                         -target_port_number                $target_port_number\
                                         -target_clock_identity             $target_clock_identity\
                                         -hexdump                           header
        }
       13 {
            #Management
            pbLib::encode_wrptp_mgmt_header\
                                      -target_port_number         $target_port_number\
                                      -target_clock_identity      $target_clock_identity\
                                      -starting_boundary_hops     $starting_boundary_hops\
                                      -boundary_hops              $boundary_hops\
                                      -action_field               $action_field\
                                      -hexdump                    header
        }
       default {
            #reserved
            set header ""
        }
    }

    prepend packet $header

    set message_length [size_of $packet 34]

    #ENCODE PTP HEADER
    pbLib::encode_wrptp_common_header\
                -transport_specific                $transport_specific\
                -message_type                      $message_type\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -correction_field                  $correction_field\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -src_clock_identity                $src_clock_identity\
                -src_port_number                   $src_port_number\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_traceable                    $time_traceable\
                -freq_traceable                    $freq_traceable\
                -hexdump                           wrptp_common_header

    prepend packet $wrptp_common_header

    if {$transport_type == "IPv4/UDP"} {

        set udp_length [expr $message_length + 8]

        set ip_pseudo_header ""
        append ip_pseudo_header [dec2hex $ip_protocol 4]
        append ip_pseudo_header [ip2hex $src_ip]
        append ip_pseudo_header [ip2hex $dest_ip]

        #ENCODE UDP HEADER
        pbLib::encode_udp_header\
                    -src_port        $src_port\
                    -dest_port       $dest_port\
                    -payload_length  $udp_length\
                    -checksum        $udp_checksum\
                    -pseudo_header   $ip_pseudo_header\
                    -data            $packet\
                    -hexdump         udp_header

        prepend packet $udp_header
        set ip_total_length [expr $udp_length + $ip_header_length]

        #ENCODE IPv4 HEADER
        pbLib::encode_ipv4_header\
                    -version         $ip_version\
                    -header_length   $ip_header_length\
                    -tos             $ip_tos\
                    -total_length    $ip_total_length\
                    -identification  $ip_identification\
                    -flags           $ip_flags\
                    -ttl             $ip_ttl\
                    -offset          $ip_offset\
                    -next_protocol   $ip_protocol\
                    -checksum        $ip_checksum\
                    -src_ip          $src_ip\
                    -dest_ip         $dest_ip\
                    -hexdump         ipv4_header

        prepend packet $ipv4_header
    }

    #ENCODE ETHERNET HEADER
    pbLib::encode_eth_header\
                -dest_mac    $dest_mac\
                -src_mac     $src_mac\
                -eth_type    $eth_type\
                -hexdump     eth_header

    prepend packet $eth_header

    if {[string is integer $vlan_id] && ($vlan_id >= 0) && ($vlan_id <= 4095) } {
        set packet [pbLib::push_vlan_tag\
                        -packet          $packet\
                        -vlan_id         $vlan_id\
                        -priority        $vlan_priority\
                        -dei             $vlan_dei]
    }

    set hexdump $packet
    return 1

}


################################################################################
#  PROCEDURE NAME       : pbLib::encode_port_identity                          #
#                                                                              #
#  DEFINITION           : This function encodes port identity.                 #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  clock_identity       : (Mandatory) Clock Identity                           #
#                                                                              #
#  port_number          : (Mandatory) Port Number                              #
#                                                                              #
#  OUTPUT PARAMETERS    : NONE                                                 #
#                                                                              #
#  RETURNS              : Port identity hex                                    #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#     pbLib::encode_port_identity $clock_identity $port_number                 #
#                                                                              #
################################################################################

proc pbLib::encode_port_identity {clock_identity port_number} {

    if {($clock_identity == "") || ($port_number == "")} {
        return ""
    }

    wrptp_port_identity port_identity

    set port_identity::clockIdentity   [dec2hex $clock_identity 16]
    set port_identity::portNumber      [dec2hex $port_number 4]

    return [port_identity::pkt]

}


################################################################################
#  PROCEDURE NAME       : pbLib::encode_wrptp_tlv                              #
#                                                                              #
#  DEFINITION           : This function encodes WRPTP TLVs                     #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#                                                                              #
#  type                 :  (Optional) Tlv type                                 #
#                              (Default: 0x0003)                               #
#                                                                              #
#  length               :  (Optional) Tlv length                               #
#                              (Default: 8)                                    #
#                                                                              #
#  organization_id      :  (Optional) Organization id                          #
#                              (Default: 0x080030)                             #
#                                                                              #
#  magic_number         :  (Optional) Magic number                             #
#                              (Default: 0xDEAD)                               #
#                                                                              #
#  version_number       :  (Optional) Version number                           #
#                              (Default: 1)                                    #
#                                                                              #
#  message_id           :  (Optional) Message id                               #
#                              (Default: 0x2000)                               #
#                                                                              #
#  wrconfig             :  (Optional) Wrconfig                                 #
#                              (Default: 0)                                    #
#                                                                              #
#  calibrated           :  (Optional) Calibrated                               #
#                              (Default: 0)                                    #
#                                                                              #
#  wrmode_on            :  (Optional) WR Mode ON                               #
#                              (Default: 0)                                    #
#                                                                              #
#  cal_send_pattern     :  (Optional) Cal send pattern                         #
#                              (Default: 0)                                    #
#                                                                              #
#  cal_retry            :  (Optional) Cal retry                                #
#                              (Default: 0)                                    #
#                                                                              #
#  cal_period           :  (Optional) Cal period                               #
#                              (Default: 0)                                    #
#                                                                              #
#  delta_tx             :  (Optional) Delta tx                                 #
#                              (Default: 0)                                    #
#                                                                              #
#  delta_rx             :  (Optional) Delta rx                                 #
#                              (Default: 0)                                    #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  hexdump              :  (Optional)  Hex dump of TLV                         #
#                                                                              #
#  RETURNS              : hex                                                  #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#     pbLib::encode_wrptp_tlv\                                                 #
#                 -type                     $tlv_type\                         #
#                 -length                   $tlv_length\                       #
#                 -organization_id          $organization_id\                  #
#                 -magic_number             $magic_number\                     #
#                 -version_number           $version_number\                   #
#                 -message_id               $message_id\                       #
#                 -wrconfig                 $wrconfig\                         #
#                 -calibrated               $calibrated\                       #
#                 -wrmode_on                $wrmode_on\                        #
#                 -cal_send_pattern         $cal_send_pattern\                 #
#                 -cal_retry                $cal_retry\                        #
#                 -cal_period               $cal_period\                       #
#                 -delta_tx                 $delta_tx\                         #
#                 -delta_rx                 $delta_rx\                         #
#                 -hexdump                  hexdump                            #
#                                                                              #
################################################################################

proc pbLib::encode_wrptp_tlv {args} {

    array set param $args

    #INPUT PARAMETERS

    if {[info exists param(-type)]} {

        set type $param(-type)
    } else {

        set type 3
    }

    if {[info exists param(-length)]} {

        set length $param(-length)
    } else {

        set length 8
    }

    if {[info exists param(-organization_id)]} {

        set organization_id $param(-organization_id)
    } else {

        set organization_id [expr 0x080030]
    }

    if {[info exists param(-magic_number)]} {

        set magic_number $param(-magic_number)
    } else {

        set magic_number [expr 0xDEAD]
    }

    if {[info exists param(-version_number)]} {

        set version_number $param(-version_number)
    } else {

        set version_number 1
    }

    if {[info exists param(-message_id)]} {

        set message_id $param(-message_id)
    } else {

        set message_id [expr 0x2000]
    }

    if {[info exists param(-wrconfig)]} {

        set wrconfig $param(-wrconfig)
    } else {

        set wrconfig 0
    }

    if {[info exists param(-calibrated)]} {

        set calibrated $param(-calibrated)
    } else {

        set calibrated 0
    }

    if {[info exists param(-wrmode_on)]} {

        set wrmode_on $param(-wrmode_on)
    } else {

        set wrmode_on 0
    }

    if {[info exists param(-cal_send_pattern)]} {

        set cal_send_pattern $param(-cal_send_pattern)
    } else {

        set cal_send_pattern 0
    }

    if {[info exists param(-cal_retry)]} {

        set cal_retry $param(-cal_retry)
    } else {

        set cal_retry 0
    }

    if {[info exists param(-cal_period)]} {

        set cal_period $param(-cal_period)
    } else {

        set cal_period 0
    }

    if {[info exists param(-delta_tx)]} {

        set delta_tx $param(-delta_tx)
    } else {

        set delta_tx 0
    }

    if {[info exists param(-delta_rx)]} {

        set delta_rx $param(-delta_rx)
    } else {

        set delta_rx 0
    }

    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    #Invalid tlv_type, may be No TLV
    if {!([string is integer $type] && ($type >= 0) && ($type <= 65535))} {
        return $hexdump
    }

    #Invalid organization specific tlv, may be No TLV
    if {$type == 3} {
        if {!([string is integer $message_id] && ($message_id >= 0) && ($message_id <= 65535))} {
            return $hexdump
        }
    }


    #TLV
    wrptp_tlv wrtlv

    set wrtlv::tlvType         [dec2hex $type 4]
    set wrtlv::lengthField     [dec2hex $length 4]
    set wrtlv::organizationId  [dec2hex $organization_id 6]
    set wrtlv::magicNumber     [dec2hex $magic_number 4]
    set wrtlv::versionNumber   [dec2hex $version_number 2]
    set wrtlv::wrMessageId     [dec2hex $message_id 4]

    switch $wrtlv::wrMessageId {

        "2000" {
            #ANN_SUFIX
            wrptp_wrflags tlv_data

            set tlv_data::wrConfig      [dec2hex $wrconfig 1]
            set tlv_data::calibrated    $calibrated
            set tlv_data::wrModeOn      $wrmode_on
            set tlv_data::reserved1     0
            set tlv_data::reserved2     0

            set data [tlv_data::pkt]
            set length [size_of $data 8]
            set wrtlv::lengthField [dec2hex $length 2]

            append hexdump [wrtlv::pkt]
            append hexdump $data

        }

        "1003" {
            #CALIBRATE
            wrptp_calibrate_data tlv_data

            set tlv_data::calSendPattern    [dec2hex $cal_send_pattern 2]
            set tlv_data::calRetry          [dec2hex $cal_retry 2]
            set tlv_data::calPeriod         [dec2hex $cal_period 8]

            set data [tlv_data::pkt]
            set length [size_of $data 8]
            set wrtlv::lengthField [dec2hex $length 2]

            append hexdump [wrtlv::pkt]
            append hexdump $data

        }

        "1004" {
            #CALIBRATED
            wrptp_calibrated_data tlv_data

            #Unit is 2^16 ps
            set delta_tx [expr wide($delta_tx)*65536]
            set delta_rx [expr wide($delta_rx)*65536]

            set tlv_data::deltaTx   [dec2hex $delta_tx 16]
            set tlv_data::deltaRx   [dec2hex $delta_rx 16]

            set data [tlv_data::pkt]
            set length [size_of $data 8]
            set wrtlv::lengthField [dec2hex $length 2]

            append hexdump [wrtlv::pkt]
            append hexdump $data

        }

        default {
            #SLAVE_PRESENT[1000], LOCK[1001], LOCKED[1002], WR_MODE_ON[1005]
            set hexdump [wrtlv::pkt]
        }
    }

    return 1

}


################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::decode_wrptp_tlv                              #
#                                                                              #
#  DEFINITION           : This function decodes WRPTP TLVs                     #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  hex                  : (Mandatory) Hex dump of TLV                          #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  type                 :  (Optional) Tlv type                                 #
#                                                                              #
#  length               :  (Optional) Tlv length                               #
#                                                                              #
#  organization_id      :  (Optional) Organization id                          #
#                                                                              #
#  magic_number         :  (Optional) Magic number                             #
#                                                                              #
#  version_number       :  (Optional) Version number                           #
#                                                                              #
#  message_id           :  (Optional) Message id                               #
#                                                                              #
#  wrconfig             :  (Optional) Wrconfig                                 #
#                                                                              #
#  calibrated           :  (Optional) Calibrated                               #
#                                                                              #
#  wrmode_on            :  (Optional) WR Mode ON                               #
#                                                                              #
#  cal_send_pattern     :  (Optional) Cal send pattern                         #
#                                                                              #
#  cal_retry            :  (Optional) Cal retry                                #
#                                                                              #
#  cal_period           :  (Optional) Cal period                               #
#                                                                              #
#  delta_tx             :  (Optional) Delta tx                                 #
#                                                                              #
#  delta_rx             :  (Optional) Delta rx                                 #
#                                                                              #
#  RETURNS              : 0 on failure                                         #
#                         1 on success                                         #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#     pbLib::decode_wrptp_tlv\                                                 #
#                 -hex                      $hex\                              #
#                 -type                     tlv_type\                          #
#                 -length                   tlv_length\                        #
#                 -organization_id          organization_id\                   #
#                 -magic_number             magic_number\                      #
#                 -version_number           version_number\                    #
#                 -message_id               message_id\                        #
#                 -wrconfig                 wrconfig\                          #
#                 -calibrated               calibrated\                        #
#                 -wrmode_on                wrmode_on\                         #
#                 -cal_send_pattern         cal_send_pattern\                  #
#                 -cal_retry                cal_retry\                         #
#                 -cal_period               cal_period\                        #
#                 -delta_tx                 delta_tx\                          #
#                 -delta_rx                 delta_rx                           #
#                                                                              #
################################################################################

proc pbLib::decode_wrptp_tlv {args} {

    array set param $args

    #INPUT PARAMETERS
    if {[info exists param(-hex)]} {

        set hex $param(-hex)

    } else {

        return -code error "pbLib::decode_wrptp_tlv: Mandatory parameter missing - hex"
    }

    #OUTPUT PARAMETERS

    #TLV Type
    if {[info exists param(-type)]} {

        upvar $param(-type) type
        set type ""
    }

    #TLV Length
    if {[info exists param(-length)]} {

        upvar $param(-length) length
        set length ""
    }

    #Organisation ID
    if {[info exists param(-organization_id)]} {

        upvar $param(-organization_id) organization_id
        set organization_id ""
    }

    #Magic Number
    if {[info exists param(-magic_number)]} {

        upvar $param(-magic_number) magic_number
        set magic_number ""
    }

    #Version Number
    if {[info exists param(-version_number)]} {

        upvar $param(-version_number) version_number
        set version_number ""
    }

    #Message ID
    if {[info exists param(-message_id)]} {

        upvar $param(-message_id) message_id
        set message_id ""
    }

    #WR Config
    if {[info exists param(-wrconfig)]} {

        upvar $param(-wrconfig) wrconfig
        set wrconfig ""
    }

    #Calibrated
    if {[info exists param(-calibrated)]} {

        upvar $param(-calibrated) calibrated
        set calibrated ""
    }

    #WR Mode ON
    if {[info exists param(-wrmode_on)]} {

        upvar $param(-wrmode_on) wrmode_on
        set wrmode_on ""
    }

    #Cal Send Pattern
    if {[info exists param(-cal_send_pattern)]} {

        upvar $param(-cal_send_pattern) cal_send_pattern
        set cal_send_pattern ""
    }

    #Cal Retry
    if {[info exists param(-cal_retry)]} {

        upvar $param(-cal_retry) cal_retry
        set cal_retry ""
    }

    #Cal Period
    if {[info exists param(-cal_period)]} {

        upvar $param(-cal_period) cal_period
        set cal_period ""
    }

    #Delta Tx
    if {[info exists param(-delta_tx)]} {

        upvar $param(-delta_tx) delta_tx
        set delta_tx ""
    }

    #Delta Rx
    if {[info exists param(-delta_rx)]} {

        upvar $param(-delta_rx) delta_rx
        set delta_rx ""
    }

    #No TLV
    if {$hex == ""} {
        return 0
    }

    #TLV
    wrptp_tlv wrtlv = $hex

    set type            [hex2dec $wrtlv::tlvType]
    set length          [hex2dec $wrtlv::lengthField]
    set organization_id [hex2dec $wrtlv::organizationId]
    set magic_number    [hex2dec $wrtlv::magicNumber]
    set version_number  [hex2dec $wrtlv::versionNumber]
    set message_id      [hex2dec $wrtlv::wrMessageId]


    set hex [string range $hex 24 end]

    switch $wrtlv::wrMessageId {

        "2000" {
            wrptp_wrflags tlv_data = $hex

            set wrconfig    [hex2dec $tlv_data::wrConfig]
            set calibrated  [hex2dec $tlv_data::calibrated]
            set wrmode_on   [hex2dec $tlv_data::wrModeOn]
        }

        "1003" {
            #CALIBRATE
            wrptp_calibrate_data tlv_data = $hex
            set cal_send_pattern [hex2dec $tlv_data::calSendPattern]
            set cal_retry        [hex2dec $tlv_data::calRetry]
            set cal_period       [hex2dec $tlv_data::calPeriod]
        }

        "1004" {
            #CALIBRATED
            wrptp_calibrated_data tlv_data = $hex
            set delta_tx [hex2dec $tlv_data::deltaTx]
            set delta_rx [hex2dec $tlv_data::deltaRx]

            #Unit is 2^16 ps
            set delta_tx [expr (wide($delta_tx)+32768)/65536]
            set delta_rx [expr (wide($delta_rx)+32768)/65536]

        }

        default {
            #SLAVE_PRESENT[1000], LOCK[1001], LOCKED[1002], WR_MODE_ON[1005]
        }
    }

    return 1
}
