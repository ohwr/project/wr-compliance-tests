#ETHERNET
struct ethernet {
    
    dmac    6
    smac    6
    type    2
    
}


#VLAN
struct vlan {
    
    priority    :3
    cfi         :1
    vlan        :12
    typelen     2
}


#IP
struct ip {
    
    version     :4
    hdrlen      :4
    tos         1
    len         2
    id          2
    flags       :3
    offset      :13
    ttl         1
    proto       1
    checksum    2
    srcip       4
    dstip       4
    
}


#ARP
struct arp {
    
    hw_type     2
    proto_type  2
    hw_size     1
    proto_size  1
    opcode      2
    src_mac     6
    src_ip      4
    dest_mac    6
    dest_ip     4
    
}


#UDP
struct udp {
    
    srcport    2
    dstport    2
    length     2
    checksum   2
}


#White Rabbit PTP - flags [2]
struct wrptp_flags {

    reserved2               :1
    profileSpecific2        :1
    profileSpecific1        :1
    reserved1               :2
    unicastFlag             :1
    twoStepFlag             :1
    alternateMasterFlag     :1

    reserved3               :2
    frequencyTraceable      :1
    timeTraceable           :1
    ptpTimescale            :1
    currentUtcOffsetValid   :1
    leap59                  :1
    leap61                  :1
}                            

                             
#White Rabbit PTP Common [34]
struct wrptp_common {

    transportSpecific   :4  
    messageType         :4
    reserved1           :4
    versionPTP          :4
    messageLength       2
    domainNumber        1
    reserved2           1
    flagField           2
    correctionField     8
    reserved3           4
    sourcePortIdentity  10
    sequenceId          2
    controlField        1
    logMessageInterval  1

}


#White Rabbit PTP - Timestamp [10]
struct wrptp_timestamp {

    secondsField        6
    nanosecondsField    4

}


#White Rabbit PTP - Port Identity [10]
struct wrptp_port_identity {

    clockIdentity   8
    portNumber      2

}


#White Rabbit PTP - Clock Quality [4]
struct wrptp_clock_quality {

    clockClass              1
    clockAccuracy           1
    offsetScaledLogVariance 2

}

#White Rabbit PTP - Announce message [30]
struct wrptp_announce {
    
    originTimestamp         10
    currentUtcOffset        2
    reserved                1
    grandmasterPriority1    1
    grandmasterClockQuality 4
    grandmasterPriority2    1
    grandmasterIdentity     8
    stepsRemoved            2
    timeSource              1
    
}


#White Rabbit PTP - Sync message [10] 
struct wrptp_sync {

    originTimestamp         10

}


#White Rabbit PTP - Delay_Req messages [10]
struct wrptp_delay_req {
    
    originTimestamp         10

}


#White Rabbit PTP - Follow_Up message [10]
struct wrptp_followup {

    preciseOriginTimestamp  10

}


#White Rabbit PTP - Delay_Resp message [20]
struct wrptp_delay_resp {

    receiveTimestamp        10
    requestingPortIdentity  10

}


#White Rabbit PTP - Pdelay_Req message [20]
struct wrptp_pdelay_req {
    
    originTimestamp         10
    reserved                10
    
}


#White Rabbit PTP - Pdelay_Resp message [20]
struct wrptp_pdelay_resp {

    requestReceiptTimestamp 10
    requestingPortIdentity  10

}


#White Rabbit PTP - Pdelay_Resp_Follow_Up message [20]
struct wrptp_pdelay_resp_followup {

    responseOriginTimestamp 10
    requestingPortIdentity  10

}


#White Rabbit PTP - Signaling message [+TLVs] [10]
struct wrptp_signal {

    targetPortIdentity      10

}


#White Rabbit PTP - Management message [+managementTLV] [14]
struct wrptp_mgmt {

    targetPortIdentity      10
    startingBoundaryHops    1
    boundaryHops            1
    reserved1               :4
    actionField             :4
    reserved2               1

}

#White Rabbit PTP - TLV [+valueField] [4+N]
struct wrptp_tlv {

    tlvType         2
    lengthField     2
    organizationId  3
    magicNumber     2
    versionNumber   1
    wrMessageId     2

}

#White Rabbit PTP - wrFlags for ANN_SUFFIX
struct wrptp_wrflags {

    reserved1   1
    reserved2   :4
    wrModeOn    :1
    calibrated  :1
    wrConfig    :2

}

#White Rabbit PTP TLV - CALIBRATE
struct wrptp_calibrate_data {

    calSendPattern  1
    calRetry        1
    calPeriod       4

}

#White Rabbit PTP TLV - CALIBRATED
struct wrptp_calibrated_data {

    deltaTx     8
    deltaRx     8

}
