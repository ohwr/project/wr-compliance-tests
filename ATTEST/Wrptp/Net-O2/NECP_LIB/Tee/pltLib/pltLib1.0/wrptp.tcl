################################################################################
# File Name         : wrptp.tcl                                                #
# File Version      : 1.0                                                      #
# Component Name    : ATTEST Platform Library                                  #
# Module Name       : White Rabbit Precision Time Protocol                     #
################################################################################
# History       Date       Author         Addition/ Alteration                 #
#                                                                              #
#  1.0         Jun/2018    CERN           Initial                              #
#                                                                              #
################################################################################
#   Copyright (C) CERN 2018                                                    #
################################################################################

#########################PROCEDURES USED########################################
#                                                                              #
#  1. pltLib::send_wrptp_pkt                                                   #
#  2. pltLib::send_arp                                                         #
#                                                                              #
################################################################################

namespace eval pltLib {}

#1.
#################################################################################
#                                                                               #
#  PROCEDURE NAME         : pltLib::send_wrptp_pkt                              #
#                                                                               #
#  INPUT PARAMETERS       :                                                     #
#                                                                               #
#   session_id            :  (Mandatory) Session identifier                     #
#                                                                               #
#   port_num              :  (Mandatory) Port number on which frame is to send. #
#                                        (e.g., 0/1)                            #
#                                                                               #
#   instance              :  (Mandatory) Instance ID                            #
#                                                                               #
#   dest_mac              :  (Mandatory) Destination MAC dddress.               #
#                                        (e.g., 01:80:C2:00:00:20)              #
#                                                                               #
#   src_mac               :  (Mandatory) Source MAC address.                    #
#                                        (e.g., 00:80:C2:00:00:21)              #
#                                                                               #
#   eth_type              :  (Optional)  EtherType (0800 or 88F7)               #
#                                        (Default : 0800)                       #
#                                                                               #
#   vlan_id               :  (Optional)  Vlan ID                                #
#                                        (Default : 0)                          #
#                                                                               #
#   vlan_priority         :  (Optional)  Vlan Priority                          #
#                                        (Default : 0)                          #
#                                                                               #
#   dest_ip               :  (Optional)  Destination IP address                 #
#                                        (Default : 224.0.1.129)                #
#                                                                               #
#   src_ip                :  (Optional)  Source IP address                      #
#                                        (Default : 0.0.0.0)                    #
#                                                                               #
#   ip_checksum           :  (Optional)  IP checksum                            #
#                                        (Default : 0)                          #
#                                                                               #
#   ip_protocol           :  (Optional)  IP Protocol                            #
#                                        (Default : 17)                         #
#                                                                               #
#  (UDP HEADER)                                                                 #
#                                                                               #
#   src_port             : (Optional) Sender's port number                      #
#                                     ( Default : 319 )                         #
#                                                                               #
#   dest_port            : (Optional) Receiver's port number                    #
#                                     ( Default : 319 )                         #
#                                                                               #
#   udp_length           : (Optional) Length in bytes of UDP header and data    #
#                                     ( Default : 52 )                          #
#                                                                               #
#   udp_checksum         : (Optional) Error-checking of the header              #
#                                     ( Default : auto )                        #
#                                                                               #
#                                                                               #
#  (PTP COMMON HEADER)                                                          #
#                                                                               #
#   transport_specific    :  (Optional)  Transport specific                     #
#                                        (Default : 0)                          #
#                                                                               #
#   message_type          :  (Optional)  Message Type                           #
#                                        (Default : 0)                          #
#                                                                               #
#   version_ptp           :  (Optional)  PTP version number                     #
#                                        (Default : 2)                          #
#                                                                               #
#   message_length        :  (Optional)  PTP Sync message length                #
#                                        (Default : 44)                         #
#                                                                               #
#   domain_number         :  (Optional)  Domain number                          #
#                                        (Default : 0)                          #
#                                                                               #
#   alternate_master_flag :  (Optional)  Alternate Master flag                  #
#                                        (Default : 0)                          #
#                                                                               #
#   two_step_flag         :  (Optional)  Two Step Flag                          #
#                                        (Default : 0)                          #
#                                                                               #
#   unicast_flag          :  (Optional)  Unicast Flag                           #
#                                        (Default : 0)                          #
#                                                                               #
#   profile_specific1     :  (Optional)  Profile specific1 Flag                 #
#                                        (Default : 0)                          #
#                                                                               #
#   profile_specific2     :  (Optional)  Profile specific2 Flag                 #
#                                        (Default : 0)                          #
#                                                                               #
#   leap61                :  (Optional)  Leap61 Flag                            #
#                                        (Default : 0)                          #
#                                                                               #
#   leap59                :  (Optional)  Leap59 Flag                            #
#                                        (Default : 0)                          #
#                                                                               #
#   current_utc_offset_valid:(Optional)  Current UTC Offset Flag                #
#                                        (Default : 0)                          #
#                                                                               #
#   ptp_timescale         :  (Optional)  PTP Timescale Flag                     #
#                                        (Default : 0)                          #
#                                                                               #
#   time_traceable        :  (Optional)  Time Traceable Flag                    #
#                                        (Default : 0)                          #
#                                                                               #
#   freq_traceable        :  (Optional)  Frequency Traceable Flag               #
#                                        (Default : 0)                          #
#                                                                               #
#   correction_field      :  (Optional)  Correction Field value                 #
#                                        (Default : 0)                          #
#                                                                               #
#   src_port_number       :  (Optional)  Source port number                     #
#                                        (Default : 0)                          #
#                                                                               #
#   src_clock_identity    :  (Optional)  Source Clock identity                  #
#                                        (Default : 0)                          #
#                                                                               #
#   sequence_id           :  (Optional)  Sequence ID                            #
#                                        (Default : 0)                          #
#                                                                               #
#   control_field         :  (Optional)  Control field                          #
#                                        (Default : 0)                          #
#                                                                               #
#   log_message_interval  :  (Optional)  Log message interval                   #
#                                        (Default : 127)                        #
#                                                                               #
#                                                                               #
#  (PTP SYNC/DELAY REQUEST HEADER)                                              #
#                                                                               #
#   origin_timestamp_sec       :  (Optional)  Origin timestamp in seconds       #
#                                             (Default : 0)                     #
#                                                                               #
#   origin_timestamp_ns        :  (Optional)  Origin timestamp in nanoseconds   #
#                                             (Default : 0)                     #
#                                                                               #
#                                                                               #
#  (PTP ANNOUNCE HEADER)                                                        #
#                                                                               #
#   current_utc_offset         :  (Optional)  Current UTC Offset                #
#                                             (Default : 0)                     #
#                                                                               #
#   gm_priority1               :  (Optional)  Grand Master Priority 1           #
#                                             (Default : 0)                     #
#                                                                               #
#   gm_priority2               :  (Optional)  Grand Master Priority 2           #
#                                             (Default : 128)                   #
#                                                                               #
#   gm_identity                :  (Optional)  Grand Master Identity             #
#                                             (Default : 0)                     #
#                                                                               #
#   steps_removed              :  (Optional)  Steps Removed                     #
#                                             (Default : 0)                     #
#                                                                               #
#   time_source                :  (Optional)  Time Source                       #
#                                             (Default : 0)                     #
#                                                                               #
#   gm_clock_class             :  (Optional)  Grand Master Clock class          #
#                                             (Default : 0)                     #
#                                                                               #
#   gm_clock_accuracy          :  (Optional)  Grand Master Clock accuracy       #
#                                             (Default : 0)                     #
#                                                                               #
#   gm_clock_variance          :  (Optional)  Grand Master Clock offset scaled  #
#                                         log variance (Default : 0)            #
#                                                                               #
#                                                                               #
#  (PTP OTHER HEADERS)                                                          #
#                                                                               #
#   precise_origin_timestamp_sec  :  (Optional)  Precise Origin Timestamp       #
#                                      (Default : 0)                            #
#                                                                               #
#   precise_origin_timestamp_ns   :  (Optional)  Precise Origin Timestamp       #
#                                      (Default : 0)                            #
#                                                                               #
#   receive_timestamp_sec         :  (Optional)  Receive Timestamp              #
#                                      (Default : 0)                            #
#                                                                               #
#   receive_timestamp_ns          :  (Optional)  Receive Timestamp              #
#                                      (Default : 0)                            #
#                                                                               #
#   requesting_port_number        :  (Optional)  Requesting Port Number         #
#                                        (Default : 0)                          #
#                                                                               #
#   requesting_clock_identity     :  (Optional)  Requesting Clock Identity      #
#                                        (Default : 0)                          #
#                                                                               #
#   request_receipt_timestamp_sec :  (Optional)  Request Receipt Timestamp      #
#                                        (Default : 0)                          #
#                                                                               #
#   request_receipt_timestamp_ns  :  (Optional)  Request Receipt Timestamp      #
#                                        (Default : 0)                          #
#                                                                               #
#   response_origin_timestamp_sec :  (Optional)  Response Origin Timestamp      #
#                                        (Default : 0)                          #
#                                                                               #
#   response_origin_timestamp_ns  :  (Optional)  Response Origin Timestamp      #
#                                       (Default : 0)                           #
#                                                                               #
#   target_port_number            :  (Optional)  Target Port number             #
#                                        (Default : 0)                          #
#                                                                               #
#   target_clock_identity         :  (Optional)  Target clock identity          #
#                                        (Default : 0)                          #
#                                                                               #
#   starting_boundary_hops        :  (Optional)  Starting Boundary Hops         #
#                                        (Default : 0)                          #
#                                                                               #
#   boundary_hops                 :  (Optional)  Boundary Hops                  #
#                                        (Default : 0)                          #
#                                                                               #
#   action_field                  :  (Optional)  Action Field                   #
#                                        (Default : 0)                          #
#                                                                               #
#                                                                               #
#  (PTP TLVs)                                                                   #
#                                                                               #
#   tlv_type                      :  (Optional)  TLV Type                       #
#                                        (Default : -1)                         #
#                                                                               #
#   tlv_length                    :  (Optional)  TLV length                     #
#                                        (Default : 8)                          #
#                                                                               #
#   organization_id               :  (Optional) Organization id                 #
#                                        (Default: 0x080030)                    #
#                                                                               #
#   magic_number                  :  (Optional) Magic number                    #
#                                        (Default: 0xDEAD)                      #
#                                                                               #
#   version_number                :  (Optional) Version number                  #
#                                        (Default: 1)                           #
#                                                                               #
#   message_id                    :  (Optional) Message id                      #
#                                        (Default: 0x2000)                      #
#                                                                               #
#   wrconfig                      :  (Optional) Wrconfig                        #
#                                        (Default: 0)                           #
#                                                                               #
#   calibrated                    :  (Optional) Calibrated                      #
#                                        (Default: 0)                           #
#                                                                               #
#   wrmode_on                     :  (Optional) WR Mode ON                      #
#                                        (Default: 0)                           #
#                                                                               #
#   cal_send_pattern              :  (Optional) Cal send pattern                #
#                                        (Default: 0)                           #
#                                                                               #
#   cal_retry                     :  (Optional) Cal retry                       #
#                                        (Default: 0)                           #
#                                                                               #
#   cal_period                    :  (Optional) Cal period                      #
#                                        (Default: 0)                           #
#                                                                               #
#   delta_tx                      :  (Optional) Delta tx                        #
#                                        (Default: 0)                           #
#                                                                               #
#   delta_rx                      :  (Optional) Delta rx                        #
#                                        (Default: 0)                           #
#                                                                               #
#   count                 :  (Optional)  Frame count                            #
#                                        (Default : 1)                          #
#                                                                               #
#   interval              :  (Optional)  Frame interval (in seconds)            #
#                                        (Default : 1)                          #
#                                                                               #
#  OUTPUT PARAMETERS      :  NONE                                               #
#                                                                               #
#  RETURNS                :  1 on success (If PTP Sync message is sent with the #
#                                          given parameters)                    #
#                                                                               #
#                                                                               #
#                         :  0 on success (If PTP Sync message could not be     #
#                                          sent with the given parameters)      #
#                                                                               #
#  DEFINITION             : This function is used to construct and send PTP     #
#                           sync message.                                       #
#                                                                               #
#  USAGE                  :                                                     #
#                                                                               #
#       pltLib::send_wrptp_pkt\                                                 #
#              -session_id                     $session_id\                     #
#              -port_num                       $port_num\                       #
#              -dest_mac                       $dest_mac\                       #
#              -src_mac                        $src_mac\                        #
#              -eth_type                       $eth_type\                       #
#              -vlan_id                        $vlan_id\                        #
#              -vlan_priority                  $vlan_priority\                  #
#              -ip_checksum                    $ip_checksum\                    #
#              -ip_protocol                    $ip_protocol\                    #
#              -src_ip                         $src_ip\                         #
#              -dest_ip                        $dest_ip\                        #
#              -src_port                       $src_port\                       #
#              -dest_port                      $dest_port\                      #
#              -udp_length                     $udp_length\                     #
#              -udp_checksum                   $udp_checksum\                   #
#              -transport_specific             $transport_specific\             #
#              -message_type                   $message_type\                   #
#              -version_ptp                    $version_ptp\                    #
#              -message_length                 $message_length\                 #
#              -domain_number                  $domain_number\                  #
#              -alternate_master_flag          $alternate_master_flag\          #
#              -two_step_flag                  $two_step_flag\                  #
#              -unicast_flag                   $unicast_flag\                   #
#              -profile_specific1              $profile_specific1\              #
#              -profile_specific2              $profile_specific2\              #
#              -leap61                         $leap61\                         #
#              -leap59                         $leap59\                         #
#              -current_utc_offset_valid       $current_utc_offset_valid\       #
#              -ptp_timescale                  $ptp_timescale\                  #
#              -time_traceable                 $time_traceable\                 #
#              -freq_traceable                 $freq_traceable\                 #
#              -correction_field               $correction_field\               #
#              -src_port_number                $src_port_number\                #
#              -src_clock_identity             $src_clock_identity\             #
#              -sequence_id                    $sequence_id\                    #
#              -control_field                  $control_field\                  #
#              -log_message_interval           $log_message_interval\           #
#              -origin_timestamp_sec           $origin_timestamp_sec\           #
#              -origin_timestamp_ns            $origin_timestamp_ns\            #
#              -current_utc_offset             $current_utc_offset\             #
#              -gm_priority1                   $gm_priority1\                   #
#              -gm_priority2                   $gm_priority2\                   #
#              -gm_identity                    $gm_identity\                    #
#              -steps_removed                  $steps_removed\                  #
#              -time_source                    $time_source\                    #
#              -gm_clock_class                 $gm_clock_class\                 #
#              -gm_clock_accuracy              $gm_clock_accuracy\              #
#              -gm_clock_variance              $gm_clock_variance\              #
#              -precise_origin_timestamp_sec   $precise_origin_timestamp_sec\   #
#              -precise_origin_timestamp_ns    $precise_origin_timestamp_ns\    #
#              -receive_timestamp_sec          $receive_timestamp_sec\          #
#              -receive_timestamp_ns           $receive_timestamp_ns\           #
#              -requesting_port_number         $requesting_port_number\         #
#              -requesting_clock_identity      $requesting_clock_identity\      #
#              -request_receipt_timestamp_sec  $request_receipt_timestamp_sec\  #
#              -request_receipt_timestamp_ns   $request_receipt_timestamp_ns\   #
#              -response_origin_timestamp_sec  $response_origin_timestamp_sec\  #
#              -response_origin_timestamp_ns   $response_origin_timestamp_ns\   #
#              -target_port_number             $target_port_number\             #
#              -target_clock_identity          $target_clock_identity\          #
#              -starting_boundary_hops         $starting_boundary_hops\         #
#              -boundary_hops                  $boundary_hops\                  #
#              -action_field                   $action_field\                   #
#              -tlv_type                       $tlv_type\                       #
#              -tlv_length                     $tlv_length\                     #
#              -organization_id                $organization_id\                #
#              -magic_number                   $magic_number\                   #
#              -version_number                 $version_number\                 #
#              -message_id                     $message_id\                     #
#              -wrconfig                       $wrconfig\                       #
#              -calibrated                     $calibrated\                     #
#              -wrmode_on                      $wrmode_on\                      #
#              -cal_send_pattern               $cal_send_pattern\               #
#              -cal_retry                      $cal_retry\                      #
#              -cal_period                     $cal_period\                     #
#              -delta_tx                       $delta_tx\                       #
#              -delta_rx                       $delta_rx                        #
#              -count                          $count\                          #
#              -interval                       $interval                        #
#                                                                               #
#################################################################################

proc pltLib::send_wrptp_pkt {args} {

    array set param $args

    #INPUT PARAMETERS

    #Session identifier
    if {[info exists param(-session_id)]} {

        set session_id $param(-session_id)
    } else {

        ERRLOG -msg "pltLib::send_wrptp_pkt: Missing Argument -> session_id"
        return 0
    }

    #Send Instance
    if {[info exists param(-instance)]} {

        set instance $param(-instance)
    } else {

        ERRLOG -msg "pltLib::send_wrptp_pkt: Missing Argument -> instance"
        return 0
    }

    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "pltLib::send_wrptp_pkt: Missing Argument -> port_num"
        return 0
    }

    #ETHERNET HEADER OPTIONS

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
    } else {

        ERRLOG -msg "pltLib::send_wrptp_pkt: Missing Argument -> dest_mac"
        return 0
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
    } else {

        ERRLOG -msg "pltLib::send_wrptp_pkt: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
    } else {

        set eth_type 0800
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
    } else {

        set vlan_id 0
    }

    #Vlan Priority
    if {[info exists param(-vlan_priority)]} {

        set vlan_priority $param(-vlan_priority)
    } else {

        set vlan_priority 0
    }

    #IP HEADER OPTIONS

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
    } else {

        set dest_ip 224.0.1.129
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
    } else {

        set src_ip 0.0.0.0
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
    } else {

        set ip_checksum auto
    }

    #IP protocol
    if {[info exists param(-ip_protocol)]} {

        set ip_protocol $param(-ip_protocol)
    } else {

        set ip_protocol 17
    }

    #UDP HEADER OPTIONS

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
    } else {

        set dest_port 319
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
    } else {

        set src_port 319
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
    } else {

        set udp_length 0
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
    } else {

        set udp_checksum 0
    }

    #PTP HEADER OPTIONS

    #Transport specific
    if {[info exists param(-transport_specific)]} {

        set transport_specific $param(-transport_specific)
    } else {

        set transport_specific 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
    } else {

        set message_type 0
    }

    #PTP version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
    } else {

        set message_length 44
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
    } else {

        set domain_number 0
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
    } else {

        set unicast_flag 0
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
    } else {

        set profile_specific2 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
    } else {

        set current_utc_offset_valid 0
    }

    #PTP Timescale Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
    } else {

        set freq_traceable 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
    } else {

        set correction_field 0
    }

    #Source port number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
    } else {

        set src_port_number 0
    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
    } else {

        set src_clock_identity 0
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
    } else {

        set sequence_id 0
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
    } else {

        set control_field 0
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
    } else {

        set log_message_interval 127
    }

    #Origin timestamp in seconds
    if {[info exists param(-origin_timestamp_sec)]} {

        set origin_timestamp_sec $param(-origin_timestamp_sec)
    } else {

        set origin_timestamp_sec 0
    }

    #Origin timestamp in nanoseconds
    if {[info exists param(-origin_timestamp_ns)]} {

        set origin_timestamp_ns $param(-origin_timestamp_ns)
    } else {

        set origin_timestamp_ns 0
    }

    #Current UTC Offset
    if {[info exists param(-current_utc_offset)]} {

        set current_utc_offset $param(-current_utc_offset)
    } else {

        set current_utc_offset 0
    }

    #Grand Master Priority 1
    if {[info exists param(-gm_priority1)]} {

        set gm_priority1 $param(-gm_priority1)
    } else {

        set gm_priority1 0
    }

    #Grand Master Priority 2
    if {[info exists param(-gm_priority2)]} {

        set gm_priority2 $param(-gm_priority2)
    } else {

        set gm_priority2 128
    }

    #Grand Master Identity
    if {[info exists param(-gm_identity)]} {

        set gm_identity $param(-gm_identity)
    } else {

        set gm_identity 0
    }

    #Steps Removed
    if {[info exists param(-steps_removed)]} {

        set steps_removed $param(-steps_removed)
    } else {

        set steps_removed 0
    }

    #Time Source
    if {[info exists param(-time_source)]} {

        set time_source $param(-time_source)
    } else {

        set time_source 0
    }

    #Grand Master Clock class
    if {[info exists param(-gm_clock_class)]} {

        set gm_clock_class $param(-gm_clock_class)
    } else {

        set gm_clock_class 0
    }

    #Grand Master Clock accuracy
    if {[info exists param(-gm_clock_accuracy)]} {

        set gm_clock_accuracy $param(-gm_clock_accuracy)
    } else {

        set gm_clock_accuracy 0
    }

    #Grand Master Clock offset scaled
    if {[info exists param(-gm_clock_variance)]} {

        set gm_clock_variance $param(-gm_clock_variance)
    } else {

        set gm_clock_variance 0
    }

    #Precise Origin Timestamp
    if {[info exists param(-precise_origin_timestamp_sec)]} {

        set precise_origin_timestamp_sec $param(-precise_origin_timestamp_sec)
    } else {

        set precise_origin_timestamp_sec 0
    }

    #Precise Origin Timestamp
    if {[info exists param(-precise_origin_timestamp_ns)]} {

        set precise_origin_timestamp_ns $param(-precise_origin_timestamp_ns)
    } else {

        set precise_origin_timestamp_ns 0
    }

    #Receive Timestamp
    if {[info exists param(-receive_timestamp_sec)]} {

        set receive_timestamp_sec $param(-receive_timestamp_sec)
    } else {

        set receive_timestamp_sec 0
    }

    #Receive Timestamp
    if {[info exists param(-receive_timestamp_ns)]} {

        set receive_timestamp_ns $param(-receive_timestamp_ns)
    } else {

        set receive_timestamp_ns 0
    }

    #Requesting Port Number
    if {[info exists param(-requesting_port_number)]} {

        set requesting_port_number $param(-requesting_port_number)
    } else {

        set requesting_port_number 0
    }

    #Requesting Clock Identity
    if {[info exists param(-requesting_clock_identity)]} {

        set requesting_clock_identity $param(-requesting_clock_identity)
    } else {

        set requesting_clock_identity 0
    }

    #Request Receipt Timestamp
    if {[info exists param(-request_receipt_timestamp_sec)]} {

        set request_receipt_timestamp_sec $param(-request_receipt_timestamp_sec)
    } else {

        set request_receipt_timestamp_sec 0
    }

    #Request Receipt Timestamp
    if {[info exists param(-request_receipt_timestamp_ns)]} {

        set request_receipt_timestamp_ns $param(-request_receipt_timestamp_ns)
    } else {

        set request_receipt_timestamp_ns 0
    }

    #Response Origin Timestamp
    if {[info exists param(-response_origin_timestamp_sec)]} {

        set response_origin_timestamp_sec $param(-response_origin_timestamp_sec)
    } else {

        set response_origin_timestamp_sec 0
    }

    #Response Origin Timestamp
    if {[info exists param(-response_origin_timestamp_ns)]} {

        set response_origin_timestamp_ns $param(-response_origin_timestamp_ns)
    } else {

        set response_origin_timestamp_ns 0
    }

    #Target Port number
    if {[info exists param(-target_port_number)]} {

        set target_port_number $param(-target_port_number)
    } else {

        set target_port_number 0
    }

    #Target clock identity
    if {[info exists param(-target_clock_identity)]} {

        set target_clock_identity $param(-target_clock_identity)
    } else {

        set target_clock_identity 0
    }

    #Starting Boundary Hops
    if {[info exists param(-starting_boundary_hops)]} {

        set starting_boundary_hops $param(-starting_boundary_hops)
    } else {

        set starting_boundary_hops 0
    }

    #Boundary Hops
    if {[info exists param(-boundary_hops)]} {

        set boundary_hops $param(-boundary_hops)
    } else {

        set boundary_hops 0
    }

    #Action Field
    if {[info exists param(-action_field)]} {

        set action_field $param(-action_field)
    } else {

        set action_field 0
    }

    #TLV Type
    if {[info exists param(-tlv_type)]} {

        set tlv_type $param(-tlv_type)
    } else {

        set tlv_type -1
    }

    #TLV length
    if {[info exists param(-tlv_length)]} {

        set tlv_length $param(-tlv_length)
    } else {

        set tlv_length 8
    }

    #Organisation ID
    if {[info exists param(-organization_id)]} {

        set organization_id $param(-organization_id)
    } else {

        set organization_id [expr 0x080030]
    }

    #Magic Number
    if {[info exists param(-magic_number)]} {

        set magic_number $param(-magic_number)
    } else {

        set magic_number [expr 0xDEAD]
    }

    #Version Number
    if {[info exists param(-version_number)]} {

        set version_number $param(-version_number)
    } else {

        set version_number 1
    }

    #Message ID
    if {[info exists param(-message_id)]} {

        set message_id $param(-message_id)
    } else {

        set message_id [expr 0x2000]
    }

    if {[info exists param(-wrconfig)]} {

        set wrconfig $param(-wrconfig)
    } else {

        set wrconfig 0
    }

    if {[info exists param(-calibrated)]} {

        set calibrated $param(-calibrated)
    } else {

        set calibrated 0
    }

    if {[info exists param(-wrmode_on)]} {

        set wrmode_on $param(-wrmode_on)
    } else {

        set wrmode_on 0
    }

    if {[info exists param(-cal_send_pattern)]} {

        set cal_send_pattern $param(-cal_send_pattern)
    } else {

        set cal_send_pattern 0
    }

    if {[info exists param(-cal_retry)]} {

        set cal_retry $param(-cal_retry)
    } else {

        set cal_retry 0
    }

    if {[info exists param(-cal_period)]} {

        set cal_period $param(-cal_period)
    } else {

        set cal_period 0
    }

    if {[info exists param(-delta_tx)]} {

        set delta_tx $param(-delta_tx)
    } else {

        set delta_tx 0
    }

    if {[info exists param(-delta_rx)]} {

        set delta_rx $param(-delta_rx)
    } else {

        set delta_rx 0
    }

    #MISCELLANEOUS
    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
    } else {

        set interval 1
    }

    #Interval in millisec
    set interval [expr int($interval*1000)]

    #CONSTRUCT PTP HA SYNC PACKET DUMP
    if {![pbLib::encode_wrptp_pkt\
                -port_num                       $port_num\
                -dest_mac                       $dest_mac\
                -src_mac                        $src_mac\
                -eth_type                       $eth_type\
                -vlan_id                        $vlan_id\
                -vlan_priority                  $vlan_priority\
                -ip_checksum                    $ip_checksum\
                -ip_protocol                    $ip_protocol\
                -src_ip                         $src_ip\
                -dest_ip                        $dest_ip\
                -src_port                       $src_port\
                -dest_port                      $dest_port\
                -udp_length                     $udp_length\
                -udp_checksum                   $udp_checksum\
                -transport_specific             $transport_specific\
                -message_type                   $message_type\
                -version_ptp                    $version_ptp\
                -message_length                 $message_length\
                -domain_number                  $domain_number\
                -alternate_master_flag          $alternate_master_flag\
                -two_step_flag                  $two_step_flag\
                -unicast_flag                   $unicast_flag\
                -profile_specific1              $profile_specific1\
                -profile_specific2              $profile_specific2\
                -leap61                         $leap61\
                -leap59                         $leap59\
                -current_utc_offset_valid       $current_utc_offset_valid\
                -ptp_timescale                  $ptp_timescale\
                -time_traceable                 $time_traceable\
                -freq_traceable                 $freq_traceable\
                -correction_field               $correction_field\
                -src_port_number                $src_port_number\
                -src_clock_identity             $src_clock_identity\
                -sequence_id                    $sequence_id\
                -control_field                  $control_field\
                -log_message_interval           $log_message_interval\
                -origin_timestamp_sec           $origin_timestamp_sec\
                -origin_timestamp_ns            $origin_timestamp_ns\
                -current_utc_offset             $current_utc_offset\
                -gm_priority1                   $gm_priority1\
                -gm_priority2                   $gm_priority2\
                -gm_identity                    $gm_identity\
                -steps_removed                  $steps_removed\
                -time_source                    $time_source\
                -gm_clock_class                 $gm_clock_class\
                -gm_clock_accuracy              $gm_clock_accuracy\
                -gm_clock_variance              $gm_clock_variance\
                -precise_origin_timestamp_sec   $precise_origin_timestamp_sec\
                -precise_origin_timestamp_ns    $precise_origin_timestamp_ns\
                -receive_timestamp_sec          $receive_timestamp_sec\
                -receive_timestamp_ns           $receive_timestamp_ns\
                -requesting_port_number         $requesting_port_number\
                -requesting_clock_identity      $requesting_clock_identity\
                -request_receipt_timestamp_sec  $request_receipt_timestamp_sec\
                -request_receipt_timestamp_ns   $request_receipt_timestamp_ns\
                -response_origin_timestamp_sec  $response_origin_timestamp_sec\
                -response_origin_timestamp_ns   $response_origin_timestamp_ns\
                -target_port_number             $target_port_number\
                -target_clock_identity          $target_clock_identity\
                -starting_boundary_hops         $starting_boundary_hops\
                -boundary_hops                  $boundary_hops\
                -action_field                   $action_field\
                -tlv_type                       $tlv_type\
                -tlv_length                     $tlv_length\
                -organization_id                $organization_id\
                -magic_number                   $magic_number\
                -version_number                 $version_number\
                -message_id                     $message_id\
                -wrconfig                       $wrconfig\
                -calibrated                     $calibrated\
                -wrmode_on                      $wrmode_on\
                -cal_send_pattern               $cal_send_pattern\
                -cal_retry                      $cal_retry\
                -cal_period                     $cal_period\
                -delta_tx                       $delta_tx\
                -delta_rx                       $delta_rx\
                -hexdump                        hexdump]} {

        return 0
    }

    pltLib::send_periodic\
                     -session_id     $session_id\
                     -port_num       $port_num\
                     -data           $hexdump\
                     -instance       $instance\
                     -interval       $interval\
                     -count          $count\
                     -handler_id     "WRPTP"

    return 1

}

#################################################################################
#                                                                               #
#  PROCEDURE NAME         : pltLib::send_arp                                    #
#                                                                               #
#  INPUT PARAMETERS       :                                                     #
#                                                                               #
#   session_id            : (Mandatory) Session identifier                      #
#                                                                               #
#   port_num              : (Mandatory) Port number on which frame is to send.  #
#                                       (e.g., 0/1)                             #
#                                                                               #
#   dest_mac              : (Mandatory) Destination MAC dddress.                #
#                                       (e.g., FF:FF:FF:FF:FF:FF)               #
#                                                                               #
#   src_mac               : (Mandatory) Source MAC address.                     #
#                                       (e.g., 00:80:C2:00:00:21)               #
#                                                                               #
#   eth_type              : (Optional)  EtherType                               #
#                                       (Default : 0806)                        #
#                                                                               #
#   vlan_id               : (Optional)  Vlan ID                                 #
#                                       (Default : 0)                           #
#                                                                               #
#   vlan_priority         : (Optional)  Vlan Priority                           #
#                                       (Default : 0)                           #
#                                                                               #
#   hardware_type         : (Optional) Specifies the network protocol type      #
#                                      (Default : 1)                            #
#                                                                               #
#   protocol_type         : (Optional) Specifies the internetwork protocol for  #
#                                      which the ARP request is intended.       #
#                                      (Default : 0800)                         #
#                                                                               #
#   hardware_size         : (Optional) Length of a hardware address             #
#                                      (Default : 6)                            #
#                                                                               #
#   protocol_size         : (Optional) Length addresses used in the upper layer #
#                                      protocol. (Default : 4)                  #
#                                                                               #
#   message_type          : (Optional) Specifies the operation that the sender  #
#                                      is performing: 1 - request, 2 - reply.   #
#                                      (Default : 1)                            #
#                                                                               #
#   sender_mac            : (Optional) Sender hardware address                  #
#                                      (Default : 00:00:00:00:00:00)            #
#                                                                               #
#   sender_ip             : (Optional) Sender protocol address                  #
#                                      (Default : 0.0.0.0)                      #
#                                                                               #
#   target_mac            : (Optional) Target hardware address                  #
#                                      (Default : 00:00:00:00:00:00)            #
#                                                                               #
#   target_ip             : (Optional) Target protocol address                  #
#                                      (Default : 0.0.0.0)                      #
#                                                                               #
#   count                 : (Optional) Frame count                              #
#                                      (Default : 1)                            #
#                                                                               #
#   interval              : (Optional) Frame interval (in seconds)              #
#                                      (Default : 1)                            #
#                                                                               #
#  OUTPUT PARAMETERS      :  NONE                                               #
#                                                                               #
#  RETURNS                :  1 on success (If ARP packet is sent with the       #
#                                          given parameters)                    #
#                                                                               #
#                                                                               #
#                         :  0 on success (If ARP packet could not be           #
#                                          sent with the given parameters)      #
#                                                                               #
#  DEFINITION             : This function is used to construct and send ARP     #
#                           packet.                                             #
#                                                                               #
#  USAGE                  :                                                     #
#                                                                               #
#       pltLib::send_arp\                                                       #
#                   -session_id                        $session_id\             #
#                   -port_num                          $port_num\               #
#                   -dest_mac                          $dest_mac\               #
#                   -src_mac                           $src_mac\                #
#                   -eth_type                          $eth_type\               #
#                   -vlan_id                           $vlan_id\                #
#                   -vlan_priority                     $vlan_priority\          #
#                   -hardware_type                     $hardware_type\          #
#                   -protocol_type                     $protocol_type\          #
#                   -hardware_size                     $hardware_size\          #
#                   -protocol_size                     $protocol_size\          #
#                   -message_type                      $message_type\           #
#                   -sender_mac                        $sender_mac\             #
#                   -sender_ip                         $sender_ip\              #
#                   -target_mac                        $target_mac\             #
#                   -target_ip                         $target_ip\              #
#                   -count                             $count\                  #
#                   -interval                          $interval                #
#                                                                               #
#################################################################################

proc pltLib::send_arp {args} {

    array set param $args

    #INPUT PARAMETERS

    #Session identifier
    if {[info exists param(-session_id)]} {

        set session_id $param(-session_id)
    } else {

        ERRLOG -msg "pltLib::send_arp: Missing Argument -> session_id"
        return 0
    }

    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "pltLib::send_arp: Missing Argument -> port_num"
        return 0
    }

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
    } else {

        ERRLOG -msg "pltLib::send_arp: Missing Argument -> dest_mac"
        return 0
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
    } else {

        ERRLOG -msg "pltLib::send_arp: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
    } else {

        set eth_type 0806
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
    } else {

        set vlan_id 0
    }

    #Vlan Priority
    if {[info exists param(-vlan_priority)]} {

        set vlan_priority $param(-vlan_priority)
    } else {

        set vlan_priority 0
    }

    #Specifies the network protocol type
    if {[info exists param(-hardware_type)]} {

        set hardware_type $param(-hardware_type)
    } else {

        set hardware_type 1
    }

    #Specifies the internetwork protocol for
    if {[info exists param(-protocol_type)]} {

        set protocol_type $param(-protocol_type)
    } else {

        set protocol_type 0800
    }

    #Length of a hardware address
    if {[info exists param(-hardware_size)]} {

        set hardware_size $param(-hardware_size)
    } else {

        set hardware_size 6
    }

    #Length addresses used in the upper layer
    if {[info exists param(-protocol_size)]} {

        set protocol_size $param(-protocol_size)
    } else {

        set protocol_size 4
    }

    #Specifies the operation that the sender
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
    } else {

        set message_type 1
    }

    #Sender hardware address
    if {[info exists param(-sender_mac)]} {

        set sender_mac $param(-sender_mac)
    } else {

        set sender_mac 00:00:00:00:00:00
    }

    #Sender protocol address
    if {[info exists param(-sender_ip)]} {

        set sender_ip $param(-sender_ip)
    } else {

        set sender_ip 0.0.0.0
    }

    #Target hardware address
    if {[info exists param(-target_mac)]} {

        set target_mac $param(-target_mac)
    } else {

        set target_mac 00:00:00:00:00:00
    }

    #Target protocol address
    if {[info exists param(-target_ip)]} {

        set target_ip $param(-target_ip)
    } else {

        set target_ip 0.0.0.0
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
    } else {

        set interval 1
    }

    #Interval in millisec
    set interval [expr int($interval*1000)]

    #Construct ARP Packet
    if {![pbLib::encode_arp_pkt\
                     -dest_mac        $dest_mac\
                     -src_mac         $src_mac\
                     -eth_type        $eth_type\
                     -hardware_type   $hardware_type\
                     -protocol_type   $protocol_type\
                     -hardware_size   $hardware_size\
                     -protocol_size   $protocol_size\
                     -message_type    $message_type\
                     -sender_mac      $sender_mac\
                     -sender_ip       $sender_ip\
                     -target_mac      $target_mac\
                     -target_ip       $target_ip\
                     -hexdump         packet]} {

        return 0
    }

    #Add VLAN Tag
    if {[string is integer $vlan_id] && ($vlan_id >= 0) && ($vlan_id <= 4095) } {
        set packet [pbLib::push_vlan_tag\
                        -packet          $packet\
                        -vlan_id         $vlan_id\
                        -vlan_priority   $vlan_priority]
    }

    pltLib::send_periodic\
                     -session_id     $session_id\
                     -port_num       $port_num\
                     -data           $packet\
                     -instance       $::INSTANCE(ARP)\
                     -interval       $interval\
                     -count          $count\
                     -handler_id     "DEFAULT"

    return 1

}

#################################################################################
#  PROCEDURE NAME    : pltLib::create_filter_wrptp_pkt                          #
#                                                                               #
#  DEFINITION        : This function is used to create filter to receive WRPTP  #
#                      packet                          .                        #
#                                                                               #
#  INPUT PARAMETERS  :                                                          #
#                                                                               #
#  session_id        : (Optional)  session id to handle session with the Tee    #
#                                                                               #
#  port_num          : (Mandatory) Port Number in which filter is created       #
#                                                                               #
#  vlan_id           : (Mandatory)  VLAN ID                                     #
#                                                                               #
#  src   _mac        : (Mandatory)  source MAC                                  #
#                                                                               #
#  src_port          : (Mandatory)  UDP source port                             #
#                                                                               #
#  USAGE             :                                                          #
#                                                                               #
#                       pltLib::create_filter_wrptp_pkt\                        #
#                           -session_id              $session_id\               #
#                           -port_num                $port_num\                 #
#                           -source_mac              $source_mac\               #
#                           -ethertype               $ether_type                #
#                           -vlan_id                 $vlan_id\                  #
#                           -ids                     ids                        #
#                                                                               #
#################################################################################

proc pltLib::create_filter_wrptp_pkt {args} {
    array set param $args
    set message_type_flag 0

    if {[info exists param(-session_id)]} {
        set session_id $param(-session_id)
    } else {
        set session_id $::session_id
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "pltLib::create_filter_data_pkt : Missing Argument -> Port Number\n"
        return 0
    }

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        set vlan_id 4096
    }

    if {[info exists param(-source_mac)]} {
        set source_mac $param(-source_mac)
    } else {
        set source_mac "00:00:00:00:00:00"
    }

    if {[info exists param(-ether_type)]} {
        set ether_type $param(-ether_type)
    } else {
        ERRLOG -msg "pltLib::create_filter_data_pkt : Missing Argument ->Ether Type \n"
        return 0
    }

    if {[info exists param(-message_type)]} {
        set message_type $param(-message_type)
        set message_type_flag 1
    } else {
        set message_type 0
    }

    set message_type        [format %.2x $message_type]
    set message_type_len    [llength $message_type]
    set message_type1        ""
    for {set i $message_type_len} {$i < 4} {incr i} {
        append message_type1 0
    }
    append message_type $message_type1

    if {[info exists param(-ids)]} {
        upvar $param(-ids) ids
    } else {
        ERRLOG -msg "pltLib::create_filter_data_pkt : Missing Argument -> ids\n"
        return 0
    }

    set vlan_id        [format %x $vlan_id]
    set vlanidlen      [llength [split $vlan_id ""]]
    set vlanid1        ""
    for {set i $vlanidlen} {$i < 4} {incr i} {
        append vlanid1 0
    }
    append vlanid1 $vlan_id
    ###         set dest_port [format "%0.4x" $dest_port]
    set source_mac       [join [split $source_mac ":"] ""]
    set filter_mode      on
    set pkt_to_keep      FILTER
    set bytes_to_keep    0
    set capture_flag     on
    set ether_type       $ether_type


    if {$ether_type == "88F7"} {
        if {$::wrptp_vlan_encap == "false" || $vlan_id == 0} {
            if {$::ptp_trans_type == "IEEE 802.3"} {
                set mask                " FFFF0000 FFFF0000"
                set position            " 12 12"
                set value               " $ether_type 8100"
                set match_term_id       " 1 2"
                set filter_id           1
                set filter_condition    " 1 | 2"
            } else {
                if { $::ptp_comm_model == "Multicast" } {

                    set mask                " FFFF0000 "
                    set position            " 12 "
                    set value               " 0800 "
                    set match_term_id       " 1 "
                    set filter_id           1
                    set filter_condition    "1"

                } else {
                    set mask                " FFFF0000 FFFF0000"
                    set position            " 12  12"
                    set value               " 0800 0806"
                    set match_term_id       " 1 2"
                    set filter_id           1
                    set filter_condition    " 1 | 2"
                }
            }
        } else {
            if {$::ptp_trans_type == "IEEE 802.3"} {
                set mask                " FFFF0000 0FFF0000 FFFF0000"
                set position            " 12 14 16"
                set value               " 8100 $vlanid1 88F7"
                set match_term_id       " 1 2 3"
                set filter_id           1
                set filter_condition    "[join [string trim $match_term_id] " & "]"
            } else {
                if { $::ptp_comm_model == "Multicast" } {
                    set mask                " FFFF0000 0FFF0000 FFFF0000 "
                    set position            " 12 14 16 "
                    set value               " 8100 $vlanid1 0800"
                    set match_term_id       " 1 2 3 "
                    set filter_id           1
                    set filter_condition    "1 & 2 & 3"
                } else {
                    set mask                " FFFF0000 0FFF0000 FFFF0000 FFFF0000"
                    set position            " 12 14 16 16"
                    set value               " 8100 $vlanid1 0800 0806"
                    set match_term_id       " 1 2 3 4"
                    set filter_id           1
                    set filter_condition    " ( 1|4 ) & ( 2|4 ) & ( 3|4 ) "
                }
            }
        }

    } else {

        if {$::wrptp_vlan_encap == "false"} {

            if {$::ptp_trans_type == "IEEE 802.3"} {
                set mask                " FFFF0000"
                set position            " 12"
                set value               " $ether_type "
                set match_term_id       " 1"
                set filter_id           1
                set filter_condition    "[join [string trim $match_term_id] " & "]"
            } elseif { $::ptp_comm_model == "Multicast" } {

                set mask                " FFFF0000 "
                set position            " 12 "
                set value               " 0800 "
                set match_term_id       " 1 "
                set filter_id           1
                set filter_condition    "1"

            } else {
                set mask                " FFFF0000 FFFF0000"
                set position            " 12  12"
                set value               " 0800 0806"
                set match_term_id       " 1 2"
                set filter_id           1
                set filter_condition    " 1 | 2"
            }
        } else {
            if {$::ptp_trans_type == "IEEE 802.3"} {
                set mask                " FFFF0000 0FFF0000 FFFF0000"
                set position            " 12 14 16"
                set value               " 8100 $vlanid1 88F7"
                set match_term_id       " 1 2 3"
                set filter_id           1
                set filter_condition    "[join [string trim $match_term_id] " & "]"
            } else {
                set mask                " FFFF0000 0FFF0000 FFFF0000 FFFF0000"
                set position            " 12 14 16 12"
                set value                " 8100 $vlanid1 0800 0806"
                set match_term_id       " 1 2 3 4"
                set filter_id           1
                set filter_condition    "( 1|4 ) & ( 2|4 ) &  ( 3|4 )"
            }
        }
    }

    for {set i 0} {$i < [llength $mask]} {incr i} {

        pltLib::create_filter_match_term\
                    -session_id         $::session_id\
                    -port_num           $port_num\
                    -match_term_id      [lindex $match_term_id $i]\
                    -mask               [lindex $mask $i]\
                    -byte_position      [lindex $position $i]\
                    -value              [lindex $value $i]
    }

    pltLib::create_filter_condition\
                -session_id         $::session_id\
                -port_num           $port_num\
                -filter_id          $filter_id\
                -filter_condition   $filter_condition\
                -filter_mode        $filter_mode

    pltLib::set_packet_capture_criteria\
                -session_id         $::session_id\
                -port_num           $port_num\
                -pkt_to_keep        $pkt_to_keep\
                -index              $filter_id\
                -bytes_to_keep      $bytes_to_keep

    pltLib::set_port_capture_mode\
                -session_id         $::session_id\
                -port_num           $port_num\
                -capture_flag       $capture_flag

    set ids "[string trim $match_term_id] $filter_id"

    return 1

}

#################################################################################
#  PROCEDURE NAME    : pltLib::wrptp_psd_name                                   #
#                                                                               #
#  DEFINITION        : This function is used to get the psd name from the       #
#                      hexdump                                                  #
#                                                                               #
#  INPUT PARAMETERS  :                                                          #
#                                                                               #
#  dump              : hex dump                                                 #
#                                                                               #
#  RETURNS           : packet name if hexdump is WRPTP packet or Unknown        # 
#                                                                               #
#  USAGE             :                                                          #
#                                                                               #
#                       pltLib::wrptp_psd_name\                                 #
#                           -hexdump                 $dump                      #
#                                                                               #
#################################################################################
proc pltLib::wrptp_psd_name {args} {

    array set param $args

    set packet $param(-hexdump)

    set pkt_name ""

    if {![pbLib::decode_wrptp_pkt\
            -packet                         $packet\
            -vlan_id                        vlan_id\
            -ip_protocol                    ip_protocol\
            -src_port                       src_port\
            -dest_port                      dest_port\
            -message_type                   message_type\
            -version_ptp                    version_ptp\
            -tlv_type                       tlv_type\
            -magic_number                   magic_number\
            -version_number                 version_number\
            -message_id                     message_id]} {
        return "Unknown"
    }

    if {$vlan_id != ""} {
        lappend pkt_name "VLAN"
    }

    if {$ip_protocol != ""} {
        lappend pkt_name "IP"
    }

    if {$message_type != ""} {
        switch $message_type {
            0  {lappend pkt_name Sync          }
            1  {lappend pkt_name Delay_Req     }
            2  {lappend pkt_name Pdelay_Req    }
            3  {lappend pkt_name Pdelay_Resp   }
            8  {lappend pkt_name Follow_Up     }
            9  {lappend pkt_name Delay_Resp    }
            10 {lappend pkt_name Pdelay_Resp_Follow_Up }
            11 {
                if {$message_id == 8192} {
                    lappend pkt_name Announce*
                } else {
                    lappend pkt_name Announce
                }
            }
            12 {
                switch $message_id {
                    4099 {lappend pkt_name CALIBRATE; set ::SENT_CALIBRATE 1 }
                    4100 {lappend pkt_name CALIBRATED    }
                    4097 {lappend pkt_name LOCK          }
                    4098 {lappend pkt_name LOCKED        }
                    4096 {lappend pkt_name SLAVE_PRESENT }
                    4101 {lappend pkt_name WR_MODE_ON    }
                    default {lappend pkt_name Signaling  }
                }
            }
            13 {lappend pkt_name Management    }
            default {lappend pkt_name Reserved }
        }
    }

    return [join $pkt_name +]

}
