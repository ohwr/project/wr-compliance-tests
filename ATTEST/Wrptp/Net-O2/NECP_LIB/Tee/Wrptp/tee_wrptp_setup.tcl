################################################################################
# File Name      : tee_wrptp_setup.tcl                                         #
# File Version   : 1.0                                                         #
# Component Name : ATTEST TEST EXECUTION ENGINE (TEE)                          #
# Module Name    : WRPTP (White Rabbit Precision Time Protocol)                #
################################################################################
# History       Date     Author         Addition/ Alteration                   #
################################################################################
#                                                                              #
#  1.0       July/2018      CERN          Initial                              #
#                                                                              #
################################################################################
# Copyright (c) 2018 CERN                                                      #
################################################################################

######################## PROCEDURES USED #######################################
#                                                                              #
#  1) wrptp::tee_configure_setup_001                                           #
#  2) wrptp::tee_cleanup_setup_001                                             #
#  3) wrptp::tee_configure_setup_002                                           #
#  4) wrptp::tee_cleanup_setup_002                                             #
#                                                                              #
################################################################################

package provide wrptp 1.0
namespace eval wrptp {

        namespace export *
}

#1.
################################################################################
#                                                                              #
# PROCEDURE NAME    : wrptp::tee_configure_setup_001                           #
#                                                                              #
# INPUT PARAMETERS  :                                                          #
#                                                                              #
#  session_id       : (Optional)  session id which is to be open with the Tee  #
#                                                                              #
#  port_list        : (Mandatory) List of port numbers in which streams        #
#                                 has to be send & received.                   #
#                                 format: ["src_port" "dst_port"]              #
#                                 (e.g,["0/0" "0/1"])                          #
#                                                                              #
# OUTPUT PARAMETERS : NONE.                                                    #
#                                                                              #
#  RETURNS          : 1 on success (if the TEE is configured for               #
#                                       Testsetup 1).                          #
#                                                                              #
#                     0 on failure (if the TEE could not be configured for     #
#                                        Testsetup 1).                         #
#                                                                              #
#  DEFINITION       : This function configures the parameters required for     #
#                         the Testsetup 1 at the TEE.                          #
#                                                                              #
# USAGE             :                                                          #
#                                                                              #
#     wrptp::tee_configure_setup_001\                                          #
#                                 -session_id       $session_id\               #
#                                 -port_list        $port_list                 #
#                                                                              #
################################################################################

proc wrptp::tee_configure_setup_001 {args} {

    global env

    array set param $args              
    
    LOG -level 1 -msg "\tAdd ports"

    if {[info exists param(-session_id)]} {

         set session_id $param(-session_id)
    } else {

             set session_id $::session_id
    } 
      
    if {[info exists param(-port_list)]} {

         set port_list $param(-port_list)
                LOG -level 1 -msg "\t\tPorts      ->     $port_list"
    } else {

         ERRLOG -msg "wrptp::tee_configure_setup_001 : ERROR - Missing Argument -> port_list\n"
         return 0
    }

    if {![wrptp::tee_cleanup_setup_001\
                 -session_id  $session_id\
                 -port_list   $port_list]} {
         ERRLOG -msg "wrptp::tee_configure_setup_001 : TEE Cleanup failed!\n"
         return 0

    }


    #### Connectivity Check ####
    foreach port_num $port_list {

       set sec 60

       while {$sec} {

         if {![pltLib::get_port_status\
                      -session_id              $session_id\
                      -port_num                $port_num\
                      -status                  status]} {

               ERRLOG -msg "Getting the status of port is failed\n"
               return 0
         }

         if {![string match -nocase "IN_SYNC" $status]} {

               incr sec -2
               after 2000
         } else {
               break
         }
       }
         if {![string match -nocase "IN_SYNC" $status]} {

               LOG -msg ""
               ERRLOG -msg "Connectivity between TEE port $port_num and DUT is DOWN. Ensure proper\
                            physical connectivity before proceeding further."
               return 0
         }
    }

    #### Adding Ports at the TEE ###
      
    foreach port_num $port_list {

        if {![pltLib::reserve_port\
                       -session_id              $session_id\
                       -port_num                $port_num]} {

                    ERRLOG -msg "Adding port at TEE is failed\n"
                    return 0
        }
    }

    return 1
}


#2.
################################################################################
# PROCEDURE NAME           : wrptp::tee_cleanup_setup_001                      #
#                                                                              #
# INPUT PARAMETERS         :                                                   #
#                                                                              #
#  session_id              : (Optional) session id to which the command is sent#
#                                                                              #
#  port_list               : (Mandatory) List of Ports to be added on the TEE  #
#                                        and on which PRP is to be enabled.    #
#                                        (e.g. ethernet 0/0, ethernet 0/1).    #
#                                                                              #
# OUTPUT PARAMETERS        : NONE.                                             #
#                                                                              #
#  RETURNS                 : 1 on success (if the configuration performed      #
#                                          for Testsetup 1 at the TEE is       #
#                                          cleared).                           #
#                                                                              #
#                            0 on failure (if the configuration performed      #
#                                          for Testsetup 1 at the TEE could    #
#                                          not be cleared).                    #
#                                                                              #
#  DEFINITION              : This function clears the configuration performed  #
#                            for Testsetup 1 at the TEE.                       #
#                                                                              #
#                            This function is internally called by the         #
#                            configure setup functions.                        #
#                                                                              #
# USAGE                    :                                                   #
#                                                                              #
#       wrptp::tee_cleanup_setup_001 \                                         #
#                -session_id                  $session_id\                     #
#                -port_list                   $port_list                       #
#                                                                              #
################################################################################

proc wrptp::tee_cleanup_setup_001 {args} {

    array set param $args
    set result 1
    
    if {[info exists param(-session_id)]} {

      set session_id $param(-session_id)
    } else {

          set session_id $::session_id
    }

    if {[info exists param(-port_list)]} {
      set port_list $param(-port_list)
    } else {
      ERRLOG -msg "wrptp::tee_cleanup_setup_001 : Missing Argument port_list\n"
          return 0
    }
    
    ### Releasing the port reserved at the TEE Ports ###
    
    foreach port_num $port_list {
        
        ### Resetting Port Configuration ###
        
           if {![pltLib::reserve_port\
                -session_id              $session_id\
                -port_num                $port_num]} {
      
                 return 0
           }
                ### Set the port transmit mode to OFF ###
                
                
                 if {![pltLib::set_traffic_operation\
                              -session_id              $session_id\
                              -port_num                $port_num\
                              -traffic_operation_flag  "OFF"]} {
                
                     return 0
                 }
                
                ### Setting Port Capture Mode "OFF" ###
                
                
                if {![pltLib::set_port_capture_mode\
                              -session_id              $session_id\
                              -port_num                $port_num\
                              -capture_flag            "OFF"]} {
                
                     return 0
                }

        
         if {![pltLib::reset_port\
                  -session_id              $session_id\
                  -port_num                $port_num]} {
        
             return 0
         }

        if {![pltLib::release_port\
                 -session_id              $session_id\
                 -port_num                $port_num]} {
       
             return 0
           }
        
      }
        
return 1
}


#3.
################################################################################
#                                                                              #
# PROCEDURE NAME    : wrptp::tee_configure_setup_002                           #
#                                                                              #
# INPUT PARAMETERS  :                                                          #
#                                                                              #
#  session_id       : (Optional)  session id which is to be open with the Tee  #
#                                                                              #
#  port_list        : (Mandatory) List of port numbers in which streams        #
#                                 has to be send & received.                   #
#                                 format: ["src_port" "dst_port"]              #
#                                 (e.g,["0/0" "0/1"])                          #
#                                                                              #
# OUTPUT PARAMETERS : NONE.                                                    #
#                                                                              #
#  RETURNS          : 1 on success (if the TEE is configured for               #
#                                       Testsetup 1).                          #
#                                                                              #
#                     0 on failure (if the TEE could not be configured for     #
#                                        Testsetup 1).                         #
#                                                                              #
#  DEFINITION       : This function configures the parameters required for     #
#                         the Testsetup 1 at the TEE.                          #
#                                                                              #
# USAGE             :                                                          #
#                                                                              #
#     wrptp::tee_configure_setup_002\                                          #
#                                 -session_id       $session_id\               #
#                                 -port_list        $port_list                 #
#                                                                              #
################################################################################
    
proc wrptp::tee_configure_setup_002 {args} {

global env

    array set param $args              
    
                LOG -level 1 -msg "\tAdd ports"

    if {[info exists param(-session_id)]} {

         set session_id $param(-session_id)
    } else {

             set session_id $::session_id
    } 
      
    if {[info exists param(-port_list)]} {

         set port_list $param(-port_list)
                LOG -level 1 -msg "\t\tPorts      ->     $port_list"
    } else {

         ERRLOG -msg "wrptp::tee_configure_setup_001 : ERROR - Missing Argument -> port_list\n"
         return 0
    }

#### Connectivity Check ####
    foreach port_num $port_list {

        set sec 60

        while {$sec} {

            if {![pltLib::get_port_status\
                            -session_id              $session_id\
                            -port_num                $port_num\
                            -status                  status]} {

                ERRLOG -msg "Getting the status of port is failed\n"
                return 0
            }

            if {![string match -nocase "IN_SYNC" $status]} {

                incr sec -2
                after 2000
            } else {
                break
            }
        }

        if {![string match -nocase "IN_SYNC" $status]} {

            LOG -msg ""
            ERRLOG -msg "Connectivity between TEE port $port_num and DUT is DOWN. Ensure proper\
                        physical connectivity before proceeding further."
            return 0
        }
    }

    wrptp::tee_cleanup_setup_002\
                -session_id  $session_id\
                -port_list   $port_list

#### Adding Ports at the TEE ###

    foreach port_num $port_list {

        if {![pltLib::reserve_port\
                    -session_id              $session_id\
                    -port_num                $port_num]} {

            ERRLOG -msg "Adding port at TEE is failed\n"
            return 0
        }
    }

    return 1
}


#4.
################################################################################
# PROCEDURE NAME           : wrptp::tee_cleanup_setup_002                      #
#                                                                              #
# INPUT PARAMETERS         :                                                   #
#                                                                              #
#  session_id              : (Optional) session id to which the command is sent#
#                                                                              #
#  port_list               : (Mandatory) List of Ports to be added on the TEE  #
#                                        and on which PRP is to be enabled.    #
#                                        (e.g. ethernet 0/0, ethernet 0/1).    #
#                                                                              #
# OUTPUT PARAMETERS        : NONE.                                             #
#                                                                              #
#  RETURNS                 : 1 on success (if the configuration performed      #
#                                          for Testsetup 1 at the TEE is       #
#                                          cleared).                           #
#                                                                              #
#                            0 on failure (if the configuration performed      #
#                                          for Testsetup 1 at the TEE could    #
#                                          not be cleared).                    #
#                                                                              #
#  DEFINITION              : This function clears the configuration performed  #
#                            for Testsetup 1 at the TEE.                       #
#                                                                              #
#                            This function is internally called by the         #
#                            configure setup functions.                        #
#                                                                              #
# USAGE                    :                                                   #
#                                                                              #
#       wrptp::tee_cleanup_setup_001 \                                         #
#                -session_id                  $session_id\                     #
#                -port_list                   $port_list                       #
#                                                                              #
################################################################################

proc wrptp::tee_cleanup_setup_002 {args} {

    array set param $args
    set result 1
    
    if {[info exists param(-session_id)]} {

      set session_id $param(-session_id)
    } else {

          set session_id $::session_id
    }

    if {[info exists param(-port_list)]} {
      set port_list $param(-port_list)
    } else {
      ERRLOG -msg "wrptp::tee_cleanup_setup_001 : Missing Argument port_list\n"
          return 0
    }

    ### Releasing the port reserved at the TEE Ports ###

    foreach port_num $port_list {

        ### Resetting Port Configuration ###

           if {![pltLib::reserve_port\
                -session_id              $session_id\
                -port_num                $port_num]} {

                 return 0
           }
                ### Set the port transmit mode to OFF ###

                 if {![pltLib::set_traffic_operation\
                              -session_id              $session_id\
                              -port_num                $port_num\
                              -traffic_operation_flag  "OFF"]} {

                     return 0
                 }

                ### Setting Port Capture Mode "OFF" ###

                if {![pltLib::set_port_capture_mode\
                              -session_id              $session_id\
                              -port_num                $port_num\
                              -capture_flag            "OFF"]} {

                     return 0
                }

         if {![pltLib::reset_port\
                  -session_id              $session_id\
                  -port_num                $port_num]} {

             return 0
         }

        if {![pltLib::release_port\
                 -session_id              $session_id\
                 -port_num                $port_num]} {

             return 0
           }

      }

    return 1
}
