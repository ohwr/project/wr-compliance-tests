###############################################################################
# File Name         : wrptp_global.tcl                                        #
# File Version      : 1.2                                                     #
# Component Name    : Global Variables Initialization                         #
# Module Name       : White Rabbit Precision Time Protocol                    #
###############################################################################
# History       Date       Author      Addition/ Alteration                   #
###############################################################################
#                                                                             #
#  1.0       Jun/2018      CERN        Initial                                #
#  1.1       Aug/2018      CERN        a) Changed the default value of        #
#                                         wrptp_log_announce_interval to 4    #
#                                      b) Added new variable                  #
#                                         wrptp_log_sync_interval             #
#                                         wrptp_cli_check_retry_limit         #
#                                         wrptp_exc_timeout_retry_extra       #
#                                         WRPTP_DEBUG_CLI                     #
#                                      c) Handled multicast MAC for UDP       #
#                                         and IEEE transport type             #
#  1.2       Dec/2018      CERN        a) Added WRPTP Timers for each state.  #
#                                                                             #
###############################################################################
# Copyright (c) 2018 CERN                                                     #
###############################################################################

### CONFIGURATION VARIABLES FROM GUI ###

global env
global wrptp_env
global NETO2HOME

global wrptp_sync_interval wrptp_sync_timeout
global wrptp_announce_interval wrptp_announce_timeout

global TEE_MAC
global dut_session_id tee_session_id
global dut_port dut_address
global dut_test_port_ip tee_test_port_ip
global dut_port_num_1 tee_port_num_1
global eth_type1 eth_type2
global tee_log_msg dut_log_msg
global ptp_comm_model
global unicast_flag ptp_two_step_flag
global ptp_trans_type
global ptp_vlan_encap
global ptp_clock_identity_format

global WRPTP_DUT_CHECK_RETRY_LIMIT
global WRPTP_DEBUG_CLI

# Maximum re-tries to check the CLI output, if the expected value is not present
set WRPTP_DUT_CHECK_RETRY_LIMIT     1

# Additional percentage wrStateTimeout to be used for EXC_TIMEOUT_RETRY
set wrptp_exc_timeout_retry_extra   10.0

set WRPTP_DEBUG_CLI 0

set ptp_dut_default_domain_number              $wrptp_env(DUT_WRPTP_DOMAIN_NUMBER)
set ptp_dut_default_priority1                  1

set ptp_version                                1

set wrptp_env(DUT_WRPTP_DELAY_MECHANISM_E2E)   1
set wrptp_env(DUT_WRPTP_DELAY_MECHANISM_P2P)   0
set wrptp_env(DUT_WRPTP_COM_MODEL)             "Multicast"
set wrptp_env(DUT_WRPTP_DEVICE_TYPE)           "Ordinary Clock"

#Removed delay in xena get command
set ::XENA_GET_DELAY         0
set ::RX_VALIDATION_INTERVAL 0
set ::RX_POLLING_INTERVAL    100

set TEE_MAC                         "00:00:00:00:00:00"
set PTP_ETHTYPE                     "88F7"
set ptp_dut_delay_mechanism         "e2e"
set reset_count_off                 "OFF"
set reset_count_on                  "ON"

set ptp_ingress_latency    0
set ptp_egress_latency     0
set ptp_constant_asymmetry 0
set ptp_delay_coefficient  0

set env(TEE_ATTEST_MANUAL_PROMPT)       0

set dut_port                            $env(dut_PORT)

set dut_address                         $env(TEST_DEVICE_IPV4_ADDRESS)

set dut_port_num_1                      $env(dut_to_tee_1_LOCATION)
set tee_port_num_1                      $env(tee_to_dut_1_LOCATION)

set dut_port_num_2                      $env(dut_to_tee_2_LOCATION)
set tee_port_num_2                      $env(tee_to_dut_2_LOCATION)

set dut_test_port_ip                    $env(dut_TEST_ADDRESS1)
set tee_test_port_ip                    "1.1.1.1"

set dut_test_port_ip_1                  $env(dut_TEST_ADDRESS1)
set tee_test_port_ip_1                  "1.1.1.1"

set dut_test_port_ip_2                  $env(dut_TEST_ADDRESS2)

set platform_type                       $env(TEE_PLATFORM_TYPE)

set dut_check_flag                      $env(DUT_CHECK_FLAG)

set ptp_user_delay                      $wrptp_env(DUT_WRPTP_USER_DELAY)

set ptp_dut_clock_step                  $wrptp_env(DUT_WRPTP_CLOCK_STEP)

set wrptp_vlan_encap                    $wrptp_env(DUT_WRPTP_IS_VLAN_ENCAP)
set wrptp_vlan_id                       [set vlan_portmap($tee_port_num_1)]
set wrptp_vlan_priority                 $wrptp_env(DUT_WRPTP_VLAN_PRIORITY)
set wrptp_vlan_id_1                     $wrptp_vlan_id
set ptp_trans_type                      $wrptp_env(DUT_WRPTP_TRANS_TYPE)
set ptp_device_type                     $wrptp_env(DUT_WRPTP_DEVICE_TYPE)
set ptp_comm_model                      $wrptp_env(DUT_WRPTP_COM_MODEL)

set wrptp_deltas_known                  $wrptp_env(DUT_WRPTP_DELTAS_KNOWN)
set wrptp_known_delta_tx                $wrptp_env(DUT_WRPTP_KNOWN_DELTA_TX)
set wrptp_known_delta_rx                $wrptp_env(DUT_WRPTP_KNOWN_DELTA_RX)

set wrptp_state_timeout                 $wrptp_env(DUT_WRPTP_STATE_TIMEOUT)
set wrptp_state_retry                   $wrptp_env(DUT_WRPTP_STATE_RETRY)

set wrptp_cal_period                    $wrptp_env(DUT_WRPTP_CAL_PERIOD)
set wrptp_cal_retry                     $wrptp_env(DUT_WRPTP_CAL_RETRY)

set wrptp_present_timeout               $wrptp_env(DUT_WR_PRESENT_TIMEOUT)
set wrptp_m_lock_timeout                $wrptp_env(DUT_WR_M_LOCK_TIMEOUT)
set wrptp_s_lock_timeout                $wrptp_env(DUT_WR_S_LOCK_TIMEOUT)
set wrptp_locked_timeout                $wrptp_env(DUT_WR_LOCKED_TIMEOUT)
set wrptp_calibration_timeout           $wrptp_env(DUT_WR_CALIBRATION_TIMEOUT)
set wrptp_calibrated_timeout            $wrptp_env(DUT_WR_CALIBRATED_TIMEOUT)
set wrptp_resp_calib_req_timeout        $wrptp_env(DUT_WR_RESP_CALIB_REQ_TIMEOUT)
set wrptp_wr_link_on_timeout            60000

#SYNC
set ptp_log_sync_interval               $wrptp_env(DUT_WRPTP_LOG_SYNC_INTERVAL)
set wrptp_log_sync_interval             $ptp_log_sync_interval
set ptp_sync_interval                   [expr pow(2, $ptp_log_sync_interval)]
set wrptp_sync_interval                 $ptp_sync_interval

#ANNOUNCE
set wrptp_log_announce_interval         1
set wrptp_announce_interval             [expr pow(2, $wrptp_log_announce_interval)]

#TIMEOUT
set wrptp_announce_receipt_timeout      3
set wrptp_announce_timeout              [expr ($wrptp_announce_receipt_timeout * $wrptp_announce_interval) + $ptp_user_delay]
set ptp_sync_timeout                    [expr (3 * $ptp_sync_interval) + $ptp_user_delay]
set wrptp_convert_state_timeout         [format "%.2f" $wrptp_state_timeout]
set wrptp_signal_timeout                [expr round((2 * ($wrptp_convert_state_timeout/1000)) + $ptp_user_delay + 5)]

#WR Timers
set wrptp_present_timeout_float         [format "%.2f" $wrptp_present_timeout]
set wrptp_present_timeout_recv          [expr round(($wrptp_state_retry * ($wrptp_present_timeout_float/1000)) +\
                                                     $ptp_user_delay + 5)]

set wrptp_m_lock_timeout_float          [format "%.2f" $wrptp_m_lock_timeout]
set wrptp_m_lock_timeout_recv           [expr round(($wrptp_state_retry * ($wrptp_m_lock_timeout_float/1000)) +\
                                                     $ptp_user_delay + 5)]

set wrptp_s_lock_timeout_float          [format "%.2f" $wrptp_s_lock_timeout]
set wrptp_s_lock_timeout_recv           [expr round(($wrptp_state_retry * ($wrptp_s_lock_timeout_float/1000)) +\
                                                     $ptp_user_delay + 5)]

set wrptp_locked_timeout_float          [format "%.2f" $wrptp_locked_timeout]
set wrptp_locked_timeout_recv           [expr round(($wrptp_state_retry * ($wrptp_locked_timeout_float/1000)) +\
                                                     $ptp_user_delay + 5)]

set wrptp_calibration_timeout_float     [format "%.2f" $wrptp_calibration_timeout]
set wrptp_calibration_timeout_recv      [expr round(($wrptp_state_retry * ($wrptp_calibration_timeout_float/1000)) +\
                                                     $ptp_user_delay + 5)]

set wrptp_calibrated_timeout_float      [format "%.2f" $wrptp_calibrated_timeout]
set wrptp_calibrated_timeout_recv       [expr round(($wrptp_state_retry * ($wrptp_calibrated_timeout_float/1000)) +\
                                                     $ptp_user_delay + 5)]

set wrptp_resp_calib_req_timeout_float  [format "%.2f" $wrptp_resp_calib_req_timeout]
set wrptp_resp_calib_req_timeout_recv   [expr round(($wrptp_state_retry * ($wrptp_resp_calib_req_timeout_float/1000)) +\
                                                     $ptp_user_delay + 5)]

set wrptp_resp_calib_req_timeout_2      [expr round(2 * $wrptp_resp_calib_req_timeout_float)]
set wrptp_resp_calib_req_timeout_210    [expr round($wrptp_resp_calib_req_timeout_2 +\
                                                  (($wrptp_exc_timeout_retry_extra/100) * $wrptp_resp_calib_req_timeout))]

set wrptp_resp_calib_req_timeout_5      [expr round(0.5 * $wrptp_resp_calib_req_timeout_float)]

set wrptp_wr_link_on_timeout_float      [format "%.2f" $wrptp_wr_link_on_timeout]
set wrptp_wr_link_on_timeout_recv       [expr round($wrptp_wr_link_on_timeout_float/1000) +\
                                                    $ptp_user_delay]

set m_lock_exc_timeout_retry            [expr round(($wrptp_m_lock_timeout * ($wrptp_state_retry + 1))+\
                                                   (($wrptp_exc_timeout_retry_extra/100) * $wrptp_m_lock_timeout))]

set calibration_exc_timeout_retry       [expr round(($wrptp_calibration_timeout * ($wrptp_state_retry + 1))+\
                                                   (($wrptp_exc_timeout_retry_extra/100) * $wrptp_calibration_timeout))]

set calibrated_exc_timeout_retry        [expr round(($wrptp_calibrated_timeout * ($wrptp_state_retry + 1))+\
                                                   (($wrptp_exc_timeout_retry_extra/100) * $wrptp_calibrated_timeout))]

set resp_calib_req_exc_timeout_retry    [expr round(($wrptp_resp_calib_req_timeout * ($wrptp_state_retry + 1))+\
                                                   (($wrptp_exc_timeout_retry_extra/100) * $wrptp_resp_calib_req_timeout))]

set present_exc_timeout_retry           [expr round(($wrptp_present_timeout * ($wrptp_state_retry + 1))+\
                                                   (($wrptp_exc_timeout_retry_extra/100) * $wrptp_present_timeout))]

set locked_exc_timeout_retry            [expr round(($wrptp_locked_timeout * ($wrptp_state_retry + 1))+\
                                                   (($wrptp_exc_timeout_retry_extra/100) * $wrptp_locked_timeout))]

set wrptp_calibrated_timeout_50         [expr round($wrptp_calibrated_timeout_float * 0.5)]

set ptp_delay_req_timeout               [expr 10 + $ptp_user_delay]
set ptp_delay_resp_timeout              [expr 10 + $ptp_user_delay]

set wrptp_intvl_tolerance               0.3
set wrptp_enable_time                   20

set wrptp_priority1                     $wrptp_env(DUT_WRPTP_PRIORITY1)
set wrptp_priority2                     $wrptp_env(DUT_WRPTP_PRIORITY2)
set wrptp_wrconfig                      $wrptp_env(DUT_WRPTP_WRCONFIG)
set ptp_clock_identity_format           "EUI-48"; #EUI-48, IEEE EUI-64, Non-IEEE EUI-64

if {$ptp_comm_model == "Unicast" } {
   set unicast_flag 1
} else {
   set unicast_flag 0
}

if {$ptp_dut_clock_step == "Two-Step"} {
    set ptp_two_step_flag 1
} else {
    set ptp_two_step_flag 0
}

### ETHERTYPE ###

global ETHERTYPE

set ETHERTYPE(PTP)                      "88F7"
set ETHERTYPE(VLAN)                     "8100"
set ETHERTYPE(IPV4)                     "0800"
set ETHERTYPE(ARP)                      "0806"

global PTP_UDP

set PTP_UDP(EVENT)                      "319"; # Sync, Delay_Req, Pdelay_Req, Pdelay_Resp
set PTP_UDP(GENERAL)                    "320"; # Announce, Follow_Up, Delay_Resp, Pdelay_Resp_Follow_Up, Management, Signaling

### PTP TRANSPORT TYPE ###

global PTP_TRANS_TYPE

set PTP_TRANS_TYPE(IEEE)                "IEEE 802.3"
set PTP_TRANS_TYPE(IPV4)                "UDP/IPv4"

if {$::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4)} {
    set PTP_E2E_MAC                     "01:00:5e:00:01:81"
} else {
    set PTP_E2E_MAC                     "01:1b:19:00:00:00"
}

set PTP_E2E_IP                          "224.0.1.129"

set PTP_P2P_MAC                         "01:80:C2:00:00:0E"
set PTP_P2P_IP                          "224.0.0.107"

### PTP DEVICE TYPE ###

global PTP_DEVICE_TYPE

set PTP_DEVICE_TYPE(OC)                 "Ordinary Clock"
set PTP_DEVICE_TYPE(BC)                 "Boundary Clock"
set PTP_DEVICE_TYPE(E2E_TC)             "E2E Transparent Clock"
set PTP_DEVICE_TYPE(P2P_TC)             "P2P Transparent Clock"

### portDS.delayMechanism ###

global PTP_DELAY_MECHANISM DUT_PTP_DELAY_MECHANISM

set PTP_DELAY_MECHANISM(E2E)            "01"
set PTP_DELAY_MECHANISM(P2P)            "02"

set DUT_PTP_DELAY_MECHANISM(E2E)        $wrptp_env(DUT_WRPTP_DELAY_MECHANISM_E2E)
set DUT_PTP_DELAY_MECHANISM(P2P)        $wrptp_env(DUT_WRPTP_DELAY_MECHANISM_P2P)

### PTP MESSAGE TYPES ###

global PTP_MESSAGE_TYPE

set PTP_MESSAGE_TYPE(SYNC)                  "0"
set PTP_MESSAGE_TYPE(DELAY_REQ)             "1"
set PTP_MESSAGE_TYPE(PDELAY_REQ)            "2"
set PTP_MESSAGE_TYPE(PDELAY_RESP)           "3"
set PTP_MESSAGE_TYPE(FOLLOW_UP)             "8"
set PTP_MESSAGE_TYPE(DELAY_RESP)            "9"
set PTP_MESSAGE_TYPE(PDELAY_RESP_FOLLOW_UP) "10"
set PTP_MESSAGE_TYPE(ANNOUNCE)              "11"
set PTP_MESSAGE_TYPE(SIGNALING)             "12"
set PTP_MESSAGE_TYPE(MANAGEMENT)            "13"


### PTP CONTROL FIELDS ###

global PTP_CONTROL_FIELD

set PTP_CONTROL_FIELD(SYNC)                     "00"
set PTP_CONTROL_FIELD(DELAY_REQ)                "01"
set PTP_CONTROL_FIELD(PDELAY_REQ)               "05"
set PTP_CONTROL_FIELD(PDELAY_RESP)              "05"
set PTP_CONTROL_FIELD(FOLLOW_UP)                "02"
set PTP_CONTROL_FIELD(DELAY_RESP)               "03"
set PTP_CONTROL_FIELD(PDELAY_RESP_FOLLOW_UP)    "05"
set PTP_CONTROL_FIELD(ANNOUNCE)                 "05"
set PTP_CONTROL_FIELD(SIGNALING)                "05"
set PTP_CONTROL_FIELD(MANAGEMENT)               "04"


### PTP MESSAGE LENGTH ###

global PTP_MESSAGE_LENGTH

set PTP_MESSAGE_LENGTH(SYNC)                    "44"
set PTP_MESSAGE_LENGTH(DELAY_REQ)               "44"
set PTP_MESSAGE_LENGTH(PDELAY_REQ)              "54"
set PTP_MESSAGE_LENGTH(PDELAY_RESP)             "54"
set PTP_MESSAGE_LENGTH(FOLLOW_UP)               "44"
set PTP_MESSAGE_LENGTH(DELAY_RESP)              "54"
set PTP_MESSAGE_LENGTH(PDELAY_RESP_FOLLOW_UP)   "54"
set PTP_MESSAGE_LENGTH(ANNOUNCE)                "64"


### XENA STATISTICS RESET ###

global RESET_COUNT

set RESET_COUNT(ON)                     "ON"
set RESET_COUNT(OFF)                    "OFF"

global PTP_ETHTYPE ARP_TYPE ARP_TIMEOUT

set ARP_TIMEOUT                         "60"
set GRANDMASTER_ID                      "ece555fffef5d9e0"
set INFINITY                            "-1"
set DEFAULT_DOMAIN                      $wrptp_env(DUT_WRPTP_DOMAIN_NUMBER)

## WRPTP TLV

set ORG_EXT_TLV                         3

### WRPTP wrMessageID ###
array set wrMessageID {
    ANN_SUFIX       8192
    CALIBRATE       4099
    CALIBRATED      4100
    LOCK            4097
    LOCKED          4098
    SLAVE_PRESENT   4096
    WR_MODE_ON      4101
    NONE            -1
}


### WRPTP SEND INSTANCES ###
array set INSTANCE {
    SYNC                    1
    ANNOUNCE                2
    DELAY_REQ               3
    DELAY_RESP              4
    FOLLOWUP                5
    PDELAY_REQ              6
    PDELAY_RESP             7
    PDELAY_RESP_FOLLOWUP    8
    SIGNAL                  9
    MGMT                    10
    ARP                     11
}

## WRPTP CONFIG ###
array set WRCONFIG {
    NON_WR                0
    WR_MASTER_ONLY        1
    WR_SLAVE_ONLY         2
    WR_MASTER_AND_SLAVE   3
}

array set WRPTP_WRCONFIG {
    NON_WR                0
    WR_M_ONLY             1
    WR_S_ONLY             2
    WR_M_AND_S            3
}

## WRPTP PORT STATE###
array set WRPTP_PORT_STATE {
    IDLE                    "IDLE"
    PRESENT                 "PRESENT"
    M_LOCK                  "M_LOCK"
    LOCKED                  "LOCKED"
    CALIBRATION             "CALIBRATION"
    CALIBRATED              "CALIBRATED"
    RESP_CALIB_REQ          "RESP_CALIB_REQ"
}

set SENT_CALIBRATE 0
