################################################################################
# File Name         : cli_wrptp_functions.tcl                                  #
# File Version      : 1.3                                                      #
# Component Name    : ATTEST DEVICE UNDER TEST (DUT)                           #
# Module Name       : White Rabbit Precision Time Protocol                     #
################################################################################
# History       Date       Author         Addition/ Alteration                 #
################################################################################
#                                                                              #
#  1.0       Jul/2018      CERN           Initial                              #
#  1.1       Aug/2018      CERN           Verification of attributes are done  #
#                                         once                                 #
#  1.2       Dec/2018      CERN           Added timer configuration for each   #
#                                         state in WRPTP state machine.        #
#  1.3       Apr/2019      JC.BAU         Added commit_changes procedure       #                     
#                                                                              #
################################################################################
# Copyright (c) 2018 CERN                                                      #
################################################################################

######################### PROCEDURES USED ######################################
#                                                                              #
#  CLI SET FUNCTIONS     :                                                     #
#                                                                              #
#  1) wrptp::cli_port_enable                                                   #
#  2) wrptp::cli_port_ptp_enable                                               #
#  3) wrptp::cli_set_vlan                                                      #
#  4) wrptp::cli_set_vlan_priority                                             #
#  5) wrptp::cli_global_ptp_enable                                             #
#  6) wrptp::cli_set_communication_mode                                        #
#  7) wrptp::cli_set_domain                                                    #
#  8) wrptp::cli_set_network_protocol                                          #
#  9) wrptp::cli_set_clock_mode                                                #
# 10) wrptp::cli_set_clock_step                                                #
# 11) wrptp::cli_set_delay_mechanism                                           #
# 12) wrptp::cli_set_priority1                                                 #
# 13) wrptp::cli_set_priority2                                                 #
# 14) wrptp::cli_set_announce_interval                                         #
# 15) wrptp::cli_set_ipv4_address                                              #
# 16) wrptp::cli_set_wr_config                                                 #
# 17) wrptp::cli_set_known_delta_tx                                            #
# 18) wrptp::cli_set_known_delta_rx                                            #
# 19) wrptp::cli_set_deltas_known                                              #
# 20) wrptp::cli_set_cal_period                                                #
# 21) wrptp::cli_set_cal_retry                                                 #
# 22) wrptp::cli_port_wrptp_enable                                             #
# 23) wrptp::cli_set_wr_present_timeout                                        #
# 24) wrptp::cli_set_wr_m_lock_timeout                                         #
# 25) wrptp::cli_set_wr_s_lock_timeout                                         #
# 26) wrptp::cli_set_wr_locked_timeout                                         #
# 27) wrptp::cli_set_wr_calibration_timeout                                    #
# 28) wrptp::cli_set_wr_calibrated_timeout                                     #
# 29) wrptp::cli_set_wr_resp_calib_req_timeout                                 #
# 30) wrptp::cli_set_wr_state_retry                                            #
# 31) wrptp::cli_set_wr_state_timeout                                          #
# 32) wrptp::cli_commit_changes                                          #
#                                                                              #
#  CLI DISABLE FUNCTIONS :                                                     #
#                                                                              #
#  1) wrptp::cli_port_disable                                                  #
#  2) wrptp::cli_port_ptp_disable                                              #
#  3) wrptp::cli_reset_vlan                                                    #
#  4) wrptp::cli_reset_vlan_priority                                           #
#  5) wrptp::cli_global_ptp_disable                                            #
#  6) wrptp::cli_reset_communication_mode                                      #
#  7) wrptp::cli_reset_domain                                                  #
#  8) wrptp::cli_reset_network_protocol                                        #
#  9) wrptp::cli_reset_clock_mode                                              #
# 10) wrptp::cli_reset_clock_step                                              #
# 11) wrptp::cli_reset_delay_mechanism                                         #
# 12) wrptp::cli_reset_priority1                                               #
# 13) wrptp::cli_reset_priority2                                               #
# 14) wrptp::cli_reset_announce_interval                                       #
# 15) wrptp::cli_reset_ipv4_address                                            #
# 16) wrptp::cli_reset_wr_config                                               #
# 17) wrptp::cli_reset_known_delta_tx                                          #
# 18) wrptp::cli_reset_known_delta_rx                                          #
# 19) wrptp::cli_reset_deltas_known                                            #
# 20) wrptp::cli_reset_cal_period                                              #
# 21) wrptp::cli_reset_cal_retry                                               #
# 22) wrptp::cli_port_wrptp_disable                                            #
# 23) wrptp::cli_reset_wr_present_timeout                                      #
# 24) wrptp::cli_reset_wr_m_lock_timeout                                       #
# 25) wrptp::cli_reset_wr_s_lock_timeout                                       #
# 26) wrptp::cli_reset_wr_locked_timeout                                       #
# 27) wrptp::cli_reset_wr_calibration_timeout                                  #
# 28) wrptp::cli_reset_wr_calibrated_timeout                                   #
# 29) wrptp::cli_reset_wr_resp_calib_req_timeout                               #
# 30) wrptp::cli_reset_wr_state_retry                                          #
# 31) wrptp::cli_reset_wr_state_timeout                                        #
#                                                                              #
#  CLI CHECK FUNCTIONS   :                                                     #
#                                                                              #
#  1) wrptp::cli_check_wr_config                                               #
#  2) wrptp::cli_check_wr_port_state                                           #
#  3) wrptp::cli_check_other_port_delta_tx                                     #
#  4) wrptp::cli_check_other_port_delta_rx                                     #
#  5) wrptp::cli_check_other_port_cal_period                                   #
#  6) wrptp::cli_check_other_port_cal_retry                                    #
#  7) wrptp::cli_check_other_port_cal_send_pattern                             #
#                                                                              #
################################################################################

################################################################################
#                           CLI SET FUNCTIONS                                  #
################################################################################


package provide wrptp 1.0

namespace eval wrptp {

    namespace export *
}

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_port_enable                                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) Port to be enabled.                         #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If port is enabled)                       #
#                                                                              #
#                      0 on Failure (If port could not be enabled)             #
#                                                                              #
#  DEFINITION        : This function enables a specific port on the DUT.       #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::cli_port_enable\                                   #
#                             -session_handler $session_handler\               #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc wrptp::cli_port_enable {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_port_enable : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_port_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_port_enable\
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_PORT_ENABLE"\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_PORT_ENABLE"\
                        -parameter1              $port_num]} {

            return 0
        }
    }

    return $result

}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_port_ptp_enable                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) Port on which ptp is to be enabled.         #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is enabled on the specified port   #
#                                    at the DUT).                              #
#                                                                              #
#                      0 on Failure (If PTP could not be enabled on the        #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function enables PTP on a specific port on the DUT.#
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::cli_port_ptp_enable\                               #
#                             -session_handler $session_handler\               #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc wrptp::cli_port_ptp_enable {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_port_ptp_enable : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_port_ptp_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_port_ptp_enable\
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_PORT_PTP_ENABLE"\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_PORT_PTP_ENABLE"\
                        -parameter1              $port_num]} {

            return 0
        }
    }

    return $result

}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_set_vlan                                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID with which VLAN has to be created.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#  port_num          : (Mandatory) Port which has to be associated to VLAN ID. #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If VLAN ID is created and port is         #
#                                    associated to it on the DUT).             #
#                                                                              #
#                      0 on Failure (If VLAN ID could not be created or port   #
#                                    is not associated to it on the DUT).      #
#                                                                              #
#  DEFINITION        : This function creates VLAN ID and associates given port #
#                      to it on the DUT.                                       #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::cli_set_vlan\                                      #
#                             -session_handler     $session_handler\           #
#                             -vlan_id             $vlan_id\                   #
#                             -port_num            $port_num                   #
#                                                                              #
################################################################################

proc wrptp::cli_set_vlan {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_set_vlan : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "wrptp::cli_set_vlan : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_set_vlan : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_set_vlan\
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_VLAN"\
                        -vlan_id                $vlan_id\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler         $session_handler \
                        -cmd_type                "DUT_SET_VLAN"\
                        -parameter1              $vlan_id\
                        -parameter2              $port_num]} {

            peturn 0
        }
    }

    return $result
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_set_vlan_priority                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID with which VLAN has to be created.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#  vlan_priority     : (Mandatory) VLAN priority to be associated              #
#                                  (e.g., 1)                                   #
#                                                                              #
#  port_num          : (Mandatory) Port with VLAN ID configured.               #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If VLAN priority is associated0           #
#                                                                              #
#                      0 on Failure (If VLAN priority could not be associated) #
#                                                                              #
#  DEFINITION        : This function associates given priority to VLAN         #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::cli_set_vlan_priority\                             #
#                             -session_handler     $session_handler\           #
#                             -vlan_id             $vlan_id\                   #
#                             -vlan_priority       $vlan_priority\             #
#                             -port_num            $port_num                   #
#                                                                              #
################################################################################

proc wrptp::cli_set_vlan_priority {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_set_vlan_priority : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "wrptp::cli_set_vlan_priority : Missing argument    -> vlan_id"
        incr missedargcounter
    }

    if {[info exists param(-vlan_priority)]} {
        set vlan_priority $param(-vlan_priority)
    } else {
        ERRLOG -msg "wrptp::cli_set_vlan_priority : Missing argument    -> vlan_priority"
        incr missedargcounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_set_vlan_priority : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_set_vlan_priority\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_VLAN_PRIORITY"\
                        -vlan_id                $vlan_id\
                        -vlan_priority          $vlan_priority\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler         $session_handler\
                        -cmd_type                "DUT_SET_VLAN_PRIORITY"\
                        -parameter1              $vlan_id\
                        -parameter2              $vlan_priority\
                        -parameter3              $port_num]} {

            return 0
        }
    }

    return $result
}


#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_global_ptp_enable                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is enabled on the DUT).            #
#                                                                              #
#                      0 on Failure (If PTP could not be enabled on the DUT).  #
#                                                                              #
#  DEFINITION        : This function enables PTP on the DUT.                   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::cli_global_ptp_enable\                             #
#                             -session_handler $session_handler                #
#                                                                              #
################################################################################

proc wrptp::cli_global_ptp_enable {args} {

    set missedArgCounter 0
    array set param $args

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_global_ptp_enable: Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_global_ptp_enable\
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_GLOBAL_PTP_ENABLE"]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_GLOBAL_PTP_ENABLE"]} {

            return 0
        }
    }

    return $result
}


#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_set_communication_mode                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  ptp_version       : (optional)  PTP version.                                #
#                                  (Default: 2)                                #
#                                                                              #
#  communication_mode : (Mandatory) Communication mode                         #
#                                  (e.g., "Unicast"|"Multicast")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If communication mode is set on the       #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If communication mode could not be        #
#                                    set on the specified port at the DUT).    #
#                                                                              #
#  DEFINITION        : This function sets the communication mode on a specific #
#                      port of the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::cli_set_communication_mode\                        #
#                             -session_handler       $session_handler\         #
#                             -port_num              $port_num\                #
#                             -clock_mode            $clock_mode\              #
#                             -ptp_version           $ptp_version\             #
#                             -communication_mode    $communication_mode       #
#                                                                              #
################################################################################

proc wrptp::cli_set_communication_mode {args} {

    array set param $args

    set missedArgCounter 0

 ## Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_set_communication_mode: Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
       set port_num $param(-port_num)
    } else {
       ERRLOG -msg "wrptp::cli_set_communication_mode : Missing Argument    -> port_num"
       incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::cli_set_communication_mode : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-communication_mode)]} {
        set communication_mode $param(-communication_mode)
    } else {
        ERRLOG -msg "wrptp::cli_set_communication_mode : Missing Argument  -> communication_mode"
        incr missedArgCounter
    }

    if {[info exists param(-ptp_version)]} {
        set ptp_version $param(-ptp_version)
    } else {
        set ptp_version "2"
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_set_communication_mode\
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_COMMUNICATION_MODE" \
                        -port_num               $port_num\
                        -clock_mode             $clock_mode\
                        -ptp_version            $ptp_version\
                        -communication_mode     $communication_mode]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_COMMUNICATION_MODE"\
                        -parameter1             $port_num\
                        -parameter2             $clock_mode\
                        -parameter3             $ptp_version\
                        -parameter4             $communication_mode]} {

            return 0
        }
    }

    return $result
}


#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_set_domain                                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  domain            : (Mandatory) Domain number.                              #
#                                  (e.g., 0..255)                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If domain number is set on the DUT).      #
#                                                                              #
#                      0 on Failure (If domain number could not able to set on #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function sets domain number on the DUT.            #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::cli_set_domain\                                    #
#                             -session_handler $session_handler\               #
#                             -clock_mode      $clock_mode\                    #
#                             -domain          $domain                         #
#                                                                              #
################################################################################

proc wrptp::cli_set_domain {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_set_domain : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::cli_set_domain : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-domain)]} {
        set domain $param(-domain)
    } else {
        ERRLOG -msg "wrptp::cli_set_domain : Missing Argument    -> domain"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_set_domain\
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_DOMAIN" \
                        -clock_mode             $clock_mode\
                        -domain                 $domain] 

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_DOMAIN"\
                        -parameter1             $clock_mode\
                        -parameter2             $domain]} {

            return 0
        }
    }

    return $result
}


#8.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_set_network_protocol                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) clock_mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  network_protocol  : (Mandatory) Network protocol.                           #
#                                  (e.g., "IEEE 802.3"|"UDP/IPv4")             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If network protocol is set on specified   #
#                                    port at the DUT).                         #
#                                                                              #
#                      0 on Failure (If network protocol could not be set      #
#                                    on specified port at the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets the network protocol on a specific   #
#                      port of the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                     wrptp::cli_set_network_protocol\                         #
#                             -session_handler  $session_handler\              #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -network_protocol $network_protocol              #
#                                                                              #
################################################################################

proc wrptp::cli_set_network_protocol {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_set_network_protocol : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_set_network_protocol : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::cli_set_network_protocol : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-network_protocol)]} {
        set network_protocol $param(-network_protocol)
    } else {
        ERRLOG -msg "wrptp::cli_set_network_protocol : Missing Argument    -> network_protocol"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_set_network_protocol\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_NETWORK_PROTOCOL"\
                        -port_num               $port_num\
                        -clock_mode             $clock_mode\
                        -network_protocol       $network_protocol]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_NETWORK_PROTOCOL"\
                        -parameter1             $port_num\
                        -parameter2             $clock_mode\
                        -parameter3             $network_protocol]} {

            return 0
        }
    }

    return $result
}


#9.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_set_clock_mode                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP clock mode is set on specified port#
#                                    of the DUT).                              #
#                                                                              #
#                      0 on Failure (If PTP clock mode could not be set on     #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function sets clock mode on a specific port of DUT.#
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::cli_set_clock_mode\                                #
#                             -session_handler $session_handler\               #
#                             -port_num        $port_num\                      #
#                             -clock_mode      $clock_mode                     #
#                                                                              #
################################################################################

proc wrptp::cli_set_clock_mode {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_set_clock_mode : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_set_clock_mode : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::cli_set_clock_mode : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_set_clock_mode\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_CLOCK_MODE"\
                        -port_num               $port_num\
                        -clock_mode             $clock_mode]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_CLOCK_MODE"\
                        -parameter1             $port_num\
                        -parameter2             $clock_mode]} {

            return 0
        }
    }

    return $result
}


#10.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_set_clock_step                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  clock_step        : (Mandatory) Clock step.                                 #
#                                  (e.g., "One-step"|"Two-step")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If clock-step is set on the specified     #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If clock-step could not be able to set    #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets the clock-step on a specific port on #
#                      the DUT.                                                #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::cli_set_clock_step\                                #
#                             -session_handler  $session_handler\              #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -clock_step       $clock_step                    #
#                                                                              #
################################################################################

proc wrptp::cli_set_clock_step {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_set_clock_step : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_set_clock_step : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::cli_set_clock_step : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-clock_step)]} {
        set clock_step $param(-clock_step)
    } else {
        ERRLOG -msg "wrptp::cli_set_clock_step : Missing Argument    -> clock_step"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_set_clock_step\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_CLOCK_STEP"\
                        -port_num               $port_num\
                        -clock_mode             $clock_mode\
                        -clock_step             $clock_step]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_CLOCK_STEP"\
                        -parameter1             $port_num\
                        -parameter1             $clock_mode\
                        -parameter3             $clock_step]} {

            return 0
        }
    }

    return $result
}


#11.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_set_delay_mechanism                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  delay_mechanism   : (Mandatory) Delay mechanism.                            #
#                                  (e.g., e2e|p2p|disable)                     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If delay mechanism is set on specified    #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If delay mechanism could not be set on    #
#                                    specified PTP port of the DUT).           #
#                                                                              #
#  DEFINITION        : This function sets the delay mechanism to a specific    #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::cli_set_delay_mechanism\                           #
#                             -session_handler  $session_handler\              #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -delay_mechanism  $delay_mechanism               #
#                                                                              #
################################################################################

proc wrptp::cli_set_delay_mechanism {args} {

    array set param $args

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_set_delay_mechanism : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_set_delay_mechanism : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::cli_set_delay_mechanism : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-delay_mechanism)]} {
        set delay_mechanism $param(-delay_mechanism)
    } else {
        ERRLOG -msg "wrptp::cli_set_delay_mechanism : Missing Argument    -> delay_mechanism"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_set_delay_mechanism\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_DELAY_MECHANISM"\
                        -port_num               $port_num\
                        -clock_mode             $clock_mode\
                        -delay_mechanism        $delay_mechanism]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_DELAY_MECHANISM"\
                        -parameter1             $port_num\
                        -parameter2             $clock_mode\
                        -parameter3             $delay_mechanism]} {

            return 0
        }
    }

    return $result
}


#12.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_set_priority1                                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority1         : (Mandatory) Prority1                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority1 is set on the DUT).          #
#                                                                              #
#                      0 on Failure (If priority1 could not be able to set on  #
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function sets the priority1 on the DUT.            #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::cli_set_priority1\                                 #
#                             -session_handler $session_handler\               #
#                             -clock_mode      $clock_mode\                    #
#                             -priority1       $priority1                      #
#                                                                              #
################################################################################

proc wrptp::cli_set_priority1 {args} {

    array set param $args

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_set_priority1 : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::cli_set_priority1 : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-priority1)]} {
        set priority1  $param(-priority1)
    } else {
        ERRLOG -msg "wrptp::cli_set_priority1 : Missing Argument    -> priority1"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_set_priority1\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_PRIORITY1"\
                        -clock_mode             $clock_mode\
                        -priority1              $priority1]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_PRIORITY1"\
                        -parameter1             $clock_mode\
                        -parameter2             $priority1]} { 

            return 0
        }
    }

    return $result
}


#13.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_set_priority2                                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority2         : (Mandatory) Prority2                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority2 is set on the DUT).          #
#                                                                              #
#                      0 on Failure (If priority2 could not be able to set on  #
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function sets the priority2 on the DUT.            #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::cli_set_priority2\                                 #
#                             -session_handler $session_handler\               #
#                             -clock_mode      $clock_mode\                    #
#                             -priority2       $priority2                      #
#                                                                              #
################################################################################

proc wrptp::cli_set_priority2 {args} {

    array set param $args

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_set_priority2 : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::cli_set_priority2 : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-priority2)]} {
        set priority2  $param(-priority2)
    } else {
        ERRLOG -msg "wrptp::cli_set_priority2 : Missing Argument    -> priority2"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_set_priority2\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_PRIORITY2"\
                        -clock_mode             $clock_mode\
                        -priority2              $priority2]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_PRIORITY2"\
                        -parameter1             $clock_mode\
                        -parameter2             $priority2]} { 

            return 0
        }
    }

    return $result
}


#14.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_set_announce_interval                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0).                              #
#                                                                              #
#  announce_interval : (Mandatory) announceInterval.                           #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If announceInterval value is set on the   #
#                                    specified port of DUT).                   #
#                                                                              #
#                      0 on Failure (If announceInterval could not be able to  #
#                                    set on specified port of the DUT).        #
#                                                                              #
#  DEFINITION        : This function sets the announceInterval value on the    #
#                      specified port of DUT.                                  #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::cli_set_announce_interval\                       #
#                             -session_handler          $session_handler\      #
#                             -port_num                 $port_num\             #
#                             -announce_interval        $announce_interval     #
#                                                                              #
################################################################################

proc wrptp::cli_set_announce_interval {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_set_announce_interval : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_set_announce_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-announce_interval)]} {
        set announce_interval $param(-announce_interval)
    } else {
        ERRLOG -msg "wrptp::cli_set_announce_interval : Missing Argument    -> announce_interval"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_set_announce_interval\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_ANNOUNCE_INTERVAL"\
                        -port_num               $port_num\
                        -announce_interval      $announce_interval]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_ANNOUNCE_INTERVAL"\
                        -parameter1             $port_num\
                        -parameter2             $announce_interval]} { 

            return 0
        }
    }

    return $result
}

#15.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_set_ipv4_address                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  ip_address        : (Mandatory) IPv4 address.                               #
#                                  (e.g., 192.168.200.1)                       #
#                                                                              #
#  net_mask          : (Mandatory) Subnet mask.                                #
#                                  (e.g., 255.255.255.0)                       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID.                                    #
#                                  (e.g., 100)                                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given IP address is set on specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given IP address could not be set on   #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function sets given IP address to a specific port  #
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::cli_set_ipv4_address\                              #
#                             -session_handler     $session_handler\           #
#                             -port_num            $port_num\                  #
#                             -ip_address          $ip_address\                #
#                             -net_mask            $net_mask\                  #
#                             -vlan_id             $vlan_id                    #
#                                                                              #
################################################################################

proc wrptp::cli_set_ipv4_address {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_set_ipv4_address : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_set_ipv4_address : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-ip_address)]} {
        set ip_address $param(-ip_address)
    } else {
        ERRLOG -msg "wrptp::cli_set_ipv4_address : Missing Argument    -> ip_address"
        incr missedArgCounter
    }

    if {[info exists param(-net_mask)]} {
        set net_mask $param(-net_mask)
    } else {
        ERRLOG -msg "wrptp::cli_set_ipv4_address : Missing Argument    -> net_mask"
        incr missedArgCounter
    }

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "wrptp::cli_set_ipv4_address : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_set_ipv4_address\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_IPV4_ADDRESS"\
                        -port_num               $port_num\
                        -ip_address             $ip_address\
                        -net_mask               $net_mask\
                        -vlan_id                $vlan_id]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_IPV4_ADDRESS"\
                        -parameter1             $port_num\
                        -parameter2             $ip_address\
                        -parameter3             $net_mask\
                        -parameter4             $vlan_id]} {

            return 0
        }
    }

    return $result
}


#16.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_set_wr_config                                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  value             : (Mandatory) Value for wrConfig.                         #
#                                  (e.g., "NON_WR"|"WR_S_ONLY"|"WR_M_ONLY"|    #
#                                         "WR_M_AND_S")                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value for wrConfig is set on     #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given value for wrConfig could not be  #
#                                    set on specifed port of the DUT).         #
#                                                                              #
#  DEFINITION        : This function sets given value for wrConfig to a        #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::cli_set_wr_config\                               #
#                             -session_handler          $session_handler\      #
#                             -port_num                 $port_num\             #
#                             -value                    $value                 #
#                                                                              #
################################################################################

proc wrptp::cli_set_wr_config {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_set_wr_config : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_set_wr_config : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-value)]} {
        set value $param(-value)
    } else {
        ERRLOG -msg "wrptp::cli_set_wr_config : Missing Argument    -> value"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_set_wr_config\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_WR_CONFIG"\
                        -port_num               $port_num\
                        -value                  $value]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_WR_CONFIG"\
                        -parameter1             $port_num\
                        -parameter2             $value]} {

            return 0
        }
    }

    return $result
}


#17.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_set_known_delta_tx                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  known_delta_tx    : (Mandatory) Transmission fixed delay                    #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given transmission fixed delay is set  #
#                                    on specified port of the DUT).            #
#                                                                              #
#                      0 on Failure (If given transmission fixed delay could   #
#                                    not be set on specifed port of the DUT).  #
#                                                                              #
#  DEFINITION        : This function sets given transmission fixed delay to a  #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::cli_set_known_delta_tx\                          #
#                             -session_handler          $session_handler\      #
#                             -port_num                 $port_num\             #
#                             -known_delta_tx           $known_delta_tx        #
#                                                                              #
################################################################################

proc wrptp::cli_set_known_delta_tx {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_set_known_delta_tx : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_set_known_delta_tx  : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-known_delta_tx)]} {
        set known_delta_tx $param(-known_delta_tx)
    } else {
        ERRLOG -msg "wrptp::cli_set_known_delta_tx  : Missing Argument    -> known_delta_tx"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_set_known_delta_tx\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_KNOWN_DELTA_TX"\
                        -port_num               $port_num\
                        -known_delta_tx         $known_delta_tx]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_KNOWN_DELTA_TX"\
                        -parameter1             $port_num\
                        -parameter2             $known_delta_tx]} {

            return 0
        }
    }

    return $result
}


#18.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_set_known_delta_rx                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  known_delta_rx    : (Mandatory) Reception fixed delay                       #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given reception fixed delay is set on  #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given reception fixed delay could not  #
#                                    be set on specifed port of the DUT).      #
#                                                                              #
#  DEFINITION        : This function sets given reception fixed delay to a     #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::cli_set_known_delta_rx\                          #
#                             -session_handler          $session_handler\      #
#                             -port_num                 $port_num\             #
#                             -known_delta_rx           $known_delta_rx        #
#                                                                              #
################################################################################

proc wrptp::cli_set_known_delta_rx {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_set_known_delta_rx : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_set_known_delta_rx  : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-known_delta_rx)]} {
        set known_delta_rx $param(-known_delta_rx)
    } else {
        ERRLOG -msg "wrptp::cli_set_known_delta_rx  : Missing Argument    -> known_delta_rx"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

     set result [wrptp::callback_cli_set_known_delta_rx\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_KNOWN_DELTA_RX"\
                        -port_num               $port_num\
                        -known_delta_rx         $known_delta_rx]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_KNOWN_DELTA_RX"\
                        -parameter1             $port_num\
                        -parameter2             $known_delta_rx]} {

            return 0
        }
    }

    return $result
}


#19.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_set_deltas_known                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If fixed delays are known is enabled on   #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If fixed delays are known could not be    #
#                                    enabled on specifed port of the DUT).     #
#                                                                              #
#  DEFINITION        : This function enables fixed delays are known on a       #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::cli_set_deltas_known\                            #
#                             -session_handler          $session_handler\      #
#                             -port_num                 $port_num              #
#                                                                              #
################################################################################

proc wrptp::cli_set_deltas_known {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_set_deltas_known : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_set_deltas_known : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

     set result [wrptp::callback_cli_set_deltas_known\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_DELTAS_KNOWN"\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_DELTAS_KNOWN"\
                        -parameter1             $port_num]} {

            return 0
        }
    }

    return $result
}


#20.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_set_cal_period                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  cal_period        : (Mandatory) calPeriod (in microseconds).                #
#                                  (e.g., 3000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given calPeriod is set on specified    #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given calPeriod could not be set on    #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function sets given calPeriod to a specific port on#
#                      the DUT.                                                #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::cli_set_cal_period\                              #
#                             -session_handler          $session_handler\      #
#                             -port_num                 $port_num\             #
#                             -cal_period               $cal_period            #
#                                                                              #
################################################################################

proc wrptp::cli_set_cal_period {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_set_cal_period : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_set_cal_period : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-cal_period)]} {
        set cal_period $param(-cal_period)
    } else {
        ERRLOG -msg "wrptp::cli_set_cal_period : Missing Argument    -> cal_period"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

      set result [wrptp::callback_cli_set_cal_period\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_CAL_PERIOD"\
                        -port_num               $port_num\
                        -cal_period             $cal_period]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_CAL_PERIOD"\
                        -parameter1             $port_num\
                        -parameter2             $cal_period]} {

            return 0
        }
    }

    return $result
}


#21.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_set_cal_retry                                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  cal_retry         : (Mandatory) calRetry.                                   #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given calRetry is set on specified port#
#                                    of the DUT).                              #
#                                                                              #
#                      0 on Failure (If given calRetry could not be set on     #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function sets given calRetry on a specific port on #
#                      the DUT.                                                #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::cli_set_cal_retry\                               #
#                             -session_handler          $session_handler\      #
#                             -port_num                 $port_num\             #
#                             -cal_retry                $cal_retry             #
#                                                                              #
################################################################################

proc wrptp::cli_set_cal_retry {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_set_cal_retry : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_set_cal_retry : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-cal_retry)]} {
        set cal_retry $param(-cal_retry)
    } else {
        ERRLOG -msg "wrptp::cli_set_cal_retry : Missing Argument    -> cal_retry"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

      set result [wrptp::callback_cli_set_cal_retry\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_CAL_RETRY"\
                        -port_num               $port_num\
                        -cal_retry              $cal_retry]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_CAL_RETRY"\
                        -parameter1             $port_num\
                        -parameter2             $cal_retry]} {

            return 0
        }
    }

    return $result
}


#22.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_port_wrptp_enable                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) Port on which wrptp is to be enabled.       #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If Wrptp is enabled on the specified port #
#                                    at the DUT).                              #
#                                                                              #
#                      0 on Failure (If Wrptp could not be enabled on the      #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function enables Wrptp on a specific port on the   #
#                      DUT.                                                    #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::cli_port_wrptp_enable\                           #
#                             -session_handler          $session_handler\      #
#                             -port_num                 $port_num              #
#                                                                              #
################################################################################

proc wrptp::cli_port_wrptp_enable {args} {

    array set param $args
    global env

    set config_mode $env(DUT_CONFIG_MODE)
    set session_handler $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_port_wrptp_enable : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_port_wrptp_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }


    set result [wrptp::callback_cli_port_wrptp_enable\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_PORT_WRPTP_ENABLE"\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_PORT_WRPTP_ENABLE"\
                        -parameter1             $port_num]} {

            return 0
        }
    }

    return $result
}


#23.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_set_wr_present_timeout                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_PRESENT_TIMEOUT (in milliseconds).       #
#                                  (e.g., 1000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_PRESENT_TIMEOUT is set on     #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_PRESENT_TIMEOUT could not be  #
#                                    set on specifed port of the DUT).         #
#                                                                              #
#  DEFINITION        : This function sets given WR_PRESENT_TIMEOUT to a        #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::cli_set_wr_present_timeout\                     #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::cli_set_wr_present_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_set_wr_present_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        ERRLOG -msg "wrptp::cli_set_wr_present_timeout : Missing Argument    -> timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_set_wr_present_timeout\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_WR_PRESENT_TIMEOUT"\
                        -port_num               $port_num\
                        -timeout                $timeout]

    if {$result == 2} {

        if {![Send_cli_command\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_WR_PRESENT_TIMEOUT"\
                        -parameter1             $port_num\
                        -parameter2             $timeout]} {

            return 0
        }
    }

    return $result
}


#24.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_set_wr_m_lock_timeout                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_M_LOCK_TIMEOUT (in milliseconds).        #
#                                  (e.g., 1000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_M_LOCK_TIMEOUT is set on      #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_M_LOCK_TIMEOUT could not be   #
#                                    set on specifed port of the DUT).         #
#                                                                              #
#  DEFINITION        : This function sets given WR_M_LOCK_TIMEOUT to a         #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::cli_set_wr_m_lock_timeout\                      #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::cli_set_wr_m_lock_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_set_wr_m_lock_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        ERRLOG -msg "wrptp::cli_set_wr_m_lock_timeout : Missing Argument    -> timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_set_wr_m_lock_timeout\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_WR_M_LOCK_TIMEOUT"\
                        -port_num               $port_num\
                        -timeout                $timeout]

    if {$result == 2} {

        if {![Send_cli_command\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_WR_M_LOCK_TIMEOUT"\
                        -parameter1             $port_num\
                        -parameter2             $timeout]} {

            return 0
        }
    }

    return $result
}


#25.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_set_wr_s_lock_timeout                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_S_LOCK_TIMEOUT (in milliseconds).        #
#                                  (e.g., 1000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_S_LOCK_TIMEOUT is set on      #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_S_LOCK_TIMEOUT could not be   #
#                                    set on specifed port of the DUT).         #
#                                                                              #
#  DEFINITION        : This function sets given WR_S_LOCK_TIMEOUT to a         #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::cli_set_wr_s_lock_timeout\                      #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::cli_set_wr_s_lock_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_set_wr_s_lock_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        ERRLOG -msg "wrptp::cli_set_wr_s_lock_timeout : Missing Argument    -> timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_set_wr_s_lock_timeout\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_WR_S_LOCK_TIMEOUT"\
                        -port_num               $port_num\
                        -timeout                $timeout]

    if {$result == 2} {

        if {![Send_cli_command\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_WR_S_LOCK_TIMEOUT"\
                        -parameter1             $port_num\
                        -parameter2             $timeout]} {

            return 0
        }
    }

    return $result
}


#26.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_set_wr_locked_timeout                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_LOCKED_TIMEOUT (in milliseconds).        #
#                                  (e.g., 1000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_LOCKED_TIMEOUT is set on      #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_LOCKED_TIMEOUT could not be   #
#                                    set on specifed port of the DUT).         #
#                                                                              #
#  DEFINITION        : This function sets given WR_LOCKED_TIMEOUT to a         #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::cli_set_wr_locked_timeout\                      #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::cli_set_wr_locked_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_set_wr_locked_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        ERRLOG -msg "wrptp::cli_set_wr_locked_timeout : Missing Argument    -> timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_set_wr_locked_timeout\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_WR_LOCKED_TIMEOUT"\
                        -port_num               $port_num\
                        -timeout                $timeout]

    if {$result == 2} {

        if {![Send_cli_command\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_WR_LOCKED_TIMEOUT"\
                        -parameter1             $port_num\
                        -parameter2             $timeout]} {

            return 0
        }
    }

    return $result
}


#27.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_set_wr_calibration_timeout                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_CALIBRATION_TIMEOUT (in milliseconds).   #
#                                  (e.g., 1000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_CALIBRATION_TIMEOUT is set on #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_CALIBRATION_TIMEOUT could not #
#                                    be set on specifed port of the DUT).      #
#                                                                              #
#  DEFINITION        : This function sets given WR_CALIBRATION_TIMEOUT to a    #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::cli_set_wr_calibration_timeout\                 #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::cli_set_wr_calibration_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_set_wr_calibration_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        ERRLOG -msg "wrptp::cli_set_wr_calibration_timeout : Missing Argument    -> timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_set_wr_calibration_timeout\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_WR_CALIBRATION_TIMEOUT"\
                        -port_num               $port_num\
                        -timeout                $timeout]

    if {$result == 2} {

        if {![Send_cli_command\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_WR_CALIBRATION_TIMEOUT"\
                        -parameter1             $port_num\
                        -parameter2             $timeout]} {

            return 0
        }
    }

    return $result
}


#28.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_set_wr_calibrated_timeout                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_CALIBRATED_TIMEOUT (in milliseconds).    #
#                                  (e.g., 1000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_CALIBRATED_TIMEOUT is set on  #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_CALIBRATED_TIMEOUT could not  #
#                                    be set on specifed port of the DUT).      #
#                                                                              #
#  DEFINITION        : This function sets given WR_CALIBRATED_TIMEOUT to a     #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::cli_set_wr_calibrated_timeout\                  #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::cli_set_wr_calibrated_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_set_wr_calibrated_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        ERRLOG -msg "wrptp::cli_set_wr_calibrated_timeout : Missing Argument    -> timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_set_wr_calibrated_timeout\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_WR_CALIBRATED_TIMEOUT"\
                        -port_num               $port_num\
                        -timeout                $timeout]

    if {$result == 2} {

        if {![Send_cli_command\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_WR_CALIBRATED_TIMEOUT"\
                        -parameter1             $port_num\
                        -parameter2             $timeout]} {

            return 0
        }
    }

    return $result
}


#29.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_set_wr_resp_calib_req_timeout                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_RESP_CALIB_REQ_TIMEOUT (in milliseconds).#
#                                  (e.g., 1000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_RESP_CALIB_REQ_TIMEOUT is set #
#                                    on specified port of the DUT).            #
#                                                                              #
#                      0 on Failure (If given WR_RESP_CALIB_REQ_TIMEOUT could  #
#                                    not be set on specifed port of the DUT).  #
#                                                                              #
#  DEFINITION        : This function sets given WR_RESP_CALIB_REQ_TIMEOUT to a #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::cli_set_wr_resp_calib_req_timeout\              #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::cli_set_wr_resp_calib_req_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_set_wr_resp_calib_req_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        ERRLOG -msg "wrptp::cli_set_wr_resp_calib_req_timeout : Missing Argument    -> timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_set_wr_resp_calib_req_timeout\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_WR_RESP_CALIB_REQ_TIMEOUT"\
                        -port_num               $port_num\
                        -timeout                $timeout]

    if {$result == 2} {

        if {![Send_cli_command\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_WR_RESP_CALIB_REQ_TIMEOUT"\
                        -parameter1             $port_num\
                        -parameter2             $timeout]} {

            return 0
        }
    }

    return $result
}


#30.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_set_wr_state_retry                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  wr_state_retry    : (Mandatory) wrStateRetry.                               #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given wrStateRetry is set on specified #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given wrStateRetry could not be set on #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function sets given wrStateRetry to a specific port#
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::cli_set_wr_state_retry\                          #
#                             -session_handler          $session_handler\      #
#                             -port_num                 $port_num\             #
#                             -wr_state_retry           $wr_state_retry        #
#                                                                              #
################################################################################

proc wrptp::cli_set_wr_state_retry {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_set_wr_state_retry : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_set_wr_state_retry : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-wr_state_retry)]} {
        set wr_state_retry $param(-wr_state_retry)
    } else {
        ERRLOG -msg "wrptp::cli_set_wr_state_retry : Missing Argument    -> wr_state_retry"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

      set result [wrptp::callback_cli_set_wr_state_retry\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_WR_STATE_RETRY"\
                        -port_num               $port_num\
                        -wr_state_retry         $wr_state_retry]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_WR_STATE_RETRY"\
                        -parameter1             $port_num\
                        -parameter2             $wr_state_retry]} {

            return 0
        }
    }

    return $result
} 


#31.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_set_wr_state_timeout                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  wr_state_timeout  : (Mandatory) wrStateTimeout (in milliseconds).           #
#                                  (e.g., 1000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given wrStateTimeout is set on         #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given wrStateTimeout could not be set  #
#                                    on specifed port of the DUT).             #
#                                                                              #
#  DEFINITION        : This function sets given wrStateTimeout to a specific   #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::cli_set_wr_state_timeout\                       #
#                             -session_handler          $session_handler\      #
#                              -port_num                $port_num\             #
#                              -wr_state_timeout        $wr_state_timeout      #
#                                                                              #
################################################################################

proc wrptp::cli_set_wr_state_timeout {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_set_wr_state_timeout : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_set_wr_state_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-wr_state_timeout)]} {
        set wr_state_timeout $param(-wr_state_timeout)
    } else {
        ERRLOG -msg "wrptp::cli_set_wr_state_timeout : Missing Argument    -> wr_state_timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

      set result [wrptp::callback_cli_set_wr_state_timeout\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_WR_STATE_TIMEOUT"\
                        -port_num               $port_num\
                        -wr_state_timeout       $wr_state_timeout]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_WR_STATE_TIMEOUT"\
                        -parameter1             $port_num\
                        -parameter2             $wr_state_timeout]} {

            return 0
        }
    }

    return $result
}

#32.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_commit_changes                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success                                            #
#                      0 on Failure                                            #
#                                                                              #
#  DEFINITION        : This function commit all changes in the DUT             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::cli_commit_changes\                             #
#                             -session_handler          $session_handler       #
#                                                                              #
################################################################################

proc wrptp::cli_commit_changes {args} {

	array set param $args
	global env

	set missedArgCounter 0

	### Checking Input Parameters ###

	if {[info exists param(-session_handler)]} {
		set session_handler $param(-session_handler)
	} else {
		ERRLOG -msg "wrptp::cli_commit_changes : Missing Argument    -> session_handler"
		incr missedArgCounter
	}

	if { $missedArgCounter != 0 } {
		return 0
	}

#	set result [wrptp::callback_cli_commit_changes\
#						-session_handler        $session_handler]

	set result 2
	if {$result == 2} {

		if {![Send_cli_command \
						-session_handler        $session_handler \
						-cmd_type               "DUT_COMMIT_CHANGES"]} {

			return 0
		}
	}

	return $result
}


################################################################################
#                       CLI DISABLING FUNCTIONS                                #
################################################################################


#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_port_disable                                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) Port to be disabled.                        #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If port is disabled)                      #
#                                                                              #
#                      0 on Failure (If port could not be disabled)            #
#                                                                              #
#  DEFINITION        : This function disables a specific port on the DUT.      #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::cli_port_disable\                                  #
#                             -session_handler $session_handler\               #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc wrptp::cli_port_disable {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_port_disable : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_port_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_port_disable\
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_PORT_DISABLE"\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_PORT_DISABLE"\
                        -parameter1              $port_num]} {

            return 0
        }
    }

    return $result

}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_port_ptp_disable                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) Port on which ptp is to be disabled.        #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is disabled on the specified port  #
#                                    at the DUT).                              #
#                                                                              #
#                      0 on Failure (If PTP could not be disabled on the       #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function disables PTP on a specific port on the DUT#
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::cli_port_ptp_disable\                              #
#                             -session_handler $session_handler\               #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc wrptp::cli_port_ptp_disable {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_port_ptp_disable : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_port_ptp_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_port_ptp_disable\
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_PORT_PTP_DISABLE"\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_PORT_PTP_DISABLE"\
                        -parameter1              $port_num]} {

            return 0
        }
    }

    return $result

}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_reset_vlan                                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID with which VLAN has to be deleted.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#  port_num          : (Mandatory) Port which has to be removed from VLAN ID.  #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If port is removed from VLAN ID and VLAN  #
#                                    ID is deleted from the DUT).              #
#                                                                              #
#                      0 on Failure (If port could not be removed from VLAN ID #
#                                    and VLAN ID is not deledted from the DUT).#
#                                                                              #
#  DEFINITION        : This function removes port from VLAN ID and deletes VLAN#
#                      ID from the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::cli_reset_vlan\                                    #
#                             -session_handler $session_handler\               #
#                             -vlan_id         $vlan_id\                       #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc wrptp::cli_reset_vlan {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_reset_vlan : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "wrptp::cli_reset_vlan : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_reset_vlan : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_reset_vlan\
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_VLAN"\
                        -vlan_id                $vlan_id\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_VLAN"\
                        -parameter1              $vlan_id\
                        -parameter2              $port_num]} {

            return 0
        }
    }

    return $result
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_reset_vlan_priority                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID with which VLAN has to be created.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#  vlan_priority     : (Mandatory) VLAN priority to be associated              #
#                                  (e.g., 1)                                   #
#                                                                              #
#  port_num          : (Mandatory) Port with VLAN ID configured.               #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If VLAN priority is disassociated)        #
#                                                                              #
#                      0 on Failure (If VLAN priority could not be             #
#                                    disassociated)                            #
#                                                                              #
#  DEFINITION        : This function disassociates given priority from VLAN    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::cli_reset_vlan_priority\                           #
#                             -session_handler     $session_handler\           #
#                             -vlan_id             $vlan_id\                   #
#                             -vlan_priority       $vlan_priority\             #
#                             -port_num            $port_num                   #
#                                                                              #
################################################################################

proc wrptp::cli_reset_vlan_priority {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_reset_vlan_priority : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "wrptp::cli_reset_vlan_priority : Missing argument    -> vlan_id"
        incr missedargcounter
    }

    if {[info exists param(-vlan_priority)]} {
        set vlan_priority $param(-vlan_priority)
    } else {
        ERRLOG -msg "wrptp::cli_reset_vlan_priority : Missing argument    -> vlan_priority"
        incr missedargcounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_reset_vlan_priority : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_reset_vlan_priority\
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_VLAN_PRIORITY"\
                        -vlan_id                $vlan_id\
                        -vlan_priority          $vlan_priority\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler         $session_handler \
                        -cmd_type                "DUT_RESET_VLAN_PRIORITY"\
                        -parameter1              $vlan_id\
                        -parameter2              $vlan_priority\
                        -parameter3              $port_num]} {

            return 0
        }
    }

    return $result
}


#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_global_ptp_disable                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is disabled on the DUT).           #
#                                                                              #
#                      0 on Failure (If PTP could not be disabled on the DUT). #
#                                                                              #
#  DEFINITION        : This function disables PTP on the DUT.                  #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::cli_port_ptp_disable\                              #
#                             -session_handler $session_handler                #
#                                                                              #
################################################################################

proc wrptp::cli_global_ptp_disable {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_global_ptp_disable : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_global_ptp_disable\
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_GLOBAL_PTP_DISABLE"]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_GLOBAL_PTP_DISABLE"]} {

            return 0
        }
    }

    return $result

}


#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_reset_communication_mode                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  ptp_version       : (optional)  PTP version.                                #
#                                  (Default: 2)                                #
#                                                                              #
#  communication_mode : (Mandatory) Communication mode                         #
#                                  (e.g., "Unicast"|"Multicast")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If communication mode is reset on the     #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If communication mode could not be        #
#                                    reset on the specified port at the DUT).  #
#                                                                              #
#  DEFINITION        : This function resets the communication mode on a        #
#                      specific port of the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::cli_reset_communication_mode\                      #
#                             -session_handler     $session_handler\           #
#                             -port_num            $port_num\                  #
#                             -clock_mode          $clock_mode\                #
#                             -ptp_version         $ptp_version\               #
#                             -communication_mode  $communication_mode         #
#                                                                              #
################################################################################

proc wrptp::cli_reset_communication_mode {args} {

    array set param $args

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_reset_communication_mode : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
       set port_num $param(-port_num)
    } else {
       ERRLOG -msg "wrptp::cli_reset_communication_mode : Missing Argument    -> port_num"
       incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::cli_reset_communication_mode : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-communication_mode)]} {
        set communication_mode $param(-communication_mode)
    } else {
        ERRLOG -msg "wrptp::cli_reset_communication_mode : Missing Argument  -> communication_mode"
        incr missedArgCounter
    }

    if {[info exists param(-ptp_version)]} {
        set ptp_version $param(-ptp_version)
    } else {
        set ptp_version "2"
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_reset_communication_mode\
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_COMMUNICATION_MODE" \
                        -port_num               $port_num\
                        -clock_mode             $clock_mode\
                        -ptp_version            $ptp_version\
                        -communication_mode     $communication_mode]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_COMMUNICATION_MODE"\
                        -parameter1             $port_num\
                        -parameter2             $clock_mode\
                        -parameter3             $ptp_version\
                        -parameter4             $communication_mode]} {

            return 0
        }
    }

    return $result
}


#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_reset_domain                                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  domain            : (Mandatory) Domain number.                              #
#                                  (e.g., 0..255)                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If domain number is reset on the DUT).    #
#                                                                              #
#                      0 on Failure (If domain number could not able to reset  #
#                                    on DUT).                                  #
#                                                                              #
#  DEFINITION        : This function resets domain number on the DUT.          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::cli_reset_domain\                                  #
#                             -session_handler $session_handler\               #
#                             -clock_mode      $clock_mode\                    #
#                             -domain          $domain                         #
#                                                                              #
################################################################################

proc wrptp::cli_reset_domain {args} {
    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_reset_domain : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::cli_reset_domain : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-domain)]} {
        set domain $param(-domain)
    } else {
        ERRLOG -msg "wrptp::cli_reset_domain : Missing Argument    -> domain"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_reset_domain\
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_DOMAIN" \
                        -clock_mode             $clock_mode\
                        -domain                 $domain] 

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_DOMAIN"\
                        -parameter1             $clock_mode\
                        -parameter2             $domain]} {

            return 0
        }
    }

    return $result
}


#8.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_reset_network_protocol                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) clock_mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  network_protocol  : (Mandatory) Network protocol.                           #
#                                  (e.g., "IEEE 802.3"|"UDP/IPv4")             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If network protocol is reset on specified #
#                                    port at the DUT).                         #
#                                                                              #
#                      0 on Failure (If network protocol could not be reset    #
#                                    on specified port at the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets the network protocol on a specific #
#                      port of the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                     wrptp::cli_reset_network_protocol\                       #
#                             -session_handler  $session_handler\              #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -network_protocol $network_protocol              #
#                                                                              #
################################################################################

proc wrptp::cli_reset_network_protocol {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_reset_network_protocol : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_reset_network_protocol : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::cli_reset_network_protocol : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-network_protocol)]} {
        set network_protocol $param(-network_protocol)
    } else {
        ERRLOG -msg "wrptp::cli_reset_network_protocol : Missing Argument    -> network_protocol"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_reset_network_protocol\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_NETWORK_PROTOCOL"\
                        -port_num               $port_num\
                        -clock_mode             $clock_mode\
                        -network_protocol       $network_protocol]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_NETWORK_PROTOCOL"\
                        -parameter1             $port_num\
                        -parameter2             $clock_mode\
                        -parameter3             $network_protocol]} {

            return 0
        }
    }

    return $result
}


#9.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_reset_clock_mode                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP clock mode is reset on specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If PTP clock mode could not be reset on   #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function resets clock mode on a specific port of   #
#                      DUT.                                                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::cli_reset_clock_mode\                              #
#                             -session_handler $session_handler\               #
#                             -port_num        $port_num\                      #
#                             -clock_mode      $clock_mode                     #
#                                                                              #
################################################################################

proc wrptp::cli_reset_clock_mode {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_reset_clock_mode : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_reset_clock_mode : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::cli_reset_clock_mode : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_reset_clock_mode\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_CLOCK_MODE"\
                        -port_num               $port_num\
                        -clock_mode             $clock_mode]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_CLOCK_MODE"\
                        -parameter1             $port_num\
                        -parameter2             $clock_mode]} {

            return 0
        }
    }

    return $result
}


#10.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_reset_clock_step                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  clock_step        : (Mandatory) Clock step.                                 #
#                                  (e.g., "One-step"|"Two-step")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If clock-step is reset on the specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If clock-step could not be able to reset  #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets the clock-step on a specific port  #
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::cli_reset_clock_step\                              #
#                             -session_handler  $session_handler\              #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -clock_step       $clock_step                    #
#                                                                              #
################################################################################

proc wrptp::cli_reset_clock_step {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_reset_clock_step : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_reset_clock_step : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::cli_reset_clock_step : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-clock_step)]} {
        set clock_step $param(-clock_step)
    } else {
        ERRLOG -msg "wrptp::cli_reset_clock_step : Missing Argument    -> clock_step"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_reset_clock_step\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_CLOCK_STEP"\
                        -port_num               $port_num\
                        -clock_mode             $clock_mode\
                        -clock_step             $clock_step]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_CLOCK_STEP"\
                        -parameter1             $port_num\
                        -parameter1             $clock_mode\
                        -parameter3             $clock_step]} {

            return 0
        }
    }

    return $result
}


#11.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_reset_delay_mechanism                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  delay_mechanism   : (Mandatory) Delay mechanism.                            #
#                                  (e.g., e2e|p2p|disable)                     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If delay mechanism is reset on specified  #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If delay mechanism could not be reset on  #
#                                    specified PTP port of the DUT).           #
#                                                                              #
#  DEFINITION        : This function resets the delay mechanism to a specific  #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::cli_reset_delay_mechanism\                         #
#                             -session_handler  $session_handler\              #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -delay_mechanism  $delay_mechanism               #
#                                                                              #
################################################################################

proc wrptp::cli_reset_delay_mechanism {args} {

    array set param $args

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_reset_delay_mechanism : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_reset_delay_mechanism : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::cli_reset_delay_mechanism : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-delay_mechanism)]} {
        set delay_mechanism $param(-delay_mechanism)
    } else {
        ERRLOG -msg "wrptp::cli_reset_delay_mechanism : Missing Argument    -> delay_mechanism"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_reset_delay_mechanism\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_DELAY_MECHANISM"\
                        -port_num               $port_num\
                        -clock_mode             $clock_mode\
                        -delay_mechanism        $delay_mechanism]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_DELAY_MECHANISM"\
                        -parameter1             $port_num\
                        -parameter2             $clock_mode\
                        -parameter3             $delay_mechanism]} {

            return 0
        }
    }

     return $result
}


#12.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_reset_priority1                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority1         : (Mandatory) Prority1                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority1 is reset on the DUT).        #
#                                                                              #
#                      0 on Failure (If priority1 could not be able to reset on#
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function resets the priority1 on the DUT.          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::cli_reset_priority1\                               #
#                             -session_handler $session_handler\               #
#                             -clock_mode      $clock_mode\                    #
#                             -priority1       $priority1                      #
#                                                                              #
################################################################################

proc wrptp::cli_reset_priority1 {args} {

    array set param $args

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_reset_priority1 : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::cli_reset_priority1 : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-priority1)]} {
        set priority1  $param(-priority1)
    } else {
        ERRLOG -msg "wrptp::cli_reset_priority1 : Missing Argument    -> priority1"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_reset_priority1\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_PRIORITY1"\
                        -clock_mode             $clock_mode\
                        -priority1              $priority1]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_PRIORITY1"\
                        -parameter1             $clock_mode\
                        -parameter2             $priority1]} {

            return 0
        }
    }

    return $result
}


#13.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_reset_priority2                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority2         : (Mandatory) Prority2                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority2 is reset on the DUT).        #
#                                                                              #
#                      0 on Failure (If priority2 could not be able to reset on#
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function resets the priority2 on the DUT.          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::cli_reset_priority2\                               #
#                             -session_handler $session_handler\               #
#                             -clock_mode      $clock_mode\                    #
#                             -priority2       $priority2                      #
#                                                                              #
################################################################################

proc wrptp::cli_reset_priority2 {args} {

    array set param $args

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_reset_priority2 : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::cli_reset_priority2 : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-priority2)]} {
        set priority2  $param(-priority2)
    } else {
        ERRLOG -msg "wrptp::cli_reset_priority2 : Missing Argument    -> priority2"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_reset_priority2\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_PRIORITY2"\
                        -clock_mode             $clock_mode\
                        -priority2              $priority2]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_PRIORITY2"\
                        -parameter1             $clock_mode\
                        -parameter2             $priority2]} {

            return 0
        }
    }

    return $result
}


#14.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_reset_announce_interval                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0).                              #
#                                                                              #
#  announce_interval : (Mandatory) announceInterval.                           #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If announceInterval value is reset on the #
#                                    specified port of DUT).                   #
#                                                                              #
#                      0 on Failure (If announceInterval could not be able to  #
#                                    reset on specified port of the DUT).      #
#                                                                              #
#  DEFINITION        : This function resets the announceInterval value on the  #
#                      specified port of DUT.                                  #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::cli_reset_announce_interval\                     #
#                             -session_handler          $session_handler\      #
#                             -port_num                 $port_num\             #
#                             -announce_interval        $announce_interval     #
#                                                                              #
################################################################################

proc wrptp::cli_reset_announce_interval {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_reset_announce_interval : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_reset_announce_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-announce_interval)]} {
        set announce_interval $param(-announce_interval)
    } else {
        ERRLOG -msg "wrptp::cli_reset_announce_interval : Missing Argument    -> announce_interval"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_reset_announce_interval\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_ANNOUNCE_INTERVAL"\
                        -port_num               $port_num\
                        -announce_interval      $announce_interval]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_ANNOUNCE_INTERVAL"\
                        -parameter1             $port_num\
                        -parameter2             $announce_interval]} { 

            return 0
        }
    }

    return $result
}

#15.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_reset_ipv4_address                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  ip_address        : (Mandatory) IPv4 address.                               #
#                                  (e.g., 192.168.200.1)                       #
#                                                                              #
#  net_mask          : (Mandatory) Subnet mask.                                #
#                                  (e.g., 255.255.255.0)                       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID.                                    #
#                                  (e.g., 100)                                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given IP address is reset on specified #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given IP address could not be reset on #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function resets given IP address to a specific port#
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::cli_reset_ipv4_address\                            #
#                             -session_handler     $session_handler\           #
#                             -port_num            $port_num\                  #
#                             -ip_address          $ip_address\                #
#                             -net_mask            $net_mask\                  #
#                             -vlan_id             $vlan_id                    #
#                                                                              #
################################################################################

proc wrptp::cli_reset_ipv4_address {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_reset_ipv4_address : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_reset_ipv4_address : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-ip_address)]} {
        set ip_address $param(-ip_address)
    } else {
        ERRLOG -msg "wrptp::cli_reset_ipv4_address : Missing Argument    -> ip_address"
        incr missedArgCounter
    }

    if {[info exists param(-net_mask)]} {
        set net_mask $param(-net_mask)
    } else {
        ERRLOG -msg "wrptp::cli_reset_ipv4_address : Missing Argument    -> net_mask"
        incr missedArgCounter
    }

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "wrptp::cli_reset_ipv4_address : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_reset_ipv4_address\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_IPV4_ADDRESS"\
                        -port_num               $port_num\
                        -ip_address             $ip_address\
                        -net_mask               $net_mask\
                        -vlan_id                $vlan_id]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_IPV4_ADDRESS"\
                        -parameter1             $port_num\
                        -parameter2             $ip_address\
                        -parameter3             $net_mask\
                        -parameter4             $vlan_id]} {

            return 0
        }
    }

    return $result
}


#16.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_reset_wr_config                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  value             : (Mandatory) Value for wrConfig.                         #
#                                  (e.g., "NON_WR"|"WR_S_ONLY"|"WR_M_ONLY"|    #
#                                         "WR_M_AND_S")                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value for wrConfig is reset on   #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given value for wrConfig could not be  #
#                                    reset on specifed port of the DUT).       #
#                                                                              #
#  DEFINITION        : This function resets given value for wrConfig to a      #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::cli_reset_wr_config\                             #
#                             -session_handler          $session_handler\      #
#                             -port_num                 $port_num\             #
#                             -value                    $value                 #
#                                                                              #
################################################################################

proc wrptp::cli_reset_wr_config {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_reset_wr_config : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_reset_wr_config : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-value)]} {
        set value $param(-value)
    } else {
        ERRLOG -msg "wrptp::cli_reset_wr_config : Missing Argument    -> value"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_reset_wr_config\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_WR_CONFIG"\
                        -port_num               $port_num\
                        -value                  $value]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_WR_CONFIG"\
                        -parameter1             $port_num\
                        -parameter2             $value]} {

            return 0
        }
    }

    return $result
}
 

#17.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_reset_known_delta_tx                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  known_delta_tx    : (Mandatory) Transmission fixed delay                    #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given transmission fixed delay is reset#
#                                    on specified port of the DUT).            #
#                                                                              #
#                      0 on Failure (If given transmission fixed delay could   #
#                                    not be reset on specifed port of the DUT).#
#                                                                              #
#  DEFINITION        : This function resets given transmission fixed delay to a#
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::cli_reset_known_delta_tx\                        #
#                             -session_handler          $session_handler\      #
#                             -port_num                 $port_num\             #
#                             -known_delta_tx           $known_delta_tx        #
#                                                                              #
################################################################################

proc wrptp::cli_reset_known_delta_tx {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_reset_known_delta_tx : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_reset_known_delta_tx  : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-known_delta_tx)]} {
        set known_delta_tx $param(-known_delta_tx)
    } else {
        ERRLOG -msg "wrptp::cli_reset_known_delta_tx  : Missing Argument    -> known_delta_tx"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_reset_known_delta_tx\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_KNOWN_DELTA_TX"\
                        -port_num               $port_num\
                        -known_delta_tx         $known_delta_tx]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_KNOWN_DELTA_TX"\
                        -parameter1             $port_num\
                        -parameter2             $known_delta_tx]} {

            return 0
        }
    }

    return $result
}


#18.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_reset_known_delta_rx                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  known_delta_rx    : (Mandatory) Reception fixed delay                       #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given reception fixed delay is reset on#
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given reception fixed delay could not  #
#                                    be reset on specifed port of the DUT).    #
#                                                                              #
#  DEFINITION        : This function sets given reception fixed delay to a     #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::cli_reset_known_delta_rx\                        #
#                             -session_handler          $session_handler\      #
#                             -port_num                 $port_num\             #
#                             -known_delta_rx           $known_delta_rx        #
#                                                                              #
################################################################################

proc wrptp::cli_reset_known_delta_rx {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_reset_known_delta_rx : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_reset_known_delta_rx  : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-known_delta_rx)]} {
        set known_delta_rx $param(-known_delta_rx)
    } else {
        ERRLOG -msg "wrptp::cli_reset_known_delta_rx  : Missing Argument    -> known_delta_rx"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_reset_known_delta_rx\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_KNOWN_DELTA_RX"\
                        -port_num               $port_num\
                        -known_delta_rx         $known_delta_rx]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_KNOWN_DELTA_RX"\
                        -parameter1             $port_num\
                        -parameter2             $known_delta_rx]} {

            return 0
        }
    }

    return $result
}


#19.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_reset_deltas_known                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If fixed delays are known is disabled on  #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If fixed delays are known could not be    #
#                                    disabled on specifed port of the DUT).    #
#                                                                              #
#  DEFINITION        : This function disables fixed delays are known on a      #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::cli_reset_deltas_known\                          #
#                             -session_handler          $session_handler\      #
#                             -port_num                 $port_num              #
#                                                                              #
################################################################################

proc wrptp::cli_reset_deltas_known {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_reset_deltas_known : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_reset_deltas_known : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_reset_deltas_known\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_DELTAS_KNOWN"\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_DELTAS_KNOWN"\
                        -parameter1             $port_num]} {

            return 0
        }
    }

    return $result
}


#20.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_reset_cal_period                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  cal_period        : (Mandatory) calPeriod (in microseconds).                #
#                                  (e.g., 3000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given calPeriod is reset on specified  #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given calPeriod could not be reset on  #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function resets given calPeriod to a specific port #
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::cli_reset_cal_period\                            #
#                             -session_handler          $session_handler\      #
#                             -port_num                 $port_num\             #
#                             -cal_period               $cal_period            #
#                                                                              #
################################################################################

proc wrptp::cli_reset_cal_period {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_reset_cal_period : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_reset_cal_period : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-cal_period)]} {
        set cal_period $param(-cal_period)
    } else {
        ERRLOG -msg "wrptp::cli_reset_cal_period : Missing Argument    -> cal_period"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_reset_cal_period\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_CAL_PERIOD"\
                        -port_num               $port_num\
                        -cal_period             $cal_period]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_CAL_PERIOD"\
                        -parameter1             $port_num\
                        -parameter2             $cal_period]} {

            return 0
        }
    }

    return $result
}


#21.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_reset_cal_retry                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  cal_retry         : (Mandatory) calRetry.                                   #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given calRetry is reset on specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given calRetry could not be reset on   #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function resets given calRetry on a specific port  #
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::cli_reset_cal_retry\                             #
#                             -session_handler          $session_handler\      #
#                             -port_num                 $port_num\             #
#                             -cal_retry                $cal_retry             #
#                                                                              #
################################################################################

proc wrptp::cli_reset_cal_retry {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_reset_cal_retry : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_reset_cal_retry : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-cal_retry)]} {
        set cal_retry $param(-cal_retry)
    } else {
        ERRLOG -msg "wrptp::cli_reset_cal_retry : Missing Argument    -> cal_retry"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_reset_cal_retry\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_CAL_RETRY"\
                        -port_num               $port_num\
                        -cal_retry              $cal_retry]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_CAL_RETRY"\
                        -parameter1             $port_num\
                        -parameter2             $cal_retry]} {

            return 0
        }
    }

    return $result
}


#22.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_port_wrptp_disable                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) Port on which wrptp is to be disabled.      #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If Wrptp is disabled on the specified port#
#                                    at the DUT).                              #
#                                                                              #
#                      0 on Failure (If Wrptp could not be disabled on the     #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function disables Wrptp on a specific port on the  #
#                      DUT.                                                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::cli_port_wrptp_disable\                            #
#                             -session_handler   $session_handler\             #
#                             -port_num          $port_num                     #
#                                                                              #
################################################################################

proc wrptp::cli_port_wrptp_disable {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_port_wrptp_disable : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_port_wrptp_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_port_wrptp_disable\
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_PORT_WRPTP_DISABLE"\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_PORT_WRPTP_DISABLE"\
                        -parameter1              $port_num]} {

            return 0
        }
    }

    return $result

}


#23.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_reset_wr_present_timeout                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Optional)  WR_PRESENT_TIMEOUT (in milliseconds).       #
#                                  (Default: 1000)                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_PRESENT_TIMEOUT is reset on   #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_PRESENT_TIMEOUT could not be  #
#                                    reset on specifed port of the DUT).       #
#                                                                              #
#  DEFINITION        : This function resets given WR_PRESENT_TIMEOUT to a      #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::cli_reset_wr_present_timeout\                   #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::cli_reset_wr_present_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_reset_wr_present_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        set timeout $::wrptp_present_timeout
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_reset_wr_present_timeout\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_WR_PRESENT_TIMEOUT"\
                        -port_num               $port_num\
                        -timeout                $timeout]

    if {$result == 2} {

        if {![Send_cli_command\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_WR_PRESENT_TIMEOUT"\
                        -parameter1             $port_num\
                        -parameter2             $timeout]} {

            return 0
        }
    }

    return $result
}


#24.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_reset_wr_m_lock_timeout                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Optional)  WR_M_LOCK_TIMEOUT (in milliseconds).        #
#                                  (Default: 15000)                            #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_M_LOCK_TIMEOUT is reset on    #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_M_LOCK_TIMEOUT could not be   #
#                                    reset on specifed port of the DUT).       #
#                                                                              #
#  DEFINITION        : This function resets given WR_M_LOCK_TIMEOUT to a       #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::cli_reset_wr_m_lock_timeout\                    #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::cli_reset_wr_m_lock_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_reset_wr_m_lock_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        set timeout $::wrptp_m_lock_timeout
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_reset_wr_m_lock_timeout\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_WR_M_LOCK_TIMEOUT"\
                        -port_num               $port_num\
                        -timeout                $timeout]

    if {$result == 2} {

        if {![Send_cli_command\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_WR_M_LOCK_TIMEOUT"\
                        -parameter1             $port_num\
                        -parameter2             $timeout]} {

            return 0
        }
    }

    return $result
}


#25.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_reset_wr_s_lock_timeout                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Optional)  WR_S_LOCK_TIMEOUT (in milliseconds).        #
#                                  (Default: 15000)                            #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_S_LOCK_TIMEOUT is reset on    #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_S_LOCK_TIMEOUT could not be   #
#                                    reset on specifed port of the DUT).       #
#                                                                              #
#  DEFINITION        : This function resets given WR_S_LOCK_TIMEOUT to a       #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::cli_reset_wr_s_lock_timeout\                    #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::cli_reset_wr_s_lock_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_reset_wr_s_lock_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        set timeout $::wrptp_s_lock_timeout
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_reset_wr_s_lock_timeout\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_WR_S_LOCK_TIMEOUT"\
                        -port_num               $port_num\
                        -timeout                $timeout]

    if {$result == 2} {

        if {![Send_cli_command\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_WR_S_LOCK_TIMEOUT"\
                        -parameter1             $port_num\
                        -parameter2             $timeout]} {

            return 0
        }
    }

    return $result
}


#26.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_reset_wr_locked_timeout                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Optional)  WR_LOCKED_TIMEOUT (in milliseconds).        #
#                                  (Default: 300)                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_LOCKED_TIMEOUT is reset on    #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_LOCKED_TIMEOUT could not be   #
#                                    reset on specifed port of the DUT).       #
#                                                                              #
#  DEFINITION        : This function resets given WR_LOCKED_TIMEOUT to a       #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::cli_reset_wr_locked_timeout\                    #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::cli_reset_wr_locked_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_reset_wr_locked_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        set timeout $::wrptp_locked_timeout
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_reset_wr_locked_timeout\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_WR_LOCKED_TIMEOUT"\
                        -port_num               $port_num\
                        -timeout                $timeout]

    if {$result == 2} {

        if {![Send_cli_command\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_WR_LOCKED_TIMEOUT"\
                        -parameter1             $port_num\
                        -parameter2             $timeout]} {

            return 0
        }
    }

    return $result
}


#27.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_reset_wr_calibration_timeout                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Optional)  WR_CALIBRATION_TIMEOUT (in milliseconds).   #
#                                  (Default: 3000)                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_CALIBRATION_TIMEOUT is reset  #
#                                    on specified port of the DUT).            #
#                                                                              #
#                      0 on Failure (If given WR_CALIBRATION_TIMEOUT could not #
#                                    be reset on specifed port of the DUT).    #
#                                                                              #
#  DEFINITION        : This function resets given WR_CALIBRATION_TIMEOUT to a  #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::cli_reset_wr_calibration_timeout\               #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::cli_reset_wr_calibration_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_reset_wr_calibration_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        set timeout $::wrptp_calibration_timeout
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_reset_wr_calibration_timeout\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_WR_CALIBRATION_TIMEOUT"\
                        -port_num               $port_num\
                        -timeout                $timeout]

    if {$result == 2} {

        if {![Send_cli_command\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_WR_CALIBRATION_TIMEOUT"\
                        -parameter1             $port_num\
                        -parameter2             $timeout]} {

            return 0
        }
    }

    return $result
}


#28.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_reset_wr_calibrated_timeout                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Optional)  WR_CALIBRATED_TIMEOUT (in milliseconds).    #
#                                  (Default: 300)                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_CALIBRATED_TIMEOUT is reset on#
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_CALIBRATED_TIMEOUT could not  #
#                                    be reset on specifed port of the DUT).    #
#                                                                              #
#  DEFINITION        : This function resets given WR_CALIBRATED_TIMEOUT to a   #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::cli_reset_wr_calibrated_timeout\                #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::cli_reset_wr_calibrated_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_reset_wr_calibrated_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        set timeout $::wrptp_calibrated_timeout
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_reset_wr_calibrated_timeout\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_WR_CALIBRATED_TIMEOUT"\
                        -port_num               $port_num\
                        -timeout                $timeout]

    if {$result == 2} {

        if {![Send_cli_command\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_WR_CALIBRATED_TIMEOUT"\
                        -parameter1             $port_num\
                        -parameter2             $timeout]} {

            return 0
        }
    }

    return $result
}


#29.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_reset_wr_resp_calib_req_timeout              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Optional)  WR_RESP_CALIB_REQ_TIMEOUT (in milliseconds).#
#                                  (Default: 3)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_RESP_CALIB_REQ_TIMEOUT is     #
#                                    reset on specified port of the DUT).      #
#                                                                              #
#                      0 on Failure (If given WR_RESP_CALIB_REQ_TIMEOUT could  #
#                                    not be reset on specifed port of the DUT).#
#                                                                              #
#  DEFINITION        : This function resets given WR_RESP_CALIB_REQ_TIMEOUT to #
#                      a specific port on the DUT.                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::cli_reset_wr_resp_calib_req_timeout\            #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::cli_reset_wr_resp_calib_req_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_reset_wr_resp_calib_req_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        set timeout $::wrptp_resp_calib_req_timeout
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_reset_wr_resp_calib_req_timeout\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_WR_RESP_CALIB_REQ_TIMEOUT"\
                        -port_num               $port_num\
                        -timeout                $timeout]

    if {$result == 2} {

        if {![Send_cli_command\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_WR_RESP_CALIB_REQ_TIMEOUT"\
                        -parameter1             $port_num\
                        -parameter2             $timeout]} {

            return 0
        }
    }

    return $result
}


#30.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_reset_wr_state_retry                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  wr_state_retry    : (Mandatory) wrStateRetry.                               #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given wrStateRetry is reset on         #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given wrStateRetry could not be reset  #
#                                    on specifed port of the DUT).             #
#                                                                              #
#  DEFINITION        : This function resets given wrStateRetry to a specific   #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::cli_reset_wr_state_retry\                        #
#                             -session_handler          $session_handler\      #
#                             -port_num                 $port_num\             #
#                             -wr_state_retry           $wr_state_retry        #
#                                                                              #
################################################################################

proc wrptp::cli_reset_wr_state_retry {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_reset_wr_state_retry : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_reset_wr_state_retry : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-wr_state_retry)]} {
        set wr_state_retry $param(-wr_state_retry)
    } else {
        ERRLOG -msg "wrptp::cli_reset_wr_state_retry : Missing Argument    -> wr_state_retry"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_reset_wr_state_retry\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_WR_STATE_RETRY"\
                        -port_num               $port_num\
                        -wr_state_retry         $wr_state_retry]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_WR_STATE_RETRY"\
                        -parameter1             $port_num\
                        -parameter2             $wr_state_retry]} {

            return 0
        }
    }

    return $result
}


#31.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_reset_wr_state_timeout                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  wr_state_timeout  : (Mandatory) wrStateTimeout (in milliseconds).           #
#                                  (e.g., 1000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given wrStateTimeout is reset on       #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given wrStateTimeout could not be reset#
#                                    on specifed port of the DUT).             #
#                                                                              #
#  DEFINITION        : This function resets given wrStateTimeout to a specific #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::cli_reset_wr_state_timeout\                     #
#                             -session_handler          $session_handler\      #
#                              -port_num                $port_num\             #
#                              -wr_state_timeout        $wr_state_timeout      #
#                                                                              #
################################################################################

proc wrptp::cli_reset_wr_state_timeout {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_reset_wr_state_timeout : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_reset_wr_state_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-wr_state_timeout)]} {
        set wr_state_timeout $param(-wr_state_timeout)
    } else {
        ERRLOG -msg "wrptp::cli_reset_wr_state_timeout : Missing Argument    -> wr_state_timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [wrptp::callback_cli_reset_wr_state_timeout\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_WR_STATE_TIMEOUT"\
                        -port_num               $port_num\
                        -wr_state_timeout       $wr_state_timeout]

    if {$result == 2} {

        if {![Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_WR_STATE_TIMEOUT"\
                        -parameter1             $port_num\
                        -parameter2             $wr_state_timeout]} {

            return 0
        }
    }

    return $result
}



################################################################################
#                           CLI CHECK FUNCTIONS                                #
################################################################################

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_check_wr_config                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  value             : (Mandatory) Value for wrConfig.                         #
#                                  (e.g., "NON_WR"|"WR_S_ONLY"|"WR_M_ONLY"|    #
#                                         "WR_M_AND_S")                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given state of wrConfig is verified for#
#                                    the specified port).                      #
#                                                                              #
#                      0 on Failure (If given state of wrConfig could not be   #
#                                    verified for the specified port).         #
#                                                                              #
#  DEFINITION        : This function verifies the given state of               #
#                       for a wrConfig on the specified port.                  #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::cli_check_wr_config\                             #
#                             -session_handler    $session_handler\            #
#                             -port_num           $port_num\                   #
#                             -value              $value                       #
#                                                                              #
################################################################################

proc wrptp::cli_check_wr_config {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_check_wr_config : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_check_wr_config : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-value)]} {
        set value $param(-value)
    } else {
        ERRLOG -msg "wrptp::cli_check_wr_config : Missing Argument    -> value"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {[info exists ::WRPTP_DUT_CHECK_RETRY_LIMIT]} {
        set ::WRPTP_DUT_CHECK_RETRY_LIMIT $::WRPTP_DUT_CHECK_RETRY_LIMIT
    } else {
        set ::WRPTP_DUT_CHECK_RETRY_LIMIT 1
    }

    set i 0

    while { $i < $::WRPTP_DUT_CHECK_RETRY_LIMIT} {
        incr i

        if {![Send_cli_command_to_read \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_CHECK_WR_CONFIG"\
                        -parameter1             $port_num\
                        -parameter2             $value\
                        -output                 buffer]} {

            LOG -level 4 -msg "ptp_ha::cli_check_wr_config: Error while checking wrConfig"

            return 0
        }

        set result [callback_cli_check_wr_config\
                        -session_handler        $session_handler\
                        -buffer                 $buffer\
                        -cmd_type               "DUT_CHECK_WR_CONFIG"\
                        -port_num               $port_num\
                        -value                  $value]

        if { $result == 1 } {
            return 1
        }
    }

    return 0
}

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_check_ptp_port_state                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  state             : (Mandatory) portState                                   #
#                                  (e.g., "MASTER"|"SLAVE")                    #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given portState is verified for the    #
#                                         "MASTER"|"SLAVE" on specified port). #
#                                                                              #
#                      0 on Failure (If given portState could not be verified  #
#                                    for the specified port).                  #
#                                                                              #
#  DEFINITION        : This function verifies the given portState on the       #
#                      specified port.                                         #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                        wrptp::cli_check_ptp_port_state\                      #
#                               -session_handler    $session_handler\          #
#                               -port_num           $port_num\                 #
#                               -state              $state                     #
#                                                                              #
################################################################################

proc wrptp::cli_check_ptp_port_state {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {

        set session_handler $param(-session_handler)

    } else {
        ERRLOG -msg "wrptp::cli_check_ptp_port_state : Missing Argument    -> session_handler"
        incr missedArgCounter
    }


    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_check_ptp_port_state : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-state)]} {
        set state $param(-state)
    } else {
        ERRLOG -msg "wrptp::cli_check_ptp_port_state : Missing Argument    -> state"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {[info exists ::WRPTP_DUT_CHECK_RETRY_LIMIT]} {
        set ::WRPTP_DUT_CHECK_RETRY_LIMIT $::WRPTP_DUT_CHECK_RETRY_LIMIT
    } else {
        set ::WRPTP_DUT_CHECK_RETRY_LIMIT 1
    }

    set i 0

    while { $i < $::WRPTP_DUT_CHECK_RETRY_LIMIT} {
        incr i

        if {![Send_cli_command_to_read \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_CHECK_PTP_PORT_STATE"\
                        -parameter1             $port_num\
                        -parameter2             $state\
                        -output                 buffer]} {

            LOG -level 4 -msg "ptp_ha::cli_check_ptp_port_state: Error while checking PTP Port State"

            return 0
        }

        set result [callback_cli_check_ptp_port_state\
                        -session_handler        $session_handler\
                        -buffer                 $buffer\
                        -cmd_type               "DUT_CHECK_PTP_PORT_STATE"\
                        -port_num               $port_num\
                        -state                  $state]

        if { $result == 1 } {
            return 1
        }
    }

    return 0
}

  
#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_check_wr_port_state                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  wr_port_state     : (Mandatory) wrPortState                                 #
#                                  (e.g., "IDLE"|"PRESENT"|"M_LOCK"|"S_LOCK"|  #
#                                         "LOCKED"|"CALIBRATION"|"CALIBRATED"| #
#                                         "RESP_CALIB_REQ"|"WR_LINK_ON")       #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given wrPortState is verified for the  #
#                                    specified port).                          #
#                                                                              #
#                      0 on Failure (If given wrPortState could not be verified#
#                                    for the specified port).                  #
#                                                                              #
#  DEFINITION        : This function verifies the given wrPortState on the     #
#                      specified port.                                         #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                        wrptp::cli_check_wr_port_state\                       #
#                               -session_handler    $session_handler\          #
#                               -port_num           $port_num\                 #
#                               -wr_port_state      $wr_port_state             #
#                                                                              #
################################################################################

proc wrptp::cli_check_wr_port_state {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {

        set session_handler $param(-session_handler)

    } else {
        ERRLOG -msg "wrptp::cli_check_wr_port_state : Missing Argument    -> session_handler"
        incr missedArgCounter
    }


    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_check_wr_port_state : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-wr_port_state)]} {
        set wr_port_state $param(-wr_port_state)
    } else {
        ERRLOG -msg "wrptp::cli_check_wr_port_state : Missing Argument    -> wr_port_state"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {[info exists ::WRPTP_DUT_CHECK_RETRY_LIMIT]} {
        set ::WRPTP_DUT_CHECK_RETRY_LIMIT $::WRPTP_DUT_CHECK_RETRY_LIMIT
    } else {
        set ::WRPTP_DUT_CHECK_RETRY_LIMIT 1
    }

    set i 0

    while { $i < $::WRPTP_DUT_CHECK_RETRY_LIMIT} {
        incr i

        if {![Send_cli_command_to_read \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_CHECK_WR_PORT_STATE"\
                        -parameter1             $port_num\
                        -parameter2             $wr_port_state\
                        -output                 buffer]} {

            LOG -level 0 -msg "ptp_ha::cli_check_wr_port_state: Error while checking WR Port State"

            return 0
        }

        set result [callback_cli_check_wr_port_state\
                        -session_handler        $session_handler\
                        -buffer                 $buffer\
                        -cmd_type               "DUT_CHECK_WR_PORT_STATE"\
                        -port_num               $port_num\
                        -wr_port_state          $wr_port_state]

        if { $result == 1 } {
            return 1
        }
    }

    return 0
}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_check_other_port_delta_tx                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num                : (Mandatory) DUT's port number.                    #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  other_port_delta_tx     : (Mandatory) Value of deltaTx                      #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of Delta Tx is verified for#
#                                    the specified port).                      #
#                                                                              #
#                      0 on Failure (If given value of Delta Tx could not be   #
#                                    verified for the specified port).         #
#                                                                              #
#  DEFINITION        : This function verifies the value of Delta Tx on the     #
#                      specified port.                                         #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::cli_check_other_port_delta_tx\                   #
#                              -session_handler        $session_handler\       #
#                              -port_num               $port_num\              #
#                              -other_port_delta_tx    $other_port_delta_tx    #
#                                                                              #
################################################################################

proc wrptp::cli_check_other_port_delta_tx {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_check_other_port_delta_tx : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_check_other_port_delta_tx : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-other_port_delta_tx)]} {
        set other_port_delta_tx $param(-other_port_delta_tx)
    } else {
        ERRLOG -msg "wrptp::cli_check_other_port_delta_tx : Missing Argument    -> other_port_delta_tx"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {[info exists ::WRPTP_DUT_CHECK_RETRY_LIMIT]} {
        set ::WRPTP_DUT_CHECK_RETRY_LIMIT $::WRPTP_DUT_CHECK_RETRY_LIMIT
    } else {
        set ::WRPTP_DUT_CHECK_RETRY_LIMIT 1
    }

    set i 0

    while { $i < $::WRPTP_DUT_CHECK_RETRY_LIMIT} {
        incr i

        if {![Send_cli_command_to_read \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_CHECK_OTHER_PORT_DELTA_TX"\
                        -parameter1             $port_num\
                        -parameter2             $other_port_delta_tx\
                        -output                 buffer]} {

            LOG -level 4 -msg "ptp_ha::cli_check_other_port_delta_tx: Error while checking Transmission fixed delay"

            return 0
        }

        set result [callback_cli_check_other_port_delta_tx\
                        -session_handler        $session_handler\
                        -buffer                 $buffer\
                        -cmd_type               "DUT_CHECK_OTHER_PORT_DELTA_TX"\
                        -port_num               $port_num\
                        -other_port_delta_tx    $other_port_delta_tx]

        if { $result == 1 } {
            return 1
        }
    }

    return 0
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_check_other_port_delta_rx                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num                : (Mandatory) DUT's port number.                    #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  other_port_delta_rx     : (Mandatory) Value of deltaRx                      #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of deltaRx is verified for #
#                                    the specified port).                      #
#                                                                              #
#                      0 on Failure (If given  value of deltaRx could not be   #
#                                    verified for the specified port).         #
#                                                                              #
#  DEFINITION        : This function verifies value of DeltaRx on the specified#
#                      port.                                                   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::cli_check_other_port_delta_rx\                  #
#                              -session_handler        $session_handler\       #
#                              -port_num                $port_num\             #
#                              -other_port_delta_rx     $other_port_delta_rx   #
#                                                                              #
################################################################################

proc wrptp::cli_check_other_port_delta_rx {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_check_other_port_delta_rx : Missing Argument    -> session_handler"
        incr missedArgCounter
    }


    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_check_other_port_delta_rx : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-other_port_delta_rx)]} {
        set other_port_delta_rx $param(-other_port_delta_rx)
    } else {
        ERRLOG -msg "wrptp::cli_check_other_port_delta_rx : Missing Argument    -> other_port_delta_rx"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {[info exists ::WRPTP_DUT_CHECK_RETRY_LIMIT]} {
        set ::WRPTP_DUT_CHECK_RETRY_LIMIT $::WRPTP_DUT_CHECK_RETRY_LIMIT
    } else {
        set ::WRPTP_DUT_CHECK_RETRY_LIMIT 1
    }

    set i 0

    while { $i < $::WRPTP_DUT_CHECK_RETRY_LIMIT} {
        incr i

        if {![Send_cli_command_to_read \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_CHECK_OTHER_PORT_DELTA_RX"\
                        -parameter1             $port_num\
                        -parameter2             $other_port_delta_rx\
                        -output                 buffer]} {

            LOG -level 4 -msg "ptp_ha::cli_check_other_port_delta_rx: Error while checking Reception fixed delay"

            return 0
        }

        set result [callback_cli_check_other_port_delta_rx\
                        -session_handler        $session_handler\
                        -buffer                 $buffer\
                        -cmd_type               "DUT_CHECK_OTHER_PORT_DELTA_RX"\
                        -port_num               $port_num\
                        -other_port_delta_rx    $other_port_delta_rx]

        if { $result == 1 } {
            return 1
        }
    }

    return 0
}


#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_check_other_port_cal_period                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num                : (Mandatory) DUT's port number.                    #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  other_port_cal_period   : (Mandatory) Value of the calPeriod                #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of the calPeriod is        #
#                                    verified for the specified port).         #
#                                                                              #
#                      0 on Failure (If given value of the calPeriod could not #
#                                    be verified for the specified port).      #
#                                                                              #
#  DEFINITION        : This function verifies the value of the calPeriod on the#
#                      specified port.                                         #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::cli_check_other_port_cal_period\                 #
#                             -session_handler          $session_handler\      #
#                             -port_num                 $port_num\             #
#                             -other_port_cal_period    $other_port_cal_period #
#                                                                              #
################################################################################

proc wrptp::cli_check_other_port_cal_period {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_check_other_port_cal_period : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_check_other_port_cal_period : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-other_port_cal_period)]} {
        set other_port_cal_period $param(-other_port_cal_period)
    } else {
        ERRLOG -msg "wrptp::cli_check_other_port_cal_period : Missing Argument    -> other_port_cal_period"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {[info exists ::WRPTP_DUT_CHECK_RETRY_LIMIT]} {
        set ::WRPTP_DUT_CHECK_RETRY_LIMIT $::WRPTP_DUT_CHECK_RETRY_LIMIT
    } else {
        set ::WRPTP_DUT_CHECK_RETRY_LIMIT 1
    }

    set i 0

    while { $i < $::WRPTP_DUT_CHECK_RETRY_LIMIT} {
        incr i

        if {![Send_cli_command_to_read \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_CHECK_OTHER_PORT_CAL_PERIOD"\
                        -parameter1             $port_num\
                        -parameter2             $other_port_cal_period\
                        -output                 buffer]} {

            LOG -level 4 -msg "ptp_ha::cli_check_other_port_cal_period: Error while checking value of calPeriod"

            return 0
        }

        set result [callback_cli_check_other_port_cal_period\
                        -session_handler        $session_handler\
                        -buffer                 $buffer\
                        -cmd_type               "DUT_CHECK_OTHER_PORT_CAL_PERIOD"\
                        -port_num               $port_num\
                        -other_port_cal_period  $other_port_cal_period]

        if { $result == 1 } {
            return 1
        }
    }

    return 0
}
 
#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_check_other_port_cal_retry                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num                : (Mandatory) DUT's port number.                    #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  other_port_cal_retry    : (Mandatory) Value of the calRetry                 #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of the calRetry is verified#
#                                    for the specified port).                  #
#                                                                              #
#                      0 on Failure (If given value of the calRetry could not  #
#                                    be verified for the specific port).       #
#                                                                              #
#  DEFINITION        : This function verifies the value of the calRetry on the #
#                      specified port.                                         #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::cli_check_other_port_cal_retry\                  #
#                             -session_handler          $session_handler\      #
#                             -port_num                $port_num\              #
#                             -other_port_cal_retry    $other_port_cal_retry   #
#                                                                              #
################################################################################

proc wrptp::cli_check_other_port_cal_retry {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_check_other_port_cal_retry : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_check_other_port_cal_retry : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-other_port_cal_retry)]} {
        set other_port_cal_retry $param(-other_port_cal_retry)
    } else {
        ERRLOG -msg "wrptp::cli_check_other_port_cal_retry : Missing Argument    -> other_port_cal_retry"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {[info exists ::WRPTP_DUT_CHECK_RETRY_LIMIT]} {
        set ::WRPTP_DUT_CHECK_RETRY_LIMIT $::WRPTP_DUT_CHECK_RETRY_LIMIT
    } else {
        set ::WRPTP_DUT_CHECK_RETRY_LIMIT 1
    }

    set i 0

    while { $i < $::WRPTP_DUT_CHECK_RETRY_LIMIT} {
        incr i

        if {![Send_cli_command_to_read \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_CHECK_OTHER_PORT_CAL_RETRY"\
                        -parameter1             $port_num\
                        -parameter2             $other_port_cal_retry\
                        -output                 buffer]} {

            LOG -level 4 -msg "ptp_ha::cli_check_other_port_cal_retry: Error while checking value of calRetry"

            return 0
        }

        set result [callback_cli_check_other_port_cal_retry\
                        -session_handler        $session_handler\
                        -buffer                 $buffer\
                        -cmd_type               "DUT_CHECK_OTHER_PORT_CAL_RETRY"\
                        -port_num               $port_num\
                        -other_port_cal_retry   $other_port_cal_retry]

        if { $result == 1 } {
            return 1
        }
    }

    return 0
}



#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::cli_check_other_port_cal_send_pattern            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num                       : (Mandatory) DUT's port number.             #
#                                        (e.g., fa0/0)                         #
#                                                                              #
#  other_port_cal_send_pattern    : (Mandatory) Value of calSendPattern        #
#                                        (e.g., TRUE|FALSE)                    #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of calSendPattern is       #
#                                    verified for the specified port).         #
#                                                                              #
#                      0 on Failure (If given value of calSendPattern could not#
#                                    be verified for the specified port).      #
#                                                                              #
#  DEFINITION        : This function verifies the value of calSendPattern on   #
#                      the specified port.                                     #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                wrptp::cli_check_other_port_cal_send_pattern\                 #
#                   -session_handler               $session_handler\           #
#                   -port_num                      $port_num\                  #
#                   -other_port_cal_send_pattern   $other_port_cal_send_pattern#
#                                                                              #
################################################################################

proc wrptp::cli_check_other_port_cal_send_pattern {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "wrptp::cli_check_other_port_cal_send_pattern : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::cli_check_other_port_cal_send_pattern : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-other_port_cal_send_pattern)]} {
        set other_port_cal_send_pattern $param(-other_port_cal_send_pattern)
    } else {
        ERRLOG -msg "wrptp::cli_check_other_port_cal_send_pattern : Missing Argument    -> other_port_cal_send_pattern"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {[info exists ::WRPTP_DUT_CHECK_RETRY_LIMIT]} {
        set ::WRPTP_DUT_CHECK_RETRY_LIMIT $::WRPTP_DUT_CHECK_RETRY_LIMIT
    } else {
        set ::WRPTP_DUT_CHECK_RETRY_LIMIT 1
    }

    set i 0

    while { $i < $::WRPTP_DUT_CHECK_RETRY_LIMIT} {
        incr i

        if {![Send_cli_command_to_read \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_CHECK_OTHER_PORT_CAL_SEND_PATTERN"\
                        -parameter1             $port_num\
                        -parameter2             $other_port_cal_send_pattern\
                        -output                 buffer]} {

            LOG -level 4 -msg "ptp_ha::cli_check_other_port_cal_send_pattern: Error while checking value of calSendPattern"

            return 0
        }

        set result [callback_cli_check_other_port_cal_send_pattern\
                        -session_handler                $session_handler\
                        -buffer                         $buffer\
                        -cmd_type                       "DUT_CHECK_OTHER_PORT_CAL_SEND_PATTERN"\
                        -port_num                       $port_num\
                        -other_port_cal_send_pattern    $other_port_cal_send_pattern]

        if { $result == 1 } {
            return 1
        }
    }

    return 0
}   





