###############################################################################
# File Name      : wrptp_initialization.tcl                                   #
# File Version   : 1.0                                                        #
# Component Name : ATTEST Initial Configuration Template                      #
# Module Name    : White Rabbit Precision Time Protocol                       #
###############################################################################
# History     Date         Author         Addition/ Alteration                #
###############################################################################
#                                                                             #
#  1.0       July/2018      CERN           Initial                            #
#                                                                             #
###############################################################################
# Copyright (c) 2018 CERN                                                     #
###############################################################################

#########################PROCEDURES USED#######################################
#                                                                             #
#  1) wrptp::Initialization                                                   #
#  2) wrptp::dut_Initialization                                               #
#  3) WRPTP_UNICAST_PRE_REQUISITE_CONFIGURATION                               #
#  4) wrptp::display_interface_details                                        #
#                                                                             #
###############################################################################

package provide wrptp 1.0
namespace eval wrptp {

    namespace export *
}


#1.
#################################################################################
# PROCEDURE NAME : wrptp::Initialization                                        #
#                                                                               #
# DEFINITION     : This function is used to initialize the TEE and DUT          #
#                                                                               #
# USAGE          :                                                              #
#                                                                               #
#                   wrptp::Initialization                                       #
#                               -port_list    $port_list                        #
#                                                                               #
#################################################################################

proc wrptp::Initialization {args} {

    global dut_port dut_address dut_session_id
    global tee_session_id
    global ip_address
    global user_name
    global passwd
    global telnet_port

    array set param $args

    if {[info exists param(-port_list)]} {

        set port_list $param(-port_list)
    } else {
        set port_list $::tee_port_num_1
    }

### Opening session with TEE ###
    if {![wrptp::tee_open_session\
                -session_id      tee_session_id\
                -user_name       $user_name]} {

                ERRLOG -msg   "Opening of session with TEE is failed\n"

                TC_ABORT -reason "Opening of session with TEE is failed\n"\

                return 0

    }

    set port_count 1

    foreach port $port_list {

        wrptp::get_port_macaddress\
            -session_id         $tee_session_id\
            -port_num           $port\
            -mac_address        ::TEEMAC_$port_count

        if {$port_count == 1} {
            set ::TEE_MAC  $::TEEMAC_1

            wrptp::tee_get_interface_ip\
                -reference_ip  $::dut_test_port_ip\
                -ip            ::tee_test_port_ip

            incr port_count
            continue
        }

        wrptp::tee_get_interface_ip\
            -reference_ip  [set ::dut_test_port_ip_$port_count]\
            -ip            ::tee_test_port_ip_$port_count


        incr port_count

    }

    if {![wrptp::tee_configure_setup_001 \
             -port_list        $port_list]} {
 
            ERRLOG -msg "TEE Initialization failed!" 

            TC_ABORT -reason "TEE Initialization failed." 

    }

    return 1

}


#2.
#################################################################################
# PROCEDURE NAME : wrptp::dut_Initialization                                    #
#                                                                               #
# DEFINITION     : This function is used to initialize the TEE and DUT          #
#                                                                               #
# USAGE          :                                                              #
#                                                                               #
#                   wrptp::dut_Initialization                                   #
#                               -port_list    $port_list                        #
#                                                                               #
#################################################################################

proc wrptp::dut_Initialization { } {

    global dut_port dut_address dut_session_id dut_session_handler

        dut_open_session\
                 -port            $dut_port \
                 -ip_address      $dut_address \
                 -session_handler dut_session_id

        set dut_session_handler $dut_session_id

        if {$dut_session_id == 0} {

            ERRLOG -msg   "Opening of session with DUT is failed\n"

            TC_ABORT -reason "Opening of session with DUT is failed\n"\

            return 0
        }

}


#3.
################################################################################
#  PROCEDURE NAME    : WRPTP_UNICAST_PREREQUISTE_CONFIGURATION                 #
#                                                                              #
#  DEFINITION        : This function get the mac address of a port from traffic#
#                      generator                                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_id        : (Optional) session id to which the command is sent      #
#                                                                              #
#  port_num          : (Mandatory) Port whose mac address has to obtain        #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  mac_address       : (Mandatory) Mac address of the traffic generator port   #
#                                                                              #
#  RETURNS           : 1 on success (If the port is sync)                      #
#                                                                              #
#                      0 on failure (If the port is not sync)                  #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#             wrptp::get_port_macaddress\                                      #
#                                      -session_id      $session_id \          #
#                                      -port_num        $port_num\             #
#                                      -mac_address     mac_address            #
#                                                                              #
################################################################################


proc WRPTP_UNICAST_PRE_REQUISITE_CONFIGURATION  { args } {

    array set param $args

    set port_num    $param(-port_num)
    set tee_mac     $param(-tee_mac)
    set tee_ip      $param(-tee_ip)
    set dut_ip      $param(-dut_ip)
    upvar $param(-dut_mac)  dut_mac
    set timeout     $param(-timeout)

    LOG -level 1 -msg "\n\t\t Resolving for ARP \t\t\n"

    if {![pltLib::create_filter_ptp_pkt\
            -port_num                $port_num\
            -ether_type              "0806"\
            -ids                     id1]} {

            return 0
    }

    set filter_id [lindex $id1 end]

    if {![wrptp::resolve_ip_to_mac\
                -port_num         $port_num\
                -src_mac          $tee_mac\
                -sender_ip        $tee_ip\
                -ip               $dut_ip\
                -filter_id        $filter_id\
                -mac              dut_mac\
                -timeout          $timeout]} {

             return 0

    }

    if {![pltLib::delete_filter\
            -port_num                $port_num\
            -session_id              $::tee_session_id\
            -ids                     $filter_id]} {

            return 0
    }

    LOG -level 1 -msg "\t\t The ARP is resolved \t\t\n"

    return 1

}

#4.
#################################################################################
# PROCEDURE NAME : wrptp::display_interface_details                             #
#                                                                               #
# DEFINITION     : This function is used to display the interface details       #
#                                                                               #
# USAGE          :                                                              #
#                                                                               #
#                   wrptp::display_interface_details                            #
#                               -port_list    $port_list                        #
#                                                                               #
#################################################################################

proc wrptp::display_interface_details {args } {

    array set param $args

    if {[info exists param(-port_list)]} {
        set port_list $param(-port_list)
    } else {
        set port_list $::tee_port_num_1
    }

    set count 1    

    foreach port $port_list {
    
        LOG -level 1 -msg "On T$count, TEE Port is : $port"

        LOG -level 1 -msg "On P$count, DUT Port is : [set ::dut_port_num_$count]"

        incr count

    }

} 
