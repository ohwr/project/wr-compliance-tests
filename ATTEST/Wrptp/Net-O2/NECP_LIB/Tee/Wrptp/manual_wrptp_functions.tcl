################################################################################
# File Name         : manual_wrptp_functions.tcl                               #
# File Version      : 1.3                                                      #
# Component Name    : ATTEST DEVICE UNDER TEST (DUT)                           #
# Module Name       : White Rabbit Precision Time Protocol                     #
################################################################################
# History       Date       Author         Addition/ Alteration                 #
################################################################################
#                                                                              #
#  1.0       Jul/2018      CERN          Initial                               #
#  1.1       Dec/2018      CERN          Added timer configuration for each    #
#                                         state in WRPTP state machine.        #
#  1.2       Jan/2019      CERN           Removed below procedures,            #
#                                         a) manual_set_wr_state_timeout       #
#                                         b) manualdut_reset_wr_state_timeout  #
#  1.3       Apr/2019      JC.BAU         Added commit_changes procedure       #                     
#                                                                              #
################################################################################
# Copyright (c) 2018 - 2019 CERN                                               #
################################################################################

######################### PROCEDURES USED ######################################
#                                                                              #
#  DUT SET FUNCTIONS     :                                                     #
#                                                                              #
#  1) wrptp::manual_port_enable                                                #
#  2) wrptp::manual_port_ptp_enable                                            #
#  3) wrptp::manual_set_vlan                                                   #
#  4) wrptp::manual_set_vlan_priority                                          #
#  5) wrptp::manual_global_ptp_enable                                          #
#  6) wrptp::manual_set_communication_mode                                     #
#  7) wrptp::manual_set_domain                                                 #
#  8) wrptp::manual_set_network_protocol                                       #
#  9) wrptp::manual_set_clock_mode                                             #
# 10) wrptp::manual_set_clock_step                                             #
# 11) wrptp::manual_set_delay_mechanism                                        #
# 12) wrptp::manual_set_priority1                                              #
# 13) wrptp::manual_set_priority2                                              #
# 14) wrptp::manual_set_announce_interval                                      #
# 15) wrptp::manual_set_ipv4_address                                           #
# 16) wrptp::manual_set_wr_config                                              #
# 17) wrptp::manual_set_known_delta_tx                                         #
# 18) wrptp::manual_set_known_delta_rx                                         #
# 19) wrptp::manual_set_deltas_known                                           #
# 20) wrptp::manual_set_cal_period                                             #
# 21) wrptp::manual_set_cal_retry                                              #
# 22) wrptp::manual_port_wrptp_enable                                          #
# 23) wrptp::manual_set_wr_present_timeout                                     #
# 24) wrptp::manual_set_wr_m_lock_timeout                                      #
# 25) wrptp::manual_set_wr_s_lock_timeout                                      #
# 26) wrptp::manual_set_wr_locked_timeout                                      #
# 27) wrptp::manual_set_wr_calibration_timeout                                 #
# 28) wrptp::manual_set_wr_calibrated_timeout                                  #
# 29) wrptp::manual_set_wr_resp_calib_req_timeout                              #
# 30) wrptp::manual_set_wr_state_retry                                         #
#                                                                              #
#  DUT DISABLE FUNCTIONS :                                                     #
#                                                                              #
#  1) wrptp::manual_port_disable                                               #
#  2) wrptp::manual_port_ptp_disable                                           #
#  3) wrptp::manual_reset_vlan                                                 #
#  4) wrptp::manual_reset_vlan_priority                                        #
#  5) wrptp::manual_global_ptp_disable                                         #
#  6) wrptp::manual_reset_communication_mode                                   #
#  7) wrptp::manual_reset_domain                                               #
#  8) wrptp::manual_reset_network_protocol                                     #
#  9) wrptp::manual_reset_clock_mode                                           #
# 10) wrptp::manual_reset_clock_step                                           #
# 11) wrptp::manual_reset_delay_mechanism                                      #
# 12) wrptp::manual_reset_priority1                                            #
# 13) wrptp::manual_reset_priority2                                            #
# 14) wrptp::manual_reset_announce_interval                                    #
# 15) wrptp::manual_reset_ipv4_address                                         #
# 16) wrptp::manual_reset_wr_config                                            #
# 17) wrptp::manual_reset_known_delta_tx                                       #
# 18) wrptp::manual_reset_known_delta_rx                                       #
# 19) wrptp::manual_reset_deltas_known                                         #
# 20) wrptp::manual_reset_cal_period                                           #
# 21) wrptp::manual_reset_cal_retry                                            #
# 22) wrptp::manual_port_wrptp_disable                                         #
# 23) wrptp::manual_reset_wr_present_timeout                                   #
# 24) wrptp::manual_reset_wr_m_lock_timeout                                    #
# 25) wrptp::manual_reset_wr_s_lock_timeout                                    #
# 26) wrptp::manual_reset_wr_locked_timeout                                    #
# 27) wrptp::manual_reset_wr_calibration_timeout                               #
# 28) wrptp::manual_reset_wr_calibrated_timeout                                #
# 29) wrptp::manual_reset_wr_resp_calib_req_timeout                            #
# 30) wrptp::manual_reset_wr_state_retry                                       #
#                                                                              #
#  DUT CHECK FUNCTIONS   :                                                     #
#                                                                              #
#  1) wrptp::manual_check_wr_mode                                              #
#  2) wrptp::manual_check_wr_port_state                                        #
#  3) wrptp::manual_check_other_port_delta_tx                                  #
#  4) wrptp::manual_check_other_port_delta_rx                                  #
#  5) wrptp::manual_check_other_port_cal_period                                #
#  6) wrptp::manual_check_other_port_cal_retry                                 #
#  7) wrptp::manual_check_other_port_cal_send_pattern                          #
#                                                                              #
#                                                                              #
################################################################################

################################################################################
#                           MANUAL SET FUNCTIONS                               #
################################################################################

package provide wrptp 1.0

namespace eval wrptp {

    namespace export *
}

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_port_enable                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) Port needs to be made administratively up.  #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If port is made administratively up)      #
#                                                                              #
#                      0 on Failure (If port could not be made                 #
#                                    administratively up)                      #
#                                                                              #
#  DEFINITION        : This function makes the specific port administratively  #
#                      up at the DUT.                                          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::manual_port_enable\                                #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc wrptp::manual_port_enable {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_port_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Enabling port $port_num                                          "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Enable port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "        Assuming that user has enabled port $port_num             "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_port_ptp_enable                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) Port on which ptp is to be enabled.         #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is enabled on the specified port   #
#                                    at the DUT).                              #
#                                                                              #
#                      0 on Failure (If PTP could not be enabled on the        #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function enables PTP on a specific port on the DUT.#
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::manual_port_ptp_enable\                            #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc wrptp::manual_port_ptp_enable {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_port_ptp_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Enabling PTP on port $port_num                                   "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Enable PTP on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has enabled PTP on port $port_num            "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_set_vlan                                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID with which VLAN has to be created.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#  port_num          : (Mandatory) Port which has to be associated to VLAN ID. #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If VLAN ID is created and port is         #
#                                    associated to it on the DUT).             #
#                                                                              #
#                      0 on Failure (If VLAN ID could not be created or port   #
#                                    is not associated to it on the DUT).      #
#                                                                              #
#  DEFINITION        : This function creates VLAN ID and associates given port #
#                      to it on the DUT.                                       #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::manual_set_vlan\                                   #
#                             -vlan_id                  $vlan_id\              #
#                             -port_num                 $port_num              #
#                                                                              #
################################################################################

proc wrptp::manual_set_vlan {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "wrptp::manual_set_vlan : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_set_vlan : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Creating VLAN $vlan_id and associating port $port_num            "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Create VLAN $vlan_id and associating port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has created VLAN $vlan_id and associating    "
        LOG -level 2 -msg "  port $port_num                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_set_vlan_priority                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID for which the priority to be        #
#                                  associated (e.g., 100)                      #
#                                                                              #
#  vlan_priority     : (Mandatory) VLAN priority to be associated              #
#                                  (e.g., 1)                                   #
#                                                                              #
#  port_num          : (Mandatory) Port which has the VLAN ID configured       #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If VLAN Priority is associated)           #
#                                                                              #
#                      0 on Failure (If VLAN ID could not be associated)       #
#                                                                              #
#  DEFINITION        : This function associates the priority to the VLAN ID    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::manual_set_vlan\                                   #
#                             -vlan_id                  $vlan_id\              #
#                             -vlan_priority            $vlan_priority\        #
#                             -port_num                 $port_num              #
#                                                                              #
################################################################################

proc wrptp::manual_set_vlan_priority {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "wrptp::manual_set_vlan_priority : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if {[info exists param(-vlan_priority)]} {
        set vlan_priority $param(-vlan_priority)
    } else {
        ERRLOG -msg "wrptp::manual_set_vlan_priority : Missing Argument    -> vlan_priority"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_set_vlan_priority : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Associating priority $vlan_priority to the VLAN $vlan_id         "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Associate priority $vlan_priority to the VLAN $vlan_id<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has associated $vlan_priority to the VLAN    "
        LOG -level 2 -msg "  $vlan_id                                                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_global_ptp_enable                         #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is enabled on the DUT).            #
#                                                                              #
#                      0 on Failure (If PTP could not be enabled on the DUT).  #
#                                                                              #
#  DEFINITION        : This function enables PTP on the DUT.                   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::manual_port_ptp_enable                             #
#                                                                              #
################################################################################

proc wrptp::manual_global_ptp_enable {args} {

    global env

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Enabling PTP globally                                            "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Enable PTP globally<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has enabled PTP globally                     "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_set_communication_mode                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  ptp_version       : (optional)  PTP version.                                #
#                                  (Default: 2)                                #
#                                                                              #
#  communication_mode : (Mandatory) Communication mode                         #
#                                  (e.g., "Unicast"|"Multicast")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If communication mode is set on the       #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If communication mode could not be        #
#                                    set on the specified port at the DUT).    #
#                                                                              #
#  DEFINITION        : This function sets the communication mode on a specific #
#                      port of the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::manual_set_communication_mode\                     #
#                             -port_num                $port_num\              #
#                             -clock_mode              $clock_mode\            #
#                             -ptp_version             $ptp_version\           #
#                             -communication_mode      $communication_mode     #
#                                                                              #
################################################################################

proc wrptp::manual_set_communication_mode {args} {

    array set param $args
    global env

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
       set port_num $param(-port_num)
    } else {
       ERRLOG -msg "wrptp::manual_set_communication_mode : Missing Argument    -> port_num"
       incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::manual_set_communication_mode : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-communication_mode)]} {
        set communication_mode $param(-communication_mode)
    } else {
        ERRLOG -msg "wrptp::manual_set_communication_mode : Missing Argument  -> communication_mode"
        incr missedArgCounter
    }

    if {[info exists param(-ptp_version)]} {
        set ptp_version $param(-ptp_version)
    } else {
        set ptp_version "2"
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting communication mode on port $port_num                     "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "          PTP Version        : $ptp_version                       "
        LOG -level 2 -msg "          Communication Mode : $communication_mode                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set communication mode on port $port_num<br>\
                            \tClock Mode         : $clock_mode<br>\
                            \tPTP Version        : $ptp_version<br>\
                            \tCommunication Mode : $communication_mode<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has set communication mode on port $port_num "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "          PTP Version        : $ptp_version                       "
        LOG -level 2 -msg "          Communication Mode : $communication_mode                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_set_domain                                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  domain            : (Mandatory) Domain number.                              #
#                                  (e.g., 0..255)                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If domain number is set on the DUT).      #
#                                                                              #
#                      0 on Failure (If domain number could not able to set on #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function sets domain number on the DUT.            #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::manual_set_domain\                                 #
#                             -clock_mode      $clock_mode\                    #
#                             -domain          $domain                         #
#                                                                              #
################################################################################

proc wrptp::manual_set_domain {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::manual_set_domain : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-domain)]} {
        set domain $param(-domain)
    } else {
        ERRLOG -msg "wrptp::manual_set_domain : Missing Argument    -> domain"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting domain number $domain                                    "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "*             Click 'Done'     If Success                         "
        LOG -level 2 -msg "*             Click 'End Test'      If Failed                     "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set domain number $domain<br>\
                            \tClock Mode         : $clock_mode<br><br>\
                            Click 'Done' if Success<br>\
                            Click 'End Test' if Failed"\
                     -type done 

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has set domain number $domain                "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#8.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_set_network_protocol                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) clock_mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  network_protocol  : (Mandatory) Network protocol.                           #
#                                  (e.g., "IEEE 802.3"|"UDP/IPv4")             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If network protocol is set on specified   #
#                                    port at the DUT).                         #
#                                                                              #
#                      0 on Failure (If network protocol could not be set      #
#                                    on specified port at the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets the network protocol on a specific   #
#                      port of the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                     wrptp::manual_set_network_protocol\                      #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -network_protocol $network_protocol              #
#                                                                              #
################################################################################

proc wrptp::manual_set_network_protocol {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_set_network_protocol : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::manual_set_network_protocol : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-network_protocol)]} {
        set network_protocol $param(-network_protocol)
    } else {
        ERRLOG -msg "wrptp::manual_set_network_protocol : Missing Argument    -> network_protocol"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting network protocol $network_protocol on port $port_num     "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set network protocol $network_protocol on port $port_num<br>\
                            \tClock Mode         : $clock_mode<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has set network protocol $network_protocol   "
        LOG -level 2 -msg "  on port $port_num                                               "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#9.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_set_clock_mode                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP clock mode is set on specified port#
#                                    of the DUT).                              #
#                                                                              #
#                      0 on Failure (If PTP clock mode could not be set on     #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function sets clock mode on a specific port of DUT.#
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::manual_set_clock_mode\                             #
#                             -port_num        $port_num\                      #
#                             -clock_mode      $clock_mode                     #
#                                                                              #
################################################################################

proc wrptp::manual_set_clock_mode {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_set_clock_mode : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::manual_set_clock_mode : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting clock mode $clock_mode on port $port_num                 "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set clock mode $clock_mode on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has set clock mode $clock_mode on port $port_num"
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#10.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_set_clock_step                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  clock_step        : (Mandatory) Clock step.                                 #
#                                  (e.g., "One-step"|"Two-step")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If clock-step is set on the specified     #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If clock-step could not be able to set    #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets the clock-step on a specific port on #
#                      the DUT.                                                #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::manual_set_clock_step\                             #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -clock_step       $clock_step                    #
#                                                                              #
################################################################################

proc wrptp::manual_set_clock_step {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_set_clock_step : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::manual_set_clock_step : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-clock_step)]} {
        set clock_step $param(-clock_step)
    } else {
        ERRLOG -msg "wrptp::manual_set_clock_step : Missing Argument    -> clock_step"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting clock step $clock_step on port $port_num                 "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set clock step $clock_step on port $port_num<br>\
                            \tClock Mode         : $clock_mode<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has set clock step $clock_step on port $port_num"
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#11.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_set_delay_mechanism                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  delay_mechanism   : (Mandatory) Delay mechanism.                            #
#                                  (e.g., e2e|p2p|disable)                     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If delay mechanism is set on specified    #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If delay mechanism could not be set on    #
#                                    specified PTP port of the DUT).           #
#                                                                              #
#  DEFINITION        : This function sets the delay mechanism to a specific    #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::manual_set_delay_mechanism\                        #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -delay_mechanism  $delay_mechanism               #
#                                                                              #
################################################################################

proc wrptp::manual_set_delay_mechanism {args} {

    array set param $args
    global env

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_set_delay_mechanism : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::manual_set_delay_mechanism : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-delay_mechanism)]} {
        set delay_mechanism $param(-delay_mechanism)
    } else {
        ERRLOG -msg "wrptp::manual_set_delay_mechanism : Missing Argument    -> delay_mechanism"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting delay mechanism $delay_mechanism on port $port_num       "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set delay mechanism $delay_mechanism on port $port_num<br>\
                            \tClock Mode         : $clock_mode<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has set delay mechanism $delay_mechanism on  "
        LOG -level 2 -msg "  port $port_num                                                  "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#12.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_set_priority1                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority1         : (Mandatory) Prority1                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority1 is set on the DUT).          #
#                                                                              #
#                      0 on Failure (If priority1 could not be able to set on  #
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function sets the priority1 on the DUT.            #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::manual_set_priority1\                              #
#                             -clock_mode      $clock_mode\                    #
#                             -priority1       $priority1                      #
#                                                                              #
################################################################################

proc wrptp::manual_set_priority1 {args} {

    array set param $args
    global env

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::manual_set_priority1 : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-priority1)]} {
        set priority1  $param(-priority1)
    } else {
        ERRLOG -msg "wrptp::manual_set_priority1 : Missing Argument    -> priority1"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting priority1 $priority1                                     "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set priority1 $priority1<br>\
                            \tClock Mode         : $clock_mode<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has set priority1 $priority1                 "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#13.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_set_priority2                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority1         : (Mandatory) Prority2                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority1 is set on the DUT).          #
#                                                                              #
#                      0 on Failure (If priority1 could not be able to set on  #
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function sets the priority1 on the DUT.            #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::manual_set_priority2\                              #
#                             -clock_mode      $clock_mode\                    #
#                             -priority2       $priority2                      #
#                                                                              #
################################################################################

proc wrptp::manual_set_priority2 {args} {

    array set param $args
    global env

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::manual_set_priority2 : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-priority2)]} {
        set priority2  $param(-priority2)
    } else {
        ERRLOG -msg "wrptp::manual_set_priority2 : Missing Argument    -> priority2"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting priority2 $priority2                                     "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set priority1 $priority2<br>\
                            \tClock Mode         : $clock_mode<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has set priority2 $priority2                 "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#14.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_set_announce_interval                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0).                              #
#                                                                              #
#  announce_interval : (Mandatory) announceInterval.                           #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If announceInterval value is set on the   #
#                                    specified port of DUT).                   #
#                                                                              #
#                      0 on Failure (If announceInterval could not be able to  #
#                                    set on specified port of the DUT).        #
#                                                                              #
#  DEFINITION        : This function sets the announceInterval value on the    #
#                      specified port of DUT.                                  #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::manual_set_announce_interval\                    #
#                             -port_num                 $port_num\             #
#                             -announce_interval        $announce_interval     #
#                                                                              #
################################################################################

proc wrptp::manual_set_announce_interval {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_set_announce_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-announce_interval)]} {
        set announce_interval $param(-announce_interval)
    } else {
        ERRLOG -msg "wrptp::manual_set_announce_interval : Missing Argument    -> announce_interval"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting announce_interval $announce_interval on port $port_num   "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set announce_interval $announce_interval<br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Assuming that user has set announce_interval $announce_interval\ "
        LOG -level 2 -msg " on port $port_num                                                "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}

#15.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_set_ipv4_address                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  ip_address        : (Mandatory) IPv4 address.                               #
#                                  (e.g., 192.168.200.1)                       #
#                                                                              #
#  net_mask          : (Mandatory) Subnet mask.                                #
#                                  (e.g., 255.255.255.0)                       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID.                                    #
#                                  (e.g., 100)                                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given IP address is set on specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given IP address could not be set on   #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function sets given IP address to a specific port  #
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::manual_set_ipv4_address\                           #
#                          -port_num              $port_num\                   #
#                          -ip_address            $ip_address\                 #
#                          -net_mask              $net_mask\                   #
#                          -vlan_id               $vlan_id                     #
#                                                                              #
################################################################################

proc wrptp::manual_set_ipv4_address {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_set_ipv4_address : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-ip_address)]} {
        set ip_address $param(-ip_address)
    } else {
        ERRLOG -msg "wrptp::manual_set_ipv4_address : Missing Argument    -> ip_address"
        incr missedArgCounter
    }

    if {[info exists param(-net_mask)]} {
        set net_mask $param(-net_mask)
    } else {
        ERRLOG -msg "wrptp::manual_set_ipv4_address : Missing Argument    -> net_mask"
        incr missedArgCounter
    }

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "wrptp::manual_set_ipv4_address : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }


    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting IP addresses on port $port_num                           "
        LOG -level 2 -msg "          IP Address         : $ip_address                        "
        LOG -level 2 -msg "          Netmask            : $net_mask                          "
        LOG -level 2 -msg "          VLAN ID            : $vlan_id                           "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set IP address on port $port_num<br>\
                            \tIP Address         : $ip_address<br>\
                            \tNetmask            : $net_mask<br>\
                            \tVLAN ID            : $vlan_id<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has set IP address on $port_num              "
        LOG -level 2 -msg "          IP Address         : $ip_address                        "
        LOG -level 2 -msg "          Netmask            : $net_mask                          "
        LOG -level 2 -msg "          VLAN ID            : $vlan_id                           "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#16.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_set_wr_config                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  value             : (Mandatory) Value for wrConfig.                         #
#                                  (e.g., "NON_WR"|"WR_S_ONLY"|"WR_M_ONLY"|    #
#                                         "WR_M_AND_S")                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value for wrConfig is set on     #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given value for wrConfig could not be  #
#                                    set on specifed port of the DUT).         #
#                                                                              #
#  DEFINITION        : This function sets given value for wrConfig to a        #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::manual_set_wr_config\                            #
#                             -port_num                 $port_num\             #
#                             -value                    $value                 #
#                                                                              #
################################################################################

proc wrptp::manual_set_wr_config {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_set_wr_config : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-value)]} {
        set value $param(-value)
    } else {
        ERRLOG -msg "wrptp::manual_set_wr_config : Missing Argument    -> value"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting the value for wrConfig $value on port $port_num          "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set value for wrConfig $value on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "-------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                     "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "  Assuming that user has set value for wrConfig $value on $port_num"
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "-------------------------------------------------------------------"
    }

    return 1
}

#17.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_set_known_delta_tx                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  known_delta_tx    : (Mandatory) Transmission fixed delay                    #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given transmission fixed delay is set  #
#                                    on specified port of the DUT).            #
#                                                                              #
#                      0 on Failure (If given transmission fixed delay could   #
#                                    not be set on specifed port of the DUT).  #
#                                                                              #
#  DEFINITION        : This function sets given transmission fixed delay to a  #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::manual_set_known_delta_tx\                       #
#                             -port_num                 $port_num\             #
#                             -known_delta_tx           $known_delta_tx        #
#                                                                              #
################################################################################

proc wrptp::manual_set_known_delta_tx {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_set_known_delta_tx  : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-known_delta_tx)]} {
        set known_delta_tx $param(-known_delta_tx)
    } else {
        ERRLOG -msg "wrptp::manual_set_known_delta_tx  : Missing Argument    -> known_delta_tx"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

     if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting the transmission fixed delay $known_delta_tx ps on port\ "
        LOG -level 2 -msg " $port_num                                                        "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set value for transmission fixed delay $known_delta_tx ps on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "-------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                     "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "  Assuming that user has set transmission fixed delay\             "
        LOG -level 2 -msg "  $known_delta_tx ps on $port_num                                  "
        LOG -level 2 -msg "-------------------------------------------------------------------"
    }

    return 1
}


#18.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_set_known_delta_rx                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  known_delta_rx    : (Mandatory) Reception fixed delay                       #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given reception fixed delay is set on  #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given reception fixed delay could not  #
#                                    be set on specifed port of the DUT).      #
#                                                                              #
#  DEFINITION        : This function sets given reception fixed delay to a     #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::manual_set_known_delta_rx\                       #
#                             -port_num                 $port_num\             #
#                             -known_delta_rx           $known_delta_rx        #
#                                                                              #
################################################################################

proc wrptp::manual_set_known_delta_rx {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_set_known_delta_rx  : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-known_delta_rx)]} {
        set known_delta_rx $param(-known_delta_rx)
    } else {
        ERRLOG -msg "wrptp::manual_set_known_delta_rx  : Missing Argument    -> known_delta_rx"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

     if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting the transmission fixed delay $known_delta_rx ps on port\ "
        LOG -level 2 -msg " $port_num                                                        "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set value for transmission fixed delay $known_delta_rx ps on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "-------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                     "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "  Assuming that user has set transmission fixed delay\             "
        LOG -level 2 -msg "  $known_delta_rx ps on $port_num                                  "
        LOG -level 2 -msg "-------------------------------------------------------------------"
    }

    return 1
}

#19.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_set_deltas_known                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If fixed delays are known is enabled on   #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If fixed delays are known could not be    #
#                                    enabled on specifed port of the DUT).     #
#                                                                              #
#  DEFINITION        : This function enables fixed delays are known on a       #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::manual_set_deltas_known\                         #
#                             -port_num                 $port_num              #
#                                                                              #
################################################################################

proc wrptp::manual_set_deltas_known {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_set_deltas_known : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

     if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting the fixed delays value on port $port_num                 "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set fixed delays value on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "-------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                     "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "  Assuming that user has set fixed delays value on port $port_num  "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "-------------------------------------------------------------------"
    }

    return 1
}


#20.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_set_cal_period                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  cal_period        : (Mandatory) calPeriod (in microseconds).                #
#                                  (e.g., 3000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given calPeriod is set on specified    #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given calPeriod could not be set on    #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function sets given calPeriod to a specific port on#
#                      the DUT.                                                #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::manual_set_cal_period\                           #
#                             -port_num                 $port_num\             #
#                             -cal_period               $cal_period            #
#                                                                              #
################################################################################

proc wrptp::manual_set_cal_period {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_set_cal_period : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-cal_period)]} {
        set cal_period $param(-cal_period)
    } else {
        ERRLOG -msg "wrptp::manual_set_cal_period : Missing Argument    -> cal_period"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

     if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Setting the calPeriod $cal_period us on port $port_num          "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set calPeriod $cal_period us on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "-------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                     "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "  Assuming that user has set calPeriod $cal_period us\             "
        LOG -level 2 -msg "  on port $port_num                                                "
        LOG -level 2 -msg "-------------------------------------------------------------------"
    }

    return 1
}


#21.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_set_cal_retry                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  cal_retry         : (Mandatory) calRetry.                                   #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given calRetry is set on specified port#
#                                    of the DUT).                              #
#                                                                              #
#                      0 on Failure (If given calRetry could not be set on     #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function sets given calRetry on a specific port on #
#                      the DUT.                                                #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::manual_set_cal_retry\                            #
#                             -port_num                 $port_num\             #
#                             -cal_retry                $cal_retry             #
#                                                                              #
################################################################################

proc wrptp::manual_set_cal_retry {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_set_cal_retry : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-cal_retry)]} {
        set cal_retry $param(-cal_retry)
    } else {
        ERRLOG -msg "wrptp::manual_set_cal_retry : Missing Argument    -> cal_retry"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

     if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Setting the calRetry $cal_retry on port $port_num               "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set calRetry $cal_retry on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "-------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                     "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "  Assuming that user has set calRetry $cal_retry \                 "
        LOG -level 2 -msg "  on port $port_num                                                "
        LOG -level 2 -msg "-------------------------------------------------------------------"
    }

    return 1
}


#22.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_port_wrptp_enable                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) Port on which Wrptp is to be enabled.       #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If Wrptp is enabled on the specified port #
#                                    at the DUT).                              #
#                                                                              #
#                      0 on Failure (If Wrptp could not be enabled on the      #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function enables Wrptp on a specific port on the   #
#                      DUT.                                                    #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::manual_port_wrptp_enable\                          #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc wrptp::manual_port_wrptp_enable {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_port_wrptp_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Enabling Wrptp on port $port_num                                 "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Enable Wrptp on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has enabled Wrptp on port $port_num          "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#23.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_set_wr_present_timeout                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_PRESENT_TIMEOUT (in milliseconds).       #
#                                  (e.g., 1000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_PRESENT_TIMEOUT is set on     #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_PRESENT_TIMEOUT could not be  #
#                                    set on specifed port of the DUT).         #
#                                                                              #
#  DEFINITION        : This function sets given WR_PRESENT_TIMEOUT to a        #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::manual_set_wr_present_timeout\                  #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::manual_set_wr_present_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_set_wr_present_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        ERRLOG -msg "wrptp::manual_set_wr_present_timeout : Missing Argument    -> timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

     if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Setting the WR_PRESENT_TIMEOUT $timeout ms on port $port_num    "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set WR_PRESENT_TIMEOUT $timeout ms on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "-------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                     "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "  Assuming that user has set WR_PRESENT_TIMEOUT $timeout ms on     "
        LOG -level 2 -msg "  port $port_num                                                   "
        LOG -level 2 -msg "-------------------------------------------------------------------"
    }

    return 1
}


#24.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_set_wr_m_lock_timeout                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_M_LOCK_TIMEOUT (in milliseconds).        #
#                                  (e.g., 1000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_M_LOCK_TIMEOUT is set on      #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_M_LOCK_TIMEOUT could not be   #
#                                    set on specifed port of the DUT).         #
#                                                                              #
#  DEFINITION        : This function sets given WR_M_LOCK_TIMEOUT to a         #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::manual_set_wr_m_lock_timeout\                   #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::manual_set_wr_m_lock_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_set_wr_m_lock_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        ERRLOG -msg "wrptp::manual_set_wr_m_lock_timeout : Missing Argument    -> timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Setting the WR_M_LOCK_TIMEOUT $timeout ms on port $port_num     "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set WR_M_LOCK_TIMEOUT $timeout ms on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "-------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                     "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "  Assuming that user has set WR_M_LOCK_TIMEOUT $timeout ms on      "
        LOG -level 2 -msg "  port $port_num                                                   "
        LOG -level 2 -msg "-------------------------------------------------------------------"
    }

    return 1
}


#25.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_set_wr_s_lock_timeout                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_S_LOCK_TIMEOUT (in milliseconds).        #
#                                  (e.g., 1000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_S_LOCK_TIMEOUT is set on      #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_S_LOCK_TIMEOUT could not be   #
#                                    set on specifed port of the DUT).         #
#                                                                              #
#  DEFINITION        : This function sets given WR_S_LOCK_TIMEOUT to a         #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::manual_set_wr_s_lock_timeout\                   #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::manual_set_wr_s_lock_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_set_wr_s_lock_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        ERRLOG -msg "wrptp::manual_set_wr_s_lock_timeout : Missing Argument    -> timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Setting the WR_S_LOCK_TIMEOUT $timeout ms on port $port_num     "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set WR_S_LOCK_TIMEOUT $timeout ms on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "-------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                     "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "  Assuming that user has set WR_S_LOCK_TIMEOUT $timeout ms on      "
        LOG -level 2 -msg "  port $port_num                                                   "
        LOG -level 2 -msg "-------------------------------------------------------------------"
    }

    return 1
}


#26.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_set_wr_locked_timeout                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_LOCKED_TIMEOUT (in milliseconds).        #
#                                  (e.g., 1000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_LOCKED_TIMEOUT is set on      #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_LOCKED_TIMEOUT could not be   #
#                                    set on specifed port of the DUT).         #
#                                                                              #
#  DEFINITION        : This function sets given WR_LOCKED_TIMEOUT to a         #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::manual_set_wr_locked_timeout\                   #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::manual_set_wr_locked_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_set_wr_locked_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        ERRLOG -msg "wrptp::manual_set_wr_locked_timeout : Missing Argument    -> timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Setting the WR_LOCKED_TIMEOUT $timeout ms on port $port_num     "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set WR_LOCKED_TIMEOUT $timeout ms on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "-------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                     "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "  Assuming that user has set WR_LOCKED_TIMEOUT $timeout ms on      "
        LOG -level 2 -msg "  port $port_num                                                   "
        LOG -level 2 -msg "-------------------------------------------------------------------"
    }

    return 1
}


#27.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_set_wr_calibration_timeout                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_CALIBRATION_TIMEOUT (in milliseconds).   #
#                                  (e.g., 1000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_CALIBRATION_TIMEOUT is set on #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_CALIBRATION_TIMEOUT could not #
#                                    be set on specifed port of the DUT).      #
#                                                                              #
#  DEFINITION        : This function sets given WR_CALIBRATION_TIMEOUT to a    #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::manual_set_wr_calibration_timeout\              #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::manual_set_wr_calibration_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_set_wr_calibration_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        ERRLOG -msg "wrptp::manual_set_wr_calibration_timeout : Missing Argument    -> timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Setting the WR_CALIBRATION_TIMEOUT $timeout ms on port $port_num"
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set WR_CALIBRATION_TIMEOUT $timeout ms on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "-------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                     "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "  Assuming that user has set WR_CALIBRATION_TIMEOUT $timeout ms on "
        LOG -level 2 -msg "  port $port_num                                                   "
        LOG -level 2 -msg "-------------------------------------------------------------------"
    }

    return 1
}


#28.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_set_wr_calibrated_timeout                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_CALIBRATED_TIMEOUT (in milliseconds).    #
#                                  (e.g., 1000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_CALIBRATED_TIMEOUT is set on  #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_CALIBRATED_TIMEOUT could not  #
#                                    be set on specifed port of the DUT).      #
#                                                                              #
#  DEFINITION        : This function sets given WR_CALIBRATED_TIMEOUT to a     #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::manual_set_wr_calibrated_timeout\               #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::manual_set_wr_calibrated_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_set_wr_calibrated_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        ERRLOG -msg "wrptp::manual_set_wr_calibrated_timeout : Missing Argument    -> timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Setting the WR_CALIBRATED_TIMEOUT $timeout ms on port $port_num "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set WR_CALIBRATED_TIMEOUT $timeout ms on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "-------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                     "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "  Assuming that user has set WR_CALIBRATED_TIMEOUT $timeout ms on  "
        LOG -level 2 -msg "  port $port_num                                                   "
        LOG -level 2 -msg "-------------------------------------------------------------------"
    }

    return 1
}


#29.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_set_wr_resp_calib_req_timeout             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_RESP_CALIB_REQ_TIMEOUT (in milliseconds).#
#                                  (e.g., 1000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_RESP_CALIB_REQ_TIMEOUT is set #
#                                    on specified port of the DUT).            #
#                                                                              #
#                      0 on Failure (If given WR_RESP_CALIB_REQ_TIMEOUT could  #
#                                    not be set on specifed port of the DUT).  #
#                                                                              #
#  DEFINITION        : This function sets given WR_RESP_CALIB_REQ_TIMEOUT to a #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::manual_set_wr_resp_calib_req_timeout\           #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::manual_set_wr_resp_calib_req_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_set_wr_resp_calib_req_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        ERRLOG -msg "wrptp::manual_set_wr_resp_calib_req_timeout : Missing Argument    -> timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Setting the WR_RESP_CALIB_REQ_TIMEOUT $timeout ms on port $port_num"
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set WR_RESP_CALIB_REQ_TIMEOUT $timeout ms on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "-------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                     "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "  Assuming that user has set WR_RESP_CALIB_REQ_TIMEOUT $timeout ms "
        LOG -level 2 -msg "  on port $port_num                                                "
        LOG -level 2 -msg "-------------------------------------------------------------------"
    }

    return 1
}


#30.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_set_wr_state_retry                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  wr_state_retry    : (Mandatory) WR_STATE_RETRY.                             #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_STATE_RETRY is set on         #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_STATE_RETRY could not be set  #
#                                    on specifed port of the DUT).             #
#                                                                              #
#  DEFINITION        : This function sets given WR_STATE_RETRY to a specific   #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::manual_set_wr_state_retry\                       #
#                             -port_num                 $port_num\             #
#                             -wr_state_retry           $wr_state_retry        #
#                                                                              #
################################################################################

proc wrptp::manual_set_wr_state_retry {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_set_wr_state_retry : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-wr_state_retry)]} {
        set wr_state_retry $param(-wr_state_retry)
    } else {
        ERRLOG -msg "wrptp::manual_set_wr_state_retry : Missing Argument    -> wr_state_retry"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

     if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Setting the WR_STATE_RETRY $wr_state_retry on port\             "
        LOG -level 2 -msg "  $port_num                                                       "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set WR_STATE_RETRY $wr_state_retry on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "-------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                     "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "  Assuming that user has set WR_STATE_RETRY $wr_state_retry\       "
        LOG -level 2 -msg "  on port $port_num                                                "
        LOG -level 2 -msg "-------------------------------------------------------------------"
    }

    return 1
}

#31.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_commit_changes                            #
#                                                                              #
#  INPUT PARAMETERS  : NONE                                                    #
#                                                                              #
#  OUTPUT PARAMETERS : NONE                                                    #
#                                                                              #
#  RETURNS           : 1 on Success                                            #
#                      0 on Failure                                            #
#                                                                              #
#  DEFINITION        : This function commit all changes in the DUT             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::manual_commit_changes\                          #
#                             -session_handler          $session_handler       #
#                                                                              #
################################################################################

proc wrptp::manual_commit_changes {args} {

	global env

	if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

		LOG -level 2 -msg "------------------------------------------------------------------"
		LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
		LOG -level 2 -msg "                                                                  "
		LOG -level 2 -msg "                                                                  "
		LOG -level 2 -msg "  Commiting all DUT changes in dot-config           \             "
		LOG -level 2 -msg "------------------------------------------------------------------"

		PromptString -text "Restart hald and ppsi daemons<br><br>\
						Click 'Done'     to Continue<br>\
						Click 'End Test' to Terminate"\
			-type done

		Echo_Off
		set answer [gets stdin]
		Echo_On

		if [string match -nocase "y" $answer] {
			set result 1
		} else {
			set result 0
		}

		return $result
	} else {

		LOG -level 2 -msg "-------------------------------------------------------------------"
		LOG -level 2 -msg "      Running Under Manual Configuration Setup                     "
		LOG -level 2 -msg "                                                                   "
		LOG -level 2 -msg "  Assuming that user has restarted hald and ppsi daemons           "
		LOG -level 2 -msg "-------------------------------------------------------------------"
	}

	return 1
}

################################################################################
#                       MANUAL DISABLING FUNCTIONS                             #
################################################################################


#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_port_disable                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) Port needs to be made administratively down.#
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If port is made administratively down)    #
#                                                                              #
#                      0 on Failure (If port could not be made                 #
#                                    administratively down)                    #
#                                                                              #
#  DEFINITION        : This function makes the specific port administratively  #
#                      down at the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::manual_port_disable\                               #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc wrptp::manual_port_disable {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_port_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Disabling port $port_num                                         "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Disable port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has disabled port $port_num                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_port_ptp_disable                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) Port on which ptp is to be disabled.        #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is disabled on the specified port  #
#                                    at the DUT).                              #
#                                                                              #
#                      0 on Failure (If PTP could not be disabled on the       #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function disables PTP on a specific port on the DUT#
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::manual_port_ptp_disable\                           #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc wrptp::manual_port_ptp_disable {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_port_ptp_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Disabling PTP on port $port_num                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Disable PTP on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has disabled PTP on port $port_num           "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_reset_vlan                                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID with which VLAN has to be deleted.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#  port_num          : (Mandatory) Port which has to be removed from VLAN ID.  #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If port is removed from VLAN ID and VLAN  #
#                                    ID is deleted from the DUT).              #
#                                                                              #
#                      0 on Failure (If port could not be removed from VLAN ID #
#                                    and VLAN ID is not deledted from the DUT).#
#                                                                              #
#  DEFINITION        : This function removes port from VLAN ID and deletes VLAN#
#                      ID from the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::manual_reset_vlan\                                 #
#                             -vlan_id                  $vlan_id\              #
#                             -port_num                 $port_num              #
#                                                                              #
################################################################################

proc wrptp::manual_reset_vlan {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "wrptp::manual_reset_vlan : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_reset_vlan : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Deleting VLAN $vlan_id and associating port $port_num            "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Delete VLAN $vlan_id and associating port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has deleted VLAN $vlan_id and associating    "
        LOG -level 2 -msg "  port $port_num                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_reset_vlan_priority                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID                                     #
#                                  (e.g., 100)                                 #
#                                                                              #
#  vlan_priority     : (Optional)  VLAN priority                               #
#                                                                              #
#  port_num          : (Mandatory) Port which has to be associated to VLAN ID. #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If VLAN priority is reset on the VLAN ID  #
#                                    on the DUT).                              #
#                                                                              #
#                      0 on Failure (If VLAN priority could not reset on the   #
#                                    VLAN ID on the DUT).                      #
#                                                                              #
#  DEFINITION        : This function resets VLAN priority on the VLAN ID on the#
#                      DUT.                                                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::manual_set_vlan\                                   #
#                             -vlan_id                  $vlan_id\              #
#                             -vlan_priority            $vlan_priority\        #
#                             -port_num                 $port_num              #
#                                                                              #
################################################################################

proc wrptp::manual_set_vlan_priority {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "wrptp::manual_set_vlan_priority : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if {[info exists param(-vlan_priority)]} {
        set vlan_priority $param(-vlan_priority)
    } else {
        ERRLOG -msg "wrptp::manual_set_vlan_priority : Missing Argument    -> vlan_priority"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_set_vlan_priority : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Associating priority $vlan_priority to the VLAN $vlan_id         "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Associate priority $vlan_priority to the VLAN $vlan_id<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has associated $vlan_priority to the VLAN    "
        LOG -level 2 -msg "  $vlan_id                                                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_global_ptp_disable                        #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is disabled on the DUT).           #
#                                                                              #
#                      0 on Failure (If PTP could not be disabled on the DUT). #
#                                                                              #
#  DEFINITION        : This function disables PTP on the DUT.                  #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::manual_port_ptp_disable                            #
#                                                                              #
################################################################################

proc wrptp::manual_global_ptp_disable {} {

    global env

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Disabling PTP globally                                           "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Disable PTP globally<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has disabled PTP globally                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_reset_communication_mode                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  ptp_version       : (optional)  PTP version.                                #
#                                  (Default: 2)                                #
#                                                                              #
#  communication_mode : (Mandatory) Communication mode                         #
#                                  (e.g., "Unicast"|"Multicast")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If communication mode is reset on the     #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If communication mode could not be        #
#                                    reset on the specified port at the DUT).  #
#                                                                              #
#  DEFINITION        : This function resets the communication mode on a        #
#                      specific port of the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::manual_reset_communication_mode\                   #
#                             -port_num                $port_num\              #
#                             -clock_mode              $clock_mode\            #
#                             -ptp_version             $ptp_version\           #
#                             -communication_mode      $communication_mode     #
#                                                                              #
################################################################################

proc wrptp::manual_reset_communication_mode {args} {

    array set param $args
    global env

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
       set port_num $param(-port_num)
    } else {
       ERRLOG -msg "wrptp::manual_reset_communication_mode : Missing Argument    -> port_num"
       incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::manual_reset_communication_mode : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-communication_mode)]} {
        set communication_mode $param(-communication_mode)
    } else {
        ERRLOG -msg "wrptp::manual_reset_communication_mode : Missing Argument  -> communication_mode"
        incr missedArgCounter
    }

    if {[info exists param(-ptp_version)]} {
        set ptp_version $param(-ptp_version)
    } else {
        set ptp_version "2"
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting communication mode on port $port_num                   "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "          PTP Version        : $ptp_version                       "
        LOG -level 2 -msg "          Communication Mode : $communication_mode                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset communication mode on port $port_num<br>\
                            \tClock Mode         : $clock_mode<br>\
                            \tPTP Version        : $ptp_version<br>\
                            \tCommunication Mode : $communication_mode<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset communication mode on port $port_num"
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "          PTP Version        : $ptp_version                       "
        LOG -level 2 -msg "          Communication Mode : $communication_mode                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_reset_domain                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  domain            : (Mandatory) Domain number.                              #
#                                  (e.g., 0..255)                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If domain number is reset on the DUT).    #
#                                                                              #
#                      0 on Failure (If domain number could not able to reset  #
#                                    on DUT).                                  #
#                                                                              #
#  DEFINITION        : This function resets domain number on the DUT.          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::manual_reset_domain\                               #
#                             -clock_mode      $clock_mode\                    #
#                             -domain          $domain                         #
#                                                                              #
################################################################################

proc wrptp::manual_reset_domain {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::manual_reset_domain : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-domain)]} {
        set domain $param(-domain)
    } else {
        ERRLOG -msg "wrptp::manual_reset_domain : Missing Argument    -> domain"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting domain number $domain                                  "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset domain number $domain<br>\
                            \tClock Mode         : $clock_mode<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset domain number $domain              "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#8.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_reset_network_protocol                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) clock_mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  network_protocol  : (Mandatory) Network protocol.                           #
#                                  (e.g., "IEEE 802.3"|"UDP/IPv4")             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If network protocol is reset on specified #
#                                    port at the DUT).                         #
#                                                                              #
#                      0 on Failure (If network protocol could not be reset    #
#                                    on specified port at the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets the network protocol on a specific #
#                      port of the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                     wrptp::manual_reset_network_protocol\                    #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -network_protocol $network_protocol              #
#                                                                              #
################################################################################

proc wrptp::manual_reset_network_protocol {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_reset_network_protocol : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::manual_reset_network_protocol : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-network_protocol)]} {
        set network_protocol $param(-network_protocol)
    } else {
        ERRLOG -msg "wrptp::manual_reset_network_protocol : Missing Argument    -> network_protocol"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting network protocol $network_protocol on port $port_num   "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset network protocol $network_protocol on port $port_num<br>\
                            \tClock Mode         : $clock_mode<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset network protocol $network_protocol "
        LOG -level 2 -msg "  on port $port_num                                               "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#9.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_reset_clock_mode                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP clock mode is reset on specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If PTP clock mode could not be reset on   #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function resets clock mode on a specific port of   #
#                      DUT.                                                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::manual_reset_clock_mode\                           #
#                             -port_num        $port_num\                      #
#                             -clock_mode      $clock_mode                     #
#                                                                              #
################################################################################

proc wrptp::manual_reset_clock_mode {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_reset_clock_mode : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::manual_reset_clock_mode : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting clock mode $clock_mode on port $port_num               "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset clock mode $clock_mode on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset clock mode $clock_mode on port $port_num"
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#10.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_reset_clock_step                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  clock_step        : (Mandatory) Clock step.                                 #
#                                  (e.g., "One-step"|"Two-step")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If clock-step is reset on the specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If clock-step could not be able to reset  #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets the clock-step on a specific port  #
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::manual_reset_clock_step\                           #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -clock_step       $clock_step                    #
#                                                                              #
################################################################################

proc wrptp::manual_reset_clock_step {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_reset_clock_step : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::manual_reset_clock_step : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-clock_step)]} {
        set clock_step $param(-clock_step)
    } else {
        ERRLOG -msg "wrptp::manual_reset_clock_step : Missing Argument    -> clock_step"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting clock step $clock_step on port $port_num               "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset clock step $clock_step on port $port_num<br>\
                            \tClock Mode         : $clock_mode<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset clock step $clock_step on port $port_num"
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#11.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_reset_delay_mechanism                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  delay_mechanism   : (Mandatory) Delay mechanism.                            #
#                                  (e.g., e2e|p2p|disable)                     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If delay mechanism is reset on specified  #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If delay mechanism could not be reset on  #
#                                    specified PTP port of the DUT).           #
#                                                                              #
#  DEFINITION        : This function resets the delay mechanism to a specific  #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::manual_reset_delay_mechanism\                      #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -delay_mechanism  $delay_mechanism               #
#                                                                              #
################################################################################

proc wrptp::manual_reset_delay_mechanism {args} {

    array set param $args
    global env

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_reset_delay_mechanism : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::manual_reset_delay_mechanism : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-delay_mechanism)]} {
        set delay_mechanism $param(-delay_mechanism)
    } else {
        ERRLOG -msg "wrptp::manual_reset_delay_mechanism : Missing Argument    -> delay_mechanism"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting delay mechanism $delay_mechanism on port $port_num     "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset delay mechanism $delay_mechanism on port $port_num<br>\
                            \tClock Mode         : $clock_mode<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset delay mechanism $delay_mechanism on"
        LOG -level 2 -msg "  port $port_num                                                  "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#12.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_reset_priority1                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority1         : (Mandatory) Prority1                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority1 is reset on the DUT).        #
#                                                                              #
#                      0 on Failure (If priority1 could not be able to reset on#
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function resets the priority1 on the DUT.          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::manual_reset_priority1\                            #
#                             -clock_mode      $clock_mode\                    #
#                             -priority1       $priority1                      #
#                                                                              #
################################################################################

proc wrptp::manual_reset_priority1 {args} {

    array set param $args
    global env

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::manual_reset_priority1 : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-priority1)]} {
        set priority1  $param(-priority1)
    } else {
        ERRLOG -msg "wrptp::manual_reset_priority1 : Missing Argument    -> priority1"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting priority1 $priority1                                   "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset priority1 $priority1<br>\
                            \tClock Mode         : $clock_mode<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset priority1 $priority1               "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#13.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_reset_priority2                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority2         : (Mandatory) Prority2                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority2 is reset on the DUT).        #
#                                                                              #
#                      0 on Failure (If priority2 could not be able to reset on#
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function resets the priority2 on the DUT.          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::manual_reset_priority2\                            #
#                             -clock_mode      $clock_mode\                    #
#                             -priority2       $priority2                      #
#                                                                              #
################################################################################

proc wrptp::manual_reset_priority2 {args} {

    array set param $args
    global env

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::manual_reset_priority2 : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-priority2)]} {
        set priority2 $param(-priority2)
    } else {
        ERRLOG -msg "wrptp::manual_reset_priority2 : Missing Argument    -> priority2"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting priority2 $priority2                                   "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset priority2 $priority2<br>\
                            \tClock Mode         : $clock_mode<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset priority2 $priority2               "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#14.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_reset_announce_interval                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0).                              #
#                                                                              #
#  announce_interval : (Mandatory) announceInterval.                           #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If announceInterval value is reset on the #
#                                    specified port of DUT).                   #
#                                                                              #
#                      0 on Failure (If announceInterval could not be able to  #
#                                    reset on specified port of the DUT).      #
#                                                                              #
#  DEFINITION        : This function resets the announceInterval value on the  #
#                      specified port of DUT.                                  #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::manual_reset_announce_interval\                  #
#                             -port_num                 $port_num\             #
#                             -announce_interval        $announce_interval     #
#                                                                              #
################################################################################

proc wrptp::manual_reset_announce_interval {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_reset_announce_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-announce_interval)]} {
        set announce_interval $param(-announce_interval)
    } else {
        ERRLOG -msg "wrptp::manual_reset_announce_interval : Missing Argument    -> announce_interval"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting announce_interval $announce_interval on port $port_num   "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set announce_interval $announce_interval<br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Assuming that user has reset announce_interval                   "
        LOG -level 2 -msg " $announce_interval\ on port $port_num                            "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#15.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_reset_ipv4_address                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  ip_address        : (Mandatory) IPv4 address.                               #
#                                  (e.g., 192.168.200.1)                       #
#                                                                              #
#  net_mask          : (Mandatory) Subnet mask.                                #
#                                  (e.g., 255.255.255.0)                       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID.                                    #
#                                  (e.g., 100)                                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given IP address is reset on specified #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given IP address could not be reset on #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function resets given IP address to a specific port#
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::manual_reset_ipv4_address\                         #
#                          -port_num              $port_num\                   #
#                          -ip_address            $ip_address\                 #
#                          -net_mask              $net_mask\                   #
#                          -vlan_id               $vlan_id                     #
#                                                                              #
################################################################################

proc wrptp::manual_reset_ipv4_address {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_reset_ipv4_address : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-ip_address)]} {
        set ip_address $param(-ip_address)
    } else {
        ERRLOG -msg "wrptp::manual_reset_ipv4_address : Missing Argument    -> ip_address"
        incr missedArgCounter
    }

    if {[info exists param(-net_mask)]} {
        set net_mask $param(-net_mask)
    } else {
        ERRLOG -msg "wrptp::manual_reset_ipv4_address : Missing Argument    -> net_mask"
        incr missedArgCounter
    }

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "wrptp::manual_reset_ipv4_address : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting IP addresses on port $port_num                         "
        LOG -level 2 -msg "          IP Address         : $ip_address                        "
        LOG -level 2 -msg "          Netmask            : $net_mask                          "
        LOG -level 2 -msg "          VLAN ID            : $vlan_id                           "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset IP address on port $port_num<br>\
                            \tIP Address         : $ip_address<br>\
                            \tNetmask            : $net_mask<br>\
                            \tVLAN ID            : $vlan_id<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset IP address on $port_num            "
        LOG -level 2 -msg "          IP Address         : $ip_address                        "
        LOG -level 2 -msg "          Netmask            : $net_mask                          "
        LOG -level 2 -msg "          VLAN ID            : $vlan_id                           "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#16.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_reset_wr_config                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  value             : (Mandatory) Value for wrConfig.                         #
#                                  (e.g., "NON_WR"|"WR_S_ONLY"|"WR_M_ONLY"|    #
#                                         "WR_M_AND_S")                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value for wrConfig is reset on   #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given value for wrConfig could not be  #
#                                    reset on specifed port of the DUT).       #
#                                                                              #
#  DEFINITION        : This function resets given value for wrConfig to a      #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::manual_reset_wr_config\                          #
#                             -port_num                 $port_num\             #
#                             -value                    $value                 #
#                                                                              #
################################################################################

proc wrptp::manual_reset_wr_config {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_reset_wr_config : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-value)]} {
        set value $param(-value)
    } else {
        ERRLOG -msg "wrptp::manual_reset_wr_config : Missing Argument    -> value"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting value for wrConfig $value on port $port_num            "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset value for wrConfig $value on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset value for wrConfig $value          "
        LOG -level 2 -msg "  on port $port_num                                               "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#17.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_reset_known_delta_tx                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  known_delta_tx    : (Mandatory) Transmission fixed delay                    #
#                                  (default: 0)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given transmission fixed delay is reset#
#                                    on specified port of the DUT).            #
#                                                                              #
#                      0 on Failure (If given transmission fixed delay could   #
#                                    not be reset on specifed port of the DUT).#
#                                                                              #
#  DEFINITION        : This function resets given transmission fixed delay to a#
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::manual_reset_known_delta_tx\                     #
#                             -port_num                 $port_num\             #
#                             -known_delta_tx           $known_delta_tx        #
#                                                                              #
################################################################################

proc wrptp::manual_reset_known_delta_tx {args} {

    array set param $args
    global env 

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_reset_known_delta_tx  : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-known_delta_tx)]} {
        set known_delta_tx $param(-known_delta_tx)
    } else {
        ERRLOG -msg "wrptp::manual_reset_known_delta_tx  : Missing Argument    -> known_delta_tx"
        incr missedArgCounter
    }

    

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting transmission fixed delay $known_delta_tx ps on port\   "
        LOG -level 2 -msg " $port_num                                                        "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset transmission fixed delay $known_delta_tx ps on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset transmission fixed delay           "
        LOG -level 2 -msg "  $known_delta_tx ps on port $port_num                            "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}

#18.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_reset_known_delta_rx                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  known_delta_rx    : (Mandatory) Reception fixed delay                       #
#                                  (default: 0)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given reception fixed delay is reset on#
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given reception fixed delay could not  #
#                                    be reset on specifed port of the DUT).    #
#                                                                              #
#  DEFINITION        : This function resets given reception fixed delay to a   #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::manual_reset_known_delta_rx\                     #
#                             -port_num                 $port_num\             #
#                             -known_delta_rx           $known_delta_rx        #
#                                                                              #
################################################################################

proc wrptp::manual_reset_known_delta_rx {args} {

    array set param $args
    global env 

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_reset_known_delta_rx  : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-known_delta_rx)]} {
        set known_delta_rx $param(-known_delta_rx)
    } else {
        ERRLOG -msg "wrptp::manual_reset_known_delta_rx  : Missing Argument    -> known_delta_rx"
        incr missedArgCounter
    }


    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting reception fixed delay $known_delta_rx ps on port\      "
        LOG -level 2 -msg " $port_num                                                        "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset reception fixed delay $known_delta_rx ps on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset reception fixed delay              "
        LOG -level 2 -msg "  $known_delta_rx ps on port $port_num                            "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#19.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_reset_deltas_known                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If fixed delays are known is disabled on  #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If fixed delays are known could not be    #
#                                    disabled on specifed port of the DUT).    #
#                                                                              #
#  DEFINITION        : This function disables fixed delays are known on a      #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::manual_reset_deltas_known\                       #
#                             -port_num                 $port_num              #
#                                                                              #
################################################################################

proc wrptp::manual_reset_deltas_known {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_reset_deltas_known : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting fixed delays on port $port_num                         "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset fixed delays on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset fixed delays on $port_num          "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#20.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_reset_cal_period                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  cal_period        : (Mandatory) calPeriod (in microseconds).                #
#                                  (default: 3000)                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given calPeriod is reset on specified  #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given calPeriod could not be reset on  #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function resets given calPeriod to a specific port #
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::manual_reset_cal_period\                         #
#                             -port_num                 $port_num\             #
#                             -cal_period               $cal_period            #
#                                                                              #
################################################################################

proc wrptp::manual_reset_cal_period {args} {

    array set param $args
    global env 

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_reset_cal_period : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-cal_period)]} {
        set cal_period $param(-cal_period)
    } else {
        ERRLOG -msg "wrptp::manual_reset_cal_period : Missing Argument    -> cal_period"
        incr missedArgCounter
    }


    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting calPeriod $cal_period us on port $port_num             "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset calPeriod $cal_period us on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset calPeriod $cal_period us on port   "
        LOG -level 2 -msg "   $port_num                                                      "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#21.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_reset_cal_retry                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  cal_retry         : (Mandatory) calRetry.                                   #
#                                  (default: 3)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given calRetry is reset on specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given calRetry could not be reset on   #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function resets given calRetry on a specific port  #
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::manual_reset_cal_retry\                          #
#                             -port_num                 $port_num\             #
#                             -cal_retry                $cal_retry             #
#                                                                              #
################################################################################

proc wrptp::manual_reset_cal_retry {args} {

    array set param $args
    global env 

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_reset_cal_retry : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-cal_retry)]} {
        set cal_retry $param(-cal_retry)
    } else {
        ERRLOG -msg "wrptp::manual_reset_cal_retry : Missing Argument    -> cal_retry"
        incr missedArgCounter
    }


    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting calRetry $cal_retry on port $port_num                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset calRetry $cal_retry on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset calRetry $cal_retry on port        "
        LOG -level 2 -msg "   $port_num                                                      "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#22.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_port_wrptp_disable                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) Port on which Wrptp is to be disabled.      #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If Wrptp is disabled on the specified port#
#                                    at the DUT).                              #
#                                                                              #
#                      0 on Failure (If Wrptp could not be disabled on the     #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function disables Wrptp on a specific port on the  #
#                      DUT.                                                    #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::manual_port_wrptp_disable\                         #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc wrptp::manual_port_wrptp_disable {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_port_wrptp_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Disabling Wrptp on port $port_num                                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Disable Wrptp on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has disabled Wrptp on port $port_num         "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#23.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_reset_wr_present_timeout                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Optional)  WR_PRESENT_TIMEOUT (in milliseconds).       #
#                                  (Default: 1000)                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_PRESENT_TIMEOUT is reset on   #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_PRESENT_TIMEOUT could not be  #
#                                    reset on specifed port of the DUT).       #
#                                                                              #
#  DEFINITION        : This function resets given WR_PRESENT_TIMEOUT to a      #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::manual_reset_wr_present_timeout\                #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::manual_reset_wr_present_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_reset_wr_present_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        set timeout $::wrptp_present_timeout
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting WR_PRESENT_TIMEOUT $timeout ms on port $port_num       "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset WR_PRESENT_TIMEOUT $timeout ms on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset WR_PRESENT_TIMEOUT $timeout ms on  "
        LOG -level 2 -msg "  port $port_num                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#24.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_reset_wr_m_lock_timeout                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Optional)  WR_M_LOCK_TIMEOUT (in milliseconds).        #
#                                  (Default: 15000)                            #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_M_LOCK_TIMEOUT is reset on    #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_M_LOCK_TIMEOUT could not be   #
#                                    reset on specifed port of the DUT).       #
#                                                                              #
#  DEFINITION        : This function resets given WR_M_LOCK_TIMEOUT to a       #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::manual_reset_wr_m_lock_timeout\                 #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::manual_reset_wr_m_lock_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_reset_wr_m_lock_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        set timeout $::wrptp_m_lock_timeout
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting WR_M_LOCK_TIMEOUT $timeout ms on port $port_num        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset WR_M_LOCK_TIMEOUT $timeout ms on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset WR_M_LOCK_TIMEOUT $timeout ms on   "
        LOG -level 2 -msg "  port $port_num                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#25.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_reset_wr_s_lock_timeout                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Optional)  WR_S_LOCK_TIMEOUT (in milliseconds).        #
#                                  (Default: 15000)                            #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_S_LOCK_TIMEOUT is reset on    #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_S_LOCK_TIMEOUT could not be   #
#                                    reset on specifed port of the DUT).       #
#                                                                              #
#  DEFINITION        : This function resets given WR_S_LOCK_TIMEOUT to a       #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::manual_reset_wr_s_lock_timeout\                 #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::manual_reset_wr_s_lock_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_reset_wr_s_lock_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        set timeout $::wrptp_s_lock_timeout
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting WR_S_LOCK_TIMEOUT $timeout ms on port $port_num        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset WR_S_LOCK_TIMEOUT $timeout ms on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset WR_S_LOCK_TIMEOUT $timeout ms on   "
        LOG -level 2 -msg "  port $port_num                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#26.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_reset_wr_locked_timeout                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Optional)  WR_LOCKED_TIMEOUT (in milliseconds).        #
#                                  (Default: 300)                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_LOCKED_TIMEOUT is reset on    #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_LOCKED_TIMEOUT could not be   #
#                                    reset on specifed port of the DUT).       #
#                                                                              #
#  DEFINITION        : This function resets given WR_LOCKED_TIMEOUT to a       #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::manual_reset_wr_locked_timeout\                 #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::manual_reset_wr_locked_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_reset_wr_locked_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        set timeout $::wrptp_locked_timeout
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting WR_LOCKED_TIMEOUT $timeout ms on port $port_num        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset WR_LOCKED_TIMEOUT $timeout ms on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset WR_LOCKED_TIMEOUT $timeout ms on   "
        LOG -level 2 -msg "  port $port_num                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#27.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_reset_wr_calibration_timeout              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Optional)  WR_CALIBRATION_TIMEOUT (in milliseconds).   #
#                                  (Default: 3000)                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_CALIBRATION_TIMEOUT is reset  #
#                                    on specified port of the DUT).            #
#                                                                              #
#                      0 on Failure (If given WR_CALIBRATION_TIMEOUT could not #
#                                    be reset on specifed port of the DUT).    #
#                                                                              #
#  DEFINITION        : This function resets given WR_CALIBRATION_TIMEOUT to a  #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::manual_reset_wr_calibration_timeout\            #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::manual_reset_wr_calibration_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_reset_wr_calibration_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        set timeout $::wrptp_calibration_timeout
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting WR_CALIBRATION_TIMEOUT $timeout ms on port $port_num   "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset WR_CALIBRATION_TIMEOUT $timeout ms on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset WR_CALIBRATION_TIMEOUT $timeout ms "
        LOG -level 2 -msg "  on port $port_num                                               "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#28.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_reset_wr_calibrated_timeout               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Optional)  WR_CALIBRATED_TIMEOUT (in milliseconds).    #
#                                  (Default: 300)                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_CALIBRATED_TIMEOUT is reset on#
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_CALIBRATED_TIMEOUT could not  #
#                                    be reset on specifed port of the DUT).    #
#                                                                              #
#  DEFINITION        : This function resets given WR_CALIBRATED_TIMEOUT to a   #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::manual_reset_wr_calibrated_timeout\             #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::manual_reset_wr_calibrated_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_reset_wr_calibrated_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        set timeout $::wrptp_calibrated_timeout
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting WR_CALIBRATED_TIMEOUT $timeout ms on port $port_num    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset WR_CALIBRATED_TIMEOUT $timeout ms on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset WR_CALIBRATED_TIMEOUT $timeout ms  "
        LOG -level 2 -msg "  on port $port_num                                               "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#29.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_reset_wr_resp_calib_req_timeout           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Optional)  WR_RESP_CALIB_REQ_TIMEOUT (in milliseconds).#
#                                  (Default: 3)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_RESP_CALIB_REQ_TIMEOUT is     #
#                                    reset on specified port of the DUT).      #
#                                                                              #
#                      0 on Failure (If given WR_RESP_CALIB_REQ_TIMEOUT could  #
#                                    not be reset on specifed port of the DUT).#
#                                                                              #
#  DEFINITION        : This function resets given WR_RESP_CALIB_REQ_TIMEOUT to #
#                      a specific port on the DUT.                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::manual_reset_wr_resp_calib_req_timeout\         #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::manual_reset_wr_resp_calib_req_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_reset_wr_resp_calib_req_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        set timeout $::wrptp_resp_calib_req_timeout
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting WR_RESP_CALIB_REQ_TIMEOUT $timeout ms on port $port_num"
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset WR_RESP_CALIB_REQ_TIMEOUT $timeout ms on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset WR_RESP_CALIB_REQ_TIMEOUT $timeout ms"
        LOG -level 2 -msg "  on port $port_num                                               "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#30.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_reset_wr_state_retry                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  wr_state_retry    : (Mandatory) WR_STATE_RETRY.                             #
#                                  (default: 3)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_STATE_RETRY is reset on       #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_STATE_RETRY could not be reset#
#                                    on specifed port of the DUT).             #
#                                                                              #
#  DEFINITION        : This function resets given WR_STATE_RETRY to a specific #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::manual_reset_wr_state_retry\                     #
#                             -port_num                 $port_num\             #
#                             -wr_state_retry           $wr_state_retry        #
#                                                                              #
################################################################################

proc wrptp::manual_reset_wr_state_retry {args} {
    
    array set param $args
    global env 

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_reset_wr_state_retry : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-wr_state_retry)]} {
        set wr_state_retry $param(-wr_state_retry)
    } else {
        ERRLOG -msg "wrptp::manual_reset_wr_state_retry : Missing Argument    -> wr_state_retry"
        incr missedArgCounter
    }
       
 
    if { $missedArgCounter != 0 } {
        return 0
    }
 
   if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting WR_STATE_RETRY $wr_state_retry on port $port_num       "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset WR_STATE_RETRY $wr_state_retry on port $port_num<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset WR_STATE_RETRY $wr_state_retry     "
        LOG -level 2 -msg "  on $port_num                                                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}



################################################################################
#                           MANUAL CHECK FUNCTIONS                             #
################################################################################

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_check_wr_config                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  value             : (Mandatory) Value for wrConfig.                         #
#                                  (e.g., "NON_WR"|"WR_S_ONLY"|"WR_M_ONLY"|    #
#                                         "WR_M_AND_S")                        #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given state of wrConfig is verified for#
#                                    the specified port).                      #
#                                                                              #
#                      0 on Failure (If given state of wrConfig could not be   #
#                                    verified for the specified port).         #
#                                                                              #
#  DEFINITION        : This function verifies the given state of               #
#                       for a wrConfig on the specified port.                  #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::manual_check_wr_config\                          #
#                             -port_num           $port_num\                   #
#                             -wr_mode            $wr_mode                     #
#                                                                              #
################################################################################

proc wrptp::manual_check_wr_config {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_check_wr_config : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-value)]} {
        set value $param(-value)
    } else {
        ERRLOG -msg "wrptp::manual_check_wr_config : Missing Argument    -> value"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*           Running Under Manual Configuration Setup            "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Checking value of wrConfig ($value) for port $port_num       "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*             Click 'Yes'     If Present                        "
        LOG -level 2 -msg  "*             Click 'No'      If Absent                         "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "****************************************************************"

        PromptString -text "Check wrMode ($value) for port $port_num<br><br>\
                                Click 'Yes'     If Present<br>\
                                Click 'No'      If Absent"\
                     -type  yesno

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*          Running Under Manual Configuration Setup             "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Assuming user has successfully found state of                "
        LOG -level 2 -msg  "*  wrMode ($value) for port $port_num                           "
        LOG -level 2 -msg  "****************************************************************"
    }

    return 1
}

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_check_ptp_port_state                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  state             : (Mandatory) portState                                   #
#                                  (e.g., "MASTER"|"SLAVE")                    #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given portState is verified for the    #
#                                         "MASTER"|"SLAVE" on specified port). #
#                                                                              #
#                      0 on Failure (If given portState could not be verified  #
#                                    for the specified port).                  #
#                                                                              #
#  DEFINITION        : This function verifies the given portState on the       #
#                      specified port.                                         #
#  USAGE             :                                                         #
#                                                                              #
#                        wrptp::manual_check_ptp_port_state\                   #
#                               -port_num           $port_num\                 #
#                               -state              $state                     #
#                                                                              #
################################################################################

proc wrptp::manual_check_ptp_port_state {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_check_ptp_port_state : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-state)]} {
        set state $param(-state)
    } else {
        ERRLOG -msg "wrptp::manual_check_ptp_port_state : Missing Argument    -> state"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*           Running Under Manual Configuration Setup            "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Checking value of PTP portState ($state) for port $port_num  "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*             Click 'Yes'     If Present                        "
        LOG -level 2 -msg  "*             Click 'No'      If Absent                         "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "****************************************************************"

        PromptString -text "Check PTP portState ($state) for port $port_num<br><br>\
                                Click 'Yes'     If Present<br>\
                                Click 'No'      If Absent"\
                     -type  yesno

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*          Running Under Manual Configuration Setup             "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Assuming user has successfully found state of                "
        LOG -level 2 -msg  "   PTP portState ($state) for port $port_num                    "
        LOG -level 2 -msg  "****************************************************************"
    }

    return 1
}



#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_check_wr_port_state                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  wr_port_state     : (Mandatory) wrPortState                                 #
#                                  (e.g., "IDLE"|"PRESENT"|"M_LOCK"|"S_LOCK"|  #
#                                         "LOCKED"|"CALIBRATION"|"CALIBRATED"| #
#                                         "RESP_CALIB_REQ"|"WR_LINK_ON")       #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given wrPortState is verified for the  #
#                                    specified port).                          #
#                                                                              #
#                      0 on Failure (If given wrPortState could not be verified#
#                                    for the specified port).                  #
#                                                                              #
#  DEFINITION        : This function verifies the given wrPortState on the     #
#                      specified port.                                         #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                        wrptp::manual_check_wr_port_state\                    #
#                               -port_num           $port_num\                 #
#                               -wr_port_state      $wr_port_state             #
#                                                                              #
################################################################################

proc wrptp::manual_check_wr_port_state {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_check_wr_port_state : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-wr_port_state)]} {
        set wr_port_state $param(-wr_port_state)
    } else {
        ERRLOG -msg "wrptp::manual_check_wr_port_state : Missing Argument    -> wr_port_state"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*           Running Under Manual Configuration Setup            "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Checking value of wrPortState ($wr_port_state) for port      "
        LOG -level 2 -msg  "*  $port_num                                                    "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*             Click 'Yes'     If Present                        "
        LOG -level 2 -msg  "*             Click 'No'      If Absent                         "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "****************************************************************"

        PromptString -text "Check wrPortstate ($wr_port_state) for port $port_num<br><br>\
                                Click 'Yes'     If Present<br>\
                                Click 'No'      If Absent"\
                     -type  yesno

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*          Running Under Manual Configuration Setup             "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Assuming user has successfully found state of                "
        LOG -level 2 -msg  "   wrPortState ($wr_port_state) for port $port_num              "
        LOG -level 2 -msg  "****************************************************************"
    }

    return 1
}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_check_other_port_delta_tx                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num                : (Mandatory) DUT's port number.                    #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  other_port_delta_tx     : (Mandatory) Value of deltaTx                      #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of Delta Tx is verified for#
#                                    the specified port).                      #
#                                                                              #
#                      0 on Failure (If given value of Delta Tx could not be   #
#                                    verified for the specified port).         #
#                                                                              #
#  DEFINITION        : This function verifies the value of Delta Tx on the     #
#                      specified port.                                         #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::manual_check_other_port_delta_tx\                #
#                              -port_num               $port_num\              #
#                              -other_port_delta_tx    $other_port_delta_tx    #
#                                                                              #
################################################################################

proc wrptp::manual_check_other_port_delta_tx {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_check_other_port_delta_tx : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-other_port_delta_tx)]} {
        set other_port_delta_tx $param(-other_port_delta_tx)
    } else {
        ERRLOG -msg "wrptp::manual_check_other_port_delta_tx : Missing Argument    -> other_port_delta_tx"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*           Running Under Manual Configuration Setup            "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Checking value of otherPortDeltaTx ($other_port_delta_tx ps) "
        LOG -level 2 -msg  "*  for port $port_num                                           "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*             Click 'Yes'     If Present                        "
        LOG -level 2 -msg  "*             Click 'No'      If Absent                         "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "****************************************************************"

        PromptString -text "Check value of otherPortDeltaTx ($other_port_delta_tx ps) for port $port_num<br><br>\
                                Click 'Yes'     If Present<br>\
                                Click 'No'      If Absent"\
                     -type  yesno

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*          Running Under Manual Configuration Setup             "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Assuming user has successfully found state of                "
        LOG -level 2 -msg  "*  value of otherPortDeltaTx ($other_port_delta_tx ps) for port $port_num"
        LOG -level 2 -msg  "****************************************************************"
    }

    return 1
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_check_other_port_delta_rx                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num                : (Mandatory) DUT's port number.                    #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  other_port_delta_rx     : (Mandatory) Value of deltaRx                      #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of deltaRx is verified for #
#                                    the specified port).                      #
#                                                                              #
#                      0 on Failure (If given  value of deltaRx could not be   #
#                                    verified for the specified port).         #
#                                                                              #
#  DEFINITION        : This function verifies value of DeltaRx on the specified#
#                      port.                                                   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::manual_check_other_port_delta_rx\               #
#                              -port_num                $port_num\             #
#                              -other_port_delta_rx     $other_port_delta_rx   #
#                                                                              #
################################################################################

proc wrptp::manual_check_other_port_delta_rx {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_check_other_port_delta_rx : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-other_port_delta_rx)]} {
        set other_port_delta_rx $param(-other_port_delta_rx)
    } else {
        ERRLOG -msg "wrptp::manual_check_other_port_delta_rx : Missing Argument    -> other_port_delta_rx"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*           Running Under Manual Configuration Setup            "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Checking value of otherPortDeltaRx ($other_port_delta_rx ps) "
        LOG -level 2 -msg  "*  for port $port_num                                           "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*             Click 'Yes'     If Present                        "
        LOG -level 2 -msg  "*             Click 'No'      If Absent                         "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "****************************************************************"

        PromptString -text "Check value of otherPortDeltaRx ($other_port_delta_rx ps) for port $port_num<br><br>\
                                Click 'Yes'     If Present<br>\
                                Click 'No'      If Absent"\
                     -type  yesno

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*          Running Under Manual Configuration Setup             "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Assuming user has successfully found value of                "
        LOG -level 2 -msg  "*  otherPortDeltaRx ($other_port_delta_rx ps) for port $port_num"
        LOG -level 2 -msg  "****************************************************************"
    }

    return 1
}


#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_check_other_port_cal_period               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num                : (Mandatory) DUT's port number.                    #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  other_port_cal_period   : (Mandatory) Value of the calPeriod                #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of the calPeriod is        #
#                                    verified for the specified port).         #
#                                                                              #
#                      0 on Failure (If given value of the calPeriod could not #
#                                    be verified for the specified port).      #
#                                                                              #
#  DEFINITION        : This function verifies the value of the calPeriod on the#
#                      specified port.                                         #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::manual_check_other_port_cal_period\              #
#                             -port_num                 $port_num\             #
#                             -other_port_cal_period    $other_port_cal_period #
#                                                                              #
################################################################################

proc wrptp::manual_check_other_port_cal_period {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_check_other_port_cal_period : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-other_port_cal_period)]} {
        set other_port_cal_period $param(-other_port_cal_period)
    } else {
        ERRLOG -msg "wrptp::manual_check_other_port_cal_period : Missing Argument    -> other_port_cal_period"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*           Running Under Manual Configuration Setup            "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Checking value of otherPortCalPeriod ($other_port_cal_period us)"
        LOG -level 2 -msg  "*  for port $port_num                                           "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*             Click 'Yes'     If Present                        "
        LOG -level 2 -msg  "*             Click 'No'      If Absent                         "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "****************************************************************"

        PromptString -text "Check value of otherPortCalPeriod ($other_port_cal_period us) for port $port_num<br><br>\
                                Click 'Yes'     If Present<br>\
                                Click 'No'      If Absent"\
                     -type  yesno

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*          Running Under Manual Configuration Setup             "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Assuming user has successfully found value of otherPortCalPeriod"
        LOG -level 2 -msg  "*  ($other_port_cal_period us) for port $port_num               "
        LOG -level 2 -msg  "****************************************************************"
    }

    return 1
}



#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_check_other_port_cal_retry                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num                : (Mandatory) DUT's port number.                    #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  other_port_cal_retry    : (Mandatory) Value of the calRetry                 #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of the calRetry is verified#
#                                    for the specified port).                  #
#                                                                              #
#                      0 on Failure (If given value of the calRetry could not  #
#                                    be verified for the specific port).       #
#                                                                              #
#  DEFINITION        : This function verifies the value of the calRetry on the #
#                      specified port.                                         #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::manual_check_other_port_cal_retry\               #
#                             -port_num                $port_num\              #
#                             -other_port_cal_retry    $other_port_cal_retry   #
#                                                                              #
################################################################################

proc wrptp::manual_check_other_port_cal_retry {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_check_other_port_cal_retry : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-other_port_cal_retry)]} {
        set other_port_cal_retry $param(-other_port_cal_retry)
    } else {
        ERRLOG -msg "wrptp::manual_check_other_port_cal_retry : Missing Argument    -> other_port_cal_retry"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*           Running Under Manual Configuration Setup            "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Checking value of otherPortCalRetry ($other_port_cal_retry)  "
        LOG -level 2 -msg  "*  for port $port_num                                           "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*             Click 'Yes'     If Present                        "
        LOG -level 2 -msg  "*             Click 'No'      If Absent                         "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "****************************************************************"

        PromptString -text "Check value of otherPortCalRetry ($other_port_cal_retry) for port $port_num<br><br>\
                                Click 'Yes'     If Present<br>\
                                Click 'No'      If Absent"\
                     -type  yesno

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*          Running Under Manual Configuration Setup             "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Assuming user has successfully found value of otherPortCalRetry"
        LOG -level 2 -msg  "*  ($other_port_cal_retry) for port $port_num                   "
        LOG -level 2 -msg  "****************************************************************"
    }

    return 1
}


#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::manual_check_other_port_cal_send_pattern         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num                       : (Mandatory) DUT's port number.             #
#                                        (e.g., fa0/0)                         #
#                                                                              #
#  other_port_cal_send_pattern    : (Mandatory) Value of calSendPattern        #
#                                        (e.g., TRUE|FALSE)                    #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of calSendPattern is       #
#                                    verified for the specified port).         #
#                                                                              #
#                      0 on Failure (If given value of calSendPattern could not#
#                                    be verified for the specified port).      #
#                                                                              #
#  DEFINITION        : This function verifies the value of calSendPattern on   #
#                      the specified port.                                     #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                wrptp::manual_check_other_port_cal_send_pattern\              #
#                   -port_num                      $port_num\                  #
#                   -other_port_cal_send_pattern   $other_port_cal_send_pattern#
#                                                                              #
################################################################################

proc wrptp::manual_check_other_port_cal_send_pattern {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::manual_check_other_port_cal_send_pattern : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-other_port_cal_send_pattern)]} {
        set other_port_cal_send_pattern $param(-other_port_cal_send_pattern)
    } else {
        ERRLOG -msg "wrptp::manual_check_other_port_cal_send_pattern : Missing Argument    -> other_port_cal_send_pattern"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*           Running Under Manual Configuration Setup            "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Checking value of otherPortCalSendPattern                    "
        LOG -level 2 -msg  "*  ($other_port_cal_send_pattern) for port $port_num            "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*             Click 'Yes'     If Present                        "
        LOG -level 2 -msg  "*             Click 'No'      If Absent                         "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "****************************************************************"

        PromptString -text "Check value of otherPortCalSendPattern ($other_port_cal_send_pattern) for port $port_num<br><br>\
                                Click 'Yes'     If Present<br>\
                                Click 'No'      If Absent"\
                     -type  yesno

        Echo_Off
        set answer [gets stdin]
        Echo_On

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*          Running Under Manual Configuration Setup             "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Assuming user has successfully found value of otherPortCalSendPattern"
        LOG -level 2 -msg  "*  ($other_port_cal_send_pattern) for port $port_num            "
        LOG -level 2 -msg  "****************************************************************"
    }

    return 1
}
