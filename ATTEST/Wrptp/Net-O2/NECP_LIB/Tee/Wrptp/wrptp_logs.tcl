###############################################################################
# File Name         : wrptp_logs.tcl                                          #
# File Version      : 1.3                                                     #
# Component Name    : Logs                                                    #
# Module Name       : White Rabbit Precision Time Protocol                    #
###############################################################################
# History       Date       Author         Addition/ Alteration                #
###############################################################################
#                                                                             #
#  1.0       Jul/2018      CERN           Initial                             #
#  1.1       Aug/2018      CERN           Changed for grammar correction      #
#  1.2       Sep/2018      CERN           a) Added WAIT_50_WR_STATE_TIMEOUT   #
#                                         b) Added log for checking           #
#                                            otherPortCalSendPattern,         #
#                                            otherPortCalPeriod and           #
#                                            otherPortCalRetry                #
#  1.3       Mar/2019      CERN           Added new logs.                     #
#                                                                             #
###############################################################################
# Copyright (c) 2018 - 2019 CERN                                              #
###############################################################################


### TEE LOG MESSAGES ###

set tee_log_msg(INIT)                    "Initialization of TEE\n"
set tee_log_msg(FILTER_F)                "Filter creation failed for WRPTP messages\n"
set tee_log_msg(ARP_F)                   "Unicast pre-requisite failed due to failure of ARP resolution\n"

set tee_log_msg(INIT_SETUP_1)            "Initialization of TEE setup 1\n"
set tee_log_msg(INIT_SETUP_1_F)          "Unable to initialize TEE setup 1\n"

set tee_log_msg(DISABLE_T1)              "Disable TEE's port T1\n"
set tee_log_msg(DISABLE_T1_F)            "Unable to disable TEE's port T1\n"

##ANNOUNCE
set tee_log_msg(ANNOUNCE_RX_P1_O)        "Observe that DUT transmits ANNOUNCE message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(ANNOUNCE_RX_P1_V)        "Verify that DUT transmits ANNOUNCE message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(ANNOUNCE_RX_P1_F)        "DUT has not transmitted ANNOUNCE message on port P1 ($::dut_port_num_1)\n"

set tee_log_msg(ANNOUNCE_TX_P1)          "Send periodic WRPTP ANNOUNCE messages on the port T1 ($::tee_port_num_1)\n"
set tee_log_msg(ANNOUNCE_TX_P1_F)        "Unable to transmit WRPTP ANNOUNCE message on port T1 ($::tee_port_num_1)\n"

##SYNC
set tee_log_msg(SYNC_RX_P1_O)            "Observe that DUT transmits SYNC message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(SYNC_RX_P1_V)            "Verify that DUT transmits SYNC message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(SYNC_RX_P1_F)            "DUT has not transmitted SYNC message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(SYNC_RX_2_P1_F)          "DUT has not re-transmitted SYNC message on port P1 ($::dut_port_num_1)\n"

set tee_log_msg(SYNC_TX_P1)              "Send periodic SYNC messages on the port T1 ($::tee_port_num_1)\n"
set tee_log_msg(SYNC_TX_P1_F)            "Unable to transmit SYNC message on port T1 ($::tee_port_num_1)\n"

##DELAY_REQ
set tee_log_msg(DELAY_REQ_RX_P1_O)       "Observe that DUT transmits DELAY REQ message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(DELAY_REQ_RX_P1_V)       "Verify that DUT transmits DELAY REQ message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(DELAY_REQ_RX_P1_F)       "DUT has not transmitted DELAY REQ message on port P1 ($::dut_port_num_1)\n"

set tee_log_msg(DELAY_REQ_TX_P1)         "Send DELAY REQ messages on the port T1 ($::tee_port_num_1)\n"
set tee_log_msg(DELAY_REQ_TX_P1_F)       "Unable to transmit DELAY REQ message on port T1 ($::tee_port_num_1)\n"

##DELAY_RESP
set tee_log_msg(DELAY_RESP_RX_P1_O)      "Observe that DUT transmits DELAY RESP message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(DELAY_RESP_RX_P1_V)      "Verify that DUT transmits DELAY RESP message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(DELAY_RESP_RX_P1_F)      "DUT has not transmitted DELAY RESP message on port P1 ($::dut_port_num_1)\n"

#FOLLOWUP
set tee_log_msg(FOLLOWUP_TX_P1)          "Send periodic FOLLOW_UP messages on the port T1 ($::tee_port_num_1)\n"
set tee_log_msg(FOLLOWUP_TX_P1_F)        "Unable to transmit FOLLOW_UP message on port T1 ($::tee_port_num_1)\n"

##SLAVE_PRESENT
set tee_log_msg(SLAVE_PRESENT_RX_P1_O)   "Observe that DUT transmits SLAVE_PRESENT message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(SLAVE_PRESENT_RX_P1_V)   "Verify that DUT transmits SLAVE_PRESENT message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(SLAVE_PRESENT_RX_P1_F)   "DUT has not transmitted SLAVE_PRESENT message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(SLAVE_PRESENT_NRX_P1_F)  "DUT has transmitted SLAVE_PRESENT message on port P1 ($::dut_port_num_1)\n"

set tee_log_msg(BMCA_SLAVE_PRESENT_RX_P1_V) "Wait until completion of BMCA and verify that DUT transmits WRPTP SLAVE_PRESENT message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(BMCA_SLAVE_PRESENT_RX_P1_O) "Wait until completion of BMCA and observe that DUT transmits WRPTP SLAVE_PRESENT message on port P1 ($::dut_port_num_1)\n"

set tee_log_msg(BMCA_SLAVE_PRESENT_NRX_P1_V) "Wait until completion of BMCA and verify that DUT does not transmit WRPTP SLAVE_PRESENT message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(BMCA_SLAVE_PRESENT_NRX_P1_O) "Wait until completion of BMCA and observe that DUT does not transmit WRPTP SLAVE_PRESENT message on port P1 ($::dut_port_num_1)\n"

set tee_log_msg(SLAVE_PRESENT_TX_P1)     "Send WRPTP SLAVE_PRESENT message on port T1 ($::tee_port_num_1)\n"
set tee_log_msg(SLAVE_PRESENT_TX_P1_F)   "Unable to send WRPTP SLAVE_PRESENT message on port T1 ($::tee_port_num_1)\n"

##LOCK
set tee_log_msg(LOCK_RX_P1_O)            "Observe that DUT transmits LOCK message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(LOCK_RX_P1_V)            "Verify that DUT transmits LOCK message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(LOCK_RX_P1_F)            "DUT has not transmitted LOCK message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(LOCK_RX_2_P1_F)          "DUT has not re-transmitted LOCK message on port P1 ($::dut_port_num_1)\n"

set tee_log_msg(LOCK_NRX_P1_V)           "Verify that DUT does not transmit LOCK message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(LOCK_NRX_P1_F)           "DUT has transmitted LOCK message on port P1 ($::dut_port_num_1)\n"

set tee_log_msg(LOCK_TX_P1)              "Send WRPTP LOCK messages on the port T1 ($::tee_port_num_1)\n"
set tee_log_msg(LOCK_TX_P1_F)            "Unable to transmit WRPTP LOCK message on port T1 ($::tee_port_num_1)\n"

##LOCKED
set tee_log_msg(LOCKED_RX_P1_O)          "Observe that DUT transmits LOCKED message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(LOCKED_RX_P1_V)          "Verify that DUT transmits LOCKED message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(LOCKED_RX_P1_F)          "DUT has not transmitted LOCKED message on port P1 ($::dut_port_num_1)\n"

set tee_log_msg(LOCKED_TX_P1)            "Send WRPTP LOCKED message on port T1 ($::tee_port_num_1)\n"
set tee_log_msg(LOCKED_TX_P1_F)          "Unable to send WRPTP LOCKED message on port T1 ($::tee_port_num_1)\n"

##CALIBRATE
set tee_log_msg(CALIBRATE_RX_P1_O)       "Observe that DUT transmits CALIBRATE message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(CALIBRATE_RX_P1_V)       "Verify that DUT transmits CALIBRATE message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(CALIBRATE_RX_P1_F)       "DUT has not transmitted CALIBRATE message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(CALIBRATE_TX_P1)         "Send WRPTP CALIBRATE message on port T1 ($::tee_port_num_1)\n"
set tee_log_msg(CALIBRATE_TX_P1_F)       "Unable to send WRPTP CALIBRATE message on port T1 ($::tee_port_num_1)\n"

##CALIBRATED
set tee_log_msg(CALIBRATED_RX_P1_O)      "Observe that DUT transmits CALIBRATED message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(CALIBRATED_RX_P1_V)      "Verify that DUT transmits CALIBRATED message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(CALIBRATED_RX_P1_F)      "DUT has not transmitted CALIBRATED message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(CALIBRATED_RX_2_P1_F)    "DUT has not re-transmitted CALIBRATED message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(CALIBRATED_TX_P1)        "Send WRPTP CALIBRATED message on port T1 ($::tee_port_num_1)\n"
set tee_log_msg(CALIBRATED_TX_P1_F)      "Unable to send WRPTP CALIBRATED message on port T1 ($::tee_port_num_1)\n"

##WR_MODE_ON
set tee_log_msg(WR_MODE_ON_RX_P1_O)      "Observe that DUT transmits WR_MODE_ON message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(WR_MODE_ON_RX_P1_V)      "Verify that DUT transmits WR_MODE_ON message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(WR_MODE_ON_RX_P1_F)      "DUT has not transmitted WR_MODE_ON message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(WR_MODE_ON_NRX_P1_O)     "Observe that DUT does not transmit WR_MODE_ON message on port P1 ($::dut_port_num_1)\n"
set tee_log_msg(WR_MODE_ON_NRX_P1_F)     "DUT has transmitted WR_MODE_ON message on port P1 ($::dut_port_num_1)\n"

set tee_log_msg(WR_MODE_ON_TX_P1)        "Send WRPTP WR_MODE_ON message on port T1 ($::tee_port_num_1)\n"
set tee_log_msg(WR_MODE_ON_TX_P1_F)      "Unable to send WRPTP WR_MODE_ON message on port T1 ($::tee_port_num_1)\n"

##EXPRESSIONS
set tee_log_msg(CHECK_INTVL_EXPR_V)      "Verify ((TS2 - TS1) + (TS3 - TS2))/2 is equal to the expected interval\n"

##WAIT
set tee_log_msg(WAIT_ONE_WR_STATE_TIMEOUT) "Wait to complete 1 x wrStateTimeout\n"
set tee_log_msg(WAIT_EXC_TIMEOUT_RETRY)    "Wait for EXC_TIMEOUT_RETRY to occur\n"
set tee_log_msg(WAIT_50_WR_STATE_TIMEOUT)  "Wait for 50% of wrStateTimeout to expire\n"
set tee_log_msg(WAIT_BMCA)                 "Wait for the completion of BMCA\n"
set dut_log_msg(WAIT_WRPTP_ENABLE)         "Wait for WRPTP to be enabled"

set tee_log_msg(WAIT_ONE_WR_M_LOCK_TIMEOUT)         "Wait to complete 1 x WR_M_LOCK_TIMEOUT ($::wrptp_m_lock_timeout ms)\n"
set tee_log_msg(WAIT_M_LOCK_EXC_TIMEOUT_RETRY)      "Wait for EXC_TIMEOUT_RETRY to occur ($::m_lock_exc_timeout_retry ms)\n"
set tee_log_msg(WAIT_ONE_WR_CALIBRATED_TIMEOUT)     "Wait to complete 1 x WR_CALIBRATED_TIMEOUT ($::wrptp_calibrated_timeout ms)\n"
set tee_log_msg(WAIT_CALIBRATED_EXC_TIMEOUT_RETRY)  "Wait for EXC_TIMEOUT_RETRY to occur ($::calibrated_exc_timeout_retry ms)\n"
set tee_log_msg(WAIT_ONE_RESP_CALIB_REQ_TIMEOUT)    "Wait to complete 1 x WR_RESP_CALIB_REQ_TIMEOUT ($::wrptp_resp_calib_req_timeout ms)\n"
set tee_log_msg(WAIT_TWICE_RESP_CALIB_REQ_TIMEOUT)   "Wait to complete 2 x WR_RESP_CALIB_REQ_TIMEOUT ($::wrptp_resp_calib_req_timeout_2 ms)\n"
set tee_log_msg(WAIT_TWICE10_RESP_CALIB_REQ_TIMEOUT) "Wait to complete 2 x WR_RESP_CALIB_REQ_TIMEOUT + 10% of WR_RESP_CALIB_REQ_TIMEOUT ($::wrptp_resp_calib_req_timeout_210 ms)\n"
set tee_log_msg(WAIT_RESP_CALIB_REQ_EXC_TIMEOUT_RETRY) "Wait for EXC_TIMEOUT_RETRY to occur ($::resp_calib_req_exc_timeout_retry ms)\n"
set tee_log_msg(WAIT_ONE_CALIBRATION_TIMEOUT)       "Wait to complete 1 x WR_CALIBRATION_TIMEOUT ($::wrptp_calibration_timeout ms)\n"
set tee_log_msg(WAIT_CALIBRATION_EXC_TIMEOUT_RETRY) "Wait for EXC_TIMEOUT_RETRY to occur ($::calibration_exc_timeout_retry ms)\n"
set tee_log_msg(WAIT_PRESENT_EXC_TIMEOUT_RETRY)     "Wait for EXC_TIMEOUT_RETRY to occur ($::present_exc_timeout_retry ms)\n"
set tee_log_msg(WAIT_ONE_LOCKED_TIMEOUT)            "Wait to complete 1 x WR_LOCKED_TIMEOUT ($::wrptp_locked_timeout ms)\n"
set tee_log_msg(WAIT_LOCKED_EXC_TIMEOUT_RETRY)      "Wait for EXC_TIMEOUT_RETRY to occur ($::locked_exc_timeout_retry ms)\n"
set tee_log_msg(WAIT_50_WR_CALIBRATED_TIMEOUT)      "Wait for 50% of WR_CALIBRATED_TIMEOUT ($::wrptp_calibrated_timeout_50 ms) to expire\n"


### DUT LOG MESSAGES ###

set dut_log_msg(INIT_SETUP1)             "Initialize the DUT for Test setup 1\n"
set dut_log_msg(INIT_SETUP1_F)           "Initialization of DUT for Test setup 1 has failed\n"

set dut_log_msg(INIT_SETUP2)             "Initialize the DUT for Test setup 2\n"
set dut_log_msg(INIT_SETUP2_F)           "Initialization of DUT for Test setup 2 has failed\n"

set dut_log_msg(INIT_SETUP3)             "Initialize the DUT for Test setup 3\n"
set dut_log_msg(INIT_SETUP3_F)           "Initialization of DUT for Test setup 3 has failed\n"

set dut_log_msg(INIT_SETUP4)             "Initialize the DUT for Test setup 4\n"
set dut_log_msg(INIT_SETUP4_F)           "Initialization of DUT for Test setup 4 has failed\n"

set dut_log_msg(INIT_SETUP5)             "Initialize the DUT for Test setup 5\n"
set dut_log_msg(INIT_SETUP5_F)           "Initialization of DUT for Test setup 5 has failed\n"

set dut_log_msg(DISABLE_PORT)            "Disable DUT's port P1\n"
set dut_log_msg(DISABLE_PORT_F)          "Unable to disable DUT's port P1\n"

set dut_log_msg(ENABLE_PORT)             "Enable DUT's port P1\n"
set dut_log_msg(ENABLE_PORT_F)           "Unable to enable DUT's port P1\n"

set dut_log_msg(DISABLE_WRPTP_PORT_P1)   "Disable WRPTP on port P1\n"
set dut_log_msg(DISABLE_WRPTP_PORT_P1_F) "Unable to disable WRPTP on port P1\n"

set dut_log_msg(ENABLE_WRPTP_PORT_P1)    "Enable WRPTP on port P1\n"
set dut_log_msg(ENABLE_WRPTP_PORT_P1_F)  "Unable to enable WRPTP on port P1\n"

set dut_log_msg(CHECK_STATE_IDLE_P1_V)   "Verify that WRPTP portState of port P1 is in IDLE state.\n"
set dut_log_msg(CHECK_STATE_IDLE_P1_O)   "Observe that WRPTP portState of port P1 is in IDLE state.\n"
set dut_log_msg(CHECK_STATE_IDLE_P1_F)   "WRPTP portState of port P1 is not in IDLE state.\n"

set dut_log_msg(CHECK_STATE_M_LOCK_P1_V)   "Verify that WRPTP portState of port P1 is in M_LOCK state.\n"
set dut_log_msg(CHECK_STATE_M_LOCK_P1_O)   "Observe that WRPTP portState of port P1 is in M_LOCK state.\n"
set dut_log_msg(CHECK_STATE_M_LOCK_P1_F)   "WRPTP portState of port P1 is not in M_LOCK state.\n"

set dut_log_msg(CHECK_STATE_LOCKED_P1_V)   "Verify that WRPTP portState of port P1 is in LOCKED state.\n"
set dut_log_msg(CHECK_STATE_LOCKED_P1_O)   "Observe that WRPTP portState of port P1 is in LOCKED state.\n"
set dut_log_msg(CHECK_STATE_LOCKED_P1_F)   "WRPTP portState of port P1 is not in LOCKED state.\n"

set dut_log_msg(CHECK_STATE_CALIBRATION_P1_V)   "Verify that WRPTP portState of port P1 is in CALIBRATION state.\n"
set dut_log_msg(CHECK_STATE_CALIBRATION_P1_O)   "Observe that WRPTP portState of port P1 is in CALIBRATION state.\n"
set dut_log_msg(CHECK_STATE_CALIBRATION_P1_F)   "WRPTP portState of port P1 is not in CALIBRATION state.\n"

set dut_log_msg(CHECK_STATE_CALIBRATED_P1_V)   "Verify that WRPTP portState of port P1 is in CALIBRATED state.\n"
set dut_log_msg(CHECK_STATE_CALIBRATED_P1_O)   "Observe that WRPTP portState of port P1 is in CALIBRATED state.\n"
set dut_log_msg(CHECK_STATE_CALIBRATED_P1_F)   "WRPTP portState of port P1 is not in CALIBRATED state.\n"

set dut_log_msg(CHECK_STATE_RESP_CALIB_REQ_P1_V)   "Verify that WRPTP portState of port P1 is in RESP_CALIB_REQ state.\n"
set dut_log_msg(CHECK_STATE_RESP_CALIB_REQ_P1_O)   "Observe that WRPTP portState of port P1 is in RESP_CALIB_REQ state.\n"
set dut_log_msg(WAIT_CHECK_STATE_RESP_CALIB_REQ_P1_V)   "Wait for $::wrptp_resp_calib_req_timeout_5 msec and verify that WRPTP portState of port P1 is in RESP_CALIB_REQ state.\n"
set dut_log_msg(WAIT_CHECK_STATE_RESP_CALIB_REQ_P1_O)   "Wait for $::wrptp_resp_calib_req_timeout_5 msec and observe that WRPTP portState of port P1 is in RESP_CALIB_REQ state.\n"
set dut_log_msg(CHECK_STATE_RESP_CALIB_REQ_P1_F)   "WRPTP portState of port P1 is not in RESP_CALIB_REQ state.\n"

set dut_log_msg(CHECK_OTHER_PORT_DELTA_TX_P1_V)    "Verify otherPortDeltaTx value on port P1 in DUT\n"
set dut_log_msg(CHECK_OTHER_PORT_DELTA_TX_P1_O)    "Observe otherPortDeltaTx value on port P1 in DUT\n"
set dut_log_msg(CHECK_OTHER_PORT_DELTA_TX_P1_F)    "Unable to verify otherPortDeltaTx value on port P1 in DUT\n"

set dut_log_msg(CHECK_OTHER_PORT_DELTA_RX_P1_V)    "Verify otherPortDeltaRx value on port P1 in DUT\n"
set dut_log_msg(CHECK_OTHER_PORT_DELTA_RX_P1_O)    "Observe otherPortDeltaRx value on port P1 in DUT\n"
set dut_log_msg(CHECK_OTHER_PORT_DELTA_RX_P1_F)    "Unable to verify otherPortDeltaRx value on port P1 in DUT\n"

set dut_log_msg(CHECK_OTHER_PORT_CAL_SEND_PATTERN_P1_V)    "Verify otherPortCalSendPattern value on port P1 in DUT\n"
set dut_log_msg(CHECK_OTHER_PORT_CAL_SEND_PATTERN_P1_O)    "Observe otherPortCalSendPattern value on port P1 in DUT\n"
set dut_log_msg(CHECK_OTHER_PORT_CAL_SEND_PATTERN_P1_F)    "Unable to verify otherPortCalSendPattern value on port P1 in DUT\n"

set dut_log_msg(CHECK_OTHER_PORT_CAL_PERIOD_P1_V)    "Verify otherPortCalPeriod value on port P1 in DUT\n"
set dut_log_msg(CHECK_OTHER_PORT_CAL_PERIOD_P1_O)    "Observe otherPortCalPeriod value on port P1 in DUT\n"
set dut_log_msg(CHECK_OTHER_PORT_CAL_PERIOD_P1_F)    "Unable to verify otherPortCalPeriod value on port P1 in DUT\n"

set dut_log_msg(CHECK_OTHER_PORT_CAL_RETRY_P1_V)    "Verify otherPortCalRetry value on port P1 in DUT\n"
set dut_log_msg(CHECK_OTHER_PORT_CAL_RETRY_P1_O)    "Observe otherPortCalRetry value on port P1 in DUT\n"
set dut_log_msg(CHECK_OTHER_PORT_CAL_RETRY_P1_F)    "Unable to verify otherPortCalRetry value on port P1 in DUT\n"

set dut_log_msg(SET_WRCONFIG_M_AND_S)              "Configure wrConfig = WR_M_AND_S.\n"
set dut_log_msg(SET_WRCONFIG_M_AND_S_F)            "Unable to configure wrConfig = WR_M_AND_S.\n"

set dut_log_msg(SET_WRCONFIG_M_ONLY)               "Configure wrConfig = WR_M_ONLY.\n"
set dut_log_msg(SET_WRCONFIG_M_ONLY_F)             "Unable to configure wrConfig = WR_M_ONLY.\n"

set dut_log_msg(CHECK_WRCONFIG_M_ONLY)             "Verify that wrConfig for port P1 is WR_M_ONLY.\n"
set dut_log_msg(CHECK_WRCONFIG_M_ONLY_F)           "wrConfig for port P1 is not WR_M_ONLY."

set dut_log_msg(SET_WRCONFIG_S_ONLY)               "Configure wrConfig = WR_S_ONLY.\n"
set dut_log_msg(SET_WRCONFIG_S_ONLY_F)             "Unable to configure wrConfig = WR_S_ONLY.\n"

set dut_log_msg(CHECK_WRCONFIG_S_ONLY)             "Verify that wrConfig for port P1 is WR_S_ONLY.\n"
set dut_log_msg(CHECK_WRCONFIG_S_ONLY_F)           "wrConfig for port P1 is not WR_S_ONLY."

set dut_log_msg(SET_WRCONFIG_NON_WR)               "Configure wrConfig = NON_WR.\n"
set dut_log_msg(SET_WRCONFIG_NON_WR_F)             "Unable to configure wrConfig = NON_WR.\n"

set dut_log_msg(CHECK_WRCONFIG_NON_WR)             "Verify that wrConfig for port P1 is NON_WR.\n"
set dut_log_msg(CHECK_WRCONFIG_NON_WR_F)           "wrConfig for port P1 is not NON_WR."

set dut_log_msg(CHECK_PTP_STATE_MASTER_P1_V)       "Verify that PTP portState of port P1 is MASTER."
set dut_log_msg(CHECK_PTP_STATE_MASTER_P1_F)       "PTP portState of port P1 is not in MASTER state"

set dut_log_msg(CHECK_PTP_STATE_SLAVE_P1_V)        "Verify that PTP portState of port P1 is SLAVE."
set dut_log_msg(CHECK_PTP_STATE_SLAVE_P1_F)        "PTP portState of port P1 is not in SLAVE state"

set dut_log_msg(SET_KNOWN_DELTA_TX_0)              "Configure knownDeltaTx = 0 on port P1."
set dut_log_msg(SET_KNOWN_DELTA_TX_F)              "Unable to configure knownDeltaTx on port P1."
set dut_log_msg(SET_KNOWN_DELTA_TX_DEFAULT)        "Configure knownDeltaTx = Default value on port P1."

set dut_log_msg(SET_KNOWN_DELTA_RX_0)              "Configure knownDeltaRx = 0 on port P1."
set dut_log_msg(SET_KNOWN_DELTA_RX_F)              "Unable to configure knownDeltaRx on port P1."
set dut_log_msg(SET_KNOWN_DELTA_RX_DEFAULT)        "Configure knownDeltaRx = Default value on port P1."

## ERROR MESSAGE

set err_desc  "is not matching"

array set tee_rx_errlog "
    -1   \"\"
     0   \"\"
     1   \"(Reason: Destination MAC $err_desc)\"
     2   \"(Reason: Source MAC $err_desc)\"
     3   \"(Reason: Ethernet Type $err_desc)\"
     4   \"(Reason: VLAN ID $err_desc)\"
     5   \"(Reason: VLAN Priority $err_desc)\"
     6   \"(Reason: VLAN DEI $err_desc)\"
     7   \"(Reason: IP Checksum $err_desc)\"
     8   \"(Reason: IP Protocol $err_desc)\"
     9   \"(Reason: Source IP $err_desc)\"
    10   \"(Reason: Destination IP $err_desc)\"
    11   \"(Reason: Source Port $err_desc)\"
    12   \"(Reason: Destination Port $err_desc)\"
    13   \"(Reason: UDP Length $err_desc)\"
    14   \"(Reason: UDP Checksum $err_desc)\"
    15   \"(Reason: transportSpecific $err_desc)\"
    16   \"\"
    17   \"(Reason: versionPTP $err_desc)\"
    18   \"(Reason: messageLength $err_desc)\"
    19   \"(Reason: domainNumber $err_desc)\"
    20   \"(Reason: alternateMasterFlag $err_desc)\"
    21   \"(Reason: twoStepFlag $err_desc)\"
    22   \"(Reason: unicastFlag $err_desc)\"
    23   \"(Reason: PTP profile Specific 1 $err_desc)\"
    24   \"(Reason: PTP profile Specific 2 $err_desc)\"
    25   \"(Reason: leap61 $err_desc)\"
    26   \"(Reason: leap59 $err_desc)\"
    27   \"(Reason: currentUtcOffsetValid $err_desc)\"
    28   \"(Reason: ptpTimescale $err_desc)\"
    29   \"(Reason: timeTraceable $err_desc)\"
    30   \"(Reason: frequencyTraceable $$err_desc)\"
    31   \"(Reason: correctionField $err_desc)\"
    32   \"(Reason: sourcePortNumber $err_desc)\"
    33   \"(Reason: sourceClockIdentity $err_desc)\"
    34   \"(Reason: sequenceId $err_desc)\"
    35   \"(Reason: controlField $err_desc)\"
    36   \"(Reason: logMessageInterval $err_desc)\"
    37   \"(Reason: originTimestamp (sec) $err_desc)\"
    38   \"(Reason: originTimestamp (ns) $err_desc)\"
    39   \"(Reason: currentUtcOffset $err_desc)\"
    40   \"(Reason: grandmasterPriority1 $err_desc)\"
    41   \"(Reason: grandmasterPriority2 $err_desc)\"
    42   \"(Reason: grandmasterIdentity $err_desc)\"
    43   \"(Reason: stepsRemoved $err_desc)\"
    44   \"(Reason: timeSource $err_desc)\"
    45   \"(Reason: grandmasterClockClass $err_desc)\"
    46   \"(Reason: grandmasterClockAccuracy $err_desc)\"
    47   \"(Reason: randmasterClockVariance $err_desc)\"
    48   \"(Reason: preciseOriginTimestamp (sec) $err_desc)\"
    49   \"(Reason: preciseOriginTimestamp (ns $err_desc)\"
    50   \"(Reason: receiveTimestmap (sec) $err_desc)\"
    51   \"(Reason: receiveTimestamp (ns) $err_desc)\"
    52   \"(Reason: requestingPortNumber $err_desc)\"
    53   \"(Reason: requestingClockIdentity $err_desc)\"
    54   \"(Reason: requestReceiptTimestamp (sec) $err_desc)\"
    55   \"(Reason: requestReceiptTimestamp (ns) $err_desc)\"
    56   \"(Reason: responseOriginTimestamp (sec) $err_desc)\"
    57   \"(Reason: responseOriginTimestamp (ns) $err_desc)\"
    58   \"(Reason: targetPortNumber $err_desc)\"
    59   \"(Reason: targetClockIdentity $err_desc)\"
    60   \"(Reason: startingBoundaryHops $err_desc)\"
    61   \"(Reason: boundaryHops $err_desc)\"
    62   \"(Reason: actionField $err_desc)\"
    63   \"(Reason: tlvType $err_desc)\"
    64   \"(Reason: TLV lengthField $err_desc)\"
    65   \"(Reason: OrganizationId $err_desc)\"
    66   \"(Reason: magicNumber $err_desc)\"
    67   \"(Reason: versionNumber $err_desc)\"
    68   \"\"
    69   \"(Reason: wrConfig $err_desc)\"
    70   \"(Reason: calibrated $err_desc)\"
    71   \"(Reason: wrModeOn $err_desc)\"
    72   \"(Reason: calSendPattern $err_desc)\"
    73   \"(Reason: calRetry $err_desc)\"
    74   \"(Reason: calPeriod $err_desc)\"
    75   \"(Reason: deltaTx $err_desc)\"
    76   \"(Reason: deltaRx $err_desc)\"
    77   \"(Reason: sourcePortIdentity $err_desc)\"
    78   \"(Reason: targetPortIdentity $err_desc)\"
"
