################################################################################
# File Name         : dut_wrptp_functions.tcl                                  #
# File Version      : 1.3                                                      #
# Component Name    : ATTEST DEVICE UNDER TEST (DUT)                           #
# Module Name       : White Rabbit Precision Time Protocol                     #
################################################################################
# History       Date       Author         Addition/ Alteration                 #
################################################################################
#                                                                              #
#  1.0       Jul/2018      CERN           Initial                              #
#  1.1       Dec/2018      CERN           Added timer configuration for each   #
#                                         state in WRPTP state machine.        #
#  1.2       Jan/2019      CERN           Removed below procedures,            #
#                                         a) wrptp::dut_set_wr_state_timeout   #
#                                         b) wrptp::dut_reset_wr_state_timeout #
#  1.3       Apr/2019      JC.BAU/CERN    Added dut_commit_changes and calls to#
#                                         dut_prepare_dot_config in setup00X   #
#                                                                              #
################################################################################
# Copyright (c) 2018 - 2019 CERN                                               #
################################################################################

######################### PROCEDURES USED ######################################
#                                                                              #
#  DUT SET FUNCTIONS     :                                                     #
#                                                                              #
#  1) wrptp::dut_port_enable                                                   #
#  2) wrptp::dut_port_ptp_enable                                               #
#  3) wrptp::dut_set_vlan                                                      #
#  4) wrptp::dut_set_vlan_priority                                             #
#  5) wrptp::dut_global_ptp_enable                                             #
#  6) wrptp::dut_set_communication_mode                                        #
#  7) wrptp::dut_set_domain                                                    #
#  8) wrptp::dut_set_network_protocol                                          #
#  9) wrptp::dut_set_clock_mode                                                #
# 10) wrptp::dut_set_clock_step                                                #
# 11) wrptp::dut_set_delay_mechanism                                           #
# 12) wrptp::dut_set_priority1                                                 #
# 13) wrptp::dut_set_priority2                                                 #
# 14) wrptp::dut_set_announce_interval                                         #
# 15) wrptp::dut_set_ipv4_address                                              #
# 16) wrptp::dut_set_wr_config                                                 #
# 17) wrptp::dut_set_known_delta_tx                                            #
# 18) wrptp::dut_set_known_delta_rx                                            #
# 19) wrptp::dut_set_deltas_known                                              #
# 20) wrptp::dut_set_cal_period                                                #
# 21) wrptp::dut_set_cal_retry                                                 #
# 22) wrptp::dut_port_wrptp_enable                                             #
# 23) wrptp::dut_set_wr_present_timeout                                        #
# 24) wrptp::dut_set_wr_m_lock_timeout                                         #
# 25) wrptp::dut_set_wr_s_lock_timeout                                         #
# 26) wrptp::dut_set_wr_locked_timeout                                         #
# 27) wrptp::dut_set_wr_calibration_timeout                                    #
# 28) wrptp::dut_set_wr_calibrated_timeout                                     #
# 29) wrptp::dut_set_wr_resp_calib_req_timeout                                 #
# 30) wrptp::dut_set_wr_state_retry                                            #
# 31) wrptp::dut_commit_changes                                                #
#                                                                              #
#  DUT DISABLE FUNCTIONS :                                                     #
#                                                                              #
#  1) wrptp::dut_port_disable                                                  #
#  2) wrptp::dut_port_ptp_disable                                              #
#  3) wrptp::dut_reset_vlan                                                    #
#  4) wrptp::dut_reset_vlan_priority                                           #
#  5) wrptp::dut_global_ptp_disable                                            #
#  6) wrptp::dut_reset_communication_mode                                      #
#  7) wrptp::dut_reset_domain                                                  #
#  8) wrptp::dut_reset_network_protocol                                        #
#  9) wrptp::dut_reset_clock_mode                                              #
# 10) wrptp::dut_reset_clock_step                                              #
# 11) wrptp::dut_reset_delay_mechanism                                         #
# 12) wrptp::dut_reset_priority1                                               #
# 13) wrptp::dut_reset_priority2                                               #
# 14) wrptp::dut_reset_announce_interval                                       #
# 15) wrptp::dut_reset_ipv4_address                                            #
# 16) wrptp::dut_reset_wr_config                                               #
# 17) wrptp::dut_reset_known_delta_tx                                          #
# 18) wrptp::dut_reset_known_delta_rx                                          #
# 19) wrptp::dut_reset_deltas_known                                            #
# 20) wrptp::dut_reset_cal_period                                              #
# 21) wrptp::dut_reset_cal_retry                                               #
# 22) wrptp::dut_port_wrptp_disable                                            #
# 23) wrptp::dut_reset_wr_present_timeout                                      #
# 24) wrptp::dut_reset_wr_m_lock_timeout                                       #
# 25) wrptp::dut_reset_wr_s_lock_timeout                                       #
# 26) wrptp::dut_reset_wr_locked_timeout                                       #
# 27) wrptp::dut_reset_wr_calibration_timeout                                  #
# 28) wrptp::dut_reset_wr_calibrated_timeout                                   #
# 29) wrptp::dut_reset_wr_resp_calib_req_timeout                               #
# 30) wrptp::dut_reset_wr_state_retry                                          #
#                                                                              #
#  DUT CHECK FUNCTIONS   :                                                     #
#                                                                              #
#  1) wrptp::dut_check_wr_config                                               #
#  2) wrptp::dut_check_wr_port_state                                           #
#  3) wrptp::dut_check_other_port_delta_tx                                     #
#  4) wrptp::dut_check_other_port_delta_rx                                     #
#  5) wrptp::dut_check_other_port_cal_period                                   #
#  6) wrptp::dut_check_other_port_cal_retry                                    #
#  7) wrptp::dut_check_other_port_cal_send_pattern                             #
#                                                                              #
#  DUT SETUP CONFIGURATION FUNCTIONS :                                         #
#                                                                              #
#  1) wrptp::dut_configure_setup_001                                           #
#  2) wrptp::dut_configure_setup_002                                           #
#  3) wrptp::dut_configure_setup_003                                           #
#                                                                              #
#  DUT SETUP CLEANUP FUNCTIONS       :                                         #
#                                                                              #
#  1) wrptp::dut_cleanup_setup_001                                             #
#  2) wrptp::dut_cleanup_setup_002                                             #
#  3) wrptp::dut_cleanup_setup_003                                             #
#                                                                              #
################################################################################


################################################################################
#                           DUT SET FUNCTIONS                                  #
################################################################################

package provide wrptp 1.0

namespace eval wrptp {

    namespace export *
}

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_port_enable                                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) Port needs to be made administratively up.  #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If port is made administratively up)      #
#                                                                              #
#                      0 on Failure (If port could not be made                 #
#                                    administratively up)                      #
#                                                                              #
#  DEFINITION        : This function makes the specific port administratively  #
#                      up at the DUT.                                          #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_port_enable\                                 #
#                             -port_num                 $port_num              #
#                                                                              #
################################################################################

proc wrptp::dut_port_enable {args} {

    array set param $args
    global env

    set config_mode $env(DUT_CONFIG_MODE)
    set session_handler $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_port_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Enable port $port_num"

    if {![wrptp::${config_mode}_port_enable\
                    -session_handler  $session_handler\
                    -port_num         $port_num]} {

        ERRLOG -msg "wrptp::dut_port_enable : Unable to enable \
                    specified port on DUT"
        return 0
    }

    return 1
}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_port_ptp_enable                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) Port on which ptp is to be enabled.         #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is enabled on the specified port   #
#                                    at the DUT).                              #
#                                                                              #
#                      0 on Failure (If PTP could not be enabled on the        #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function enables PTP on a specific port on the DUT.#
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_port_ptp_enable\                             #
#                             -port_num                 $port_num              #
#                                                                              #
################################################################################

proc wrptp::dut_port_ptp_enable {args} {

    array set param $args
    global env

    set config_mode $env(DUT_CONFIG_MODE)
    set session_handler $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_port_ptp_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Enable PTP on port $port_num"

    if {![wrptp::${config_mode}_port_ptp_enable\
                           -session_handler  $session_handler\
                           -port_num         $port_num]} {

        ERRLOG -msg "wrptp::dut_port_ptp_enable : Unable to enable PTP \
                    on specified port on DUT"
        return 0
    }

    return 1
}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_set_vlan                                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID with which VLAN has to be created.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#  port_num          : (Mandatory) Port which has to be associated to VLAN ID. #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If VLAN ID is created and port is         #
#                                    associated to it on the DUT).             #
#                                                                              #
#                      0 on Failure (If VLAN ID could not be created or port   #
#                                    is not associated to it on the DUT).      #
#                                                                              #
#  DEFINITION        : This function creates VLAN ID and associates given port #
#                      to it on the DUT.                                       #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_set_vlan\                                    #
#                             -vlan_id                  $vlan_id\              #
#                             -port_num                 $port_num              #
#                                                                              #
################################################################################

proc wrptp::dut_set_vlan {args} {

    array set param $args
    global env

    set config_mode $env(DUT_CONFIG_MODE)
    set session_handler $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "wrptp::dut_set_vlan : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_set_vlan : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Create VLAN $vlan_id and associate port $port_num to it"

    if {![wrptp::${config_mode}_set_vlan\
                           -session_handler  $session_handler\
                           -vlan_id          $vlan_id\
                           -port_num         $port_num]} {

        ERRLOG -msg "wrptp::dut_set_vlan : Unable to create VLAN and associate port on DUT"
        return 0
    }

    return 1
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_set_vlan_priority                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID                                     #
#                                  (e.g., 100)                                 #
#                                                                              #
#  vlan_priority     : (Mandatory) VLAN priority                               #
#                                                                              #
#  port_num          : (Mandatory) Port which has to be associated to VLAN ID. #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If VLAN priority is associated to         #
#                                    VLAN ID on the DUT).                      #
#                                                                              #
#                      0 on Failure (If VLAN priority is not associated to the #
#                                    VLAN ID on the DUT).                      #
#                                                                              #
#  DEFINITION        : This function associates VLAN priority to the VLAN ID   #
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_set_vlan_priority\                           #
#                             -vlan_id                  $vlan_id\              #
#                             -vlan_priority            $vlan_priority\        #
#                             -port_num                 $port_num              #
#                                                                              #
################################################################################

proc wrptp::dut_set_vlan_priority {args} {

    array set param $args
    global env

    set config_mode     $env(DUT_CONFIG_MODE)
    set session_handler $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "wrptp::dut_set_vlan_priority : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if {[info exists param(-vlan_priority)]} {
        set vlan_priority $param(-vlan_priority)
    } else {
        ERRLOG -msg "wrptp::dut_set_vlan_priority : Missing Argument    -> vlan_priority"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_set_vlan_priority : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Associate VLAN priority $vlan_priority to VLAN $vlan_id"

    if {![wrptp::${config_mode}_set_vlan_priority\
                    -session_handler  $session_handler\
                    -vlan_id          $vlan_id\
                    -vlan_priority    $vlan_priority\
                    -port_num         $port_num]} {

        ERRLOG -msg "wrptp::dut_set_vlan_priority : Unable to associate priority to VLAN on DUT"
        return 0
    }

    return 1
}


#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_global_ptp_enable                            #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is enabled on the DUT).            #
#                                                                              #
#                      0 on Failure (If PTP could not be enabled on the DUT).  #
#                                                                              #
#  DEFINITION        : This function enables PTP on the DUT.                   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_port_ptp_enable                              #
#                                                                              #
################################################################################

proc wrptp::dut_global_ptp_enable {} {

    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    LOG -level 2 -msg "\t Enable PTP globally on DUT"

    if {![wrptp::${config_mode}_global_ptp_enable \
                          -session_handler  $session_handler]} {

        ERRLOG -msg "wrptp::dut_global_ptp_enable : Unable to enable PTP globally"
        return 0
    }

    return 1
}


#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_set_communication_mode                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  ptp_version       : (optional)  PTP version.                                #
#                                  (Default: 2)                                #
#                                                                              #
#  communication_mode : (Mandatory) Communication mode                         #
#                                  (e.g., "Unicast"|"Multicast")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If communication mode is set on the       #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If communication mode could not be        #
#                                    set on the specified port at the DUT).    #
#                                                                              #
#  DEFINITION        : This function sets the communication mode on a specific #
#                      port of the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_set_communication_mode\                      #
#                             -port_num                 $port_num\             #
#                             -clock_mode               $clock_mode\           #
#                             -ptp_version              $ptp_version\          #
#                             -communication_mode       $communication_mode    #
#                                                                              #
################################################################################

proc wrptp::dut_set_communication_mode {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_set_communication_mode : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::dut_set_communication_mode : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-communication_mode)]} {
        set communication_mode $param(-communication_mode)
    } else {
        ERRLOG -msg "wrptp::dut_set_communication_mode : Missing Argument  -> communication_mode"
        incr missedArgCounter
    }

    if {[info exists param(-ptp_version)]} {
        set ptp_version $param(-ptp_version)
    } else {
        set ptp_version "2"
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set communication mode ($communication_mode) on port $port_num"

    if {![wrptp::${config_mode}_set_communication_mode\
                           -session_handler      $session_handler\
                           -port_num             $port_num\
                           -clock_mode           $clock_mode\
                           -ptp_version          $ptp_version\
                           -communication_mode   $communication_mode]} {

        ERRLOG -msg "wrptp::dut_set_communication_mode : \
                    Unable to set PTP communication mode on specific port of the DUT"
        return 0
    }

    return 1
}


#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_set_domain                                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  domain            : (Mandatory) Domain number.                              #
#                                  (e.g., 0..255)                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If domain number is set on the DUT).      #
#                                                                              #
#                      0 on Failure (If domain number could not able to set on #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function sets domain number on the DUT.            #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_set_domain\                                  #
#                             -clock_mode               $clock_mode\           #
#                             -domain                   $domain                #
#                                                                              #
################################################################################

proc wrptp::dut_set_domain {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::dut_set_domain : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-domain)]} {
        set domain $param(-domain)
    } else {
        ERRLOG -msg "wrptp::dut_set_domain : Missing Argument    -> domain"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set domain ($domain) on DUT"

    if {![wrptp::${config_mode}_set_domain\
                           -session_handler  $session_handler\
                           -clock_mode       $clock_mode\
                           -domain           $domain]} {

        ERRLOG -msg "wrptp::dut_set_domain : Unable to set domain on DUT"
        return 0
    }

    return 1
}


#8.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_set_network_protocol                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) clock_mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  network_protocol  : (Mandatory) Network protocol.                           #
#                                  (e.g., "IEEE 802.3"|"UDP/IPv4")             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If network protocol is set on specified   #
#                                    port at the DUT).                         #
#                                                                              #
#                      0 on Failure (If network protocol could not be set      #
#                                    on specified port at the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets the network protocol on a specific   #
#                      port of the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_set_network_protocol\                        #
#                             -port_num                 $port_num\             #
#                             -clock_mode               $clock_mode\           #
#                             -network_protocol         $network_protocol      #
#                                                                              #
################################################################################

proc wrptp::dut_set_network_protocol {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_set_network_protocol : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::dut_set_network_protocol : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-network_protocol)]} {
        set network_protocol $param(-network_protocol)
    } else {
        ERRLOG -msg "wrptp::dut_set_network_protocol : Missing Argument    -> network_protocol"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set network protocol ($network_protocol) on port $port_num"

    if {![wrptp::${config_mode}_set_network_protocol\
                    -session_handler  $session_handler\
                    -port_num         $port_num\
                    -clock_mode       $clock_mode\
                    -network_protocol $network_protocol]} {

        ERRLOG -msg "wrptp::dut_set_network_protocol : Unable to set network protocol \
                    on specified port at the DUT"
        return 0
    }

    return 1
}


#9.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_set_clock_mode                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP clock mode is set on specified port#
#                                    of the DUT).                              #
#                                                                              #
#                      0 on Failure (If PTP clock mode could not be set on     #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function sets clock mode on a specific port of DUT.#
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_set_clock_mode\                              #
#                             -port_num                 $port_num\             #
#                             -clock_mode               $clock_mode            #
#                                                                              #
################################################################################

proc wrptp::dut_set_clock_mode {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_set_clock_mode : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::dut_set_clock_mode : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set clock mode ($clock_mode) on port $port_num"

    if {![wrptp::${config_mode}_set_clock_mode\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -clock_mode       $clock_mode]} {

        ERRLOG -msg "wrptp::dut_set_clock_mode : Unable to set clock mode \
                    on specified port on DUT"
        return 0
    }

    return 1
}


#10.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_set_clock_step                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  clock_step        : (Mandatory) Clock step.                                 #
#                                  (e.g., "One-step"|"Two-step")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If clock-step is set on the specified     #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If clock-step could not be able to set    #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets the clock-step on a specific port on #
#                      the DUT.                                                #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_set_clock_step\                              #
#                             -port_num                 $port_num\             #
#                             -clock_mode               $clock_mode\           #
#                             -clock_step               $clock_step            #
#                                                                              #
################################################################################

proc wrptp::dut_set_clock_step {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_set_clock_step : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::dut_set_clock_step : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-clock_step)]} {
        set clock_step $param(-clock_step)
    } else {
        ERRLOG -msg "wrptp::dut_set_clock_step : Missing Argument    -> clock_step"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set clock step ($clock_step) on port $port_num"

    if {![wrptp::${config_mode}_set_clock_step\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -clock_mode       $clock_mode\
                           -clock_step       $clock_step]} {

        ERRLOG -msg "wrptp::dut_set_clock_step : Unable to set clock step\
                    on specified port on DUT"
        return 0
    }

    return 1
}


#11.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_set_delay_mechanism                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  delay_mechanism   : (Mandatory) Delay mechanism.                            #
#                                  (e.g., e2e|p2p|disable)                     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If delay mechanism is set on specified    #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If delay mechanism could not be set on    #
#                                    specified PTP port of the DUT).           #
#                                                                              #
#  DEFINITION        : This function sets the delay mechanism to a specific    #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_set_delay_mechanism\                         #
#                             -port_num                 $port_num\             #
#                             -clock_mode               $clock_mode\           #
#                             -delay_mechanism          $delay_mechanism       #
#                                                                              #
################################################################################

proc wrptp::dut_set_delay_mechanism {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_set_delay_mechanism : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::dut_set_delay_mechanism : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-delay_mechanism)]} {
        set delay_mechanism $param(-delay_mechanism)
    } else {
        ERRLOG -msg "wrptp::dut_set_delay_mechanism : Missing Argument    -> delay_mechanism"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set delay mechanism ($delay_mechanism) on port $port_num"

    if {![wrptp::${config_mode}_set_delay_mechanism\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -clock_mode       $clock_mode\
                           -delay_mechanism  $delay_mechanism]} {

        ERRLOG -msg "wrptp::dut_set_delay_mechanism : Unable to set delay mechanism\
                    on specified port on DUT"
        return 0
    }

    return 1
}


#12.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_set_priority1                                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority1         : (Mandatory) Prority1                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority1 is set on the DUT).          #
#                                                                              #
#                      0 on Failure (If priority1 could not be able to set on  #
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function sets the priority1 on the DUT.            #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_set_priority1\                               #
#                             -clock_mode               $clock_mode\           #
#                             -priority1                $priority1             #
#                                                                              #
################################################################################

proc wrptp::dut_set_priority1 {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::dut_set_priority1 : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-priority1)]} {
        set priority1  $param(-priority1)
    } else {
        ERRLOG -msg "wrptp::dut_set_priority1 : Missing Argument    -> priority1"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set priority1 ($priority1) on DUT"

    if {![wrptp::${config_mode}_set_priority1\
                           -session_handler  $session_handler\
                           -clock_mode       $clock_mode\
                           -priority1        $priority1]} {

        ERRLOG  -msg "wrptp::dut_set_priority1: Unable to set priority1 on the DUT"
        return 0
    }

    return 1
}


#13.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_set_priority2                                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority2         : (Mandatory) Priority2                                   #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority2 is set on the DUT).          #
#                                                                              #
#                      0 on Failure (If priority2 could not be able to set on  #
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function sets the priority2 on the DUT.            #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_set_priority2\                               #
#                             -clock_mode               $clock_mode\           #
#                             -priority2                $priority2             #
#                                                                              #
################################################################################

proc wrptp::dut_set_priority2 {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "wrptp::dut_set_priority2 : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-priority2)]} {
        set priority2  $param(-priority2)
    } else {
        ERRLOG -msg "wrptp::dut_set_priority2 : Missing Argument    -> priority2"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set priority2 ($priority2) on DUT"

    if {![wrptp::${config_mode}_set_priority2\
                           -session_handler  $session_handler\
                           -clock_mode       $clock_mode\
                           -priority2        $priority2]} {

        ERRLOG  -msg "wrptp::dut_set_priority2: Unable to set priority2 on the DUT"
        return 0
    }

    return 1
}


#14.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_set_announce_interval                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0).                              #
#                                                                              #
#  announce_interval : (Mandatory) announceInterval.                           #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If announceInterval value is set on the   #
#                                    specified port of DUT).                   #
#                                                                              #
#                      0 on Failure (If announceInterval could not be able to  #
#                                    set on specified port of the DUT).        #
#                                                                              #
#  DEFINITION        : This function sets the announceInterval value on the    #
#                      specified port of DUT.                                  #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_set_announce_interval\                       #
#                             -port_num                 $port_num\             #
#                             -announce_interval        $announce_interval     #
#                                                                              #
################################################################################

proc wrptp::dut_set_announce_interval {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_set_announce_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-announce_interval)]} {
        set announce_interval $param(-announce_interval)
    } else {
        ERRLOG -msg "wrptp::dut_set_announce_interval : Missing Argument    -> announce_interval"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set announceInterval ($announce_interval) on port $port_num"

    if {![wrptp::${config_mode}_set_announce_interval\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -announce_interval $announce_interval]} {

        ERRLOG -msg "wrptp::dut_set_announce_interval : Unable to set announceInterval \
                    on specified port on DUT"
        return 0
    }

    return 1
}


#15.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_set_ipv4_address                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  ip_address        : (Mandatory) IPv4 address.                               #
#                                  (e.g., 192.168.200.1)                       #
#                                                                              #
#  net_mask          : (Mandatory) Subnet mask.                                #
#                                  (e.g., 255.255.255.0)                       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID.                                    #
#                                  (e.g., 100)                                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given IP address is set on specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given IP address could not be set on   #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function sets given IP address to a specific port  #
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_set_ipv4_address\                            #
#                          -port_num                    $port_num\             #
#                          -ip_address                  $ip_address\           #
#                          -net_mask                    $net_mask\             #
#                          -vlan_id                     $vlan_id               #
#                                                                              #
################################################################################

proc wrptp::dut_set_ipv4_address {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_set_ipv4_address : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-ip_address)]} {
        set ip_address $param(-ip_address)
    } else {
        ERRLOG -msg "wrptp::dut_set_ipv4_address : Missing Argument    -> ip_address"
        incr missedArgCounter
    }

    if {[info exists param(-net_mask)]} {
        set net_mask $param(-net_mask)
    } else {
        ERRLOG -msg "wrptp::dut_set_ipv4_address : Missing Argument    -> net_mask"
        incr missedArgCounter
    }

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "wrptp::dut_set_ipv4_address : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set IP address ($ip_address) on port $port_num"

    if {![wrptp::${config_mode}_set_ipv4_address \
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -ip_address         $ip_address\
                    -net_mask           $net_mask\
                    -vlan_id            $vlan_id]} {

        ERRLOG -msg "wrptp::dut_set_ipv4_address : Unable to set \
                    IP address on specified port on DUT"
        return 0
    }

    return 1
}


#16.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_set_wr_config                                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  value             : (Mandatory) Value for wrConfig.                         #
#                                  (e.g., "NON_WR"|"WR_S_ONLY"|"WR_M_ONLY"|    #
#                                         "WR_M_AND_S")                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value for wrConfig is set on     #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given value for wrConfig could not be  #
#                                    set on specifed port of the DUT).         #
#                                                                              #
#  DEFINITION        : This function sets given value for wrConfig to a        #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_set_wr_config\                               #
#                             -port_num                 $port_num\             #
#                             -value                    $value                 #
#                                                                              #
################################################################################

proc wrptp::dut_set_wr_config {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_set_wr_config : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-value)]} {
        set value $param(-value)
    } else {
        ERRLOG -msg "wrptp::dut_set_wr_config : Missing Argument    -> value"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set value ($value) for wrConfig on port $port_num"

    if {![wrptp::${config_mode}_set_wr_config\
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -value              $value]} {

        ERRLOG -msg "wrptp::dut_set_wr_config : Unable to set value for wrConfig on \
                    specified port on DUT"
        return 0
    }

    return 1
}


#17.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_set_known_delta_tx                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  known_delta_tx    : (Mandatory) Transmission fixed delay                    #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given transmission fixed delay is set  #
#                                    on specified port of the DUT).            #
#                                                                              #
#                      0 on Failure (If given transmission fixed delay could   #
#                                    not be set on specifed port of the DUT).  #
#                                                                              #
#  DEFINITION        : This function sets given transmission fixed delay to a  #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_set_known_delta_tx\                          #
#                             -port_num                 $port_num\             #
#                             -known_delta_tx           $known_delta_tx        #
#                                                                              #
################################################################################

proc wrptp::dut_set_known_delta_tx {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_set_known_delta_tx  : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-known_delta_tx)]} {
        set known_delta_tx $param(-known_delta_tx)
    } else {
        ERRLOG -msg "wrptp::dut_set_known_delta_tx  : Missing Argument    -> known_delta_tx"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set transmission fixed delay ($known_delta_tx ps) on port $port_num"

    if {![wrptp::${config_mode}_set_known_delta_tx \
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -known_delta_tx     $known_delta_tx]} {

        ERRLOG -msg "wrptp::dut_set_known_delta_tx : Unable to set \
                    transmission fixed delay on specified port on DUT"
        return 0
    }

    return 1
}

#18.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_set_known_delta_rx                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  known_delta_rx    : (Mandatory) Reception fixed delay                       #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given reception fixed delay is set on  #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given reception fixed delay could not  #
#                                    be set on specifed port of the DUT).      #
#                                                                              #
#  DEFINITION        : This function sets given reception fixed delay to a     #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_set_known_delta_rx\                          #
#                             -port_num                 $port_num\             #
#                             -known_delta_rx           $known_delta_rx        #
#                                                                              #
################################################################################

proc wrptp::dut_set_known_delta_rx {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_set_known_delta_rx  : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-known_delta_rx)]} {
        set known_delta_rx $param(-known_delta_rx)
    } else {
        ERRLOG -msg "wrptp::dut_set_known_delta_rx  : Missing Argument    -> known_delta_rx"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set reception fixed delay ($known_delta_rx ps) on port $port_num"

    if {![wrptp::${config_mode}_set_known_delta_rx \
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -known_delta_rx     $known_delta_rx]} {

        ERRLOG -msg "wrptp::dut_set_known_delta_rx : Unable to set \
                    reception fixed delay on specified port on DUT"
        return 0
    }

    return 1
}


#19.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_set_deltas_known                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#  deltas_known      : (Mandatory) Fixed delays.                               #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If fixed delays are known is enabled on   #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If fixed delays are known could not be    #
#                                    enabled on specifed port of the DUT).     #
#                                                                              #
#  DEFINITION        : This function enables fixed delays are known on a       #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_set_deltas_known\                            #
#                             -port_num                 $port_num\             #
#                             -deltas_known             $deltas_known          #
#                                                                              #
################################################################################

proc wrptp::dut_set_deltas_known {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_set_deltas_known : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-deltas_known)]} {
        set deltas_known $param(-deltas_known)
    } else {
        ERRLOG -msg "wrptp::dut_set_deltas_known : Missing Argument    -> deltas_known"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Enable fixed delays on port $port_num"

    if {![wrptp::${config_mode}_set_deltas_known \
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -deltas_known       $deltas_known]} {

        ERRLOG -msg "wrptp::dut_set_deltas_known : Unable to enable \
                     fixed delays on specified port on DUT"
        return 0
    }

    return 1
}


#20.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_set_cal_period                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  cal_period        : (Mandatory) calPeriod (in microseconds).                #
#                                  (e.g., 3000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given calPeriod is set on specified    #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given calPeriod could not be set on    #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function sets given calPeriod to a specific port on#
#                      the DUT.                                                #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_set_cal_period\                              #
#                             -port_num                 $port_num\             #
#                             -cal_period               $cal_period            #
#                                                                              #
################################################################################

proc wrptp::dut_set_cal_period {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_set_cal_period : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-cal_period)]} {
        set cal_period $param(-cal_period)
    } else {
        ERRLOG -msg "wrptp::dut_set_cal_period : Missing Argument    -> cal_period"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set calPeriod ($cal_period us) on port $port_num"

    if {![wrptp::${config_mode}_set_cal_period \
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -cal_period         $cal_period]} {

        ERRLOG -msg "wrptp::dut_set_cal_period : Unable to set \
                    calPeriod value on specified port on DUT"
        return 0
    }

    return 1
}


#21.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_set_cal_retry                                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  cal_retry         : (Mandatory) calRetry.                                   #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given calRetry is set on specified port#
#                                    of the DUT).                              #
#                                                                              #
#                      0 on Failure (If given calRetry could not be set on     #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function sets given calRetry on a specific port on #
#                      the DUT.                                                #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_set_cal_retry\                               #
#                             -port_num                 $port_num\             #
#                             -cal_retry                $cal_retry             #
#                                                                              #
################################################################################

proc wrptp::dut_set_cal_retry {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_set_cal_retry : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-cal_retry)]} {
        set cal_retry $param(-cal_retry)
    } else {
        ERRLOG -msg "wrptp::dut_set_cal_retry : Missing Argument    -> cal_retry"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set calRetry ($cal_retry) on port $port_num"

    if {![wrptp::${config_mode}_set_cal_retry \
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -cal_retry          $cal_retry]} {

        ERRLOG -msg "wrptp::dut_set_cal_retry : Unable to set \
                    calRetry on specified port on DUT"
        return 0
    }

    return 1
}


#22.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_port_wrptp_enable                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) Port on which wrptp is to be enabled.       #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If Wrptp is enabled on the specified port #
#                                    at the DUT).                              #
#                                                                              #
#                      0 on Failure (If Wrptp could not be enabled on the      #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function enables Wrptp on a specific port on the   #
#                      DUT.                                                    #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_port_wrptp_enable\                           #
#                             -port_num                 $port_num              #
#                                                                              #
################################################################################

proc wrptp::dut_port_wrptp_enable {args} {

    array set param $args
    global env

    set config_mode $env(DUT_CONFIG_MODE)
    set session_handler $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_port_wrptp_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Enable WRPTP on port $port_num"

    if {![wrptp::${config_mode}_port_wrptp_enable\
                           -session_handler  $session_handler\
                           -port_num         $port_num]} {

        ERRLOG -msg "wrptp::dut_port_wrptp_enable : Unable to enable PTP \
                    on specified port on DUT"
        return 0
    }

    return 1
}


#23.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_set_wr_present_timeout                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_PRESENT_TIMEOUT (in milliseconds).       #
#                                  (e.g., 1000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_PRESENT_TIMEOUT is set on     #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_PRESENT_TIMEOUT could not be  #
#                                    set on specifed port of the DUT).         #
#                                                                              #
#  DEFINITION        : This function sets given WR_PRESENT_TIMEOUT to a        #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::dut_set_wr_present_timeout\                     #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::dut_set_wr_present_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_set_wr_present_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        ERRLOG -msg "wrptp::dut_set_wr_present_timeout : Missing Argument    -> timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set WR_PRESENT_TIMEOUT ($timeout ms) on port $port_num"

    if {![wrptp::${config_mode}_set_wr_present_timeout \
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -timeout            $timeout]} {

        ERRLOG -msg "wrptp::dut_set_wr_present_timeout : Unable to set\
                    WR_PRESENT_TIMEOUT on specified port on DUT"
        return 0
    }

    return 1
}


#24.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_set_wr_m_lock_timeout                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_M_LOCK_TIMEOUT (in milliseconds).        #
#                                  (e.g., 1000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_M_LOCK_TIMEOUT is set on      #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_M_LOCK_TIMEOUT could not be   #
#                                    set on specifed port of the DUT).         #
#                                                                              #
#  DEFINITION        : This function sets given WR_M_LOCK_TIMEOUT to a         #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::dut_set_wr_m_lock_timeout\                      #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::dut_set_wr_m_lock_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_set_wr_m_lock_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        ERRLOG -msg "wrptp::dut_set_wr_m_lock_timeout : Missing Argument    -> timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set WR_M_LOCK_TIMEOUT ($timeout ms) on port $port_num"

    if {![wrptp::${config_mode}_set_wr_m_lock_timeout \
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -timeout            $timeout]} {

        ERRLOG -msg "wrptp::dut_set_wr_m_lock_timeout : Unable to set\
                    WR_M_LOCK_TIMEOUT on specified port on DUT"
        return 0
    }

    return 1
}


#25.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_set_wr_s_lock_timeout                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_S_LOCK_TIMEOUT (in milliseconds).        #
#                                  (e.g., 1000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_S_LOCK_TIMEOUT is set on      #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_S_LOCK_TIMEOUT could not be   #
#                                    set on specifed port of the DUT).         #
#                                                                              #
#  DEFINITION        : This function sets given WR_S_LOCK_TIMEOUT to a         #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::dut_set_wr_s_lock_timeout\                      #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::dut_set_wr_s_lock_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_set_wr_s_lock_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        ERRLOG -msg "wrptp::dut_set_wr_s_lock_timeout : Missing Argument    -> timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set WR_S_LOCK_TIMEOUT ($timeout ms) on port $port_num"

    if {![wrptp::${config_mode}_set_wr_s_lock_timeout \
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -timeout            $timeout]} {

        ERRLOG -msg "wrptp::dut_set_wr_s_lock_timeout : Unable to set\
                    WR_S_LOCK_TIMEOUT on specified port on DUT"
        return 0
    }

    return 1
}


#26.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_set_wr_locked_timeout                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_LOCKED_TIMEOUT (in milliseconds).        #
#                                  (e.g., 1000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_LOCKED_TIMEOUT is set on      #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_LOCKED_TIMEOUT could not be   #
#                                    set on specifed port of the DUT).         #
#                                                                              #
#  DEFINITION        : This function sets given WR_LOCKED_TIMEOUT to a         #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::dut_set_wr_locked_timeout\                      #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::dut_set_wr_locked_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_set_wr_locked_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        ERRLOG -msg "wrptp::dut_set_wr_locked_timeout : Missing Argument    -> timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set WR_LOCKED_TIMEOUT ($timeout ms) on port $port_num"

    if {![wrptp::${config_mode}_set_wr_locked_timeout \
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -timeout            $timeout]} {

        ERRLOG -msg "wrptp::dut_set_wr_locked_timeout : Unable to set\
                    WR_LOCKED_TIMEOUT on specified port on DUT"
        return 0
    }

    return 1
}


#27.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_set_wr_calibration_timeout                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_CALIBRATION_TIMEOUT (in milliseconds).   #
#                                  (e.g., 1000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_CALIBRATION_TIMEOUT is set on #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_CALIBRATION_TIMEOUT could not #
#                                    be set on specifed port of the DUT).      #
#                                                                              #
#  DEFINITION        : This function sets given WR_CALIBRATION_TIMEOUT to a    #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::dut_set_wr_calibration_timeout\                 #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::dut_set_wr_calibration_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_set_wr_calibration_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        ERRLOG -msg "wrptp::dut_set_wr_calibration_timeout : Missing Argument    -> timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set WR_CALIBRATION_TIMEOUT ($timeout ms) on port $port_num"

    if {![wrptp::${config_mode}_set_wr_calibration_timeout \
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -timeout            $timeout]} {

        ERRLOG -msg "wrptp::dut_set_wr_calibration_timeout : Unable to set\
                    WR_CALIBRATION_TIMEOUT on specified port on DUT"
        return 0
    }

    return 1
}


#28.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_set_wr_calibrated_timeout                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_CALIBRATED_TIMEOUT (in milliseconds).    #
#                                  (e.g., 1000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_CALIBRATED_TIMEOUT is set on  #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_CALIBRATED_TIMEOUT could not  #
#                                    be set on specifed port of the DUT).      #
#                                                                              #
#  DEFINITION        : This function sets given WR_CALIBRATED_TIMEOUT to a     #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::dut_set_wr_calibrated_timeout\                  #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::dut_set_wr_calibrated_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_set_wr_calibrated_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        ERRLOG -msg "wrptp::dut_set_wr_calibrated_timeout : Missing Argument    -> timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set WR_CALIBRATED_TIMEOUT ($timeout ms) on port $port_num"

    if {![wrptp::${config_mode}_set_wr_calibrated_timeout \
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -timeout            $timeout]} {

        ERRLOG -msg "wrptp::dut_set_wr_calibrated_timeout : Unable to set\
                    WR_CALIBRATED_TIMEOUT on specified port on DUT"
        return 0
    }

    return 1
}


#29.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_set_wr_resp_calib_req_timeout                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_RESP_CALIB_REQ_TIMEOUT (in milliseconds).#
#                                  (e.g., 1000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_RESP_CALIB_REQ_TIMEOUT is set #
#                                    on specified port of the DUT).            #
#                                                                              #
#                      0 on Failure (If given WR_RESP_CALIB_REQ_TIMEOUT could  #
#                                    not be set on specifed port of the DUT).  #
#                                                                              #
#  DEFINITION        : This function sets given WR_RESP_CALIB_REQ_TIMEOUT to a #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::dut_set_wr_resp_calib_req_timeout\              #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::dut_set_wr_resp_calib_req_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_set_wr_resp_calib_req_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        ERRLOG -msg "wrptp::dut_set_wr_resp_calib_req_timeout : Missing Argument    -> timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set WR_RESP_CALIB_REQ_TIMEOUT ($timeout ms) on port $port_num"

    if {![wrptp::${config_mode}_set_wr_resp_calib_req_timeout \
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -timeout            $timeout]} {

        ERRLOG -msg "wrptp::dut_set_wr_resp_calib_req_timeout : Unable to set\
                    WR_RESP_CALIB_REQ_TIMEOUT on specified port on DUT"
        return 0
    }

    return 1
}


#30.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_set_wr_state_retry                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  wr_state_retry    : (Mandatory) WR_STATE_RETRY.                             #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_STATE_RETRY is set on         #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_STATE_RETRY could not be set  #
#                                    on specifed port of the DUT).             #
#                                                                              #
#  DEFINITION        : This function sets given WR_STATE_RETRY to a specific   #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_set_wr_state_retry\                          #
#                             -port_num                 $port_num\             #
#                             -wr_state_retry           $wr_state_retry        #
#                                                                              #
################################################################################

proc wrptp::dut_set_wr_state_retry {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_set_wr_state_retry : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-wr_state_retry)]} {
        set wr_state_retry $param(-wr_state_retry)
    } else {
        ERRLOG -msg "wrptp::dut_set_wr_state_retry : Missing Argument    -> wr_state_retry"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set WR_STATE_RETRY ($wr_state_retry) on port $port_num"

    if {![wrptp::${config_mode}_set_wr_state_retry \
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -wr_state_retry     $wr_state_retry]} {

        ERRLOG -msg "wrptp::dut_set_wr_state_retry : Unable to set \
                    WR_STATE_RETRY on specified port on DUT"
        return 0
    }

    return 1
}



################################################################################
#                       DUT DISABLING FUNCTIONS                                #
################################################################################


#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_port_disable                                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) Port needs to be made administratively down.#
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If port is made administratively down)    #
#                                                                              #
#                      0 on Failure (If port could not be made                 #
#                                    administratively down)                    #
#                                                                              #
#  DEFINITION        : This function makes the specific port administratively  #
#                      down at the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::dut_port_disable\                                  #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc wrptp::dut_port_disable {args} {

    array set param $args
    global env

    set config_mode     $env(DUT_CONFIG_MODE)
    set session_handler $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_port_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Disable port $port_num"

    if {![wrptp::${config_mode}_port_disable\
                    -session_handler  $session_handler\
                    -port_num         $port_num]} {

        ERRLOG -msg "wrptp::dut_port_disable : Unable to disable \
                     specified port on DUT"
        return 0
    }

    return 1
}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_port_ptp_disable                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) Port on which ptp is to be disabled.        #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is disabled on the specified port  #
#                                    at the DUT).                              #
#                                                                              #
#                      0 on Failure (If PTP could not be disabled on the       #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function disables PTP on a specific port on the DUT#
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::dut_port_ptp_disable\                              #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc wrptp::dut_port_ptp_disable {args} {

    array set param $args
    global env

    set config_mode $env(DUT_CONFIG_MODE)
    set session_handler $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_port_ptp_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Disable PTP on port $port_num"

    if {![wrptp::${config_mode}_port_ptp_disable\
                           -session_handler  $session_handler\
                           -port_num         $port_num]} {

        ERRLOG -msg "wrptp::dut_port_ptp_disable : Unable to disable PTP \
                    on specified port on DUT"
        return 0
    }

    return 1
}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_reset_vlan                                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID with which VLAN has to be deleted.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#  port_num          : (Mandatory) Port which has to be removed from VLAN ID.  #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If port is removed from VLAN ID and VLAN  #
#                                    ID is deleted from the DUT).              #
#                                                                              #
#                      0 on Failure (If port could not be removed from VLAN ID #
#                                    and VLAN ID is not deledted from the DUT).#
#                                                                              #
#  DEFINITION        : This function removes port from VLAN ID and deletes VLAN#
#                      ID from the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::dut_reset_vlan\                                    #
#                             -vlan_id                  $vlan_id\              #
#                             -port_num                 $port_num              #
#                                                                              #
################################################################################

proc wrptp::dut_reset_vlan {args} {

    array set param $args
    global env

    set config_mode $env(DUT_CONFIG_MODE)
    set session_handler $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "wrptp::dut_reset_vlan : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_reset_vlan : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Delete VLAN $vlan_id and disassociate port $port_num"

    if {![wrptp::${config_mode}_reset_vlan\
                           -session_handler  $session_handler\
                           -vlan_id          $vlan_id\
                           -port_num         $port_num]} {

        ERRLOG -msg "wrptp::dut_reset_vlan : Unable to delete VLAN and disassociate port on DUT"
        return 0
    }

    return 1
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_reset_vlan_priority                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID                                     #
#                                  (e.g., 100)                                 #
#                                                                              #
#  vlan_priority     : (Optional)  VLAN priority                               #
#                                                                              #
#  port_num          : (Mandatory) Port which has to be associated to VLAN ID. #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If VLAN priority is reset on the VLAN ID  #
#                                    on the DUT).                              #
#                                                                              #
#                      0 on Failure (If VLAN priority could not reset on the   #
#                                    VLAN ID on the DUT).                      #
#                                                                              #
#  DEFINITION        : This function resets VLAN priority on the VLAN ID on the#
#                      DUT.                                                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::dut_reset_vlan_priority\                           #
#                             -vlan_id                  $vlan_id\              #
#                             -vlan_priority            $vlan_priority\        #
#                             -port_num                 $port_num              #
#                                                                              #
################################################################################

proc wrptp::dut_reset_vlan_priority {args} {

    array set param $args
    global env wrptp_env

    set config_mode     $env(DUT_CONFIG_MODE)
    set session_handler $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "wrptp::dut_reset_vlan_priority : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if {[info exists param(-vlan_priority)]} {
        set vlan_priority $param(-vlan_priority)
    } else {
        set vlan_priority  $::wrptp_vlan_priority
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_reset_vlan_priority : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset VLAN priority ($vlan_priority) to VLAN $vlan_id"

    if {![wrptp::${config_mode}_reset_vlan_priority\
                    -session_handler  $session_handler\
                    -vlan_id          $vlan_id\
                    -vlan_priority    $vlan_priority\
                    -port_num         $port_num]} {

        ERRLOG -msg "wrptp::dut_reset_vlan_priority : Unable to reset priority to VLAN on DUT"
        return 0
    }

    return 1
}


#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_global_ptp_disable                           #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is disabled on the DUT).           #
#                                                                              #
#                      0 on Failure (If PTP could not be disabled on the DUT). #
#                                                                              #
#  DEFINITION        : This function disables PTP on the DUT.                  #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::dut_global_ptp_disable                             #
#                                                                              #
################################################################################

proc wrptp::dut_global_ptp_disable {} {

    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    LOG -level 2 -msg "\t Disable PTP globally on DUT"

    if {![wrptp::${config_mode}_global_ptp_disable\
                           -session_handler  $session_handler]} {

        ERRLOG -msg "wrptp::dut_global_ptp_disable : Unable to disable PTP globally"
        return 0
    }

    return 1
}


#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_reset_communication_mode                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Optional)  Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  ptp_version       : (optional)  PTP version.                                #
#                                  (Default: 2)                                #
#                                                                              #
#  communication_mode : (Optional) Communication mode                          #
#                                  (e.g., "Unicast"|"Multicast")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If communication mode is reset on the     #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If communication mode could not be        #
#                                    reset on the specified port at the DUT).  #
#                                                                              #
#  DEFINITION        : This function resets the communication mode on a        #
#                      specific port of the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::dut_reset_communication_mode\                      #
#                             -port_num                $port_num\              #
#                             -clock_mode              $clock_mode\            #
#                             -ptp_version             $ptp_version\           #
#                             -communication_mode      $communication_mode     #
#                                                                              #
################################################################################

proc wrptp::dut_reset_communication_mode {args} {

    array set param $args
    global env wrptp_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_reset_communication_mode : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        set clock_mode $::ptp_device_type
    }

    if {[info exists param(-communication_mode)]} {
        set communication_mode $param(-communication_mode)
    } else {
        set communication_mode $::ptp_comm_model
    }

    if {[info exists param(-ptp_version)]} {
        set ptp_version $param(-ptp_version)
    } else {
        set ptp_version $::ptp_version
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset communication mode ($communication_mode) on port $port_num"

    if {![wrptp::${config_mode}_reset_communication_mode\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -clock_mode       $clock_mode\
                           -ptp_version      $ptp_version\
                           -communication_mode   $communication_mode]} {

        ERRLOG -msg "wrptp::dut_reset_communication_mode : \
                    Unable to reset PTP communication mode on specific port of the DUT"
        return 0
    }

    return 1
}


#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_reset_domain                                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  clock_mode        : (Optional)  Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  domain            : (Optional)  Domain number.                              #
#                                  (Default: 0)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If domain number is reset on the DUT).    #
#                                                                              #
#                      0 on Failure (If domain number could not able to reset  #
#                                    on DUT).                                  #
#                                                                              #
#  DEFINITION        : This function resets domain number on the DUT.          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::dut_reset_domain\                                  #
#                             -clock_mode      $clock_mode\                    #
#                             -domain          $domain                         #
#                                                                              #
################################################################################

proc wrptp::dut_reset_domain {args} {

    array set param $args
    global env wrptp_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        set clock_mode $::ptp_device_type
    }

    if {[info exists param(-domain)]} {
        set domain $param(-domain)
    } else {
        set domain $::ptp_dut_default_domain_number
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset domain ($domain) on DUT"

    if {![wrptp::${config_mode}_reset_domain\
                           -session_handler  $session_handler\
                           -clock_mode       $clock_mode\
                           -domain           $domain]} {

        ERRLOG -msg "wrptp::dut_reset_domain : Unable to reset domain on DUT"
        return 0
    }

    return 1
}


#8.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_reset_network_protocol                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Optional)  clock_mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  network_protocol  : (Optional)  Network protocol.                           #
#                                  (e.g., "IEEE 802.3"|"UDP/IPv4")             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If network protocol is reset on specified #
#                                    port at the DUT).                         #
#                                                                              #
#                      0 on Failure (If network protocol could not be reset    #
#                                    on specified port at the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets the network protocol on a specific #
#                      port of the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                     wrptp::dut_reset_network_protocol\                       #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -network_protocol $network_protocol              #
#                                                                              #
################################################################################

proc wrptp::dut_reset_network_protocol {args} {

    array set param $args
    global env wrptp_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_reset_network_protocol : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        set clock_mode $::ptp_device_type
    }

    if {[info exists param(-network_protocol)]} {
        set network_protocol $param(-network_protocol)
    } else {
        set network_protocol $::ptp_trans_type
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset network protocol ($network_protocol) on port $port_num"

    if {![wrptp::${config_mode}_reset_network_protocol\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -clock_mode       $clock_mode\
                           -network_protocol $network_protocol]} {

        ERRLOG -msg "wrptp::dut_reset_network_protocol : Unable to set network protocol \
                    on specified port at the DUT"
        return 0
    }

    return 1
}


#9.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_reset_clock_mode                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Optional)  Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP clock mode is reset on specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If PTP clock mode could not be reset on   #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function resets clock mode on a specific port of   #
#                      DUT.                                                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::dut_reset_clock_mode\                              #
#                             -port_num        $port_num\                      #
#                             -clock_mode      $clock_mode                     #
#                                                                              #
################################################################################

proc wrptp::dut_reset_clock_mode {args} {

    array set param $args
    global env wrptp_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_reset_clock_mode : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        set clock_mode $::ptp_device_type
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset clock mode ($clock_mode) on port $port_num"

    if {![wrptp::${config_mode}_reset_clock_mode\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -clock_mode       $clock_mode]} {

        ERRLOG -msg "wrptp::dut_reset_clock_mode : Unable to reset clock mode \
                    on specified port on DUT"
        return 0
    }

    return 1
}


#10.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_reset_clock_step                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Optional)  Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  clock_step        : (Optional)  Clock step.                                 #
#                                  (e.g., "One-step"|"Two-step")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If clock-step is reset on the specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If clock-step could not be able to reset  #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets the clock-step on a specific port  #
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::dut_reset_clock_step\                              #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -clock_step       $clock_step                    #
#                                                                              #
################################################################################

proc wrptp::dut_reset_clock_step {args} {

    array set param $args
    global env wrptp_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_reset_clock_step : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        set clock_mode $::ptp_device_type
    }

    if {[info exists param(-clock_step)]} {
        set clock_step $param(-clock_step)
    } else {
        set clock_step $::ptp_dut_clock_step
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset clock step ($clock_step) on port $port_num"

    if {![wrptp::${config_mode}_reset_clock_step\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -clock_mode       $clock_mode\
                           -clock_step       $clock_step]} {

        ERRLOG -msg "wrptp::dut_reset_clock_step : Unable to reset clock step \
                    on specified port on DUT"
        return 0
    }

    return 1
}


#11.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_reset_delay_mechanism                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Optional)  Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  delay_mechanism   : (Optional)  Delay mechanism.                            #
#                                  (e.g., e2e|p2p|disable)                     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If delay mechanism is reset on specified  #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If delay mechanism could not be reset on  #
#                                    specified PTP port of the DUT).           #
#                                                                              #
#  DEFINITION        : This function resets the delay mechanism to a specific  #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::dut_reset_delay_mechanism\                         #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -delay_mechanism  $delay_mechanism               #
#                                                                              #
################################################################################

proc wrptp::dut_reset_delay_mechanism {args} {

    array set param $args
    global env wrptp_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_reset_delay_mechanism : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        set clock_mode $::ptp_device_type
    }

    if {[info exists param(-delay_mechanism)]} {
        set delay_mechanism $param(-delay_mechanism)
    } else {
        set delay_mechanism $::ptp_dut_delay_mechanism
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset delay mechanism ($delay_mechanism) on port $port_num"

    if {![wrptp::${config_mode}_reset_delay_mechanism\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -clock_mode       $clock_mode\
                           -delay_mechanism  $delay_mechanism]} {

        ERRLOG -msg "wrptp::dut_reset_delay_mechanism : Unable to reset delay mechanism\
                    on specified port on DUT"
        return 0
    }

    return 1
}


#12.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_reset_priority1                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  clock_mode        : (Optional)  Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority1         : (Optional)  Prority1                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority1 is reset on the DUT).        #
#                                                                              #
#                      0 on Failure (If priority1 could not be able to reset on#
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function resets the priority1 on the DUT.          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::dut_reset_priority1\                               #
#                             -clock_mode      $clock_mode\                    #
#                             -priority1       $priority1                      #
#                                                                              #
################################################################################

proc wrptp::dut_reset_priority1 {args} {

    array set param $args
    global env wrptp_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        set clock_mode $::ptp_device_type
    }

    if {[info exists param(-priority1)]} {
        set priority1  $param(-priority1)
    } else {
        set priority1  $::wrptp_priority1
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset priority1 ($priority1) on DUT"

    if {![wrptp::${config_mode}_reset_priority1\
                           -session_handler  $session_handler\
                           -clock_mode       $clock_mode\
                           -priority1        $priority1]} {

        ERRLOG  -msg "wrptp::dut_reset_priority1: Unable to reset priority1 on the DUT"
        return 0
    }

    return 1
}


#13.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_reset_priority2                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  clock_mode        : (Optional)  Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority2         : (Optional)  Prority2                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority2 is reset on the DUT).        #
#                                                                              #
#                      0 on Failure (If priority2 could not be able to reset on#
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function resets the priority2 on the DUT.          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::dut_reset_priority2\                               #
#                             -clock_mode      $clock_mode\                    #
#                             -priority2       $priority2                      #
#                                                                              #
################################################################################

proc wrptp::dut_reset_priority2 {args} {

    array set param $args
    global env wrptp_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        set clock_mode $::ptp_device_type
    }

    if {[info exists param(-priority2)]} {
        set priority2  $param(-priority2)
    } else {
        set priority2  $::wrptp_priority2
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset priority2 ($priority2) on DUT"

    if {![wrptp::${config_mode}_reset_priority2\
                           -session_handler  $session_handler\
                           -clock_mode       $clock_mode\
                           -priority2        $priority2]} {

        ERRLOG  -msg "wrptp::dut_reset_priority2: Unable to reset priority2 on the DUT"
        return 0
    }

    return 1
}


#14.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_reset_announce_interval                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0).                              #
# announce_interval  : (optional)  announceInterval.                           #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If announceInterval value is reset on the #
#                                    specified port of DUT).                   #
#                                                                              #
#                      0 on Failure (If announceInterval could not be able to  #
#                                    reset on specified port of the DUT).      #
#                                                                              #
#  DEFINITION        : This function resets the announceInterval value on the  #
#                      specified port of DUT.                                  #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_reset_announce_interval\                     #
#                             -port_num                 $port_num\             #
#                             -announce_interval        $announce_interval     #
#                                                                              #
################################################################################

proc wrptp::dut_reset_announce_interval {args} {

    array set param $args
    global env wrptp_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_reset_announce_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-announce_interval)]} {
        set announce_interval $param(-announce_interval)
    } else {
        set announce_interval $::wrptp_announce_interval
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set announceInterval ($announce_interval) on port $port_num"

    if {![wrptp::${config_mode}_reset_announce_interval\
                           -session_handler   $session_handler\
                           -port_num          $port_num\
                           -announce_interval $announce_interval]} {

        ERRLOG -msg "wrptp::dut_reset_announce_interval : Unable to reset announceInterval \
                    on specified port on DUT"
        return 0
    }

    return 1
}


#15.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_reset_ipv4_address                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  ip_address        : (Mandatory) IPv4 address.                               #
#                                  (e.g., 192.168.200.1)                       #
#                                                                              #
#  net_mask          : (Mandatory) Subnet mask.                                #
#                                  (e.g., 255.255.255.0)                       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID.                                    #
#                                  (e.g., 100)                                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given IP address is reset on specified #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given IP address could not be reset on #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function resets given IP address to a specific port#
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::dut_reset_ipv4_address\                            #
#                          -port_num              $port_num\                   #
#                          -ip_address            $ip_address\                 #
#                          -net_mask              $net_mask\                   #
#                          -vlan_id               $vlan_id                     #
#                                                                              #
################################################################################

proc wrptp::dut_reset_ipv4_address {args} {

    array set param $args
    global env wrptp_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_reset_ipv4_address : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-ip_address)]} {
        set ip_address $param(-ip_address)
    } else {
        ERRLOG -msg "wrptp::dut_reset_ipv4_address : Missing Argument    -> ip_address"
        incr missedArgCounter
    }

    if {[info exists param(-net_mask)]} {
        set net_mask $param(-net_mask)
    } else {
        ERRLOG -msg "wrptp::dut_reset_ipv4_address : Missing Argument    -> net_mask"
        incr missedArgCounter
    }

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "wrptp::dut_reset_ipv4_address : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset IP address ($ip_address) on port $port_num"

    if {![wrptp::${config_mode}_reset_ipv4_address \
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -ip_address         $ip_address\
                    -net_mask           $net_mask\
                    -vlan_id            $vlan_id]} {

        ERRLOG -msg "wrptp::dut_reset_ipv4_address : Unable to set \
                    IP address on specified port on DUT"
        return 0
    }

    return 1
}

#16.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_reset_wr_config                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  value             : (Mandatory) Value for wrConfig.                         #
#                                  (e.g., "NON_WR"|"WR_S_ONLY"|"WR_M_ONLY"|    #
#                                         "WR_M_AND_S")                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value for wrConfig is reset on   #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given value for wrConfig could not be  #
#                                    reset on specifed port of the DUT).       #
#                                                                              #
#  DEFINITION        : This function resets given value for wrConfig to a      #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_reset_wr_config\                             #
#                             -port_num                 $port_num\             #
#                             -value                    $value                 #
#                                                                              #
################################################################################

proc wrptp::dut_reset_wr_config {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_reset_wr_config : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-value)]} {
        set value $param(-value)
    } else {
        set value $::wrptp_wrconfig
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset value ($value) for wrConfig on port $port_num"

    if {![wrptp::${config_mode}_reset_wr_config\
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -value              $value]} {

        ERRLOG -msg "wrptp::dut_reset_wr_config : Unable to reset value for wrConfig on \
                    specified port on DUT"
        return 0
    }

    return 1
}


#17.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_reset_known_delta_tx                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  known_delta_tx    : (optional)  Transmission fixed delay                    #
#                                  (default: 0)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given transmission fixed delay is reset#
#                                    on specified port of the DUT).            #
#                                                                              #
#                      0 on Failure (If given transmission fixed delay could   #
#                                    not be reset on specifed port of the DUT).#
#                                                                              #
#  DEFINITION        : This function resets given transmission fixed delay to a#
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_reset_known_delta_tx\                        #
#                             -port_num                 $port_num\             #
#                             -known_delta_tx           $known_delta_tx        #
#                                                                              #
################################################################################

proc wrptp::dut_reset_known_delta_tx {args} {

    array set param $args
    global env wrptp_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_reset_known_delta_tx  : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-known_delta_tx)]} {
        set known_delta_tx $param(-known_delta_tx)
    } else {
        set known_delta_tx $::wrptp_known_delta_tx
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset transmission fixed delay ($known_delta_tx ps) on port $port_num"

    if {![wrptp::${config_mode}_reset_known_delta_tx \
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -known_delta_tx     $known_delta_tx]} {

        ERRLOG -msg "wrptp::dut_reset_known_delta_tx : Unable to reset \
                     transmission fixed delay on specified port on DUT"
        return 0
    }

    return 1
}

#18.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_reset_known_delta_rx                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  known_delta_rx    : (Optional)  Reception fixed delay                       #
#                                  (default: 0)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given reception fixed delay is reset on#
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given reception fixed delay could not  #
#                                    be reset on specifed port of the DUT).    #
#                                                                              #
#  DEFINITION        : This function resets given reception fixed delay to a   #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_reset_known_delta_rx\                        #
#                             -port_num                 $port_num\             #
#                             -known_delta_rx           $known_delta_rx        #
#                                                                              #
################################################################################

proc wrptp::dut_reset_known_delta_rx {args} {

    array set param $args
    global env wrptp_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_reset_known_delta_rx  : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-known_delta_rx)]} {
        set known_delta_rx $param(-known_delta_rx)
    } else {
        set known_delta_rx $::wrptp_known_delta_rx
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset reception fixed delay ($known_delta_rx ps) on port $port_num"

    if {![wrptp::${config_mode}_reset_known_delta_rx \
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -known_delta_rx     $known_delta_rx]} {

        ERRLOG -msg "wrptp::dut_reset_known_delta_rx : Unable to reset \
                     reception fixed delay on specified port on DUT"
        return 0
    }

    return 1
}


#19.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_reset_deltas_known                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#  deltas_known      : (Mandatory) Fixed Delays.                               #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If fixed delays is disabled on            #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If fixed delays could not be              #
#                                    disabled on specifed port of the DUT).    #
#                                                                              #
#  DEFINITION        : This function disables fixed delays are known on a      #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_reset_deltas_known\                          #
#                             -port_num                 $port_num\             #
#                             -deltas_known             $deltas_known          #
#                                                                              #
################################################################################

proc wrptp::dut_reset_deltas_known {args} {

    array set param $args
    global env wrptp_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_reset_deltas_known : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-deltas_known)]} {
        set deltas_known $param(-deltas_known)
    } else {
        set deltas_known $::wrptp_deltas_known
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Disable fixed delays are known on port $port_num"

    if {![wrptp::${config_mode}_reset_deltas_known \
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -deltas_known       $deltas_known]} {

        ERRLOG -msg "wrptp::dut_reset_deltas_known : Unable to disable \
                     fixed delays are known on specified port on DUT"
        return 0
    }

    return 1
}


#20.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_reset_cal_period                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  cal_period        : (Optional)  calPeriod (in microseconds).                #
#                                  (default: 3000)                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given calPeriod is reset on specified  #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given calPeriod could not be reset on  #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function resets given calPeriod to a specific port #
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_reset_cal_period\                            #
#                             -port_num                 $port_num\             #
#                             -cal_period               $cal_period            #
#                                                                              #
################################################################################

proc wrptp::dut_reset_cal_period {args} {

    array set param $args
    global env wrptp_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_reset_cal_period : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-cal_period)]} {
        set cal_period $param(-cal_period)
    } else {
        set cal_period $::wrptp_cal_period
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset calPeriod ($cal_period us) on port $port_num"

    if {![wrptp::${config_mode}_reset_cal_period \
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -cal_period         $cal_period]} {

        ERRLOG -msg "wrptp::dut_reset_cal_period : Unable to reset \
                    calPeriod value on specified port on DUT"
        return 0
    }

    return 1
}


#21.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_reset_cal_retry                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  cal_retry         : (Optional)  calRetry.                                   #
#                                  (default: 3)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given calRetry is reset on specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given calRetry could not be reset on   #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function resets given calRetry on a specific port  #
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_reset_cal_retry\                             #
#                             -port_num                 $port_num\             #
#                             -cal_retry                $cal_retry             #
#                                                                              #
################################################################################

proc wrptp::dut_reset_cal_retry {args} {

    array set param $args
    global env wrptp_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_reset_cal_retry : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-cal_retry)]} {
        set cal_retry $param(-cal_retry)
    } else {
        set cal_retry $::wrptp_cal_retry
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset calRetry ($cal_retry) on port $port_num"

    if {![wrptp::${config_mode}_reset_cal_retry \
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -cal_retry          $cal_retry]} {

        ERRLOG -msg "wrptp::dut_reset_cal_retry : Unable to reset \
                    calRetry on specified port on DUT"
        return 0
    }

    return 1
}


#22.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_port_wrptp_disable                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) Port on which wrptp is to be disabled.      #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If Wrptp is disabled on the specified port#
#                                    at the DUT).                              #
#                                                                              #
#                      0 on Failure (If Wrptp could not be disabled on the     #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function disables Wrptp on a specific port on the  #
#                      DUT                                                     #
#  USAGE             :                                                         #
#                                                                              #
#                    wrptp::dut_port_wrptp_disable\                            #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc wrptp::dut_port_wrptp_disable {args} {

    array set param $args
    global env

    set config_mode $env(DUT_CONFIG_MODE)
    set session_handler $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_port_wrptp_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Disable WRPTP on port $port_num"

    if {![wrptp::${config_mode}_port_wrptp_disable\
                           -session_handler  $session_handler\
                           -port_num         $port_num]} {

        ERRLOG -msg "wrptp::dut_port_wrptp_disable : Unable to disable Wrptp \
                    on specified port on DUT"
        return 0
    }

    return 1
}


#23.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_reset_wr_present_timeout                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Optional)  WR_PRESENT_TIMEOUT (in milliseconds).       #
#                                  (Default: 1000)                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_PRESENT_TIMEOUT is reset on   #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_PRESENT_TIMEOUT could not be  #
#                                    reset on specifed port of the DUT).       #
#                                                                              #
#  DEFINITION        : This function resets given WR_PRESENT_TIMEOUT to a      #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::dut_reset_wr_present_timeout\                   #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::dut_reset_wr_present_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_reset_wr_present_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        set timeout $::wrptp_present_timeout
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset WR_PRESENT_TIMEOUT ($timeout ms) on port $port_num"

    if {![wrptp::${config_mode}_reset_wr_present_timeout \
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -timeout            $timeout]} {

        ERRLOG -msg "wrptp::dut_reset_wr_present_timeout : Unable to reset\
                    WR_PRESENT_TIMEOUT on specified port on DUT"
        return 0
    }

    return 1
}


#24.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_reset_wr_m_lock_timeout                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Optional)  WR_M_LOCK_TIMEOUT (in milliseconds).        #
#                                  (Default: 15000)                            #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_M_LOCK_TIMEOUT is reset on    #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_M_LOCK_TIMEOUT could not be   #
#                                    reset on specifed port of the DUT).       #
#                                                                              #
#  DEFINITION        : This function resets given WR_M_LOCK_TIMEOUT to a       #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::dut_reset_wr_m_lock_timeout\                    #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::dut_reset_wr_m_lock_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_reset_wr_m_lock_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        set timeout $::wrptp_m_lock_timeout
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset WR_M_LOCK_TIMEOUT ($timeout ms) on port $port_num"

    if {![wrptp::${config_mode}_reset_wr_m_lock_timeout \
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -timeout            $timeout]} {

        ERRLOG -msg "wrptp::dut_reset_wr_m_lock_timeout : Unable to reset\
                    WR_M_LOCK_TIMEOUT on specified port on DUT"
        return 0
    }

    return 1
}


#25.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_reset_wr_s_lock_timeout                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Optional)  WR_S_LOCK_TIMEOUT (in milliseconds).        #
#                                  (Default: 15000)                            #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_S_LOCK_TIMEOUT is reset on    #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_S_LOCK_TIMEOUT could not be   #
#                                    reset on specifed port of the DUT).       #
#                                                                              #
#  DEFINITION        : This function resets given WR_S_LOCK_TIMEOUT to a       #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::dut_reset_wr_s_lock_timeout\                    #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::dut_reset_wr_s_lock_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_reset_wr_s_lock_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        set timeout $::wrptp_s_lock_timeout
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset WR_S_LOCK_TIMEOUT ($timeout ms) on port $port_num"

    if {![wrptp::${config_mode}_reset_wr_s_lock_timeout \
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -timeout            $timeout]} {

        ERRLOG -msg "wrptp::dut_reset_wr_s_lock_timeout : Unable to reset\
                    WR_S_LOCK_TIMEOUT on specified port on DUT"
        return 0
    }

    return 1
}


#26.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_reset_wr_locked_timeout                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Optional)  WR_LOCKED_TIMEOUT (in milliseconds).        #
#                                  (Default: 300)                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_LOCKED_TIMEOUT is reset on    #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_LOCKED_TIMEOUT could not be   #
#                                    reset on specifed port of the DUT).       #
#                                                                              #
#  DEFINITION        : This function resets given WR_LOCKED_TIMEOUT to a       #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::dut_reset_wr_locked_timeout\                    #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::dut_reset_wr_locked_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_reset_wr_locked_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        set timeout $::wrptp_locked_timeout
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset WR_LOCKED_TIMEOUT ($timeout ms) on port $port_num"

    if {![wrptp::${config_mode}_reset_wr_locked_timeout \
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -timeout            $timeout]} {

        ERRLOG -msg "wrptp::dut_reset_wr_locked_timeout : Unable to reset\
                    WR_LOCKED_TIMEOUT on specified port on DUT"
        return 0
    }

    return 1
}


#27.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_reset_wr_calibration_timeout                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Optional)  WR_CALIBRATION_TIMEOUT (in milliseconds).   #
#                                  (Default: 3000)                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_CALIBRATION_TIMEOUT is reset  #
#                                    on specified port of the DUT).            #
#                                                                              #
#                      0 on Failure (If given WR_CALIBRATION_TIMEOUT could not #
#                                    be reset on specifed port of the DUT).    #
#                                                                              #
#  DEFINITION        : This function resets given WR_CALIBRATION_TIMEOUT to a  #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::dut_reset_wr_calibration_timeout\               #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::dut_reset_wr_calibration_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_reset_wr_calibration_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        set timeout $::wrptp_calibration_timeout
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset WR_CALIBRATION_TIMEOUT ($timeout ms) on port $port_num"

    if {![wrptp::${config_mode}_reset_wr_calibration_timeout \
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -timeout            $timeout]} {

        ERRLOG -msg "wrptp::dut_reset_wr_calibration_timeout : Unable to reset\
                    WR_CALIBRATION_TIMEOUT on specified port on DUT"
        return 0
    }

    return 1
}


#28.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_reset_wr_calibrated_timeout                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Optional)  WR_CALIBRATED_TIMEOUT (in milliseconds).    #
#                                  (Default: 300)                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_CALIBRATED_TIMEOUT is reset on#
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_CALIBRATED_TIMEOUT could not  #
#                                    be reset on specifed port of the DUT).    #
#                                                                              #
#  DEFINITION        : This function resets given WR_CALIBRATED_TIMEOUT to a   #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::dut_reset_wr_calibrated_timeout\                #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::dut_reset_wr_calibrated_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_reset_wr_calibrated_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        set timeout $::wrptp_calibrated_timeout
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset WR_CALIBRATED_TIMEOUT ($timeout ms) on port $port_num"

    if {![wrptp::${config_mode}_reset_wr_calibrated_timeout \
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -timeout            $timeout]} {

        ERRLOG -msg "wrptp::dut_reset_wr_calibrated_timeout : Unable to reset\
                    WR_CALIBRATED_TIMEOUT on specified port on DUT"
        return 0
    }

    return 1
}


#29.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_reset_wr_resp_calib_req_timeout              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Optional)  WR_RESP_CALIB_REQ_TIMEOUT (in milliseconds).#
#                                  (Default: 3)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_RESP_CALIB_REQ_TIMEOUT is     #
#                                    reset on specified port of the DUT).      #
#                                                                              #
#                      0 on Failure (If given WR_RESP_CALIB_REQ_TIMEOUT could  #
#                                    not be reset on specifed port of the DUT).#
#                                                                              #
#  DEFINITION        : This function resets given WR_RESP_CALIB_REQ_TIMEOUT to #
#                      a specific port on the DUT.                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::dut_reset_wr_resp_calib_req_timeout\            #
#                              -port_num       $port_num\                      #
#                              -timeout        $timeout                        #
#                                                                              #
################################################################################

proc wrptp::dut_reset_wr_resp_calib_req_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_reset_wr_resp_calib_req_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-timeout)]} {
        set timeout $param(-timeout)
    } else {
        set timeout $::wrptp_resp_calib_req_timeout
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset WR_RESP_CALIB_REQ_TIMEOUT ($timeout ms) on port $port_num"

    if {![wrptp::${config_mode}_reset_wr_resp_calib_req_timeout \
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -timeout            $timeout]} {

        ERRLOG -msg "wrptp::dut_reset_wr_resp_calib_req_timeout : Unable to reset\
                    WR_RESP_CALIB_REQ_TIMEOUT on specified port on DUT"
        return 0
    }

    return 1
}


#30.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_reset_wr_state_retry                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  wr_state_retry    : (Optional)  WR_STATE_RETRY.                             #
#                                  (default: 3)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given WR_STATE_RETRY is reset on       #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given WR_STATE_RETRY could not be reset#
#                                    on specifed port of the DUT).             #
#                                                                              #
#  DEFINITION        : This function resets given WR_STATE_RETRY to a specific #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_reset_wr_state_retry\                        #
#                             -port_num                 $port_num\             #
#                             -wr_state_retry           $wr_state_retry        #
#                                                                              #
################################################################################

proc wrptp::dut_reset_wr_state_retry {args} {

    array set param $args
    global env wrptp_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_reset_wr_state_retry : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-wr_state_retry)]} {
        set wr_state_retry $param(-wr_state_retry)
    } else {
        set wr_state_retry $::wrptp_state_retry
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset WR_STATE_RETRY ($wr_state_retry) on port $port_num"

    if {![wrptp::${config_mode}_reset_wr_state_retry \
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -wr_state_retry     $wr_state_retry]} {

        ERRLOG -msg "wrptp::dut_reset_wr_state_retry : Unable to reset \
                     WR_STATE_RETRY on specified port on DUT"
        return 0
    }

    return 1
}


#31.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_commit_changes                               #
#                                                                              #
#  INPUT PARAMETERS   : NONE.                                                  #
#                                                                              #
#  OUTPUT PARAMETERS  : NONE.                                                  #
#                                                                              #
#  RETURNS           : 1 on success                                            #
#                                                                              #
#                      0 on failure                                            #
#                                                                              #
#  DEFINITION        : This function commits changes in dot-config in DUT      #
#                                                                              #
#                                                                              #
# USAGE              :                                                         #
#                                                                              #
#     wrptp::dut_commit_changes				                                   #
#                                                                              #
################################################################################

proc wrptp::dut_commit_changes {} {

	global env wrptp_env

	set config_mode $env(DUT_CONFIG_MODE)
	set session_handler  $::dut_session_id

	LOG -level 0 -msg "Commit all changes in the DUT\n"

	if {![wrptp::${config_mode}_commit_changes\
				-session_handler  $session_handler] } {

		ERRLOG -msg "wrptp::dut_commit_changes : Unable to commit"
		return 0
	}

	set env(TEE_ATTEST_MANUAL_PROMPT)      1

	return 1
}

################################################################################
#                           DUT CHECK FUNCTIONS                                #
################################################################################

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_check_wr_config                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  value             : (Mandatory) Value for wrConfig.                         #
#                                  (e.g., "NON_WR"|"WR_S_ONLY"|"WR_M_ONLY"|    #
#                                         "WR_M_AND_S")                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given state of wrConfig is verified for#
#                                    the specified port).                      #
#                                                                              #
#                      0 on Failure (If given state of wrConfig could not be   #
#                                    verified for the specified port).         #
#                                                                              #
#  DEFINITION        : This function verifies the given state of               #
#                       for a wrConfig on the specified port.                  #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_check_wr_config\                             #
#                             -port_num           $port_num\                   #
#                             -value              $value                       #
#                                                                              #
################################################################################

proc wrptp::dut_check_wr_config {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_check_wr_config : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-value)]} {
        set value $param(-value)
    } else {
        ERRLOG -msg "wrptp::dut_check_wr_config : Missing Argument    -> value"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Check wrConfig ($value) on port $port_num"

    if {![wrptp::${config_mode}_check_wr_config\
                    -session_handler  $session_handler\
                    -port_num         $port_num\
                    -value            $value]} {

        ERRLOG -msg "wrptp::dut_check_wr_config : Unable to check \
                     wrConfig on specified port \
                     on DUT"
        return 0
    }

    return 1
}

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_check_ptp_port_state                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  state             : (Mandatory) portState                                   #
#                                  (e.g., "MASTER"|"SLAVE")                    #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given portState is verified for the    #
#                                         "MASTER"|"SLAVE" on specified port). #
#                                                                              #
#                      0 on Failure (If given portState could not be verified  #
#                                    for the specified port).                  #
#                                                                              #
#  DEFINITION        : This function verifies the given portState on the       #
#                      specified port.                                         #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                        wrptp::dut_check_ptp_port_state\                      #
#                               -port_num           $port_num\                 #
#                               -state              $state                     #
#                                                                              #
################################################################################

proc wrptp::dut_check_ptp_port_state {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_check_ptp_port_state : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-state)]} {
        set state $param(-state)
    } else {
        ERRLOG -msg "wrptp::dut_check_ptp_port_state : Missing Argument    -> state"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Check PTP portState ($state) on port $port_num"

    if {![wrptp::${config_mode}_check_ptp_port_state\
                    -session_handler  $session_handler\
                    -port_num         $port_num\
                    -state            $state]} {

        ERRLOG -msg "wrptp::dut_check_ptp_port_state : Unable to check PTP\
                     portState on specified port \
                     on DUT"
        return 0
    }

    return 1
}

#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_check_wr_port_state                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  wr_port_state     : (Mandatory) wrPortState                                 #
#                                  (e.g., "IDLE"|"PRESENT"|"M_LOCK"|"S_LOCK"|  #
#                                         "LOCKED"|"CALIBRATION"|"CALIBRATED"| #
#                                         "RESP_CALIB_REQ"|"WR_LINK_ON")       #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given wrPortState is verified for the  #
#                                    specified port).                          #
#                                                                              #
#                      0 on Failure (If given wrPortState could not be verified#
#                                    for the specified port).                  #
#                                                                              #
#  DEFINITION        : This function verifies the given wrPortState on the     #
#                      specified port.                                         #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                        wrptp::dut_check_wr_port_state\                       #
#                               -port_num           $port_num\                 #
#                               -wr_port_state      $wr_port_state             #
#                                                                              #
################################################################################

proc wrptp::dut_check_wr_port_state {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_check_wr_port_state : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-wr_port_state)]} {
        set wr_port_state $param(-wr_port_state)
    } else {
        ERRLOG -msg "wrptp::dut_check_wr_port_state : Missing Argument    -> wr_port_state"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Check wrPortState ($wr_port_state) on port $port_num"

    if {![wrptp::${config_mode}_check_wr_port_state\
                    -session_handler  $session_handler\
                    -port_num         $port_num\
                    -wr_port_state    $wr_port_state]} {

        ERRLOG -msg "wrptp::dut_check_wr_port_state : Unable to check \
                     wrPortState on specified port \
                     on DUT"
        return 0
    }

    return 1
}

#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_check_other_port_delta_tx                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num                : (Mandatory) DUT's port number.                    #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  other_port_delta_tx     : (Mandatory) Value of deltaTx                      #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of Delta Tx is verified for#
#                                    the specified port).                      #
#                                                                              #
#                      0 on Failure (If given value of Delta Tx could not be   #
#                                    verified for the specified port).         #
#                                                                              #
#  DEFINITION        : This function verifies the value of Delta Tx on the     #
#                      specified port.                                         #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_check_other_port_delta_tx\                   #
#                              -port_num               $port_num\              #
#                              -other_port_delta_tx    $other_port_delta_tx    #
#                                                                              #
################################################################################

proc wrptp::dut_check_other_port_delta_tx {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_check_other_port_delta_tx : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-other_port_delta_tx)]} {
        set other_port_delta_tx $param(-other_port_delta_tx)
    } else {
        ERRLOG -msg "wrptp::dut_check_other_port_delta_tx : Missing Argument    -> other_port_delta_tx"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Check value of otherPortDeltaTx ($other_port_delta_tx ps) on port $port_num"

    if {![wrptp::${config_mode}_check_other_port_delta_tx\
                    -session_handler           $session_handler\
                    -port_num                  $port_num\
                    -other_port_delta_tx       $other_port_delta_tx]} {

        ERRLOG -msg "wrptp::dut_check_other_port_delta_tx : Unable to check \
                     value of otherPortDeltaTx on specified port \
                     on DUT"
        return 0
    }

    return 1
}

#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_check_other_port_delta_rx                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num                : (Mandatory) DUT's port number.                    #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  other_port_delta_rx     : (Mandatory) Value of deltaRx                      #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of deltaRx is verified for #
#                                    the specified port).                      #
#                                                                              #
#                      0 on Failure (If given  value of deltaRx could not be   #
#                                    verified for the specified port).         #
#                                                                              #
#  DEFINITION        : This function verifies value of DeltaRx on the specified#
#                      port.                                                   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                       wrptp::dut_check_other_port_delta_rx\                  #
#                              -port_num                $port_num\             #
#                              -other_port_delta_rx     $other_port_delta_rx   #
#                                                                              #
################################################################################

proc wrptp::dut_check_other_port_delta_rx {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_check_other_port_delta_rx : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-other_port_delta_rx)]} {
        set other_port_delta_rx $param(-other_port_delta_rx)
    } else {
        ERRLOG -msg "wrptp::dut_check_other_port_delta_rx : Missing Argument    -> other_port_delta_rx"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Check value of otherPortDeltaRx ($other_port_delta_rx ps) on port $port_num"

    if {![wrptp::${config_mode}_check_other_port_delta_rx\
                    -session_handler           $session_handler\
                    -port_num                  $port_num\
                    -other_port_delta_rx       $other_port_delta_rx]} {

        ERRLOG -msg "wrptp::dut_check_other_port_delta_rx : Unable to check \
                     value of otherPortDeltaRx on specified port \
                     on DUT"
        return 0
    }

    return 1
}


#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_check_other_port_cal_period                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num                : (Mandatory) DUT's port number.                    #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  other_port_cal_period   : (Mandatory) Value of the calPeriod                #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of the calPeriod is        #
#                                    verified for the specified port).         #
#                                                                              #
#                      0 on Failure (If given value of the calPeriod could not #
#                                    be verified for the specified port).      #
#                                                                              #
#  DEFINITION        : This function verifies the value of the calPeriod on the#
#                      specified port.                                         #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_check_other_port_cal_period\                 #
#                             -port_num                 $port_num\             #
#                             -other_port_cal_period    $other_port_cal_period #
#                                                                              #
################################################################################

proc wrptp::dut_check_other_port_cal_period {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_check_other_port_cal_period : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-other_port_cal_period)]} {
        set other_port_cal_period $param(-other_port_cal_period)
    } else {
        ERRLOG -msg "wrptp::dut_check_other_port_cal_period : Missing Argument    -> other_port_cal_period"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Check value of otherPortCalPeriod ($other_port_cal_period us) on port $port_num"

    if {![wrptp::${config_mode}_check_other_port_cal_period\
                    -session_handler           $session_handler\
                    -port_num                  $port_num\
                    -other_port_cal_period     $other_port_cal_period]} {

        ERRLOG -msg "wrptp::dut_check_other_port_cal_period : Unable to check \
                     value of the otherPortCalPeriod on specified port \
                     on DUT"
        return 0
    }

    return 1
}

#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_check_other_port_cal_retry                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num                : (Mandatory) DUT's port number.                    #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  other_port_cal_retry    : (Mandatory) Value of the calRetry                 #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of the calRetry is verified#
#                                    for the specified port).                  #
#                                                                              #
#                      0 on Failure (If given value of the calRetry could not  #
#                                    be verified for the specific port).       #
#                                                                              #
#  DEFINITION        : This function verifies the value of the calRetry on the #
#                      specified port.                                         #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      wrptp::dut_check_other_port_cal_retry\                  #
#                             -port_num                $port_num\              #
#                             -other_port_cal_retry    $other_port_cal_retry   #
#                                                                              #
################################################################################

proc wrptp::dut_check_other_port_cal_retry {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_check_other_port_cal_retry : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-other_port_cal_retry)]} {
        set other_port_cal_retry $param(-other_port_cal_retry)
    } else {
        ERRLOG -msg "wrptp::dut_check_other_port_cal_retry : Missing Argument    -> other_port_cal_retry"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Check value of otherPortCalRetry ($other_port_cal_retry) on port $port_num"

    if {![wrptp::${config_mode}_check_other_port_cal_retry\
                    -session_handler           $session_handler\
                    -port_num                  $port_num\
                    -other_port_cal_retry      $other_port_cal_retry]} {

        ERRLOG -msg "wrptp::dut_check_other_port_cal_retry : Unable to check \
                     value of the otherPortCalRetry on specified port \
                     on DUT"
        return 0
    }

    return 1
}

#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_check_other_port_cal_send_pattern            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num                       : (Mandatory) DUT's port number.             #
#                                        (e.g., fa0/0)                         #
#                                                                              #
#  other_port_cal_send_pattern    : (Mandatory) Value of calSendPattern        #
#                                        (e.g., TRUE|FALSE)                    #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of calSendPattern is       #
#                                    verified for the specified port).         #
#                                                                              #
#                      0 on Failure (If given value of calSendPattern could not#
#                                    be verified for the specified port).      #
#                                                                              #
#  DEFINITION        : This function verifies the value of calSendPattern on   #
#                      the specified port.                                     #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                wrptp::dut_check_other_port_cal_send_pattern\                 #
#                   -port_num                      $port_num\                  #
#                   -other_port_cal_send_pattern   $other_port_cal_send_pattern#
#                                                                              #
################################################################################

proc wrptp::dut_check_other_port_cal_send_pattern {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::dut_check_other_port_cal_send_pattern : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-other_port_cal_send_pattern)]} {
        set other_port_cal_send_pattern $param(-other_port_cal_send_pattern)
    } else {
        ERRLOG -msg "wrptp::dut_check_other_port_cal_send_pattern : Missing Argument    -> other_port_cal_send_pattern"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Check value of otherPortCalSendPattern ($other_port_cal_send_pattern) on port $port_num"

    if {![wrptp::${config_mode}_check_other_port_cal_send_pattern\
                    -session_handler                $session_handler\
                    -port_num                       $port_num\
                    -other_port_cal_send_pattern    $other_port_cal_send_pattern]} {

        ERRLOG -msg "wrptp::dut_check_other_port_cal_send_pattern : Unable to check \
                     value of otherPortCalSendPattern on specified port \
                     on DUT"
        return 0
    }

    return 1
}


################################################################################
#                     DUT SETUP CONFIGURATION FUNCTIONS                        #
################################################################################


#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_configure_setup_001                          #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on success (If the DUT is configured for test setup 1)#
#                                                                              #
#                      0 on failure (If the DUT could not be configured for    #
#                                    test setup 1).                            #
#                                                                              #
#  DEFINITION        : This function configures the parameters required for the#
#                      test setup 1 at the DUT.                                #
#                                                                              #
#                      This function in turn calls below functions             #
#                                                                              #
#                         wrptp::dut_port_enable                               #
#                         wrptp::dut_global_ptp_enable                         #
#                         wrptp::dut_port_ptp_enable                           #
#                         wrptp::dut_set_domain                                #
#                         wrptp::dut_set_clock_mode                            #
#                         wrptp::dut_set_clock_step                            #
#                         wrptp::dut_set_delay_mechanism                       #
#                         wrptp::dut_set_communication_mode                    #
#                         wrptp::dut_set_network_protocol                      #
#                         wrptp::dut_set_vlan                                  #
#                         wrptp::dut_set_vlan_priority                         #
#                         wrptp::dut_set_ipv4_address                          #
#                         wrptp::dut_set_priority1                             #
#                         wrptp::dut_set_priority2                             #
#                         wrptp::dut_wrptp_enable                              #
#                         wrptp::dut_set_wr_config                             #
#                         wrptp::dut_set_deltas_known                          #
#                         wrptp::dut_set_known_delta_tx                        #
#                         wrptp::dut_set_known_delta_rx                        #
#                         wrptp::dut_set_cal_period                            #
#                         wrptp::dut_set_cal_retry                             #
#                         wrptp::dut_set_wr_present_timeout                    #
#                         wrptp::dut_set_wr_m_lock_timeout                     #
#                         wrptp::dut_set_wr_s_lock_timeout                     #
#                         wrptp::dut_set_wr_locked_timeout                     #
#                         wrptp::dut_set_wr_calibration_timeout                #
#                         wrptp::dut_set_wr_calibrated_timeout                 #
#                         wrptp::dut_set_wr_resp_calib_req_timeout             #
#                         wrptp::dut_set_wr_state_retry                        #
#                                                                              #
# USAGE              :                                                         #
#                                                                              #
#     wrptp::dut_configure_setup_001                                           #
#                                                                              #
################################################################################

proc wrptp::dut_configure_setup_001 {args} {

    array set param $args

    global env wrptp_env

    set ip_address             $env(dut_TEST_ADDRESS1)
    set net_mask               $env(dut_TEST_MASK1)
    set vlan_id                $::wrptp_vlan_id

    set vlan_priority          $::wrptp_vlan_priority

    set ptp_vlan_encap         $::wrptp_vlan_encap

    set delay_mechanism        $::ptp_dut_delay_mechanism
    set clock_mode             $::ptp_device_type
    set clock_step             $::ptp_dut_clock_step
    set network_protocol       $::ptp_trans_type
    set communication_mode     $::ptp_comm_model
    set priority1              $::wrptp_priority1
    set priority2              $::wrptp_priority2
    set port_list              $::dut_port_num_1
    set domain                 $::ptp_dut_default_domain_number

    set ptp_version            $::ptp_version
    set wr_config              $::wrptp_wrconfig
    
    set deltas_known           $::wrptp_deltas_known
    set known_delta_tx         $::wrptp_known_delta_tx
    set known_delta_rx         $::wrptp_known_delta_rx
    set cal_period             $::wrptp_cal_period
    set cal_retry              $::wrptp_cal_retry

    set present_timeout        $::wrptp_present_timeout
    set m_lock_timeout         $::wrptp_m_lock_timeout
    set s_lock_timeout         $::wrptp_s_lock_timeout
    set locked_timeout         $::wrptp_locked_timeout
    set calibration_timeout    $::wrptp_calibration_timeout
    set calibrated_timeout     $::wrptp_calibrated_timeout
    set resp_calib_req_timeout $::wrptp_resp_calib_req_timeout
    set wr_link_on_timeout     $::wrptp_wr_link_on_timeout
    set wr_state_retry         $::wrptp_state_retry

    set wr_state_timeout       $::wrptp_state_timeout

    ### Check Input parameters ###

    set env(TEE_ATTEST_MANUAL_PROMPT)      0

	## prepare dot-config file
	if { ![wrptp::dut_prepare_dot_config] } {
		ERRLOG -msg   "Cannot prepare dot-config file\n"

		TC_ABORT -reason "Cannot prepare dot-config file\n"\

		return 0
	}

    ###########################################################################
    #  1) Ensure the DUT port P1 admin status Up.                             #
    ###########################################################################

    if {![wrptp::dut_port_enable \
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###########################################################################
    #  2) Ensure PTP global on the DUT.                                       #
    ###########################################################################

    if {![wrptp::dut_global_ptp_enable]} {

        return 0
    }

    ###########################################################################
    #  3) Ensure PTP enabled on the port P1 on the DUT.                       #
    ###########################################################################

    if {![wrptp::dut_port_ptp_enable \
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    #  4) Configure Domain.                                                      #
    ##############################################################################

    if {![wrptp::dut_set_domain\
                    -clock_mode             $clock_mode\
                    -domain                 $domain ]} {

        return 0
    }

    ###########################################################################
    #  5) Ensure PTP set clock mode                                           #
    ###########################################################################

    if {![wrptp::dut_set_clock_mode \
                    -clock_mode             $clock_mode\
                    -port_num               [lindex $port_list 0] ]} {

        return 0
    }

    ###########################################################################
    #  6) Ensure PTP set clock step                                           #
    ###########################################################################

    if {![wrptp::dut_set_clock_step\
                    -port_num               [lindex $port_list 0]\
                    -clock_mode             $clock_mode\
                    -clock_step             $clock_step]} {

        return 0
    }

    ##############################################################################
    #  7) Configure Delay Mechanism.                                             #
    ##############################################################################

    if {![wrptp::dut_set_delay_mechanism \
                    -delay_mechanism        $delay_mechanism\
                    -clock_mode             $clock_mode\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    #  8) Configure Communication mode.                                          #
    ##############################################################################

    if {![wrptp::dut_set_communication_mode\
                   -port_num                [lindex $port_list 0]\
                   -clock_mode              $clock_mode\
                   -ptp_version             $ptp_version\
                   -communication_mode      $communication_mode]} {

        return 0
    }

    ###########################################################################
    #  9) Set Network protocol                                                #
    ###########################################################################

    if {![wrptp::dut_set_network_protocol\
                    -port_num               [lindex $port_list 0]\
                    -clock_mode             $clock_mode\
                    -network_protocol       $network_protocol]} {

        return 0
    }

    if {$ptp_vlan_encap == "true"} {

        ##############################################################################
        # 10) Create VLAN VLAN-A and associate port P1 to it.                        #
        ##############################################################################

        if {![wrptp::dut_set_vlan \
                        -vlan_id                $vlan_id\
                        -port_num               [lindex $port_list 0]]} {

            return 0
        }

        ##############################################################################
        # 11) Create VLAN VLAN-A and associate port P1 to it.                        #
        ##############################################################################

        if {![wrptp::dut_set_vlan_priority \
                        -vlan_id                $vlan_id\
                        -vlan_priority          $vlan_priority\
                        -port_num               [lindex $port_list 0]]} {

            return 0
        }
    }

    if {$::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4)} {

        ##############################################################################
        # 12) Set IP address.                                                        #
        ##############################################################################

        if {![wrptp::dut_set_ipv4_address\
                        -port_num              [lindex $port_list 0]\
                        -ip_address            $ip_address\
                        -net_mask              $net_mask\
                        -vlan_id               $vlan_id]} {

            return 0
        }
    }

    ##############################################################################
    # 13) Configure Priority1.                                                   #
    ##############################################################################

    if {![wrptp::dut_set_priority1\
                    -clock_mode             $clock_mode\
                    -priority1              $priority1]} {

        return 0
    }

    ##############################################################################
    # 14) Configure Priority2.                                                   #
    ##############################################################################

    if {![wrptp::dut_set_priority2\
                    -clock_mode             $clock_mode\
                    -priority2              $priority2]} {

        return 0
    }

    ###############################################################################
    # 15) Enable Wrptp                                                            #
    ###############################################################################

    if {![wrptp::dut_port_wrptp_enable \
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###############################################################################
    # 16) Configure wrConfig                                                      #
    ###############################################################################

    if {![wrptp::dut_set_wr_config\
                    -port_num               [lindex $port_list 0]\
                    -value                  $wr_config]} {

        return 0
    }

    ###############################################################################
    # 17) Configure deltasKnown                                                   #
    ###############################################################################

    if {![wrptp::dut_set_deltas_known \
                    -port_num               [lindex $port_list 0]\
                    -deltas_known           $deltas_known]} {

        return 0
    }

    ###############################################################################
    # 18) Configure knownDeltaTx                                                  #
    ###############################################################################

    if {![wrptp::dut_set_known_delta_tx\
                    -port_num               [lindex $port_list 0]\
                    -known_delta_tx         $known_delta_tx]} {

        return 0
    }

    ###############################################################################
    # 19) Configure knownDeltaRx                                                  #
    ###############################################################################

    if {![wrptp::dut_set_known_delta_rx\
                    -port_num               [lindex $port_list 0]\
                    -known_delta_rx         $known_delta_rx]} {

        return 0
    }


    ##############################################################################
    # 20) Configure calPeriod.                                                   #
    ##############################################################################

    if {![wrptp::dut_set_cal_period \
                    -cal_period             $cal_period\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 21) Conigure calRetry.                                                     #
    ##############################################################################

    if {![wrptp::dut_set_cal_retry \
                    -cal_retry              $cal_retry\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 22) Configure WR_PRESENT_TIMEOUT                                           #
    ##############################################################################

    if {![wrptp::dut_set_wr_present_timeout \
                    -timeout                $present_timeout\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 23) Configure WR_M_LOCK_TIMEOUT                                            #
    ##############################################################################

    if {![wrptp::dut_set_wr_m_lock_timeout \
                    -timeout                $m_lock_timeout\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 24) Configure WR_S_LOCK_TIMEOUT                                            #
    ##############################################################################

    if {![wrptp::dut_set_wr_s_lock_timeout \
                    -timeout                $s_lock_timeout\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 25) Configure WR_LOCKED_TIMEOUT                                            #
    ##############################################################################

    if {![wrptp::dut_set_wr_locked_timeout \
                    -timeout                $locked_timeout\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 26) Configure WR_CALIBRATION_TIMEOUT                                       #
    ##############################################################################

    if {![wrptp::dut_set_wr_calibration_timeout \
                    -timeout                $calibration_timeout\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 27) Configure WR_CALIBRATED_TIMEOUT                                        #
    ##############################################################################

    if {![wrptp::dut_set_wr_calibrated_timeout \
                    -timeout                $calibrated_timeout\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 28) Configure WR_RESP_CALIB_REQ_TIMEOUT                                    #
    ##############################################################################

    if {![wrptp::dut_set_wr_resp_calib_req_timeout \
                    -timeout                $resp_calib_req_timeout\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 29) Configure WR_STATE_RETRY.                                              #
    ##############################################################################

    if {![wrptp::dut_set_wr_state_retry \
                    -wr_state_retry         $wr_state_retry\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    set env(TEE_ATTEST_MANUAL_PROMPT)      1

    return 1
 
}   

#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_configure_setup_002                          #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on success (If the DUT is configured for test setup 2)#
#                                                                              #
#                      0 on failure (If the DUT could not be configured for    #
#                                    test setup 2).                            #
#                                                                              #
#  DEFINITION        : This function configures the parameters required for the#
#                      test setup 2 at the DUT.                                #
#                                                                              #
#                      This function in turn calls below functions             #
#                                                                              #
#                         wrptp::dut_port_enable                               #
#                         wrptp::dut_global_ptp_enable                         #
#                         wrptp::dut_port_ptp_enable                           #
#                         wrptp::dut_set_domain                                #
#                         wrptp::dut_set_clock_mode                            #
#                         wrptp::dut_set_clock_step                            #
#                         wrptp::dut_set_delay_mechanism                       #
#                         wrptp::dut_set_communication_mode                    #
#                         wrptp::dut_set_network_protocol                      #
#                         wrptp::dut_set_vlan                                  #
#                         wrptp::dut_set_vlan_priority                         #
#                         wrptp::dut_set_ipv4_address                          #
#                         wrptp::dut_set_priority1                             #
#                         wrptp::dut_set_priority2                             #
#                         wrptp::dut_set_announce_interval                     #
#                         wrptp::dut_wrptp_enable                              #
#                         wrptp::dut_set_wr_config                             #
#                         wrptp::dut_set_deltas_known                          #
#                         wrptp::dut_set_known_delta_tx                        #
#                         wrptp::dut_set_known_delta_rx                        #
#                         wrptp::dut_set_cal_period                            #
#                         wrptp::dut_set_cal_retry                             #
#                         wrptp::dut_set_wr_present_timeout                    #
#                         wrptp::dut_set_wr_m_lock_timeout                     #
#                         wrptp::dut_set_wr_s_lock_timeout                     #
#                         wrptp::dut_set_wr_locked_timeout                     #
#                         wrptp::dut_set_wr_calibration_timeout                #
#                         wrptp::dut_set_wr_calibrated_timeout                 #
#                         wrptp::dut_set_wr_resp_calib_req_timeout             #
#                         wrptp::dut_set_wr_state_retry                        #
#                                                                              #
# USAGE              :                                                         #
#                                                                              #
#     wrptp::dut_configure_setup_002                                           #
#                                                                              #
################################################################################

proc wrptp::dut_configure_setup_002 {args} {

    array set param $args

    global env wrptp_env

    set ip_address             $env(dut_TEST_ADDRESS1)
    set net_mask               $env(dut_TEST_MASK1)
    set vlan_id                $::wrptp_vlan_id

    set vlan_priority          $::wrptp_vlan_priority

    set ptp_vlan_encap         $::wrptp_vlan_encap

    set delay_mechanism        $::ptp_dut_delay_mechanism
    set clock_mode             $::ptp_device_type
    set clock_step             $::ptp_dut_clock_step
    set network_protocol       $::ptp_trans_type
    set communication_mode     $::ptp_comm_model
    set priority1              $::wrptp_priority1
    set priority2              $::wrptp_priority2
    set port_list              $::dut_port_num_1
    set domain                 $::ptp_dut_default_domain_number

    set ptp_version            $::ptp_version
    set wr_config              $::wrptp_wrconfig
    
    set deltas_known           $::wrptp_deltas_known
    set known_delta_tx         $::wrptp_known_delta_tx
    set known_delta_rx         $::wrptp_known_delta_rx
    set cal_period             $::wrptp_cal_period
    set cal_retry              $::wrptp_cal_retry
    set announce_interval      4

    set present_timeout        $::wrptp_present_timeout
    set m_lock_timeout         $::wrptp_m_lock_timeout
    set s_lock_timeout         $::wrptp_s_lock_timeout
    set locked_timeout         $::wrptp_locked_timeout
    set calibration_timeout    $::wrptp_calibration_timeout
    set calibrated_timeout     $::wrptp_calibrated_timeout
    set resp_calib_req_timeout $::wrptp_resp_calib_req_timeout
    set wr_link_on_timeout     $::wrptp_wr_link_on_timeout
    set wr_state_retry         $::wrptp_state_retry

    set wr_state_timeout       $::wrptp_state_timeout

    ### Check Input parameters ###

    set env(TEE_ATTEST_MANUAL_PROMPT)      0

	## prepare dot-config file
	if { ![wrptp::dut_prepare_dot_config] } {
		ERRLOG -msg   "Cannot prepare dot-config file\n"

		TC_ABORT -reason "Cannot prepare dot-config file\n"\

		return 0
	}

    ###########################################################################
    #  1) Ensure the DUT port P1 admin status Up.                             #
    ###########################################################################

    if {![wrptp::dut_port_enable \
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###########################################################################
    #  2) Ensure PTP global on the DUT.                                       #
    ###########################################################################

    if {![wrptp::dut_global_ptp_enable]} {

        return 0
    }

    ###########################################################################
    #  3) Ensure PTP enabled on the port P1 on the DUT.                       #
    ###########################################################################

    if {![wrptp::dut_port_ptp_enable \
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    #  4) Configure Domain.                                                      #
    ##############################################################################

    if {![wrptp::dut_set_domain\
                    -clock_mode             $clock_mode\
                    -domain                 $domain ]} {

        return 0
    }

    ###########################################################################
    #  5) Ensure PTP set clock mode                                           #
    ###########################################################################

    if {![wrptp::dut_set_clock_mode \
                    -clock_mode             $clock_mode\
                    -port_num               [lindex $port_list 0] ]} {

        return 0
    }

    ###########################################################################
    #  6) Ensure PTP set clock step                                           #
    ###########################################################################

    if {![wrptp::dut_set_clock_step\
                    -port_num               [lindex $port_list 0]\
                    -clock_mode             $clock_mode\
                    -clock_step             $clock_step]} {

        return 0
    }

    ##############################################################################
    #  7) Configure Delay Mechanism.                                             #
    ##############################################################################

    if {![wrptp::dut_set_delay_mechanism \
                    -delay_mechanism        $delay_mechanism\
                    -clock_mode             $clock_mode\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    #  8) Configure Communication mode.                                          #
    ##############################################################################

    if {![wrptp::dut_set_communication_mode\
                   -port_num                [lindex $port_list 0]\
                   -clock_mode              $clock_mode\
                   -ptp_version             $ptp_version\
                   -communication_mode      $communication_mode]} {

        return 0
    }

    ###########################################################################
    #  9) Set Network protocol                                                #
    ###########################################################################

    if {![wrptp::dut_set_network_protocol\
                    -port_num               [lindex $port_list 0]\
                    -clock_mode             $clock_mode\
                    -network_protocol       $network_protocol]} {

        return 0
    }

    if {$ptp_vlan_encap == "true"} {

        ##############################################################################
        # 10) Create VLAN VLAN-A and associate port P1 to it.                        #
        ##############################################################################

        if {![wrptp::dut_set_vlan \
                        -vlan_id                $vlan_id\
                        -port_num               [lindex $port_list 0]]} {

            return 0
        }

        ##############################################################################
        # 11) Create VLAN VLAN-A and associate port P1 to it.                        #
        ##############################################################################

        if {![wrptp::dut_set_vlan_priority \
                        -vlan_id                $vlan_id\
                        -vlan_priority          $vlan_priority\
                        -port_num               [lindex $port_list 0]]} {

            return 0
        }
    }

    if {$::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4)} {

        ##############################################################################
        # 12) Set IP address.                                                        #
        ##############################################################################

        if {![wrptp::dut_set_ipv4_address\
                        -port_num              [lindex $port_list 0]\
                        -ip_address            $ip_address\
                        -net_mask              $net_mask\
                        -vlan_id               $vlan_id]} {

            return 0
        }
    }

    ##############################################################################
    # 13) Configure Priority1.                                                   #
    ##############################################################################

    if {![wrptp::dut_set_priority1\
                    -clock_mode             $clock_mode\
                    -priority1              $priority1]} {

        return 0
    }

    ##############################################################################
    # 14) Configure Priority2.                                                   #
    ##############################################################################

    if {![wrptp::dut_set_priority2\
                    -clock_mode             $clock_mode\
                    -priority2              $priority2]} {

        return 0
    }

    ##############################################################################
    # 15) Configure Announce Interval.                                           #
    ##############################################################################

    if {![wrptp::dut_set_announce_interval \
                    -announce_interval      $announce_interval\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###############################################################################
    # 16) Enable Wrptp                                                            #
    ###############################################################################

    if {![wrptp::dut_port_wrptp_enable \
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###############################################################################
    # 17) Configure wrConfig                                                      #
    ###############################################################################

    if {![wrptp::dut_set_wr_config\
                    -port_num               [lindex $port_list 0]\
                    -value                  $wr_config]} {

        return 0
    }

    ###############################################################################
    # 18) Configure deltasKnown                                                   #
    ###############################################################################

    if {![wrptp::dut_set_deltas_known \
                    -port_num               [lindex $port_list 0]\
                    -deltas_known           $deltas_known]} {

        return 0
    }

    ###############################################################################
    # 19) Configure knownDeltaTx                                                  #
    ###############################################################################

    if {![wrptp::dut_set_known_delta_tx\
                    -port_num               [lindex $port_list 0]\
                    -known_delta_tx         $known_delta_tx]} {

        return 0
    }

    ###############################################################################
    # 20) Configure knownDeltaRx                                                  #
    ###############################################################################

    if {![wrptp::dut_set_known_delta_rx\
                    -port_num               [lindex $port_list 0]\
                    -known_delta_rx         $known_delta_rx]} {

        return 0
    }

    ##############################################################################
    # 21) Configure calPeriod.                                                   #
    ##############################################################################

    if {![wrptp::dut_set_cal_period \
                    -cal_period             $cal_period\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 22) Conigure calRetry.                                                     #
    ##############################################################################

    if {![wrptp::dut_set_cal_retry \
                    -cal_retry              $cal_retry\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 23) Configure WR_PRESENT_TIMEOUT                                           #
    ##############################################################################

    if {![wrptp::dut_set_wr_present_timeout \
                    -timeout                $present_timeout\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 24) Configure WR_M_LOCK_TIMEOUT                                            #
    ##############################################################################

    if {![wrptp::dut_set_wr_m_lock_timeout \
                    -timeout                $m_lock_timeout\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 25) Configure WR_S_LOCK_TIMEOUT                                            #
    ##############################################################################

    if {![wrptp::dut_set_wr_s_lock_timeout \
                    -timeout                $s_lock_timeout\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 26) Configure WR_LOCKED_TIMEOUT                                            #
    ##############################################################################

    if {![wrptp::dut_set_wr_locked_timeout \
                    -timeout                $locked_timeout\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 27) Configure WR_CALIBRATION_TIMEOUT                                       #
    ##############################################################################

    if {![wrptp::dut_set_wr_calibration_timeout \
                    -timeout                $calibration_timeout\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 28) Configure WR_CALIBRATED_TIMEOUT                                        #
    ##############################################################################

    if {![wrptp::dut_set_wr_calibrated_timeout \
                    -timeout                $calibrated_timeout\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 29) Configure WR_RESP_CALIB_REQ_TIMEOUT                                    #
    ##############################################################################

    if {![wrptp::dut_set_wr_resp_calib_req_timeout \
                    -timeout                $resp_calib_req_timeout\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 30) Configure WR_STATE_RETRY.                                              #
    ##############################################################################

    if {![wrptp::dut_set_wr_state_retry \
                    -wr_state_retry         $wr_state_retry\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    set env(TEE_ATTEST_MANUAL_PROMPT)      1

    return 1

}   

#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_configure_setup_003                          #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on success (If the DUT is configured for test setup 3)#
#                                                                              #
#                      0 on failure (If the DUT could not be configured for    #
#                                    test setup 3).                            #
#                                                                              #
#  DEFINITION        : This function configures the parameters required for the#
#                      test setup 5 at the DUT.                                #
#                                                                              #
#                      This function in turn calls below functions             #
#                                                                              #
#                         wrptp::dut_port_enable                               #
#                         wrptp::dut_global_ptp_enable                         #
#                         wrptp::dut_port_ptp_enable                           #
#                         wrptp::dut_set_domain                                #
#                         wrptp::dut_set_clock_mode                            #
#                         wrptp::dut_set_clock_step                            #
#                         wrptp::dut_set_delay_mechanism                       #
#                         wrptp::dut_set_communication_mode                    #
#                         wrptp::dut_set_network_protocol                      #
#                         wrptp::dut_set_vlan                                  #
#                         wrptp::dut_set_vlan_priority                         #
#                         wrptp::dut_set_ipv4_address                          #
#                         wrptp::dut_set_priority1                             #
#                         wrptp::dut_set_priority2                             #
#                         wrptp::dut_wrptp_enable                              #
#                                                                              #
# USAGE              :                                                         #
#                                                                              #
#     wrptp::dut_configure_setup_003                                           #
#                                                                              #
################################################################################

proc wrptp::dut_configure_setup_003 {args} {

    array set param $args

    global env wrptp_env

    set ip_address             $env(dut_TEST_ADDRESS1)
    set net_mask               $env(dut_TEST_MASK1)
    set vlan_id                $::wrptp_vlan_id

    set vlan_priority          $::wrptp_vlan_priority

    set ptp_vlan_encap         $::wrptp_vlan_encap

    set delay_mechanism        $::ptp_dut_delay_mechanism
    set clock_mode             $::ptp_device_type
    set clock_step             $::ptp_dut_clock_step
    set network_protocol       $::ptp_trans_type
    set communication_mode     $::ptp_comm_model
    set priority1              $::wrptp_priority1
    set priority2              $::wrptp_priority2
    set port_list              $::dut_port_num_1
    set domain                 $::ptp_dut_default_domain_number

    set ptp_version            $::ptp_version
    set wr_config              $::wrptp_wrconfig
    
    set deltas_known           $::wrptp_deltas_known
    set known_delta_tx         $::wrptp_known_delta_tx
    set known_delta_rx         $::wrptp_known_delta_rx
    set wr_state_timeout       $::wrptp_state_timeout
    set wr_state_retry         $::wrptp_state_retry
    set cal_period             $::wrptp_cal_period
    set cal_retry              $::wrptp_cal_retry

    ### Check Input parameters ###

    set env(TEE_ATTEST_MANUAL_PROMPT)      0

	## prepare dot-config file
	if { ![wrptp::dut_prepare_dot_config] } {
		ERRLOG -msg   "Cannot prepare dot-config file\n"

		TC_ABORT -reason "Cannot prepare dot-config file\n"\

		return 0
	}
	
    ###########################################################################
    #  1) Ensure the DUT port P1 admin status Up.                             #
    ###########################################################################

    if {![wrptp::dut_port_enable \
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###########################################################################
    #  2) Ensure PTP global on the DUT.                                       #
    ###########################################################################

    if {![wrptp::dut_global_ptp_enable]} {

        return 0
    }

    ###########################################################################
    #  3) Ensure PTP enabled on the port P1 on the DUT.                       #
    ###########################################################################

    if {![wrptp::dut_port_ptp_enable \
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    #  4) Configure Domain.                                                      #
    ##############################################################################

    if {![wrptp::dut_set_domain\
                    -clock_mode             $clock_mode\
                    -domain                 $domain ]} {

        return 0
    }

    ###########################################################################
    #  5) Ensure PTP set clock mode                                           #
    ###########################################################################

    if {![wrptp::dut_set_clock_mode \
                    -clock_mode             $clock_mode\
                    -port_num               [lindex $port_list 0] ]} {

        return 0
    }

    ###########################################################################
    #  6) Ensure PTP set clock step                                           #
    ###########################################################################

    if {![wrptp::dut_set_clock_step\
                    -port_num               [lindex $port_list 0]\
                    -clock_mode             $clock_mode\
                    -clock_step             $clock_step]} {

        return 0
    }

    ##############################################################################
    #  7) Configure Delay Mechanism.                                             #
    ##############################################################################

    if {![wrptp::dut_set_delay_mechanism \
                    -delay_mechanism        $delay_mechanism\
                    -clock_mode             $clock_mode\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    #  8) Configure Communication mode.                                          #
    ##############################################################################

    if {![wrptp::dut_set_communication_mode\
                   -port_num                [lindex $port_list 0]\
                   -clock_mode              $clock_mode\
                   -ptp_version             $ptp_version\
                   -communication_mode      $communication_mode]} {

        return 0
    }

    ###########################################################################
    #  9) Set Network protocol                                                #
    ###########################################################################

    if {![wrptp::dut_set_network_protocol\
                    -port_num               [lindex $port_list 0]\
                    -clock_mode             $clock_mode\
                    -network_protocol       $network_protocol]} {

        return 0
    }

    if {$ptp_vlan_encap == "true"} {

        ##############################################################################
        # 10) Create VLAN VLAN-A and associate port P1 to it.                        #
        ##############################################################################

        if {![wrptp::dut_set_vlan \
                        -vlan_id                $vlan_id\
                        -port_num               [lindex $port_list 0]]} {

            return 0
        }

        ##############################################################################
        # 11) Create VLAN VLAN-A and associate port P1 to it.                        #
        ##############################################################################

        if {![wrptp::dut_set_vlan_priority \
                        -vlan_id                $vlan_id\
                        -vlan_priority          $vlan_priority\
                        -port_num               [lindex $port_list 0]]} {

            return 0
        }
    }

    if {$::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4)} {

        ##############################################################################
        # 12) Set IP address.                                                        #
        ##############################################################################

        if {![wrptp::dut_set_ipv4_address\
                        -port_num              [lindex $port_list 0]\
                        -ip_address            $ip_address\
                        -net_mask              $net_mask\
                        -vlan_id               $vlan_id]} {

            return 0
        }
    }

    ##############################################################################
    # 13) Configure Priority1.                                                   #
    ##############################################################################

    if {![wrptp::dut_set_priority1\
                    -clock_mode             $clock_mode\
                    -priority1              $priority1]} {

        return 0
    }

    ##############################################################################
    # 14) Configure Priority2.                                                   #
    ##############################################################################

    if {![wrptp::dut_set_priority2\
                    -clock_mode             $clock_mode\
                    -priority2              $priority2]} {

        return 0
    }

    ###############################################################################
    # 15) Enable Wrptp                                                            #
    ###############################################################################

    if {![wrptp::dut_port_wrptp_enable \
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    set env(TEE_ATTEST_MANUAL_PROMPT)      1

    return 1

}   


################################################################################
#                       DUT SETUP CLEANUP FUNCTIONS                            #
################################################################################

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_cleanup_setup_001                            #
#                                                                              #
#  INPUT PARAMETERS   : NONE.                                                  #
#                                                                              #
#  OUTPUT PARAMETERS  : NONE.                                                  #
#                                                                              #
#  RETURNS           : 1 on success (if the configuration of test setup 1 is   #
#                                    cleared in DUT).                          #
#                                                                              #
#                      0 on failure (if the DUT could not be cleared for       #
#                                    test setup 1).                            #
#                                                                              #
#  DEFINITION        : This function clears the configurations made for test   #
#                      setup 1 at the DUT.                                     #
#                                                                              #
#                      This function in turn calls below functions             #
#                                                                              #
#                         wrptp::dut_reset_priority1                           #
#                         wrptp::dut_reset_priority2                           #
#                         wrptp::dut_reset_clock_mode                          #
#                         wrptp::dut_reset_clock_step                          #
#                         wrptp::dut_reset_delay_mechanism                     #
#                         wrptp::dut_reset_communication_mode                  #
#                         wrptp::dut_reset_network_protocol                    #
#                         wrptp::dut_reset_domain                              #
#                         wrptp::dut_reset_known_delta_tx                      #
#                         wrptp::dut_reset_known_delta_rx                      #
#                         wrptp::dut_reset_deltas_known                        #
#                         wrptp::dut_reset_wr_present_timeout                  #
#                         wrptp::dut_reset_wr_m_lock_timeout                   #
#                         wrptp::dut_reset_wr_s_lock_timeout                   #
#                         wrptp::dut_reset_wr_locked_timeout                   #
#                         wrptp::dut_reset_wr_calibration_timeout              #
#                         wrptp::dut_reset_wr_calibrated_timeout               #
#                         wrptp::dut_reset_wr_resp_calib_req_timeout           #
#                         wrptp::dut_reset_wr_state_retry                      #
#                         wrptp::dut_reset_wr_state_retry                      #
#                         wrptp::dut_reset_cal_period                          #
#                         wrptp::dut_reset_cal_retry                           #
#                         wrptp::dut_reset_wr_config                           #
#                         wrptp::dut_wrptp_disable                             #
#                         wrptp::dut_port_ptp_disable                          #
#                         wrptp::dut_global_ptp_disable                        #
#                         wrptp::dut_reset_vlan_priority                       #
#                         wrptp::dut_reset_vlan                                #
#                         wrptp::dut_reset_ipv4_address                        #
#                         wrptp::dut_port_disable                              #
#                                                                              #
# USAGE              :                                                         #
#                                                                              #
#     wrptp::dut_cleanup_setup_001                                             #
#                                                                              #
################################################################################

proc wrptp::dut_cleanup_setup_001 {args} {

    array set param $args

    global env wrptp_env

    set ip_address             $env(dut_TEST_ADDRESS1)
    set net_mask               $env(dut_TEST_MASK1)
    set vlan_id                $::wrptp_vlan_id

    set vlan_priority          $::wrptp_vlan_priority

    set ptp_vlan_encap         $::wrptp_vlan_encap

    set network_protocol       $::ptp_trans_type
    set communication_mode     $::ptp_comm_model

    set port_list              $::dut_port_num_1
    set domain                 $::ptp_dut_default_domain_number

    set ptp_version            $::ptp_version

    ### Check Input parameters ###

    LOG -level 0 -msg "Configuration Cleanup. Please ignore the ERRORS \n"

    wrptp::dut_reset_priority1

    wrptp::dut_reset_priority2

    wrptp::dut_reset_clock_mode \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_clock_step\
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_delay_mechanism \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_communication_mode\
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_network_protocol\
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_domain

    wrptp::dut_reset_known_delta_tx\
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_known_delta_rx\
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_deltas_known \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_wr_present_timeout \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_wr_m_lock_timeout \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_wr_s_lock_timeout \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_wr_locked_timeout \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_wr_calibration_timeout \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_wr_calibrated_timeout \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_wr_resp_calib_req_timeout \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_wr_state_retry \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_cal_period \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_cal_retry \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_wr_config\
                    -port_num               [lindex $port_list 0]

    wrptp::dut_port_wrptp_disable \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_port_ptp_disable \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_global_ptp_disable

    if {$ptp_vlan_encap == "true"} {

      wrptp::dut_reset_vlan_priority \
                        -vlan_id                $vlan_id\
                        -vlan_priority          $vlan_priority\
                        -port_num               [lindex $port_list 0]

       wrptp::dut_reset_vlan \
                        -vlan_id                $vlan_id\
                        -port_num               [lindex $port_list 0]
    }

    if {$::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4)} {

       wrptp::dut_reset_ipv4_address\
                        -port_num               [lindex $port_list 0]\
                        -ip_address             $ip_address\
                        -net_mask               $net_mask\
                        -vlan_id                $vlan_id
    }

    wrptp::dut_port_disable\
                    -port_num               [lindex $port_list 0]

    set env(TEE_ATTEST_MANUAL_PROMPT)      1

    return 1
}

#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_cleanup_setup_002                            #
#                                                                              #
#  INPUT PARAMETERS   : NONE.                                                  #
#                                                                              #
#  OUTPUT PARAMETERS  : NONE.                                                  #
#                                                                              #
#  RETURNS           : 1 on success (if the configuration of test setup 2 is   #
#                                    cleared in DUT).                          #
#                                                                              #
#                      0 on failure (if the DUT could not be cleared for       #
#                                    test setup 2).                            #
#                                                                              #
#  DEFINITION        : This function clears the configurations made for test   #
#                      setup 3 at the DUT.                                     #
#                                                                              #
#                      This function in turn calls below functions             #
#                                                                              #
#                         wrptp::dut_reset_priority1                           #
#                         wrptp::dut_reset_priority2                           #
#                         wrptp::dut_reset_announce_interval                   #
#                         wrptp::dut_reset_clock_mode                          #
#                         wrptp::dut_reset_clock_step                          #
#                         wrptp::dut_reset_delay_mechanism                     #
#                         wrptp::dut_reset_communication_mode                  #
#                         wrptp::dut_reset_network_protocol                    #
#                         wrptp::dut_reset_domain                              #
#                         wrptp::dut_reset_known_delta_tx                      #
#                         wrptp::dut_reset_known_delta_rx                      #
#                         wrptp::dut_reset_deltas_known                        #
#                         wrptp::dut_reset_wr_present_timeout                  #
#                         wrptp::dut_reset_wr_m_lock_timeout                   #
#                         wrptp::dut_reset_wr_s_lock_timeout                   #
#                         wrptp::dut_reset_wr_locked_timeout                   #
#                         wrptp::dut_reset_wr_calibration_timeout              #
#                         wrptp::dut_reset_wr_calibrated_timeout               #
#                         wrptp::dut_reset_wr_resp_calib_req_timeout           #
#                         wrptp::dut_reset_wr_state_retry                      #
#                         wrptp::dut_reset_cal_period                          #
#                         wrptp::dut_reset_cal_retry                           #
#                         wrptp::dut_reset_wr_config                           #
#                         wrptp::dut_wrptp_disable                             #
#                         wrptp::dut_port_ptp_disable                          #
#                         wrptp::dut_global_ptp_disable                        #
#                         wrptp::dut_reset_vlan_priority                       #
#                         wrptp::dut_reset_vlan                                #
#                         wrptp::dut_reset_ipv4_address                        #
#                         wrptp::dut_port_disable                              #
#                                                                              #
# USAGE              :                                                         #
#                                                                              #
#     wrptp::dut_cleanup_setup_002                                             #
#                                                                              #
################################################################################

proc wrptp::dut_cleanup_setup_002 {args} {

    array set param $args

    global env wrptp_env

    set ip_address             $env(dut_TEST_ADDRESS1)
    set net_mask               $env(dut_TEST_MASK1)
    set vlan_id                $::wrptp_vlan_id

    set vlan_priority          $wrptp_env(DUT_WRPTP_VLAN_PRIORITY)

    set ptp_vlan_encap         $wrptp_env(DUT_WRPTP_IS_VLAN_ENCAP)

    set network_protocol       $wrptp_env(DUT_WRPTP_TRANS_TYPE)
    set communication_mode     $wrptp_env(DUT_WRPTP_COM_MODEL)

    set port_list              $::dut_port_num_1
    set domain                 $::ptp_dut_default_domain_number

    set ptp_version            $::ptp_version

    ### Check Input parameters ###

    LOG -level 0 -msg "Configuration Cleanup. Please ignore the ERRORS \n"

    wrptp::dut_reset_priority1

    wrptp::dut_reset_priority2

    wrptp::dut_reset_announce_interval \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_clock_mode \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_clock_step\
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_delay_mechanism \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_communication_mode\
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_network_protocol\
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_domain

    wrptp::dut_reset_known_delta_tx\
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_known_delta_rx\
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_deltas_known \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_wr_present_timeout \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_wr_m_lock_timeout \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_wr_s_lock_timeout \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_wr_locked_timeout \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_wr_calibration_timeout \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_wr_calibrated_timeout \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_wr_resp_calib_req_timeout \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_wr_state_retry \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_cal_period \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_cal_retry \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_wr_config\
                    -port_num               [lindex $port_list 0]

    wrptp::dut_port_wrptp_disable \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_port_ptp_disable \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_global_ptp_disable

    if {$ptp_vlan_encap == "true"} {

       wrptp::dut_reset_vlan_priority \
                        -vlan_id                $vlan_id\
                        -vlan_priority          $vlan_priority\
                        -port_num               [lindex $port_list 0]

        wrptp::dut_reset_vlan \
                        -vlan_id                $vlan_id\
                        -port_num               [lindex $port_list 0]
    }

    if {$::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4)} {

        wrptp::dut_reset_ipv4_address\
                        -port_num               [lindex $port_list 0]\
                        -ip_address             $ip_address\
                        -net_mask               $net_mask\
                        -vlan_id                $vlan_id
    }

    wrptp::dut_port_disable\
                    -port_num               [lindex $port_list 0]

    set env(TEE_ATTEST_MANUAL_PROMPT)      1

    return 1
}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::dut_cleanup_setup_003                            #
#                                                                              #
#  INPUT PARAMETERS   : NONE.                                                  #
#                                                                              #
#  OUTPUT PARAMETERS  : NONE.                                                  #
#                                                                              #
#  RETURNS           : 1 on success (if the configuration of test setup 3 is   #
#                                    cleared in DUT).                          #
#                                                                              #
#                      0 on failure (if the DUT could not be cleared for       #
#                                    test setup 3).                            #
#                                                                              #
#  DEFINITION        : This function clears the configurations made for test   #
#                      setup 1 at the DUT.                                     #
#                                                                              #
#                      This function in turn calls below functions             #
#                                                                              #
#                         wrptp::dut_reset_priority1                           #
#                         wrptp::dut_reset_priority2                           #
#                         wrptp::dut_reset_clock_mode                          #
#                         wrptp::dut_reset_clock_step                          #
#                         wrptp::dut_reset_delay_mechanism                     #
#                         wrptp::dut_reset_communication_mode                  #
#                         wrptp::dut_reset_network_protocol                    #
#                         wrptp::dut_reset_domain                              #
#                         wrptp::dut_wrptp_disable                             #
#                         wrptp::dut_port_ptp_disable                          #
#                         wrptp::dut_global_ptp_disable                        #
#                         wrptp::dut_reset_vlan_priority                       #
#                         wrptp::dut_reset_vlan                                #
#                         wrptp::dut_reset_ipv4_address                        #
#                         wrptp::dut_port_disable                              #
#                                                                              #
# USAGE              :                                                         #
#                                                                              #
#     wrptp::dut_cleanup_setup_003                                             #
#                                                                              #
################################################################################

proc wrptp::dut_cleanup_setup_003 {args} {

    array set param $args

    global env wrptp_env

    set ip_address             $env(dut_TEST_ADDRESS1)
    set net_mask               $env(dut_TEST_MASK1)
    set vlan_id                $::wrptp_vlan_id

    set vlan_priority          $::wrptp_vlan_priority

    set ptp_vlan_encap         $::wrptp_vlan_encap

    set network_protocol       $::ptp_trans_type
    set communication_mode     $::ptp_comm_model

    set port_list              $::dut_port_num_1
    set domain                 $::ptp_dut_default_domain_number

    set ptp_version            $::ptp_version

    ### Check Input parameters ###

    LOG -level 0 -msg "Configuration Cleanup. Please ignore the ERRORS \n"

    wrptp::dut_reset_priority1

    wrptp::dut_reset_priority2

    wrptp::dut_reset_clock_mode \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_clock_step\
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_delay_mechanism \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_communication_mode\
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_network_protocol\
                    -port_num               [lindex $port_list 0]

    wrptp::dut_reset_domain

    wrptp::dut_port_wrptp_disable \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_port_ptp_disable \
                    -port_num               [lindex $port_list 0]

    wrptp::dut_global_ptp_disable

    if {$ptp_vlan_encap == "true"} {

        wrptp::dut_reset_vlan_priority \
                        -vlan_id                $vlan_id\
                        -vlan_priority          $vlan_priority\
                        -port_num               [lindex $port_list 0]

        wrptp::dut_reset_vlan \
                        -vlan_id                $vlan_id\
                        -port_num               [lindex $port_list 0]
    }

    if {$::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4)} {

        wrptp::dut_reset_ipv4_address\
                        -port_num               [lindex $port_list 0]\
                        -ip_address             $ip_address\
                        -net_mask               $net_mask\
                        -vlan_id                $vlan_id
    }

    wrptp::dut_port_disable\
                    -port_num               [lindex $port_list 0]

    set env(TEE_ATTEST_MANUAL_PROMPT)      1

    return 1
}
