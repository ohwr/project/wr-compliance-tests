################################################################################
# File Name         : tee_wrptp_functions.tcl                                  #
# File Version      : 1.4                                                      #
# Component Name    : ATTEST TEST EXECUTION ENGINE (TEE)                       #
# Module Name       : White Rabbit Precision Time Protocol                     #
################################################################################
# History       Date       Author        Addition/ Alteration                  #
################################################################################
#                                                                              #
#  1.0          Jun/2018   CERN          Initial                               #
#  1.1          Aug/2018   CERN          Default source IP address is derived  #
#                                        from the IP given in UI               #
#  1.2          Jan/2019   CERN          Handled locking mechanism for         #
#                                        resetting capture buffer.             #
#  1.3          Jan/2019   CERN          Added wrptp::wait_msec.               #
#  1.4          Apr/2019   JC.BAU/CERN   Added a test in wait_msec to check    #
#                                        for a negative value                  #
#                                                                              #
################################################################################
################################################################################
#   Copyright (C) 2018 - 2019 CERN                                             #
################################################################################
######################### PROCEDURES USED ######################################
#                                                                              #
#  1. wrptp::send_sync                                                         #
#  2. wrptp::send_announce                                                     #
#  3. wrptp::send_delay_req                                                    #
#  4. wrptp::send_delay_resp                                                   #
#  5. wrptp::send_followup                                                     #
#  6. wrptp::send_pdelay_req                                                   #
#  7. wrptp::send_pdelay_resp                                                  #
#  8. wrptp::send_pdelay_resp_followup                                         #
#  9. wrptp::send_signal                                                       #
# 10. wrptp::send_mgmt                                                         #
# 11. wrptp::recv_sync                                                         #
# 12. wrptp::recv_announce                                                     #
# 13. wrptp::recv_delay_req                                                    #
# 14. wrptp::recv_delay_resp                                                   #
# 15. wrptp::recv_followup                                                     #
# 16. wrptp::recv_pdelay_req                                                   #
# 17. wrptp::recv_pdelay_resp                                                  #
# 18. wrptp::recv_pdelay_resp_followup                                         #
# 19. wrptp::recv_signal                                                       #
# 20. wrptp::recv_mgmt                                                         #
# 21. wrptp::stop_sync                                                         #
# 22. wrptp::stop_announce                                                     #
# 23. wrptp::stop_delay_req                                                    #
# 24. wrptp::stop_delay_resp                                                   #
# 25. wrptp::stop_followup                                                     #
# 26. wrptp::stop_pdelay_req                                                   #
# 27. wrptp::stop_pdelay_resp                                                  #
# 28. wrptp::stop_pdelay_resp_followup                                         #
# 29. wrptp::stop_signal                                                       #
# 30. wrptp::stop_mgmt                                                         #
# 31. wrptp::check_announce                                                    #
# 32. wrptp::check_signal                                                      #
# 33. wrptp::resolve_ip_to_mac                                                 #
# 34. wrptp::respond_to_arp_requests                                           #
# 35. wrptp::get_port_macaddress                                               #
# 36. wrptp::tee_get_interface_ip                                              #
# 37. wrptp::reset_capture_stats                                               #
#                                                                              #
################################################################################

namespace eval wrptp {}

#Initialization
set ::handler(WRPTP)                          update_wrptp_dynamic_fields
set ::validation_handler(WRPTP)               validate_wrptp_pkt
set ::validation_handler(ARP)                 validate_arp_pkt
set ::validation_handler(ARP_RESPONDER)       send_arp_response

################################################################################
#                                                                              #
#  PROCEDURE NAME         :  wrptp::send_sync                                  #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Transmission port              #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   dest_mac                      : (Mandatory) Destination MAC dddress.       #
#                                               (e.g., 01:80:C2:00:00:20)      #
#                                                                              #
#   src_mac                       : (Mandatory) Source MAC address.            #
#                                               (e.g., 00:80:C2:00:00:21)      #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                               (Default : 88F7)               #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                               (Default : 0)                  #
#                                                                              #
#   vlan_priority                 : (Optional)  Vlan Priority                  #
#                                               (Default : 0)                  #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                               (Default : 224.0.1.129)        #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                               (Default : 0.0.0.0)            #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                               (Default : auto)               #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                               (Default : 319)                #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                               (Default : 319)                #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                               (Default : 52)                 #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                               (Default : 0)                  #
#                                                                              #
#   transport_specific            : (Optional)  Transport Specific             #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default : 0)                  #
#                                                                              #
#   version_ptp                   : (Optional)  PTP version number             #
#                                               (Default : 2)                  #
#                                                                              #
#   message_length                : (Optional)  PTP Annouce message length     #
#                                               (Default : 44)                 #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                               (Default : 0)                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                               (Default : 0)                  #
#                                                                              #
#   two_step_flag                 : (Optional)  Two Step Flag                  #
#                                               (Default : 0)                  #
#                                                                              #
#   unicast_flag                  : (Optional)  Unicast Flag                   #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                               (Default : 0)                  #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP TimeSclae Flag             #
#                                               (Default : 0)                  #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                               (Default : 0)                  #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                               (Default : 0)                  #
#                                                                              #
#   correction_field              : (Optional)  Correction Field value         #
#                                               (Default : 0)                  #
#                                                                              #
#   src_port_number               : (Optional)  Source Port number             #
#                                               (Default : 0)                  #
#                                                                              #
#   src_clock_identity            : (Optional)  Source Clock identity          #
#                                               (Default : 0)                  #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                               (Default : 0)                  #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                               (Default : 0)                  #
#                                                                              #
#   log_message_interval          : (Optional)  Log message interval           #
#                                               (Default : 127)                #
#                                                                              #
#   origin_timestamp_sec          : (Optional)  Origin Timestamp Sec           #
#                                               (Default : 0)                  #
#                                                                              #
#   origin_timestamp_ns           : (Optional)  Origin Timestamp Ns            #
#                                               (Default : 0)                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default : 1)                  #
#                                                                              #
#   interval                      : (Optional)  Frame interval (in seconds)    #
#                                               (Default : 1)                  #
#                                                                              #
#  OUTPUT PARAMETERS      :  NONE                                              #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is successfully send   #
#                                         with the given parameters)           #
#                                                                              #
#                         : 0 on failure (If the packet could not be send      #
#                                         with the given parameters)           #
#                                                                              #
#  DEFINITION             : This function is used to construct and send        #
#                           PTP sync message                                   #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       wrptp::send_sync\                                                      #
#           -port_num                         $port_num\                       #
#           -dest_mac                         $dest_mac\                       #
#           -src_mac                          $src_mac\                        #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -vlan_priority                    $vlan_priority\                  #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -transport_specific               $transport_specific\             #
#           -message_type                     $message_type\                   #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -two_step_flag                    $two_step_flag\                  #
#           -unicast_flag                     $unicast_flag\                   #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -correction_field                 $correction_field\               #
#           -src_port_number                  $src_port_number\                #
#           -src_clock_identity               $src_clock_identity\             #
#           -sequence_id                      $sequence_id\                    #
#           -control_field                    $control_field\                  #
#           -log_message_interval             $log_message_interval\           #
#           -origin_timestamp_sec             $origin_timestamp_sec\           #
#           -origin_timestamp_ns              $origin_timestamp_ns\            #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#                                                                              #
################################################################################
proc wrptp::send_sync {args} {

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "wrptp::send_sync: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #MISCELLANEOUS
    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
        LOG -level 1 -msg "\t\tport_num                       -> $port_num"
    } else {

        ERRLOG -msg "wrptp::send_sync: Missing Argument -> port_num"
        return 0
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
        LOG -level 1 -msg "\t\tcount                          -> $count"
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
        LOG -level 1 -msg "\t\tinterval                       -> $interval"
    } else {

        set interval $::wrptp_sync_interval
    }


    #ETHERNET HEADER OPTIONS
    LOG -level 1 -msg "\tEthernet Details"

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
        LOG -level 1 -msg "\t\tdest_mac                       -> $dest_mac"
    } else {

        ERRLOG -msg "wrptp::send_sync: Missing Argument -> dest_mac"
        return 0
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
        LOG -level 1 -msg "\t\tsrc_mac                        -> $src_mac"
    } else {

        ERRLOG -msg "wrptp::send_sync: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        LOG -level 1 -msg "\t\teth_type                       -> $eth_type"
    } else {

        if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
            set eth_type 0800
        } else {
            set eth_type 88F7
        }
    }

    #Vlan ID
    if { $::wrptp_vlan_encap == "true" } {

        LOG -level 1 -msg "\tVLAN Details"

        if {[info exists param(-vlan_id)]} {

            set vlan_id $param(-vlan_id)
            LOG -level 1 -msg "\t\tvlan_id                        -> $vlan_id"
        } else {

            set vlan_id $::wrptp_vlan_id
        }

        if {[info exists param(-vlan_priority)]} {

            set vlan_priority $param(-vlan_priority)
            LOG -level 1 -msg "\t\tvlan_priority                  -> $vlan_priority"
        } else {

            set vlan_priority $::wrptp_vlan_priority
        }

    } else {

        set vlan_id -1
        set vlan_priority 0
    }

    #IP HEADER OPTIONS
    if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {

        LOG -level 1 -msg "\tIP Details"

        #Destination IP address
        if {[info exists param(-dest_ip)]} {

            set dest_ip $param(-dest_ip)
            LOG -level 1 -msg "\t\tdest_ip                        -> $dest_ip"
        } else {

            set dest_ip 224.0.1.129
        }

        #Source IP address
        if {[info exists param(-src_ip)]} {

            set src_ip $param(-src_ip)
            LOG -level 1 -msg "\t\tsrc_ip                         -> $src_ip"
        } else {

            set src_ip $::tee_test_port_ip
        }

        #IP checksum
        if {[info exists param(-ip_checksum)]} {

            set ip_checksum $param(-ip_checksum)
            LOG -level 1 -msg "\t\tip_checksum                    -> $ip_checksum"
        } else {

            set ip_checksum auto
        }

        #UDP HEADER OPTIONS

        #UDP destination port number
        if {[info exists param(-dest_port)]} {

            set dest_port $param(-dest_port)
            LOG -level 1 -msg "\t\tdest_port                      -> $dest_port"
        } else {

            set dest_port 319
        }

        #UDP source port number
        if {[info exists param(-src_port)]} {

            set src_port $param(-src_port)
            LOG -level 1 -msg "\t\tsrc_port                       -> $src_port"
        } else {

            set src_port 319
        }

        #UDP length
        if {[info exists param(-udp_length)]} {

            set udp_length $param(-udp_length)
            LOG -level 1 -msg "\t\tudp_length                     -> $udp_length"
        } else {

            set udp_length 52
        }

        #UDP checksum
        if {[info exists param(-udp_checksum)]} {

            set udp_checksum $param(-udp_checksum)
            LOG -level 1 -msg "\t\tudp_checksum                   -> $udp_checksum"
        } else {

            set udp_checksum 0
        }
    } else {
        set dest_ip 0.0.0.0
        set src_ip 0.0.0.0
        set ip_checksum 0
        set dest_port 0
        set src_port 0
        set udp_length 0
        set udp_checksum 0
    }

    #PTP HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Details"

    #Transport Specific
    if {[info exists param(-transport_specific)]} {

        set transport_specific $param(-transport_specific)
        LOG -level 1 -msg "\t\ttransport_specific             -> $transport_specific"
    } else {

        set transport_specific 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        LOG -level 1 -msg "\t\tmessage_type                   -> $message_type"
    } else {

        set message_type 0
    }

    #PTP version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        LOG -level 1 -msg "\t\tversion_ptp                    -> $version_ptp"
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        LOG -level 1 -msg "\t\tmessage_length                 -> $message_length"
    } else {

        set message_length 44
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        LOG -level 1 -msg "\t\tdomain_number                  -> $domain_number"
    } else {

        set domain_number 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
        LOG -level 1 -msg "\t\tcorrection_field               -> $correction_field"
    } else {

        set correction_field 0
    }

    #Source Port Number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
        LOG -level 1 -msg "\t\tsrc_port_number                -> $src_port_number"
    } else {

        if {![regexp {.*?([0-9]+)$} $port_num match src_port_number]} {
            set src_port_number 1
        }

    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
        LOG -level 1 -msg "\t\tsrc_clock_identity             -> 0x[dec2hex $src_clock_identity 16]"
    } else {

        get_clock_identity\
                    -mac               $src_mac\
                    -format            $::ptp_clock_identity_format\
                    -clock_identity    src_clock_identity

    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        LOG -level 1 -msg "\t\tsequence_id                    -> $sequence_id"
    } else {

        set sequence_id 1
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        LOG -level 1 -msg "\t\tcontrol_field                  -> $control_field"
    } else {

        set control_field 0
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
        LOG -level 1 -msg "\t\tlog_message_interval           -> $log_message_interval"
    } else {

        set log_message_interval 127
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    LOG -level 1 -msg "\tPTP Flag Details"
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        LOG -level 1 -msg "\t\talternate_master_flag          -> $alternate_master_flag"
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
        LOG -level 1 -msg "\t\ttwo_step_flag                  -> $two_step_flag"
    } else {

        set two_step_flag $::ptp_two_step_flag
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
        LOG -level 1 -msg "\t\tunicast_flag                   -> $unicast_flag"
    } else {

        set unicast_flag $::unicast_flag
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        LOG -level 1 -msg "\t\tprofile_specific1              -> $profile_specific1"
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        LOG -level 1 -msg "\t\tprofile_specific2              -> $profile_specific2"
    } else {

        set profile_specific2 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        LOG -level 1 -msg "\t\tleap61                         -> $leap61"
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        LOG -level 1 -msg "\t\tleap59                         -> $leap59"
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        LOG -level 1 -msg "\t\tcurrent_utc_offset_valid       -> $current_utc_offset_valid"
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        LOG -level 1 -msg "\t\tptp_timescale                  -> $ptp_timescale"
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        LOG -level 1 -msg "\t\ttime_traceable                 -> $time_traceable"
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        LOG -level 1 -msg "\t\tfreq_traceable                 -> $freq_traceable"
    } else {

        set freq_traceable 0
    }

    #PTP SYNC HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Sync Message Details"

    if {[info exists param(-origin_timestamp_sec)]} {

        set origin_timestamp_sec $param(-origin_timestamp_sec)
        LOG -level 1 -msg "\t\torigin_timestamp_sec           -> $origin_timestamp_sec"
    } else {

        set origin_timestamp_sec [clock seconds]
    }

    if {[info exists param(-origin_timestamp_ns)]} {

        set origin_timestamp_ns $param(-origin_timestamp_ns)
        LOG -level 1 -msg "\t\torigin_timestamp_ns            -> $origin_timestamp_ns"
    } else {

        set origin_timestamp_ns 0
    }

    set instance $::INSTANCE(SYNC)

    #CALL PLATFORM FUNCTION
    if {![pltLib::send_wrptp_pkt\
                -session_id                        $session_id\
                -instance                          $instance\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -transport_specific                $transport_specific\
                -message_type                      $message_type\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_traceable                    $time_traceable\
                -freq_traceable                    $freq_traceable\
                -correction_field                  $correction_field\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -origin_timestamp_sec              $origin_timestamp_sec\
                -origin_timestamp_ns               $origin_timestamp_ns\
                -count                             $count\
                -interval                          $interval]} {

        return 0

    }

    return 1

}

################################################################################
#                                                                              #
#  PROCEDURE NAME         :  wrptp::send_announce                              #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Transmission port              #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   dest_mac                      : (Mandatory) Destination MAC dddress.       #
#                                               (e.g., 01:80:C2:00:00:20)      #
#                                                                              #
#   src_mac                       : (Mandatory) Source MAC address.            #
#                                               (e.g., 00:80:C2:00:00:21)      #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                               (Default : 88F7)               #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                               (Default : 0)                  #
#                                                                              #
#   vlan_priority                 : (Optional)  Vlan Priority                  #
#                                               (Default : 0)                  #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                               (Default : 224.0.1.129)        #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                               (Default : 0.0.0.0)            #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                               (Default : auto)               #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                               (Default : 320)                #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                               (Default : 320)                #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                               (Default : 72)                 #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                               (Default : 0)                  #
#                                                                              #
#   transport_specific            : (Optional)  Transport Specific             #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default : 11)                 #
#                                                                              #
#   version_ptp                   : (Optional)  PTP version number             #
#                                               (Default : 2)                  #
#                                                                              #
#   message_length                : (Optional)  PTP Annouce message length     #
#                                               (Default : 64)                 #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                               (Default : 0)                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                               (Default : 0)                  #
#                                                                              #
#   two_step_flag                 : (Optional)  Two Step Flag                  #
#                                               (Default : 0)                  #
#                                                                              #
#   unicast_flag                  : (Optional)  Unicast Flag                   #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                               (Default : 0)                  #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP TimeSclae Flag             #
#                                               (Default : 0)                  #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                               (Default : 0)                  #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                               (Default : 0)                  #
#                                                                              #
#   correction_field              : (Optional)  Correction Field value         #
#                                               (Default : 0)                  #
#                                                                              #
#   src_port_number               : (Optional)  Source Port number             #
#                                               (Default : 0)                  #
#                                                                              #
#   src_clock_identity            : (Optional)  Source Clock identity          #
#                                               (Default : 0)                  #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                               (Default : 0)                  #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                               (Default : 0)                  #
#                                                                              #
#   log_message_interval          : (Optional)  Log message interval           #
#                                               (Default : 127)                #
#                                                                              #
#   origin_timestamp_sec          : (Optional)  Origin Timestamp Sec           #
#                                               (Default : 0)                  #
#                                                                              #
#   origin_timestamp_ns           : (Optional)  Origin Timestamp Ns            #
#                                               (Default : 0)                  #
#                                                                              #
#   current_utc_offset            : (Optional)  Current Utc Offset             #
#                                               (Default : 0)                  #
#                                                                              #
#   gm_priority1                  : (Optional)  Gm Priority1                   #
#                                               (Default : 0)                  #
#                                                                              #
#   gm_priority2                  : (Optional)  Gm Priority2                   #
#                                               (Default : 0)                  #
#                                                                              #
#   gm_identity                   : (Optional)  Gm Identity                    #
#                                               (Default : 0)                  #
#                                                                              #
#   steps_removed                 : (Optional)  Steps Removed                  #
#                                               (Default : 0)                  #
#                                                                              #
#   time_source                   : (Optional)  Time Source                    #
#                                               (Default : 0)                  #
#                                                                              #
#   gm_clock_class                : (Optional)  Gm Clock Class                 #
#                                               (Default : 0)                  #
#                                                                              #
#   gm_clock_accuracy             : (Optional)  Gm Clock Accuracy              #
#                                               (Default : 0)                  #
#                                                                              #
#   gm_clock_variance             : (Optional)  Gm Clock Variance              #
#                                               (Default : 0)                  #
#                                                                              #
#  (PTP TLVs)                                                                  #
#                                                                              #
#   tlv_type                      : (Optional)  TLV Type                       #
#                                               (Default : -1)                 #
#                                                                              #
#   tlv_length                    : (Optional)  TLV length                     #
#                                               (Default : 8)                  #
#                                                                              #
#   organization_id               : (Optional) Organization id                 #
#                                               (Default: 0x080030)            #
#                                                                              #
#   magic_number                  : (Optional) Magic number                    #
#                                               (Default: 0xDEAD)              #
#                                                                              #
#   version_number                : (Optional) Version number                  #
#                                               (Default: 1)                   #
#                                                                              #
#   message_id                    : (Optional) Message id                      #
#                                               (Default: 0x2000)              #
#                                                                              #
#   wrconfig                      : (Optional) Wrconfig                        #
#                                               (Default: 0)                   #
#                                                                              #
#   calibrated                    : (Optional) Calibrated                      #
#                                               (Default: 0)                   #
#                                                                              #
#   wrmode_on                     : (Optional) WR Mode ON                      #
#                                               (Default: 0)                   #
#                                                                              #
#  (MISC)                                                                      #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default : 1)                  #
#                                                                              #
#   interval                      : (Optional)  Frame interval (in seconds)    #
#                                               (Default : 1)                  #
#                                                                              #
#  OUTPUT PARAMETERS      :  NONE                                              #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is successfully send   #
#                                         with the given parameters)           #
#                                                                              #
#                         : 0 on failure (If the packet could not be send      #
#                                         with the given parameters)           #
#                                                                              #
#  DEFINITION             : This function is used to construct and send        #
#                           PTP announce message                               #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       wrptp::send_announce\                                                  #
#           -port_num                         $port_num\                       #
#           -dest_mac                         $dest_mac\                       #
#           -src_mac                          $src_mac\                        #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -vlan_priority                    $vlan_priority\                  #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -transport_specific               $transport_specific\             #
#           -message_type                     $message_type\                   #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -two_step_flag                    $two_step_flag\                  #
#           -unicast_flag                     $unicast_flag\                   #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -correction_field                 $correction_field\               #
#           -src_port_number                  $src_port_number\                #
#           -src_clock_identity               $src_clock_identity\             #
#           -sequence_id                      $sequence_id\                    #
#           -control_field                    $control_field\                  #
#           -log_message_interval             $log_message_interval\           #
#           -origin_timestamp_sec             $origin_timestamp_sec\           #
#           -origin_timestamp_ns              $origin_timestamp_ns\            #
#           -current_utc_offset               $current_utc_offset\             #
#           -gm_priority1                     $gm_priority1\                   #
#           -gm_priority2                     $gm_priority2\                   #
#           -gm_identity                      $gm_identity\                    #
#           -steps_removed                    $steps_removed\                  #
#           -gm_clock_class                   $gm_clock_class\                 #
#           -gm_clock_accuracy                $gm_clock_accuracy\              #
#           -gm_clock_variance                $gm_clock_variance\              #
#           -tlv_type                         $tlv_type\                       #
#           -tlv_length                       $tlv_length\                     #
#           -organization_id                  $organization_id\                #
#           -magic_number                     $magic_number\                   #
#           -version_number                   $version_number\                 #
#           -message_id                       $message_id\                     #
#           -wrconfig                         $wrconfig\                       #
#           -calibrated                       $calibrated\                     #
#           -wrmode_on                        $wrmode_on\                      #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#                                                                              #
################################################################################
proc wrptp::send_announce {args} {

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "wrptp::send_announce: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #MISCELLANEOUS
    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
        LOG -level 1 -msg "\t\tport_num                       -> $port_num"
    } else {

        ERRLOG -msg "wrptp::send_announce: Missing Argument -> port_num"
        return 0
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
        LOG -level 1 -msg "\t\tcount                          -> $count"
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
        LOG -level 1 -msg "\t\tinterval                       -> $interval"
    } else {

        set interval $::wrptp_announce_interval
    }


    #ETHERNET HEADER OPTIONS
    LOG -level 1 -msg "\tEthernet Details"

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
        LOG -level 1 -msg "\t\tdest_mac                       -> $dest_mac"
    } else {

        ERRLOG -msg "wrptp::send_announce: Missing Argument -> dest_mac"
        return 0
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
        LOG -level 1 -msg "\t\tsrc_mac                        -> $src_mac"
    } else {

        ERRLOG -msg "wrptp::send_announce: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        LOG -level 1 -msg "\t\teth_type                       -> $eth_type"
    } else {

        if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
            set eth_type 0800
        } else {
            set eth_type 88F7
        }
    }

    #Vlan ID
    if { $::wrptp_vlan_encap == "true" } {

        LOG -level 1 -msg "\tVLAN Details"

        if {[info exists param(-vlan_id)]} {

            set vlan_id $param(-vlan_id)
            LOG -level 1 -msg "\t\tvlan_id                        -> $vlan_id"
        } else {

            set vlan_id $::wrptp_vlan_id
        }

        if {[info exists param(-vlan_priority)]} {

            set vlan_priority $param(-vlan_priority)
            LOG -level 1 -msg "\t\tvlan_priority                  -> $vlan_priority"
        } else {

            set vlan_priority $::wrptp_vlan_priority
        }

    } else {

        set vlan_id -1
        set vlan_priority 0
    }

    #IP HEADER OPTIONS
    if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {

        LOG -level 1 -msg "\tIP Details"

        #Destination IP address
        if {[info exists param(-dest_ip)]} {

            set dest_ip $param(-dest_ip)
            LOG -level 1 -msg "\t\tdest_ip                        -> $dest_ip"
        } else {

            set dest_ip 224.0.1.129
        }

        #Source IP address
        if {[info exists param(-src_ip)]} {

            set src_ip $param(-src_ip)
            LOG -level 1 -msg "\t\tsrc_ip                         -> $src_ip"
        } else {

            set src_ip $::tee_test_port_ip
        }

        #IP checksum
        if {[info exists param(-ip_checksum)]} {

            set ip_checksum $param(-ip_checksum)
            LOG -level 1 -msg "\t\tip_checksum                    -> $ip_checksum"
        } else {

            set ip_checksum auto
        }

        #UDP HEADER OPTIONS

        #UDP destination port number
        if {[info exists param(-dest_port)]} {

            set dest_port $param(-dest_port)
            LOG -level 1 -msg "\t\tdest_port                      -> $dest_port"
        } else {

            set dest_port 320
        }

        #UDP source port number
        if {[info exists param(-src_port)]} {

            set src_port $param(-src_port)
            LOG -level 1 -msg "\t\tsrc_port                       -> $src_port"
        } else {

            set src_port 320
        }

        #UDP length
        if {[info exists param(-udp_length)]} {

            set udp_length $param(-udp_length)
            LOG -level 1 -msg "\t\tudp_length                     -> $udp_length"
        } else {

            set udp_length 72
        }

        #UDP checksum
        if {[info exists param(-udp_checksum)]} {

            set udp_checksum $param(-udp_checksum)
            LOG -level 1 -msg "\t\tudp_checksum                   -> $udp_checksum"
        } else {

            set udp_checksum 0
        }
    } else {
        set dest_ip 0.0.0.0
        set src_ip 0.0.0.0
        set ip_checksum 0
        set dest_port 0
        set src_port 0
        set udp_length 0
        set udp_checksum 0
    }

    #PTP HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Details"

    #Transport Specific
    if {[info exists param(-transport_specific)]} {

        set transport_specific $param(-transport_specific)
        LOG -level 1 -msg "\t\ttransport_specific             -> $transport_specific"
    } else {

        set transport_specific 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        LOG -level 1 -msg "\t\tmessage_type                   -> $message_type"
    } else {

        set message_type 11
    }

    #PTP version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        LOG -level 1 -msg "\t\tversion_ptp                    -> $version_ptp"
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        LOG -level 1 -msg "\t\tmessage_length                 -> $message_length"
    } else {

        set message_length 64
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        LOG -level 1 -msg "\t\tdomain_number                  -> $domain_number"
    } else {

        set domain_number 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
        LOG -level 1 -msg "\t\tcorrection_field               -> $correction_field"
    } else {

        set correction_field 0
    }

    #Source Port Number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
        LOG -level 1 -msg "\t\tsrc_port_number                -> $src_port_number"
    } else {

        if {![regexp {.*?([0-9]+)$} $port_num match src_port_number]} {
            set src_port_number 1
        }

    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
        LOG -level 1 -msg "\t\tsrc_clock_identity             -> 0x[dec2hex $src_clock_identity 16]"
    } else {

        get_clock_identity\
                    -mac               $src_mac\
                    -format            $::ptp_clock_identity_format\
                    -clock_identity    src_clock_identity
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        LOG -level 1 -msg "\t\tsequence_id                    -> $sequence_id"
    } else {

        set sequence_id 1
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        LOG -level 1 -msg "\t\tcontrol_field                  -> $control_field"
    } else {

        set control_field 5
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
        LOG -level 1 -msg "\t\tlog_message_interval           -> $log_message_interval"
    } else {

        set log_message_interval 127
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    LOG -level 1 -msg "\tPTP Flag Details"
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        LOG -level 1 -msg "\t\talternate_master_flag          -> $alternate_master_flag"
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
        LOG -level 1 -msg "\t\ttwo_step_flag                  -> $two_step_flag"
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
        LOG -level 1 -msg "\t\tunicast_flag                   -> $unicast_flag"
    } else {

        set unicast_flag $::unicast_flag
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        LOG -level 1 -msg "\t\tprofile_specific1              -> $profile_specific1"
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        LOG -level 1 -msg "\t\tprofile_specific2              -> $profile_specific2"
    } else {

        set profile_specific2 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        LOG -level 1 -msg "\t\tleap61                         -> $leap61"
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        LOG -level 1 -msg "\t\tleap59                         -> $leap59"
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        LOG -level 1 -msg "\t\tcurrent_utc_offset_valid       -> $current_utc_offset_valid"
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        LOG -level 1 -msg "\t\tptp_timescale                  -> $ptp_timescale"
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        LOG -level 1 -msg "\t\ttime_traceable                 -> $time_traceable"
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        LOG -level 1 -msg "\t\tfreq_traceable                 -> $freq_traceable"
    } else {

        set freq_traceable 0
    }

    #PTP ANNOUNCE HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Announce Message Details"

    if {[info exists param(-origin_timestamp_sec)]} {

        set origin_timestamp_sec $param(-origin_timestamp_sec)
        LOG -level 1 -msg "\t\torigin_timestamp_sec           -> $origin_timestamp_sec"
    } else {

        set origin_timestamp_sec [clock seconds]
    }

    if {[info exists param(-origin_timestamp_ns)]} {

        set origin_timestamp_ns $param(-origin_timestamp_ns)
        LOG -level 1 -msg "\t\torigin_timestamp_ns            -> $origin_timestamp_ns"
    } else {

        set origin_timestamp_ns 0
    }

    if {[info exists param(-current_utc_offset)]} {

        set current_utc_offset $param(-current_utc_offset)
        LOG -level 1 -msg "\t\tcurrent_utc_offset             -> $current_utc_offset"
    } else {

        set current_utc_offset  0
    }

    if {[info exists param(-gm_priority1)]} {

        set gm_priority1 $param(-gm_priority1)
        LOG -level 1 -msg "\t\tgm_priority1                   -> $gm_priority1"
    } else {

        set gm_priority1 0
    }

    if {[info exists param(-gm_priority2)]} {

        set gm_priority2 $param(-gm_priority2)
        LOG -level 1 -msg "\t\tgm_priority2                   -> $gm_priority2"
    } else {

        set gm_priority2 0
    }

    if {[info exists param(-gm_identity)]} {

        set gm_identity $param(-gm_identity)
        LOG -level 1 -msg "\t\tgm_identity                    -> $gm_identity"
    } else {

        set gm_identity 0
    }

    if {[info exists param(-steps_removed)]} {

        set steps_removed $param(-steps_removed)
        LOG -level 1 -msg "\t\tsteps_removed                  -> $steps_removed"
    } else {

        set steps_removed 0
    }

    if {[info exists param(-time_source)]} {

        set time_source $param(-time_source)
        LOG -level 1 -msg "\t\ttime_source                    -> $time_source"
    } else {

        set time_source 0
    }

    if {[info exists param(-gm_clock_class)]} {

        set gm_clock_class $param(-gm_clock_class)
        LOG -level 1 -msg "\t\tgm_clock_class                 -> $gm_clock_class"
    } else {

        set gm_clock_class 0
    }

    if {[info exists param(-gm_clock_accuracy)]} {

        set gm_clock_accuracy $param(-gm_clock_accuracy)
        LOG -level 1 -msg "\t\tgm_clock_accuracy              -> $gm_clock_accuracy"
    } else {

        set gm_clock_accuracy 0
    }

    if {[info exists param(-gm_clock_variance)]} {

        set gm_clock_variance $param(-gm_clock_variance)
        LOG -level 1 -msg "\t\tgm_clock_variance              -> $gm_clock_variance"
    } else {

        set gm_clock_variance 0
    }

    #TLV Type
    if {[info exists param(-tlv_type)]} {

        set tlv_type $param(-tlv_type)
    } else {

        set tlv_type -1
    }

    #TLV length
    if {[info exists param(-tlv_length)]} {

        set tlv_length $param(-tlv_length)
    } else {

        set tlv_length 8
    }

    #Organisation ID
    if {[info exists param(-organization_id)]} {

        set organization_id $param(-organization_id)
    } else {

        set organization_id 0x080030
    }

    #Magic Number
    if {[info exists param(-magic_number)]} {

        set magic_number $param(-magic_number)
    } else {

        set magic_number 0xDEAD
    }

    #Version Number
    if {[info exists param(-version_number)]} {

        set version_number $param(-version_number)
    } else {

        set version_number 1
    }

    #Message ID
    if {[info exists param(-message_id)]} {

        set message_id $param(-message_id)
    } else {

        set message_id 0x2000
    }

    #WR Config
    if {[info exists param(-wrconfig)]} {

        set wrconfig $param(-wrconfig)
    } else {

        set wrconfig 0
    }

    #Calibrated
    if {[info exists param(-calibrated)]} {

        set calibrated $param(-calibrated)
    } else {

        set calibrated 0
    }

    #WR Mode ON
    if {[info exists param(-wrmode_on)]} {

        set wrmode_on $param(-wrmode_on)
    } else {

        set wrmode_on 0
    }

    set instance $::INSTANCE(ANNOUNCE)

    #CALL PLATFORM FUNCTION
    if {![pltLib::send_wrptp_pkt\
                -session_id                        $session_id\
                -instance                          $instance\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -transport_specific                $transport_specific\
                -message_type                      $message_type\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_traceable                    $time_traceable\
                -freq_traceable                    $freq_traceable\
                -correction_field                  $correction_field\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -origin_timestamp_sec              $origin_timestamp_sec\
                -origin_timestamp_ns               $origin_timestamp_ns\
                -current_utc_offset                $current_utc_offset\
                -gm_priority1                      $gm_priority1\
                -gm_priority2                      $gm_priority2\
                -gm_identity                       $gm_identity\
                -steps_removed                     $steps_removed\
                -time_source                       $time_source\
                -gm_clock_class                    $gm_clock_class\
                -gm_clock_accuracy                 $gm_clock_accuracy\
                -gm_clock_variance                 $gm_clock_variance\
                -tlv_type                          $tlv_type\
                -tlv_length                        $tlv_length\
                -organization_id                   $organization_id\
                -magic_number                      $magic_number\
                -version_number                    $version_number\
                -message_id                        $message_id\
                -wrconfig                          $wrconfig\
                -calibrated                        $calibrated\
                -wrmode_on                         $wrmode_on\
                -count                             $count\
                -interval                          $interval]} {

        return 0

    }

    return 1

}

################################################################################
#                                                                              #
#  PROCEDURE NAME         :  wrptp::send_delay_req                             #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Transmission port              #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   dest_mac                      : (Mandatory) Destination MAC dddress.       #
#                                               (e.g., 01:80:C2:00:00:20)      #
#                                                                              #
#   src_mac                       : (Mandatory) Source MAC address.            #
#                                               (e.g., 00:80:C2:00:00:21)      #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                               (Default : 88F7)               #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                               (Default : 0)                  #
#                                                                              #
#   vlan_priority                 : (Optional)  Vlan Priority                  #
#                                               (Default : 0)                  #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                               (Default : 224.0.1.129)        #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                               (Default : 0.0.0.0)            #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                               (Default : auto)               #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                               (Default : 319)                #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                               (Default : 319)                #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                               (Default : 52)                 #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                               (Default : 0)                  #
#                                                                              #
#   transport_specific            : (Optional)  Transport Specific             #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default : 1)                  #
#                                                                              #
#   version_ptp                   : (Optional)  PTP version number             #
#                                               (Default : 2)                  #
#                                                                              #
#   message_length                : (Optional)  PTP Annouce message length     #
#                                               (Default : 44)                 #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                               (Default : 0)                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                               (Default : 0)                  #
#                                                                              #
#   two_step_flag                 : (Optional)  Two Step Flag                  #
#                                               (Default : 0)                  #
#                                                                              #
#   unicast_flag                  : (Optional)  Unicast Flag                   #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                               (Default : 0)                  #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP TimeSclae Flag             #
#                                               (Default : 0)                  #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                               (Default : 0)                  #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                               (Default : 0)                  #
#                                                                              #
#   correction_field              : (Optional)  Correction Field value         #
#                                               (Default : 0)                  #
#                                                                              #
#   src_port_number               : (Optional)  Source Port number             #
#                                               (Default : 0)                  #
#                                                                              #
#   src_clock_identity            : (Optional)  Source Clock identity          #
#                                               (Default : 0)                  #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                               (Default : 0)                  #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                               (Default : 1)                  #
#                                                                              #
#   log_message_interval          : (Optional)  Log message interval           #
#                                               (Default : 127)                #
#                                                                              #
#   origin_timestamp_sec          : (Optional)  Origin Timestamp Sec           #
#                                               (Default : 0)                  #
#                                                                              #
#   origin_timestamp_ns           : (Optional)  Origin Timestamp Ns            #
#                                               (Default : 0)                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default : 1)                  #
#                                                                              #
#   interval                      : (Optional)  Frame interval (in seconds)    #
#                                               (Default : 1)                  #
#                                                                              #
#  OUTPUT PARAMETERS      :  NONE                                              #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is successfully send   #
#                                         with the given parameters)           #
#                                                                              #
#                         : 0 on failure (If the packet could not be send      #
#                                         with the given parameters)           #
#                                                                              #
#  DEFINITION             : This function is used to construct and send        #
#                           PTP delay req message                              #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       wrptp::send_delay_req\                                                 #
#           -port_num                         $port_num\                       #
#           -dest_mac                         $dest_mac\                       #
#           -src_mac                          $src_mac\                        #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -vlan_priority                    $vlan_priority\                  #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -transport_specific               $transport_specific\             #
#           -message_type                     $message_type\                   #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -two_step_flag                    $two_step_flag\                  #
#           -unicast_flag                     $unicast_flag\                   #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -correction_field                 $correction_field\               #
#           -src_port_number                  $src_port_number\                #
#           -src_clock_identity               $src_clock_identity\             #
#           -sequence_id                      $sequence_id\                    #
#           -control_field                    $control_field\                  #
#           -log_message_interval             $log_message_interval\           #
#           -origin_timestamp_sec             $origin_timestamp_sec\           #
#           -origin_timestamp_ns              $origin_timestamp_ns\            #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#                                                                              #
################################################################################
proc wrptp::send_delay_req {args} {

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "wrptp::send_delay_req: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #MISCELLANEOUS
    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
        LOG -level 1 -msg "\t\tport_num                       -> $port_num"
    } else {

        ERRLOG -msg "wrptp::send_delay_req: Missing Argument -> port_num"
        return 0
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
        LOG -level 1 -msg "\t\tcount                          -> $count"
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
        LOG -level 1 -msg "\t\tinterval                       -> $interval"
    } else {

        set interval $::wrptp_sync_interval
    }


    #ETHERNET HEADER OPTIONS
    LOG -level 1 -msg "\tEthernet Details"

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
        LOG -level 1 -msg "\t\tdest_mac                       -> $dest_mac"
    } else {

        ERRLOG -msg "wrptp::send_delay_req: Missing Argument -> dest_mac"
        return 0
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
        LOG -level 1 -msg "\t\tsrc_mac                        -> $src_mac"
    } else {

        ERRLOG -msg "wrptp::send_delay_req: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        LOG -level 1 -msg "\t\teth_type                       -> $eth_type"
    } else {

        if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
            set eth_type 0800
        } else {
            set eth_type 88F7
        }
    }

    #Vlan ID
    if { $::wrptp_vlan_encap == "true" } {

        LOG -level 1 -msg "\tVLAN Details"

        if {[info exists param(-vlan_id)]} {

            set vlan_id $param(-vlan_id)
            LOG -level 1 -msg "\t\tvlan_id                        -> $vlan_id"
        } else {

            set vlan_id $::wrptp_vlan_id
        }

        if {[info exists param(-vlan_priority)]} {

            set vlan_priority $param(-vlan_priority)
            LOG -level 1 -msg "\t\tvlan_priority                  -> $vlan_priority"
        } else {

            set vlan_priority $::wrptp_vlan_priority
        }

    } else {

        set vlan_id -1
        set vlan_priority 0
    }

    #IP HEADER OPTIONS
    if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {

        LOG -level 1 -msg "\tIP Details"

        #Destination IP address
        if {[info exists param(-dest_ip)]} {

            set dest_ip $param(-dest_ip)
            LOG -level 1 -msg "\t\tdest_ip                        -> $dest_ip"
        } else {

            set dest_ip 224.0.1.129
        }

        #Source IP address
        if {[info exists param(-src_ip)]} {

            set src_ip $param(-src_ip)
            LOG -level 1 -msg "\t\tsrc_ip                         -> $src_ip"
        } else {

            set src_ip $::tee_test_port_ip
        }

        #IP checksum
        if {[info exists param(-ip_checksum)]} {

            set ip_checksum $param(-ip_checksum)
            LOG -level 1 -msg "\t\tip_checksum                    -> $ip_checksum"
        } else {

            set ip_checksum auto
        }

        #UDP HEADER OPTIONS

        #UDP destination port number
        if {[info exists param(-dest_port)]} {

            set dest_port $param(-dest_port)
            LOG -level 1 -msg "\t\tdest_port                      -> $dest_port"
        } else {

            set dest_port 319
        }

        #UDP source port number
        if {[info exists param(-src_port)]} {

            set src_port $param(-src_port)
            LOG -level 1 -msg "\t\tsrc_port                       -> $src_port"
        } else {

            set src_port 319
        }

        #UDP length
        if {[info exists param(-udp_length)]} {

            set udp_length $param(-udp_length)
            LOG -level 1 -msg "\t\tudp_length                     -> $udp_length"
        } else {

            set udp_length 52
        }

        #UDP checksum
        if {[info exists param(-udp_checksum)]} {

            set udp_checksum $param(-udp_checksum)
            LOG -level 1 -msg "\t\tudp_checksum                   -> $udp_checksum"
        } else {

            set udp_checksum 0
        }
    } else {
        set dest_ip 0.0.0.0
        set src_ip 0.0.0.0
        set ip_checksum 0
        set dest_port 0
        set src_port 0
        set udp_length 0
        set udp_checksum 0
    }

    #PTP HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Details"

    #Transport Specific
    if {[info exists param(-transport_specific)]} {

        set transport_specific $param(-transport_specific)
        LOG -level 1 -msg "\t\ttransport_specific             -> $transport_specific"
    } else {

        set transport_specific 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        LOG -level 1 -msg "\t\tmessage_type                   -> $message_type"
    } else {

        set message_type 1
    }

    #PTP version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        LOG -level 1 -msg "\t\tversion_ptp                    -> $version_ptp"
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        LOG -level 1 -msg "\t\tmessage_length                 -> $message_length"
    } else {

        set message_length 44
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        LOG -level 1 -msg "\t\tdomain_number                  -> $domain_number"
    } else {

        set domain_number 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
        LOG -level 1 -msg "\t\tcorrection_field               -> $correction_field"
    } else {

        set correction_field 0
    }

    #Source Port Number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
        LOG -level 1 -msg "\t\tsrc_port_number                -> $src_port_number"
    } else {

        if {![regexp {.*?([0-9]+)$} $port_num match src_port_number]} {
            set src_port_number 1
        }

    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
        LOG -level 1 -msg "\t\tsrc_clock_identity             -> 0x[dec2hex $src_clock_identity 16]"
    } else {

        get_clock_identity\
                    -mac               $src_mac\
                    -format            $::ptp_clock_identity_format\
                    -clock_identity    src_clock_identity
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        LOG -level 1 -msg "\t\tsequence_id                    -> $sequence_id"
    } else {

        set sequence_id 1
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        LOG -level 1 -msg "\t\tcontrol_field                  -> $control_field"
    } else {

        set control_field 1
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
        LOG -level 1 -msg "\t\tlog_message_interval           -> $log_message_interval"
    } else {

        set log_message_interval 127
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    LOG -level 1 -msg "\tPTP Flag Details"
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        LOG -level 1 -msg "\t\talternate_master_flag          -> $alternate_master_flag"
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
        LOG -level 1 -msg "\t\ttwo_step_flag                  -> $two_step_flag"
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
        LOG -level 1 -msg "\t\tunicast_flag                   -> $unicast_flag"
    } else {

        set unicast_flag $::unicast_flag
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        LOG -level 1 -msg "\t\tprofile_specific1              -> $profile_specific1"
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        LOG -level 1 -msg "\t\tprofile_specific2              -> $profile_specific2"
    } else {

        set profile_specific2 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        LOG -level 1 -msg "\t\tleap61                         -> $leap61"
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        LOG -level 1 -msg "\t\tleap59                         -> $leap59"
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        LOG -level 1 -msg "\t\tcurrent_utc_offset_valid       -> $current_utc_offset_valid"
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        LOG -level 1 -msg "\t\tptp_timescale                  -> $ptp_timescale"
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        LOG -level 1 -msg "\t\ttime_traceable                 -> $time_traceable"
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        LOG -level 1 -msg "\t\tfreq_traceable                 -> $freq_traceable"
    } else {

        set freq_traceable 0
    }

    #PTP DELAY_REQ HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Delay_req Message Details"

    if {[info exists param(-origin_timestamp_sec)]} {

        set origin_timestamp_sec $param(-origin_timestamp_sec)
        LOG -level 1 -msg "\t\torigin_timestamp_sec           -> $origin_timestamp_sec"
    } else {

        set origin_timestamp_sec [clock seconds]
    }

    if {[info exists param(-origin_timestamp_ns)]} {

        set origin_timestamp_ns $param(-origin_timestamp_ns)
        LOG -level 1 -msg "\t\torigin_timestamp_ns            -> $origin_timestamp_ns"
    } else {

        set origin_timestamp_ns 0
    }

    set instance $::INSTANCE(DELAY_REQ)

    #CALL PLATFORM FUNCTION
    if {![pltLib::send_wrptp_pkt\
                -session_id                        $session_id\
                -instance                          $instance\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -transport_specific                $transport_specific\
                -message_type                      $message_type\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_traceable                    $time_traceable\
                -freq_traceable                    $freq_traceable\
                -correction_field                  $correction_field\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -origin_timestamp_sec              $origin_timestamp_sec\
                -origin_timestamp_ns               $origin_timestamp_ns\
                -count                             $count\
                -interval                          $interval]} {

        return 0

    }

    return 1

}

################################################################################
#                                                                              #
#  PROCEDURE NAME         :  wrptp::send_delay_resp                            #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Transmission port              #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   dest_mac                      : (Mandatory) Destination MAC dddress.       #
#                                               (e.g., 01:80:C2:00:00:20)      #
#                                                                              #
#   src_mac                       : (Mandatory) Source MAC address.            #
#                                               (e.g., 00:80:C2:00:00:21)      #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                               (Default : 88F7)               #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                               (Default : 0)                  #
#                                                                              #
#   vlan_priority                 : (Optional)  Vlan Priority                  #
#                                               (Default : 0)                  #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                               (Default : 224.0.1.129)        #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                               (Default : 0.0.0.0)            #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                               (Default : auto)               #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                               (Default : 320)                #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                               (Default : 320)                #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                               (Default : 62)                 #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                               (Default : 0)                  #
#                                                                              #
#   transport_specific            : (Optional)  Transport Specific             #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default : 9)                  #
#                                                                              #
#   version_ptp                   : (Optional)  PTP version number             #
#                                               (Default : 2)                  #
#                                                                              #
#   message_length                : (Optional)  PTP message length             #
#                                               (Default : 54)                 #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                               (Default : 0)                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                               (Default : 0)                  #
#                                                                              #
#   two_step_flag                 : (Optional)  Two Step Flag                  #
#                                               (Default : 0)                  #
#                                                                              #
#   unicast_flag                  : (Optional)  Unicast Flag                   #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                               (Default : 0)                  #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP TimeSclae Flag             #
#                                               (Default : 0)                  #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                               (Default : 0)                  #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                               (Default : 0)                  #
#                                                                              #
#   correction_field              : (Optional)  Correction Field value         #
#                                               (Default : 0)                  #
#                                                                              #
#   src_port_number               : (Optional)  Source Port number             #
#                                               (Default : 0)                  #
#                                                                              #
#   src_clock_identity            : (Optional)  Source Clock identity          #
#                                               (Default : 0)                  #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                               (Default : 0)                  #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                               (Default : 3)                  #
#                                                                              #
#   log_message_interval          : (Optional)  Log message interval           #
#                                               (Default : 127)                #
#                                                                              #
#   receive_timestamp_sec         : (Optional)  Receive Timestamp Sec          #
#                                               (Default : 0)                  #
#                                                                              #
#   receive_timestamp_ns          : (Optional)  Receive Timestamp Ns           #
#                                               (Default : 0)                  #
#                                                                              #
#   requesting_port_number        : (Optional)  Requesting Port Number         #
#                                               (Default : 0)                  #
#                                                                              #
#   requesting_clock_identity     : (Optional)  Requesting Clock Identity      #
#                                               (Default : 0)                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default : 1)                  #
#                                                                              #
#   interval                      : (Optional)  Frame interval (in seconds)    #
#                                               (Default : 1)                  #
#                                                                              #
#  OUTPUT PARAMETERS      :  NONE                                              #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is successfully send   #
#                                         with the given parameters)           #
#                                                                              #
#                         : 0 on failure (If the packet could not be send      #
#                                         with the given parameters)           #
#                                                                              #
#  DEFINITION             : This function is used to construct and send        #
#                           PTP delay resp message                             #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       wrptp::send_delay_resp\                                                #
#           -port_num                         $port_num\                       #
#           -dest_mac                         $dest_mac\                       #
#           -src_mac                          $src_mac\                        #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -vlan_priority                    $vlan_priority\                  #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -transport_specific               $transport_specific\             #
#           -message_type                     $message_type\                   #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -two_step_flag                    $two_step_flag\                  #
#           -unicast_flag                     $unicast_flag\                   #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -correction_field                 $correction_field\               #
#           -src_port_number                  $src_port_number\                #
#           -src_clock_identity               $src_clock_identity\             #
#           -sequence_id                      $sequence_id\                    #
#           -control_field                    $control_field\                  #
#           -log_message_interval             $log_message_interval\           #
#           -receive_timestamp_sec            $receive_timestamp_sec\          #
#           -receive_timestamp_ns             $receive_timestamp_ns\           #
#           -requesting_port_number           $requesting_port_number\         #
#           -requesting_clock_identity        $requesting_clock_identity\      #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#                                                                              #
################################################################################
proc wrptp::send_delay_resp {args} {

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "wrptp::send_delay_resp: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #MISCELLANEOUS
    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
        LOG -level 1 -msg "\t\tport_num                       -> $port_num"
    } else {

        ERRLOG -msg "wrptp::send_delay_resp: Missing Argument -> port_num"
        return 0
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
        LOG -level 1 -msg "\t\tcount                          -> $count"
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
        LOG -level 1 -msg "\t\tinterval                       -> $interval"
    } else {

        set interval $::wrptp_sync_interval
    }


    #ETHERNET HEADER OPTIONS
    LOG -level 1 -msg "\tEthernet Details"

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
        LOG -level 1 -msg "\t\tdest_mac                       -> $dest_mac"
    } else {

        ERRLOG -msg "wrptp::send_delay_resp: Missing Argument -> dest_mac"
        return 0
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
        LOG -level 1 -msg "\t\tsrc_mac                        -> $src_mac"
    } else {

        ERRLOG -msg "wrptp::send_delay_resp: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        LOG -level 1 -msg "\t\teth_type                       -> $eth_type"
    } else {

        if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
            set eth_type 0800
        } else {
            set eth_type 88F7
        }
    }

    #Vlan ID
    if { $::wrptp_vlan_encap == "true" } {

        LOG -level 1 -msg "\tVLAN Details"

        if {[info exists param(-vlan_id)]} {

            set vlan_id $param(-vlan_id)
            LOG -level 1 -msg "\t\tvlan_id                        -> $vlan_id"
        } else {

            set vlan_id $::wrptp_vlan_id
        }

        if {[info exists param(-vlan_priority)]} {

            set vlan_priority $param(-vlan_priority)
            LOG -level 1 -msg "\t\tvlan_priority                  -> $vlan_priority"
        } else {

            set vlan_priority $::wrptp_vlan_priority
        }

    } else {

        set vlan_id -1
        set vlan_priority 0
    }

    #IP HEADER OPTIONS
    if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {

        LOG -level 1 -msg "\tIP Details"

        #Destination IP address
        if {[info exists param(-dest_ip)]} {

            set dest_ip $param(-dest_ip)
            LOG -level 1 -msg "\t\tdest_ip                        -> $dest_ip"
        } else {

            set dest_ip 224.0.1.129
        }

        #Source IP address
        if {[info exists param(-src_ip)]} {

            set src_ip $param(-src_ip)
            LOG -level 1 -msg "\t\tsrc_ip                         -> $src_ip"
        } else {

            set src_ip $::tee_test_port_ip
        }

        #IP checksum
        if {[info exists param(-ip_checksum)]} {

            set ip_checksum $param(-ip_checksum)
            LOG -level 1 -msg "\t\tip_checksum                    -> $ip_checksum"
        } else {

            set ip_checksum auto
        }

        #UDP HEADER OPTIONS

        #UDP destination port number
        if {[info exists param(-dest_port)]} {

            set dest_port $param(-dest_port)
            LOG -level 1 -msg "\t\tdest_port                      -> $dest_port"
        } else {

            set dest_port 320
        }

        #UDP source port number
        if {[info exists param(-src_port)]} {

            set src_port $param(-src_port)
            LOG -level 1 -msg "\t\tsrc_port                       -> $src_port"
        } else {

            set src_port 320
        }

        #UDP length
        if {[info exists param(-udp_length)]} {

            set udp_length $param(-udp_length)
            LOG -level 1 -msg "\t\tudp_length                     -> $udp_length"
        } else {

            set udp_length 62
        }

        #UDP checksum
        if {[info exists param(-udp_checksum)]} {

            set udp_checksum $param(-udp_checksum)
            LOG -level 1 -msg "\t\tudp_checksum                   -> $udp_checksum"
        } else {

            set udp_checksum 0
        }
    } else {
        set dest_ip 0.0.0.0
        set src_ip 0.0.0.0
        set ip_checksum 0
        set dest_port 0
        set src_port 0
        set udp_length 0
        set udp_checksum 0
    }

    #PTP HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Details"

    #Transport Specific
    if {[info exists param(-transport_specific)]} {

        set transport_specific $param(-transport_specific)
        LOG -level 1 -msg "\t\ttransport_specific             -> $transport_specific"
    } else {

        set transport_specific 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        LOG -level 1 -msg "\t\tmessage_type                   -> $message_type"
    } else {

        set message_type 9
    }

    #PTP version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        LOG -level 1 -msg "\t\tversion_ptp                    -> $version_ptp"
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        LOG -level 1 -msg "\t\tmessage_length                 -> $message_length"
    } else {

        set message_length 54
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        LOG -level 1 -msg "\t\tdomain_number                  -> $domain_number"
    } else {

        set domain_number 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
        LOG -level 1 -msg "\t\tcorrection_field               -> $correction_field"
    } else {

        set correction_field 0
    }

    #Source Port Number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
        LOG -level 1 -msg "\t\tsrc_port_number                -> $src_port_number"
    } else {

        if {![regexp {.*?([0-9]+)$} $port_num match src_port_number]} {
            set src_port_number 1
        }

    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
        LOG -level 1 -msg "\t\tsrc_clock_identity             -> 0x[dec2hex $src_clock_identity 16]"
    } else {

        get_clock_identity\
                    -mac               $src_mac\
                    -format            $::ptp_clock_identity_format\
                    -clock_identity    src_clock_identity
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        LOG -level 1 -msg "\t\tsequence_id                    -> $sequence_id"
    } else {

        set sequence_id 1
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        LOG -level 1 -msg "\t\tcontrol_field                  -> $control_field"
    } else {

        set control_field 3
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
        LOG -level 1 -msg "\t\tlog_message_interval           -> $log_message_interval"
    } else {

        set log_message_interval 127
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    LOG -level 1 -msg "\tPTP Flag Details"
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        LOG -level 1 -msg "\t\talternate_master_flag          -> $alternate_master_flag"
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
        LOG -level 1 -msg "\t\ttwo_step_flag                  -> $two_step_flag"
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
        LOG -level 1 -msg "\t\tunicast_flag                   -> $unicast_flag"
    } else {

        set unicast_flag $::unicast_flag
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        LOG -level 1 -msg "\t\tprofile_specific1              -> $profile_specific1"
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        LOG -level 1 -msg "\t\tprofile_specific2              -> $profile_specific2"
    } else {

        set profile_specific2 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        LOG -level 1 -msg "\t\tleap61                         -> $leap61"
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        LOG -level 1 -msg "\t\tleap59                         -> $leap59"
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        LOG -level 1 -msg "\t\tcurrent_utc_offset_valid       -> $current_utc_offset_valid"
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        LOG -level 1 -msg "\t\tptp_timescale                  -> $ptp_timescale"
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        LOG -level 1 -msg "\t\ttime_traceable                 -> $time_traceable"
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        LOG -level 1 -msg "\t\tfreq_traceable                 -> $freq_traceable"
    } else {

        set freq_traceable 0
    }

    #PTP DELAY_RESP HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Delay_resp Message Details"

    if {[info exists param(-receive_timestamp_sec)]} {

        set receive_timestamp_sec $param(-receive_timestamp_sec)
        LOG -level 1 -msg "\t\treceive_timestamp_sec          -> $receive_timestamp_sec"
    } else {

        set receive_timestamp_sec 0
    }

    if {[info exists param(-receive_timestamp_ns)]} {

        set receive_timestamp_ns $param(-receive_timestamp_ns)
        LOG -level 1 -msg "\t\treceive_timestamp_ns           -> $receive_timestamp_ns"
    } else {

        set receive_timestamp_ns 0
    }

    if {[info exists param(-requesting_port_number)]} {

        set requesting_port_number $param(-requesting_port_number)
        LOG -level 1 -msg "\t\trequesting_port_number         -> $requesting_port_number"
    } else {

        set requesting_port_number 0
    }

    if {[info exists param(-requesting_clock_identity)]} {

        set requesting_clock_identity $param(-requesting_clock_identity)
        LOG -level 1 -msg "\t\trequesting_clock_identity      -> $requesting_clock_identity"
    } else {

        set requesting_clock_identity 0
    }

    set instance $::INSTANCE(DELAY_RESP)

    #CALL PLATFORM FUNCTION
    if {![pltLib::send_wrptp_pkt\
                -session_id                        $session_id\
                -instance                          $instance\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -transport_specific                $transport_specific\
                -message_type                      $message_type\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_traceable                    $time_traceable\
                -freq_traceable                    $freq_traceable\
                -correction_field                  $correction_field\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -receive_timestamp_sec             $receive_timestamp_sec\
                -receive_timestamp_ns              $receive_timestamp_ns\
                -requesting_port_number            $requesting_port_number\
                -requesting_clock_identity         $requesting_clock_identity\
                -count                             $count\
                -interval                          $interval]} {

        return 0

    }

    return 1

}

################################################################################
#                                                                              #
#  PROCEDURE NAME         :  wrptp::send_followup                              #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Transmission port              #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   dest_mac                      : (Mandatory) Destination MAC dddress.       #
#                                               (e.g., 01:80:C2:00:00:20)      #
#                                                                              #
#   src_mac                       : (Mandatory) Source MAC address.            #
#                                               (e.g., 00:80:C2:00:00:21)      #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                               (Default : 88F7)               #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                               (Default : 0)                  #
#                                                                              #
#   vlan_priority                 : (Optional)  Vlan Priority                  #
#                                               (Default : 0)                  #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                               (Default : 224.0.1.129)        #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                               (Default : 0.0.0.0)            #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                               (Default : auto)               #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                               (Default : 320)                #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                               (Default : 320)                #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                               (Default : 52)                 #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                               (Default : 0)                  #
#                                                                              #
#   transport_specific            : (Optional)  Transport Specific             #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default : 8)                  #
#                                                                              #
#   version_ptp                   : (Optional)  PTP version number             #
#                                               (Default : 2)                  #
#                                                                              #
#   message_length                : (Optional)  PTP message length             #
#                                               (Default : 44)                 #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                               (Default : 0)                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                               (Default : 0)                  #
#                                                                              #
#   two_step_flag                 : (Optional)  Two Step Flag                  #
#                                               (Default : 0)                  #
#                                                                              #
#   unicast_flag                  : (Optional)  Unicast Flag                   #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                               (Default : 0)                  #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP TimeSclae Flag             #
#                                               (Default : 0)                  #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                               (Default : 0)                  #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                               (Default : 0)                  #
#                                                                              #
#   correction_field              : (Optional)  Correction Field value         #
#                                               (Default : 0)                  #
#                                                                              #
#   src_port_number               : (Optional)  Source Port number             #
#                                               (Default : 0)                  #
#                                                                              #
#   src_clock_identity            : (Optional)  Source Clock identity          #
#                                               (Default : 0)                  #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                               (Default : 0)                  #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                               (Default : 2)                  #
#                                                                              #
#   log_message_interval          : (Optional)  Log message interval           #
#                                               (Default : 127)                #
#                                                                              #
#   precise_origin_timestamp_sec  : (Optional)  Precise Origin Timestamp Sec   #
#                                               (Default : 0)                  #
#                                                                              #
#   precise_origin_timestamp_ns   : (Optional)  Precise Origin Timestamp Ns    #
#                                               (Default : 0)                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default : 1)                  #
#                                                                              #
#   interval                      : (Optional)  Frame interval (in seconds)    #
#                                               (Default : 1)                  #
#                                                                              #
#  OUTPUT PARAMETERS      :  NONE                                              #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is successfully send   #
#                                         with the given parameters)           #
#                                                                              #
#                         : 0 on failure (If the packet could not be send      #
#                                         with the given parameters)           #
#                                                                              #
#  DEFINITION             : This function is used to construct and send        #
#                           PTP followup message                               #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       wrptp::send_followup\                                                  #
#           -port_num                         $port_num\                       #
#           -dest_mac                         $dest_mac\                       #
#           -src_mac                          $src_mac\                        #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -vlan_priority                    $vlan_priority\                  #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -transport_specific               $transport_specific\             #
#           -message_type                     $message_type\                   #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -two_step_flag                    $two_step_flag\                  #
#           -unicast_flag                     $unicast_flag\                   #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -correction_field                 $correction_field\               #
#           -src_port_number                  $src_port_number\                #
#           -src_clock_identity               $src_clock_identity\             #
#           -sequence_id                      $sequence_id\                    #
#           -control_field                    $control_field\                  #
#           -log_message_interval             $log_message_interval\           #
#           -precise_origin_timestamp_sec     $precise_origin_timestamp_sec\   #
#           -precise_origin_timestamp_ns      $precise_origin_timestamp_ns\    #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#                                                                              #
################################################################################
proc wrptp::send_followup {args} {

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "wrptp::send_followup: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #MISCELLANEOUS
    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
        LOG -level 1 -msg "\t\tport_num                       -> $port_num"
    } else {

        ERRLOG -msg "wrptp::send_followup: Missing Argument -> port_num"
        return 0
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
        LOG -level 1 -msg "\t\tcount                          -> $count"
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
        LOG -level 1 -msg "\t\tinterval                       -> $interval"
    } else {

        set interval $::wrptp_sync_interval
    }


    #ETHERNET HEADER OPTIONS
    LOG -level 1 -msg "\tEthernet Details"

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
        LOG -level 1 -msg "\t\tdest_mac                       -> $dest_mac"
    } else {

        ERRLOG -msg "wrptp::send_followup: Missing Argument -> dest_mac"
        return 0
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
        LOG -level 1 -msg "\t\tsrc_mac                        -> $src_mac"
    } else {

        ERRLOG -msg "wrptp::send_followup: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        LOG -level 1 -msg "\t\teth_type                       -> $eth_type"
    } else {

        if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
            set eth_type 0800
        } else {
            set eth_type 88F7
        }
    }

    #Vlan ID
    if { $::wrptp_vlan_encap == "true" } {

        LOG -level 1 -msg "\tVLAN Details"

        if {[info exists param(-vlan_id)]} {

            set vlan_id $param(-vlan_id)
            LOG -level 1 -msg "\t\tvlan_id                        -> $vlan_id"
        } else {

            set vlan_id $::wrptp_vlan_id
        }

        if {[info exists param(-vlan_priority)]} {

            set vlan_priority $param(-vlan_priority)
            LOG -level 1 -msg "\t\tvlan_priority                  -> $vlan_priority"
        } else {

            set vlan_priority $::wrptp_vlan_priority
        }

    } else {

        set vlan_id -1
        set vlan_priority 0
    }

    #IP HEADER OPTIONS
    if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {

        LOG -level 1 -msg "\tIP Details"

        #Destination IP address
        if {[info exists param(-dest_ip)]} {

            set dest_ip $param(-dest_ip)
            LOG -level 1 -msg "\t\tdest_ip                        -> $dest_ip"
        } else {

            set dest_ip 224.0.1.129
        }

        #Source IP address
        if {[info exists param(-src_ip)]} {

            set src_ip $param(-src_ip)
            LOG -level 1 -msg "\t\tsrc_ip                         -> $src_ip"
        } else {

            set src_ip $::tee_test_port_ip
        }

        #IP checksum
        if {[info exists param(-ip_checksum)]} {

            set ip_checksum $param(-ip_checksum)
            LOG -level 1 -msg "\t\tip_checksum                    -> $ip_checksum"
        } else {

            set ip_checksum auto
        }

        #UDP HEADER OPTIONS

        #UDP destination port number
        if {[info exists param(-dest_port)]} {

            set dest_port $param(-dest_port)
            LOG -level 1 -msg "\t\tdest_port                      -> $dest_port"
        } else {

            set dest_port 320
        }

        #UDP source port number
        if {[info exists param(-src_port)]} {

            set src_port $param(-src_port)
            LOG -level 1 -msg "\t\tsrc_port                       -> $src_port"
        } else {

            set src_port 320
        }

        #UDP length
        if {[info exists param(-udp_length)]} {

            set udp_length $param(-udp_length)
            LOG -level 1 -msg "\t\tudp_length                     -> $udp_length"
        } else {

            set udp_length 52
        }

        #UDP checksum
        if {[info exists param(-udp_checksum)]} {

            set udp_checksum $param(-udp_checksum)
            LOG -level 1 -msg "\t\tudp_checksum                   -> $udp_checksum"
        } else {

            set udp_checksum 0
        }
    } else {
        set dest_ip 0.0.0.0
        set src_ip 0.0.0.0
        set ip_checksum 0
        set dest_port 0
        set src_port 0
        set udp_length 0
        set udp_checksum 0
    }

    #PTP HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Details"

    #Transport Specific
    if {[info exists param(-transport_specific)]} {

        set transport_specific $param(-transport_specific)
        LOG -level 1 -msg "\t\ttransport_specific             -> $transport_specific"
    } else {

        set transport_specific 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        LOG -level 1 -msg "\t\tmessage_type                   -> $message_type"
    } else {

        set message_type 8
    }

    #PTP version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        LOG -level 1 -msg "\t\tversion_ptp                    -> $version_ptp"
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        LOG -level 1 -msg "\t\tmessage_length                 -> $message_length"
    } else {

        set message_length 44
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        LOG -level 1 -msg "\t\tdomain_number                  -> $domain_number"
    } else {

        set domain_number 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
        LOG -level 1 -msg "\t\tcorrection_field               -> $correction_field"
    } else {

        set correction_field 0
    }

    #Source Port Number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
        LOG -level 1 -msg "\t\tsrc_port_number                -> $src_port_number"
    } else {

        if {![regexp {.*?([0-9]+)$} $port_num match src_port_number]} {
            set src_port_number 1
        }

    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
        LOG -level 1 -msg "\t\tsrc_clock_identity             -> 0x[dec2hex $src_clock_identity 16]"
    } else {

        get_clock_identity\
                    -mac               $src_mac\
                    -format            $::ptp_clock_identity_format\
                    -clock_identity    src_clock_identity
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        LOG -level 1 -msg "\t\tsequence_id                    -> $sequence_id"
    } else {

        set sequence_id 1
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        LOG -level 1 -msg "\t\tcontrol_field                  -> $control_field"
    } else {

        set control_field 2
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
        LOG -level 1 -msg "\t\tlog_message_interval           -> $log_message_interval"
    } else {

        set log_message_interval 127
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    LOG -level 1 -msg "\tPTP Flag Details"
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        LOG -level 1 -msg "\t\talternate_master_flag          -> $alternate_master_flag"
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
        LOG -level 1 -msg "\t\ttwo_step_flag                  -> $two_step_flag"
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
        LOG -level 1 -msg "\t\tunicast_flag                   -> $unicast_flag"
    } else {

        set unicast_flag $::unicast_flag
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        LOG -level 1 -msg "\t\tprofile_specific1              -> $profile_specific1"
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        LOG -level 1 -msg "\t\tprofile_specific2              -> $profile_specific2"
    } else {

        set profile_specific2 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        LOG -level 1 -msg "\t\tleap61                         -> $leap61"
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        LOG -level 1 -msg "\t\tleap59                         -> $leap59"
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        LOG -level 1 -msg "\t\tcurrent_utc_offset_valid       -> $current_utc_offset_valid"
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        LOG -level 1 -msg "\t\tptp_timescale                  -> $ptp_timescale"
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        LOG -level 1 -msg "\t\ttime_traceable                 -> $time_traceable"
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        LOG -level 1 -msg "\t\tfreq_traceable                 -> $freq_traceable"
    } else {

        set freq_traceable 0
    }

    #PTP FOLLOWUP HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Followup Message Details"

    if {[info exists param(-precise_origin_timestamp_sec)]} {

        set precise_origin_timestamp_sec $param(-precise_origin_timestamp_sec)
        LOG -level 1 -msg "\t\tprecise_origin_timestamp_sec   -> $precise_origin_timestamp_sec"
    } else {

        set precise_origin_timestamp_sec 0
    }

    if {[info exists param(-precise_origin_timestamp_ns)]} {

        set precise_origin_timestamp_ns $param(-precise_origin_timestamp_ns)
        LOG -level 1 -msg "\t\tprecise_origin_timestamp_ns    -> $precise_origin_timestamp_ns"
    } else {

        set precise_origin_timestamp_ns 0
    }

    set instance $::INSTANCE(FOLLOWUP)

    #CALL PLATFORM FUNCTION
    if {![pltLib::send_wrptp_pkt\
                -session_id                        $session_id\
                -instance                          $instance\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -transport_specific                $transport_specific\
                -message_type                      $message_type\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_traceable                    $time_traceable\
                -freq_traceable                    $freq_traceable\
                -correction_field                  $correction_field\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -precise_origin_timestamp_sec      $precise_origin_timestamp_sec\
                -precise_origin_timestamp_ns       $precise_origin_timestamp_ns\
                -count                             $count\
                -interval                          $interval]} {

        return 0

    }

    return 1

}

################################################################################
#                                                                              #
#  PROCEDURE NAME         :  wrptp::send_pdelay_req                            #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Transmission port              #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   dest_mac                      : (Mandatory) Destination MAC dddress.       #
#                                               (e.g., 01:80:C2:00:00:20)      #
#                                                                              #
#   src_mac                       : (Mandatory) Source MAC address.            #
#                                               (e.g., 00:80:C2:00:00:21)      #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                               (Default : 88F7)               #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                               (Default : 0)                  #
#                                                                              #
#   vlan_priority                 : (Optional)  Vlan Priority                  #
#                                               (Default : 0)                  #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                               (Default : 224.0.1.129)        #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                               (Default : 0.0.0.0)            #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                               (Default : auto)               #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                               (Default : 319)                #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                               (Default : 319)                #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                               (Default : 62)                 #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                               (Default : 0)                  #
#                                                                              #
#   transport_specific            : (Optional)  Transport Specific             #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default : 2)                  #
#                                                                              #
#   version_ptp                   : (Optional)  PTP version number             #
#                                               (Default : 2)                  #
#                                                                              #
#   message_length                : (Optional)  PTP message length             #
#                                               (Default : 54)                 #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                               (Default : 0)                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                               (Default : 0)                  #
#                                                                              #
#   two_step_flag                 : (Optional)  Two Step Flag                  #
#                                               (Default : 0)                  #
#                                                                              #
#   unicast_flag                  : (Optional)  Unicast Flag                   #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                               (Default : 0)                  #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP TimeSclae Flag             #
#                                               (Default : 0)                  #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                               (Default : 0)                  #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                               (Default : 0)                  #
#                                                                              #
#   correction_field              : (Optional)  Correction Field value         #
#                                               (Default : 0)                  #
#                                                                              #
#   src_port_number               : (Optional)  Source Port number             #
#                                               (Default : 0)                  #
#                                                                              #
#   src_clock_identity            : (Optional)  Source Clock identity          #
#                                               (Default : 0)                  #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                               (Default : 0)                  #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                               (Default : 5)                  #
#                                                                              #
#   log_message_interval          : (Optional)  Log message interval           #
#                                               (Default : 127)                #
#                                                                              #
#   origin_timestamp_sec          : (Optional)  Origin Timestamp Sec           #
#                                               (Default : 0)                  #
#                                                                              #
#   origin_timestamp_ns           : (Optional)  Origin Timestamp Ns            #
#                                               (Default : 0)                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default : 1)                  #
#                                                                              #
#   interval                      : (Optional)  Frame interval (in seconds)    #
#                                               (Default : 1)                  #
#                                                                              #
#  OUTPUT PARAMETERS      :  NONE                                              #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is successfully send   #
#                                         with the given parameters)           #
#                                                                              #
#                         : 0 on failure (If the packet could not be send      #
#                                         with the given parameters)           #
#                                                                              #
#  DEFINITION             : This function is used to construct and send        #
#                           PTP pdelay req message                             #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       wrptp::send_pdelay_req\                                                #
#           -port_num                         $port_num\                       #
#           -dest_mac                         $dest_mac\                       #
#           -src_mac                          $src_mac\                        #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -vlan_priority                    $vlan_priority\                  #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -transport_specific               $transport_specific\             #
#           -message_type                     $message_type\                   #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -two_step_flag                    $two_step_flag\                  #
#           -unicast_flag                     $unicast_flag\                   #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -correction_field                 $correction_field\               #
#           -src_port_number                  $src_port_number\                #
#           -src_clock_identity               $src_clock_identity\             #
#           -sequence_id                      $sequence_id\                    #
#           -control_field                    $control_field\                  #
#           -log_message_interval             $log_message_interval\           #
#           -origin_timestamp_sec             $origin_timestamp_sec\           #
#           -origin_timestamp_ns              $origin_timestamp_ns\            #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#                                                                              #
################################################################################
proc wrptp::send_pdelay_req {args} {

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "wrptp::send_pdelay_req: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #MISCELLANEOUS
    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
        LOG -level 1 -msg "\t\tport_num                       -> $port_num"
    } else {

        ERRLOG -msg "wrptp::send_pdelay_req: Missing Argument -> port_num"
        return 0
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
        LOG -level 1 -msg "\t\tcount                          -> $count"
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
        LOG -level 1 -msg "\t\tinterval                       -> $interval"
    } else {

        set interval $::wrptp_sync_interval
    }


    #ETHERNET HEADER OPTIONS
    LOG -level 1 -msg "\tEthernet Details"

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
        LOG -level 1 -msg "\t\tdest_mac                       -> $dest_mac"
    } else {

        ERRLOG -msg "wrptp::send_pdelay_req: Missing Argument -> dest_mac"
        return 0
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
        LOG -level 1 -msg "\t\tsrc_mac                        -> $src_mac"
    } else {

        ERRLOG -msg "wrptp::send_pdelay_req: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        LOG -level 1 -msg "\t\teth_type                       -> $eth_type"
    } else {

        if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
            set eth_type 0800
        } else {
            set eth_type 88f7
        }
    }

    #Vlan ID
    if { $::wrptp_vlan_encap == "true" } {

        LOG -level 1 -msg "\tVLAN Details"

        if {[info exists param(-vlan_id)]} {

            set vlan_id $param(-vlan_id)
            LOG -level 1 -msg "\t\tvlan_id                        -> $vlan_id"
        } else {

            set vlan_id $::wrptp_vlan_id
        }

        if {[info exists param(-vlan_priority)]} {

            set vlan_priority $param(-vlan_priority)
            LOG -level 1 -msg "\t\tvlan_priority                  -> $vlan_priority"
        } else {

            set vlan_priority $::wrptp_vlan_priority
        }

    } else {

        set vlan_id -1
        set vlan_priority 0
    }

    #IP HEADER OPTIONS
    if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {

        LOG -level 1 -msg "\tIP Details"

        #Destination IP address
        if {[info exists param(-dest_ip)]} {

            set dest_ip $param(-dest_ip)
            LOG -level 1 -msg "\t\tdest_ip                        -> $dest_ip"
        } else {

            set dest_ip 224.0.1.129
        }

        #Source IP address
        if {[info exists param(-src_ip)]} {

            set src_ip $param(-src_ip)
            LOG -level 1 -msg "\t\tsrc_ip                         -> $src_ip"
        } else {

            set src_ip $::tee_test_port_ip
        }

        #IP checksum
        if {[info exists param(-ip_checksum)]} {

            set ip_checksum $param(-ip_checksum)
            LOG -level 1 -msg "\t\tip_checksum                    -> $ip_checksum"
        } else {

            set ip_checksum auto
        }

        #UDP HEADER OPTIONS

        #UDP destination port number
        if {[info exists param(-dest_port)]} {

            set dest_port $param(-dest_port)
            LOG -level 1 -msg "\t\tdest_port                      -> $dest_port"
        } else {

            set dest_port 319
        }

        #UDP source port number
        if {[info exists param(-src_port)]} {

            set src_port $param(-src_port)
            LOG -level 1 -msg "\t\tsrc_port                       -> $src_port"
        } else {

            set src_port 319
        }

        #UDP length
        if {[info exists param(-udp_length)]} {

            set udp_length $param(-udp_length)
            LOG -level 1 -msg "\t\tudp_length                     -> $udp_length"
        } else {

            set udp_length 62
        }

        #UDP checksum
        if {[info exists param(-udp_checksum)]} {

            set udp_checksum $param(-udp_checksum)
            LOG -level 1 -msg "\t\tudp_checksum                   -> $udp_checksum"
        } else {

            set udp_checksum 0
        }
    } else {
        set dest_ip 0.0.0.0
        set src_ip 0.0.0.0
        set ip_checksum 0
        set dest_port 0
        set src_port 0
        set udp_length 0
        set udp_checksum 0
    }

    #PTP HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Details"

    #Transport Specific
    if {[info exists param(-transport_specific)]} {

        set transport_specific $param(-transport_specific)
        LOG -level 1 -msg "\t\ttransport_specific             -> $transport_specific"
    } else {

        set transport_specific 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        LOG -level 1 -msg "\t\tmessage_type                   -> $message_type"
    } else {

        set message_type 2
    }

    #PTP version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        LOG -level 1 -msg "\t\tversion_ptp                    -> $version_ptp"
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        LOG -level 1 -msg "\t\tmessage_length                 -> $message_length"
    } else {

        set message_length 54
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        LOG -level 1 -msg "\t\tdomain_number                  -> $domain_number"
    } else {

        set domain_number 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
        LOG -level 1 -msg "\t\tcorrection_field               -> $correction_field"
    } else {

        set correction_field 0
    }

    #Source Port Number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
        LOG -level 1 -msg "\t\tsrc_port_number                -> $src_port_number"
    } else {

        if {![regexp {.*?([0-9]+)$} $port_num match src_port_number]} {
            set src_port_number 1
        }

    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
        LOG -level 1 -msg "\t\tsrc_clock_identity             -> 0x[dec2hex $src_clock_identity 16]"
    } else {

        get_clock_identity\
                    -mac               $src_mac\
                    -format            $::ptp_clock_identity_format\
                    -clock_identity    src_clock_identity
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        LOG -level 1 -msg "\t\tsequence_id                    -> $sequence_id"
    } else {

        set sequence_id 1
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        LOG -level 1 -msg "\t\tcontrol_field                  -> $control_field"
    } else {

        set control_field 5
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
        LOG -level 1 -msg "\t\tlog_message_interval           -> $log_message_interval"
    } else {

        set log_message_interval 127
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    LOG -level 1 -msg "\tPTP Flag Details"
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        LOG -level 1 -msg "\t\talternate_master_flag          -> $alternate_master_flag"
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
        LOG -level 1 -msg "\t\ttwo_step_flag                  -> $two_step_flag"
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
        LOG -level 1 -msg "\t\tunicast_flag                   -> $unicast_flag"
    } else {

        set unicast_flag $::unicast_flag
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        LOG -level 1 -msg "\t\tprofile_specific1              -> $profile_specific1"
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        LOG -level 1 -msg "\t\tprofile_specific2              -> $profile_specific2"
    } else {

        set profile_specific2 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        LOG -level 1 -msg "\t\tleap61                         -> $leap61"
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        LOG -level 1 -msg "\t\tleap59                         -> $leap59"
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        LOG -level 1 -msg "\t\tcurrent_utc_offset_valid       -> $current_utc_offset_valid"
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        LOG -level 1 -msg "\t\tptp_timescale                  -> $ptp_timescale"
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        LOG -level 1 -msg "\t\ttime_traceable                 -> $time_traceable"
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        LOG -level 1 -msg "\t\tfreq_traceable                 -> $freq_traceable"
    } else {

        set freq_traceable 0
    }

    #PTP PDELAY_REQ HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Pdelay_req Message Details"

    if {[info exists param(-origin_timestamp_sec)]} {

        set origin_timestamp_sec $param(-origin_timestamp_sec)
        LOG -level 1 -msg "\t\torigin_timestamp_sec           -> $origin_timestamp_sec"
    } else {

        set origin_timestamp_sec [clock seconds]
    }

    if {[info exists param(-origin_timestamp_ns)]} {

        set origin_timestamp_ns $param(-origin_timestamp_ns)
        LOG -level 1 -msg "\t\torigin_timestamp_ns            -> $origin_timestamp_ns"
    } else {

        set origin_timestamp_ns 0
    }

    set instance $::INSTANCE(PDELAY_REQ)

    #CALL PLATFORM FUNCTION
    if {![pltLib::send_wrptp_pkt\
                -session_id                        $session_id\
                -instance                          $instance\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -transport_specific                $transport_specific\
                -message_type                      $message_type\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_traceable                    $time_traceable\
                -freq_traceable                    $freq_traceable\
                -correction_field                  $correction_field\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -origin_timestamp_sec              $origin_timestamp_sec\
                -origin_timestamp_ns               $origin_timestamp_ns\
                -count                             $count\
                -interval                          $interval]} {

        return 0

    }

    return 1

}

################################################################################
#                                                                              #
#  PROCEDURE NAME         :  wrptp::send_pdelay_resp                           #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Transmission port              #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   dest_mac                      : (Mandatory) Destination MAC dddress.       #
#                                               (e.g., 01:80:C2:00:00:20)      #
#                                                                              #
#   src_mac                       : (Mandatory) Source MAC address.            #
#                                               (e.g., 00:80:C2:00:00:21)      #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                               (Default : 88F7)               #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                               (Default : 0)                  #
#                                                                              #
#   vlan_priority                 : (Optional)  Vlan Priority                  #
#                                               (Default : 0)                  #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                               (Default : 224.0.1.129)        #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                               (Default : 0.0.0.0)            #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                               (Default : auto)               #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                               (Default : 319)                #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                               (Default : 319)                #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                               (Default : 62)                 #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                               (Default : 0)                  #
#                                                                              #
#   transport_specific            : (Optional)  Transport Specific             #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default : 3)                  #
#                                                                              #
#   version_ptp                   : (Optional)  PTP version number             #
#                                               (Default : 2)                  #
#                                                                              #
#   message_length                : (Optional)  PTP message length             #
#                                               (Default : 54)                 #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                               (Default : 0)                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                               (Default : 0)                  #
#                                                                              #
#   two_step_flag                 : (Optional)  Two Step Flag                  #
#                                               (Default : 0)                  #
#                                                                              #
#   unicast_flag                  : (Optional)  Unicast Flag                   #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                               (Default : 0)                  #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP TimeSclae Flag             #
#                                               (Default : 0)                  #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                               (Default : 0)                  #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                               (Default : 0)                  #
#                                                                              #
#   correction_field              : (Optional)  Correction Field value         #
#                                               (Default : 0)                  #
#                                                                              #
#   src_port_number               : (Optional)  Source Port number             #
#                                               (Default : 0)                  #
#                                                                              #
#   src_clock_identity            : (Optional)  Source Clock identity          #
#                                               (Default : 0)                  #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                               (Default : 0)                  #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                               (Default : 5)                  #
#                                                                              #
#   log_message_interval          : (Optional)  Log message interval           #
#                                               (Default : 127)                #
#                                                                              #
#   request_receipt_timestamp_sec : (Optional)  Request Receipt Timestamp Sec  #
#                                               (Default : 0)                  #
#                                                                              #
#   request_receipt_timestamp_ns  : (Optional)  Request Receipt Timestamp Ns   #
#                                               (Default : 0)                  #
#                                                                              #
#   requesting_port_number        : (Optional)  Requesting Port Number         #
#                                               (Default : 0)                  #
#                                                                              #
#   requesting_clock_identity     : (Optional)  Requesting Clock Identity      #
#                                               (Default : 0)                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default : 1)                  #
#                                                                              #
#   interval                      : (Optional)  Frame interval (in seconds)    #
#                                               (Default : 1)                  #
#                                                                              #
#  OUTPUT PARAMETERS      :  NONE                                              #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is successfully send   #
#                                         with the given parameters)           #
#                                                                              #
#                         : 0 on failure (If the packet could not be send      #
#                                         with the given parameters)           #
#                                                                              #
#  DEFINITION             : This function is used to construct and send        #
#                           PTP pdelay resp message                            #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       wrptp::send_pdelay_resp\                                               #
#           -port_num                         $port_num\                       #
#           -dest_mac                         $dest_mac\                       #
#           -src_mac                          $src_mac\                        #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -vlan_priority                    $vlan_priority\                  #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -transport_specific               $transport_specific\             #
#           -message_type                     $message_type\                   #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -two_step_flag                    $two_step_flag\                  #
#           -unicast_flag                     $unicast_flag\                   #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -correction_field                 $correction_field\               #
#           -src_port_number                  $src_port_number\                #
#           -src_clock_identity               $src_clock_identity\             #
#           -sequence_id                      $sequence_id\                    #
#           -control_field                    $control_field\                  #
#           -log_message_interval             $log_message_interval\           #
#           -request_receipt_timestamp_sec    $request_receipt_timestamp_sec\  #
#           -request_receipt_timestamp_ns     $request_receipt_timestamp_ns\   #
#           -requesting_port_number           $requesting_port_number\         #
#           -requesting_clock_identity        $requesting_clock_identity\      #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#                                                                              #
################################################################################
proc wrptp::send_pdelay_resp {args} {

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "wrptp::send_pdelay_resp: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #MISCELLANEOUS
    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
        LOG -level 1 -msg "\t\tport_num                       -> $port_num"
    } else {

        ERRLOG -msg "wrptp::send_pdelay_resp: Missing Argument -> port_num"
        return 0
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
        LOG -level 1 -msg "\t\tcount                          -> $count"
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
        LOG -level 1 -msg "\t\tinterval                       -> $interval"
    } else {

        set interval $::wrptp_sync_interval
    }


    #ETHERNET HEADER OPTIONS
    LOG -level 1 -msg "\tEthernet Details"

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
        LOG -level 1 -msg "\t\tdest_mac                       -> $dest_mac"
    } else {

        ERRLOG -msg "wrptp::send_pdelay_resp: Missing Argument -> dest_mac"
        return 0
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
        LOG -level 1 -msg "\t\tsrc_mac                        -> $src_mac"
    } else {

        ERRLOG -msg "wrptp::send_pdelay_resp: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        LOG -level 1 -msg "\t\teth_type                       -> $eth_type"
    } else {

        if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
            set eth_type 0800
        } else {
            set eth_type 88F7
        }
    }

    #Vlan ID
    if { $::wrptp_vlan_encap == "true" } {

        LOG -level 1 -msg "\tVLAN Details"

        if {[info exists param(-vlan_id)]} {

            set vlan_id $param(-vlan_id)
            LOG -level 1 -msg "\t\tvlan_id                        -> $vlan_id"
        } else {

            set vlan_id $::wrptp_vlan_id
        }

        if {[info exists param(-vlan_priority)]} {

            set vlan_priority $param(-vlan_priority)
            LOG -level 1 -msg "\t\tvlan_priority                  -> $vlan_priority"
        } else {

            set vlan_priority $::wrptp_vlan_priority
        }

    } else {

        set vlan_id -1
        set vlan_priority 0
    }

    #IP HEADER OPTIONS
    if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {

        LOG -level 1 -msg "\tIP Details"

        #Destination IP address
        if {[info exists param(-dest_ip)]} {

            set dest_ip $param(-dest_ip)
            LOG -level 1 -msg "\t\tdest_ip                        -> $dest_ip"
        } else {

            set dest_ip 224.0.1.129
        }

        #Source IP address
        if {[info exists param(-src_ip)]} {

            set src_ip $param(-src_ip)
            LOG -level 1 -msg "\t\tsrc_ip                         -> $src_ip"
        } else {

            set src_ip $::tee_test_port_ip
        }

        #IP checksum
        if {[info exists param(-ip_checksum)]} {

            set ip_checksum $param(-ip_checksum)
            LOG -level 1 -msg "\t\tip_checksum                    -> $ip_checksum"
        } else {

            set ip_checksum auto
        }

        #UDP HEADER OPTIONS

        #UDP destination port number
        if {[info exists param(-dest_port)]} {

            set dest_port $param(-dest_port)
            LOG -level 1 -msg "\t\tdest_port                      -> $dest_port"
        } else {

            set dest_port 319
        }

        #UDP source port number
        if {[info exists param(-src_port)]} {

            set src_port $param(-src_port)
            LOG -level 1 -msg "\t\tsrc_port                       -> $src_port"
        } else {

            set src_port 319
        }

        #UDP length
        if {[info exists param(-udp_length)]} {

            set udp_length $param(-udp_length)
            LOG -level 1 -msg "\t\tudp_length                     -> $udp_length"
        } else {

            set udp_length 62
        }

        #UDP checksum
        if {[info exists param(-udp_checksum)]} {

            set udp_checksum $param(-udp_checksum)
            LOG -level 1 -msg "\t\tudp_checksum                   -> $udp_checksum"
        } else {

            set udp_checksum 0
        }
    } else {
        set dest_ip 0.0.0.0
        set src_ip 0.0.0.0
        set ip_checksum 0
        set dest_port 0
        set src_port 0
        set udp_length 0
        set udp_checksum 0
    }

    #PTP HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Details"

    #Transport Specific
    if {[info exists param(-transport_specific)]} {

        set transport_specific $param(-transport_specific)
        LOG -level 1 -msg "\t\ttransport_specific             -> $transport_specific"
    } else {

        set transport_specific 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        LOG -level 1 -msg "\t\tmessage_type                   -> $message_type"
    } else {

        set message_type 3
    }

    #PTP version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        LOG -level 1 -msg "\t\tversion_ptp                    -> $version_ptp"
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        LOG -level 1 -msg "\t\tmessage_length                 -> $message_length"
    } else {

        set message_length 54
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        LOG -level 1 -msg "\t\tdomain_number                  -> $domain_number"
    } else {

        set domain_number 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
        LOG -level 1 -msg "\t\tcorrection_field               -> $correction_field"
    } else {

        set correction_field 0
    }

    #Source Port Number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
        LOG -level 1 -msg "\t\tsrc_port_number                -> $src_port_number"
    } else {

        if {![regexp {.*?([0-9]+)$} $port_num match src_port_number]} {
            set src_port_number 1
        }

    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
        LOG -level 1 -msg "\t\tsrc_clock_identity             -> 0x[dec2hex $src_clock_identity 16]"
    } else {

        get_clock_identity\
                    -mac               $src_mac\
                    -format            $::ptp_clock_identity_format\
                    -clock_identity    src_clock_identity
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        LOG -level 1 -msg "\t\tsequence_id                    -> $sequence_id"
    } else {

        set sequence_id 1
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        LOG -level 1 -msg "\t\tcontrol_field                  -> $control_field"
    } else {

        set control_field 5
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
        LOG -level 1 -msg "\t\tlog_message_interval           -> $log_message_interval"
    } else {

        set log_message_interval 127
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    LOG -level 1 -msg "\tPTP Flag Details"
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        LOG -level 1 -msg "\t\talternate_master_flag          -> $alternate_master_flag"
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
        LOG -level 1 -msg "\t\ttwo_step_flag                  -> $two_step_flag"
    } else {

        set two_step_flag $::ptp_two_step_flag
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
        LOG -level 1 -msg "\t\tunicast_flag                   -> $unicast_flag"
    } else {

        set unicast_flag $::unicast_flag
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        LOG -level 1 -msg "\t\tprofile_specific1              -> $profile_specific1"
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        LOG -level 1 -msg "\t\tprofile_specific2              -> $profile_specific2"
    } else {

        set profile_specific2 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        LOG -level 1 -msg "\t\tleap61                         -> $leap61"
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        LOG -level 1 -msg "\t\tleap59                         -> $leap59"
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        LOG -level 1 -msg "\t\tcurrent_utc_offset_valid       -> $current_utc_offset_valid"
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        LOG -level 1 -msg "\t\tptp_timescale                  -> $ptp_timescale"
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        LOG -level 1 -msg "\t\ttime_traceable                 -> $time_traceable"
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        LOG -level 1 -msg "\t\tfreq_traceable                 -> $freq_traceable"
    } else {

        set freq_traceable 0
    }

    #PTP PDELAY_RESP HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Pdelay_resp Message Details"

    if {[info exists param(-request_receipt_timestamp_sec)]} {

        set request_receipt_timestamp_sec $param(-request_receipt_timestamp_sec)
        LOG -level 1 -msg "\t\trequest_receipt_timestamp_sec  -> $request_receipt_timestamp_sec"
    } else {

        set request_receipt_timestamp_sec 0
    }

    if {[info exists param(-request_receipt_timestamp_ns)]} {

        set request_receipt_timestamp_ns $param(-request_receipt_timestamp_ns)
        LOG -level 1 -msg "\t\trequest_receipt_timestamp_ns   -> $request_receipt_timestamp_ns"
    } else {

        set request_receipt_timestamp_ns 0
    }

    if {[info exists param(-requesting_port_number)]} {

        set requesting_port_number $param(-requesting_port_number)
        LOG -level 1 -msg "\t\trequesting_port_number         -> $requesting_port_number"
    } else {

        set requesting_port_number 0
    }

    if {[info exists param(-requesting_clock_identity)]} {

        set requesting_clock_identity $param(-requesting_clock_identity)
        LOG -level 1 -msg "\t\trequesting_clock_identity      -> $requesting_clock_identity"
    } else {

        set requesting_clock_identity 0
    }

    set instance $::INSTANCE(PDELAY_RESP)

    #CALL PLATFORM FUNCTION
    if {![pltLib::send_wrptp_pkt\
                -session_id                        $session_id\
                -instance                          $instance\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -transport_specific                $transport_specific\
                -message_type                      $message_type\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_traceable                    $time_traceable\
                -freq_traceable                    $freq_traceable\
                -correction_field                  $correction_field\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -request_receipt_timestamp_sec     $request_receipt_timestamp_sec\
                -request_receipt_timestamp_ns      $request_receipt_timestamp_ns\
                -requesting_port_number            $requesting_port_number\
                -requesting_clock_identity         $requesting_clock_identity\
                -count                             $count\
                -interval                          $interval]} {

        return 0

    }

    return 1

}

################################################################################
#                                                                              #
#  PROCEDURE NAME         :  wrptp::send_pdelay_resp_followup                  #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Transmission port              #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   dest_mac                      : (Mandatory) Destination MAC dddress.       #
#                                               (e.g., 01:80:C2:00:00:20)      #
#                                                                              #
#   src_mac                       : (Mandatory) Source MAC address.            #
#                                               (e.g., 00:80:C2:00:00:21)      #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                               (Default : 88F7)               #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                               (Default : 0)                  #
#                                                                              #
#   vlan_priority                 : (Optional)  Vlan Priority                  #
#                                               (Default : 0)                  #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                               (Default : 224.0.1.129)        #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                               (Default : 0.0.0.0)            #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                               (Default : auto)               #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                               (Default : 320)                #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                               (Default : 320)                #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                               (Default : 62)                 #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                               (Default : 0)                  #
#                                                                              #
#   transport_specific            : (Optional)  Transport Specific             #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default : 10)                 #
#                                                                              #
#   version_ptp                   : (Optional)  PTP version number             #
#                                               (Default : 2)                  #
#                                                                              #
#   message_length                : (Optional)  PTP message length             #
#                                               (Default : 54)                 #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                               (Default : 0)                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                               (Default : 0)                  #
#                                                                              #
#   two_step_flag                 : (Optional)  Two Step Flag                  #
#                                               (Default : 0)                  #
#                                                                              #
#   unicast_flag                  : (Optional)  Unicast Flag                   #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                               (Default : 0)                  #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP TimeSclae Flag             #
#                                               (Default : 0)                  #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                               (Default : 0)                  #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                               (Default : 0)                  #
#                                                                              #
#   correction_field              : (Optional)  Correction Field value         #
#                                               (Default : 0)                  #
#                                                                              #
#   src_port_number               : (Optional)  Source Port number             #
#                                               (Default : 0)                  #
#                                                                              #
#   src_clock_identity            : (Optional)  Source Clock identity          #
#                                               (Default : 0)                  #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                               (Default : 0)                  #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                               (Default : 5)                  #
#                                                                              #
#   log_message_interval          : (Optional)  Log message interval           #
#                                               (Default : 127)                #
#                                                                              #
#   response_origin_timestamp_sec : (Optional)  Response Origin Timestamp Sec  #
#                                               (Default : 0)                  #
#                                                                              #
#   response_origin_timestamp_ns  : (Optional)  Response Origin Timestamp Ns   #
#                                               (Default : 0)                  #
#                                                                              #
#   requesting_port_number        : (Optional)  Requesting Port Number         #
#                                               (Default : 0)                  #
#                                                                              #
#   requesting_clock_identity     : (Optional)  Requesting Clock Identity      #
#                                               (Default : 0)                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default : 1)                  #
#                                                                              #
#   interval                      : (Optional)  Frame interval (in seconds)    #
#                                               (Default : 1)                  #
#                                                                              #
#  OUTPUT PARAMETERS      :  NONE                                              #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is successfully send   #
#                                         with the given parameters)           #
#                                                                              #
#                         : 0 on failure (If the packet could not be send      #
#                                         with the given parameters)           #
#                                                                              #
#  DEFINITION             : This function is used to construct and send        #
#                           PTP pdelay resp followup message                   #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       wrptp::send_pdelay_resp_followup\                                      #
#           -port_num                         $port_num\                       #
#           -dest_mac                         $dest_mac\                       #
#           -src_mac                          $src_mac\                        #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -vlan_priority                    $vlan_priority\                  #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -transport_specific               $transport_specific\             #
#           -message_type                     $message_type\                   #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -two_step_flag                    $two_step_flag\                  #
#           -unicast_flag                     $unicast_flag\                   #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -correction_field                 $correction_field\               #
#           -src_port_number                  $src_port_number\                #
#           -src_clock_identity               $src_clock_identity\             #
#           -sequence_id                      $sequence_id\                    #
#           -control_field                    $control_field\                  #
#           -log_message_interval             $log_message_interval\           #
#           -response_origin_timestamp_sec    $response_origin_timestamp_sec\  #
#           -response_origin_timestamp_ns     $response_origin_timestamp_ns\   #
#           -requesting_port_number           $requesting_port_number\         #
#           -requesting_clock_identity        $requesting_clock_identity\      #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#                                                                              #
################################################################################
proc wrptp::send_pdelay_resp_followup {args} {

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "wrptp::send_pdelay_resp_followup: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #MISCELLANEOUS
    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
        LOG -level 1 -msg "\t\tport_num                       -> $port_num"
    } else {

        ERRLOG -msg "wrptp::send_pdelay_resp_followup: Missing Argument -> port_num"
        return 0
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
        LOG -level 1 -msg "\t\tcount                          -> $count"
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
        LOG -level 1 -msg "\t\tinterval                       -> $interval"
    } else {

        set interval $::wrptp_sync_interval
    }


    #ETHERNET HEADER OPTIONS
    LOG -level 1 -msg "\tEthernet Details"

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
        LOG -level 1 -msg "\t\tdest_mac                       -> $dest_mac"
    } else {

        ERRLOG -msg "wrptp::send_pdelay_resp_followup: Missing Argument -> dest_mac"
        return 0
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
        LOG -level 1 -msg "\t\tsrc_mac                        -> $src_mac"
    } else {

        ERRLOG -msg "wrptp::send_pdelay_resp_followup: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        LOG -level 1 -msg "\t\teth_type                       -> $eth_type"
    } else {

        if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
            set eth_type 0800
        } else {
            set eth_type 88F7
        }
    }

    #Vlan ID
    if { $::wrptp_vlan_encap == "true" } {

        LOG -level 1 -msg "\tVLAN Details"

        if {[info exists param(-vlan_id)]} {

            set vlan_id $param(-vlan_id)
            LOG -level 1 -msg "\t\tvlan_id                        -> $vlan_id"
        } else {

            set vlan_id $::wrptp_vlan_id
        }

        if {[info exists param(-vlan_priority)]} {

            set vlan_priority $param(-vlan_priority)
            LOG -level 1 -msg "\t\tvlan_priority                  -> $vlan_priority"
        } else {

            set vlan_priority $::wrptp_vlan_priority
        }

    } else {

        set vlan_id -1
        set vlan_priority 0
    }

    #IP HEADER OPTIONS
    if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {

        LOG -level 1 -msg "\tIP Details"

        #Destination IP address
        if {[info exists param(-dest_ip)]} {

            set dest_ip $param(-dest_ip)
            LOG -level 1 -msg "\t\tdest_ip                        -> $dest_ip"
        } else {

            set dest_ip 224.0.1.129
        }

        #Source IP address
        if {[info exists param(-src_ip)]} {

            set src_ip $param(-src_ip)
            LOG -level 1 -msg "\t\tsrc_ip                         -> $src_ip"
        } else {

            set src_ip $::tee_test_port_ip
        }

        #IP checksum
        if {[info exists param(-ip_checksum)]} {

            set ip_checksum $param(-ip_checksum)
            LOG -level 1 -msg "\t\tip_checksum                    -> $ip_checksum"
        } else {

            set ip_checksum auto
        }

        #UDP HEADER OPTIONS

        #UDP destination port number
        if {[info exists param(-dest_port)]} {

            set dest_port $param(-dest_port)
            LOG -level 1 -msg "\t\tdest_port                      -> $dest_port"
        } else {

            set dest_port 320
        }

        #UDP source port number
        if {[info exists param(-src_port)]} {

            set src_port $param(-src_port)
            LOG -level 1 -msg "\t\tsrc_port                       -> $src_port"
        } else {

            set src_port 320
        }

        #UDP length
        if {[info exists param(-udp_length)]} {

            set udp_length $param(-udp_length)
            LOG -level 1 -msg "\t\tudp_length                     -> $udp_length"
        } else {

            set udp_length 62
        }

        #UDP checksum
        if {[info exists param(-udp_checksum)]} {

            set udp_checksum $param(-udp_checksum)
            LOG -level 1 -msg "\t\tudp_checksum                   -> $udp_checksum"
        } else {

            set udp_checksum 0
        }
    } else {
        set dest_ip 0.0.0.0
        set src_ip 0.0.0.0
        set ip_checksum 0
        set dest_port 0
        set src_port 0
        set udp_length 0
        set udp_checksum 0
    }

    #PTP HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Details"

    #Transport Specific
    if {[info exists param(-transport_specific)]} {

        set transport_specific $param(-transport_specific)
        LOG -level 1 -msg "\t\ttransport_specific             -> $transport_specific"
    } else {

        set transport_specific 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        LOG -level 1 -msg "\t\tmessage_type                   -> $message_type"
    } else {

        set message_type 10
    }

    #PTP version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        LOG -level 1 -msg "\t\tversion_ptp                    -> $version_ptp"
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        LOG -level 1 -msg "\t\tmessage_length                 -> $message_length"
    } else {

        set message_length 54
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        LOG -level 1 -msg "\t\tdomain_number                  -> $domain_number"
    } else {

        set domain_number 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
        LOG -level 1 -msg "\t\tcorrection_field               -> $correction_field"
    } else {

        set correction_field 0
    }

    #Source Port Number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
        LOG -level 1 -msg "\t\tsrc_port_number                -> $src_port_number"
    } else {

        if {![regexp {.*?([0-9]+)$} $port_num match src_port_number]} {
            set src_port_number 1
        }

    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
        LOG -level 1 -msg "\t\tsrc_clock_identity             -> 0x[dec2hex $src_clock_identity 16]"
    } else {

        get_clock_identity\
                    -mac               $src_mac\
                    -format            $::ptp_clock_identity_format\
                    -clock_identity    src_clock_identity
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        LOG -level 1 -msg "\t\tsequence_id                    -> $sequence_id"
    } else {

        set sequence_id 1
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        LOG -level 1 -msg "\t\tcontrol_field                  -> $control_field"
    } else {

        set control_field 5
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
        LOG -level 1 -msg "\t\tlog_message_interval           -> $log_message_interval"
    } else {

        set log_message_interval 127
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    LOG -level 1 -msg "\tPTP Flag Details"
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        LOG -level 1 -msg "\t\talternate_master_flag          -> $alternate_master_flag"
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
        LOG -level 1 -msg "\t\ttwo_step_flag                  -> $two_step_flag"
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
        LOG -level 1 -msg "\t\tunicast_flag                   -> $unicast_flag"
    } else {

        set unicast_flag $::unicast_flag
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        LOG -level 1 -msg "\t\tprofile_specific1              -> $profile_specific1"
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        LOG -level 1 -msg "\t\tprofile_specific2              -> $profile_specific2"
    } else {

        set profile_specific2 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        LOG -level 1 -msg "\t\tleap61                         -> $leap61"
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        LOG -level 1 -msg "\t\tleap59                         -> $leap59"
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        LOG -level 1 -msg "\t\tcurrent_utc_offset_valid       -> $current_utc_offset_valid"
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        LOG -level 1 -msg "\t\tptp_timescale                  -> $ptp_timescale"
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        LOG -level 1 -msg "\t\ttime_traceable                 -> $time_traceable"
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        LOG -level 1 -msg "\t\tfreq_traceable                 -> $freq_traceable"
    } else {

        set freq_traceable 0
    }

    #PTP PDELAY_RESP_FOLLOWUP HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Pdelay_resp_followup Message Details"

    if {[info exists param(-response_origin_timestamp_sec)]} {

        set response_origin_timestamp_sec $param(-response_origin_timestamp_sec)
        LOG -level 1 -msg "\t\tresponse_origin_timestamp_sec  -> $response_origin_timestamp_sec"
    } else {

        set response_origin_timestamp_sec 0
    }

    if {[info exists param(-response_origin_timestamp_ns)]} {

        set response_origin_timestamp_ns $param(-response_origin_timestamp_ns)
        LOG -level 1 -msg "\t\tresponse_origin_timestamp_ns   -> $response_origin_timestamp_ns"
    } else {

        set response_origin_timestamp_ns 0
    }

    if {[info exists param(-requesting_port_number)]} {

        set requesting_port_number $param(-requesting_port_number)
        LOG -level 1 -msg "\t\trequesting_port_number         -> $requesting_port_number"
    } else {

        set requesting_port_number 0
    }

    if {[info exists param(-requesting_clock_identity)]} {

        set requesting_clock_identity $param(-requesting_clock_identity)
        LOG -level 1 -msg "\t\trequesting_clock_identity      -> $requesting_clock_identity"
    } else {

        set requesting_clock_identity 0
    }

    set instance $::INSTANCE(PDELAY_RESP_FOLLOWUP)

    #CALL PLATFORM FUNCTION
    if {![pltLib::send_wrptp_pkt\
                -session_id                        $session_id\
                -instance                          $instance\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -transport_specific                $transport_specific\
                -message_type                      $message_type\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_traceable                    $time_traceable\
                -freq_traceable                    $freq_traceable\
                -correction_field                  $correction_field\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -response_origin_timestamp_sec     $response_origin_timestamp_sec\
                -response_origin_timestamp_ns      $response_origin_timestamp_ns\
                -requesting_port_number            $requesting_port_number\
                -requesting_clock_identity         $requesting_clock_identity\
                -count                             $count\
                -interval                          $interval]} {

        return 0

    }

    return 1

}

################################################################################
#                                                                              #
#  PROCEDURE NAME         :  wrptp::send_signal                                #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Transmission port              #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   dest_mac                      : (Mandatory) Destination MAC dddress.       #
#                                               (e.g., 01:80:C2:00:00:20)      #
#                                                                              #
#   src_mac                       : (Mandatory) Source MAC address.            #
#                                               (e.g., 00:80:C2:00:00:21)      #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                               (Default : 88F7)               #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                               (Default : 0)                  #
#                                                                              #
#   vlan_priority                 : (Optional)  Vlan Priority                  #
#                                               (Default : 0)                  #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                               (Default : 224.0.1.129)        #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                               (Default : 0.0.0.0)            #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                               (Default : auto)               #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                               (Default : 320)                #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                               (Default : 320)                #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                               (Default : 96)                 #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                               (Default : 0)                  #
#                                                                              #
#   transport_specific            : (Optional)  Transport Specific             #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default : 12)                 #
#                                                                              #
#   version_ptp                   : (Optional)  PTP version number             #
#                                               (Default : 2)                  #
#                                                                              #
#   message_length                : (Optional)  PTP message length             #
#                                               (Default : 88)                 #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                               (Default : 0)                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                               (Default : 0)                  #
#                                                                              #
#   two_step_flag                 : (Optional)  Two Step Flag                  #
#                                               (Default : 0)                  #
#                                                                              #
#   unicast_flag                  : (Optional)  Unicast Flag                   #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                               (Default : 0)                  #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP TimeSclae Flag             #
#                                               (Default : 0)                  #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                               (Default : 0)                  #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                               (Default : 0)                  #
#                                                                              #
#   correction_field              : (Optional)  Correction Field value         #
#                                               (Default : 0)                  #
#                                                                              #
#   src_port_number               : (Optional)  Source Port number             #
#                                               (Default : 0)                  #
#                                                                              #
#   src_clock_identity            : (Optional)  Source Clock identity          #
#                                               (Default : 0)                  #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                               (Default : 0)                  #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                               (Default : 5)                  #
#                                                                              #
#   log_message_interval          : (Optional)  Log message interval           #
#                                               (Default : 127)                #
#                                                                              #
#   target_port_number            : (Optional)  Target Port Number             #
#                                               (Default : 0)                  #
#                                                                              #
#   target_clock_identity         : (Optional)  Target Clock Identity          #
#                                               (Default : 0)                  #
#                                                                              #
#  (PTP TLVs)                                                                  #
#                                                                              #
#   tlv_type                      : (Optional)  TLV Type                       #
#                                               (Default : -1)                 #
#                                                                              #
#   tlv_length                    : (Optional)  TLV length                     #
#                                               (Default : 8)                  #
#                                                                              #
#   organization_id               : (Optional) Organization id                 #
#                                               (Default: 0x080030)            #
#                                                                              #
#   magic_number                  : (Optional) Magic number                    #
#                                               (Default: 0xDEAD)              #
#                                                                              #
#   version_number                : (Optional) Version number                  #
#                                               (Default: 1)                   #
#                                                                              #
#   message_id                    : (Optional) Message id                      #
#                                               (Default: 0x1000)              #
#                                                                              #
#   cal_send_pattern              : (Optional) Cal send pattern                #
#                                               (Default: 0)                   #
#                                                                              #
#   cal_retry                     : (Optional) Cal retry                       #
#                                               (Default: 0)                   #
#                                                                              #
#   cal_period                    : (Optional) Cal period                      #
#                                               (Default: 0)                   #
#                                                                              #
#   delta_tx                      : (Optional) Delta tx                        #
#                                               (Default: 0)                   #
#                                                                              #
#   delta_rx                      : (Optional) Delta rx                        #
#                                               (Default: 0)                   #
#                                                                              #
#  (MISC)                                                                      #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default : 1)                  #
#                                                                              #
#   interval                      : (Optional)  Frame interval (in seconds)    #
#                                               (Default : 1)                  #
#                                                                              #
#  OUTPUT PARAMETERS      :  NONE                                              #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is successfully send   #
#                                         with the given parameters)           #
#                                                                              #
#                         : 0 on failure (If the packet could not be send      #
#                                         with the given parameters)           #
#                                                                              #
#  DEFINITION             : This function is used to construct and send        #
#                           PTP signal message                                 #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       wrptp::send_signal\                                                    #
#           -port_num                         $port_num\                       #
#           -dest_mac                         $dest_mac\                       #
#           -src_mac                          $src_mac\                        #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -vlan_priority                    $vlan_priority\                  #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -transport_specific               $transport_specific\             #
#           -message_type                     $message_type\                   #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -two_step_flag                    $two_step_flag\                  #
#           -unicast_flag                     $unicast_flag\                   #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -correction_field                 $correction_field\               #
#           -src_port_number                  $src_port_number\                #
#           -src_clock_identity               $src_clock_identity\             #
#           -sequence_id                      $sequence_id\                    #
#           -control_field                    $control_field\                  #
#           -log_message_interval             $log_message_interval\           #
#           -target_port_number               $target_port_number\             #
#           -target_clock_identity            $target_clock_identity\          #
#           -tlv_type                         $tlv_type\                       #
#           -tlv_length                       $tlv_length\                     #
#           -organization_id                  $organization_id\                #
#           -magic_number                     $magic_number\                   #
#           -version_number                   $version_number\                 #
#           -message_id                       $message_id\                     #
#           -cal_send_pattern                 $cal_send_pattern\               #
#           -cal_retry                        $cal_retry\                      #
#           -cal_period                       $cal_period\                     #
#           -delta_tx                         $delta_tx\                       #
#           -delta_rx                         $delta_rx\                       #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#                                                                              #
################################################################################
proc wrptp::send_signal {args} {

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "wrptp::send_signal: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #MISCELLANEOUS
    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
        LOG -level 1 -msg "\t\tport_num                       -> $port_num"
    } else {

        ERRLOG -msg "wrptp::send_signal: Missing Argument -> port_num"
        return 0
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
        LOG -level 1 -msg "\t\tcount                          -> $count"
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
        LOG -level 1 -msg "\t\tinterval                       -> $interval"
    } else {

        set interval $::wrptp_sync_interval
    }


    #ETHERNET HEADER OPTIONS
    LOG -level 1 -msg "\tEthernet Details"

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
        LOG -level 1 -msg "\t\tdest_mac                       -> $dest_mac"
    } else {

        ERRLOG -msg "wrptp::send_signal: Missing Argument -> dest_mac"
        return 0
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
        LOG -level 1 -msg "\t\tsrc_mac                        -> $src_mac"
    } else {

        ERRLOG -msg "wrptp::send_signal: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        LOG -level 1 -msg "\t\teth_type                       -> $eth_type"
    } else {

        if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
            set eth_type 0800
        } else {
            set eth_type 88F7
        }
    }

    #Vlan ID
    if { $::wrptp_vlan_encap == "true" } {

        LOG -level 1 -msg "\tVLAN Details"

        if {[info exists param(-vlan_id)]} {

            set vlan_id $param(-vlan_id)
            LOG -level 1 -msg "\t\tvlan_id                        -> $vlan_id"
        } else {

            set vlan_id $::wrptp_vlan_id
        }

        if {[info exists param(-vlan_priority)]} {

            set vlan_priority $param(-vlan_priority)
            LOG -level 1 -msg "\t\tvlan_priority                  -> $vlan_priority"
        } else {

            set vlan_priority $::wrptp_vlan_priority
        }

    } else {

        set vlan_id -1
        set vlan_priority 0
    }

    #IP HEADER OPTIONS
    if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {

        LOG -level 1 -msg "\tIP Details"

        #Destination IP address
        if {[info exists param(-dest_ip)]} {

            set dest_ip $param(-dest_ip)
            LOG -level 1 -msg "\t\tdest_ip                        -> $dest_ip"
        } else {

            set dest_ip 224.0.1.129
        }

        #Source IP address
        if {[info exists param(-src_ip)]} {

            set src_ip $param(-src_ip)
            LOG -level 1 -msg "\t\tsrc_ip                         -> $src_ip"
        } else {

            set src_ip $::tee_test_port_ip
        }

        #IP checksum
        if {[info exists param(-ip_checksum)]} {

            set ip_checksum $param(-ip_checksum)
            LOG -level 1 -msg "\t\tip_checksum                    -> $ip_checksum"
        } else {

            set ip_checksum auto
        }

        #UDP HEADER OPTIONS

        #UDP destination port number
        if {[info exists param(-dest_port)]} {

            set dest_port $param(-dest_port)
            LOG -level 1 -msg "\t\tdest_port                      -> $dest_port"
        } else {

            set dest_port 320
        }

        #UDP source port number
        if {[info exists param(-src_port)]} {

            set src_port $param(-src_port)
            LOG -level 1 -msg "\t\tsrc_port                       -> $src_port"
        } else {

            set src_port 320
        }

        #UDP length
        if {[info exists param(-udp_length)]} {

            set udp_length $param(-udp_length)
            LOG -level 1 -msg "\t\tudp_length                     -> $udp_length"
        } else {

            set udp_length 96
        }

        #UDP checksum
        if {[info exists param(-udp_checksum)]} {

            set udp_checksum $param(-udp_checksum)
            LOG -level 1 -msg "\t\tudp_checksum                   -> $udp_checksum"
        } else {

            set udp_checksum 0
        }
    } else {
        set dest_ip 0.0.0.0
        set src_ip 0.0.0.0
        set ip_checksum 0
        set dest_port 0
        set src_port 0
        set udp_length 0
        set udp_checksum 0
    }

    #PTP HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Details"

    #Transport Specific
    if {[info exists param(-transport_specific)]} {

        set transport_specific $param(-transport_specific)
        LOG -level 1 -msg "\t\ttransport_specific             -> $transport_specific"
    } else {

        set transport_specific 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        LOG -level 1 -msg "\t\tmessage_type                   -> $message_type"
    } else {

        set message_type 12
    }

    #PTP version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        LOG -level 1 -msg "\t\tversion_ptp                    -> $version_ptp"
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        LOG -level 1 -msg "\t\tmessage_length                 -> $message_length"
    } else {

        set message_length 88
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        LOG -level 1 -msg "\t\tdomain_number                  -> $domain_number"
    } else {

        set domain_number 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
        LOG -level 1 -msg "\t\tcorrection_field               -> $correction_field"
    } else {

        set correction_field 0
    }

    #Source Port Number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
        LOG -level 1 -msg "\t\tsrc_port_number                -> $src_port_number"
    } else {

        if {![regexp {.*?([0-9]+)$} $port_num match src_port_number]} {
            set src_port_number 1
        }

    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
        LOG -level 1 -msg "\t\tsrc_clock_identity             -> 0x[dec2hex $src_clock_identity 16]"
    } else {

        get_clock_identity\
                    -mac               $src_mac\
                    -format            $::ptp_clock_identity_format\
                    -clock_identity    src_clock_identity
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        LOG -level 1 -msg "\t\tsequence_id                    -> $sequence_id"
    } else {

        set sequence_id 1
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        LOG -level 1 -msg "\t\tcontrol_field                  -> $control_field"
    } else {

        set control_field 5
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
        LOG -level 1 -msg "\t\tlog_message_interval           -> $log_message_interval"
    } else {

        set log_message_interval 127
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    LOG -level 1 -msg "\tPTP Flag Details"
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        LOG -level 1 -msg "\t\talternate_master_flag          -> $alternate_master_flag"
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
        LOG -level 1 -msg "\t\ttwo_step_flag                  -> $two_step_flag"
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
        LOG -level 1 -msg "\t\tunicast_flag                   -> $unicast_flag"
    } else {

        set unicast_flag $::unicast_flag
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        LOG -level 1 -msg "\t\tprofile_specific1              -> $profile_specific1"
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        LOG -level 1 -msg "\t\tprofile_specific2              -> $profile_specific2"
    } else {

        set profile_specific2 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        LOG -level 1 -msg "\t\tleap61                         -> $leap61"
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        LOG -level 1 -msg "\t\tleap59                         -> $leap59"
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        LOG -level 1 -msg "\t\tcurrent_utc_offset_valid       -> $current_utc_offset_valid"
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        LOG -level 1 -msg "\t\tptp_timescale                  -> $ptp_timescale"
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        LOG -level 1 -msg "\t\ttime_traceable                 -> $time_traceable"
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        LOG -level 1 -msg "\t\tfreq_traceable                 -> $freq_traceable"
    } else {

        set freq_traceable 0
    }

    #PTP SIGNAL HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Signal Message Details"

    if {[info exists param(-target_port_number)]} {

        set target_port_number $param(-target_port_number)
        LOG -level 1 -msg "\t\ttarget_port_number             -> $target_port_number"
    } else {

        set target_port_number 0
    }

    if {[info exists param(-target_clock_identity)]} {

        set target_clock_identity $param(-target_clock_identity)
        LOG -level 1 -msg "\t\ttarget_clock_identity          -> 0x[dec2hex $target_clock_identity 16]"
    } else {

        set target_clock_identity 0
    }

    #TLV Type
    if {[info exists param(-tlv_type)]} {

        set tlv_type $param(-tlv_type)
        LOG -level 1 -msg "\t\ttlv_type                       -> $tlv_type"
    } else {

        set tlv_type -1
    }

    #TLV length
    if {[info exists param(-tlv_length)]} {

        set tlv_length $param(-tlv_length)
    } else {

        set tlv_length 8
    }

    #Organisation ID
    if {[info exists param(-organization_id)]} {

        set organization_id $param(-organization_id)
    } else {

        set organization_id 0x080030
    }

    #Magic Number
    if {[info exists param(-magic_number)]} {

        set magic_number $param(-magic_number)
    } else {

        set magic_number 0xDEAD
    }

    #Version Number
    if {[info exists param(-version_number)]} {

        set version_number $param(-version_number)
    } else {

        set version_number 1
    }

    #Message ID
    if {[info exists param(-message_id)]} {

        set message_id $param(-message_id)
    } else {

        set message_id 0x1000
    }

    #Cal Send Pattern
    if {[info exists param(-cal_send_pattern)]} {

        set cal_send_pattern $param(-cal_send_pattern)
        LOG -level 1 -msg "\t\tcal_send_pattern               -> $cal_send_pattern"
    } else {

        set cal_send_pattern 0
    }

    #Cal Retry
    if {[info exists param(-cal_retry)]} {

        set cal_retry $param(-cal_retry)
        LOG -level 1 -msg "\t\tcal_retry                      -> $cal_retry"
    } else {

        set cal_retry 0
    }

    #Cal Period
    if {[info exists param(-cal_period)]} {

        set cal_period $param(-cal_period)
        LOG -level 1 -msg "\t\tcal_period                     -> $cal_period"
    } else {

        set cal_period 0
    }

    #Delta Tx
    if {[info exists param(-delta_tx)]} {

        set delta_tx $param(-delta_tx)
        LOG -level 1 -msg "\t\tdelta_tx                       -> $delta_tx"
    } else {

        set delta_tx $::wrptp_known_delta_tx
    }

    #Delta Rx
    if {[info exists param(-delta_rx)]} {

        set delta_rx $param(-delta_rx)
        LOG -level 1 -msg "\t\tdelta_rx                       -> $delta_rx"
    } else {

        set delta_rx $::wrptp_known_delta_rx
    }

    set instance $::INSTANCE(SIGNAL)

    #CALL PLATFORM FUNCTION
    if {![pltLib::send_wrptp_pkt\
                -session_id                        $session_id\
                -instance                          $instance\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -transport_specific                $transport_specific\
                -message_type                      $message_type\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_traceable                    $time_traceable\
                -freq_traceable                    $freq_traceable\
                -correction_field                  $correction_field\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -target_port_number                $target_port_number\
                -target_clock_identity             $target_clock_identity\
                -tlv_type                          $tlv_type\
                -tlv_length                        $tlv_length\
                -organization_id                   $organization_id\
                -magic_number                      $magic_number\
                -version_number                    $version_number\
                -message_id                        $message_id\
                -cal_send_pattern                  $cal_send_pattern\
                -cal_retry                         $cal_retry\
                -cal_period                        $cal_period\
                -delta_tx                          $delta_tx\
                -delta_rx                          $delta_rx\
                -count                             $count\
                -interval                          $interval]} {

        return 0

    }

    return 1

}

################################################################################
#                                                                              #
#  PROCEDURE NAME         :  wrptp::send_mgmt                                  #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Transmission port              #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   dest_mac                      : (Mandatory) Destination MAC dddress.       #
#                                               (e.g., 01:80:C2:00:00:20)      #
#                                                                              #
#   src_mac                       : (Mandatory) Source MAC address.            #
#                                               (e.g., 00:80:C2:00:00:21)      #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                               (Default : 88F7)               #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                               (Default : 0)                  #
#                                                                              #
#   vlan_priority                 : (Optional)  Vlan Priority                  #
#                                               (Default : 0)                  #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                               (Default : 224.0.1.129)        #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                               (Default : 0.0.0.0)            #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                               (Default : auto)               #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                               (Default : 320)                #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                               (Default : 320)                #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                               (Default : 64)                 #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                               (Default : 0)                  #
#                                                                              #
#   transport_specific            : (Optional)  Transport Specific             #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default : 14)                 #
#                                                                              #
#   version_ptp                   : (Optional)  PTP version number             #
#                                               (Default : 2)                  #
#                                                                              #
#   message_length                : (Optional)  PTP message length             #
#                                               (Default : 56)                 #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                               (Default : 0)                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                               (Default : 0)                  #
#                                                                              #
#   two_step_flag                 : (Optional)  Two Step Flag                  #
#                                               (Default : 0)                  #
#                                                                              #
#   unicast_flag                  : (Optional)  Unicast Flag                   #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                               (Default : 0)                  #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP TimeSclae Flag             #
#                                               (Default : 0)                  #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                               (Default : 0)                  #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                               (Default : 0)                  #
#                                                                              #
#   correction_field              : (Optional)  Correction Field value         #
#                                               (Default : 0)                  #
#                                                                              #
#   src_port_number               : (Optional)  Source Port number             #
#                                               (Default : 0)                  #
#                                                                              #
#   src_clock_identity            : (Optional)  Source Clock identity          #
#                                               (Default : 0)                  #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                               (Default : 0)                  #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                               (Default : 4)                  #
#                                                                              #
#   log_message_interval          : (Optional)  Log message interval           #
#                                               (Default : 127)                #
#                                                                              #
#   target_port_number            : (Optional)  Target Port Number             #
#                                               (Default : 0)                  #
#                                                                              #
#   target_clock_identity         : (Optional)  Target Clock Identity          #
#                                               (Default : 0)                  #
#                                                                              #
#   starting_boundary_hops        : (Optional)  Starting Boundary Hops         #
#                                               (Default : 0)                  #
#                                                                              #
#   boundary_hops                 : (Optional)  Boundary Hops                  #
#                                               (Default : 0)                  #
#                                                                              #
#   action_field                  : (Optional)  Action Field                   #
#                                               (Default : 0)                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default : 1)                  #
#                                                                              #
#   interval                      : (Optional)  Frame interval (in seconds)    #
#                                               (Default : 1)                  #
#                                                                              #
#  OUTPUT PARAMETERS      :  NONE                                              #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is successfully send   #
#                                         with the given parameters)           #
#                                                                              #
#                         : 0 on failure (If the packet could not be send      #
#                                         with the given parameters)           #
#                                                                              #
#  DEFINITION             : This function is used to construct and send        #
#                           PTP mgmt message                                   #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       wrptp::send_mgmt\                                                      #
#           -port_num                         $port_num\                       #
#           -dest_mac                         $dest_mac\                       #
#           -src_mac                          $src_mac\                        #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -vlan_priority                    $vlan_priority\                  #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -transport_specific               $transport_specific\             #
#           -message_type                     $message_type\                   #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -two_step_flag                    $two_step_flag\                  #
#           -unicast_flag                     $unicast_flag\                   #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -correction_field                 $correction_field\               #
#           -src_port_number                  $src_port_number\                #
#           -src_clock_identity               $src_clock_identity\             #
#           -sequence_id                      $sequence_id\                    #
#           -control_field                    $control_field\                  #
#           -log_message_interval             $log_message_interval\           #
#           -target_port_number               $target_port_number\             #
#           -target_clock_identity            $target_clock_identity\          #
#           -starting_boundary_hops           $starting_boundary_hops\         #
#           -boundary_hops                    $boundary_hops\                  #
#           -action_field                     $action_field\                   #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#                                                                              #
################################################################################
proc wrptp::send_mgmt {args} {

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "wrptp::send_mgmt: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #MISCELLANEOUS
    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
        LOG -level 1 -msg "\t\tport_num                       -> $port_num"
    } else {

        ERRLOG -msg "wrptp::send_mgmt: Missing Argument -> port_num"
        return 0
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
        LOG -level 1 -msg "\t\tcount                          -> $count"
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
        LOG -level 1 -msg "\t\tinterval                       -> $interval"
    } else {

        set interval $::wrptp_sync_interval
    }


    #ETHERNET HEADER OPTIONS
    LOG -level 1 -msg "\tEthernet Details"

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
        LOG -level 1 -msg "\t\tdest_mac                       -> $dest_mac"
    } else {

        ERRLOG -msg "wrptp::send_mgmt: Missing Argument -> dest_mac"
        return 0
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
        LOG -level 1 -msg "\t\tsrc_mac                        -> $src_mac"
    } else {

        ERRLOG -msg "wrptp::send_mgmt: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        LOG -level 1 -msg "\t\teth_type                       -> $eth_type"
    } else {

        if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
            set eth_type 0800
        } else {
            set eth_type 88F7
        }
    }

    #Vlan ID
    if { $::wrptp_vlan_encap == "true" } {

        LOG -level 1 -msg "\tVLAN Details"

        if {[info exists param(-vlan_id)]} {

            set vlan_id $param(-vlan_id)
            LOG -level 1 -msg "\t\tvlan_id                        -> $vlan_id"
        } else {

            set vlan_id $::wrptp_vlan_id
        }

        if {[info exists param(-vlan_priority)]} {

            set vlan_priority $param(-vlan_priority)
            LOG -level 1 -msg "\t\tvlan_priority                  -> $vlan_priority"
        } else {

            set vlan_priority $::wrptp_vlan_priority
        }

    } else {

        set vlan_id -1
        set vlan_priority 0
    }

    #IP HEADER OPTIONS
    if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {

        LOG -level 1 -msg "\tIP Details"

        #Destination IP address
        if {[info exists param(-dest_ip)]} {

            set dest_ip $param(-dest_ip)
            LOG -level 1 -msg "\t\tdest_ip                        -> $dest_ip"
        } else {

            set dest_ip 224.0.1.129
        }

        #Source IP address
        if {[info exists param(-src_ip)]} {

            set src_ip $param(-src_ip)
            LOG -level 1 -msg "\t\tsrc_ip                         -> $src_ip"
        } else {

            set src_ip $::tee_test_port_ip
        }

        #IP checksum
        if {[info exists param(-ip_checksum)]} {

            set ip_checksum $param(-ip_checksum)
            LOG -level 1 -msg "\t\tip_checksum                    -> $ip_checksum"
        } else {

            set ip_checksum auto
        }

        #UDP HEADER OPTIONS

        #UDP destination port number
        if {[info exists param(-dest_port)]} {

            set dest_port $param(-dest_port)
            LOG -level 1 -msg "\t\tdest_port                      -> $dest_port"
        } else {

            set dest_port 320
        }

        #UDP source port number
        if {[info exists param(-src_port)]} {

            set src_port $param(-src_port)
            LOG -level 1 -msg "\t\tsrc_port                       -> $src_port"
        } else {

            set src_port 320
        }

        #UDP length
        if {[info exists param(-udp_length)]} {

            set udp_length $param(-udp_length)
            LOG -level 1 -msg "\t\tudp_length                     -> $udp_length"
        } else {

            set udp_length 64
        }

        #UDP checksum
        if {[info exists param(-udp_checksum)]} {

            set udp_checksum $param(-udp_checksum)
            LOG -level 1 -msg "\t\tudp_checksum                   -> $udp_checksum"
        } else {

            set udp_checksum 0
        }
    } else {
        set dest_ip 0.0.0.0
        set src_ip 0.0.0.0
        set ip_checksum 0
        set dest_port 0
        set src_port 0
        set udp_length 0
        set udp_checksum 0
    }

    #PTP HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Details"

    #Transport Specific
    if {[info exists param(-transport_specific)]} {

        set transport_specific $param(-transport_specific)
        LOG -level 1 -msg "\t\ttransport_specific             -> $transport_specific"
    } else {

        set transport_specific 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        LOG -level 1 -msg "\t\tmessage_type                   -> $message_type"
    } else {

        set message_type 13
    }

    #PTP version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        LOG -level 1 -msg "\t\tversion_ptp                    -> $version_ptp"
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        LOG -level 1 -msg "\t\tmessage_length                 -> $message_length"
    } else {

        set message_length 56
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        LOG -level 1 -msg "\t\tdomain_number                  -> $domain_number"
    } else {

        set domain_number 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
        LOG -level 1 -msg "\t\tcorrection_field               -> $correction_field"
    } else {

        set correction_field 0
    }

    #Source Port Number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
        LOG -level 1 -msg "\t\tsrc_port_number                -> $src_port_number"
    } else {

        if {![regexp {.*?([0-9]+)$} $port_num match src_port_number]} {
            set src_port_number 1
        }

    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
        LOG -level 1 -msg "\t\tsrc_clock_identity             -> 0x[dec2hex $src_clock_identity 16]"
    } else {

        get_clock_identity\
                    -mac               $src_mac\
                    -format            $::ptp_clock_identity_format\
                    -clock_identity    src_clock_identity
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        LOG -level 1 -msg "\t\tsequence_id                    -> $sequence_id"
    } else {

        set sequence_id 1
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        LOG -level 1 -msg "\t\tcontrol_field                  -> $control_field"
    } else {

        set control_field 0
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
        LOG -level 1 -msg "\t\tlog_message_interval           -> $log_message_interval"
    } else {

        set log_message_interval 127
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    LOG -level 1 -msg "\tPTP Flag Details"
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        LOG -level 1 -msg "\t\talternate_master_flag          -> $alternate_master_flag"
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
        LOG -level 1 -msg "\t\ttwo_step_flag                  -> $two_step_flag"
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
        LOG -level 1 -msg "\t\tunicast_flag                   -> $unicast_flag"
    } else {

        set unicast_flag $::unicast_flag
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        LOG -level 1 -msg "\t\tprofile_specific1              -> $profile_specific1"
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        LOG -level 1 -msg "\t\tprofile_specific2              -> $profile_specific2"
    } else {

        set profile_specific2 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        LOG -level 1 -msg "\t\tleap61                         -> $leap61"
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        LOG -level 1 -msg "\t\tleap59                         -> $leap59"
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        LOG -level 1 -msg "\t\tcurrent_utc_offset_valid       -> $current_utc_offset_valid"
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        LOG -level 1 -msg "\t\tptp_timescale                  -> $ptp_timescale"
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        LOG -level 1 -msg "\t\ttime_traceable                 -> $time_traceable"
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        LOG -level 1 -msg "\t\tfreq_traceable                 -> $freq_traceable"
    } else {

        set freq_traceable 0
    }

    #PTP MGMT HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Mgmt Message Details"

    if {[info exists param(-target_port_number)]} {

        set target_port_number $param(-target_port_number)
        LOG -level 1 -msg "\t\ttarget_port_number             -> $target_port_number"
    } else {

        set target_port_number 0
    }

    if {[info exists param(-target_clock_identity)]} {

        set target_clock_identity $param(-target_clock_identity)
        LOG -level 1 -msg "\t\ttarget_clock_identity          -> 0x[dec2hex $target_clock_identity 16]"
    } else {

        set target_clock_identity 0
    }

    if {[info exists param(-starting_boundary_hops)]} {

        set starting_boundary_hops $param(-starting_boundary_hops)
        LOG -level 1 -msg "\t\tstarting_boundary_hops         -> $starting_boundary_hops"
    } else {

        set starting_boundary_hops 0
    }

    if {[info exists param(-boundary_hops)]} {

        set boundary_hops $param(-boundary_hops)
        LOG -level 1 -msg "\t\tboundary_hops                  -> $boundary_hops"
    } else {

        set boundary_hops 0
    }

    if {[info exists param(-action_field)]} {

        set action_field $param(-action_field)
        LOG -level 1 -msg "\t\taction_field                   -> $action_field"
    } else {

        set action_field 0
    }

    if {[info exists param(-tlv_type)]} {

        set tlv_type $param(-tlv_type)
        LOG -level 1 -msg "\t\ttlv_type                       -> $tlv_type"
    } else {

        set tlv_type -1
    }

    if {[info exists param(-tlv_length)]} {

        set tlv_length $param(-tlv_length)
        LOG -level 1 -msg "\t\ttlv_length                     -> $tlv_length"
    } else {

        set tlv_length 4
    }

    set instance $::INSTANCE(MGMT)

    #CALL PLATFORM FUNCTION
    if {![pltLib::send_wrptp_pkt\
                -session_id                        $session_id\
                -instance                          $instance\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -transport_specific                $transport_specific\
                -message_type                      $message_type\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_traceable                    $time_traceable\
                -freq_traceable                    $freq_traceable\
                -correction_field                  $correction_field\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -target_port_number                $target_port_number\
                -target_clock_identity             $target_clock_identity\
                -starting_boundary_hops            $starting_boundary_hops\
                -boundary_hops                     $boundary_hops\
                -action_field                      $action_field\
                -count                             $count\
                -interval                          $interval]} {

        return 0

    }

    return 1

}

################################################################################
#                                                                              #
#  PROCEDURE NAME         :  wrptp::recv_sync                                  #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Reception port                 #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   filter_id                     : (Mandatory) Filter id                      #
#                                                                              #
#   reset_count                   : (Optional)  Reset Count                    #
#                                               (Default: ON)                  #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                                                              #
#   transport_specific            : (Optional)  Transport Specific             #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default: 0)                   #
#                                                                              #
#   version_ptp                   : (Optional)  PTP version number             #
#                                                                              #
#   message_length                : (Optional)  PTP Annouce message length     #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP Timescale Flag             #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default: 1)                   #
#                                                                              #
#   timeout                       : (Optional)  Frame timeout (in seconds)     #
#                                               (Default: 5)                   #
#                                                                              #
#  OUTPUT PARAMETERS      :                                                    #
#                                                                              #
#   recvd_dest_mac                : (Optional)  Destination MAC dddress.       #
#                                                                              #
#   recvd_src_mac                 : (Optional)  Source MAC address.            #
#                                                                              #
#   recvd_two_step_flag           : (Optional)  Two Step Flag                  #
#                                                                              #
#   recvd_unicast_flag            : (Optional)  Unicast Flag                   #
#                                                                              #
#   recvd_correction_field        : (Optional)  Correction Field value         #
#                                                                              #
#   recvd_src_port_number         : (Optional)  Source port number             #
#                                                                              #
#   recvd_src_clock_identity      : (Optional)  Source clock identity          #
#                                                                              #
#   recvd_sequence_id             : (Optional)  Sequence ID                    #
#                                                                              #
#   recvd_log_message_interval    : (Optional)  Log message interval           #
#                                                                              #
#   recvd_origin_timestamp_sec    : (Optional)  Recvd Origin Timestamp Sec     #
#                                                                              #
#   recvd_origin_timestamp_ns     : (Optional)  Recvd Origin Timestamp Ns      #
#                                                                              #
#   rx_timestamp_sec              : (Optional)  Packet's rx time in seconds    #
#                                                                              #
#   rx_timestamp_ns               : (Optional)  Packet's rx time in ns         #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is received as         #
#                                         expected)                            #
#                                                                              #
#                         : 0 on failure (If the packet is not received        #
#                                         as expected)                         #
#                                                                              #
#  DEFINITION             : This function is used to receive                   #
#                           PTP sync message                                   #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       wrptp::recv_sync\                                                      #
#           -port_num                         $port_num\                       #
#           -filter_id                        $filter_id\                      #
#           -reset_count                      $reset_count\                    #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -transport_specific               $transport_specific\             #
#           -message_type                     $message_type\                   #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -control_field                    $control_field\                  #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#           -recvd_dest_mac                   recvd_dest_mac\                  #
#           -recvd_src_mac                    recvd_src_mac\                   #
#           -recvd_two_step_flag              recvd_two_step_flag\             #
#           -recvd_unicast_flag               recvd_unicast_flag\              #
#           -recvd_correction_field           recvd_correction_field\          #
#           -recvd_src_port_number            recvd_src_port_identity\         #
#           -recvd_src_clock_identity         recvd_src_clock_identity\        #
#           -recvd_sequence_id                recvd_sequence_id\               #
#           -recvd_log_message_interval       recvd_message_interval\          #
#           -recvd_origin_timestamp_sec       recvd_origin_timestamp_sec\      #
#           -recvd_origin_timestamp_ns        recvd_origin_timestamp_ns\       #
#           -rx_timestamp_sec                 rx_timestamp_sec\                #
#           -rx_timestamp_ns                  rx_timestamp_ns                  #
#                                                                              #
################################################################################
proc wrptp::recv_sync {args} {

    array unset ::validation_param
    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "wrptp::recv_sync: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #Reception port
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "wrptp::recv_sync: Missing Argument -> port_num"
        return 0
    }

    #Filter id
    if {[info exists param(-filter_id)]} {

        set filter_id $param(-filter_id)
    } else {

        ERRLOG -msg "wrptp::recv_sync: Missing Argument -> filter_id"
        return 0
    }

    #Reset Count
    if {[info exists param(-reset_count)]} {

        set reset_count $param(-reset_count)
    } else {

        set reset_count ON
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        set ::validation_param(eth_type) $param(-eth_type)
    } else {

        if { $::wrptp_vlan_encap == "true" } {
            set ::validation_param(eth_type) 8100
        } else {
            if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
                set ::validation_param(eth_type) 0800
            } else {
                set ::validation_param(eth_type) 88F7
            }
        }
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
        set ::validation_param(vlan_id) $param(-vlan_id)
    } else {

        if { $::wrptp_vlan_encap == "true" } {
            set ::validation_param(vlan_id) $::wrptp_vlan_id
        } else {
            set ::validation_param(vlan_id) "dont_care"
        }
    }

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
        set ::validation_param(dest_ip) $param(-dest_ip)
    } else {

        set ::validation_param(dest_ip) "dont_care"
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
        set ::validation_param(src_ip) $param(-src_ip)
    } else {

        set ::validation_param(src_ip) "dont_care"
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
        set ::validation_param(ip_checksum) $param(-ip_checksum)
    } else {

        set ::validation_param(ip_checksum) "dont_care"
    }

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
        set ::validation_param(dest_port) $param(-dest_port)
    } else {

        set ::validation_param(dest_port) "dont_care"
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
        set ::validation_param(src_port) $param(-src_port)
    } else {

        set ::validation_param(src_port) "dont_care"
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
        set ::validation_param(udp_length) $param(-udp_length)
    } else {

        set ::validation_param(udp_length) "dont_care"
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
        set ::validation_param(udp_checksum) $param(-udp_checksum)
    } else {

        set ::validation_param(udp_checksum) "dont_care"
    }

    #Transport Specific
    if {[info exists param(-transport_specific)]} {

        set transport_specific $param(-transport_specific)
        set ::validation_param(transport_specific) $param(-transport_specific)
    } else {

        set ::validation_param(transport_specific) "dont_care"
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        set ::validation_param(message_type) $param(-message_type)
    } else {

        set ::validation_param(message_type) 0
    }

    #PTP version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        set ::validation_param(version_ptp) $param(-version_ptp)
    } else {

        set ::validation_param(version_ptp) "dont_care"
    }

    #PTP Annouce message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        set ::validation_param(message_length) $param(-message_length)
    } else {

        set ::validation_param(message_length) "dont_care"
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        set ::validation_param(domain_number) $param(-domain_number)
    } else {

        set ::validation_param(domain_number) "dont_care"
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        set ::validation_param(alternate_master_flag) $param(-alternate_master_flag)
    } else {

        set ::validation_param(alternate_master_flag) "dont_care"
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        set ::validation_param(profile_specific1) $param(-profile_specific1)
    } else {

        set ::validation_param(profile_specific1) "dont_care"
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        set ::validation_param(profile_specific2) $param(-profile_specific2)
    } else {

        set ::validation_param(profile_specific2) "dont_care"
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        set ::validation_param(leap61) $param(-leap61)
    } else {

        set ::validation_param(leap61) "dont_care"
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        set ::validation_param(leap59) $param(-leap59)
    } else {

        set ::validation_param(leap59) "dont_care"
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        set ::validation_param(current_utc_offset_valid) $param(-current_utc_offset_valid)
    } else {

        set ::validation_param(current_utc_offset_valid) "dont_care"
    }

    #PTP Timescale Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        set ::validation_param(ptp_timescale) $param(-ptp_timescale)
    } else {

        set ::validation_param(ptp_timescale) "dont_care"
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        set ::validation_param(time_traceable) $param(-time_traceable)
    } else {

        set ::validation_param(time_traceable) "dont_care"
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        set ::validation_param(freq_traceable) $param(-freq_traceable)
    } else {

        set ::validation_param(freq_traceable) "dont_care"
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        set ::validation_param(sequence_id) $param(-sequence_id)
    } else {

        set ::validation_param(sequence_id) "dont_care"
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        set ::validation_param(control_field) $param(-control_field)
    } else {

        set ::validation_param(control_field) "dont_care"
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame timeout
    if {[info exists param(-timeout)]} {

        set timeout $param(-timeout)
    } else {

        set timeout 5
    }

    #OUTPUT PARAMETERS

    #Destination MAC dddress
    if {[info exists param(-recvd_dest_mac)]} {

        upvar $param(-recvd_dest_mac) recvd_dest_mac
        set recvd_dest_mac ""
    }

    #Source MAC address
    if {[info exists param(-recvd_src_mac)]} {

        upvar $param(-recvd_src_mac) recvd_src_mac
        set recvd_src_mac ""
    }

    #Two Step Flag
    if {[info exists param(-recvd_two_step_flag)]} {

        upvar $param(-recvd_two_step_flag) recvd_two_step_flag
        set recvd_two_step_flag ""
    }

    #Unicast Flag
    if {[info exists param(-recvd_unicast_flag)]} {

        upvar $param(-recvd_unicast_flag) recvd_unicast_flag
        set recvd_unicast_flag ""
    }

    #Correction Field value
    if {[info exists param(-recvd_correction_field)]} {

        upvar $param(-recvd_correction_field) recvd_correction_field
        set recvd_correction_field ""
    }

    #Source port number
    if {[info exists param(-recvd_src_port_number)]} {

        upvar $param(-recvd_src_port_number) recvd_src_port_number
        set recvd_src_port_number ""
    }

    #Source clock identity
    if {[info exists param(-recvd_src_clock_identity)]} {

        upvar $param(-recvd_src_clock_identity) recvd_src_clock_identity
        set recvd_src_clock_identity ""
    }

    #Sequence ID
    if {[info exists param(-recvd_sequence_id)]} {

        upvar $param(-recvd_sequence_id) recvd_sequence_id
        set recvd_sequence_id ""
    }

    #Log message interval
    if {[info exists param(-recvd_log_message_interval)]} {

        upvar $param(-recvd_log_message_interval) recvd_log_message_interval
        set recvd_log_message_interval ""
    }

    #Recvd Origin Timestamp Sec
    if {[info exists param(-recvd_origin_timestamp_sec)]} {

        upvar $param(-recvd_origin_timestamp_sec) recvd_origin_timestamp_sec
        set recvd_origin_timestamp_sec ""
    }

    #Recvd Origin Timestamp Ns
    if {[info exists param(-recvd_origin_timestamp_ns)]} {

        upvar $param(-recvd_origin_timestamp_ns) recvd_origin_timestamp_ns
        set recvd_origin_timestamp_ns ""
    }

    #Received packet timestamp in seconds
    if {[info exists param(-rx_timestamp_sec)]} {

        upvar $param(-rx_timestamp_sec) rx_timestamp_sec
        set rx_timestamp_sec ""
    }

    #Received packet timestamp in nanoseconds
    if {[info exists param(-rx_timestamp_ns)]} {

        upvar $param(-rx_timestamp_ns) rx_timestamp_ns
        set rx_timestamp_ns ""
    }

    #Error Reason
    if {[info exists param(-error_reason)]} {

        upvar $param(-error_reason) error_reason
        set error_reason ""
    }

    #LOGIC GOES HERE
    #Timeout in millisec
    set timeout [expr int($timeout*1000)]
    set error_code 0

    print_timeout $timeout

    set result [pltLib::recv_periodic\
            -session_id           $session_id\
            -port_num             $port_num\
            -filter_id            $filter_id\
            -timeout              $timeout\
            -reset_count          $reset_count\
            -validation_handler   "WRPTP"\
            -error_code           error_code\
            -rx_timestamp         rx_timestamp_sec\
            -rx_time_nanoseconds  rx_timestamp_ns\
            -pkt_content          pkt_content\
            -pkt_size             pkt_size]

    set error_reason [wrptp::tee_recv_error_reason $error_code]

    if {[info exists pkt_content]} {

        set hex $pkt_content

        pbLib::decode_wrptp_pkt\
                 -packet                         $hex\
                 -dest_mac                       recvd_dest_mac\
                 -src_mac                        recvd_src_mac\
                 -eth_type                       recvd_eth_type\
                 -vlan_id                        recvd_vlan_id\
                 -vlan_priority                  recvd_vlan_priority\
                 -vlan_dei                       recvd_vlan_dei\
                 -src_ip                         recvd_src_ip\
                 -dest_ip                        recvd_dest_ip\
                 -src_port                       recvd_src_port\
                 -dest_port                      recvd_dest_port\
                 -udp_length                     recvd_udp_length\
                 -udp_checksum                   recvd_udp_checksum\
                 -transport_specific             recvd_transport_specific\
                 -message_type                   recvd_message_type\
                 -version_ptp                    recvd_version_ptp\
                 -message_length                 recvd_message_length\
                 -domain_number                  recvd_domain_number\
                 -alternate_master_flag          recvd_alternate_master_flag\
                 -two_step_flag                  recvd_two_step_flag\
                 -unicast_flag                   recvd_unicast_flag\
                 -profile_specific1              recvd_profile_specific1\
                 -profile_specific2              recvd_profile_specific2\
                 -leap61                         recvd_leap61\
                 -leap59                         recvd_leap59\
                 -current_utc_offset_valid       recvd_current_utc_offset_valid\
                 -ptp_timescale                  recvd_ptp_timescale\
                 -time_traceable                 recvd_time_traceable\
                 -freq_traceable                 recvd_freq_traceable\
                 -correction_field               recvd_correction_field\
                 -src_port_number                recvd_src_port_number\
                 -src_clock_identity             recvd_src_clock_identity\
                 -sequence_id                    recvd_sequence_id\
                 -control_field                  recvd_control_field\
                 -log_message_interval           recvd_log_message_interval\
                 -origin_timestamp_sec           recvd_origin_timestamp_sec\
                 -origin_timestamp_ns            recvd_origin_timestamp_ns

    }

    return $result

}


################################################################################
#                                                                              #
#  PROCEDURE NAME         :  wrptp::recv_announce                              #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Reception port                 #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   filter_id                     : (Mandatory) Filter id                      #
#                                                                              #
#   reset_count                   : (Optional)  Reset Count                    #
#                                               (Default: ON)                  #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                                                              #
#   transport_specific            : (Optional)  Transport Specific             #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default: 11)                  #
#                                                                              #
#   version_ptp                   : (Optional)  PTP version number             #
#                                                                              #
#   message_length                : (Optional)  PTP Annouce message length     #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                                                              #
#   unicast_flag                  : (Optional)  Unicast Flag                   #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP Timescale Flag             #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                                                              #
#   log_message_interval          : (Optional)  Log Message Interval           #
#                                                                              #
#   current_utc_offset            : (Optional)  Current Utc Offset             #
#                                                                              #
#   steps_removed                 : (Optional)  Steps Removed                  #
#                                                                              #
#   time_source                   : (Optional)  Time Source                    #
#                                                                              #
#   gm_clock_class                : (Optional)  Gm Clock Class                 #
#                                                                              #
#   gm_clock_accuracy             : (Optional)  Gm Clock Accuracy              #
#                                                                              #
#   gm_clock_variance             : (Optional)  Gm Clock Variance              #
#                                                                              #
#   gm_priority1                  : (Optional)  Gm Priority1                   #
#                                                                              #
#   gm_priority2                  : (Optional)  Gm Priority2                   #
#                                                                              #
#  (PTP TLVs)                                                                  #
#                                                                              #
#   tlv_type                      : (Optional)  TLV Type                       #
#                                               (Default : -1)                 #
#                                                                              #
#   tlv_length                    : (Optional)  TLV length                     #
#                                               (Default : 8)                  #
#                                                                              #
#   organization_id               : (Optional) Organization id                 #
#                                               (Default: 0x080030)            #
#                                                                              #
#   magic_number                  : (Optional) Magic number                    #
#                                               (Default: 0xDEAD)              #
#                                                                              #
#   version_number                : (Optional) Version number                  #
#                                               (Default: 1)                   #
#                                                                              #
#   message_id                    : (Optional) Message id                      #
#                                               (Default: 0x2000)              #
#                                                                              #
#   wrconfig                      : (Optional) Wrconfig                        #
#                                               (Default: 0)                   #
#                                                                              #
#   calibrated                    : (Optional) Calibrated                      #
#                                               (Default: 0)                   #
#                                                                              #
#   wrmode_on                     : (Optional) WR Mode ON                      #
#                                               (Default: 0)                   #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default: 1)                   #
#                                                                              #
#   timeout                       : (Optional)  Frame timeout (in seconds)     #
#                                               (Default: 5)                   #
#                                                                              #
#  OUTPUT PARAMETERS      :                                                    #
#                                                                              #
#   recvd_dest_mac                : (Optional)  Destination MAC dddress.       #
#                                                                              #
#   recvd_src_mac                 : (Optional)  Source MAC address.            #
#                                                                              #
#   recvd_two_step_flag           : (Optional)  Two Step Flag                  #
#                                                                              #
#   recvd_unicast_flag            : (Optional)  Unicast Flag                   #
#                                                                              #
#   recvd_correction_field        : (Optional)  Correction Field value         #
#                                                                              #
#   recvd_src_port_number        : (Optional)  Source port number              #
#                                                                              #
#   recvd_src_clock_identity      : (Optional)  Source clock identity          #
#                                                                              #
#   recvd_sequence_id             : (Optional)  Sequence ID                    #
#                                                                              #
#   recvd_log_message_interval    : (Optional)  Log message interval           #
#                                                                              #
#   recvd_origin_timestamp_sec    : (Optional)  Recvd Origin Timestamp Sec     #
#                                                                              #
#   recvd_origin_timestamp_ns     : (Optional)  Recvd Origin Timestamp Ns      #
#                                                                              #
#   recvd_gm_priority1            : (Optional)  Recvd Gm Priority1             #
#                                                                              #
#   recvd_gm_priority2            : (Optional)  Recvd Gm Priority2             #
#                                                                              #
#   recvd_gm_identity             : (Optional)  Recvd Gm Identity              #
#                                                                              #
#   rx_timestamp_sec              : (Optional)  Packet's rx time in seconds    #
#                                                                              #
#   rx_timestamp_ns               : (Optional)  Packet's rx time in ns         #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is received as         #
#                                         expected)                            #
#                                                                              #
#                         : 0 on failure (If the packet is not received        #
#                                         as expected)                         #
#                                                                              #
#  DEFINITION             : This function is used to receive                   #
#                           PTP announce message                               #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       wrptp::recv_announce\                                                  #
#           -port_num                         $port_num\                       #
#           -filter_id                        $filter_id\                      #
#           -reset_count                      $reset_count\                    #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -transport_specific               $transport_specific\             #
#           -message_type                     $message_type\                   #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -unicast_flag                     $unicast_flag\                   #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -control_field                    $control_field\                  #
#           -log_message_interval             $log_message_interval\           #
#           -gm_priority1                     $gm_priority1\                   #
#           -gm_priority2                     $gm_priority2\                   #
#           -current_utc_offset               $current_utc_offset\             #
#           -steps_removed                    $steps_removed\                  #
#           -time_source                      $time_source\                    #
#           -gm_clock_class                   $gm_clock_class\                 #
#           -gm_clock_accuracy                $gm_clock_accuracy\              #
#           -gm_clock_variance                $gm_clock_variance\              #
#           -tlv_type                         $tlv_type\                       #
#           -tlv_length                       $tlv_length\                     #
#           -organization_id                  $organization_id\                #
#           -magic_number                     $magic_number\                   #
#           -version_number                   $version_number\                 #
#           -message_id                       $message_id\                     #
#           -wrconfig                         $wrconfig\                       #
#           -calibrated                       $calibrated\                     #
#           -wrmode_on                        $wrmode_on\                      #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#           -recvd_dest_mac                   recvd_dest_mac\                  #
#           -recvd_src_mac                    recvd_src_mac\                   #
#           -recvd_two_step_flag              recvd_two_step_flag\             #
#           -recvd_unicast_flag               recvd_unicast_flag\              #
#           -recvd_correction_field           recvd_correction_field\          #
#           -recvd_src_port_number            recvd_src_port_identity\         #
#           -recvd_src_clock_identity         recvd_src_clock_identity\        #
#           -recvd_sequence_id                recvd_sequence_id\               #
#           -recvd_log_message_interval       recvd_message_interval\          #
#           -recvd_origin_timestamp_sec       recvd_origin_timestamp_sec\      #
#           -recvd_origin_timestamp_ns        recvd_origin_timestamp_ns\       #
#           -recvd_gm_priority1               recvd_gm_priority1\              #
#           -recvd_gm_priority2               recvd_gm_priority2\              #
#           -recvd_gm_identity                recvd_gm_identity\               #
#           -recvd_tlv_type                   recvd_tlv_type\                  #
#           -recvd_tlv_length                 recvd_tlv_length\                #
#           -recvd_organization_id            recvd_organization_id\           #
#           -recvd_magic_number               recvd_magic_number\              #
#           -recvd_version_number             recvd_version_number\            #
#           -recvd_message_id                 recvd_message_id\                #
#           -recvd_wrconfig                   recvd_wrconfig\                  #
#           -recvd_calibrated                 recvd_calibrated\                #
#           -recvd_wrmode_on                  recvd_wrmode_on\                 #
#           -rx_timestamp_sec                 rx_timestamp_sec\                #
#           -rx_timestamp_ns                  rx_timestamp_ns                  #
#                                                                              #
################################################################################
proc wrptp::recv_announce {args} {

    array unset ::validation_param

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "wrptp::recv_announce: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #Reception port
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "wrptp::recv_announce: Missing Argument -> port_num"
        return 0
    }

    #Filter id
    if {[info exists param(-filter_id)]} {

        set filter_id $param(-filter_id)
    } else {

        ERRLOG -msg "wrptp::recv_announce: Missing Argument -> filter_id"
        return 0
    }

    #Reset Count
    if {[info exists param(-reset_count)]} {

        set reset_count $param(-reset_count)
    } else {

        set reset_count ON
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        set ::validation_param(eth_type) $param(-eth_type)
    } else {

        if { $::wrptp_vlan_encap == "true" } {
            set ::validation_param(eth_type) 8100
        } else {
            if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
                set ::validation_param(eth_type) 0800
            } else {
                set ::validation_param(eth_type) 88F7
            }
        }
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
        set ::validation_param(vlan_id) $param(-vlan_id)
    } else {

        if { $::wrptp_vlan_encap == "true" } {
            set ::validation_param(vlan_id) $::wrptp_vlan_id
        } else {
            set ::validation_param(vlan_id) "dont_care"
        }
    }

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
        set ::validation_param(dest_ip) $param(-dest_ip)
    } else {

        set ::validation_param(dest_ip) "dont_care"
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
        set ::validation_param(src_ip) $param(-src_ip)
    } else {

        set ::validation_param(src_ip) "dont_care"
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
        set ::validation_param(ip_checksum) $param(-ip_checksum)
    } else {

        set ::validation_param(ip_checksum) "dont_care"
    }

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
        set ::validation_param(dest_port) $param(-dest_port)
    } else {

        set ::validation_param(dest_port) "dont_care"
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
        set ::validation_param(src_port) $param(-src_port)
    } else {

        set ::validation_param(src_port) "dont_care"
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
        set ::validation_param(udp_length) $param(-udp_length)
    } else {

        set ::validation_param(udp_length) "dont_care"
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
        set ::validation_param(udp_checksum) $param(-udp_checksum)
    } else {

        set ::validation_param(udp_checksum) "dont_care"
    }

    #Transport Specific
    if {[info exists param(-transport_specific)]} {

        set transport_specific $param(-transport_specific)
        set ::validation_param(transport_specific) $param(-transport_specific)
    } else {

        set ::validation_param(transport_specific) "dont_care"
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        set ::validation_param(message_type) $param(-message_type)
    } else {

        set ::validation_param(message_type) 11
    }

    #PTP version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        set ::validation_param(version_ptp) $param(-version_ptp)
    } else {

        set ::validation_param(version_ptp) "dont_care"
    }

    #PTP Annouce message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        set ::validation_param(message_length) $param(-message_length)
    } else {

        set ::validation_param(message_length) "dont_care"
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        set ::validation_param(domain_number) $param(-domain_number)
    } else {

        set ::validation_param(domain_number) "dont_care"
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        set ::validation_param(alternate_master_flag) $param(-alternate_master_flag)
    } else {

        set ::validation_param(alternate_master_flag) "dont_care"
    }

    #Unicast flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
        set ::validation_param(unicast_flag) $param(-unicast_flag)
    } else {

        set ::validation_param(unicast_flag) "dont_care"
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        set ::validation_param(profile_specific1) $param(-profile_specific1)
    } else {

        set ::validation_param(profile_specific1) "dont_care"
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        set ::validation_param(profile_specific2) $param(-profile_specific2)
    } else {

        set ::validation_param(profile_specific2) "dont_care"
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        set ::validation_param(leap61) $param(-leap61)
    } else {

        set ::validation_param(leap61) "dont_care"
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        set ::validation_param(leap59) $param(-leap59)
    } else {

        set ::validation_param(leap59) "dont_care"
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        set ::validation_param(current_utc_offset_valid) $param(-current_utc_offset_valid)
    } else {

        set ::validation_param(current_utc_offset_valid) "dont_care"
    }

    #PTP Timescale Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        set ::validation_param(ptp_timescale) $param(-ptp_timescale)
    } else {

        set ::validation_param(ptp_timescale) "dont_care"
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        set ::validation_param(time_traceable) $param(-time_traceable)
    } else {

        set ::validation_param(time_traceable) "dont_care"
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        set ::validation_param(freq_traceable) $param(-freq_traceable)
    } else {

        set ::validation_param(freq_traceable) "dont_care"
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        set ::validation_param(sequence_id) $param(-sequence_id)
    } else {

        set ::validation_param(sequence_id) "dont_care"
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        set ::validation_param(control_field) $param(-control_field)
    } else {

        set ::validation_param(control_field) "dont_care"
    }

    #Log Message Interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
        set ::validation_param(log_message_interval) $param(-log_message_interval)
    } else {

        set ::validation_param(log_message_interval) "dont_care"
    }

    #Current Utc Offset
    if {[info exists param(-current_utc_offset)]} {

        set current_utc_offset $param(-current_utc_offset)
        set ::validation_param(current_utc_offset) $param(-current_utc_offset)
    } else {

        set ::validation_param(current_utc_offset) "dont_care"
    }

    #Steps Removed
    if {[info exists param(-steps_removed)]} {

        set steps_removed $param(-steps_removed)
        set ::validation_param(steps_removed) $param(-steps_removed)
    } else {

        set ::validation_param(steps_removed) "dont_care"
    }

    #Time Source
    if {[info exists param(-time_source)]} {

        set time_source $param(-time_source)
        set ::validation_param(time_source) $param(-time_source)
    } else {

        set ::validation_param(time_source) "dont_care"
    }

    #Gm Clock Class
    if {[info exists param(-gm_clock_class)]} {

        set gm_clock_class $param(-gm_clock_class)
        set ::validation_param(gm_clock_class) $param(-gm_clock_class)
    } else {

        set ::validation_param(gm_clock_class) "dont_care"
    }

    #Gm Clock Accuracy
    if {[info exists param(-gm_clock_accuracy)]} {

        set gm_clock_accuracy $param(-gm_clock_accuracy)
        set ::validation_param(gm_clock_accuracy) $param(-gm_clock_accuracy)
    } else {

        set ::validation_param(gm_clock_accuracy) "dont_care"
    }

    #Gm Clock Variance
    if {[info exists param(-gm_clock_variance)]} {

        set gm_clock_variance $param(-gm_clock_variance)
        set ::validation_param(gm_clock_variance) $param(-gm_clock_variance)
    } else {

        set ::validation_param(gm_clock_variance) "dont_care"
    }

    #Grandmaster priority 1
    if {[info exists param(-gm_priority1)]} {

        set gm_priority1 $param(-gm_priority1)
        set ::validation_param(gm_priority1) $param(-gm_priority1)
    } else {

        set ::validation_param(gm_priority1) "dont_care"
    }

    #Grandmaster priority 2
    if {[info exists param(-gm_priority2)]} {

        set gm_priority2 $param(-gm_priority2)
        set ::validation_param(gm_priority2) $param(-gm_priority2)
    } else {

        set ::validation_param(gm_priority2) "dont_care"
    }

    if {[info exists param(-tlv_type)]} {

        set tlv_type $param(-tlv_type)
        set ::validation_param(tlv_type) $param(-tlv_type)
    } else {

        set ::validation_param(tlv_type) "dont_care"
    }

    if {[info exists param(-tlv_length)]} {

        set tlv_length $param(-tlv_length)
        set ::validation_param(tlv_length) $param(-tlv_length)
    } else {

        set ::validation_param(tlv_length) "dont_care"
    }

    if {[info exists param(-organization_id)]} {

        set organization_id $param(-organization_id)
        set ::validation_param(organization_id) $param(-organization_id)
    } else {

        set ::validation_param(organization_id) "dont_care"
    }

    if {[info exists param(-magic_number)]} {

        set magic_number $param(-magic_number)
        set ::validation_param(magic_number) $param(-magic_number)
    } else {

        set ::validation_param(magic_number) "dont_care"
    }

    if {[info exists param(-version_number)]} {

        set version_number $param(-version_number)
        set ::validation_param(version_number) $param(-version_number)
    } else {

        set ::validation_param(version_number) "dont_care"
    }

    if {[info exists param(-message_id)]} {

        set message_id $param(-message_id)
        set ::validation_param(message_id) $param(-message_id)
    } else {

        set ::validation_param(message_id) "dont_care"
    }

    if {[info exists param(-wrconfig)]} {

        set wrconfig $param(-wrconfig)
        set ::validation_param(wrconfig) $param(-wrconfig)
    } else {

        set ::validation_param(wrconfig) "dont_care"
    }

    if {[info exists param(-calibrated)]} {

        set calibrated $param(-calibrated)
        set ::validation_param(calibrated) $param(-calibrated)
    } else {

        set ::validation_param(calibrated) "dont_care"
    }

    if {[info exists param(-wrmode_on)]} {

        set wrmode_on $param(-wrmode_on)
        set ::validation_param(wrmode_on) $param(-wrmode_on)
    } else {

        set ::validation_param(wrmode_on) "dont_care"
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame timeout
    if {[info exists param(-timeout)]} {

        set timeout $param(-timeout)
    } else {

        set timeout 5
    }

    #OUTPUT PARAMETERS
    #all the fields in the matched packet
    foreach key [array names param] {

        set var [string trimleft $key -]

        if [regexp {^recvd_.*} $var] {

            upvar [set param($key)] $var
            set $var ""
        }
    }

    #Received packet timestamp in seconds
    if {[info exists param(-rx_timestamp_sec)]} {

        upvar $param(-rx_timestamp_sec) rx_timestamp_sec
        set rx_timestamp_sec ""
    }

    #Received packet timestamp in nanoseconds
    if {[info exists param(-rx_timestamp_ns)]} {

        upvar $param(-rx_timestamp_ns) rx_timestamp_ns
        set rx_timestamp_ns ""
    }

    #Error Reason
    if {[info exists param(-error_reason)]} {

        upvar $param(-error_reason) error_reason
        set error_reason ""
    }

    #LOGIC GOES HERE
    #Timeout in millisec
    set timeout [expr int($timeout*1000)]
    set error_code 0

    print_timeout $timeout

    set result [pltLib::recv_periodic\
            -session_id           $session_id\
            -port_num             $port_num\
            -filter_id            $filter_id\
            -timeout              $timeout\
            -reset_count          $reset_count\
            -validation_handler   "WRPTP"\
            -error_code           error_code\
            -rx_timestamp         rx_timestamp_sec\
            -rx_time_nanoseconds  rx_timestamp_ns\
            -pkt_content          pkt_content\
            -pkt_size             pkt_size]

    set error_reason [wrptp::tee_recv_error_reason $error_code]

    if {[info exists pkt_content]} {

        set hex $pkt_content

        pbLib::decode_wrptp_pkt\
                 -packet                         $hex\
                 -dest_mac                       recvd_dest_mac\
                 -src_mac                        recvd_src_mac\
                 -eth_type                       recvd_eth_type\
                 -vlan_id                        recvd_vlan_id\
                 -vlan_priority                  recvd_vlan_priority\
                 -vlan_dei                       recvd_vlan_dei\
                 -src_ip                         recvd_src_ip\
                 -dest_ip                        recvd_dest_ip\
                 -src_port                       recvd_src_port\
                 -dest_port                      recvd_dest_port\
                 -udp_length                     recvd_udp_length\
                 -udp_checksum                   recvd_udp_checksum\
                 -transport_specific             recvd_transport_specific\
                 -message_type                   recvd_message_type\
                 -version_ptp                    recvd_version_ptp\
                 -message_length                 recvd_message_length\
                 -domain_number                  recvd_domain_number\
                 -alternate_master_flag          recvd_alternate_master_flag\
                 -two_step_flag                  recvd_two_step_flag\
                 -unicast_flag                   recvd_unicast_flag\
                 -profile_specific1              recvd_profile_specific1\
                 -profile_specific2              recvd_profile_specific2\
                 -leap61                         recvd_leap61\
                 -leap59                         recvd_leap59\
                 -current_utc_offset_valid       recvd_current_utc_offset_valid\
                 -ptp_timescale                  recvd_ptp_timescale\
                 -time_traceable                 recvd_time_traceable\
                 -freq_traceable                 recvd_freq_traceable\
                 -correction_field               recvd_correction_field\
                 -src_port_number                recvd_src_port_number\
                 -src_clock_identity             recvd_src_clock_identity\
                 -sequence_id                    recvd_sequence_id\
                 -control_field                  recvd_control_field\
                 -log_message_interval           recvd_log_message_interval\
                 -origin_timestamp_sec           recvd_origin_timestamp_sec\
                 -origin_timestamp_ns            recvd_origin_timestamp_ns\
                 -current_utc_offset             recvd_current_utc_offset\
                 -gm_priority1                   recvd_gm_priority1\
                 -gm_priority2                   recvd_gm_priority2\
                 -gm_identity                    recvd_gm_identity\
                 -steps_removed                  recvd_steps_removed\
                 -time_source                    recvd_time_source\
                 -gm_clock_class                 recvd_gm_clock_class\
                 -gm_clock_accuracy              recvd_gm_clock_accuracy\
                 -gm_clock_variance              recvd_gm_clock_variance\
                 -tlv_type                       recvd_tlv_type\
                 -tlv_length                     recvd_tlv_length\
                 -organization_id                recvd_organization_id\
                 -magic_number                   recvd_magic_number\
                 -version_number                 recvd_version_number\
                 -message_id                     recvd_message_id\
                 -wrconfig                       recvd_wrconfig\
                 -calibrated                     recvd_calibrated\
                 -wrmode_on                      recvd_wrmode_on

    }

    return $result

}


################################################################################
#                                                                              #
#  PROCEDURE NAME         :  wrptp::recv_delay_req                             #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Reception port                 #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   filter_id                     : (Mandatory) Filter id                      #
#                                                                              #
#   reset_count                   : (Optional)  Reset Count                    #
#                                               (Default: ON)                  #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                                                              #
#   transport_specific            : (Optional)  Transport Specific             #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default: 1)                   #
#                                                                              #
#   version_ptp                   : (Optional)  PTP version number             #
#                                                                              #
#   message_length                : (Optional)  PTP Annouce message length     #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP Timescale Flag             #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default: 1)                   #
#                                                                              #
#   timeout                       : (Optional)  Frame timeout (in seconds)     #
#                                               (Default: 5)                   #
#                                                                              #
#  OUTPUT PARAMETERS      :                                                    #
#                                                                              #
#   recvd_dest_mac                : (Optional)  Destination MAC dddress.       #
#                                                                              #
#   recvd_src_mac                 : (Optional)  Source MAC address.            #
#                                                                              #
#   recvd_two_step_flag           : (Optional)  Two Step Flag                  #
#                                                                              #
#   recvd_unicast_flag            : (Optional)  Unicast Flag                   #
#                                                                              #
#   recvd_correction_field        : (Optional)  Correction Field value         #
#                                                                              #
#   recvd_src_port_number         : (Optional)  Source port number             #
#                                                                              #
#   recvd_src_clock_identity      : (Optional)  Source clock identity          #
#                                                                              #
#   recvd_sequence_id             : (Optional)  Sequence ID                    #
#                                                                              #
#   recvd_log_message_interval    : (Optional)  Log message interval           #
#                                                                              #
#   recvd_origin_timestamp_sec    : (Optional)  Recvd Origin Timestamp Sec     #
#                                                                              #
#   recvd_origin_timestamp_ns     : (Optional)  Recvd Origin Timestamp Ns      #
#                                                                              #
#   rx_timestamp_sec              : (Optional)  Packet's rx time in seconds    #
#                                                                              #
#   rx_timestamp_ns               : (Optional)  Packet's rx time in ns         #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is received as         #
#                                         expected)                            #
#                                                                              #
#                         : 0 on failure (If the packet is not received        #
#                                         as expected)                         #
#                                                                              #
#  DEFINITION             : This function is used to receive                   #
#                           PTP delay req message                              #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       wrptp::recv_delay_req\                                                 #
#           -port_num                         $port_num\                       #
#           -filter_id                        $filter_id\                      #
#           -reset_count                      $reset_count\                    #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -transport_specific               $transport_specific\             #
#           -message_type                     $message_type\                   #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -control_field                    $control_field\                  #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#           -recvd_dest_mac                   recvd_dest_mac\                  #
#           -recvd_src_mac                    recvd_src_mac\                   #
#           -recvd_two_step_flag              recvd_two_step_flag\             #
#           -recvd_unicast_flag               recvd_unicast_flag\              #
#           -recvd_correction_field           recvd_correction_field\          #
#           -recvd_src_port_number            recvd_src_port_identity\         #
#           -recvd_src_clock_identity         recvd_src_clock_identity\        #
#           -recvd_sequence_id                recvd_sequence_id\               #
#           -recvd_log_message_interval       recvd_message_interval\          #
#           -recvd_origin_timestamp_sec       recvd_origin_timestamp_sec\      #
#           -recvd_origin_timestamp_ns        recvd_origin_timestamp_ns\       #
#           -rx_timestamp_sec                 rx_timestamp_sec\                #
#           -rx_timestamp_ns                  rx_timestamp_ns                  #
#                                                                              #
################################################################################
proc wrptp::recv_delay_req {args} {

    array unset ::validation_param

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "wrptp::recv_delay_req: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #Reception port
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "wrptp::recv_delay_req: Missing Argument -> port_num"
        return 0
    }

    #Filter id
    if {[info exists param(-filter_id)]} {

        set filter_id $param(-filter_id)
    } else {

        ERRLOG -msg "wrptp::recv_delay_req: Missing Argument -> filter_id"
        return 0
    }

    #Reset Count
    if {[info exists param(-reset_count)]} {

        set reset_count $param(-reset_count)
    } else {

        set reset_count ON
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        set ::validation_param(eth_type) $param(-eth_type)
    } else {

        if { $::wrptp_vlan_encap == "true" } {
            set ::validation_param(eth_type) 8100
        } else {
            if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
                set ::validation_param(eth_type) 0800
            } else {
                set ::validation_param(eth_type) 88F7
            }
        }
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
        set ::validation_param(vlan_id) $param(-vlan_id)
    } else {

        if { $::wrptp_vlan_encap == "true" } {
            set ::validation_param(vlan_id) $::wrptp_vlan_id
        } else {
            set ::validation_param(vlan_id) "dont_care"
        }
    }

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
        set ::validation_param(dest_ip) $param(-dest_ip)
    } else {

        set ::validation_param(dest_ip) "dont_care"
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
        set ::validation_param(src_ip) $param(-src_ip)
    } else {

        set ::validation_param(src_ip) "dont_care"
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
        set ::validation_param(ip_checksum) $param(-ip_checksum)
    } else {

        set ::validation_param(ip_checksum) "dont_care"
    }

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
        set ::validation_param(dest_port) $param(-dest_port)
    } else {

        set ::validation_param(dest_port) "dont_care"
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
        set ::validation_param(src_port) $param(-src_port)
    } else {

        set ::validation_param(src_port) "dont_care"
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
        set ::validation_param(udp_length) $param(-udp_length)
    } else {

        set ::validation_param(udp_length) "dont_care"
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
        set ::validation_param(udp_checksum) $param(-udp_checksum)
    } else {

        set ::validation_param(udp_checksum) "dont_care"
    }

    #Transport Specific
    if {[info exists param(-transport_specific)]} {

        set transport_specific $param(-transport_specific)
        set ::validation_param(transport_specific) $param(-transport_specific)
    } else {

        set ::validation_param(transport_specific) "dont_care"
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        set ::validation_param(message_type) $param(-message_type)
    } else {

        set ::validation_param(message_type) 1
    }

    #PTP version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        set ::validation_param(version_ptp) $param(-version_ptp)
    } else {

        set ::validation_param(version_ptp) "dont_care"
    }

    #PTP Annouce message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        set ::validation_param(message_length) $param(-message_length)
    } else {

        set ::validation_param(message_length) "dont_care"
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        set ::validation_param(domain_number) $param(-domain_number)
    } else {

        set ::validation_param(domain_number) "dont_care"
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        set ::validation_param(alternate_master_flag) $param(-alternate_master_flag)
    } else {

        set ::validation_param(alternate_master_flag) "dont_care"
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        set ::validation_param(profile_specific1) $param(-profile_specific1)
    } else {

        set ::validation_param(profile_specific1) "dont_care"
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        set ::validation_param(profile_specific2) $param(-profile_specific2)
    } else {

        set ::validation_param(profile_specific2) "dont_care"
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        set ::validation_param(leap61) $param(-leap61)
    } else {

        set ::validation_param(leap61) "dont_care"
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        set ::validation_param(leap59) $param(-leap59)
    } else {

        set ::validation_param(leap59) "dont_care"
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        set ::validation_param(current_utc_offset_valid) $param(-current_utc_offset_valid)
    } else {

        set ::validation_param(current_utc_offset_valid) "dont_care"
    }

    #PTP Timescale Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        set ::validation_param(ptp_timescale) $param(-ptp_timescale)
    } else {

        set ::validation_param(ptp_timescale) "dont_care"
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        set ::validation_param(time_traceable) $param(-time_traceable)
    } else {

        set ::validation_param(time_traceable) "dont_care"
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        set ::validation_param(freq_traceable) $param(-freq_traceable)
    } else {

        set ::validation_param(freq_traceable) "dont_care"
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        set ::validation_param(sequence_id) $param(-sequence_id)
    } else {

        set ::validation_param(sequence_id) "dont_care"
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        set ::validation_param(control_field) $param(-control_field)
    } else {

        set ::validation_param(control_field) "dont_care"
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame timeout
    if {[info exists param(-timeout)]} {

        set timeout $param(-timeout)
    } else {

        set timeout 5
    }

    #OUTPUT PARAMETERS

    #Destination MAC dddress
    if {[info exists param(-recvd_dest_mac)]} {

        upvar $param(-recvd_dest_mac) recvd_dest_mac
        set recvd_dest_mac ""
    }

    #Source MAC address
    if {[info exists param(-recvd_src_mac)]} {

        upvar $param(-recvd_src_mac) recvd_src_mac
        set recvd_src_mac ""
    }

    #Two Step Flag
    if {[info exists param(-recvd_two_step_flag)]} {

        upvar $param(-recvd_two_step_flag) recvd_two_step_flag
        set recvd_two_step_flag ""
    }

    #Sequence ID
    if {[info exists param(-recvd_sequence_id)]} {

        upvar $param(-recvd_sequence_id) recvd_sequence_id
        set recvd_sequence_id ""
    }

    #Unicast Flag
    if {[info exists param(-recvd_unicast_flag)]} {

        upvar $param(-recvd_unicast_flag) recvd_unicast_flag
        set recvd_unicast_flag ""
    }

    #Correction Field value
    if {[info exists param(-recvd_correction_field)]} {

        upvar $param(-recvd_correction_field) recvd_correction_field
        set recvd_correction_field ""
    }

    #Source port number
    if {[info exists param(-recvd_src_port_number)]} {

        upvar $param(-recvd_src_port_number) recvd_src_port_number
        set recvd_src_port_number ""
    }

    #Source clock identity
    if {[info exists param(-recvd_src_clock_identity)]} {

        upvar $param(-recvd_src_clock_identity) recvd_src_clock_identity
        set recvd_src_clock_identity ""
    }

    #Sequence ID
    if {[info exists param(-recvd_sequence_id)]} {

        upvar $param(-recvd_sequence_id) recvd_sequence_id
        set recvd_sequence_id ""
    }

    #Log message interval
    if {[info exists param(-recvd_log_message_interval)]} {

        upvar $param(-recvd_log_message_interval) recvd_log_message_interval
        set recvd_log_message_interval ""
    }

    #Recvd Origin Timestamp Sec
    if {[info exists param(-recvd_origin_timestamp_sec)]} {

        upvar $param(-recvd_origin_timestamp_sec) recvd_origin_timestamp_sec
        set recvd_origin_timestamp_sec ""
    }

    #Recvd Origin Timestamp Ns
    if {[info exists param(-recvd_origin_timestamp_ns)]} {

        upvar $param(-recvd_origin_timestamp_ns) recvd_origin_timestamp_ns
        set recvd_origin_timestamp_ns ""
    }

    #Received packet timestamp in seconds
    if {[info exists param(-rx_timestamp_sec)]} {

        upvar $param(-rx_timestamp_sec) rx_timestamp_sec
        set rx_timestamp_sec ""
    }

    #Received packet timestamp in nanoseconds
    if {[info exists param(-rx_timestamp_ns)]} {

        upvar $param(-rx_timestamp_ns) rx_timestamp_ns
        set rx_timestamp_ns ""
    }

    #Error Reason
    if {[info exists param(-error_reason)]} {

        upvar $param(-error_reason) error_reason
        set error_reason ""
    }

    #LOGIC GOES HERE
    #Timeout in millisec
    set timeout [expr int($timeout*1000)]
    set error_code 0

    print_timeout $timeout

    set result [pltLib::recv_periodic\
            -session_id           $session_id\
            -port_num             $port_num\
            -filter_id            $filter_id\
            -timeout              $timeout\
            -reset_count          $reset_count\
            -validation_handler   "WRPTP"\
            -error_code           error_code\
            -rx_timestamp         rx_timestamp_sec\
            -rx_time_nanoseconds  rx_timestamp_ns\
            -pkt_content          pkt_content\
            -pkt_size             pkt_size]

    set error_reason [wrptp::tee_recv_error_reason $error_code]

    if {[info exists pkt_content]} {

        set hex $pkt_content

        pbLib::decode_wrptp_pkt\
                 -packet                         $hex\
                 -dest_mac                       recvd_dest_mac\
                 -src_mac                        recvd_src_mac\
                 -eth_type                       recvd_eth_type\
                 -vlan_id                        recvd_vlan_id\
                 -vlan_priority                  recvd_vlan_priority\
                 -vlan_dei                       recvd_vlan_dei\
                 -src_ip                         recvd_src_ip\
                 -dest_ip                        recvd_dest_ip\
                 -src_port                       recvd_src_port\
                 -dest_port                      recvd_dest_port\
                 -udp_length                     recvd_udp_length\
                 -udp_checksum                   recvd_udp_checksum\
                 -transport_specific             recvd_transport_specific\
                 -message_type                   recvd_message_type\
                 -version_ptp                    recvd_version_ptp\
                 -message_length                 recvd_message_length\
                 -domain_number                  recvd_domain_number\
                 -alternate_master_flag          recvd_alternate_master_flag\
                 -two_step_flag                  recvd_two_step_flag\
                 -unicast_flag                   recvd_unicast_flag\
                 -profile_specific1              recvd_profile_specific1\
                 -profile_specific2              recvd_profile_specific2\
                 -leap61                         recvd_leap61\
                 -leap59                         recvd_leap59\
                 -current_utc_offset_valid       recvd_current_utc_offset_valid\
                 -ptp_timescale                  recvd_ptp_timescale\
                 -time_traceable                 recvd_time_traceable\
                 -freq_traceable                 recvd_freq_traceable\
                 -correction_field               recvd_correction_field\
                 -src_port_number                recvd_src_port_number\
                 -src_clock_identity             recvd_src_clock_identity\
                 -sequence_id                    recvd_sequence_id\
                 -control_field                  recvd_control_field\
                 -log_message_interval           recvd_log_message_interval\
                 -origin_timestamp_sec           recvd_origin_timestamp_sec\
                 -origin_timestamp_ns            recvd_origin_timestamp_ns

    }

    return $result

}


################################################################################
#                                                                              #
#  PROCEDURE NAME         :  wrptp::recv_delay_resp                            #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Reception port                 #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   filter_id                     : (Mandatory) Filter id                      #
#                                                                              #
#   reset_count                   : (Optional)  Reset Count                    #
#                                               (Default: ON)                  #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                                                              #
#   transport_specific            : (Optional)  Transport Specific             #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default: 9)                   #
#                                                                              #
#   version_ptp                   : (Optional)  PTP version number             #
#                                                                              #
#   message_length                : (Optional)  PTP Annouce message length     #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP Timescale Flag             #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default: 1)                   #
#                                                                              #
#   timeout                       : (Optional)  Frame timeout (in seconds)     #
#                                               (Default: 5)                   #
#                                                                              #
#  OUTPUT PARAMETERS      :                                                    #
#                                                                              #
#   recvd_dest_mac                : (Optional)  Destination MAC dddress.       #
#                                                                              #
#   recvd_src_mac                 : (Optional)  Source MAC address.            #
#                                                                              #
#   recvd_two_step_flag           : (Optional)  Two Step Flag                  #
#                                                                              #
#   recvd_unicast_flag            : (Optional)  Unicast Flag                   #
#                                                                              #
#   recvd_correction_field        : (Optional)  Correction Field value         #
#                                                                              #
#   recvd_src_port_number         : (Optional)  Source port number             #
#                                                                              #
#   recvd_src_clock_identity      : (Optional)  Source clock identity          #
#                                                                              #
#   recvd_sequence_id             : (Optional)  Sequence ID                    #
#                                                                              #
#   recvd_log_message_interval    : (Optional)  Log message interval           #
#                                                                              #
#   recvd_receive_timestamp_sec   : (Optional)  Recvd Receive Timestamp Sec    #
#                                                                              #
#   recvd_receive_timestamp_ns    : (Optional)  Recvd Receive Timestamp Ns     #
#                                                                              #
#   recvd_requesting_clock_identity: (Optional)  Recvd Requesting Clock Identity #
#                                                                              #
#   recvd_requesting_port_number  : (Optional)  Recvd Requesting Port Number   #
#                                                                              #
#   rx_timestamp_sec              : (Optional)  Packet's rx time in seconds    #
#                                                                              #
#   rx_timestamp_ns               : (Optional)  Packet's rx time in ns         #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is received as         #
#                                         expected)                            #
#                                                                              #
#                         : 0 on failure (If the packet is not received        #
#                                         as expected)                         #
#                                                                              #
#  DEFINITION             : This function is used to receive                   #
#                           PTP delay resp message                             #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       wrptp::recv_delay_resp\                                                #
#           -port_num                         $port_num\                       #
#           -filter_id                        $filter_id\                      #
#           -reset_count                      $reset_count\                    #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -transport_specific               $transport_specific\             #
#           -message_type                     $message_type\                   #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -control_field                    $control_field\                  #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#           -recvd_dest_mac                   recvd_dest_mac\                  #
#           -recvd_src_mac                    recvd_src_mac\                   #
#           -recvd_two_step_flag              recvd_two_step_flag\             #
#           -recvd_unicast_flag               recvd_unicast_flag\              #
#           -recvd_correction_field           recvd_correction_field\          #
#           -recvd_src_port_number            recvd_src_port_identity\         #
#           -recvd_src_clock_identity         recvd_src_clock_identity\        #
#           -recvd_sequence_id                recvd_sequence_id\               #
#           -recvd_log_message_interval       recvd_message_interval\          #
#           -recvd_receive_timestamp_sec      recvd_receive_timestamp_sec\     #
#           -recvd_receive_timestamp_ns       recvd_receive_timestamp_ns\      #
#           -recvd_requesting_clock_identity  recvd_requesting_clock_identity\ #
#           -recvd_requesting_port_number     recvd_requesting_port_number\    #
#           -rx_timestamp_sec                 rx_timestamp_sec\                #
#           -rx_timestamp_ns                  rx_timestamp_ns                  #
#                                                                              #
################################################################################
proc wrptp::recv_delay_resp {args} {

    array unset ::validation_param

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "wrptp::recv_delay_resp: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #Reception port
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "wrptp::recv_delay_resp: Missing Argument -> port_num"
        return 0
    }

    #Filter id
    if {[info exists param(-filter_id)]} {

        set filter_id $param(-filter_id)
    } else {

        ERRLOG -msg "wrptp::recv_delay_resp: Missing Argument -> filter_id"
        return 0
    }

    #Reset Count
    if {[info exists param(-reset_count)]} {

        set reset_count $param(-reset_count)
    } else {

        set reset_count ON
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        set ::validation_param(eth_type) $param(-eth_type)
    } else {

        if { $::wrptp_vlan_encap == "true" } {
            set ::validation_param(eth_type) 8100
        } else {
            if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
                set ::validation_param(eth_type) 0800
            } else {
                set ::validation_param(eth_type) 88F7
            }
        }
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
        set ::validation_param(vlan_id) $param(-vlan_id)
    } else {

        if { $::wrptp_vlan_encap == "true" } {
            set ::validation_param(vlan_id) $::wrptp_vlan_id
        } else {
            set ::validation_param(vlan_id) "dont_care"
        }
    }

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
        set ::validation_param(dest_ip) $param(-dest_ip)
    } else {

        set ::validation_param(dest_ip) "dont_care"
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
        set ::validation_param(src_ip) $param(-src_ip)
    } else {

        set ::validation_param(src_ip) "dont_care"
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
        set ::validation_param(ip_checksum) $param(-ip_checksum)
    } else {

        set ::validation_param(ip_checksum) "dont_care"
    }

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
        set ::validation_param(dest_port) $param(-dest_port)
    } else {

        set ::validation_param(dest_port) "dont_care"
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
        set ::validation_param(src_port) $param(-src_port)
    } else {

        set ::validation_param(src_port) "dont_care"
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
        set ::validation_param(udp_length) $param(-udp_length)
    } else {

        set ::validation_param(udp_length) "dont_care"
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
        set ::validation_param(udp_checksum) $param(-udp_checksum)
    } else {

        set ::validation_param(udp_checksum) "dont_care"
    }

    #Transport Specific
    if {[info exists param(-transport_specific)]} {

        set transport_specific $param(-transport_specific)
        set ::validation_param(transport_specific) $param(-transport_specific)
    } else {

        set ::validation_param(transport_specific) "dont_care"
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        set ::validation_param(message_type) $param(-message_type)
    } else {

        set ::validation_param(message_type) 9
    }

    #PTP version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        set ::validation_param(version_ptp) $param(-version_ptp)
    } else {

        set ::validation_param(version_ptp) "dont_care"
    }

    #PTP Annouce message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        set ::validation_param(message_length) $param(-message_length)
    } else {

        set ::validation_param(message_length) "dont_care"
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        set ::validation_param(domain_number) $param(-domain_number)
    } else {

        set ::validation_param(domain_number) "dont_care"
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        set ::validation_param(alternate_master_flag) $param(-alternate_master_flag)
    } else {

        set ::validation_param(alternate_master_flag) "dont_care"
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        set ::validation_param(profile_specific1) $param(-profile_specific1)
    } else {

        set ::validation_param(profile_specific1) "dont_care"
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        set ::validation_param(profile_specific2) $param(-profile_specific2)
    } else {

        set ::validation_param(profile_specific2) "dont_care"
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        set ::validation_param(leap61) $param(-leap61)
    } else {

        set ::validation_param(leap61) "dont_care"
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        set ::validation_param(leap59) $param(-leap59)
    } else {

        set ::validation_param(leap59) "dont_care"
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        set ::validation_param(current_utc_offset_valid) $param(-current_utc_offset_valid)
    } else {

        set ::validation_param(current_utc_offset_valid) "dont_care"
    }

    #PTP Timescale Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        set ::validation_param(ptp_timescale) $param(-ptp_timescale)
    } else {

        set ::validation_param(ptp_timescale) "dont_care"
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        set ::validation_param(time_traceable) $param(-time_traceable)
    } else {

        set ::validation_param(time_traceable) "dont_care"
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        set ::validation_param(freq_traceable) $param(-freq_traceable)
    } else {

        set ::validation_param(freq_traceable) "dont_care"
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        set ::validation_param(sequence_id) $param(-sequence_id)
    } else {

        set ::validation_param(sequence_id) "dont_care"
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        set ::validation_param(control_field) $param(-control_field)
    } else {

        set ::validation_param(control_field) "dont_care"
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame timeout
    if {[info exists param(-timeout)]} {

        set timeout $param(-timeout)
    } else {

        set timeout 5
    }

    #OUTPUT PARAMETERS

    #Destination MAC dddress
    if {[info exists param(-recvd_dest_mac)]} {

        upvar $param(-recvd_dest_mac) recvd_dest_mac
        set recvd_dest_mac ""
    }

    #Source MAC address
    if {[info exists param(-recvd_src_mac)]} {

        upvar $param(-recvd_src_mac) recvd_src_mac
        set recvd_src_mac ""
    }

    #Two Step Flag
    if {[info exists param(-recvd_two_step_flag)]} {

        upvar $param(-recvd_two_step_flag) recvd_two_step_flag
        set recvd_two_step_flag ""
    }

    #Unicast Flag
    if {[info exists param(-recvd_unicast_flag)]} {

        upvar $param(-recvd_unicast_flag) recvd_unicast_flag
        set recvd_unicast_flag ""
    }

    #Correction Field value
    if {[info exists param(-recvd_correction_field)]} {

        upvar $param(-recvd_correction_field) recvd_correction_field
        set recvd_correction_field ""
    }

    #Source port number
    if {[info exists param(-recvd_src_port_number)]} {

        upvar $param(-recvd_src_port_number) recvd_src_port_number
        set recvd_src_port_number ""
    }

    #Source clock identity
    if {[info exists param(-recvd_src_clock_identity)]} {

        upvar $param(-recvd_src_clock_identity) recvd_src_clock_identity
        set recvd_src_clock_identity ""
    }

    #Sequence ID
    if {[info exists param(-recvd_sequence_id)]} {

        upvar $param(-recvd_sequence_id) recvd_sequence_id
        set recvd_sequence_id ""
    }

    #Log message interval
    if {[info exists param(-recvd_log_message_interval)]} {

        upvar $param(-recvd_log_message_interval) recvd_log_message_interval
        set recvd_log_message_interval ""
    }

    #Recvd Receive Timestamp Sec
    if {[info exists param(-recvd_receive_timestamp_sec)]} {

        upvar $param(-recvd_receive_timestamp_sec) recvd_receive_timestamp_sec
        set recvd_receive_timestamp_sec ""
    }

    #Recvd Receive Timestamp Ns
    if {[info exists param(-recvd_receive_timestamp_ns)]} {

        upvar $param(-recvd_receive_timestamp_ns) recvd_receive_timestamp_ns
        set recvd_receive_timestamp_ns ""
    }

    #Recvd Requesting Port Number
    if {[info exists param(-recvd_requesting_port_number)]} {

        upvar $param(-recvd_requesting_port_number) recvd_requesting_port_number
        set recvd_requesting_port_number ""
    }

    #Received packet timestamp in seconds
    if {[info exists param(-rx_timestamp_sec)]} {

        upvar $param(-rx_timestamp_sec) rx_timestamp_sec
        set rx_timestamp_sec ""
    }

    #Received packet timestamp in nanoseconds
    if {[info exists param(-rx_timestamp_ns)]} {

        upvar $param(-rx_timestamp_ns) rx_timestamp_ns
        set rx_timestamp_ns ""
    }

    #Error Reason
    if {[info exists param(-error_reason)]} {

        upvar $param(-error_reason) error_reason
        set error_reason ""
    }

    #LOGIC GOES HERE
    #Timeout in millisec
    set timeout [expr int($timeout*1000)]
    set error_code 0

    print_timeout $timeout

    set result [pltLib::recv_periodic\
            -session_id           $session_id\
            -port_num             $port_num\
            -filter_id            $filter_id\
            -timeout              $timeout\
            -reset_count          $reset_count\
            -validation_handler   "WRPTP"\
            -error_code           error_code\
            -rx_timestamp         rx_timestamp_sec\
            -rx_time_nanoseconds  rx_timestamp_ns\
            -pkt_content          pkt_content\
            -pkt_size             pkt_size]

    set error_reason [wrptp::tee_recv_error_reason $error_code]

    if {[info exists pkt_content]} {

        set hex $pkt_content

        pbLib::decode_wrptp_pkt\
                 -packet                         $hex\
                 -dest_mac                       recvd_dest_mac\
                 -src_mac                        recvd_src_mac\
                 -eth_type                       recvd_eth_type\
                 -vlan_id                        recvd_vlan_id\
                 -vlan_priority                  recvd_vlan_priority\
                 -vlan_dei                       recvd_vlan_dei\
                 -src_ip                         recvd_src_ip\
                 -dest_ip                        recvd_dest_ip\
                 -src_port                       recvd_src_port\
                 -dest_port                      recvd_dest_port\
                 -udp_length                     recvd_udp_length\
                 -udp_checksum                   recvd_udp_checksum\
                 -transport_specific             recvd_transport_specific\
                 -message_type                   recvd_message_type\
                 -version_ptp                    recvd_version_ptp\
                 -message_length                 recvd_message_length\
                 -domain_number                  recvd_domain_number\
                 -alternate_master_flag          recvd_alternate_master_flag\
                 -two_step_flag                  recvd_two_step_flag\
                 -unicast_flag                   recvd_unicast_flag\
                 -profile_specific1              recvd_profile_specific1\
                 -profile_specific2              recvd_profile_specific2\
                 -leap61                         recvd_leap61\
                 -leap59                         recvd_leap59\
                 -current_utc_offset_valid       recvd_current_utc_offset_valid\
                 -ptp_timescale                  recvd_ptp_timescale\
                 -time_traceable                 recvd_time_traceable\
                 -freq_traceable                 recvd_freq_traceable\
                 -correction_field               recvd_correction_field\
                 -src_port_number                recvd_src_port_number\
                 -src_clock_identity             recvd_src_clock_identity\
                 -sequence_id                    recvd_sequence_id\
                 -control_field                  recvd_control_field\
                 -log_message_interval           recvd_log_message_interval\
                 -receive_timestamp_sec          recvd_receive_timestamp_sec\
                 -receive_timestamp_ns           recvd_receive_timestamp_ns\
                 -requesting_port_number         recvd_requesting_port_number\
                 -requesting_clock_identity      recvd_requesting_clock_identity

    }

    return $result

}


################################################################################
#                                                                              #
#  PROCEDURE NAME         :  wrptp::recv_followup                              #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Reception port                 #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   filter_id                     : (Mandatory) Filter id                      #
#                                                                              #
#   reset_count                   : (Optional)  Reset Count                    #
#                                               (Default: ON)                  #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                                                              #
#   transport_specific            : (Optional)  Transport Specific             #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default: 8)                   #
#                                                                              #
#   version_ptp                   : (Optional)  PTP version number             #
#                                                                              #
#   message_length                : (Optional)  PTP Annouce message length     #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP Timescale Flag             #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default: 1)                   #
#                                                                              #
#   timeout                       : (Optional)  Frame timeout (in seconds)     #
#                                               (Default: 5)                   #
#                                                                              #
#  OUTPUT PARAMETERS      :                                                    #
#                                                                              #
#   recvd_dest_mac                : (Optional)  Destination MAC dddress.       #
#                                                                              #
#   recvd_src_mac                 : (Optional)  Source MAC address.            #
#                                                                              #
#   recvd_two_step_flag           : (Optional)  Two Step Flag                  #
#                                                                              #
#   recvd_unicast_flag            : (Optional)  Unicast Flag                   #
#                                                                              #
#   recvd_correction_field        : (Optional)  Correction Field value         #
#                                                                              #
#   recvd_src_port_number         : (Optional)  Source port number             #
#                                                                              #
#   recvd_src_clock_identity      : (Optional)  Source clock identity          #
#                                                                              #
#   recvd_sequence_id             : (Optional)  Sequence ID                    #
#                                                                              #
#   recvd_log_message_interval    : (Optional)  Log message interval           #
#                                                                              #
#   recvd_precise_origin_timestamp_sec: (Optional)  Recvd Precise Origin Timestamp Sec #
#                                                                              #
#   recvd_precise_origin_timestamp_ns: (Optional)  Recvd Precise Origin Timestamp Ns #
#                                                                              #
#   rx_timestamp_sec              : (Optional)  Packet's rx time in seconds    #
#                                                                              #
#   rx_timestamp_ns               : (Optional)  Packet's rx time in ns         #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is received as         #
#                                         expected)                            #
#                                                                              #
#                         : 0 on failure (If the packet is not received        #
#                                         as expected)                         #
#                                                                              #
#  DEFINITION             : This function is used to receive                   #
#                           PTP followup message                               #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#   wrptp::recv_followup\                                                      #
#       -port_num                         $port_num\                           #
#       -filter_id                        $filter_id\                          #
#       -reset_count                      $reset_count\                        #
#       -eth_type                         $eth_type\                           #
#       -vlan_id                          $vlan_id\                            #
#       -dest_ip                          $dest_ip\                            #
#       -src_ip                           $src_ip\                             #
#       -ip_checksum                      $ip_checksum\                        #
#       -dest_port                        $dest_port\                          #
#       -src_port                         $src_port\                           #
#       -udp_length                       $udp_length\                         #
#       -udp_checksum                     $udp_checksum\                       #
#       -transport_specific               $transport_specific\                 #
#       -message_type                     $message_type\                       #
#       -version_ptp                      $version_ptp\                        #
#       -message_length                   $message_length\                     #
#       -domain_number                    $domain_number\                      #
#       -alternate_master_flag            $alternate_master_flag\              #
#       -profile_specific1                $profile_specific1\                  #
#       -profile_specific2                $profile_specific2\                  #
#       -leap61                           $leap61\                             #
#       -leap59                           $leap59\                             #
#       -current_utc_offset_valid         $current_utc_offset_valid\           #
#       -ptp_timescale                    $ptp_timescale\                      #
#       -time_traceable                   $time_traceable\                     #
#       -freq_traceable                   $freq_traceable\                     #
#       -control_field                    $control_field\                      #
#       -count                            $count\                              #
#       -interval                         $interval                            #
#       -recvd_dest_mac                   recvd_dest_mac\                      #
#       -recvd_src_mac                    recvd_src_mac\                       #
#       -recvd_two_step_flag              recvd_two_step_flag\                 #
#       -recvd_unicast_flag               recvd_unicast_flag\                  #
#       -recvd_correction_field           recvd_correction_field\              #
#       -recvd_src_port_number            recvd_src_port_identity\             #
#       -recvd_src_clock_identity         recvd_src_clock_identity\            #
#       -recvd_sequence_id                recvd_sequence_id\                   #
#       -recvd_log_message_interval       recvd_message_interval\              #
#       -recvd_precise_origin_timestamp_sec recvd_precise_origin_timestamp_sec\#
#       -recvd_precise_origin_timestamp_ns  recvd_precise_origin_timestamp_ns\ #
#       -rx_timestamp_sec                 rx_timestamp_sec\                    #
#       -rx_timestamp_ns                  rx_timestamp_ns                      #
#                                                                              #
################################################################################
proc wrptp::recv_followup {args} {

    array unset ::validation_param

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "wrptp::recv_followup: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #Reception port
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "wrptp::recv_followup: Missing Argument -> port_num"
        return 0
    }

    #Filter id
    if {[info exists param(-filter_id)]} {

        set filter_id $param(-filter_id)
    } else {

        ERRLOG -msg "wrptp::recv_followup: Missing Argument -> filter_id"
        return 0
    }

    #Reset Count
    if {[info exists param(-reset_count)]} {

        set reset_count $param(-reset_count)
    } else {

        set reset_count ON
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        set ::validation_param(eth_type) $param(-eth_type)
    } else {

        if { $::wrptp_vlan_encap == "true" } {
            set ::validation_param(eth_type) 8100
        } else {
            if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
                set ::validation_param(eth_type) 0800
            } else {
                set ::validation_param(eth_type) 88F7
            }
        }
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
        set ::validation_param(vlan_id) $param(-vlan_id)
    } else {

        if { $::wrptp_vlan_encap == "true" } {
            set ::validation_param(vlan_id) $::wrptp_vlan_id
        } else {
            set ::validation_param(vlan_id) "dont_care"
        }
    }

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
        set ::validation_param(dest_ip) $param(-dest_ip)
    } else {

        set ::validation_param(dest_ip) "dont_care"
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
        set ::validation_param(src_ip) $param(-src_ip)
    } else {

        set ::validation_param(src_ip) "dont_care"
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
        set ::validation_param(ip_checksum) $param(-ip_checksum)
    } else {

        set ::validation_param(ip_checksum) "dont_care"
    }

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
        set ::validation_param(dest_port) $param(-dest_port)
    } else {

        set ::validation_param(dest_port) "dont_care"
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
        set ::validation_param(src_port) $param(-src_port)
    } else {

        set ::validation_param(src_port) "dont_care"
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
        set ::validation_param(udp_length) $param(-udp_length)
    } else {

        set ::validation_param(udp_length) "dont_care"
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
        set ::validation_param(udp_checksum) $param(-udp_checksum)
    } else {

        set ::validation_param(udp_checksum) "dont_care"
    }

    #Transport Specific
    if {[info exists param(-transport_specific)]} {

        set transport_specific $param(-transport_specific)
        set ::validation_param(transport_specific) $param(-transport_specific)
    } else {

        set ::validation_param(transport_specific) "dont_care"
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        set ::validation_param(message_type) $param(-message_type)
    } else {

        set ::validation_param(message_type) 8
    }

    #PTP version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        set ::validation_param(version_ptp) $param(-version_ptp)
    } else {

        set ::validation_param(version_ptp) "dont_care"
    }

    #PTP Annouce message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        set ::validation_param(message_length) $param(-message_length)
    } else {

        set ::validation_param(message_length) "dont_care"
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        set ::validation_param(domain_number) $param(-domain_number)
    } else {

        set ::validation_param(domain_number) "dont_care"
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        set ::validation_param(alternate_master_flag) $param(-alternate_master_flag)
    } else {

        set ::validation_param(alternate_master_flag) "dont_care"
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        set ::validation_param(profile_specific1) $param(-profile_specific1)
    } else {

        set ::validation_param(profile_specific1) "dont_care"
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        set ::validation_param(profile_specific2) $param(-profile_specific2)
    } else {

        set ::validation_param(profile_specific2) "dont_care"
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        set ::validation_param(leap61) $param(-leap61)
    } else {

        set ::validation_param(leap61) "dont_care"
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        set ::validation_param(leap59) $param(-leap59)
    } else {

        set ::validation_param(leap59) "dont_care"
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        set ::validation_param(current_utc_offset_valid) $param(-current_utc_offset_valid)
    } else {

        set ::validation_param(current_utc_offset_valid) "dont_care"
    }

    #PTP Timescale Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        set ::validation_param(ptp_timescale) $param(-ptp_timescale)
    } else {

        set ::validation_param(ptp_timescale) "dont_care"
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        set ::validation_param(time_traceable) $param(-time_traceable)
    } else {

        set ::validation_param(time_traceable) "dont_care"
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        set ::validation_param(freq_traceable) $param(-freq_traceable)
    } else {

        set ::validation_param(freq_traceable) "dont_care"
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        set ::validation_param(sequence_id) $param(-sequence_id)
    } else {

        set ::validation_param(sequence_id) "dont_care"
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        set ::validation_param(control_field) $param(-control_field)
    } else {

        set ::validation_param(control_field) "dont_care"
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame timeout
    if {[info exists param(-timeout)]} {

        set timeout $param(-timeout)
    } else {

        set timeout 5
    }

    #OUTPUT PARAMETERS

    #Destination MAC dddress
    if {[info exists param(-recvd_dest_mac)]} {

        upvar $param(-recvd_dest_mac) recvd_dest_mac
        set recvd_dest_mac ""
    }

    #Source MAC address
    if {[info exists param(-recvd_src_mac)]} {

        upvar $param(-recvd_src_mac) recvd_src_mac
        set recvd_src_mac ""
    }

    #Two Step Flag
    if {[info exists param(-recvd_two_step_flag)]} {

        upvar $param(-recvd_two_step_flag) recvd_two_step_flag
        set recvd_two_step_flag ""
    }

    #Unicast Flag
    if {[info exists param(-recvd_unicast_flag)]} {

        upvar $param(-recvd_unicast_flag) recvd_unicast_flag
        set recvd_unicast_flag ""
    }

    #Correction Field value
    if {[info exists param(-recvd_correction_field)]} {

        upvar $param(-recvd_correction_field) recvd_correction_field
        set recvd_correction_field ""
    }

    #Source port number
    if {[info exists param(-recvd_src_port_number)]} {

        upvar $param(-recvd_src_port_number) recvd_src_port_number
        set recvd_src_port_number ""
    }

    #Source clock identity
    if {[info exists param(-recvd_src_clock_identity)]} {

        upvar $param(-recvd_src_clock_identity) recvd_src_clock_identity
        set recvd_src_clock_identity ""
    }

    #Sequence ID
    if {[info exists param(-recvd_sequence_id)]} {

        upvar $param(-recvd_sequence_id) recvd_sequence_id
        set recvd_sequence_id ""
    }

    #Log message interval
    if {[info exists param(-recvd_log_message_interval)]} {

        upvar $param(-recvd_log_message_interval) recvd_log_message_interval
        set recvd_log_message_interval ""
    }

    #Precise Origin Timestamp in seconds
    if {[info exists param(-recvd_precise_origin_timestamp_sec)]} {

        upvar $param(-recvd_precise_origin_timestamp_sec) recvd_precise_origin_timestamp_sec
        set recvd_precise_origin_timestamp_sec ""
    }

    #Precise Origin Timestamp in nano seconds
    if {[info exists param(-recvd_precise_origin_timestamp_ns)]} {

        upvar $param(-recvd_precise_origin_timestamp_ns) recvd_precise_origin_timestamp_ns
        set recvd_precise_origin_timestamp_ns ""
    }

    #Received packet timestamp in seconds
    if {[info exists param(-rx_timestamp_sec)]} {

        upvar $param(-rx_timestamp_sec) rx_timestamp_sec
        set rx_timestamp_sec ""
    }

    #Received packet timestamp in nanoseconds
    if {[info exists param(-rx_timestamp_ns)]} {

        upvar $param(-rx_timestamp_ns) rx_timestamp_ns
        set rx_timestamp_ns ""
    }

    #Error Reason
    if {[info exists param(-error_reason)]} {

        upvar $param(-error_reason) error_reason
        set error_reason ""
    }

    #LOGIC GOES HERE
    #Timeout in millisec
    set timeout [expr int($timeout*1000)]
    set error_code 0

    print_timeout $timeout

    set result [pltLib::recv_periodic\
            -session_id           $session_id\
            -port_num             $port_num\
            -filter_id            $filter_id\
            -timeout              $timeout\
            -reset_count          $reset_count\
            -validation_handler   "WRPTP"\
            -error_code           error_code\
            -rx_timestamp         rx_timestamp_sec\
            -rx_time_nanoseconds  rx_timestamp_ns\
            -pkt_content          pkt_content\
            -pkt_size             pkt_size]

    set error_reason [wrptp::tee_recv_error_reason $error_code]

    if {[info exists pkt_content]} {

        set hex $pkt_content

        pbLib::decode_wrptp_pkt\
                 -packet                         $hex\
                 -dest_mac                       recvd_dest_mac\
                 -src_mac                        recvd_src_mac\
                 -eth_type                       recvd_eth_type\
                 -vlan_id                        recvd_vlan_id\
                 -vlan_priority                  recvd_vlan_priority\
                 -vlan_dei                       recvd_vlan_dei\
                 -src_ip                         recvd_src_ip\
                 -dest_ip                        recvd_dest_ip\
                 -src_port                       recvd_src_port\
                 -dest_port                      recvd_dest_port\
                 -udp_length                     recvd_udp_length\
                 -udp_checksum                   recvd_udp_checksum\
                 -transport_specific             recvd_transport_specific\
                 -message_type                   recvd_message_type\
                 -version_ptp                    recvd_version_ptp\
                 -message_length                 recvd_message_length\
                 -domain_number                  recvd_domain_number\
                 -alternate_master_flag          recvd_alternate_master_flag\
                 -two_step_flag                  recvd_two_step_flag\
                 -unicast_flag                   recvd_unicast_flag\
                 -profile_specific1              recvd_profile_specific1\
                 -profile_specific2              recvd_profile_specific2\
                 -leap61                         recvd_leap61\
                 -leap59                         recvd_leap59\
                 -current_utc_offset_valid       recvd_current_utc_offset_valid\
                 -ptp_timescale                  recvd_ptp_timescale\
                 -time_traceable                 recvd_time_traceable\
                 -freq_traceable                 recvd_freq_traceable\
                 -correction_field               recvd_correction_field\
                 -src_port_number                recvd_src_port_number\
                 -src_clock_identity             recvd_src_clock_identity\
                 -sequence_id                    recvd_sequence_id\
                 -control_field                  recvd_control_field\
                 -log_message_interval           recvd_log_message_interval\
                 -precise_origin_timestamp_sec   recvd_precise_origin_timestamp_sec\
                 -precise_origin_timestamp_ns    recvd_precise_origin_timestamp_ns

    }

    return $result

}


################################################################################
#                                                                              #
#  PROCEDURE NAME         :  wrptp::recv_pdelay_req                            #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Reception port                 #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   filter_id                     : (Mandatory) Filter id                      #
#                                                                              #
#   reset_count                   : (Optional)  Reset Count                    #
#                                               (Default: ON)                  #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                                                              #
#   transport_specific            : (Optional)  Transport Specific             #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default: 2)                   #
#                                                                              #
#   version_ptp                   : (Optional)  PTP version number             #
#                                                                              #
#   message_length                : (Optional)  PTP Annouce message length     #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP Timescale Flag             #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default: 1)                   #
#                                                                              #
#   timeout                       : (Optional)  Frame timeout (in seconds)     #
#                                               (Default: 5)                   #
#                                                                              #
#  OUTPUT PARAMETERS      :                                                    #
#                                                                              #
#   recvd_dest_mac                : (Optional)  Destination MAC dddress.       #
#                                                                              #
#   recvd_src_mac                 : (Optional)  Source MAC address.            #
#                                                                              #
#   recvd_two_step_flag           : (Optional)  Two Step Flag                  #
#                                                                              #
#   recvd_unicast_flag            : (Optional)  Unicast Flag                   #
#                                                                              #
#   recvd_correction_field        : (Optional)  Correction Field value         #
#                                                                              #
#   recvd_src_port_number         : (Optional)  Source port number             #
#                                                                              #
#   recvd_src_clock_identity      : (Optional)  Source clock identity          #
#                                                                              #
#   recvd_sequence_id             : (Optional)  Sequence ID                    #
#                                                                              #
#   recvd_log_message_interval    : (Optional)  Log message interval           #
#                                                                              #
#   recvd_origin_timestamp_sec    : (Optional)  Recvd Origin Timestamp Sec     #
#                                                                              #
#   recvd_origin_timestamp_ns     : (Optional)  Recvd Origin Timestamp Ns      #
#                                                                              #
#   rx_timestamp_sec              : (Optional)  Packet's rx time in seconds    #
#                                                                              #
#   rx_timestamp_ns               : (Optional)  Packet's rx time in ns         #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is received as         #
#                                         expected)                            #
#                                                                              #
#                         : 0 on failure (If the packet is not received        #
#                                         as expected)                         #
#                                                                              #
#  DEFINITION             : This function is used to receive                   #
#                           PTP pdelay req message                             #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       wrptp::recv_pdelay_req\                                                #
#           -port_num                         $port_num\                       #
#           -filter_id                        $filter_id\                      #
#           -reset_count                      $reset_count\                    #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -transport_specific               $transport_specific\             #
#           -message_type                     $message_type\                   #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -control_field                    $control_field\                  #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#           -recvd_dest_mac                   recvd_dest_mac\                  #
#           -recvd_src_mac                    recvd_src_mac\                   #
#           -recvd_two_step_flag              recvd_two_step_flag\             #
#           -recvd_unicast_flag               recvd_unicast_flag\              #
#           -recvd_correction_field           recvd_correction_field\          #
#           -recvd_src_port_number            recvd_src_port_identity\         #
#           -recvd_src_clock_identity         recvd_src_clock_identity\        #
#           -recvd_sequence_id                recvd_sequence_id\               #
#           -recvd_log_message_interval       recvd_message_interval\          #
#           -recvd_origin_timestamp_sec       recvd_origin_timestamp_sec\      #
#           -recvd_origin_timestamp_ns        recvd_origin_timestamp_ns\       #
#           -rx_timestamp_sec                 rx_timestamp_sec\                #
#           -rx_timestamp_ns                  rx_timestamp_ns                  #
#                                                                              #
################################################################################
proc wrptp::recv_pdelay_req {args} {

    array unset ::validation_param

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "wrptp::recv_pdelay_req: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #Reception port
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "wrptp::recv_pdelay_req: Missing Argument -> port_num"
        return 0
    }

    #Filter id
    if {[info exists param(-filter_id)]} {

        set filter_id $param(-filter_id)
    } else {

        ERRLOG -msg "wrptp::recv_pdelay_req: Missing Argument -> filter_id"
        return 0
    }

    #Reset Count
    if {[info exists param(-reset_count)]} {

        set reset_count $param(-reset_count)
    } else {

        set reset_count ON
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        set ::validation_param(eth_type) $param(-eth_type)
    } else {

        if { $::wrptp_vlan_encap == "true" } {
            set ::validation_param(eth_type) 8100
        } else {
            if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
                set ::validation_param(eth_type) 0800
            } else {
                set ::validation_param(eth_type) 88F7
            }
        }
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
        set ::validation_param(vlan_id) $param(-vlan_id)
    } else {

        if { $::wrptp_vlan_encap == "true" } {
            set ::validation_param(vlan_id) $::wrptp_vlan_id
        } else {
            set ::validation_param(vlan_id) "dont_care"
        }
    }

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
        set ::validation_param(dest_ip) $param(-dest_ip)
    } else {

        set ::validation_param(dest_ip) "dont_care"
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
        set ::validation_param(src_ip) $param(-src_ip)
    } else {

        set ::validation_param(src_ip) "dont_care"
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
        set ::validation_param(ip_checksum) $param(-ip_checksum)
    } else {

        set ::validation_param(ip_checksum) "dont_care"
    }

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
        set ::validation_param(dest_port) $param(-dest_port)
    } else {

        set ::validation_param(dest_port) "dont_care"
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
        set ::validation_param(src_port) $param(-src_port)
    } else {

        set ::validation_param(src_port) "dont_care"
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
        set ::validation_param(udp_length) $param(-udp_length)
    } else {

        set ::validation_param(udp_length) "dont_care"
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
        set ::validation_param(udp_checksum) $param(-udp_checksum)
    } else {

        set ::validation_param(udp_checksum) "dont_care"
    }

    #Transport Specific
    if {[info exists param(-transport_specific)]} {

        set transport_specific $param(-transport_specific)
        set ::validation_param(transport_specific) $param(-transport_specific)
    } else {

        set ::validation_param(transport_specific) "dont_care"
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        set ::validation_param(message_type) $param(-message_type)
    } else {

        set ::validation_param(message_type) 2
    }

    #PTP version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        set ::validation_param(version_ptp) $param(-version_ptp)
    } else {

        set ::validation_param(version_ptp) "dont_care"
    }

    #PTP Annouce message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        set ::validation_param(message_length) $param(-message_length)
    } else {

        set ::validation_param(message_length) "dont_care"
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        set ::validation_param(domain_number) $param(-domain_number)
    } else {

        set ::validation_param(domain_number) "dont_care"
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        set ::validation_param(alternate_master_flag) $param(-alternate_master_flag)
    } else {

        set ::validation_param(alternate_master_flag) "dont_care"
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        set ::validation_param(profile_specific1) $param(-profile_specific1)
    } else {

        set ::validation_param(profile_specific1) "dont_care"
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        set ::validation_param(profile_specific2) $param(-profile_specific2)
    } else {

        set ::validation_param(profile_specific2) "dont_care"
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        set ::validation_param(leap61) $param(-leap61)
    } else {

        set ::validation_param(leap61) "dont_care"
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        set ::validation_param(leap59) $param(-leap59)
    } else {

        set ::validation_param(leap59) "dont_care"
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        set ::validation_param(current_utc_offset_valid) $param(-current_utc_offset_valid)
    } else {

        set ::validation_param(current_utc_offset_valid) "dont_care"
    }

    #PTP Timescale Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        set ::validation_param(ptp_timescale) $param(-ptp_timescale)
    } else {

        set ::validation_param(ptp_timescale) "dont_care"
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        set ::validation_param(time_traceable) $param(-time_traceable)
    } else {

        set ::validation_param(time_traceable) "dont_care"
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        set ::validation_param(freq_traceable) $param(-freq_traceable)
    } else {

        set ::validation_param(freq_traceable) "dont_care"
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        set ::validation_param(sequence_id) $param(-sequence_id)
    } else {

        set ::validation_param(sequence_id) "dont_care"
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        set ::validation_param(control_field) $param(-control_field)
    } else {

        set ::validation_param(control_field) "dont_care"
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame timeout
    if {[info exists param(-timeout)]} {

        set timeout $param(-timeout)
    } else {

        set timeout 5
    }

    #OUTPUT PARAMETERS

    #Destination MAC dddress
    if {[info exists param(-recvd_dest_mac)]} {

        upvar $param(-recvd_dest_mac) recvd_dest_mac
        set recvd_dest_mac ""
    }

    #Source MAC address
    if {[info exists param(-recvd_src_mac)]} {

        upvar $param(-recvd_src_mac) recvd_src_mac
        set recvd_src_mac ""
    }

    #Two Step Flag
    if {[info exists param(-recvd_two_step_flag)]} {

        upvar $param(-recvd_two_step_flag) recvd_two_step_flag
        set recvd_two_step_flag ""
    }

    #Unicast Flag
    if {[info exists param(-recvd_unicast_flag)]} {

        upvar $param(-recvd_unicast_flag) recvd_unicast_flag
        set recvd_unicast_flag ""
    }

    #Correction Field value
    if {[info exists param(-recvd_correction_field)]} {

        upvar $param(-recvd_correction_field) recvd_correction_field
        set recvd_correction_field ""
    }

    #Source port number
    if {[info exists param(-recvd_src_port_number)]} {

        upvar $param(-recvd_src_port_number) recvd_src_port_number
        set recvd_src_port_number ""
    }

    #Source clock identity
    if {[info exists param(-recvd_src_clock_identity)]} {

        upvar $param(-recvd_src_clock_identity) recvd_src_clock_identity
        set recvd_src_clock_identity ""
    }

    #Sequence ID
    if {[info exists param(-recvd_sequence_id)]} {

        upvar $param(-recvd_sequence_id) recvd_sequence_id
        set recvd_sequence_id ""
    }

    #Log message interval
    if {[info exists param(-recvd_log_message_interval)]} {

        upvar $param(-recvd_log_message_interval) recvd_log_message_interval
        set recvd_log_message_interval ""
    }

    #Recvd Origin Timestamp Sec
    if {[info exists param(-recvd_origin_timestamp_sec)]} {

        upvar $param(-recvd_origin_timestamp_sec) recvd_origin_timestamp_sec
        set recvd_origin_timestamp_sec ""
    }

    #Recvd Origin Timestamp Ns
    if {[info exists param(-recvd_origin_timestamp_ns)]} {

        upvar $param(-recvd_origin_timestamp_ns) recvd_origin_timestamp_ns
        set recvd_origin_timestamp_ns ""
    }

    #Received packet timestamp in seconds
    if {[info exists param(-rx_timestamp_sec)]} {

        upvar $param(-rx_timestamp_sec) rx_timestamp_sec
        set rx_timestamp_sec ""
    }

    #Received packet timestamp in nanoseconds
    if {[info exists param(-rx_timestamp_ns)]} {

        upvar $param(-rx_timestamp_ns) rx_timestamp_ns
        set rx_timestamp_ns ""
    }

    #Error Reason
    if {[info exists param(-error_reason)]} {

        upvar $param(-error_reason) error_reason
        set error_reason ""
    }

    #LOGIC GOES HERE
    #Timeout in millisec
    set timeout [expr int($timeout*1000)]
    set error_code 0

    print_timeout $timeout

    set result [pltLib::recv_periodic\
            -session_id           $session_id\
            -port_num             $port_num\
            -filter_id            $filter_id\
            -timeout              $timeout\
            -reset_count          $reset_count\
            -validation_handler   "WRPTP"\
            -error_code           error_code\
            -rx_timestamp         rx_timestamp_sec\
            -rx_time_nanoseconds  rx_timestamp_ns\
            -pkt_content          pkt_content\
            -pkt_size             pkt_size]

    set error_reason [wrptp::tee_recv_error_reason $error_code]

    if {[info exists pkt_content]} {

        set hex $pkt_content

        pbLib::decode_wrptp_pkt\
                 -packet                         $hex\
                 -dest_mac                       recvd_dest_mac\
                 -src_mac                        recvd_src_mac\
                 -eth_type                       recvd_eth_type\
                 -vlan_id                        recvd_vlan_id\
                 -vlan_priority                  recvd_vlan_priority\
                 -vlan_dei                       recvd_vlan_dei\
                 -src_ip                         recvd_src_ip\
                 -dest_ip                        recvd_dest_ip\
                 -src_port                       recvd_src_port\
                 -dest_port                      recvd_dest_port\
                 -udp_length                     recvd_udp_length\
                 -udp_checksum                   recvd_udp_checksum\
                 -transport_specific             recvd_transport_specific\
                 -message_type                   recvd_message_type\
                 -version_ptp                    recvd_version_ptp\
                 -message_length                 recvd_message_length\
                 -domain_number                  recvd_domain_number\
                 -alternate_master_flag          recvd_alternate_master_flag\
                 -two_step_flag                  recvd_two_step_flag\
                 -unicast_flag                   recvd_unicast_flag\
                 -profile_specific1              recvd_profile_specific1\
                 -profile_specific2              recvd_profile_specific2\
                 -leap61                         recvd_leap61\
                 -leap59                         recvd_leap59\
                 -current_utc_offset_valid       recvd_current_utc_offset_valid\
                 -ptp_timescale                  recvd_ptp_timescale\
                 -time_traceable                 recvd_time_traceable\
                 -freq_traceable                 recvd_freq_traceable\
                 -correction_field               recvd_correction_field\
                 -src_port_number                recvd_src_port_number\
                 -src_clock_identity             recvd_src_clock_identity\
                 -sequence_id                    recvd_sequence_id\
                 -control_field                  recvd_control_field\
                 -log_message_interval           recvd_log_message_interval\
                 -origin_timestamp_sec           recvd_origin_timestamp_sec\
                 -origin_timestamp_ns            recvd_origin_timestamp_ns

    }

    return $result

}


################################################################################
#                                                                              #
#  PROCEDURE NAME         :  wrptp::recv_pdelay_resp                           #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Reception port                 #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   filter_id                     : (Mandatory) Filter id                      #
#                                                                              #
#   reset_count                   : (Optional)  Reset Count                    #
#                                               (Default: ON)                  #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                                                              #
#   transport_specific            : (Optional)  Transport Specific             #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default: 3)                   #
#                                                                              #
#   version_ptp                   : (Optional)  PTP version number             #
#                                                                              #
#   message_length                : (Optional)  PTP Annouce message length     #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP Timescale Flag             #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default: 1)                   #
#                                                                              #
#   timeout                       : (Optional)  Frame timeout (in seconds)     #
#                                               (Default: 5)                   #
#                                                                              #
#  OUTPUT PARAMETERS      :                                                    #
#                                                                              #
#   recvd_dest_mac                : (Optional)  Destination MAC dddress.       #
#                                                                              #
#   recvd_src_mac                 : (Optional)  Source MAC address.            #
#                                                                              #
#   recvd_two_step_flag           : (Optional)  Two Step Flag                  #
#                                                                              #
#   recvd_unicast_flag            : (Optional)  Unicast Flag                   #
#                                                                              #
#   recvd_correction_field        : (Optional)  Correction Field value         #
#                                                                              #
#   recvd_src_port_number         : (Optional)  Source port number             #
#                                                                              #
#   recvd_src_clock_identity      : (Optional)  Source clock identity          #
#                                                                              #
#   recvd_sequence_id             : (Optional)  Sequence ID                    #
#                                                                              #
#   recvd_log_message_interval    : (Optional)  Log message interval           #
#                                                                              #
#   recvd_request_receipt_timestamp_sec: (Optional)  Recvd Request Receipt Timestamp Sec #
#                                                                              #
#   recvd_request_receipt_timestamp_ns: (Optional)  Recvd Request Receipt Timestamp Ns #
#                                                                              #
#   recvd_requesting_clock_identity: (Optional)  Recvd Requesting Clock Identity #
#                                                                              #
#   recvd_requesting_port_number  : (Optional)  Recvd Requesting Port Number   #
#                                                                              #
#   rx_timestamp_sec              : (Optional)  Packet's rx time in seconds    #
#                                                                              #
#   rx_timestamp_ns               : (Optional)  Packet's rx time in ns         #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is received as         #
#                                         expected)                            #
#                                                                              #
#                         : 0 on failure (If the packet is not received        #
#                                         as expected)                         #
#                                                                              #
#  DEFINITION             : This function is used to receive                   #
#                           PTP pdelay resp message                            #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#  wrptp::recv_pdelay_resp\                                                    #
#   -port_num                         $port_num\                               #
#   -filter_id                        $filter_id\                              #
#   -reset_count                      $reset_count\                            #
#   -eth_type                         $eth_type\                               #
#   -vlan_id                          $vlan_id\                                #
#   -dest_ip                          $dest_ip\                                #
#   -src_ip                           $src_ip\                                 #
#   -ip_checksum                      $ip_checksum\                            #
#   -dest_port                        $dest_port\                              #
#   -src_port                         $src_port\                               #
#   -udp_length                       $udp_length\                             #
#   -udp_checksum                     $udp_checksum\                           #
#   -transport_specific               $transport_specific\                     #
#   -message_type                     $message_type\                           #
#   -version_ptp                      $version_ptp\                            #
#   -message_length                   $message_length\                         #
#   -domain_number                    $domain_number\                          #
#   -alternate_master_flag            $alternate_master_flag\                  #
#   -profile_specific1                $profile_specific1\                      #
#   -profile_specific2                $profile_specific2\                      #
#   -leap61                           $leap61\                                 #
#   -leap59                           $leap59\                                 #
#   -current_utc_offset_valid         $current_utc_offset_valid\               #
#   -ptp_timescale                    $ptp_timescale\                          #
#   -time_traceable                   $time_traceable\                         #
#   -freq_traceable                   $freq_traceable\                         #
#   -control_field                    $control_field\                          #
#   -count                            $count\                                  #
#   -interval                         $interval                                #
#   -recvd_dest_mac                   recvd_dest_mac\                          #
#   -recvd_src_mac                    recvd_src_mac\                           #
#   -recvd_two_step_flag              recvd_two_step_flag\                     #
#   -recvd_unicast_flag               recvd_unicast_flag\                      #
#   -recvd_correction_field           recvd_correction_field\                  #
#   -recvd_src_port_number            recvd_src_port_identity\                 #
#   -recvd_src_clock_identity         recvd_src_clock_identity\                #
#   -recvd_sequence_id                recvd_sequence_id\                       #
#   -recvd_log_message_interval       recvd_message_interval\                  #
#   -recvd_request_receipt_timestamp_sec  recvd_request_receipt_timestamp_sec\ #
#   -recvd_request_receipt_timestamp_ns  recvd_request_receipt_timestamp_ns\   #
#   -recvd_requesting_clock_identity  recvd_requesting_clock_identity\         #
#   -recvd_requesting_port_number     recvd_requesting_port_number\            #
#   -rx_timestamp_sec                 rx_timestamp_sec\                        #
#   -rx_timestamp_ns                  rx_timestamp_ns                          #
#                                                                              #
################################################################################
proc wrptp::recv_pdelay_resp {args} {

    array unset ::validation_param

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "wrptp::recv_pdelay_resp: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #Reception port
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "wrptp::recv_pdelay_resp: Missing Argument -> port_num"
        return 0
    }

    #Filter id
    if {[info exists param(-filter_id)]} {

        set filter_id $param(-filter_id)
    } else {

        ERRLOG -msg "wrptp::recv_pdelay_resp: Missing Argument -> filter_id"
        return 0
    }

    #Reset Count
    if {[info exists param(-reset_count)]} {

        set reset_count $param(-reset_count)
    } else {

        set reset_count ON
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        set ::validation_param(eth_type) $param(-eth_type)
    } else {

        if { $::wrptp_vlan_encap == "true" } {
            set ::validation_param(eth_type) 8100
        } else {
            if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
                set ::validation_param(eth_type) 0800
            } else {
                set ::validation_param(eth_type) 88F7
            }
        }
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
        set ::validation_param(vlan_id) $param(-vlan_id)
    } else {

        if { $::wrptp_vlan_encap == "true" } {
            set ::validation_param(vlan_id) $::wrptp_vlan_id
        } else {
            set ::validation_param(vlan_id) "dont_care"
        }
    }

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
        set ::validation_param(dest_ip) $param(-dest_ip)
    } else {

        set ::validation_param(dest_ip) "dont_care"
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
        set ::validation_param(src_ip) $param(-src_ip)
    } else {

        set ::validation_param(src_ip) "dont_care"
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
        set ::validation_param(ip_checksum) $param(-ip_checksum)
    } else {

        set ::validation_param(ip_checksum) "dont_care"
    }

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
        set ::validation_param(dest_port) $param(-dest_port)
    } else {

        set ::validation_param(dest_port) "dont_care"
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
        set ::validation_param(src_port) $param(-src_port)
    } else {

        set ::validation_param(src_port) "dont_care"
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
        set ::validation_param(udp_length) $param(-udp_length)
    } else {

        set ::validation_param(udp_length) "dont_care"
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
        set ::validation_param(udp_checksum) $param(-udp_checksum)
    } else {

        set ::validation_param(udp_checksum) "dont_care"
    }

    #Transport Specific
    if {[info exists param(-transport_specific)]} {

        set transport_specific $param(-transport_specific)
        set ::validation_param(transport_specific) $param(-transport_specific)
    } else {

        set ::validation_param(transport_specific) "dont_care"
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        set ::validation_param(message_type) $param(-message_type)
    } else {

        set ::validation_param(message_type) 3
    }

    #PTP version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        set ::validation_param(version_ptp) $param(-version_ptp)
    } else {

        set ::validation_param(version_ptp) "dont_care"
    }

    #PTP Annouce message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        set ::validation_param(message_length) $param(-message_length)
    } else {

        set ::validation_param(message_length) "dont_care"
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        set ::validation_param(domain_number) $param(-domain_number)
    } else {

        set ::validation_param(domain_number) "dont_care"
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        set ::validation_param(alternate_master_flag) $param(-alternate_master_flag)
    } else {

        set ::validation_param(alternate_master_flag) "dont_care"
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        set ::validation_param(profile_specific1) $param(-profile_specific1)
    } else {

        set ::validation_param(profile_specific1) "dont_care"
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        set ::validation_param(profile_specific2) $param(-profile_specific2)
    } else {

        set ::validation_param(profile_specific2) "dont_care"
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        set ::validation_param(leap61) $param(-leap61)
    } else {

        set ::validation_param(leap61) "dont_care"
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        set ::validation_param(leap59) $param(-leap59)
    } else {

        set ::validation_param(leap59) "dont_care"
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        set ::validation_param(current_utc_offset_valid) $param(-current_utc_offset_valid)
    } else {

        set ::validation_param(current_utc_offset_valid) "dont_care"
    }

    #PTP Timescale Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        set ::validation_param(ptp_timescale) $param(-ptp_timescale)
    } else {

        set ::validation_param(ptp_timescale) "dont_care"
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        set ::validation_param(time_traceable) $param(-time_traceable)
    } else {

        set ::validation_param(time_traceable) "dont_care"
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        set ::validation_param(freq_traceable) $param(-freq_traceable)
    } else {

        set ::validation_param(freq_traceable) "dont_care"
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        set ::validation_param(sequence_id) $param(-sequence_id)
    } else {

        set ::validation_param(sequence_id) "dont_care"
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        set ::validation_param(control_field) $param(-control_field)
    } else {

        set ::validation_param(control_field) "dont_care"
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame timeout
    if {[info exists param(-timeout)]} {

        set timeout $param(-timeout)
    } else {

        set timeout 5
    }

    #OUTPUT PARAMETERS

    #Destination MAC dddress
    if {[info exists param(-recvd_dest_mac)]} {

        upvar $param(-recvd_dest_mac) recvd_dest_mac
        set recvd_dest_mac ""
    }

    #Source MAC address
    if {[info exists param(-recvd_src_mac)]} {

        upvar $param(-recvd_src_mac) recvd_src_mac
        set recvd_src_mac ""
    }

    #Two Step Flag
    if {[info exists param(-recvd_two_step_flag)]} {

        upvar $param(-recvd_two_step_flag) recvd_two_step_flag
        set recvd_two_step_flag ""
    }

    #Unicast Flag
    if {[info exists param(-recvd_unicast_flag)]} {

        upvar $param(-recvd_unicast_flag) recvd_unicast_flag
        set recvd_unicast_flag ""
    }

    #Correction Field value
    if {[info exists param(-recvd_correction_field)]} {

        upvar $param(-recvd_correction_field) recvd_correction_field
        set recvd_correction_field ""
    }

    #Source port number
    if {[info exists param(-recvd_src_port_number)]} {

        upvar $param(-recvd_src_port_number) recvd_src_port_number
        set recvd_src_port_number ""
    }

    #Source clock identity
    if {[info exists param(-recvd_src_clock_identity)]} {

        upvar $param(-recvd_src_clock_identity) recvd_src_clock_identity
        set recvd_src_clock_identity ""
    }

    #Sequence ID
    if {[info exists param(-recvd_sequence_id)]} {

        upvar $param(-recvd_sequence_id) recvd_sequence_id
        set recvd_sequence_id ""
    }

    #Log message interval
    if {[info exists param(-recvd_log_message_interval)]} {

        upvar $param(-recvd_log_message_interval) recvd_log_message_interval
        set recvd_log_message_interval ""
    }

    #Recvd Requesting Port Number
    if {[info exists param(-recvd_requesting_port_number)]} {

        upvar $param(-recvd_requesting_port_number) recvd_requesting_port_number
        set recvd_requesting_port_number ""
    }

    #Received packet timestamp in seconds
    if {[info exists param(-rx_timestamp_sec)]} {

        upvar $param(-rx_timestamp_sec) rx_timestamp_sec
        set rx_timestamp_sec ""
    }

    #Received packet timestamp in nanoseconds
    if {[info exists param(-rx_timestamp_ns)]} {

        upvar $param(-rx_timestamp_ns) rx_timestamp_ns
        set rx_timestamp_ns ""
    }

    #Error Reason
    if {[info exists param(-error_reason)]} {

        upvar $param(-error_reason) error_reason
        set error_reason ""
    }

    #LOGIC GOES HERE
    #Timeout in millisec
    set timeout [expr int($timeout*1000)]
    set error_code 0

    print_timeout $timeout

    set result [pltLib::recv_periodic\
            -session_id           $session_id\
            -port_num             $port_num\
            -filter_id            $filter_id\
            -timeout              $timeout\
            -reset_count          $reset_count\
            -validation_handler   "WRPTP"\
            -error_code           error_code\
            -rx_timestamp         rx_timestamp_sec\
            -rx_time_nanoseconds  rx_timestamp_ns\
            -pkt_content          pkt_content\
            -pkt_size             pkt_size]

    set error_reason [wrptp::tee_recv_error_reason $error_code]

    if {[info exists pkt_content]} {

        set hex $pkt_content

        pbLib::decode_wrptp_pkt\
                 -packet                         $hex\
                 -dest_mac                       recvd_dest_mac\
                 -src_mac                        recvd_src_mac\
                 -eth_type                       recvd_eth_type\
                 -vlan_id                        recvd_vlan_id\
                 -vlan_priority                  recvd_vlan_priority\
                 -vlan_dei                       recvd_vlan_dei\
                 -src_ip                         recvd_src_ip\
                 -dest_ip                        recvd_dest_ip\
                 -src_port                       recvd_src_port\
                 -dest_port                      recvd_dest_port\
                 -udp_length                     recvd_udp_length\
                 -udp_checksum                   recvd_udp_checksum\
                 -transport_specific             recvd_transport_specific\
                 -message_type                   recvd_message_type\
                 -version_ptp                    recvd_version_ptp\
                 -message_length                 recvd_message_length\
                 -domain_number                  recvd_domain_number\
                 -alternate_master_flag          recvd_alternate_master_flag\
                 -two_step_flag                  recvd_two_step_flag\
                 -unicast_flag                   recvd_unicast_flag\
                 -profile_specific1              recvd_profile_specific1\
                 -profile_specific2              recvd_profile_specific2\
                 -leap61                         recvd_leap61\
                 -leap59                         recvd_leap59\
                 -current_utc_offset_valid       recvd_current_utc_offset_valid\
                 -ptp_timescale                  recvd_ptp_timescale\
                 -time_traceable                 recvd_time_traceable\
                 -freq_traceable                 recvd_freq_traceable\
                 -correction_field               recvd_correction_field\
                 -src_port_number                recvd_src_port_number\
                 -src_clock_identity             recvd_src_clock_identity\
                 -sequence_id                    recvd_sequence_id\
                 -control_field                  recvd_control_field\
                 -log_message_interval           recvd_log_message_interval\
                 -requesting_port_number         recvd_requesting_port_number\
                 -requesting_clock_identity      recvd_requesting_clock_identity\
                 -request_receipt_timestamp_sec  recvd_request_receipt_timestamp_sec\
                 -request_receipt_timestamp_ns   recvd_request_receipt_timestamp_ns

    }

    return $result

}


################################################################################
#                                                                              #
#  PROCEDURE NAME         :  wrptp::recv_pdelay_resp_followup                  #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Reception port                 #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   filter_id                     : (Mandatory) Filter id                      #
#                                                                              #
#   reset_count                   : (Optional)  Reset Count                    #
#                                               (Default: ON)                  #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                                                              #
#   transport_specific            : (Optional)  Transport Specific             #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default: 10)                  #
#                                                                              #
#   version_ptp                   : (Optional)  PTP version number             #
#                                                                              #
#   message_length                : (Optional)  PTP Annouce message length     #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP Timescale Flag             #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default: 1)                   #
#                                                                              #
#   timeout                       : (Optional)  Frame timeout (in seconds)     #
#                                               (Default: 5)                   #
#                                                                              #
#  OUTPUT PARAMETERS      :                                                    #
#                                                                              #
#   recvd_dest_mac                : (Optional)  Destination MAC dddress.       #
#                                                                              #
#   recvd_src_mac                 : (Optional)  Source MAC address.            #
#                                                                              #
#   recvd_two_step_flag           : (Optional)  Two Step Flag                  #
#                                                                              #
#   recvd_unicast_flag            : (Optional)  Unicast Flag                   #
#                                                                              #
#   recvd_correction_field        : (Optional)  Correction Field value         #
#                                                                              #
#   recvd_src_port_number         : (Optional)  Source port number             #
#                                                                              #
#   recvd_src_clock_identity      : (Optional)  Source clock identity          #
#                                                                              #
#   recvd_sequence_id             : (Optional)  Sequence ID                    #
#                                                                              #
#   recvd_log_message_interval    : (Optional)  Log message interval           #
#                                                                              #
#   recvd_response_origin_timestamp_sec: (Optional)  Recvd Response Origin Timestamp Sec #
#                                                                              #
#   recvd_response_origin_timestamp_ns: (Optional)  Recvd Response Origin Timestamp Ns #
#                                                                              #
#   recvd_requesting_clock_identity: (Optional)  Recvd Requesting Clock Identity #
#                                                                              #
#   recvd_requesting_port_number  : (Optional)  Recvd Requesting Port Number   #
#                                                                              #
#   rx_timestamp_sec              : (Optional)  Packet's rx time in seconds    #
#                                                                              #
#   rx_timestamp_ns               : (Optional)  Packet's rx time in ns         #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is received as         #
#                                         expected)                            #
#                                                                              #
#                         : 0 on failure (If the packet is not received        #
#                                         as expected)                         #
#                                                                              #
#  DEFINITION             : This function is used to receive                   #
#                           PTP pdelay resp followup message                   #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#   wrptp::recv_pdelay_resp_followup\                                          #
#    -port_num                         $port_num\                              #
#    -filter_id                        $filter_id\                             #
#    -reset_count                      $reset_count\                           #
#    -eth_type                         $eth_type\                              #
#    -vlan_id                          $vlan_id\                               #
#    -dest_ip                          $dest_ip\                               #
#    -src_ip                           $src_ip\                                #
#    -ip_checksum                      $ip_checksum\                           #
#    -dest_port                        $dest_port\                             #
#    -src_port                         $src_port\                              #
#    -udp_length                       $udp_length\                            #
#    -udp_checksum                     $udp_checksum\                          #
#    -transport_specific               $transport_specific\                    #
#    -message_type                     $message_type\                          #
#    -version_ptp                      $version_ptp\                           #
#    -message_length                   $message_length\                        #
#    -domain_number                    $domain_number\                         #
#    -alternate_master_flag            $alternate_master_flag\                 #
#    -profile_specific1                $profile_specific1\                     #
#    -profile_specific2                $profile_specific2\                     #
#    -leap61                           $leap61\                                #
#    -leap59                           $leap59\                                #
#    -current_utc_offset_valid         $current_utc_offset_valid\              #
#    -ptp_timescale                    $ptp_timescale\                         #
#    -time_traceable                   $time_traceable\                        #
#    -freq_traceable                   $freq_traceable\                        #
#    -control_field                    $control_field\                         #
#    -count                            $count\                                 #
#    -interval                         $interval                               #
#    -recvd_dest_mac                   recvd_dest_mac\                         #
#    -recvd_src_mac                    recvd_src_mac\                          #
#    -recvd_two_step_flag              recvd_two_step_flag\                    #
#    -recvd_unicast_flag               recvd_unicast_flag\                     #
#    -recvd_correction_field           recvd_correction_field\                 #
#    -recvd_src_port_number            recvd_src_port_identity\                #
#    -recvd_src_clock_identity         recvd_src_clock_identity\               #
#    -recvd_sequence_id                recvd_sequence_id\                      #
#    -recvd_log_message_interval       recvd_message_interval\                 #
#    -recvd_response_origin_timestamp_sec  recvd_response_origin_timestamp_sec\#
#    -recvd_response_origin_timestamp_ns  recvd_response_origin_timestamp_ns\  #
#    -recvd_requesting_clock_identity  recvd_requesting_clock_identity\        #
#    -recvd_requesting_port_number     recvd_requesting_port_number\           #
#    -rx_timestamp_sec                 rx_timestamp_sec\                       #
#    -rx_timestamp_ns                  rx_timestamp_ns                         #
#                                                                              #
################################################################################
proc wrptp::recv_pdelay_resp_followup {args} {

    array unset ::validation_param

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "wrptp::recv_pdelay_resp_followup: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #Reception port
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "wrptp::recv_pdelay_resp_followup: Missing Argument -> port_num"
        return 0
    }

    #Filter id
    if {[info exists param(-filter_id)]} {

        set filter_id $param(-filter_id)
    } else {

        ERRLOG -msg "wrptp::recv_pdelay_resp_followup: Missing Argument -> filter_id"
        return 0
    }

    #Reset Count
    if {[info exists param(-reset_count)]} {

        set reset_count $param(-reset_count)
    } else {

        set reset_count ON
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        set ::validation_param(eth_type) $param(-eth_type)
    } else {

        if { $::wrptp_vlan_encap == "true" } {
            set ::validation_param(eth_type) 8100
        } else {
            if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
                set ::validation_param(eth_type) 0800
            } else {
                set ::validation_param(eth_type) 88F7
            }
        }
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
        set ::validation_param(vlan_id) $param(-vlan_id)
    } else {

        if { $::wrptp_vlan_encap == "true" } {
            set ::validation_param(vlan_id) $::wrptp_vlan_id
        } else {
            set ::validation_param(vlan_id) "dont_care"
        }
    }

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
        set ::validation_param(dest_ip) $param(-dest_ip)
    } else {

        set ::validation_param(dest_ip) "dont_care"
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
        set ::validation_param(src_ip) $param(-src_ip)
    } else {

        set ::validation_param(src_ip) "dont_care"
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
        set ::validation_param(ip_checksum) $param(-ip_checksum)
    } else {

        set ::validation_param(ip_checksum) "dont_care"
    }

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
        set ::validation_param(dest_port) $param(-dest_port)
    } else {

        set ::validation_param(dest_port) "dont_care"
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
        set ::validation_param(src_port) $param(-src_port)
    } else {

        set ::validation_param(src_port) "dont_care"
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
        set ::validation_param(udp_length) $param(-udp_length)
    } else {

        set ::validation_param(udp_length) "dont_care"
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
        set ::validation_param(udp_checksum) $param(-udp_checksum)
    } else {

        set ::validation_param(udp_checksum) "dont_care"
    }

    #Transport Specific
    if {[info exists param(-transport_specific)]} {

        set transport_specific $param(-transport_specific)
        set ::validation_param(transport_specific) $param(-transport_specific)
    } else {

        set ::validation_param(transport_specific) "dont_care"
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        set ::validation_param(message_type) $param(-message_type)
    } else {

        set ::validation_param(message_type) 10
    }

    #PTP version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        set ::validation_param(version_ptp) $param(-version_ptp)
    } else {

        set ::validation_param(version_ptp) "dont_care"
    }

    #PTP Annouce message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        set ::validation_param(message_length) $param(-message_length)
    } else {

        set ::validation_param(message_length) "dont_care"
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        set ::validation_param(domain_number) $param(-domain_number)
    } else {

        set ::validation_param(domain_number) "dont_care"
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        set ::validation_param(alternate_master_flag) $param(-alternate_master_flag)
    } else {

        set ::validation_param(alternate_master_flag) "dont_care"
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        set ::validation_param(profile_specific1) $param(-profile_specific1)
    } else {

        set ::validation_param(profile_specific1) "dont_care"
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        set ::validation_param(profile_specific2) $param(-profile_specific2)
    } else {

        set ::validation_param(profile_specific2) "dont_care"
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        set ::validation_param(leap61) $param(-leap61)
    } else {

        set ::validation_param(leap61) "dont_care"
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        set ::validation_param(leap59) $param(-leap59)
    } else {

        set ::validation_param(leap59) "dont_care"
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        set ::validation_param(current_utc_offset_valid) $param(-current_utc_offset_valid)
    } else {

        set ::validation_param(current_utc_offset_valid) "dont_care"
    }

    #PTP Timescale Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        set ::validation_param(ptp_timescale) $param(-ptp_timescale)
    } else {

        set ::validation_param(ptp_timescale) "dont_care"
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        set ::validation_param(time_traceable) $param(-time_traceable)
    } else {

        set ::validation_param(time_traceable) "dont_care"
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        set ::validation_param(freq_traceable) $param(-freq_traceable)
    } else {

        set ::validation_param(freq_traceable) "dont_care"
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        set ::validation_param(sequence_id) $param(-sequence_id)
    } else {

        set ::validation_param(sequence_id) "dont_care"
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        set ::validation_param(control_field) $param(-control_field)
    } else {

        set ::validation_param(control_field) "dont_care"
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame timeout
    if {[info exists param(-timeout)]} {

        set timeout $param(-timeout)
    } else {

        set timeout 5
    }

    #OUTPUT PARAMETERS

    #Destination MAC dddress
    if {[info exists param(-recvd_dest_mac)]} {

        upvar $param(-recvd_dest_mac) recvd_dest_mac
        set recvd_dest_mac ""
    }

    #Source MAC address
    if {[info exists param(-recvd_src_mac)]} {

        upvar $param(-recvd_src_mac) recvd_src_mac
        set recvd_src_mac ""
    }

    #Two Step Flag
    if {[info exists param(-recvd_two_step_flag)]} {

        upvar $param(-recvd_two_step_flag) recvd_two_step_flag
        set recvd_two_step_flag ""
    }

    #Unicast Flag
    if {[info exists param(-recvd_unicast_flag)]} {

        upvar $param(-recvd_unicast_flag) recvd_unicast_flag
        set recvd_unicast_flag ""
    }

    #Correction Field value
    if {[info exists param(-recvd_correction_field)]} {

        upvar $param(-recvd_correction_field) recvd_correction_field
        set recvd_correction_field ""
    }

    #Source port number
    if {[info exists param(-recvd_src_port_number)]} {

        upvar $param(-recvd_src_port_number) recvd_src_port_number
        set recvd_src_port_number ""
    }

    #Source clock identity
    if {[info exists param(-recvd_src_clock_identity)]} {

        upvar $param(-recvd_src_clock_identity) recvd_src_clock_identity
        set recvd_src_clock_identity ""
    }

    #Sequence ID
    if {[info exists param(-recvd_sequence_id)]} {

        upvar $param(-recvd_sequence_id) recvd_sequence_id
        set recvd_sequence_id ""
    }

    #Log message interval
    if {[info exists param(-recvd_log_message_interval)]} {

        upvar $param(-recvd_log_message_interval) recvd_log_message_interval
        set recvd_log_message_interval ""
    }

    #Recvd Requesting Port Number
    if {[info exists param(-recvd_requesting_port_number)]} {

        upvar $param(-recvd_requesting_port_number) recvd_requesting_port_number
        set recvd_requesting_port_number ""
    }

    #Received packet timestamp in seconds
    if {[info exists param(-rx_timestamp_sec)]} {

        upvar $param(-rx_timestamp_sec) rx_timestamp_sec
        set rx_timestamp_sec ""
    }

    #Received packet timestamp in nanoseconds
    if {[info exists param(-rx_timestamp_ns)]} {

        upvar $param(-rx_timestamp_ns) rx_timestamp_ns
        set rx_timestamp_ns ""
    }

    #Error Reason
    if {[info exists param(-error_reason)]} {

        upvar $param(-error_reason) error_reason
        set error_reason ""
    }

    #LOGIC GOES HERE
    #Timeout in millisec
    set timeout [expr int($timeout*1000)]
    set error_code 0

    print_timeout $timeout

    set result [pltLib::recv_periodic\
            -session_id           $session_id\
            -port_num             $port_num\
            -filter_id            $filter_id\
            -timeout              $timeout\
            -reset_count          $reset_count\
            -validation_handler   "WRPTP"\
            -error_code           error_code\
            -rx_timestamp         rx_timestamp_sec\
            -rx_time_nanoseconds  rx_timestamp_ns\
            -pkt_content          pkt_content\
            -pkt_size             pkt_size]

    set error_reason [wrptp::tee_recv_error_reason $error_code]

    if {[info exists pkt_content]} {

        set hex $pkt_content

        pbLib::decode_wrptp_pkt\
                 -packet                         $hex\
                 -dest_mac                       recvd_dest_mac\
                 -src_mac                        recvd_src_mac\
                 -eth_type                       recvd_eth_type\
                 -vlan_id                        recvd_vlan_id\
                 -vlan_priority                  recvd_vlan_priority\
                 -vlan_dei                       recvd_vlan_dei\
                 -src_ip                         recvd_src_ip\
                 -dest_ip                        recvd_dest_ip\
                 -src_port                       recvd_src_port\
                 -dest_port                      recvd_dest_port\
                 -udp_length                     recvd_udp_length\
                 -udp_checksum                   recvd_udp_checksum\
                 -transport_specific             recvd_transport_specific\
                 -message_type                   recvd_message_type\
                 -version_ptp                    recvd_version_ptp\
                 -message_length                 recvd_message_length\
                 -domain_number                  recvd_domain_number\
                 -alternate_master_flag          recvd_alternate_master_flag\
                 -two_step_flag                  recvd_two_step_flag\
                 -unicast_flag                   recvd_unicast_flag\
                 -profile_specific1              recvd_profile_specific1\
                 -profile_specific2              recvd_profile_specific2\
                 -leap61                         recvd_leap61\
                 -leap59                         recvd_leap59\
                 -current_utc_offset_valid       recvd_current_utc_offset_valid\
                 -ptp_timescale                  recvd_ptp_timescale\
                 -time_traceable                 recvd_time_traceable\
                 -freq_traceable                 recvd_freq_traceable\
                 -correction_field               recvd_correction_field\
                 -src_port_number                recvd_src_port_number\
                 -src_clock_identity             recvd_src_clock_identity\
                 -sequence_id                    recvd_sequence_id\
                 -control_field                  recvd_control_field\
                 -log_message_interval           recvd_log_message_interval\
                 -requesting_port_number         recvd_requesting_port_number\
                 -requesting_clock_identity      recvd_requesting_clock_identity\
                 -response_origin_timestamp_sec  recvd_response_origin_timestamp_sec\
                 -response_origin_timestamp_ns   recvd_response_origin_timestamp_ns

    }

    return $result

}


################################################################################
#                                                                              #
#  PROCEDURE NAME         :  wrptp::recv_signal                                #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Reception port                 #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   filter_id                     : (Mandatory) Filter id                      #
#                                                                              #
#   reset_count                   : (Optional)  Reset Count                    #
#                                               (Default: ON)                  #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                                                              #
#   transport_specific            : (Optional)  Transport Specific             #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default: 12)                  #
#                                                                              #
#   version_ptp                   : (Optional)  PTP version number             #
#                                                                              #
#   message_length                : (Optional)  PTP Annouce message length     #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP Timescale Flag             #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                                                              #
#                                                                              #
#  (PTP TLVs)                                                                  #
#                                                                              #
#   tlv_type                      :  (Optional)  TLV Type                      #
#                                                                              #
#   tlv_length                    :  (Optional)  TLV length                    #
#                                                                              #
#   organization_id               :  (Optional) Organization id                #
#                                                                              #
#   magic_number                  :  (Optional) Magic number                   #
#                                                                              #
#   version_number                :  (Optional) Version number                 #
#                                                                              #
#   message_id                    :  (Optional) Message id                     #
#                                                                              #
#   cal_send_pattern              :  (Optional) Cal send pattern               #
#                                                                              #
#   cal_retry                     :  (Optional) Cal retry                      #
#                                                                              #
#   cal_period                    :  (Optional) Cal period                     #
#                                                                              #
#   delta_tx                      :  (Optional) Delta tx                       #
#                                                                              #
#   delta_rx                      :  (Optional) Delta rx                       #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default: 1)                   #
#                                                                              #
#   timeout                       : (Optional)  Frame timeout (in seconds)     #
#                                               (Default: 5)                   #
#                                                                              #
#  OUTPUT PARAMETERS      :                                                    #
#                                                                              #
#   recvd_dest_mac                : (Optional)  Destination MAC dddress.       #
#                                                                              #
#   recvd_src_mac                 : (Optional)  Source MAC address.            #
#                                                                              #
#   recvd_two_step_flag           : (Optional)  Two Step Flag                  #
#                                                                              #
#   recvd_unicast_flag            : (Optional)  Unicast Flag                   #
#                                                                              #
#   recvd_correction_field        : (Optional)  Correction Field value         #
#                                                                              #
#   recvd_src_port_number         : (Optional)  Source port number             #
#                                                                              #
#   recvd_src_clock_identity      : (Optional)  Source clock identity          #
#                                                                              #
#   recvd_sequence_id             : (Optional)  Sequence ID                    #
#                                                                              #
#   recvd_log_message_interval    : (Optional)  Log message interval           #
#                                                                              #
#   recvd_target_clock_identity   : (Optional)  Recvd Target Clock Identity    #
#                                                                              #
#   recvd_target_port_number      : (Optional)  Recvd Target Port Number       #
#                                                                              #
#   rx_timestamp_sec              : (Optional)  Packet's rx time in seconds    #
#                                                                              #
#   rx_timestamp_ns               : (Optional)  Packet's rx time in ns         #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is received as         #
#                                         expected)                            #
#                                                                              #
#                         : 0 on failure (If the packet is not received        #
#                                         as expected)                         #
#                                                                              #
#  DEFINITION             : This function is used to receive                   #
#                           PTP signal message                                 #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#   wrptp::recv_signal\                                                        #
#    -port_num                         $port_num\                              #
#    -filter_id                        $filter_id\                             #
#    -reset_count                      $reset_count\                           #
#    -eth_type                         $eth_type\                              #
#    -vlan_id                          $vlan_id\                               #
#    -dest_ip                          $dest_ip\                               #
#    -src_ip                           $src_ip\                                #
#    -ip_checksum                      $ip_checksum\                           #
#    -dest_port                        $dest_port\                             #
#    -src_port                         $src_port\                              #
#    -udp_length                       $udp_length\                            #
#    -udp_checksum                     $udp_checksum\                          #
#    -transport_specific               $transport_specific\                    #
#    -message_type                     $message_type\                          #
#    -version_ptp                      $version_ptp\                           #
#    -message_length                   $message_length\                        #
#    -domain_number                    $domain_number\                         #
#    -alternate_master_flag            $alternate_master_flag\                 #
#    -profile_specific1                $profile_specific1\                     #
#    -profile_specific2                $profile_specific2\                     #
#    -leap61                           $leap61\                                #
#    -leap59                           $leap59\                                #
#    -current_utc_offset_valid         $current_utc_offset_valid\              #
#    -ptp_timescale                    $ptp_timescale\                         #
#    -time_traceable                   $time_traceable\                        #
#    -freq_traceable                   $freq_traceable\                        #
#    -control_field                    $control_field\                         #
#    -tlv_type                         $tlv_type\                              #
#    -tlv_length                       $tlv_length\                            #
#    -organization_id                  $organization_id\                       #
#    -magic_number                     $magic_number\                          #
#    -version_number                   $version_number\                        #
#    -message_id                       $message_id\                            #
#    -cal_send_pattern                 $cal_send_pattern\                      #
#    -cal_retry                        $cal_retry\                             #
#    -cal_period                       $cal_period\                            #
#    -delta_tx                         $delta_tx\                              #
#    -delta_rx                         $delta_rx                               #
#    -count                            $count\                                 #
#    -timeout                          $timeout\                               #
#    -recvd_dest_mac                   recvd_dest_mac\                         #
#    -recvd_src_mac                    recvd_src_mac\                          #
#    -recvd_two_step_flag              recvd_two_step_flag\                    #
#    -recvd_unicast_flag               recvd_unicast_flag\                     #
#    -recvd_correction_field           recvd_correction_field\                 #
#    -recvd_src_port_number            recvd_src_port_identity\                #
#    -recvd_src_clock_identity         recvd_src_clock_identity\               #
#    -recvd_sequence_id                recvd_sequence_id\                      #
#    -recvd_log_message_interval       recvd_message_interval\                 #
#    -recvd_target_clock_identity      recvd_target_clock_identity\            #
#    -recvd_target_port_number         recvd_target_port_number\               #
#    -rx_timestamp_sec                 rx_timestamp_sec\                       #
#    -rx_timestamp_ns                  rx_timestamp_ns                         #
#                                                                              #
################################################################################
proc wrptp::recv_signal {args} {

    array unset ::validation_param

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "wrptp::recv_signal: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #Reception port
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "wrptp::recv_signal: Missing Argument -> port_num"
        return 0
    }

    #Filter id
    if {[info exists param(-filter_id)]} {

        set filter_id $param(-filter_id)
    } else {

        ERRLOG -msg "wrptp::recv_signal: Missing Argument -> filter_id"
        return 0
    }

    #Reset Count
    if {[info exists param(-reset_count)]} {

        set reset_count $param(-reset_count)
    } else {

        set reset_count ON
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        set ::validation_param(eth_type) $param(-eth_type)
    } else {

        if { $::wrptp_vlan_encap == "true" } {
            set ::validation_param(eth_type) 8100
        } else {
            if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
                set ::validation_param(eth_type) 0800
            } else {
                set ::validation_param(eth_type) 88F7
            }
        }
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
        set ::validation_param(vlan_id) $param(-vlan_id)
    } else {

        if { $::wrptp_vlan_encap == "true" } {
            set ::validation_param(vlan_id) $::wrptp_vlan_id
        } else {
            set ::validation_param(vlan_id) "dont_care"
        }
    }

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
        set ::validation_param(dest_ip) $param(-dest_ip)
    } else {

        set ::validation_param(dest_ip) "dont_care"
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
        set ::validation_param(src_ip) $param(-src_ip)
    } else {

        set ::validation_param(src_ip) "dont_care"
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
        set ::validation_param(ip_checksum) $param(-ip_checksum)
    } else {

        set ::validation_param(ip_checksum) "dont_care"
    }

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
        set ::validation_param(dest_port) $param(-dest_port)
    } else {

        set ::validation_param(dest_port) "dont_care"
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
        set ::validation_param(src_port) $param(-src_port)
    } else {

        set ::validation_param(src_port) "dont_care"
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
        set ::validation_param(udp_length) $param(-udp_length)
    } else {

        set ::validation_param(udp_length) "dont_care"
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
        set ::validation_param(udp_checksum) $param(-udp_checksum)
    } else {

        set ::validation_param(udp_checksum) "dont_care"
    }

    #Transport Specific
    if {[info exists param(-transport_specific)]} {

        set transport_specific $param(-transport_specific)
        set ::validation_param(transport_specific) $param(-transport_specific)
    } else {

        set ::validation_param(transport_specific) "dont_care"
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        set ::validation_param(message_type) $param(-message_type)
    } else {

        set ::validation_param(message_type) 12
    }

    #PTP version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        set ::validation_param(version_ptp) $param(-version_ptp)
    } else {

        set ::validation_param(version_ptp) "dont_care"
    }

    #PTP Annouce message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        set ::validation_param(message_length) $param(-message_length)
    } else {

        set ::validation_param(message_length) "dont_care"
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        set ::validation_param(domain_number) $param(-domain_number)
    } else {

        set ::validation_param(domain_number) "dont_care"
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        set ::validation_param(alternate_master_flag) $param(-alternate_master_flag)
    } else {

        set ::validation_param(alternate_master_flag) "dont_care"
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        set ::validation_param(profile_specific1) $param(-profile_specific1)
    } else {

        set ::validation_param(profile_specific1) "dont_care"
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        set ::validation_param(profile_specific2) $param(-profile_specific2)
    } else {

        set ::validation_param(profile_specific2) "dont_care"
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        set ::validation_param(leap61) $param(-leap61)
    } else {

        set ::validation_param(leap61) "dont_care"
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        set ::validation_param(leap59) $param(-leap59)
    } else {

        set ::validation_param(leap59) "dont_care"
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        set ::validation_param(current_utc_offset_valid) $param(-current_utc_offset_valid)
    } else {

        set ::validation_param(current_utc_offset_valid) "dont_care"
    }

    #PTP Timescale Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        set ::validation_param(ptp_timescale) $param(-ptp_timescale)
    } else {

        set ::validation_param(ptp_timescale) "dont_care"
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        set ::validation_param(time_traceable) $param(-time_traceable)
    } else {

        set ::validation_param(time_traceable) "dont_care"
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        set ::validation_param(freq_traceable) $param(-freq_traceable)
    } else {

        set ::validation_param(freq_traceable) "dont_care"
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        set ::validation_param(sequence_id) $param(-sequence_id)
    } else {

        set ::validation_param(sequence_id) "dont_care"
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        set ::validation_param(control_field) $param(-control_field)
    } else {

        set ::validation_param(control_field) "dont_care"
    }

    #Tlv Type
    if {[info exists param(-tlv_type)]} {

        set tlv_type $param(-tlv_type)
        set ::validation_param(tlv_type) $param(-tlv_type)
    } else {

        set ::validation_param(tlv_type) "dont_care"
    }

    #Tlv Length
    if {[info exists param(-tlv_length)]} {

        set tlv_length $param(-tlv_length)
        set ::validation_param(tlv_length) $param(-tlv_length)
    } else {

        set ::validation_param(tlv_length) "dont_care"
    }

    #Organization id
    if {[info exists param(-organization_id)]} {

        set organization_id $param(-organization_id)
        set ::validation_param(organization_id) $param(-organization_id)
    } else {

        set ::validation_param(organization_id) "dont_care"
    }

    #Magic number
    if {[info exists param(-magic_number)]} {

        set magic_number $param(-magic_number)
        set ::validation_param(magic_number) $param(-magic_number)
    } else {

        set ::validation_param(magic_number) "dont_care"
    }

    #Version number
    if {[info exists param(-version_number)]} {

        set version_number $param(-version_number)
        set ::validation_param(version_number) $param(-version_number)
    } else {

        set ::validation_param(version_number) "dont_care"
    }

    #Message id
    if {[info exists param(-message_id)]} {

        set message_id $param(-message_id)
        set ::validation_param(message_id) $param(-message_id)
    } else {

        set ::validation_param(message_id) "dont_care"
    }

    #Cal send pattern
    if {[info exists param(-cal_send_pattern)]} {

        set cal_send_pattern $param(-cal_send_pattern)
        set ::validation_param(cal_send_pattern) $param(-cal_send_pattern)
    } else {

        set ::validation_param(cal_send_pattern) "dont_care"
    }

    #Cal retry
    if {[info exists param(-cal_retry)]} {

        set cal_retry $param(-cal_retry)
        set ::validation_param(cal_retry) $param(-cal_retry)
    } else {

        set ::validation_param(cal_retry) "dont_care"
    }

    #Cal period
    if {[info exists param(-cal_period)]} {

        set cal_period $param(-cal_period)
        set ::validation_param(cal_period) $param(-cal_period)
    } else {

        set ::validation_param(cal_period) "dont_care"
    }

    #Delta tx
    if {[info exists param(-delta_tx)]} {

        set delta_tx $param(-delta_tx)
        set ::validation_param(delta_tx) $param(-delta_tx)
    } else {

        set ::validation_param(delta_tx) "dont_care"
    }

    #Delta rx
    if {[info exists param(-delta_rx)]} {

        set delta_rx $param(-delta_rx)
        set ::validation_param(delta_rx) $param(-delta_rx)
    } else {

        set ::validation_param(delta_rx) "dont_care"
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame timeout
    if {[info exists param(-timeout)]} {

        set timeout $param(-timeout)
    } else {

        set timeout 5
    }

    #OUTPUT PARAMETERS
    #all the fields in the matched packet
    foreach key [array names param] {

        set var [string trimleft $key -]

        if [regexp {^recvd_.*} $var] {

            upvar [set param($key)] $var
            set $var ""
        }
    }

    #Received packet timestamp in seconds
    if {[info exists param(-rx_timestamp_sec)]} {

        upvar $param(-rx_timestamp_sec) rx_timestamp_sec
        set rx_timestamp_sec ""
    }

    #Received packet timestamp in nanoseconds
    if {[info exists param(-rx_timestamp_ns)]} {

        upvar $param(-rx_timestamp_ns) rx_timestamp_ns
        set rx_timestamp_ns ""
    }

    #Error_reason
    if {[info exists param(-error_reason)]} {

        upvar $param(-error_reason) error_reason
        set error_reason ""
    }

    #LOGIC GOES HERE
    #Timeout in millisec
    set timeout [expr int($timeout*1000)]
    set error_code 0

    print_timeout $timeout

    set result [pltLib::recv_periodic\
            -session_id           $session_id\
            -port_num             $port_num\
            -filter_id            $filter_id\
            -timeout              $timeout\
            -reset_count          $reset_count\
            -validation_handler   "WRPTP"\
            -error_code           error_code\
            -rx_timestamp         rx_timestamp_sec\
            -rx_time_nanoseconds  rx_timestamp_ns\
            -pkt_content          pkt_content\
            -pkt_size             pkt_size]

    set error_reason [wrptp::tee_recv_error_reason $error_code]

    if {[info exists pkt_content]} {

        set hex $pkt_content

        pbLib::decode_wrptp_pkt\
                 -packet                         $hex\
                 -dest_mac                       recvd_dest_mac\
                 -src_mac                        recvd_src_mac\
                 -eth_type                       recvd_eth_type\
                 -vlan_id                        recvd_vlan_id\
                 -vlan_priority                  recvd_vlan_priority\
                 -vlan_dei                       recvd_vlan_dei\
                 -src_ip                         recvd_src_ip\
                 -dest_ip                        recvd_dest_ip\
                 -src_port                       recvd_src_port\
                 -dest_port                      recvd_dest_port\
                 -udp_length                     recvd_udp_length\
                 -udp_checksum                   recvd_udp_checksum\
                 -transport_specific             recvd_transport_specific\
                 -message_type                   recvd_message_type\
                 -version_ptp                    recvd_version_ptp\
                 -message_length                 recvd_message_length\
                 -domain_number                  recvd_domain_number\
                 -alternate_master_flag          recvd_alternate_master_flag\
                 -two_step_flag                  recvd_two_step_flag\
                 -unicast_flag                   recvd_unicast_flag\
                 -profile_specific1              recvd_profile_specific1\
                 -profile_specific2              recvd_profile_specific2\
                 -leap61                         recvd_leap61\
                 -leap59                         recvd_leap59\
                 -current_utc_offset_valid       recvd_current_utc_offset_valid\
                 -ptp_timescale                  recvd_ptp_timescale\
                 -time_traceable                 recvd_time_traceable\
                 -freq_traceable                 recvd_freq_traceable\
                 -correction_field               recvd_correction_field\
                 -src_port_number                recvd_src_port_number\
                 -src_clock_identity             recvd_src_clock_identity\
                 -sequence_id                    recvd_sequence_id\
                 -control_field                  recvd_control_field\
                 -log_message_interval           recvd_log_message_interval\
                 -target_port_number             recvd_target_port_number\
                 -target_clock_identity          recvd_target_clock_identity\
                 -tlv_type                       recvd_tlv_type\
                 -tlv_length                     recvd_tlv_length\
                 -organization_id                recvd_organization_id\
                 -magic_number                   recvd_magic_number\
                 -version_number                 recvd_version_number\
                 -message_id                     recvd_message_id\
                 -cal_send_pattern               recvd_cal_send_pattern\
                 -cal_retry                      recvd_cal_retry\
                 -cal_period                     recvd_cal_period\
                 -delta_tx                       recvd_delta_tx\
                 -delta_rx                       recvd_delta_rx

    }

    return $result

}


################################################################################
#                                                                              #
#  PROCEDURE NAME         :  wrptp::recv_mgmt                                  #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Reception port                 #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   filter_id                     : (Mandatory) Filter id                      #
#                                                                              #
#   reset_count                   : (Optional)  Reset Count                    #
#                                               (Default: ON)                  #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                                                              #
#   transport_specific            : (Optional)  Transport Specific             #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default: 13)                  #
#                                                                              #
#   version_ptp                   : (Optional)  PTP version number             #
#                                                                              #
#   message_length                : (Optional)  PTP Annouce message length     #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP Timescale Flag             #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                                                              #
#   starting_boundary_hops        : (Optional)  Starting Boundary Hops         #
#                                                                              #
#   boundary_hops                 : (Optional)  Boundary Hops                  #
#                                                                              #
#   action_field                  : (Optional)  Action Field                   #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default: 1)                   #
#                                                                              #
#   timeout                       : (Optional)  Frame timeout (in seconds)     #
#                                               (Default: 5)                   #
#                                                                              #
#  OUTPUT PARAMETERS      :                                                    #
#                                                                              #
#   recvd_dest_mac                : (Optional)  Destination MAC dddress.       #
#                                                                              #
#   recvd_src_mac                 : (Optional)  Source MAC address.            #
#                                                                              #
#   recvd_two_step_flag           : (Optional)  Two Step Flag                  #
#                                                                              #
#   recvd_unicast_flag            : (Optional)  Unicast Flag                   #
#                                                                              #
#   recvd_correction_field        : (Optional)  Correction Field value         #
#                                                                              #
#   recvd_src_port_number         : (Optional)  Source port number             #
#                                                                              #
#   recvd_src_clock_identity      : (Optional)  Source clock identity          #
#                                                                              #
#   recvd_sequence_id             : (Optional)  Sequence ID                    #
#                                                                              #
#   recvd_log_message_interval    : (Optional)  Log message interval           #
#                                                                              #
#   recvd_target_clock_identity   : (Optional)  Recvd Target Clock Identity    #
#                                                                              #
#   recvd_target_port_number      : (Optional)  Recvd Target Port Number       #
#                                                                              #
#   rx_timestamp_sec              : (Optional)  Packet's rx time in seconds    #
#                                                                              #
#   rx_timestamp_ns               : (Optional)  Packet's rx time in ns         #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is received as         #
#                                         expected)                            #
#                                                                              #
#                         : 0 on failure (If the packet is not received        #
#                                         as expected)                         #
#                                                                              #
#  DEFINITION             : This function is used to receive                   #
#                           PTP mgmt message                                   #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       wrptp::recv_mgmt\                                                      #
#           -port_num                         $port_num\                       #
#           -filter_id                        $filter_id\                      #
#           -reset_count                      $reset_count\                    #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -transport_specific               $transport_specific\             #
#           -message_type                     $message_type\                   #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -control_field                    $control_field\                  #
#           -starting_boundary_hops           $starting_boundary_hops\         #
#           -boundary_hops                    $boundary_hops\                  #
#           -action_field                     $action_field\                   #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#           -recvd_dest_mac                   recvd_dest_mac\                  #
#           -recvd_src_mac                    recvd_src_mac\                   #
#           -recvd_two_step_flag              recvd_two_step_flag\             #
#           -recvd_unicast_flag               recvd_unicast_flag\              #
#           -recvd_correction_field           recvd_correction_field\          #
#           -recvd_src_port_number            recvd_src_port_identity\         #
#           -recvd_src_clock_identity         recvd_src_clock_identity\        #
#           -recvd_sequence_id                recvd_sequence_id\               #
#           -recvd_log_message_interval       recvd_message_interval\          #
#           -recvd_target_clock_identity      recvd_target_clock_identity\     #
#           -recvd_target_port_number         recvd_target_port_number\        #
#           -rx_timestamp_sec                 rx_timestamp_sec\                #
#           -rx_timestamp_ns                  rx_timestamp_ns                  #
#                                                                              #
################################################################################
proc wrptp::recv_mgmt {args} {

    array unset ::validation_param

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "wrptp::recv_mgmt: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #Reception port
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "wrptp::recv_mgmt: Missing Argument -> port_num"
        return 0
    }

    #Filter id
    if {[info exists param(-filter_id)]} {

        set filter_id $param(-filter_id)
    } else {

        ERRLOG -msg "wrptp::recv_mgmt: Missing Argument -> filter_id"
        return 0
    }

    #Reset Count
    if {[info exists param(-reset_count)]} {

        set reset_count $param(-reset_count)
    } else {

        set reset_count ON
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        set ::validation_param(eth_type) $param(-eth_type)
    } else {

        if { $::wrptp_vlan_encap == "true" } {
            set ::validation_param(eth_type) 8100
        } else {
            if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
                set ::validation_param(eth_type) 0800
            } else {
                set ::validation_param(eth_type) 88F7
            }
        }
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
        set ::validation_param(vlan_id) $param(-vlan_id)
    } else {

        if { $::wrptp_vlan_encap == "true" } {
            set ::validation_param(vlan_id) $::wrptp_vlan_id
        } else {
            set ::validation_param(vlan_id) "dont_care"
        }
    }

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
        set ::validation_param(dest_ip) $param(-dest_ip)
    } else {

        set ::validation_param(dest_ip) "dont_care"
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
        set ::validation_param(src_ip) $param(-src_ip)
    } else {

        set ::validation_param(src_ip) "dont_care"
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
        set ::validation_param(ip_checksum) $param(-ip_checksum)
    } else {

        set ::validation_param(ip_checksum) "dont_care"
    }

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
        set ::validation_param(dest_port) $param(-dest_port)
    } else {

        set ::validation_param(dest_port) "dont_care"
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
        set ::validation_param(src_port) $param(-src_port)
    } else {

        set ::validation_param(src_port) "dont_care"
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
        set ::validation_param(udp_length) $param(-udp_length)
    } else {

        set ::validation_param(udp_length) "dont_care"
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
        set ::validation_param(udp_checksum) $param(-udp_checksum)
    } else {

        set ::validation_param(udp_checksum) "dont_care"
    }

    #Transport Specific
    if {[info exists param(-transport_specific)]} {

        set transport_specific $param(-transport_specific)
        set ::validation_param(transport_specific) $param(-transport_specific)
    } else {

        set ::validation_param(transport_specific) "dont_care"
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        set ::validation_param(message_type) $param(-message_type)
    } else {

        set ::validation_param(message_type) 13
    }

    #PTP version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        set ::validation_param(version_ptp) $param(-version_ptp)
    } else {

        set ::validation_param(version_ptp) "dont_care"
    }

    #PTP Annouce message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        set ::validation_param(message_length) $param(-message_length)
    } else {

        set ::validation_param(message_length) "dont_care"
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        set ::validation_param(domain_number) $param(-domain_number)
    } else {

        set ::validation_param(domain_number) "dont_care"
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        set ::validation_param(alternate_master_flag) $param(-alternate_master_flag)
    } else {

        set ::validation_param(alternate_master_flag) "dont_care"
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        set ::validation_param(profile_specific1) $param(-profile_specific1)
    } else {

        set ::validation_param(profile_specific1) "dont_care"
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        set ::validation_param(profile_specific2) $param(-profile_specific2)
    } else {

        set ::validation_param(profile_specific2) "dont_care"
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        set ::validation_param(leap61) $param(-leap61)
    } else {

        set ::validation_param(leap61) "dont_care"
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        set ::validation_param(leap59) $param(-leap59)
    } else {

        set ::validation_param(leap59) "dont_care"
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        set ::validation_param(current_utc_offset_valid) $param(-current_utc_offset_valid)
    } else {

        set ::validation_param(current_utc_offset_valid) "dont_care"
    }

    #PTP Timescale Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        set ::validation_param(ptp_timescale) $param(-ptp_timescale)
    } else {

        set ::validation_param(ptp_timescale) "dont_care"
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        set ::validation_param(time_traceable) $param(-time_traceable)
    } else {

        set ::validation_param(time_traceable) "dont_care"
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        set ::validation_param(freq_traceable) $param(-freq_traceable)
    } else {

        set ::validation_param(freq_traceable) "dont_care"
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        set ::validation_param(sequence_id) $param(-sequence_id)
    } else {

        set ::validation_param(sequence_id) "dont_care"
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        set ::validation_param(control_field) $param(-control_field)
    } else {

        set ::validation_param(control_field) "dont_care"
    }

    #Starting Boundary Hops
    if {[info exists param(-starting_boundary_hops)]} {

        set starting_boundary_hops $param(-starting_boundary_hops)
        set ::validation_param(starting_boundary_hops) $param(-starting_boundary_hops)
    } else {

        set ::validation_param(starting_boundary_hops) "dont_care"
    }

    #Boundary Hops
    if {[info exists param(-boundary_hops)]} {

        set boundary_hops $param(-boundary_hops)
        set ::validation_param(boundary_hops) $param(-boundary_hops)
    } else {

        set ::validation_param(boundary_hops) "dont_care"
    }

    #Action Field
    if {[info exists param(-action_field)]} {

        set action_field $param(-action_field)
        set ::validation_param(action_field) $param(-action_field)
    } else {

        set ::validation_param(action_field) "dont_care"
    }

    #Tlv Type
    if {[info exists param(-tlv_type)]} {

        set tlv_type $param(-tlv_type)
        set ::validation_param(tlv_type) $param(-tlv_type)
    } else {

        set ::validation_param(tlv_type) "dont_care"
    }

    #Tlv Length
    if {[info exists param(-tlv_length)]} {

        set tlv_length $param(-tlv_length)
        set ::validation_param(tlv_length) $param(-tlv_length)
    } else {

        set ::validation_param(tlv_length) "dont_care"
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame timeout
    if {[info exists param(-timeout)]} {

        set timeout $param(-timeout)
    } else {

        set timeout 5
    }

    #OUTPUT PARAMETERS

    #Destination MAC dddress
    if {[info exists param(-recvd_dest_mac)]} {

        upvar $param(-recvd_dest_mac) recvd_dest_mac
        set recvd_dest_mac ""
    }

    #Source MAC address
    if {[info exists param(-recvd_src_mac)]} {

        upvar $param(-recvd_src_mac) recvd_src_mac
        set recvd_src_mac ""
    }

    #Two Step Flag
    if {[info exists param(-recvd_two_step_flag)]} {

        upvar $param(-recvd_two_step_flag) recvd_two_step_flag
        set recvd_two_step_flag ""
    }

    #Unicast Flag
    if {[info exists param(-recvd_unicast_flag)]} {

        upvar $param(-recvd_unicast_flag) recvd_unicast_flag
        set recvd_unicast_flag ""
    }

    #Correction Field value
    if {[info exists param(-recvd_correction_field)]} {

        upvar $param(-recvd_correction_field) recvd_correction_field
        set recvd_correction_field ""
    }

    #Source port number
    if {[info exists param(-recvd_src_port_number)]} {

        upvar $param(-recvd_src_port_number) recvd_src_port_number
        set recvd_src_port_number ""
    }

    #Source clock identity
    if {[info exists param(-recvd_src_clock_identity)]} {

        upvar $param(-recvd_src_clock_identity) recvd_src_clock_identity
        set recvd_src_clock_identity ""
    }

    #Sequence ID
    if {[info exists param(-recvd_sequence_id)]} {

        upvar $param(-recvd_sequence_id) recvd_sequence_id
        set recvd_sequence_id ""
    }

    #Log message interval
    if {[info exists param(-recvd_log_message_interval)]} {

        upvar $param(-recvd_log_message_interval) recvd_log_message_interval
        set recvd_log_message_interval ""
    }

    #Recvd Target Clock Identity
    if {[info exists param(-recvd_target_clock_identity)]} {

        upvar $param(-recvd_target_clock_identity) recvd_target_clock_identity
        set recvd_target_clock_identity ""
    }

    #Recvd Target Port Number
    if {[info exists param(-recvd_target_port_number)]} {

        upvar $param(-recvd_target_port_number) recvd_target_port_number
        set recvd_target_port_number ""
    }

    #Received packet timestamp in seconds
    if {[info exists param(-rx_timestamp_sec)]} {

        upvar $param(-rx_timestamp_sec) rx_timestamp_sec
        set rx_timestamp_sec ""
    }

    #Received packet timestamp in nanoseconds
    if {[info exists param(-rx_timestamp_ns)]} {

        upvar $param(-rx_timestamp_ns) rx_timestamp_ns
        set rx_timestamp_ns ""
    }

    #Error Reason
    if {[info exists param(-error_reason)]} {

        upvar $param(-error_reason) error_reason
        set error_reason ""
    }

    #LOGIC GOES HERE
    #Timeout in millisec
    set timeout [expr int($timeout*1000)]
    set error_code 0

    print_timeout $timeout

    set result [pltLib::recv_periodic\
            -session_id           $session_id\
            -port_num             $port_num\
            -filter_id            $filter_id\
            -timeout              $timeout\
            -reset_count          $reset_count\
            -validation_handler   "WRPTP"\
            -error_code           error_code\
            -rx_timestamp         rx_timestamp_sec\
            -rx_time_nanoseconds  rx_timestamp_ns\
            -pkt_content          pkt_content\
            -pkt_size             pkt_size]

    set error_reason [wrptp::tee_recv_error_reason $error_code]

    if {[info exists pkt_content]} {

        set hex $pkt_content

        pbLib::decode_wrptp_pkt\
                 -packet                         $hex\
                 -dest_mac                       recvd_dest_mac\
                 -src_mac                        recvd_src_mac\
                 -eth_type                       recvd_eth_type\
                 -vlan_id                        recvd_vlan_id\
                 -vlan_priority                  recvd_vlan_priority\
                 -vlan_dei                       recvd_vlan_dei\
                 -src_ip                         recvd_src_ip\
                 -dest_ip                        recvd_dest_ip\
                 -src_port                       recvd_src_port\
                 -dest_port                      recvd_dest_port\
                 -udp_length                     recvd_udp_length\
                 -udp_checksum                   recvd_udp_checksum\
                 -transport_specific             recvd_transport_specific\
                 -message_type                   recvd_message_type\
                 -version_ptp                    recvd_version_ptp\
                 -message_length                 recvd_message_length\
                 -domain_number                  recvd_domain_number\
                 -alternate_master_flag          recvd_alternate_master_flag\
                 -two_step_flag                  recvd_two_step_flag\
                 -unicast_flag                   recvd_unicast_flag\
                 -profile_specific1              recvd_profile_specific1\
                 -profile_specific2              recvd_profile_specific2\
                 -leap61                         recvd_leap61\
                 -leap59                         recvd_leap59\
                 -current_utc_offset_valid       recvd_current_utc_offset_valid\
                 -ptp_timescale                  recvd_ptp_timescale\
                 -time_traceable                 recvd_time_traceable\
                 -freq_traceable                 recvd_freq_traceable\
                 -correction_field               recvd_correction_field\
                 -src_port_number                recvd_src_port_number\
                 -src_clock_identity             recvd_src_clock_identity\
                 -sequence_id                    recvd_sequence_id\
                 -control_field                  recvd_control_field\
                 -log_message_interval           recvd_log_message_interval\
                 -target_port_number             recvd_target_port_number\
                 -target_clock_identity          recvd_target_clock_identity\
                 -starting_boundary_hops         recvd_starting_boundary_hops\
                 -boundary_hops                  recvd_boundary_hops\
                 -action_field                   recvd_action_field

    }
    return $result

}

################################################################################
#                                                                              #
#  PROCEDURE NAME         :  wrptp::check_announce                             #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Reception port                 #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   filter_id                     : (Mandatory) Filter id                      #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default: 1)                   #
#                                                                              #
#   timeout                       : (Optional)  Frame timeout (in seconds)     #
#                                               (Default: 5)                   #
#                                                                              #
#   tlv_type                      : (Optional)  TLV Type                       #
#                                               (Default : 3)                  #
#                                                                              #
#   tlv_length                    : (Optional)  TLV length                     #
#                                               (Default : 10)                 #
#                                                                              #
#   organization_id               : (Optional) Organization id                 #
#                                               (Default: 0x080030)            #
#                                                                              #
#   magic_number                  : (Optional) Magic number                    #
#                                               (Default: 0xDEAD)              #
#                                                                              #
#   version_number                : (Optional) Version number                  #
#                                               (Default: 1)                   #
#                                                                              #
#   message_id                    : (Optional) Message id                      #
#                                               (Default: 0x2000)              #
#                                                                              #
#   wrconfig                      : (Optional) Wrconfig                        #
#                                               (Default: 3)                   #
#                                                                              #
#   calibrated                    : (Optional) Calibrated                      #
#                                               (Default: 1)                   #
#                                                                              #
#   wrmode_on                     : (Optional) WR Mode ON                      #
#                                               (Default: 0)                   #
#                                                                              #
#  OUTPUT PARAMETERS      :                                                    #
#                                                                              #
#   rx_timestamp_sec              : (Optional)  Packet's rx time in seconds    #
#                                                                              #
#   rx_timestamp_ns               : (Optional)  Packet's rx time in ns         #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is received as         #
#                                         expected)                            #
#                                                                              #
#                         : 0 on failure (If the packet is not received        #
#                                         as expected)                         #
#                                                                              #
#  DEFINITION             : This function is used to check WRPTP announce      #
#                           message format                                     #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#   wrptp::check_announce\                                                     #
#             -port_num                  $port_num\                            #
#             -filter_id                 $filter_id\                           #
#             -reset_count               $reset_count\                         #
#             -count                     $count\                               #
#             -timeout                   $timeout\                             #
#             -rx_timestamp_sec          rx_timestamp_sec\                     #
#             -rx_timestamp_ns           rx_timestamp_ns                       #
#             -tlv_type                  $tlv_type\                            #
#             -tlv_length                $tlv_length\                          #
#             -organization_id           $organization_id\                     #
#             -magic_number              $magic_number\                        #
#             -version_number            $version_number\                      #
#             -message_id                $message_id\                          #
#             -wrconfig                  $wrconfig\                            #
#             -calibrated                $calibrated\                          #
#             -wrmode_on                 $wrmode_on                            #
#                                                                              #
################################################################################

proc wrptp::check_announce {args} {

    array unset ::validation_param

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "wrptp::check_announce: Tee session is not initialized."
    }

    #Port number on which frame is to be received
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
        LOG -level 1 -msg "\t\tport_num                       -> $port_num"
    } else {

        ERRLOG -msg "wrptp::check_announce: Missing Argument -> port_num"
        return 0
    }

    #Filter id
    if {[info exists param(-filter_id)]} {

        set filter_id $param(-filter_id)
    } else {

        ERRLOG -msg "wrptp::check_announce: Missing Argument -> filter_id"
        return 0
    }

    #Reset Count
    if {[info exists param(-reset_count)]} {

        set reset_count $param(-reset_count)
    } else {

        set reset_count ON
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame timeout
    if {[info exists param(-timeout)]} {

        set timeout $param(-timeout)
    } else {

        set timeout 5
    }

    #Received packet timestamp in seconds
    if {[info exists param(-rx_timestamp_sec)]} {

        upvar $param(-rx_timestamp_sec) rx_timestamp_sec
        set rx_timestamp_sec ""
    }

    #Received packet timestamp in nanoseconds
    if {[info exists param(-rx_timestamp_ns)]} {

        upvar $param(-rx_timestamp_ns) rx_timestamp_ns
        set rx_timestamp_ns ""
    }

    #TLV Type
    if {[info exists param(-tlv_type)]} {

        set tlv_type $param(-tlv_type)
    } else {

        set tlv_type 3
    }

    #TLV length
    if {[info exists param(-tlv_length)]} {

        set tlv_length $param(-tlv_length)
    } else {

        set tlv_length 10
    }

    #Organisation ID
    if {[info exists param(-organization_id)]} {

        set organization_id $param(-organization_id)
    } else {

        set organization_id 0x080030
    }

    #Magic Number
    if {[info exists param(-magic_number)]} {

        set magic_number $param(-magic_number)
    } else {

        set magic_number 0xDEAD
    }

    #Version Number
    if {[info exists param(-version_number)]} {

        set version_number $param(-version_number)
    } else {

        set version_number 1
    }

    #Message ID
    if {[info exists param(-message_id)]} {

        set message_id $param(-message_id)
    } else {

        set message_id 0x2000
    }

    #WR Config
    if {[info exists param(-wrconfig)]} {

        set wrconfig $param(-wrconfig)
    } else {

        set wrconfig 3
    }

    #Calibrated
    if {[info exists param(-calibrated)]} {

        set calibrated $param(-calibrated)
    } else {

        set calibrated 1
    }

    #WR Mode ON
    if {[info exists param(-wrmode_on)]} {

        set wrmode_on $param(-wrmode_on)
    } else {

        set wrmode_on 0
    }

    #OUTPUT PARAMETERS
    #all the fields in the matched packet
    foreach key [array names param] {

        set var [string trimleft $key -]

        if [regexp {^recvd_.*} $var] {

            upvar [set param($key)] $var
            set $var ""
        }
    }

    #LOGIC GOES HERE
    #Timeout in millisec
    set timeout [expr int($timeout*1000)]

    if {($::wrptp_vlan_encap == "true")} {
        set vlan_id  $::wrptp_vlan_id
    } else {
        set vlan_id  "dont_care"
    }

    if {[string match -nocase $::ptp_comm_model "Unicast"]} {
        set dest_mac "Unicast"
        set dest_ip  "Unicast"
    } else {
        set dest_mac $::PTP_E2E_MAC
        set dest_ip  $::PTP_E2E_IP
    }

    if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
        set ip_checksum  "Valid"
        set ip_protocol  17
        set src_ip       "Unicast"
        set dest_mac     $::PTP_E2E_MAC
        set dest_ip      $::PTP_E2E_IP
        set dest_port    320
        set udp_checksum "Valid"
    } else {
        set ip_checksum  "dont_care"
        set ip_protocol  "dont_care"
        set src_ip       "dont_care"
        set dest_ip      "dont_care"
        set dest_port    "dont_care"
        set udp_checksum "dont_care"
    }

    #Receive only WRPTP announce messages
    set ::validation_param(version_ptp)   2
    set ::validation_param(message_type) 11

    print_timeout $timeout

    set result [pltLib::recv_periodic\
            -session_id           $session_id\
            -port_num             $port_num\
            -filter_id            $filter_id\
            -timeout              $timeout\
            -reset_count          $reset_count\
            -validation_handler   "WRPTP"\
            -rx_timestamp         rx_timestamp_sec\
            -rx_time_nanoseconds  rx_timestamp_ns\
            -pkt_content          pkt_content\
            -pkt_size             pkt_size]

    if {[info exists pkt_content]} {

        pbLib::decode_wrptp_pkt\
                 -packet                         $pkt_content\
                 -dest_mac                       recvd_dest_mac\
                 -src_mac                        recvd_src_mac\
                 -eth_type                       recvd_eth_type\
                 -vlan_id                        recvd_vlan_id\
                 -vlan_priority                  recvd_vlan_priority\
                 -vlan_dei                       recvd_vlan_dei\
                 -ip_version                     recvd_ip_version\
                 -ip_header_length               recvd_ip_header_length\
                 -ip_tos                         recvd_ip_tos\
                 -ip_total_length                recvd_ip_total_length\
                 -ip_identification              recvd_ip_identification\
                 -ip_flags                       recvd_ip_flags\
                 -ip_offset                      recvd_ip_offset\
                 -ip_ttl                         recvd_ip_ttl\
                 -ip_checksum                    recvd_ip_checksum\
                 -ip_protocol                    recvd_ip_protocol\
                 -src_ip                         recvd_src_ip\
                 -dest_ip                        recvd_dest_ip\
                 -src_port                       recvd_src_port\
                 -dest_port                      recvd_dest_port\
                 -udp_length                     recvd_udp_length\
                 -udp_checksum                   recvd_udp_checksum\
                 -transport_specific             recvd_transport_specific\
                 -message_type                   recvd_message_type\
                 -version_ptp                    recvd_version_ptp\
                 -message_length                 recvd_message_length\
                 -domain_number                  recvd_domain_number\
                 -alternate_master_flag          recvd_alternate_master_flag\
                 -two_step_flag                  recvd_two_step_flag\
                 -unicast_flag                   recvd_unicast_flag\
                 -profile_specific1              recvd_profile_specific1\
                 -profile_specific2              recvd_profile_specific2\
                 -leap61                         recvd_leap61\
                 -leap59                         recvd_leap59\
                 -current_utc_offset_valid       recvd_current_utc_offset_valid\
                 -ptp_timescale                  recvd_ptp_timescale\
                 -time_traceable                 recvd_time_traceable\
                 -freq_traceable                 recvd_freq_traceable\
                 -correction_field               recvd_correction_field\
                 -src_port_number                recvd_src_port_number\
                 -src_clock_identity             recvd_src_clock_identity\
                 -sequence_id                    recvd_sequence_id\
                 -control_field                  recvd_control_field\
                 -log_message_interval           recvd_log_message_interval\
                 -origin_timestamp_sec           recvd_origin_timestamp_sec\
                 -origin_timestamp_ns            recvd_origin_timestamp_ns\
                 -current_utc_offset             recvd_current_utc_offset\
                 -gm_priority1                   recvd_gm_priority1\
                 -gm_priority2                   recvd_gm_priority2\
                 -gm_identity                    recvd_gm_identity\
                 -steps_removed                  recvd_steps_removed\
                 -time_source                    recvd_time_source\
                 -gm_clock_class                 recvd_gm_clock_class\
                 -gm_clock_accuracy              recvd_gm_clock_accuracy\
                 -gm_clock_variance              recvd_gm_clock_variance\
                 -precise_origin_timestamp_sec   recvd_precise_origin_timestamp_sec\
                 -precise_origin_timestamp_ns    recvd_precise_origin_timestamp_ns\
                 -receive_timestamp_sec          recvd_receive_timestamp_sec\
                 -receive_timestamp_ns           recvd_receive_timestamp_ns\
                 -requesting_port_number         recvd_requesting_port_number\
                 -requesting_clock_identity      recvd_requesting_clock_identity\
                 -request_receipt_timestamp_sec  recvd_request_receipt_timestamp_sec\
                 -request_receipt_timestamp_ns   recvd_request_receipt_timestamp_ns\
                 -response_origin_timestamp_sec  recvd_response_origin_timestamp_sec\
                 -response_origin_timestamp_ns   recvd_response_origin_timestamp_ns\
                 -target_port_number             recvd_target_port_number\
                 -target_clock_identity          recvd_target_clock_identity\
                 -starting_boundary_hops         recvd_starting_boundary_hops\
                 -boundary_hops                  recvd_boundary_hops\
                 -action_field                   recvd_action_field\
                 -tlv_type                       recvd_tlv_type\
                 -tlv_length                     recvd_tlv_length\
                 -organization_id                recvd_organization_id\
                 -magic_number                   recvd_magic_number\
                 -version_number                 recvd_version_number\
                 -message_id                     recvd_message_id\
                 -wrconfig                       recvd_wrconfig\
                 -calibrated                     recvd_calibrated\
                 -wrmode_on                      recvd_wrmode_on\
                 -cal_send_pattern               recvd_cal_send_pattern\
                 -cal_retry                      recvd_cal_retry\
                 -cal_period                     recvd_cal_period\
                 -delta_tx                       recvd_delta_tx\
                 -delta_rx                       recvd_delta_rx

        set result [check_wrptp_pkt\
                                 -packet                         $pkt_content\
                                 -dest_mac                       $dest_mac\
                                 -src_mac                        "Unicast"\
                                 -vlan_id                        $vlan_id\
                                 -ip_checksum                    $ip_checksum\
                                 -ip_protocol                    $ip_protocol\
                                 -src_ip                         $src_ip\
                                 -dest_ip                        $dest_ip\
                                 -dest_port                      $dest_port\
                                 -udp_checksum                   $udp_checksum\
                                 -transport_specific             0-1\
                                 -message_type                   11\
                                 -version_ptp                    2\
                                 -message_length                 78\
                                 -domain_number                  0-255\
                                 -correction_field               0\
                                 -src_port_identity              non-zero\
                                 -sequence_id                    0-65535\
                                 -control_field                  5\
                                 -log_message_interval           0-4\
                                 -gm_priority1                   0-255\
                                 -gm_clock_class                 0-255\
                                 -gm_clock_accuracy              0-255\
                                 -gm_clock_variance              0-65535\
                                 -gm_priority2                   0-255\
                                 -steps_removed                  0-65535\
                                 -time_source                    0-255\
                                 -tlv_type                       $tlv_type\
                                 -tlv_length                     $tlv_length\
                                 -organization_id                $organization_id\
                                 -magic_number                   $magic_number\
                                 -version_number                 $version_number\
                                 -message_id                     $message_id\
                                 -wrconfig                       $wrconfig\
                                 -calibrated                     $calibrated\
                                 -wrmode_on                      $wrmode_on]

    }

    return $result
}


################################################################################
#                                                                              #
#  PROCEDURE NAME         :  wrptp::check_signal                               #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Reception port                 #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   filter_id                     : (Mandatory) Filter id                      #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default: 1)                   #
#                                                                              #
#   timeout                       : (Optional)  Frame timeout (in seconds)     #
#                                               (Default: 5)                   #
#  (PTP TLVs)                                                                  #
#                                                                              #
#   tlv_type                      : (Optional)  TLV Type                       #
#                                               (Default : 3)                  #
#                                                                              #
#   tlv_length                    : (Optional)  TLV length                     #
#                                               (Default : dont_care)          #
#                                                                              #
#   organization_id               : (Optional) Organization id                 #
#                                               (Default: 0x080030)            #
#                                                                              #
#   magic_number                  : (Optional) Magic number                    #
#                                               (Default: 0xDEAD)              #
#                                                                              #
#   version_number                : (Optional) Version number                  #
#                                               (Default: 1)                   #
#                                                                              #
#   message_id                    : (Optional) Message id                      #
#                                               (Default: dont_care)           #
#                                                                              #
#   cal_send_pattern              : (Optional) Cal send pattern                #
#                                               (Default: dont_care)           #
#                                                                              #
#   cal_retry                     : (Optional) Cal retry                       #
#                                               (Default: dont_care)           #
#                                                                              #
#   cal_period                    : (Optional) Cal period                      #
#                                               (Default: dont_care)           #
#                                                                              #
#   delta_tx                      : (Optional) Delta tx                        #
#                                               (Default: dont_care)           #
#                                                                              #
#   delta_rx                      : (Optional) Delta rx                        #
#                                               (Default: dont_care)           #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS      :                                                    #
#                                                                              #
#   rx_timestamp_sec              : (Optional)  Packet's rx time in seconds    #
#                                                                              #
#   rx_timestamp_ns               : (Optional)  Packet's rx time in ns         #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is received as         #
#                                         expected)                            #
#                                                                              #
#                         : 0 on failure (If the packet is not received        #
#                                         as expected)                         #
#                                                                              #
#  DEFINITION             : This function is used to check WRPTP signal        #
#                           message format                                     #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#   wrptp::check_signal\                                                       #
#           -port_num                         $port_num\                       #
#           -filter_id                        $filter_id\                      #
#           -reset_count                      $reset_count\                    #
#           -count                            $count\                          #
#           -timeout                          $timeout\                        #
#           -rx_timestamp_sec                 rx_timestamp_sec\                #
#           -rx_timestamp_ns                  rx_timestamp_ns                  #
#           -tlv_type                         $tlv_type\                       #
#           -tlv_length                       $tlv_length\                     #
#           -organization_id                  $organization_id\                #
#           -magic_number                     $magic_number\                   #
#           -version_number                   $version_number\                 #
#           -message_id                       $message_id\                     #
#           -cal_send_pattern                 $cal_send_pattern\               #
#           -cal_retry                        $cal_retry\                      #
#           -cal_period                       $cal_period\                     #
#           -delta_tx                         $delta_tx\                       #
#           -delta_rx                         $delta_rx                        #
#                                                                              #
################################################################################

proc wrptp::check_signal {args} {

    array unset ::validation_param

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "wrptp::check_signal: Tee session is not initialized."
    }

    #Port number on which frame is to be received
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
        LOG -level 1 -msg "\t\tport_num                       -> $port_num"
    } else {

        ERRLOG -msg "wrptp::check_signal: Missing Argument -> port_num"
        return 0
    }

    #Filter id
    if {[info exists param(-filter_id)]} {

        set filter_id $param(-filter_id)
    } else {

        ERRLOG -msg "wrptp::check_signal: Missing Argument -> filter_id"
        return 0
    }

    #Reset Count
    if {[info exists param(-reset_count)]} {

        set reset_count $param(-reset_count)
    } else {

        set reset_count ON
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame timeout
    if {[info exists param(-timeout)]} {

        set timeout $param(-timeout)
    } else {

        set timeout 5
    }

    #Received packet timestamp in seconds
    if {[info exists param(-rx_timestamp_sec)]} {

        upvar $param(-rx_timestamp_sec) rx_timestamp_sec
        set rx_timestamp_sec ""
    }

    #Received packet timestamp in nanoseconds
    if {[info exists param(-rx_timestamp_ns)]} {

        upvar $param(-rx_timestamp_ns) rx_timestamp_ns
        set rx_timestamp_ns ""
    }

    #TLV Type
    if {[info exists param(-tlv_type)]} {

        set tlv_type $param(-tlv_type)
    } else {

        set tlv_type 3
    }

    #TLV length
    if {[info exists param(-tlv_length)]} {

        set tlv_length $param(-tlv_length)
    } else {

        set tlv_length 8
    }

    #Organisation ID
    if {[info exists param(-organization_id)]} {

        set organization_id $param(-organization_id)
    } else {

        set organization_id 0x080030
    }

    #Magic Number
    if {[info exists param(-magic_number)]} {

        set magic_number $param(-magic_number)
    } else {

        set magic_number 0xDEAD
    }

    #Version Number
    if {[info exists param(-version_number)]} {

        set version_number $param(-version_number)
    } else {

        set version_number 1
    }

    #Message ID
    if {[info exists param(-message_id)]} {

        set message_id $param(-message_id)
    } else {

        set message_id "dont_care"
    }

    #Cal Send Pattern
    if {[info exists param(-cal_send_pattern)]} {

        set cal_send_pattern $param(-cal_send_pattern)
    } else {

        set cal_send_pattern "dont_care"
    }

    #Cal Retry
    if {[info exists param(-cal_retry)]} {

        set cal_retry $param(-cal_retry)
    } else {

        set cal_retry "dont_care"
    }

    #Cal Period
    if {[info exists param(-cal_period)]} {

        set cal_period $param(-cal_period)
    } else {

        set cal_period "dont_care"
    }

    #Delta Tx
    if {[info exists param(-delta_tx)]} {

        set delta_tx $param(-delta_tx)
    } else {

        set delta_tx "dont_care"
    }

    #Delta Rx
    if {[info exists param(-delta_rx)]} {

        set delta_rx $param(-delta_rx)
    } else {

        set delta_rx "dont_care"
    }

    #OUTPUT PARAMETERS
    #all the fields in the matched packet
    foreach key [array names param] {

        set var [string trimleft $key -]

        if [regexp {^recvd_.*} $var] {

            upvar [set param($key)] $var
            set $var ""
        }
    }

    #LOGIC GOES HERE
    #Timeout in millisec
    set timeout [expr int($timeout*1000)]

    #VLAN
    if {($::wrptp_vlan_encap == "true")} {
        set vlan_id  $::wrptp_vlan_id
    } else {
        set vlan_id  "dont_care"
    }

    #UNICAST/MULTICAST
    if {[string match -nocase $::ptp_comm_model "Unicast"]} {
        set dest_mac "Unicast"
        set dest_ip  "Unicast"
    } else {
        set dest_mac $::PTP_E2E_MAC
        set dest_ip  $::PTP_E2E_IP
    }

    #TRANSPORT TYPE
    if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
        set ip_checksum  "Valid"
        set ip_protocol  17
        set src_ip       "Unicast"
        set dest_mac     $::PTP_E2E_MAC
        set dest_ip      $::PTP_E2E_IP
        set dest_port    320
        set udp_checksum "Valid"
    } else {
        set ip_checksum  "dont_care"
        set ip_protocol  "dont_care"
        set src_ip       "dont_care"
        set dest_ip      "dont_care"
        set dest_port    "dont_care"
        set udp_checksum "dont_care"
    }

    #TLV
    switch $message_id {

        4096 {
            #slave_present
            set message_length 56
            set tlv_length      8
        }

        4097 {
            #lock
            set message_length 56
            set tlv_length      8
        }

        4098 {
            #locked
            set message_length 56
            set tlv_length      8
        }

        4099 {
            #calibrate
            set message_length 62
            set tlv_length 14
            set cal_send_pattern 0
        }

        4100 {
            #calibrated
            set message_length 72
            set tlv_length 24
            set delta_tx $::wrptp_known_delta_tx
            set delta_rx $::wrptp_known_delta_rx
            set delta_rx "$delta_rx - [expr $delta_rx + 16000]"
        }

        default {
            set message_length "non-zero"
            set message_id     "dont_care"
        }

    }

    #Receive only WRPTP signalling messages
    set ::validation_param(version_ptp)   2
    set ::validation_param(message_type) 12
    set ::validation_param(message_id)   $message_id

    print_timeout $timeout

    set result [pltLib::recv_periodic\
            -session_id           $session_id\
            -port_num             $port_num\
            -filter_id            $filter_id\
            -timeout              $timeout\
            -reset_count          $reset_count\
            -validation_handler   "WRPTP"\
            -rx_timestamp         rx_timestamp_sec\
            -rx_time_nanoseconds  rx_timestamp_ns\
            -pkt_content          pkt_content\
            -pkt_size             pkt_size]

    if {[info exists pkt_content]} {

        pbLib::decode_wrptp_pkt\
                 -packet                         $pkt_content\
                 -dest_mac                       recvd_dest_mac\
                 -src_mac                        recvd_src_mac\
                 -eth_type                       recvd_eth_type\
                 -vlan_id                        recvd_vlan_id\
                 -vlan_priority                  recvd_vlan_priority\
                 -vlan_dei                       recvd_vlan_dei\
                 -ip_version                     recvd_ip_version\
                 -ip_header_length               recvd_ip_header_length\
                 -ip_tos                         recvd_ip_tos\
                 -ip_total_length                recvd_ip_total_length\
                 -ip_identification              recvd_ip_identification\
                 -ip_flags                       recvd_ip_flags\
                 -ip_offset                      recvd_ip_offset\
                 -ip_ttl                         recvd_ip_ttl\
                 -ip_checksum                    recvd_ip_checksum\
                 -ip_protocol                    recvd_ip_protocol\
                 -src_ip                         recvd_src_ip\
                 -dest_ip                        recvd_dest_ip\
                 -src_port                       recvd_src_port\
                 -dest_port                      recvd_dest_port\
                 -udp_length                     recvd_udp_length\
                 -udp_checksum                   recvd_udp_checksum\
                 -transport_specific             recvd_transport_specific\
                 -message_type                   recvd_message_type\
                 -version_ptp                    recvd_version_ptp\
                 -message_length                 recvd_message_length\
                 -domain_number                  recvd_domain_number\
                 -alternate_master_flag          recvd_alternate_master_flag\
                 -two_step_flag                  recvd_two_step_flag\
                 -unicast_flag                   recvd_unicast_flag\
                 -profile_specific1              recvd_profile_specific1\
                 -profile_specific2              recvd_profile_specific2\
                 -leap61                         recvd_leap61\
                 -leap59                         recvd_leap59\
                 -current_utc_offset_valid       recvd_current_utc_offset_valid\
                 -ptp_timescale                  recvd_ptp_timescale\
                 -time_traceable                 recvd_time_traceable\
                 -freq_traceable                 recvd_freq_traceable\
                 -correction_field               recvd_correction_field\
                 -src_port_number                recvd_src_port_number\
                 -src_clock_identity             recvd_src_clock_identity\
                 -sequence_id                    recvd_sequence_id\
                 -control_field                  recvd_control_field\
                 -log_message_interval           recvd_log_message_interval\
                 -origin_timestamp_sec           recvd_origin_timestamp_sec\
                 -origin_timestamp_ns            recvd_origin_timestamp_ns\
                 -current_utc_offset             recvd_current_utc_offset\
                 -gm_priority1                   recvd_gm_priority1\
                 -gm_priority2                   recvd_gm_priority2\
                 -gm_identity                    recvd_gm_identity\
                 -steps_removed                  recvd_steps_removed\
                 -time_source                    recvd_time_source\
                 -gm_clock_class                 recvd_gm_clock_class\
                 -gm_clock_accuracy              recvd_gm_clock_accuracy\
                 -gm_clock_variance              recvd_gm_clock_variance\
                 -precise_origin_timestamp_sec   recvd_precise_origin_timestamp_sec\
                 -precise_origin_timestamp_ns    recvd_precise_origin_timestamp_ns\
                 -receive_timestamp_sec          recvd_receive_timestamp_sec\
                 -receive_timestamp_ns           recvd_receive_timestamp_ns\
                 -requesting_port_number         recvd_requesting_port_number\
                 -requesting_clock_identity      recvd_requesting_clock_identity\
                 -request_receipt_timestamp_sec  recvd_request_receipt_timestamp_sec\
                 -request_receipt_timestamp_ns   recvd_request_receipt_timestamp_ns\
                 -response_origin_timestamp_sec  recvd_response_origin_timestamp_sec\
                 -response_origin_timestamp_ns   recvd_response_origin_timestamp_ns\
                 -target_port_number             recvd_target_port_number\
                 -target_clock_identity          recvd_target_clock_identity\
                 -starting_boundary_hops         recvd_starting_boundary_hops\
                 -boundary_hops                  recvd_boundary_hops\
                 -action_field                   recvd_action_field\
                 -tlv_type                       recvd_tlv_type\
                 -tlv_length                     recvd_tlv_length\
                 -organization_id                recvd_organization_id\
                 -magic_number                   recvd_magic_number\
                 -version_number                 recvd_version_number\
                 -message_id                     recvd_message_id\
                 -wrconfig                       recvd_wrconfig\
                 -calibrated                     recvd_calibrated\
                 -wrmode_on                      recvd_wrmode_on\
                 -cal_send_pattern               recvd_cal_send_pattern\
                 -cal_retry                      recvd_cal_retry\
                 -cal_period                     recvd_cal_period\
                 -delta_tx                       recvd_delta_tx\
                 -delta_rx                       recvd_delta_rx

        set result [check_wrptp_pkt\
                                 -packet                         $pkt_content\
                                 -dest_mac                       $dest_mac\
                                 -src_mac                        "Unicast"\
                                 -vlan_id                        $vlan_id\
                                 -ip_checksum                    $ip_checksum\
                                 -ip_protocol                    $ip_protocol\
                                 -src_ip                         $src_ip\
                                 -dest_ip                        $dest_ip\
                                 -dest_port                      $dest_port\
                                 -udp_checksum                   $udp_checksum\
                                 -transport_specific             0\
                                 -message_type                   12\
                                 -version_ptp                    2\
                                 -message_length                 $message_length\
                                 -domain_number                  0-127\
                                 -correction_field               0\
                                 -src_port_identity              non-zero\
                                 -sequence_id                    0-65535\
                                 -control_field                  5\
                                 -log_message_interval           127\
                                 -target_port_identity           non-zero\
                                 -tlv_type                       $tlv_type\
                                 -tlv_length                     $tlv_length\
                                 -organization_id                $organization_id\
                                 -magic_number                   $magic_number\
                                 -version_number                 $version_number\
                                 -message_id                     $message_id\
                                 -cal_send_pattern               $cal_send_pattern\
                                 -cal_retry                      $cal_retry\
                                 -cal_period                     $cal_period\
                                 -delta_tx                       $delta_tx\
                                 -delta_rx                       $delta_rx]

    }

    return $result
}

################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::resolve_ip_to_mac                                #
#                                                                              #
#  DEFINITION        : This function resolves the ip to mac using ARP          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   port_num         : (Mandatory) Transmission port                           #
#                                  (e.g., 0/1)                                 #
#                                                                              #
#   dest_mac         : (Optional)  Destination MAC dddress.                    #
#                                  (e.g., FF:FF:FF:FF:FF:FF)                   #
#                                                                              #
#   src_mac          : (Mandatory) Source MAC address.                         #
#                                  (e.g., 00:80:C2:00:00:21)                   #
#                                                                              #
#   eth_type         : (Optional)  ARP EtherType                               #
#                                  (Default : 0806)                            #
#                                                                              #
#   vlan_id          : (Optional)  Vlan ID                                     #
#                                  (Default : 0)                               #
#                                                                              #
#   vlan_priority    : (Optional)  Vlan Priority                               #
#                                  (Default : 0)                               #
#                                                                              #
#   ip               : (Mandatory) IP address which must be resolved to mac    #
#                                                                              #
#   sender_ip        : (Optional)  Sender IP address                           #
#                                  (Default : 0.0.0.0)                         #
#                                                                              #
#   filter_id        : (Mandatory) Filter id                                   #
#                                                                              #
#   reset_count      : (Optional)  Reset Count  (Default: ON)                  #
#                                                                              #
#   timeout          : (Optional)  Frame timeout (in seconds)                  #
#                                  (Default: 5)                                #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#   mac              : (Mandatory) Resolved Mac address                        #
#                                                                              #
#  RETURNS           : 1 on success (If mac address is resolved)               #
#                                                                              #
#                      0 on failure (If mac address is not resolved)           #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#              wrptp::resolve_ip_to_mac\                                       #
#                                -port_num              $port_num\             #
#                                -dest_mac              $dest_mac\             #
#                                -src_mac               $src_mac\              #
#                                -eth_type              $eth_type\             #
#                                -vlan_id               $vlan_id\              #
#                                -vlan_priority         $vlan_priority\        #
#                                -ip                    $ip\                   #
#                                -sender_ip             $sender_ip\            #
#                                -filter_id             $filter_id\            #
#                                -reset_count           $reset_count\          #
#                                -timeout               $timeout\              #
#                                -mac                   mac                    #
#                                                                              #
################################################################################

proc wrptp::resolve_ip_to_mac {args} {

    array set param $args

    #INPUT PARAMETERS
    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "wrptp::resolve_ip_to_mac: Tee session is not initialized."
        return 0
    }

    #Transmission port
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "wrptp::resolve_ip_to_mac: Missing Argument -> port_num"
        return 0
    }

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
    } else {

        set dest_mac ff:ff:ff:ff:ff:ff
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
    } else {

        ERRLOG -msg "wrptp::resolve_ip_to_mac: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
    } else {

        set eth_type 0806
    }

    #Vlan ID
    if { $::wrptp_vlan_encap == "true" } {
        if {[info exists param(-vlan_id)]} {

            set vlan_id $param(-vlan_id)
        } else {

            set vlan_id $::wrptp_vlan_id
        }

        set ::validation_param(vlan_id) $vlan_id

        if {[info exists param(-vlan_priority)]} {

            set vlan_priority $param(-vlan_priority)
        } else {

            set vlan_priority $::wrptp_vlan_priority
        }

    } else {

        set vlan_id -1
        set vlan_priority 0
    }

    #IP address which must be resolved
    if {[info exists param(-ip)]} {

        set ip $param(-ip)
    } else {

        ERRLOG -msg "wrptp::resolve_ip_to_mac: Missing Argument -> ip"
        return 0
    }

    #Sender IP address
    if {[info exists param(-sender_ip)]} {

        set sender_ip $param(-sender_ip)
    } else {

        set sender_ip 0.0.0.0
    }

    #Filter id
    if {[info exists param(-filter_id)]} {

        set filter_id $param(-filter_id)
    } else {

        ERRLOG -msg "wrptp::resolve_ip_to_mac: Missing Argument -> filter_id"
        return 0
    }

    #Reset Count
    if {[info exists param(-reset_count)]} {

        set reset_count $param(-reset_count)
    } else {

        set reset_count ON
    }

    #Frame timeout
    if {[info exists param(-timeout)]} {

        set timeout $param(-timeout)
    } else {

        set timeout 5
    }

    #OUTPUT PARAMETERS

    #Resolved Mac address
    if {[info exists param(-mac)]} {

        upvar $param(-mac) mac
        set mac ""
    }

    #LOGIC GOES HERE
    #Timeout in millisec
    set timeout [expr int($timeout*1000)]

    #Send ARP REQUEST
    if {![pltLib::send_arp\
                -session_id                        $session_id\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -message_type                      1\
                -sender_mac                        $src_mac\
                -sender_ip                         $sender_ip\
                -target_mac                        00:00:00:00:00:00\
                -target_ip                         $ip\
                -count                             1\
                -interval                          1]} {

        return 0
    }

    set ::validation_param(dest_mac)         $src_mac
    set ::validation_param(sender_ip)        $ip
    set ::validation_param(target_mac)       $src_mac
    set ::validation_param(target_ip)        $sender_ip
    set ::validation_param(message_type)     2

    print_timeout $timeout

    #Receive ARP REQUEST
    set result [pltLib::recv_periodic\
            -session_id           $session_id\
            -port_num             $port_num\
            -filter_id            $filter_id\
            -timeout              $timeout\
            -reset_count          $reset_count\
            -validation_handler   "ARP"\
            -rx_timestamp         rx_timestamp_sec\
            -rx_time_nanoseconds  rx_timestamp_ns\
            -pkt_content          pkt_content\
            -pkt_size             pkt_size]

    if {[info exists pkt_content]} {
        pbLib::decode_arp_pkt\
                     -packet                      $pkt_content\
                     -sender_mac                  mac
    }

    return $result

}

#################################################################################
#                                                                               #
#  PROCEDURE NAME         : wrptp::respond_to_arp_requests                      #
#                                                                               #
#  INPUT PARAMETERS       :                                                     #
#                                                                               #
#   port_num              : (Mandatory) Port number on which frame is to send.  #
#                                       (e.g., 0/1)                             #
#                                                                               #
#   mac                   : (Mandatory) MAC address.                            #
#                                                                               #
#   ip                    : (Mandatory) IP address                              #
#                                                                               #
#   filter_id             : (Mandatory) Filter ID                               #
#                                                                               #
#   vlan_id               : (Optional)  Vlan ID                                 #
#                                       (Default : 0)                           #
#                                                                               #
#   vlan_priority         : (Optional)  Vlan Priority                           #
#                                       (Default : 0)                           #
#                                                                               #
#  OUTPUT PARAMETERS      :  NONE                                               #
#                                                                               #
#  RETURNS                :  1 on success                                       #
#                                                                               #
#                         :  0 on failure                                       #
#                                                                               #
#                                                                               #
#  DEFINITION             : This function is used to respond to arp requests    #
#                                                                               #
#  USAGE                  :                                                     #
#                                                                               #
#      wrptp::respond_to_arp_requests\                                          #
#                   -port_num                          $port_num\               #
#                   -filter_id                         $filter_id\              #
#                   -mac                               $mac\                    #
#                   -vlan_id                           $vlan_id\                #
#                   -vlan_priority                     $vlan_priority\          #
#                   -ip                                $ip                      #
#                                                                               #
#################################################################################

proc wrptp::respond_to_arp_requests {args} {

    array set param $args

    #INPUT PARAMETERS

    #Session identifier
    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "wrptp::respond_to_arp_requests: Tee session is not initialized."
        return 0
    }

    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "wrptp::respond_to_arp_requests: Missing Argument -> port_num"
        return 0
    }

    #Filter ID
    if {[info exists param(-filter_id)]} {

        set filter_id $param(-filter_id)
    } else {

        ERRLOG -msg "wrptp::respond_to_arp_requests: Missing Argument -> filter_id"
        return 0
    }

    #MAC dddress
    if {[info exists param(-mac)]} {

        set mac $param(-mac)
    } else {

        ERRLOG -msg "wrptp::respond_to_arp_requests: Missing Argument -> mac"
        return 0
    }

    #Source MAC address
    if {[info exists param(-ip)]} {

        set ip $param(-ip)
    } else {

        ERRLOG -msg "wrptp::respond_to_arp_requests: Missing Argument -> ip"
        return 0
    }

    #Vlan ID
    if { $::wrptp_vlan_encap == "true" } {
        if {[info exists param(-vlan_id)]} {

            set vlan_id $param(-vlan_id)
        } else {

            set vlan_id $::wrptp_vlan_id
        }

        set ::validation_param(vlan_id)          $vlan_id

        if {[info exists param(-vlan_priority)]} {

            set vlan_priority $param(-vlan_priority)
        } else {

            set vlan_priority $::wrptp_vlan_priority
        }

    } else {

        set vlan_id -1
        set vlan_priority 0
    }

    #Source MAC address
    if {[info exists param(-timeout)]} {

        set timeout $param(-timeout)
    } else {

        set timeout 10
    }

    #Timeout in millisec
    set timeout [expr int($timeout*1000)]

    set ::validation_param(target_ip)        $ip
    set ::validation_param(message_type)     1

    set ::validation_param(session_id)       $session_id
    set ::validation_param(port_num)         $port_num
    set ::validation_param(tee_mac)          $mac

    print_timeout $timeout

    #Respond if ARP request is received
    set result [pltLib::recv_periodic\
            -session_id           $session_id\
            -port_num             $port_num\
            -filter_id            $filter_id\
            -timeout              $timeout\
            -reset_count          "OFF"\
            -validation_handler   "ARP_RESPONDER"\
            -rx_timestamp         rx_timestamp_sec\
            -rx_time_nanoseconds  rx_timestamp_ns\
            -pkt_content          pkt_content\
            -pkt_size             pkt_size]

    return 1

}


################################################################################
# PROCEDURE NAME    : wrptp::disable_port                                      #
#                                                                              #
# DEFINITION        : This procedure disables the given TEE port               #
#                                                                              #
# INPUT PARAMETERS  :                                                          #
#                                                                              #
# port_num          : (Mandatory) Interface that has to be disabled.           #
#                                                                              #
# USAGE             :                                                          #
#           wrptp::disable_port\                                               #
#                    -port_num                  $port_num\                     #
#                                                                              #
# RETURNS           : 1 on Success (If the port is disabled successfully).     #
#                     0 on Failure (If the port could not be disabled).        #
#                                                                              #
################################################################################

proc wrptp::disable_port {args} {

    array set param $args

    #Session identifier
    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "wrptp::disable_port: Tee session is not initialized."
        return 0
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "wrptp::disable_port: Missing Argument -> port_num"
        return 0
    }

    return [pltLib::disable_interface\
                        -session_id       $session_id\
                        -port_num         $port_num]

}

#################################################################################
#                                                                               #
#  PROCEDURE NAME    : wrptp::stop_$message                                     #
#                                                                               #
#  DEFINITION        : This function will stop periodic transmission of an      #
#                      message.                                                 #
#                                                                               #
#  INPUT PARAMETERS  :                                                          #
#                                                                               #
#  message           : message name which must be stopped                       #
#                                                                               #
#  USAGE             :                                                          #
#                                                                               #
#     wrptp::stop_$message                                                      #
#                                                                               #
#################################################################################

proc wrptp::stop_sync { args } {
     return [pltLib::cancel_send_periodic -instance      $::INSTANCE(SYNC)]
}

proc wrptp::stop_announce { args } {
     return [pltLib::cancel_send_periodic -instance      $::INSTANCE(ANNOUNCE)]
}

proc wrptp::stop_delay_req { args } {
     return [pltLib::cancel_send_periodic -instance      $::INSTANCE(DELAY_REQ)]
}

proc wrptp::stop_delay_resp { args } {
     return [pltLib::cancel_send_periodic -instance      $::INSTANCE(DELAY_RESP)]
}

proc wrptp::stop_followup { args } {
     return [pltLib::cancel_send_periodic -instance      $::INSTANCE(FOLLOWUP)]
}

proc wrptp::stop_pdelay_req { args } {
     return [pltLib::cancel_send_periodic -instance      $::INSTANCE(PDELAY_REQ)]
}

proc wrptp::stop_pdelay_resp { args } {
     return [pltLib::cancel_send_periodic -instance      $::INSTANCE(PDELAY_RESP)]
}

proc wrptp::stop_pdelay_resp_followup { args } {
     return [pltLib::cancel_send_periodic -instance      $::INSTANCE(PDELAY_RESP_FOLLOWUP)]
}

proc wrptp::stop_signal { args } {
     return [pltLib::cancel_send_periodic -instance      $::INSTANCE(SIGNAL)]
}

proc wrptp::stop_mgmt { args } {
     return [pltLib::cancel_send_periodic -instance      $::INSTANCE(MGMT)]
}

################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::get_port_macaddress                              #
#                                                                              #
#  DEFINITION        : This function get the mac address of a port from traffic#
#                      generator                                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_id        : (Optional) session id to which the command is sent      #
#                                                                              #
#  port_num          : (Mandatory) Port whose mac address has to obtain        #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  mac_address       : (Mandatory) Mac address of the traffic generator port   #
#                                                                              #
#  RETURNS           : 1 on success (If the port is sync)                      #
#                                                                              #
#                      0 on failure (If the port is not sync)                  #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#             wrptp::get_port_macaddress\                                      #
#                                      -session_id      $session_id\           #
#                                      -port_num        $port_num\             #
#                                      -mac_address     mac_address            #
#                                                                              #
################################################################################

proc wrptp::get_port_macaddress {args} {

    array set param $args

    if {[info exists param(-session_id)]} {
        set session_id  $param(-session_id)
    } else {
        set session_id $::tee_session_id
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG  -msg "wrptp::get_port_macaddress :  Missing Argument -> port_num"
        return 0
    }

    if {[info exists param(-mac_address)]} {
        upvar  $param(-mac_address) mac_address
    } else {
        ERRLOG  -msg "wrptp::get_port_macaddress :  Missing Argument -> mac_address"
        return 0
    }


    if {![ pltLib::get_port_macaddress\
                 -session_id      $session_id\
                 -port_num        $port_num\
                 -mac_address     mac_address]} {

        ERRLOG  -msg "Getting of TEE port MAC is failed."
        return 0
    }

    return 1
}

proc TEE_CLEANUP {} {

    foreach after_info [after info] {
        after cancel $after_info
    }

}


#########################################################################
#  PROCEDURE NAME   : wrptp::tee_get_interface_ip                       #
#                                                                       #
#  INPUT PARAMETERS                                                     #
#                                                                       #
#  reference_ip     : (Mandatory) The Reference IP Address.             #
#                                                                       #
#  OUTPUT PARAMETERS                                                    #
#                                                                       #
#  ip               : The IP Address in given network from the          #
#                     reference IP Address.                             #
#                                                                       #
#  RETURNS          : 1 on success                                      #
#                     0 on failure                                      #
#                                                                       #
#  DEFINITION       : This function returns an ip address in the same   #
#                     network                                           #
#                                                                       #
#  USAGE            :                                                   #
#                                                                       #
#  wrptp::tee_get_interface_ip\                                         #
#                         -reference_ip    $reference_ip\               #
#                         -ip              ip                           #
#                                                                       #
#########################################################################

proc wrptp::tee_get_interface_ip {args} {

     array set param $args

    ### Check the Input Parameters ###
     if {[info exists param(-reference_ip)]} {
       set reference_ip $param(-reference_ip)
     } else {
          ERRLOG -msg "wrptp::tee_get_interface_ip: Missing Argument -> reference_ip\n"
          return 0
     }

     if {[info exists param(-ip)]} {
          upvar $param(-ip) ip
     }

     set ip_list [split $reference_ip .]

     if {[llength $ip_list] < 4} {
          ERRLOG -msg "wrptp::tee_get_interface_ip: Not a valid IP Address $reference_ip\n"
          return 0
     }

     set l1 [lindex $ip_list 0]
     set l2 [lindex $ip_list 1]
     set l3 [lindex $ip_list 2]
     set l4 [lindex $ip_list 3]


     if { $l4 >= 254 } {
       incr l4 -1
     } else {
       incr l4
     }

     set ip [join "$l1 $l2 $l3 $l4" .]

     return 1
}

#########################################################################
#  PROCEDURE NAME   : wrptp::tee_recv_error_reason                      #
#                                                                       #
#  INPUT PARAMETERS                                                     #
#                                                                       #
#  error_code       : (Mandatory) Error code                            #
#                                                                       #
#  OUTPUT PARAMETERS: None                                              #
#                                                                       #
#  RETURNS          : Error Reason                                      #
#                                                                       #
#  DEFINITION       : This function returns an error reason for the     #
#                     given error code                                  #
#                                                                       #
#  USAGE            :                                                   #
#                                                                       #
#               wrptp::tee_recv_error_reason $error_code                #
#                                                                       #
#########################################################################

proc wrptp::tee_recv_error_reason {error_code} {

    if [info exists ::tee_rx_errlog($error_code)] {
        set error_reason [set ::tee_rx_errlog($error_code)]
    } else {
        set error_reason ""
    }

    return $error_reason
}

################################################################################
#  PROCEDURE NAME    : digits                                                  #
#                                                                              #
#  INPUT             :                                                         #
#     number         : (Mandatory) Number                                      #
#                                                                              #
#     separator      : (Optional)  Character as separator.                     #
#                                  Default: ","                                #
#                                                                              #
#  RETURN            : Number with separator.                                  #
#                                                                              #
#  DEFINITION        : This function returns number with separator.            #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      digit_separator $number $separator                      #
#                                                                              #
################################################################################

proc digits {number {separator ,}} {
    while {[regsub {^([-+]?\d+)(\d\d\d)} $number "\\1$separator\\2" number]} {}
    return $number
}

proc print_timeout {timeout} {

#   puts "Maximum waiting time [digits $timeout] ms"
    return 1
}


#################################################################################
#                                                                               #
#  PROCEDURE NAME    : wrptp::reset_capture_stats                               #
#                                                                               #
#  DEFINITION        : This function reset the packet capture index & clear pkt #
#                      capture buffer.                                          #
#                                                                               #
#  INPUT PARAMETERS  :                                                          #
#                                                                               #
#  OUTPUT PARAMETERS  :                                                         #
#                                                                               #
#      wrptp::reset_capture_stats\                                              #
#               -port_num           $port_num                                   #
#                                                                               #
#################################################################################

proc wrptp::reset_capture_stats { args } {

    array set param $args

    set port_num      $param(-port_num)

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "wrptp::reset_capture_stats: TEE session is not initialized."
    }

    #Resource is in-use
    if { $::semlock == "LOCKED" } {

        after 5 wrptp::reset_capture_stats\
                        -port_num           $port_num

        return 0
    } else {

        set ::semlock "LOCKED"

        pltLib::reset_all_stats\
                -session_id         $session_id\
                -port_num           $port_num

        set ::semlock "UNLOCKED"
    }

    return 1
}


#################################################################################
#                                                                               #
#  PROCEDURE NAME    : wrptp::wait_msec                                         #
#                                                                               #
#  DEFINITION        : This function waits for the given time in milliseconds.  #
#                                                                               #
#  INPUT PARAMETERS  :                                                          #
#                                                                               #
#  OUTPUT PARAMETERS :                                                          #
#                                                                               #
#      wrptp::wait_msec $msec                                                   #
#                                                                               #
#################################################################################

proc wrptp::wait_msec { msec } {

    LOG -level 4 -msg "\t Waiting for $msec msec."

	if { $msec > 0 } {
		if { $msec < 1000 } {

			after [expr int($msec)]
		} else {

			set sec         [expr $msec / 1000]
			set millisec    [expr $msec % 1000]

			sleep [expr int($sec)]
			after [expr int($millisec)]
		}
		}
    return 1
}
