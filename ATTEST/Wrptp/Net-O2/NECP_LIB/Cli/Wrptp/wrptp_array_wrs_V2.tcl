#!/usr/bin/tcl
################################################################################
# File Name      : wrptp_array_wrs_V2.tcl                                      #
# File Version   : 1.0                                                         #
# Component Name : ATTEST CLI ARRAY MAPPING                                    #
# Module Name    : White Rabbit Precision Time Protocol                        #
#                  New switch interface (HA)                                   #
#                                                                              #
################################################################################
#                                                                              #
#     SET ARRAY INDEX                                                          #
#                                                                              #
#  DUT SET FUNCTIONS     :                                                     #
#                                                                              #
#  1) DUT_PORT_ENABLE                                                          #
#  2) DUT_PORT_PTP_ENABLE                                                      #
#  3) DUT_SET_VLAN                                                             #
#  4) DUT_SET_VLAN_PRIORITY                                                    #
#  5) DUT_GLOBAL_PTP_ENABLE                                                    #
#  6) DUT_SET_COMMUNICATION_MODE                                               #
#  7) DUT_SET_DOMAIN                                                           #
#  8) DUT_SET_NETWORK_PROTOCOL                                                 #
#  9) DUT_SET_CLOCK_MODE                                                       #
# 10) DUT_SET_CLOCK_STEP                                                       #
# 11) DUT_SET_DELAY_MECHANISM                                                  #
# 12) DUT_SET_PRIORITY1                                                        #
# 13) DUT_SET_PRIORITY2                                                        #
# 14) DUT_SET_ANNOUNCE_INTERVAL                                                #
# 15) DUT_SET_IPV4_ADDRESS                                                     #
# 16) DUT_SET_WR_CONFIG                                                        #
# 17) DUT_SET_KNOWN_DELTA_TX                                                   #
# 18) DUT_SET_KNOWN_DELTA_RX                                                   #
# 19) DUT_SET_DELTAS_KNOWN                                                     #
# 20) DUT_SET_CAL_PERIOD                                                       #
# 21) DUT_SET_CAL_RETRY                                                        #
# 22) DUT_PORT_WRPTP_ENABLE                                                    #
# 23) DUT_SET_WR_PRESENT_TIMEOUT                                               #
# 24) DUT_SET_WR_M_LOCK_TIMEOUT                                                #
# 25) DUT_SET_WR_S_LOCK_TIMEOUT                                                #
# 26) DUT_SET_WR_LOCKED_TIMEOUT                                                #
# 27) DUT_SET_WR_CALIBRATION_TIMEOUT                                           #
# 28) DUT_SET_WR_CALIBRATED_TIMEOUT                                            #
# 29) DUT_SET_WR_RESP_CALIB_REQ_TIMEOUT                                        #
# 30) DUT_SET_WR_STATE_RETRY                                                   #
#                                                                              #
#  DUT DISABLE FUNCTIONS :                                                     #
#                                                                              #
#  1) DUT_PORT_DISABLE                                                         #
#  2) DUT_PORT_PTP_DISABLE                                                     #
#  3) DUT_RESET_VLAN                                                           #
#  4) DUT_RESET_VLAN_PRIORITY                                                  #
#  5) DUT_GLOBAL_PTP_DISABLE                                                   #
#  6) DUT_RESET_COMMUNICATION_MODE                                             #
#  7) DUT_RESET_DOMAIN                                                         #
#  8) DUT_RESET_NETWORK_PROTOCOL                                               #
#  9) DUT_RESET_CLOCK_MODE                                                     #
# 10) DUT_RESET_CLOCK_STEP                                                     #
# 11) DUT_RESET_DELAY_MECHANISM                                                #
# 12) DUT_RESET_PRIORITY1                                                      #
# 13) DUT_RESET_PRIORITY2                                                      #
# 14) DUT_RESET_ANNOUNCE_INTERVAL                                              #
# 15) DUT_RESET_IPV4_ADDRESS                                                   #
# 16) DUT_RESET_WR_CONFIG                                                      #
# 17) DUT_RESET_KNOWN_DELTA_TX                                                 #
# 18) DUT_RESET_KNOWN_DELTA_RX                                                 #
# 19) DUT_RESET_DELTAS_KNOWN                                                   #
# 20) DUT_RESET_CAL_PERIOD                                                     #
# 21) DUT_RESET_CAL_RETRY                                                      #
# 22) DUT_PORT_WRPTP_DISABLE                                                   #
# 23) DUT_RESET_WR_PRESENT_TIMEOUT                                             #
# 24) DUT_RESET_WR_M_LOCK_TIMEOUT                                              #
# 25) DUT_RESET_WR_S_LOCK_TIMEOUT                                              #
# 26) DUT_RESET_WR_LOCKED_TIMEOUT                                              #
# 27) DUT_RESET_WR_CALIBRATION_TIMEOUT                                         #
# 28) DUT_RESET_WR_CALIBRATED_TIMEOUT                                          #
# 29) DUT_RESET_WR_RESP_CALIB_REQ_TIMEOUT                                      #
# 30) DUT_RESET_WR_STATE_RETRY                                                 #
#                                                                              #
#  DUT CHECK FUNCTIONS   :                                                     #
#                                                                              #
#  1) DUT_CHECK_PTP_PORT_STATE                                                 #
#  2) DUT_CHECK_WR_PORT_STATE                                                  #
#  3) DUT_CHECK_OTHER_PORT_DELTA_TX                                            #
#  4) DUT_CHECK_OTHER_PORT_DELTA_RX                                            #
#  5) DUT_CHECK_OTHER_PORT_CAL_PERIOD                                          #
#  6) DUT_CHECK_OTHER_PORT_CAL_RETRY                                           #
#  7) DUT_CHECK_OTHER_PORT_CAL_SEND_PATTERN                                    #
#                                                                              #
################################################################################
# History     Date         Author        Addition/ Alteration                  #
#                                                                              #
#  1.0       Apr/2019      jc.bau/CERN   Initial version based on              #
#                                        wrptp_array_wrs.tcl                   #
#                                                                              #
################################################################################
# Copyright (c) 2018 - 2019 CERN                                               #
################################################################################

##########################################################################################
# Help on mapping CLI commands:                                                          #
#                                                                                        #
# ATTEST DUT functions are of three types - SET, GET, Callback                           #
#                                                                                        #
#  - SET (ATTEST_SET) function are used to set/configure the DUT                         #
#                                                                                        #
#  - GET (ATTEST_GET) function used to get/retrieve value(s) from the DUT                #
#                                                                                        #
#  - Callback function used for modifying the input parameters according to DUT for SET  #
#    and parsing the CLI show command output for GET function (DUT specific customization#
#    are done using this function). This function return following values 0/1/2          #
#    return 0 - Failure                                                                  #
#    return 1 - Success and validation is done by the function                           #
#    return $result                                                                      #
#    return 2 - Success and validation should be done by the test script                 #
#                                                                                        #
# Sub types - OUTPUT, WAIT, TIME                                                         #
#                                                                                        #
#  - WAIT (ATTEST_WAIT) function used to wait until it gets the given string.            #
#    e.g.: set cli_set_record(DUT_GLOBAL_PTP_ENABLE,ATTEST_SET,1)"your DUT CLI command"  #
#          set cli_set_record(DUT_GLOBAL_PTP_ENABLE,ATTEST_WAIT,1) "config"              #
#                                                                                        #
#  - TIME (ATTEST_TIME) function used to customize the ATTEST timeout period when a CLI  #
#    command execution takes more than the default time (5 seconds). ATTEST_TIME waits   #
#    for the DUT prompt or timeout period to elapse (whichever occurs first) before      #
#    executing the next command or returning failure/success. ATTEST_TIME accepts time in#
#    seconds.                                                                            #
#    e.g.: set cli_set_record(DUT_MVRP_SET_MVRP_REGISTRATION,ATTEST_TIME,1) "10"         #
#    will increase the ATTEST's timeout period to 10 seconds.                            #
#                                                                                        #
#  - OUTPUT (ATTEST_OUTPUT) function used to capture the DUT CLI output to a buffer      #
#    This command is used in all GET functions.                                          #
#                                                                                        #
# Syntax:                                                                                #
# -------                                                                                #
#  Set command:                                                                          #
#  set cli_set_record(<COMMAND>,ATTEST_SET,<N>)  "your DUT CLI command"                  #
#  set cli_set_record(<COMMAND>,ATTEST_SET,<N>)  "NONE"                                  #
#                                                                                        #
#  Get command:                                                                          #
#  set cli_get_record(<COMMAND>,ATTEST_OUTPUT,<N>)  "your DUT CLI show command"          #
#  set cli_get_record(<COMMAND>,ATTEST_GET,<N>)     "NONE"                               #
#                                                                                        #
#  callback_protocol_set_xxx()                                                           #
#  callback_protocol_get_xxx()                                                           #
#                                                                                        #
#  Where N = 1,2,3 ..n                                                                   #
#  The order in which the commands are executed and hence must be sequential.            #
#                                                                                        #
#  Note:                                                                                 #
#    ATTEST_WAIT and ATTEST_TIME must use the command sequence number (N) of previous    #
#    ATTEST_SET/ATTEST_GET/ATTEST_OUTPUT.                                                #
#                                                                                        #
#    e.g.: set cli_set_record(DUT_GLOBAL_PTP_ENABLE,ATTEST_SET,1)"your DUT CLI command"  #
#          set cli_set_record(DUT_GLOBAL_PTP_ENABLE,ATTEST_TIME,1) "10"                  #
#    The command sequence number in the above two statements are 1.                      #
#                                                                                        #
#    GET function only executes the particular DUT CLI command and use ATTEST_OUTPUT to  #
#    capture the CLI output to buffer. The associated Callback function does the         #
#    comparison between value in the buffer and argument provided by the test script     #
#    when callback function is called.                                                   #
#                                                                                        #
##########################################################################################

package require Expect

global env

global dut_error_msg

#set ::WRPTP_DEBUG_CLI 1
#set ::WRPTP_DUT_CHECK_RETRY_LIMIT 1

set dut_error_msg "Available commands*|Next possible*|Invalid*|constraint*| is required*|MAC Address must be unicast*|Incorrect input*|Command not found*|overlaps*"

global username password host

set username    $env(DUT_SSH_USER_NAME)
set password    $env(DUT_SSH_PASSWORD)
set host        $env(TEST_DEVICE_IPV4_ADDRESS)

global ATTEST_CORE tc_name case_id group_name

set PktFileName     $ATTEST_CORE(-pktFileName)
set log_instance    [lindex [split $PktFileName "."] 0]
set tc_name_list    [split $log_instance "_"]
set group_name      [lindex $tc_name_list 3]
set case_id         [lindex $tc_name_list 4]

set dotconfig_path      /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Wrptp
set dotconfig_file      $dotconfig_path/dot-config-v2
set dotconfig_file_act  $dotconfig_file.active


proc tee_set_dut_wrptp_cli_from_file {} {

	global cli_set_record

	global env wrptp_env
	global case_id group_name

	################################################################################
	#                            DUT SET FUNCTIONS                                 #
	################################################################################

	#1.
	################################################################################
	#                                                                              #
	#  COMMAND           : PORT_ENABLE                                             #
	#                                                                              #
	#  ARRAY INDEX       : DUT_PORT_ENABLE                                         #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
	#                                  (e.g., fa0/0)                               #
	#                                                                              #
	#  DEFINITION        : This function makes the specific port administratively  #
	#                      up at the DUT.                                          #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_PORT_ENABLE,ATTEST_SET,[incr idx]) "ifconfig ATTEST_PARAM1 up"
	set cli_set_record(DUT_PORT_ENABLE,ATTEST_SET,[incr idx]) "NONE"

	#2.
	################################################################################
	#                                                                              #
	#  COMMAND           : PORT_PTP_ENABLE                                         #
	#                                                                              #
	#  ARRAY INDEX       : DUT_PORT_PTP_ENABLE                                     #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
	#                                  (e.g., fa0/0)                               #
	#                                                                              #
	#  DEFINITION        : This function enables PTP on a specific port on the DUT.#
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_PORT_PTP_ENABLE,ATTEST_SET,[incr idx]) "NOOP"
	set cli_set_record(DUT_PORT_PTP_ENABLE,ATTEST_SET,[incr idx]) "NONE"

	#3.
	################################################################################
	#                                                                              #
	#  COMMAND           : SET_VLAN                                                #
	#                                                                              #
	#  ARRAY INDEX       : DUT_SET_VLAN                                            #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = vlan_id     VLAN ID with which VLAN has to be created.  #
	#                                  (e.g., 100)                                 #
	#                                                                              #
	#   ATTEST_PARAM2    = port_num    Port which has to be associated to VLAN ID. #
	#                                  (e.g., fa0/0)                               #
	#                                                                              #
	#  DEFINITION        : This function creates VLAN ID and associates given port #
	#                      to it on the DUT.                                       #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_SET_VLAN,ATTEST_SET,[incr idx])   "NOOP"
	set cli_set_record(DUT_SET_VLAN,ATTEST_SET,[incr idx])   "NONE"

	#4.
	################################################################################
	#                                                                              #
	#  COMMAND           : SET_VLAN_PRIORITY                                       #
	#                                                                              #
	#  ARRAY INDEX       : DUT_SET_VLAN_PRIORITY                                   #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = vlan_id     VLAN ID                                     #
	#                                  (e.g., 100)                                 #
	#                                                                              #
	#   ATTEST_PARAM2    = vlan_priority VLAN priority                             #
	#                                  (e.g., 1)                                   #
	#                                                                              #
	#   ATTEST_PARAM3    = port_num    Port which has to be associated to VLAN ID. #
	#                                  (e.g., fa0/0)                               #
	#                                                                              #
	#  DEFINITION        : This function associates VLAN priority to the VLAN ID   #
	#                      on the DUT.                                             #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_SET_VLAN_PRIORITY,ATTEST_SET,[incr idx])   "NOOP"
	set cli_set_record(DUT_SET_VLAN_PRIORITY,ATTEST_SET,[incr idx])   "NONE"

	#5.
	################################################################################
	#                                                                              #
	#  COMMAND           : GLOBAL_PTP_ENABLE                                       #
	#                                                                              #
	#  ARRAY INDEX       : DUT_GLOBAL_PTP_ENABLE                                   #
	#                                                                              #
	#  INPUT PARAMETERS  : NONE.                                                   #
	#                                                                              #
	#  DEFINITION        : This function enables PTP on the DUT.                   #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_GLOBAL_PTP_ENABLE,ATTEST_SET,[incr idx])   "NOOP"
	set cli_set_record(DUT_GLOBAL_PTP_ENABLE,ATTEST_SET,[incr idx])   "NONE"

	#6.
	################################################################################
	#                                                                              #
	#  COMMAND           : SET_COMMUNICATION_MODE                                  #
	#                                                                              #
	#  ARRAY INDEX       : DUT_SET_COMMUNICATION_MODE                              #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
	#                                  (e.g., fa0/0)                               #
	#                                                                              #
	#   ATTEST_PARAM2    = clock_mode  Clock mode.                                 #
	#                                  (e.g., Ordinary Clock)                      #
	#                                                                              #
	#   ATTEST_PARAM3    = ptp_version PTP version.                                #
	#                                  (Default: 2)                                #
	#                                                                              #
	#   ATTEST_PARAM4    = communication_mode Communication mode                   #
	#                                  (e.g., "Unicast"|"Multicast")               #
	#                                                                              #
	#  DEFINITION        : This function sets the communication mode on a specific #
	#                      port of the DUT.                                        #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_SET_COMMUNICATION_MODE,ATTEST_SET,[incr idx])     "NOOP"
	set cli_set_record(DUT_SET_COMMUNICATION_MODE,ATTEST_SET,[incr idx])     "NONE"

	#7.
	################################################################################
	#                                                                              #
	#  COMMAND           : SET_DOMAIN                                              #
	#                                                                              #
	#  ARRAY INDEX       : DUT_SET_DOMAIN                                          #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = clock_mode  Clock mode.                                 #
	#                                  (e.g., Ordinary Clock)                      #
	#                                                                              #
	#   ATTEST_PARAM2    = domain      Domain number.                              #
	#                                  (e.g., 0..255)                              #
	#                                                                              #
	#  DEFINITION        : This function sets domain number on the DUT.            #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_SET_DOMAIN,ATTEST_SET,[incr idx])          "NOOP"
	set cli_set_record(DUT_SET_DOMAIN,ATTEST_SET,[incr idx])          "NONE"

	#8.
	################################################################################
	#                                                                              #
	#  COMMAND           : SET_NETWORK_PROTOCOL                                    #
	#                                                                              #
	#  ARRAY INDEX       : DUT_SET_NETWORK_PROTOCOL                                #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
	#                                  (e.g., fa0/0)                               #
	#                                                                              #
	#   ATTEST_PARAM2    = clock_mode  clock_mode.                                 #
	#                                  (e.g., Ordinary Clock)                      #
	#                                                                              #
	#   ATTEST_PARAM3    = network_protocol Network protocol.                      #
	#                                  (e.g., "IEEE 802.3"|"UDP/IPv4")             #
	#                                                                              #
	#  DEFINITION        : This function sets the network protocol on a specific   #
	#                      port of the DUT.                                        #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_SET_NETWORK_PROTOCOL,ATTEST_SET,[incr idx]) "NOOP"
	set cli_set_record(DUT_SET_NETWORK_PROTOCOL,ATTEST_SET,[incr idx]) "NONE"

	#9.
	################################################################################
	#                                                                              #
	#  COMMAND           : SET_CLOCK_MODE                                          #
	#                                                                              #
	#  ARRAY INDEX       : DUT_SET_CLOCK_MODE                                      #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
	#                                  (e.g., fa0/0)                               #
	#                                                                              #
	#   ATTEST_PARAM2    = clock_mode  Clock mode.                  .              #
	#                                  (e.g., Ordinary Clock)                      #
	#                                                                              #
	#  DEFINITION        : This function sets clock mode on a specific port of DUT.#
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_SET_CLOCK_MODE,ATTEST_SET,[incr idx])      "NOOP"
	set cli_set_record(DUT_SET_CLOCK_MODE,ATTEST_SET,[incr idx])      "NONE"

	#10.
	################################################################################
	#                                                                              #
	#  COMMAND           : SET_CLOCK_STEP                                          #
	#                                                                              #
	#  ARRAY INDEX       : DUT_SET_CLOCK_STEP                                      #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
	#                                  (e.g., fa0/0)                               #
	#                                                                              #
	#   ATTEST_PARAM2    = clock_mode  Clock mode.                  .              #
	#                                  (e.g., Ordinary Clock)                      #
	#                                                                              #
	#   ATTEST_PARAM3    = clock_step  Clock step.                                 #
	#                                  (e.g., "One-step"|"Two-step")               #
	#                                                                              #
	#  DEFINITION        : This function sets the clock-step on a specific port on #
	#                      the DUT.                                                #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_SET_CLOCK_STEP,ATTEST_SET,[incr idx])      "NOOP"
	set cli_set_record(DUT_SET_CLOCK_STEP,ATTEST_SET,[incr idx])      "NONE"

	#11.
	################################################################################
	#                                                                              #
	#  COMMAND           : SET_DELAY_MECHANISM                                     #
	#                                                                              #
	#  ARRAY INDEX       : DUT_SET_DELAY_MECHANISM                                 #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
	#                                  (e.g., fa0/0)                               #
	#                                                                              #
	#   ATTEST_PARAM2    = clock_mode  Clock mode.                  .              #
	#                                  (e.g., Ordinary Clock)                      #
	#                                                                              #
	#   ATTEST_PARAM3    = delay_mechanism Delay mechanism.                        #
	#                                  (e.g., e2e|p2p|disable)                     #
	#                                                                              #
	#  DEFINITION        : This function sets the delay mechanism to a specific    #
	#                      port on the DUT.                                        #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_SET_DELAY_MECHANISM,ATTEST_SET,[incr idx]) "NOOP"
	set cli_set_record(DUT_SET_DELAY_MECHANISM,ATTEST_SET,[incr idx]) "NONE"

	#12.
	################################################################################
	#                                                                              #
	#  COMMAND           : SET_PRIORITY1                                           #
	#                                                                              #
	#  ARRAY INDEX       : DUT_SET_PRIORITY1                                       #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = clock_mode  Clock mode.                                 #
	#                                  (e.g., Ordinary Clock).                     #
	#                                                                              #
	#   ATTEST_PARAM2    = priority1   Prority1                                    #
	#                                  (e.g., 0..255).                             #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_SET_PRIORITY1,ATTEST_SET,[incr idx])    "NOOP"
	set cli_set_record(DUT_SET_PRIORITY1,ATTEST_SET,[incr idx])    "NONE"

	#13.
	################################################################################
	#                                                                              #
	#  COMMAND           : SET_PRIORITY2                                           #
	#                                                                              #
	#  ARRAY INDEX       : DUT_SET_PRIORITY2                                       #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = clock_mode  Clock mode.                                 #
	#                                  (e.g., Ordinary Clock).                     #
	#                                                                              #
	#   ATTEST_PARAM2    = priority2   Prority2                                    #
	#                                  (e.g., 0..255).                             #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_SET_PRIORITY2,ATTEST_SET,[incr idx])    "NOOP"
	set cli_set_record(DUT_SET_PRIORITY2,ATTEST_SET,[incr idx])    "NONE"

	#14.
	################################################################################
	#                                                                              #
	#  COMMAND           : SET_ANNOUNCE_INTERVAL                                   #
	#                                                                              #
	#  ARRAY INDEX       : DUT_SET_ANNOUNCE_INTERVAL                               #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = port_num            DUT's port number.                  #
	#                                          (e.g., fa0/0).                      #
	#                                                                              #
	#   ATTEST_PARAM2    = announce_interval   announceInterval                    #
	#                                          (e.g., 1|2|4|8|16).                 #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_SET_ANNOUNCE_INTERVAL,ATTEST_SET,[incr idx]) "NOOP"
	set cli_set_record(DUT_SET_ANNOUNCE_INTERVAL,ATTEST_SET,[incr idx]) "NONE"

	#15.
	################################################################################
	#                                                                              #
	#  COMMAND           : SET_IPV4_ADDRESS                                        #
	#                                                                              #
	#  ARRAY INDEX       : DUT_SET_IPV4_ADDRESS                                    #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
	#                                  (e.g., fa0/0)                               #
	#                                                                              #
	#   ATTEST_PARAM2    = ip_address  IPv4 address.                               #
	#                                  (e.g., 192.168.200.1)                       #
	#                                                                              #
	#   ATTEST_PARAM3    = net_mask    Subnet mask.                                #
	#                                  (e.g., 255.255.255.0)                       #
	#                                                                              #
	#   ATTEST_PARAM4    = vlan_id     VLAN ID.                                    #
	#                                  (e.g., 100)                                 #
	#                                                                              #
	#  DEFINITION        : This function sets given IP address to a specific port  #
	#                      on the DUT.                                             #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_SET_IPV4_ADDRESS,ATTEST_SET,[incr idx])  "ifconfig ATTEST_PARAM1 ATTEST_PARAM2 netmask ATTEST_PARAM3"
	set cli_set_record(DUT_SET_IPV4_ADDRESS,ATTEST_SET,[incr idx])  "NONE"

	#16.
	################################################################################
	#                                                                              #
	#  COMMAND           : SET_WR_CONFIG                                           #
	#                                                                              #
	#  ARRAY INDEX       : DUT_SET_WR_CONFIG                                       #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#  ATTEST_PARAM1     = port_num       DUT's port number.                       #
	#                                     (e.g., fa0/0)                            #
	#                                                                              #
	#  ATTEST_PARAM2     = value          Value for wrConfig.                      #
	#                                     (e.g., "NON_WR"|"WR_S_ONLY"|"WR_M_ONLY"| #
	#                                            "WR_M_AND_S")                     #
	#                                                                              #
	#                                                                              #
	#  DEFINITION        : This function sets given value for wrConfig to a        #
	#                      specific port on the DUT.                               #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE                                                    #
	################################################################################

	set idx 0
	set cli_set_record(DUT_SET_WR_CONFIG,ATTEST_SET,[incr idx])    "NOOP"
	set cli_set_record(DUT_SET_WR_CONFIG,ATTEST_SET,[incr idx])    "NONE"

	#17.
	################################################################################
	#                                                                              #
	#  COMMAND           : SET_KNOWN_DELTA_TX                                      #
	#                                                                              #
	#  ARRAY INDEX       : DUT_SET_KNOWN_DELTA_TX                                  #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#  ATTEST_PARAM1     = port_num           DUT's port number.                   #
	#                                         (e.g., fa0/0)                        #
	#                                                                              #
	#  ATTEST_PARAM2     = known_delta_tx     Transmission fixed delay             #
	#                                         (e.g., 0)                            #
	#                                                                              #
	#                                                                              #
	#  DEFINITION        : This function sets given value for Transmission fixed   #
	#                      delay to a specific port on the DUT.                    #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE                                                    #
	################################################################################

	set idx 0
	set cli_set_record(DUT_SET_KNOWN_DELTA_TX,ATTEST_SET,[incr idx])  "NOOP"
	set cli_set_record(DUT_SET_KNOWN_DELTA_TX,ATTEST_SET,[incr idx])  "NONE"

	#18.
	################################################################################
	#                                                                              #
	#  COMMAND           : SET_KNOWN_DELTA_RX                                      #
	#                                                                              #
	#  ARRAY INDEX       : DUT_SET_KNOWN_DELTA_RX                                  #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#  ATTEST_PARAM1     = port_num           DUT's port number.                   #
	#                                         (e.g., fa0/0)                        #
	#                                                                              #
	#  ATTEST_PARAM2     = known_delta_rx     Reception fixed delay                #
	#                                         (e.g., 0)                            #
	#                                                                              #
	#                                                                              #
	#  DEFINITION        : This function sets given value for Reception fixed      #
	#                      delay to a specific port on the DUT.                    #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE                                                    #
	################################################################################

	set idx 0
	set cli_set_record(DUT_SET_KNOWN_DELTA_RX,ATTEST_SET,[incr idx])  "NOOP"
	set cli_set_record(DUT_SET_KNOWN_DELTA_RX,ATTEST_SET,[incr idx])  "NONE"

	#19.
	################################################################################
	#                                                                              #
	#  COMMAND           : SET_DELTAS_KNOWN                                        #
	#                                                                              #
	#  ARRAY INDEX       : DUT_SET_DELTAS_KNOWN                                    #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#  ATTEST_PARAM1     = port_num           DUT's port number.                   #
	#                                         (e.g., fa0/0)                        #
	#                                                                              #
	#  ATTEST_PARAM2     = deltas_known       Fixed delays                         #
	#                                         (e.g., 0)                            #
	#                                                                              #
	#                                                                              #
	#  DEFINITION        : This function sets given value for fixed delays to a    #
	#                      specific port on the DUT.                               #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE                                                    #
	################################################################################

	set idx 0
	set cli_set_record(DUT_SET_DELTAS_KNOWN,ATTEST_SET,[incr idx])    "NOOP"
	set cli_set_record(DUT_SET_DELTAS_KNOWN,ATTEST_SET,[incr idx])    "NONE"

	#20.
	################################################################################
	#                                                                              #
	#  COMMAND           : SET_CAL_PERIOD                                          #
	#                                                                              #
	#  ARRAY INDEX       : DUT_SET_CAL_PERIOD                                      #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#  ATTEST_PARAM1     = port_num           DUT's port number.                   #
	#                                         (e.g., fa0/0)                        #
	#                                                                              #
	#  ATTEST_PARAM2     = cal_period         calPeriod  (in microseconds)         #
	#                                         (e.g., 3000)                         #
	#                                                                              #
	#                                                                              #
	#  DEFINITION        : This function sets given value for calPeriod to a       #
	#                      specific port on the DUT.                               #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE                                                    #
	################################################################################

	set idx 0
	set cli_set_record(DUT_SET_CAL_PERIOD,ATTEST_SET,[incr idx])      "NOOP"
	set cli_set_record(DUT_SET_CAL_PERIOD,ATTEST_SET,[incr idx])      "NONE"

	#21.
	################################################################################
	#                                                                              #
	#  COMMAND           : SET_CAL_RETRY                                           #
	#                                                                              #
	#  ARRAY INDEX       : DUT_SET_CAL_RETRY                                       #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#  ATTEST_PARAM1     = port_num           DUT's port number.                   #
	#                                         (e.g., fa0/0)                        #
	#                                                                              #
	#  ATTEST_PARAM2     = cal_retry          calRetry                             #
	#                                         (e.g., 3)                            #
	#                                                                              #
	#                                                                              #
	#  DEFINITION        : This function sets given value for calRetry to a        #
	#                      specific port on the DUT.                               #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE                                                    #
	################################################################################

	set idx 0
	set cli_set_record(DUT_SET_CAL_RETRY,ATTEST_SET,[incr idx])       "NOOP"
	set cli_set_record(DUT_SET_CAL_RETRY,ATTEST_SET,[incr idx])       "NONE"

	#22.
	################################################################################
	#                                                                              #
	#  COMMAND           : PORT_WRPTP_ENABLE                                       #
	#                                                                              #
	#  ARRAY INDEX       : DUT_PORT_WRPTP_ENABLE                                   #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
	#                                  (e.g., fa0/0)                               #
	#                                                                              #
	#  DEFINITION        : This function enables Wrptp on a specific port on the   #
	#                      DUT.                                                    #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_PORT_WRPTP_ENABLE,ATTEST_SET,[incr idx])   "NOOP"
	set cli_set_record(DUT_PORT_WRPTP_ENABLE,ATTEST_SET,[incr idx])   "NONE"

	#23.
	################################################################################
	#                                                                              #
	#  COMMAND           : SET_WR_PRESENT_TIMEOUT                                  #
	#                                                                              #
	#  ARRAY INDEX       : DUT_SET_WR_PRESENT_TIMEOUT                              #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#  ATTEST_PARAM1     = port_num           DUT's port number.                   #
	#                                         (e.g., fa0/0)                        #
	#                                                                              #
	#  ATTEST_PARAM2     = timeout            WR_PRESENT_TIMEOUT (in milliseconds) #
	#                                         (e.g., 1000)                         #
	#                                                                              #
	#                                                                              #
	#  DEFINITION        : This function sets given value for WR_PRESENT_TIMEOUT to#
	#                      a specific port on the DUT.                             #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE                                                    #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_SET_WR_PRESENT_TIMEOUT,ATTEST_SET,[incr idx]) "NOOP"
	set cli_set_record(DUT_SET_WR_PRESENT_TIMEOUT,ATTEST_SET,[incr idx]) "NONE"

	#24.
	################################################################################
	#                                                                              #
	#  COMMAND           : SET_WR_M_LOCK_TIMEOUT                                   #
	#                                                                              #
	#  ARRAY INDEX       : DUT_SET_WR_M_LOCK_TIMEOUT                               #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#  ATTEST_PARAM1     = port_num           DUT's port number.                   #
	#                                         (e.g., fa0/0)                        #
	#                                                                              #
	#  ATTEST_PARAM2     = timeout            WR_M_LOCK_TIMEOUT (in milliseconds). #
	#                                         (e.g., 15000)                        #
	#                                                                              #
	#                                                                              #
	#  DEFINITION        : This function sets given value for WR_M_LOCK_TIMEOUT to #
	#                      a specific port on the DUT.                             #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE                                                    #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_SET_WR_M_LOCK_TIMEOUT,ATTEST_SET,[incr idx])  "NOOP"
	set cli_set_record(DUT_SET_WR_M_LOCK_TIMEOUT,ATTEST_SET,[incr idx])  "NONE"

	#25.
	################################################################################
	#                                                                              #
	#  COMMAND           : SET_WR_S_LOCK_TIMEOUT                                   #
	#                                                                              #
	#  ARRAY INDEX       : DUT_SET_WR_S_LOCK_TIMEOUT                               #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#  ATTEST_PARAM1     = port_num           DUT's port number.                   #
	#                                         (e.g., fa0/0)                        #
	#                                                                              #
	#  ATTEST_PARAM2     = timeout            WR_S_LOCK_TIMEOUT (in milliseconds). #
	#                                         (e.g., 15000)                        #
	#                                                                              #
	#                                                                              #
	#  DEFINITION        : This function sets given value for WR_S_LOCK_TIMEOUT to #
	#                      a specific port on the DUT.                             #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE                                                    #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_SET_WR_S_LOCK_TIMEOUT,ATTEST_SET,[incr idx]) "NOOP"
	set cli_set_record(DUT_SET_WR_S_LOCK_TIMEOUT,ATTEST_SET,[incr idx]) "NONE"

	#26.
	################################################################################
	#                                                                              #
	#  COMMAND           : SET_WR_LOCKED_TIMEOUT                                   #
	#                                                                              #
	#  ARRAY INDEX       : DUT_SET_WR_LOCKED_TIMEOUT                               #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#  ATTEST_PARAM1     = port_num           DUT's port number.                   #
	#                                         (e.g., fa0/0)                        #
	#                                                                              #
	#  ATTEST_PARAM2     = timeout            WR_LOCKED_TIMEOUT (in milliseconds). #
	#                                         (e.g., 300)                          #
	#                                                                              #
	#                                                                              #
	#  DEFINITION        : This function sets given value for WR_LOCKED_TIMEOUT to #
	#                      a specific port on the DUT.                             #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE                                                    #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_SET_WR_LOCKED_TIMEOUT,ATTEST_SET,[incr idx]) "NOOP"
	set cli_set_record(DUT_SET_WR_LOCKED_TIMEOUT,ATTEST_SET,[incr idx]) "NONE"

	#27.
	################################################################################
	#                                                                              #
	#  COMMAND           : SET_WR_CALIBRATION_TIMEOUT                              #
	#                                                                              #
	#  ARRAY INDEX       : DUT_SET_WR_CALIBRATION_TIMEOUT                          #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#  ATTEST_PARAM1     = port_num           DUT's port number.                   #
	#                                         (e.g., fa0/0)                        #
	#                                                                              #
	#  ATTEST_PARAM2     = timeout            WR_CALIBRATION_TIMEOUT               #
	#                                         (in milliseconds).                   #
	#                                         (e.g., 3000)                         #
	#                                                                              #
	#                                                                              #
	#  DEFINITION        : This function sets given value for                      #
	#                      WR_CALIBRATION_TIMEOUT to a specific port on the DUT.   #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE                                                    #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_SET_WR_CALIBRATION_TIMEOUT,ATTEST_SET,[incr idx]) "NOOP"
	set cli_set_record(DUT_SET_WR_CALIBRATION_TIMEOUT,ATTEST_SET,[incr idx]) "NONE"

	#28.
	################################################################################
	#                                                                              #
	#  COMMAND           : SET_WR_CALIBRATED_TIMEOUT                               #
	#                                                                              #
	#  ARRAY INDEX       : DUT_SET_WR_CALIBRATED_TIMEOUT                           #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#  ATTEST_PARAM1     = port_num           DUT's port number.                   #
	#                                         (e.g., fa0/0)                        #
	#                                                                              #
	#  ATTEST_PARAM2     = timeout            WR_CALIBRATED_TIMEOUT                #
	#                                         (in milliseconds).                   #
	#                                         (e.g., 300)                          #
	#                                                                              #
	#                                                                              #
	#  DEFINITION        : This function sets given value for WR_CALIBRATED_TIMEOUT#
	#                      to a specific port on the DUT.                          #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE                                                    #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_SET_WR_CALIBRATED_TIMEOUT,ATTEST_SET,[incr idx]) "NOOP"
	set cli_set_record(DUT_SET_WR_CALIBRATED_TIMEOUT,ATTEST_SET,[incr idx]) "NONE"

	#29.
	################################################################################
	#                                                                              #
	#  COMMAND           : SET_WR_RESP_CALIB_REQ_TIMEOUT                           #
	#                                                                              #
	#  ARRAY INDEX       : DUT_SET_WR_RESP_CALIB_REQ_TIMEOUT                       #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#  ATTEST_PARAM1     = port_num           DUT's port number.                   #
	#                                         (e.g., fa0/0)                        #
	#                                                                              #
	#  ATTEST_PARAM2     = timeout            WR_RESP_CALIB_REQ_TIMEOUT            #
	#                                         (in milliseconds).                   #
	#                                         (e.g., 3)                            #
	#                                                                              #
	#                                                                              #
	#  DEFINITION        : This function sets given value for                      #
	#                      WR_RESP_CALIB_REQ_TIMEOUT to a specific port on the DUT.#
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE                                                    #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_SET_WR_RESP_CALIB_REQ_TIMEOUT,ATTEST_SET,[incr idx]) "NOOP"
	set cli_set_record(DUT_SET_WR_RESP_CALIB_REQ_TIMEOUT,ATTEST_SET,[incr idx]) "NONE"

	#30.
	################################################################################
	#                                                                              #
	#  COMMAND           : SET_WR_STATE_RETRY                                      #
	#                                                                              #
	#  ARRAY INDEX       : DUT_SET_WR_STATE_RETRY                                  #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#  ATTEST_PARAM1     = port_num           DUT's port number.                   #
	#                                         (e.g., fa0/0)                        #
	#                                                                              #
	#  ATTEST_PARAM2     = wr_state_retry     wrStateRetry                         #
	#                                         (e.g., 3)                            #
	#                                                                              #
	#                                                                              #
	#  DEFINITION        : This function sets given value for wrStateRetry to a    #
	#                      specific port on the DUT.                               #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE                                                    #
	################################################################################

	set idx 0
	set cli_set_record(DUT_SET_WR_STATE_RETRY,ATTEST_SET,[incr idx])  "NOOP"
	set cli_set_record(DUT_SET_WR_STATE_RETRY,ATTEST_SET,[incr idx])  "NONE"

	################################################################################
	#                       DUT DISABLING FUNCTIONS                                #
	################################################################################

	#1.
	################################################################################
	#                                                                              #
	#  COMMAND           : PORT_DISABLE                                            #
	#                                                                              #
	#  ARRAY INDEX       : DUT_PORT_DISABLE                                        #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
	#                                  (e.g., fa0/0)                               #
	#                                                                              #
	#  DEFINITION        : This function makes the specific port administratively  #
	#                      down at the DUT.                                        #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_PORT_DISABLE,ATTEST_SET,[incr idx])  "NOOP"
	set cli_set_record(DUT_PORT_DISABLE,ATTEST_SET,[incr idx])  "NONE"

	#2.
	################################################################################
	#                                                                              #
	#  COMMAND           : PORT_PTP_DISABLE                                        #
	#                                                                              #
	#  ARRAY INDEX       : DUT_PORT_PTP_DISABLE                                    #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = port_num    Port on which ptp is to be disabled.        #
	#                                  (e.g., fa0/1)                               #
	#                                                                              #
	#  DEFINITION        : This function disables PTP on a specific port on the DUT#
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_PORT_PTP_DISABLE,ATTEST_SET,[incr idx])  "NOOP"
	set cli_set_record(DUT_PORT_PTP_DISABLE,ATTEST_SET,[incr idx])  "NONE"

	#3.
	################################################################################
	#                                                                              #
	#  COMMAND           : RESET_VLAN                                              #
	#                                                                              #
	#  ARRAY INDEX       : DUT_RESET_VLAN                                          #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = vlan_id     VLAN ID with which VLAN has to be deleted.  #
	#                                  (e.g., 100)                                 #
	#                                                                              #
	#   ATTEST_PARAM2    = port_num    Port which has to be removed from VLAN ID.  #
	#                                  (e.g., fa0/0)                               #
	#                                                                              #
	#  DEFINITION        : This function removes port from VLAN ID and deletes VLAN#
	#                      ID from the DUT.                                        #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_RESET_VLAN,ATTEST_SET,[incr idx])   "NOOP"
	set cli_set_record(DUT_RESET_VLAN,ATTEST_SET,[incr idx])   "NONE"

	#4.
	################################################################################
	#                                                                              #
	#  COMMAND           : RESET_VLAN_PRIORITY                                     #
	#                                                                              #
	#  ARRAY INDEX       : DUT_ReSET_VLAN_PRIORITY                                 #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = vlan_id     VLAN ID                                     #
	#                                  (e.g., 100)                                 #
	#                                                                              #
	#   ATTEST_PARAM2    = vlan_priority VLAN priority                             #
	#                                  (e.g., 1)                                   #
	#                                                                              #
	#   ATTEST_PARAM3    = port_num    Port which has to be associated to VLAN ID. #
	#                                  (e.g., fa0/0)                               #
	#                                                                              #
	#  DEFINITION        : This function resets priority to the VLAN ID on the DUT.#
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_RESET_VLAN_PRIORITY,ATTEST_SET,[incr idx]) "NOOP"
	set cli_set_record(DUT_RESET_VLAN_PRIORITY,ATTEST_SET,[incr idx]) "NONE"

	#5.
	################################################################################
	#                                                                              #
	#  COMMAND           : GLOBAL_PTP_DISABLE                                      #
	#                                                                              #
	#  ARRAY INDEX       : DUT_GLOBAL_PTP_DISABLE                                  #
	#                                                                              #
	#  INPUT PARAMETERS  : NONE.                                                   #
	#                                                                              #
	#  DEFINITION        : This function disables PTP on the DUT.                  #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_GLOBAL_PTP_DISABLE,ATTEST_SET,[incr idx])      "NOOP"
	set cli_set_record(DUT_GLOBAL_PTP_DISABLE,ATTEST_SET,[incr idx])      "NONE"

	#6.
	################################################################################
	#                                                                              #
	#  COMMAND           : RESET_COMMUNICATION_MODE                                #
	#                                                                              #
	#  ARRAY INDEX       : DUT_RESET_COMMUNICATION_MODE                            #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
	#                                  (e.g., fa0/0)                               #
	#                                                                              #
	#   ATTEST_PARAM2    = clock_mode  Clock mode.                                 #
	#                                  (e.g., Ordinary Clock)                      #
	#                                                                              #
	#   ATTEST_PARAM3    = ptp_version PTP version.                                #
	#                                  (Default: 2)                                #
	#                                                                              #
	#   ATTEST_PARAM4    = communication_mode Communication mode                   #
	#                                  (e.g., "Unicast"|"Multicast")               #
	#                                                                              #
	#  DEFINITION        : This function resets the communication mode on a        #
	#                      specific port of the DUT.                               #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_RESET_COMMUNICATION_MODE,ATTEST_SET,[incr idx]) "NOOP"
	set cli_set_record(DUT_RESET_COMMUNICATION_MODE,ATTEST_SET,[incr idx]) "NONE"

	#7.
	################################################################################
	#                                                                              #
	#  COMMAND           : RESET_DOMAIN                                            #
	#                                                                              #
	#  ARRAY INDEX       : DUT_RESET_DOMAIN                                        #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = clock_mode  Clock mode.                                 #
	#                                  (e.g., Ordinary Clock)                      #
	#                                                                              #
	#   ATTEST_PARAM2    = domain      Domain number.                              #
	#                                  (e.g., 0..255)                              #
	#                                                                              #
	#  DEFINITION        : This function resets domain number on the DUT.          #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_RESET_DOMAIN,ATTEST_SET,[incr idx])        "NOOP"
	set cli_set_record(DUT_RESET_DOMAIN,ATTEST_SET,[incr idx])        "NONE"

	#8.
	################################################################################
	#                                                                              #
	#  COMMAND           : RESET_NETWORK_PROTOCOL                                  #
	#                                                                              #
	#  ARRAY INDEX       : DUT_RESET_NETWORK_PROTOCOL                              #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
	#                                  (e.g., fa0/0)                               #
	#                                                                              #
	#   ATTEST_PARAM2    = clock_mode  clock_mode.                                 #
	#                                  (e.g., Ordinary Clock)                      #
	#                                                                              #
	#   ATTEST_PARAM3    = network_protocol Network protocol.                      #
	#                                  (e.g., "IEEE 802.3"|"UDP/IPv4")             #
	#                                                                              #
	#  DEFINITION        : This function resets the network protocol on a specific #
	#                      port of the DUT.                                        #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_RESET_NETWORK_PROTOCOL,ATTEST_SET,[incr idx]) "NOOP"
	set cli_set_record(DUT_RESET_NETWORK_PROTOCOL,ATTEST_SET,[incr idx]) "NONE"

	#9.
	################################################################################
	#                                                                              #
	#  COMMAND           : RESET_CLOCK_MODE                                        #
	#                                                                              #
	#  ARRAY INDEX       : DUT_RESET_CLOCK_MODE                                    #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
	#                                  (e.g., fa0/0)                               #
	#                                                                              #
	#   ATTEST_PARAM2    = clock_mode  Clock mode.                  .              #
	#                                  (e.g., Ordinary Clock)                      #
	#                                                                              #
	#  DEFINITION        : This function resets clock mode on a specific port of   #
	#                      DUT.                                                    #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_RESET_CLOCK_MODE,ATTEST_SET,[incr idx])    "NOOP"
	set cli_set_record(DUT_RESET_CLOCK_MODE,ATTEST_SET,[incr idx])    "NONE"

	#10.
	################################################################################
	#                                                                              #
	#  COMMAND           : RESET_CLOCK_STEP                                        #
	#                                                                              #
	#  ARRAY INDEX       : DUT_RESET_CLOCK_STEP                                    #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
	#                                  (e.g., fa0/0)                               #
	#                                                                              #
	#   ATTEST_PARAM2    = clock_mode  Clock mode.                  .              #
	#                                  (e.g., Ordinary Clock)                      #
	#                                                                              #
	#   ATTEST_PARAM3    = clock_step  Clock step.                                 #
	#                                  (e.g., "One-step"|"Two-step")               #
	#                                                                              #
	#  DEFINITION        : This function resets the clock-step on a specific port  #
	#                      on the DUT.                                             #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_RESET_CLOCK_STEP,ATTEST_SET,[incr idx])    "NOOP"
	set cli_set_record(DUT_RESET_CLOCK_STEP,ATTEST_SET,[incr idx])    "NONE"

	#11.
	################################################################################
	#                                                                              #
	#  COMMAND           : RESET_DELAY_MECHANISM                                   #
	#                                                                              #
	#  ARRAY INDEX       : DUT_RESET_DELAY_MECHANISM                               #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
	#                                  (e.g., fa0/0)                               #
	#                                                                              #
	#   ATTEST_PARAM2    = clock_mode  Clock mode.                  .              #
	#                                  (e.g., Ordinary Clock)                      #
	#                                                                              #
	#   ATTEST_PARAM3    = delay_mechanism Delay mechanism.                        #
	#                                  (e.g., e2e|p2p|disable)                     #
	#                                                                              #
	#  DEFINITION        : This function resets the delay mechanism to a specific  #
	#                      port on the DUT.                                        #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_RESET_DELAY_MECHANISM,ATTEST_SET,[incr idx])  "NOOP"
	set cli_set_record(DUT_RESET_DELAY_MECHANISM,ATTEST_SET,[incr idx])  "NONE"

	#12.
	################################################################################
	#                                                                              #
	#  COMMAND           : RESET_PRIORITY1                                         #
	#                                                                              #
	#  ARRAY INDEX       : DUT_RESET_PRIORITY1                                     #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = clock_mode  Clock mode.                                 #
	#                                  (e.g., Ordinary Clock).                     #
	#                                                                              #
	#   ATTEST_PARAM2    = priority1   Prority1                                    #
	#                                  (e.g., 0..255).                             #
	#                                                                              #
	#  DEFINITION        : This function resets the priority1 on the DUT.          #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_RESET_PRIORITY1,ATTEST_SET,[incr idx])     "NOOP"
	set cli_set_record(DUT_RESET_PRIORITY1,ATTEST_SET,[incr idx])     "NONE"

	#13.
	################################################################################
	#                                                                              #
	#  COMMAND           : RESET_PRIORITY2                                         #
	#                                                                              #
	#  ARRAY INDEX       : DUT_RESET_PRIORITY2                                     #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = clock_mode  Clock mode.                                 #
	#                                  (e.g., Ordinary Clock).                     #
	#                                                                              #
	#   ATTEST_PARAM2    = priority2   Prority2                                    #
	#                                  (e.g., 0..255).                             #
	#                                                                              #
	#  DEFINITION        : This function resets the priority2 on the DUT.          #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_RESET_PRIORITY2,ATTEST_SET,[incr idx])     "NOOP"
	set cli_set_record(DUT_RESET_PRIORITY2,ATTEST_SET,[incr idx])     "NONE"

	#14.
	################################################################################
	#                                                                              #
	#  COMMAND           : RESET_ANNOUNCE_INTERVAL                                 #
	#                                                                              #
	#  ARRAY INDEX       : DUT_RESET_ANNOUNCE_INTERVAL                             #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = port_num           DUT's port number.                   #
	#                                         (e.g., fa0/0).                       #
	#                                                                              #
	#   ATTEST_PARAM2    = announce_interval  announceInterval                     #
	#                                         (e.g., 1|2|4|8|16).                  #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_RESET_ANNOUNCE_INTERVAL,ATTEST_SET,[incr idx]) "NOOP"
	set cli_set_record(DUT_RESET_ANNOUNCE_INTERVAL,ATTEST_SET,[incr idx]) "NONE"

	#15.
	################################################################################
	#                                                                              #
	#  COMMAND           : RESET_IPV4_ADDRESS                                      #
	#                                                                              #
	#  ARRAY INDEX       : DUT_SET_IPV4_ADDRESS                                    #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
	#                                  (e.g., fa0/0)                               #
	#                                                                              #
	#   ATTEST_PARAM2    = ip_address  IPv4 address.                               #
	#                                  (e.g., 192.168.200.1)                       #
	#                                                                              #
	#   ATTEST_PARAM3    = net_mask    Subnet mask.                                #
	#                                  (e.g., 255.255.255.0)                       #
	#                                                                              #
	#   ATTEST_PARAM4    = vlan_id     VLAN ID.                                    #
	#                                  (e.g., 100)                                 #
	#                                                                              #
	#  DEFINITION        : This function resets given IP address to a specific port#
	#                      on the DUT.                                             #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_RESET_IPV4_ADDRESS,ATTEST_SET,[incr idx])  "ifconfig ATTEST_PARAM1 0.0.0.0"
	set cli_set_record(DUT_RESET_IPV4_ADDRESS,ATTEST_SET,[incr idx])  "NONE"

	#16.
	################################################################################
	#                                                                              #
	#  COMMAND           : RESET_WR_CONFIG                                         #
	#                                                                              #
	#  ARRAY INDEX       : DUT_RESET_WR_CONFIG                                     #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#  ATTEST_PARAM1     = port_num       DUT's port number.                       #
	#                                     (e.g., fa0/0)                            #
	#                                                                              #
	#  ATTEST_PARAM2     = value          Value for wrConfig.                      #
	#                                     (e.g., "NON_WR"|"WR_S_ONLY"|"WR_M_ONLY"| #
	#                                            "WR_M_AND_S")                     #
	#                                                                              #
	#                                                                              #
	#  DEFINITION        : This function resets given value for wrConfig to a      #
	#                      specific port on the DUT.                               #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE                                                    #
	################################################################################

	set idx 0
	set cli_set_record(DUT_RESET_WR_CONFIG,ATTEST_SET,[incr idx])     "NOOP"
	set cli_set_record(DUT_RESET_WR_CONFIG,ATTEST_SET,[incr idx])     "NONE"

	#17.
	################################################################################
	#                                                                              #
	#  COMMAND           : RESET_KNOWN_DELTA_TX                                    #
	#                                                                              #
	#  ARRAY INDEX       : DUT_RESET_KNOWN_DELTA_TX                                #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#  ATTEST_PARAM1     = port_num           DUT's port number.                   #
	#                                         (e.g., fa0/0)                        #
	#                                                                              #
	#  ATTEST_PARAM2     = known_delta_tx     Transmission fixed delay             #
	#                                         (e.g., 0)                            #
	#                                                                              #
	#                                                                              #
	#  DEFINITION        : This function resets given value for Transmission fixed #
	#                      delay to a specific port on the DUT.                    #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE                                                    #
	################################################################################

	set idx 0
	set cli_set_record(DUT_RESET_KNOWN_DELTA_TX,ATTEST_SET,[incr idx]) "NOOP"
	set cli_set_record(DUT_RESET_KNOWN_DELTA_TX,ATTEST_SET,[incr idx]) "NONE"

	#18.
	################################################################################
	#                                                                              #
	#  COMMAND           : RESET_KNOWN_DELTA_RX                                    #
	#                                                                              #
	#  ARRAY INDEX       : DUT_RESET_KNOWN_DELTA_RX                                #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#  ATTEST_PARAM1     = port_num           DUT's port number.                   #
	#                                         (e.g., fa0/0)                        #
	#                                                                              #
	#  ATTEST_PARAM2     = known_delta_rx     Reception fixed delay                #
	#                                         (e.g., 0)                            #
	#                                                                              #
	#                                                                              #
	#  DEFINITION        : This function resets given value for Reception fixed    #
	#                      delay to a specific port on the DUT.                    #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE                                                    #
	################################################################################

	set idx 0
	set cli_set_record(DUT_RESET_KNOWN_DELTA_RX,ATTEST_SET,[incr idx]) "NOOP"
	set cli_set_record(DUT_RESET_KNOWN_DELTA_RX,ATTEST_SET,[incr idx]) "NONE"

	#19.
	################################################################################
	#                                                                              #
	#  COMMAND           : RESET_DELTAS_KNOWN                                      #
	#                                                                              #
	#  ARRAY INDEX       : DUT_RESET_DELTAS_KNOWN                                  #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#  ATTEST_PARAM1     = port_num           DUT's port number.                   #
	#                                         (e.g., fa0/0)                        #
	#                                                                              #
	#  ATTEST_PARAM2     = deltas_known       Fixed delays                         #
	#                                         (e.g., 0)                            #
	#                                                                              #
	#                                                                              #
	#  DEFINITION        : This function resets given value for fixed delays to a  #
	#                      specific port on the DUT.                               #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE                                                    #
	################################################################################

	set idx 0
	set cli_set_record(DUT_RESET_DELTAS_KNOWN,ATTEST_SET,[incr idx])  "NOOP"
	set cli_set_record(DUT_RESET_DELTAS_KNOWN,ATTEST_SET,[incr idx])  "NONE"

	#20.
	################################################################################
	#                                                                              #
	#  COMMAND           : RESET_CAL_PERIOD                                        #
	#                                                                              #
	#  ARRAY INDEX       : DUT_RESET_CAL_PERIOD                                    #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#  ATTEST_PARAM1     = port_num           DUT's port number.                   #
	#                                         (e.g., fa0/0)                        #
	#                                                                              #
	#  ATTEST_PARAM2     = cal_period         calPeriod  (in microseconds)         #
	#                                         (e.g., 3000)                         #
	#                                                                              #
	#                                                                              #
	#  DEFINITION        : This function resets given value for calPeriod to a     #
	#                      specific port on the DUT.                               #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE                                                    #
	################################################################################

	set idx 0
	set cli_set_record(DUT_RESET_CAL_PERIOD,ATTEST_SET,[incr idx])    "NOOP"
	set cli_set_record(DUT_RESET_CAL_PERIOD,ATTEST_SET,[incr idx])    "NONE"

	#21.
	################################################################################
	#                                                                              #
	#  COMMAND           : RESET_CAL_RETRY                                         #
	#                                                                              #
	#  ARRAY INDEX       : DUT_RESET_CAL_RETRY                                     #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#  ATTEST_PARAM1     = port_num           DUT's port number.                   #
	#                                         (e.g., fa0/0)                        #
	#                                                                              #
	#  ATTEST_PARAM2     = cal_retry          calRetry                             #
	#                                         (e.g., 3)                            #
	#                                                                              #
	#                                                                              #
	#  DEFINITION        : This function resets given value for calRetry to a      #
	#                      specific port on the DUT.                               #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE                                                    #
	################################################################################

	set idx 0
	set cli_set_record(DUT_RESET_CAL_RETRY,ATTEST_SET,[incr idx])     "NOOP"
	set cli_set_record(DUT_RESET_CAL_RETRY,ATTEST_SET,[incr idx])     "NONE"

	#22.
	################################################################################
	#                                                                              #
	#  COMMAND           : PORT_WRPTP_DISABLE                                      #
	#                                                                              #
	#  ARRAY INDEX       : DUT_PORT_WRPTP_DISABLE                                  #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = port_num    Port on which wrptp is to be disabled.      #
	#                                  (e.g., fa0/1)                               #
	#                                                                              #
	#  DEFINITION        : This function disables Wrptp on a specific port on the  #
	#                      DUT.                                                    #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_PORT_WRPTP_DISABLE,ATTEST_SET,[incr idx])   "NOOP"
	set cli_set_record(DUT_PORT_WRPTP_DISABLE,ATTEST_SET,[incr idx])   "NONE"

	#23.
	################################################################################
	#                                                                              #
	#  COMMAND           : RESET_WR_PRESENT_TIMEOUT                                #
	#                                                                              #
	#  ARRAY INDEX       : DUT_RESET_WR_PRESENT_TIMEOUT                            #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#  ATTEST_PARAM1     = port_num           DUT's port number.                   #
	#                                         (e.g., fa0/0)                        #
	#                                                                              #
	#  ATTEST_PARAM2     = timeout            WR_PRESENT_TIMEOUT (in milliseconds) #
	#                                         (e.g., 1000)                         #
	#                                                                              #
	#                                                                              #
	#  DEFINITION        : This function resets given value for WR_PRESENT_TIMEOUT #
	#                      to a specific port on the DUT.                          #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE                                                    #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_RESET_WR_PRESENT_TIMEOUT,ATTEST_SET,[incr idx]) "NOOP"
	set cli_set_record(DUT_RESET_WR_PRESENT_TIMEOUT,ATTEST_SET,[incr idx]) "NONE"

	#24.
	################################################################################
	#                                                                              #
	#  COMMAND           : RESET_WR_M_LOCK_TIMEOUT                                 #
	#                                                                              #
	#  ARRAY INDEX       : DUT_RESET_WR_M_LOCK_TIMEOUT                             #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#  ATTEST_PARAM1     = port_num           DUT's port number.                   #
	#                                         (e.g., fa0/0)                        #
	#                                                                              #
	#  ATTEST_PARAM2     = timeout            WR_M_LOCK_TIMEOUT (in milliseconds). #
	#                                         (e.g., 15000)                        #
	#                                                                              #
	#                                                                              #
	#  DEFINITION        : This function resets given value for WR_M_LOCK_TIMEOUT  #
	#                      to a specific port on the DUT.                          #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE                                                    #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_RESET_WR_M_LOCK_TIMEOUT,ATTEST_SET,[incr idx]) "NOOP"
	set cli_set_record(DUT_RESET_WR_M_LOCK_TIMEOUT,ATTEST_SET,[incr idx]) "NONE"

	#25.
	################################################################################
	#                                                                              #
	#  COMMAND           : RESET_WR_S_LOCK_TIMEOUT                                 #
	#                                                                              #
	#  ARRAY INDEX       : DUT_RESET_WR_S_LOCK_TIMEOUT                             #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#  ATTEST_PARAM1     = port_num           DUT's port number.                   #
	#                                         (e.g., fa0/0)                        #
	#                                                                              #
	#  ATTEST_PARAM2     = timeout            WR_S_LOCK_TIMEOUT (in milliseconds). #
	#                                         (e.g., 15000)                        #
	#                                                                              #
	#                                                                              #
	#  DEFINITION        : This function resets given value for WR_S_LOCK_TIMEOUT  #
	#                      to a specific port on the DUT.                          #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE                                                    #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_RESET_WR_S_LOCK_TIMEOUT,ATTEST_SET,[incr idx]) "NOOP"
	set cli_set_record(DUT_RESET_WR_S_LOCK_TIMEOUT,ATTEST_SET,[incr idx]) "NONE"

	#26.
	################################################################################
	#                                                                              #
	#  COMMAND           : RESET_WR_LOCKED_TIMEOUT                                 #
	#                                                                              #
	#  ARRAY INDEX       : DUT_RESET_WR_LOCKED_TIMEOUT                             #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#  ATTEST_PARAM1     = port_num           DUT's port number.                   #
	#                                         (e.g., fa0/0)                        #
	#                                                                              #
	#  ATTEST_PARAM2     = timeout            WR_LOCKED_TIMEOUT (in milliseconds). #
	#                                         (e.g., 300)                          #
	#                                                                              #
	#                                                                              #
	#  DEFINITION        : This function resets given value for WR_LOCKED_TIMEOUT  #
	#                      to a specific port on the DUT.                          #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE                                                    #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_RESET_WR_LOCKED_TIMEOUT,ATTEST_SET,[incr idx]) "NOOP"
	set cli_set_record(DUT_RESET_WR_LOCKED_TIMEOUT,ATTEST_SET,[incr idx]) "NONE"

	#27.
	################################################################################
	#                                                                              #
	#  COMMAND           : RESET_WR_CALIBRATION_TIMEOUT                            #
	#                                                                              #
	#  ARRAY INDEX       : DUT_RESET_WR_CALIBRATION_TIMEOUT                        #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#  ATTEST_PARAM1     = port_num           DUT's port number.                   #
	#                                         (e.g., fa0/0)                        #
	#                                                                              #
	#  ATTEST_PARAM2     = timeout            WR_CALIBRATION_TIMEOUT               #
	#                                         (in milliseconds).                   #
	#                                         (e.g., 3000)                         #
	#                                                                              #
	#                                                                              #
	#  DEFINITION        : This function resets given value for                    #
	#                      WR_CALIBRATION_TIMEOUT to a specific port on the DUT.   #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE                                                    #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_RESET_WR_CALIBRATION_TIMEOUT,ATTEST_SET,[incr idx]) "NOOP"
	set cli_set_record(DUT_RESET_WR_CALIBRATION_TIMEOUT,ATTEST_SET,[incr idx]) "NONE"

	#28.
	################################################################################
	#                                                                              #
	#  COMMAND           : RESET_WR_CALIBRATED_TIMEOUT                             #
	#                                                                              #
	#  ARRAY INDEX       : DUT_RESET_WR_CALIBRATED_TIMEOUT                         #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#  ATTEST_PARAM1     = port_num           DUT's port number.                   #
	#                                         (e.g., fa0/0)                        #
	#                                                                              #
	#  ATTEST_PARAM2     = timeout            WR_CALIBRATED_TIMEOUT                #
	#                                         (in milliseconds).                   #
	#                                         (e.g., 300)                          #
	#                                                                              #
	#                                                                              #
	#  DEFINITION        : This function resets given value for                    #
	#                      WR_CALIBRATED_TIMEOUT to a specific port on the DUT.    #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE                                                    #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_RESET_WR_CALIBRATED_TIMEOUT,ATTEST_SET,[incr idx]) "NOOP"
	set cli_set_record(DUT_RESET_WR_CALIBRATED_TIMEOUT,ATTEST_SET,[incr idx]) "NONE"

	#29.
	################################################################################
	#                                                                              #
	#  COMMAND           : RESET_WR_RESP_CALIB_REQ_TIMEOUT                         #
	#                                                                              #
	#  ARRAY INDEX       : DUT_RESET_WR_RESP_CALIB_REQ_TIMEOUT                     #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#  ATTEST_PARAM1     = port_num           DUT's port number.                   #
	#                                         (e.g., fa0/0)                        #
	#                                                                              #
	#  ATTEST_PARAM2     = timeout            WR_RESP_CALIB_REQ_TIMEOUT            #
	#                                         (in milliseconds).                   #
	#                                         (e.g., 3)                            #
	#                                                                              #
	#                                                                              #
	#  DEFINITION        : This function resets given value for                    #
	#                      WR_RESP_CALIB_REQ_TIMEOUT to a specific port on the DUT.#
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE                                                    #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_RESET_WR_RESP_CALIB_REQ_TIMEOUT,ATTEST_SET,[incr idx]) "NOOP"
	set cli_set_record(DUT_RESET_WR_RESP_CALIB_REQ_TIMEOUT,ATTEST_SET,[incr idx]) "NONE"

	#30.
	################################################################################
	#                                                                              #
	#  COMMAND           : RESET_WR_STATE_RETRY                                    #
	#                                                                              #
	#  ARRAY INDEX       : DUT_RESET_WR_STATE_RETRY                                #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#  ATTEST_PARAM1     = port_num           DUT's port number.                   #
	#                                         (e.g., fa0/0)                        #
	#                                                                              #
	#  ATTEST_PARAM2     = wr_state_retry     wrStateRetry                         #
	#                                         (e.g., 3)                            #
	#                                                                              #
	#                                                                              #
	#  DEFINITION        : This function resets given value for wrStateRetry to a  #
	#                      specific port on the DUT.                               #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE                                                    #
	################################################################################

	set idx 0
	set cli_set_record(DUT_RESET_WR_STATE_RETRY,ATTEST_SET,[incr idx]) "NOOP"
	set cli_set_record(DUT_RESET_WR_STATE_RETRY,ATTEST_SET,[incr idx]) "NONE"

	#31.
	################################################################################
	#                                                                              #
	#  COMMAND           : COMMIT_CHANGES	                                       #
	#                                                                              #
	#  ARRAY INDEX       : DUT_COMMIT_CHANGES                                      #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = port_num    Port on which wrptp is to be disabled.      #
	#                                  (e.g., fa0/1)                               #
	#                                                                              #
	#  DEFINITION        : This function commit all PPSI changes on DUT			   #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_set_record(DUT_COMMIT_CHANGES,ATTEST_SET,[incr idx])    "/wr/bin/apply_dot-config local_config"
	set cli_set_record(DUT_COMMIT_CHANGES,ATTEST_TIME,$idx)         "30"
	set cli_set_record(DUT_COMMIT_CHANGES,ATTEST_SET,[incr idx])    "/etc/init.d/ppsi.sh restart"
	set cli_set_record(DUT_COMMIT_CHANGES,ATTEST_TIME,$idx)         "30"
	set cli_set_record(DUT_COMMIT_CHANGES,ATTEST_SET,[incr idx])    "NONE"

}

################################################################################
#                           DUT CHECK FUNCTIONS                                #
################################################################################

proc tee_get_dut_wrptp_cli_from_file {} {

	global cli_get_record
	global wrptp_env

	#1.
	################################################################################
	#                                                                              #
	#  COMMAND           : CHECK_PTP_PORT_STATE                                    #
	#                                                                              #
	#  ARRAY INDEX       : DUT_CHECK_PTP_PORT_STATE                                #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = port_num         DUT's port number.                     #
	#                                       (e.g., fa0/0)                          #
	#   ATTEST_PARAM2    = state            PTP portState.                         #
	#                                       (e.g.,"MASTER"|"SLAVE")                #
	#                                                                              #
	#  DEFINITION        : This function verifies the given PTP portState in a     #
	#                      specific port.                                          #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_get_record(DUT_CHECK_PTP_PORT_STATE,ATTEST_OUTPUT,[incr idx]) "wrs_dump_shmem | egrep 'cfg.iface_name:|.info.state:' | tr -s ' ' | cut -d' ' -f 1-2"
	set cli_get_record(DUT_CHECK_PTP_PORT_STATE,ATTEST_GET,[incr idx])    "NONE"

	#2.
	################################################################################
	#                                                                              #
	#  COMMAND           : CHECK_WR_PORT_STATE                                     #
	#                                                                              #
	#  ARRAY INDEX       : DUT_CHECK_WR_PORT_STATE                                 #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = port_num         DUT's port number.                     #
	#                                       (e.g., fa0/0)                          #
	#   ATTEST_PARAM2    = wr_port_state    wrPortState.                           #
	#                                       (e.g.,"IDLE"|"PRESENT"|"M_LOCK"|       #
	#                                              "S_LOCK"|"LOCKED"|"CALIBRATED"| #
	#                                              "CALIBRATION"|"RESP_CALIB_REQ"| #
	#                                              "WR_LINK_ON")                   #
	#                                                                              #
	#  DEFINITION        : This function verifies the given wrPortState in a       #
	#                      specific port.                                          #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_get_record(DUT_CHECK_WR_PORT_STATE,ATTEST_OUTPUT,[incr idx]) "wrs_dump_shmem | egrep 'cfg.iface_name:|.wr.state:' | tr -s ' ' | cut -d' ' -f 1-2"
	set cli_get_record(DUT_CHECK_WR_PORT_STATE,ATTEST_WAIT,$idx)         "ctdwa*"
	set cli_get_record(DUT_CHECK_WR_PORT_STATE,ATTEST_GET,[incr idx])    "NONE"

	#3.
	################################################################################
	#                                                                              #
	#  COMMAND           : CHECK_OTHER_PORT_DELTA_TX                               #
	#                                                                              #
	#  ARRAY INDEX       : DUT_CHECK_OTHER_PORT_DELTA_TX                           #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = port_num               DUT's port number.               #
	#                                             (e.g., fa0/0)                    #
	#   ATTEST_PARAM2    = other_port_delta_tx    value of deltaTx                 #
	#                                             (e.g., 0)                        #
	#                                                                              #
	#  DEFINITION        : This function verifies the value of deltaTx in a        #
	#                      specific port.                                          #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_get_record(DUT_CHECK_OTHER_PORT_DELTA_TX,ATTEST_OUTPUT,[incr idx]) "wrs_dump_shmem | egrep 'cfg.iface_name:|.wr.otherNodeDeltaTx:'"
	set cli_get_record(DUT_CHECK_OTHER_PORT_DELTA_TX,ATTEST_GET,[incr idx])    "NONE"

	#4.
	################################################################################
	#                                                                              #
	#  COMMAND           : CHECK_OTHER_PORT_DELTA_RX                               #
	#                                                                              #
	#  ARRAY INDEX       : DUT_CHECK_OTHER_PORT_DELTA_RX                           #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = port_num               DUT's port number.               #
	#                                             (e.g., fa0/0)                    #
	#   ATTEST_PARAM2    = other_port_delta_rx    value of deltaRx                 #
	#                                             (e.g., 0)                        #
	#                                                                              #
	#  DEFINITION        : This function verifies the value of deltaRx in a        #
	#                      specific port.                                          #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_get_record(DUT_CHECK_OTHER_PORT_DELTA_RX,ATTEST_OUTPUT,[incr idx]) "wrs_dump_shmem | egrep 'cfg.iface_name:|.wr.otherNodeDeltaRx:'"
	set cli_get_record(DUT_CHECK_OTHER_PORT_DELTA_RX,ATTEST_GET,[incr idx])    "NONE"

	#5.
	################################################################################
	#                                                                              #
	#  COMMAND           : CHECK_OTHER_PORT_CAL_PERIOD                             #
	#                                                                              #
	#  ARRAY INDEX       : DUT_CHECK_OTHER_PORT_CAL_PERIOD                         #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = port_num                 DUT's port number.             #
	#                                               (e.g., fa0/0)                  #
	#   ATTEST_PARAM2    = other_port_cal_period    value of calPeriod             #
	#                                               (e.g., 0)                      #
	#                                                                              #
	#  DEFINITION        : This function verifies the value of calPeriod in a      #
	#                      specific port.                                          #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_get_record(DUT_CHECK_OTHER_PORT_CAL_PERIOD,ATTEST_OUTPUT,[incr idx]) "wrs_dump_shmem | egrep 'cfg.iface_name:|.wr.otherNodeCalPeriod:'"
	set cli_get_record(DUT_CHECK_OTHER_PORT_CAL_PERIOD,ATTEST_GET,[incr idx])    "NONE"

	#6.
	################################################################################
	#                                                                              #
	#  COMMAND           : CHECK_OTHER_PORT_CAL_RETRY                              #
	#                                                                              #
	#  ARRAY INDEX       : DUT_CHECK_OTHER_PORT_CAL_RETRY                          #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = port_num                 DUT's port number.             #
	#                                               (e.g., fa0/0)                  #
	#   ATTEST_PARAM2    = other_port_cal_retry     value of calRetry              #
	#                                               (e.g., 0)                      #
	#                                                                              #
	#  DEFINITION        : This function verifies the value of calRetry in a       #
	#                      specific port.                                          #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_get_record(DUT_CHECK_OTHER_PORT_CAL_RETRY,ATTEST_OUTPUT,[incr idx]) "wrs_dump_shmem | egrep 'cfg.iface_name:|.wr.otherNodeCalRetry:'"
	set cli_get_record(DUT_CHECK_OTHER_PORT_CAL_RETRY,ATTEST_GET,[incr idx])    "NONE"

	#7.
	################################################################################
	#                                                                              #
	#  COMMAND           : CHECK_OTHER_PORT_CAL_SEND_PATTERN                       #
	#                                                                              #
	#  ARRAY INDEX       : DUT_CHECK_OTHER_PORT_CAL_SEND_PATTERN                   #
	#                                                                              #
	#  INPUT PARAMETERS  :                                                         #
	#                                                                              #
	#   ATTEST_PARAM1    = port_num                      DUT's port number.        #
	#                                                    (e.g., fa0/0)             #
	#   ATTEST_PARAM2    = other_port_cal_send_pattern   value of calSendPattern   #
	#                                                    (e.g.,TRUE|FALSE)         #
	#                                                                              #
	#  DEFINITION        : This function verifies the value of calSendPattern in a #
	#                      specific port.                                          #
	#                                                                              #
	#  OUTPUT PARAMETERS : NONE.                                                   #
	#                                                                              #
	################################################################################

	set idx 0
	set cli_get_record(DUT_CHECK_OTHER_PORT_CAL_SEND_PATTERN,ATTEST_OUTPUT,[incr idx])   "wrs_dump_shmem | egrep 'cfg.iface_name:|.wr.otherNodeCalSendPattern:'"
	set cli_get_record(DUT_CHECK_OTHER_PORT_CAL_SEND_PATTERN,ATTEST_GET,[incr idx])      "NONE"

}

################################################################################
#                          GENERAL UTILITY PROCEDURES                          #
################################################################################

#1.
################################################################################
# PROCEDURE NAME    : doScp                                                    #
#                                                                              #
# DEFINITION        : This is internal function used for copying dot-config to #
#                     DUT                                                      #
#                                                                              #
################################################################################

proc doScp {from to {password rootroot}} {

	spawn scp $from $to

	set rval  1
	set scp_session $spawn_id

	expect  {

		-i $scp_session

		"Password:" {
			send "$password"

			expect {
				-i $scp_session

					"denied" {
						printLog "Invalid password"
						set rval 0
					}
					eof {
#						printLog "(\"$from\" to \"$to\") copied successfully."
					}
					timeout {
						printLog "Connection timed out while copying file."
						set rval 0
					}
			}
		}
		"yes/no" {
			send "yes\n"
			exp_continue
		}
		eof {
			printLog "SCP: Unable to establish connection to DUT."
			set rval 0
		}
		timeout {
			printLog "SCP: Connection timed out."
			set rval 0
		}
		default {
			printLog "SCP: Cannot connect."
			set rval 0
		}
	}
	return $rval
}

#2.
################################################################################
# PROCEDURE NAME    : delEmptyEof                                              #
#                                                                              #
# DEFINITION        : This is internal function used for deleting empty lines  #
#                     at EOF                                                   #
#                                                                              #
################################################################################

proc delEmptyEof { config } {

	set read_fd [open $config "r"]
	set read_content [read $read_fd]
	close $read_fd

	set empty_line      ""
	set write_content   ""

	foreach line [split $read_content "\n"] {
		if {$line == ""} {
			append empty_line "\n"
		} else {
			append write_content $empty_line
			append write_content "$line\n"
			set empty_line ""
		}
	}

	set write_fd [open $config "w+"]
	puts $write_fd $write_content
	close $write_fd

	return 1
}

#3.
################################################################################
# PROCEDURE NAME    : printLog                                                 #
#                                                                              #
# DEFINITION        : This is internal function used for print logs of         #
#                     configuration parameter.                                 #
#                                                                              #
################################################################################

proc printLog {message} {

	puts "CLI: INFO: $message"
	return 1
}

#4.
################################################################################
# PROCEDURE NAME    : wrptp::dut_prepare_dot_config                            #                                                                              #
# DEFINITION        : Copy dot-config-v2.tmpl to dot-config-v2.active          #
#                                                                              #
################################################################################

proc  wrptp::dut_prepare_dot_config {} {

	global dotconfig_file
	global dotconfig_file_act
	
	set template_file       $dotconfig_file.tmpl

	if [file exists $dotconfig_file_act] {
		#delete active file
		exec rm -f $dotconfig_file_act
	}
	if [file exists $template_file] {
		#delete active file
		exec cp -f $template_file $dotconfig_file_act
	} else {
		printLog "Template file $template_file is missing !!!"
		return 0
	}
	return 1
}

#5.
################################################################################
# PROCEDURE NAME    : readDotConfig                                            #
#                                                                              #
# DEFINITION        : This is internal function used to read dot-config file   #
#                                                                              #
################################################################################

proc readDotConfig {contents} {
	upvar 1 $contents cfg
	global dotconfig_file_act
	
	if {[file exists $dotconfig_file_act]} {
		#Read dot-config file
		set dotconfig_fd [open $dotconfig_file_act "r"]
		set cfg       [read $dotconfig_fd]
		close $dotconfig_fd
		return 1
	} else {
		puts "$dotconfig_file_act file does not exists"
		return 0
	}

}

#6.
################################################################################
# PROCEDURE NAME    : saveDeployDotConfig                                      #
#                                                                              #
# DEFINITION        : This is internal function used to save dot-config file   #
#                                                                              #
################################################################################

proc saveDeployDotConfig {contents} {
	global dotconfig_file_act
	global host username password
	
	#Write configuration to dot-config file
	set dotconfig_fd [open $dotconfig_file_act  "w+"]
	puts $dotconfig_fd $contents
	close $dotconfig_fd

	delEmptyEof $dotconfig_file_act

	##copy the updated dot_config file to device
	doScp $dotconfig_file_act $username@$host:/wr/etc/dot-config $password


}


################################################################################
#                            DUT SET CALLBACK                                  #
################################################################################

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_port_enable                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) Port to be enabled.                         #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If port is enabled)                       #
#                                                                              #
#                      0 on Failure (If port could not be enabled)             #
#                                                                              #
#  DEFINITION        : This function enables a specific port on the DUT.       #
#                                                                              #
################################################################################

proc wrptp::callback_cli_port_enable {args} {

	array set param $args

	set session_handler $param(-session_handler)
	set port_num        $param(-port_num)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#        if {![Send_cli_command \
	-session_handler        $session_handler \
	-cmd_type               "DUT_PORT_ENABLE"\
	#                           -parameter1              $port_num]} {

	#             return 0
	#           }

	return 2
}

#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_port_ptp_enable                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) Port on which ptp is to be enabled.         #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is enabled on the specified port   #
#                                    at the DUT).                              #
#                                                                              #
#                      0 on Failure (If PTP could not be enabled on the        #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function enables PTP on a specific port on the DUT.#
#                                                                              #
################################################################################

proc wrptp::callback_cli_port_ptp_enable {args} {

	array set param $args

	set session_handler $param(-session_handler)
	set port_num        $param(-port_num)	

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#        if {![Send_cli_command \
	#                        -session_handler        $session_handler \
	#                        -cmd_type               "DUT_PORT_PTP_ENABLE"\
	#                        -parameter1              $port_num]} {
	#
	#            return 0
	#        }

	return 2
}

#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_set_vlan                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID with which VLAN has to be created.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#  port_num          : (Mandatory) Port which has to be associated to VLAN ID. #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If VLAN ID is created and port is         #
#                                    associated to it on the DUT).             #
#                                                                              #
#                      0 on Failure (If VLAN ID could not be created or port   #
#                                    is not associated to it on the DUT).      #
#                                                                              #
#  DEFINITION        : This function creates VLAN ID and associates given port #
#                      to it on the DUT.                                       #
#                                                                              #
################################################################################

proc wrptp::callback_cli_set_vlan {args} {

	array set param $args

	### Check Input parameters ###
	set session_handler          $param(-session_handler)
	set port_num                 $param(-port_num)
	set vlan_id                  $param(-vlan_id)

	#    if{![Send_cli_command \
	-session_handler        $session_handler \
	-cmd_type               "DUT_SET_VLAN"\
	-parameter1              $port_num\
	-parameter2              $vlan_id]

	#             return 0
	#      }
	return 2
}

#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_set_vlan_priority                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID with which VLAN has to be created.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#  vlan_priority     : (Mandatory) VLAN priority to be associated              #
#                                  (e.g., 1)                                   #
#                                                                              #
#  port_num          : (Mandatory) Port with VLAN ID configured.               #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If VLAN priority is associated0           #
#                                                                              #
#                      0 on Failure (If VLAN priority could not be associated) #
#                                                                              #
#  DEFINITION        : This function associates given priority to VLAN         #
#                                                                              #
################################################################################

proc wrptp::callback_cli_set_vlan_priority {args} {

	array set param $args

	set session_handler $param(-session_handler)
	set vlan_id         $param(-vlan_id)
	set vlan_priority   $param(-vlan_priority)
	set port_num        $param(-port_num)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler         $session_handler \
	#                       -cmd_type                "DUT_SET_VLAN_PRIORITY"\
	#                       -parameter1              $vlan_id\
	#                       -parameter2              $vlan_priority\
	#                       -parameter3              $port_num]} {
	#
	#           return 0
	#       }

	return 2
}

#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_global_ptp_enable                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is enabled on the DUT).            #
#                                                                              #
#                      0 on Failure (If PTP could not be enabled on the DUT).  #
#                                                                              #
#  DEFINITION        : This function enables PTP on the DUT.                   #
#                                                                              #
################################################################################

proc wrptp::callback_cli_global_ptp_enable { args } {

	## This proc set values in dot-config file based on Values from ATTEST GUI.
	array set param $args
	set session_handler $param(-session_handler)

	global ptp_env wrptp_env env

	set is_trunk_param_exists   0
	set is_vid_param_exists     0

	## TRANSPORT TYPE CONFIG ###
	array set TRANS_TYPE {
		"IEEE 802.3"    "RAW"
		"UDP/IPv4"      "UDP_IPV4"
	}

	set transport_from_ui   $TRANS_TYPE($wrptp_env(DUT_WRPTP_TRANS_TYPE))

	## WRPTP CONFIG ###
	array set WRCONFIG {
		NON_WR          ""
		WR_M_ONLY       "MASTER"
		WR_S_ONLY       "SLAVE"
		WR_M_AND_S      ""
	}

	set role_from_ui        $WRCONFIG($wrptp_env(DUT_WRPTP_WRCONFIG))

	set domain_from_ui      $wrptp_env(DUT_WRPTP_DOMAIN_NUMBER)
	set priority1_from_ui   $wrptp_env(DUT_WRPTP_PRIORITY1)
	set priority2_from_ui   $wrptp_env(DUT_WRPTP_PRIORITY2)

	set known_delta_tx_from_ui $wrptp_env(DUT_WRPTP_KNOWN_DELTA_TX)
	set known_delta_rx_from_ui $wrptp_env(DUT_WRPTP_KNOWN_DELTA_RX)

	set vlan_id             $::wrptp_vlan_id
	set dut_port_1          $env(dut_to_tee_1_LOCATION)

	set announce_interval   $::wrptp_log_announce_interval
	set sync_interval       $::wrptp_log_sync_interval

	set config ""
	set new_config ""
	if { ![ readDotConfig config ] } {
		return 0
	}

	#   puts "Performing configuration updation in dot-config."
	set portno [regexp {[0-9]+} $dut_port_1 port]
	if {[string length $port] == 1 } {
		set port "0$port"
	}

	set config_profile        "CONFIG_PORT${port}_INST01_PROFILE_"
	set config_domain         "CONFIG_PTP_OPT_DOMAIN_NUMBER"
	set config_priority1	  "CONFIG_PTP_OPT_PRIORITY1"
	set config_priority2	  "CONFIG_PTP_OPT_PRIORITY2"
	set config_ext_port_conf  "CONFIG_PTP_OPT_EXT_PORT_CONFIG_ENABLED"
	set config_desirade_state "CONFIG_PORT${port}_INST01_DESIRADE_STATE"
	set config_ingress        "CONFIG_PORT${port}_INST01_INGRESS_LATENCY"
	set config_egress         "CONFIG_PORT${port}_INST01_EGRESS_LATENCY"
	set config_protocol       "CONFIG_PORT${port}_INST01_PROTOCOL_"
	set config_ann_interval   "CONFIG_PORT${port}_INST01_ANNOUNCE_INTERVAL"
	set config_sync_interval  "CONFIG_PORT${port}_INST01_SYNC_INTERVAL"

	foreach line [split $config "\n"] {

		##  Profile
		set key $config_profile
		if {[regexp "^$key" $line ]} {
			set line "${key}WR=y"
			printLog $line
		}
		
		## ingress latency
		set key $config_ingress
		if {[regexp "^$key" $line ]} {
			set line "${key}=$known_delta_rx_from_ui"
			printLog $line
		}
		
		## egress latency
		set key $config_egress
		if {[regexp "^$key" $line ]} {
			set line "${key}=$known_delta_tx_from_ui"
			printLog $line
		}
		
		## external port configuration enabled
		set key $config_ext_port_conf
		if {[regexp "^$key" $line ]} {
			if { $role_from_ui == "" } {
				# Disable external port configuration
				set line "${key}=no"
			} else {
				# Enable external port configuration
				set line "${key}=yes"
			}
			printLog $line
		}

		## Set desired state
		set key $config_desirade_state
		if {[regexp "^$key" $line ]} {
			if { $role_from_ui != "" } {
				# Set desired state
				set line "${key}_${role_from_ui}=y"
			} else {
				set line "${key}_PASSIVE=n"
			}
			printLog $line
		}

		## Set protocol
		set key $config_protocol
		if {[regexp "^$key" $line ]} {
			set line "${key}$transport_from_ui=y"
			printLog $line
		}

		#set default domain number
		set key $config_domain
		if {[regexp "^$key" $line]} {
			## change domain based on default value
			set line "${key}=$domain_from_ui"
			printLog "$line"
		}

		## set announce interval
		set key $config_ann_interval
		if {[regexp "^$key" $line]} {
			set line "${key}=$announce_interval"
			printLog "$line"
		}

		## set sync interval
		set key $config_sync_interval
		if {[regexp "^$key" $line]} {
			set line "${key}=$sync_interval"
			printLog "$line"
		}

		## Change priority1 based on config
		set key $config_priority1
		if {[regexp $key $line]} {
			if {[regexp "^$key" $line]} {
				## change priority1 based on default value
				set line "${key}=$priority1_from_ui"
				printLog "$line"
			}
		}

		##Change priority2 based on config
		set key $config_priority2
		if {[regexp $key $line]} {
			if {[regexp "^$key" $line]} {
				## change priority1 based on default value
				set line "${key}=$priority2_from_ui"
				printLog "$line"
			}
		}

		##Change Vlan based on config
		if { [string match -nocase $wrptp_env(DUT_WRPTP_IS_VLAN_ENCAP) "True"] } {

			if {[string match "*CONFIG_VLANS_ENABLE*" $line]} {
				append new_config "CONFIG_VLANS_ENABLE=y\n"
				printLog "CONFIG_VLANS_ENABLE=y"
				continue
			}

			if {[string match "*CONFIG_VLANS_PORT$port\_MODE_TRUNK*" $line]} {
				append new_config "CONFIG_VLANS_PORT$port\_MODE_TRUNK=y\n"
				printLog "($dut_port_1) CONFIG_VLANS_PORT$port\_MODE_TRUNK=y"
				set is_trunk_param_exists   1
				continue
			}

			if {[string match "*CONFIG_VLANS_PORT$port\_VID*" $line]} {
				append new_config "CONFIG_VLANS_PORT$port\_VID=\"$vlan_id\"\n"
				printLog "($dut_port_1) CONFIG_VLANS_PORT$port\_VID=\"$vlan_id\""
				set is_vid_param_exists     1
				continue
			}

		}

		append new_config "$line\n"
	}

	if { [string match -nocase $wrptp_env(DUT_WRPTP_IS_VLAN_ENCAP) "True"] } {
		if {$is_trunk_param_exists != 1} {
			append new_config "CONFIG_VLANS_PORT$port\_MODE_TRUNK=y\n"
			printLog "($dut_port_1) CONFIG_VLANS_PORT$port\_MODE_TRUNK=y"
		}

		if {$is_vid_param_exists != 1} {
			append new_config "CONFIG_VLANS_PORT$port\_VID=\"$vlan_id\"\n"
			printLog "($dut_port_1) CONFIG_VLANS_PORT$port\_VID=\"$vlan_id\""
		}
	}

	saveDeployDotConfig $new_config
	
	set result [Send_cli_command\
		-session_handler $session_handler\
		-cmd_type        "DUT_GLOBAL_PTP_ENABLE"]
	return $result
}

#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_set_communication_mode              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  ptp_version       : (optional)  PTP version.                                #
#                                  (Default: 2)                                #
#                                                                              #
#  communication_mode : (Mandatory) Communication mode                         #
#                                  (e.g., "Unicast"|"Multicast")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If communication mode is set on the       #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If communication mode could not be        #
#                                    set on the specified port at the DUT).    #
#                                                                              #
#  DEFINITION        : This function sets the communication mode on a specific #
#                      port of the DUT.                                        #
#                                                                              #
################################################################################

proc wrptp::callback_cli_set_communication_mode {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set clock_mode          $param(-clock_mode)
	set communication_mode  $param(-communication_mode)
	set ptp_version         $param(-ptp_version)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -parameter1             $port_num\
	#                       -parameter2             $clock_mode\
	#                       -parameter3             $ptp_version\
	#                       -parameter4             $communication_mode]} {
	#           return 0
	#       }

	return 2
}

#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_set_domain                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  domain            : (Mandatory) Domain number.                              #
#                                  (e.g., 0..255)                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If domain number is set on the DUT).      #
#                                                                              #
#                      0 on Failure (If domain number could not able to set on #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function sets domain number on the DUT.            #
#                                                                              #
################################################################################

proc wrptp::callback_cli_set_domain {args} {

	array set param $args

	set session_handler $param(-session_handler)
	set clock_mode      $param(-clock_mode)
	set domain          $param(-domain)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_SET_DOMAIN"\
	#                       -parameter1             $clock_mode\
	#                       -parameter2             $domain]} {
	#           return 0
	#       }

	return 2
}

#8.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_set_network_protocol                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) clock_mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  network_protocol  : (Mandatory) Network protocol.                           #
#                                  (e.g., "IEEE 802.3"|"UDP/IPv4")             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If network protocol is set on specified   #
#                                    port at the DUT).                         #
#                                                                              #
#                      0 on Failure (If network protocol could not be set      #
#                                    on specified port at the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets the network protocol on a specific   #
#                      port of the DUT.                                        #
#                                                                              #
################################################################################

proc wrptp::callback_cli_set_network_protocol {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set clock_mode          $param(-clock_mode)
	set network_protocol    $param(-network_protocol)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_SET_NETWORK_PROTOCOL"\
	#                       -parameter1             $clock_mode\
	#                       -parameter2             $network_protocol]} {
	#           return 0
	#       }

	return 2
}

#9.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_set_clock_mode                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP clock mode is set on specified port#
#                                    of the DUT).                              #
#                                                                              #
#                      0 on Failure (If PTP clock mode could not be set on     #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function sets clock mode on a specific port of DUT.#
#                                                                              #
################################################################################

proc wrptp::callback_cli_set_clock_mode {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set clock_mode          $param(-clock_mode)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_SET_CLOCK_MODE"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $clock_mode]} {
	#           return 0
	#       }

	return 2
}

#10.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_set_clock_step                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  clock_step        : (Mandatory) Clock step.                                 #
#                                  (e.g., "One-step"|"Two-step")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If clock-step is set on the specified     #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If clock-step could not be able to set    #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets the clock-step on a specific port on #
#                      the DUT.                                                #
#                                                                              #
################################################################################

proc wrptp::callback_cli_set_clock_step {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set clock_mode          $param(-clock_mode)
	set clock_step          $param(-clock_step)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_SET_CLOCK_STEP"\
	#                       -parameter1             $port_num\
	#                       -parameter1             $clock_mode\
	#                       -parameter3             $clock_step]} {
	#           return 0
	#       }

	return 2
}

#11.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_set_delay_mechanism                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  delay_mechanism   : (Mandatory) Delay mechanism.                            #
#                                  (e.g., e2e|p2p|disable)                     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If delay mechanism is set on specified    #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If delay mechanism could not be set on    #
#                                    specified PTP port of the DUT).           #
#                                                                              #
#  DEFINITION        : This function sets the delay mechanism to a specific    #
#                      port on the DUT.                                        #
#                                                                              #
################################################################################

proc wrptp::callback_cli_set_delay_mechanism {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set clock_mode          $param(-clock_mode)
	set delay_mechanism     $param(-delay_mechanism)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_SET_DELAY_MECHANISM"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $clock_mode\
	#                       -parameter3             $delay_mechanism]} {
	#           return 0
	#       }

	return 2
}

#12.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_set_priority1                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority1         : (Mandatory) Prority1                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority1 is set on the DUT).          #
#                                                                              #
#                      0 on Failure (If priority1 could not be able to set on  #
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function sets the priority1 on the DUT.            #
#                                                                              #
################################################################################

proc wrptp::callback_cli_set_priority1 {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set clock_mode          $param(-clock_mode)
	set priority1           $param(-priority1)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_SET_PRIORITY1"\
	#                       -parameter1             $clock_mode\
	#                       -parameter2             $priority1]} {
	#           return 0
	#       }

	return 2
}

#13.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_set_priority2                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority2         : (Mandatory) Prority2                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority2 is set on the DUT).          #
#                                                                              #
#                      0 on Failure (If priority2 could not be able to set on  #
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function sets the priority2 on the DUT.            #
#                                                                              #
################################################################################

proc wrptp::callback_cli_set_priority2 {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set clock_mode          $param(-clock_mode)
	set priority2           $param(-priority2)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_SET_PRIORITY2"\
	#                       -parameter1             $clock_mode\
	#                       -parameter2             $priority2]} {
	#           return 0
	#       }

	return 2
}

#14.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_set_announce_interval               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  announce_interval  : (Mandatory) announceInterval.                          #
#                                  (e.g., 1|2|4|8|16)                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If announceInterval is set on the DUT).   #
#                                                                              #
#                      0 on Failure (If announceInterval could not be able to  #
#                                    set on the DUT).                          #
#                                                                              #
#  DEFINITION        : This function sets the announceInterval on the DUT.     #
#                                                                              #
################################################################################

proc wrptp::callback_cli_set_announce_interval {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set announce_interval   $param(-announce_interval)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_SET_ANNOUNCE_INTERVAL"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $announce_interval]} {
	#           return 0
	#       }

	return 2

}

#15.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_set_ipv4_address                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  ip_address        : (Mandatory) IPv4 address.                               #
#                                  (e.g., 192.168.200.1)                       #
#                                                                              #
#  net_mask          : (Mandatory) Subnet mask.                                #
#                                  (e.g., 255.255.255.0)                       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID.                                    #
#                                  (e.g., 100)                                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given IP address is set on specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given IP address could not be set on   #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function sets given IP address to a specific port  #
#                      on the DUT.                                             #
#                                                                              #
################################################################################

proc wrptp::callback_cli_set_ipv4_address {args} {

	array set param $args

	set session_handler $param(-session_handler)
	set port_num        $param(-port_num)
	set ip_address      $param(-ip_address)
	set net_mask        $param(-net_mask)
	set vlan_id         $param(-vlan_id)

	if {0} {

		global wrptp_env env

		## TRANSPORT TYPE CONFIG ###
		array set TRANS_TYPE {
			"IEEE 802.3"    "RAW"
			"UDP/IPv4"      "UDP_IPV4"
		}

		set transport_from_ui   $TRANS_TYPE($wrptp_env(DUT_WRPTP_TRANS_TYPE))

		set config ""
		set new_config ""
		if { ![ readDotConfig config ] } {
			return 0
		}
	
		#   puts "Performing configuration updation in dot-config."

		set portno [regexp {[0-9]+} $port_num port]
		if {[string length $port] == 1 } {
			set port "0$port"
		}

		set config_protocol       "CONFIG_PORT${port}_INST01_PROTOCOL_"

		foreach line [split $config "\n"] {

			## Set protocol
			set key $config_protocol
			if {[regexp "^$key" $line ]} {
				set line "${key}$transport_from_ui=y"
				printLog $line
			} else {
				if {[regexp $key $line ]} {
					continue; # skip this line
				}
			}
			append new_config "$line\n"
		}

		saveDeployDotConfig $new_config
		
		if {![Send_cli_command \
				   -session_handler        $session_handler \
				   -cmd_type               "DUT_SET_IPV4_ADDRESS"\
				   -parameter1             $port_num\
				   -parameter2             $ip_address\
				   -parameter3             $net_mask\
				   -parameter4             $vlan_id]} {

			return 0
		}

	}
	return 2
}

#16.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_set_wr_config                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  value             : (Mandatory) Value for wrConfig.                         #
#                                  (e.g., "NON_WR"|"WR_S_ONLY"|"WR_M_ONLY"|    #
#                                         "WR_M_AND_S")                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value for wrConfig is set on     #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given value for wrConfig could not be  #
#                                    set on specifed port of the DUT).         #
#                                                                              #
#  DEFINITION        : This function sets given value for wrConfig to a        #
#                      specific port on the DUT.                               #
#                                                                              #
################################################################################

proc wrptp::callback_cli_set_wr_config {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set value               $param(-value)

	global case_id group_name
	global ptp_env wrptp_env env

	if {!($group_name == "wcg" && $case_id == "002")} {

		set result [Send_cli_command \
			-session_handler        $session_handler \
			-cmd_type               "DUT_SET_WR_CONFIG"]

		return $result
	}

	## WRPTP CONFIG ###
	array set WRCONFIG {
		NON_WR          ""
		WR_M_ONLY       "MASTER"
		WR_S_ONLY       "SLAVE"
		WR_M_AND_S      ""
	}

	set role_from_ui        $WRCONFIG($value)

	set dut_port_1          $env(dut_to_tee_1_LOCATION)

	set config ""
	set new_config ""
	if { ![ readDotConfig config ] } {
		return 0
	}

	#   puts "Performing configuration updation in dot-config."

	set portno [regexp {[0-9]+} $dut_port_1 port]
	if {[string length $port] == 1 } {
		set port "0$port"
	}
	set config_ext_port_conf  "CONFIG_PTP_OPT_EXT_PORT_CONFIG_ENABLED"
	set config_desirade_state "CONFIG_PORT${port}_INST01_DESIRADE_STATE"

	printLog " role=\"$role_from_ui\" value=$value"
	foreach line [split $config "\n"] {

		## external port configuration enabled
		set key $config_ext_port_conf
		if {[regexp "^$key" $line ]} {
			if { $role_from_ui == "" } {
				# Disable external port configuration
				set line "${key}=no"
			} else {
				# Enable external port configuration
				set line "${key}=yes"
			}
			printLog $line
		} 

		## Set desired state
		set key $config_desirade_state
		if {[regexp "^$key" $line ]} {
			if { $role_from_ui == "" } {
				# External port configuration is disabled
				set line "${key}_PASSIVE=no"
			} else {
				# Enable external port configuration
				set line "${key}_${role_from_ui}=yes"
			}
			printLog $line
		} 
		
		append new_config "$line\n"
	}

	saveDeployDotConfig $new_config
	
	set result  [Send_cli_command \
		-session_handler        $session_handler \
		-cmd_type               "DUT_SET_WR_CONFIG"]

	return $result
}

#17.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_set_known_delta_tx                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  known_delta_tx    : (Mandatory) Transmission fixed delay.                   #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of transmission fixed delay#
#                                    is set on specified port of the DUT).     #
#                                                                              #
#                      0 on Failure (If given value of transmission fixed delay#
#                                    could not be set on specifed port of the  #
#                                    DUT).                                     #
#  DEFINITION        : This function sets Transmission fixed delay to a        #
#                      specific port on the DUT.                               #
#                                                                              #
################################################################################

proc wrptp::callback_cli_set_known_delta_tx {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set known_delta_tx      $param(-known_delta_tx)

	global case_id group_name
	global ptp_env wrptp_env env

	#   if {(($group_name == "wcg" && $case_id == "003") || ($group_name == "wcg" && $case_id == "004"))} {

	set config ""
	set new_config ""
	if { ![ readDotConfig config ] } {
		return 0
	}

	#       puts "Performing configuration updation in dot-config."

	set portno [regexp {[0-9]+} $port_num port]
	if {[string length $port] == 1 } {
		set port "0$port"
	}

	set config_egress         "CONFIG_PORT${port}_INST01_EGRESS_LATENCY"

	foreach line [split $config "\n"] {

		## egress latency
		set key $config_egress
		if {[regexp "^$key" $line ]} {
			set line "${key}=$known_delta_tx"
			printLog $line
		} else {
			if {[regexp $key $line ]} {
				continue; # skip this line
			}
		}

		append new_config "$line\n"
	}

	saveDeployDotConfig $new_config
	
	set result  [Send_cli_command \
		-session_handler        $session_handler \
		-cmd_type               "DUT_SET_KNOWN_DELTA_TX"\
		-parameter1             $port_num\
		-parameter2             $known_delta_tx]

	return $result
	#   } else {
	#
	#       set result [Send_cli_command \
	#                           -session_handler        $session_handler \
	#                           -cmd_type               "DUT_SET_KNOWN_DELTA_TX"\
	#                           -parameter1             $port_num\
	#                           -parameter2             $known_delta_tx]
	#
	#       return $result
	#
	#   }
}

#18.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_set_known_delta_rx                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  known_delta_rx    : (Mandatory) Reception fixed delay.                      #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of reception fixed delay   #
#                                    is set on specified port of the DUT).     #
#                                                                              #
#                      0 on Failure (If given value of reception fixed delay   #
#                                    could not be set on specifed port of the  #
#                                    DUT).                                     #
#  DEFINITION        : This function sets reception fixed delay to a           #
#                      specific port on the DUT.                               #
#                                                                              #
################################################################################

proc wrptp::callback_cli_set_known_delta_rx {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set known_delta_rx      $param(-known_delta_rx)

	global case_id group_name
	global ptp_env wrptp_env env

	#   if {(($group_name == "wcg" && $case_id == "005") || ($group_name == "wcg" && $case_id == "006"))} {

	set config ""
	set new_config ""
	if { ![ readDotConfig config ] } {
		return 0
	}

	#       puts "Performing configuration updation in dot-config."

	set portno [regexp {[0-9]+} $port_num port]
	if {[string length $port] == 1 } {
		set port "0$port"
	}

	set config_ingress        "CONFIG_PORT${port}_INST01_INGRESS_LATENCY"

	foreach line [split $config "\n"] {

		## ingress latency
		set key $config_ingress
		if {[regexp "^$key" $line ]} {
			set line "${key}=$known_delta_rx"
			printLog $line
		} else {
			if {[regexp $key $line ]} {
				continue; # skip this line
			}
		}

		append new_config "$line\n"
	}

	saveDeployDotConfig $new_config
	
	set result  [Send_cli_command \
		-session_handler        $session_handler \
		-cmd_type               "DUT_SET_KNOWN_DELTA_RX"\
		-parameter1             $port_num\
		-parameter2             $known_delta_rx]

	return $result
	#   } else {
	#
	#       set result [Send_cli_command \
	#                           -session_handler        $session_handler \
	#                           -cmd_type               "DUT_SET_KNOWN_DELTA_RX"\
	#                           -parameter1             $port_num\
	#                           -parameter2             $known_delta_rx]
	#
	#       return $result
	#   }
}

#19.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_set_deltas_known                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of fixed delays            #
#                                    is set on specified port of the DUT).     #
#                                                                              #
#                      0 on Failure (If given value of fixed delays            #
#                                    could not be set on specifed port of the  #
#                                    DUT).                                     #
#  DEFINITION        : This function sets  fixed delays to a                   #
#                      specific port on the DUT.                               #
#                                                                              #
################################################################################

proc wrptp::callback_cli_set_deltas_known {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_SET_DELTAS_KNOWN"\
	#                       -parameter1             $port_num]} {
	#           return 0
	#       }

	return 2
}

#20.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_set_cal_period                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  cal_period        : (Mandatory) calPeriod  (in microseconds)                #
#                                  (e.g., 3000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of calPeriod is set on     #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given value of calPeriod could not be  #
#                                    set on specifed port of the DUT).         #
#                                                                              #
#  DEFINITION        : This function sets calPeriod to a                       #
#                      specific port on the DUT.                               #
#                                                                              #
################################################################################

proc wrptp::callback_cli_set_cal_period {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set cal_period          $param(-cal_period)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_SET_CAL_PERIOD"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $cal_period]} {
	#           return 0
	#       }

	return 2
}

#21.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_set_cal_retry                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  cal_retry         : (Mandatory) calRetry                                    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of calRetry                #
#                                    is set on specified port of the DUT).     #
#                                                                              #
#                      0 on Failure (If given value of calRetry                #
#                                    could not be set on specifed port of the  #
#                                    DUT).                                     #
#  DEFINITION        : This function sets calRetry to a  specified port on the #
#                      DUT.                                                    #
#                                                                              #
################################################################################

proc wrptp::callback_cli_set_cal_retry {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set cal_retry           $param(-cal_retry)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_SET_CAL_RETRY"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $cal_retry]} {
	#           return 0
	#       }

	return 2
}

#22.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_port_wrptp_enable                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) Port on which wrptp is to be enabled.       #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If Wrptp is enabled on the specified port #
#                                    at the DUT).                              #
#                                                                              #
#                      0 on Failure (If Wrptp could not be enabled on the      #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function enables Wrptp on a specific port on the   #
#                      DUT.                                                    #
#                                                                              #
################################################################################

proc wrptp::callback_cli_port_wrptp_enable {args} {

	array set param $args

	set session_handler $param(-session_handler)
	set port_num        $param(-port_num)

	set config ""
	set new_config ""
	if { ![ readDotConfig config ] } {
		return 0
	}

	#   puts "Performing configuration updation in dot-config."

	set portno [regexp {[0-9]+} $port_num port]
	if {[string length $port] == 1 } {
		set port "0$port"
	}

	set config_profile        "CONFIG_PORT${port}_INST01_PROFILE_"
	set config_ext_port_conf  "CONFIG_PTP_OPT_EXT_PORT_CONFIG_ENABLED"

	foreach line [split $config "\n"] {

		##  Profile
		set key $config_profile
		if {[regexp "^$key" $line ]} {
			set line "${key}WR=y"
			printLog $line
		} else {
			if {[regexp $key $line ]} {
				continue; # skip this line
			}
		}

		## external port configuration enabled
		set key $config_ext_port_conf
		if {[regexp "^$key" $line ]} {
			# Disable external port configuration
			set line "${key}=no"
			printLog $line
		} else {
			if {[regexp $key $line ]} {
				continue; # skip this line
			}
		}

		append new_config "$line\n"
	}

	saveDeployDotConfig $new_config
	
	if {![Send_cli_command \
					-session_handler        $session_handler \
					-cmd_type               "DUT_PORT_WRPTP_ENABLE"\
					-parameter1             $port_num]} {
		return 0
	}

	return 1
}

#23.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_set_wr_present_timeout              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_PRESENT_TIMEOUT (in milliseconds).       #
#                                  (e.g., 1000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of WR_PRESENT_TIMEOUT      #
#                                    is set on specified port of the DUT).     #
#                                                                              #
#                      0 on Failure (If given value of WR_PRESENT_TIMEOUT      #
#                                    could not be set on specifed port of the  #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function sets WR_PRESENT_TIMEOUT to a specific port#
#                      on the DUT.                                             #
#                                                                              #
################################################################################

proc wrptp::callback_cli_set_wr_present_timeout {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set timeout             $param(-timeout)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_SET_WR_PRESENT_TIMEOUT"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $timeout]} {
	#           return 0
	#       }

	return 2
}

#24.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_set_wr_m_lock_timeout               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_M_LOCK_TIMEOUT (in milliseconds).        #
#                                  (e.g., 15000)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of WR_M_LOCK_TIMEOUT       #
#                                    is set on specified port of the DUT).     #
#                                                                              #
#                      0 on Failure (If given value of WR_M_LOCK_TIMEOUT       #
#                                    could not be set on specifed port of the  #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function sets WR_M_LOCK_TIMEOUT to a specific port #
#                      on the DUT.                                             #
#                                                                              #
################################################################################

proc wrptp::callback_cli_set_wr_m_lock_timeout {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set timeout             $param(-timeout)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_SET_WR_M_LOCK_TIMEOUT"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $timeout]} {
	#           return 0
	#       }

	return 2
}

#25.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_set_wr_s_lock_timeout               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_S_LOCK_TIMEOUT (in milliseconds).        #
#                                  (e.g., 15000)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of WR_S_LOCK_TIMEOUT       #
#                                    is set on specified port of the DUT).     #
#                                                                              #
#                      0 on Failure (If given value of WR_S_LOCK_TIMEOUT       #
#                                    could not be set on specifed port of the  #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function sets WR_S_LOCK_TIMEOUT to a specific port #
#                      on the DUT.                                             #
#                                                                              #
################################################################################

proc wrptp::callback_cli_set_wr_s_lock_timeout {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set timeout             $param(-timeout)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_SET_WR_S_LOCK_TIMEOUT"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $timeout]} {
	#           return 0
	#       }

	return 2
}

#26.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_set_wr_locked_timeout               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_LOCKED_TIMEOUT (in milliseconds).        #
#                                  (e.g., 300)                                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of WR_LOCKED_TIMEOUT       #
#                                    is set on specified port of the DUT).     #
#                                                                              #
#                      0 on Failure (If given value of WR_LOCKED_TIMEOUT       #
#                                    could not be set on specifed port of the  #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function sets WR_LOCKED_TIMEOUT to a specific port #
#                      on the DUT.                                             #
#                                                                              #
################################################################################

proc wrptp::callback_cli_set_wr_locked_timeout {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set timeout             $param(-timeout)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_SET_WR_LOCKED_TIMEOUT"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $timeout]} {
	#           return 0
	#       }

	return 2
}

#27.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_set_wr_calibration_timeout          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_CALIBRATION_TIMEOUT (in milliseconds).   #
#                                  (e.g., 3000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of WR_CALIBRATION_TIMEOUT  #
#                                    is set on specified port of the DUT).     #
#                                                                              #
#                      0 on Failure (If given value of WR_CALIBRATION_TIMEOUT  #
#                                    could not be set on specifed port of the  #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function sets WR_CALIBRATION_TIMEOUT to a specific #
#                      port on the DUT.                                        #
#                                                                              #
################################################################################

proc wrptp::callback_cli_set_wr_calibration_timeout {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set timeout             $param(-timeout)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_SET_WR_CALIBRATION_TIMEOUT"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $timeout]} {
	#           return 0
	#       }

	return 2
}

#28.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_set_wr_calibrated_timeout           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_CALIBRATED_TIMEOUT (in milliseconds).    #
#                                  (e.g., 300)                                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of WR_CALIBRATED_TIMEOUT   #
#                                    is set on specified port of the DUT).     #
#                                                                              #
#                      0 on Failure (If given value of WR_CALIBRATED_TIMEOUT   #
#                                    could not be set on specifed port of the  #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function sets WR_CALIBRATED_TIMEOUT to a specific  #
#                      port on the DUT.                                        #
#                                                                              #
################################################################################

proc wrptp::callback_cli_set_wr_calibrated_timeout {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set timeout             $param(-timeout)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_SET_WR_CALIBRATED_TIMEOUT"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $timeout]} {
	#           return 0
	#       }

	return 2
}

#29.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_set_wr_resp_calib_req_timeout       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_RESP_CALIB_REQ_TIMEOUT (in milliseconds).#
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of                         #
#                                    WR_RESP_CALIB_REQ_TIMEOUT is set on       #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given value of                         #
#                                    WR_RESP_CALIB_REQ_TIMEOUT could not be set#
#                                    on specifed port of the DUT).             #
#                                                                              #
#  DEFINITION        : This function sets WR_RESP_CALIB_REQ_TIMEOUT to a       #
#                      specific port on the DUT.                               #
#                                                                              #
################################################################################

proc wrptp::callback_cli_set_wr_resp_calib_req_timeout {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set timeout             $param(-timeout)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_SET_WR_RESP_CALIB_REQ_TIMEOUT"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $timeout]} {
	#           return 0
	#       }

	return 2
}

#30.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_set_wr_state_retry                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  wr_state_retry    : (Mandatory) wrStateRetry                                #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of wrStateRetry is set on  #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given value of wrStateRetry could not  #
#                                    be set on specifed port of the DUT).      #
#                                                                              #
#  DEFINITION        : This function sets  wrStateRetry to a                   #
#                      specific port on the DUT.                               #
#                                                                              #
################################################################################

proc wrptp::callback_cli_set_wr_state_retry {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set wr_state_retry      $param(-wr_state_retry)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_SET_WR_STATE_RETRY"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $wr_state_retry]} {
	#           return 0
	#       }

	return 2
}

################################################################################
#                       DUT DISABLING CALLBACK                                 #
################################################################################

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_port_disable                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) Port to be disabled.                        #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If port is disabled)                      #
#                                                                              #
#                      0 on Failure (If port could not be disabled)            #
#                                                                              #
#  DEFINITION        : This function disables a specific port on the DUT.      #
#                                                                              #
################################################################################

proc wrptp::callback_cli_port_disable {args} {

	array set param $args

	set session_handler $param(-session_handler)
	set port_num        $param(-port_num)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#        if {![Send_cli_command \
	#                        -session_handler        $session_handler \
	#                        -cmd_type               "DUT_PORT_DISABLE"\
	#                        -parameter1              $port_num]} {
	#
	#            return 0
	#        }

	return 2
}

#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_port_ptp_disable                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) Port on which ptp is to be disabled.        #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is disabled on the specified port  #
#                                    at the DUT).                              #
#                                                                              #
#                      0 on Failure (If PTP could not be disabled on the       #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function disables PTP on a specific port on the DUT#
#                                                                              #
################################################################################

proc wrptp::callback_cli_port_ptp_disable {args} {

	array set param $args

	set session_handler $param(-session_handler)
	set port_num        $param(-port_num)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#        if {![Send_cli_command \
	#                        -session_handler        $session_handler \
	#                        -cmd_type               "DUT_PORT_PTP_DISABLE"\
	#                        -parameter1              $port_num]} {
	#
	#            return 0
	#        }

	return 2
}

#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_reset_vlan                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID with which VLAN has to be deleted.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#  port_num          : (Mandatory) Port which has to be removed from VLAN ID.  #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If port is removed from VLAN ID and VLAN  #
#                                    ID is deleted from the DUT).              #
#                                                                              #
#                      0 on Failure (If port could not be removed from VLAN ID #
#                                    and VLAN ID is not deledted from the DUT).#
#                                                                              #
#  DEFINITION        : This function removes port from VLAN ID and deletes VLAN#
#                      ID from the DUT.                                        #
#                                                                              #
################################################################################

proc wrptp::callback_cli_reset_vlan {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set vlan_id             $param(-vlan_id)
	set port_num            $param(-port_num)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_RESET_VLAN"\
	#                       -parameter1              $vlan_id\
	#                       -parameter2              $port_num]} {
	#           return 0
	#       }

	return 2
}

#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_reset_vlan_priority                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID with which VLAN has to be created.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#  vlan_priority     : (Mandatory) VLAN priority to be associated              #
#                                  (e.g., 1)                                   #
#                                                                              #
#  port_num          : (Mandatory) Port with VLAN ID configured.               #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If VLAN priority is disassociated)        #
#                                                                              #
#                      0 on Failure (If VLAN priority could not be             #
#                                    disassociated)                            #
#                                                                              #
#  DEFINITION        : This function disassociates given priority from VLAN    #
#                                                                              #
################################################################################

proc wrptp::callback_cli_reset_vlan_priority {args} {

	array set param $args

	set session_handler $param(-session_handler)
	set vlan_id         $param(-vlan_id)
	set vlan_priority   $param(-vlan_priority)
	set port_num        $param(-port_num)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler         $session_handler \
	#                       -cmd_type                "DUT_RESET_VLAN_PRIORITY"\
	#                       -parameter1              $vlan_id\
	#                       -parameter2              $vlan_priority\
	#                       -parameter3              $port_num]} {
	#
	#           return 0
	#       }

	return 2
}

#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_global_ptp_disable                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is disabled on the DUT).           #
#                                                                              #
#                      0 on Failure (If PTP could not be disabled on the DUT). #
#                                                                              #
#  DEFINITION        : This function disables PTP on the DUT.                  #
#                                                                              #
################################################################################

proc wrptp::callback_cli_global_ptp_disable {args} {

	array set param $args
	set session_handler $param(-session_handler)

	global ptp_env wrptp_env env

	## TRANSPORT TYPE CONFIG ###
	array set TRANS_TYPE {
		"IEEE 802.3"    "RAW"
		"UDP/IPv4"      "UDP_IPV4"
	}

	set transport_from_ui   $TRANS_TYPE($wrptp_env(DUT_WRPTP_TRANS_TYPE))

	## WRPTP CONFIG ###
	array set WRCONFIG {
		NON_WR          ""
		WR_M_ONLY       "MASTER"
		WR_S_ONLY       "SLAVE"
		WR_M_AND_S      ""
	}

	set role_from_ui        $WRCONFIG($wrptp_env(DUT_WRPTP_WRCONFIG))

	set domain_from_ui      $wrptp_env(DUT_WRPTP_DOMAIN_NUMBER)
	set priority1_from_ui   $wrptp_env(DUT_WRPTP_PRIORITY1)
	set priority2_from_ui   $wrptp_env(DUT_WRPTP_PRIORITY2)
	set vlan_id             $::wrptp_vlan_id
	set dut_port_1          $env(dut_to_tee_1_LOCATION)

	set announce_interval   $::wrptp_log_announce_interval

	set config ""
	set new_config ""
	if { ![ readDotConfig config ] } {
		return 0
	}

	#   puts "Performing configuration updation in dot-config."

	set portno [regexp {[0-9]+} $dut_port_1 port]
	if {[string length $port] == 1 } {
		set port "0$port"
	}

	set config_profile "CONFIG_PORT${port}_INST01_PROFILE_"
	set config_protocol "CONFIG_PORT${port}_INST01_PROTOCOL_"
	set config_mechanism "CONFIG_PORT${port}_INST01_MECHANISM_"
	set config_announce_interval "CONFIG_PORT${port}_INST01_ANNOUNCE_INTERVAL"
	set config_domain "CONFIG_PTP_OPT_DOMAIN_NUMBER"
	set config_priority1 "CONFIG_PTP_OPT_PRIORITY1"
	set config_priority2 "CONFIG_PTP_OPT_PRIORITY2"

	foreach line [split $config "\n"] {

		# Set profile
		set key $config_profile
		if {[regexp "^$key" $line ]} {
			set line "${key}PTP=y\n"
		} else {
			if {[regexp $key $line ]} {
				continue; # skip line
			}
		}

		# Set mechanism
		set key $config_mechanism
		if {[regexp "^$key" $line ]} {
			set line "${key}E2E=y\n"
		} else {
			if {[regexp $key $line ]} {
				continue; # skip line
			}
		}

		#set default domain number
		set key $config_domain
		if {[regexp "^$key" $line]} {
			## change domain based on default value
			set line "${key}=$domain_from_ui"
			printLog "$line"
		} else {
			if {[regexp $key $line ]} {
				continue; # skip line
			}
		}

		##Change priority1 based on config
		set key $config_priority1
		if {[regexp $key $line]} {
			if {[regexp "^$key" $line]} {
				## change priority1 based on default value
				set line "${key}=$priority1_from_ui"
				printLog "$line"
			} else {
				if {[regexp $key $line ]} {
					continue; # skip line
				}
			}
		}

		##Change priority2 based on config
		set key $config_priority2
		if {[regexp $key $line]} {
			if {[regexp "^$key" $line]} {
				## change priority1 based on default value
				set line "${key}=$priority2_from_ui"
				printLog "$line"
			} else {
				if {[regexp $key $line ]} {
					continue; # skip line
				}
			}
		}

		##Change Vlan based on config
		if { [string match -nocase $wrptp_env(DUT_WRPTP_IS_VLAN_ENCAP) "True"] } {

			if {[string match "*CONFIG_VLANS_ENABLE*" $line]} {
				append new_config "#CONFIG_VLANS_ENABLE is not set\n"
				printLog "#CONFIG_VLANS_ENABLE is not set"
				continue
			}

			if {[string match "*CONFIG_VLANS_PORT$port\_MODE_TRUNK*" $line]} {
				append new_config "#CONFIG_VLANS_PORT$port\_MODE_TRUNK is not set\n"
				printLog "($dut_port_1) #CONFIG_VLANS_PORT$port\_MODE_TRUNK is not set"
				continue
			}

			if {[string match "*CONFIG_VLANS_PORT$port\_VID*" $line]} {
				append new_config "#CONFIG_VLANS_PORT$port\_VID is not set\n"
				printLog "($dut_port_1) #CONFIG_VLANS_PORT$port\_VID is not set"
				continue
			}
		}

		append new_config "$line\n"
	}

	saveDeployDotConfig $new_config
	
	set result [Send_cli_command\
		-session_handler $session_handler\
		-cmd_type        "DUT_GLOBAL_PTP_DISABLE"]

	return $result
}

#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_reset_communication_mode            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  ptp_version       : (optional)  PTP version.                                #
#                                  (Default: 2)                                #
#                                                                              #
#  communication_mode : (Mandatory) Communication mode                         #
#                                  (e.g., "Unicast"|"Multicast")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If communication mode is reset on the     #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If communication mode could not be        #
#                                    reset on the specified port at the DUT).  #
#                                                                              #
#  DEFINITION        : This function resets the communication mode on a        #
#                      specific port of the DUT.                               #
#                                                                              #
################################################################################

proc wrptp::callback_cli_reset_communication_mode {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set clock_mode          $param(-clock_mode)
	set communication_mode  $param(-communication_mode)
	set ptp_version         $param(-ptp_version)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_RESET_COMMUNICATION_MODE"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $clock_mode\
	#                       -parameter3             $ptp_version\
	#                       -parameter4             $communication_mode]} {
	#           return 0
	#       }

	return 2
}

#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_reset_domain                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  domain            : (Mandatory) Domain number.                              #
#                                  (e.g., 0..255)                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If domain number is reset on the DUT).    #
#                                                                              #
#                      0 on Failure (If domain number could not able to reset  #
#                                    on DUT).                                  #
#                                                                              #
#  DEFINITION        : This function resets domain number on the DUT.          #
#                                                                              #
################################################################################

proc wrptp::callback_cli_reset_domain {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set clock_mode          $param(-clock_mode)
	set domain              $param(-domain)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_RESET_DOMAIN"\
	#                       -parameter1             $clock_mode\
	#                       -parameter2             $domain]} {
	#           return 0
	#       }

	return 2
}

#8.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_reset_network_protocol              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) clock_mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  network_protocol  : (Mandatory) Network protocol.                           #
#                                  (e.g., "IEEE 802.3"|"UDP/IPv4")             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If network protocol is reset on specified #
#                                    port at the DUT).                         #
#                                                                              #
#                      0 on Failure (If network protocol could not be reset    #
#                                    on specified port at the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets the network protocol on a specific #
#                      port of the DUT.                                        #
#                                                                              #
################################################################################

proc wrptp::callback_cli_reset_network_protocol {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set clock_mode          $param(-clock_mode)
	set network_protocol    $param(-network_protocol)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_RESET_NETWORK_PROTOCOL"\
	#                       -parameter1             $clock_mode\
	#                       -parameter2             $network_protocol]} {
	#           return 0
	#       }

	return 2
}

#9.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_reset_clock_mode                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP clock mode is reset on specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If PTP clock mode could not be reset on   #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function resets clock mode on a specific port of   #
#                      DUT.                                                    #
#                                                                              #
################################################################################

proc wrptp::callback_cli_reset_clock_mode {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set clock_mode          $param(-clock_mode)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_RESET_CLOCK_MODE"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $clock_mode]} {
	#           return 0
	#       }

	return 2
}

#10.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_reset_clock_step                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  clock_step        : (Mandatory) Clock step.                                 #
#                                  (e.g., "One-step"|"Two-step")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If clock-step is reset on the specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If clock-step could not be able to reset  #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets the clock-step on a specific port  #
#                      on the DUT.                                             #
#                                                                              #
################################################################################

proc wrptp::callback_cli_reset_clock_step {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set clock_mode          $param(-clock_mode)
	set clock_step          $param(-clock_step)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_RESET_CLOCK_STEP"\
	#                       -parameter1             $port_num\
	#                       -parameter1             $clock_mode\
	#                       -parameter3             $clock_step]} {
	#           return 0
	#       }

	return 2
}

#11.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_reset_delay_mechanism               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  delay_mechanism   : (Mandatory) Delay mechanism.                            #
#                                  (e.g., e2e|p2p|disable)                     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If delay mechanism is reset on specified  #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If delay mechanism could not be reset on  #
#                                    specified PTP port of the DUT).           #
#                                                                              #
#  DEFINITION        : This function resets the delay mechanism to a specific  #
#                      port on the DUT.                                        #
#                                                                              #
################################################################################

proc wrptp::callback_cli_reset_delay_mechanism {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set clock_mode          $param(-clock_mode)
	set delay_mechanism     $param(-delay_mechanism)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_RESET_DELAY_MECHANISM"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $clock_mode\
	#                       -parameter3             $delay_mechanism]} {
	#           return 0
	#       }

	return 2
}

#12.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_reset_priority1                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority1         : (Mandatory) Prority1                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority1 is reset on the DUT).        #
#                                                                              #
#                      0 on Failure (If priority1 could not be able to reset on#
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function resets the priority1 on the DUT.          #
#                                                                              #
################################################################################

proc wrptp::callback_cli_reset_priority1 {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set clock_mode          $param(-clock_mode)
	set priority1           $param(-priority1)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_RESET_PRIORITY1"\
	#                       -parameter1             $clock_mode\
	#                       -parameter2             $priority1]} {
	#           return 0
	#       }

	return 2
}

#13.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_reset_priority2                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority2         : (Mandatory) Prority2                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority2 is reset on the DUT).        #
#                                                                              #
#                      0 on Failure (If priority2 could not be able to reset on#
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function resets the priority2 on the DUT.          #
#                                                                              #
################################################################################

proc wrptp::callback_cli_reset_priority2 {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set clock_mode          $param(-clock_mode)
	set priority2           $param(-priority2)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_RESET_PRIORITY1"\
	#                       -parameter1             $clock_mode\
	#                       -parameter2             $priority2]} {
	#           return 0
	#       }

	return 2
}

#14.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_reset_announce_interval             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  announce_interval  : (Mandatory) announceInterval.                          #
#                                  (e.g., 1|2|4|8|16)                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If announceInterval is reset on the DUT). #
#                                                                              #
#                      0 on Failure (If announceInterval could not be able to  #
#                                    reset on the DUT).                        #
#                                                                              #
#  DEFINITION        : This function resets the announceInterval on the DUT.   #
#                                                                              #
################################################################################

proc wrptp::callback_cli_reset_announce_interval {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set announce_interval   $param(-announce_interval)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_RESET_ANNOUNCE_INTERVAL"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $announce_interval]} {
	#           return 0
	#       }

	return 2
}

#15.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_reset_ipv4_address                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  ip_address        : (Mandatory) IPv4 address.                               #
#                                  (e.g., 192.168.200.1)                       #
#                                                                              #
#  net_mask          : (Mandatory) Subnet mask.                                #
#                                  (e.g., 255.255.255.0)                       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID.                                    #
#                                  (e.g., 100)                                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given IP address is reset on specified #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given IP address could not be reset on #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function resets given IP address to a specific port#
#                      on the DUT.                                             #
#                                                                              #
################################################################################

proc wrptp::callback_cli_reset_ipv4_address {args} {

	array set param $args

	set session_handler $param(-session_handler)
	set port_num        $param(-port_num)
	set ip_address      $param(-ip_address)
	set net_mask        $param(-net_mask)
	set vlan_id         $param(-vlan_id)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_RESET_IPV4_ADDRESS"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $ip_address\
	#                       -parameter3             $net_mask\
	#                       -parameter4             $vlan_id]} {
	#
	#           return 0
	#       }

	return 2
}

#16.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_reset_wr_config                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  value             : (Mandatory) Value for wrConfig.                         #
#                                  (e.g., "NON_WR"|"WR_S_ONLY"|"WR_M_ONLY"|    #
#                                         "WR_M_AND_S")                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value for wrConfig is reset on   #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given value for wrConfig could not be  #
#                                    reset on specifed port of the DUT).       #
#                                                                              #
#  DEFINITION        : This function resets given value for wrConfig to a      #
#                      specific port on the DUT.                               #
#                                                                              #
################################################################################

proc wrptp::callback_cli_reset_wr_config {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set value               $param(-value)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_RESET_WR_CONFIG"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $value]} {
	#           return 0
	#       }

	return 2
}

#17.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_reset_known_delta_tx                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  known_delta_tx    : (Mandatory) Transmission fixed delay.                   #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of transmission fixed delay#
#                                    is reset on specified port of the DUT).   #
#                                                                              #
#                      0 on Failure (If given value of transmission fixed delay#
#                                    could not be reset on specifed port of the#
#                                    DUT).                                     #
#  DEFINITION        : This function resets Transmission fixed delay to a      #
#                      specific port on the DUT.                               #
#                                                                              #
################################################################################

proc wrptp::callback_cli_reset_known_delta_tx {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set known_delta_tx      $param(-known_delta_tx)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_RESET_KNOWN_DELTA_TX"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $known_delta_tx]} {
	#           return 0
	#       }

	return 2
}

#18.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_reset_known_delta_rx                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  known_delta_rx    : (Mandatory) Reception fixed delay.                      #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of reception fixed delay   #
#                                    is reset on specified port of the DUT).   #
#                                                                              #
#                      0 on Failure (If given value of reception fixed delay   #
#                                    could not be reset on specifed port of the#
#                                    DUT).                                     #
#  DEFINITION        : This function resets reception fixed delay to a         #
#                      specific port on the DUT.                               #
#                                                                              #
################################################################################

proc wrptp::callback_cli_reset_known_delta_rx {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set known_delta_rx      $param(-known_delta_rx)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_RESET_KNOWN_DELTA_RX"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $known_delta_rx]} {
	#           return 0
	#       }

	return 2
}

#19.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_reset_deltas_known                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  deltas_known      : (Mandatory) Fixed delays.                               #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of fixed delays            #
#                                    is reset on specified port of the DUT).   #
#                                                                              #
#                      0 on Failure (If given value of fixed delays            #
#                                    could not be reset on specifed port of the#
#                                    DUT).                                     #
#  DEFINITION        : This function resets  fixed delays to a                 #
#                      specific port on the DUT.                               #
#                                                                              #
################################################################################

proc wrptp::callback_cli_reset_deltas_known {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_RESET_DELTAS_KNOWN"\
	#                       -parameter1             $port_num]} {
	#           return 0
	#       }

	return 2
}

#20.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_reset_cal_period                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  cal_period        : (Mandatory) calPeriod  (in microseconds)                #
#                                  (e.g., 3000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of calPeriod is reset on   #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given value of calPeriod could not be  #
#                                    reset on specifed port of the DUT).       #
#                                                                              #
#  DEFINITION        : This function resets calPeriod to a                     #
#                      specific port on the DUT.                               #
#                                                                              #
################################################################################

proc wrptp::callback_cli_reset_cal_period {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set cal_period          $param(-cal_period)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_RESET_CAL_PERIOD"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $cal_period]} {
	#           return 0
	#       }

	return 2
}

#21.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_reset_cal_retry                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  cal_retry         : (Mandatory) calRetry                                    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of calRetry                #
#                                    is reset on specified port of the DUT).   #
#                                                                              #
#                      0 on Failure (If given value of calRetry                #
#                                    could not be reset on specifed port of the#
#                                    DUT).                                     #
#  DEFINITION        : This function resets calRetry to a  specified port on   #
#                      the DUT.                                                #
#                                                                              #
################################################################################

proc wrptp::callback_cli_reset_cal_retry {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set cal_retry           $param(-cal_retry)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_RESET_CAL_RETRY"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $cal_retry]} {
	#           return 0
	#       }

	return 2
}

#22.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_port_wrptp_disable                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) Port on which wrptp is to be disabled.      #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If Wrptp is disabled on the specified port#
#                                    at the DUT).                              #
#                                                                              #
#                      0 on Failure (If Wrptp could not be disabled on the     #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function disables Wrptp on a specific port on the  #
#                      DUT.                                                    #
#                                                                              #
################################################################################

proc wrptp::callback_cli_port_wrptp_disable {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)

	set config ""
	set new_config ""
	if { ![ readDotConfig config ] } {
		return 0
	}

	#   puts "Performing configuration updation in dot-config."

	set portno [regexp {[0-9]+} $port_num port]
	if {[string length $port] == 1 } {
		set port "0$port"
	}

	set config_profile "CONFIG_PORT$port\_INST01_PROFILE_"

	foreach line [split $config "\n"] {
		# Set profile
		set key $config_profile
		if {[regexp "^$key" $line ]} {
			set line "${key}PTP=y"
		} else {
			if {[regexp $key $line ]} {
				continue; # skip this line
			}
		}

		append new_config "$line\n"
	}

	saveDeployDotConfig $new_config
	
	if {![Send_cli_command \
					-session_handler        $session_handler \
					-cmd_type               "DUT_PORT_WRPTP_DISABLE"\
					-parameter1             $port_num]} {
		return 0
	}

	return 1
}

#23.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_reset_wr_present_timeout            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_PRESENT_TIMEOUT (in milliseconds).       #
#                                  (e.g., 1000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of WR_PRESENT_TIMEOUT      #
#                                    is reset on specified port of the DUT).   #
#                                                                              #
#                      0 on Failure (If given value of WR_PRESENT_TIMEOUT      #
#                                    could not be reset on specifed port of the#
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function resets WR_PRESENT_TIMEOUT to a specific   #
#                      port on the DUT.                                        #
#                                                                              #
################################################################################

proc wrptp::callback_cli_reset_wr_present_timeout {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set timeout             $param(-timeout)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_RESET_WR_PRESENT_TIMEOUT"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $timeout]} {
	#           return 0
	#       }

	return 2
}

#24.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_reset_wr_m_lock_timeout             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_M_LOCK_TIMEOUT (in milliseconds).        #
#                                  (e.g., 15000)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of WR_M_LOCK_TIMEOUT       #
#                                    is reset on specified port of the DUT).   #
#                                                                              #
#                      0 on Failure (If given value of WR_M_LOCK_TIMEOUT       #
#                                    could not be reset on specifed port of the#
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function resets WR_M_LOCK_TIMEOUT to a specific    #
#                      port on the DUT.                                        #
#                                                                              #
################################################################################

proc wrptp::callback_cli_reset_wr_m_lock_timeout {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set timeout             $param(-timeout)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_RESET_WR_M_LOCK_TIMEOUT"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $timeout]} {
	#           return 0
	#       }

	return 2
}

#25.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_reset_wr_s_lock_timeout             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_S_LOCK_TIMEOUT (in milliseconds).        #
#                                  (e.g., 15000)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of WR_S_LOCK_TIMEOUT       #
#                                    is reset on specified port of the DUT).   #
#                                                                              #
#                      0 on Failure (If given value of WR_S_LOCK_TIMEOUT       #
#                                    could not be reset on specifed port of the#
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function resets WR_S_LOCK_TIMEOUT to a specific    #
#                      port on the DUT.                                        #
#                                                                              #
################################################################################

proc wrptp::callback_cli_reset_wr_s_lock_timeout {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set timeout             $param(-timeout)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_RESET_WR_S_LOCK_TIMEOUT"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $timeout]} {
	#           return 0
	#       }

	return 2
}

#26.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_reset_wr_locked_timeout             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_LOCKED_TIMEOUT (in milliseconds).        #
#                                  (e.g., 300)                                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of WR_LOCKED_TIMEOUT       #
#                                    is reset on specified port of the DUT).   #
#                                                                              #
#                      0 on Failure (If given value of WR_LOCKED_TIMEOUT       #
#                                    could not be reset on specifed port of the#
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function resets WR_LOCKED_TIMEOUT to a specific    #
#                      port on the DUT.                                        #
#                                                                              #
################################################################################

proc wrptp::callback_cli_reset_wr_locked_timeout {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set timeout             $param(-timeout)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_RESET_WR_LOCKED_TIMEOUT"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $timeout]} {
	#           return 0
	#       }

	return 2
}

#27.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_reset_wr_calibration_timeout        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_CALIBRATION_TIMEOUT (in milliseconds).   #
#                                  (e.g., 3000)                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of WR_CALIBRATION_TIMEOUT  #
#                                    is reset on specified port of the DUT).   #
#                                                                              #
#                      0 on Failure (If given value of WR_CALIBRATION_TIMEOUT  #
#                                    could not be reset on specifed port of the#
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function resets WR_CALIBRATION_TIMEOUT to a        #
#                      specific port on the DUT.                               #
#                                                                              #
################################################################################

proc wrptp::callback_cli_reset_wr_calibration_timeout {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set timeout             $param(-timeout)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_RESET_WR_CALIBRATION_TIMEOUT"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $timeout]} {
	#           return 0
	#       }

	return 2
}

#28.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_reset_wr_calibrated_timeout         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_CALIBRATED_TIMEOUT (in milliseconds).    #
#                                  (e.g., 300)                                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of WR_CALIBRATED_TIMEOUT   #
#                                    is reset on specified port of the DUT).   #
#                                                                              #
#                      0 on Failure (If given value of WR_CALIBRATED_TIMEOUT   #
#                                    could not be reset on specifed port of the#
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function resets WR_CALIBRATED_TIMEOUT to a specific#
#                      port on the DUT.                                        #
#                                                                              #
################################################################################

proc wrptp::callback_cli_reset_wr_calibrated_timeout {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set timeout             $param(-timeout)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_RESET_WR_CALIBRATED_TIMEOUT"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $timeout]} {
	#           return 0
	#       }

	return 2
}

#29.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_reset_wr_resp_calib_req_timeout     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  timeout           : (Mandatory) WR_RESP_CALIB_REQ_TIMEOUT (in milliseconds).#
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of                         #
#                                    WR_RESP_CALIB_REQ_TIMEOUT is reset on     #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given value of                         #
#                                    WR_RESP_CALIB_REQ_TIMEOUT could not be    #
#                                    reset on specifed port of the DUT).       #
#                                                                              #
#  DEFINITION        : This function resets WR_RESP_CALIB_REQ_TIMEOUT to a     #
#                      specific port on the DUT.                               #
#                                                                              #
################################################################################

proc wrptp::callback_cli_reset_wr_resp_calib_req_timeout {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set timeout             $param(-timeout)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_RESET_WR_RESP_CALIB_REQ_TIMEOUT"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $timeout]} {
	#           return 0
	#       }

	return 2
}

#30.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_reset_wr_state_retry                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  wr_state_retry    : (Mandatory) wrStateRetry                                #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of wrStateRetry is reset on#
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If given value of wrStateRetry could not  #
#                                    be reset on specifed port of the DUT).    #
#                                                                              #
#  DEFINITION        : This function resets  wrStateRetry to a                 #
#                      specific port on the DUT.                               #
#                                                                              #
################################################################################

proc wrptp::callback_cli_reset_wr_state_retry {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set wr_state_retry      $param(-wr_state_retry)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	#       if {![Send_cli_command \
	#                       -session_handler        $session_handler \
	#                       -cmd_type               "DUT_RESET_WR_STATE_RETRY"\
	#                       -parameter1             $port_num\
	#                       -parameter2             $wr_state_retry]} {
	#           return 0
	#       }

	return 2
}

#31.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_commit_changes                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success								               #
#                                                                              #
#                      0 on Failure                                            #
#                                                                              #
#  DEFINITION        : This function commits all dut changes in dot-config     #
#                                                                              #
################################################################################

proc wrptp::callback_cli_commit_changes {args} {

	array set param $args

	set session_handler     $param(-session_handler)

	# To set different values to the variables, set them above and uncomment
	# the following lines. Comment the last return statement.

	if {![Send_cli_command \
				 -session_handler        $session_handler \
				 -cmd_type               "DUT_COMMIT_CHANGES"]} {
		return 0
	}

	return 1
}

################################################################################
#                           DUT CHECK CALLBACK                                 #
################################################################################

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_check_ptp_port_state                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  state             : (Mandatory) PTP portState                               #
#                                  (e.g., "MASTER"|"SLAVE")                    #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP portState is verified in the       #
#                                    specified port).                          #
#                                                                              #
#                      0 on Failure (If value of PTP portState is not verified #
#                                    in the specified port).                   #
#                                                                              #
#  DEFINITION        : This function verifies value of PTP portState in a      #
#                      specific port.                                          #
#                                                                              #
################################################################################

proc wrptp::callback_cli_check_ptp_port_state {args} {

	array set param $args

	set session_handler     $param(-session_handler)
	set port_num            $param(-port_num)
	set state               $param(-state)
	set buffer              $param(-buffer)

	## portState ###
	array set PORTSTATE {
		INITIALIZING        "1"
		FAULTY              "2"
		DISABLED            "3"
		LISTENING           "4"
		PRE_MASTER          "5"
		MASTER              "6"
		PASSIVE             "7"
		UNCALIBRATED        "8"
		SLAVE               "9"
	}

	array set MAP {
		"1"                 "INITIALIZING"
		"2"                 "FAULTY"
		"3"                 "DISABLED"
		"4"                 "LISTENING"
		"5"                 "PRE_MASTER"
		"6"                 "MASTER"
		"7"                 "PASSIVE"
		"8"                 "UNCALIBRATED"
		"9"                 "SLAVE"
	}

	set state_expected   $PORTSTATE($state)

	foreach line [split $buffer "\n"] {

		if {[regexp {(.info.state:)\s+([a-zA-Z0-9 ]+)} $line match a state_retrieved]} {
		}

		if {[regexp {(cfg.iface_name:)\s+([a-zA-Z0-9\"]+)} $line match dump port_retrieved]} {

			set port_retrieved [string trim $port_retrieved "\""]

			if {$port_num == $port_retrieved} {

				puts "Port is matching (Exp->$port_num, Rcvd->$port_retrieved)"

				if {$state_expected == $state_retrieved} {
					puts "portState is matching (Exp->$MAP($state_expected)($state_expected), Rcvd->$MAP($state_retrieved)($state_retrieved))"
					return 1
				} else {
					puts "portState is not matching (Exp->$MAP($state_expected)($state_expected), Rcvd->$MAP($state_retrieved)($state_retrieved))"
					return 0
				}

			} else {
				puts "Port is not matching (Exp->$port_num, Rcvd->$port_retrieved)"
			}
		}

	}
	return 0
}

#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_check_wr_port_state                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  wr_port_state     : (Mandatory) wrPortState                                 #
#                                  (e.g., "IDLE"|"PRESENT"|"M_LOCK"|"S_LOCK"|  #
#                                         "LOCKED"|"CALIBRATION"|"CALIBRATED"| #
#                                         "RESP_CALIB_REQ"|"WR_LINK_ON")       #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If wrPortState is verified in the         #
#                                    specified port).                          #
#                                                                              #
#                      0 on Failure (If value of wrPortState is not verified in#
#                                    the specified port).                      #
#                                                                              #
#  DEFINITION        : This function verifies Value of wrPortState in a        #
#                      specific port.                                          #
#                                                                              #
################################################################################

proc  wrptp::callback_cli_check_wr_port_state {args} {

	array set param $args

	set buffer           $param(-buffer)
	set session_handler  $param(-session_handler)
	set port_num         $param(-port_num)
	set wr_port_state    $param(-wr_port_state)

	## WRPTP CONFIG ###
	array set WRPORTSTATE {
		IDLE                "0"
		PRESENT             "1"
		S_LOCK              "2"
		M_LOCK              "3"
		LOCKED              "4"
		CALIBRATION         "5"
		CALIBRATED          "6"
		RESP_CALIB_REQ      "7"
		WR_LINK_ON          "8"
	}

	array set MAP {
		"0"               "IDLE"
		"1"               "PRESENT"
		"2"               "S_LOCK"
		"3"               "M_LOCK"
		"4"               "LOCKED"
		"5"               "CALIBRATION"
		"6"               "CALIBRATED"
		"7"               "RESP_CALIB_REQ"
		"8"               "WR_LINK_ON"
	}

	set state_expected   $WRPORTSTATE($wr_port_state)
	set state_retrieved ""
	set port_retrieved  ""

	foreach line [split $buffer "\n"] {

		if {[regexp {(wr.state:)\s+([a-zA-Z0-9 ]+)} $line match a state_retrieved]} {
		}

		if {[regexp {(cfg.iface_name:)\s+([a-zA-Z0-9\"]+)} $line match dump port_retrieved]} {
		}

		## Check if we have retrieved both
		if { $state_retrieved!="" && $port_retrieved!="" } {
			set port_retrieved [string trimright [string trimleft $port_retrieved "\""] "\""]

			if {$port_num == $port_retrieved} {

				puts "Port is matching (Exp->$port_num, Rcvd->$port_retrieved)"

				if {$state_expected == $state_retrieved} {
					puts "WR portState is matching (Exp->$MAP($state_expected)($state_expected), Rcvd->$MAP($state_retrieved)($state_retrieved))"
					return 1
				} else {
					puts "WR portState is not matching (Exp->$MAP($state_expected)($state_expected), Rcvd->$MAP($state_retrieved)($state_retrieved))"
					return 0
				}

			} else {
				puts "Port is not matching (Exp->$port_num, Rcvd->$port_retrieved)"
			}
		}
	}
	return 0
}

#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_check_other_port_delta_tx           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  other_port_delta_tx    : (Mandatory) Value of deltaTx                       #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If value of deltaTx is verified in the    #
#                                    specified port).                          #
#                                                                              #
#                      0 on Failure (If value of deltaTx is not verified in    #
#                                    the specified port).                      #
#                                                                              #
#  DEFINITION        : This function verifies value of deltaTx in a            #
#                      specific port.                                          #
#                                                                              #
################################################################################

proc wrptp::callback_cli_check_other_port_delta_tx {args} {

	array set param $args

	set session_handler        $param(-session_handler)
	set port_num               $param(-port_num)
	set other_port_delta_tx    $param(-other_port_delta_tx)
	set buffer                 $param(-buffer)

	foreach line [split $buffer "\n"] {

		if {[regexp {(cfg.iface_name:)\s+([a-zA-Z0-9\"]+)} $line match dump port_retrieved]} {

			set port_retrieved [string trimright [string trimleft $port_retrieved "\""] "\""]
		}

		if {[regexp {(.wr.otherNodeDeltaTx:)\s+([0-9]+)} $line match a delta_tx_retrieved]} {

			if {$port_num == $port_retrieved} {

				puts "Port is matching (Exp->$port_num, Rcvd->$port_retrieved)"
				if {$other_port_delta_tx == $delta_tx_retrieved} {
					puts "otherPortDeltaTx is matching (Exp->$other_port_delta_tx, Rcvd->$delta_tx_retrieved)"
					return 1
				} else {
					puts "otherPortDeltaTx is not matching (Exp->$other_port_delta_tx, Rcvd->$delta_tx_retrieved)"
					return 0
				}
			} else {
				puts "Port is not matching (Exp->$port_num, Rcvd->$port_retrieved)"
			}
		}
	}
	return 0
}

#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_check_other_port_delta_rx           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  other_port_delta_rx    : (Mandatory) Value of deltaRx                       #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If value of deltaRx is verified in the    #
#                                    specified port).                          #
#                                                                              #
#                      0 on Failure (If value of deltaRx is not verified in    #
#                                    the specified port).                      #
#                                                                              #
#  DEFINITION        : This function verifies value of deltaRx in a            #
#                      specific port.                                          #
#                                                                              #
################################################################################

proc wrptp::callback_cli_check_other_port_delta_rx {args} {

	array set param $args

	set session_handler        $param(-session_handler)
	set port_num               $param(-port_num)
	set other_port_delta_rx    $param(-other_port_delta_rx)
	set buffer                 $param(-buffer)

	foreach line [split $buffer "\n"] {

		if {[regexp {(cfg.iface_name:)\s+([a-zA-Z0-9\"]+)} $line match dump port_retrieved]} {

			set port_retrieved [string trimright [string trimleft $port_retrieved "\""] "\""]
		}

		if {[regexp {(.wr.otherNodeDeltaRx:)\s+([0-9]+)} $line match a delta_rx_retrieved]} {

			if {$port_num == $port_retrieved} {

				puts "Port is matching (Exp->$port_num, Rcvd->$port_retrieved)"
				if {$other_port_delta_rx == $delta_rx_retrieved} {
					puts "otherPortDeltaRx is matching (Exp->$other_port_delta_rx, Rcvd->$delta_rx_retrieved)"
					return 1
				} else {callback_cli
					puts "otherPortDeltaRx is not matching (Exp->$other_port_delta_rx, Rcvd->$delta_rx_retrieved)"
					return 0
				}
			} else {
				puts "Port is not matching (Exp->$port_num, Rcvd->$port_retrieved)"
			}
		}
	}
	return 0
}

#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_check_other_port_cal_period         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  other_port_cal_period    : (Mandatory) value of calPeriod                   #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If value of calPeriod is verified in the  #
#                                    specified port).                          #
#                                                                              #
#                      0 on Failure (If value of calPeriod is not verified in  #
#                                    the specified port).                      #
#                                                                              #
#  DEFINITION        : This function verifies value of calperiod in a          #
#                      specific port.                                          #
#                                                                              #
################################################################################

proc wrptp::callback_cli_check_other_port_cal_period {args} {

	array set param $args

	set session_handler        $param(-session_handler)
	set port_num               $param(-port_num)
	set other_port_cal_period  $param(-other_port_cal_period)
	set buffer                 $param(-buffer)

	foreach line [split $buffer "\n"] {

		if {[regexp {(cfg.iface_name:)\s+([a-zA-Z0-9\"]+)} $line match dump port_retrieved]} {

			set port_retrieved [string trimright [string trimleft $port_retrieved "\""] "\""]
		}

		if {[regexp {(.wr.otherNodeCalPeriod:)\s+([0-9]+)} $line match a other_port_cal_period_retrieved]} {

			if {$port_num == $port_retrieved} {

				puts "Port is matching (Exp->$port_num, Rcvd->$port_retrieved)"
				if {$other_port_cal_period == $other_port_cal_period_retrieved} {
					puts "otherPortCalPeriod is matching (Exp->$other_port_cal_period, Rcvd->$other_port_cal_period_retrieved)"
					return 1
				} else {
					puts "otherPortCalPeriod is not matching (Exp->$other_port_cal_period, Rcvd->$other_port_cal_period_retrieved)"
					return 0
				}
			} else {
				puts "Port is not matching (Exp->$port_num, Rcvd->$port_retrieved)"
			}
		}
	}
	return 0
}

#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_check_other_port_cal_retry          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                  return                                                            #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  other_port_cal_retry    : (Mandatory) value of calRetry                     #
#                                  (e.g., 0)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If value of calRetry is verified in the   #
#                                    specified port).                          #
#                                                                              #
#                      0 on Failure (If value of calRetry is not verified in   #
#                                    the specified port).                      #
#                                                                              #
#  DEFINITION        : This function verifies value of calRetry in a           #
#                      specific port.                                          #
#                                                                              #
################################################################################

proc wrptp::callback_cli_check_other_port_cal_retry {args} {

	array set param $args

	set session_handler        $param(-session_handler)
	set port_num               $param(-port_num)
	set other_port_cal_retry   $param(-other_port_cal_retry)
	set buffer                 $param(-buffer)

	foreach line [split $buffer "\n"] {

		if {[regexp {(cfg.iface_name:)\s+([a-zA-Z0-9\"]+)} $line match dump port_retrieved]} {

			set port_retrieved [string trimright [string trimleft $port_retrieved "\""] "\""]
		}

		if {[regexp {(.wr.otherNodeCalRetry:)\s+([0-9]+)} $line match a other_port_cal_retry_retrieved]} {

			if {$port_num == $port_retrieved} {

				puts "Port is matching (Exp->$port_num, Rcvd->$port_retrieved)"
				if {$other_port_cal_retry == $other_port_cal_retry_retrieved} {
					puts "otherPortCalRetry is matching (Exp->$other_port_cal_retry, Rcvd->$other_port_cal_retry_retrieved)"
					return 1
				} else {
					puts "otherPortCalRetry is not matching (Exp->$other_port_cal_retry, Rcvd->$other_port_cal_retry_retrieved)"
					return 0
				}
			} else {
				puts "Port is not matching (Exp->$port_num, Rcvd->$port_retrieved)"
			}
		}
	}
	return 0
}

#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : wrptp::callback_cli_check_other_port_cal_send_pattern   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num                      : (Mandatory) DUT's port number.              #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  other_port_cal_send_pattern   : (Mandatory) value of calSendPattern         #
#                                  (e.g., TRUE|FALSE)                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If value of calSendPattern is verified in #
#                                    the specified port).                      #
#                                                                              #
#                      0 on Failure (If value of calSendPattern is not verified#
#                                    in the specified port).                   #
#                                                                              #
#  DEFINITION        : This function verifies value of calSendPattern in a     #
#                      specific port.                                          #
#                                                                              #
################################################################################

proc wrptp::callback_cli_check_other_port_cal_send_pattern {args} {

	array set param $args

	set session_handler               $param(-session_handler)
	set port_num                      $param(-port_num)
	set other_port_cal_send_pattern   $param(-other_port_cal_send_pattern)
	set buffer                        $param(-buffer)

	array set CSPBOOLEAN {
		0       "FALSE"
		1       "TRUE"
		2       "UNKNOWN"
		3       "UNKNOWN"
		4       "UNKNOWN"
		5       "UNKNOWN"
		6       "UNKNOWN"
		7       "UNKNOWN"
		8       "UNKNOWN"
		9       "UNKNOWN"
		10      "UNKNOWN"
	}

	foreach line [split $buffer "\n"] {

		if {[regexp {(cfg.iface_name:)\s+([a-zA-Z0-9\"]+)} $line match dump port_retrieved]} {

			set port_retrieved [string trimright [string trimleft $port_retrieved "\""] "\""]
		}

		if {[regexp {(.wr.otherNodeCalSendPattern:)\s+([0-9]+)} $line match a other_port_cal_send_pattern_retrieved]} {

			if {$port_num == $port_retrieved} {

				puts "Port is matching (Exp->$port_num, Rcvd->$port_retrieved)"
				if {$other_port_cal_send_pattern == [set CSPBOOLEAN($other_port_cal_send_pattern_retrieved)]} {
					puts "otherPortCalSendPattern is matching (Exp->$other_port_cal_send_pattern, Rcvd->[set CSPBOOLEAN($other_port_cal_send_pattern_retrieved)]($other_port_cal_send_pattern_retrieved))"
					return 1
				} else {
					puts "otherPortCalSendPattern is not matching (Exp->$other_port_cal_send_pattern, Rcvd->[set CSPBOOLEAN($other_port_cal_send_pattern_retrieved)]($other_port_cal_send_pattern_retrieved))"
					return 0
				}
			} else {
				puts "Port is not matching (Exp->$port_num, Rcvd->$port_retrieved)"
			}
		}
	}
	return 0
}
