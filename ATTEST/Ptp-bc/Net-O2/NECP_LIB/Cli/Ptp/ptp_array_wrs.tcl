#!/usr/bin/tcl
################################################################################
# File Name      : ptp_array_wrs.tcl                                           #
# File Version   : 1.2                                                         #
# Component Name : ATTEST CLI ARRAY MAPPING for WRS                            #
# Module Name    : PTP (Precision Time Protocol)                               #
#                                                                              #
################################################################################
#                                                                              #
#     SET ARRAY INDEX                                                          #
#                                                                              #
#  1. DUT_PTP_GLOBAL_PTP_ENABLE                                                #
#  2. DUT_PTP_PORT_ENABLE                                                      #
#  3. DUT_PTP_PORT_PTP_ENABLE                                                  #
#  4. DUT_PTP_SET_CLOCK_MODE                                                   #
#  5. DUT_PTP_SET_DOMAIN                                                       #
#  6. DUT_PTP_SET_PRIORITY1                                                    #
#  7. DUT_PTP_SET_PRIORITY2                                                    #
#  8. DUT_PTP_SET_ANNOUNCE_INTERVAL                                            #
#  9. DUT_PTP_SET_ANNOUNCE_TIMEOUT                                             #
# 10. DUT_PTP_SET_DELAY_MECHANISM                                              #
# 11. DUT_PTP_SET_CLOCK_STEP                                                   #
# 12. DUT_PTP_SET_NETWORK_PROTOCOL                                             #
# 13. DUT_PTP_SET_MESSAGE_TRANSPORT_MODE                                       #
# 14. DUT_PTP_SET_PDELAY_INTERVAL                                              #
# 15. DUT_PTP_SET_SYNC_INTERVAL                                                #
# 16. DUT_PTP_SET_VLAN                                                         #
# 17. DUT_PTP_SET_VLAN_PRIORITY                                                #
# 18. DUT_PTP_ENABLE_SLAVE_ONLY                                                #
# 19. DUT_PTP_SET_PTP_VERSION                                                  #
#                                                                              #
#  DUT DISABLE FUNCTIONS :                                                     #
#                                                                              #
#  1. DUT_PTP_GLOBAL_PTP_DISABLE                                               #
#  2. DUT_PTP_PORT_DISABLE                                                     #
#  3. DUT_PTP_PORT_PTP_DISABLE                                                 #
#  4. DUT_PTP_DISABLE_SLAVE_ONLY                                               #
#  5. DUT_PTP_DISABLE_VLANID                                                   #
#  6. DUT_DELETE_VLANID                                                        #
#  7. DUT_PTP_REMOVE_IP_INTERFACE                                              #
#                                                                              #
#  DUT CHECK FUNCTIONS :                                                       #
#                                                                              #
#  1. DUT_PTP_CHECK_PORT_STATE                                                 #
#  2. DUT_PTP_CHECK_DOMAIN_NUM                                                 #
#  3. DUT_PTP_CHECK_PRIORITY1_VALUE                                            #
#  4. DUT_PTP_CHECK_PRIORITY2_VALUE                                            #
#                                                                              #
################################################################################
# History     Date         Author         Addition/ Alteration                 #
#                                                                              #
#    1.0      Feb/2018     Veryx          Initial version.                     #
#    1.1      Mar/2018     Veryx          Updated to handle broken dot-config. #
#    1.2      Nov/2018     Veryx          Removed dependency on cli_array.tcl. #
#                                                                              #
################################################################################
# Copyright (C) 2017 - 2018 Veryx Technologies (P) Ltd.                        #
################################################################################

##########################################################################################
# Help on mapping CLI commands:                                                          #
#                                                                                        #
# ATTEST DUT functions are of three types - SET, GET, Callback                           #
#                                                                                        #
#  - SET (ATTEST_SET) function are used to set/configure the DUT                         #
#                                                                                        #
#  - GET (ATTEST_GET) function used to get/retrieve value(s) from the DUT                #
#                                                                                        #
#  - Callback function used for modifying the input parameters according to DUT for SET  #
#    and parsing the CLI show command output for GET function (DUT specific customization#
#    are done using this function). This function return following values 0/1/2          #
#    return 0 - Failure                                                                  #
#    return 1 - Success and validation is done by the function                           #
#   return $result                                                                       #
#    return 2 - Success and validation should be done by the test script                 #
#                                                                                        #
# Sub types - OUTPUT, WAIT, TIME                                                         #
#                                                                                        #
#  - WAIT (ATTEST_WAIT) function used to wait until it gets the given string.            #
#    e.g.: set cli_set_record(DUT_MVRP_SET_MVRP_REGISTRATION,ATTEST_SET,1)"your DUT CLI  #
#                                                                          command"      #
#          set cli_set_record(DUT_MVRP_SET_MVRP_REGISTRATION,ATTEST_WAIT,1) "config"     #
#                                                                                        #
#  - TIME (ATTEST_TIME) function used to customize the ATTEST timeout period when a CLI  #
#    command execution takes more than the default time (5 seconds). ATTEST_TIME waits   #
#    for the DUT prompt or timeout period to elapse (whichever occurs first) before      #
#    executing the next command or returning failure/success. ATTEST_TIME accepts time in#
#    seconds.                                                                            #
#    e.g.: set cli_set_record(DUT_MVRP_SET_MVRP_REGISTRATION,ATTEST_TIME,1) "10"         #
#    will increase the ATTEST's timeout period to 10 seconds.                            #
#                                                                                        #
#  - OUTPUT (ATTEST_OUTPUT) function used to capture the DUT CLI output to a buffer      #
#    This command is used in all GET functions.                                          #
#                                                                                        #
# Syntax:                                                                                #
# -------                                                                                #
#  Set command:                                                                          #
#  set cli_set_record(<COMMAND>,ATTEST_SET,<N>)  "your DUT CLI command"                  #
#  set cli_set_record(<COMMAND>,ATTEST_SET,<N>)  "NONE"                                  #
#                                                                                        #
#  Get command:                                                                          #
#  set cli_get_record(<COMMAND>,ATTEST_OUTPUT,<N>)  "your DUT CLI show command"          #
#  set cli_get_record(<COMMAND>,ATTEST_GET,<N>)     "NONE"                               #
#                                                                                        #
#  callback_protocol_set_xxx()                                                           #
#  callback_protocol_get_xxx()                                                           #
#                                                                                        #
#  Where N = 1,2,3 ..n                                                                   #
#  The order in which the commands are executed and hence must be sequential.            #
#                                                                                        #
#  Note:                                                                                 #
#    ATTEST_WAIT and ATTEST_TIME must use the command sequence number (N) of previous    #
#    ATTEST_SET/ATTEST_GET/ATTEST_OUTPUT.                                                #
#                                                                                        #
#    e.g.: set cli_set_record(DUT_MVRP_SET_MVRP_REGISTRATION,ATTEST_SET,1)"your DUT CLI  #
#                                                                          command"      #
#          set cli_set_record(DUT_MVRP_SET_MVRP_REGISTRATION,ATTEST_TIME,1) "10"         #
#    The command sequence number in the above two statements are 1.                      #
#                                                                                        #
#    GET function only executes the particular DUT CLI command and use ATTEST_OUTPUT to  #
#    capture the CLI output to buffer. The associated Callback function does the         #
#    comparison between value in the buffer and argument provided by the test script     #
#    when callback function is called.                                                   #
#                                                                                        #
#  General format(s):                                                                    #
#  Priority - integer value                                                              #
#  MAC Address - ':' seperated, Example 00:02:03:04:05:06                                #
#  Bridge Id - Priority/Mac_address                                                      #
#                                                                                        #
##########################################################################################


##########################################################################################
# This mapping file works for only for following template.                               #
# For Port Timing Configuration                                                          #
#                                                                                        #
# CONFIG_PORTXX_PARAMS="name=wri1,other options ..."                                     #
#                                                                                        #
# PTP options                                                                            #
#                                                                                        #
# CONFIG_PTP_OPT_DOMAIN_NUMBER="0"                                                       #
# CONFIG_PTP_OPT_ANNOUNCE_INTERVAL="1"                                                   #
# CONFIG_PTP_OPT_SYNC_INTERVAL="0"                                                       #
# CONFIG_PTP_OPT_PRIORITY1="128"                                                         #
# CONFIG_PTP_OPT_PRIORITY2="128"                                                         #
# For Vlan setting                                                                       #
# # VLANs                                                                                #
# CONFIG_VLANS_ENABLE=y                                                                  #
#                                                                                        #
#CONFIG_VLANS_PORT01_MODE_UNQUALIFIED=y                                                  #
#CONFIG_VLANS_PORT01_PRIO=x                                                              #
#CONFIG_VLANS_PORT01_VID=""                                                              #
##########################################################################################

package require Expect

global host username password

set host        $env(TEST_DEVICE_IPV4_ADDRESS)
set username    $env(DUT_SSH_USER_NAME)
set password    $env(DUT_SSH_PASSWORD)

global ATTEST_CORE tc_name case_id group_name

set PktFileName $ATTEST_CORE(-pktFileName)
set log_instance [lindex [split $PktFileName "."] 0]
set tc_name_list [split $log_instance "_"]
set group_name  [lindex $tc_name_list 3]
set case_id [lindex $tc_name_list 4]

global dut_error_msg

set dut_error_msg "Available commands*|Next possible*|Invalid*|constraint*| is required*|MAC Address must be unicast*|Incorrect input*|Command not found*|overlaps*"

set dut_config_delay 0


proc  tee_set_dut_ptp_cli_from_file {} {

global cli_set_record
global ptp_env
global case_id group_name


#1.
################################################################################
#                                                                              #
#  COMMAND           : GLOBAL_PTP_ENABLE                                       #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_GLOBAL_PTP_ENABLE                               #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  DEFINITION        : This function enables PTP in DUT                        #
#                                                                              #
#                      The last command should be "NONE".                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE                                                    #
#                                                                              #
################################################################################

    set cli_set_record(DUT_PTP_GLOBAL_PTP_ENABLE,ATTEST_SET,1)          "/wr/bin/apply_dot-config local_config"
    set cli_set_record(DUT_PTP_GLOBAL_PTP_ENABLE,ATTEST_TIME,1)         "30"
    set cli_set_record(DUT_PTP_GLOBAL_PTP_ENABLE,ATTEST_SET,2)          "/etc/init.d/hald.sh restart"
    set cli_set_record(DUT_PTP_GLOBAL_PTP_ENABLE,ATTEST_TIME,2)         "30"
    set cli_set_record(DUT_PTP_GLOBAL_PTP_ENABLE,ATTEST_SET,3)          "sleep 1"
    set cli_set_record(DUT_PTP_GLOBAL_PTP_ENABLE,ATTEST_TIME,3)         "30"
    set cli_set_record(DUT_PTP_GLOBAL_PTP_ENABLE,ATTEST_SET,4)          "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_PTP_GLOBAL_PTP_ENABLE,ATTEST_TIME,4)         "30"
    set cli_set_record(DUT_PTP_GLOBAL_PTP_ENABLE,ATTEST_SET,5)          "NONE"


#2.
################################################################################
#                                                                              #
#  COMMAND           : PORT_ENABLE                                             #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_PORT_ENABLE                                     #
#                                                                              #
#  INPUT PARAMETERS                                                            #
#                                                                              #
#  ATTEST_PARAM_1    = port_num                                                #
#                     (e.g., fa0/0, 1/1 etc ).                                 #
#                                                                              #
#  DEFINITION        : This function enables the ports on DUT.                 #
#                                                                              #
#                      The last command should be "NONE".                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE                                                    #
#                                                                              #
################################################################################

    set cli_set_record(DUT_PTP_PORT_ENABLE,ATTEST_SET,1)                "ifconfig ATTEST_PARAM1 up"
    set cli_set_record(DUT_PTP_PORT_ENABLE,ATTEST_SET,2)                "NONE"


#3.
################################################################################
#                                                                              #
#  COMMAND           : PTP_PORT_PTP_ENABLE                                     #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_PORT_PTP_ENABLE                                 #
#                                                                              #
#  INPUT PARAMETERS                                                            #
#                                                                              #
#  ATTEST_PARAM_1    = port_num                                                #
#                     (e.g., fa0/0, 1/1 etc ).                                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  DEFINITION        : This function enables PTP on the specified list of      #
#                      ports at the DUT.                                       #
#                                                                              #
#                      The last command should be "NONE".                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE                                                    #
#                                                                              #
################################################################################

    set cli_set_record(DUT_PTP_PORT_PTP_ENABLE,ATTEST_SET,1)            "/wr/bin/apply_dot-config local_config"
    set cli_set_record(DUT_PTP_PORT_PTP_ENABLE,ATTEST_TIME,1)           "30"
    set cli_set_record(DUT_PTP_PORT_PTP_ENABLE,ATTEST_SET,2)            "/etc/init.d/hald.sh restart"
    set cli_set_record(DUT_PTP_PORT_PTP_ENABLE,ATTEST_TIME,2)           "30"
    set cli_set_record(DUT_PTP_PORT_PTP_ENABLE,ATTEST_SET,3)            "sleep 1"
    set cli_set_record(DUT_PTP_PORT_PTP_ENABLE,ATTEST_TIME,3)           "30"
    set cli_set_record(DUT_PTP_PORT_PTP_ENABLE,ATTEST_SET,4)            "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_PTP_PORT_PTP_ENABLE,ATTEST_TIME,4)           "30"
    set cli_set_record(DUT_PTP_PORT_PTP_ENABLE,ATTEST_SET,5)            "NONE"


#4.
################################################################################
#                                                                              #
#  COMMAND           : PTP_SET_CLOCK_MODE                                      #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_SET_CLOCK_MODE                                  #
#                                                                              #
#  INPUT PARAMETERS                                                            #
#                                                                              #
#  ATTEST_PARAM_1    = Clock mode                                              #
#                      (e.g. Boundary Clock, Ordinary Clock )                  #
#                                                                              #
#  DEFINITION        : This function enables the PTP clock mode                #
#                                                                              #
#                      The last command should be "NONE".                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_PTP_SET_CLOCK_MODE,ATTEST_SET,1)             "NOOP"
    set cli_set_record(DUT_PTP_SET_CLOCK_MODE,ATTEST_SET,2)             "NONE"


#5.
################################################################################
#                                                                              #
#  COMMAND           : PTP_SET_DOMAIN                                          #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_SET_DOMAIN                                      #
#                                                                              #
#  INPUT PARAMETERS                                                            #
#                                                                              #
#  ATTEST_PARAM_1    = Clock mode                                              #
#                      (e.g. Boundary Clock, Ordinary Clock )                  #
#                                                                              #
#  ATTEST_PARAM_2    = domain                                                  #
#                      (e.g., 0..255 ).                                        #
#                                                                              #
#  DEFINITION        : This function sets domain number on the specific clock  #
#                      mode of the DUT,                                        #
#                                                                              #
#                      The last command should be "NONE".                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_PTP_SET_DOMAIN,ATTEST_SET,1)                 "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_PTP_SET_DOMAIN,ATTEST_TIME,1)                "10"
    set cli_set_record(DUT_PTP_SET_DOMAIN,ATTEST_SET,2)                 "/wr/bin/apply_dot-config local_config"
    set cli_set_record(DUT_PTP_SET_DOMAIN,ATTEST_TIME,2)                "30"
    set cli_set_record(DUT_PTP_SET_DOMAIN,ATTEST_SET,3)                 "/etc/init.d/hald.sh restart"
    set cli_set_record(DUT_PTP_SET_DOMAIN,ATTEST_TIME,3)                "30"
    set cli_set_record(DUT_PTP_SET_DOMAIN,ATTEST_SET,4)                 "sleep 1"
    set cli_set_record(DUT_PTP_SET_DOMAIN,ATTEST_TIME,4)                "30"
    set cli_set_record(DUT_PTP_SET_DOMAIN,ATTEST_SET,5)                 "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_PTP_SET_DOMAIN,ATTEST_TIME,5)                "30"
    set cli_set_record(DUT_PTP_SET_DOMAIN,ATTEST_SET,6)                 "sleep 4"
    set cli_set_record(DUT_PTP_SET_DOMAIN,ATTEST_SET,7)                 "NONE"


#6.
################################################################################
#                                                                              #
#  COMMAND           : PTP_SET_PRIORITY1                                       #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_SET_PRIORITY1                                   #
#                                                                              #
#  INPUT PARAMETERS                                                            #
#                                                                              #
#  ATTEST_PARAM_1    = Clock mode                                              #
#                      (e.g. Boundary Clock, Ordinary Clock )                  #
#                                                                              #
#  ATTEST_PARAM_2    = priority1                                               #
#                      (e.g., 0..255 ).                                        #
#                                                                              #
#  DEFINITION        : This function sets the priority1 value for specific     #
#                      clock mode on the DUT.                                  #
#                                                                              #
#                      The last command should be "NONE".                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_PTP_SET_PRIORITY1,ATTEST_SET,1)              "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_PTP_SET_PRIORITY1,ATTEST_TIME,1)             "10"
    set cli_set_record(DUT_PTP_SET_PRIORITY1,ATTEST_SET,2)              "/wr/bin/apply_dot-config local_config"
    set cli_set_record(DUT_PTP_SET_PRIORITY1,ATTEST_TIME,2)             "30"
    set cli_set_record(DUT_PTP_SET_PRIORITY1,ATTEST_SET,3)              "/etc/init.d/hald.sh restart"
    set cli_set_record(DUT_PTP_SET_PRIORITY1,ATTEST_TIME,3)             "30"
    set cli_set_record(DUT_PTP_SET_PRIORITY1,ATTEST_SET,4)              "sleep 1"
    set cli_set_record(DUT_PTP_SET_PRIORITY1,ATTEST_TIME,4)             "30"
    set cli_set_record(DUT_PTP_SET_PRIORITY1,ATTEST_SET,5)              "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_PTP_SET_PRIORITY1,ATTEST_TIME,5)             "30"
    set cli_set_record(DUT_PTP_SET_PRIORITY1,ATTEST_SET,6)              "sleep 4"
    set cli_set_record(DUT_PTP_SET_PRIORITY1,ATTEST_SET,7)              "NONE"


#7.
################################################################################
#                                                                              #
#  COMMAND           : PTP_SET_PRIORITY2                                       #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_SET_PRIORITY2                                   #
#                                                                              #
#  INPUT PARAMETERS                                                            #
#                                                                              #
#  ATTEST_PARAM_1    = Clock mode                                              #
#                      (e.g. Boundary Clock, Ordinary Clock )                  #
#                                                                              #
#  ATTEST_PARAM_2    = priority2                                               #
#                      (e.g., 0..255 ).                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  DEFINITION        : This function set the PTP priority2 value on the        #
#                      specific clock mode on the DUT.                         #
#                                                                              #
#                      The last command should be "NONE".                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_PTP_SET_PRIORITY2,ATTEST_SET,1)              "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_PTP_SET_PRIORITY2,ATTEST_TIME,1)             "10"
    set cli_set_record(DUT_PTP_SET_PRIORITY2,ATTEST_SET,2)              "/wr/bin/apply_dot-config local_config"
    set cli_set_record(DUT_PTP_SET_PRIORITY2,ATTEST_TIME,2)             "30"
    set cli_set_record(DUT_PTP_SET_PRIORITY2,ATTEST_SET,3)              "/etc/init.d/hald.sh restart"
    set cli_set_record(DUT_PTP_SET_PRIORITY2,ATTEST_TIME,3)             "30"
    set cli_set_record(DUT_PTP_SET_PRIORITY2,ATTEST_SET,4)              "sleep 1"
    set cli_set_record(DUT_PTP_SET_PRIORITY2,ATTEST_TIME,4)             "30"
    set cli_set_record(DUT_PTP_SET_PRIORITY2,ATTEST_SET,5)              "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_PTP_SET_PRIORITY2,ATTEST_TIME,5)             "30"
    set cli_set_record(DUT_PTP_SET_PRIORITY2,ATTEST_SET,6)              "sleep 4"
    set cli_set_record(DUT_PTP_SET_PRIORITY2,ATTEST_SET,7)              "NONE"


#8.
################################################################################
#                                                                              #
#  COMMAND           : SET_ANNONCE_INTERVAL                                    #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_SET_ANNOUNCE_INTERVAL                           #
#                                                                              #
#  INPUT PARAMETERS                                                            #
#                                                                              #
#  ATTEST_PARAM_1    = port num                                                #
#                     (e.g., fa0/0, 1/1 etc ).                                 #
#                                                                              #
#  ATTEST_PARAM_2    = clock mode                                              #
#                      (e.g. Boundary Clock, Ordinary Clock )                  #
#                                                                              #
#  ATTEST_PARAM_3    = announce interval                                       #
#                     (e.g. 0,1,2,3,4 ).                                       #
#                                                                              #
#  DEFINITION        : This function set the PTP announce interval value to the#
#                      specific PTP port on the DUT.                           #
#                                                                              #
#                      The last command should be "NONE".                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_PTP_SET_ANNOUNCE_INTERVAL,ATTEST_SET,1)      "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_PTP_SET_ANNOUNCE_INTERVAL,ATTEST_TIME,1)     "10"
    set cli_set_record(DUT_PTP_SET_ANNOUNCE_INTERVAL,ATTEST_SET,2)      "/wr/bin/apply_dot-config local_config"
    set cli_set_record(DUT_PTP_SET_ANNOUNCE_INTERVAL,ATTEST_TIME,2)     "30"
    set cli_set_record(DUT_PTP_SET_ANNOUNCE_INTERVAL,ATTEST_SET,3)      "/etc/init.d/hald.sh restart"
    set cli_set_record(DUT_PTP_SET_ANNOUNCE_INTERVAL,ATTEST_TIME,3)     "30"
    set cli_set_record(DUT_PTP_SET_ANNOUNCE_INTERVAL,ATTEST_SET,4)      "sleep 1"
    set cli_set_record(DUT_PTP_SET_ANNOUNCE_INTERVAL,ATTEST_TIME,4)     "30"
    set cli_set_record(DUT_PTP_SET_ANNOUNCE_INTERVAL,ATTEST_SET,5)      "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_PTP_SET_ANNOUNCE_INTERVAL,ATTEST_TIME,5)     "30"
    set cli_set_record(DUT_PTP_SET_ANNOUNCE_INTERVAL,ATTEST_SET,6)      "sleep 4"
    set cli_set_record(DUT_PTP_SET_ANNOUNCE_INTERVAL,ATTEST_SET,7)      "NONE"


#9.
################################################################################
#                                                                              #
#  COMMAND           : SET_ANNOUNCE_TIMEOUT                                    #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_SET_ANNOUNCE_TIMEOUT                            #
#                                                                              #
#  INPUT PARAMETERS                                                            #
#                                                                              #
#  ATTEST_PARAM_1    = port num                                                #
#                     (e.g., fa0/0, 1/1 etc ).                                 #
#                                                                              #
#  ATTEST_PARAM_2    = clock mode                                              #
#                      (e.g. Boundary Clock, Ordinary Clock )                  #
#                                                                              #
#  ATTEST_PARAM_3    = announce timeout                                        #
#                      (e.g.,3 )                                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  DEFINITION        : This function set the PTP announce timeout value to the #
#                      specific PTP port on the DUT.                           #
#                                                                              #
#                      The last command should be "NONE".                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_PTP_SET_ANNOUNCE_TIMEOUT,ATTEST_SET,1)       "NOOP"
    set cli_set_record(DUT_PTP_SET_ANNOUNCE_TIMEOUT,ATTEST_SET,2)       "NONE"


#10.
################################################################################
#                                                                              #
#  COMMAND           : SET_DELAY_MECHANISM                                     #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_SET_DELAY_MECHANISM                             #
#                                                                              #
#  INPUT PARAMETERS                                                            #
#                                                                              #
#  ATTEST_PARAM_1    = port num                                                #
#                     (e.g., fa0/0, 1/1 etc ).                                 #
#                                                                              #
#  ATTEST_PARAM_2    = clock mode                                              #
#                      (e.g. Boundary Clock, Ordinary Clock )                  #
#                                                                              #
#  ATTEST_PARAM_3    = delay mechanism                                         #
#                      (e.g., e2e|p2p|disable  )                               #
#                                                                              #
#  DEFINITION        : This function set the Delay mechanism value to the      #
#                      specific PTP port on the DUT.                           #
#                                                                              #
#                      The last command should be "NONE".                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_PTP_SET_DELAY_MECHANISM,ATTEST_SET,1)        "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_PTP_SET_DELAY_MECHANISM,ATTEST_TIME,1)       "10"
    set cli_set_record(DUT_PTP_SET_DELAY_MECHANISM,ATTEST_SET,2)        "/wr/bin/apply_dot-config local_config"
    set cli_set_record(DUT_PTP_SET_DELAY_MECHANISM,ATTEST_TIME,2)       "30"
    set cli_set_record(DUT_PTP_SET_DELAY_MECHANISM,ATTEST_SET,3)        "/etc/init.d/hald.sh restart"
    set cli_set_record(DUT_PTP_SET_DELAY_MECHANISM,ATTEST_TIME,3)       "30"
    set cli_set_record(DUT_PTP_SET_DELAY_MECHANISM,ATTEST_SET,4)        "sleep 1"
    set cli_set_record(DUT_PTP_SET_DELAY_MECHANISM,ATTEST_TIME,4)       "30"
    set cli_set_record(DUT_PTP_SET_DELAY_MECHANISM,ATTEST_SET,5)        "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_PTP_SET_DELAY_MECHANISM,ATTEST_TIME,5)       "30"
    set cli_set_record(DUT_PTP_SET_DELAY_MECHANISM,ATTEST_SET,6)        "NONE"


#11.
################################################################################
#                                                                              #
#  COMMAND           : SET_CLOCK_STEP                                          #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_SET_CLOCK_STEP                                  #
#                                                                              #
#  INPUT PARAMETERS                                                            #
#                                                                              #
#  ATTEST_PARAM_1    = port num                                                #
#                     (e.g., fa0/0, 1/1 etc ).                                 #
#                                                                              #
#  ATTEST_PARAM_2    = clock mode                                              #
#                      (e.g. Boundary Clock, Ordinary Clock )                  #
#                                                                              #
#  ATTEST_PARAM_3    = clock step                                              #
#                      (e.g., one-step | two-step )                            #
#                                                                              #
#  DEFINITION        : This function set the clock-set to the specific PTP port#
#                      on the DUT.                                             #
#                                                                              #
#                      The last command should be "NONE".                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_PTP_SET_CLOCK_STEP,ATTEST_SET,1)             "NOOP"
    set cli_set_record(DUT_PTP_SET_CLOCK_STEP,ATTEST_SET,2)             "NONE"


#12.
################################################################################
#                                                                              #
#  COMMAND           : SET_NETWORK_PROTOCOL                                    #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_SET_NETWORK_PROTOCOL                            #
#                                                                              #
#  INPUT PARAMETERS                                                            #
#                                                                              #
#  ATTEST_PARAM_1    = port num                                                #
#                     (e.g., fa0/0, 1/1 etc ).                                 #
#                                                                              #
#  ATTEST_PARAM_2    = clock mode                                              #
#                      (e.g. Boundary Clock, Ordinary Clock )                  #
#                                                                              #
#  ATTEST_PARAM_3    = network_protocol                                        #
#                     (e.g., ieee802_3 | udp ipv4 )                            #
#                                                                              #
#  ATTEST_PARAM_4    = IP adddress                                             #
#                     (e.g., 192.168.7.5 )                                     #
#                                                                              #
#  DEFINITION        : This function set the network protocol to the specific  #
#                      PTP port on the DUT.                                    #
#                                                                              #
#                      The last command should be "NONE".                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_PTP_SET_NETWORK_PROTOCOL,ATTEST_SET,1)       "NOOP"
    set cli_set_record(DUT_PTP_SET_NETWORK_PROTOCOL,ATTEST_SET,2)       "NONE"


#13.
################################################################################
#                                                                              #
#  COMMAND           : SET_MESSAGE_TRANSPORT_MODE                              #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_SET_MESSAGE_TRANSPORT_MODE                      #
#                                                                              #
#  INPUT PARAMETERS                                                            #
#                                                                              #
#  ATTEST_PARAM_1    = port num                                                #
#                     (e.g., fa0/0, 1/1 etc ).                                 #
#                                                                              #
#  ATTEST_PARAM_2    = clock mode                                              #
#                      (e.g. Boundary Clock, Ordinary Clock )                  #
#                                                                              #
#  ATTEST_PARAM_3    = ptp version                                             #
#                      (e.g. 2 )                                               #
#                                                                              #
#  ATTEST_PARAM_4    = message_transport_mode                                  #
#                     (e.g., unicast | multicast )                             #
#                                                                              #
#  DEFINITION        : This function set the message transport mode to the     #
#                       specific PTP port on the DUT.                          #
#                                                                              #
#                      The last command should be "NONE".                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_PTP_SET_MESSAGE_TRANSPORT_MODE,ATTEST_SET,1) "NOOP"
    set cli_set_record(DUT_PTP_SET_MESSAGE_TRANSPORT_MODE,ATTEST_SET,2) "NONE"


#14.
################################################################################
#                                                                              #
#  COMMAND           : SET_PDELAY_INTERVAL                                     #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_SET_PDELAY_INTERVAL                             #
#                                                                              #
#  INPUT PARAMETERS                                                            #
#                                                                              #
#  ATTEST_PARAM_1    = port num                                                #
#                     (e.g., fa0/0, 1/1 etc ).                                 #
#                                                                              #
#  ATTEST_PARAM_2    = clock mode                                              #
#                      (e.g. Boundary Clock, Ordinary Clock )                  #
#                                                                              #
#  ATTEST_PARAM_3    = pdelay interval                                         #
#                                                                              #
#  DEFINITION        : This function sets the pdelay interval to the specific  #
#                      PTP port on the DUT.                                    #
#                                                                              #
#                      The last command should be "NONE".                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_PTP_SET_PDELAY_INTERVAL,ATTEST_SET,1)        "NOOP"
    set cli_set_record(DUT_PTP_SET_PDELAY_INTERVAL,ATTEST_SET,2)        "NONE"


#15.
################################################################################
#                                                                              #
#  COMMAND           : PTP_SET_SYNC_INTERVAL                                   #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_SET_SYNC_INTERVAL                               #
#                                                                              #
#  INPUT PARAMETERS                                                            #
#                                                                              #
#  ATTEST_PARAM_1    = port num                                                #
#                     (e.g., fa0/0, 1/1 etc ).                                 #
#                                                                              #
#  ATTEST_PARAM_2    = clock mode                                              #
#                      (e.g. Boundary Clock, Ordinary Clock )                  #
#                                                                              #
#  ATTEST_PARAM_3    = sync_interval                                           #
#                                                                              #
#  DEFINITION        : This function sets the sync interval to the specific    #
#                      PTP port on DUT.                                        #
#                      The last command should be "NONE".                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_PTP_SET_SYNC_INTERVAL,ATTEST_SET,1)          "/wr/bin/apply_dot-config local_config"
    set cli_set_record(DUT_PTP_SET_SYNC_INTERVAL,ATTEST_TIME,1)         "30"
    set cli_set_record(DUT_PTP_SET_SYNC_INTERVAL,ATTEST_SET,2)          "/etc/init.d/hald.sh restart"
    set cli_set_record(DUT_PTP_SET_SYNC_INTERVAL,ATTEST_TIME,2)         "30"
    set cli_set_record(DUT_PTP_SET_SYNC_INTERVAL,ATTEST_SET,3)          "sleep 1"
    set cli_set_record(DUT_PTP_SET_SYNC_INTERVAL,ATTEST_TIME,3)         "30"
    set cli_set_record(DUT_PTP_SET_SYNC_INTERVAL,ATTEST_SET,4)          "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_PTP_SET_SYNC_INTERVAL,ATTEST_TIME,4)         "30"
    set cli_set_record(DUT_PTP_SET_SYNC_INTERVAL,ATTEST_SET,5)          "sleep 10"
    set cli_set_record(DUT_PTP_SET_SYNC_INTERVAL,ATTEST_SET,6)          "NONE"


#16.
################################################################################
#                                                                              #
#  COMMAND           : PTP_SET_VLAN                                            #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_SET_VLAN                                        #
#                                                                              #
#  INPUT PARAMETERS                                                            #
#                                                                              #
#  ATTEST_PARAM_1    = port num                                                #
#                     (e.g., fa0/0, 1/1 etc ).                                 #
#                                                                              #
#  ATTEST_PARAM_2    = clock mode                                              #
#                      (e.g. Boundary Clock, Ordinary Clock )                  #
#                                                                              #
#  ATTEST_PARAM_3    = vlan_id                                                 #
#                                                                              #
#  DEFINITION        : This function sets the vlan id to the specific          #
#                      PTP port on the DUT.                                    #
#                                                                              #
#                      The last command should be "NONE".                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_PTP_SET_VLAN,ATTEST_SET,1)                   "/wr/bin/apply_dot-config local_config"
    set cli_set_record(DUT_PTP_SET_VLAN,ATTEST_TIME,1)                  "30"
    set cli_set_record(DUT_PTP_SET_VLAN,ATTEST_SET,2)                   "/etc/init.d/hald.sh restart"
    set cli_set_record(DUT_PTP_SET_VLAN,ATTEST_TIME,2)                  "120"
    set cli_set_record(DUT_PTP_SET_VLAN,ATTEST_SET,3)                   "sleep 1"
    set cli_set_record(DUT_PTP_SET_VLAN,ATTEST_TIME,3)                  "120"
    set cli_set_record(DUT_PTP_SET_VLAN,ATTEST_SET,4)                   "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_PTP_SET_VLAN,ATTEST_TIME,4)                  "120"
    set cli_set_record(DUT_PTP_SET_VLAN,ATTEST_SET,5)                   "NONE"


#17.
################################################################################
#                                                                              #
#  COMMAND           : SET_VLAN_PRIORITY                                       #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_SET_VLAN_PRIORITY                               #
#                                                                              #
#  INPUT PARAMETERS                                                            #
#                                                                              #
#  ATTEST_PARAM_1    = port num                                                #
#                     (e.g., fa0/0, 1/1 etc ).                                 #
#                                                                              #
#  ATTEST_PARAM_2    = clock mode                                              #
#                      (e.g. Boundary Clock, Ordinary Clock )                  #
#                                                                              #
#  ATTEST_PARAM_3    = vlan_priority                                           #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  DEFINITION        : This function sets the vlan priority to the specific    #
#                      PTP port on the DUT.                                    #
#                                                                              #
#                      The last command should be "NONE".                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#                                                                              #
################################################################################

    set cli_set_record(DUT_PTP_SET_VLAN_PRIORITY,ATTEST_SET,1)          "NOOP"
    set cli_set_record(DUT_PTP_SET_VLAN_PRIORITY,ATTEST_SET,2)          "NONE"

#18.
################################################################################
#                                                                              #
#  COMMAND           : ENABLE_SLAVE_ONLY                                       #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_ENABLE_SLAVE_ONLY                               #
#                                                                              #
#  INPUT PARAMETERS                                                            #
#                                                                              #
#  ATTEST_PARAM_1    = port num                                                #
#                     (e.g., fa0/0, 1/1 etc ).                                 #
#                                                                              #
#  ATTEST_PARAM_2    = clock mode                                              #
#                      (e.g. Boundary Clock, Ordinary Clock )                  #
#                                                                              #
#  ATTEST_PARAM_3    = slave_only                                              #
#                                                                              #
#  DEFINITION        : This function enable slave only mode to the specific    #
#                      PTP port on the DUT.                                    #
#                                                                              #
#                      The last command should be "NONE".                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#  NOTE              : This mapping is specific to ordinary clock              #
################################################################################

    set cli_set_record(DUT_PTP_ENABLE_SLAVE_ONLY,ATTEST_SET,1)          "/wr/bin/apply_dot-config local_config"
    set cli_set_record(DUT_PTP_ENABLE_SLAVE_ONLY,ATTEST_TIME,1)         "30"
    set cli_set_record(DUT_PTP_ENABLE_SLAVE_ONLY,ATTEST_SET,2)          "/etc/init.d/hald.sh restart"
    set cli_set_record(DUT_PTP_ENABLE_SLAVE_ONLY,ATTEST_TIME,2)         "120"
    set cli_set_record(DUT_PTP_ENABLE_SLAVE_ONLY,ATTEST_SET,3)          "sleep 1"
    set cli_set_record(DUT_PTP_ENABLE_SLAVE_ONLY,ATTEST_TIME,3)         "120"
    set cli_set_record(DUT_PTP_ENABLE_SLAVE_ONLY,ATTEST_SET,4)          "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_PTP_ENABLE_SLAVE_ONLY,ATTEST_TIME,4)         "120"
    set cli_set_record(DUT_PTP_ENABLE_SLAVE_ONLY,ATTEST_SET,5)          "NONE"


#19.
################################################################################
#                                                                              #
#  COMMAND           : SET PTP VERSION                                         #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_SET_PTP_VERSION                                 #
#                                                                              #
#  INPUT PARAMETERS                                                            #
#                                                                              #
#  ATTEST_PARAM_1    = PTP version                                             #
#                     (e.g., 2 ).                                              #
#                                                                              #
#  DEFINITION        : This function sets the PTP version the specific         #
#                      PTP port on the DUT.                                    #
#                                                                              #
#                      The last command should be "NONE".                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_PTP_SET_PTP_VERSION,ATTEST_SET,1)            "NOOP"
    set cli_set_record(DUT_PTP_SET_PTP_VERSION,ATTEST_SET,2)            "NONE"

#19.
################################################################################
#                                                                              #
#  COMMAND           : SET PTP PROFILE                                         #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_SET_PTP_PROFILE                                 #
#                                                                              #
#  INPUT PARAMETERS                                                            #
#                                                                              #
#  ATTEST_PARAM1      = port_num                                               #
#                     (e.g., 0/0, 1/1).                                        #
#                                                                              #
#  ATTEST_PARAM2     = PTP clock mode                                          #
#                     (e.g., Transparent clock)                                #
#                                                                              #
#  ATTEST_PARAM3     = PTP profile                                             #
#                      (e.g., power | default )                                #
#                                                                              #
#  DEFINITION        : This function sets the PTP profile for the specified    #
#                      clock mode on the DUT.                                  #
#                                                                              #
#                      The last command should be "NONE".                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

   set cli_set_record(DUT_PTP_SET_PTP_PROFILE,ATTEST_SET,1)          "NONE"


#############################################################################
#  COMMAND          : Add IP Interface                                      #
#                                                                           #
#  ARRAY INDEX      : DUT_PTP_ADD_IP_INTERFACE                              #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#     ATTEST_PARAM1 = interface                                             #
#     ATTEST_PARAM2 = ip_address                                            #
#     ATTEST_PARAM3 = mask                                                  #
#     ATTEST_PARAM4 = vlan_id                                               #
#                                                                           #
#  DEFINITION       : This Command  enables the administrative status of    #
#                     the location (interface) as up,and assigns an IP      #
#                     address with subnet mask for the specified location.  #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                      None.                                                #
#                                                                           #
#############################################################################

   set cli_set_record(DUT_PTP_ADD_IP_INTERFACE,ATTEST_SET,1)   "ifconfig ATTEST_PARAM1 ATTEST_PARAM2 up"
   set cli_set_record(DUT_PTP_ADD_IP_INTERFACE,ATTEST_SET,2)   "sleep 4"
   set cli_set_record(DUT_PTP_ADD_IP_INTERFACE,ATTEST_SET,3)   "NONE"


################################################################################
#                           DUT DISABLE FUNCTIONS                              #
################################################################################

#1.
################################################################################
#                                                                              #
#  COMMAND           : GLOBAL_PTP_DISABLE                                      #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_GLOBAL_PTP_DISABLE                              #
#                                                                              #
#  INPUT PARAMETERS                                                            #
#                                                                              #
#  DEFINITION        : This function disable the ptp operation in DUT          #
#                                                                              #
#                      The last command should be "NONE".                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_PTP_GLOBAL_PTP_DISABLE,ATTEST_SET,1)         "/wr/bin/apply_dot-config local_config"
    set cli_set_record(DUT_PTP_GLOBAL_PTP_DISABLE,ATTEST_TIME,1)        "30"
    set cli_set_record(DUT_PTP_GLOBAL_PTP_DISABLE,ATTEST_SET,2)         "/etc/init.d/hald.sh restart"
    set cli_set_record(DUT_PTP_GLOBAL_PTP_DISABLE,ATTEST_TIME,2)        "120"
    set cli_set_record(DUT_PTP_GLOBAL_PTP_DISABLE,ATTEST_SET,3)         "sleep 1"
    set cli_set_record(DUT_PTP_GLOBAL_PTP_DISABLE,ATTEST_TIME,3)        "120"
    set cli_set_record(DUT_PTP_GLOBAL_PTP_DISABLE,ATTEST_SET,4)         "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_PTP_GLOBAL_PTP_DISABLE,ATTEST_TIME,4)        "120"
    set cli_set_record(DUT_PTP_GLOBAL_PTP_DISABLE,ATTEST_SET,5)         "NONE"


#2.
################################################################################
#                                                                              #
#  COMMAND           : PORT_DISABLE                                            #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_PORT_DISABLE                                    #
#                                                                              #
#  INPUT PARAMETERS                                                            #
#                                                                              #
#  ATTEST_PARAM_1    = port num                                                #
#                     (e.g., fa0/0, 1/1 etc ).                                 #
#                                                                              #
#  DEFINITION         : This function disables the admin status of ports on    #
#                       the DUT.                                               #
#                                                                              #
#                      The last command should be "NONE".                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_PTP_PORT_DISABLE,ATTEST_SET,1)               "ifconfig ATTEST_PARAM1 0.0.0.0"
    set cli_set_record(DUT_PTP_PORT_DISABLE,ATTEST_SET,2)               "NONE"


#3.
################################################################################
#                                                                              #
#  COMMAND           : PORT_PTP_DISABLE                                        #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_PORT_PTP_DISABLE                                #
#                                                                              #
#  INPUT PARAMETERS                                                            #
#                                                                              #
#  ATTEST_PARAM_1    = port num                                                #
#                     (e.g., fa0/0, 1/1 etc ).                                 #
#                                                                              #
#  DEFINITION        : This function disables the ptp operation on port of DUT #
#                                                                              #
#                      The last command should be "NONE".                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_PTP_PORT_PTP_DISABLE,ATTEST_SET,1)           "/wr/bin/apply_dot-config local_config"
    set cli_set_record(DUT_PTP_PORT_PTP_DISABLE,ATTEST_TIME,1)          "30"
    set cli_set_record(DUT_PTP_PORT_PTP_DISABLE,ATTEST_SET,2)           "/etc/init.d/hald.sh restart"
    set cli_set_record(DUT_PTP_PORT_PTP_DISABLE,ATTEST_TIME,2)          "120"
    set cli_set_record(DUT_PTP_PORT_PTP_DISABLE,ATTEST_SET,3)           "sleep 1"
    set cli_set_record(DUT_PTP_PORT_PTP_DISABLE,ATTEST_TIME,3)          "120"
    set cli_set_record(DUT_PTP_PORT_PTP_DISABLE,ATTEST_SET,4)           "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_PTP_PORT_PTP_DISABLE,ATTEST_TIME,4)          "120"
    set cli_set_record(DUT_PTP_PORT_PTP_DISABLE,ATTEST_SET,5)           "sleep 4"
    set cli_set_record(DUT_PTP_PORT_PTP_DISABLE,ATTEST_SET,6)           "NONE"


#4.
################################################################################
#                                                                              #
#  COMMAND           : DISABLE_SLAVE_ONLY                                      #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_DISABLE_SLAVE_ONLY                              #
#                                                                              #
#  INPUT PARAMETERS                                                            #
#                                                                              #
#  ATTEST_PARAM_1    = port num                                                #
#                     (e.g., fa0/0, 1/1 etc ).                                 #
#                                                                              #
#  ATTEST_PARAM_2    = clock mode                                              #
#                      (e.g. Boundary Clock, Ordinary Clock )                  #
#                                                                              #
#  ATTEST_PARAM_3    = slave_only                                              #
#                                                                              #
#  DEFINITION        : This function disables slave only mode on specific      #
#                      PTP port of the DUT.                                    #
#                                                                              #
#                      The last command should be "NONE".                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_PTP_DISABLE_SLAVE_ONLY,ATTEST_SET,1)         "/wr/bin/apply_dot-config local_config"
    set cli_set_record(DUT_PTP_DISABLE_SLAVE_ONLY,ATTEST_TIME,1)        "30"
    set cli_set_record(DUT_PTP_DISABLE_SLAVE_ONLY,ATTEST_SET,2)         "/etc/init.d/hald.sh restart"
    set cli_set_record(DUT_PTP_DISABLE_SLAVE_ONLY,ATTEST_TIME,2)        "120"
    set cli_set_record(DUT_PTP_DISABLE_SLAVE_ONLY,ATTEST_SET,3)         "sleep 1"
    set cli_set_record(DUT_PTP_DISABLE_SLAVE_ONLY,ATTEST_TIME,3)        "120"
    set cli_set_record(DUT_PTP_DISABLE_SLAVE_ONLY,ATTEST_SET,4)         "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_PTP_DISABLE_SLAVE_ONLY,ATTEST_TIME,4)        "120"
    set cli_set_record(DUT_PTP_DISABLE_SLAVE_ONLY,ATTEST_SET,5)         "NONE"


#5.
################################################################################
#                                                                              #
#  COMMAND           : PORT_PTP_DISABLE_VLANID                                 #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_DISABLE_VLANID                                  #
#                                                                              #
#  INPUT PARAMETERS                                                            #
#                                                                              #
#  ATTEST_PARAM_1    = port num                                                #
#                     (e.g., fa0/0, 1/1 etc ).                                 #
#                                                                              #
#  ATTEST_PARAM_2    = clock_mode                                              #
#                      (e.g. Boundary Clock, Ordinary Clock )                  #
#                                                                              #
#  ATTEST_PARAM_3    = vlan_id                                                 #
#                                                                              #
#  DEFINITION        : This function removes the vlan membership of the        #
#                      PTP port on the DUT.                                    #
#                                                                              #
#                      The last command should be "NONE".                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_PTP_DISABLE_VLANID,ATTEST_SET,1)             "/wr/bin/apply_dot-config local_config"
    set cli_set_record(DUT_PTP_DISABLE_VLANID,ATTEST_TIME,1)            "30"
    set cli_set_record(DUT_PTP_DISABLE_VLANID,ATTEST_SET,2)             "/etc/init.d/hald.sh restart"
    set cli_set_record(DUT_PTP_DISABLE_VLANID,ATTEST_TIME,2)            "120"
    set cli_set_record(DUT_PTP_DISABLE_VLANID,ATTEST_SET,3)             "sleep 1"
    set cli_set_record(DUT_PTP_DISABLE_VLANID,ATTEST_TIME,3)            "120"
    set cli_set_record(DUT_PTP_DISABLE_VLANID,ATTEST_SET,4)             "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_PTP_DISABLE_VLANID,ATTEST_TIME,4)            "120"
    set cli_set_record(DUT_PTP_DISABLE_VLANID,ATTEST_SET,5)             "NONE"


#6.
################################################################################
#                                                                              #
#  COMMAND           : DUT_DELETE_VLANID                                       #
#                                                                              #
#  ARRAY INDEX       : DUT_DELETE_VLANID                                       #
#                                                                              #
#  INPUT PARAMETERS                                                            #
#                                                                              #
#  ATTEST_PARAM_1    = port num                                                #
#                     (e.g., fa0/0, 1/1 etc ).                                 #
#                                                                              #
#  ATTEST_PARAM_2    = clock_mode                                              #
#                      (e.g. Boundary Clock, Ordinary Clock )                  #
#                                                                              #
#  ATTEST_PARAM_3    = vlan_id                                                 #
#                                                                              #
#  DEFINITION        : This function sets the port mode to the specific        #
#                      PTP port on the DUT.                                    #
#                                                                              #
#                      The last command should be "NONE".                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_DELETE_VLANID,ATTEST_SET,1)                  "NONE"



#7.
#############################################################################
#  COMMAND          : Remove IP Interface                                   #
#                                                                           #
#  ARRAY INDEX      : DUT_PTP_REMOVE_IP_INTERFACE                           #
#                                                                           #
#  INPUT PARAMETERS :                                                       #
#                                                                           #
#     ATTEST_PARAM1 = interface                                             #
#     ATTEST_PARAM2 = ip address                                            #
#     ATTEST_PARAM3 = mask                                                  #
#     ATTEST_PARAM4 = vlan_id                                               #
#                                                                           #
#  DEFINITION       : This Command  removes the ip address and sets the     #
#                     admin status to 'down' for the specified location.    #
#                                                                           #
#  OUTPUT PARAMETERS:                                                       #
#                      None.                                                #
#                                                                           #
#############################################################################

   set cli_set_record(DUT_PTP_REMOVE_IP_INTERFACE,ATTEST_SET,1)   "ifconfig ATTEST_PARAM1 0.0.0.0"
   set cli_set_record(DUT_PTP_REMOVE_IP_INTERFACE,ATTEST_SET,2)   "NONE"

}



################################################################################
#                           DUT CHECK FUNCTIONS                                #
################################################################################

proc  tee_get_dut_ptp_cli_from_file {} {

global cli_get_record
global env ptp_env


#1.
################################################################################
#                                                                              #
#  COMMAND           : CHECK PTP PORT STATE                                    #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_CHECK_PORT_STATE                                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  ATTEST_PARAM_1    : Port Number                                             #
#                     (e.g., fa0/0, 1/1 etc ).                                 #
#  ATTEST_PARAM_2    : Clock mode                                              #
#                      (e.g. Boundary Clock, Ordinary Clock )                  #
#  ATTEST_PARAM_3    : port state                                              #
#                      (e.g master or slave)                                   #
#  OUTPUT PARAMETERS :                                                         #
#  buffer            : (mandatory) buffer used to store the value              #
#                               (e.g master or slave)                          #
#                                                                              #
#  DEFINITION        : This function check the State of specific clock         #
#                          mode on the DUT.                                    #
#                                                                              #
#                     The last command should be "NONE"                        #
#                                                                              #
################################################################################

    set cli_get_record(DUT_PTP_CHECK_PORT_STATE,ATTEST_OUTPUT,1)        "wrs_dump_shmem | egrep 'cfg.iface_name:| state:'"
    set cli_get_record(DUT_PTP_CHECK_PORT_STATE,ATTEST_GET,2)           "NONE"


#2.
################################################################################
#                                                                              #
#  COMMAND           : CHECK_DOMAIN_NUM                                        #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_CHECK_DOMAIN_NUM                                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                 (e.g.,, exp 6)                               #
#  ATTEST_PARAM_1    : (Mandatory) Clock mode                                  #
#                                  (e.g., fa0/0 ).                             #
#  ATTEST_PARAM_2    : (Mandatory) Domain Number                               #
#                                  (e.g.,1,100 ).                              #
#  OUTPUT PARAMETERS :                                                         #
#  buffer            : (mandatory) buffer used to store the value              #
#                               (e.g master or slave)                          #
#                                                                              #
#  DEFINITION        : This function check the Domain Number configured        #
#                       on the DUT.                                            #
#                                                                              #
#                     The last command should be "NONE"                        #
#  NOTE              : This mapping is specific to ordinary clock              #
#                                                                              #
################################################################################

    set cli_get_record(DUT_PTP_CHECK_DOMAIN_NUM,ATTEST_GET,1)           "NONE"


#3.
################################################################################
#                                                                              #
#  COMMAND           : CHECK_PRIORITY1_VALUE                                   #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_CHECK_PRIORITY1_VALUE                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                 (e.g.,, exp 6)                               #
#  ATTEST_PARAM_1    : (Mandatory) Clock mode                                  #
#                                  (e.g., fa0/0).                              #
#  ATTEST_PARAM_2    : (Mandatory) Priority 1 Value                            #
#                                  (e.g.,1,100).                               #
#  OUTPUT PARAMETERS :                                                         #
#  buffer            : (mandatory) buffer used to store the value              #
#                               (e.g master or slave)                          #
#                                                                              #
#  DEFINITION        : This function check the Priority 1 value configured     #
#                      on the DUT.                                             #
#                                                                              #
#                     The last command should be "NONE"                        #
#  NOTE              : This mapping is specific to ordinary clock              #
#                                                                              #
################################################################################

    set cli_get_record(DUT_PTP_CHECK_PRIORITY1_VALUE,ATTEST_GET,1)      "NONE"


#4.
################################################################################
#                                                                              #
#  COMMAND           : CHECK_PRIORITY2_VALUE                                   #
#                                                                              #
#  ARRAY INDEX       : DUT_PTP_CHECK_PRIORITY2_VALUE                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                 (e.g.,, exp 6)                               #
#  ATTEST_PARAM_1    : (Mandatory) Clock mode                                  #
#                                  (e.g., fa0/0).                              #
#  ATTEST_PARAM_2    : (Mandatory) Priority 2 Value                            #
#                                  (e.g.,1,100).                               #
#  OUTPUT PARAMETERS :                                                         #
#  buffer            : (mandatory) buffer used to store the value              #
#                               (e.g master or slave)                          #
#                                                                              #
#  DEFINITION        : This function check the Priority 2 value configured     #
#                      on the DUT.                                             #
#                                                                              #
#                     The last command should be "NONE"                        #
#  NOTE              : This mapping is specific to ordinary clock              #
#                                                                              #
################################################################################

    set cli_get_record(DUT_PTP_CHECK_PRIORITY2_VALUE,ATTEST_GET,1)      "NONE"

}



################################################################################
#                 CALL BACK FOR CLI ENABLE FUNCTIONS                           #
################################################################################

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : doScp                                                   #
#                                                                              #
#  DEFINITION        : This is internal function used for copying dot-config to#
#                      DUT                                                     #
#                                                                              #
################################################################################

proc doScp {from to {password rootroot}} {

    spawn scp $from $to

    set scp_session $spawn_id

    expect {

        -i $scp_session

        "password:" {
            send "$password"

            expect {
                -i $scp_session

                "denied" {
                    printLog "Invalid password"
                }
                eof {#printLog "(\"$from\" to \"$to\") copied successfully."}
                timeout {printLog "Connection timed out while copying file."}

            }
        }
        "yes/no" {
            send "yes\n"
            exp_continue
        }
        eof {printLog "SCP: Unable to establish connection to DUT."}
        timeout {printLog "SCP: Connection timed out."}
        default {printLog "SCP: Cannot connect."}
    }
}


#2.
################################################################################
# PROCEDURE NAME    : delEmptyEof                                              #
#                                                                              #
# DEFINITION        : This is internal function used for deleting empty lines  #
#                     at EOF                                                   #
#                                                                              #
################################################################################

proc delEmptyEof { config } {

    set read_fd [open $config "r"]
    set read_content [read $read_fd]
    close $read_fd

    set empty_line      ""
    set write_content   ""

    foreach line [split $read_content "\n"] {
        if {$line == ""} {
            append empty_line "\n"
        } else {
            append write_content $empty_line
            append write_content "$line\n"
            set empty_line ""
        }
    }

    set write_fd [open $config "w+"]
    puts $write_fd $write_content
    close $write_fd

    return 1
}


#3.
################################################################################
# PROCEDURE NAME    : printLog                                                 #
#                                                                              #
# DEFINITION        : This is internal function used for print logs of         #
#                     configuration parameter.                                 #
#                                                                              #
################################################################################

proc printLog {message} {

    puts "INFO: $message"
    return 1
}


################################################################################
#                 CALL BACK FOR CLI ENABLE FUNCTIONS                           #
################################################################################


#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp::callback_cli_global_ptp_enable                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                 (e.g.,, exp 6)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP enabled globally at the DUT).      #
#                                                                              #
#                      0 on Failure (If PTP is not enabled globally at the DUT)#
#                                                                              #
#  DEFINITION        : This function enables PTP in DUT                        #
#                                                                              #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::callback_cli_global_ptp_enable\                      #
#                              -session_handler $session_handler               #
#                                                                              #
################################################################################

proc ptp::callback_cli_global_ptp_enable { args } {

    array set param $args

    set session_handler $param(-session_handler)

    global host username password
    global ptp_env

    set domain_number       $::ptp_dut_default_domain_number
    set priority1           $::ptp_dut_default_priority1
    set priority2           $::ptp_dut_default_priority2
    set vlan_id             $::ptp_vlan_id

    set announce_interval   $::ptp_announce_interval
    set sync_interval       $::ptp_sync_interval

    set is_trunk_param_exists   0
    set is_vid_param_exists     0

    set dotconfig_path      /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp
    set dotconfig_file      $dotconfig_path/dot-config
    set backup_file         $dotconfig_file.old

    doScp $username@$host:/wr/etc/dot-config $dotconfig_file "\n"

    if [file exists $dotconfig_file] {
        #Backup dot-config file
        exec cp $dotconfig_file $backup_file

        #Read dot-config file
        set dotconfig_fd [open $dotconfig_file "r"]
        set config       [read $dotconfig_fd]
        close $dotconfig_fd
    } else {
        printLog "$dotconfig_file file does not exists"
        return 0
    }

    set new_config ""

#   printLog "Performing configuration updation in dot-config."

    foreach line [split $config "\n"] {

        #set default domain number
        set config_parameter CONFIG_PTP_OPT_DOMAIN_NUMBER

        if {[regexp $config_parameter $line]} {

            set d [lindex  [split $line =]  1]

            if { $line == "CONFIG_PTP_OPT_DOMAIN_NUMBER=\"\"" } {

                set line [string map "CONFIG_PTP_OPT_DOMAIN_NUMBER=\"\" CONFIG_PTP_OPT_DOMAIN_NUMBER=\"$domain_number\"" $line]
                printLog "CONFIG_PTP_OPT_DOMAIN_NUMBER=\"$domain_number\""
            } else {

                set line [string map "$d $domain_number" $line]
                printLog "CONFIG_PTP_OPT_DOMAIN_NUMBER=\"$domain_number\""
            }
        }

        ## change Announce interval based on config
        set config_parameter CONFIG_PTP_OPT_ANNOUNCE_INTERVAL

        if {[regexp $config_parameter $line]} {

            set d [lindex  [split $line =]  1]

            if { $line == "CONFIG_PTP_OPT_ANNOUNCE_INTERVAL=\"\"" } {

                set line [string map "CONFIG_PTP_OPT_ANNOUNCE_INTERVAL=\"\" CONFIG_PTP_OPT_ANNOUNCE_INTERVAL=\"$announce_interval\"" $line]
                printLog "CONFIG_PTP_OPT_ANNOUNCE_INTERVAL=\"$announce_interval\""
            } else {

                set line [string map "$d $announce_interval" $line]
                printLog "CONFIG_PTP_OPT_ANNOUNCE_INTERVAL=\"$announce_interval\""
            }
        }

        ## change Sync interval based on config
        set config_parameter CONFIG_PTP_OPT_SYNC_INTERVAL

        if {[regexp $config_parameter $line]} {

            set d [lindex  [split $line =]  1]

            if { $line == "CONFIG_PTP_OPT_SYNC_INTERVAL=\"\"" } {

                set line [string map "CONFIG_PTP_OPT_SYNC_INTERVAL=\"\" CONFIG_PTP_OPT_SYNC_INTERVAL=\"$sync_interval\"" $line]
                printLog "CONFIG_PTP_OPT_SYNC_INTERVAL=\"$sync_interval\""
            } else {

                set line [string map "$d $sync_interval" $line]
                printLog "CONFIG_PTP_OPT_SYNC_INTERVAL=\"$sync_interval\""
            }
        }

        ##Change priority1 based on config
        set config_parameter "CONFIG_PTP_OPT_PRIORITY1"

        if {[regexp $config_parameter $line]} {

            set d [lindex  [split $line =]  1]

            if { $line == "CONFIG_PTP_OPT_PRIORITY1=\"\"" } {

                set line [string map "CONFIG_PTP_OPT_PRIORITY1=\"\" CONFIG_PTP_OPT_PRIORITY1=\"$priority1\"" $line]
                printLog "CONFIG_PTP_OPT_PRIORITY1=\"$priority1\""
            } else {

                set line [string map "$d $priority1" $line]
                printLog "CONFIG_PTP_OPT_PRIORITY1=\"$priority1\""
            }
        }

        ##Change priority2 based on config
        set config_parameter "CONFIG_PTP_OPT_PRIORITY2"

        if {[regexp $config_parameter $line]} {

            set d [lindex  [split $line =]  1]

            if { $line == "CONFIG_PTP_OPT_PRIORITY2=\"\"" } {

                set line [string map "CONFIG_PTP_OPT_PRIORITY2=\"\" CONFIG_PTP_OPT_PRIORITY2=\"$priority2\"" $line]
                printLog "CONFIG_PTP_OPT_PRIORITY2=\"$priority2\""
            } else {

                set line [string map "$d $priority2" $line]
                printLog "CONFIG_PTP_OPT_PRIORITY2=\"$priority2\""
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to dot-config file
    set dotconfig_fd [open $dotconfig_file  "w+"]
    puts $dotconfig_fd $new_config
    close $dotconfig_fd

    delEmptyEof $dotconfig_file

    ##copy the updated dot_config file to device
    doScp $dotconfig_file $username@$host:/wr/etc $password

    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_PTP_GLOBAL_PTP_ENABLE"]

    return $result
}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp::callback_cli_ptp_port_enable                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                  (e.g., exp6 )                               #
#                                                                              #
#  port_num         : (Mandatory) Port on the DUT                              #
#                                  (e.g.,, fa0/1, fa0/2)                       #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If ports are enabled on the DUT)          #
#                                                                              #
#                      0 on Failure (If DUT ports could not be able to enable) #
#                                                                              #
#  DEFINITION        : This function enables the ports on DUT.                 #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::cli_port_enable\                                     #
#                             -session_handler $session_handler\               #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp::callback_cli_ptp_port_enable {args} {

    array set param $args

    set session_handler       $param(-session_handler)
    set port_num              $param(-port_num)

    #set result [Send_cli_command \
                     -session_handler $session_handler \
                     -cmd_type        "DUT_PTP_PORT_ENABLE"\
                     -parameter1      $port_num]

    #return $result

    return 2
}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp::callback_cli_ptp_port_ptp_enable                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                  (e.g., exp6 )                               #
#                                                                              #
#  port_num          : (Mandatory) Port number on which ptp is to be           #
#                                  enabled.                                    #
#                                  (e.g.,, [list fa0/1, fa0/2)                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is enabled on the specified list   #
#                                    of ports at the DUT).                     #
#                                                                              #
#                      0 on Failure (If PTP could not be enabled on the        #
#                                    specified list of ports at the DUT).      #
#                                                                              #
#  DEFINITION        : This function enables PTP on the specified list of      #
#                      ports at the DUT.                                       #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::cli_port_ptp_enable                                  #
#                             -session_handler $session_handler\               #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp::callback_cli_ptp_port_ptp_enable {args} {

    array set param $args

    set session_handler          $param(-session_handler)
    set port_num                 $param(-port_num)

    global host username password

    ## TRANSPORT TYPE CONFIG ###
    array set TRANS_TYPE {
        "IEEE 802.3"    "raw"
        "UDP/IPv4"      "udp"
    }

    set transport           [set TRANS_TYPE($::ptp_transport_type)]

    set is_trunk_param_exists   0
    set is_vid_param_exists     0

    set dotconfig_path      /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp
    set dotconfig_file      $dotconfig_path/dot-config
    set backup_file         $dotconfig_file.old

    doScp $username@$host:/wr/etc/dot-config $dotconfig_file "\n"

    if [file exists $dotconfig_file] {
        #Backup dot-config file
        exec cp $dotconfig_file $backup_file

        #Read dot-config file
        set dotconfig_fd [open $dotconfig_file "r"]
        set config       [read $dotconfig_fd]
        close $dotconfig_fd
    } else {
        printLog "$dotconfig_file file does not exists"
        return 0
    }

    set new_config ""

#   printLog "Performing configuration updation in dot-config."

    foreach line [split $config "\n"] {

        set portno [regexp {[0-9]+} $port_num port]
        if {[string length $port] == 1 } {
            set port "0$port"
        } else {
            set port $port
        }

        set config_parameter CONFIG_PORT$port\_PARAMS
        if {[regexp $config_parameter $line]} {
            set items           [split $line ,]
            set index_role      [lsearch $items "role*"]
            set index_proto     [lsearch $items "proto*"]
            set role_retrieved  [lindex $items $index_role]
            set proto_retrieved [lindex $items $index_proto]
            set line [string map "$proto_retrieved proto=$transport $role_retrieved role=auto" $line]
            printLog "($port_num) proto=$transport, role=auto"
        }

        append new_config "$line\n"
    }

    #Write configuration to dot-config file
    set dotconfig_fd [open $dotconfig_file  "w+"]
    puts $dotconfig_fd $new_config
    close $dotconfig_fd

    delEmptyEof $dotconfig_file

    ##copy the updated dot_config file to device
    doScp $dotconfig_file $username@$host:/wr/etc $password

    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_PTP_PORT_PTP_ENABLE"]

#   sleep $::dut_config_delay

    return $result
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp::callback_cli_ptp_set_clock_mode                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                  (e.g., exp6 )                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode                   .              #
#                      (e.g. Boundary Clock, Ordinary Clock )                  #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP periodic timer is enabled on the   #
#                                    specified list of ports at the DUT).      #
#                                                                              #
#                      0 on Failure (If PTP periodic timer could not be        #
#                                    enabled on the specified list of ports    #
#                                    at the DUT)                               #
#                                                                              #
#  DEFINITION        : This function enables the PTP clock mode                #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::cli_set_clock_mode\                                  #
#                             -session_handler $session_handler\               #
#                             -clock_mode      $clock_mode                     #
#                                                                              #
################################################################################

proc ptp::callback_cli_ptp_set_clock_mode {args} {

    array set param $args

    set session_handler          $param(-session_handler)
    set clock_mode               $param(-clock_mode)

    #set result  [Send_cli_command \
                -session_handler        $session_handler \
                -cmd_type               "DUT_PTP_SET_CLOCK_MODE"\
                -parameter1             $clock_mode]

    # return $result

    return 2
}


#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp::callback_cli_ptp_set_domain                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                      (e.g., exp6 ).                                          #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                      (e.g. Boundary Clock, Ordinary Clock )                  #
#                                                                              #
#  domain            : (Mandatory) Domain Number.                              #
#                      (e.g., 0..255 ).                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If Domain Number is set on the specific   #
#                                    Clock mode of the DUT).                   #
#                                                                              #
#                      0 on Failure (If Domain Number is not able to set on    #
#                                    the specific Clock mode of the DUT).      #
#                                                                              #
#  DEFINITION        : This function set Domain number on the specific clock   #
#                      mode of the DUT,                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::cli_set_domain\                                      #
#                              -session_handler $session_handler\              #
#                              -clock_mode      $clock_mode\                   #
#                              -domain          $domain                        #
#                                                                              #
################################################################################

proc ptp::callback_cli_ptp_set_domain {args} {

    array set param $args

    set session_handler          $param(-session_handler)
    set clock_mode               $param(-clock_mode)
    set domain                   $param(-domain)

    global group_name case_id

    if {!(($group_name == "pcg") && ($case_id == "002"))} {

#       printLog "Skipping this configuration as this is already done in global configuration."
        return 1
    }

    global host username password
    global ptp_env

    set dotconfig_path      /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp
    set dotconfig_file      $dotconfig_path/dot-config
    set backup_file         $dotconfig_file.old

    doScp $username@$host:/wr/etc/dot-config $dotconfig_file "\n"

    if [file exists $dotconfig_file] {
        #Backup dot-config file
        exec cp $dotconfig_file $backup_file

        #Read dot-config file
        set dotconfig_fd [open $dotconfig_file "r"]
        set config       [read $dotconfig_fd]
        close $dotconfig_fd
    } else {
        printLog "$dotconfig_file file does not exists"
        return 0
    }

    set new_config ""

#   printLog "Performing configuration updation in dot-config."

    foreach line [split $config "\n"] {

        #set default domain number
        set config_parameter CONFIG_PTP_OPT_DOMAIN_NUMBER

        if {[regexp $config_parameter $line]} {

            set d [lindex  [split $line =]  1]

            if { $line == "CONFIG_PTP_OPT_DOMAIN_NUMBER=\"\"" } {

                set line [string map "CONFIG_PTP_OPT_DOMAIN_NUMBER=\"\" CONFIG_PTP_OPT_DOMAIN_NUMBER=\"$domain\"" $line]
                printLog "CONFIG_PTP_OPT_DOMAIN_NUMBER=\"$domain\""
            } else {

                set line [string map "$d $domain" $line]
                printLog "CONFIG_PTP_OPT_DOMAIN_NUMBER=\"$domain\""
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to dot-config file
    set dotconfig_fd [open $dotconfig_file  "w+"]
    puts $dotconfig_fd $new_config
    close $dotconfig_fd

    delEmptyEof $dotconfig_file

    ##copy the updated dot_config file to device
    doScp $dotconfig_file $username@$host:/wr/etc $password

    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_PTP_SET_DOMAIN"]

    sleep $::dut_config_delay

    return $result
}


#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp::callback_cli_ptp_set_priority1                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                      (e.g., exp6 ).                                          #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                      (e.g. Boundary Clock, Ordinary Clock )                  #
#                                                                              #
#  priority1         : (Mandatory) Priority1                                   #
#                      (e.g., 0..255 ).                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority1 value is set to specific     #
#                                    clock mode on the DUT).                   #
#                                                                              #
#                      0 on Failure (If priority1 value could not be set to    #
#                                    specific clock mode on the DUT).          #
#                                                                              #
#  DEFINITION        : This function sets the priority1 value for specific     #
#                      clock mode on the DUT.                                  #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::cli_set_priority1\                                   #
#                             -session_handler $session_handler\               #
#                             -clock_mode      $clock_mode\                    #
#                             -priority1       $priority1                      #
#                                                                              #
################################################################################

proc ptp::callback_cli_ptp_set_priority1 {args} {

    array set param $args

    set session_handler          $param(-session_handler)
    set clock_mode               $param(-clock_mode)
    set priority1                $param(-priority1)

    global group_name case_id

    if {!(($group_name == "pcg") && ($case_id == "011"))} {

#       printLog "Skipping this configuration as this is already done in global configuration."
        return 1
    }

    global host username password
    global ptp_env

    set dotconfig_path      /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp
    set dotconfig_file      $dotconfig_path/dot-config
    set backup_file         $dotconfig_file.old

    doScp $username@$host:/wr/etc/dot-config $dotconfig_file "\n"

    if [file exists $dotconfig_file] {
        #Backup dot-config file
        exec cp $dotconfig_file $backup_file

        #Read dot-config file
        set dotconfig_fd [open $dotconfig_file "r"]
        set config       [read $dotconfig_fd]
        close $dotconfig_fd
    } else {
        printLog "$dotconfig_file file does not exists"
        return 0
    }

    set new_config ""

#   printLog "Performing configuration updation in dot-config."

    foreach line [split $config "\n"] {

        ##Change priority1 based on config
        set config_parameter "CONFIG_PTP_OPT_PRIORITY1"

        if {[regexp $config_parameter $line]} {

            set d [lindex  [split $line =]  1]

            if { $line == "CONFIG_PTP_OPT_PRIORITY1=\"\"" } {

                set line [string map "CONFIG_PTP_OPT_PRIORITY1=\"\" CONFIG_PTP_OPT_PRIORITY1=\"$priority1\"" $line]
                printLog "CONFIG_PTP_OPT_PRIORITY1=\"$priority1\""
            } else {

                set line [string map "$d $priority1" $line]
                printLog "CONFIG_PTP_OPT_PRIORITY1=\"$priority1\""
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to dot-config file
    set dotconfig_fd [open $dotconfig_file  "w+"]
    puts $dotconfig_fd $new_config
    close $dotconfig_fd

    delEmptyEof $dotconfig_file

    ##copy the updated dot_config file to device
    doScp $dotconfig_file $username@$host:/wr/etc $password

    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_PTP_SET_PRIORITY1"]

    sleep $::dut_config_delay

    return $result
}


#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp::callback_cli_ptp_set_priority2                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                      (e.g., exp6 ).                                          #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                      (e.g. Boundary Clock, Ordinary Clock )                  #
#                                                                              #
#  priority2         : (Mandatory) Priority2                                   #
#                      (e.g., 0..255 ).                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority2 is set on the specific clock #
#                                    mode on the DUT).                         #
#                                                                              #
#                      0 on Failure (If priority2 value could not be set on the#
#                                    specific clock                            #
#                                                                              #
#  DEFINITION        : This function sets  PTP priority2 value on the          #
#                      specific clock mode on the DUT.                         #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::cli_set_priority2\                                   #
#                             -session_handler $session_handler\               #
#                             -clock_mode      $clock_mode\                    #
#                             -priority2       $priority2                      #
#                                                                              #
################################################################################

proc ptp::callback_cli_ptp_set_priority2 {args} {

    array set param $args

    set session_handler          $param(-session_handler)
    set clock_mode               $param(-clock_mode)
    set priority2                $param(-priority2)

    global group_name case_id

    if {!((($group_name == "pcg") && ($case_id == "012")) || (($group_name == "beg") && ($case_id == "005"))) } {

#       printLog "Skipping this configuration as this is already done in global configuration."
        return 1
    }

    global host username password
    global ptp_env

    set dotconfig_path      /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp
    set dotconfig_file      $dotconfig_path/dot-config
    set backup_file         $dotconfig_file.old

    doScp $username@$host:/wr/etc/dot-config $dotconfig_file "\n"

    if [file exists $dotconfig_file] {
        #Backup dot-config file
        exec cp $dotconfig_file $backup_file

        #Read dot-config file
        set dotconfig_fd [open $dotconfig_file "r"]
        set config       [read $dotconfig_fd]
        close $dotconfig_fd
    } else {
        printLog "$dotconfig_file file does not exists"
        return 0
    }

    set new_config ""

#   printLog "Performing configuration updation in dot-config."

    foreach line [split $config "\n"] {

        ##Change priority1 based on config
        set config_parameter "CONFIG_PTP_OPT_PRIORITY2"

        if {[regexp $config_parameter $line]} {

            set d [lindex  [split $line =]  1]

            if { $line == "CONFIG_PTP_OPT_PRIORITY2=\"\"" } {

                set line [string map "CONFIG_PTP_OPT_PRIORITY2=\"\" CONFIG_PTP_OPT_PRIORITY2=\"$priority2\"" $line]
                printLog "CONFIG_PTP_OPT_PRIORITY2=\"$priority2\""
            } else {

                set line [string map "$d $priority2" $line]
                printLog "CONFIG_PTP_OPT_PRIORITY2=\"$priority2\""
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to dot-config file
    set dotconfig_fd [open $dotconfig_file  "w+"]
    puts $dotconfig_fd $new_config
    close $dotconfig_fd

    delEmptyEof $dotconfig_file

    ##copy the updated dot_config file to device
    doScp $dotconfig_file $username@$host:/wr/etc $password

    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_PTP_SET_PRIORITY2"]

    sleep $::dut_config_delay

    return $result
}


#8.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp::callback_cli_ptp_set_announce_interval             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                      (e.g., exp6 ).                                          #
#                                                                              #
#  port_num          : (Mandatory) port on which the PTP is enabled.           #
#                      (e.g., fa0/0, 1/1 etc ).                                #
#                                                                              #
#  clock_mode        : (Mandatory) clock_mode                                  #
#                      (e.g. Boundary Clock, Ordinary Clock )                  #
#                                                                              #
#  announce_interval : (Mandatory) Interval at which Annonce Message is        #
#                                  transmitted from DUT in Sec                 #
#                     (e.g. 0,1,2,3,4 ).                                       #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If Annonce interval value is configured   #
#                                    on specific clock mode on the DUT).       #
#                                                                              #
#                      0 on Failure (If Annonce interval value could not be    #
#                                    configured on specific clock mode on DUT).#
#                                                                              #
#  DEFINITION        : This function set the PTP announce interval value to the#
#                      specific PTP port on the DUT.                           #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::cli_set_announce_interval\                           #
#                             -session_handler  $session_handler\              #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -announce_interval $announce_interval            #
#                                                                              #
################################################################################

proc ptp::callback_cli_ptp_set_announce_interval {args} {

    array set param $args

    set session_handler          $param(-session_handler)
    set port_num                 $param(-port_num)
    set clock_mode               $param(-clock_mode)
    set announce_interval        $param(-announce_interval)

    global group_name case_id

    if {!(($group_name == "pcg") && ($case_id == "014")) } {

#       printLog "Skipping this configuration as this is already done in global configuration."
        return 1
    }

    global host username password
    global ptp_env

    set dotconfig_path      /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp
    set dotconfig_file      $dotconfig_path/dot-config
    set backup_file         $dotconfig_file.old

    doScp $username@$host:/wr/etc/dot-config $dotconfig_file "\n"

    if [file exists $dotconfig_file] {

        #Backup dot-config file
        exec cp $dotconfig_file $backup_file

        #Read dot-config file
        set dotconfig_fd [open $dotconfig_file "r"]
        set config       [read $dotconfig_fd]
        close $dotconfig_fd
    } else {

        printLog "$dotconfig_file file does not exists"
        return 0
    }

    set new_config ""

#   printLog "Performing configuration updation in dot-config."

    foreach line [split $config "\n"] {

        ## change Announce interval based on config
        set config_parameter CONFIG_PTP_OPT_ANNOUNCE_INTERVAL

        if {[regexp $config_parameter $line]} {

            set d [lindex  [split $line =]  1]

            if { $line == "CONFIG_PTP_OPT_ANNOUNCE_INTERVAL=\"\"" } {

                set line [string map "CONFIG_PTP_OPT_ANNOUNCE_INTERVAL=\"\" CONFIG_PTP_OPT_ANNOUNCE_INTERVAL=\"$announce_interval\"" $line]
                printLog "CONFIG_PTP_OPT_ANNOUNCE_INTERVAL=\"$announce_interval\""
            } else {

                set line [string map "$d $announce_interval" $line]
                printLog "CONFIG_PTP_OPT_ANNOUNCE_INTERVAL=\"$announce_interval\""
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to dot-config file
    set dotconfig_fd [open $dotconfig_file  "w+"]
    puts $dotconfig_fd $new_config
    close $dotconfig_fd

    delEmptyEof $dotconfig_file

    ##copy the updated dot_config file to device
    doScp $dotconfig_file $username@$host:/wr/etc $password

    set result [Send_cli_command \
                -session_handler        $session_handler \
                -cmd_type               "DUT_PTP_SET_ANNOUNCE_INTERVAL"]

    sleep $::dut_config_delay

    return $result
}


#9.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp::callback_cli_ptp_set_announce_timeout              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                      (e.g., exp6 ).                                          #
#                                                                              #
#  port_num          : (Mandatory) port on which the PTP is enabled.           #
#                      (e.g., fa0/0, 1/1 etc ).                                #
#                                                                              #
#  clock_mode        : (Mandatory) clock_mode.                                 #
#                      (e.g. Boundary Clock, Ordinary Clock )                  #
#                                                                              #
#  announce_timeout  : (Mandatory) Announce message timeout value.             #
#                      (e.g. 2 - 10 )                                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If Annonce timeout value is configured    #
#                                    on PTP port of the DUT).                  #
#                                                                              #
#                      0 on Failure (If Annonce timeout value could not be     #
#                                    configured on PTP port on DUT).           #
#                                                                              #
#  DEFINITION        : This function set the PTP announce timeout value to the #
#                      specific PTP port on the DUT.                           #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::cli_set_announce_timeout\                            #
#                             -session_handler  $session_handler\              #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -announce_timeout  $announce_timeout             #
#                                                                              #
################################################################################

proc ptp::callback_cli_ptp_set_announce_timeout {args} {

    array set param $args

    set session_handler          $param(-session_handler)
    set port_num                 $param(-port_num)
    set clock_mode               $param(-clock_mode)
    set announce_timeout          $param(-announce_timeout)

    #set result [Send_cli_command \
                -session_handler        $session_handler \
                -cmd_type               "DUT_PTP_SET_ANNOUNCE_TIMEOUT"\
                -parameter1             $port_num\
                -parameter2             $clock_mode\
                -parameter3             $announce_timeout]

    #return $result

    sleep $::dut_config_delay

    return 2
}


#10.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp::callback_cli_ptp_set_delay_mechanism               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                  (e.g., exp6 )                               #
#                                                                              #
#  port_num          : (Mandatory) port on which the PTP is enabled.           #
#                                  (e.g., fa0/0 ).                             #
#                                                                              #
#  clock_mode        : (Mandatory) clock_mode.                                 #
#                                  (e.g., BC ).                                #
#                                                                              #
#  delay_mechanism   : (Mandatory) Delay mechanism.                            #
#                                  (e.g., e2e|p2p|disable  )                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If Delay mechanism is configured          #
#                                    on PTP port of the DUT).                  #
#                                                                              #
#                      0 on Failure (If Delay mechanism could not be           #
#                                    configured on PTP port of the DUT).       #
#                                                                              #
#  DEFINITION        : This function set the Delay mechanism value to the      #
#                      specific PTP port on the DUT.                           #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::cli_set_delay_mechanism\                             #
#                             -session_handler  $session_handler\              #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -delay_mechanism  $delay_mechanism               #
#                                                                              #
################################################################################

proc ptp::callback_cli_ptp_set_delay_mechanism {args} {

    array set param $args

    set session_handler          $param(-session_handler)
    set port_num                 $param(-port_num)
    set clock_mode               $param(-clock_mode)
    set delay_mechanism          $param(-delay_mechanism)

    set port_list                $port_num

    global host username password

    set dotconfig_path      /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp
    set dotconfig_file      $dotconfig_path/dot-config
    set backup_file         $dotconfig_file.old

    doScp $username@$host:/wr/etc/dot-config $dotconfig_file "\n"

    foreach port_num [split $port_list " "] {

        if [file exists $dotconfig_file] {
            #Backup dot-config file
            exec cp $dotconfig_file $backup_file

            #Read dot-config file
            set dotconfig_fd [open $dotconfig_file "r"]
            set config       [read $dotconfig_fd]
            close $dotconfig_fd
        } else {
            printLog "$dotconfig_file file does not exists"
            return 0
        }

        set new_config ""

#       printLog "Performing configuration updation in dot-config."

        foreach line [split $config "\n"] {

            set portno [regexp {[0-9]+} $port_num port]

            if {[string length $port] == 1 } {
                set port "0$port"
            } else {
                set port $port
            }

            set config_parameter CONFIG_PORT$port\_PARAMS
            if {[regexp $config_parameter $line]} {
                set items           [split $line ,]
                set index_dm        [lsearch $items "dm*"]
                set dm_retrieved    [lindex $items $index_dm]
                set line [string map "$dm_retrieved dm=$delay_mechanism" $line]
                printLog "($port_num) dm=$delay_mechanism"
            }

            append new_config "$line\n"
        }

        #Write configuration to dot-config file
        set dotconfig_fd [open $dotconfig_file  "w+"]
        puts $dotconfig_fd $new_config
        close $dotconfig_fd

        delEmptyEof $dotconfig_file
    }

    ##copy the updated dot_config file to device
    doScp $dotconfig_file $username@$host:/wr/etc $password

    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_PTP_SET_DELAY_MECHANISM"]

    return $result
}


#11.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp::callback_cli_ptp_set_clock_step                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                  (e.g., exp6 )                               #
#                                                                              #
#  port_num          : (Mandatory) port on which the PTP is enabled.           #
#                                  (e.g., fa0/0 ).                             #
#                                                                              #
#  clock_mode        : (Mandatory) clock_mode.                                 #
#                                  (e.g., BC ).                                #
#                                                                              #
#  clock_step        : (Mandatory) Clock step.                                 #
#                                  (e.g., one-step | two-step )                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If clock-set is configured                #
#                                    on PTP port of the DUT).                  #
#                                                                              #
#                      0 on Failure (If clock-set could not be                 #
#                                    configured on PTP port of the DUT).       #
#                                                                              #
#  DEFINITION        : This function set the clock-set to the specific PTP port#
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::cli_set_clock_step\                                  #
#                             -session_handler  $session_handler\              #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -clock_step       $clock_step                    #
#                                                                              #
################################################################################

proc ptp::callback_cli_ptp_set_clock_step {args} {

    array set param $args

    set session_handler          $param(-session_handler)
    set port_num                 $param(-port_num)
    set clock_mode               $param(-clock_mode)
    set clock_step               $param(-clock_step)

    #set result  [Send_cli_command \
                -session_handler        $session_handler \
                -cmd_type               "DUT_PTP_SET_CLOCK_STEP"\
                -parameter1             $port_num\
                -parameter2             $clock_mode\
                -parameter3             $clock_step]

    #return $result

    return 2
}


#12.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp::callback_cli_ptp_set_network_protocol              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                  (e.g., exp6 )                               #
#                                                                              #
#  port_num          : (Mandatory) port on which the PTP is enabled.           #
#                                  (e.g., fa0/0 ).                             #
#                                                                              #
#  clock_mode        : (Mandatory) clock_mode.                                 #
#                                  (e.g., BC ).                                #
#                                                                              #
#  network_protocol  : (Mandatory) Network protocol.                           #
#                                  (e.g., ieee802_3 | udp ipv4 )               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If network protocol is configured         #
#                                    on PTP port of the DUT).                  #
#                                                                              #
#                      0 on Failure (If network protocol could not be          #
#                                    configured on PTP port of the DUT).       #
#                                                                              #
#  DEFINITION        : This function set the network protocol to the specific  #
#                      PTP port on the DUT.                                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::cli_set_network_protocol\                            #
#                             -session_handler  $session_handler\              #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -network_protocol $network_protocol              #
#                                                                              #
################################################################################

proc ptp::callback_cli_ptp_set_network_protocol {args} {

    array set param $args

    set session_handler          $param(-session_handler)
    set port_num                 $param(-port_num)
    set clock_mode               $param(-clock_mode)
    set network_protocol         $param(-network_protocol)

    return 2
}


#13.
################################################################################
#                                                                              #
#  PROCEDURE NAME          :  ptp::callback_cli_ptp_set_message_transport_mode #
#                                                                              #
#  INPUT PARAMETERS          :                                                 #
#                                                                              #
#  session_handler           : (Mandatory) Session ID of the DUT Session       #
#                                          opened.(e.g., exp6 )                #
#                                                                              #
#  port_num                 : (Mandatory) port on which the PTP is enabled.    #
#                                  (e.g., fa0/0 ).                             #
#                                                                              #
#  clock_mode               : (Mandatory) clock_mode.                          #
#                                  (e.g., BC ).                                #
#                                                                              #
#  ptp_version              : (optional)  ptp version (e.g., 2)                #
#                                                                              #
#  message_transport_mode   : (Mandatory) message transport mode               #
#                                  (e.g., unicast | multicast )                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If network protocol is configured         #
#                                    on PTP port of the DUT).                  #
#                                                                              #
#                      0 on Failure (If network protocol could not be          #
#                                    configured on PTP port of the DUT).       #
#                                                                              #
#  DEFINITION        : This function set the message transport mode to the     #
#                       specific PTP port on the DUT.                          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::cli_set_message_transport_mode\                      #
#                             -session_handler         $session_handler\       #
#                             -port_num                $port_num\              #
#                             -ptp_version             $ptp_version\           #
#                             -clock_mode              $clock_mode\            #
#                             -message_transport_mode  $message_transport_mode #
#                                                                              #
################################################################################

proc ptp::callback_cli_ptp_set_message_transport_mode {args} {

    array set param $args

    set session_handler          $param(-session_handler)
    set port_num                 $param(-port_num)
    set clock_mode               $param(-clock_mode)
    set ptp_version              $param(-ptp_version)
    set message_transport_mode   $param(-message_transport_mode)

    #set result [Send_cli_command \
                -session_handler        $session_handler \
                -cmd_type               "DUT_PTP_SET_MESSAGE_TRANSPORT_MODE"\
                -parameter1             $port_num\
                -parameter2             $clock_mode\
                -parameter3             $ptp_version\
                -parameter4             $message_transport_mode]

    #return $result

    return 2
}


#14.
################################################################################
#                                                                              #
#  PROCEDURE NAME            :  ptp::callback_cli_ptp_set_pdelay_interval      #
#                                                                              #
#  INPUT PARAMETERS          :                                                 #
#                                                                              #
#  session_handler           : (Mandatory) Session ID of the DUT Session       #
#                                          opened.(e.g., exp6 )                #
#                                                                              #
#  port_num                 : (Mandatory) port on which the PTP is enabled.    #
#                                  (e.g., fa0/0 ).                             #
#                                                                              #
#  clock_mode               : (Mandatory) clock_mode.                          #
#                                  (e.g., BC ).                                #
#                                                                              #
#  pdelay_interval          : (Mandatory) pdelay interval.                     #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If pdelay interval is configured          #
#                                    on PTP port of the DUT).                  #
#                                                                              #
#                      0 on Failure (If pdelay interval could not be           #
#                                    configured on PTP port of the DUT).       #
#                                                                              #
#  DEFINITION        : This function sets the pdelay interval to the specific  #
#                      PTP port on the DUT.                                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::cli_set_pdelay_interval\                             #
#                             -session_handler          $session_handler\      #
#                             -port_num                 $port_num\             #
#                             -clock_mode               $clock_mode\           #
#                             -pdelay_interval          $pdelay_interval       #
#                                                                              #
################################################################################

proc ptp::callback_cli_ptp_set_pdelay_interval {args} {

    array set param $args

    set session_handler          $param(-session_handler)
    set port_num                 $param(-port_num)
    set clock_mode               $param(-clock_mode)
    set pdelay_interval          $param(-pdelay_interval)

    #set result [Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_PTP_SET_PDELAY_INTERVAL"\
                        -parameter1             $port_num\
                        -parameter2             $clock_mode\
                        -parameter3             $pdelay_interval]

    #return $result

    return 2
}


#15.
################################################################################
#                                                                              #
#  PROCEDURE NAME            : ptp::callback_cli_ptp_set_sync_interval         #
#                                                                              #
#  INPUT PARAMETERS          :                                                 #
#                                                                              #
#  session_handler           : (Mandatory) Session ID of the DUT Session       #
#                                          opened.(e.g., exp6 )                #
#                                                                              #
#  port_num                 : (Mandatory) port on which the PTP is enabled.    #
#                                  (e.g., fa0/0 ).                             #
#                                                                              #
#  clock_mode               : (Mandatory) clock_mode.                          #
#                                  (e.g., BC ).                                #
#                                                                              #
#  sync_interval           : (Mandatory) Sync Message interval                 #
#                                  (e.g., 0.25|0.5|1|2 ).                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If sync interval is configured            #
#                                    on PTP port of the DUT).                  #
#                                                                              #
#                      0 on Failure (If sync interval could not be             #
#                                    configured on PTP port of the DUT).       #
#                                                                              #
#  DEFINITION        : This function sets the sync interval to the specific    #
#                      PTP port on the DUT.                                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::cli_set_sync_interval\                               #
#                             -session_handler          $session_handler\      #
#                             -port_num                 $port_num\             #
#                             -clock_mode               $clock_mode\           #
#                             -sync_interval            $sync_interval         #
#                                                                              #
################################################################################

proc ptp::callback_cli_ptp_set_sync_interval {args} {

    array set param $args

    set session_handler          $param(-session_handler)
    set port_num                 $param(-port_num)
    set clock_mode               $param(-clock_mode)
    set sync_interval            $param(-sync_interval)

    global group_name case_id

    if {!(($group_name == "pcg") && ($case_id == "015"))} {

#       printLog "Skipping this configuration as this is already done in global configuration."
        return 1
    }

    global host username password
    global ptp_env

    set dotconfig_path      /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp
    set dotconfig_file      $dotconfig_path/dot-config
    set backup_file         $dotconfig_file.old

    doScp $username@$host:/wr/etc/dot-config $dotconfig_file "\n"

    if [file exists $dotconfig_file] {

        #Backup dot-config file
        exec cp $dotconfig_file $backup_file

        #Read dot-config file
        set dotconfig_fd [open $dotconfig_file "r"]
        set config       [read $dotconfig_fd]
        close $dotconfig_fd
    } else {

        printLog "$dotconfig_file file does not exists"
        return 0
    }

    set new_config ""

#   printLog "Performing configuration updation in dot-config."

    foreach line [split $config "\n"] {

        ## change Sync interval based on config
        set config_parameter CONFIG_PTP_OPT_SYNC_INTERVAL

        if {[regexp $config_parameter $line]} {

            set d [lindex  [split $line =]  1]

            if { $line == "CONFIG_PTP_OPT_SYNC_INTERVAL=\"\"" } {

                set line [string map "CONFIG_PTP_OPT_SYNC_INTERVAL=\"\" CONFIG_PTP_OPT_SYNC_INTERVAL=\"$sync_interval\"" $line]
                printLog "CONFIG_PTP_OPT_SYNC_INTERVAL=\"$sync_interval\""
            } else {

                set line [string map "$d $sync_interval" $line]
                printLog "CONFIG_PTP_OPT_SYNC_INTERVAL=\"$sync_interval\""
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to dot-config file
    set dotconfig_fd [open $dotconfig_file  "w+"]
    puts $dotconfig_fd $new_config
    close $dotconfig_fd

    delEmptyEof $dotconfig_file

    ##copy the updated dot_config file to device
    doScp $dotconfig_file $username@$host:/wr/etc $password

    set result  [Send_cli_command \
                -session_handler        $session_handler \
                -cmd_type               "DUT_PTP_SET_SYNC_INTERVAL"]

    sleep $::dut_config_delay

    return $result
}


#16.
################################################################################
#                                                                              #
#  PROCEDURE NAME            : ptp::callback_cli_ptp_set_vlan                  #
#                                                                              #
#  INPUT PARAMETERS          :                                                 #
#                                                                              #
#  session_handler           : (Mandatory) Session ID of the DUT Session       #
#                                          opened.(e.g., exp6 )                #
#                                                                              #
#  port_num                 : (Mandatory) port on which the PTP is enabled.    #
#                                  (e.g., fa0/0 ).                             #
#                                                                              #
#  clock_mode               : (Mandatory) clock_mode.                          #
#                                  (e.g., BC ).                                #
#                                                                              #
#  vlan_id                 : (Mandatory) vlan id                               #
#                                  (e.g., 1 - 4095 ).                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If vlan id is configured                  #
#                                    on PTP port of the DUT).                  #
#                                                                              #
#                      0 on Failure (If vlan id could not be                   #
#                                    configured on PTP port of the DUT).       #
#                                                                              #
#  DEFINITION        : This function sets the vlan id to the specific          #
#                      PTP port on the DUT.                                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::cli_set_vlan\                                        #
#                             -session_handler          $session_handler\      #
#                             -port_num                 $port_num\             #
#                             -clock_mode               $clock_mode\           #
#                             -vlan_id                  $vlan_id               #
#                                                                              #
################################################################################

proc ptp::callback_cli_ptp_set_vlan {args} {

    array set param $args

    set session_handler          $param(-session_handler)
    set port_num                 $param(-port_num)
    set clock_mode               $param(-clock_mode)
    set vlan_id                  $param(-vlan_id)

    set port_list                $port_num

    global host username password
    global ptp_env

    set dotconfig_path      /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp
    set dotconfig_file      $dotconfig_path/dot-config
    set backup_file         $dotconfig_file.old

    doScp $username@$host:/wr/etc/dot-config $dotconfig_file "\n"

    foreach port_num [split $port_list " "] {

        set is_trunk_param_exists   0
        set is_vid_param_exists     0

        if [file exists $dotconfig_file] {
            #Backup dot-config file
            exec cp $dotconfig_file $backup_file

            #Read dot-config file
            set dotconfig_fd [open $dotconfig_file "r"]
            set config       [read $dotconfig_fd]
            close $dotconfig_fd
        } else {
            printLog "$dotconfig_file file does not exists"
            return 0
        }

        set new_config ""

#       printLog "Performing configuration updation in dot-config."


        foreach line [split $config "\n"] {

            if {[string match "*CONFIG_VLANS_ENABLE*" $line]} {
                append new_config "CONFIG_VLANS_ENABLE=y\n"
                printLog "CONFIG_VLANS_ENABLE=y"
                continue
            }

            set portno [regexp {[0-9]+} $port_num port]
            if {[string length $port] == 1 } {
                set port "0$port"
            } else {
                set port $port
            }

            if {[string match "*CONFIG_VLANS_PORT$port\_MODE_TRUNK*" $line]} {

                append new_config "CONFIG_VLANS_PORT$port\_MODE_TRUNK=y\n"
                printLog "($port_num) CONFIG_VLANS_PORT$port\_MODE_TRUNK=y"
                set is_trunk_param_exists   1
                continue
            }

            if {[string match "*CONFIG_VLANS_PORT$port\_VID*" $line]} {

                append new_config "CONFIG_VLANS_PORT$port\_VID=\"$vlan_id\"\n"
                printLog "($port_num) CONFIG_VLANS_PORT$port\_VID=\"$vlan_id\""
                set is_vid_param_exists     1
                continue
            }

            append new_config "$line\n"
        }

        if {$is_trunk_param_exists != 1} {

            append new_config "CONFIG_VLANS_PORT$port\_MODE_TRUNK=y\n"
            printLog "($port_num) CONFIG_VLANS_PORT$port\_MODE_TRUNK=y"
        }

        if {$is_vid_param_exists != 1} {

            append new_config "CONFIG_VLANS_PORT$port\_VID=\"$vlan_id\"\n"
            printLog "($port_num) CONFIG_VLANS_PORT$port\_VID=\"$vlan_id\""
        }


        #Write configuration to dot-config file
        set dotconfig_fd [open $dotconfig_file  "w+"]
        puts $dotconfig_fd $new_config
        close $dotconfig_fd

        delEmptyEof $dotconfig_file
    }

    ##copy the updated dot_config file to device
    doScp $dotconfig_file $username@$host:/wr/etc $password

    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_PTP_SET_VLAN"]

    return $result
}


#17.
################################################################################
#                                                                              #
#  PROCEDURE NAME            : ptp::callback_cli_ptp_set_vlan_priority         #
#                                                                              #
#  INPUT PARAMETERS          :                                                 #
#                                                                              #
#  session_handler           : (Mandatory) Session ID of the DUT Session       #
#                                          opened.(e.g., exp6 )                #
#                                                                              #
#  port_num                 : (Mandatory) port on which the PTP is enabled.    #
#                                  (e.g., fa0/0 ).                             #
#                                                                              #
#  clock_mode               : (Mandatory) clock_mode.                          #
#                                  (e.g., BC ).                                #
#                                                                              #
#  vlan_id                  : (Mandatory) vlan priority                        #
#                                  (e.g., 1 - 7 ).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If vlan priority is configured            #
#                                    on PTP port of the DUT).                  #
#                                                                              #
#                      0 on Failure (If vlan priority could not be             #
#                                    configured on PTP port of the DUT).       #
#                                                                              #
#  DEFINITION        : This function sets the vlan priority to the specific    #
#                      PTP port on the DUT.                                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::cli_set_vlan_priority\                               #
#                             -session_handler          $session_handler\      #
#                             -port_num                 $port_num\             #
#                             -clock_mode               $clock_mode\           #
#                             -vlan_priority            $vlan_priority         #
#                                                                              #
################################################################################

proc ptp::callback_cli_ptp_set_vlan_priority {args} {

    array set param $args

    set session_handler          $param(-session_handler)
    set port_num                 $param(-port_num)
    set clock_mode               $param(-clock_mode)
    set vlan_priority            $param(-vlan_priority)

    set result [Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_PTP_SET_VLAN_PRIORITY"\
                        -parameter1              $port_num\
                        -parameter2              $clock_mode\
                        -parameter3              $vlan_priority]

    sleep $::dut_config_delay

    return $result
}


#18.
################################################################################
#                                                                              #
#  PROCEDURE NAME           :  ptp::callback_cli_ptp_enable_slave_only         #
#                                                                              #
#  INPUT PARAMETERS         :                                                  #
#                                                                              #
#  session_handler          : (Mandatory) Session ID of the DUT Session        #
#                                          opened.(e.g., exp6 )                #
#                                                                              #
#  port_num                 : (Mandatory) port on which the PTP is enabled.    #
#                                  (e.g., fa0/0 ).                             #
#                                                                              #
#  clock_mode               : (Mandatory) clock_mode.                          #
#                                  (e.g., BC ).                                #
#                                                                              #
#  slave_only               : (Mandatory) slave_only                           #
#                                  (e.g., true ).                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If DUT is set to slave only mode ).       #
#                                                                              #
#                      0 on Failure (If DUT is not able to set to slave only   #
#                                    mode ).                                   #
#                                                                              #
#  DEFINITION        : This function enable slave only mode to the specific    #
#                      PTP port on the DUT.                                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::cli_enable_slave_only\                               #
#                             -session_handler          $session_handler\      #
#                             -port_num                 $port_num\             #
#                             -clock_mode               $clock_mode\           #
#                             -slave_only               $slave_only            #
#                                                                              #
################################################################################

proc ptp::callback_cli_ptp_enable_slave_only {args} {

    array set param $args

    set session_handler          $param(-session_handler)
    set port_num                 $param(-port_num)
    set clock_mode               $param(-clock_mode)
    set slave_only               $param(-slave_only)

    global host username password

    set dotconfig_path      /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp
    set dotconfig_file      $dotconfig_path/dot-config
    set backup_file         $dotconfig_file.old

    doScp $username@$host:/wr/etc/dot-config $dotconfig_file "\n"

    if [file exists $dotconfig_file] {
        #Backup dot-config file
        exec cp $dotconfig_file $backup_file

        #Read dot-config file
        set dotconfig_fd [open $dotconfig_file "r"]
        set config       [read $dotconfig_fd]
        close $dotconfig_fd
    } else {
        printLog "$dotconfig_file file does not exists"
        return 0
    }

    set new_config ""

#   printLog "Performing configuration updation in dot-config."

    foreach line [split $config "\n"] {

        set portno [regexp {[0-9]+} $port_num port]
        if {[string length $port] == 1 } {
            set port "0$port"
        } else {
            set port $port
        }

        set config_parameter CONFIG_PORT$port\_PARAMS
        if {[regexp $config_parameter $line]} {
            set items           [split $line ,]
            set index_role      [lsearch $items "role*"]
            set role_retrieved  [lindex $items $index_role]
            set line [string map "$role_retrieved role=slave" $line]
            printLog "($port_num) role=slave"
        }

        append new_config "$line\n"
    }

    #Write configuration to dot-config file
    set dotconfig_fd [open $dotconfig_file  "w+"]
    puts $dotconfig_fd $new_config
    close $dotconfig_fd

    delEmptyEof $dotconfig_file

    ##copy the updated dot_config file to device
    doScp $dotconfig_file $username@$host:/wr/etc $password

    set result [Send_cli_command \
                    -session_handler        $session_handler \
                    -cmd_type               "DUT_PTP_ENABLE_SLAVE_ONLY"]

    sleep $::dut_config_delay

    return $result
}


#19.
################################################################################
#                                                                              #
#  PROCEDURE NAME           :  ptp::callback_cli_ptp_set_ptp_version           #
#                                                                              #
#  INPUT PARAMETERS         :                                                  #
#                                                                              #
#  session_handler          : (Mandatory) Session ID of the DUT Session        #
#                                          opened.(e.g., exp6 )                #
#                                                                              #
#  port_num                 : (Mandatory) port on which the PTP is enabled.    #
#                                  (e.g., fa0/0 ).                             #
#                                                                              #
#  clock_mode               : (Mandatory) clock_mode.                          #
#                                  (e.g., BC ).                                #
#                                                                              #
#  slave_only               : (Mandatory) slave_only                           #
#                                  (e.g., true ).                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If DUT is set to slave only mode ).       #
#                                                                              #
#                      0 on Failure (If DUT is not able to set to slave only   #
#                                    mode ).                                   #
#                                                                              #
#  DEFINITION        : This function enable slave only mode to the specific    #
#                      PTP port on the DUT.                                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::cli_ptp_set_ptp_version\                             #
#                             -session_handler          $session_handler\      #
#                             -port_num                 $port_num\             #
#                             -clock_mode               $clock_mode\           #
#                             -slave_only               $slave_only            #
#                                                                              #
################################################################################

proc ptp::callback_cli_ptp_set_ptp_version {args} {

    array set param $args

    set session_handler          $param(-session_handler)
    set ptp_version              $param(-ptp_version)

    #set result [Send_cli_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_PTP_SET_PTP_VERSION"\
                        -parameter1             $port_num\
                        -parameter2             $clock_mode\
                        -parameter3             $slave_only]

    #return $result

    return 2
}


################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp::callback_cli_ptp_set_profile                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                  (e.g., exp6 )                               #
#                                                                              #
#  port_list         : (Mandatory) List of ports on which the profile to be    #
#                                  configured.                                 #
#                                  (e.g.,  0/1, 0/2)                           #                                 
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode                                  #
#                                  (e.g. Transparent Clock )                   #
#                                                                              #
#  profile           : (Optional) Profile to be configured                     #
#                                  (e.g., power | default )                    #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If configured profile is enabled on       #
#                                    specified clock mode at the DUT).         #
#                                                                              #
#                      0 on Failure (If configured profile could not be        #
#                                    enabled on the specified clock mode       #
#                                    at the DUT)                               #
#                                                                              #
#                                                                              #
#  DEFINITION        : This function enables the PTP profile for specified     #
#                      clock mode.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::callback_cli_ptp_set_profile\                        #
#                             -session_handler $session_handler\               #
#                             -port_list       $port_list\                     #
#                             -clock_mode      $clock_mode\                    #
#                             -profile         $profile                        #
#                                                                              #
################################################################################

proc ptp::callback_cli_ptp_set_profile {args} {

 array set param $args

### Check Input parameters ###

      set session_handler          $param(-session_handler)
      set port_list                $param(-port_list)
      set clock_mode               $param(-clock_mode)
      set delay_mechanism          $param(-delay_mechanism)   
      set profile                  $param(-profile)   

#### If the configuration of the PTP profile is at the interface level ####
#### uncomment the below code and comment return 2 #####

#for {set i 0 } {$i < [llength $port_list]} {incr i } {

#       set result  [Send_cli_command \
#                       -session_handler         $session_handler \
#                       -cmd_type                "DUT_PTP_SET_PTP_PROFILE"\
#                       -parameter1              [lindex $port_list $i]\   
#                       -parameter2              $clock_mode\   
#                       -parameter3              $delay_mechanism\
#                       -parameter4              $profile]      
#}
# return $result

return 2
}


#############################################################################
#  PROCEDURE NAME    : ptp::callback_ipv4_add_ip_one_interface              #
#                                                                           #
#  INPUT PARAMETERS  :                                                      #
#                                                                           #
#   session_handler  : (Mandatory) Session ID of the DUT session opened.    #
#   cmd_type         : (Mandatory) Command type.  Used by send_cli_command  #
#   interface        : (Mandatory) Location/Interface to be added on the    #
#                                  DUT.                                     #
#                                  (e.g. ethernet 0/0)                      #
#   ip_address       : (Mandatory) IP address to be assigned to the         #
#                                  specified interface.                     #
#                                  (e.g. 192.168.9.13)                      #
#   vlan_id          : (Mandatory) VLAN ID.                                 #
#   mask             : (Mandatory) Subnet Mask for the specified IPv4       #
#                                  address.                                 #
#                                  (e.g. 255.255.255.0)                     #
#                                                                           #
#  OUTPUT PARAMETERS :                                                      #
#                                                                           #
#                    : NONE                                                 #
#                                                                           #
#  RETURNS           : 1 on success (If the given interface is added on the #
#                                    DUT with the specified IP address and  #
#                                    network mask).                         #
#                      0 on failure (If the given interface could not be    #
#                                    added on the DUT with the specified    #
#                                    IP address and network mask).          #
#                      2 if the ATTEST has to call the command.             #
#                                                                           #
#############################################################################
proc ptp::callback_ipv4_add_ip_interface {args} {

   array set param $args

### Check Input parameters ###
   set session_handler $param(-session_handler)
   set cmd_type        $param(-cmd_type)
   set interface       $param(-interface)
   set ip_address      $param(-ip_address)
   set mask            $param(-mask)

   #set result  [Send_cli_command \
                       -session_handler         $session_handler \
                       -cmd_type                "DUT_PTP_ADD_IP_INTERFACE"\
                       -parameter1              $interface\
                       -parameter2              $ip_address\
                       -parameter3              $mask]

  #          return $result

   return 2
}
################################################################################
#                           CLI DISABLE FUNCTIONS                              #
################################################################################

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp::callback_cli_global_ptp_disable                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                  (e.g., exp6 )                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If ptp and dynamic mac creation are       #
#                                     disabled globally at the DUT).           #
#                                                                              #
#                      0 on Failure (If ptp and dynamic mac creation could     #
#                                    not be disabled globally at the DUT).     #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::cli_global_ptp_disable\                              #
#                                   -session_handler $session_handler          #
#                                                                              #
################################################################################

proc ptp::callback_cli_global_ptp_disable {args} {

    array set param $args

    set session_handler     $param(-session_handler)

    set domain_number       $::ptp_dut_default_domain_number
    set priority1           $::ptp_dut_default_priority1
    set priority2           $::ptp_dut_default_priority2
    set vlan_id             $::ptp_vlan_id

    set announce_interval   $::ptp_announce_interval
    set sync_interval       $::ptp_sync_interval

    set is_trunk_param_exists   0
    set is_vid_param_exists     0

    global host username password

    set dotconfig_path      /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp
    set dotconfig_file      $dotconfig_path/dot-config
    set backup_file         $dotconfig_file.old

    doScp $username@$host:/wr/etc/dot-config $dotconfig_file "\n"

    if [file exists $dotconfig_file] {
        #Backup dot-config file
        exec cp $dotconfig_file $backup_file

        #Read dot-config file
        set dotconfig_fd [open $dotconfig_file "r"]
        set config       [read $dotconfig_fd]
        close $dotconfig_fd
    } else {
        printLog "$dotconfig_file file does not exists"
        return 0
    }

    set new_config ""

#   printLog "Performing configuration updation in dot-config."

    foreach line [split $config "\n"] {

        #set default domain number
        set config_parameter CONFIG_PTP_OPT_DOMAIN_NUMBER

        if {[regexp $config_parameter $line]} {

            set d [lindex  [split $line =]  1]

            if { $line == "CONFIG_PTP_OPT_DOMAIN_NUMBER=\"\"" } {

                set line [string map "CONFIG_PTP_OPT_DOMAIN_NUMBER=\"\" CONFIG_PTP_OPT_DOMAIN_NUMBER=\"$domain_number\"" $line]
                printLog "CONFIG_PTP_OPT_DOMAIN_NUMBER=\"$domain_number\""
            } else {

                set line [string map "$d $domain_number" $line]
                printLog "CONFIG_PTP_OPT_DOMAIN_NUMBER=\"$domain_number\""
            }
        }

        ## change Announce interval based on config
        set config_parameter CONFIG_PTP_OPT_ANNOUNCE_INTERVAL

        if {[regexp $config_parameter $line]} {

            set d [lindex  [split $line =]  1]

            if { $line == "CONFIG_PTP_OPT_ANNOUNCE_INTERVAL=\"\"" } {

                set line [string map "CONFIG_PTP_OPT_ANNOUNCE_INTERVAL=\"\" CONFIG_PTP_OPT_ANNOUNCE_INTERVAL=\"$announce_interval\"" $line]
                printLog "CONFIG_PTP_OPT_ANNOUNCE_INTERVAL=\"$announce_interval\""
            } else {

                set line [string map "$d $announce_interval" $line]
                printLog "CONFIG_PTP_OPT_ANNOUNCE_INTERVAL=\"$announce_interval\""
            }
        }

        ## change Sync interval based on config
        set config_parameter CONFIG_PTP_OPT_SYNC_INTERVAL

        if {[regexp $config_parameter $line]} {

            set d [lindex  [split $line =]  1]

            if { $line == "CONFIG_PTP_OPT_SYNC_INTERVAL=\"\"" } {

                set line [string map "CONFIG_PTP_OPT_SYNC_INTERVAL=\"\" CONFIG_PTP_OPT_SYNC_INTERVAL=\"$sync_interval\"" $line]
                printLog "CONFIG_PTP_OPT_SYNC_INTERVAL=\"$sync_interval\""
            } else {

                set line [string map "$d $sync_interval" $line]
                printLog "CONFIG_PTP_OPT_SYNC_INTERVAL=\"$sync_interval\""
            }
        }

        ##Change priority1 based on config
        set config_parameter "CONFIG_PTP_OPT_PRIORITY1"

        if {[regexp $config_parameter $line]} {

            set d [lindex  [split $line =]  1]

            if { $line == "CONFIG_PTP_OPT_PRIORITY1=\"\"" } {

                set line [string map "CONFIG_PTP_OPT_PRIORITY1=\"\" CONFIG_PTP_OPT_PRIORITY1=\"$priority1\"" $line]
                printLog "CONFIG_PTP_OPT_PRIORITY1=\"$priority1\""
            } else {

                set line [string map "$d $priority1" $line]
                printLog "CONFIG_PTP_OPT_PRIORITY1=\"$priority1\""
            }
        }

        ##Change priority2 based on config
        set config_parameter "CONFIG_PTP_OPT_PRIORITY2"

        if {[regexp $config_parameter $line]} {

            set d [lindex  [split $line =]  1]

            if { $line == "CONFIG_PTP_OPT_PRIORITY2=\"\"" } {

                set line [string map "CONFIG_PTP_OPT_PRIORITY2=\"\" CONFIG_PTP_OPT_PRIORITY2=\"$priority2\"" $line]
                printLog "CONFIG_PTP_OPT_PRIORITY2=\"$priority2\""
            } else {

                set line [string map "$d $priority2" $line]
                printLog "CONFIG_PTP_OPT_PRIORITY2=\"$priority2\""
            }
        }

        ##Change Vlan based on config
        if { ($::ptp_vlan_encap == "true") } {

            if {[string match "*CONFIG_VLANS_ENABLE*" $line]} {
                append new_config "# CONFIG_VLANS_ENABLE is not set\n"
                printLog "# CONFIG_VLANS_ENABLE is not set"
                continue
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to dot-config file
    set dotconfig_fd [open $dotconfig_file  "w+"]
    puts $dotconfig_fd $new_config
    close $dotconfig_fd

    delEmptyEof $dotconfig_file

    ##copy the updated dot_config file to device
    doScp $dotconfig_file $username@$host:/wr/etc $password

    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_PTP_GLOBAL_PTP_DISABLE"]

#   sleep $::dut_config_delay

    return $result
}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp::callback_cli_ptp_port_disable                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                  (e.g., exp6 )                               #
#                                                                              #
#  port_list         : (Mandatory) List of Test ports on DUT.                  #
#                                  (e.g., [list fa0/1 fa0/2] ).                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If Test port on DUT are disabled )        #
#                                                                              #
#                      0 on Failure (If Test port on DUT could not be          #
#                                    disabled ).                               #
#                                                                              #
#  DEFINITION         : This function disables the admin status of ports on    #
#                       the DUT.                                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::cli_port_disable\                                    #
#                             -session_handler $session_handler\               #
#                             -port_list       $port_list                      #
#                                                                              #
################################################################################

proc ptp::callback_cli_ptp_port_disable {args} {

    array set param $args

    set session_handler          $param(-session_handler)
    set port_num                 $param(-port_num)

    #set result  [Send_cli_command \
                            -session_handler        $session_handler\
                            -cmd_type               "DUT_PTP_PORT_DISABLE"\
                            -parameter1             $port_num]

    #return $result

    return 2
}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp::callback_cli_ptp_port_ptp_disable                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                  (e.g., exp6 )                               #
#                                                                              #
#  port_list         : (Mandatory) List of ports on which ptp port             #
#                                  need to be disabled.                        #
#                                  (e.g., [list fa0/1 fa0/2] ).                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If ptp port is disabled on the            #
#                                    specified list of ports at the DUT).      #
#                                                                              #
#                      0 on Failure (If ptp port could not be disabled on the  #
#                                    port).                                    #
#                                                                              #
#  DEFINITION        : This function disables the ptp operation on port of DUT #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::cli_port_ptp_disable\                                #
#                             -session_handler $session_handler                #
#                             -port_list       $port_list                      #
#                                                                              #
################################################################################

proc ptp::callback_cli_ptp_port_ptp_disable {args} {

    array set param $args

    set session_handler         $param(-session_handler)
    set port_num                $param(-port_num)

    global host username password

    ## TRANSPORT TYPE CONFIG ###
    array set TRANS_TYPE {
        "IEEE 802.3"    "raw"
        "UDP/IPv4"      "udp"
    }

    set transport           [set TRANS_TYPE($::ptp_transport_type)]
    set delay_mechanism     $::ptp_dut_delay_mechanism

    set is_trunk_param_exists   0
    set is_vid_param_exists     0

    set dotconfig_path      /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp
    set dotconfig_file      $dotconfig_path/dot-config
    set backup_file         $dotconfig_file.old

    doScp $username@$host:/wr/etc/dot-config $dotconfig_file "\n"

    if [file exists $dotconfig_file] {
        #Backup dot-config file
        exec cp $dotconfig_file $backup_file

        #Read dot-config file
        set dotconfig_fd [open $dotconfig_file "r"]
        set config       [read $dotconfig_fd]
        close $dotconfig_fd
    } else {
        printLog "$dotconfig_file file does not exists"
        return 0
    }

    set new_config ""

#   printLog "Performing configuration updation in dot-config."

    foreach line [split $config "\n"] {

        set portno [regexp {[0-9]+} $port_num port]
        if {[string length $port] == 1 } {
            set port "0$port"
        } else {
            set port $port
        }

        set config_parameter CONFIG_PORT$port\_PARAMS
        if {[regexp $config_parameter $line]} {
            set items           [split $line ,]
            set index_role      [lsearch $items "role*"]
            set index_proto     [lsearch $items "proto*"]
            set index_dm        [lsearch $items "dm*"]
            set role_retrieved  [lindex $items $index_role]
            set proto_retrieved [lindex $items $index_proto]
            set dm_retrieved    [lindex $items $index_dm]
            set line [string map "$proto_retrieved proto=$transport $role_retrieved role=none $dm_retrieved dm=$delay_mechanism" $line]
            printLog "($port_num) proto=$transport, role=none, dm=$delay_mechanism"
        }

        append new_config "$line\n"
    }

    #Write configuration to dot-config file
    set dotconfig_fd [open $dotconfig_file  "w+"]
    puts $dotconfig_fd $new_config
    close $dotconfig_fd

    delEmptyEof $dotconfig_file

    ##copy the updated dot_config file to device
    doScp $dotconfig_file $username@$host:/wr/etc $password

    if {![Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_PTP_PORT_PTP_DISABLE"]} {

        return 0
    }

    sleep $::dut_config_delay

    return 1
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME           :  ptp::callback_cli_ptp_disable_slave_only        #
#                                                                              #
#  INPUT PARAMETERS         :                                                  #
#                                                                              #
#  session_handler          : (Mandatory) Session ID of the DUT Session        #
#                                          opened.(e.g., exp6 )                #
#                                                                              #
#  port_num                 : (Mandatory) port on which the PTP is enabled.    #
#                                  (e.g., fa0/0 ).                             #
#                                                                              #
#  clock_mode               : (Mandatory) clock_mode.                          #
#                                  (e.g., BC ).                                #
#                                                                              #
#  slave_only               : (Mandatory) slave_only                           #
#                                  (e.g., false ).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If DUT unset slave only mode ).           #
#                                                                              #
#                      0 on Failure (If DUT is not able to unset slave only    #
#                                    mode ).                                   #
#                                                                              #
#  DEFINITION        : This function sets the port mode to the specific        #
#                      PTP port on the DUT.                                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::cli_disable_slave_only\                              #
#                             -session_handler          $session_handler\      #
#                             -port_num                 $port_num\             #
#                             -clock_mode               $clock_mode\           #
#                             -slave_only               $slave_only            #
#                                                                              #
################################################################################

proc ptp::callback_cli_ptp_disable_slave_only {args} {

    array set param $args

    set session_handler          $param(-session_handler)
    set port_num                 $param(-port_num)
    set clock_mode               $param(-clock_mode)
    set slave_only               $param(-slave_only)

    global host username password

    set dotconfig_path      /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp
    set dotconfig_file      $dotconfig_path/dot-config
    set backup_file         $dotconfig_file.old

    doScp $username@$host:/wr/etc/dot-config $dotconfig_file "\n"

    if [file exists $dotconfig_file] {
        #Backup dot-config file
        exec cp $dotconfig_file $backup_file

        #Read dot-config file
        set dotconfig_fd [open $dotconfig_file "r"]
        set config       [read $dotconfig_fd]
        close $dotconfig_fd
    } else {
        printLog "$dotconfig_file file does not exists"
        return 0
    }

    set new_config ""

#   printLog "Performing configuration updation in dot-config."

    foreach line [split $config "\n"] {

        set portno [regexp {[0-9]+} $port_num port]
        if {[string length $port] == 1 } {
            set port "0$port"
        } else {
            set port $port
        }

        set config_parameter CONFIG_PORT$port\_PARAMS
        if {[regexp $config_parameter $line]} {
            set items           [split $line ,]
            set index_role      [lsearch $items "role*"]
            set role_retrieved  [lindex $items $index_role]
            set line [string map "$role_retrieved role=auto" $line]
            printLog "($port_num) role=auto"
        }

        append new_config "$line\n"
    }

    #Write configuration to dot-config file
    set dotconfig_fd [open $dotconfig_file  "w+"]
    puts $dotconfig_fd $new_config
    close $dotconfig_fd

    delEmptyEof $dotconfig_file

    ##copy the updated dot_config file to device
    doScp $dotconfig_file $username@$host:/wr/etc $password

    set result [Send_cli_command \
                    -session_handler        $session_handler \
                    -cmd_type               "DUT_PTP_DISABLE_SLAVE_ONLY"]

#   sleep $::dut_config_delay

    return $result
}


#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME           :  ptp::callback_cli_vlan_id_disable               #
#                                                                              #
#  INPUT PARAMETERS         :                                                  #
#                                                                              #
#  session_handler          : (Mandatory) Session ID of the DUT Session        #
#                                          opened.(e.g., exp6 )                #
#                                                                              #
#  port_num                 : (Mandatory) port on which the PTP is enabled.    #
#                                  (e.g., fa0/0 ).                             #
#                                                                              #
#  clock_mode               : (Mandatory) clock_mode.                          #
#                                  (e.g., BC ).                                #
#                                                                              #
#  vlan_id                  : (Mandatory) vlan_id                              #
#                                  (e.g., 200 ).                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If DUT unset slave only mode ).           #
#                                                                              #
#                      0 on Failure (If DUT is not able to unset slave only    #
#                                    mode ).                                   #
#                                                                              #
#  DEFINITION        : This function sets the port mode to the specific        #
#                      PTP port on the DUT.                                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::cli_vlan_id_disable\                                 #
#                             -session_handler          $session_handler\      #
#                             -port_num                 $port_num\             #
#                             -clock_mode               $clock_mode\           #
#                             -vlan_id                  $vlan_id               #
#                                                                              #
################################################################################

proc ptp::callback_cli_vlan_id_disable  {args} {

    array set param $args

    set session_handler          $param(-session_handler)
    set port_num                 $param(-port_num)
    set clock_mode               $param(-clock_mode)
    set vlan_id                  $param(-vlanid)

    set port_list                $port_num

    global host username password
    global ptp_env

    set dotconfig_path      /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp
    set dotconfig_file      $dotconfig_path/dot-config
    set backup_file         $dotconfig_file.old

    doScp $username@$host:/wr/etc/dot-config $dotconfig_file "\n"

    foreach port_num [split $port_list " "] {

        set is_trunk_param_exists   0
        set is_vid_param_exists     0

        if [file exists $dotconfig_file] {
            #Backup dot-config file
            exec cp $dotconfig_file $backup_file

            #Read dot-config file
            set dotconfig_fd [open $dotconfig_file "r"]
            set config       [read $dotconfig_fd]
            close $dotconfig_fd
        } else {
            printLog "$dotconfig_file file does not exists"
            return 0
        }

        set new_config ""

#       printLog "Performing configuration updation in dot-config."


        foreach line [split $config "\n"] {

            if {[string match "*CONFIG_VLANS_ENABLE*" $line]} {
                append new_config "#CONFIG_VLANS_ENABLE is not set\n"
                printLog "#CONFIG_VLANS_ENABLE is not set"
                continue
            }

            set portno [regexp {[0-9]+} $port_num port]
            if {[string length $port] == 1 } {
                set port "0$port"
            } else {
                set port $port
            }

            if {[string match "*CONFIG_VLANS_PORT$port\_MODE_TRUNK*" $line]} {

                append new_config "#CONFIG_VLANS_PORT$port\_MODE_TRUNK is not set\n"
                printLog "($port_num) #CONFIG_VLANS_PORT$port\_MODE_TRUNK is not set"
                set is_trunk_param_exists   1
                continue
            }

            if {[string match "*CONFIG_VLANS_PORT$port\_VID*" $line]} {

                append new_config "#CONFIG_VLANS_PORT$port\_VID is not set\n"
                printLog "($port_num) #CONFIG_VLANS_PORT$port\_VID is not set"
                set is_vid_param_exists     1
                continue
            }

            append new_config "$line\n"
        }

        if {$is_trunk_param_exists != 1} {

            append new_config "#CONFIG_VLANS_PORT$port\_MODE_TRUNK is not set\n"
            printLog "($port_num) #CONFIG_VLANS_PORT$port\_MODE_TRUNK is not set"
        }

        if {$is_vid_param_exists != 1} {

            append new_config "#CONFIG_VLANS_PORT$port\_VID is not set\n"
            printLog "($port_num) #CONFIG_VLANS_PORT$port\_VID is not set"
        }


        #Write configuration to dot-config file
        set dotconfig_fd [open $dotconfig_file  "w+"]
        puts $dotconfig_fd $new_config
        close $dotconfig_fd

        delEmptyEof $dotconfig_file
    }
    ##copy the updated dot_config file to device
    doScp $dotconfig_file $username@$host:/wr/etc $password

    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_PTP_DISABLE_VLANID"]

#   sleep $::dut_config_delay

    return $result
    return 2
}


################################################################################
#                           DUT CHECK FUNCTIONS                                #
################################################################################

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp::callback_cli_check_state                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                 (e.g.,, exp 6)                               #
#  port_num                 : (Mandatory) port on which the PTP is enabled.    #
#                                  (e.g., fa0/0 ).                             #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g.,BC ).                                 #
#  port_state        : (Mandatory) port state                                  #
#                                  (e.g master or slave)                       #
#  OUTPUT PARAMETERS :                                                         #
#  buffer            : (mandatory) buffer used to store the value              #
#                               (e.g master or slave)                          #
#                                                                              #
#                                                                              #
#  RETURNS           : 1 on Success (Reture the state of the DUT port ).       #
#                                                                              #
#                                                                              #
#                      0 on Failure (Not return the state of the DUT port).    #
#                                                                              #
#                                                                              #
#  DEFINITION        : This function check the State of specific clock         #
#                          mode on the DUT.                                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::callback_cli_check_state\                            #
#                             -session_handler $session_handler\               #
#                             -port_num        $port_num\                      #
#                             -clock_mode      $clock_mode\                    #
#                             -port_state      $port_state                     #
#                             -buffer          $buffer                         #
#                                                                              #
#                                                                              #
################################################################################

proc  ptp::callback_cli_check_state {args} {

    array set param $args

    set buffer        $param(-buffer)
    set clock_mode    $param(-clock_mode)
    set port_num      $param(-port_num)
    set port_state    $param(-port_state)

    ## portState ###
    array set PORTSTATE {
        IDLE                "0"
        INITIALIZING        "1"
        FAULTY              "2"
        DISABLED            "3"
        LISTENING           "4"
        PRE_MASTER          "5"
        MASTER              "6"
        PASSIVE             "7"
        UNCALIBRATED        "8"
        SLAVE               "9"
        PRESENT             "100"
        S_LOCK              "101"
        M_LOCK              "102"
        LOCKED              "103"
        CALIBRATION         "104"
        CALIBRATED          "105"
        RESP_CALIB_REQ      "106"
        WR_LINK_ON          "107"
    }

    array set MAP {
        "0"                 "IDLE"
        "1"                 "INITIALIZING"
        "2"                 "FAULTY"
        "3"                 "DISABLED"
        "4"                 "LISTENING"
        "5"                 "PRE_MASTER"
        "6"                 "MASTER"
        "7"                 "PASSIVE"
        "8"                 "UNCALIBRATED"
        "9"                 "SLAVE"
        "100"               "PRESENT"
        "101"               "S_LOCK"
        "102"               "M_LOCK"
        "103"               "LOCKED"
        "104"               "CALIBRATION"
        "105"               "CALIBRATED"
        "106"               "RESP_CALIB_REQ"
        "107"               "WR_LINK_ON"
    }

    set state_expected   $PORTSTATE($port_state)

    set portDoestNotExist   0

    foreach line [split $buffer "\n"] {

        if {[regexp {\s+(state:)\s+([a-zA-Z0-9 ]+)} $line match a state_retrieved]} {
        }

        if {[regexp {(cfg.iface_name:)\s+([a-zA-Z0-9\"]+)} $line match dump port_retrieved]} {

            set port_retrieved [string trim $port_retrieved "\""]

            if {$port_num == $port_retrieved} {

                printLog "Port is matching (Exp->$port_num, Rcvd->$port_retrieved)"

                if {$state_expected == $state_retrieved} {
                    printLog "portState is matching (Exp->$MAP($state_expected)($state_expected), Rcvd->$MAP($state_retrieved)($state_retrieved))"
                    return 1
                } else {
                    printLog "portState is not matching (Exp->$MAP($state_expected)($state_expected), Rcvd->$MAP($state_retrieved)($state_retrieved))"
                    return 0
                }

            } else {
                printLog "Port is not matching (Exp->$port_num, Rcvd->$port_retrieved)"
                set portDoestNotExist   1
            }
        }

    }

    if {$portDoestNotExist == 1} {

        printLog "portState is matching (Exp->$port_state, Rcvd->DISABLED (port does not exist))"
        return 1
    }

    return 0
}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp::callback_cli_check_domain                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                 (e.g.,, exp 6)                               #
#  clock_mode        : (Mandatory) Clock mode                                  #
#                                  (e.g., BC ).                                #
#  domain_number     : (Mandatory) Domain Number.                              #
#                                  (e.g.,0,100 ).                              #
#  OUTPUT PARAMETERS :                                                         #
#  buffer            : (mandatory) buffer used to store the value              #
#                               (e.g master or slave)                          #
#                                                                              #
#                                                                              #
#  RETURNS           : 1 on Success (Reture the state of the DUT port ).       #
#                                                                              #
#                                                                              #
#                      0 on Failure (Not return the state of the DUT port).    #
#                                                                              #
#                                                                              #
#  DEFINITION        : This function check the domain number configured        #
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::callback_cli_check_domain\                           #
#                             -session_handler $session_handler\               #
#                             -clock_mode      $clock_mode\                    #
#                             -domain          $domain                         #
#                             -buffer          $buffer                         #
#                                                                              #
#                                                                              #
################################################################################

proc  ptp::callback_cli_check_domain {args} {

    array set param $args

    set buffer        $param(-buffer)
    set clock_mode    $param(-clock_mode)
    set domain        $param(-domain)

    #set result  [Send_cli_command \
                -session_handler        $session_handler \
                -cmd_type               "DUT_PTP_CHECK_DOMAIN_NUM"\
                -clock_mode             $clock_mode\
                -domain                 $domain]

    #return $result

    return 2
}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp::callback_cli_priority1                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                 (e.g.,, exp 6)                               #
#  clock_mode        : (Mandatory) Clock mode                                  #
#                                  (e.g., BC ).                                #
#  priority1         : (Mandatory) Priority1 value                             #
#                                  (e.g.,0,100 ).                              #
#  OUTPUT PARAMETERS :                                                         #
#  buffer            : (mandatory) buffer used to store the value              #
#                               (e.g master or slave)                          #
#                                                                              #
#                                                                              #
#  RETURNS           : 1 on Success (Reture the state of the DUT port ).       #
#                                                                              #
#                                                                              #
#                      0 on Failure (Not return the state of the DUT port).    #
#                                                                              #
#                                                                              #
#  DEFINITION        : This function check the priority1 value configured      #
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::callback_cli_priority1\                              #
#                             -session_handler $session_handler\               #
#                             -clock_mode      $clock_mode\                    #
#                             -priority1       $priority1                      #
#                             -buffer          $buffer                         #
#                                                                              #
#                                                                              #
################################################################################

proc  ptp::callback_cli_check_priority1 {args} {

    array set param $args

    set buffer        $param(-buffer)
    set clock_mode    $param(-clock_mode)
    set priority1     $param(-priority1)

    #set result  [Send_cli_command \
                -session_handler        $session_handler \
                -cmd_type               "DUT_PTP_CHECK_PRIORITY1_VALUE"\
                -clock_mode             $clock_mode\
                -priority1              $priority1]

    #return $result

    return 2
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp::callback_cli_priority2                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                 (e.g.,, exp 6)                               #
#  clock_mode        : (Mandatory) Clock mode                                  #
#                                  (e.g., BC ).                                #
#  priority2         : (Mandatory) Priority2 value                             #
#                                  (e.g.,0,100 ).                              #
#  OUTPUT PARAMETERS :                                                         #
#  buffer            : (mandatory) buffer used to store the value              #
#                               (e.g master or slave)                          #
#                                                                              #
#                                                                              #
#  RETURNS           : 1 on Success (Reture the state of the DUT port ).       #
#                                                                              #
#                                                                              #
#                      0 on Failure (Not return the state of the DUT port).    #
#                                                                              #
#                                                                              #
#  DEFINITION        : This function check the priority1 value configured      #
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp::callback_cli_priority1\                              #
#                             -session_handler $session_handler\               #
#                             -clock_mode      $clock_mode\                    #
#                             -priority2       $priority2                      #
#                             -buffer          $buffer                         #
#                                                                              #
#                                                                              #
################################################################################

proc  ptp::callback_cli_check_priority2 {args} {

    array set param $args

    set buffer        $param(-buffer)
    set clock_mode    $param(-clock_mode)
    set priority2     $param(-priority2)

    #set result  [Send_cli_command \
                -session_handler        $session_handler \
                -cmd_type               "DUT_PTP_CHECK_PRIORITY1_VALUE"\
                -clock_mode             $clock_mode\
                -priority2              $priority2]

    #return $result

    return 2
}


#############################################################################
#  PROCEDURE NAME    : ptp::callback_ipv4_remove_ip_interface               #
#                                                                           #
#  INPUT PARAMETERS  :                                                      #
#                                                                           #
#   session_handler  : (Mandatory) Session ID of the DUT session opened.    #
#                                                                           #
#   cmd_type         : (Mandatory) Command type.  Used by send_cli_command  #
#                                                                           #
#   interface        : (Mandatory) Location/Interface to be removed from    #
#                                  the DUT.                                 #
#                                  (e.g. ethernet 0/0)                      #
#                                                                           #
#   ip_address       : (Mandatory) IP address to be removed from the        #
#                                  specified interface.                     #
#                                  (e.g. 192.168.9.13)                      #
#                                                                           #
#   mask             : (Mandatory) Subnet Mask for the specified IPv4       #
#                                  address.                                 #
#                                  (e.g. 255.255.255.0)                     #
#                                                                           #
#   vlan_id          : (Mandatory) VLAN ID.                                 #
#                                                                           #
#  OUTPUT PARAMETERS :                                                      #
#                                                                           #
#                    : NONE                                                 #
#                                                                           #
#  RETURNS           : 1 on success (If the given interface is removed from #
#                                    DUT with the specified IP address and  #
#                                    network mask).                         #
#                                                                           #
#                      0 on failure (If the given interface could not be    #
#                                    removed from the DUT with the specified#
#                                    IP address and network mask).          #
#                                                                           #
#                      2 if the ATTEST has to call the command.             #
#                                                                           #
#############################################################################

proc ptp::callback_ipv4_remove_ip_interface {args} {

   array set param $args

   set session_handler $param(-session_handler)
   set cmd_type        $param(-cmd_type)
   set interface       $param(-interface)
   set ip_address      $param(-ip_address)
   set mask            $param(-mask)
   set vlan_id         $param(-vlan_id)

#set result  [Send_cli_command \
                       -session_handler        $session_handler \
                       -cmd_type               "DUT_PTP_REMOVE_IP_INTERFACE"\
                       -parameter1              $interface\
                       -parameter2              $ip_address\
                       -parameter3              $mask\
                       -parameter4              $vlan_id]    

#        return $result

   return 2
}
