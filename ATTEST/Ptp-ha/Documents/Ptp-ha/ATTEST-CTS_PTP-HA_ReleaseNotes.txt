ATTEST(TM) PTP-HA Version 1.1 Release 1.7
================================================
Copyright (c) 2018 - 2019 CERN. All rights reserved.

Date of release: 15-Apr-2019

A1. What's New?
===============

    None.


A2. Recent Changes
==================

    1) Below changes are implemented in PAG_004, PAG_005 and PAG_006 test cases,
         - Fixed errors while comparing negative values of meanDelay and
           delayAsymmetry with margin of error.
         - Implemented conversion of correctionField to signed nanoseconds.
         - Initialized egressLatency and ingressLatency values to -4 000 000 000 ns.
         - Removed comparison of meanDelay values.
    2) Resolved script error in PAG_005.
    3) Added 2 seconds delay in PEG_002 before verifying PASSIVE state.
    4) To help debugging, support added in PAG_005 to proceed test execution
       till last step by ignoring validation of intermediate steps.
    5) To help debugging, support added in PAG_005 to stop the test execution
       at any step.
    6) Made two supports for debugging (4) and (5) as configurable in CLI
       integration file. This can be used in PAG_002, PAG_003, PAG_004, PAG_005,
       PAG_006, PAG_007, PAG_008 and PAG_011.


A3. Dependent Packages
======================

    This release is compatible with the following packages

    ATTEST Framework      :  Attest 6.2R4.2 and higher versions
    ATTEST Common         :  Attest Common 2.0R4.3 and higher versions


A4. Known Issues in this Release
================================

    None.



ATTEST(TM) PTP-HA Version 1.1 Release 1.6
================================================
Copyright (c) 2018 - 2019 CERN. All rights reserved.

Date of release: 04-Feb-2019

A1. What's New?
===============

    None.


A2. Recent Changes
==================

    1) Added margin of error as new configurable parameter in protocol options.
    2) Implemented margin of error for the validation of meanLinkDelay and
       delayAsymmetry in PAG_004 and PAG_006.
    3) Changed limits of value range of scaledDelayCoefficient attribute in
       PCG_017.
    4) Corrected errors in comparison of difference value between SEET and
       device's timestamp in PAG_008.
    5) To help debugging, support added in PAG_006 to proceed test execution
       till last step by ignoring validation of intermediate steps.
    6) To help debugging, support added in PAG_006 to stop the test execution
       at any step.
    7) Made two supports for debugging (5) and (6) as configurable in CLI
       integration file. This can be used in PAG_004, PAG_006 and PAG_008.


A3. Dependent Packages
======================

    This release is compatible with the following packages

    ATTEST Framework      :  Attest 6.2R4.2 and higher versions
    ATTEST Common         :  Attest Common 2.0R4.3 and higher versions


A4. Known Issues in this Release
================================

    None.



ATTEST(TM) PTP-HA Version 1.1 Release 1.5
================================================
Copyright (c) 2018 - 2019 CERN. All rights reserved.

Date of release: 23-Jan-2019

A1. What's New?
===============

    None.


A2. Recent Changes
==================

    1) Added steps in PAG_004, 005 and 008 test cases to check the lowest
       possible absolute value of currrentDS.offsetFromMaster to ensure that
       the time between DUT and TEE is synchronized.
    2) Modified the initial configuration of timestampCorrectionPortDS parameters
       in PAG_003, 005, 006, 007, 010 and 011 test cases to have values 0.
    3) Changed default value of currentDS.offsetFromMaster in UI to 100000000 nsec.
    4) To help debugging, support added in PAG_004, 005 and 008 test cases to
       proceed test execution till last step by ignoring validation of
       intermediate steps.
    5) To help debugging, support added in PAG_004, 005 and 008 test cases to
       stop the test execution at any step.


A3. Dependent Packages
======================

    This release is compatible with the following packages

    ATTEST Framework      :  Attest 6.2R4.2 and higher versions
    ATTEST Common         :  Attest Common 2.0R4.3 and higher versions


A4. Known Issues in this Release
================================

    None.



ATTEST(TM) PTP-HA Version 1.1 Release 1.4
================================================
Copyright (c) 2018 - 2019 CERN. All rights reserved.

Date of release: 18-Jan-2019

A1. What's New?
===============

    None.


A2. Recent Changes
==================

    1) Added support to pre and post operation in device while executing
       tests.
    2) Modified MHG_003 test case to reduce the timeouts used for ensuring
       change in domain number.
    3) Resolved the error in using floating value less than 1 in test cases
       PAG_004, 005 and 006.


A3. Dependent Packages
======================

    This release is compatible with the following packages

    ATTEST Framework      :  Attest 6.2R4.2 and higher versions
    ATTEST Common         :  Attest Common 2.0R4.3 and higher versions


A4. Known Issues in this Release
================================

    None.



ATTEST(TM) PTP-HA Version 1.1 Release 1.3
================================================
Copyright (c) 2018 CERN. All rights reserved.

Date of release: 06-Dec-2018

A1. What's New?
===============

    None.


A2. Recent Changes
==================

    1) Resolved incremental and synchronization of sequenceId in Sync and
       Follow_Up messages in PAG group.
    2) Resolved synchronization of sequenceId in Pdelay_Req and Pdelay_Resp
       messages in PEG_016, 017, 018, 019, 020 and 021.
    3) Modified the timestamps in Pdelay_Resp and Pdelay_Resp_Follow_up
       messages to the actual timestamp while sending these messages.


A3. Dependent Packages
======================

This release is compatible with the following packages

ATTEST Framework      :  Attest 6.2R4.1 and higher versions
ATTEST Common         :  Attest Common 2.0R4.1 and higher versions


A4. Known Issues in this Release
================================

    None.



ATTEST(TM) PTP-HA Version 1.1 Release 1.2
================================================
Copyright (c) 2018 CERN. All rights reserved.

Date of release: 30-Nov-2018

A1. What's New?
===============

    None.


A2. Recent Changes
==================

    1) Enabled auto-responder in all test cases of PAG group to send necessary
       response messages to every Delay_Req and Pdelay_Req messages received
       from DUT.
    2) Re-ordered steps in PAG_002 - PAG_006 and PAG_007 test cases by removing
       the steps for sending response messages, thus avoided mis-leading steps
       for the reception and transmission of delay request and response
       messages which happens in background.
    3) Fixed incorrect expectation of TLV length in MFG_002 and 004.
    4) Fixed the seconds value of responseOriginTimestamp in Pdelay_Resp_Follow_Up
       message being 0 always.
    5) Corrected pop-up message in OPV_001 to enable timestampsCorrectedTx.


A3. Dependent Packages
======================

This release is compatible with the following packages

ATTEST Framework      :  Attest 6.2R4.1 and higher versions
ATTEST Common         :  Attest Common 2.0R4.1 and higher versions


A4. Known Issues in this Release
================================

    1) In test cases of PAG group, all Sync and Follow_Up messages are sent
       with same sequenceId (i.e., without incrementing)



ATTEST(TM) PTP-HA Version 1.1 Release 1.1
================================================
Copyright (c) 2018 CERN. All rights reserved.

Date of release: 16-Nov-2018

A1. What's New?
===============

    None.


A2. Recent Changes
==================

    1) Resolved script error in PCG_006.
    2) Resolved difference in the timestamps taken while reception of Sync
       message and transmission of Delay_Req message in PAG_009.


A3. Dependent Packages
======================

This release is compatible with the following packages

ATTEST Framework      :  Attest 6.2R4.1 and higher versions
ATTEST Common         :  Attest Common 2.0R4.1 and higher versions


A4. Known Issues in this Release
================================

None



ATTEST(TM) PTP-HA Version 1.1 Release 1
================================================
Copyright (c) 2018 CERN. All rights reserved.

Date of release: 26-Oct-2018

A1. What's New?
===============

    1) Added 27 new test cases.


A2. Recent Changes
==================

    1) Added steps in test cases of PEG and SMG group to transmit L1Sync
       messages from TEE to ensure L1Sync state transition.


A3. Dependent Packages
======================

This release is compatible with the following packages

ATTEST Framework      :  Attest 6.2R4.1 and higher versions
ATTEST Common         :  Attest Common 2.0R4.1 and higher versions


A4. Known Issues in this Release
================================

None



ATTEST(TM) PTP-HA Version 1.0 Release 4.2
================================================
Copyright (c) 2018 CERN. All rights reserved.

Date of release: 28-Sep-2018

A1. What's New?
===============

None


A2. Recent Changes
==================

    1) Added support to transmit PTP messages at background when manual pop-up
       in on the screen.


A3. Dependent Packages
======================

This release is compatible with the following packages

ATTEST Framework      :  Attest 6.2R4.1 and higher versions
ATTEST Common         :  Attest Common 2.0R3.3 and higher versions


A4. Known Issues in this Release
================================

None



ATTEST(TM) PTP-HA Version 1.0 Release 4.1
================================================
Copyright (c) 2018 CERN. All rights reserved.

Date of release: 28-Sep-2018

A1. What's New?
===============

None


A2. Recent Changes
==================

    1) In PAG_002, added steps to make sure L1Sync state to be in L1_SYNC_UP,
       while taking meanDelay from DUT.
    2) In PCG_005, modified acceptable range of MinDelayRequestInterval to
       0 - 200%.
    3) In PCG_006, fixed errors in timeouts for receiving Announce messages.
    4) In PCG_009, added steps to receive L1Sync messages to ensure DUT is
       ready with applied configuration.
    5) In SMG_014, modified step to send L1Sync message periodically to
       maintain L1Sync state in L1_SYNC_UP.
    6) In SMG_021, fixed error in L1Sync message which is not allowing state
       L1Sync state transition from IDLE to LINK_ALIVE.


A3. Dependent Packages
======================

This release is compatible with the following packages

ATTEST Framework      :  Attest 6.2R4.1 and higher versions
ATTEST Common         :  Attest Common 2.0R3.3 and higher versions


A4. Known Issues in this Release
================================

None



ATTEST(TM) PTP-HA Version 1.0 Release 4
================================================
Copyright (c) 2018 CERN. All rights reserved.

Date of release: 17-Sep-2018

A1. What's New?
===============

None


A2. Recent Changes
==================

    1) Resolved incorrect validation of fields (source MAC address and IP
       checksum)in MFG_003.
    2) Added additional wait for receiving Delay_Req message in IDP_003.
    3) Added wait for completing BMCA in PAG_002 and 004.
    4) Resolved infinite waiting of tests during BMCA in test cases in PCG,
       PEG and SMG groups.
    5) Corrected the procedure call to verify the configuration status of
       portDS.masterOnly and defaultDS.slaveOnly in PCG_001.
    6) Changed the verification of subsequent step to optional if the
       configuration of current step fails in PCG_002.
    7) Improved the description in headers of PCG_014, 015, 016, 017 and
       test cases in PAG group.
    8) Capture buffer of WRPTP messages is handled properly in PCG_ 003,
       004, 005, 006, 007, 008, SMG_021 and SMG_022.
    9) Corrected fields in Announce messages sent from TEE in PCG_012, 013,
       SMG_10, 011 and 014.


A3. Dependent Packages
======================

This release is compatible with the following packages

ATTEST Framework      :  Attest 6.2R4.1 and higher versions
ATTEST Common         :  Attest Common 2.0R3.3 and higher versions


A4. Known Issues in this Release
================================

None



ATTEST(TM) PTP-HA Version 1.0 Release 3
================================================
Copyright (c) 2018 CERN. All rights reserved.

Date of release: 03-Jul-2018

A1. What's New?
===============

None


A2. Recent Changes
==================

    1) Resolved timing issues by increasing timeoout to receive Delay_Request
       and pDelay_Req messages.
    2) Resolved incorrect passing of MHG_002.
    3) Resolved incorrect validation of IP address in test cases in SMG group.
    4) Resolved timing issues in test cases in PCG group.


A3. Dependent Packages
======================

This release is compatible with the following packages

ATTEST Framework      :  Attest 6.2R4.1 and higher versions
ATTEST Common         :  Attest Common 2.0R3.3 and higher versions


A4. Known Issues in this Release
================================

None



ATTEST(TM) PTP-HA Version 1.0 Release 2
================================================
Copyright (c) 2018 CERN. All rights reserved.

Date of release: 25-Jun-2018

A1. What's New?
===============

None


A2. Recent Changes
==================

1) Fixed script errors in test scripts while executing tests in CLI-SSH mode.


A3. Dependent Packages
======================

This release is compatible with the following packages

ATTEST Framework      :  Attest 6.2R4.1 and higher versions
ATTEST Common         :  Attest Common 2.0R3.3 and higher versions


A4. Known Issues in this Release
================================

None


ATTEST(TM) PTP-HA Version 1.0 Release 1
================================================
Copyright (c) 2018 CERN. All rights reserved.

Date of release: 08-Jun-2018

A1. What's New?
===============

1) This version has 54 test cases.
2) This release supports CLI, SNMP and Manual based DUT configuration.


A2. Dependent Packages
======================

This release is compatible with the following packages

ATTEST Framework      :  Attest 6.2R4.1 and higher versions
ATTEST Common         :  Attest Common 2.0R3.3 and higher versions


A3. Known Issues in this Release
================================

None



Releases - History
==================

-----------------------------------------------------------------------------------
    Version            Release Type                                 Release Date
-----------------------------------------------------------------------------------
    1.1R1.7           Patch Release                                 15-Apr-2019
    1.1R1.6           Patch Release                                 04-Feb-2019
    1.1R1.5           Patch Release                                 23-Jan-2019
    1.1R1.4           Patch Release                                 18-Jan-2019
    1.1R1.3           Patch Release                                 06-Dec-2018
    1.1R1.2           Patch Release                                 30-Nov-2018
    1.1R1.1           Patch Release                                 16-Nov-2018
    1.1R1             Minor Release                                 26-Oct-2018
    1.0R4.2           Patch Release                                 28-Sep-2018
    1.0R4.1           Patch Release                                 28-Sep-2018
    1.0R4             Maintenance Release                           17-Sep-2018
    1.0R3             Maintenance Release                           03-Jul-2018
    1.0R2             Maintenance Release                           25-Jun-2018
    1.0R1             Initial Release                               08-Jun-2018
-----------------------------------------------------------------------------------
