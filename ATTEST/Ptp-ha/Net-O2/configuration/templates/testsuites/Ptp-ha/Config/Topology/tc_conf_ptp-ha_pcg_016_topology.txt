`
Topology


                  TEE                                 DUT
            +--------------+                     +-------------+
            |              |                     |             |
            |              |                     |             |
            |              |                     |             |
            |              |                     |             |
            |              | T1               P1 |             |
            |       BC     |---------------------|    OC/BC    |
            |              |                     |             |
            |              |                     |             |
            |              |                     |             |
            |              |                     |             |
            +--------------+                     +-------------+


    Legends:

        TEE     : Test Execution Engine
        DUT     : Device Under Test
        OC      : Ordinary Clock
        BC      : Boundary Clock
        T1      : Port 1 at TEE
        P1      : Port 1 at DUT


