 Test Case         : tc_conf_ptp-ha_pcg_001                                  
 Test Case Version : 1.4                                                     
 Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    
 Module Name       : PTP-HA Configuration Group (PCG)                        
                                                                             
 Title             : Default initialization values for attributes - High     
                     Accuracy Delay Request-Response mechanism               
                                                                             
 Purpose           : To verify that a PTP enabled device stores all          
                     attributes with default initialization values for High  
                     Accuracy Delay Request-Respone mechanism. Checking that 
                     the following attributes have correct default values.   
                     1) defaultDS.domainNumber = 0                           
                     2) portDS.logAnnounceInterval = 1                       
                     3) portDS.logSyncInterval = 0                           
                     4) portDS.logMinDelayReqInterval = 0                    
                     5) portDS.announceReceiptTimeout = 3                    
                     6) defaultDS.priority1 = 128                            
                     7) defaultDS.priority2 = 128                            
                     8) defaultDS.slaveOnly = FALSE                          
                     9) defaultDS.SdoId = 0x000                              
                     10) L1SyncBasicPortDS.L1SyncEnabled = TRUE              
                     11) L1SyncBasicPortDS.txCoherencyIsRequired = TRUE      
                     12) L1SyncBasicPortDS.rxCoherencyIsRequired = TRUE      
                     13) L1SyncBasicPortDS.congruencyIsRequired = TRUE       
                     14) L1SyncBasicPortDS.optParametersConfigured = FALSE   
                     15) L1SyncBasicPortDS.logL1SyncInterval = 0             
                     16) L1SyncBasicPortDS.L1SyncReceiptTimeout = 3          
                     17) defaultDS.externalPortConfigurationEnabled = FALSE  
                     18) timestampCorrectionPortDS.egressLatency = Default is
                         zero unless specified otherwise by implementation.  
                     19) timestampCorrectionPortDS.ingressLatency = Default  
                         is zero unless specified otherwise by               
                         implementation.                                     
                     20) asymmetryCorrectionPortDS.constantAsymmetry =       
                         Default is zero unless specified otherwise by       
                         implementation.                                     
                     21) asymmetryCorrectionPortDS.scaledDelayCoefficient =  
                         Default is zero unless specified otherwise by       
                         implementation.                                     
                     22) asymmetryCorrectionPortDS.enable = TRUE             
                     23) portDS.masterOnly = FALSE                           
                     Note: The default values of these attributes can be     
                     changed through ATTEST GUI (Go to Configuration Manager 
                     and select desired configuration, go to                 
                     Protocol Options > PTP-HA > PTP-HA Attributes).         
                                                                             
 Reference         : P1588/D1.3, February 2018 V3.01 Clause J.5.2 Page 412,  
                     Table 150 Page 413                                      
                                                                             
 Conformance Type  : MUST                                                    
                                                                             
