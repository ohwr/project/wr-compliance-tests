`
Test Setup & Values:


            +-------------+                     +-------------+
            |             |                     |             |
            |             |                     |             |
            |             |                     |             |
            |             | T1               P1 |             |
            |     TEE     |---------------------|     DUT     |
            |             |                     |             |
            |             |                     |             |
            |             |                     |             |
            |             |                     |             |
            +-------------+                     +-------------+


    +-------------------------------------------------------------------------+
    | Global Parameters                                                       |
    +----------------------------------+--------------------------------------+
    | PTP                              | Enable                               |
    +----------------------------------+--------------------------------------+
    | Version                          | 2                                    |
    +----------------------------------+--------------------------------------+
    | PTP Device Type                  | "Ordinary Clock" or "Boundary Clock" |
    |                                  | (Choose from Protocol Options)       |
    +----------------------------------+--------------------------------------+
    | Network Transport Protocol       | "IEEE 802.3" or "UDP/IPv4"           |
    |                                  | (Choose from Protocol Options)       |
    +----------------------------------+--------------------------------------+
    | Communication Model              | "Multicast" or "Unicast"             |
    |                                  | (Choose from Protocol Options)       |
    +----------------------------------+--------------------------------------+
    | Clock Step                       | "One-Step" or "Two-Step"             |
    |                                  | (Choose from Protocol Options)       |
    +----------------------------------+--------------------------------------+
    | VLAN Encapsulation Support       | "true" or "false"                    |
    |                                  | (Choose from Protocol Options)       |
    +----------------------------------+--------------------------------------+

    +-------------------------------------------------------------------------+
    | Port Parameters                                                         |
    +----------------------------------+--------------------------------------+
    | PTP                              | Enable                               |
    +----------------------------------+--------------------------------------+
    | Delay Mechanism                  | Delay Request-Response               |
    +----------------------------------+--------------------------------------+

    +-------------------------------------------------------------------------+
    | PTP Attributes and Default Values                                       |
    +---------------------------------------------------+---------------------+
    | defaultDS.domainNumber                            | 0                   |
    +---------------------------------------------------+---------------------+
    | defaultDS.priority1                               | 128                 |
    +---------------------------------------------------+---------------------+
    | defaultDS.priority2                               | 128                 |
    +---------------------------------------------------+---------------------+
    | defaultDS.externalPortConfigurationEnabled        | FALSE               |
    +---------------------------------------------------+---------------------+
    | portDS.logAnnounceInterval                        | 1                   |
    +---------------------------------------------------+---------------------+
    | portDS.logSyncInterval                            | 0                   |
    +---------------------------------------------------+---------------------+
    | portDS.logMinDelayReqInterval                     | 0                   |
    +---------------------------------------------------+---------------------+
    | portDS.announceReceiptTimeout                     | 3                   |
    +---------------------------------------------------+---------------------+
    | portDS.masterOnly                                 | FALSE               |
    +---------------------------------------------------+---------------------+
    | L1SyncBasicPortDS.L1SyncEnabled                   | TRUE                |
    +---------------------------------------------------+---------------------+
    | L1SyncBasicPortDS.txCoherencyIsRequired           | TRUE                |
    +---------------------------------------------------+---------------------+
    | L1SyncBasicPortDS.rxCoherencyIsRequired           | TRUE                |
    +---------------------------------------------------+---------------------+
    | L1SyncBasicPortDS.congruencyIsRequired            | TRUE                |
    +---------------------------------------------------+---------------------+
    | L1SyncBasicPortDS.optParametersConfigured         | FALSE               |
    +---------------------------------------------------+---------------------+
    | L1SyncBasicPortDS.logL1SyncInterval               | 0                   |
    +---------------------------------------------------+---------------------+
    | L1SyncBasicPortDS.L1SyncReceiptTimeout            | 3                   |
    +---------------------------------------------------+---------------------+
    | timestampCorrectionPortDS.egressLatency           | Default value       |
    +---------------------------------------------------+---------------------+
    | timestampCorrectionPortDS.ingressLatency          | Default value       |
    +---------------------------------------------------+---------------------+
    | asymmetryCorrectionPortDS.constantAsymmetry       | Default value       |
    +---------------------------------------------------+---------------------+
    | asymmetryCorrectionPortDS.scaledDelayCoefficient  | Default value       |
    +---------------------------------------------------+---------------------+
    | asymmetryCorrectionPortDS.enable                  | TRUE                |
    +---------------------------------------------------+---------------------+

NOTE: Default values can be modified in DUT and the same value should be given
      to ATTEST through Protocol Options


Test Setup Procedure

    1. Enable DUT's port P1.

    2. Enable PTP on port P1.

    3. Set PTP version as 2.

    4. Enable PTP globally with device type as Boundary/Ordinary clock.

    5. Configure clock mode as One-step/Two-step.

    6. Configure delay mechanism as Delay Request-Response delay mechanism.

    7. Configure default values for attributes in default data set as given in
       above table.

    8. Configure default values for attributes in port data set as given in
       above table.

    9. Configure default values for attributes in L1 Sync basic port data set
       as given in above table.

   10. Configure default values for attributes in timestamp correction port
       data set as given in above table.

   11. Configure default values for attributes in asymmetry correction port
       data set as given in above table.


