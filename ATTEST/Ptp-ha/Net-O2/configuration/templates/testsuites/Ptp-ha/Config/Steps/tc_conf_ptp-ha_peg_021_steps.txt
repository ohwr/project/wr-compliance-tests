                                                                             
 Procedure         :                                                         
                                                                             
 (Initial Part)                                                              
                                                                             
 Step 1 : Initialization of DUT                                              
     i.   Enable DUT's port P1.                                              
    ii.   Enable PTP on port P1.                                             
   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   
    iv.   Configure clock mode as One-step/Two-step.                         
     v.   Configure delaymechanism as Peer to peer.                          
    vi.   Configure default values for Priority1, Priority2,                 
          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   
          logMinDelayReqInterval.                                            
   vii.   Enable L1SYNC on DUT's port P1.                                    
  viii.   Configure default values for L1SyncInterval and                    
          L1SyncReceiptTimeout.                                              
    ix.   Disable L1SynOptParams on DUT.                                     
     x.   Enable asymmetryCorrectionPortDS.enable.                           
    xi.   Configure default values for timestampCorrectionPortDS.            
          egressLatency, timestampCorrectionPortDS.egressLatency,            
          asymmetryCorrectionPortDS.constantAsymmetry and                    
          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  
   xii.   Disable defaultDS.externalPortConfigurationEnabled on port P1.     
                                                                             
 Step 2 : Initialization of TEE                                              
     i.   Add port T1 at TEE.                                                
                                                                             
 (Part 1)                                                                    
                                                                             
 Step 3 : Observe that DUT transmits ANNOUNCE message on port P1 with        
          following parameters.                                              
                                                                             
               PTP Header                                                    
                   Message Type  = 0x0B                                      
                   Domain Number = DN1                                       
                   Priority      = X                                         
                                                                             
 Step 4 : Send periodic ANNOUNCE messages on port T1 with following          
          parameters.                                                        
                                                                             
               PTP Header                                                    
                   Message Type  = 0x0B                                      
                   Domain Number = DN1                                       
                   Priority      = X-1                                       
                                                                             
 Step 5 : Wait for 6s for completing BMCA.                                   
                                                                             
 Step 6 : Send periodic PTP SIGNALING message with L1 Sync TLV on the port P1
          with following parameters:                                         
                                                                             
              PTP Header                                                     
                  Message Type  = 0xC                                        
                  Domain Number = DN1                                        
              L1_SYNC TLV                                                    
                  TLV_TYPE      = 0x8001                                     
                  TCR           = 1                                          
                  RCR           = 1                                          
                  CR            = 1                                          
                  ITC           = 1                                          
                  IRC           = 1                                          
                  IC            = 1                                          
                                                                             
 Step 7 : Observe that the port status of P1 in DUT is in SLAVE state.       
                                                                             
 Step 8 : Enable auto responder to respond every Pdelay_Req messages received
          on port T1.                                                        
                                                                             
 Step 9 : Enable defaultDS.externalPortConfigurationEnabled on port P1.      
                                                                             
 Step 10: Configure externalPortConfigurationPortDS.desiredState as MASTER.  
                                                                             
 Step 11: Observe that the port status of P1 in DUT is in MASTER state.      
                                                                             
 Step 12: Observe that DUT transmits PDELAY_REQ message on the port P1 with  
          following parameters :                                             
                                                                             
              PTP Header                                                     
                  Message Type              = 0x02                           
                  Domain Number             = DN1                            
                  Sequence ID               = D                              
                  Source Port Identity      = E                              
                                                                             
 Step 12a:Send PDELAY_RESP to every PDELAY_REQ on the port P1 and with       
          following parameters:                                              
                                                                             
              PTP Header                                                     
                  Source Mac               = SRC1                            
                  Clock ID                 = CLK1                            
                  Message Type             = 0x03                            
                  Domain Number            = DN1                             
                  Sequence Id              = D                               
                  Requesting Port Identity = E                               
                                                                             
 Step 12a:Send PDELAY_RESP to every PDELAY_REQ on the port P1 and with       
          following parameters:                                              
                                                                             
              PTP Header                                                     
                  Source Mac               = SRC2                            
                  Clock ID                 = CLK2                            
                  Message Type             = 0x03                            
                  Domain Number            = DN1                             
                  Sequence Id              = D                               
                  Requesting Port Identity = E                               
                                                                             
 Step 13 : Verify that the port status of P1 in DUT continues to be in MASTER
           state.                                                            
                                                                             
 Step 14 : Disable defaultDS.externalPortConfigurationEnabled on port P1 in  
           DUT.                                                              
                                                                             
 Step 15: Observe that DUT transmits PDELAY_REQ message on the port P1 with  
          following parameters :                                             
                                                                             
              PTP Header                                                     
                  Message Type              = 0x02                           
                  Domain Number             = DN1                            
                  Sequence ID               = D                              
                  Source Port Identity      = E                              
                                                                             
 Step 15a:Send PDELAY_RESP to every PDELAY_REQ on the port P1 and with       
          following parameters:                                              
                                                                             
              PTP Header                                                     
                  Source Mac               = SRC1                            
                  Clock ID                 = CLK1                            
                  Message Type             = 0x03                            
                  Domain Number            = DN1                             
                  Sequence Id              = D                               
                  Requesting Port Identity = E                               
                                                                             
 Step 15a:Send PDELAY_RESP to every PDELAY_REQ on the port P1 and with       
          following parameters:                                              
                                                                             
              PTP Header                                                     
                  Source Mac               = SRC2                            
                  Clock ID                 = CLK2                            
                  Message Type             = 0x03                            
                  Domain Number            = DN1                             
                  Sequence Id              = D                               
                  Requesting Port Identity = E                               
                                                                             
 Step 16 : Verify that the port status of P1 in DUT is in FAULTY state.      
                                                                             
