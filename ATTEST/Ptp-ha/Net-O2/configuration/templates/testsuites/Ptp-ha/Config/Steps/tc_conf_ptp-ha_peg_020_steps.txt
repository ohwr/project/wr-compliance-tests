                                                                             
 Procedure         :                                                         
                                                                             
 (Initial Part)                                                              
                                                                             
 Step 1 : Initialization of DUT                                              
     i.   Enable DUT's port P1.                                              
    ii.   Enable PTP on port P1.                                             
   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   
    iv.   Configure clock mode as One-step/Two-step.                         
     v.   Configure delaymechanism as Peer to peer.                          
    vi.   Configure default values for Priority1, Priority2,                 
          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   
          logMinDelayReqInterval.                                            
   vii.   Enable L1SYNC on DUT's port P1.                                    
  viii.   Configure default values for L1SyncInterval and                    
          L1SyncReceiptTimeout.                                              
    ix.   Disable L1SynOptParams on DUT.                                     
     x.   Enable asymmetryCorrectionPortDS.enable.                           
    xi.   Configure default values for timestampCorrectionPortDS.            
          egressLatency, timestampCorrectionPortDS.egressLatency,            
          asymmetryCorrectionPortDS.constantAsymmetry and                    
          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  
   xii.   Disable defaultDS.externalPortConfigurationEnabled on port P1.     
                                                                             
 Step 2 : Initialization of TEE                                              
     i.   Add port T1 at TEE.                                                
                                                                             
 (Part 1)                                                                    
                                                                             
 Step 3 : Observe that DUT transmits ANNOUNCE message on port P1 with        
          following parameters.                                              
                                                                             
               PTP Header                                                    
                   Message Type  = 0x0B                                      
                   Domain Number = DN1                                       
                   Priority      = X                                         
                                                                             
 Step 4 : Send periodic ANNOUNCE messages on port T1 with following          
          parameters.                                                        
                                                                             
               PTP Header                                                    
                   Message Type  = 0x0B                                      
                   Domain Number = DN1                                       
                   Priority      = X+1                                       
                                                                             
 Step 5 : Observe that the port status of P1 in DUT is in MASTER state.      
                                                                             
 Step 6 : Enable auto responder to respond every Pdelay_Req messages received
          on port T1.                                                        
                                                                             
 Step 7 : Enable defaultDS.externalPortConfigurationEnabled on port P1.      
                                                                             
 Step 8 : Configure externalPortConfigurationPortDS.desiredState as          
          PRE_MASTER.                                                        
                                                                             
 Step 9 : Observe that the port status of P1 in DUT is in PRE-MASTER         
          state.                                                             
                                                                             
 Step 10: Observe that DUT transmits PDELAY_REQ message on the port P1 with  
          following parameters :                                             
                                                                             
              PTP Header                                                     
                  Message Type              = 0x02                           
                  Domain Number             = DN1                            
                  Sequence ID               = D                              
                  Source Port Identity      = E                              
                                                                             
 Step 10a:Send PDELAY_RESP to every PDELAY_REQ on the port P1 and with       
          following parameters:                                              
                                                                             
              PTP Header                                                     
                  Source Mac               = SRC1                            
                  Clock ID                 = CLK1                            
                  Message Type             = 0x03                            
                  Domain Number            = DN1                             
                  Sequence Id              = D                               
                  Requesting Port Identity = E                               
                                                                             
 Step 10a:Send PDELAY_RESP to every PDELAY_REQ on the port P1 and with       
          following parameters:                                              
                                                                             
              PTP Header                                                     
                  Source Mac               = SRC2                            
                  Clock ID                 = CLK2                            
                  Message Type             = 0x03                            
                  Domain Number            = DN1                             
                  Sequence Id              = D                               
                  Requesting Port Identity = E                               
                                                                             
 Step 11 : Verify that the port status of P1 in DUT continues to be in       
           PRE_MASTER state.                                                 
                                                                             
 Step 12 : Disable defaultDS.externalPortConfigurationEnabled on port P1 in  
           DUT.                                                              
                                                                             
 Step 13: Observe that DUT transmits PDELAY_REQ message on the port P1 with  
          following parameters :                                             
                                                                             
              PTP Header                                                     
                  Message Type              = 0x02                           
                  Domain Number             = DN1                            
                  Sequence ID               = D                              
                  Source Port Identity      = E                              
                                                                             
 Step 13a:Send PDELAY_RESP to every PDELAY_REQ on the port P1 and with       
          following parameters:                                              
                                                                             
              PTP Header                                                     
                  Source Mac               = SRC1                            
                  Clock ID                 = CLK1                            
                  Message Type             = 0x03                            
                  Domain Number            = DN1                             
                  Sequence Id              = D                               
                  Requesting Port Identity = E                               
                                                                             
 Step 13a:Send PDELAY_RESP to every PDELAY_REQ on the port P1 and with       
          following parameters:                                              
                                                                             
              PTP Header                                                     
                  Source Mac               = SRC2                            
                  Clock ID                 = CLK2                            
                  Message Type             = 0x03                            
                  Domain Number            = DN1                             
                  Sequence Id              = D                               
                  Requesting Port Identity = E                               
                                                                             
 Step 14 : Verify that the port status of P1 in DUT is in FAULTY state.      
                                                                             
