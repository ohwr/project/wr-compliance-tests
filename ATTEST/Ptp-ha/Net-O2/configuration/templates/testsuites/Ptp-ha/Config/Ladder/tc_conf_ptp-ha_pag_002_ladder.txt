                                                                             
 Ladder Diagram    :                                                         
                                                                             
          TEE                                                   DUT          
           |                                                     |           
           |                                        <Enable PTP> | P1        
           |                       <Enable PTP with BC/OC clock> |           
           |                    <Clock mode - one-step/two-step> |           
           |                     <Configure logAnnounceInterval, | P1        
           |                             announceReceiptTimeout> |           
           |                         <Configure logSyncInterval> | P1        
           |                  <Configure logMinDelayReqInterval> | P1        
           |                    <Configure Priority1, Priority2> | P1        
           |                                     <Enable L1SYNC> | P1        
           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        
           |                     <Disable L1SynOptParams option> | P1        
           |           <Enable asymmetryCorrectionPortDS.enable> | P1        
           | <Configure timestampCorrectionPortDS.egressLatency, | P1        
           |           timestampCorrectionPortDS.ingressLatency, |           
           |        asymmetryCorrectionPortDS.constantAsymmetry, |           
           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           
           |                                                     |           
           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           
           |                                 DN = DN1, PRI1 = X] |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           | ANNOUNCE [MSG_TYPE = 0x0B,                          |           
           | DN = DN1, PRI1 = X-1]                               |           
        T1 |-->>-------------------------------------------------| P1        
           |                                                     |           
        T1 | <Enable auto responder to Delay_Req messages>       | P1        
           |                                                     |           
           | <Send Sync/ Sync & Follow-up messages               |           
           | based on clock step>                                |           
           |                                                     |           
           |          < Wait for 6s to complete BMCA >           |           
           |                                                     |           
           |                      PTP SIGNALING with L1 Sync TLV |           
           |                          [MSG_TYPE = 0xC, DN = DN1, |           
           |                                  TLV_TYPE = 0x8001] |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           | PTP SIGNALING with L1 Sync TLV                      |           
           | [MSG_TYPE = 0xC, DN = DN1,                          |           
           | TLV_TYPE = 0x8001, TCR = 1,                         |           
           | RCR = 1, CR = 1, ITC = 1,                           |           
           | IRC = 1, IC = 1]                                    |           
        T1 |-->>-------------------------------------------------| P1        
           |                                                     |           
           |             <Check L1SYNC port status - L1_SYNC_UP> | P1        
           |                                                     |           
           | <Check non-zero absolute value of currentDS.        |           
           |             offsetFromMaster is lowest as possible> |           
           |                                                     |           
           |                     <Get currentDS.meanDelay (MD1)> |           
           |                                                     |           
           |  <Configure timestampCorrectionPortDS.egressLatency |           
           |                                 value to 2^(32+16)> |           
           |                                                     |           
           |               DELAY_REQ [MSG_TYPE = 0x01, DN = DN1] |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           | <Check non-zero absolute value of currentDS.        |           
           |             offsetFromMaster is lowest as possible> |           
           |                                                     |           
           |                     <Get currentDS.meanDelay (MD2)> |           
           |                                                     |           
           |                       MD2 < MD1                     |           
           |                                                     |           
           |  <Configure timestampCorrectionPortDS.egressLatency |           
           |                                value to -2^(32+16)> |           
           |                                                     |           
           |               DELAY_REQ [MSG_TYPE = 0x01, DN = DN1] |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           | <Check non-zero absolute value of currentDS.        |           
           |             offsetFromMaster is lowest as possible> |           
           |                                                     |           
           |                     <Get currentDS.meanDelay (MD3)> |           
           |                                                     |           
           |                       MD3 > MD1                     |           
           |                                                     |           
                                                                             
 Legends           :                                                         
                                                                             
     MSG_TYPE  = Message Type                                                
     DN        = Domain Number                                               
     PRT1      = priority1                                                   
     MD1 - MD3 = currentDS.meanDelay                                         
                                                                             
 NOTE :                                                                      
                                                                             
  1. This objective is verified using the High Accuracy Delay Request-       
     Response Default PTP Profile                                            
                                                                             
