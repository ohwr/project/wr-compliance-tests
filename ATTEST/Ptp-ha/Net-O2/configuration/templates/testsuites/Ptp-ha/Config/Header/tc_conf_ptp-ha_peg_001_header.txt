 Test Case         : tc_conf_ptp-ha_peg_001                                  
 Test Case Version : 1.3                                                     
 Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    
 Module Name       : PTP ExternalPortConfiguration Group (PEG)               
                                                                             
 Title             : externalPortConfigurationPortDS.desiredState - Port     
                     state remains in MASTER state                           
                                                                             
 Purpose           : To verify that a PTP enabled device remains in MASTER   
                     state if externalPortConfigurationPortDS.desiredState is
                     set to MASTER.                                          
                                                                             
 Reference         : P1588/D1.3, February 2018 V3.01 Clause 17.6.5.4         
                     Page 356                                                
                                                                             
 Conformance Type  : SHALL                                                   
                                                                             
