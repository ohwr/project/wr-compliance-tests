 Test Case         : tc_conf_ptp-ha_pag_011                                  
 Test Case Version : 1.2                                                     
 Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    
 Module Name       : PTP Accuracy Group (PAG)                                
                                                                             
 Title             : Ingress timestamp in Pdelay_Resp message                
                                                                             
 Purpose           : To verify that a PTP enabled device generates Ingress   
                     timestamp in Pdelay_Resp (event) messages from          
                     timestampCorrectionPortDS.ingressLatency when using     
                     Peer to Peer Delay mechanism.                           
                                                                             
 Reference         : IEEE 1588-2017 Clause 16.7.1 Page 301, Clause 7.3.4.2   
                     Page 68, Clause 8.2.16.2 Page 128                       
                                                                             
 Conformance Type  : SHALL                                                   
                                                                             
