 Test Case         : tc_conf_ptp-ha_pcg_017                                  
 Test Case Version : 1.4                                                     
 Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    
 Module Name       : PTP-HA Configuration Group (PCG)                        
                                                                             
 Title             : asymmetryCorrectionPortDS.scaledDelayCoefficient        
                                                                             
 Purpose           : To verify that a PTP enabled device supports to         
                     configure asymmetryCorrectionPortDS.                    
                     scaledDelayCoefficient (allowable range: -2^62 to 2^62).
                                                                             
 Reference         : P1588/D1.3, February 2018 V3.01 Clause 7.4.2 Page 75,   
                     Clause 8.2.17.3 Page 130, Clause J.5.3 Table 150        
                     Page 413                                                
                                                                             
 Conformance Type  : SHALL                                                   
                                                                             
