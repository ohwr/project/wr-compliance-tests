                                                                             
 Ladder Diagram    :                                                         
                                                                             
          TEE                                                   DUT          
           |                                                     |           
           |                                        <Enable PTP> | P1        
           |                       <Enable PTP with BC/OC clock> |           
           |                    <Clock mode - one-step/two-step> |           
           |                     <Configure logAnnounceInterval, | P1        
           |                             announceReceiptTimeout> |           
           |                         <Configure logSyncInterval> | P1        
           |                  <Configure logMinDelayReqInterval> | P1        
           |                <Configure Priority1 (X), Priority2> | P1        
           |                                     <Enable L1SYNC> | P1        
           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        
           |                     <Disable L1SynOptParams option> | P1        
           |           <Enable asymmetryCorrectionPortDS.enable> | P1        
           | <Configure timestampCorrectionPortDS.egressLatency, | P1        
           |           timestampCorrectionPortDS.ingressLatency, |           
           |        asymmetryCorrectionPortDS.constantAsymmetry, |           
           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           
           |                                            <Enable  |           
           |        <defaultDS.externalPortConfigurationEnabled> |           
           |                                                     |           
           |                                          <Configure |           
           | externalPortConfigurationPortDS.desiredState=       |           
           |                                           LISTENING>|           
           |                                                     |           
           |                     <Check Port Status = LISTENING> |           
           |                                                     |           
           | ANNOUNCE [MSG_TYPE = 0x0B, DN = DN1,                |           
           |           SEQ_ID = A, PRI1 = X+1]                   |           
        T1 |-->>-------------------------------------------------| P1        
           |                                                     |           
           |          < Wait for 6s to complete BMCA >           |           
           |                                                     |           
           |                     <Check Port Status = LISTENING> |           
           |                                                     |           
           |                                           <Disable  |           
           |        <defaultDS.externalPortConfigurationEnabled> |           
           |                                                     |           
           |  <Wait for the expiry of AnnounceReceipt timeout>   |           
           |                                                     |           
           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           
           |                     DN = DN1, SEQ_ID = Y, PRI1 = X] |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           | ANNOUNCE [MSG_TYPE = 0x0B, DN = DN1,                |           
           |           SEQ_ID = A, PRI1 = X+1]                   |           
        T1 |-->>-------------------------------------------------| P1        
           |                                                     |           
           |          < Wait for 6s to complete BMCA >           |           
           |                                                     |           
           |                          <Check port state = MASTER>| P1        
           |                                                     |           
                                                                             
 NOTE :                                                                      
                                                                             
  1. This objective is verified using the High Accuracy Delay Request-       
     Response Default PTP Profile                                            
                                                                             
