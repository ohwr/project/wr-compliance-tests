                                                                             
 Procedure         :                                                         
                                                                             
 (Initial Part)                                                              
                                                                             
 Step 1 : Initialization of DUT                                              
     i.   Enable DUT's port P1.                                              
    ii.   Enable PTP on port P1.                                             
   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   
    iv.   Configure clock mode as One-step/Two-step.                         
     v.   Configure default values for Priority1, Priority2,                 
          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   
          logMinDelayReqInterval.                                            
    vi.   Enable L1SYNC on DUT's port P1.                                    
   vii.   Configure default values for L1SyncInterval and                    
          L1SyncReceiptTimeout.                                              
  viii.   Disable L1SynOptParams on DUT.                                     
    ix.   Enable asymmetryCorrectionPortDS.enable.                           
     x.   Configure default values for timestampCorrectionPortDS.            
          egressLatency, timestampCorrectionPortDS.ingressLatency,           
          asymmetryCorrectionPortDS.constantAsymmetry and                    
          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  
                                                                             
 Step 2 : Initialization of TEE                                              
     i.   Add port T1 at TEE.                                                
                                                                             
 (Part 1)                                                                    
                                                                             
 Step 3 : Observe that DUT transmits ANNOUNCE message on the port P1 with    
          following parameters.                                              
                                                                             
              PTP Header                                                     
                  Message Type  = 0x0B                                       
                  Domain Number = DN1                                        
                  Sequence ID   = Y                                          
                  Priority1     = X                                          
                                                                             
 Step 4 : Send periodic ANNOUNCE message with Priority1 value incremented    
          from the Priority1 value of received Announce message on port T1   
          with following parameters.                                         
                                                                             
              PTP Header                                                     
                  Message Type  = 0x0B                                       
                  Domain Number = DN1                                        
                  Sequence ID   = A                                          
                  Priority1     = X+1                                        
                                                                             
 Step 5 : Observe that the DUT's L1SYNC port status P1 is in IDLE state.     
                                                                             
 Step 6 : Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV  
          on the port P1 with following parameters:                          
                                                                             
              PTP Header                                                     
                  Message Type  = 0xC                                        
                  Domain Number = DN1                                        
              L1_SYNC TLV                                                    
                  TLV_TYPE      = 0x8001                                     
                                                                             
 Step 7 : Send PTP SIGNALING message with L1 Sync TLV on the port P1 with    
          following parameters:                                              
                                                                             
              PTP Header                                                     
                  Message Type  = 0xC                                        
                  Domain Number = DN1                                        
              L1_SYNC TLV                                                    
                  TLV_TYPE      = 0x8001                                     
                  TCR           = 0                                          
                  RCR           = 0                                          
                  CR            = 0                                          
                  ITC           = 0                                          
                  IRC           = 0                                          
                  IC            = 0                                          
                                                                             
 Step 8 : Wait for the expiry of L1SyncReceiptTimeout.                       
                                                                             
 Step 9 : Observe that the DUT's L1SYNC port status P1 is in LINK_ALIVE state
                                                                             
 Step 10: Send PTP SIGNALING message with L1 Sync TLV on the port P1 with    
          following parameters:                                              
                                                                             
              PTP Header                                                     
                  Message Type  = 0xC                                        
                  Domain Number = 128                                        
              L1_SYNC TLV                                                    
                  TLV_TYPE      = 0x8001                                     
                  TCR           = 0                                          
                  RCR           = 0                                          
                  CR            = 0                                          
                  ITC           = 0                                          
                  IRC           = 0                                          
                  IC            = 0                                          
                                                                             
 Step 11: Wait for the expiry of twice the L1SyncReceiptTimeout.             
                                                                             
 Step 12: Verify that the DUT's L1SYNC port status P1 is in IDLE state.      
                                                                             
 Step 13: Configure domain number as 127 on Port P1 in DUT.                  
                                                                             
 Step 14: Send PTP SIGNALING message with L1 Sync TLV on the port P1 with    
          following parameters:                                              
                                                                             
              PTP Header                                                     
                  Message Type  = 0xC                                        
                  Domain Number = 127                                        
              L1_SYNC TLV                                                    
                  TLV_TYPE      = 0x8001                                     
                  TCR           = 0                                          
                  RCR           = 0                                          
                  CR            = 0                                          
                  ITC           = 0                                          
                  IRC           = 0                                          
                  IC            = 0                                          
                                                                             
 Step 15: Wait for the expiry of L1SyncReceiptTimeout.                       
                                                                             
 Step 16: Observe that the DUT's L1SYNC port status P1 is in LINK_ALIVE state
                                                                             
 Step 17: Send PTP SIGNALING message with L1 Sync TLV on the port P1 with    
          following parameters:                                              
                                                                             
              PTP Header                                                     
                  Message Type  = 0xC                                        
                  Domain Number = 128                                        
              L1_SYNC TLV                                                    
                  TLV_TYPE      = 0x8001                                     
                  TCR           = 0                                          
                  RCR           = 0                                          
                  CR            = 0                                          
                  ITC           = 0                                          
                  IRC           = 0                                          
                  IC            = 0                                          
                                                                             
 Step 18: Wait for the expiry of twice the L1SyncReceiptTimeout.             
                                                                             
 Step 19 : Verify that the DUT's L1SYNC port status P1 is in IDLE state.     
                                                                             
