 Test Case         : tc_conf_ptp-ha_pcg_012                                  
 Test Case Version : 1.3                                                     
 Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    
 Module Name       : PTP-HA Configuration Group (PCG)                        
                                                                             
 Title             : Port State: masterOnly - remains in master state        
                                                                             
 Purpose           : To verify that a PTP enabled device does not allow      
                     PTP port state to enter into SLAVE state when the PTP   
                     port state is configured as masterOnly.                 
                                                                             
 Reference         : P1588/D1.3, February 2018 V3.01 Section 9.2.2.2 Page 139
                                                                             
 Conformance Type  : SHALL                                                   
                                                                             
