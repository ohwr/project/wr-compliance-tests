 Test Case         : tc_conf_ptp-ha_smg_009                                  
 Test Case Version : 1.3                                                     
 Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    
 Module Name       : PTP-HA State Machine Group (SMG)                        
                                                                             
 Title             : L1 SYNC port changes from CONFIG_MATCH to L1_SYNC_UP    
                     [DUT port state - Slave]                                
                                                                             
 Purpose           : To verify that L1 SYNC port changes its state from      
                     CONFIG_MATCH to L1_SYNC_UP when communicating L1 sync   
                     ports has the relationship required by configuration    
                     in place. [DUT port state - Slave].                     
                                                                             
 Reference         : IEEE 1588-2017 Clause O.7.2 Table 157 Page 449,         
                     Clause O.7.3 Figure 70 Page 450                         
                                                                             
 Conformance Type  : MUST                                                    
                                                                             
