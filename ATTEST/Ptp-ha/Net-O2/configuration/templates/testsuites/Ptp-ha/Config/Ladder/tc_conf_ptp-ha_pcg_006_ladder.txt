                                                                             
 Ladder Diagram    :                                                         
                                                                             
          TEE                                                   DUT          
           |                                                     |           
           |                                        <Enable PTP> | P1        
           |                       <Enable PTP with BC/OC clock> |           
           |                    <Clock mode - one-step/two-step> |           
           |                     <Configure logAnnounceInterval, | P1        
           |                             announceReceiptTimeout> |           
           |                         <Configure logSyncInterval> | P1        
           |                  <Configure logMinDelayReqInterval> | P1        
           |                    <Configure Priority1, Priority2> | P1        
           |                                     <Enable L1SYNC> | P1        
           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        
           |                     <Disable L1SynOptParams option> | P1        
           |           <Enable asymmetryCorrectionPortDS.enable> | P1        
           | <Configure timestampCorrectionPortDS.egressLatency, | P1        
           |           timestampCorrectionPortDS.ingressLatency, |           
           |        asymmetryCorrectionPortDS.constantAsymmetry, |           
           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           
           |                                                     |           
           |               ANNOUNCE [MSG_TYPE = 0x0B, DN = DN1,  |           
           |                             ANN_INT = 1, PRIOR = Z] |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           |  ANNOUNCE [MSG_TYPE = 0x0B,                         |           
           |  DN = DN1, ANN_INT = 1, PRIOR = Z-1]                |           
        T1 |-->>-------------------------------------------------| P1        
           |                                                     |           
           | <Send Sync/ Sync & Follow-up messages               |           
           | based on clock step>                                |           
           |                                                     |           
           |          < Wait for 6s to complete BMCA >           |           
           |                                                     |           
           |  ANNOUNCE [MSG_TYPE = 0x0B,                         |           
           |  DN = DN1, ANN_INT = 1, PRIOR = Z-1]                |           
        T1 |---->>----XX                                         | P1        
           |                                                     |           
           |               ANNOUNCE [MSG_TYPE = 0x0B, DN = DN1,  |           
           |                                        ANN_INT = 1] |           
        T1 |                     XX--------------------------<<--| P1        
           |                                         (within 6s) |           
           |                                                     |           
           |               ANNOUNCE [MSG_TYPE = 0x0B, DN = DN1,  |           
           |                                        ANN_INT = 1] |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           |              <Configure announceReceiptTimeout = 2> |           
           |                                                     |           
           |  ANNOUNCE [MSG_TYPE = 0x0B,                         |           
           |  DN = DN1, ANN_INT = 1, PRIOR = Z-1]                |           
        T1 |-->>-------------------------------------------------| P1        
           |                                                     |           
           | <Send Sync/ Sync & Follow-up messages               |           
           | based on clock step>                                |           
           |                                                     |           
           |          < Wait for 6s to complete BMCA >           |           
           |                                                     |           
           |  ANNOUNCE [MSG_TYPE = 0x0B,                         |           
           |  DN = DN1, ANN_INT = 1, PRIOR = Z-1]                |           
        T1 |---->>----XX                                         | P1        
           |                                                     |           
           |               ANNOUNCE [MSG_TYPE = 0x0B, DN = DN1,  |           
           |                                        ANN_INT = 1] |           
        T1 |                     XX--------------------------<<--| P1        
           |                                         (within 4s) |           
           |                                                     |           
           |               ANNOUNCE [MSG_TYPE = 0x0B, DN = DN1,  |           
           |                                        ANN_INT = 1] |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           |             <Configure announceReceiptTimeout = 10> |           
           |                                                     |           
           |  ANNOUNCE [MSG_TYPE = 0x0B,                         |           
           |  DN = DN1, ANN_INT = 1, PRIOR = Z-1]                |           
        T1 |-->>-------------------------------------------------| P1        
           |                                                     |           
           | <Send Sync/ Sync & Follow-up messages               |           
           | based on clock step>                                |           
           |                                                     |           
           |          < Wait for 6s to complete BMCA >           |           
           |                                                     |           
           |  ANNOUNCE [MSG_TYPE = 0x0B,                         |           
           |  DN = DN1, ANN_INT = 1, PRIOR = Z-1]                |           
        T1 |---->>----XX                                         | P1        
           |                                                     |           
           |               ANNOUNCE [MSG_TYPE = 0x0B, DN = DN1,  |           
           |                                        ANN_INT = 1] |           
        T1 |                     XX--------------------------<<--| P1        
           |                                         within 20s) |           
           |                                                     |           
           |               ANNOUNCE [MSG_TYPE = 0x0B, DN = DN1,  |           
           |                                        ANN_INT = 1] |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           |              <Configure announceReceiptTimeout = 3> |           
           |                                                     |           
           |  ANNOUNCE [MSG_TYPE = 0x0B,                         |           
           |  DN = DN1, ANN_INT = 1, PRIOR = Z-1]                |           
        T1 |-->>-------------------------------------------------| P1        
           |                                                     |           
           | <Send Sync/ Sync & Follow-up messages               |           
           | based on clock step>                                |           
           |                                                     |           
           |          < Wait for 6s to complete BMCA >           |           
           |                                                     |           
           |  ANNOUNCE [MSG_TYPE = 0x0B,                         |           
           |  DN = DN1, ANN_INT = 1, PRIOR = Z-1]                |           
        T1 |---->>----XX                                         | P1        
           |                                                     |           
           |               ANNOUNCE [MSG_TYPE = 0x0B, DN = DN1,  |           
           |                                        ANN_INT = 1] |           
        T1 |                     XX--------------------------<<--| P1        
           |                                          within 6s) |           
           |                                                     |           
           |               ANNOUNCE [MSG_TYPE = 0x0B, DN = DN1,  |           
           |                                        ANN_INT = 1] |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
                                                                             
 Legends           :                                                         
                                                                             
     MSG_TYPE  = Message Type                                                
     DN        = Domain Number                                               
     BC        = Boundary Clock                                              
     OC        = Ordinary Clock                                              
                                                                             
 NOTE :                                                                      
                                                                             
  1. This objective is verified using the High Accuracy Delay Request-       
     Response Default PTP Profile                                            
                                                                             
  2. Timeout = (2 ^ logAnnounceInterval) * announceReceiptTimeout            
                                                                             
