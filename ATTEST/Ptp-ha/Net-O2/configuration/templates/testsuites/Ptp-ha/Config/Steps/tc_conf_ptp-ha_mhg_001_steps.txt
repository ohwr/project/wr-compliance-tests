                                                                             
 Procedure         :                                                         
                                                                             
 (Initial Part)                                                              
                                                                             
 Step 1 : Initialization of DUT                                              
     i.   Enable DUT's ports P1 and P2.                                      
    ii.   Enable PTP on ports P1 and P2.                                     
   iii.   Enable PTP globally with device type as Boundary clock.            
    iv.   Configure clock mode as One-step/Two-step.                         
     v.   Configure default values for Priority1, Priority2,                 
          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   
          logMinDelayReqInterval.                                            
    vi.   Enable L1SYNC on DUT's ports P1 and P2.                            
   vii.   Configure default values for L1SyncInterval and                    
          L1SyncReceiptTimeout.                                              
  viii.   Disable L1SynOptParams on DUT.                                     
    ix.   Enable asymmetryCorrectionPortDS.enable.                           
     x.   Configure default values for timestampCorrectionPortDS.            
          egressLatency, timestampCorrectionPortDS.ingressLatency,           
          asymmetryCorrectionPortDS.constantAsymmetry and                    
          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  
                                                                             
 Step 2 : Initialization of TEE                                              
     i.   Add ports T1 and T2 at TEE.                                        
                                                                             
 (Part 1)                                                                    
                                                                             
 Step 3 : Observe that DUT transmits ANNOUNCE message on the port P1 with    
          following parameters.                                              
                                                                             
              PTP Header                                                     
                  Message Type  = 0x0B                                       
                  Domain Number = DN1                                        
                                                                             
 Step 4 : Observe that DUT transmits ANNOUNCE message on the port P2 with    
          following parameters.                                              
                                                                             
              PTP Header                                                     
                  Message Type  = 0x0B                                       
                  Domain Number = DN1                                        
                                                                             
 Step 5 : Send periodic L1SYNC Signaling message on port T1 with             
          following parameters.                                              
                                                                             
              PTP Header                                                     
                  Message Type  = 0x0C                                       
                  Domain Number = DN1                                        
                  Destination IP= 224.0.0.107                                
                                                                             
 Step 6 : Verify that DUT transmits only its own L1 Sync Signaling messages  
          and does not re-transmit L1 Sync Signaling messages from TEE port  
          T1. Observe that DUT does not transmits L1 Sync Signaling messages 
          with non-forwardable address and source IP is not equal to TEE port
          IP (T2) on port P2.                                                
                                                                             
 Step 7 : Disable L1SYNC on port P2.                                         
                                                                             
 Step 8 : Send periodic L1SYNC Signaling message on port T1 with             
          following parameters.                                              
                                                                             
              PTP Header                                                     
                  Message Type  = 0x0C                                       
                  Domain Number = DN1                                        
                  Destination IP= 224.0.0.107                                
                                                                             
 Step 9 : Verify that DUT does not transmit any L1 Sync Signaling messages on
          port P2.                                                           
                                                                             
