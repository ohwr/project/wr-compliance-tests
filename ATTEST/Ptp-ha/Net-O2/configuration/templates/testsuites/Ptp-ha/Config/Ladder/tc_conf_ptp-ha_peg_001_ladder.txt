                                                                             
 Ladder Diagram    :                                                         
                                                                             
                                                                             
          TEE                                                   DUT          
           |                                                     |           
           |                                        <Enable PTP> | P1        
           |                       <Enable PTP with BC/OC clock> |           
           |                    <Clock mode - one-step/two-step> |           
           |                     <Configure logAnnounceInterval, | P1        
           |                             announceReceiptTimeout> |           
           |                         <Configure logSyncInterval> | P1        
           |                  <Configure logMinDelayReqInterval> | P1        
           |                    <Configure Priority1, Priority2> | P1        
           |                                     <Enable L1SYNC> | P1        
           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        
           |                     <Disable L1SynOptParams option> | P1        
           |           <Enable asymmetryCorrectionPortDS.enable> | P1        
           | <Configure timestampCorrectionPortDS.egressLatency, | P1        
           |           timestampCorrectionPortDS.ingressLatency, |           
           |        asymmetryCorrectionPortDS.constantAsymmetry, |           
           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           
           |                                            <Enable  |           
           |         defaultDS.externalPortConfigurationEnabled> | P1        
           |                                                     |           
           |                                          <Configure |           
           | externalPortConfigurationPortDS.desiredState=MASTER>|           
           |                                                     |           
           |                        <Check Port Status = MASTER> | P1        
           |                                                     |           
           |                         ANNOUNCE [MSG_TYPE = 0x0B,  |           
           |                                    PRI=X, DN = DN1] |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           | ANNOUNCE [MSG_TYPE = 0x0B,                          |           
           |  PRI=X-1, DN = DN1]                                 |           
        T1 |-->>-------------------------------------------------| P1        
           |                                                     |           
           |          < Wait for 6s to complete BMCA >           |           
           |                                                     |           
           | PTP SIGNALING with L1 Sync TLV                      |           
           | [MSG_TYPE = 0xC, DN = DN1,                          |           
           | TLV_TYPE = 0x8001, TCR = 1,                         |           
           | RCR = 1, CR = 1, ITC = 1,                           |           
           | IRC = 1, IC = 1]                                    |           
        T1 |-->>-------------------------------------------------| P1        
           |                                                     |           
           |                        <Check Port Status = MASTER> | P1        
           |                                                     |           
           |                                            <Disable |           
           |         defaultDS.externalPortConfigurationEnabled> | P1        
           |                                                     |           
           |          < Wait for 6s to complete BMCA >           |           
           |                                                     |           
           |                        <Check Port Status = SLAVE>  | P1        
           |                                                     |           
                                                                             
 NOTE :                                                                      
                                                                             
  1. This objective is verified using the High Accuracy Delay Request-       
     Response Default PTP Profile                                            
                                                                             
