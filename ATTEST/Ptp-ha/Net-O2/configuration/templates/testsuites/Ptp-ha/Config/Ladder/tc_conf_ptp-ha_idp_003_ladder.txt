                                                                             
 Ladder Diagram    :                                                         
                                                                             
                                                                             
          TEE                                                   DUT          
        [master]                                              [slave]        
           |                                                     |           
           |                                        <Enable PTP> | P1        
           |                       <Enable PTP with BC/OC clock> |           
           |                    <Clock mode - one-step/two-step> |           
           |                     <Configure logAnnounceInterval, | P1        
           |                             announceReceiptTimeout> |           
           |                         <Configure logSyncInterval> | P1        
           |                  <Configure logMinDelayReqInterval> | P1        
           |                    <Configure Priority1, Priority2> | P1        
           |                                     <Enable L1SYNC> | P1        
           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        
           |                     <Disable L1SynOptParams option> | P1        
           |           <Enable asymmetryCorrectionPortDS.enable> | P1        
           | <Configure timestampCorrectionPortDS.egressLatency, | P1        
           |           timestampCorrectionPortDS.ingressLatency, |           
           |        asymmetryCorrectionPortDS.constantAsymmetry, |           
           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           
           |                                                     |           
           |                           ANNOUNCE [MSG_TYPE = 0x0B,|           
           |                                    PRI=X, DN = DN1] |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           | ANNOUNCE [MSG_TYPE = 0x0B,                          |           
           | PRI=X-1, DN = DN1]                                  |           
        T1 |-->>-------------------------------------------------| P1        
           |                                                     |           
           | <Send Sync/ Sync & Follow-up messages               |           
           | based on clock step>                                |           
           |                                                     |           
           |    PTP SIGNALLING with L1 Sync TLV [MSG_TYPE = 0x0C,|           
           |                                           DN = DN1] |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           |                          DELAY_REQ [MSG_TYPE = 0x01,|           
           |                               DN = DN1, SEQ_ID = D] |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           | DELAY_RESP [MSG_TYPE = 0x09,                        |           
           | DN = DN1, SEQ_ID = D]                               |           
        T1 |-->>-------------------------------------------------| P1        
           |                                                     |           
           |                   <Check L1SYNC port status - IDLE> | P1        
           |                                                     |           
                                                                             
 Legends           :                                                         
                                                                             
     MSG_TYPE  = Message Type                                                
     DN        = Domain Number                                               
     PRI       = Priority                                                    
     BC        = Boundary Clock                                              
     OC        = Ordinary Clock                                              
     SEQ_ID    = Sequence ID                                                 
                                                                             
 NOTE :                                                                      
                                                                             
  1. This objective is verified using the High Accuracy Delay Request-       
     Response Default PTP Profile                                            
                                                                             
