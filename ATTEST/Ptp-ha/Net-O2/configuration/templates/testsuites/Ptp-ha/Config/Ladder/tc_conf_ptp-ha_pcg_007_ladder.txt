                                                                             
 Ladder Diagram    :                                                         
                                                                             
          TEE                                                   DUT          
           |                                                     |           
           |                                        <Enable PTP> | P1        
           |                       <Enable PTP with BC/OC clock> |           
           |                    <Clock mode - one-step/two-step> |           
           |                        <Set delay mechanism as P2P> |           
           |                     <Configure logAnnounceInterval, | P1        
           |                             announceReceiptTimeout> |           
           |                         <Configure logSyncInterval> | P1        
           |                  <Configure logMinDelayReqInterval> | P1        
           |                    <Configure Priority1, Priority2> | P1        
           |                                     <Enable L1SYNC> | P1        
           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        
           |                     <Disable L1SynOptParams option> | P1        
           |           <Enable asymmetryCorrectionPortDS.enable> | P1        
           | <Configure timestampCorrectionPortDS.egressLatency, | P1        
           |           timestampCorrectionPortDS.ingressLatency, |           
           |        asymmetryCorrectionPortDS.constantAsymmetry, |           
           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           
           |                                                     |           
           |              PDELAY_REQ [MSG_TYPE = 0x02, DN = DN1, |           
           |                                    Timestamp = Ta]  |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           |              PDELAY_REQ [MSG_TYPE = 0x02, DN = DN1, |           
           |                                    Timestamp = Tb]  |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           |              PDELAY_REQ [MSG_TYPE = 0x02, DN = DN1, |           
           |                                    Timestamp = Tc]  |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           |       <Check ((Tb - Ta) + (Tc - Tb))/2 = 0.9s - 2s> |           
           |                                                     |           
           |              <Configure PdelayRequestInterval = 2 > |           
           |                                                     |           
           |              PDELAY_REQ [MSG_TYPE = 0x02, DN = DN1, |           
           |                                    Timestamp = Ta]  |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           |              PDELAY_REQ [MSG_TYPE = 0x02, DN = DN1, |           
           |                                    Timestamp = Tb]  |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           |              PDELAY_REQ [MSG_TYPE = 0x02, DN = DN1, |           
           |                                    Timestamp = Tc]  |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           |       <Check ((Tb - Ta) + (Tc - Tb))/2 = 3.6s - 8s> |           
           |                                                     |           
           |              <Configure PdelayRequestInterval = 5 > |           
           |                                                     |           
           |              PDELAY_REQ [MSG_TYPE = 0x02, DN = DN1, |           
           |                                    Timestamp = Ta]  |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           |              PDELAY_REQ [MSG_TYPE = 0x02, DN = DN1, |           
           |                                    Timestamp = Tb]  |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           |              PDELAY_REQ [MSG_TYPE = 0x02, DN = DN1, |           
           |                                    Timestamp = Tc]  |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           |     <Check ((Tb - Ta) + (Tc - Tb))/2 = 28.8s - 64s> |           
           |                                                     |           
           |               <Configure PdelayRequestInterval = 0> |           
           |                                                     |           
           |              PDELAY_REQ [MSG_TYPE = 0x02, DN = DN1, |           
           |                                    Timestamp = Ta]  |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           |              PDELAY_REQ [MSG_TYPE = 0x02, DN = DN1, |           
           |                                    Timestamp = Tb]  |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           |              PDELAY_REQ [MSG_TYPE = 0x02, DN = DN1, |           
           |                                    Timestamp = Tc]  |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           |       <Check ((Tb - Ta) + (Tc - Tb))/2 = 0.9s - 2s> |           
           |                                                     |           
                                                                             
 Legends           :                                                         
                                                                             
     MSG_TYPE  = Message Type                                                
     DN        = Domain Number                                               
                                                                             
 NOTE :                                                                      
                                                                             
  1. This objective is verified using the High Accuracy Peer to Peer Default 
     PTP Profile                                                             
                                                                             
