                                                                             
 Ladder Diagram    :                                                         
                                                                             
                                                                             
          TEE                                                   DUT          
           |                                                     |           
           |                                        <Enable PTP> | P1        
           |                       <Enable PTP with BC/OC clock> |           
           |                    <Clock mode - one-step/two-step> |           
           |                        <Set delay mechanism as P2P> |           
           |                     <Configure logAnnounceInterval, | P1        
           |                             announceReceiptTimeout> |           
           |                         <Configure logSyncInterval> | P1        
           |                  <Configure logMinDelayReqInterval> | P1        
           |                    <Configure Priority1, Priority2> | P1        
           |                                     <Enable L1SYNC> | P1        
           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        
           |                     <Disable L1SynOptParams option> | P1        
           |           <Enable asymmetryCorrectionPortDS.enable> | P1        
           | <Configure timestampCorrectionPortDS.egressLatency, | P1        
           |           timestampCorrectionPortDS.ingressLatency, |           
           |        asymmetryCorrectionPortDS.constantAsymmetry, |           
           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           
           |                                            <Disable |           
           |         defaultDS.externalPortConfigurationEnabled> | P1        
           |                                                     |           
           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           
           |                                    PRI=X, DN = DN1] |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           | ANNOUNCE [MSG_TYPE = 0x0B,                          |           
           |  PRI=X+1, DN = DN1]                                 |           
        T1 |-->>-------------------------------------------------| P1        
           |                                                     |           
           |                        <Check Port Status = MASTER> | P1        
           |                                                     |           
        T1 | <Enable auto responder to Pdelay_Req messages>      | P1        
           |                                                     |           
           |                                            <Enable  |           
           |         defaultDS.externalPortConfigurationEnabled> | P1        
           |                                                     |           
           |                                          <Configure |           
           |       externalPortConfigurationPortDS.desiredState= |           
           |                                       UNCALIBRATED> | P1        
           |                                                     |           
           |                  <Check Port Status = UNCALIBRATED> | P1        
           |                                                     |           
           |              PDELAY_REQ [MSG_TYPE = 0x02, DN = DN1, |           
           |                         SRC_PRT_ID = E, SEQ_ID = D] |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           | PDELAY_RESP [SRC_MAC = SRC1,CLK_ID = CLK1,          |           
           | REQ_PRT_ID = E, SEQ_ID = D                          |           
           | MSG_TYPE = 0x03, DN = DN1]                          |           
        T1 |-->>-------------------------------------------------| P1        
           |                                                     |           
           | PDELAY_RESP [SRC_MAC = SRC2,CLK_ID = CLK2,          |           
           | REQ_PRT_ID = E, SEQ_ID = D                          |           
           | MSG_TYPE = 0x03, DN = DN1]                          |           
        T1 |-->>-------------------------------------------------| P1        
           |                                                     |           
           |                  <Check Port Status = UNCALIBRATED> | P1        
           |                                                     |           
           |                                           <Disable  |           
           |         defaultDS.externalPortConfigurationEnabled> | P1        
           |                                                     |           
           |              PDELAY_REQ [MSG_TYPE = 0x02, DN = DN1, |           
           |                         SRC_PRT_ID = E, SEQ_ID = D] |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           | PDELAY_RESP [SRC_MAC = SRC1,CLK_ID = CLK1,          |           
           | REQ_PRT_ID = E, SEQ_ID = D                          |           
           | MSG_TYPE = 0x03, DN = DN1]                          |           
        T1 |-->>-------------------------------------------------| P1        
           |                                                     |           
           | PDELAY_RESP [SRC_MAC = SRC2,CLK_ID = CLK2,          |           
           | REQ_PRT_ID = E, SEQ_ID = D                          |           
           | MSG_TYPE = 0x03, DN = DN1]                          |           
        T1 |-->>-------------------------------------------------| P1        
           |                                                     |           
           |                        <Check Port Status = FAULTY> | P1        
           |                                                     |           
                                                                             
 Legends           :                                                         
                                                                             
     MSG_TYPE  = Message Type                                                
     DN        = Domain Number                                               
     PRI       = Priority                                                    
     P2P       = Peer to Peer                                                
     SEQ_ID    = Sequence ID                                                 
     SRC_MAC   = Source mac address                                          
     CLK_ID    = Clock Identity                                              
     SRC_PRT_ID= Source Port Identity                                        
     REQ_PRT_ID= Requesting Port Identity                                    
                                                                             
 NOTE :                                                                      
                                                                             
  1. This objective is verified using the High Accuracy Peer to Peer Default 
     PTP Profile                                                             
                                                                             
