                                                                             
 Procedure         :                                                         
                                                                             
 (Initial Part)                                                              
                                                                             
 Step 1 : Initialization of DUT                                              
     i.   Enable DUT's port P1.                                              
    ii.   Enable PTP on port P1.                                             
   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   
    iv.   Configure clock mode as One-step/Two-step.                         
     v.   Configure default values for Priority1 (X), Priority2,             
          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   
          logMinDelayReqInterval.                                            
    vi.   Enable L1SYNC on DUT's port P1.                                    
   vii.   Configure default values for L1SyncInterval and                    
          L1SyncReceiptTimeout.                                              
  viii.   Disable L1SynOptParams on DUT.                                     
    ix.   Enable asymmetryCorrectionPortDS.enable.                           
     x.   Configure default values for timestampCorrectionPortDS.            
          egressLatency, timestampCorrectionPortDS.egressLatency,            
          asymmetryCorrectionPortDS.constantAsymmetry and                    
          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  
    xi.   Enable defaultDS.externalPortConfigurationEnabled on port P1.      
                                                                             
 Step 2 : Initialization of TEE                                              
     i.   Add port T1 at TEE.                                                
                                                                             
 (Part 1)                                                                    
                                                                             
 Step 3 : Configure externalPortConfigurationPortDS.desiredState as LISTENING
                                                                             
 Step 4 : Observe that the port status of P1 in DUT is in LISTENING state.   
                                                                             
 Step 5 : Send periodic ANNOUNCE message on port T1 with following parameters
                                                                             
              PTP Header                                                     
                  Message Type  = 0x0B                                       
                  Domain Number = DN1                                        
                  Sequence ID   = A                                          
                  Priority1     = X+1                                        
                                                                             
 Step 6: Wait for 6s for completing BMCA.                                    
                                                                             
 Step 7 : Verify that the port status of P1 in DUT is in LISTENING state.    
                                                                             
 Step 8 : Disable defaultDS.externalPortConfigurationEnabled on port P1 in   
          DUT.                                                               
                                                                             
 Step 9 : Wait for the expiry of AnnounceReceiptTimeout.                     
                                                                             
 Step 10: Observe that DUT transmits ANNOUNCE message on the port P1 with    
          following parameters.                                              
                                                                             
              PTP Header                                                     
                  Message Type  = 0x0B                                       
                  Domain Number = DN1                                        
                  Sequence ID   = Y                                          
                  Priority1     = X                                          
                                                                             
 Step 11: Send periodic ANNOUNCE message on port T1 with following parameters
                                                                             
              PTP Header                                                     
                  Message Type  = 0x0B                                       
                  Domain Number = DN1                                        
                  Sequence ID   = A                                          
                  Priority1     = X+1                                        
                                                                             
 Step 12: Wait for 6s for completing BMCA.                                   
                                                                             
 Step 13: Verify that the port status of P1 in DUT is in MASTER state.       
                                                                             
