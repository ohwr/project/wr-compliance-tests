                                                                             
 Ladder Diagram    :                                                         
                                                                             
          TEE                                                  DUT           
        [slave]                                              [master]        
           |                                                     |           
           |                                        <Enable PTP> | P1        
           |                       <Enable PTP with BC/OC clock> |           
           |                    <Clock mode - one-step/two-step> |           
           |                     <Configure logAnnounceInterval, | P1        
           |                             announceReceiptTimeout> |           
           |                         <Configure logSyncInterval> | P1        
           |                  <Configure logMinDelayReqInterval> | P1        
           |                    <Configure Priority1, Priority2> | P1        
           |                                     <Enable L1SYNC> | P1        
           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        
           |                     <Disable L1SynOptParams option> | P1        
           |           <Enable asymmetryCorrectionPortDS.enable> | P1        
           | <Configure timestampCorrectionPortDS.egressLatency, | P1        
           |           timestampCorrectionPortDS.ingressLatency, |           
           |        asymmetryCorrectionPortDS.constantAsymmetry, |           
           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           
           |                                                     |           
           |                   <Check L1SYNC port status - IDLE> | P1        
           |                                                     |           
           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           
           |                     DN = DN1, SEQ_ID = Y, PRI1 = X] |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           | ANNOUNCE [MSG_TYPE = 0x0B, DN = DN1,                |           
           | SEQ_ID = A, PRI1 = X+1]                             |           
        T1 |-->>-------------------------------------------------| P1        
           |                                                     |           
           |                      PTP SIGNALING with L1 Sync TLV |           
           |                          [MSG_TYPE = 0xC, DN = DN1, |           
           |                                  TLV_TYPE = 0x8001] |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
           | PTP SIGNALING with L1 Sync TLV                      |           
           | [MSG_TYPE = 0xC, DN = DN1,                          |           
           | TLV_TYPE = 0x8001, TCR = 0,                         |           
           | RCR = 0, CR = 0]                                    |           
        T1 |-->>-------------------------------------------------| P1        
           |                                                     |           
           |             <Check L1SYNC port status - LINK_ALIVE> | P1        
           |                                                     |           
           |                      PTP SIGNALING with L1 Sync TLV |           
           |                          [MSG_TYPE = 0xC, DN = DN1, |           
           |                                  TLV_TYPE = 0x8001] |           
        T1 |-------------------------------------------------<<--| P1        
           |                                                     |           
                                                                             
 Legends           :                                                         
                                                                             
     MSG_TYPE  = Message Type                                                
     DN        = Domain Number                                               
     BC        = Boundary Clock                                              
     OC        = Ordinary Clock                                              
     SEQ_ID    = Sequence ID                                                 
     PRI       = Priority                                                    
                                                                             
 NOTE :                                                                      
                                                                             
  1. This objective is verified using the High Accuracy Delay Request-       
     Response Default PTP Profile                                            
                                                                             
