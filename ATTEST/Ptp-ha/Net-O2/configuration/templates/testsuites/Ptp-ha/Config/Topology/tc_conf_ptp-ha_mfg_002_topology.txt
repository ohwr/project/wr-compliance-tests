`
Topology


                  TEE                                 DUT
            +--------------+                     +-------------+
            |              |                     |             |
            |              |                     |             |
            |              |                     |             |
            |              |                     |             |
            |       BC     | T1               P1 |    OC/BC    |
            |    (Slave)   |---------------------|   (Master)  |
            |              |                     |             |
            |              |                     |             |
            |              |                     |             |
            |              |                     |             |
            +--------------+                     +-------------+


    Legends:

        TEE     : Test Execution Engine
        DUT     : Device Under Test
        OC      : Ordinary Clock
        BC      : Boundary Clock
        T1      : Port 1 at TEE
        P1      : Port 1 at DUT

