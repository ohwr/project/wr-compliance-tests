 Test Case         : tc_conf_ptp-ha_peg_009                                  
 Test Case Version : 1.3                                                     
 Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    
 Module Name       : PTP ExternalPortConfiguration Group (PEG)               
                                                                             
 Title             : portDS.portState remains in slave state - expiry of     
                     Announcereceipttimeout                                  
                                                                             
 Purpose           : To verify that a PTP enabled device remains in SLAVE    
                     state even after the expiry of Announcereceipttimeout   
                     when defaultDS.externalPortConfigurationEnabled is set  
                     to TRUE and externalPortConfigurationPortDS.desiredState
                     is set to SLAVE.                                        
                                                                             
 Reference         : P1588/D1.3,February 2018 V3.01 Clause 17.6.5.3 Page 355 
                                                                             
 Conformance Type  : SHALL                                                   
                                                                             
