                                                                             
 Procedure         :                                                         
                                                                             
 (Initial Part)                                                              
                                                                             
 Step 1 : Initialization of DUT                                              
     i.   Enable DUT's port P1.                                              
    ii.   Enable PTP on port P1.                                             
   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   
    iv.   Configure clock mode as One-step/Two-step.                         
     v.   Configure default values for Priority1, Priority2,                 
          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   
          logMinDelayReqInterval.                                            
    vi.   Enable L1SYNC on DUT's port P1.                                    
   vii.   Configure default values for L1SyncInterval and                    
          L1SyncReceiptTimeout.                                              
  viii.   Disable L1SynOptParams on DUT.                                     
    ix.   Enable asymmetryCorrectionPortDS.enable.                           
     x.   Configure default values for egressLatency, ingressLatency,        
          constantAsymmetry and scaledDelayCoefficient.                      
                                                                             
 Step 2 : Initialization of TEE                                              
     i.   Add port T1 at TEE.                                                
                                                                             
 (Part 1)                                                                    
                                                                             
 Step 3 : Check whether scaledDelayCoefficient has the default value on port 
          P1.                                                                
                                                                             
 Step 4 : Observe that DUT does not allow to configure out-of-range value of 
          scaledDelayCoefficient on port P1 by trying to set                 
          scaledDelayCoefficient to - 2 (i.e., the value of dataset expressed
          in RelativeDifference asymmetryCorrectionPortDS.                   
          scaledDelayCoefficient = -2^63).                                   
                                                                             
 Step 5 : Check whether scaledDelayCoefficient has still the default value or
          zero on port P1.                                                   
                                                                             
 Step 6 : Observe that DUT allows to configure the minimum allowed value of  
          the scaledDelayCoefficient on port P1 by setting                   
          scaledDelayCoefficient to - 1 (i.e., the value of dataset expressed
          in RelativeDifference asymmetryCorrectionPortDS.                   
          scaledDelayCoefficient = -2^62).                                   
                                                                             
 Step 7 : Check whether scaledDelayCoefficient on port P1 is - 1 (i.e.,      
          asymmetryCorrectionPortDS.scaledDelayCoefficient = -2^62 expressed 
          in RelativeDifference).                                            
                                                                             
 Step 8 : Observe that DUT does not allow to configure out-of-range value of 
          scaledDelayCoefficient on port P1 by trying to set                 
          scaledDelayCoefficient to 2 (i.e., the value of dataset expressed  
          in RelativeDifference asymmetryCorrectionPortDS.                   
          scaledDelayCoefficient = 2^63).                                    
                                                                             
 Step 9 : Check whether scaledDelayCoefficient on port P1 is still -1 (i.e., 
          asymmetryCorrectionPortDS.scaledDelayCoefficient = -2^62 expressed 
          in RelativeDifference) or zero.                                    
                                                                             
 Step 10: Observe that DUT allows to configure maximum allowed value of the  
          scaledDelayCoefficient on port P1 by trying to set                 
          scaledDelayCoefficient to 1 (i.e., the value of dataset expressed  
          in RelativeDifference asymmetryCorrectionPortDS.                   
          scaledDelayCoefficient = 2^62).                                    
                                                                             
 Step 11: Check whether scaledDelayCoefficient on port P1 is                 
          1 (i.e., asymmetryCorrectionPortDS.scaledDelayCoefficient = 2^62   
          expressed in RelativeDifference).                                  
                                                                             
 Step 12: Observe that DUT allows to configure scaledDelayCoefficient to the 
          default value on port P1.                                          
                                                                             
 Step 13: Verify whether scaledDelayCoefficient has the default value on port
          P1.                                                                
                                                                             
