#!/usr/bin/tcl
###############################################################################
# File Name      : ptp_ha_array_wrs.tcl                                       #
# File Version   : 1.4                                                        #
# Component Name : ATTEST CLI ARRAY MAPPING                                   #
# Module Name    : PTP High Accuracy                                          #
#                                                                             #
###############################################################################
#                                                                             #
#     SET ARRAY INDEX                                                         #
#                                                                             #
#  DUT SET FUNCTIONS     :                                                    #
#                                                                             #
#  1) DUT_PORT_ENABLE                                                         #
#  2) DUT_PORT_PTP_ENABLE                                                     #
#  3) DUT_SET_VLAN                                                            #
#  4) DUT_SET_VLAN_PRIORITY                                                   #
#  5) DUT_GLOBAL_PTP_ENABLE                                                   #
#  6) DUT_SET_COMMUNICATION_MODE                                              #
#  7) DUT_SET_DOMAIN                                                          #
#  8) DUT_SET_NETWORK_PROTOCOL                                                #
#  9) DUT_SET_CLOCK_MODE                                                      #
# 10) DUT_SET_CLOCK_STEP                                                      #
# 11) DUT_SET_DELAY_MECHANISM                                                 #
# 12) DUT_SET_PRIORITY1                                                       #
# 13) DUT_SET_PRIORITY2                                                       #
# 14) DUT_TCR_RCR_CR_ENABLE                                                   #
# 15) DUT_L1SYNC_ENABLE                                                       #
# 16) DUT_L1SYNC_OPT_PARAMS_ENABLE                                            #
# 17) DUT_SLAVEONLY_ENABLE                                                    #
# 18) DUT_MASTERONLY_ENABLE                                                   #
# 19) DUT_SET_ANNOUNCE_INTERVAL                                               #
# 20) DUT_SET_ANNOUNCE_TIMEOUT                                                #
# 21) DUT_SET_SYNC_INTERVAL                                                   #
# 22) DUT_SET_DELAYREQ_INTERVAL                                               #
# 23) DUT_SET_PDELAYREQ_INTERVAL                                              #
# 24) DUT_SET_L1SYNC_INTERVAL                                                 #
# 25) DUT_SET_L1SYNC_TIMEOUT                                                  #
# 26) DUT_SET_EGRESS_LATENCY                                                  #
# 27) DUT_SET_INGRESS_LATENCY                                                 #
# 28) DUT_SET_CONSTANT_ASYMMETRY                                              #
# 29) DUT_SET_SCALED_DELAY_COEFFICIENT                                        #
# 30) DUT_SET_ASYMMETRY_CORRECTION                                            #
# 31) DUT_EXTERNAL_PORT_CONFIG_ENABLE                                         #
# 32) DUT_SET_EPC_DESIRED_STATE                                               #
# 33) DUT_SET_IPV4_ADDRESS                                                    #
# 34) DUT_TIMESTAMP_CORRECTED_TX_ENABLE                                       #
#                                                                             #
#  DUT DISABLE FUNCTIONS :                                                    #
#                                                                             #
#  1) DUT_PORT_DISABLE                                                        #
#  2) DUT_PORT_PTP_DISABLE                                                    #
#  3) DUT_RESET_VLAN                                                          #
#  4) DUT_RESET_VLAN_PRIORITY                                                 #
#  5) DUT_GLOBAL_PTP_DISABLE                                                  #
#  6) DUT_RESET_COMMUNICATION_MODE                                            #
#  7) DUT_RESET_DOMAIN                                                        #
#  8) DUT_RESET_NETWORK_PROTOCOL                                              #
#  9) DUT_RESET_CLOCK_MODE                                                    #
# 10) DUT_RESET_CLOCK_STEP                                                    #
# 11) DUT_RESET_DELAY_MECHANISM                                               #
# 12) DUT_RESET_PRIORITY1                                                     #
# 13) DUT_RESET_PRIORITY2                                                     #
# 14) DUT_TCR_RCR_CR_DISABLE                                                  #
# 15) DUT_L1SYNC_DISABLE                                                      #
# 16) DUT_L1SYNC_OPT_PARAMS_DISABLE                                           #
# 17) DUT_SLAVEONLY_DISABLE                                                   #
# 18) DUT_MASTERONLY_DISABLE                                                  #
# 19) DUT_RESET_ANNOUNCE_INTERVAL                                             #
# 20) DUT_RESET_ANNOUNCE_TIMEOUT                                              #
# 21) DUT_RESET_SYNC_INTERVAL                                                 #
# 22) DUT_RESET_DELAYREQ_INTERVAL                                             #
# 23) DUT_RESET_PDELAYREQ_INTERVAL                                            #
# 24) DUT_RESET_L1SYNC_INTERVAL                                               #
# 25) DUT_RESET_L1SYNC_TIMEOUT                                                #
# 26) DUT_RESET_EGRESS_LATENCY                                                #
# 27) DUT_RESET_INGRESS_LATENCY                                               #
# 28) DUT_RESET_CONSTANT_ASYMMETRY                                            #
# 29) DUT_RESET_SCALED_DELAY_COEFFICIENT                                      #
# 30) DUT_RESET_ASYMMETRY_CORRECTION                                          #
# 31) DUT_EXTERNAL_PORT_CONFIG_DISABLE                                        #
# 32) DUT_RESET_EPC_DESIRED_STATE                                             #
# 33) DUT_RESET_IPV4_ADDRESS                                                  #
# 34) DUT_TIMESTAMP_CORRECTED_TX_DISABLE                                      #
# 35) WRS_RESTART_PPSI                                                        #
#                                                                             #
#  DUT CHECK FUNCTIONS   :                                                    #
#                                                                             #
#  1) DUT_CHECK_L1SYNC_STATE                                                  #
#  2) DUT_CHECK_PTP_PORT_STATE                                                #
#  3) DUT_CHECK_ASYMMETRY_CORRECTION                                          #
#  4) DUT_CHECK_EGRESS_LATENCY                                                #
#  5) DUT_CHECK_INGRESS_LATENCY                                               #
#  6) DUT_CHECK_CONSTANT_ASYMMETRY                                            #
#  7) DUT_CHECK_SCALED_DELAY_COEFFICIENT                                      #
#  8) DUT_CHECK_EXTERNAL_PORT_CONFIG                                          #
#  9) DUT_CHECK_SLAVE_ONLY                                                    #
# 10) DUT_CHECK_MASTER_ONLY                                                   #
# 11) DUT_CHECK_OFFSET_FROM_MASTER                                            #
#                                                                             #
#  DUT GET FUNCTIONS     :                                                    #
#                                                                             #
#  1) DUT_GET_TIMESTAMP                                                       #
#  2) DUT_GET_MEAN_DELAY                                                      #
#  3) DUT_GET_MEAN_LINK_DELAY                                                 #
#  4) DUT_GET_DELAY_ASYMMETRY                                                 #
#                                                                             #
###############################################################################
# History     Date         Author         Addition/ Alteration                #
#                                                                             #
#  1.0       Nov/2018      CERN         Initial version                       #
#  1.1       Nov/2018      CERN         Added                                 #
#                                       l1SyncTimestampsCorrectedTxEnabled    #
#  1.2       Jan/2019      CERN         1) Modified to use ppsi.conf          #
#                                          parameters from template.          #
#                                       2) Added below procedures,            #
#                                          a) ptp_ha::pre_test_operation      #
#                                          b) ptp_ha::post_test_operation     #
#                                       3) Added commands "wr_date get" and   #
#                                          portDS.meanLinkDelay.              #
#  1.3       Jan/2019      CERN         Added condition to discard value zero #
#                                       while checking offsetFromMaster.      #
#  1.4       Feb/2019      CERN         Added configurable parameter to skip  #
#                                       the validation in test case.          #
#                                                                             #
###############################################################################
# Copyright (c) 2018 - 2019 CERN                                              #
###############################################################################

##########################################################################################
# Help on mapping CLI commands:                                                          #
#                                                                                        #
# ATTEST DUT functions are of three types - SET, GET, Callback                           #
#                                                                                        #
#  - SET (ATTEST_SET) function are used to set/configure the DUT                         #
#                                                                                        #
#  - GET (ATTEST_GET) function used to get/retrieve value(s) from the DUT                #
#                                                                                        #
#  - Callback function used for modifying the input parameters according to DUT for SET  #
#    and parsing the CLI show command output for GET function (DUT specific customization#
#    are done using this function). This function return following values 0/1/2          #
#    return 0 - Failure                                                                  #
#    return 1 - Success and validation is done by the function                           #
#    return $result                                                                      #
#    return 2 - Success and validation should be done by the test script                 #
#                                                                                        #
# Sub types - OUTPUT, WAIT, TIME                                                         #
#                                                                                        #
#  - WAIT (ATTEST_WAIT) function used to wait until it gets the given string.            #
#    e.g.: set cli_set_record(DUT_GLOBAL_PTP_ENABLE,ATTEST_SET,1)"your DUT CLI command"  #
#          set cli_set_record(DUT_GLOBAL_PTP_ENABLE,ATTEST_WAIT,1) "config"              #
#                                                                                        #
#  - TIME (ATTEST_TIME) function used to customize the ATTEST timeout period when a CLI  #
#    command execution takes more than the default time (5 seconds). ATTEST_TIME waits   #
#    for the DUT prompt or timeout period to elapse (whichever occurs first) before      #
#    executing the next command or returning failure/success. ATTEST_TIME accepts time in#
#    seconds.                                                                            #
#    e.g.: set cli_set_record(DUT_MVRP_SET_MVRP_REGISTRATION,ATTEST_TIME,1) "10"         #
#    will increase the ATTEST's timeout period to 10 seconds.                            #
#                                                                                        #
#  - OUTPUT (ATTEST_OUTPUT) function used to capture the DUT CLI output to a buffer      #
#    This command is used in all GET functions.                                          #
#                                                                                        #
# Syntax:                                                                                #
# -------                                                                                #
#  Set command:                                                                          #
#  set cli_set_record(<COMMAND>,ATTEST_SET,<N>)  "your DUT CLI command"                  #
#  set cli_set_record(<COMMAND>,ATTEST_SET,<N>)  "NONE"                                  #
#                                                                                        #
#  Get command:                                                                          #
#  set cli_get_record(<COMMAND>,ATTEST_OUTPUT,<N>)  "your DUT CLI show command"          #
#  set cli_get_record(<COMMAND>,ATTEST_GET,<N>)     "NONE"                               #
#                                                                                        #
#  callback_protocol_set_xxx()                                                           #
#  callback_protocol_get_xxx()                                                           #
#                                                                                        #
#  Where N = 1,2,3 ..n                                                                   #
#  The order in which the commands are executed and hence must be sequential.            #
#                                                                                        #
#  Note:                                                                                 #
#    ATTEST_WAIT and ATTEST_TIME must use the command sequence number (N) of previous    #
#    ATTEST_SET/ATTEST_GET/ATTEST_OUTPUT.                                                #
#                                                                                        #
#    e.g.: set cli_set_record(DUT_GLOBAL_PTP_ENABLE,ATTEST_SET,1)"your DUT CLI command"  #
#          set cli_set_record(DUT_GLOBAL_PTP_ENABLE,ATTEST_TIME,1) "10"                  #
#    The command sequence number in the above two statements are 1.                      #
#                                                                                        #
#    GET function only executes the particular DUT CLI command and use ATTEST_OUTPUT to  #
#    capture the CLI output to buffer. The associated Callback function does the         #
#    comparison between value in the buffer and argument provided by the test script     #
#    when callback function is called.                                                   #
#                                                                                        #
##########################################################################################

package require Expect

global env

global dut_error_msg

#set ::PTP_HA_DEBUG_CLI 1
#set ::PTP_HA_DUT_CHECK_RETRY_LIMIT 1

set SKIP_VALIDATION_FOR_DEBUG   0
set STOP_AT_STEP                55

set dut_error_msg "Available commands*|Next possible*|Invalid*|constraint*| is required*|MAC Address must be unicast*|Incorrect input*|Command not found*|overlaps*"

#exp_internal 1
global username password host

set username    $env(DUT_SSH_USER_NAME)
set password    $env(DUT_SSH_PASSWORD)
set host        $env(TEST_DEVICE_IPV4_ADDRESS)

global ATTEST_CORE tc_name case_id group_name

set PktFileName $ATTEST_CORE(-pktFileName)
set log_instance [lindex [split $PktFileName "."] 0]
set tc_name_list [split $log_instance "_"]
set group_name  [lindex $tc_name_list 3]
set case_id [lindex $tc_name_list 4]


################################################################################
#                            DUT SET FUNCTIONS                                 #
################################################################################

proc  tee_set_dut_ptp_ha_cli_from_file {} {

    global cli_set_record

#1.
################################################################################
#                                                                              #
#  COMMAND           : PORT_ENABLE                                             #
#                                                                              #
#  ARRAY INDEX       : DUT_PORT_ENABLE                                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function makes the specific port administratively  #
#                      up at the DUT.                                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_PORT_ENABLE,ATTEST_SET,1)         "ifconfig ATTEST_PARAM1 up"
    set cli_set_record(DUT_PORT_ENABLE,ATTEST_SET,2)         "NONE"


#2.
################################################################################
#                                                                              #
#  COMMAND           : PORT_PTP_ENABLE                                         #
#                                                                              #
#  ARRAY INDEX       : DUT_PORT_PTP_ENABLE                                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function enables PTP on a specific port on the DUT.#
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_PORT_PTP_ENABLE,ATTEST_SET,1)          "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_PORT_PTP_ENABLE,ATTEST_TIME,1)         "30"
    set cli_set_record(DUT_PORT_PTP_ENABLE,ATTEST_SET,2)          "NONE"


#3.
################################################################################
#                                                                              #
#  COMMAND           : SET_VLAN                                                #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_VLAN                                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = vlan_id     VLAN ID with which VLAN has to be created.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#   ATTEST_PARAM2    = port_num    Port which has to be associated to VLAN ID. #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function creates VLAN ID and associates given port #
#                      to it on the DUT.                                       #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_SET_VLAN,ATTEST_SET,1)           "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_SET_VLAN,ATTEST_TIME,1)          "30"
    set cli_set_record(DUT_SET_VLAN,ATTEST_SET,2)           "NONE"


#4.
################################################################################
#                                                                              #
#  COMMAND           : SET_VLAN_PRIORITY                                       #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_VLAN_PRIORITY                                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = vlan_id     VLAN ID                                     #
#                                  (e.g., 100)                                 #
#                                                                              #
#   ATTEST_PARAM2    = vlan_priority VLAN priority                             #
#                                  (e.g., 1)                                   #
#                                                                              #
#   ATTEST_PARAM3    = port_num    Port which has to be associated to VLAN ID. #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function associates VLAN priority to the VLAN ID   #
#                      on the DUT.                                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_SET_VLAN_PRIORITY,ATTEST_SET,1)        "NOOP"
    set cli_set_record(DUT_SET_VLAN_PRIORITY,ATTEST_SET,2)        "NONE"


#5.
################################################################################
#                                                                              #
#  COMMAND           : GLOBAL_PTP_ENABLE                                       #
#                                                                              #
#  ARRAY INDEX       : DUT_GLOBAL_PTP_ENABLE                                   #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  DEFINITION        : This function enables PTP on the DUT.                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_GLOBAL_PTP_ENABLE,ATTEST_SET,1)         "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_GLOBAL_PTP_ENABLE,ATTEST_TIME,1)        "30"
    set cli_set_record(DUT_GLOBAL_PTP_ENABLE,ATTEST_SET,2)         "NONE"


#6.
################################################################################
#                                                                              #
#  COMMAND           : SET_COMMUNICATION_MODE                                  #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_COMMUNICATION_MODE                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = clock_mode  Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#   ATTEST_PARAM3    = ptp_version PTP version.                                #
#                                  (Default: 2)                                #
#                                                                              #
#   ATTEST_PARAM4    = communication_mode Communication mode                   #
#                                  (e.g., "Unicast"|"Multicast")               #
#                                                                              #
#  DEFINITION        : This function sets the communication mode on a specific #
#                      port of the DUT.                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_SET_COMMUNICATION_MODE,ATTEST_SET,1)   "NOOP"
    set cli_set_record(DUT_SET_COMMUNICATION_MODE,ATTEST_SET,2)   "NONE"


#7.
################################################################################
#                                                                              #
#  COMMAND           : SET_DOMAIN                                              #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_DOMAIN                                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = clock_mode  Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#   ATTEST_PARAM2    = domain      Domain number.                              #
#                                  (e.g., 0..255)                              #
#                                                                              #
#  DEFINITION        : This function sets domain number on the DUT.            #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_SET_DOMAIN,ATTEST_SET,1)           "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_SET_DOMAIN,ATTEST_TIME,1)          "30"
    set cli_set_record(DUT_SET_DOMAIN,ATTEST_SET,2)           "NONE"


#8.
################################################################################
#                                                                              #
#  COMMAND           : SET_NETWORK_PROTOCOL                                    #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_NETWORK_PROTOCOL                                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = clock_mode  clock_mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#   ATTEST_PARAM3    = network_protocol Network protocol.                      #
#                                  (e.g., "IEEE 802.3"|"UDP/IPv4")             #
#                                                                              #
#  DEFINITION        : This function sets the network protocol on a specific   #
#                      port of the DUT.                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_SET_NETWORK_PROTOCOL,ATTEST_SET,1)     "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_SET_NETWORK_PROTOCOL,ATTEST_TIME,1)    "30"
    set cli_set_record(DUT_SET_NETWORK_PROTOCOL,ATTEST_SET,2)     "NONE"


#9.
################################################################################
#                                                                              #
#  COMMAND           : SET_CLOCK_MODE                                          #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_CLOCK_MODE                                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = clock_mode  Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  DEFINITION        : This function sets clock mode on a specific port of DUT.#
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_SET_CLOCK_MODE,ATTEST_SET,1)           "NOOP"
    set cli_set_record(DUT_SET_CLOCK_MODE,ATTEST_SET,2)           "NONE"


#10.
################################################################################
#                                                                              #
#  COMMAND           : SET_CLOCK_STEP                                          #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_CLOCK_STEP                                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = clock_mode  Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#   ATTEST_PARAM3    = clock_step  Clock step.                                 #
#                                  (e.g., "One-step"|"Two-step")               #
#                                                                              #
#  DEFINITION        : This function sets the clock-step on a specific port on #
#                      the DUT.                                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_SET_CLOCK_STEP,ATTEST_SET,1)           "NOOP"
    set cli_set_record(DUT_SET_CLOCK_STEP,ATTEST_SET,2)           "NONE"


#11.
################################################################################
#                                                                              #
#  COMMAND           : SET_DELAY_MECHANISM                                     #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_DELAY_MECHANISM                                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = clock_mode  Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#   ATTEST_PARAM3    = delay_mechanism Delay mechanism.                        #
#                                  (e.g., e2e|p2p|disable)                     #
#                                                                              #
#  DEFINITION        : This function sets the delay mechanism to a specific    #
#                      port on the DUT.                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_SET_DELAY_MECHANISM,ATTEST_SET,1)      "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_SET_DELAY_MECHANISM,ATTEST_TIME,1)     "30"
    set cli_set_record(DUT_SET_DELAY_MECHANISM,ATTEST_SET,2)      "NONE"


#12.
################################################################################
#                                                                              #
#  COMMAND           : SET_PRIORITY1                                           #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_PRIORITY1                                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = clock_mode  Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#   ATTEST_PARAM2    = priority1   Prority1                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_SET_PRIORITY1,ATTEST_SET,1)           "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_SET_PRIORITY1,ATTEST_TIME,1)          "30"
    set cli_set_record(DUT_SET_PRIORITY1,ATTEST_SET,2)           "NONE"

#13.
################################################################################
#                                                                              #
#  COMMAND           : SET_PRIORITY2                                           #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_PRIORITY2                                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = clock_mode  Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#   ATTEST_PARAM2    = priority2   Prority2                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_SET_PRIORITY2,ATTEST_SET,1)           "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_SET_PRIORITY2,ATTEST_TIME,1)          "30"
    set cli_set_record(DUT_SET_PRIORITY2,ATTEST_SET,2)           "NONE"


#14.
################################################################################
#                                                                              #
#  COMMAND           : TCR_RCR_CR_ENABLE                                       #
#                                                                              #
#  ARRAY INDEX       : DUT_TCR_RCR_CR_ENABLE                                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function enables peerTxCoherentIsRequired,         #
#                      peerRxCoherentIsRequired and peerCongruentIsRequired on #
#                      the DUT.                                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_TCR_RCR_CR_ENABLE,ATTEST_SET,1)        "NOOP"
    set cli_set_record(DUT_TCR_RCR_CR_ENABLE,ATTEST_SET,2)        "NONE"


#15.
################################################################################
#                                                                              #
#  COMMAND           : L1SYNC_ENABLE                                           #
#                                                                              #
#  ARRAY INDEX       : DUT_L1SYNC_ENABLE                                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function enables L1Sync option on a specific port  #
#                      at the DUT.                                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_L1SYNC_ENABLE,ATTEST_SET,1)        "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_L1SYNC_ENABLE,ATTEST_TIME,1)       "30"
    set cli_set_record(DUT_L1SYNC_ENABLE,ATTEST_SET,2)        "NONE"


#16.
################################################################################
#                                                                              #
#  COMMAND           : L1SYNC_OPT_PARAMS_ENABLE                                #
#                                                                              #
#  ARRAY INDEX       : DUT_L1SYNC_OPT_PARAMS_ENABLE                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function enables extended format of L1Sync TLV on  #
#                      the specified port at the DUT.                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_L1SYNC_OPT_PARAMS_ENABLE,ATTEST_SET,1)    "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_L1SYNC_OPT_PARAMS_ENABLE,ATTEST_TIME,1)   "30"
    set cli_set_record(DUT_L1SYNC_OPT_PARAMS_ENABLE,ATTEST_SET,2)    "NONE"


#17.
################################################################################
#                                                                              #
#  COMMAND           : SLAVEONLY_ENABLE                                        #
#                                                                              #
#  ARRAY INDEX       : DUT_SLAVEONLY_ENABLE                                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function enables slave-only on the specified port  #
#                      at the DUT.                                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_SLAVEONLY_ENABLE,ATTEST_SET,1)         "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_SLAVEONLY_ENABLE,ATTEST_TIME,1)        "30"
    set cli_set_record(DUT_SLAVEONLY_ENABLE,ATTEST_SET,2)         "NONE"


#18.
################################################################################
#                                                                              #
#  COMMAND           : MASTERONLY_ENABLE                                       #
#                                                                              #
#  ARRAY INDEX       : DUT_MASTERONLY_ENABLE                                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function enables master-only on the specified port #
#                      at the DUT.                                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_MASTERONLY_ENABLE,ATTEST_SET,1)        "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_MASTERONLY_ENABLE,ATTEST_TIME,1)       "30"
    set cli_set_record(DUT_MASTERONLY_ENABLE,ATTEST_SET,2)        "NONE"


#19.
################################################################################
#                                                                              #
#  COMMAND           : SET_ANNOUNCE_INTERVAL                                   #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_ANNOUNCE_INTERVAL                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = announce_interval announceInterval.                     #
#                                  (e.g., 1|2|4|8|16)                          #
#                                                                              #
#  DEFINITION        : This function sets announceInterval value to a specific #
#                      port on the DUT.                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_SET_ANNOUNCE_INTERVAL,ATTEST_SET,1)        "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_SET_ANNOUNCE_INTERVAL,ATTEST_TIME,1)       "30"
    set cli_set_record(DUT_SET_ANNOUNCE_INTERVAL,ATTEST_SET,2)        "NONE"


#20.
################################################################################
#                                                                              #
#  COMMAND           : SET_ANNOUNCE_TIMEOUT                                    #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_ANNOUNCE_TIMEOUT                                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = announce_timeout announceReceiptTimeout.                #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  DEFINITION        : This function sets announceReceiptTimeout value to a    #
#                      specific port on the DUT.                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_SET_ANNOUNCE_TIMEOUT,ATTEST_SET,1)    "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_SET_ANNOUNCE_TIMEOUT,ATTEST_TIME,1)   "30"
    set cli_set_record(DUT_SET_ANNOUNCE_TIMEOUT,ATTEST_SET,2)    "NONE"


#21.
################################################################################
#                                                                              #
#  COMMAND           : SET_SYNC_INTERVAL                                       #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_SYNC_INTERVAL                                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = sync_interval logSyncInterval                           #
#                                  (e.g., 3)                                   #
#                                                                              #
#  DEFINITION        : This function sets logSyncInterval value to a specific  #
#                      port on the DUT.                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_SET_SYNC_INTERVAL,ATTEST_SET,1)        "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_SET_SYNC_INTERVAL,ATTEST_TIME,1)       "30"
    set cli_set_record(DUT_SET_SYNC_INTERVAL,ATTEST_SET,2)        "NONE"


#22.
################################################################################
#                                                                              #
#  COMMAND           : SET_DELAYREQ_INTERVAL                                   #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_DELAYREQ_INTERVAL                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num     DUT's port number.                         #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = delayreq_interval MinDelayReqInterval.                  #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  DEFINITION        : This function sets the MinDelayReqInterval to the       #
#                      specific PTP port on the DUT.                           #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_SET_DELAYREQ_INTERVAL,ATTEST_SET,1)    "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_SET_DELAYREQ_INTERVAL,ATTEST_TIME,1)   "30"
    set cli_set_record(DUT_SET_DELAYREQ_INTERVAL,ATTEST_SET,2)    "NONE"


#23.
################################################################################
#                                                                              #
#  COMMAND           : SET_PDELAYREQ_INTERVAL                                  #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_PDELAYREQ_INTERVAL                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = pdelayreq_interval  minPdelayReqInterval.               #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  DEFINITION        : This function sets the minPdelayReqInterval to the      #
#                      specific PTP port on the DUT.                           #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_SET_PDELAYREQ_INTERVAL,ATTEST_SET,1)   "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_SET_PDELAYREQ_INTERVAL,ATTEST_TIME,1)  "30"
    set cli_set_record(DUT_SET_PDELAYREQ_INTERVAL,ATTEST_SET,2)   "NONE"


#24.
################################################################################
#                                                                              #
#  COMMAND           : SET_L1SYNC_INTERVAL                                     #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_L1SYNC_INTERVAL                                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = l1sync_interval logL1SyncInterval.                      #
#                                  (e.g., 3)                                   #
#                                                                              #
#  DEFINITION        : This function sets the logL1SyncInterval to a specific  #
#                      port on the DUT.                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_SET_L1SYNC_INTERVAL,ATTEST_SET,1)      "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_SET_L1SYNC_INTERVAL,ATTEST_TIME,1)     "30"
    set cli_set_record(DUT_SET_L1SYNC_INTERVAL,ATTEST_SET,2)      "NONE"


#25.
################################################################################
#                                                                              #
#  COMMAND           : SET_L1SYNC_TIMEOUT                                      #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_L1SYNC_TIMEOUT                                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = l1sync_timeout L1SyncReceiptTimeout.                    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  DEFINITION        : This function sets L1SyncReceiptTimeout to a specific   #
#                      port on the DUT.                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_SET_L1SYNC_TIMEOUT,ATTEST_SET,1)       "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_SET_L1SYNC_TIMEOUT,ATTEST_TIME,1)      "30"
    set cli_set_record(DUT_SET_L1SYNC_TIMEOUT,ATTEST_SET,2)       "NONE"


#26.
################################################################################
#                                                                              #
#  COMMAND           : SET_EGRESS_LATENCY                                      #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_EGRESS_LATENCY                                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = latency     timestampCorrectionPortDS.egressLatency.    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  DEFINITION        : This function sets timestampCorrectionPortDS.           #
#                      egressLatency to a specific port on the DUT.            #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_SET_EGRESS_LATENCY,ATTEST_SET,1)        "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_SET_EGRESS_LATENCY,ATTEST_TIME,1)       "30"
    set cli_set_record(DUT_SET_EGRESS_LATENCY,ATTEST_SET,2)        "NONE"


#27.
################################################################################
#                                                                              #
#  COMMAND           : SET_INGRESS_LATENCY                                     #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_INGRESS_LATENCY                                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = latency     timestampCorrectionPortDS.ingressLatency.   #
#                                  (e.g., 3)                                   #
#                                                                              #
#  DEFINITION        : This function sets timestampCorrectionPortDS.           #
#                      ingressLatency to a specific port on the DUT.           #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_SET_INGRESS_LATENCY,ATTEST_SET,1)        "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_SET_INGRESS_LATENCY,ATTEST_TIME,1)       "30"
    set cli_set_record(DUT_SET_INGRESS_LATENCY,ATTEST_SET,2)        "NONE"


#28.
################################################################################
#                                                                              #
#  COMMAND           : SET_CONSTANT_ASYMMETRY                                  #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_CONSTANT_ASYMMETRY                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = constant_asymmetry asymmetryCorrectionPortDS.           #
#                                  constantAsymmetry.                          #
#                                  (e.g., 3)                                   #
#                                                                              #
#  DEFINITION        : This function sets asymmetryCorrectionPortDS.           #
#                      constantAsymmetry to a specific port on the DUT.        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_SET_CONSTANT_ASYMMETRY,ATTEST_SET,1)        "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_SET_CONSTANT_ASYMMETRY,ATTEST_TIME,1)       "30"
    set cli_set_record(DUT_SET_CONSTANT_ASYMMETRY,ATTEST_SET,2)        "NONE"


#29.
################################################################################
#                                                                              #
#  COMMAND           : SET_SCALED_DELAY_COEFFICIENT                            #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_SCALED_DELAY_COEFFICIENT                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = delay_coefficient asymmetryCorrectionPortDS.            #
#                                  scaledDelayCoefficient.                     #
#                                  (e.g., 3)                                   #
#                                                                              #
#  DEFINITION        : This function sets asymmetryCorrectionPortDS.           #
#                      scaledDelayCoefficient to a specific port on the DUT.   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_SET_SCALED_DELAY_COEFFICIENT,ATTEST_SET,1)  "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_SET_SCALED_DELAY_COEFFICIENT,ATTEST_TIME,1) "30"
    set cli_set_record(DUT_SET_SCALED_DELAY_COEFFICIENT,ATTEST_SET,2)  "NONE"


#30.
################################################################################
#                                                                              #
#  COMMAND           : SET_ASYMMETRY_CORRECTION                                #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_ASYMMETRY_CORRECTION                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function sets the asymmetryCorrectionPortDS.enable #
#                      to the specific PTP port on the DUT.                    #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_SET_ASYMMETRY_CORRECTION,ATTEST_SET,1)    "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_SET_ASYMMETRY_CORRECTION,ATTEST_TIME,1)   "30"
    set cli_set_record(DUT_SET_ASYMMETRY_CORRECTION,ATTEST_SET,2)    "NONE"


#31.
################################################################################
#                                                                              #
#  COMMAND           : EXTERNAL_PORT_CONFIG_ENABLE                             #
#                                                                              #
#  ARRAY INDEX       : DUT_EXTERNAL_PORT_CONFIG_ENABLE                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function enables external port configuration option#
#                      on a specific port at the DUT.                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_EXTERNAL_PORT_CONFIG_ENABLE,ATTEST_SET,1)    "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_EXTERNAL_PORT_CONFIG_ENABLE,ATTEST_TIME,1)   "30"
    set cli_set_record(DUT_EXTERNAL_PORT_CONFIG_ENABLE,ATTEST_SET,2)    "NONE"


#32.
################################################################################
#                                                                              #
#  COMMAND           : SET_EPC_DESIRED_STATE                                   #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_EPC_DESIRED_STATE                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = desired_state externalPortConfigurationPortDS.          #
#                                  desiredState.                               #
#                                  (e.g., "INITIALIZING"|"FAULTY"|"DISABLED"|  #
#                                         "LISTENING"|"PRE_MASTER"|"MASTER"|   #
#                                         "PASSIVE"|"UNCALIBRATED"|"SLAVE"     #
#                                                                              #
#  DEFINITION        : This function sets given externalPortConfigurationPortDS#
#                      desiredState to a specific port on the DUT.             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_SET_EPC_DESIRED_STATE,ATTEST_SET,1)        "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_SET_EPC_DESIRED_STATE,ATTEST_TIME,1)       "30"
    set cli_set_record(DUT_SET_EPC_DESIRED_STATE,ATTEST_SET,2)        "NONE"


#33.
################################################################################
#                                                                              #
#  COMMAND           : SET_IPV4_ADDRESS                                        #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_IPV4_ADDRESS                                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = ip_address  IPv4 address.                               #
#                                  (e.g., 192.168.200.1)                       #
#                                                                              #
#   ATTEST_PARAM3    = net_mask    Subnet mask.                                #
#                                  (e.g., 255.255.255.0)                       #
#                                                                              #
#   ATTEST_PARAM4    = vlan_id     VLAN ID.                                    #
#                                  (e.g., 100)                                 #
#                                                                              #
#  DEFINITION        : This function sets given IP address to a specific port  #
#                      on the DUT.                                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_SET_IPV4_ADDRESS,ATTEST_SET,1)        "ifconfig ATTEST_PARAM1 ATTEST_PARAM2 netmask ATTEST_PARAM3"
    set cli_set_record(DUT_SET_IPV4_ADDRESS,ATTEST_SET,2)        "NONE"


#34.
################################################################################
#                                                                              #
#  COMMAND           : SET_TIMESTAMP_CORRECTED_TX_ENABLE                       #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_TIMESTAMP_CORRECTED_TX_ENABLE                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function sets given L1SyncOptParamsPortDS.         #
#                      timestampsCorrectedTx option to a specific port on the  #
#                      DUT.                                                    #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_TIMESTAMP_CORRECTED_TX_ENABLE,ATTEST_SET,1)    "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_TIMESTAMP_CORRECTED_TX_ENABLE,ATTEST_TIME,1)   "30"
    set cli_set_record(DUT_TIMESTAMP_CORRECTED_TX_ENABLE,ATTEST_SET,2)    "NONE"


################################################################################
#                       DUT DISABLING FUNCTIONS                                #
################################################################################


#1.
################################################################################
#                                                                              #
#  COMMAND           : PORT_DISABLE                                            #
#                                                                              #
#  ARRAY INDEX       : DUT_PORT_DISABLE                                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function makes the specific port administratively  #
#                      down at the DUT.                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_PORT_DISABLE,ATTEST_SET,1)             "NOOP"
    set cli_set_record(DUT_PORT_DISABLE,ATTEST_SET,2)             "NONE"


#2.
################################################################################
#                                                                              #
#  COMMAND           : PORT_PTP_DISABLE                                        #
#                                                                              #
#  ARRAY INDEX       : DUT_PORT_PTP_DISABLE                                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    Port on which ptp is to be disabled.        #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  DEFINITION        : This function disables PTP on a specific port on the DUT#
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_PORT_PTP_DISABLE,ATTEST_SET,1)        "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_PORT_PTP_DISABLE,ATTEST_TIME,1)       "30"
    set cli_set_record(DUT_PORT_PTP_DISABLE,ATTEST_SET,2)        "NONE"


#3.
################################################################################
#                                                                              #
#  COMMAND           : RESET_VLAN                                              #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_VLAN                                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = vlan_id     VLAN ID with which VLAN has to be deleted.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#   ATTEST_PARAM2    = port_num    Port which has to be removed from VLAN ID.  #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function removes port from VLAN ID and deletes VLAN#
#                      ID from the DUT.                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_RESET_VLAN,ATTEST_SET,1)          "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_RESET_VLAN,ATTEST_TIME,1)         "30"
    set cli_set_record(DUT_RESET_VLAN,ATTEST_SET,2)          "NONE"


#4.
################################################################################
#                                                                              #
#  COMMAND           : RESET_VLAN_PRIORITY                                     #
#                                                                              #
#  ARRAY INDEX       : DUT_ReSET_VLAN_PRIORITY                                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = vlan_id     VLAN ID                                     #
#                                  (e.g., 100)                                 #
#                                                                              #
#   ATTEST_PARAM2    = vlan_priority VLAN priority                             #
#                                  (e.g., 1)                                   #
#                                                                              #
#   ATTEST_PARAM3    = port_num    Port which has to be associated to VLAN ID. #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function resets priority to the VLAN ID on the DUT.#
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_RESET_VLAN_PRIORITY,ATTEST_SET,1)      "NOOP"
    set cli_set_record(DUT_RESET_VLAN_PRIORITY,ATTEST_SET,2)      "NONE"


#5.
################################################################################
#                                                                              #
#  COMMAND           : GLOBAL_PTP_DISABLE                                      #
#                                                                              #
#  ARRAY INDEX       : DUT_GLOBAL_PTP_DISABLE                                  #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  DEFINITION        : This function disables PTP on the DUT.                  #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_GLOBAL_PTP_DISABLE,ATTEST_SET,1)         "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_GLOBAL_PTP_DISABLE,ATTEST_TIME,1)        "30"
    set cli_set_record(DUT_GLOBAL_PTP_DISABLE,ATTEST_SET,2)         "NONE"


#6.
################################################################################
#                                                                              #
#  COMMAND           : RESET_COMMUNICATION_MODE                                #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_COMMUNICATION_MODE                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = clock_mode  Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#   ATTEST_PARAM3    = ptp_version PTP version.                                #
#                                  (Default: 2)                                #
#                                                                              #
#   ATTEST_PARAM4    = communication_mode Communication mode                   #
#                                  (e.g., "Unicast"|"Multicast")               #
#                                                                              #
#  DEFINITION        : This function resets the communication mode on a        #
#                      specific port of the DUT.                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_RESET_COMMUNICATION_MODE,ATTEST_SET,1)   "NOOP"
    set cli_set_record(DUT_RESET_COMMUNICATION_MODE,ATTEST_SET,2)   "NONE"


#7.
################################################################################
#                                                                              #
#  COMMAND           : RESET_DOMAIN                                            #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_DOMAIN                                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = clock_mode  Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#   ATTEST_PARAM2    = domain      Domain number.                              #
#                                  (e.g., 0..255)                              #
#                                                                              #
#  DEFINITION        : This function resets domain number on the DUT.          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_RESET_DOMAIN,ATTEST_SET,1)           "NOOP"
    set cli_set_record(DUT_RESET_DOMAIN,ATTEST_SET,2)           "NONE"


#8.
################################################################################
#                                                                              #
#  COMMAND           : RESET_NETWORK_PROTOCOL                                  #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_NETWORK_PROTOCOL                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = clock_mode  clock_mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#   ATTEST_PARAM3    = network_protocol Network protocol.                      #
#                                  (e.g., "IEEE 802.3"|"UDP/IPv4")             #
#                                                                              #
#  DEFINITION        : This function resets the network protocol on a specific #
#                      port of the DUT.                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_RESET_NETWORK_PROTOCOL,ATTEST_SET,1)   "NOOP"
    set cli_set_record(DUT_RESET_NETWORK_PROTOCOL,ATTEST_SET,2)   "NONE"


#9.
################################################################################
#                                                                              #
#  COMMAND           : RESET_CLOCK_MODE                                        #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_CLOCK_MODE                                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = clock_mode  Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  DEFINITION        : This function resets clock mode on a specific port of   #
#                      DUT.                                                    #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_RESET_CLOCK_MODE,ATTEST_SET,1)         "NOOP"
    set cli_set_record(DUT_RESET_CLOCK_MODE,ATTEST_SET,2)         "NONE"


#10.
################################################################################
#                                                                              #
#  COMMAND           : RESET_CLOCK_STEP                                        #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_CLOCK_STEP                                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = clock_mode  Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#   ATTEST_PARAM3    = clock_step  Clock step.                                 #
#                                  (e.g., "One-step"|"Two-step")               #
#                                                                              #
#  DEFINITION        : This function resets the clock-step on a specific port  #
#                      on the DUT.                                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_RESET_CLOCK_STEP,ATTEST_SET,1)         "NOOP"
    set cli_set_record(DUT_RESET_CLOCK_STEP,ATTEST_SET,2)         "NONE"


#11.
################################################################################
#                                                                              #
#  COMMAND           : RESET_DELAY_MECHANISM                                   #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_DELAY_MECHANISM                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = clock_mode  Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#   ATTEST_PARAM3    = delay_mechanism Delay mechanism.                        #
#                                  (e.g., e2e|p2p|disable)                     #
#                                                                              #
#  DEFINITION        : This function resets the delay mechanism to a specific  #
#                      port on the DUT.                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_RESET_DELAY_MECHANISM,ATTEST_SET,1)    "NOOP"
    set cli_set_record(DUT_RESET_DELAY_MECHANISM,ATTEST_SET,2)    "NONE"


#12.
################################################################################
#                                                                              #
#  COMMAND           : RESET_PRIORITY1                                         #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_PRIORITY1                                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = clock_mode  Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#   ATTEST_PARAM2    = priority1   Prority1                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  DEFINITION        : This function resets the priority1 on the DUT.          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_RESET_PRIORITY1,ATTEST_SET,1)        "NOOP"
    set cli_set_record(DUT_RESET_PRIORITY1,ATTEST_SET,2)        "NONE"


#13.
################################################################################
#                                                                              #
#  COMMAND           : RESET_PRIORITY2                                         #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_PRIORITY2                                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = clock_mode  Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#   ATTEST_PARAM2    = priority2   Prority2                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  DEFINITION        : This function resets the priority2 on the DUT.          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_RESET_PRIORITY2,ATTEST_SET,1)          "NOOP"
    set cli_set_record(DUT_RESET_PRIORITY2,ATTEST_SET,2)          "NONE"


#14.
################################################################################
#                                                                              #
#  COMMAND           : TCR_RCR_CR_DISABLE                                      #
#                                                                              #
#  ARRAY INDEX       : DUT_TCR_RCR_CR_DISABLE                                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function disables peerTxCoherentIsRequired,        #
#                      peerRxCoherentIsRequired and peerCongruentIsRequired on #
#                      the DUT.                                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_TCR_RCR_CR_DISABLE,ATTEST_SET,1)       "NOOP"
    set cli_set_record(DUT_TCR_RCR_CR_DISABLE,ATTEST_SET,2)       "NONE"


#15.
################################################################################
#                                                                              #
#  COMMAND           : L1SYNC_DISABLE                                          #
#                                                                              #
#  ARRAY INDEX       : DUT_L1SYNC_DISABLE                                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function disables L1Sync option on a specific port #
#                      at the DUT.                                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_L1SYNC_DISABLE,ATTEST_SET,1)        "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_L1SYNC_DISABLE,ATTEST_TIME,1)       "30"
    set cli_set_record(DUT_L1SYNC_DISABLE,ATTEST_SET,2)        "NONE"


#16.
################################################################################
#                                                                              #
#  COMMAND           : L1SYNC_OPT_PARAMS_DISABLE                               #
#                                                                              #
#  ARRAY INDEX       : DUT_L1SYNC_OPT_PARAMS_DISABLE                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function disables extended format of L1Sync TLV on #
#                      the specified port at the DUT.                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_L1SYNC_OPT_PARAMS_DISABLE,ATTEST_SET,1)   "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_L1SYNC_OPT_PARAMS_DISABLE,ATTEST_TIME,1)  "30"
    set cli_set_record(DUT_L1SYNC_OPT_PARAMS_DISABLE,ATTEST_SET,2)   "NONE"


#17.
################################################################################
#                                                                              #
#  COMMAND           : SLAVEONLY_DISABLE                                       #
#                                                                              #
#  ARRAY INDEX       : DUT_SLAVEONLY_DISABLE                                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function disables slave-only on the specified port #
#                      at the DUT.                                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_SLAVEONLY_DISABLE,ATTEST_SET,1)      "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_SLAVEONLY_DISABLE,ATTEST_TIME,1)     "30"
    set cli_set_record(DUT_SLAVEONLY_DISABLE,ATTEST_SET,2)      "NONE"


#18.
################################################################################
#                                                                              #
#  COMMAND           : MASTERONLY_DISABLE                                      #
#                                                                              #
#  ARRAY INDEX       : DUT_MASTERONLY_DISABLE                                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function disables master-only on a specific port   #
#                      at the DUT.                                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_MASTERONLY_DISABLE,ATTEST_SET,1)      "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_MASTERONLY_DISABLE,ATTEST_TIME,1)     "30"
    set cli_set_record(DUT_MASTERONLY_DISABLE,ATTEST_SET,2)      "NONE"


#19.
################################################################################
#                                                                              #
#  COMMAND           : RESET_ANNOUNCE_INTERVAL                                 #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_ANNOUNCE_INTERVAL                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = announce_interval announceInterval.                     #
#                                  (e.g., 1|2|4|8|16)                          #
#                                                                              #
#  DEFINITION        : This function resets announceInterval value to a        #
#                      specific port on the DUT.                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_RESET_ANNOUNCE_INTERVAL,ATTEST_SET,1)    "NOOP"
    set cli_set_record(DUT_RESET_ANNOUNCE_INTERVAL,ATTEST_SET,2)    "NONE"


#20.
################################################################################
#                                                                              #
#  COMMAND           : RESET_ANNOUNCE_TIMEOUT                                  #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_ANNOUNCE_TIMEOUT                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = announce_timeout announceReceiptTimeout.                #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  DEFINITION        : This function resets announceReceiptTimeout value to a  #
#                      specific port on the DUT.                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_RESET_ANNOUNCE_TIMEOUT,ATTEST_SET,1)    "NOOP"
    set cli_set_record(DUT_RESET_ANNOUNCE_TIMEOUT,ATTEST_SET,2)    "NONE"


#21.
################################################################################
#                                                                              #
#  COMMAND           : RESET_SYNC_INTERVAL                                     #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_SYNC_INTERVAL                                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = sync_interval logSyncInterval                           #
#                                  (e.g., 3)                                   #
#                                                                              #
#  DEFINITION        : This function resets logSyncInterval value to a specific#
#                      port on the DUT.                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_RESET_SYNC_INTERVAL,ATTEST_SET,1)      "NOOP"
    set cli_set_record(DUT_RESET_SYNC_INTERVAL,ATTEST_SET,2)      "NONE"


#22.
################################################################################
#                                                                              #
#  COMMAND           : RESET_DELAYREQ_INTERVAL                                 #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_DELAYREQ_INTERVAL                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = delayreq_interval MinDelayReqInterval.                  #
#                                  (e.g., 1|2|4|8|16)                          #
#                                                                              #
#  DEFINITION        : This function resets the MinDelayReqInterval to the     #
#                      specific PTP port on the DUT.                           #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_RESET_DELAYREQ_INTERVAL,ATTEST_SET,1)    "NOOP"
    set cli_set_record(DUT_RESET_DELAYREQ_INTERVAL,ATTEST_SET,2)    "NONE"


#23.
################################################################################
#                                                                              #
#  COMMAND           : RESET_PDELAYREQ_INTERVAL                                #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_PDELAYREQ_INTERVAL                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = pdelayreq_interval minPdelayReqInterval.                #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  DEFINITION        : This function resets the minPdelayReqInterval to the    #
#                      specific PTP port on the DUT.                           #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_RESET_PDELAYREQ_INTERVAL,ATTEST_SET,1)   "NOOP"
    set cli_set_record(DUT_RESET_PDELAYREQ_INTERVAL,ATTEST_SET,2)   "NONE"


#24.
################################################################################
#                                                                              #
#  COMMAND           : RESET_L1SYNC_INTERVAL                                   #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_L1SYNC_INTERVAL                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = l1sync_interval logL1SyncInterval.                      #
#                                  (e.g.,3 )                                   #
#                                                                              #
#  DEFINITION        : This function resets the logL1SyncInterval to a specific#
#                      port on the DUT.                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_RESET_L1SYNC_INTERVAL,ATTEST_SET,1)    "NOOP"
    set cli_set_record(DUT_RESET_L1SYNC_INTERVAL,ATTEST_SET,2)    "NONE"


#25.
################################################################################
#                                                                              #
#  COMMAND           : RESET_L1SYNC_TIMEOUT                                    #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_L1SYNC_TIMEOUT                                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = l1sync_timeout L1SyncReceiptTimeout.                    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  DEFINITION        : This function resets L1SyncReceiptTimeout to a specific #
#                      port on the DUT.                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_RESET_L1SYNC_TIMEOUT,ATTEST_SET,1)     "NOOP"
    set cli_set_record(DUT_RESET_L1SYNC_TIMEOUT,ATTEST_SET,2)     "NONE"


#26.
################################################################################
#                                                                              #
#  COMMAND           : RESET_EGRESS_LATENCY                                    #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_EGRESS_LATENCY                                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = latency     timestampCorrectionPortDS.egressLatency.    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  DEFINITION        : This function resets timestampCorrectionPortDS.         #
#                      egressLatency to a specific port on the DUT.            #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_RESET_EGRESS_LATENCY,ATTEST_SET,1)     "NOOP"
    set cli_set_record(DUT_RESET_EGRESS_LATENCY,ATTEST_SET,2)     "NONE"


#27.
################################################################################
#                                                                              #
#  COMMAND           : RESET_INGRESS_LATENCY                                   #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_INGRESS_LATENCY                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = latency     timestampCorrectionPortDS.ingressLatency.   #
#                                  (e.g., 3)                                   #
#                                                                              #
#  DEFINITION        : This function resets timestampCorrectionPortDS.         #
#                      ingressLatency to a specific port on the DUT.           #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_RESET_INGRESS_LATENCY,ATTEST_SET,1)    "NOOP"
    set cli_set_record(DUT_RESET_INGRESS_LATENCY,ATTEST_SET,2)    "NONE"


#28.
################################################################################
#                                                                              #
#  COMMAND           : RESET_CONSTANT_ASYMMETRY                                #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_CONSTANT_ASYMMETRY                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = constant_asymmetry asymmetryCorrectionPortDS.           #
#                                  constantAsymmetry.                          #
#                                  (e.g., 3)                                   #
#                                                                              #
#  DEFINITION        : This function resets asymmetryCorrectionPortDS.         #
#                      constantAsymmetry to a specific port on the DUT.        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_RESET_CONSTANT_ASYMMETRY,ATTEST_SET,1)   "NOOP"
    set cli_set_record(DUT_RESET_CONSTANT_ASYMMETRY,ATTEST_SET,2)   "NONE"


#29.
################################################################################
#                                                                              #
#  COMMAND           : RESET_SCALED_DELAY_COEFFICIENT                          #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_SCALED_DELAY_COEFFICIENT                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = delay_coefficient asymmetryCorrectionPortDS.            #
#                                  scaledDelayCoefficient.                     #
#                                  (e.g., 3)                                   #
#                                                                              #
#  DEFINITION        : This function resets asymmetryCorrectionPortDS.         #
#                      scaledDelayCoefficient to a specific port on the DUT.   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_RESET_SCALED_DELAY_COEFFICIENT,ATTEST_SET,1)  "NOOP"
    set cli_set_record(DUT_RESET_SCALED_DELAY_COEFFICIENT,ATTEST_SET,2)  "NONE"


#30.
################################################################################
#                                                                              #
#  COMMAND           : RESET_ASYMMETRY_CORRECTION                              #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_ASYMMETRY_CORRECTION                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION       : This function resets the asymmetryCorrectionPortDS.enable#
#                      to the specific PTP port on the DUT.                    #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_RESET_ASYMMETRY_CORRECTION,ATTEST_SET,1)   "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_RESET_ASYMMETRY_CORRECTION,ATTEST_TIME,1)  "30"
    set cli_set_record(DUT_RESET_ASYMMETRY_CORRECTION,ATTEST_SET,2)   "NONE"


#31.
################################################################################
#                                                                              #
#  COMMAND           : EXTERNAL_PORT_CONFIG_DISABLE                            #
#                                                                              #
#  ARRAY INDEX       : DUT_EXTERNAL_PORT_CONFIG_DISABle                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function disables external port configuration      #
#                      option on a specific port at the DUT.                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_EXTERNAL_PORT_CONFIG_DISABLE,ATTEST_SET,1)   "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_EXTERNAL_PORT_CONFIG_DISABLE,ATTEST_TIME,1)  "30"
    set cli_set_record(DUT_EXTERNAL_PORT_CONFIG_DISABLE,ATTEST_SET,2)   "NONE"


#32.
################################################################################
#                                                                              #
#  COMMAND           : RESET_EPC_DESIRED_STATE                                 #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_EPC_DESIRED_STATE                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = desired_state externalPortConfigurationPortDS.          #
#                                  desiredState.                               #
#                                  (e.g., "INITIALIZING"|"FAULTY"|"DISABLED"|  #
#                                         "LISTENING"|"PRE_MASTER"|"MASTER"|   #
#                                         "PASSIVE"|"UNCALIBRATED"|"SLAVE"     #
#                                                                              #
#  DEFINITION      : This function resets given externalPortConfigurationPortDS#
#                      desiredState to a specific port on the DUT.             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_RESET_EPC_DESIRED_STATE,ATTEST_SET,1)    "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_RESET_EPC_DESIRED_STATE,ATTEST_TIME,1)   "30"
    set cli_set_record(DUT_RESET_EPC_DESIRED_STATE,ATTEST_SET,2)    "NONE"


#33.
################################################################################
#                                                                              #
#  COMMAND           : RESET_IPV4_ADDRESS                                      #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_IPV4_ADDRESS                                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = ip_address  IPv4 address.                               #
#                                  (e.g., 192.168.200.1)                       #
#                                                                              #
#   ATTEST_PARAM3    = net_mask    Subnet mask.                                #
#                                  (e.g., 255.255.255.0)                       #
#                                                                              #
#   ATTEST_PARAM4    = vlan_id     VLAN ID.                                    #
#                                  (e.g., 100)                                 #
#                                                                              #
#  DEFINITION        : This function resets given IP address to a specific port#
#                      on the DUT.                                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_RESET_IPV4_ADDRESS,ATTEST_SET,1)     "ifconfig ATTEST_PARAM1 0.0.0.0"
    set cli_set_record(DUT_RESET_IPV4_ADDRESS,ATTEST_SET,2)     "NONE"


#34.
################################################################################
#                                                                              #
#  COMMAND           : SET_TIMESTAMP_CORRECTED_TX_DISABLE                      #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_TIMESTAMP_CORRECTED_TX_DISABLE                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function resets given L1SyncOptParamsPortDS.       #
#                      timestampsCorrectedTx option to a specific port on the  #
#                      DUT.                                                    #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(DUT_TIMESTAMP_CORRECTED_TX_DISABLE,ATTEST_SET,1)    "/etc/init.d/ppsi.sh restart"
    set cli_set_record(DUT_TIMESTAMP_CORRECTED_TX_DISABLE,ATTEST_TIME,1)   "30"
    set cli_set_record(DUT_TIMESTAMP_CORRECTED_TX_DISABLE,ATTEST_SET,2)    "NONE"

#35.
################################################################################
#                                                                              #
#  COMMAND           : WRS_RESTART_PPSI                                        #
#                                                                              #
#  ARRAY INDEX       : WRS_RESTART_PPSI                                        #
#                                                                              #
#  INPUT PARAMETERS  : NONE                                                    #
#                                                                              #
#  DEFINITION        : This function restarts PPSi module of WRS device.       #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_set_record(WRS_RESTART_PPSI,ATTEST_SET,1)               "/etc/init.d/ppsi.sh restart"
    set cli_set_record(WRS_RESTART_PPSI,ATTEST_TIME,1)              "30"
    set cli_set_record(WRS_RESTART_PPSI,ATTEST_SET,2)               "NONE"
}


################################################################################
#                           DUT CHECK FUNCTIONS                                #
################################################################################

proc tee_get_dut_ptp_ha_cli_from_file {} {

    global cli_get_record
    global ptp_ha_env

#1.
################################################################################
#                                                                              #
#  COMMAND           : CHECK_L1SYNC_STATE                                      #
#                                                                              #
#  ARRAY INDEX       : DUT_CHECK_L1SYNC_STATE                                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function verifies the given L1 Sync State in a     #
#                      specific port.                                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_get_record(DUT_CHECK_L1SYNC_STATE,ATTEST_OUTPUT,1)    "wr_mon"
    set cli_get_record(DUT_CHECK_L1SYNC_STATE,ATTEST_WAIT,1)      "*"
    set cli_get_record(DUT_CHECK_L1SYNC_STATE,ATTEST_GET,2)       "q"
    set cli_get_record(DUT_CHECK_L1SYNC_STATE,ATTEST_GET,3)       "NONE"


#2.
################################################################################
#                                                                              #
#  COMMAND           : CHECK_PTP_PORT_STATE                                    #
#                                                                              #
#  ARRAY INDEX       : DUT_CHECK_PTP_PORT_STATE                                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function verifies the given portState in a specific#
#                      port.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_get_record(DUT_CHECK_PTP_PORT_STATE,ATTEST_OUTPUT,1)  "wrs_dump_shmem | egrep 'cfg.iface_name:|.info.state'"
    set cli_get_record(DUT_CHECK_PTP_PORT_STATE,ATTEST_WAIT,1)    "*wrs*"
    set cli_get_record(DUT_CHECK_PTP_PORT_STATE,ATTEST_GET,2)     "NONE"


#3.
################################################################################
#                                                                              #
#  COMMAND           : CHECK_ASYMMETRY_CORRECTION                              #
#                                                                              #
#  ARRAY INDEX       : DUT_CHECK_ASYMMETRY_CORRECTION                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function verifies the given state of               #
#                      asymmetryCorrectionPortDS.enable for a specific port.   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_get_record(DUT_CHECK_ASYMMETRY_CORRECTION,ATTEST_OUTPUT,1) "wrs_dump_shmem | egrep 'info.asymmetryCorrectionPortDS.enable:'"
    set cli_get_record(DUT_CHECK_ASYMMETRY_CORRECTION,ATTEST_WAIT,1)   "*wrs*"
    set cli_get_record(DUT_CHECK_ASYMMETRY_CORRECTION,ATTEST_GET,2)    "NONE"


#4.
################################################################################
#                                                                              #
#  COMMAND           : CHECK_EGRESS_LATENCY                                    #
#                                                                              #
#  ARRAY INDEX       : DUT_CHECK_EGRESS_LATENCY                                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      timestampCorrectionPortDS.egressLatency for a specific  #
#                      port.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_get_record(DUT_CHECK_EGRESS_LATENCY,ATTEST_OUTPUT,1)  "wrs_dump_shmem | egrep 'info.timestampCorrectionPortDS.egressLatency'"
    set cli_get_record(DUT_CHECK_EGRESS_LATENCY,ATTEST_WAIT,1)    "*wrs*"
    set cli_get_record(DUT_CHECK_EGRESS_LATENCY,ATTEST_GET,2)     "NONE"


#5.
################################################################################
#                                                                              #
#  COMMAND           : CHECK_INGRESS_LATENCY                                   #
#                                                                              #
#  ARRAY INDEX       : DUT_CHECK_INGRESS_LATENCY                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      timestampCorrectionPortDS.ingressLatency for a specific #
#                      port.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_get_record(DUT_CHECK_INGRESS_LATENCY,ATTEST_OUTPUT,1) "wrs_dump_shmem | egrep 'info.timestampCorrectionPortDS.ingressLatency'"
    set cli_get_record(DUT_CHECK_INGRESS_LATENCY,ATTEST_WAIT,1)   "*wrs*"
    set cli_get_record(DUT_CHECK_INGRESS_LATENCY,ATTEST_GET,2)    "NONE"


#6.
################################################################################
#                                                                              #
#  COMMAND           : CHECK_CONSTANT_ASYMMETRY                                #
#                                                                              #
#  ARRAY INDEX       : DUT_CHECK_CONSTANT_ASYMMETRY                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      asymmetryCorrectionPortDS.constantAsymmetry for a       #
#                      specific port.                                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_get_record(DUT_CHECK_CONSTANT_ASYMMETRY,ATTEST_OUTPUT,1)  "wrs_dump_shmem | egrep 'info.asymmetryCorrectionPortDS.constantAsymmetry:'"
    set cli_get_record(DUT_CHECK_CONSTANT_ASYMMETRY,ATTEST_WAIT,1)    "*wrs*"
    set cli_get_record(DUT_CHECK_CONSTANT_ASYMMETRY,ATTEST_GET,2)     "NONE"


#7.
################################################################################
#                                                                              #
#  COMMAND           : CHECK_SCALED_DELAY_COEFFICIENT                          #
#                                                                              #
#  ARRAY INDEX       : DUT_CHECK_SCALED_DELAY_COEFFICIENT                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      asymmetryCorrectionPortDS.scaledDelayCoefficient for a  #
#                      specific port.                                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_get_record(DUT_CHECK_SCALED_DELAY_COEFFICIENT,ATTEST_OUTPUT,1)  "wrs_dump_shmem | egrep 'info.asymmetryCorrectionPortDS.scaledDelayCoefficient'"
    set cli_get_record(DUT_CHECK_SCALED_DELAY_COEFFICIENT,ATTEST_WAIT,1)    "*wrs*"
    set cli_get_record(DUT_CHECK_SCALED_DELAY_COEFFICIENT,ATTEST_GET,2)     "NONE"


#8.
################################################################################
#                                                                              #
#  COMMAND           : CHECK_EXTERNAL_PORT_CONFIG                              #
#                                                                              #
#  ARRAY INDEX       : DUT_CHECK_EXTERNAL_PORT_CONFIG                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function verifies the given state of external      #
#                      port configuration for a specific port.                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_get_record(DUT_CHECK_EXTERNAL_PORT_CONFIG,ATTEST_OUTPUT,1) "wrs_dump_shmem | grep externalPortConfigurationEnabled:"
    set cli_get_record(DUT_CHECK_EXTERNAL_PORT_CONFIG,ATTEST_GET,2)    "NONE"


#9.
################################################################################
#                                                                              #
#  COMMAND           : CHECK_SLAVE_ONLY                                        #
#                                                                              #
#  ARRAY INDEX       : DUT_CHECK_SLAVE_ONLY                                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = value      DUT's slave_only value                       #
#                                  (e.g., "TRUE"|"FALSE")                      #
#                                                                              #
#  DEFINITION        : This function verifies the given value of defaultDS.    #
#                      slaveOnly port configuration for DUT.                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_get_record(DUT_CHECK_SLAVE_ONLY,ATTEST_OUTPUT,1) "wrs_dump_shmem | grep slaveOnly:"
    set cli_get_record(DUT_CHECK_SLAVE_ONLY,ATTEST_WAIT,1)   "*wrs*"
    set cli_get_record(DUT_CHECK_SLAVE_ONLY,ATTEST_GET,2)    "NONE"


#10.
################################################################################
#                                                                              #
#  COMMAND           : CHECK_MASTER_ONLY                                       #
#                                                                              #
#  ARRAY INDEX       : DUT_CHECK_MASTER_ONLY                                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = value       DUT's master_only value                     #
#                                  (e.g., "TRUE"|"FALSE")                      #
#                                                                              #
#  DEFINITION        : This function verifies the given value of portDS.       #
#                      masterOnly port configuration for a specific port.      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_get_record(DUT_CHECK_MASTER_ONLY,ATTEST_OUTPUT,1) "wrs_dump_shmem | egrep '.portDS.masterOnly:'"
    set cli_get_record(DUT_CHECK_MASTER_ONLY,ATTEST_WAIT,1)   "*wrs*"
    set cli_get_record(DUT_CHECK_MASTER_ONLY,ATTEST_GET,2)    "NONE"


#11.
################################################################################
#                                                                              #
#  COMMAND           : CHECK_OFFSET_FROM_MASTER                                #
#                                                                              #
#  ARRAY INDEX       : DUT_CHECK_OFFSET_FROM_MASTER                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = offset_from_master currentDS.offsetFromMaster.          #
#                                         (e.g., 100ns)                        #
#                                                                              #
#  DEFINITION        : This function checks absolute value of currentDS.       #
#                      offsetFromMaster in DUT.                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_get_record(DUT_CHECK_OFFSET_FROM_MASTER,ATTEST_OUTPUT,1) "wrs_dump_shmem | grep offsetFromMaster:"
    set cli_get_record(DUT_CHECK_OFFSET_FROM_MASTER,ATTEST_WAIT,1)   "*wrs*"
    set cli_get_record(DUT_CHECK_OFFSET_FROM_MASTER,ATTEST_GET,2)    "NONE"


################################################################################
#                           DUT GET FUNCTIONS                                  #
################################################################################


#1.
################################################################################
#                                                                              #
#  COMMAND           : GET_TIMESTAMP                                           #
#                                                                              #
#  ARRAY INDEX       : DUT_GET_TIMESTAMP                                       #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  DEFINITION        : This function fetch timestamp from DUT.                 #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  timestamp         : (Mandatory) Timestamp.                                  #
#                                  (e.g., xxxxx)                               #
#                                                                              #
################################################################################

    set cli_get_record(DUT_GET_TIMESTAMP,ATTEST_OUTPUT,1)         "date +%s%N; wr_date get"
    set cli_get_record(DUT_GET_TIMESTAMP,ATTEST_GET,2)            "NONE"


#2.
################################################################################
#                                                                              #
#  COMMAND           : GET_MEAN_DELAY                                          #
#                                                                              #
#  ARRAY INDEX       : DUT_GET_MEAN_DELAY                                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function extracts currentDS.meanDelay of a specific#
#                      port from the DUT.                                      #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  mean_delay        : (Mandatory) currentDS.meanDelay.                        #
#                                  (e.g., 3)                                   #
#                                                                              #
################################################################################

    set cli_get_record(DUT_GET_MEAN_DELAY,ATTEST_OUTPUT,1)    "wr_date get; wrs_dump_shmem | egrep 'currentDS.meanDelay:'"
    set cli_get_record(DUT_GET_MEAN_DELAY,ATTEST_WAIT,1)      "wrs*"
    set cli_get_record(DUT_GET_MEAN_DELAY,ATTEST_GET,2)       "NONE"


#3.
################################################################################
#                                                                              #
#  COMMAND           : GET_MEAN_LINK_DELAY                                     #
#                                                                              #
#  ARRAY INDEX       : DUT_GET_MEAN_LINK_DELAY                                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function extracts portDS.meanLinkDelay of a        #
#                      specific port from the DUT.                             #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  mean_link_delay   : (Mandatory) portDS.meanLinkDelay.                       #
#                                  (e.g., 3)                                   #
#                                                                              #
################################################################################

    set cli_get_record(DUT_GET_MEAN_LINK_DELAY,ATTEST_OUTPUT,1) "wr_date get; wrs_dump_shmem | egrep 'portDS.meanLinkDelay:'"
    set cli_get_record(DUT_GET_MEAN_LINK_DELAY,ATTEST_WAIT,1)   "wrs*"
    set cli_get_record(DUT_GET_MEAN_LINK_DELAY,ATTEST_GET,2)    "NONE"


#4.
################################################################################
#                                                                              #
#  COMMAND           : GET_DELAY_ASYMMETRY                                     #
#                                                                              #
#  ARRAY INDEX       : DUT_GET_DELAY_ASYMMETRY                                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function extracts portDS.delayAsymmetry of a       #
#                      specific port from the DUT.                             #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  delay_asymmetry   : (Mandatory) portDS.delayAsymmetry.                      #
#                                  (e.g., 3)                                   #
#                                                                              #
################################################################################

    set cli_get_record(DUT_GET_DELAY_ASYMMETRY,ATTEST_OUTPUT,1)   "wrs_dump_shmem | egrep 'portDS.delayAsymmetry:'"
    set cli_get_record(DUT_GET_DELAY_ASYMMETRY,ATTEST_WAIT,1)     "wrs*"
    set cli_get_record(DUT_GET_DELAY_ASYMMETRY,ATTEST_GET,2)      "NONE"
}

#1.
################################################################################
# PROCEDURE NAME    : doScp                                                    #
#                                                                              #
# DEFINITION        : This is internal function used for copying ppsi.conf to  #
#                     DUT                                                      #
#                                                                              #
################################################################################

proc doScp {from to {password rootroot}} {

    spawn scp $from $to

    set scp_session $spawn_id

    expect {

        -i $scp_session

        "password:" {
            send "$password"

            expect {
                -i $scp_session

                    "denied" {
                        printLog "Invalid password"
                    }
#               eof {printLog "(\"$from\" to \"$to\") copied successfully."}
                timeout {printLog "Connection timed out while copying file."}

            }
        }
        "yes/no" {
            send "yes\n"
            exp_continue
        }
        eof {printLog "SCP: Unable to establish connection to DUT."}
        timeout {printLog "SCP: Connection timed out."}
        default {printLog "SCP: Cannot connect."}
    }

}


#2.
################################################################################
# PROCEDURE NAME    : delEmptyEof                                              #
#                                                                              #
# DEFINITION        : This is internal function used for deleting empty lines  #
#                     at EOF                                                   #
#                                                                              #
################################################################################

proc delEmptyEof { config } {

    set read_fd [open $config "r"]
    set read_content [read $read_fd]
    close $read_fd

    set empty_line      ""
    set write_content   ""

    foreach line [split $read_content "\n"] {
        if {$line == ""} {
            append empty_line "\n"
        } else {
            append write_content $empty_line
            append write_content "$line\n"
            set empty_line ""
        }
    }

    set write_fd [open $config "w+"]
    puts $write_fd $write_content
    close $write_fd

    return 1
}


#3.
################################################################################
# PROCEDURE NAME    : getPortName                                              #
#                                                                              #
# DEFINITION        : This is internal function used for retrieving port name  #
#                     from the iface given.                                    #
#                                                                              #
################################################################################

proc getPortName {conf iface} {

    foreach line [split $conf "\n"] {

        if {[regexp {(port)\s+(.+)} $line match a port_retrieved]} { # do nothing }

        if {[regexp {(iface)\s+(.+)} $line match a iface_retrieved]} {
            if {$iface == $iface_retrieved} {
                return $port_retrieved
            }
        }
    }

    return 0
}


#4.
################################################################################
# PROCEDURE NAME    : cleanJunk                                                #
#                                                                              #
# DEFINITION        : This is internal function used for cleaning special      #
#                     characters in the output of wr_mon.                      #
#                                                                              #
################################################################################

proc cleanJunk {input output} {

    upvar $output myoutput

    set     myoutput    ""

    regsub -all {\[01;31m} $input "" match
    regsub -all {\[01;32m} $match "" match
    regsub -all {\[01;34m} $match "" match
    regsub -all {\[01;36m} $match "" match
    regsub -all {\[01;39m} $match "" match
    regsub -all {\[02;39m} $match "" match
    regsub -all {\[0m} $match "" match
    regsub -all {; \[1;1f} $match "" match
    regsub -all {\[2J\[1} $match "" match
    regsub -all {1;1f} $match "" match
    regsub -all {2m} $match "" match
    regsub -all {4m} $match "" match
    regsub -all {6m} $match "" match
    regsub -all {9m} $match "" match
    regsub -all {\[} $match "" match
    regsub -all {;} $match "" match
    regsub -all {2J} $match "" match

    set myoutput $match

    return 1
}


#5.
################################################################################
# PROCEDURE NAME    : printLog                                                 #
#                                                                              #
# DEFINITION        : This is internal function used for print logs of         #
#                     configuration parameter.                                 #
#                                                                              #
################################################################################

proc printLog {message} {

    puts "CLI: INFO: $message"
    return 1
}


#6.
################################################################################
# PROCEDURE NAME    : removeDecimal                                            #
#                                                                              #
# DEFINITION        : This is internal function used for remove zero after .   #
#                     in number.                                               #
#                                                                              #
################################################################################

proc removeDecimal { number } {

    set myNumber [split $number "."]

    if {([lindex $myNumber 1] == 0)} {
        return [lindex $myNumber 0]
    }

    return $number
}



################################################################################
#                            DUT SET CALLBACK                                  #
################################################################################


#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_port_enable                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) Port to be enabled.                         #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If port is enabled)                       #
#                                                                              #
#                      0 on Failure (If port could not be enabled)             #
#                                                                              #
#  DEFINITION        : This function enables a specific port on the DUT.       #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_port_enable {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#        if {![Send_cli_command \
#                        -session_handler        $session_handler \
#                        -cmd_type               "DUT_PORT_ENABLE"\
#                        -parameter1              $port_num]} {
#
#            return 0
#        }

    return 2
}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_port_ptp_enable                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) Port on which ptp is to be enabled.         #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is enabled on the specified port   #
#                                    at the DUT).                              #
#                                                                              #
#                      0 on Failure (If PTP could not be enabled on the        #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function enables PTP on a specific port on the DUT.#
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_port_ptp_enable {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    ## TRANSPORT TYPE CONFIG ###
    array set TRANS_TYPE {
        "IEEE 802.3"    "raw"
        "UDP/IPv4"      "udp"
    }

    set network_protocol_from_ui    [set TRANS_TYPE($::ptp_transport_type)]

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set port_name [getPortName $config $port_num]

    set new_config ""

    foreach line [split $config "\n"] {

        if {[regexp {(iface)\s+([a-zA-Z0-9]+)} $line match a port_retrieved]} {# Do nothing.}

        if {[regexp {(port)\s+(.+)} $line match a port_retrieved]} {# Do nothing.}

        #set default value
        if {[string match "*proto*" $line]} {
            if {$port_name == $port_retrieved} {
                append new_config "proto $network_protocol_from_ui\n"
                printLog "($port_retrieved) proto $network_protocol_from_ui"
                continue
            }
        }

        #set default announce interval
        if {[string match "*logAnnounceInterval*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "logAnnounceInterval $::ptp_announce_interval\n"
                printLog "($port_retrieved) logAnnounceInterval $::ptp_announce_interval"
                continue
            }
        }

        #set default announce timeout 
        if {[string match "*announceReceiptTimeout*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "announceReceiptTimeout $::ptp_announce_timeout\n"
                printLog "($port_retrieved) announceReceiptTimeout $::ptp_announce_timeout"
                continue
            }
        }

        #set default sync interval
        if {[string match "logSyncInterval*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "logSyncInterval $::ptp_sync_interval\n"
                printLog "($port_retrieved) logSyncInterval $::ptp_sync_interval"
                continue
            }
        }

        #set default L1Sync interval
        if {[string match "*logL1SyncInterval*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "logL1SyncInterval $::ptp_l1sync_interval\n"
                printLog "($port_retrieved) logL1SyncInterval $::ptp_l1sync_interval"
                continue
            }
        }

        #set default l1sync timeout
        if {[string match "*l1SyncReceiptTimeout*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "l1SyncReceiptTimeout $::ptp_l1sync_timeout\n"
                printLog "($port_retrieved) l1SyncReceiptTimeout $::ptp_l1sync_timeout"
                continue
            }
        }

        #set default logMinDelayReqInterval
        if {[string match "*logMinDelayReqInterval*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "logMinDelayReqInterval $::ptp_delayreq_interval\n"
                printLog "($port_retrieved) logMinDelayReqInterval $::ptp_delayreq_interval"
                continue
            }
        }

        #set default logMinPDelayReqInterval
        if {[string match "*logMinPDelayReqInterval*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "logMinPDelayReqInterval $::ptp_pdelayreq_interval\n"
                printLog "($port_retrieved) logMinPDelayReqInterval $::ptp_pdelayreq_interval"
                continue
            }
        }

        #set default desiredState value
        if {[string match "*desiredState*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "desiredState passive\n"
                printLog "($port_retrieved) desiredState passive"
                continue
            }
        }

        #set default egress latency value
        set latency [expr $::ptp_egress_latency * pow(10,3)]
        set latency [removeDecimal $latency]

        if {[string match "*egressLatency*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "egressLatency $latency\n"
                printLog "($port_retrieved) egressLatency $latency"
                continue
            }
        }

        #set default ingress latency value
        set latency [expr $::ptp_ingress_latency * pow(10,3)]
        set latency [removeDecimal $latency]

        if {[string match "*ingressLatency*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "ingressLatency $latency\n"
                printLog "($port_retrieved) ingressLatency $latency"
                continue
            }
        }

        #set default constant asymmetry value
        set constant_asymmetry  [expr $::ptp_constant_asymmetry * pow(10,3)]
        set constant_asymmetry  [removeDecimal $constant_asymmetry]

        if {[string match "*constantAsymmetry*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "constantAsymmetry $constant_asymmetry\n"
                printLog "($port_retrieved) constantAsymmetry $constant_asymmetry"
                continue
            }
        }

        #set default delay coefficient
        if {[string match "*delayCoefficient*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "delayCoefficient $::ptp_delay_coefficient\n"
                printLog "($port_retrieved) delayCoefficient $::ptp_delay_coefficient"
                continue
            }
        }

        #set default profile
        if {[string match "*profile*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "profile ha\n"
                printLog "($port_retrieved) profile ha"
                continue
            }
        }

        #set default l1SyncEnabled
        if {[string match "*l1SyncEnabled*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "l1SyncEnabled y\n"
                printLog "($port_retrieved) l1SyncEnabled y"
                continue
            }
        }

        #set default l1SyncOptParamsEnabled
        if {[string match "*l1SyncOptParamsEnabled*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "l1SyncOptParamsEnabled n\n"
                printLog "($port_retrieved) l1SyncOptParamsEnabled n"
                continue
            }
        }

        #set default l1SyncTimestampsCorrectedTxEnabled 
        if {[string match "*l1SyncTimestampsCorrectedTxEnabled*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "l1SyncTimestampsCorrectedTxEnabled y\n"
                printLog "($port_retrieved) l1SyncTimestampsCorrectedTxEnabled y"
                continue
            }
        }
        #set default masterOnly 
        if {[string match "*masterOnly*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "masterOnly n\n"
                printLog "($port_retrieved) masterOnly n"
                continue
            }
        }

        #unset VLAN
        if {[string match "*vlan*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "#vlan is not set\n"
                printLog "($port_retrieved) #vlan is not set"
                continue
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_PORT_PTP_ENABLE"]

    return $result
}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_set_vlan                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID with which VLAN has to be created.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#  port_num          : (Mandatory) Port which has to be associated to VLAN ID. #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If VLAN ID is created and port is         #
#                                    associated to it on the DUT).             #
#                                                                              #
#                      0 on Failure (If VLAN ID could not be created or port   #
#                                    is not associated to it on the DUT).      #
#                                                                              #
#  DEFINITION        : This function creates VLAN ID and associates given port #
#                      to it on the DUT.                                       #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_set_vlan {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set vlan_id         $param(-vlan_id)
    set port_num        $param(-port_num)

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    set port_name [getPortName $config $port_num]

    foreach line [split $config "\n"] {

        if {[regexp {(port)\s+(.+)} $line match a port_retrieved]} { }

        #set default value
        if {[string match "*vlan*" $line]} {
            if {$port_name == $port_retrieved} {
                append new_config "vlan $vlan_id\n"
                printLog "($port_retrieved) vlan $vlan_id"
                continue
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_SET_VLAN"]

    return $result
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_set_vlan_priority                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID with which VLAN has to be created.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#  vlan_priority     : (Mandatory) VLAN priority to be associated              #
#                                  (e.g., 1)                                   #
#                                                                              #
#  port_num          : (Mandatory) Port with VLAN ID configured.               #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If VLAN priority is associated0           #
#                                                                              #
#                      0 on Failure (If VLAN priority could not be associated) #
#                                                                              #
#  DEFINITION        : This function associates given priority to VLAN         #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_set_vlan_priority {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set vlan_id         $param(-vlan_id)
    set vlan_priority   $param(-vlan_priority)
    set port_num        $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler         $session_handler \
#                       -cmd_type                "DUT_SET_VLAN_PRIORITY"\
#                       -parameter1              $vlan_priority\
#                       -parameter2              $port_num]} {
#
#           return 0
#       }

    return 2
}


#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_global_ptp_enable                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is enabled on the DUT).            #
#                                                                              #
#                      0 on Failure (If PTP could not be enabled on the DUT).  #
#                                                                              #
#  DEFINITION        : This function enables PTP on the DUT.                   #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_global_ptp_enable {args} {

    array set param $args

    set session_handler $param(-session_handler)

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        #set default domain number
        if {[string match "*domain-number*" $line]} {
            append new_config "domain-number $::ptp_dut_default_domain_number\n"
            printLog "domain-number $::ptp_dut_default_domain_number"
            continue
        }
        
        #set default priority1 value
        if {[string match "*priority1*" $line]} {
            append new_config "priority1 $::ptp_dut_default_priority1\n"
            printLog "priority1 $::ptp_dut_default_priority1"
            continue
        }

        #set default priority2 value
        if {[string match "*priority2*" $line]} {
            append new_config "priority2 $::ptp_dut_default_priority2\n"
            printLog "priority2 $::ptp_dut_default_priority2"
            continue
        }

        #set default externalPortConfigurationEnabled value
        if {[string match "*externalPortConfigurationEnabled*" $line]} {
            append new_config "externalPortConfigurationEnabled n\n"
            printLog "externalPortConfigurationEnabled n"
            continue
        }

        #set default slaveOnly value
        if {[string match "*slaveOnly*" $line]} {
            append new_config "slaveOnly n\n"
            printLog "slaveOnly n"
            continue
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_GLOBAL_PTP_ENABLE"]

    return $result
}


#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_set_communication_mode             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  ptp_version       : (optional)  PTP version.                                #
#                                  (Default: 2)                                #
#                                                                              #
#  communication_mode : (Mandatory) Communication mode                         #
#                                  (e.g., "Unicast"|"Multicast")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If communication mode is set on the       #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If communication mode could not be        #
#                                    set on the specified port at the DUT).    #
#                                                                              #
#  DEFINITION        : This function sets the communication mode on a specific #
#                      port of the DUT.                                        #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_set_communication_mode {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set clock_mode          $param(-clock_mode)
    set communication_mode  $param(-communication_mode)
    set ptp_version         $param(-ptp_version)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_SET_COMMUNICATION_MODE"\
#                       -parameter1             $port_num\
#                       -parameter2             $clock_mode\
#                       -parameter3             $ptp_version\
#                       -parameter4             $communication_mode]} {
#           return 0
#       }

    return 2
}


#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_set_domain                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  domain            : (Mandatory) Domain number.                              #
#                                  (e.g., 0..255)                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If domain number is set on the DUT).      #
#                                                                              #
#                      0 on Failure (If domain number could not able to set on #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function sets domain number on the DUT.            #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_set_domain {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set clock_mode      $param(-clock_mode)
    set domain          $param(-domain)

    global group_name case_id

    if {!((($group_name == "pcg") && ($case_id == "002")) || (($group_name == "mhg") && ($case_id == "003"))) } {

#       printLog "Skipping this configuration as this is already done in global configuration."
        return 1
    }

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        #set default value
        if {[string match "*domain-number*" $line]} {
            append new_config "domain-number $domain\n"
            printLog "domain-number $domain"
            continue
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_SET_DOMAIN"]

    return $result
}


#8.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_set_network_protocol               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) clock_mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  network_protocol  : (Mandatory) Network protocol.                           #
#                                  (e.g., "IEEE 802.3"|"UDP/IPv4")             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If network protocol is set on specified   #
#                                    port at the DUT).                         #
#                                                                              #
#                      0 on Failure (If network protocol could not be set      #
#                                    on specified port at the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets the network protocol on a specific   #
#                      port of the DUT.                                        #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_set_network_protocol {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set clock_mode          $param(-clock_mode)
    set network_protocol    $param(-network_protocol)

    global group_name case_id

#   printLog "Skipping this configuration as this is already done in global configuration."
    return 1

    ## TRANSPORT TYPE CONFIG ###
    array set TRANS_TYPE {
        "IEEE 802.3"    "raw"
        "UDP/IPv4"      "udp"
    }

    set network_protocol_from_ui    [set TRANS_TYPE($network_protocol)]

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    set port_name [getPortName $config $port_num]

    foreach line [split $config "\n"] {

        if {[regexp {(port)\s+(.+)} $line match a port_retrieved]} { }

        #set default value
        if {[string match "*proto*" $line]} {
            if {$port_name == $port_retrieved} {
                append new_config "proto $network_protocol_from_ui\n"
                printLog "($port_retrieved) proto $network_protocol_from_ui"
                continue
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler  $session_handler\
                -cmd_type         "DUT_SET_NETWORK_PROTOCOL"]

    return $result
}


#9.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_set_clock_mode                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP clock mode is set on specified port#
#                                    of the DUT).                              #
#                                                                              #
#                      0 on Failure (If PTP clock mode could not be set on     #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function sets clock mode on a specific port of DUT.#
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_set_clock_mode {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set clock_mode          $param(-clock_mode)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_SET_CLOCK_MODE"\
#                       -parameter1             $port_num\
#                       -parameter2             $clock_mode]} {
#           return 0
#       }

    return 2
}


#10.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_set_clock_step                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  clock_step        : (Mandatory) Clock step.                                 #
#                                  (e.g., "One-step"|"Two-step")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If clock-step is set on the specified     #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If clock-step could not be able to set    #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets the clock-step on a specific port on #
#                      the DUT.                                                #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_set_clock_step {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set clock_mode          $param(-clock_mode)
    set clock_step          $param(-clock_step)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_SET_CLOCK_STEP"\
#                       -parameter1             $port_num\
#                       -parameter1             $clock_mode\
#                       -parameter3             $clock_step]} {
#           return 0
#       }

    return 2
}


#11.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_set_delay_mechanism                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  delay_mechanism   : (Mandatory) Delay mechanism.                            #
#                                  (e.g., e2e|p2p|disable)                     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If delay mechanism is set on specified    #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If delay mechanism could not be set on    #
#                                    specified PTP port of the DUT).           #
#                                                                              #
#  DEFINITION        : This function sets the delay mechanism to a specific    #
#                      port on the DUT.                                        #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_set_delay_mechanism {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set clock_mode          $param(-clock_mode)
    set delay_mechanism     $param(-delay_mechanism)

    array set mechanism {
        "Delay Request-Response"    "e2e"
        "Peer-to-Peer Delay"        "p2p"
    }

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        if {[regexp {(iface)\s+([a-zA-Z0-9]+)} $line match a port_retrieved]} { }

        #set default value
        if {[string match "*mechanism*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "mechanism [set mechanism($delay_mechanism)]\n"
                printLog "($port_retrieved) mechanism [set mechanism($delay_mechanism)]"
                continue
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler  $session_handler\
                -cmd_type         "DUT_SET_DELAY_MECHANISM"]

    return $result
}


#12.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_set_priority1                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority1         : (Mandatory) Prority1                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority1 is set on the DUT).          #
#                                                                              #
#                      0 on Failure (If priority1 could not be able to set on  #
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function sets the priority1 on the DUT.            #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_set_priority1 {args} {

    array set param $args

    set session_handler  $param(-session_handler)
    set clock_mode       $param(-clock_mode)
    set priority1        $param(-priority1)

    global group_name case_id

#   printLog "Skipping this configuration as this is already done in global configuration."
    return 1

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        #set default value
        if {[string match "*priority1*" $line]} {
            append new_config "priority1 $priority1\n"
            printLog "priority1 $priority1"
            continue
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_SET_PRIORITY1"]

    return $result
}


#13.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_set_priority2                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority2         : (Mandatory) Prority2                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority2 is set on the DUT).          #
#                                                                              #
#                      0 on Failure (If priority2 could not be able to set on  #
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function sets the priority2 on the DUT.            #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_set_priority2 {args} {

    array set param $args

    set session_handler  $param(-session_handler)
    set clock_mode       $param(-clock_mode)
    set priority2        $param(-priority2)

    global group_name case_id

#   printLog "Skipping this configuration as this is already done in global configuration."
    return 1

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        #set default value
        if {[string match "*priority2*" $line]} {
            append new_config "priority2 $priority2\n"
            printLog "priority2 $priority2"
            continue
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file


    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_SET_PRIORITY2"]

    return $result
}


#14.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::callback_cli_tcr_rcr_cr_enable                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If peerTxCoherentIsRequired,              #
#                                    peerRxCoherentIsRequired and              #
#                                    peerCongruentIsRequired is enabled on the #
#                                    DUT).                                     #
#                                                                              #
#                      0 on Failure (If peerTxCoherentIsRequired,              #
#                                    peerRxCoherentIsRequired and              #
#                                    peerCongruentIsRequired could not be      #
#                                    enabled on the DUT).                      #
#                                                                              #
#                                                                              #
#  DEFINITION        : This function enables peerTxCoherentIsRequired,         #
#                      peerRxCoherentIsRequired and peerCongruentIsRequired on #
#                      the DUT.                                                #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_tcr_rcr_cr_enable {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_TCR_RCR_CR_ENABLE"\
#                       -parameter1             $port_num]} {
#           return 0
#       }

    return 2
}


#15.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_l1sync_enable                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If L1Sync option is enabled on the        #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If L1Sync option could not be enabled on  #
#                                    the specified port of the DUT).           #
#                                                                              #
#  DEFINITION        : This function enables L1Sync option on a specific port  #
#                      at the DUT.                                             #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_l1sync_enable {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)

    global group_name case_id

    if {!(($group_name == "smg") && ($case_id == "002")) } {

#       printLog "Skipping this configuration as this is already done in global configuration."
        return 1
    }

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        if {[regexp {(iface)\s+([a-zA-Z0-9]+)} $line match a port_retrieved]} { }

        #set default value
        if {[string match "*profile*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "profile ha\n"
                printLog "($port_retrieved) profile ha"
                continue
            }
        }

        #Enable l1SyncEnabled
        if {[string match "*l1SyncEnabled*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "l1SyncEnabled y\n"
                printLog "($port_retrieved) l1SyncEnabled y"
                continue
            }
        }
        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler  $session_handler\
                -cmd_type         "DUT_L1SYNC_ENABLE"]

    return $result
}


#16.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_l1sync_opt_params_enable           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If extended format of L1Sync TLV is       #
#                                    enabled on the specified port at the DUT).#
#                                                                              #
#                      0 on Failure (If extended format of L1Sync TLV could not#
#                                    be enabled on the specified port at the   #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function enables extended format of L1Sync TLV on  #
#                      the specified port at the DUT.                          #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_l1sync_opt_params_enable {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)

    global group_name case_id

    if {!((($group_name == "mfg") && ($case_id == "002")) || (($group_name == "mfg") && ($case_id == "004")) ||\
          (($group_name == "opv") && ($case_id == "001"))) } {

#       printLog "Skipping this configuration as this is already done in global configuration."
        return 1
    }

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        if {[regexp {(iface)\s+([a-zA-Z0-9]+)} $line match a port_retrieved]} { }

        #set default value
        if {[string match "*profile*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "profile custom\n"
                printLog "($port_retrieved) profile custom"
                continue
            }
        }

        #Enable l1SyncOptParamsEnabled
        if {[string match "*l1SyncOptParamsEnabled*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "l1SyncOptParamsEnabled y\n"
                printLog "($port_retrieved) l1SyncOptParamsEnabled y"
                continue
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler  $session_handler\
                -cmd_type         "DUT_L1SYNC_OPT_PARAMS_ENABLE"]

    return $result
}


#17.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_slaveonly_enable                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If slave-only is enabled on the           #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If slave-only could not be enabled on the #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function enables slave-only on the specified port  #
#                      at the DUT.                                             #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_slaveonly_enable {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)

    global group_name case_id

    if {!((($group_name == "peg") && ($case_id == "014")) || (($group_name == "peg") && ($case_id == "022"))) } {

#       printLog "Skipping this configuration as this is already done in global configuration."
        return 1
    }

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        #set default value
        if {[string match "*slaveOnly*" $line]} {
            append new_config "slaveOnly y\n"
            printLog "slaveOnly y"
            continue
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_SLAVEONLY_ENABLE"]

    return $result
}


#18.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_masteronly_enable                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If master-only is enabled on the          #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If master-only could not be enabled on the#
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function enables master-only on the specified port #
#                      at the DUT.                                             #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_masteronly_enable {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)

    global group_name case_id

    if {!((($group_name == "pcg") && ($case_id == "012")) || (($group_name == "pcg") && ($case_id == "013")) ||\
          (($group_name == "peg") && ($case_id == "015")) || (($group_name == "peg") && ($case_id == "023")))} {

#       printLog "Skipping this configuration as this is already done in global configuration."
        return 1
    }

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        if {[regexp {(iface)\s+([a-zA-Z0-9]+)} $line match a port_retrieved]} { }

        #set default value
        if {[string match "*masterOnly*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "masterOnly y\n"
                printLog "($port_retrieved) masterOnly y"
                continue
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_MASTERONLY_ENABLE"]

    return $result
}


#19.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::callback_cli_set_announce_interval             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  announce_interval : (Mandatory) announceInterval.                           #
#                                  (e.g., 1|2|4|8|16)                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If announceInterval value is set on       #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If announceInterval value could not be    #
#                                    set on specified port of the DUT).        #
#                                                                              #
#  DEFINITION        : This function sets announceInterval value to a specific #
#                      port on the DUT.                                        #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_set_announce_interval {args} {

    array set param $args

    set session_handler    $param(-session_handler)
    set announce_interval  $param(-announce_interval)
    set port_num           $param(-port_num)

    global group_name case_id

    if {!(($group_name == "pcg") && ($case_id == "003"))} {

#       printLog "Skipping this configuration as this is already done in global configuration."
        return 1
    }

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        if {[regexp {(iface)\s+([a-zA-Z0-9]+)} $line match a port_retrieved]} { }

        #set default value
        if {[string match "*logAnnounceInterval*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "logAnnounceInterval $announce_interval\n"
                printLog "($port_retrieved) logAnnounceInterval $announce_interval"
                continue
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_SET_ANNOUNCE_INTERVAL"]

    return $result
}


#20.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::callback_cli_set_announce_timeout              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  announce_timeout  : (Mandatory) announceReceiptTimeout.                     #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If announceReceiptTimeout value is set on #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If announceReceiptTimeout value could not #
#                                    be set on specified port of the DUT).     #
#                                                                              #
#  DEFINITION        : This function sets announceReceiptTimeout value to a    #
#                      specific port on the DUT.                               #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_set_announce_timeout {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set announce_timeout    $param(-announce_timeout)

    global group_name case_id

    if {!(($group_name == "pcg") && ($case_id == "006"))} {

#       printLog "Skipping this configuration as this is already done in global configuration."
        return 1
    }

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        if {[regexp {(iface)\s+([a-zA-Z0-9]+)} $line match a port_retrieved]} { }

        #set default value
        if {[string match "*announceReceiptTimeout*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "announceReceiptTimeout $announce_timeout\n"
                printLog "($port_retrieved) announceReceiptTimeout $announce_timeout"
                continue
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_SET_ANNOUNCE_TIMEOUT"]

    return $result
}


#21.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_set_sync_interval                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  sync_interval     : (Mandatory) logSyncInterval                             #
#                                  (e.g., 3)                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If logSyncInterval value is set on        #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If logSyncInterval value could not be     #
#                                    set on specified port of the DUT).        #
#                                                                              #
#  DEFINITION        : This function sets logSyncInterval value to a specific  #
#                      port on the DUT.                                        #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_set_sync_interval {args} {

    array set param $args

    set session_handler    $param(-session_handler)
    set sync_interval      $param(-sync_interval)
    set port_num           $param(-port_num)

    global group_name case_id

    if {!(($group_name == "pcg") && ($case_id == "004"))} {

#       printLog "Skipping this configuration as this is already done in global configuration."
        return 1
    }

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        if {[regexp {(iface)\s+([a-zA-Z0-9]+)} $line match a port_retrieved]} { }

        #set default value
        if {[string match "*logSyncInterval*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "logSyncInterval $sync_interval\n"
                printLog "($port_retrieved) logSyncInterval $sync_interval"
                continue
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_SET_SYNC_INTERVAL"]

    return $result
}


#22.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::callback_cli_set_delayreq_interval             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  delayreq_interval : (Mandatory) MinDelayReqInterval.                        #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If MinDelayReqInterval is set on specified#
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If MinDelayReqInterval could not be set   #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets the MinDelayReqInterval to the       #
#                      specific PTP port on the DUT.                           #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_set_delayreq_interval {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set delayreq_interval   $param(-delayreq_interval)

    global group_name case_id

    if {!(($group_name == "pcg") && ($case_id == "005"))} {

#       printLog "Skipping this configuration as this is already done in global configuration."
        return 1
    }

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        if {[regexp {(iface)\s+([a-zA-Z0-9]+)} $line match a port_retrieved]} { }

        #set default value
        if {[string match "*logMinDelayReqInterval*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "logMinDelayReqInterval $delayreq_interval\n"
                printLog "($port_retrieved) logMinDelayReqInterval $delayreq_interval"
                continue
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_SET_DELAYREQ_INTERVAL"]

    return $result
}


#23.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::callback_cli_set_pdelayreq_interval            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  pdelayreq_interval: (Mandatory) minPdelayReqInterval.                       #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If minPdelayReqInterval is set on         #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If minPdelayReqInterval could not be set  #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets the minPdelayReqInterval to the      #
#                      specific PTP port on the DUT.                           #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_set_pdelayreq_interval {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set pdelayreq_interval   $param(-pdelayreq_interval)

    global group_name case_id

    if {!(($group_name == "pcg") && ($case_id == "007"))} {

#       printLog "Skipping this configuration as this is already done in global configuration."
        return 1
    }

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        if {[regexp {(iface)\s+([a-zA-Z0-9]+)} $line match a port_retrieved]} { }

        #set default value
        if {[string match "*logMinPDelayReqInterval*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "logMinPDelayReqInterval $pdelayreq_interval\n"
                printLog "($port_retrieved) logMinPDelayReqInterval $pdelayreq_interval"
                continue
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_SET_PDELAYREQ_INTERVAL"]

    return $result
}


#24.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_set_l1sync_interval                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  l1sync_interval   : (Mandatory) logL1SyncInterval.                          #
#                                  (e.g.,3 )                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If logL1SyncInterval is set on specified  #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If logL1SyncInterval could not be set on  #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function sets the logL1SyncInterval to a specific  #
#                      port on the DUT.                                        #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_set_l1sync_interval {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set l1sync_interval     $param(-l1sync_interval)

    global group_name case_id

    if {!(($group_name == "pcg") && ($case_id == "008"))} {

#       printLog "Skipping this configuration as this is already done in global configuration."
        return 1
    }

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        if {[regexp {(iface)\s+([a-zA-Z0-9]+)} $line match a port_retrieved]} { }

        #set default value
        if {[string match "*logL1SyncInterval*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "logL1SyncInterval $l1sync_interval\n"
                printLog "($port_retrieved) logL1SyncInterval $l1sync_interval"
                continue
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_SET_L1SYNC_INTERVAL"]

    return $result
}


#25.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_set_l1sync_timeout                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  l1sync_timeout    : (Mandatory) L1SyncReceiptTimeout.                       #
#                                  (e.g., 3)                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If L1SyncReceiptTimeout is set on         #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If L1SyncReceiptTimeout could not be set  #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets L1SyncReceiptTimeout to a specific   #
#                      port on the DUT.                                        #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_set_l1sync_timeout {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set l1sync_timeout      $param(-l1sync_timeout)

    global group_name case_id

    if {!(($group_name == "pcg") && ($case_id == "009")) } {

#       printLog "Skipping this configuration as this is already done in global configuration."
        return 1
    }

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        if {[regexp {(iface)\s+([a-zA-Z0-9]+)} $line match a port_retrieved]} { }

        #set default value
        if {[string match "*l1SyncReceiptTimeout*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "l1SyncReceiptTimeout $l1sync_timeout\n"
                printLog "($port_retrieved) l1SyncReceiptTimeout $l1sync_timeout"
                continue
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler  $session_handler\
                -cmd_type         "DUT_SET_L1SYNC_TIMEOUT"]

    return $result
}

#26.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_set_egress_latency                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Mandatory) timestampCorrectionPortDS.egressLatency.    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If timestampCorrectionPortDS.egressLatency#
#                                    is set on specified port of the DUT).     #
#                                                                              #
#                      0 on Failure (If timestampCorrectionPortDS.egressLatency#
#                                    could not be set on specified port of the #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function sets timestampCorrectionPortDS.           #
#                      egressLatency to a specific port on the DUT.            #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_set_egress_latency {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)
    set latency         $param(-latency)

    global group_name case_id

    if {!((($group_name == "pag") && ($case_id == "002")) || (($group_name == "pag") && ($case_id == "003")) ||\
          (($group_name == "pag") && ($case_id == "004")) || (($group_name == "pag") && ($case_id == "005")) ||\
          (($group_name == "pag") && ($case_id == "006")) || (($group_name == "pag") && ($case_id == "007")) ||\
          (($group_name == "pag") && ($case_id == "008")) || (($group_name == "pag") && ($case_id == "009")) ||\
          (($group_name == "pag") && ($case_id == "010")) || (($group_name == "pag") && ($case_id == "011")) ||\
          (($group_name == "pcg") && ($case_id == "014")))} {

#       printLog "Skipping this configuration as this is already done in global configuration."
        return 1
    }

    set latency         [expr $latency * 1000]
    set latency         [removeDecimal $latency]

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        if {[regexp {(iface)\s+([a-zA-Z0-9]+)} $line match a port_retrieved]} { }

        #set default value
        if {[string match "*egressLatency*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "egressLatency $latency\n"
                printLog "($port_retrieved) egressLatency $latency"
                continue
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler  $session_handler\
                -cmd_type         "DUT_SET_EGRESS_LATENCY"]

    return $result
}


#27.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_set_ingress_latency                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Mandatory) timestampCorrectionPortDS.ingressLatency.   #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS          : 1 on Success (If timestampCorrectionPortDS.ingressLatency#
#                                    is set on specified port of the DUT).     #
#                                                                              #
#                     0 on Failure (If timestampCorrectionPortDS.ingressLatency#
#                                    could not be set on specified port of the #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function sets timestampCorrectionPortDS.           #
#                      ingressLatency to a specific port on the DUT.           #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_set_ingress_latency {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)
    set latency         $param(-latency)

    global group_name case_id

    if {!((($group_name == "pag") && ($case_id == "002")) || (($group_name == "pag") && ($case_id == "003")) ||\
          (($group_name == "pag") && ($case_id == "004")) || (($group_name == "pag") && ($case_id == "005")) ||\
          (($group_name == "pag") && ($case_id == "006")) || (($group_name == "pag") && ($case_id == "007")) ||\
          (($group_name == "pag") && ($case_id == "008")) || (($group_name == "pag") && ($case_id == "009")) ||\
          (($group_name == "pag") && ($case_id == "010")) || (($group_name == "pag") && ($case_id == "011")) ||\
          (($group_name == "pcg") && ($case_id == "015"))) } {

#       printLog "Skipping this configuration as this is already done in global configuration."
        return 1
    }

    set latency         [expr $latency * 1000]
    set latency         [removeDecimal $latency]

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        if {[regexp {(iface)\s+([a-zA-Z0-9]+)} $line match a port_retrieved]} { }

        #set default value
        if {[string match "*ingressLatency*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "ingressLatency $latency\n"
                printLog "($port_retrieved) ingressLatency $latency"
                continue
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler  $session_handler\
                -cmd_type         "DUT_SET_INGRESS_LATENCY"]

    return $result
}


#28.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_set_constant_asymmetry             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  constant_asymmetry :(Mandatory) asymmetryCorrectionPortDS.constantAsymmetry.#
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If asymmetryCorrectionPortDS.             #
#                                    constantAsymmetry is set on specified port#
#                                    of the DUT).                              #
#                                                                              #
#                      0 on Failure (If asymmetryCorrectionPortDS.             #
#                                    constantAsymmetry could not be set on     #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function sets asymmetryCorrectionPortDS.           #
#                      constantAsymmetry to a specific port on the DUT.        #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_set_constant_asymmetry {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set constant_asymmetry  $param(-constant_asymmetry)

    global group_name case_id

    if {!((($group_name == "pag") && ($case_id == "002")) || (($group_name == "pag") && ($case_id == "003")) ||\
          (($group_name == "pag") && ($case_id == "004")) || (($group_name == "pag") && ($case_id == "005")) ||\
          (($group_name == "pag") && ($case_id == "006")) || (($group_name == "pag") && ($case_id == "007")) ||\
          (($group_name == "pag") && ($case_id == "008")) || (($group_name == "pag") && ($case_id == "009")) ||\
          (($group_name == "pag") && ($case_id == "010")) || (($group_name == "pag") && ($case_id == "011")) ||\
          (($group_name == "pcg") && ($case_id == "016")))} {

#       printLog "Skipping this configuration as this is already done in global configuration."
        return 1
    }

    set constant_asymmetry  [expr $constant_asymmetry * 1000]

    set constant_asymmetry  [removeDecimal $constant_asymmetry]

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        if {[regexp {(iface)\s+([a-zA-Z0-9]+)} $line match a port_retrieved]} { }

        #set default value
        if {[string match "*constantAsymmetry*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "constantAsymmetry $constant_asymmetry\n"
                printLog "($port_retrieved) constantAsymmetry $constant_asymmetry"
                continue
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler  $session_handler\
                -cmd_type         "DUT_SET_CONSTANT_ASYMMETRY"]

    return $result
}


#29.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_set_scaled_delay_coefficient       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  delay_coefficient : (Mandatory) asymmetryCorrectionPortDS.                  #
#                                  scaledDelayCoefficient .                    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If asymmetryCorrectionPortDS.             #
#                                    scaledDelayCoefficient is set on specified#
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If asymmetryCorrectionPortDS.             #
#                                    scaledDelayCoefficient could not be set on#
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function sets asymmetryCorrectionPortDS.           #
#                      scaledDelayCoefficient to a specific port on the DUT.   #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_set_scaled_delay_coefficient {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set delay_coefficient   $param(-delay_coefficient)

    global group_name case_id

    if {!((($group_name == "pag") && ($case_id == "002")) || (($group_name == "pag") && ($case_id == "003")) ||\
          (($group_name == "pag") && ($case_id == "004")) || (($group_name == "pag") && ($case_id == "005")) ||\
          (($group_name == "pag") && ($case_id == "006")) || (($group_name == "pag") && ($case_id == "007")) ||\
          (($group_name == "pag") && ($case_id == "008")) || (($group_name == "pag") && ($case_id == "009")) ||\
          (($group_name == "pag") && ($case_id == "010")) || (($group_name == "pag") && ($case_id == "011")) ||\
          (($group_name == "pcg") && ($case_id == "017")))} {

#       printLog "Skipping this configuration as this is already done in global configuration."
        return 1
    }

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        if {[regexp {(iface)\s+([a-zA-Z0-9]+)} $line match a port_retrieved]} { }

        #set default value
        if {[string match "*delayCoefficient*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "delayCoefficient $delay_coefficient\n"
                printLog "($port_retrieved) delayCoefficient $delay_coefficient"
                continue
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler  $session_handler\
                -cmd_type         "DUT_SET_SCALED_DELAY_COEFFICIENT"]

    return $result
}


#30.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_set_asymmetry_correction           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If asymmetryCorrectionPortDS.enable is set#
#                                    on specified port of the DUT).            #
#                                                                              #
#                      0 on Failure (If asymmetryCorrectionPortDS.enable could #
#                                    not be set on specified port of the DUT). #
#                                                                              #
#  DEFINITION        : This function sets the asymmetryCorrectionPortDS.enable #
#                      to the specific PTP port on the DUT.                    #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_set_asymmetry_correction {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)

    global group_name case_id

    if {!((($group_name == "pag") && ($case_id == "004")) || (($group_name == "pag") && ($case_id == "005")) ||\
          (($group_name == "pag") && ($case_id == "006")))} {

#       printLog "Skipping this configuration as this is already done in global configuration."
        return 1
    }

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        if {[regexp {(iface)\s+([a-zA-Z0-9]+)} $line match a port_retrieved]} { }

        #Enable asymmetryCorrectionEnable
        if {[string match "*asymmetryCorrectionEnable*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "asymmetryCorrectionEnable y\n"
                printLog "($port_retrieved) asymmetryCorrectionEnable y"
                continue
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler  $session_handler\
                -cmd_type         "DUT_SET_ASYMMETRY_CORRECTION"]

    return $result
}


#31.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_external_port_config_enable        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If external port configuration option is  #
#                                    enabled on the specified port at the DUT).#
#                                                                              #
#                      0 on Failure (If external port configuration option     #
#                                    could not be enabled on the specified port#
#                                    at the DUT).                              #
#                                                                              #
#  DEFINITION        : This function enables external port configuration option#
#                      on a specific port at the DUT.                          #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_external_port_config_enable {args} {

    array set param $args

    set session_handler $param(-session_handler)

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        #set default value
        if {[string match "*externalPortConfigurationEnabled*" $line]} {
            append new_config "externalPortConfigurationEnabled y\n"
            printLog "externalPortConfigurationEnabled y"
            continue
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_EXTERNAL_PORT_CONFIG_ENABLE"]

    return $result
}


#32.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_set_epc_desired_state              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  desired_state     : (Mandatory) externalPortConfigurationPortDS.desiredState#
#                                  (e.g., "INITIALIZING"|"FAULTY"|"DISABLED"|  #
#                                         "LISTENING"|"PRE_MASTER"|"MASTER"|   #
#                                         "PASSIVE"|"UNCALIBRATED"|"SLAVE"     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given externalPortConfigurationPortDS. #
#                                    desiredState is set on specified port of  #
#                                    the DUT).                                 #
#                                                                              #
#                      0 on Failure (If given externalPortConfigurationPortDS. #
#                                    desiredState could not be set on specifed #
#                                    port of the DUT).                         #
#                                                                              #
#  DEFINITION        : This function sets given externalPortConfigurationPortDS#
#                      desiredState to a specific port on the DUT.             #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_set_epc_desired_state {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)
    set desired_state   $param(-desired_state)


    switch $desired_state {

        PRE_MASTER {
            set desired_state   "pre_master"
        }
        default {
            set desired_state   [string tolower $desired_state]
        }
    }

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        if {[regexp {(iface)\s+([a-zA-Z0-9]+)} $line match a port_retrieved]} { }

        #set default value
        if {[string match "*desiredState*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "desiredState $desired_state\n"
                printLog "($port_retrieved) desiredState $desired_state"
                continue
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler  $session_handler\
                -cmd_type         "DUT_SET_EPC_DESIRED_STATE"]

    return $result
}


#33.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_set_ipv4_address                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  ip_address        : (Mandatory) IPv4 address.                               #
#                                  (e.g., 192.168.200.1)                       #
#                                                                              #
#  net_mask          : (Mandatory) Subnet mask.                                #
#                                  (e.g., 255.255.255.0)                       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID.                                    #
#                                  (e.g., 100)                                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given IP address is set on specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given IP address could not be set on   #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function sets given IP address to a specific port  #
#                      on the DUT.                                             #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_set_ipv4_address {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)
    set ip_address      $param(-ip_address)
    set net_mask        $param(-net_mask)
    set vlan_id         $param(-vlan_id)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_SET_IPV4_ADDRESS"\
#                       -parameter1             $port_num\
#                       -parameter2             $ip_address\
#                       -parameter3             $net_mask\
#                       -parameter4             $vlan_id]} {
#
#           return 0
#       }

    return 2
}


#34.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_timestamp_corrected_tx_enable      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given L1SyncOptParamsPortDS.           #
#                                    timestampsCorrectedTx is set on specified #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given L1SyncOptParamsPortDS.           #
#                                    timestampsCorrectedTx could not be set on #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function sets given L1SyncOptParamsPortDS.         #
#                      timestampsCorrectedTx to a specific port on the DUT.    #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_timestamp_corrected_tx_enable {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)

    global group_name case_id

    if {!(($group_name == "opv") && ($case_id == "001"))} {

#       printLog "Skipping this configuration as this is already done in global configuration."
        return 1
    }

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        if {[regexp {(iface)\s+([a-zA-Z0-9]+)} $line match a port_retrieved]} { }

        #set default value
        if {[string match "*profile*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "profile custom\n"
                printLog "($port_retrieved) profile custom"
                continue
            }
        }

        #Enable l1SyncTimestampsCorrectedTxEnabled 
        if {[string match "*l1SyncTimestampsCorrectedTxEnabled*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "l1SyncTimestampsCorrectedTxEnabled y\n"
                printLog "($port_retrieved) l1SyncTimestampsCorrectedTxEnabled y"
                continue
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler  $session_handler\
                -cmd_type         "DUT_TIMESTAMP_CORRECTED_TX_ENABLE"]

    return $result
}


################################################################################
#                       DUT DISABLING CALLBACK                                 #
################################################################################


#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_port_disable                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) Port to be disabled.                        #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If port is disabled)                      #
#                                                                              #
#                      0 on Failure (If port could not be disabled)            #
#                                                                              #
#  DEFINITION        : This function disables a specific port on the DUT.      #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_port_disable {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#        if {![Send_cli_command \
#                        -session_handler        $session_handler \
#                        -cmd_type               "DUT_PORT_DISABLE"\
#                        -parameter1              $port_num]} {
#
#            return 0
#        }

    return 2
}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_port_ptp_disable                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) Port on which ptp is to be disabled.        #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is disabled on the specified port  #
#                                    at the DUT).                              #
#                                                                              #
#                      0 on Failure (If PTP could not be disabled on the       #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function disables PTP on a specific port on the DUT#
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_port_ptp_disable {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set port_name [getPortName $config $port_num]

    set new_config ""

    foreach line [split $config "\n"] {

        if {[regexp {(iface)\s+([a-zA-Z0-9]+)} $line match a port_retrieved]} {# Do nothing.}

        if {[regexp {(port)\s+(.+)} $line match a port_retrieved]} {# Do nothing.}

        #set default value
        if {[string match "*proto*" $line]} {
            if {$port_name == $port_retrieved} {
                append new_config "proto raw\n"
                printLog "($port_retrieved) proto raw"
                continue
            }
        }

        #set default announce interval
        if {[string match "*logAnnounceInterval*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "logAnnounceInterval $::ptp_announce_interval\n"
                printLog "($port_retrieved) logAnnounceInterval $::ptp_announce_interval"
                continue
            }
        }

        #set default announce timeout 
        if {[string match "*announceReceiptTimeout*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "announceReceiptTimeout $::ptp_announce_timeout\n"
                printLog "($port_retrieved) announceReceiptTimeout $::ptp_announce_timeout"
                continue
            }
        }

        #set default sync interval
        if {[string match "logSyncInterval*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "logSyncInterval $::ptp_sync_interval\n"
                printLog "($port_retrieved) logSyncInterval $::ptp_sync_interval"
                continue
            }
        }

        #set default L1Sync interval
        if {[string match "*logL1SyncInterval*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "logL1SyncInterval $::ptp_l1sync_interval\n"
                printLog "($port_retrieved) logL1SyncInterval $::ptp_l1sync_interval"
                continue
            }
        }

        #set default l1sync timeout
        if {[string match "*l1SyncReceiptTimeout*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "l1SyncReceiptTimeout $::ptp_l1sync_timeout\n"
                printLog "($port_retrieved) l1SyncReceiptTimeout $::ptp_l1sync_timeout"
                continue
            }
        }

        #set default logMinDelayReqInterval
        if {[string match "*logMinDelayReqInterval*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "logMinDelayReqInterval $::ptp_delayreq_interval\n"
                printLog "($port_retrieved) logMinDelayReqInterval $::ptp_delayreq_interval"
                continue
            }
        }

        #set default logMinPDelayReqInterval
        if {[string match "*logMinPDelayReqInterval*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "logMinPDelayReqInterval $::ptp_pdelayreq_interval\n"
                printLog "($port_retrieved) logMinPDelayReqInterval $::ptp_pdelayreq_interval"
                continue
            }
        }

        #set default desiredState value
        if {[string match "*desiredState*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "desiredState passive\n"
                printLog "($port_retrieved) desiredState passive"
                continue
            }
        }

        #set default egress latency value
        set latency [expr $::ptp_egress_latency * pow(10,3)]
        set latency [removeDecimal $latency]

        if {[string match "*egressLatency*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "egressLatency $latency\n"
                printLog "($port_retrieved) egressLatency $latency"
                continue
            }
        }

        #set default ingress latency value
        set latency [expr $::ptp_ingress_latency * pow(10,3)]
        set latency [removeDecimal $latency]

        if {[string match "*ingressLatency*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "ingressLatency $latency\n"
                printLog "($port_retrieved) ingressLatency $latency"
                continue
            }
        }

        #set default constant asymmetry value
        set constant_asymmetry  [expr $::ptp_constant_asymmetry * pow(10,3)]
        set constant_asymmetry  [removeDecimal $constant_asymmetry]

        if {[string match "*constantAsymmetry*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "constantAsymmetry $constant_asymmetry\n"
                printLog "($port_retrieved) constantAsymmetry $constant_asymmetry"
                continue
            }
        }

        #set default delay coefficient
        if {[string match "*delayCoefficient*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "delayCoefficient $::ptp_delay_coefficient\n"
                printLog "($port_retrieved) delayCoefficient $::ptp_delay_coefficient"
                continue
            }
        }

        #set default profile
        if {[string match "*profile*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "profile ha\n"
                printLog "($port_retrieved) profile ha"
                continue
            }
        }

        #set default l1SyncEnabled
        if {[string match "*l1SyncEnabled*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "l1SyncEnabled y\n"
                printLog "($port_retrieved) l1SyncEnabled y"
                continue
            }
        }

        #set default l1SyncOptParamsEnabled
        if {[string match "*l1SyncOptParamsEnabled*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "l1SyncOptParamsEnabled n\n"
                printLog "($port_retrieved) l1SyncOptParamsEnabled n"
                continue
            }
        }

        #set default masterOnly 
        if {[string match "*masterOnly*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "masterOnly n\n"
                printLog "($port_retrieved) masterOnly n"
                continue
            }
        }

        #unset VLAN
        if {[string match "*vlan*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "#vlan is not set\n"
                printLog "($port_retrieved) #vlan is not set"
                continue
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_PORT_PTP_DISABLE"]

    return $result
}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_reset_vlan                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID with which VLAN has to be deleted.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#  port_num          : (Mandatory) Port which has to be removed from VLAN ID.  #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If port is removed from VLAN ID and VLAN  #
#                                    ID is deleted from the DUT).              #
#                                                                              #
#                      0 on Failure (If port could not be removed from VLAN ID #
#                                    and VLAN ID is not deledted from the DUT).#
#                                                                              #
#  DEFINITION        : This function removes port from VLAN ID and deletes VLAN#
#                      ID from the DUT.                                        #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_reset_vlan {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set vlan_id             $param(-vlan_id)
    set port_num            $param(-port_num)

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    set port_name [getPortName $config $port_num]

    foreach line [split $config "\n"] {

        if {[regexp {(port)\s+(.+)} $line match a port_retrieved]} { }

        #set default value
        if {[string match "*vlan*" $line]} {
            if {$port_name == $port_retrieved} {
                append new_config "# vlan is not set\n"
                printLog "($port_retrieved) # vlan is not set"
                continue
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_RESET_VLAN"]

    return $result
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_reset_vlan_priority                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID with which VLAN has to be created.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#  vlan_priority     : (Mandatory) VLAN priority to be associated              #
#                                  (e.g., 1)                                   #
#                                                                              #
#  port_num          : (Mandatory) Port with VLAN ID configured.               #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If VLAN priority is disassociated)        #
#                                                                              #
#                      0 on Failure (If VLAN priority could not be             #
#                                    disassociated)                            #
#                                                                              #
#  DEFINITION        : This function disassociates given priority from VLAN    #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_reset_vlan_priority {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set vlan_id         $param(-vlan_id)
    set vlan_priority   $param(-vlan_priority)
    set port_num        $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler         $session_handler \
#                       -cmd_type                "DUT_RESET_VLAN_PRIORITY"\
#                       -parameter1              $vlan_priority\
#                       -parameter2              $port_num]} {
#
#           return 0
#       }

    return 2
}


#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_global_ptp_disable                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is disabled on the DUT).           #
#                                                                              #
#                      0 on Failure (If PTP could not be disabled on the DUT). #
#                                                                              #
#  DEFINITION        : This function disables PTP on the DUT.                  #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_global_ptp_disable {args} {

    array set param $args

    set session_handler     $param(-session_handler)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_GLOBAL_PTP_DISABLE"]} {
#           return 0
#       }

    return 2
}


#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_reset_communication_mode           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  ptp_version       : (optional)  PTP version.                                #
#                                  (Default: 2)                                #
#                                                                              #
#  communication_mode : (Mandatory) Communication mode                         #
#                                  (e.g., "Unicast"|"Multicast")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If communication mode is reset on the     #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If communication mode could not be        #
#                                    reset on the specified port at the DUT).  #
#                                                                              #
#  DEFINITION        : This function resets the communication mode on a        #
#                      specific port of the DUT.                               #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_reset_communication_mode {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set clock_mode          $param(-clock_mode)
    set communication_mode  $param(-communication_mode)
    set ptp_version         $param(-ptp_version)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_COMMUNICATION_MODE"\
#                       -parameter1             $port_num\
#                       -parameter2             $clock_mode\
#                       -parameter3             $ptp_version\
#                       -parameter4             $communication_mode]} {
#           return 0
#       }

    return 2
}


#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_reset_domain                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  domain            : (Mandatory) Domain number.                              #
#                                  (e.g., 0..255)                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If domain number is reset on the DUT).    #
#                                                                              #
#                      0 on Failure (If domain number could not able to reset  #
#                                    on DUT).                                  #
#                                                                              #
#  DEFINITION        : This function resets domain number on the DUT.          #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_reset_domain {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set clock_mode          $param(-clock_mode)
    set domain              $param(-domain)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_DOMAIN"\
#                       -parameter1             $clock_mode\
#                       -parameter2             $domain]} {
#           return 0
#       }

    return 2
}


#8.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_reset_network_protocol             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) clock_mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  network_protocol  : (Mandatory) Network protocol.                           #
#                                  (e.g., "IEEE 802.3"|"UDP/IPv4")             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If network protocol is reset on specified #
#                                    port at the DUT).                         #
#                                                                              #
#                      0 on Failure (If network protocol could not be reset    #
#                                    on specified port at the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets the network protocol on a specific #
#                      port of the DUT.                                        #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_reset_network_protocol {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set clock_mode          $param(-clock_mode)
    set network_protocol    $param(-network_protocol)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_NETWORK_PROTOCOL"\
#                       -parameter1             $clock_mode\
#                       -parameter2             $network_protocol]} {
#           return 0
#       }

    return 2
}


#9.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_reset_clock_mode                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP clock mode is reset on specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If PTP clock mode could not be reset on   #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function resets clock mode on a specific port of   #
#                      DUT.                                                    #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_reset_clock_mode {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set clock_mode          $param(-clock_mode)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_CLOCK_MODE"\
#                       -parameter1             $port_num\
#                       -parameter2             $clock_mode]} {
#           return 0
#       }

    return 2
}


#10.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_reset_clock_step                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  clock_step        : (Mandatory) Clock step.                                 #
#                                  (e.g., "One-step"|"Two-step")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If clock-step is reset on the specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If clock-step could not be able to reset  #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets the clock-step on a specific port  #
#                      on the DUT.                                             #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_reset_clock_step {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set clock_mode          $param(-clock_mode)
    set clock_step          $param(-clock_step)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_CLOCK_STEP"\
#                       -parameter1             $port_num\
#                       -parameter1             $clock_mode\
#                       -parameter3             $clock_step]} {
#           return 0
#       }

    return 2
}


#11.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_reset_delay_mechanism              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  delay_mechanism   : (Mandatory) Delay mechanism.                            #
#                                  (e.g., e2e|p2p|disable)                     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If delay mechanism is reset on specified  #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If delay mechanism could not be reset on  #
#                                    specified PTP port of the DUT).           #
#                                                                              #
#  DEFINITION        : This function resets the delay mechanism to a specific  #
#                      port on the DUT.                                        #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_reset_delay_mechanism {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set clock_mode          $param(-clock_mode)
    set delay_mechanism     $param(-delay_mechanism)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_DELAY_MECHANISM"\
#                       -parameter1             $port_num\
#                       -parameter2             $clock_mode\
#                       -parameter3             $delay_mechanism]} { 
#           return 0
#       }

    return 2
}


#12.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_reset_priority1                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority1         : (Mandatory) Prority1                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority1 is reset on the DUT).        #
#                                                                              #
#                      0 on Failure (If priority1 could not be able to reset on#
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function resets the priority1 on the DUT.          #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_reset_priority1 {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set clock_mode          $param(-clock_mode)
    set priority1           $param(-priority1)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_PRIORITY1"\
#                       -parameter1             $clock_mode\
#                       -parameter2             $priority1]} { 
#           return 0
#       }

    return 2
}


#13.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_reset_priority2                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority2         : (Mandatory) Prority2                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority2 is reset on the DUT).        #
#                                                                              #
#                      0 on Failure (If priority2 could not be able to reset on#
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function resets the priority2 on the DUT.          #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_reset_priority2 {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set clock_mode          $param(-clock_mode)
    set priority2           $param(-priority2)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_PRIORITY1"\
#                       -parameter1             $clock_mode\
#                       -parameter2             $priority2]} { 
#           return 0
#       }

    return 2
}


#14.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::callback_cli_tcr_rcr_cr_disable                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If peerTxCoherentIsRequired,              #
#                                    peerRxCoherentIsRequired and              #
#                                    peerCongruentIsRequired is disabled on the#
#                                    DUT).                                     #
#                                                                              #
#                      0 on Failure (If peerTxCoherentIsRequired,              #
#                                    peerRxCoherentIsRequired and              #
#                                    peerCongruentIsRequired could not be      #
#                                    disabled on the DUT).                     #
#                                                                              #
#                                                                              #
#  DEFINITION        : This function disables peerTxCoherentIsRequired,        #
#                      peerRxCoherentIsRequired and peerCongruentIsRequired on #
#                      the DUT.                                                #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_tcr_rcr_cr_disable {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_TCR_RCR_CR_DISABLE"\
#                       -parameter1             $port_num]} {
#           return 0
#       }

    return 2
}


#15.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_l1sync_disable                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If L1Sync option is disabled on the       #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If L1Sync option could not be disabled on #
#                                    the specified port of the DUT).           #
#                                                                              #
#  DEFINITION        : This function disables L1Sync option on a specific port #
#                      at the DUT.                                             #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_l1sync_disable {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        if {[regexp {(iface)\s+([a-zA-Z0-9]+)} $line match a port_retrieved]} { }

        #set default profile
        if {[string match "*profile*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "profile custom\n"
                printLog "($port_retrieved) profile custom"
                continue
            }
        }
        #Disable l1SyncEnabled
        if {[string match "*l1SyncEnabled*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "l1SyncEnabled n\n"
                printLog "($port_retrieved) l1SyncEnabled n"
                continue
            }
        }
        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler  $session_handler\
                -cmd_type         "DUT_L1SYNC_DISABLE"]

    return $result
}


#16.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_l1sync_opt_params_disable          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If extended format of L1Sync TLV is       #
#                                    disabled on the specified port at the DUT)#
#                                                                              #
#                      0 on Failure (If extended format of L1Sync TLV could not#
#                                    be disabled on the specified port at the  #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function disables extended format of L1Sync TLV on #
#                      the specified port at the DUT.                          #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_l1sync_opt_params_disable {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        if {[regexp {(iface)\s+([a-zA-Z0-9]+)} $line match a port_retrieved]} { }

        #Enable l1SyncOptParamsEnabled
        if {[string match "*l1SyncOptParamsEnabled*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "l1SyncOptParamsEnabled n\n"
                printLog "($port_retrieved) l1SyncOptParamsEnabled n"
                continue
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler  $session_handler\
                -cmd_type         "DUT_L1SYNC_OPT_PARAMS_DISABLE"]

    return $result
}


#17.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_slaveonly_disable                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If slave-only is disabled on the          #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If slave-only could not be disabled on the#
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function disables slave-only on the specified port #
#                      at the DUT.                                             #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_slaveonly_disable {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        #set default value
        if {[string match "*slaveOnly*" $line]} {
            append new_config "slaveOnly n\n"
            printLog "slaveOnly n"
            continue
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_SLAVEONLY_DISABLE"]

    return $result
}


#18.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_masteronly_disable                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If master-only is disabled on the         #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If master-only could not be disabled on   #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function disables master-only on a specific port   #
#                      at the DUT.                                             #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_masteronly_disable {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        if {[regexp {(iface)\s+([a-zA-Z0-9]+)} $line match a port_retrieved]} { }

        #set default value
        if {[string match "*masterOnly*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "masterOnly n\n"
                printLog "($port_retrieved) masterOnly n"
                continue
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_MASTERONLY_DISABLE"]

    return $result
}


#19.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::callback_cli_reset_announce_interval           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  announce_interval : (Mandatory) announceInterval.                           #
#                                  (e.g., 1|2|4|8|16)                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If announceInterval value is reset on     #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If announceInterval value could not be    #
#                                    reset on specified port of the DUT).      #
#                                                                              #
#  DEFINITION        : This function resets announceInterval value to a        #
#                      specific port on the DUT.                               #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_reset_announce_interval {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set announce_interval   $param(-announce_interval)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_ANNOUNCE_INTERVAL"\
#                       -parameter1             $port_num\
#                       -parameter2             $announce_interval]} {
#           return 0
#       }

    return 2
}


#20.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::callback_cli_reset_announce_timeout            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  announce_timeout  : (Mandatory) announceReceiptTimeout.                     #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If announceReceiptTimeout value is reset  #
#                                    on specified port of the DUT).            #
#                                                                              #
#                      0 on Failure (If announceReceiptTimeout value could not #
#                                    be reset on specified port of the DUT).   #
#                                                                              #
#  DEFINITION        : This function resets announceReceiptTimeout value to a  #
#                      specific port on the DUT.                               #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_reset_announce_timeout {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set announce_timeout    $param(-announce_timeout)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_ANNOUNCE_TIMEOUT"\
#                       -parameter1             $port_num\
#                       -parameter2             $announce_timeout]} {
#           return 0
#       }

    return 2
}


#21.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_reset_sync_interval                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  sync_interval     : (Mandatory) logSyncInterval                             #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If logSyncInterval value is reset on      #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If logSyncInterval value could not be     #
#                                    reset on specified port of the DUT).      #
#                                                                              #
#  DEFINITION        : This function resets logSyncInterval value to a specific#
#                      port on the DUT.                                        #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_reset_sync_interval {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set sync_interval       $param(-sync_interval)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_SYNC_INTERVAL"\
#                       -parameter1             $port_num\
#                       -parameter2             $sync_interval]} {
#           return 0
#       }

    return 2
}


#22.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::callback_cli_reset_delayreq_interval           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  delayreq_interval : (Mandatory) MinDelayReqInterval.                        #
#                                  (e.g., 1|2|4|8|16)                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If MinDelayReqInterval is reset on        #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If MinDelayReqInterval could not be reset #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets the MinDelayReqInterval to the     #
#                      specific PTP port on the DUT.                           #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_reset_delayreq_interval {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set delayreq_interval   $param(-delayreq_interval)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_DELAYREQ_INTERVAL"\
#                       -parameter1             $port_num\
#                       -parameter2             $delayreq_interval]} {
#           return 0
#       }

    return 2
}


#23.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::callback_cli_reset_pdelayreq_interval          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  pdelayreq_interval: (Mandatory) minPdelayReqInterval.                       #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If minPdelayReqInterval is reset on       #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If minPdelayReqInterval could not be reset#
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets the minPdelayReqInterval to the    #
#                      specific PTP port on the DUT.                           #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_reset_pdelayreq_interval {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set pdelayreq_interval  $param(-pdelayreq_interval)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_PDELAYREQ_INTERVAL"\
#                       -parameter1             $port_num\
#                       -parameter2             $pdelayreq_interval]} {
#           return 0
#       }

    return 2
}


#24.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_reset_l1sync_interval              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  l1sync_interval   : (Mandatory) logL1SyncInterval.                          #
#                                  (e.g.,3 )                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If logL1SyncInterval is reset on specified#
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If logL1SyncInterval could not be reset on#
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function resets the logL1SyncInterval to a specific#
#                      port on the DUT.                                        #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_reset_l1sync_interval {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set l1sync_interval     $param(-l1sync_interval)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_L1SYNC_INTERVAL"\
#                       -parameter1             $port_num\
#                       -parameter2             $l1sync_interval]} {
#           return 0
#       }

    return 2
}


#25.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_reset_l1sync_timeout               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  l1sync_timeout    : (Mandatory) L1SyncReceiptTimeout.                       #
#                                  (e.g., 3)                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If L1SyncReceiptTimeout is reset on       #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If L1SyncReceiptTimeout could not be      #
#                                    reset on specified port of the DUT).      #
#                                                                              #
#  DEFINITION        : This function resets L1SyncReceiptTimeout to a specific #
#                      port on the DUT.                                        #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_reset_l1sync_timeout {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set l1sync_timeout      $param(-l1sync_timeout)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_L1SYNC_TIMEOUT"\
#                       -parameter1             $port_num\
#                       -parameter2             $l1sync_timeout]} {
#           return 0
#       }

    return 2
}


#26.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_reset_egress_latency               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Mandatory) timestampCorrectionPortDS.egressLatency.    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If timestampCorrectionPortDS.egressLatency#
#                                    is reset on specified port of the DUT).   #
#                                                                              #
#                      0 on Failure (If timestampCorrectionPortDS.egressLatency#
#                                    could not be reset on specified port of   #
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function resets timestampCorrectionPortDS.         #
#                      egressLatency to a specific port on the DUT.            #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_reset_egress_latency {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)
    set latency         $param(-latency)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_EGRESS_LATENCY"\
#                       -parameter1             $port_num\
#                       -parameter2             $latency]} {
#
#           return 0
#       }

    return 2
}


#27.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_reset_ingress_latency              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Mandatory) timestampCorrectionPortDS.ingressLatency.   #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS          : 1 on Success (If timestampCorrectionPortDS.ingressLatency#
#                                    is reset on specified port of the DUT).   #
#                                                                              #
#                     0 on Failure (If timestampCorrectionPortDS.ingressLatency#
#                                    could not be reset on specified port of   #
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function resets timestampCorrectionPortDS.         #
#                      ingressLatency to a specific port on the DUT.           #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_reset_ingress_latency {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)
    set latency         $param(-latency)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_INGRESS_LATENCY"\
#                       -parameter1             $port_num\
#                       -parameter2             $latency]} {
#
#           return 0
#       }

    return 2
}


#28.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_reset_constant_asymmetry           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  constant_asymmetry :(Mandatory) asymmetryCorrectionPortDS.constantAsymmetry.#
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If asymmetryCorrectionPortDS.             #
#                                    constantAsymmetry is reset on specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If asymmetryCorrectionPortDS.             #
#                                    constantAsymmetry could not be reset on   #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function resets asymmetryCorrectionPortDS.         #
#                      constantAsymmetry to a specific port on the DUT.        #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_reset_constant_asymmetry {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set constant_asymmetry  $param(-constant_asymmetry)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_CONSTANT_ASYMMETRY"\
#                       -parameter1             $port_num\
#                       -parameter2             $constant_asymmetry]} {
#
#           return 0
#       }

    return 2
}


#29.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_reset_scaled_delay_coefficient     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  delay_coefficient : (Mandatory) asymmetryCorrectionPortDS.                  #
#                                  scaledDelayCoefficient .                    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If asymmetryCorrectionPortDS.             #
#                                    scaledDelayCoefficient is reset on        #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If asymmetryCorrectionPortDS.             #
#                                    scaledDelayCoefficient could not be reset #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets asymmetryCorrectionPortDS.         #
#                      scaledDelayCoefficient to a specific port on the DUT.   #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_reset_scaled_delay_coefficient {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set delay_coefficient   $param(-delay_coefficient)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_SCALED_DELAY_COEFFICIENT"\
#                       -parameter1             $port_num\
#                       -parameter2             $delay_coefficient]} {
#
#           return 0
#       }

    return 2
}


#30.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_reset_asymmetry_correction         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If asymmetryCorrectionPortDS.enable is    #
#                                    reset on specified port of the DUT).      #
#                                                                              #
#                      0 on Failure (If asymmetryCorrectionPortDS.enable could #
#                                    not be reset on specified port of the DUT)#
#                                                                              #
#  DEFINITION       : This function resets the asymmetryCorrectionPortDS.enable#
#                      to the specific PTP port on the DUT.                    #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_reset_asymmetry_correction {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        if {[regexp {(iface)\s+([a-zA-Z0-9]+)} $line match a port_retrieved]} { }

        #Enable asymmetryCorrectionEnable
        if {[string match "*asymmetryCorrectionEnable*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "asymmetryCorrectionEnable n\n"
                printLog "($port_retrieved) asymmetryCorrectionEnable n"
                continue
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler  $session_handler\
                -cmd_type         "DUT_RESET_ASYMMETRY_CORRECTION"]

    return $result
}


#31.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_external_port_config_disable       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If external port configuration option is  #
#                                    disabled on the specified port at the DUT)#
#                                                                              #
#                      0 on Failure (If external port configuration option     #
#                                    could not be disabled on the specified    #
#                                    port at the DUT).                         #
#                                                                              #
#  DEFINITION        : This function disables external port configuration      #
#                      option on a specific port at the DUT.                   #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_external_port_config_disable {args} {

    array set param $args

    set session_handler $param(-session_handler)

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        #set default value
        if {[string match "*externalPortConfigurationEnabled*" $line]} {
            append new_config "externalPortConfigurationEnabled n\n"
            printLog "externalPortConfigurationEnabled n"
            continue
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler $session_handler\
                -cmd_type        "DUT_EXTERNAL_PORT_CONFIG_DISABLE"]

    return $result
}


#32.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_reset_epc_desired_state            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  desired_state     : (Mandatory) externalPortConfigurationPortDS.desiredState#
#                                  (e.g., "INITIALIZING"|"FAULTY"|"DISABLED"|  #
#                                         "LISTENING"|"PRE_MASTER"|"MASTER"|   #
#                                         "PASSIVE"|"UNCALIBRATED"|"SLAVE"     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given externalPortConfigurationPortDS. #
#                                    desiredState is reset on specified port of#
#                                    the DUT).                                 #
#                                                                              #
#                      0 on Failure (If given externalPortConfigurationPortDS. #
#                                    desiredState could not be reset on        #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION      : This function resets given externalPortConfigurationPortDS#
#                      desiredState to a specific port on the DUT.             #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_reset_epc_desired_state {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)
    set desired_state   $param(-desired_state)

    switch $desired_state {

        PRE_MASTER {
            set desired_state   "premaster"
        }
        default {
            set desired_state   [string tolower $desired_state]
        }
    }

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        if {[regexp {(iface)\s+([a-zA-Z0-9]+)} $line match a port_retrieved]} { }

        #set default value
        if {[string match "*desiredState*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "desiredState $desired_state\n"
                printLog "($port_retrieved) desiredState $desired_state"
                continue
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler  $session_handler\
                -cmd_type         "DUT_RESET_EPC_DESIRED_STATE"]

    return $result
}


#33.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_reset_ipv4_address                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  ip_address        : (Mandatory) IPv4 address.                               #
#                                  (e.g., 192.168.200.1)                       #
#                                                                              #
#  net_mask          : (Mandatory) Subnet mask.                                #
#                                  (e.g., 255.255.255.0)                       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID.                                    #
#                                  (e.g., 100)                                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given IP address is reset on specified #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given IP address could not be reset on #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function resets given IP address to a specific port#
#                      on the DUT.                                             #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_reset_ipv4_address {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)
    set ip_address      $param(-ip_address)
    set net_mask        $param(-net_mask)
    set vlan_id         $param(-vlan_id)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_cli_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_IPV4_ADDRESS"\
#                       -parameter1             $port_num\
#                       -parameter2             $ip_address\
#                       -parameter3             $net_mask\
#                       -parameter4             $vlan_id]} {
#
#           return 0
#       }

    return 2
}


#34.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_timestamp_corrected_tx_disable     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given L1SyncOptParamsPortDS.           #
#                                   timestampsCorrectedTx is reset on specified#
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given L1SyncOptParamsPortDS.           #
#                                   timestampsCorrectedTx could not be reset on#
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function resets given L1SyncOptParamsPortDS.       #
#                      timestampsCorrectedTx to a specific port on the DUT.    #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_timestamp_corrected_tx_disable {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)

    global group_name case_id

    if {!(($group_name == "opv") && ($case_id == "001"))} {

#       printLog "Skipping this configuration as this is already done in global configuration."
        return 1
    }

    ## This proc set values in ppsi.conf file based on Values from ATTEST GUI.

    global host username password

    set ppsiconfig_path     /home/attest/AttestServer/NetO2Attest/Net-O2/NECP_LIB/Cli/Ptp-ha

    set ppsitemplate_file   $ppsiconfig_path/ppsi.tmpl
    set ppsiconfig_file     $ppsiconfig_path/ppsi.conf

    if [file exists $ppsitemplate_file] {

        #Copy template to ppsi.conf
        exec cp $ppsitemplate_file $ppsiconfig_file

        #Read ppsi.conf file
        set ppsiconfig_fd [open $ppsiconfig_file "r"]
        set config        [read $ppsiconfig_fd]
        close $ppsiconfig_fd
    } else {
        printLog "$ppsitemplate_file file does not exists"
        return 0
    }

    set new_config ""

    foreach line [split $config "\n"] {

        if {[regexp {(iface)\s+([a-zA-Z0-9]+)} $line match a port_retrieved]} { }

        #set default value
        if {[string match "*profile*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "profile custom\n"
                printLog "($port_retrieved) profile custom"
                continue
            }
        }

        #Disable l1SyncTimestampsCorrectedTxEnabled
        if {[string match "*l1SyncTimestampsCorrectedTxEnabled*" $line]} {
            if {$port_num == $port_retrieved} {
                append new_config "l1SyncTimestampsCorrectedTxEnabled n\n"
                printLog "($port_retrieved) l1SyncTimestampsCorrectedTxEnabled n"
                continue
            }
        }

        append new_config "$line\n"
    }

    #Write configuration to ppsi.conf file
    set ppsiconfig_fd [open $ppsiconfig_file  "w+"]
    puts $ppsiconfig_fd $new_config
    close $ppsiconfig_fd

    delEmptyEof $ppsiconfig_file

    ##copy the updated ppsi.conf file to device
    doScp $ppsiconfig_file $username@$host:/etc $password

    #Copy ppsi.conf to template
    exec cp $ppsiconfig_file $ppsitemplate_file

    set result [Send_cli_command\
                -session_handler  $session_handler\
                -cmd_type         "DUT_TIMESTAMP_CORRECTED_TX_DISABLE"]

    return $result
}

################################################################################
#                           DUT CHECK CALLBACK                                 #
################################################################################

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_check_l1sync_state                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  l1sync_state      : (Mandatory) L1 Sync State.                              #
#                                  (e.g., "DISABLED"|"IDLE"|"LINK_ALIVE"|      #
#                                         "CONFIG_MATCH"|"L1_SYNC_UP")         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given L1 Sync State is verified in the #
#                                    specified port).                          #
#                                                                              #
#                      0 on Failure (If given L1 Sync State is not verified in #
#                                    the specified port).                      #
#                                                                              #
#  DEFINITION        : This function verifies the given L1 Sync State in a     #
#                      specific port.                                          #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_check_l1sync_state {args} {

    global debugCount
    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set l1sync_state        $param(-l1sync_state)
    set buffer              $param(-buffer)

    array set L1_SYNC_STATE {
        IDLE            "IDLE"
        DISABLED        "DISABLED"
        DISABLED1       ""
        LINK_ALIVE      "LINK ALIVE"
        CONFIG_MATCH    "CFG MATCH"
        L1_SYNC_UP      "UP"
    }

    array set MAP {
        ""              "DISABLED"
        "IDLE"          "IDLE"
        "DISABLED"      "DISABLED"
        "LINK ALIVE"    "LINK_ALIVE"
        "CFG MATCH"     "CONFIG_MATCH"
        "UP"            "L1_SYNC_UP"
    }

    cleanJunk $buffer buffer

    printLog ""
    printLog ""
    printLog "**************************************************************************"
    printLog "**************************************************************************"
    printLog $buffer
    printLog "**************************************************************************"
    printLog "**************************************************************************"
    printLog ""
    printLog ""

    foreach line [ split $buffer "\n" ] {

        set first_index_element [ lindex [split $line "|" ] 0 ]
        set third_index_element [ lindex [split $line "|" ] 2 ]

        set result_1 [regexp {(wri[0-9]+)} $first_index_element port_num_retrieved]
        set result_2 [regexp {([0-9]+)} $third_index_element y]

        if {$result_1 && $result_2} {
            if {$port_num_retrieved == $port_num} {
                set matching_line $line
                continue
            }
        }
    }

    if {![info exists matching_line]} {
        printLog "Expected lines are not available in the buffer"
        return 0
    }

    set states [lindex [split $matching_line "|"] 6]

    set l1sync_state_retrieved [lindex [split $states "/"] 1]

    if {[regexp {([a-zA-Z0-9\_\ ]+)} $l1sync_state_retrieved l1sync_state_retrieved]} { # Do Nothing. }

    set l1sync_state_retrieved [string trimleft  $l1sync_state_retrieved " "]
    set l1sync_state_retrieved [string trimright $l1sync_state_retrieved " "]

    if {$l1sync_state == [set L1_SYNC_STATE(DISABLED)]} {

        if {([set L1_SYNC_STATE(DISABLED)] == $l1sync_state_retrieved) ||\
            ([set L1_SYNC_STATE(DISABLED1)] == $l1sync_state_retrieved)} {

            printLog "L1 Sync state is matching (Exp-->$l1sync_state, Rcvd-->[set MAP($l1sync_state_retrieved)])"
            return 1
        }

    } else {

        if {[set L1_SYNC_STATE($l1sync_state)] == $l1sync_state_retrieved} {

            printLog "L1 Sync state is matching (Exp-->$l1sync_state, Rcvd-->[set MAP($l1sync_state_retrieved)])"
            return 1

        } else {

            printLog "L1 Sync state is not matching (Exp-->$l1sync_state, Rcvd-->[set MAP($l1sync_state_retrieved)])"
        }

    }

    return 0
}

#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_check_ptp_port_state               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  port_state        : (Mandatory) portState.                                  #
#                                  (e.g., "INITIALIZING"|"FAULTY"|"DISABLED"|  #
#                                         "LISTENING"|"PRE_MASTER"|"MASTER"|   #
#                                         "PASSIVE"|"UNCALIBRATED"|"SLAVE"     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given portState is verified in the     #
#                                    specified port).                          #
#                                                                              #
#                      0 on Failure (If given portState is not verified in the #
#                                    specified port).                          #
#                                                                              #
#  DEFINITION        : This function verifies the given portState in a specific#
#                      port.                                                   #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_check_ptp_port_state {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set port_state          $param(-port_state)
    set buffer              $param(-buffer)

    ## portState ###
    array set PORTSTATE {
        IDLE                "0"
        INITIALIZING        "1"
        FAULTY              "2"
        DISABLED            "3"
        LISTENING           "4"
        PRE_MASTER          "5"
        MASTER              "6"
        PASSIVE             "7"
        UNCALIBRATED        "8"
        SLAVE               "9"
        PRESENT             "100"
        S_LOCK              "101"
        M_LOCK              "102"
        LOCKED              "103"
        CALIBRATION         "104"
        CALIBRATED          "105"
        RESP_CALIB_REQ      "106"
        WR_LINK_ON          "107"
    }

    array set MAP {
        "1"                 "INITIALIZING"
        "2"                 "FAULTY"
        "3"                 "DISABLED"
        "4"                 "LISTENING"
        "5"                 "PRE_MASTER"
        "6"                 "MASTER"
        "7"                 "PASSIVE"
        "8"                 "UNCALIBRATED"
        "9"                 "SLAVE"
        "0"                 "IDLE"
        "100"               "SLAVE"
        "101"               "S_LOCK"
        "102"               "M_LOCK"
        "103"               "LOCKED"
        "104"               "CALIBRATION"
        "105"               "CALIBRATED"
        "106"               "RESP_CALIB_REQ"
        "107"               "WR_LINK_ON"
    }

    set state_expected   $PORTSTATE($port_state)

    foreach line [split $buffer "\n"] {

        if {[regexp {(.info.state:)\s+([a-zA-Z0-9]+)} $line match a state_retrieved]} {
        }

        if {[regexp {(.info.cfg.iface_name:)\s+([a-zA-Z0-9\"]+)} $line match dump port_retrieved]} {

            set port_retrieved [string trim $port_retrieved "\""]

            if {$port_num == $port_retrieved} {

                printLog "Port is matching (Exp->$port_num, Rcvd->$port_retrieved)"

                if {$state_expected == $state_retrieved} {
                    printLog "portState is matching (Exp->$MAP($state_expected)($state_expected), Rcvd->$MAP($state_retrieved)($state_retrieved))"
                    return 1
                } else {
                    printLog "portState is not matching (Exp->$MAP($state_expected)($state_expected), Rcvd->$MAP($state_retrieved)($state_retrieved))"
                    return 0
                }

            } else {
                printLog "Port is not matching (Exp->$port_num, Rcvd->$port_retrieved)"
            }
        }

    }

    return 0
}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_check_asymmetry_correction         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  state             : (Mandatory) asymmetryCorrectionPortDS.enable.           #
#                                  (e.g., "ENABLED"|"DISABLED"                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given state of                         #
#                                    asymmetryCorrectionPortDS.enable is       #
#                                    verified for the specified port).         #
#                                                                              #
#                      0 on Failure (If given state of                         #
#                                    asymmetryCorrectionPortDS.enable could not#
#                                    be verified for the specified port).      #
#                                                                              #
#  DEFINITION        : This function verifies the given state of               #
#                      asymmetryCorrectionPortDS.enable for a specific port.   #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_check_asymmetry_correction {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set state               $param(-state)
    set buffer              $param(-buffer)

    set portno [regexp {([0-9]+)} $port_num port_expected]
    set port_expected [expr $port_expected - 1]

    array set VALUE {
       0  "FALSE"
       1  "TRUE"
    }

    foreach line [split $buffer "\n"] {

        if {[regexp {(ppsi.inst.)([0-9]+)(.info.asymmetryCorrectionPortDS.enable:)\s+([0-9\+\-\.]+)} $line match a port_retrieved b state_retrieved]} {

            if {$port_expected == $port_retrieved} {

                printLog "Port is matching (Exp->$port_expected, Rcvd->$port_retrieved)"

                if {$state == $VALUE($state_retrieved)} {

                    printLog "asymmetryCorrectionPortDS.enable is matching (Exp->$state, Rcvd->$VALUE($state_retrieved))"
                    return 1
                } else {

                    printLog "asymmetryCorrectionPortDS.enable is not matching (Exp->$state, Rcvd->$VALUE($state_retrieved))"
                }

            } else {
                printLog "Port is not matching (Exp->$port_expected, Rcvd->$port_retrieved)"
            }
        }
    }

    return 0
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_check_egress_latency               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Mandatory) timestampCorrectionPortDS.egressLatency.    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of                         #
#                                    timestampCorrectionPortDS.egressLatency   #
#                                    verified for the specified port).         #
#                                                                              #
#                      0 on Failure (If given value of                         #
#                                    timestampCorrectionPortDS.egressLatency   #
#                                    could not be verified for the specified   #
#                                    port).                                    #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      timestampCorrectionPortDS.egressLatency for a specific  #
#                      port.                                                   #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_check_egress_latency {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set latency             $param(-latency)
    set buffer              $param(-buffer)

    set portno [regexp {([0-9]+)} $port_num port_expected]
    set port_expected [expr $port_expected - 1]

#   set latency [ expr $latency - 0.001 ]
#   set latency [ format {%014.3f} $latency ]

    if { $latency > 0 } { set latency "+$latency" }

    foreach line [split $buffer "\n"] {

        if {[regexp {(ppsi.inst.)([0-9]+)(.info.timestampCorrectionPortDS.egressLatency:)\s+([0-9\+\-\.]+)} $line match a port_retrieved b latency_retrieved]} {

            if {$port_expected == $port_retrieved} {

                printLog "Port is matching (Exp->$port_expected, Rcvd->$port_retrieved)"

                if {[regsub -all {(--)} $latency_retrieved "-" latency_retrieved]} {# Do nothing.}; # This has to be removed, as this added due to bug in device.

                if {$latency == $latency_retrieved} {

                    printLog "egressLatency is matching (Exp->$latency, Rcvd->$latency_retrieved)"
                    return 1
                } else {

                    printLog "egressLatency is not matching (Exp->$latency, Rcvd->$latency_retrieved)"
                }

            } else {
                printLog "Port is not matching (Exp->$port_expected, Rcvd->$port_retrieved)"
            }
        }
    }

    return 0
}


#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_check_ingress_latency              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Mandatory) timestampCorrectionPortDS.ingressLatency.   #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of                         #
#                                    timestampCorrectionPortDS.ingressLatency  #
#                                    verified for the specified port).         #
#                                                                              #
#                      0 on Failure (If given value of                         #
#                                    timestampCorrectionPortDS.ingressLatency  #
#                                    could not be verified for the specified   #
#                                    port).                                    #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      timestampCorrectionPortDS.ingressLatency for a specific #
#                      port.                                                   #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_check_ingress_latency {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set latency             $param(-latency)
    set buffer              $param(-buffer)

    set portno [regexp {([0-9]+)} $port_num port_expected]
    set port_expected [expr $port_expected - 1]

    if { $latency > 0 } { set latency "+$latency" }

    foreach line [split $buffer "\n"] {

        if {[regexp {(ppsi.inst.)([0-9]+)(.info.timestampCorrectionPortDS.ingressLatency:)\s+([0-9\+\-\.]+)} $line match a port_retrieved b latency_retrieved]} {

            if {$port_expected == $port_retrieved} {

                printLog "Port is matching (Exp->$port_expected, Rcvd->$port_retrieved)"

                if {[regsub -all {(--)} $latency_retrieved "-" latency_retrieved]} {# Do nothing.}; # This has to be removed, as this added due to bug in device.

                if {$latency == $latency_retrieved} {

                    printLog "ingressLatency is matching (Exp->$latency, Rcvd->$latency_retrieved)"
                    return 1
                } else {

                    printLog "ingressLatency is not matching (Exp->$latency, Rcvd->$latency_retrieved)"
                }

            } else {
                printLog "Port is not matching (Exp->$port_expected, Rcvd->$port_retrieved)"
            }
        }
    }

    return 0
}

#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_check_constant_asymmetry           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  constant_asymmetry: (Mandatory) asymmetryCorrectionPortDS.constantAsymmetry.#
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of                         #
#                                    asymmetryCorrectionPortDS.                #
#                                    constantAsymmetry verified for the        #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If given value of                         #
#                                    asymmetryCorrectionPortDS.                #
#                                    constantAsymmetry could not be verified   #
#                                    for specified port at the DUT).           #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      asymmetryCorrectionPortDS.constantAsymmetry for a       #
#                      specific port.                                          #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_check_constant_asymmetry {args} {

    array set param $args

    set session_handler               $param(-session_handler)
    set port_num                      $param(-port_num)
    set constant_asymmetry            $param(-constant_asymmetry)
    set buffer                        $param(-buffer)

    set portno [regexp {([0-9]+)} $port_num port_expected]
    set port_expected [expr $port_expected - 1]

    if { $constant_asymmetry > 0 } { set constant_asymmetry "+$constant_asymmetry" }

    foreach line [split $buffer "\n"] {

        if {[regexp {(ppsi.inst.)([0-9]+)(.info.asymmetryCorrectionPortDS.constantAsymmetry:)\s+([0-9\+\-\.]+)} $line match a port_retrieved b constant_asymmetry_retrieved]} {

            if {$port_expected == $port_retrieved} {

                printLog "Port is matching (Exp->$port_expected, Rcvd->$port_retrieved)"

                if {[regsub -all {(--)} $constant_asymmetry_retrieved "-" constant_asymmetry_retrieved]} {# Do nothing.}; # This has to be removed, as this added due to bug in device.

                if {$constant_asymmetry == $constant_asymmetry_retrieved} {

                    printLog "constantAsymmetry is matching (Exp->$constant_asymmetry, Rcvd->$constant_asymmetry_retrieved)"
                    return 1
                } else {

                    printLog "constantAsymmetry is not matching (Exp->$constant_asymmetry, Rcvd->$constant_asymmetry_retrieved)"
                }

            } else {
                printLog "Port is not matching (Exp->$port_expected, Rcvd->$port_retrieved)"
            }
        }
    }

    return 0
}


#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_check_scaled_delay_coefficient     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  delay_coefficient : (Mandatory) asymmetryCorrectionPortDS.                  #
#                                  scaledDelayCoefficient.                     #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of                         #
#                                    asymmetryCorrectionPortDS.                #
#                                    scaledDelayCoefficient verified for the   #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If given value of                         #
#                                    asymmetryCorrectionPortDS.                #
#                                    scaledDelayCoefficient could not be       #
#                                    verified for specified port at the DUT).  #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      asymmetryCorrectionPortDS.scaledDelayCoefficient for a  #
#                      specific port.                                          #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_check_scaled_delay_coefficient {args} {

    array set param $args

    set session_handler             $param(-session_handler)
    set port_num                    $param(-port_num)
    set delay_coefficient           $param(-delay_coefficient)
    set buffer                      $param(-buffer)
 
    set portno [regexp {([0-9]+)} $port_num port_expected]
    set port_expected [expr $port_expected - 1]

    if { $delay_coefficient > 0 } { set delay_coefficient "+$delay_coefficient" }

    foreach line [split $buffer "\n"] {

        if {[regexp {(ppsi.inst.)([0-9]+)(.info.asymmetryCorrectionPortDS.scaledDelayCoefficient:)\s+([0-9\+\-\.]+)} $line match a port_retrieved b delay_coefficient_retrieved]} {

            if {$port_expected == $port_retrieved} {

                printLog "Port is matching (Exp->$port_expected, Rcvd->$port_retrieved)"

                set delay_coefficient_min       [expr $delay_coefficient_retrieved - 0.000000000001]
                set delay_coefficient_max       [expr $delay_coefficient_retrieved + 0.000000000001]

                if {($delay_coefficient >= $delay_coefficient_min) && ($delay_coefficient <= $delay_coefficient_max)} {

                    printLog "scaledDelayCoefficient is matching (Exp->$delay_coefficient, Rcvd->$delay_coefficient_retrieved)"
                    return 1
                } else {

                    printLog "scaledDelayCoefficient is not matching (Exp->$delay_coefficient, Rcvd->$delay_coefficient_retrieved)"
                }

            } else {
                printLog "Port is not matching (Exp->$port_expected, Rcvd->$port_retrieved)"
            }
        }
    }

    return 0
}


#8.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_check_external_port_config         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  state             : (Mandatory) State of external port configuration.       #
#                                  (e.g., "ENABLED"|"DISABLED")                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given state of external port           #
#                                    configuration is verified for the         #
#                                    specified port).                          #
#                                                                              #
#                      0 on Failure (If given state of external port           #
#                                    configuration could not be verified for   #
#                                    the specified port).                      #
#                                                                              #
#  DEFINITION        : This function verifies the given state of external      #
#                      port configuration for a specific port.                 #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_check_external_port_config {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set state               $param(-state)
    set buffer              $param(-buffer)

    array set VALUE {
       0  "FALSE"
       1  "TRUE"
    }

    foreach line [ split $buffer "\n" ] {
        if {[regexp {(ppsi.defaulDS.externalPortConfigurationEnabled:)\s+([0-9]+)} $line match a state_retrieved]} {

            if {$state == $VALUE($state_retrieved)} {

                printLog "externalPortConfigurationEnabled is matching (Exp->$state, Rcvd->$VALUE($state_retrieved))"
                return 1
            } else {

                printLog "externalPortConfigurationEnabled is not matching (Exp->$state, Rcvd->$VALUE($state_retrieved))"
            }
        }
    }
    return 0
}


#9.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_check_slave_only                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  value             : (Mandatory) value of defaultDS.slaveOnly                #
#                                  (e.g., "TRUE"|"FALSE")                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of defaultDS.slaveOnly     #
#                                    is verified for the specified port).      #
#                                                                              #
#                      0 on Failure (If given value of defaultDS.slaveOnly     #
#                                    could not be verified for the specified   #
#                                    port).                                    #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      defaultDS.slaveOnly for a specific port.                #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_check_slave_only {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set value               $param(-value)
    set buffer              $param(-buffer)

    array set VALUE {
       0  "FALSE"
       1  "TRUE"
    }

    foreach line [ split $buffer "\n" ] {
       if {[regexp {(ppsi.defaulDS.slaveOnly:)\s+([0-9]+)} $line match a value_retrieved]} { }
    }

    if {$value == $VALUE($value_retrieved)} {

         printLog "The value of defaultDS.slaveOnly is matching.(Exp->$value, Rcvd->$VALUE($value_retrieved))"
         return 1
    } else {

         printLog "The value of defaultDS.slaveOnly is not matching.(Exp->$value, Rcvd->$VALUE($value_retrieved))"
    }

    return 0
}


#10.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_check_master_only                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  value             : (Mandatory) value of portDS.masterOnly                  #
#                                  (e.g., "TRUE"|"FALSE")                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of portDS.masterOnly       #
#                                    is verified for the specified port).      #
#                                                                              #
#                      0 on Failure (If given value of portDS.masterOnly       #
#                                    could not be verified for the specified   #
#                                    port).                                    #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      portDS.masterOnly for a specific port.                  #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_check_master_only {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set value_expected      $param(-value)
    set buffer              $param(-buffer)

    array set MAP {
       0  "FALSE"
       1  "TRUE"
    }

    set portno [regexp {([0-9]+)} $port_num port_expected]
    set port_expected [expr $port_expected - 1]

    foreach line [split $buffer "\n"] {

        if {[regexp {(ppsi.inst.)([0-9]+)(.portDS.masterOnly:)\s+([0-9\+\-\.]+)} $line match a port_retrieved b value_retrieved]} {

            if {$port_expected == $port_retrieved} {

                printLog "Port is matching (Exp->$port_expected, Rcvd->$port_retrieved)"

                if {$value_expected == $MAP($value_retrieved)} {

                    printLog "portDS.masterOnly is matching (Exp->$value_expected, Rcvd->$MAP($value_retrieved))"
                    return 1
                } else {

                    printLog "portDS.masterOnly is not matching (Exp->$value_expected, Rcvd->$MAP($value_retrieved))"
                }
            } else {
                printLog "Port is not matching (Exp->$port_expected, Rcvd->$port_retrieved)"
            }
        }
    }

    return 0
}


#11.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_check_for_least_offset_from_master #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  offset_from_master: (Mandatory) currentDS.offsetFromMaster.                 #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  RETURNS           : 1 on Success (If the absolute value of currentDS.       #
#                                    offsetFromMaster in DUT becomes lesser    #
#                                    than the specified value to ensure that   #
#                                    the DUT synchronized it's time with       #
#                                    messages from TEE).                       #
#                                                                              #
#                      0 on Failure (If the absolute value of currentDS.       #
#                                    offsetFromMaster in DUT does not become   #
#                                    lesser than the specified value to ensure #
#                                    that the DUT synchronized it's time with  #
#                                    messages from TEE).                       #
#                                                                              #
#  DEFINITION        : This function invokes recursive procedure to check      #
#                      whether the absolute value of currentDS.offsetFromMaster#
#                      is lesser than the specified value to ensure that DUT   #
#                      synchronized it's time with messages from TEE.          #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_check_for_least_offset_from_master {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set offset_from_master  $param(-offset_from_master)
    set buffer              $param(-buffer)

    foreach line [split $buffer "\n"] {

        set parameters  [split $line ":"]

        set name        [lindex $parameters 0]
        set value       [lindex $parameters 1]

        if {[string match "*ppsi.currentDS.offsetFromMaster*" $name]} {

            if {[regexp {([0-9\.\-\+]+)} $value match offset_from_master_retrieved]} {

                printLog "currentDS.offsetFromMaster retrieved: $offset_from_master_retrieved nsec"
                break
            }
        }
    }

    if { ([expr abs($offset_from_master_retrieved)] <= $offset_from_master) && ([expr abs($offset_from_master_retrieved)] != 0) } {

        printLog "currentDS.offsetFromMaster becomes lesser than expected non-zero value (Exp-><= $offset_from_master nsec, Rcvd->[expr abs($offset_from_master_retrieved)] nsec)"
        return 1

    }

    printLog "currentDS.offsetFromMaster does not become lesser than expected non-zero value (Exp-><= $offset_from_master nsec, Rcvd->[expr abs($offset_from_master_retrieved)] nsec)"

    set retry_max       100
    set retry_count     1

    while {($retry_count <= $retry_max)} {

        printLog "retry: $retry_count/$retry_max"

        incr retry_count

        if {[ptp_ha::callback_get_offset_from_master\
                    -session_handler     $session_handler\
                    -offset_from_master  offset_from_master_retrieved]} {

            if { ([expr abs($offset_from_master_retrieved)] <= $offset_from_master) && ([expr abs($offset_from_master_retrieved)] != 0) } {

                printLog "currentDS.offsetFromMaster becomes lesser than expected non-zero value (Exp-><= $offset_from_master nsec, Rcvd->[expr abs($offset_from_master_retrieved)] nsec)"
                return 1
            }

            printLog "currentDS.offsetFromMaster does not become lesser than expected non-zero value (Exp-><= $offset_from_master nsec, Rcvd->[expr abs($offset_from_master_retrieved)] nsec)"
        }

        sleep 1
    }

    return 0
}


################################################################################
#                           DUT GET CALLBACK                                   #
################################################################################

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_get_timestamp                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  timestamp         : (Mandatory) Timestamp. (Epoch Format)                   #
#                                  (e.g., xxxxx)                               #
#                                                                              #
#                                                                              #
#  RETURNS           : 1 on Success (If able to get timestamp from DUT).       #
#                                                                              #
#                      0 on Failure (If could not able to get timestamp from   #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function fetch timestamp from DUT.                 #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_get_timestamp {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set buffer              $param(-buffer)

    upvar $param(-timestamp) timestamp

    foreach line [split $buffer "\n"] {

        if {[regexp {([0-9\.]+) (TAI)} $line match utc_timestamp_complete]} {

            set utc_timestamp_sec  [string trimleft [lindex [split $utc_timestamp_complete "."] 0] "0"]
            set utc_timestamp_msec [string trimleft [lindex [split $utc_timestamp_complete "."] 1] "0"]

            set utc_epoch_timestamp_sec $utc_timestamp_sec
#           set utc_epoch_timestamp_sec [clock scan "$utc_timestamp_sec" -gmt true]

            set utc_epoch_timestamp_sec [format "%.f" [expr $utc_epoch_timestamp_sec * 1000000000.0]]
            set utc_timestamp_msec  [format "%.f" [expr $utc_timestamp_msec * 1.0]]

            set timestamp [expr $utc_epoch_timestamp_sec + $utc_timestamp_msec]

            printLog "Timestamp retrieved: $timestamp"
            return 1
        }
    }

    return 0
}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_get_mean_delay                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  mean_delay        : (Mandatory) currentDS.meanDelay.                        #
#                                  (e.g., 3)                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If able to extract currentDS.meanDelay of #
#                                    specified port from the DUT).             #
#                                                                              #
#                      0 on Failure (If could not able to extract              #
#                                    currentDS.meanDelay of specified port from#
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function extracts currentDS.meanDelay of a specific#
#                      port from the DUT.                                      #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_get_mean_delay {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set buffer              $param(-buffer)

    upvar $param(-mean_delay) mean_delay

    foreach line [split $buffer "\n"] {

        if {[regexp {(ppsi.currentDS.meanDelay:)\s+([0-9\+\-\.]+)} $line match a mean_delay]} {

            printLog "currentDS.meanDelay retrieved: $mean_delay nsec"
            set mean_delay  $mean_delay
        }
    }

    if { $mean_delay != 0 } { return 1 }

    set retry_max       100
    set retry_count     1

    while {($retry_count <= $retry_max)} {

        printLog "retry: $retry_count/$retry_max"

        incr retry_count

        if {[ptp_ha::custom_callback_get_mean_delay\
                    -session_handler     $session_handler\
                    -port_num            $port_num\
                    -mean_delay          mean_delay]} {

            if { $mean_delay != 0 } { return 1 }
        }

        sleep 1
    }

    return 0
}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_get_mean_link_delay                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  mean_link_delay   : (Mandatory) portDS.meanLinkDelay.                       #
#                                  (e.g., 3)                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If able to extract portDS.meanLinkDelay of#
#                                    specified port from the DUT).             #
#                                                                              #
#                      0 on Failure (If could not able to extract              #
#                                    portDS.meanLinkDelay of specified port    #
#                                    from the DUT).                            #
#                                                                              #
#  DEFINITION        : This function extracts portDS.meanLinkDelay of a        #
#                      specific port from the DUT.                             #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_get_mean_link_delay {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set buffer              $param(-buffer)

    upvar $param(-mean_link_delay) mean_link_delay

    set portno [regexp {([0-9]+)} $port_num port_expected]
    set port_expected [expr $port_expected - 1]

    foreach line [split $buffer "\n"] {

        if {[regexp {(ppsi.inst.)([0-9]+)(.portDS.meanLinkDelay:)\s+([0-9\+\-\.]+)} $line match a port_retrieved b mean_link_delay]} {

            if {$port_expected == $port_retrieved} {

                printLog "Port is matching (Exp->$port_expected, Rcvd->$port_retrieved)"
                printLog "portDS.meanLinkDelay retrieved: $mean_link_delay nsec"
                break
            } else {

                printLog "Port is not matching (Exp->$port_expected, Rcvd->$port_retrieved)"
            }
        }
    }

    if { $mean_link_delay != 0 } { return 1 }

    set retry_max       100
    set retry_count     1

    while {($retry_count <= $retry_max)} {

        printLog "retry: $retry_count/$retry_max"

        incr retry_count

        if {[ptp_ha::custom_callback_get_mean_link_delay\
                    -session_handler     $session_handler\
                    -port_num            $port_num\
                    -mean_link_delay     mean_link_delay]} {

            if { $mean_link_delay != 0 } { return 1 }
        }

        sleep 1
    }

    return 0
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_cli_get_delay_asymmetry                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  delay_asymmetry   : (Mandatory) portDS.delayAsymmetry.                      #
#                                  (e.g., 3)                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If able to extract portDS.delayAsymmetry  #
#                                    of specified port from the DUT).          #
#                                                                              #
#                      0 on Failure (If could not able to extract              #
#                                    portDS.delayAsymmetry of specified port   #
#                                    from the DUT).                            #
#                                                                              #
#  DEFINITION        : This function extracts portDS.delayAsymmetry of a       #
#                      specific port from the DUT.                             #
#                                                                              #
################################################################################

proc ptp_ha::callback_cli_get_delay_asymmetry {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set buffer              $param(-buffer)

    upvar $param(-delay_asymmetry) delay_asymmetry

    set portno [regexp {([0-9]+)} $port_num port_expected]
    set port_expected [expr $port_expected - 1]

    foreach line [split $buffer "\n"] {

        if {[regexp {(ppsi.inst.)([0-9]+)(.portDS.delayAsymmetry:)\s+([0-9\+\-\.]+)} $line match a port_retrieved b delay_asymmetry_retrieved]} {

            if { $port_expected == $port_retrieved } {

                set delay_asymmetry $delay_asymmetry_retrieved
                printLog "portDS.delayAsymmetry retrieved: $delay_asymmetry_retrieved"
                return 1
            } else {

                printLog "Port is not matching (Exp->$port_expected, Rcvd->$port_retrieved)"
            }
        }
    }

   return 0
}



################################################################################
#                          DUT MISCELLANEOUS FUNCTIONS                         #
################################################################################

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_get_offset_from_master                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  offset_from_master : (Mandatory) currentDS.offsetFromMaster                 #
#                                  (e.g., 3)                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If able to extract currentDS.             #
#                                    offsetFromMaster from the DUT).           #
#                                                                              #
#                      0 on Failure (If could not able to extract              #
#                                    currentDS.offsetFromMaster from the DUT). #
#                                                                              #
#  DEFINITION        : This function extracts currentDS.offsetFromMaster from  #
#                      the DUT.                                                #
#                                                                              #
################################################################################

proc ptp_ha::callback_get_offset_from_master {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::callback_get_offset_from_master    -> session_handler"
        incr missedArgCounter
    }

    upvar $param(-offset_from_master) offset_from_master 

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {![Send_cli_command_to_read \
                    -session_handler        $session_handler \
                    -cmd_type               "DUT_CHECK_OFFSET_FROM_MASTER"\
                    -output                 buffer]} {

        LOG -level 4 -msg "ptp_ha::callback_get_offset_from_master: Error while getting currentDS.offsetFromMaster"
        return 0
    }

    foreach line [split $buffer "\n"] {

        set parameters  [split $line ":"]

        set name        [lindex $parameters 0]
        set value       [lindex $parameters 1]

        if {[string match "*ppsi.currentDS.offsetFromMaster*" $name]} {

            if {[regexp {([0-9\.\-\+]+)} $value match offset_from_master]} {

                printLog "currentDS.offsetFromMaster retrieved: $offset_from_master nsec"
            }
        }
    }

    return 1
}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::custom_callback_get_mean_delay                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  mean_delay        : (Mandatory) currentDS.meanDelay.                        #
#                                  (e.g., 3)                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If able to extract currentDS.meanDelay of #
#                                    specified port from the DUT).             #
#                                                                              #
#                      0 on Failure (If could not able to extract              #
#                                    currentDS.meanDelay of specified port from#
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function extracts currentDS.meanDelay of a specific#
#                      port from the DUT.                                      #
#                                                                              #
################################################################################

proc ptp_ha::custom_callback_get_mean_delay {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::custom_callback_get_mean_delay : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::custom_callback_get_mean_delay : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    ### Checking Output Parameters ###

    if {[info exists param(-mean_delay)]} {
        upvar $param(-mean_delay) mean_delay
    } else {
        ERRLOG -msg "ptp_ha::custom_callback_get_mean_delay : Missing Argument    -> mean_delay"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {![Send_cli_command_to_read \
                    -session_handler        $session_handler \
                    -port_num               $port_num\
                    -cmd_type               "DUT_GET_MEAN_DELAY"\
                    -output                 buffer]} {

        LOG -level 4 -msg "ptp_ha::custom_callback_get_mean_delay : Error while getting currentDS.meanDelay"
        return 0
    }

    foreach line [split $buffer "\n"] {

        if {[regexp {(ppsi.currentDS.meanDelay:)\s+([0-9\+\-\.]+)} $line match a mean_delay_retrieved]} {

            printLog "currentDS.meanDelay retrieved: $mean_delay_retrieved"
            set mean_delay  $mean_delay_retrieved
        }
    }

    return 1
}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::custom_callback_get_mean_link_delay             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  mean_link_delay   : (Mandatory) currentDS.meanLinkDelay.                    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If able to extract currentDS.meanLinkDelay#
#                                    of specified port from the DUT).          #
#                                                                              #
#                      0 on Failure (If could not able to extract              #
#                                    currentDS.meanLinkDelay of specified port #
#                                    from the DUT).                            #
#                                                                              #
#  DEFINITION        : This function extracts currentDS.meanLinkDelay of a     #
#                      specific port from the DUT.                             #
#                                                                              #
################################################################################

proc ptp_ha::custom_callback_get_mean_link_delay {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::custom_callback_get_mean_link_delay : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::custom_callback_get_mean_link_delay : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    ### Checking Output Parameters ###

    if {[info exists param(-mean_link_delay)]} {
        upvar $param(-mean_link_delay) mean_link_delay
    } else {
        ERRLOG -msg "ptp_ha::custom_callback_get_mean_link_delay : Missing Argument    -> mean_delay"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {![Send_cli_command_to_read \
                    -session_handler        $session_handler \
                    -port_num               $port_num\
                    -cmd_type               "DUT_GET_MEAN_LINK_DELAY"\
                    -output                 buffer]} {

        LOG -level 4 -msg "ptp_ha::custom_callback_get_mean_link_delay : Error while getting currentDS.meanLinkDelay"
        return 0
    }

    set portno [regexp {([0-9]+)} $port_num port_expected]
    set port_expected [expr $port_expected - 1]

    foreach line [split $buffer "\n"] {

        if {[regexp {(ppsi.inst.)([0-9]+)(.portDS.meanLinkDelay:)\s+([0-9\+\-\.]+)} $line match a port_retrieved b mean_link_delay]} {

            if {$port_expected == $port_retrieved} {

                printLog "Port is matching (Exp->$port_expected, Rcvd->$port_retrieved)"
                printLog "portDS.meanLinkDelay retrieved: $mean_link_delay nsec"
                break
            } else {

                printLog "Port is not matching (Exp->$port_expected, Rcvd->$port_retrieved)"
            }
        }
    }

    return 1
}
