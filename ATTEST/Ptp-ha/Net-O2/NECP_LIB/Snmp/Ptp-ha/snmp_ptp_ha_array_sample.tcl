#!/usr/bin/tcl
###############################################################################
# File Name      : snmp_ptp_ha_array_sample.tcl                               #
# File Version   : 1.3                                                        #
# Component Name : ATTEST SNMP ARRAY MAPPING                                  #
# Module Name    : PTP High Accuracy                                          #
#                                                                             #
###############################################################################
#                                                                             #
#     SET ARRAY INDEX                                                         #
#                                                                             #
#  DUT SET FUNCTIONS     :                                                    #
#                                                                             #
#  1) DUT_PORT_ENABLE                                                         #
#  2) DUT_PORT_PTP_ENABLE                                                     #
#  3) DUT_SET_VLAN                                                            #
#  4) DUT_SET_VLAN_PRIORITY                                                   #
#  5) DUT_GLOBAL_PTP_ENABLE                                                   #
#  6) DUT_SET_COMMUNICATION_MODE                                              #
#  7) DUT_SET_DOMAIN                                                          #
#  8) DUT_SET_NETWORK_PROTOCOL                                                #
#  9) DUT_SET_CLOCK_MODE                                                      #
# 10) DUT_SET_CLOCK_STEP                                                      #
# 11) DUT_SET_DELAY_MECHANISM                                                 #
# 12) DUT_SET_PRIORITY1                                                       #
# 13) DUT_SET_PRIORITY2                                                       #
# 14) DUT_TCR_RCR_CR_ENABLE                                                   #
# 15) DUT_L1SYNC_ENABLE                                                       #
# 16) DUT_L1SYNC_OPT_PARAMS_ENABLE                                            #
# 17) DUT_SLAVEONLY_ENABLE                                                    #
# 18) DUT_MASTERONLY_ENABLE                                                   #
# 19) DUT_SET_ANNOUNCE_INTERVAL                                               #
# 20) DUT_SET_ANNOUNCE_TIMEOUT                                                #
# 21) DUT_SET_SYNC_INTERVAL                                                   #
# 22) DUT_SET_DELAYREQ_INTERVAL                                               #
# 23) DUT_SET_PDELAYREQ_INTERVAL                                              #
# 24) DUT_SET_L1SYNC_INTERVAL                                                 #
# 25) DUT_SET_L1SYNC_TIMEOUT                                                  #
# 26) DUT_SET_EGRESS_LATENCY                                                  #
# 27) DUT_SET_INGRESS_LATENCY                                                 #
# 28) DUT_SET_CONSTANT_ASYMMETRY                                              #
# 29) DUT_SET_SCALED_DELAY_COEFFICIENT                                        #
# 30) DUT_SET_ASYMMETRY_CORRECTION                                            #
# 31) DUT_EXTERNAL_PORT_CONFIG_ENABLE                                         #
# 32) DUT_SET_EPC_DESIRED_STATE                                               #
# 33) DUT_SET_IPV4_ADDRESS                                                    #
# 34) DUT_TIMESTAMP_CORRECTED_TX_ENABLE                                       #
#                                                                             #
#  DUT DISABLE FUNCTIONS :                                                    #
#                                                                             #
#  1) DUT_PORT_DISABLE                                                        #
#  2) DUT_PORT_PTP_DISABLE                                                    #
#  3) DUT_RESET_VLAN                                                          #
#  4) DUT_RESET_VLAN_PRIORITY                                                 #
#  5) DUT_GLOBAL_PTP_DISABLE                                                  #
#  6) DUT_RESET_COMMUNICATION_MODE                                            #
#  7) DUT_RESET_DOMAIN                                                        #
#  8) DUT_RESET_NETWORK_PROTOCOL                                              #
#  9) DUT_RESET_CLOCK_MODE                                                    #
# 10) DUT_RESET_CLOCK_STEP                                                    #
# 11) DUT_RESET_DELAY_MECHANISM                                               #
# 12) DUT_RESET_PRIORITY1                                                     #
# 13) DUT_RESET_PRIORITY2                                                     #
# 14) DUT_TCR_RCR_CR_DISABLE                                                  #
# 15) DUT_L1SYNC_DISABLE                                                      #
# 16) DUT_L1SYNC_OPT_PARAMS_DISABLE                                           #
# 17) DUT_SLAVEONLY_DISABLE                                                   #
# 18) DUT_MASTERONLY_DISABLE                                                  #
# 19) DUT_RESET_ANNOUNCE_INTERVAL                                             #
# 20) DUT_RESET_ANNOUNCE_TIMEOUT                                              #
# 21) DUT_RESET_SYNC_INTERVAL                                                 #
# 22) DUT_RESET_DELAYREQ_INTERVAL                                             #
# 23) DUT_RESET_PDELAYREQ_INTERVAL                                            #
# 24) DUT_RESET_L1SYNC_INTERVAL                                               #
# 25) DUT_RESET_L1SYNC_TIMEOUT                                                #
# 26) DUT_RESET_EGRESS_LATENCY                                                #
# 27) DUT_ReSET_INGRESS_LATENCY                                               #
# 28) DUT_RESET_CONSTANT_ASYMMETRY                                            #
# 29) DUT_RESET_SCALED_DELAY_COEFFICIENT                                      #
# 30) DUT_RESET_ASYMMETRY_CORRECTION                                          #
# 31) DUT_EXTERNAL_PORT_CONFIG_DISABLE                                        #
# 32) DUT_RESET_EPC_DESIRED_STATE                                             #
# 33) DUT_RESET_IPV4_ADDRESS                                                  #
# 34) DUT_TIMESTAMP_CORRECTED_TX_DISABLE                                      #
#                                                                             #
#  DUT CHECK FUNCTIONS   :                                                    #
#                                                                             #
#  1) DUT_CHECK_L1SYNC_STATE                                                  #
#  2) DUT_CHECK_PTP_PORT_STATE                                                #
#  3) DUT_CHECK_ASYMMETRY_CORRECTION                                          #
#  4) DUT_CHECK_EGRESS_LATENCY                                                #
#  5) DUT_CHECK_INGRESS_LATENCY                                               #
#  6) DUT_CHECK_CONSTANT_ASYMMETRY                                            #
#  7) DUT_CHECK_SCALED_DELAY_COEFFICIENT                                      #
#  8) DUT_CHECK_EXTERNAL_PORT_CONFIG                                          #
#  9) DUT_CHECK_SLAVE_ONLY                                                    #
# 10) DUT_CHECK_MASTER_ONLY                                                   #
# 11) DUT_CHECK_OFFSET_FROM_MASTER                                            #
#                                                                             #
#  DUT GET FUNCTIONS     :                                                    #
#                                                                             #
#  1) DUT_GET_TIMESTAMP                                                       #
#  2) DUT_GET_MEAN_DELAY                                                      #
#  3) DUT_GET_MEAN_LINK_DELAY                                                 #
#  4) DUT_GET_DELAY_ASYMMETRY                                                 #
#                                                                             #
###############################################################################
# History     Date         Author         Addition/ Alteration                #
#                                                                             #
#  1.0       May/2018      CERN         Initial version                       #
#  1.1       Aug/2018      CERN         Added new procedures                  #
#                                       a) timestamp_corrected_tx_enable      #
#                                       b) timestamp_corrected_tx_disable     #
#                                       c) check_slave_only                   #
#                                       d) check_master_only                  #
#  1.2       Oct/2018      CERN         Added get_mean_link_delay.            #
#  1.3       Nov/2018      CERN         a) Removed get_offset_from_master.    #
#                                       b) Added                              #
#                                          check_for_least_offset_from_master #
#                                                                             #
###############################################################################
# Copyright (c) 2018 CERN                                                     #
###############################################################################


################################################################################
#                            DUT SET FUNCTIONS                                 #
################################################################################

proc  tee_set_dut_ptp_ha_snmp_from_file {} {

    global snmp_set_record
    global env ptp_ha_env


#1.
################################################################################
#                                                                              #
#  COMMAND           : PORT_ENABLE                                             #
#                                                                              #
#  ARRAY INDEX       : DUT_PORT_ENABLE                                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function makes the specific port administratively  #
#                      up at the DUT.                                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_PORT_ENABLE,1)              "NONE"


#2.
################################################################################
#                                                                              #
#  COMMAND           : PORT_PTP_ENABLE                                         #
#                                                                              #
#  ARRAY INDEX       : DUT_PORT_PTP_ENABLE                                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function enables PTP on a specific port on the DUT.#
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_PORT_PTP_ENABLE,1)          "NONE"


#3.
################################################################################
#                                                                              #
#  COMMAND           : SET_VLAN                                                #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_VLAN                                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = vlan_id     VLAN ID with which VLAN has to be created.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#   ATTEST_PARAM2    = port_num    Port which has to be associated to VLAN ID. #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function creates VLAN ID and associates given port #
#                      to it on the DUT.                                       #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_SET_VLAN,1)                 "NONE"


#4.
################################################################################
#                                                                              #
#  COMMAND           : SET_VLAN_PRIORITY                                       #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_VLAN_PRIORITY                                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = vlan_id     VLAN ID                                     #
#                                  (e.g., 100)                                 #
#                                                                              #
#   ATTEST_PARAM2    = vlan_priority VLAN priority                             #
#                                  (e.g., 1)                                   #
#                                                                              #
#   ATTEST_PARAM3    = port_num    Port which has to be associated to VLAN ID. #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function associates VLAN priority to the VLAN ID   #
#                      on the DUT.                                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_SET_VLAN_PRIORITY,1)        "NONE"


#5.
################################################################################
#                                                                              #
#  COMMAND           : GLOBAL_PTP_ENABLE                                       #
#                                                                              #
#  ARRAY INDEX       : DUT_GLOBAL_PTP_ENABLE                                   #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  DEFINITION        : This function enables PTP on the DUT.                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_GLOBAL_PTP_ENABLE,1)        "NONE"


#6.
################################################################################
#                                                                              #
#  COMMAND           : SET_COMMUNICATION_MODE                                  #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_COMMUNICATION_MODE                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = clock_mode  Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#   ATTEST_PARAM3    = ptp_version PTP version.                                #
#                                  (Default: 2)                                #
#                                                                              #
#   ATTEST_PARAM4    = communication_mode Communication mode                   #
#                                  (e.g., "Unicast"|"Multicast")               #
#                                                                              #
#  DEFINITION        : This function sets the communication mode on a specific #
#                      port of the DUT.                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_SET_COMMUNICATION_MODE,1)   "NONE"


#7.
################################################################################
#                                                                              #
#  COMMAND           : SET_DOMAIN                                              #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_DOMAIN                                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = clock_mode  Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#   ATTEST_PARAM2    = domain      Domain number.                              #
#                                  (e.g., 0..255)                              #
#                                                                              #
#  DEFINITION        : This function sets domain number on the DUT.            #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_SET_DOMAIN,1)               "NONE"


#8.
################################################################################
#                                                                              #
#  COMMAND           : SET_NETWORK_PROTOCOL                                    #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_NETWORK_PROTOCOL                                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = clock_mode  clock_mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#   ATTEST_PARAM3    = network_protocol Network protocol.                      #
#                                  (e.g., "IEEE 802.3"|"UDP/IPv4")             #
#                                                                              #
#  DEFINITION        : This function sets the network protocol on a specific   #
#                      port of the DUT.                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_SET_NETWORK_PROTOCOL,1)     "NONE"


#9.
################################################################################
#                                                                              #
#  COMMAND           : SET_CLOCK_MODE                                          #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_CLOCK_MODE                                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = clock_mode  Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  DEFINITION        : This function sets clock mode on a specific port of DUT.#
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_SET_CLOCK_MODE,1)           "NONE"


#10.
################################################################################
#                                                                              #
#  COMMAND           : SET_CLOCK_STEP                                          #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_CLOCK_STEP                                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = clock_mode  Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#   ATTEST_PARAM3    = clock_step  Clock step.                                 #
#                                  (e.g., "One-step"|"Two-step")               #
#                                                                              #
#  DEFINITION        : This function sets the clock-step on a specific port on #
#                      the DUT.                                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_SET_CLOCK_STEP,1)           "NONE"


#11.
################################################################################
#                                                                              #
#  COMMAND           : SET_DELAY_MECHANISM                                     #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_DELAY_MECHANISM                                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = clock_mode  Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#   ATTEST_PARAM3    = delay_mechanism Delay mechanism.                        #
#                                  (e.g., e2e|p2p|disable)                     #
#                                                                              #
#  DEFINITION        : This function sets the delay mechanism to a specific    #
#                      port on the DUT.                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_SET_DELAY_MECHANISM,1)      "NONE"


#12.
################################################################################
#                                                                              #
#  COMMAND           : SET_PRIORITY1                                           #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_PRIORITY1                                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = clock_mode  Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#   ATTEST_PARAM2    = priority1   Prority1                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_SET_PRIORITY1,1)            "NONE"


#13.
################################################################################
#                                                                              #
#  COMMAND           : SET_PRIORITY2                                           #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_PRIORITY2                                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = clock_mode  Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#   ATTEST_PARAM2    = priority2   Prority2                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_SET_PRIORITY2,1)            "NONE"


#14.
################################################################################
#                                                                              #
#  COMMAND           : TCR_RCR_CR_ENABLE                                       #
#                                                                              #
#  ARRAY INDEX       : DUT_TCR_RCR_CR_ENABLE                                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function enables peerTxCoherentIsRequired,         #
#                      peerRxCoherentIsRequired and peerCongruentIsRequired on #
#                      the DUT.                                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_TCR_RCR_CR_ENABLE,1)        "NONE"


#15.
################################################################################
#                                                                              #
#  COMMAND           : L1SYNC_ENABLE                                           #
#                                                                              #
#  ARRAY INDEX       : DUT_L1SYNC_ENABLE                                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function enables L1Sync option on a specific port  #
#                      at the DUT.                                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_L1SYNC_ENABLE,1)            "NONE"


#16.
################################################################################
#                                                                              #
#  COMMAND           : L1SYNC_OPT_PARAMS_ENABLE                                #
#                                                                              #
#  ARRAY INDEX       : DUT_L1SYNC_OPT_PARAMS_ENABLE                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function enables extended format of L1Sync TLV on  #
#                      the specified port at the DUT.                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_L1SYNC_OPT_PARAMS_ENABLE,1) "NONE"


#17.
################################################################################
#                                                                              #
#  COMMAND           : SLAVEONLY_ENABLE                                        #
#                                                                              #
#  ARRAY INDEX       : DUT_SLAVEONLY_ENABLE                                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function enables slave-only on the specified port  #
#                      at the DUT.                                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_SLAVEONLY_ENABLE,1)         "NONE"


#18.
################################################################################
#                                                                              #
#  COMMAND           : MASTERONLY_ENABLE                                       #
#                                                                              #
#  ARRAY INDEX       : DUT_MASTERONLY_ENABLE                                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function enables master-only on the specified port #
#                      at the DUT.                                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_MASTERONLY_ENABLE,1)        "NONE"


#19.
################################################################################
#                                                                              #
#  COMMAND           : SET_ANNOUNCE_INTERVAL                                   #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_ANNOUNCE_INTERVAL                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = announce_interval announceInterval.                     #
#                                  (e.g., 1|2|4|8|16)                          #
#                                                                              #
#  DEFINITION        : This function sets announceInterval value to a specific #
#                      port on the DUT.                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_SET_ANNOUNCE_INTERVAL,1)    "NONE"


#20.
################################################################################
#                                                                              #
#  COMMAND           : SET_ANNOUNCE_TIMEOUT                                    #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_ANNOUNCE_TIMEOUT                                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = announce_timeout announceReceiptTimeout.                #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  DEFINITION        : This function sets announceReceiptTimeout value to a    #
#                      specific port on the DUT.                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_SET_ANNOUNCE_TIMEOUT,1)     "NONE"


#21.
################################################################################
#                                                                              #
#  COMMAND           : SET_SYNC_INTERVAL                                       #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_SYNC_INTERVAL                                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = sync_interval logSyncInterval                           #
#                                  (e.g., 3)                                   #
#                                                                              #
#  DEFINITION        : This function sets logSyncInterval value to a specific  #
#                      port on the DUT.                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_SET_SYNC_INTERVAL,1)        "NONE"


#22.
################################################################################
#                                                                              #
#  COMMAND           : SET_DELAYREQ_INTERVAL                                   #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_DELAYREQ_INTERVAL                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num     DUT's port number.                         #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = delayreq_interval MinDelayReqInterval.                  #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  DEFINITION        : This function sets the MinDelayReqInterval to the       #
#                      specific PTP port on the DUT.                           #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_SET_DELAYREQ_INTERVAL,1)    "NONE"


#23.
################################################################################
#                                                                              #
#  COMMAND           : SET_PDELAYREQ_INTERVAL                                  #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_PDELAYREQ_INTERVAL                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = pdelayreq_interval  minPdelayReqInterval.               #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  DEFINITION        : This function sets the minPdelayReqInterval to the      #
#                      specific PTP port on the DUT.                           #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_SET_PDELAYREQ_INTERVAL,1)   "NONE"


#24.
################################################################################
#                                                                              #
#  COMMAND           : SET_L1SYNC_INTERVAL                                     #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_L1SYNC_INTERVAL                                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = l1sync_interval logL1SyncInterval.                      #
#                                  (e.g., 3)                                   #
#                                                                              #
#  DEFINITION        : This function sets the logL1SyncInterval to a specific  #
#                      port on the DUT.                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_SET_L1SYNC_INTERVAL,1)      "NONE"


#25.
################################################################################
#                                                                              #
#  COMMAND           : SET_L1SYNC_TIMEOUT                                      #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_L1SYNC_TIMEOUT                                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = l1sync_timeout L1SyncReceiptTimeout.                    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  DEFINITION        : This function sets L1SyncReceiptTimeout to a specific   #
#                      port on the DUT.                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_SET_L1SYNC_TIMEOUT,1)       "NONE"


#26.
################################################################################
#                                                                              #
#  COMMAND           : SET_EGRESS_LATENCY                                      #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_EGRESS_LATENCY                                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = latency     timestampCorrectionPortDS.egressLatency.    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  DEFINITION        : This function sets timestampCorrectionPortDS.           #
#                      egressLatency to a specific port on the DUT.            #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_SET_EGRESS_LATENCY,1)       "NONE"


#27.
################################################################################
#                                                                              #
#  COMMAND           : SET_INGRESS_LATENCY                                     #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_INGRESS_LATENCY                                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = latency     timestampCorrectionPortDS.ingressLatency.   #
#                                  (e.g., 3)                                   #
#                                                                              #
#  DEFINITION        : This function sets timestampCorrectionPortDS.           #
#                      ingressLatency to a specific port on the DUT.           #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_SET_INGRESS_LATENCY,1)      "NONE"


#28.
################################################################################
#                                                                              #
#  COMMAND           : SET_CONSTANT_ASYMMETRY                                  #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_CONSTANT_ASYMMETRY                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = constant_asymmetry asymmetryCorrectionPortDS.           #
#                                  constantAsymmetry.                          #
#                                  (e.g., 3)                                   #
#                                                                              #
#  DEFINITION        : This function sets asymmetryCorrectionPortDS.           #
#                      constantAsymmetry to a specific port on the DUT.        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_SET_CONSTANT_ASYMMETRY,1)   "NONE"


#29.
################################################################################
#                                                                              #
#  COMMAND           : SET_SCALED_DELAY_COEFFICIENT                            #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_SCALED_DELAY_COEFFICIENT                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = delay_coefficient asymmetryCorrectionPortDS.            #
#                                  scaledDelayCoefficient.                     #
#                                  (e.g., 3)                                   #
#                                                                              #
#  DEFINITION        : This function sets asymmetryCorrectionPortDS.           #
#                      scaledDelayCoefficient to a specific port on the DUT.   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_SET_SCALED_DELAY_COEFFICIENT,1) "NONE"


#30.
################################################################################
#                                                                              #
#  COMMAND           : SET_ASYMMETRY_CORRECTION                                #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_ASYMMETRY_CORRECTION                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function sets the asymmetryCorrectionPortDS.enable #
#                      to the specific PTP port on the DUT.                    #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_SET_ASYMMETRY_CORRECTION,1) "NONE"


#31.
################################################################################
#                                                                              #
#  COMMAND           : EXTERNAL_PORT_CONFIG_ENABLE                             #
#                                                                              #
#  ARRAY INDEX       : DUT_EXTERNAL_PORT_CONFIG_ENABLE                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function enables external port configuration option#
#                      on a specific port at the DUT.                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_EXTERNAL_PORT_CONFIG_ENABLE,1) "NONE"


#32.
################################################################################
#                                                                              #
#  COMMAND           : SET_EPC_DESIRED_STATE                                   #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_EPC_DESIRED_STATE                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = desired_state externalPortConfigurationPortDS.          #
#                                  desiredState.                               #
#                                  (e.g., "INITIALIZING"|"FAULTY"|"DISABLED"|  #
#                                         "LISTENING"|"PRE_MASTER"|"MASTER"|   #
#                                         "PASSIVE"|"UNCALIBRATED"|"SLAVE"     #
#                                                                              #
#  DEFINITION        : This function sets given externalPortConfigurationPortDS#
#                      desiredState to a specific port on the DUT.             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_SET_EPC_DESIRED_STATE,1)    "NONE"


#33.
################################################################################
#                                                                              #
#  COMMAND           : SET_IPV4_ADDRESS                                        #
#                                                                              #
#  ARRAY INDEX       : DUT_SET_IPV4_ADDRESS                                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = ip_address  IPv4 address.                               #
#                                  (e.g., 192.168.200.1)                       #
#                                                                              #
#   ATTEST_PARAM3    = net_mask    Subnet mask.                                #
#                                  (e.g., 255.255.255.0)                       #
#                                                                              #
#   ATTEST_PARAM4    = vlan_id     VLAN ID.                                    #
#                                  (e.g., 100)                                 #
#                                                                              #
#  DEFINITION        : This function sets given IP address to a specific port  #
#                      on the DUT.                                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_SET_IPV4_ADDRESS,1)         "NONE"


#34.
################################################################################
#                                                                              #
#  COMMAND           : TIMESTAMP_CORRECTED_TX_ENABLE                           #
#                                                                              #
#  ARRAY INDEX       : DUT_TIMESTAMP_CORRECTED_TX_ENABLE                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function sets L1SyncOptParamsPortDS.               #
#                      timestampsCorrectedTx to a specific port on the DUT.    #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_TIMESTAMP_CORRECTED_TX_ENABLE,1)         "NONE"


################################################################################
#                       DUT DISABLING FUNCTIONS                                #
################################################################################


#1.
################################################################################
#                                                                              #
#  COMMAND           : PORT_DISABLE                                            #
#                                                                              #
#  ARRAY INDEX       : DUT_PORT_DISABLE                                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function makes the specific port administratively  #
#                      down at the DUT.                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_PORT_DISABLE,1)             "NONE"


#2.
################################################################################
#                                                                              #
#  COMMAND           : PORT_PTP_DISABLE                                        #
#                                                                              #
#  ARRAY INDEX       : DUT_PORT_PTP_DISABLE                                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    Port on which ptp is to be disabled.        #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  DEFINITION        : This function disables PTP on a specific port on the DUT#
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_PORT_PTP_DISABLE,1)         "NONE"


#3.
################################################################################
#                                                                              #
#  COMMAND           : RESET_VLAN                                              #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_VLAN                                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = vlan_id     VLAN ID with which VLAN has to be deleted.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#   ATTEST_PARAM2    = port_num    Port which has to be removed from VLAN ID.  #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function removes port from VLAN ID and deletes VLAN#
#                      ID from the DUT.                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_RESET_VLAN,1)               "NONE"


#4.
################################################################################
#                                                                              #
#  COMMAND           : RESET_VLAN_PRIORITY                                     #
#                                                                              #
#  ARRAY INDEX       : DUT_ReSET_VLAN_PRIORITY                                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = vlan_id     VLAN ID                                     #
#                                  (e.g., 100)                                 #
#                                                                              #
#   ATTEST_PARAM2    = vlan_priority VLAN priority                             #
#                                  (e.g., 1)                                   #
#                                                                              #
#   ATTEST_PARAM3    = port_num    Port which has to be associated to VLAN ID. #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function resets priority to the VLAN ID on the DUT.#
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_RESET_VLAN_PRIORITY,1)      "NONE"


#5.
################################################################################
#                                                                              #
#  COMMAND           : GLOBAL_PTP_DISABLE                                      #
#                                                                              #
#  ARRAY INDEX       : DUT_GLOBAL_PTP_DISABLE                                  #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  DEFINITION        : This function disables PTP on the DUT.                  #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_GLOBAL_PTP_DISABLE,1)       "NONE"


#6.
################################################################################
#                                                                              #
#  COMMAND           : RESET_COMMUNICATION_MODE                                #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_COMMUNICATION_MODE                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = clock_mode  Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#   ATTEST_PARAM3    = ptp_version PTP version.                                #
#                                  (Default: 2)                                #
#                                                                              #
#   ATTEST_PARAM4    = communication_mode Communication mode                   #
#                                  (e.g., "Unicast"|"Multicast")               #
#                                                                              #
#  DEFINITION        : This function resets the communication mode on a        #
#                      specific port of the DUT.                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_RESET_COMMUNICATION_MODE,1) "NONE"


#7.
################################################################################
#                                                                              #
#  COMMAND           : RESET_DOMAIN                                            #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_DOMAIN                                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = clock_mode  Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#   ATTEST_PARAM2    = domain      Domain number.                              #
#                                  (e.g., 0..255)                              #
#                                                                              #
#  DEFINITION        : This function resets domain number on the DUT.          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_RESET_DOMAIN,1)             "NONE"


#8.
################################################################################
#                                                                              #
#  COMMAND           : RESET_NETWORK_PROTOCOL                                  #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_NETWORK_PROTOCOL                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = clock_mode  clock_mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#   ATTEST_PARAM3    = network_protocol Network protocol.                      #
#                                  (e.g., "IEEE 802.3"|"UDP/IPv4")             #
#                                                                              #
#  DEFINITION        : This function resets the network protocol on a specific #
#                      port of the DUT.                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_RESET_NETWORK_PROTOCOL,1)   "NONE"


#9.
################################################################################
#                                                                              #
#  COMMAND           : RESET_CLOCK_MODE                                        #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_CLOCK_MODE                                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = clock_mode  Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  DEFINITION        : This function resets clock mode on a specific port of   #
#                      DUT.                                                    #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_RESET_CLOCK_MODE,1)         "NONE"


#10.
################################################################################
#                                                                              #
#  COMMAND           : RESET_CLOCK_STEP                                        #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_CLOCK_STEP                                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = clock_mode  Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#   ATTEST_PARAM3    = clock_step  Clock step.                                 #
#                                  (e.g., "One-step"|"Two-step")               #
#                                                                              #
#  DEFINITION        : This function resets the clock-step on a specific port  #
#                      on the DUT.                                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_RESET_CLOCK_STEP,1)         "NONE"


#11.
################################################################################
#                                                                              #
#  COMMAND           : RESET_DELAY_MECHANISM                                   #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_DELAY_MECHANISM                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = clock_mode  Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#   ATTEST_PARAM3    = delay_mechanism Delay mechanism.                        #
#                                  (e.g., e2e|p2p|disable)                     #
#                                                                              #
#  DEFINITION        : This function resets the delay mechanism to a specific  #
#                      port on the DUT.                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_RESET_DELAY_MECHANISM,1)    "NONE"


#12.
################################################################################
#                                                                              #
#  COMMAND           : RESET_PRIORITY1                                         #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_PRIORITY1                                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = clock_mode  Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#   ATTEST_PARAM2    = priority1   Prority1                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  DEFINITION        : This function resets the priority1 on the DUT.          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_RESET_PRIORITY1,1)          "NONE"


#13.
################################################################################
#                                                                              #
#  COMMAND           : RESET_PRIORITY2                                         #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_PRIORITY2                                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = clock_mode  Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#   ATTEST_PARAM2    = priority2   Prority2                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  DEFINITION        : This function resets the priority2 on the DUT.          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_RESET_PRIORITY2,1)          "NONE"


#14.
################################################################################
#                                                                              #
#  COMMAND           : TCR_RCR_CR_DISABLE                                      #
#                                                                              #
#  ARRAY INDEX       : DUT_TCR_RCR_CR_DISABLE                                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function disables peerTxCoherentIsRequired,        #
#                      peerRxCoherentIsRequired and peerCongruentIsRequired on #
#                      the DUT.                                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_TCR_RCR_CR_DISABLE,1)       "NONE"


#15.
################################################################################
#                                                                              #
#  COMMAND           : L1SYNC_DISABLE                                          #
#                                                                              #
#  ARRAY INDEX       : DUT_L1SYNC_DISABLE                                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function disables L1Sync option on a specific port #
#                      at the DUT.                                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_L1SYNC_DISABLE,1)           "NONE"


#16.
################################################################################
#                                                                              #
#  COMMAND           : L1SYNC_OPT_PARAMS_DISABLE                               #
#                                                                              #
#  ARRAY INDEX       : DUT_L1SYNC_OPT_PARAMS_DISABLE                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function disables extended format of L1Sync TLV on #
#                      the specified port at the DUT.                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_L1SYNC_OPT_PARAMS_DISABLE,1) "NONE"


#17.
################################################################################
#                                                                              #
#  COMMAND           : SLAVEONLY_DISABLE                                       #
#                                                                              #
#  ARRAY INDEX       : DUT_SLAVEONLY_DISABLE                                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function disables slave-only on the specified port #
#                      at the DUT.                                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_SLAVEONLY_DISABLE,1)        "NONE"


#18.
################################################################################
#                                                                              #
#  COMMAND           : MASTERONLY_DISABLE                                      #
#                                                                              #
#  ARRAY INDEX       : DUT_MASTERONLY_DISABLE                                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function disables master-only on a specific port   #
#                      at the DUT.                                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_MASTERONLY_DISABLE,1)       "NONE"


#19.
################################################################################
#                                                                              #
#  COMMAND           : RESET_ANNOUNCE_INTERVAL                                 #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_ANNOUNCE_INTERVAL                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = announce_interval announceInterval.                     #
#                                  (e.g., 1|2|4|8|16)                          #
#                                                                              #
#  DEFINITION        : This function resets announceInterval value to a        #
#                      specific port on the DUT.                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_RESET_ANNOUNCE_INTERVAL,1)  "NONE"


#20.
################################################################################
#                                                                              #
#  COMMAND           : RESET_ANNOUNCE_TIMEOUT                                  #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_ANNOUNCE_TIMEOUT                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = announce_timeout announceReceiptTimeout.                #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  DEFINITION        : This function resets announceReceiptTimeout value to a  #
#                      specific port on the DUT.                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_RESET_ANNOUNCE_TIMEOUT,1)   "NONE"


#21.
################################################################################
#                                                                              #
#  COMMAND           : RESET_SYNC_INTERVAL                                     #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_SYNC_INTERVAL                                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = sync_interval logSyncInterval                           #
#                                  (e.g., 3)                                   #
#                                                                              #
#  DEFINITION        : This function resets logSyncInterval value to a specific#
#                      port on the DUT.                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_RESET_SYNC_INTERVAL,1)      "NONE"


#22.
################################################################################
#                                                                              #
#  COMMAND           : RESET_DELAYREQ_INTERVAL                                 #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_DELAYREQ_INTERVAL                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = delayreq_interval MinDelayReqInterval.                  #
#                                  (e.g., 1|2|4|8|16)                          #
#                                                                              #
#  DEFINITION        : This function resets the MinDelayReqInterval to the     #
#                      specific PTP port on the DUT.                           #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_RESET_DELAYREQ_INTERVAL,1)  "NONE"


#23.
################################################################################
#                                                                              #
#  COMMAND           : RESET_PDELAYREQ_INTERVAL                                #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_PDELAYREQ_INTERVAL                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = pdelayreq_interval minPdelayReqInterval.                #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  DEFINITION        : This function resets the minPdelayReqInterval to the    #
#                      specific PTP port on the DUT.                           #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_RESET_PDELAYREQ_INTERVAL,1) "NONE"


#24.
################################################################################
#                                                                              #
#  COMMAND           : RESET_L1SYNC_INTERVAL                                   #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_L1SYNC_INTERVAL                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = l1sync_interval logL1SyncInterval.                      #
#                                  (e.g.,3 )                                   #
#                                                                              #
#  DEFINITION        : This function resets the logL1SyncInterval to a specific#
#                      port on the DUT.                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_RESET_L1SYNC_INTERVAL,1)    "NONE"


#25.
################################################################################
#                                                                              #
#  COMMAND           : RESET_L1SYNC_TIMEOUT                                    #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_L1SYNC_TIMEOUT                                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = l1sync_timeout L1SyncReceiptTimeout.                    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  DEFINITION        : This function resets L1SyncReceiptTimeout to a specific #
#                      port on the DUT.                                        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_RESET_L1SYNC_TIMEOUT,1)     "NONE"


#26.
################################################################################
#                                                                              #
#  COMMAND           : RESET_EGRESS_LATENCY                                    #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_EGRESS_LATENCY                                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = latency     timestampCorrectionPortDS.egressLatency.    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  DEFINITION        : This function resets timestampCorrectionPortDS.         #
#                      egressLatency to a specific port on the DUT.            #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_RESET_EGRESS_LATENCY,1)     "NONE"


#27.
################################################################################
#                                                                              #
#  COMMAND           : RESET_INGRESS_LATENCY                                   #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_INGRESS_LATENCY                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = latency     timestampCorrectionPortDS.ingressLatency.   #
#                                  (e.g., 3)                                   #
#                                                                              #
#  DEFINITION        : This function resets timestampCorrectionPortDS.         #
#                      ingressLatency to a specific port on the DUT.           #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_RESET_INGRESS_LATENCY,1)    "NONE"


#28.
################################################################################
#                                                                              #
#  COMMAND           : RESET_CONSTANT_ASYMMETRY                                #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_CONSTANT_ASYMMETRY                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = constant_asymmetry asymmetryCorrectionPortDS.           #
#                                  constantAsymmetry.                          #
#                                  (e.g., 3)                                   #
#                                                                              #
#  DEFINITION        : This function resets asymmetryCorrectionPortDS.         #
#                      constantAsymmetry to a specific port on the DUT.        #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_RESET_CONSTANT_ASYMMETRY,1) "NONE"


#29.
################################################################################
#                                                                              #
#  COMMAND           : RESET_SCALED_DELAY_COEFFICIENT                          #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_SCALED_DELAY_COEFFICIENT                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = delay_coefficient asymmetryCorrectionPortDS.            #
#                                  scaledDelayCoefficient.                     #
#                                  (e.g., 3)                                   #
#                                                                              #
#  DEFINITION        : This function resets asymmetryCorrectionPortDS.         #
#                      scaledDelayCoefficient to a specific port on the DUT.   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_RESET_SCALED_DELAY_COEFFICIENT,1) "NONE"


#30.
################################################################################
#                                                                              #
#  COMMAND           : RESET_ASYMMETRY_CORRECTION                              #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_ASYMMETRY_CORRECTION                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION       : This function resets the asymmetryCorrectionPortDS.enable#
#                      to the specific PTP port on the DUT.                    #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_RESET_ASYMMETRY_CORRECTION,1) "NONE"


#31.
################################################################################
#                                                                              #
#  COMMAND           : EXTERNAL_PORT_CONFIG_DISABLE                            #
#                                                                              #
#  ARRAY INDEX       : DUT_EXTERNAL_PORT_CONFIG_DISABle                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function disables external port configuration      #
#                      option on a specific port at the DUT.                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_EXTERNAL_PORT_CONFIG_DISABLE,1) "NONE"


#32.
################################################################################
#                                                                              #
#  COMMAND           : RESET_EPC_DESIRED_STATE                                 #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_EPC_DESIRED_STATE                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = desired_state externalPortConfigurationPortDS.          #
#                                  desiredState.                               #
#                                  (e.g., "INITIALIZING"|"FAULTY"|"DISABLED"|  #
#                                         "LISTENING"|"PRE_MASTER"|"MASTER"|   #
#                                         "PASSIVE"|"UNCALIBRATED"|"SLAVE"     #
#                                                                              #
#  DEFINITION        : This function resets given                              #
#                      externalPortConfigurationPortDS desiredState to a       #
#                      specific port on the DUT.                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_RESET_EPC_DESIRED_STATE,1)  "NONE"


#33.
################################################################################
#                                                                              #
#  COMMAND           : RESET_IPV4_ADDRESS                                      #
#                                                                              #
#  ARRAY INDEX       : DUT_RESET_IPV4_ADDRESS                                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#   ATTEST_PARAM2    = ip_address  IPv4 address.                               #
#                                  (e.g., 192.168.200.1)                       #
#                                                                              #
#   ATTEST_PARAM3    = net_mask    Subnet mask.                                #
#                                  (e.g., 255.255.255.0)                       #
#                                                                              #
#   ATTEST_PARAM4    = vlan_id     VLAN ID.                                    #
#                                  (e.g., 100)                                 #
#                                                                              #
#  DEFINITION        : This function resets given IP address to a specific port#
#                      on the DUT.                                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_RESET_IPV4_ADDRESS,1)       "NONE"


#34.
################################################################################
#                                                                              #
#  COMMAND           : TIMESTAMP_CORRECTED_TX_DISABLE                          #
#                                                                              #
#  ARRAY INDEX       : DUT_TIMESTAMP_CORRECTED_TX_DISABLE                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function resets L1SyncOptParamsPortDS.             #
#                      timestampsCorrectedTx to a specific port on the DUT.    #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_set_record(DUT_TIMESTAMP_CORRECTED_TX_DISABLE,1)         "NONE"

}


################################################################################
#                           DUT CHECK FUNCTIONS                                #
################################################################################

proc tee_get_dut_ptp_ha_snmp_from_file {} {

    global snmp_get_record
    global ptp_ha_env

#1.
################################################################################
#                                                                              #
#  COMMAND           : CHECK_L1SYNC_STATE                                      #
#                                                                              #
#  ARRAY INDEX       : DUT_CHECK_L1SYNC_STATE                                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function verifies the given L1 Sync State in a     #
#                      specific port.                                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_get_record(DUT_CHECK_L1SYNC_STATE,1)       "NONE"


#2.
################################################################################
#                                                                              #
#  COMMAND           : CHECK_PTP_PORT_STATE                                    #
#                                                                              #
#  ARRAY INDEX       : DUT_CHECK_PTP_PORT_STATE                                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function verifies the given portState in a specific#
#                      port.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_get_record(DUT_CHECK_PTP_PORT_STATE,1)     "NONE"


#3.
################################################################################
#                                                                              #
#  COMMAND           : CHECK_ASYMMETRY_CORRECTION                              #
#                                                                              #
#  ARRAY INDEX       : DUT_CHECK_ASYMMETRY_CORRECTION                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function verifies the given state of               #
#                      asymmetryCorrectionPortDS.enable for a specific port.   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_get_record(DUT_CHECK_ASYMMETRY_CORRECTION,1) "NONE"


#4.
################################################################################
#                                                                              #
#  COMMAND           : CHECK_EGRESS_LATENCY                                    #
#                                                                              #
#  ARRAY INDEX       : DUT_CHECK_EGRESS_LATENCY                                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      timestampCorrectionPortDS.egressLatency for a specific  #
#                      port.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_get_record(DUT_CHECK_EGRESS_LATENCY,1)     "NONE"


#5.
################################################################################
#                                                                              #
#  COMMAND           : CHECK_INGRESS_LATENCY                                   #
#                                                                              #
#  ARRAY INDEX       : DUT_CHECK_INGRESS_LATENCY                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      timestampCorrectionPortDS.ingressLatency for a specific #
#                      port.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_get_record(DUT_CHECK_INGRESS_LATENCY,1)    "NONE"


#6.
################################################################################
#                                                                              #
#  COMMAND           : CHECK_CONSTANT_ASYMMETRY                                #
#                                                                              #
#  ARRAY INDEX       : DUT_CHECK_CONSTANT_ASYMMETRY                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      asymmetryCorrectionPortDS.constantAsymmetry for a       #
#                      specific port.                                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_get_record(DUT_CHECK_CONSTANT_ASYMMETRY,1) "NONE"


#7.
################################################################################
#                                                                              #
#  COMMAND           : CHECK_SCALED_DELAY_COEFFICIENT                          #
#                                                                              #
#  ARRAY INDEX       : DUT_CHECK_SCALED_DELAY_COEFFICIENT                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      asymmetryCorrectionPortDS.scaledDelayCoefficient for a  #
#                      specific port.                                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_get_record(DUT_CHECK_SCALED_DELAY_COEFFICIENT,1) "NONE"


#8.
################################################################################
#                                                                              #
#  COMMAND           : CHECK_EXTERNAL_PORT_CONFIG                              #
#                                                                              #
#  ARRAY INDEX       : DUT_CHECK_EXTERNAL_PORT_CONFIG                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function verifies the given state of external      #
#                      port configuration for a specific port.                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_get_record(DUT_CHECK_EXTERNAL_PORT_CONFIG,1) "NONE"


#9.
################################################################################
#                                                                              #
#  COMMAND           : CHECK_SLAVE_ONLY                                        #
#                                                                              #
#  ARRAY INDEX       : DUT_CHECK_SLAVE_ONLY                                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function verifies the given state of defaultDS.    #
#                      slaveOnly configuration for a specific port.            #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_get_record(DUT_CHECK_SLAVE_ONLY,1) "NONE"


#10.
################################################################################
#                                                                              #
#  COMMAND           : CHECK_MASTER_ONLY                                       #
#                                                                              #
#  ARRAY INDEX       : DUT_CHECK_MASTER_ONLY                                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function verifies the given state of defaultDS.    #
#                      masterOnly configuration for a specific port.           #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set snmp_get_record(DUT_CHECK_MASTER_ONLY,1) "NONE"


#11.
################################################################################
#                                                                              #
#  COMMAND           : CHECK_OFFSET_FROM_MASTER                                #
#                                                                              #
#  ARRAY INDEX       : DUT_CHECK_OFFSET_FROM_MASTER                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = offset_from_master currentDS.offsetFromMaster.          #
#                                         (e.g., 100ns)                        #
#                                                                              #
#  DEFINITION        : This function checks absolute value of currentDS.       #
#                      offsetFromMaster in DUT.                                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
################################################################################

    set cli_get_record(DUT_CHECK_OFFSET_FROM_MASTER,ATTEST_GET,1)   "NONE"


################################################################################
#                           DUT GET FUNCTIONS                                  #
################################################################################


#1.
################################################################################
#                                                                              #
#  COMMAND           : GET_TIMESTAMP                                           #
#                                                                              #
#  ARRAY INDEX       : DUT_GET_TIMESTAMP                                       #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  DEFINITION        : This function fetch timestamp from DUT.                 #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  timestamp         : (Mandatory) Timestamp.                                  #
#                                  (e.g., xxxxx)                               #
#                                                                              #
################################################################################

    set snmp_get_record(DUT_GET_TIMESTAMP,1)            "NONE"


#2.
################################################################################
#                                                                              #
#  COMMAND           : GET_MEAN_DELAY                                          #
#                                                                              #
#  ARRAY INDEX       : DUT_GET_MEAN_DELAY                                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function extracts currentDS.meanDelay of a specific#
#                      port from the DUT.                                      #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  mean_delay        : (Mandatory) currentDS.meanDelay.                        #
#                                  (e.g., 3)                                   #
#                                                                              #
################################################################################

    set snmp_get_record(DUT_GET_MEAN_DELAY,1)           "NONE"


#3.
################################################################################
#                                                                              #
#  COMMAND           : GET_MEAN_LINK_DELAY                                     #
#                                                                              #
#  ARRAY INDEX       : DUT_GET_MEAN_LINK_DELAY                                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function extracts portDS.meanLinkDelay of a        #
#                      specific port from the DUT.                             #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  mean_link_delay   : (Mandatory) portDS.meanLinkDelay.                       #
#                                  (e.g., 3)                                   #
#                                                                              #
################################################################################

    set snmp_get_record(DUT_GET_MEAN_LINK_DELAY,1)      "NONE"


#4.
################################################################################
#                                                                              #
#  COMMAND           : GET_DELAY_ASYMMETRY                                     #
#                                                                              #
#  ARRAY INDEX       : DUT_GET_DELAY_ASYMMETRY                                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   ATTEST_PARAM1    = port_num    DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  DEFINITION        : This function extracts portDS.delayAsymmetry of a       #
#                      specific port from the DUT.                             #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  delay_asymmetry   : (Mandatory) portDS.delayAsymmetry.                      #
#                                  (e.g., 3)                                   #
#                                                                              #
################################################################################

    set snmp_get_record(DUT_GET_DELAY_ASYMMETRY,1)      "NONE"


}

################################################################################
#                            DUT SET CALLBACK                                  #
################################################################################


#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_port_enable                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) Port to be enabled.                         #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If port is enabled)                       #
#                                                                              #
#                      0 on Failure (If port could not be enabled)             #
#                                                                              #
#  DEFINITION        : This function enables a specific port on the DUT.       #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_port_enable {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#        if {![Send_snmp_set_command \
#                        -session_handler        $session_handler \
#                        -cmd_type               "DUT_PORT_ENABLE"\
#                        -parameter1              $port_num]} {
#
#            return 0
#        }

    return 2
}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_port_ptp_enable                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) Port on which ptp is to be enabled.         #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is enabled on the specified port   #
#                                    at the DUT).                              #
#                                                                              #
#                      0 on Failure (If PTP could not be enabled on the        #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function enables PTP on a specific port on the DUT.#
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_port_ptp_enable {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_PORT_PTP_ENABLE"\
#                       -parameter1             $port_num]} {
#           return 0
#       }

    return 2

}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_set_vlan                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID with which VLAN has to be created.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#  port_num          : (Mandatory) Port which has to be associated to VLAN ID. #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If VLAN ID is created and port is         #
#                                    associated to it on the DUT).             #
#                                                                              #
#                      0 on Failure (If VLAN ID could not be created or port   #
#                                    is not associated to it on the DUT).      #
#                                                                              #
#  DEFINITION        : This function creates VLAN ID and associates given port #
#                      to it on the DUT.                                       #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_set_vlan {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set vlan_id         $param(-vlan_id)
    set port_num        $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler         $session_handler \
#                       -cmd_type                "DUT_SET_VLAN"\
#                       -parameter1              $vlan_id\
#                       -parameter2              $port_num]} {
#           return 0
#       }

    return 2
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_set_vlan_priority                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID with which VLAN has to be created.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#  vlan_priority     : (Mandatory) VLAN priority to be associated              #
#                                  (e.g., 1)                                   #
#                                                                              #
#  port_num          : (Mandatory) Port with VLAN ID configured.               #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If VLAN priority is associated0           #
#                                                                              #
#                      0 on Failure (If VLAN priority could not be associated) #
#                                                                              #
#  DEFINITION        : This function associates given priority to VLAN         #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_set_vlan_priority {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set vlan_id         $param(-vlan_id)
    set vlan_priority   $param(-vlan_priority)
    set port_num        $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler         $session_handler \
#                       -cmd_type                "DUT_SET_VLAN_PRIORITY"\
#                       -parameter1              $vlan_priority\
#                       -parameter2              $port_num]} {
#
#           return 0
#       }

    return 2
}


#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_global_ptp_enable                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is enabled on the DUT).            #
#                                                                              #
#                      0 on Failure (If PTP could not be enabled on the DUT).  #
#                                                                              #
#  DEFINITION        : This function enables PTP on the DUT.                   #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_global_ptp_enable {args} {

    array set param $args

    set session_handler $param(-session_handler)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_GLOBAL_PTP_ENABLE"]} {
#           return 0
#       }

    return 2
}


#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_set_communication_mode            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  ptp_version       : (optional)  PTP version.                                #
#                                  (Default: 2)                                #
#                                                                              #
#  communication_mode : (Mandatory) Communication mode                         #
#                                  (e.g., "Unicast"|"Multicast")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If communication mode is set on the       #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If communication mode could not be        #
#                                    set on the specified port at the DUT).    #
#                                                                              #
#  DEFINITION        : This function sets the communication mode on a specific #
#                      port of the DUT.                                        #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_set_communication_mode {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set clock_mode          $param(-clock_mode)
    set communication_mode  $param(-communication_mode)
    set ptp_version         $param(-ptp_version)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_SET_COMMUNICATION_MODE"\
#                       -parameter1             $port_num\
#                       -parameter2             $clock_mode\
#                       -parameter3             $ptp_version\
#                       -parameter4             $communication_mode]} {
#           return 0
#       }

    return 2
}


#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_set_domain                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  domain            : (Mandatory) Domain number.                              #
#                                  (e.g., 0..255)                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If domain number is set on the DUT).      #
#                                                                              #
#                      0 on Failure (If domain number could not able to set on #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function sets domain number on the DUT.            #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_set_domain {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set clock_mode      $param(-clock_mode)
    set domain          $param(-domain)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_SET_DOMAIN"\
#                       -parameter1             $clock_mode\
#                       -parameter2             $domain]} {
#           return 0
#       }

    return 2
}


#8.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_set_network_protocol              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) clock_mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  network_protocol  : (Mandatory) Network protocol.                           #
#                                  (e.g., "IEEE 802.3"|"UDP/IPv4")             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If network protocol is set on specified   #
#                                    port at the DUT).                         #
#                                                                              #
#                      0 on Failure (If network protocol could not be set      #
#                                    on specified port at the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets the network protocol on a specific   #
#                      port of the DUT.                                        #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_set_network_protocol {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set clock_mode          $param(-clock_mode)
    set network_protocol    $param(-network_protocol)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_SET_NETWORK_PROTOCOL"\
#                       -parameter1             $clock_mode\
#                       -parameter2             $network_protocol]} {
#           return 0
#       }

    return 2
}


#9.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_set_clock_mode                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP clock mode is set on specified port#
#                                    of the DUT).                              #
#                                                                              #
#                      0 on Failure (If PTP clock mode could not be set on     #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function sets clock mode on a specific port of DUT.#
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_set_clock_mode {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set clock_mode          $param(-clock_mode)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_SET_CLOCK_MODE"\
#                       -parameter1             $port_num\
#                       -parameter2             $clock_mode]} {
#           return 0
#       }

    return 2
}


#10.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_set_clock_step                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  clock_step        : (Mandatory) Clock step.                                 #
#                                  (e.g., "One-step"|"Two-step")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If clock-step is set on the specified     #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If clock-step could not be able to set    #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets the clock-step on a specific port on #
#                      the DUT.                                                #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_set_clock_step {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set clock_mode          $param(-clock_mode)
    set clock_step          $param(-clock_step)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_SET_CLOCK_STEP"\
#                       -parameter1             $port_num\
#                       -parameter1             $clock_mode\
#                       -parameter3             $clock_step]} {
#           return 0
#       }

    return 2
}


#11.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_set_delay_mechanism               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  delay_mechanism   : (Mandatory) Delay mechanism.                            #
#                                  (e.g., e2e|p2p|disable)                     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If delay mechanism is set on specified    #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If delay mechanism could not be set on    #
#                                    specified PTP port of the DUT).           #
#                                                                              #
#  DEFINITION        : This function sets the delay mechanism to a specific    #
#                      port on the DUT.                                        #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_set_delay_mechanism {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set clock_mode          $param(-clock_mode)
    set delay_mechanism     $param(-delay_mechanism)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_SET_DELAY_MECHANISM"\
#                       -parameter1             $port_num\
#                       -parameter2             $clock_mode\
#                       -parameter3             $delay_mechanism]} { 
#           return 0
#       }

    return 2
}


#12.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_set_priority1                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority1         : (Mandatory) Prority1                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority1 is set on the DUT).          #
#                                                                              #
#                      0 on Failure (If priority1 could not be able to set on  #
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function sets the priority1 on the DUT.            #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_set_priority1 {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set clock_mode          $param(-clock_mode)
    set priority1           $param(-priority1)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_SET_PRIORITY1"\
#                       -parameter1             $clock_mode\
#                       -parameter2             $priority1]} { 
#           return 0
#       }

    return 2
}


#13.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_set_priority2                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority2         : (Mandatory) Prority2                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority2 is set on the DUT).          #
#                                                                              #
#                      0 on Failure (If priority2 could not be able to set on  #
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function sets the priority2 on the DUT.            #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_set_priority2 {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set clock_mode          $param(-clock_mode)
    set priority2           $param(-priority2)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_SET_PRIORITY1"\
#                       -parameter1             $clock_mode\
#                       -parameter2             $priority2]} { 
#           return 0
#       }

    return 2
}


#14.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::callback_snmp_tcr_rcr_cr_enable                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If peerTxCoherentIsRequired,              #
#                                    peerRxCoherentIsRequired and              #
#                                    peerCongruentIsRequired is enabled on the #
#                                    DUT).                                     #
#                                                                              #
#                      0 on Failure (If peerTxCoherentIsRequired,              #
#                                    peerRxCoherentIsRequired and              #
#                                    peerCongruentIsRequired could not be      #
#                                    enabled on the DUT).                      #
#                                                                              #
#                                                                              #
#  DEFINITION        : This function enables peerTxCoherentIsRequired,         #
#                      peerRxCoherentIsRequired and peerCongruentIsRequired on #
#                      the DUT.                                                #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_tcr_rcr_cr_enable {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_TCR_RCR_CR_ENABLE"\
#                       -parameter1             $port_num]} {
#           return 0
#       }

    return 2
}


#15.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_l1sync_enable                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If L1Sync option is enabled on the        #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If L1Sync option could not be enabled on  #
#                                    the specified port of the DUT).           #
#                                                                              #
#  DEFINITION        : This function enables L1Sync option on a specific port  #
#                      at the DUT.                                             #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_l1sync_enable {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_L1SYNC_ENABLE"\
#                       -parameter1             $port_num]} {
#           return 0
#       }

    return 2
}


#16.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_l1sync_opt_params_enable          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If extended format of L1Sync TLV is       #
#                                    enabled on the specified port at the DUT).#
#                                                                              #
#                      0 on Failure (If extended format of L1Sync TLV could not#
#                                    be enabled on the specified port at the   #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function enables extended format of L1Sync TLV on  #
#                      the specified port at the DUT.                          #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_l1sync_opt_params_enable {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_L1SYNC_OPT_PARAMS_ENABLE"\
#                       -parameter1             $port_num]} {
#           return 0
#       }

    return 2
}


#17.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_slaveonly_enable                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If slave-only is enabled on the           #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If slave-only could not be enabled on the #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function enables slave-only on the specified port  #
#                      at the DUT.                                             #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_slaveonly_enable {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_SLAVEONLY_ENABLE"
#                       -parameter1             $port_num]} {
#           return 0
#       }

    return 2
}


#18.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_masteronly_enable                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If master-only is enabled on the          #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If master-only could not be enabled on the#
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function enables master-only on the specified port #
#                      at the DUT.                                             #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_masteronly_enable {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#        if {![Send_snmp_set_command \
#                        -session_handler        $session_handler \
#                        -cmd_type               "DUT_MASTERONLY_ENABLE"\
#                        -parameter1             $port_num]} {
#            return 0
#        }

    return 2
}


#19.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::callback_snmp_set_announce_interval            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  announce_interval : (Mandatory) announceInterval.                           #
#                                  (e.g., 1|2|4|8|16)                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If announceInterval value is set on       #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If announceInterval value could not be    #
#                                    set on specified port of the DUT).        #
#                                                                              #
#  DEFINITION        : This function sets announceInterval value to a specific #
#                      port on the DUT.                                        #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_set_announce_interval {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set announce_interval   $param(-announce_interval)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_SET_ANNOUNCE_INTERVAL"\
#                       -parameter1             $port_num\
#                       -parameter2             $announce_interval]} {
#           return 0
#       }

    return 2
}


#20.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::callback_snmp_set_announce_timeout             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  announce_timeout  : (Mandatory) announceReceiptTimeout.                     #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If announceReceiptTimeout value is set on #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If announceReceiptTimeout value could not #
#                                    be set on specified port of the DUT).     #
#                                                                              #
#  DEFINITION        : This function sets announceReceiptTimeout value to a    #
#                      specific port on the DUT.                               #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_set_announce_timeout {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set announce_timeout)   $param(-announce_timeout)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_SET_ANNOUNCE_TIMEOUT"\
#                       -parameter1             $port_num\
#                       -parameter2             $announce_timeout]} {
#           return 0
#       }

    return 2
}


#21.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_set_sync_interval                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  sync_interval     : (Mandatory) logSyncInterval                             #
#                                  (e.g., 3)                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If logSyncInterval value is set on        #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If logSyncInterval value could not be     #
#                                    set on specified port of the DUT).        #
#                                                                              #
#  DEFINITION        : This function sets logSyncInterval value to a specific  #
#                      port on the DUT.                                        #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_set_sync_interval {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set sync_interval       $param(-sync_interval)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_SET_SYNC_INTERVAL"\
#                       -parameter1             $port_num\
#                       -parameter2             $sync_interval]} {
#           return 0
#       }

    return 2
}


#22.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::callback_snmp_set_delayreq_interval            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  delayreq_interval : (Mandatory) MinDelayReqInterval.                        #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If MinDelayReqInterval is set on specified#
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If MinDelayReqInterval could not be set   #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets the MinDelayReqInterval to the       #
#                      specific PTP port on the DUT.                           #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_set_delayreq_interval {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set delayreq_interval   $param(-delayreq_interval)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_SET_DELAYREQ_INTERVAl"\
#                       -parameter1             $port_num\
#                       -parameter2             $delayreq_interval]} {
#           return 0
#       }

    return 2
}


#23.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::callback_snmp_set_pdelayreq_interval           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  pdelayreq_interval: (Mandatory) minPdelayReqInterval.                       #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If minPdelayReqInterval is set on         #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If minPdelayReqInterval could not be set  #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets the minPdelayReqInterval to the      #
#                      specific PTP port on the DUT.                           #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_set_pdelayreq_interval {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set delayreq_interval   $param(-pdelayreq_interval)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_SET_PDELAYREQ_INTERVAl"\
#                       -parameter1             $port_num\
#                       -parameter2             $pdelayreq_interval]} {
#           return 0
#       }

    return 2
}


#24.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_set_l1sync_interval               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  l1sync_interval   : (Mandatory) logL1SyncInterval.                          #
#                                  (e.g.,3 )                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If logL1SyncInterval is set on specified  #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If logL1SyncInterval could not be set on  #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function sets the logL1SyncInterval to a specific  #
#                      port on the DUT.                                        #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_set_l1sync_interval {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set l1sync_interval     $param(-l1sync_interval)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_SET_L1SYNC_INTERVAL"\
#                       -parameter1             $port_num\
#                       -parameter2             $l1sync_interval]} {
#           return 0
#       }

    return 2
}


#25.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_set_l1sync_timeout                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  l1sync_timeout    : (Mandatory) L1SyncReceiptTimeout.                       #
#                                  (e.g., 3)                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If L1SyncReceiptTimeout is set on         #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If L1SyncReceiptTimeout could not be set  #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets L1SyncReceiptTimeout to a specific   #
#                      port on the DUT.                                        #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_set_l1sync_timeout {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set l1sync_timeout      $param(-l1sync_timeout)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_SET_L1SYNC_TIMEOUT"\
#                       -parameter1             $port_num\
#                       -parameter2             $l1sync_timeout]} {
#           return 0
#       }

    return 2
}


#26.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_set_egress_latency                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Mandatory) timestampCorrectionPortDS.egressLatency.    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If timestampCorrectionPortDS.egressLatency#
#                                    is set on specified port of the DUT).     #
#                                                                              #
#                      0 on Failure (If timestampCorrectionPortDS.egressLatency#
#                                    could not be set on specified port of the #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function sets timestampCorrectionPortDS.           #
#                      egressLatency to a specific port on the DUT.            #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_set_egress_latency {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)
    set latency         $param(-latency)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_SET_EGRESS_LATENCY"\
#                       -parameter1             $port_num\
#                       -parameter2             $latency]} {
#
#           return 0
#       }

    return 2
}


#27.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_set_ingress_latency               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Mandatory) timestampCorrectionPortDS.ingressLatency.   #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS          : 1 on Success (If timestampCorrectionPortDS.ingressLatency#
#                                    is set on specified port of the DUT).     #
#                                                                              #
#                     0 on Failure (If timestampCorrectionPortDS.ingressLatency#
#                                    could not be set on specified port of the #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function sets timestampCorrectionPortDS.           #
#                      ingressLatency to a specific port on the DUT.           #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_set_ingress_latency {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)
    set latency         $param(-latency)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_SET_INGRESS_LATENCY"\
#                       -parameter1             $port_num\
#                       -parameter2             $latency]} {
#
#           return 0
#       }

    return 2
}


#28.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_set_constant_asymmetry            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  constant_asymmetry :(Mandatory) asymmetryCorrectionPortDS.constantAsymmetry.#
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If asymmetryCorrectionPortDS.             #
#                                    constantAsymmetry is set on specified port#
#                                    of the DUT).                              #
#                                                                              #
#                      0 on Failure (If asymmetryCorrectionPortDS.             #
#                                    constantAsymmetry could not be set on     #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function sets asymmetryCorrectionPortDS.           #
#                      constantAsymmetry to a specific port on the DUT.        #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_set_constant_asymmetry {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set constant_asymmetry  $param(-constant_asymmetry)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_SET_CONSTANT_ASYMMETRY"\
#                       -parameter1             $port_num\
#                       -parameter2             $constant_asymmetry]} {
#
#           return 0
#       }

    return 2
}


#29.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_set_scaled_delay_coefficient      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  delay_coefficient : (Mandatory) asymmetryCorrectionPortDS.                  #
#                                  scaledDelayCoefficient .                    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If asymmetryCorrectionPortDS.             #
#                                    scaledDelayCoefficient is set on specified#
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If asymmetryCorrectionPortDS.             #
#                                    scaledDelayCoefficient could not be set on#
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function sets asymmetryCorrectionPortDS.           #
#                      scaledDelayCoefficient to a specific port on the DUT.   #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_set_scaled_delay_coefficient {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set delay_coefficient   $param(-delay_coefficient)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_SET_SCALED_DELAY_COEFFICIENT"\
#                       -parameter1             $port_num\
#                       -parameter2             $delay_coefficient]} {
#
#           return 0
#       }

    return 2
}


#30.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_set_asymmetry_correction          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If asymmetryCorrectionPortDS.enable is set#
#                                    on specified port of the DUT).            #
#                                                                              #
#                      0 on Failure (If asymmetryCorrectionPortDS.enable could #
#                                    not be set on specified port of the DUT). #
#                                                                              #
#  DEFINITION        : This function sets the asymmetryCorrectionPortDS.enable #
#                      to the specific PTP port on the DUT.                    #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_set_asymmetry_correction {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_SET_ASYMMETRY_CORRECTION"\
#                       -parameter1             $port_num]} {
#
#           return 0
#       }

    return 2
}


#31.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_external_port_config_enable       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If external port configuration option is  #
#                                    enabled on the specified port at the DUT).#
#                                                                              #
#                      0 on Failure (If external port configuration option     #
#                                    could not be enabled on the specified port#
#                                    at the DUT).                              #
#                                                                              #
#  DEFINITION        : This function enables external port configuration option#
#                      on a specific port at the DUT.                          #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_external_port_config_enable {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_EXTERNAL_PORT_CONFIG_ENABLE"\
#                       -parameter1             $port_num]} {
#           return 0
#       }

    return 2
}


#32.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_set_epc_desired_state             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  desired_state     : (Mandatory) externalPortConfigurationPortDS.desiredState#
#                                  (e.g., "INITIALIZING"|"FAULTY"|"DISABLED"|  #
#                                         "LISTENING"|"PRE_MASTER"|"MASTER"|   #
#                                         "PASSIVE"|"UNCALIBRATED"|"SLAVE"     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given externalPortConfigurationPortDS. #
#                                    desiredState is set on specified port of  #
#                                    the DUT).                                 #
#                                                                              #
#                      0 on Failure (If given externalPortConfigurationPortDS. #
#                                    desiredState could not be set on specifed #
#                                    port of the DUT).                         #
#                                                                              #
#  DEFINITION        : This function sets given externalPortConfigurationPortDS#
#                      desiredState to a specific port on the DUT.             #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_set_epc_desired_state {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)
    set desired_state   $param(-desired_state)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_SET_EPC_DESIRED_STATE"\
#                       -parameter1             $port_num\
#                       -parameter2             $desired_state]} {
#
#           return 0
#       }

    return 2
}


#33.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_set_ipv4_address                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  ip_address        : (Mandatory) IPv4 address.                               #
#                                  (e.g., 192.168.200.1)                       #
#                                                                              #
#  net_mask          : (Mandatory) Subnet mask.                                #
#                                  (e.g., 255.255.255.0)                       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID.                                    #
#                                  (e.g., 100)                                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given IP address is set on specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given IP address could not be set on   #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function sets given IP address to a specific port  #
#                      on the DUT.                                             #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_set_ipv4_address {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)
    set ip_address      $param(-ip_address)
    set net_mask        $param(-net_mask)
    set vlan_id         $param(-vlan_id)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_SET_IPV4_ADDRESS"\
#                       -parameter1             $port_num\
#                       -parameter2             $ip_address\
#                       -parameter3             $net_mask\
#                       -parameter4             $vlan_id]} {
#
#           return 0
#       }

    return 2
}


#34.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_timestamp_corrected_tx_enable     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given L1SyncOptParamsPortDS.           #
#                                    timestampsCorrectedTx is set on specified #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given L1SyncOptParamsPortDS.           #
#                                    timestampsCorrectedTx could not be set on #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function sets given L1SyncOptParamsPortDS.         #
#                      timestampsCorrectedTx to a specific port on the DUT.    #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_timestamp_corrected_tx_enable {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_TIMESTAMP_CORRECTED_TX_ENABLE"\
#                       -parameter1             $port_num]} {
#
#           return 0
#       }

    return 2
}


################################################################################
#                       DUT DISABLING CALLBACK                                 #
################################################################################


#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_port_disable                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) Port to be disabled.                        #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If port is disabled)                      #
#                                                                              #
#                      0 on Failure (If port could not be disabled)            #
#                                                                              #
#  DEFINITION        : This function disables a specific port on the DUT.      #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_port_disable {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#        if {![Send_snmp_set_command \
#                        -session_handler        $session_handler \
#                        -cmd_type               "DUT_PORT_DISABLE"\
#                        -parameter1              $port_num]} {
#
#            return 0
#        }

    return 2
}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_port_ptp_disable                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) Port on which ptp is to be disabled.        #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is disabled on the specified port  #
#                                    at the DUT).                              #
#                                                                              #
#                      0 on Failure (If PTP could not be disabled on the       #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function disables PTP on a specific port on the DUT#
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_port_ptp_disable {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_PORT_PTP_DISABLE"\
#                       -parameter1             $port_num]} {
#           return 0
#       }

    return 2
}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_reset_vlan                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID with which VLAN has to be deleted.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#  port_num          : (Mandatory) Port which has to be removed from VLAN ID.  #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If port is removed from VLAN ID and VLAN  #
#                                    ID is deleted from the DUT).              #
#                                                                              #
#                      0 on Failure (If port could not be removed from VLAN ID #
#                                    and VLAN ID is not deledted from the DUT).#
#                                                                              #
#  DEFINITION        : This function removes port from VLAN ID and deletes VLAN#
#                      ID from the DUT.                                        #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_reset_vlan {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set vlan_id             $param(-vlan_id)
    set port_num            $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_VLAN"\
#                       -parameter1              $vlan_id\
#                       -parameter2              $port_num]} {
#           return 0
#       }

    return 2
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_reset_vlan_priority               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID with which VLAN has to be created.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#  vlan_priority     : (Mandatory) VLAN priority to be associated              #
#                                  (e.g., 1)                                   #
#                                                                              #
#  port_num          : (Mandatory) Port with VLAN ID configured.               #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If VLAN priority is disassociated)        #
#                                                                              #
#                      0 on Failure (If VLAN priority could not be             #
#                                    disassociated)                            #
#                                                                              #
#  DEFINITION        : This function disassociates given priority from VLAN    #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_reset_vlan_priority {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set vlan_id         $param(-vlan_id)
    set vlan_priority   $param(-vlan_priority)
    set port_num        $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler         $session_handler \
#                       -cmd_type                "DUT_RESET_VLAN_PRIORITY"\
#                       -parameter1              $vlan_priority\
#                       -parameter2              $port_num]} {
#
#           return 0
#       }

    return 2
}


#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_global_ptp_disable                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is disabled on the DUT).           #
#                                                                              #
#                      0 on Failure (If PTP could not be disabled on the DUT). #
#                                                                              #
#  DEFINITION        : This function disables PTP on the DUT.                  #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_global_ptp_disable {args} {

    array set param $args

    set session_handler     $param(-session_handler)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_GLOBAL_PTP_DISABLE"]} {
#           return 0
#       }

    return 2

}


#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_reset_communication_mode          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  ptp_version       : (optional)  PTP version.                                #
#                                  (Default: 2)                                #
#                                                                              #
#  communication_mode : (Mandatory) Communication mode                         #
#                                  (e.g., "Unicast"|"Multicast")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If communication mode is reset on the     #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If communication mode could not be        #
#                                    reset on the specified port at the DUT).  #
#                                                                              #
#  DEFINITION        : This function resets the communication mode on a        #
#                      specific port of the DUT.                               #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_reset_communication_mode {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set clock_mode          $param(-clock_mode)
    set communication_mode  $param(-communication_mode)
    set ptp_version         $param(-ptp_version)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_COMMUNICATION_MODE"\
#                       -parameter1             $port_num\
#                       -parameter2             $clock_mode\
#                       -parameter3             $ptp_version\
#                       -parameter4             $communication_mode]} {
#           return 0
#       }

    return 2
}


#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_reset_domain                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  domain            : (Mandatory) Domain number.                              #
#                                  (e.g., 0..255)                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If domain number is reset on the DUT).    #
#                                                                              #
#                      0 on Failure (If domain number could not able to reset  #
#                                    on DUT).                                  #
#                                                                              #
#  DEFINITION        : This function resets domain number on the DUT.          #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_reset_domain {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set clock_mode          $param(-clock_mode)
    set domain              $param(-domain)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_DOMAIN"\
#                       -parameter1             $clock_mode\
#                       -parameter2             $domain]} {
#           return 0
#       }

    return 2
}


#8.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_reset_network_protocol            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) clock_mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  network_protocol  : (Mandatory) Network protocol.                           #
#                                  (e.g., "IEEE 802.3"|"UDP/IPv4")             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If network protocol is reset on specified #
#                                    port at the DUT).                         #
#                                                                              #
#                      0 on Failure (If network protocol could not be reset    #
#                                    on specified port at the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets the network protocol on a specific #
#                      port of the DUT.                                        #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_reset_network_protocol {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set clock_mode          $param(-clock_mode)
    set network_protocol    $param(-network_protocol)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_NETWORK_PROTOCOL"\
#                       -parameter1             $clock_mode\
#                       -parameter2             $network_protocol]} {
#           return 0
#       }

    return 2
}


#9.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_reset_clock_mode                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP clock mode is reset on specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If PTP clock mode could not be reset on   #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function resets clock mode on a specific port of   #
#                      DUT.                                                    #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_reset_clock_mode {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set clock_mode          $param(-clock_mode)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_CLOCK_MODE"\
#                       -parameter1             $port_num\
#                       -parameter2             $clock_mode]} {
#           return 0
#       }

    return 2
}


#10.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_reset_clock_step                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  clock_step        : (Mandatory) Clock step.                                 #
#                                  (e.g., "One-step"|"Two-step")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If clock-step is reset on the specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If clock-step could not be able to reset  #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets the clock-step on a specific port  #
#                      on the DUT.                                             #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_reset_clock_step {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set clock_mode          $param(-clock_mode)
    set clock_step          $param(-clock_step)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_CLOCK_STEP"\
#                       -parameter1             $port_num\
#                       -parameter1             $clock_mode\
#                       -parameter3             $clock_step]} {
#           return 0
#       }

    return 2
}


#11.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_reset_delay_mechanism             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  delay_mechanism   : (Mandatory) Delay mechanism.                            #
#                                  (e.g., e2e|p2p|disable)                     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If delay mechanism is reset on specified  #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If delay mechanism could not be reset on  #
#                                    specified PTP port of the DUT).           #
#                                                                              #
#  DEFINITION        : This function resets the delay mechanism to a specific  #
#                      port on the DUT.                                        #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_reset_delay_mechanism {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set clock_mode          $param(-clock_mode)
    set delay_mechanism     $param(-delay_mechanism)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_DELAY_MECHANISM"\
#                       -parameter1             $port_num\
#                       -parameter2             $clock_mode\
#                       -parameter3             $delay_mechanism]} { 
#           return 0
#       }

    return 2
}


#12.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_reset_priority1                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority1         : (Mandatory) Prority1                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority1 is reset on the DUT).        #
#                                                                              #
#                      0 on Failure (If priority1 could not be able to reset on#
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function resets the priority1 on the DUT.          #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_reset_priority1 {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set clock_mode          $param(-clock_mode)
    set priority1           $param(-priority1)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_PRIORITY1"\
#                       -parameter1             $clock_mode\
#                       -parameter2             $priority1]} { 
#           return 0
#       }

    return 2
}


#13.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_reset_priority2                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority2         : (Mandatory) Prority2                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority2 is reset on the DUT).        #
#                                                                              #
#                      0 on Failure (If priority2 could not be able to reset on#
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function resets the priority2 on the DUT.          #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_reset_priority2 {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set clock_mode          $param(-clock_mode)
    set priority2           $param(-priority2)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_PRIORITY1"\
#                       -parameter1             $clock_mode\
#                       -parameter2             $priority2]} { 
#           return 0
#       }

    return 2
}


#14.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::callback_snmp_tcr_rcr_cr_disable               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If peerTxCoherentIsRequired,              #
#                                    peerRxCoherentIsRequired and              #
#                                    peerCongruentIsRequired is disabled on the#
#                                    DUT).                                     #
#                                                                              #
#                      0 on Failure (If peerTxCoherentIsRequired,              #
#                                    peerRxCoherentIsRequired and              #
#                                    peerCongruentIsRequired could not be      #
#                                    disabled on the DUT).                     #
#                                                                              #
#                                                                              #
#  DEFINITION        : This function disables peerTxCoherentIsRequired,        #
#                      peerRxCoherentIsRequired and peerCongruentIsRequired on #
#                      the DUT.                                                #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_tcr_rcr_cr_disable {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_TCR_RCR_CR_DISABLE"\
#                       -parameter1             $port_num]} {
#           return 0
#       }

    return 2
}


#15.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_l1sync_disable                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If L1Sync option is disabled on the       #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If L1Sync option could not be disabled on #
#                                    the specified port of the DUT).           #
#                                                                              #
#  DEFINITION        : This function disables L1Sync option on a specific port #
#                      at the DUT.                                             #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_l1sync_disable {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_L1SYNC_DISABLE"\
#                       -parameter1             $port_num]} {
#           return 0
#       }

    return 2
}


#16.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_l1sync_opt_params_disable         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If extended format of L1Sync TLV is       #
#                                    disabled on the specified port at the DUT)#
#                                                                              #
#                      0 on Failure (If extended format of L1Sync TLV could not#
#                                    be disabled on the specified port at the  #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function disables extended format of L1Sync TLV on #
#                      the specified port at the DUT.                          #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_l1sync_opt_params_disable {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_L1SYNC_OPT_PARAMS_DISABLE"\
#                       -parameter1             $port_num]} {
#           return 0
#       }

    return 2
}


#17.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_slaveonly_disable                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If slave-only is disabled on the          #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If slave-only could not be disabled on the#
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function disables slave-only on the specified port #
#                      at the DUT.                                             #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_slaveonly_disable {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_SLAVEONLY_DISABLE"
#                       -parameter1             $port_num]} {
#           return 0
#       }

    return 2
}


#18.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_masteronly_disable                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If master-only is disabled on the         #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If master-only could not be disabled on   #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function disables master-only on a specific port   #
#                      at the DUT.                                             #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_masteronly_disable {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_MASTERONLY_DISABLE"
#                       -parameter1             $port_num]} {
#           return 0
#       }

    return 2
}


#19.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::callback_snmp_reset_announce_interval          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  announce_interval : (Mandatory) announceInterval.                           #
#                                  (e.g., 1|2|4|8|16)                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If announceInterval value is reset on     #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If announceInterval value could not be    #
#                                    reset on specified port of the DUT).      #
#                                                                              #
#  DEFINITION        : This function resets announceInterval value to a        #
#                      specific port on the DUT.                               #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_reset_announce_interval {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set announce_interval   $param(-announce_interval)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_ANNOUNCE_INTERVAL"\
#                       -parameter1             $port_num\
#                       -parameter2             $announce_interval]} {
#           return 0
#       }

    return 2
}


#20.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::callback_snmp_reset_announce_timeout           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  announce_timeout  : (Mandatory) announceReceiptTimeout.                     #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If announceReceiptTimeout value is reset  #
#                                    on specified port of the DUT).            #
#                                                                              #
#                      0 on Failure (If announceReceiptTimeout value could not #
#                                    be reset on specified port of the DUT).   #
#                                                                              #
#  DEFINITION        : This function resets announceReceiptTimeout value to a  #
#                      specific port on the DUT.                               #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_reset_announce_timeout {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set announce_timeout)   $param(-announce_timeout)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_ANNOUNCE_TIMEOUT"\
#                       -parameter1             $port_num\
#                       -parameter2             $announce_timeout]} {
#           return 0
#       }

    return 2
}


#21.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_reset_sync_interval               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  sync_interval     : (Mandatory) logSyncInterval                             #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If logSyncInterval value is reset on      #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If logSyncInterval value could not be     #
#                                    reset on specified port of the DUT).      #
#                                                                              #
#  DEFINITION        : This function resets logSyncInterval value to a specific#
#                      port on the DUT.                                        #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_reset_sync_interval {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set sync_interval       $param(-sync_interval)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_SYNC_INTERVAL"\
#                       -parameter1             $port_num\
#                       -parameter2             $sync_interval]} {
#           return 0
#       }

    return 2
}


#22.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::callback_snmp_reset_delayreq_interval          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  delayreq_interval : (Mandatory) MinDelayReqInterval.                        #
#                                  (e.g., 1|2|4|8|16)                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If MinDelayReqInterval is reset on        #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If MinDelayReqInterval could not be reset #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets the MinDelayReqInterval to the     #
#                      specific PTP port on the DUT.                           #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_reset_delayreq_interval {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set delayreq_interval   $param(-delayreq_interval)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_DELAYREQ_INTERVAl"\
#                       -parameter1             $port_num\
#                       -parameter2             $delayreq_interval]} {
#           return 0
#       }

    return 2
}


#23.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::callback_snmp_reset_pdelayreq_interval         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  pdelayreq_interval: (Mandatory) minPdelayReqInterval.                       #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If minPdelayReqInterval is reset on       #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If minPdelayReqInterval could not be reset#
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets the minPdelayReqInterval to the    #
#                      specific PTP port on the DUT.                           #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_reset_pdelayreq_interval {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set pdelayreq_interval  $param(-pdelayreq_interval)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_PDELAYREQ_INTERVAl"\
#                       -parameter1             $port_num\
#                       -parameter2             $pdelayreq_interval]} {
#           return 0
#       }

    return 2
}


#24.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_reset_l1sync_interval             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  l1sync_interval   : (Mandatory) logL1SyncInterval.                          #
#                                  (e.g.,3 )                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If logL1SyncInterval is reset on specified#
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If logL1SyncInterval could not be reset on#
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function resets the logL1SyncInterval to a specific#
#                      port on the DUT.                                        #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_reset_l1sync_interval {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set l1sync_interval     $param(-l1sync_interval)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_L1SYNC_INTERVAL"\
#                       -parameter1             $port_num\
#                       -parameter2             $l1sync_interval]} {
#           return 0
#       }

    return 2
}


#25.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_reset_l1sync_timeout              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  l1sync_timeout    : (Mandatory) L1SyncReceiptTimeout.                       #
#                                  (e.g., 3)                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If L1SyncReceiptTimeout is reset on       #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If L1SyncReceiptTimeout could not be      #
#                                    reset on specified port of the DUT).      #
#                                                                              #
#  DEFINITION        : This function resets L1SyncReceiptTimeout to a specific #
#                      port on the DUT.                                        #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_reset_l1sync_timeout {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set l1sync_timeout      $param(-l1sync_timeout)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_L1SYNC_TIMEOUT"\
#                       -parameter1             $port_num\
#                       -parameter2             $l1sync_timeout]} {
#           return 0
#       }

    return 2
}


#26.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_reset_egress_latency              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Mandatory) timestampCorrectionPortDS.egressLatency.    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If timestampCorrectionPortDS.egressLatency#
#                                    is reset on specified port of the DUT).   #
#                                                                              #
#                      0 on Failure (If timestampCorrectionPortDS.egressLatency#
#                                    could not be reset on specified port of   #
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function resets timestampCorrectionPortDS.         #
#                      egressLatency to a specific port on the DUT.            #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_reset_egress_latency {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)
    set latency         $param(-latency)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_EGRESS_LATENCY"\
#                       -parameter1             $port_num\
#                       -parameter2             $latency]} {
#
#           return 0
#       }

    return 2
}


#27.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_reset_ingress_latency             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Mandatory) timestampCorrectionPortDS.ingressLatency.   #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS          : 1 on Success (If timestampCorrectionPortDS.ingressLatency#
#                                    is reset on specified port of the DUT).   #
#                                                                              #
#                     0 on Failure (If timestampCorrectionPortDS.ingressLatency#
#                                    could not be reset on specified port of   #
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function resets timestampCorrectionPortDS.         #
#                      ingressLatency to a specific port on the DUT.           #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_reset_ingress_latency {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)
    set latency         $param(-latency)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_INGRESS_LATENCY"\
#                       -parameter1             $port_num\
#                       -parameter2             $latency]} {
#
#           return 0
#       }

    return 2
}


#28.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_reset_constant_asymmetry          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  constant_asymmetry :(Mandatory) asymmetryCorrectionPortDS.constantAsymmetry.#
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If asymmetryCorrectionPortDS.             #
#                                    constantAsymmetry is reset on specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If asymmetryCorrectionPortDS.             #
#                                    constantAsymmetry could not be reset on   #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function resets asymmetryCorrectionPortDS.         #
#                      constantAsymmetry to a specific port on the DUT.        #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_reset_constant_asymmetry {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set constant_asymmetry  $param(-constant_asymmetry)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_CONSTANT_ASYMMETRY"\
#                       -parameter1             $port_num\
#                       -parameter2             $constant_asymmetry]} {
#
#           return 0
#       }

    return 2
}


#29.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_reset_scaled_delay_coefficient    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  delay_coefficient : (Mandatory) asymmetryCorrectionPortDS.                  #
#                                  scaledDelayCoefficient .                    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If asymmetryCorrectionPortDS.             #
#                                    scaledDelayCoefficient is reset on        #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If asymmetryCorrectionPortDS.             #
#                                    scaledDelayCoefficient could not be reset #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets asymmetryCorrectionPortDS.         #
#                      scaledDelayCoefficient to a specific port on the DUT.   #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_reset_scaled_delay_coefficient {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set delay_coefficient   $param(-delay_coefficient)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_SCALED_DELAY_COEFFICIENT"\
#                       -parameter1             $port_num\
#                       -parameter2             $delay_coefficient]} {
#
#           return 0
#       }

    return 2
}


#30.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_reset_asymmetry_correction        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If asymmetryCorrectionPortDS.enable is    #
#                                    reset on specified port of the DUT).      #
#                                                                              #
#                      0 on Failure (If asymmetryCorrectionPortDS.enable could #
#                                    not be reset on specified port of the DUT)#
#                                                                              #
#  DEFINITION       : This function resets the asymmetryCorrectionPortDS.enable#
#                      to the specific PTP port on the DUT.                    #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_reset_asymmetry_correction {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_ASYMMETRY_CORRECTION"\
#                       -parameter1             $port_num]} {
#
#           return 0
#       }

    return 2
}


#31.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_external_port_config_disable      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If external port configuration option is  #
#                                    disabled on the specified port at the DUT)#
#                                                                              #
#                      0 on Failure (If external port configuration option     #
#                                    could not be disabled on the specified    #
#                                    port at the DUT).                         #
#                                                                              #
#  DEFINITION        : This function disables external port configuration      #
#                      option on a specific port at the DUT.                   #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_external_port_config_disable {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_EXTERNAL_PORT_CONFIG_DISABLE"\
#                       -parameter1             $port_num]} {
#           return 0
#       }

    return 2
}


#32.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_reset_epc_desired_state           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  desired_state     : (Mandatory) externalPortConfigurationPortDS.desiredState#
#                                  (e.g., "INITIALIZING"|"FAULTY"|"DISABLED"|  #
#                                         "LISTENING"|"PRE_MASTER"|"MASTER"|   #
#                                         "PASSIVE"|"UNCALIBRATED"|"SLAVE"     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given externalPortConfigurationPortDS. #
#                                    desiredState is reset on specified port of#
#                                    the DUT).                                 #
#                                                                              #
#                      0 on Failure (If given externalPortConfigurationPortDS. #
#                                    desiredState could not be reset on        #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function resets given                              #
#                      externalPortConfigurationPortDS desiredState to a       #
#                      specific port on the DUT.                               #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_reset_epc_desired_state {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)
    set desired_state   $param(-desired_state)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_EPC_DESIRED_STATE"\
#                       -parameter1             $port_num\
#                       -parameter2             $desired_state]} {
#
#           return 0
#       }

    return 2
}


#33.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_reset_ipv4_address                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  ip_address        : (Mandatory) IPv4 address.                               #
#                                  (e.g., 192.168.200.1)                       #
#                                                                              #
#  net_mask          : (Mandatory) Subnet mask.                                #
#                                  (e.g., 255.255.255.0)                       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID.                                    #
#                                  (e.g., 100)                                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given IP address is reset on specified #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given IP address could not be reset on #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function resets given IP address to a specific port#
#                      on the DUT.                                             #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_reset_ipv4_address {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)
    set ip_address      $param(-ip_address)
    set net_mask        $param(-net_mask)
    set vlan_id         $param(-vlan_id)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_RESET_IPV4_ADDRESS"\
#                       -parameter1             $port_num\
#                       -parameter2             $ip_address\
#                       -parameter3             $net_mask\
#                       -parameter4             $vlan_id]} {
#
#           return 0
#       }

    return 2
}


#34.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_timestamp_corrected_tx_disable    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given L1SyncOptParamsPortDS.           #
#                                   timestampsCorrectedTx is reset on specified#
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given L1SyncOptParamsPortDS.           #
#                                   timestampsCorrectedTx could not be reset on#
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function resets given L1SyncOptParamsPortDS.       #
#                      timestampsCorrectedTx to a specific port on the DUT.    #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_timestamp_corrected_tx_disable {args} {

    array set param $args

    set session_handler $param(-session_handler)
    set port_num        $param(-port_num)

# To set different values to the variables, set them above and uncomment
# the following lines. Comment the last return statement.

#       if {![Send_snmp_set_command \
#                       -session_handler        $session_handler \
#                       -cmd_type               "DUT_TIMESTAMP_CORRECTED_TX_DISABLE"\
#                       -parameter1             $port_num]} {
#
#           return 0
#       }

    return 2
}


################################################################################
#                           DUT CHECK CALLBACK                                 #
################################################################################

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_check_l1sync_state                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  l1sync_state      : (Mandatory) L1 Sync State.                              #
#                                  (e.g., "DISABLED"|"IDLE"|"LINK_ALIVE"|      #
#                                         "CONFIG_MATCH"|"L1_SYNC_UP")         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given L1 Sync State is verified in the #
#                                    specified port).                          #
#                                                                              #
#                      0 on Failure (If given L1 Sync State is not verified in #
#                                    the specified port).                      #
#                                                                              #
#  DEFINITION        : This function verifies the given L1 Sync State in a     #
#                      specific port.                                          #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_check_l1sync_state {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set l1sync_state        $param(-l1sync_state)
    set buffer              $param(-buffer)

#############################################################################
# Parse the SNMP output using $buffer to check the L1 Sync State for the    #
# port $port_num. If $l1sync_state (expected value passed from test script) #
# matches the SNMP output, then return 1, else return 0 (failure condition) #
#############################################################################

    return 0
}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_check_ptp_port_state              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  port_state        : (Mandatory) portState.                                  #
#                                  (e.g., "INITIALIZING"|"FAULTY"|"DISABLED"|  #
#                                         "LISTENING"|"PRE_MASTER"|"MASTER"|   #
#                                         "PASSIVE"|"UNCALIBRATED"|"SLAVE"     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given portState is verified in the     #
#                                    specified port).                          #
#                                                                              #
#                      0 on Failure (If given portState is not verified in the #
#                                    specified port).                          #
#                                                                              #
#  DEFINITION        : This function verifies the given portState in a specific#
#                      port.                                                   #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_check_ptp_port_state {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set port_state          $param(-port_state)
    set buffer              $param(-buffer)

#############################################################################
# Parse the SNMP output using $buffer to check the portState for the        #
# port $port_num. If $port_state (expected value passed from test script)   #
# matches the SNMP output, then return 1, else return 0 (failure condition) #
#############################################################################

    return 0
}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_check_asymmetry_correction        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  state             : (Mandatory) asymmetryCorrectionPortDS.enable.           #
#                                  (e.g., "ENABLED"|"DISABLED"                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given state of                         #
#                                    asymmetryCorrectionPortDS.enable is       #
#                                    verified for the specified port).         #
#                                                                              #
#                      0 on Failure (If given state of                         #
#                                    asymmetryCorrectionPortDS.enable could not#
#                                    be verified for the specified port).      #
#                                                                              #
#  DEFINITION        : This function verifies the given state of               #
#                      asymmetryCorrectionPortDS.enable for a specific port.   #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_check_asymmetry_correction {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set state               $param(-state)
    set buffer              $param(-buffer)

#############################################################################
# Parse the SNMP output using $buffer to check the asymmetryCorrectionPortDS.#
# enable for the port $port_num. If $state (expected value passed from test #
# script) matches the SNMP output, then return 1, else return 0 (failure    #
# condition)                                                                #
#############################################################################

    return 0
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_check_egress_latency              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Mandatory) timestampCorrectionPortDS.egressLatency.    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of                         #
#                                    timestampCorrectionPortDS.egressLatency   #
#                                    verified for the specified port).         #
#                                                                              #
#                      0 on Failure (If given value of                         #
#                                    timestampCorrectionPortDS.egressLatency   #
#                                    could not be verified for the specified   #
#                                    port).                                    #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      timestampCorrectionPortDS.egressLatency for a specific  #
#                      port.                                                   #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_check_egress_latency {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set latency             $param(-latency)
    set buffer              $param(-buffer)

#############################################################################
# Parse the SNMP output using $buffer to check the timestampCorrectionPortDS.#
# egressLatency for the port $port_num. If $latency (expected value passed  #
# from test script) matches the SNMP output, then return 1, else return 0   #
# (failure condition)                                                       #
#############################################################################

    return 0
}


#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_check_ingress_latency             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Mandatory) timestampCorrectionPortDS.ingressLatency.   #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of                         #
#                                    timestampCorrectionPortDS.ingressLatency  #
#                                    verified for the specified port).         #
#                                                                              #
#                      0 on Failure (If given value of                         #
#                                    timestampCorrectionPortDS.ingressLatency  #
#                                    could not be verified for the specified   #
#                                    port).                                    #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      timestampCorrectionPortDS.ingressLatency for a specific #
#                      port.                                                   #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_check_ingress_latency {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set latency             $param(-latency)
    set buffer              $param(-buffer)

#############################################################################
# Parse the SNMP output using $buffer to check the timestampCorrectionPortDS.#
# ingressLatency for the port $port_num. If $latency (expected value passed #
# from test script) matches the SNMP output, then return 1, else return 0   #
# (failure condition)                                                       #
#############################################################################

    return 0
}


#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_check_constant_asymmetry          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  constant_asymmetry: (Mandatory) asymmetryCorrectionPortDS.constantAsymmetry.#
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of                         #
#                                    asymmetryCorrectionPortDS.                #
#                                    constantAsymmetry verified for the        #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If given value of                         #
#                                    asymmetryCorrectionPortDS.                #
#                                    constantAsymmetry could not be verified   #
#                                    for specified port at the DUT).           #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      asymmetryCorrectionPortDS.constantAsymmetry for a       #
#                      specific port.                                          #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_check_constant_asymmetry {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set constant_asymmetry  $param(-constant_asymmetry)
    set buffer              $param(-buffer)

#############################################################################
# Parse the SNMP output using $buffer to check the asymmetryCorrectionPortDS.#
# constantAsymmetry for the port $port_num. If $constant_asymmetry (expected#
# value passed from test script) matches the SNMP output, then return 1,    #
# else return 0 (failure condition)                                         #
#############################################################################

    return 0
}


#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_check_scaled_delay_coefficient    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  delay_coefficient : (Mandatory) asymmetryCorrectionPortDS.                  #
#                                  scaledDelayCoefficient.                     #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of                         #
#                                    asymmetryCorrectionPortDS.                #
#                                    scaledDelayCoefficient verified for the   #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If given value of                         #
#                                    asymmetryCorrectionPortDS.                #
#                                    scaledDelayCoefficient could not be       #
#                                    verified for specified port at the DUT).  #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      asymmetryCorrectionPortDS.scaledDelayCoefficient for a  #
#                      specific port.                                          #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_check_scaled_delay_coefficient {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set delay_coefficient   $param(-delay_coefficient)
    set buffer              $param(-buffer)

#############################################################################
# Parse the SNMP output using $buffer to check the asymmetryCorrectionPortDS.#
# scaledDelayCoefficient for the port $port_num. If $delay_coefficient      #
# (expected value passed from test script) matches the SNMP output, then    #
# return 1, else return 0 (failure condition)                               #
#############################################################################

    return 0
}


#8.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_check_external_port_config        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  state             : (Mandatory) State of external port configuration.       #
#                                  (e.g., "ENABLED"|"DISABLED")                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given state of external port           #
#                                    configuration is verified for the         #
#                                    specified port).                          #
#                                                                              #
#                      0 on Failure (If given state of external port           #
#                                    configuration could not be verified for   #
#                                    the specified port).                      #
#                                                                              #
#  DEFINITION        : This function verifies the given state of external      #
#                      port configuration for a specific port.                 #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_check_external_port_config {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set state               $param(-state)
    set buffer              $param(-buffer)

#############################################################################
# Parse the SNMP output using $buffer to check the state of external port   #
# configuration for the port $port_num. If $state (expected value passed    #
# from test script) matches the SNMP output, then return 1, else return 0   #
# (failure condition)                                                       #
#############################################################################

    return 0
}


#9.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_check_slave_only                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  value             : (Mandatory) value of defaultDS.slaveOnly.               #
#                                  (e.g., "TRUE"|"FALSE")                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of defaultDS.slaveOnly     #
#                                    is verified for the specified port).      #
#                                                                              #
#                      0 on Failure (If given value of defaultDS.slaveOnly     #
#                                    could not be verified for the specified   #
#                                    port).                                    #
#                                                                              #
#  DEFINITION        : This function verifies the given value of defaultDS.    #
#                      slaveOnly for a specific port.                          #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_check_slave_only {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set value               $param(-value)
    set buffer              $param(-buffer)

#############################################################################
# Parse the SNMP output using $buffer to check the value of defaultDS.      #
# slaveOnly for the port $port_num. If $value (expected value passed        #
# from test script) matches the SNMP output, then return 1, else return 0   #
# (failure condition)                                                       #
#############################################################################

    return 0
}


#10.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_check_master_only                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  value             : (Mandatory) value of defaultDS.masterOnly.              #
#                                  (e.g., "TRUE"|"FALSE")                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of defaultDS.masterOnly    #
#                                    is verified for the specified port).      #
#                                                                              #
#                      0 on Failure (If given value of defaultDS.masterOnly    #
#                                    could not be verified for the specified   #
#                                    port).                                    #
#                                                                              #
#  DEFINITION        : This function verifies the given value of defaultDS.    #
#                      masterOnly for a specific port.                         #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_check_master_only {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set value               $param(-value)
    set buffer              $param(-buffer)

#############################################################################
# Parse the SNMP output using $buffer to check the value of defaultDS.      #
# masterOnly for the port $port_num. If $value (expected value passed       #
# from test script) matches the SNMP output, then return 1, else return 0   #
# (failure condition)                                                       #
#############################################################################

    return 0
}


#11.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_check_for_least_offset_from_master#
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  offset_from_master: (Mandatory) currentDS.offsetFromMaster.                 #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  RETURNS           : 1 on Success (If the absolute value of currentDS.       #
#                                    offsetFromMaster in DUT becomes lesser    #
#                                    than the specified value to ensure that   #
#                                    the DUT synchronized it's time with       #
#                                    messages from TEE).                       #
#                                                                              #
#                      0 on Failure (If the absolute value of currentDS.       #
#                                    offsetFromMaster in DUT does not become   #
#                                    lesser than the specified value to ensure #
#                                    that the DUT synchronized it's time with  #
#                                    messages from TEE).                       #
#                                                                              #
#  DEFINITION        : This function invokes recursive procedure to check      #
#                      whether the absolute value of currentDS.offsetFromMaster#
#                      is lesser than the specified value to ensure that DUT   #
#                      synchronized it's time with messages from TEE.          #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_check_for_least_offset_from_master {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set offset_from_master  $param(-offset_from_master)
    set buffer              $param(-buffer)

#############################################################################
# Parse the SNMP output using $buffer to check the absolute value of        #
# currentDS.offsetFromMaster from DUT. If the checked value is lesser than  #
# $offset_from_master (value passed from test script) in the SNMP output,   #
# then return 1, else return 0 (failure condition)                          #
#############################################################################

    return 0
}



################################################################################
#                           DUT GET CALLBACK                                   #
################################################################################

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_get_timestamp                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  timestamp         : (Mandatory) Timestamp.                                  #
#                                  (e.g., xxxxx)                               #
#                                                                              #
#                                                                              #
#  RETURNS           : 1 on Success (If able to get timestamp from DUT).       #
#                                                                              #
#                      0 on Failure (If could not able to get timestamp from   #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function fetch timestamp from DUT.                 #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_get_timestamp {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set buffer              $param(-buffer)

    upvar $param(-timestamp) timestamp

#############################################################################
# Parse the SNMP output using $buffer and extract the timestamp from DUT and#
# convert it into epoch format.                                             #
# If timestamp is able to extract and export to calling procedure, then     #
# return 1, else return 0 (failure condition)                               #
#############################################################################

    return 0
}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_get_mean_delay                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  mean_delay        : (Mandatory) currentDS.meanDelay.                        #
#                                  (e.g., 3)                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If able to extract currentDS.meanDelay of #
#                                    specified port from the DUT).             #
#                                                                              #
#                      0 on Failure (If could not able to extract              #
#                                    currentDS.meanDelay of specified port from#
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function extracts currentDS.meanDelay of a specific#
#                      port from the DUT.                                      #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_get_mean_delay {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set buffer              $param(-buffer)

    upvar $param(-mean_delay) mean_delay

#############################################################################
# Parse the SNMP output using $buffer and extract the currentDS.meanDelay   #
# from DUT. If currentDS.meanDelay is able to extract and export to calling #
# procedure, then return 1, else return 0 (failure condition)               #
#############################################################################

    return 0
}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_get_mean_link_delay               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  mean_link_delay   : (Mandatory) portDS.meanLinkDelay.                       #
#                                  (e.g., 3)                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If able to extract portDS.meanLinkDelay of#
#                                    specified port from the DUT).             #
#                                                                              #
#                      0 on Failure (If could not able to extract              #
#                                    portDS.meanLinkDelay of specified port    #
#                                    from the DUT).                            #
#                                                                              #
#  DEFINITION        : This function extracts portDS.meanLinkDelay of a        #
#                      specific port from the DUT.                             #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_get_mean_link_delay {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set buffer              $param(-buffer)

    upvar $param(-mean_link_delay) mean_link_delay

#############################################################################
# Parse the SNMP output using $buffer and extract the portDS.meanLinkDelay  #
# from DUT. If portDS.meanLinkDelay is able to extract and export to calling#
# procedure, then return 1, else return 0 (failure condition)               #
#############################################################################

    return 0
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_get_offset_from_master            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  offset_from_master: (Mandatory) currentDS.offsetFromMaster.                 #
#                                  (e.g., 3)                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If able to extract currentDS.             #
#                                    offsetFromMaster of specified port from   #
#                                    the DUT).                                 #
#                                                                              #
#                      0 on Failure (If could not able to extract              #
#                                    currentDS.offsetFromMaster of specified   #
#                                    port from the DUT).                       #
#                                                                              #
#  DEFINITION        : This function extracts currentDS.offsetFromMaster of a  #
#                      specific port from the DUT.                             #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_get_offset_from_master {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set buffer              $param(-buffer)

    upvar $param(-offset_from_master) offset_from_master

#############################################################################
# Parse the SNMP output using $buffer and extract the currentDS.            #
# offsetFromMaster from DUT. If currentDS.offsetFromMaster is able to       #
# extract and export to calling procedure, then return 1, else return 0     #
# (failure condition)                                                       #
#############################################################################

    return 0
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::callback_snmp_get_delay_asymmetry               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  delay_asymmetry   : (Mandatory) portDS.delayAsymmetry.                      #
#                                  (e.g., 3)                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If able to extract portDS.delayAsymmetry  #
#                                    of specified port from the DUT).          #
#                                                                              #
#                      0 on Failure (If could not able to extract              #
#                                    portDS.delayAsymmetry of specified port   #
#                                    from the DUT).                            #
#                                                                              #
#  DEFINITION        : This function extracts portDS.delayAsymmetry of a       #
#                      specific port from the DUT.                             #
#                                                                              #
################################################################################

proc ptp_ha::callback_snmp_get_delay_asymmetry {args} {

    array set param $args

    set session_handler     $param(-session_handler)
    set port_num            $param(-port_num)
    set buffer              $param(-buffer)

    upvar $param(-delay_asymmetry) delay_asymmetry

#############################################################################
# Parse the SNMP output using $buffer and extract the portDS.delayAsymmetry #
# from DUT. If portDS.delayAsymmetry is able to extract and export to       #
# calling procedure, then return 1, else return 0 (failure condition)       #
#############################################################################

    return 0
}
