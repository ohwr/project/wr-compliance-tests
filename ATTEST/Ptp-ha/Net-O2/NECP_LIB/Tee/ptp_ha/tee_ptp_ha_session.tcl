
################################################################################
# File Name      : tee_ptp_session.tcl                                         #
# File Version   : 1.2                                                         #
# Component Name : ATTEST TEST EXECUTION ENGINE (TEE)                          #
# Module Name    : Precision Time Protocol - High Accuracy                     #
################################################################################
# History       Date     Author         Addition/ Alteration                   #
#                                                                              #
#  1.0       May/2018      CERN          Initial                               #
#  1.1       Jan/2019      CERN          a) Added dut_close_session.           #
#                                        b) Handled locking mechanism for      #
#                                           closing TEE session.               #
#  1.2       Mar/2019      CERN          Stopped sending of messages before    #
#                                        disconnecting Xena chassis.           #
#                                                                              #
################################################################################
# Copyright (c) 2018 - 2019 CERN                                               #
################################################################################

#########################PROCEDURES USED########################################
#                                                                              #
#  1) ptp_ha::tee_open_session                                                 #
#  2) ptp_ha::tee_close_session                                                #
#  3) dut_close_session                                                        #
#                                                                              #
################################################################################

package provide ptp_ha 1.0
namespace eval ptp_ha {

        namespace export *
}

################################################################################
#  PROCEDURE NAME    : ptp_ha::tee_open_session                                #
#                                                                              #
#  DEFINITION        : This function opens telnet session with the given       #
#                      Tee.                                                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_id        : (Mandatory) session id which is to be open with the     #
#                                  traffic generator.                          #
#                                                                              #
#  ip_address        : (Mandatory) The IP Address of traffic generator         #
#                                   to which the list of commands need to be   #
#                                   sent to.                                   #
#                                                                              #
#  user_name         : (Mandatory)  User Name.                                 #
#                                                                              #
#  passwd            : (Mandatory)  Password.                                  #
#                                                                              #
#  telnet_port       : (Mandatory)  Traffic generator Telnet Port.             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE                                                    #
#                                                                              #
#  RETURNS           : 1 on success (If the tee session is opened.)            #
#                                                                              #
#                      0 on failure (If the tee session is not opened.)        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#      ptp_ha::tee_open_session         -session_id      $session_id\          #
#                                      -ip_address      $ip_address\           #
#                                      -user_name       $user_name\            #
#                                      -passwd          $passwd\               #
#                                      -telnet_port     $telnet_port           #
#                                                                              #
################################################################################

proc ptp_ha::tee_open_session {args} {
    
   array set param $args
    
   global PKTLOGFILE
   global fileptr
   global child_pid

   set fileptr [open "$PKTLOGFILE" "w"]


     ### Checking Input Parameters ###

    if {[info exists param(-session_id)]} {

            upvar  $param(-session_id) session_id
    } else {

            ERRLOG -msg "ptp_ha::tee_open_session :  Missing Argument -> session_id\n"
            return 0
    }

    if {[info exists param(-user_name)]} {

            set user_name $param(-user_name)
    } else {

            ERRLOG -msg "ptp_ha::tee_open_session  :  Missing Argument -> user_name\n"
            return 0
    }

     ### CALL OPEN SESSION PROCEDURE ###

    if {![pltLib::tee_open_session\
               -session_id  session_id\
               -user_name   $user_name]} {

            ERRLOG -msg "Opening session with TEE is failed\n" 
        return 0
    }

   set child_pid 1

   return 1
}


#2.
################################################################################
#  PROCEDURE NAME    : ptp_ha::tee_close_session                               #
#                                                                              #
#  DEFINITION        : This function closes the session with the given TEE     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_id        : (Mandatory) session id to close the session             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE                                                    #
#                                                                              #
#  RETURNS           : 1 on success (If the session is closed)                 #
#                                                                              #
#                      0 on failure (If the session is not closed)             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#       ptp_ha::tee_close_session      -session_id      $session_id            #
#                                                                              #
################################################################################

proc ptp_ha::tee_close_session {args} {

    array set param $args

# Closing pkt file
    global fileptr

    close $fileptr

    if {[info exists param(-session_id)]} {
        set session_id $param(-session_id)
    } else {
        set session_id ${::session_id}
    }

    set instance 0
    while {$instance <= 17} {

        if [info exists ::_status_$instance] {
        
            #stop running instance
            pltLib::cancel_send_periodic\
                          -instance           $instance
        }
        incr instance
    }

    #Resource is in-use
    if { $::semlock == "LOCKED" } {

        after 5; ptp_ha::tee_close_session

    } else {

        set ::semlock "LOCKED"

        ptp_ha::tee_cleanup_setup_001\
                    -port_list      $::tee_port_num_1

        if {![pltLib::tee_close_session \
                    -session_id     $session_id]} {

            set ::semlock "UNLOCKED"
            return 0
        }

        set ::semlock "UNLOCKED"
    }

    return 1
}


#3.
################################################################################
#  PROCEDURE NAME    : dut_close_session                                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   session_handler  : (Mandatory) Session handler                             #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  RETURNS           : 1 on success                                            #
#                      0 on failure                                            #
#                                                                              #
#  DEFINITION        : Close the DUT CLI session with the given session        #
#                      handler.                                                #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      dut_close_session\                                      #
#                           -session_handler    session_id                     #
#                                                                              #
################################################################################

proc dut_close_session {args} {

    global env

    array set param $args

    set config_mode $env(DUT_CONFIG_MODE)
    set dut_type    $env(DUT_TYPE)

### CR ID : 17006 Set an env variable to check in sessions.tcl for logout   ###
    set env(DUT_SESSION_FLAG)  0  

### Check the Input Parameters ###
    if {[info exists param(-session_handler)]} {
        set session_id $param(-session_handler)
    } else {
        ERRLOG -msg "dut_close_session : Missing Argument -> -session_handler\n"
        return 0 
    }

    if {$config_mode == "cli"} {

        if { $dut_type == "pimd"} {

            pimd_close_session -session_handler $session_id
        } elseif { $dut_type == "linux"} {

            linux_close_session -session_handler $session_id
        } else {

            if {[info procs ptp_ha::post_test_operation] == "::ptp_ha::post_test_operation"} {

                ptp_ha::post_test_operation -session_handler $session_id
            }

            default_close_session -session_handler $session_id
        }

    } elseif {$config_mode == "http" } {

        set result [http_close_session -session_handler $session_id]
    }

    sleep 1

    catch {close -i $session_id}

    wait -nowait

    return 1
}
