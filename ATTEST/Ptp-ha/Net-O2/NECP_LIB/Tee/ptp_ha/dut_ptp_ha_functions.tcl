###############################################################################
# File Name         : dut_ptp-ha_functions.tcl                                #
# File Version      : 1.7                                                     #
# Component Name    : ATTEST DEVICE UNDER TEST (DUT)                          #
# Module Name       : Precision Time Protocol - High Accuracy                 #
###############################################################################
# History       Date       Author       Addition/Alteration                   #
###############################################################################
#                                                                             #
#  1.0       May/2018      CERN         Initial                               #
#  1.1       Jun/2018      CERN         Fixed script error in                 #
#                                       dut_get_timestamp                     #
#  1.2       Jul/2018      CERN         Added extracted values in get         #
#                                       procedures                            #
#  1.3       Sep/2018      CERN         Added new procedures                  #
#                                       a) dut_timestamp_corrected_tx_enable  #
#                                       b) dut_timestamp_corrected_tx_disable #
#                                       c) dut_check_slave_only               #
#                                       d) dut_check_master_only              #
#  1.4       Oct/2018      CERN         a) Added ptp_ha::dut_l1sync_enable in #
#                                          setup 004.                         #
#                                       b) Added configuration and cleanup    #
#                                          setup function for setup 008.      #
#                                       c) Added dut_get_mean_link_delay      #
#  1.5       Nov/2018      CERN         a) Removed dut_get_offset_from_master #
#                                       b) Added                              #
#                                       dut_check_for_least_offset_from_master#
#  1.6       Jan/2019      CERN         Did editorial change in               #
#                                       dut_check_for_least_offset_from_master#
#  1.7       Jan/2019      CERN         Changed values of                     #
#                                       timestampCorrectionPortDS's parameters#
#                                       in dut_configure_setup_002 to 0.      #
#                                                                             #
###############################################################################
# Copyright (c) 2018 - 2019 CERN                                              #
###############################################################################

######################### PROCEDURES USED #####################################
#                                                                             #
#  DUT SET FUNCTIONS     :                                                    #
#                                                                             #
#  1) ptp_ha::dut_port_enable                                                 #
#  2) ptp_ha::dut_port_ptp_enable                                             #
#  3) ptp_ha::dut_set_vlan                                                    #
#  4) ptp_ha::dut_set_vlan_priority                                           #
#  5) ptp_ha::dut_global_ptp_enable                                           #
#  6) ptp_ha::dut_set_communication_mode                                      #
#  7) ptp_ha::dut_set_domain                                                  #
#  8) ptp_ha::dut_set_network_protocol                                        #
#  9) ptp_ha::dut_set_clock_mode                                              #
# 10) ptp_ha::dut_set_clock_step                                              #
# 11) ptp_ha::dut_set_delay_mechanism                                         #
# 12) ptp_ha::dut_set_priority1                                               #
# 13) ptp_ha::dut_set_priority2                                               #
# 14) ptp_ha::dut_tcr_rcr_cr_enable                                           #
# 15) ptp_ha::dut_l1sync_enable                                               #
# 16) ptp_ha::dut_l1sync_opt_params_enable                                    #
# 17) ptp_ha::dut_slaveonly_enable                                            #
# 18) ptp_ha::dut_masteronly_enable                                           #
# 19) ptp_ha::dut_set_announce_interval                                       #
# 20) ptp_ha::dut_set_announce_timeout                                        #
# 21) ptp_ha::dut_set_sync_interval                                           #
# 22) ptp_ha::dut_set_delayreq_interval                                       #
# 23) ptp_ha::dut_set_pdelayreq_interval                                      #
# 24) ptp_ha::dut_set_l1sync_interval                                         #
# 25) ptp_ha::dut_set_l1sync_timeout                                          #
# 26) ptp_ha::dut_set_egress_latency                                          #
# 27) ptp_ha::dut_set_ingress_latency                                         #
# 28) ptp_ha::dut_set_constant_asymmetry                                      #
# 29) ptp_ha::dut_set_scaled_delay_coefficient                                #
# 30) ptp_ha::dut_set_asymmetry_correction                                    #
# 31) ptp_ha::dut_external_port_config_enable                                 #
# 32) ptp_ha::dut_set_epc_desired_state                                       #
# 33) ptp_ha::dut_set_ipv4_address                                            #
# 34) ptp_ha::dut_timestamp_corrected_tx_enable                               #
#                                                                             #
#  DUT DISABLE FUNCTIONS :                                                    #
#                                                                             #
#  1) ptp_ha::dut_port_disable                                                #
#  2) ptp_ha::dut_port_ptp_disable                                            #
#  3) ptp_ha::dut_reset_vlan                                                  #
#  4) ptp_ha::dut_reset_vlan_priority                                         #
#  5) ptp_ha::dut_global_ptp_disable                                          #
#  6) ptp_ha::dut_reset_communication_mode                                    #
#  7) ptp_ha::dut_reset_domain                                                #
#  8) ptp_ha::dut_reset_network_protocol                                      #
#  9) ptp_ha::dut_reset_clock_mode                                            #
# 10) ptp_ha::dut_reset_clock_step                                            #
# 11) ptp_ha::dut_reset_delay_mechanism                                       #
# 12) ptp_ha::dut_reset_priority1                                             #
# 13) ptp_ha::dut_reset_priority2                                             #
# 14) ptp_ha::dut_tcr_rcr_cr_disable                                          #
# 15) ptp_ha::dut_l1sync_disable                                              #
# 16) ptp_ha::dut_l1sync_opt_params_disable                                   #
# 17) ptp_ha::dut_slaveonly_disable                                           #
# 18) ptp_ha::dut_masteronly_disable                                          #
# 19) ptp_ha::dut_reset_announce_interval                                     #
# 20) ptp_ha::dut_reset_announce_timeout                                      #
# 21) ptp_ha::dut_reset_sync_interval                                         #
# 22) ptp_ha::dut_reset_delayreq_interval                                     #
# 23) ptp_ha::dut_reset_pdelayreq_interval                                    #
# 24) ptp_ha::dut_reset_l1sync_interval                                       #
# 25) ptp_ha::dut_reset_l1sync_timeout                                        #
# 26) ptp_ha::dut_reset_egress_latency                                        #
# 27) ptp_ha::dut_reset_ingress_latency                                       #
# 28) ptp_ha::dut_reset_constant_asymmetry                                    #
# 29) ptp_ha::dut_reset_scaled_delay_coefficient                              #
# 30) ptp_ha::dut_reset_asymmetry_correction                                  #
# 31) ptp_ha::dut_external_port_config_disable                                #
# 32) ptp_ha::dut_reset_epc_desired_state                                     #
# 33) ptp_ha::dut_reset_ipv4_address                                          #
# 34) ptp_ha::dut_timestamp_corrected_tx_disable                              #
#                                                                             #
#  DUT CHECK FUNCTIONS   :                                                    #
#                                                                             #
#  1) ptp_ha::dut_check_l1sync_state                                          #
#  2) ptp_ha::dut_check_ptp_port_state                                        #
#  3) ptp_ha::dut_check_asymmetry_correction                                  #
#  4) ptp_ha::dut_check_egress_latency                                        #
#  5) ptp_ha::dut_check_ingress_latency                                       #
#  6) ptp_ha::dut_check_constant_asymmetry                                    #
#  7) ptp_ha::dut_check_scaled_delay_coefficient                              #
#  8) ptp_ha::dut_check_external_port_config                                  #
#  9) ptp_ha::dut_check_slave_only                                            #
# 10) ptp_ha::dut_check_master_only                                           #
# 11) ptp_ha::dut_check_for_least_offset_from_master                          #
#                                                                             #
#  DUT GET FUNCTIONS     :                                                    #
#                                                                             #
#  1) ptp_ha::dut_get_timestamp                                               #
#  2) ptp_ha::dut_get_mean_delay                                              #
#  3) ptp_ha::dut_get_mean_link_delay                                         #
#  4) ptp_ha::dut_get_delay_asymmetry                                         #
#                                                                             #
#  DUT SETUP CONFIGURATION FUNCTIONS :                                        #
#                                                                             #
#  1) ptp_ha::dut_configure_setup_001                                         #
#  2) ptp_ha::dut_configure_setup_002                                         #
#  3) ptp_ha::dut_configure_setup_003                                         #
#  4) ptp_ha::dut_configure_setup_004                                         #
#  5) ptp_ha::dut_configure_setup_005                                         #
#  6) ptp_ha::dut_configure_setup_006                                         #
#  7) ptp_ha::dut_configure_setup_007                                         #
#  8) ptp_ha::dut_configure_setup_008                                         #
#  9) ptp_ha::dut_configure_setup_009                                         #
#                                                                             #
#  DUT SETUP CLEANUP FUNCTIONS       :                                        #
#                                                                             #
#  1) ptp_ha::dut_cleanup_setup_001                                           #
#  2) ptp_ha::dut_cleanup_setup_002                                           #
#  3) ptp_ha::dut_cleanup_setup_003                                           #
#  4) ptp_ha::dut_cleanup_setup_004                                           #
#  5) ptp_ha::dut_cleanup_setup_005                                           #
#  6) ptp_ha::dut_cleanup_setup_006                                           #
#  7) ptp_ha::dut_cleanup_setup_007                                           #
#  8) ptp_ha::dut_cleanup_setup_008                                           #
#  9) ptp_ha::dut_cleanup_setup_009                                           #
#                                                                             #
###############################################################################


################################################################################
#                           DUT SET FUNCTIONS                                  #
################################################################################

package provide ptp_ha 1.0

namespace eval ptp_ha {

    namespace export *
}

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_port_enable                                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) Port needs to be made administratively up.  #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If port is made administratively up)      #
#                                                                              #
#                      0 on Failure (If port could not be made                 #
#                                    administratively up)                      #
#                                                                              #
#  DEFINITION        : This function makes the specific port administratively  #
#                      up at the DUT.                                          #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_port_enable\                                  #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::dut_port_enable {args} {

    array set param $args
    global env

    set config_mode $env(DUT_CONFIG_MODE)
    set session_handler $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_port_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Enable port ($port_num)"

    if {![ptp_ha::${config_mode}_port_enable\
                    -session_handler  $session_handler\
                    -port_num         $port_num]} {
    
        ERRLOG -msg "ptp_ha::dut_port_enable : Unable to enable\
        specified port on DUT"
        return 0
    }

    return 1
}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_port_ptp_enable                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) Port on which ptp is to be enabled.         #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is enabled on the specified port   #
#                                    at the DUT).                              #
#                                                                              #
#                      0 on Failure (If PTP could not be enabled on the        #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function enables PTP on a specific port on the DUT.#
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_port_ptp_enable\                              #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::dut_port_ptp_enable {args} {

    array set param $args
    global env

    set config_mode $env(DUT_CONFIG_MODE)
    set session_handler $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_port_ptp_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Enable PTP on port ($port_num)"

    if {![ptp_ha::${config_mode}_port_ptp_enable\
                           -session_handler  $session_handler\
                           -port_num         $port_num]} {

        ERRLOG -msg "ptp_ha::dut_port_ptp_enable : Unable to enable PTP\
                    on specified port on DUT"
        return 0
    }

    return 1
}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_set_vlan                                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID with which VLAN has to be created.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#  port_num          : (Mandatory) Port which has to be associated to VLAN ID. #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If VLAN ID is created and port is         #
#                                    associated to it on the DUT).             #
#                                                                              #
#                      0 on Failure (If VLAN ID could not be created or port   #
#                                    is not associated to it on the DUT).      #
#                                                                              #
#  DEFINITION        : This function creates VLAN ID and associates given port #
#                      to it on the DUT.                                       #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_set_vlan\                                     #
#                             -vlan_id                  $vlan_id\              #
#                             -port_num                 $port_num              #
#                                                                              #
################################################################################

proc ptp_ha::dut_set_vlan {args} {

    array set param $args
    global env

    set config_mode $env(DUT_CONFIG_MODE)
    set session_handler $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_vlan : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_vlan : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Create VLAN ($vlan_id) and associate port ($port_num) to it"

    if {![ptp_ha::${config_mode}_set_vlan\
                           -session_handler  $session_handler\
                           -vlan_id          $vlan_id\
                           -port_num         $port_num]} {
    
        ERRLOG -msg "ptp_ha::dut_set_vlan : Unable to create VLAN and associate port on DUT"
        return 0
    }

    return 1
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_set_vlan_priority                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID                                     #
#                                  (e.g., 100)                                 #
#                                                                              #
#  vlan_priority     : (Mandatory) VLAN priority                               #
#                                                                              #
#  port_num          : (Mandatory) Port which has to be associated to VLAN ID. #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If VLAN priority is associated to         #
#                                    VLAN ID on the DUT).                      #
#                                                                              #
#                      0 on Failure (If VLAN priority is not associated to the #
#                                    VLAN ID on the DUT).                      #
#                                                                              #
#  DEFINITION        : This function associates VLAN priority to the VLAN ID   #
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_set_vlan_priority\                            #
#                             -vlan_id                  $vlan_id\              #
#                             -vlan_priority            $vlan_priority\        #
#                             -port_num                 $port_num              #
#                                                                              #
################################################################################

proc ptp_ha::dut_set_vlan_priority {args} {

    array set param $args
    global env

    set config_mode     $env(DUT_CONFIG_MODE)
    set session_handler $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_vlan_priority : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if {[info exists param(-vlan_priority)]} {
        set vlan_priority $param(-vlan_priority)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_vlan_priority : Missing Argument    -> vlan_priority"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_vlan_priority : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Associate VLAN priority ($vlan_priority) to VLAN ($vlan_id)"

    if {![ptp_ha::${config_mode}_set_vlan_priority\
                    -session_handler  $session_handler\
                    -vlan_id          $vlan_id\
                    -vlan_priority    $vlan_priority\
                    -port_num         $port_num]} {

        ERRLOG -msg "ptp_ha::dut_set_vlan_priority : Unable to associate priority to VLAN on DUT"
        return 0
    }

    return 1
}


#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_global_ptp_enable                           #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is enabled on the DUT).            #
#                                                                              #
#                      0 on Failure (If PTP could not be enabled on the DUT).  #
#                                                                              #
#  DEFINITION        : This function enables PTP on the DUT.                   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_port_ptp_enable                               #
#                                                                              #
################################################################################

proc ptp_ha::dut_global_ptp_enable {} {

    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    LOG -level 2 -msg "\t Enable PTP globally on DUT"

    if {![ptp_ha::${config_mode}_global_ptp_enable\
                          -session_handler  $session_handler]} {

        ERRLOG -msg "ptp_ha::dut_global_ptp_enable : Unable to enable PTP globally"
        return 0
    }

    return 1
}


#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_set_communication_mode                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  ptp_version       : (optional)  PTP version.                                #
#                                  (Default: 2)                                #
#                                                                              #
#  communication_mode : (Mandatory) Communication mode                         #
#                                  (e.g., "Unicast"|"Multicast")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If communication mode is set on the       #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If communication mode could not be        #
#                                    set on the specified port at the DUT).    #
#                                                                              #
#  DEFINITION        : This function sets the communication mode on a specific #
#                      port of the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_set_communication_mode\                       #
#                             -port_num                $port_num\              #
#                             -clock_mode              $clock_mode\            #
#                             -ptp_version             $ptp_version\           #
#                             -communication_mode      $communication_mode     #
#                                                                              #
################################################################################

proc ptp_ha::dut_set_communication_mode {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_communication_mode : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_communication_mode : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-communication_mode)]} {
        set communication_mode $param(-communication_mode)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_communication_mode : Missing Argument  -> communication_mode"
        incr missedArgCounter
    }

    if {[info exists param(-ptp_version)]} {
        set ptp_version $param(-ptp_version)
    } else {
        set ptp_version "2"
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set communication mode ($communication_mode) on port ($port_num)"

    if {![ptp_ha::${config_mode}_set_communication_mode\
                           -session_handler      $session_handler\
                           -port_num             $port_num\
                           -clock_mode           $clock_mode\
                           -ptp_version          $ptp_version\
                           -communication_mode   $communication_mode]} {

        ERRLOG -msg "ptp_ha::dut_set_communication_mode :\
                    Unable to set PTP communication mode on specific port of the DUT"
        return 0
    }

    return 1
}


#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_set_domain                                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  domain            : (Mandatory) Domain number.                              #
#                                  (e.g., 0..255)                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If domain number is set on the DUT).      #
#                                                                              #
#                      0 on Failure (If domain number could not able to set on #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function sets domain number on the DUT.            #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_set_domain\                                   #
#                             -clock_mode      $clock_mode\                    #
#                             -domain          $domain                         #
#                                                                              #
################################################################################

proc ptp_ha::dut_set_domain {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_domain : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-domain)]} {
        set domain $param(-domain)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_domain : Missing Argument    -> domain"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set defaultDS.domainNumber ($domain) on DUT"

    if {![ptp_ha::${config_mode}_set_domain\
                           -session_handler  $session_handler\
                           -clock_mode       $clock_mode\
                           -domain           $domain]} {

        ERRLOG -msg "ptp_ha::dut_set_domain : Unable to set domain on DUT"
        return 0
    }

    return 1
}


#8.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_set_network_protocol                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) clock_mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  network_protocol  : (Mandatory) Network protocol.                           #
#                                  (e.g., "IEEE 802.3"|"UDP/IPv4")             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If network protocol is set on specified   #
#                                    port at the DUT).                         #
#                                                                              #
#                      0 on Failure (If network protocol could not be set      #
#                                    on specified port at the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets the network protocol on a specific   #
#                      port of the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                     ptp_ha::dut_set_network_protocol\                        #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -network_protocol $network_protocol              #
#                                                                              #
################################################################################

proc ptp_ha::dut_set_network_protocol {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_network_protocol : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_network_protocol : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-network_protocol)]} {
        set network_protocol $param(-network_protocol)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_network_protocol : Missing Argument    -> network_protocol"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set networkProtocol ($network_protocol) on port ($port_num)"

    if {![ptp_ha::${config_mode}_set_network_protocol\
                    -session_handler  $session_handler\
                    -port_num         $port_num\
                    -clock_mode       $clock_mode\
                    -network_protocol $network_protocol]} {

        ERRLOG -msg "ptp_ha::dut_set_network_protocol : Unable to set network protocol\
                    on specified port at the DUT"
        return 0
    }

    return 1
}


#9.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_set_clock_mode                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP clock mode is set on specified port#
#                                    of the DUT).                              #
#                                                                              #
#                      0 on Failure (If PTP clock mode could not be set on     #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function sets clock mode on a specific port of DUT.#
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_set_clock_mode\                               #
#                             -port_num        $port_num\                      #
#                             -clock_mode      $clock_mode                     #
#                                                                              #
################################################################################

proc ptp_ha::dut_set_clock_mode {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_clock_mode : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_clock_mode : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set clock mode ($clock_mode) on port ($port_num)"

    if {![ptp_ha::${config_mode}_set_clock_mode\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -clock_mode       $clock_mode]} {

        ERRLOG -msg "ptp_ha::dut_set_clock_mode : Unable to set clock mode\
                    on specified port on DUT"
        return 0
    }

    return 1
}


#10.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_set_clock_step                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  clock_step        : (Mandatory) Clock step.                                 #
#                                  (e.g., "One-step"|"Two-step")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If clock-step is set on the specified     #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If clock-step could not be able to set    #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets the clock-step on a specific port on #
#                      the DUT.                                                #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_set_clock_step\                               #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -clock_step       $clock_step                    #
#                                                                              #
################################################################################

proc ptp_ha::dut_set_clock_step {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_clock_step : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_clock_step : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-clock_step)]} {
        set clock_step $param(-clock_step)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_clock_step : Missing Argument    -> clock_step"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set clock step ($clock_step) on port ($port_num)"

    if {![ptp_ha::${config_mode}_set_clock_step\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -clock_mode       $clock_mode\
                           -clock_step       $clock_step]} {

        ERRLOG -msg "ptp_ha::dut_set_clock_step : Unable to set clock step\
                    on specified port on DUT"
        return 0
    }

    return 1
}


#11.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_set_delay_mechanism                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  delay_mechanism   : (Mandatory) Delay mechanism.                            #
#                                  (e.g., e2e|p2p|disable)                     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If delay mechanism is set on specified    #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If delay mechanism could not be set on    #
#                                    specified PTP port of the DUT).           #
#                                                                              #
#  DEFINITION        : This function sets the delay mechanism to a specific    #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_set_delay_mechanism\                          #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -delay_mechanism  $delay_mechanism               #
#                                                                              #
################################################################################

proc ptp_ha::dut_set_delay_mechanism {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_delay_mechanism : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_delay_mechanism : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-delay_mechanism)]} {
        set delay_mechanism $param(-delay_mechanism)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_delay_mechanism : Missing Argument    -> delay_mechanism"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set portDS.delayMechanism ($delay_mechanism) on port ($port_num)"

    if {![ptp_ha::${config_mode}_set_delay_mechanism\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -clock_mode       $clock_mode\
                           -delay_mechanism  $delay_mechanism]} {

        ERRLOG -msg "ptp_ha::dut_set_delay_mechanism : Unable to set delay mechanism\
                    on specified port on DUT"
        return 0
    }

    return 1
}


#12.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_set_priority1                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority1         : (Mandatory) Prority1                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority1 is set on the DUT).          #
#                                                                              #
#                      0 on Failure (If priority1 could not be able to set on  #
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function sets the priority1 on the DUT.            #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_set_priority1\                                #
#                             -clock_mode      $clock_mode\                    #
#                             -priority1       $priority1                      #
#                                                                              #
################################################################################

proc ptp_ha::dut_set_priority1 {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_priority1 : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-priority1)]} {
        set priority1  $param(-priority1)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_priority1 : Missing Argument    -> priority1"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set defaultDS.priority1 ($priority1) on DUT"

    if {![ptp_ha::${config_mode}_set_priority1\
                           -session_handler  $session_handler\
                           -clock_mode       $clock_mode\
                           -priority1        $priority1]} {

        ERRLOG  -msg "ptp_ha::dut_set_priority1: Unable to set priority1 on the DUT"
        return 0
    }

    return 1
}


#13.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_set_priority2                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority2         : (Mandatory) Priority2                                   #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority2 is set on the DUT).          #
#                                                                              #
#                      0 on Failure (If priority2 could not be able to set on  #
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function sets the priority2 on the DUT.            #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_set_priority2\                                #
#                             -clock_mode      $clock_mode\                    #
#                             -priority2       $priority2                      #
#                                                                              #
################################################################################

proc ptp_ha::dut_set_priority2 {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_priority2 : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-priority2)]} {
        set priority2  $param(-priority2)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_priority2 : Missing Argument    -> priority2"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set defaultDS.priority2 ($priority2) on DUT"

    if {![ptp_ha::${config_mode}_set_priority2\
                           -session_handler  $session_handler\
                           -clock_mode       $clock_mode\
                           -priority2        $priority2]} {

        ERRLOG  -msg "ptp_ha::dut_set_priority2: Unable to set priority2 on the DUT"
        return 0
    }

    return 1
}


#14.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_tcr_rcr_cr_enable                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If peerTxCoherentIsRequired,              #
#                                    peerRxCoherentIsRequired and              #
#                                    peerCongruentIsRequired is enabled on the #
#                                    DUT).                                     #
#                                                                              #
#                      0 on Failure (If peerTxCoherentIsRequired,              #
#                                    peerRxCoherentIsRequired and              #
#                                    peerCongruentIsRequired could not be      #
#                                    enabled on the DUT).                      #
#                                                                              #
#                                                                              #
#  DEFINITION        : This function enables peerTxCoherentIsRequired,         #
#                      peerRxCoherentIsRequired and peerCongruentIsRequired on #
#                      the DUT.                                                #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_tcr_rcr_cr_enable\                            #
#                             -port_num                 $port_num              #
#                                                                              #
################################################################################

proc ptp_ha::dut_tcr_rcr_cr_enable {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_tcr_rcr_cr_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Enable L1SyncBasicPortDS.peerTxCoherentIsRequired,\
                          L1SyncBasicPortDS.peerRxCoherentIsRequired\
                          and L1SyncBasicPortDS.peerCongruentIsRequired on port ($port_num)"

    if {![ptp_ha::${config_mode}_tcr_rcr_cr_enable\
                           -session_handler  $session_handler\
                           -port_num         $port_num]} {

        ERRLOG  -msg "ptp_ha::dut_tcr_rcr_cr_enable : Unable to enable\
                      L1SyncBasicPortDS.peerTxCoherentIsRequired,\
                      L1SyncBasicPortDS.peerRxCoherentIsRequired and\
                      L1SyncBasicPortDS.peerCongruentIsRequired on DUT"
        return 0
    }

    return 1
}


#15.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_l1sync_enable                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If L1Sync option is enabled on the        #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If L1Sync option could not be enabled on  #
#                                    the specified port of the DUT).           #
#                                                                              #
#  DEFINITION        : This function enables L1Sync option on a specific port  #
#                      at the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_l1sync_enable\                                #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::dut_l1sync_enable {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_l1sync_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Enable L1SyncBasicPortDS.L1SyncEnabled on port ($port_num)"

    if {![ptp_ha::${config_mode}_l1sync_enable\
                           -session_handler  $session_handler\
                           -port_num         $port_num]} {

        ERRLOG -msg "ptp_ha::dut_l1sync_enable : Unable to enable L1SyncBasicPortDS.L1SyncEnabled\
                    on specified port on DUT"
        return 0
    }

    return 1
}


#16.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_l1sync_opt_params_enable                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If extended format of L1Sync TLV is       #
#                                    enabled on the specified port at the DUT).#
#                                                                              #
#                      0 on Failure (If extended format of L1Sync TLV could not#
#                                    be enabled on the specified port at the   #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function enables extended format of L1Sync TLV on  #
#                      the specified port at the DUT.                          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_l1sync_opt_params_enable\                     #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::dut_l1sync_opt_params_enable {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_l1sync_opt_params_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Enable L1SyncBasicPortDS.optParamsEnabled on port ($port_num)"

    if {![ptp_ha::${config_mode}_l1sync_opt_params_enable\
                           -session_handler  $session_handler\
                           -port_num         $port_num]} {

        ERRLOG -msg "ptp_ha::dut_l1sync_opt_params_enable : Unable to enable\
                     L1SyncBasicPortDS.optParamsEnabled on specified port on DUT"
        return 0
    }

    return 1
}


#17.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_slaveonly_enable                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If slave-only is enabled on the           #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If slave-only could not be enabled on the #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function enables slave-only on the specified port  #
#                      at the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_slaveonly_enable\                             #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::dut_slaveonly_enable {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_slaveonly_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Enable defaultDS.slaveOnly"

    if {![ptp_ha::${config_mode}_slaveonly_enable\
                           -session_handler  $session_handler\
                           -port_num         $port_num]} {

        ERRLOG -msg "ptp_ha::dut_slaveonly_enable : Unable to enable defaultDS.slaveOnly\n"
        return 0
    }

    return 1
}


#18.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_masteronly_enable                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If master-only is enabled on the          #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If master-only could not be enabled on the#
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function enables master-only on the specified port #
#                      at the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_masteronly_enable\                            #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::dut_masteronly_enable {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_masteronly_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Enable portDS.masterOnly on port ($port_num)"

    if {![ptp_ha::${config_mode}_masteronly_enable\
                           -session_handler  $session_handler\
                           -port_num         $port_num]} {

        ERRLOG -msg "ptp_ha::dut_masteronly_enable : Unable to enable portDS.masterOnly\
                    on specified port on DUT"
        return 0
    }

    return 1
}


#19.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_set_announce_interval                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  announce_interval : (Mandatory) announceInterval.                           #
#                                  (e.g., 1|2|4|8|16)                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If announceInterval value is set on       #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If announceInterval value could not be    #
#                                    set on specified port of the DUT).        #
#                                                                              #
#  DEFINITION        : This function sets announceInterval value to a specific #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_set_announce_interval\                        #
#                             -port_num                 $port_num\             #
#                             -announce_interval        $announce_interval     #
#                                                                              #
################################################################################

proc ptp_ha::dut_set_announce_interval {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_announce_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-announce_interval)]} {
        set announce_interval $param(-announce_interval)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_announce_interval : Missing Argument    -> announce_interval"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set portDS.logAnnounceInterval ($announce_interval) on port ($port_num)"

    if {![ptp_ha::${config_mode}_set_announce_interval\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -announce_interval $announce_interval]} {

        ERRLOG -msg "ptp_ha::dut_set_announce_interval : Unable to set portDS.logAnnounceInterval\
                    on specified port on DUT"
        return 0
    }

    return 1
}


#20.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_set_announce_timeout                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  announce_timeout  : (Mandatory) announceReceiptTimeout.                     #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If announceReceiptTimeout value is set on #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If announceReceiptTimeout value could not #
#                                    be set on specified port of the DUT).     #
#                                                                              #
#  DEFINITION        : This function sets announceReceiptTimeout value to a    #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_set_announce_timeout\                         #
#                             -port_num                 $port_num\             #
#                             -announce_timeout         $announce_timeout      #
#                                                                              #
################################################################################

proc ptp_ha::dut_set_announce_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_announce_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-announce_timeout)]} {
        set announce_timeout $param(-announce_timeout)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_announce_timeout : Missing Argument    -> announce_timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set portDS.announceReceiptTimeout ($announce_timeout) on port ($port_num)"

    if {![ptp_ha::${config_mode}_set_announce_timeout\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -announce_timeout $announce_timeout]} {

        ERRLOG -msg "ptp_ha::dut_set_announce_timeout : Unable to set portDS.announceReceiptTimeout\
                     on specified port on DUT\n"
        return 0
    }

    return 1
}


#21.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_set_sync_interval                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  sync_interval     : (Mandatory) logSyncInterval                             #
#                                  (e.g., 3)                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If logSyncInterval value is set on        #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If logSyncInterval value could not be     #
#                                    set on specified port of the DUT).        #
#                                                                              #
#  DEFINITION        : This function sets logSyncInterval value to a specific  #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_set_sync_interval\                            #
#                          -port_num              $port_num\                   #
#                          -sync_interval         $sync_interval               #
#                                                                              #
################################################################################

proc ptp_ha::dut_set_sync_interval {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter  0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_sync_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-sync_interval)]} {
        set sync_interval $param(-sync_interval)
    } else {
        ERRLOG  -msg "ptp_ha::dut_set_sync_interval : Missing Argument    -> sync_interval"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set portDS.logSyncInterval ($sync_interval) on port ($port_num)"

    if {![ptp_ha::${config_mode}_set_sync_interval\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -sync_interval    $sync_interval]} {

        ERRLOG -msg "ptp_ha::dut_sync_interval_enable : Unable to set portDS.logSyncInterval\
                    on specified port on DUT"
        return 0
    }

    return 1
}


#22.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_set_delayreq_interval                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  delayreq_interval : (Mandatory) MinDelayReqInterval.                        #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If MinDelayReqInterval is set on specified#
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If MinDelayReqInterval could not be set   #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets the MinDelayReqInterval to the       #
#                      specific PTP port on the DUT.                           #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_set_delayreq_interval\                        #
#                             -port_num                 $port_num\             #
#                             -delayreq_interval        $delayreq_interval     #
#                                                                              #
################################################################################

proc ptp_ha::dut_set_delayreq_interval {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG  -msg "ptp_ha::dut_set_delayreq_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-delayreq_interval)]} {
        set delayreq_interval $param(-delayreq_interval)
    } else {
        ERRLOG  -msg "ptp_ha::dut_set_delayreq_interval : Missing Argument    -> delayreq_interval"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set portDS.logMinDelayReqInterval ($delayreq_interval) on port ($port_num)"

    if {![ptp_ha::${config_mode}_set_delayreq_interval\
                           -session_handler    $session_handler\
                           -port_num           $port_num\
                           -delayreq_interval  $delayreq_interval]} {

        ERRLOG -msg "ptp_ha::dut_set_delayreq_interval : Unable to set portDS.logMinDelayReqInterval\
                    on specified port on DUT"
        return 0
    }

    return 1
}


#23.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_set_pdelayreq_interval                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  pdelayreq_interval: (Mandatory) minPdelayReqInterval.                       #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If minPdelayReqInterval is set on         #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If minPdelayReqInterval could not be set  #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets the minPdelayReqInterval to the      #
#                      specific PTP port on the DUT.                           #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_set_pdelayreq_interval\                       #
#                             -port_num                 $port_num\             #
#                             -pdelayreq_interval       $pdelayreq_interval    #
#                                                                              #
################################################################################

proc ptp_ha::dut_set_pdelayreq_interval {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_pdelayreq_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-pdelayreq_interval)]} {
        set pdelayreq_interval $param(-pdelayreq_interval)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_pdelayreq_interval : Missing Argument    -> pdelayreq_interval"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set portDS.logMinPdelayReqInterval ($pdelayreq_interval)\
                          on port ($port_num)"

    if {![ptp_ha::${config_mode}_set_pdelayreq_interval\
                           -session_handler    $session_handler\
                           -port_num           $port_num\
                           -pdelayreq_interval $pdelayreq_interval]} {

        ERRLOG -msg "ptp_ha::dut_set_pdelayreq_interval : Unable to set portDS.logMinPdelayReqInterval\
                     on specified port on DUT\n"
        return 0
    }

    return 1
}


#24.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_set_l1sync_interval                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  l1sync_interval   : (Mandatory) logL1SyncInterval.                          #
#                                  (e.g.,3 )                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If logL1SyncInterval is set on specified  #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If logL1SyncInterval could not be set on  #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function sets the logL1SyncInterval to a specific  #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_set_l1sync_interval\                          #
#                          -port_num              $port_num\                   #
#                          -l1sync_interval       $l1sync_interval             #
#                                                                              #
################################################################################

proc ptp_ha::dut_set_l1sync_interval {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_l1sync_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-l1sync_interval)]} {
        set l1sync_interval $param(-l1sync_interval)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_l1sync_interval : Missing Argument    -> l1sync_interval"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set L1SyncBasicPortDS.logL1SyncInterval ($l1sync_interval)\
                          on port ($port_num)"

    if {![ptp_ha::${config_mode}_set_l1sync_interval\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -l1sync_interval  $l1sync_interval]} {

        ERRLOG -msg "ptp_ha::dut_set_l1sync_interval : Unable to set L1SyncBasicPortDS.logL1SyncInterval\
                     on specified port on DUT"
        return 0
    }

    return 1
}


#25.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_set_l1sync_timeout                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  l1sync_timeout    : (Mandatory) L1SyncReceiptTimeout.                       #
#                                  (e.g., 3)                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If L1SyncReceiptTimeout is set on         #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If L1SyncReceiptTimeout could not be set  #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets L1SyncReceiptTimeout to a specific   #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_set_l1sync_timeout\                           #
#                          -port_num              $port_num\                   #
#                          -l1sync_timeout        $l1sync_timeout              #
#                                                                              #
################################################################################

proc ptp_ha::dut_set_l1sync_timeout {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_l1sync_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-l1sync_timeout)]} {
        set l1sync_timeout $param(-l1sync_timeout)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_l1sync_timeout : Missing Argument    -> l1sync_timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set L1SyncBasicPortDS.L1SyncReceiptTimeout ($l1sync_timeout)\
                          on port ($port_num)"

    if {![ptp_ha::${config_mode}_set_l1sync_timeout\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -l1sync_timeout   $l1sync_timeout]} {

        ERRLOG -msg "ptp_ha::dut_set_l1sync_timeout : Unable to set L1SyncBasicPortDS.L1SyncReceiptTimeout\
                     on specified port on DUT"
        return 0
    }

    return 1
}


#26.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_set_egress_latency                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Mandatory) timestampCorrectionPortDS.egressLatency.    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If timestampCorrectionPortDS.egressLatency#
#                                    is set on specified port of the DUT).     #
#                                                                              #
#                      0 on Failure (If timestampCorrectionPortDS.egressLatency#
#                                    could not be set on specified port of the #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function sets timestampCorrectionPortDS.           #
#                      egressLatency to a specific port on the DUT.            #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_set_egress_latency\                           #
#                          -port_num              $port_num\                   #
#                          -latency               $latency                     #
#                                                                              #
################################################################################

proc ptp_ha::dut_set_egress_latency {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_egress_latency : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-latency)]} {
        set latency $param(-latency)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_egress_latency : Missing Argument    -> latency"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set timestampCorrectionPortDS.egressLatency ($latency ns) on port ($port_num)"

    if {![ptp_ha::${config_mode}_set_egress_latency\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -latency          $latency]} {

        ERRLOG -msg "ptp_ha::dut_set_egress_latency : Unable to set timestampCorrectionPortDS.egressLatency\
                     on specified port on DUT"
        return 0
    }

    return 1
}


#27.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_set_ingress_latency                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Mandatory) timestampCorrectionPortDS.ingressLatency.   #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If timestampCorrectionPortDS.             #
#                                    ingressLatency is set on specified port of#
#                                    the DUT).                                 #
#                                                                              #
#                      0 on Failure (If timestampCorrectionPortDS.             #
#                                    ingressLatency could not be set on        #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function sets timestampCorrectionPortDS.           #
#                      ingressLatency to a specific port on the DUT.           #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_set_ingress_latency\                          #
#                          -port_num              $port_num\                   #
#                          -latency               $latency                     #
#                                                                              #
################################################################################

proc ptp_ha::dut_set_ingress_latency {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_ingress_latency : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-latency)]} {
        set latency $param(-latency)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_ingress_latency : Missing Argument    -> latency"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set timestampCorrectionPortDS.ingressLatency ($latency ns) on port ($port_num)"

    if {![ptp_ha::${config_mode}_set_ingress_latency\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -latency          $latency]} {

        ERRLOG -msg "ptp_ha::dut_set_ingress_latency : Unable to set timestampCorrectionPortDS.ingressLatency\
                     on specified port on DUT"
        return 0
    }

    return 1
}


#28.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_set_constant_asymmetry                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  constant_asymmetry :(Mandatory) asymmetryCorrectionPortDS.constantAsymmetry.#
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If asymmetryCorrectionPortDS.             #
#                                    constantAsymmetry is set on specified port#
#                                    of the DUT).                              #
#                                                                              #
#                      0 on Failure (If asymmetryCorrectionPortDS.             #
#                                    constantAsymmetry could not be set on     #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function sets asymmetryCorrectionPortDS.           #
#                      constantAsymmetry to a specific port on the DUT.        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_set_constant_asymmetry\                       #
#                          -port_num              $port_num\                   #
#                          -constant_asymmetry    $constant_asymmetry          #
#                                                                              #
################################################################################

proc ptp_ha::dut_set_constant_asymmetry {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_constant_asymmetry : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-constant_asymmetry)]} {
        set constant_asymmetry $param(-constant_asymmetry)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_constant_asymmetry : Missing Argument    -> constant_asymmetry"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set asymmetryCorrectionPortDS.constantAsymmetry ($constant_asymmetry ns) on port ($port_num)"

    if {![ptp_ha::${config_mode}_set_constant_asymmetry\
                           -session_handler     $session_handler\
                           -port_num            $port_num\
                           -constant_asymmetry  $constant_asymmetry]} {

        ERRLOG -msg "ptp_ha::dut_set_l1sync_timeout : Unable to set asymmetryCorrectionPortDS.constantAsymmetry\
                     on specified port on DUT"
        return 0
    }

    return 1
}


#29.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_set_scaled_delay_coefficient                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  delay_coefficient : (Mandatory) asymmetryCorrectionPortDS.                  #
#                                  scaledDelayCoefficient .                    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If asymmetryCorrectionPortDS.             #
#                                    scaledDelayCoefficient is set on specified#
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If asymmetryCorrectionPortDS.             #
#                                    scaledDelayCoefficient could not be set on#
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function sets asymmetryCorrectionPortDS.           #
#                      scaledDelayCoefficient to a specific port on the DUT.   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_set_scaled_delay_coefficient\                 #
#                          -port_num              $port_num\                   #
#                          -delay_coefficient     $delay_coefficient           #
#                                                                              #
################################################################################

proc ptp_ha::dut_set_scaled_delay_coefficient {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_scaled_delay_coefficient : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-delay_coefficient)]} {
        set delay_coefficient $param(-delay_coefficient)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_scaled_delay_coefficient : Missing Argument    -> delay_coefficient"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set asymmetryCorrectionPortDS.scaledDelayCoefficient ($delay_coefficient (fractional number))\
                          on port ($port_num)"

    if {![ptp_ha::${config_mode}_set_scaled_delay_coefficient\
                           -session_handler    $session_handler\
                           -port_num           $port_num\
                           -delay_coefficient  $delay_coefficient]} {

        ERRLOG -msg "ptp_ha::dut_set_scaled_delay_coefficient : Unable to set\
                     asymmetryCorrectionPortDS.scaledDelayCoefficient\
                     on specified port on DUT"
        return 0
    }

    return 1
}


#30.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_set_asymmetry_correction                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If asymmetryCorrectionPortDS.enable is set#
#                                    on specified port of the DUT).            #
#                                                                              #
#                      0 on Failure (If asymmetryCorrectionPortDS.enable could #
#                                    not be set on specified port of the DUT). #
#                                                                              #
#  DEFINITION        : This function sets the asymmetryCorrectionPortDS.enable #
#                      to the specific PTP port on the DUT.                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_set_asymmetry_correction\                     #
#                        -port_num          $port_num                          #
#                                                                              #
################################################################################

proc ptp_ha::dut_set_asymmetry_correction {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_asymmetry_correction : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set asymmetryCorrectionPortDS.enable on port ($port_num)"

    if {![ptp_ha::${config_mode}_set_asymmetry_correction\
                           -session_handler  $session_handler\
                           -port_num         $port_num]} { 

        ERRLOG -msg "ptp_ha::dut_set_asymmetry_correction : Unable to set asymmetryCorrectionPortDS.enable\
                    on specified port on DUT"
        return 0
    }

    return 1
}


#31.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_external_port_config_enable                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If external port configuration option is  #
#                                    enabled on the specified port at the DUT).#
#                                                                              #
#                      0 on Failure (If external port configuration option     #
#                                    could not be enabled on the specified port#
#                                    at the DUT).                              #
#                                                                              #
#  DEFINITION        : This function enables external port configuration option#
#                      on a specific port at the DUT.                          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_external_port_config_enable\                  #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::dut_external_port_config_enable {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_external_port_config_enable: Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Enable defaultDS.externalPortConfigurationEnabled on port ($port_num)"

    if {![ptp_ha::${config_mode}_external_port_config_enable\
                    -session_handler  $session_handler\
                    -port_num         $port_num]} {
    
        ERRLOG -msg "ptp_ha::dut_external_port_config_enable : Unable to enable\
                     defaultDS.externalPortConfigurationEnabled on specified port on DUT\n"
        return 0
    }

    return 1
}


#32.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_set_epc_desired_state                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  desired_state     : (Mandatory) externalPortConfigurationPortDS.desiredState#
#                                  (e.g., "INITIALIZING"|"FAULTY"|"DISABLED"|  #
#                                         "LISTENING"|"PRE_MASTER"|"MASTER"|   #
#                                         "PASSIVE"|"UNCALIBRATED"|"SLAVE"     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given externalPortConfigurationPortDS. #
#                                    desiredState is set on specified port of  #
#                                    the DUT).                                 #
#                                                                              #
#                      0 on Failure (If given externalPortConfigurationPortDS. #
#                                    desiredState could not be set on specifed #
#                                    port of the DUT).                         #
#                                                                              #
#  DEFINITION        : This function sets given externalPortConfigurationPortDS#
#                      desiredState to a specific port on the DUT.             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_set_scaled_delay_coefficient\                 #
#                          -port_num              $port_num\                   #
#                          -desired_state         $desired_state               #
#                                                                              #
################################################################################

proc ptp_ha::dut_set_epc_desired_state {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_epc_desired_state : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-desired_state)]} {
        set desired_state $param(-desired_state)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_epc_desired_state : Missing Argument    -> desired_state"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set externalPortConfigurationPortDS.desiredState ($desired_state)\
                      on port ($port_num)"

    if {![ptp_ha::${config_mode}_set_epc_desired_state\
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -desired_state      $desired_state]} {

        ERRLOG -msg "ptp_ha::dut_set_epc_desired_state : Unable to set\
        externalPortConfigurationPortDS.desiredState\
        on specified port on DUT"
        return 0
    }

    return 1
}


#33.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_set_ipv4_address                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  ip_address        : (Mandatory) IPv4 address.                               #
#                                  (e.g., 192.168.200.1)                       #
#                                                                              #
#  net_mask          : (Mandatory) Subnet mask.                                #
#                                  (e.g., 255.255.255.0)                       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID.                                    #
#                                  (e.g., 100)                                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given IP address is set on specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given IP address could not be set on   #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function sets given IP address to a specific port  #
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_set_ipv4_address\                             #
#                          -port_num              $port_num\                   #
#                          -ip_address            $ip_address\                 #
#                          -net_mask              $net_mask\                   #
#                          -vlan_id               $vlan_id                     #
#                                                                              #
################################################################################

proc ptp_ha::dut_set_ipv4_address {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_ipv4_address : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-ip_address)]} {
        set ip_address $param(-ip_address)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_ipv4_address : Missing Argument    -> ip_address"
        incr missedArgCounter
    }

    if {[info exists param(-net_mask)]} {
        set net_mask $param(-net_mask)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_ipv4_address : Missing Argument    -> net_mask"
        incr missedArgCounter
    }

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "ptp_ha::dut_set_ipv4_address : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Set IP address ($ip_address) on port ($port_num)"

    if {![ptp_ha::${config_mode}_set_ipv4_address\
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -ip_address         $ip_address\
                    -net_mask           $net_mask\
                    -vlan_id            $vlan_id]} {

        ERRLOG -msg "ptp_ha::dut_set_ipv4_address : Unable to set\
                    IP address on specified port on DUT"
        return 0
    }

    return 1
}


#34.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_timestamp_corrected_tx_enable               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If L1SyncOptParamsPortDS.                 #
#                                    timestampsCorrectedTx option is enabled on#
#                                    the specified port at the DUT).           #
#                                                                              #
#                      0 on Failure (If L1SyncOptParamsPortDS.                 #
#                                    timestampsCorrectedTx option could not be #
#                                    enabled on the specified port at the DUT).#
#                                                                              #
#  DEFINITION        : This function enables L1SyncOptParamsPortDS.            #
#                      timestampsCorrectedTx option on a specific port at the  #
#                      DUT.                                                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_timestamp_corrected_tx_enable\                #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::dut_timestamp_corrected_tx_enable {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_timestamp_corrected_tx_enable: Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Enable L1SyncOptParamsPortDS.timestampsCorrectedTx option on port ($port_num)"

    if {![ptp_ha::${config_mode}_timestamp_corrected_tx_enable\
                    -session_handler  $session_handler\
                    -port_num         $port_num]} {

        ERRLOG -msg "ptp_ha::dut_timestamp_corrected_tx_enable: Unable to enable\
                     L1SyncOptParamsPortDS.timestampsCorrectedTx option on specified\
                     port on DUT\n"

        return 0
    }

    return 1
}

################################################################################
#                       DUT DISABLING FUNCTIONS                                #
################################################################################


#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_port_disable                                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) Port needs to be made administratively down.#
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If port is made administratively down)    #
#                                                                              #
#                      0 on Failure (If port could not be made                 #
#                                    administratively down)                    #
#                                                                              #
#  DEFINITION        : This function makes the specific port administratively  #
#                      down at the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_port_disable\                                 #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::dut_port_disable {args} {

    array set param $args
    global env

    set config_mode     $env(DUT_CONFIG_MODE)
    set session_handler $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_port_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Disable port ($port_num)"

    if {![ptp_ha::${config_mode}_port_disable\
                    -session_handler  $session_handler\
                    -port_num         $port_num]} {

        ERRLOG -msg "ptp_ha::dut_port_disable : Unable to disable\
                     specified port on DUT"
        return 0
    }

    return 1
}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_port_ptp_disable                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) Port on which ptp is to be disabled.        #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is disabled on the specified port  #
#                                    at the DUT).                              #
#                                                                              #
#                      0 on Failure (If PTP could not be disabled on the       #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function disables PTP on a specific port on the DUT#
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_port_ptp_disable\                             #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::dut_port_ptp_disable {args} {

    array set param $args
    global env

    set config_mode $env(DUT_CONFIG_MODE)
    set session_handler $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_port_ptp_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Disable PTP on port ($port_num)"

    if {![ptp_ha::${config_mode}_port_ptp_disable\
                           -session_handler  $session_handler\
                           -port_num         $port_num]} {

        ERRLOG -msg "ptp_ha::dut_port_ptp_disable : Unable to disable PTP\
                    on specified port on DUT"
        return 0
    }

    return 1
}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_reset_vlan                                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID with which VLAN has to be deleted.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#  port_num          : (Mandatory) Port which has to be removed from VLAN ID.  #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If port is removed from VLAN ID and VLAN  #
#                                    ID is deleted from the DUT).              #
#                                                                              #
#                      0 on Failure (If port could not be removed from VLAN ID #
#                                    and VLAN ID is not deledted from the DUT).#
#                                                                              #
#  DEFINITION        : This function removes port from VLAN ID and deletes VLAN#
#                      ID from the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_reset_vlan\                                   #
#                             -vlan_id                  $vlan_id\              #
#                             -port_num                 $port_num              #
#                                                                              #
################################################################################

proc ptp_ha::dut_reset_vlan {args} {

    array set param $args
    global env

    set config_mode $env(DUT_CONFIG_MODE)
    set session_handler $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "ptp_ha::dut_reset_vlan : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_reset_vlan : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Delete VLAN ($vlan_id) and disassociate port ($port_num)"

    if {![ptp_ha::${config_mode}_reset_vlan\
                           -session_handler  $session_handler\
                           -vlan_id          $vlan_id\
                           -port_num         $port_num]} {

        ERRLOG -msg "ptp_ha::dut_reset_vlan : Unable to delete VLAN and disassociate port on DUT"
        return 0
    }

    return 1
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_reset_vlan_priority                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID                                     #
#                                  (e.g., 100)                                 #
#                                                                              #
#  vlan_priority     : (Optional)  VLAN priority                               #
#                                                                              #
#  port_num          : (Mandatory) Port which has to be associated to VLAN ID. #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If VLAN priority is reset on the VLAN ID  #
#                                    on the DUT).                              #
#                                                                              #
#                      0 on Failure (If VLAN priority could not reset on the   #
#                                    VLAN ID on the DUT).                      #
#                                                                              #
#  DEFINITION        : This function resets VLAN priority on the VLAN ID on the#
#                      DUT.                                                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_reset_vlan_priority\                          #
#                             -vlan_id                  $vlan_id\              #
#                             -vlan_priority            $vlan_priority\        #
#                             -port_num                 $port_num              #
#                                                                              #
################################################################################

proc ptp_ha::dut_reset_vlan_priority {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode     $env(DUT_CONFIG_MODE)
    set session_handler $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "ptp_ha::dut_reset_vlan_priority : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if {[info exists param(-vlan_priority)]} {
        set vlan_priority $param(-vlan_priority)
    } else {
        set vlan_priority $ptp_ha_env(DUT_PTP_HA_VLAN_PRIORITY)
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_reset_vlan_priority : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset VLAN priority ($vlan_priority) to VLAN ($vlan_id)"

    if {![ptp_ha::${config_mode}_reset_vlan_priority\
                    -session_handler  $session_handler\
                    -vlan_id          $vlan_id\
                    -vlan_priority    $vlan_priority\
                    -port_num         $port_num]} {

        ERRLOG -msg "ptp_ha::dut_reset_vlan_priority : Unable to reset priority to VLAN on DUT"
        return 0
    }

    return 1
}


#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_global_ptp_disable                          #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is disabled on the DUT).           #
#                                                                              #
#                      0 on Failure (If PTP could not be disabled on the DUT). #
#                                                                              #
#  DEFINITION        : This function disables PTP on the DUT.                  #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_global_ptp_disable                            #
#                                                                              #
################################################################################

proc ptp_ha::dut_global_ptp_disable {} {

    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    LOG -level 2 -msg "\t Disable PTP globally on DUT"

    if {![ptp_ha::${config_mode}_global_ptp_disable\
                           -session_handler  $session_handler]} {

        ERRLOG -msg "ptp_ha::dut_global_ptp_disable : Unable to disable PTP globally"
        return 0
    }

    return 1
}


#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_reset_communication_mode                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Optional)  Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  ptp_version       : (optional)  PTP version.                                #
#                                  (Default: 2)                                #
#                                                                              #
#  communication_mode : (Optional) Communication mode                          #
#                                  (e.g., "Unicast"|"Multicast")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If communication mode is reset on the     #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If communication mode could not be        #
#                                    reset on the specified port at the DUT).  #
#                                                                              #
#  DEFINITION        : This function resets the communication mode on a        #
#                      specific port of the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_reset_communication_mode\                     #
#                             -port_num                $port_num\              #
#                             -clock_mode              $clock_mode\            #
#                             -ptp_version             $ptp_version\           #
#                             -communication_mode      $communication_mode     #
#                                                                              #
################################################################################

proc ptp_ha::dut_reset_communication_mode {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_reset_communication_mode : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        set clock_mode $ptp_ha_env(DUT_PTP_HA_DEVCIE_TYPE)
    }

    if {[info exists param(-communication_mode)]} {
        set communication_mode $param(-communication_mode)
    } else {
        set communication_mode $ptp_ha_env(DUT_PTP_HA_COM_MODEL)
    }

    if {[info exists param(-ptp_version)]} {
        set ptp_version $param(-ptp_version)
    } else {
        set ptp_version $::ptp_version
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset communication mode ($communication_mode) on port ($port_num)"

    if {![ptp_ha::${config_mode}_reset_communication_mode\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -clock_mode       $clock_mode\
                           -ptp_version      $ptp_version\
                           -communication_mode   $communication_mode]} {

        ERRLOG -msg "ptp_ha::dut_reset_communication_mode :\
                    Unable to reset PTP communication mode on specific port of the DUT"
        return 0
    }

    return 1
}


#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_reset_domain                                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  clock_mode        : (Optional)  Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  domain            : (Optional)  Domain number.                              #
#                                  (e.g., 0..255)                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If domain number is reset on the DUT).    #
#                                                                              #
#                      0 on Failure (If domain number could not able to reset  #
#                                    on DUT).                                  #
#                                                                              #
#  DEFINITION        : This function resets domain number on the DUT.          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_reset_domain\                                 #
#                             -clock_mode      $clock_mode\                    #
#                             -domain          $domain                         #
#                                                                              #
################################################################################

proc ptp_ha::dut_reset_domain {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        set clock_mode $ptp_ha_env(DUT_PTP_HA_DEVCIE_TYPE)
    }

    if {[info exists param(-domain)]} {
        set domain $param(-domain)
    } else {
        set domain $::ptp_dut_default_domain_number
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset defaultDS.domainNumber ($domain) on DUT"

    if {![ptp_ha::${config_mode}_reset_domain\
                           -session_handler  $session_handler\
                           -clock_mode       $clock_mode\
                           -domain           $domain]} {

        ERRLOG -msg "ptp_ha::dut_reset_domain : Unable to reset domain on DUT"
        return 0
    }

    return 1
}


#8.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_reset_network_protocol                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Optional)  clock_mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  network_protocol  : (Optional)  Network protocol.                           #
#                                  (e.g., "IEEE 802.3"|"UDP/IPv4")             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If network protocol is reset on specified #
#                                    port at the DUT).                         #
#                                                                              #
#                      0 on Failure (If network protocol could not be reset    #
#                                    on specified port at the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets the network protocol on a specific #
#                      port of the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                     ptp_ha::dut_reset_network_protocol\                      #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -network_protocol $network_protocol              #
#                                                                              #
################################################################################

proc ptp_ha::dut_reset_network_protocol {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_reset_network_protocol : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        set clock_mode $ptp_ha_env(DUT_PTP_HA_DEVCIE_TYPE)
    }

    if {[info exists param(-network_protocol)]} {
        set network_protocol $param(-network_protocol)
    } else {
        set network_protocol $ptp_ha_env(DUT_PTP_HA_TRANS_TYPE)
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset networkProtocol ($network_protocol) on port ($port_num)"

    if {![ptp_ha::${config_mode}_reset_network_protocol\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -clock_mode       $clock_mode\
                           -network_protocol $network_protocol]} {

        ERRLOG -msg "ptp_ha::dut_reset_network_protocol : Unable to set network protocol\
                    on specified port at the DUT"
        return 0
    }

    return 1
}


#9.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_reset_clock_mode                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Optional)  Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP clock mode is reset on specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If PTP clock mode could not be reset on   #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function resets clock mode on a specific port of   #
#                      DUT.                                                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_reset_clock_mode\                             #
#                             -port_num        $port_num\                      #
#                             -clock_mode      $clock_mode                     #
#                                                                              #
################################################################################

proc ptp_ha::dut_reset_clock_mode {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_reset_clock_mode : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        set clock_mode $ptp_ha_env(DUT_PTP_HA_DEVCIE_TYPE)
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset clock mode ($clock_mode) on port ($port_num)"

    if {![ptp_ha::${config_mode}_reset_clock_mode\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -clock_mode       $clock_mode]} {

        ERRLOG -msg "ptp_ha::dut_reset_clock_mode : Unable to reset clock mode\
                    on specified port on DUT"
        return 0
    }

    return 1
}


#10.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_reset_clock_step                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Optional)  Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  clock_step        : (Optional)  Clock step.                                 #
#                                  (e.g., "One-step"|"Two-step")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If clock-step is reset on the specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If clock-step could not be able to reset  #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets the clock-step on a specific port  #
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_reset_clock_step\                             #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -clock_step       $clock_step                    #
#                                                                              #
################################################################################

proc ptp_ha::dut_reset_clock_step {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_reset_clock_step : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        set clock_mode $ptp_ha_env(DUT_PTP_HA_DEVCIE_TYPE)
    }

    if {[info exists param(-clock_step)]} {
        set clock_step $param(-clock_step)
    } else {
        set clock_step $::ptp_dut_clock_step
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset clock step ($clock_step) on port ($port_num)"

    if {![ptp_ha::${config_mode}_reset_clock_step\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -clock_mode       $clock_mode\
                           -clock_step       $clock_step]} {

        ERRLOG -msg "ptp_ha::dut_reset_clock_step : Unable to reset clock step\
                    on specified port on DUT"
        return 0
    }

    return 1
}


#11.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_reset_delay_mechanism                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Optional)  Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  delay_mechanism   : (Mandatory) Delay mechanism.                            #
#                                  (e.g., e2e|p2p|disable)                     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If delay mechanism is reset on specified  #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If delay mechanism could not be reset on  #
#                                    specified PTP port of the DUT).           #
#                                                                              #
#  DEFINITION        : This function resets the delay mechanism to a specific  #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_reset_delay_mechanism\                        #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -delay_mechanism  $delay_mechanism               #
#                                                                              #
################################################################################

proc ptp_ha::dut_reset_delay_mechanism {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_reset_delay_mechanism : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        set clock_mode $ptp_ha_env(DUT_PTP_HA_DEVCIE_TYPE)
    }

    if {[info exists param(-delay_mechanism)]} {
        set delay_mechanism $param(-delay_mechanism)
    } else {
        ERRLOG -msg "ptp_ha::dut_reset_delay_mechanism : Missing Argument    -> delay_mechanism"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset portDS.delayMechanism ($delay_mechanism) on port ($port_num)"

    if {![ptp_ha::${config_mode}_reset_delay_mechanism\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -clock_mode       $clock_mode\
                           -delay_mechanism  $delay_mechanism]} {

        ERRLOG -msg "ptp_ha::dut_reset_delay_mechanism : Unable to reset delay mechanism\
                    on specified port on DUT"
        return 0
    }

    return 1
}


#12.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_reset_priority1                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  clock_mode        : (Optional)  Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority1         : (Optional)  Prority1                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority1 is reset on the DUT).        #
#                                                                              #
#                      0 on Failure (If priority1 could not be able to reset on#
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function resets the priority1 on the DUT.          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_reset_priority1\                              #
#                             -clock_mode      $clock_mode\                    #
#                             -priority1       $priority1                      #
#                                                                              #
################################################################################

proc ptp_ha::dut_reset_priority1 {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        set clock_mode $ptp_ha_env(DUT_PTP_HA_DEVCIE_TYPE)
    }

    if {[info exists param(-priority1)]} {
        set priority1  $param(-priority1)
    } else {
        set priority1  $ptp_ha_env(DUT_PTP_HA_PRIORITY1)
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset defaultDS.priority1 ($priority1) on DUT"

    if {![ptp_ha::${config_mode}_reset_priority1\
                           -session_handler  $session_handler\
                           -clock_mode       $clock_mode\
                           -priority1        $priority1]} {

        ERRLOG  -msg "ptp_ha::dut_reset_priority1: Unable to reset priority1 on the DUT"
        return 0
    }

    return 1
}


#13.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_reset_priority2                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  clock_mode        : (Optional)  Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority2         : (Optional)  Prority2                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority2 is reset on the DUT).        #
#                                                                              #
#                      0 on Failure (If priority2 could not be able to reset on#
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function resets the priority2 on the DUT.          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_reset_priority2\                              #
#                             -clock_mode      $clock_mode\                    #
#                             -priority2       $priority2                      #
#                                                                              #
################################################################################

proc ptp_ha::dut_reset_priority2 {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        set clock_mode $ptp_ha_env(DUT_PTP_HA_DEVCIE_TYPE)
    }

    if {[info exists param(-priority2)]} {
        set priority2  $param(-priority2)
    } else {
        set priority2  $ptp_ha_env(DUT_PTP_HA_PRIORITY2)
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset defaultDS.priority2 ($priority2) on DUT"

    if {![ptp_ha::${config_mode}_reset_priority2\
                           -session_handler  $session_handler\
                           -clock_mode       $clock_mode\
                           -priority2        $priority2]} {

        ERRLOG  -msg "ptp_ha::dut_reset_priority2: Unable to reset priority2 on the DUT"
        return 0
    }

    return 1
}


#14.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_tcr_rcr_cr_disable                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If peerTxCoherentIsRequired,              #
#                                    peerRxCoherentIsRequired and              #
#                                    peerCongruentIsRequired is disabled on the#
#                                    DUT).                                     #
#                                                                              #
#                      0 on Failure (If peerTxCoherentIsRequired,              #
#                                    peerRxCoherentIsRequired and              #
#                                    peerCongruentIsRequired could not be      #
#                                    disabled on the DUT).                     #
#                                                                              #
#                                                                              #
#  DEFINITION        : This function disables peerTxCoherentIsRequired,        #
#                      peerRxCoherentIsRequired and peerCongruentIsRequired on #
#                      the DUT.                                                #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_tcr_rcr_cr_disable\                           #
#                             -port_num                 $port_num              #
#                                                                              #
################################################################################

proc ptp_ha::dut_tcr_rcr_cr_disable {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_tcr_rcr_cr_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Disable L1SyncBasicPortDS.peerTxCoherentIsRequired,\
                          L1SyncBasicPortDS.peerRxCoherentIsRequired\
                          and L1SyncBasicPortDS.peerCongruentIsRequired on port ($port_num)"

    if {![ptp_ha::${config_mode}_tcr_rcr_cr_disable\
                           -session_handler  $session_handler\
                           -port_num         $port_num]} {

        ERRLOG  -msg "ptp_ha::dut_tcr_rcr_cr_disable : Unable to disable\
                      L1SyncBasicPortDS.peerTxCoherentIsRequired,\
                      L1SyncBasicPortDS.peerRxCoherentIsRequired and\
                      L1SyncBasicPortDS.peerCongruentIsRequired on DUT"
        return 0
    }

    return 1
}


#15.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_l1sync_disable                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If L1Sync option is disabled on the       #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If L1Sync option could not be disabled on #
#                                    the specified port of the DUT).           #
#                                                                              #
#  DEFINITION        : This function disables L1Sync option on a specific port #
#                      at the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_l1sync_disable\                               #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::dut_l1sync_disable {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_l1sync_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Disable L1SyncBasicPortDS.L1SyncEnabled on port ($port_num)"

    if {![ptp_ha::${config_mode}_l1sync_disable\
                           -session_handler  $session_handler\
                           -port_num         $port_num]} {

        ERRLOG -msg "ptp_ha::dut_l1sync_disable : Unable to disable L1SyncBasicPortDS.L1SyncEnabled\
                    on specified port on DUT"
        return 0
    }

    return 1
}


#16.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_l1sync_opt_params_disable                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If extended format of L1Sync TLV is       #
#                                    disabled on the specified port at the DUT)#
#                                                                              #
#                      0 on Failure (If extended format of L1Sync TLV could not#
#                                    be disabled on the specified port at the  #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function disables extended format of L1Sync TLV on #
#                      the specified port at the DUT.                          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_l1sync_opt_params_disable\                    #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::dut_l1sync_opt_params_disable {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_l1sync_opt_params_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Disable L1SyncBasicPortDS.optParamsEnabled on port ($port_num)"

    if {![ptp_ha::${config_mode}_l1sync_opt_params_disable\
                           -session_handler  $session_handler\
                           -port_num         $port_num]} {

        ERRLOG -msg "ptp_ha::dut_l1sync_opt_params_disable : Unable to disable\
                     L1SyncBasicPortDS.optParamsEnabled on specified port on DUT"
        return 0
    }

    return 1
}

#17.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_slaveonly_disable                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If slave-only is disabled on the          #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If slave-only could not be disabled on the#
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function disables slave-only on the specified port #
#                      at the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_slaveonly_disable\                            #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::dut_slaveonly_disable {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_slaveonly_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Disable defaultDS.slaveOnly"

    if {![ptp_ha::${config_mode}_slaveonly_disable\
                           -session_handler  $session_handler\
                           -port_num         $port_num]} {

        ERRLOG -msg "ptp_ha::dut_slaveonly_disable : Unable to disable defaultDS.slaveOnly\n"
        return 0
    }

    return 1
}


#18.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_masteronly_disable                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If master-only is disabled on the         #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If master-only could not be disabled on   #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function disables master-only on a specific port   #
#                      at the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_masteronly_disable\                           #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::dut_masteronly_disable {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_masteronly_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Disable portDS.masterOnly on port ($port_num)"

    if {![ptp_ha::${config_mode}_masteronly_disable\
                           -session_handler  $session_handler\
                           -port_num         $port_num]} {

        ERRLOG -msg "ptp_ha::dut_masteronly_disable : Unable to disable portDS.masterOnly\
                    on specified port on DUT"
        return 0
    }

    return 1
}


#19.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_reset_announce_interval                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  announce_interval : (Optional)  announceInterval.                           #
#                                  (e.g., 1|2|4|8|16)                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If announceInterval value is reset on     #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If announceInterval value could not be    #
#                                    reset on specified port of the DUT).      #
#                                                                              #
#  DEFINITION        : This function resets announceInterval value to a        #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_reset_announce_interval\                      #
#                             -port_num                 $port_num\             #
#                             -announce_interval        $announce_interval     #
#                                                                              #
################################################################################

proc ptp_ha::dut_reset_announce_interval {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_reset_announce_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-announce_interval)]} {
        set announce_interval $param(-announce_interval)
    } else {
        set announce_interval $ptp_ha_env(DUT_PTP_HA_ANNOUNCE_INTERVAL)
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset portDS.logAnnounceInterval ($announce_interval) on port ($port_num)"

    if {![ptp_ha::${config_mode}_reset_announce_interval\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -announce_interval $announce_interval]} {

        ERRLOG -msg "ptp_ha::dut_reset_announce_interval : Unable to reset portDS.logAnnounceInterval\
                    on specified port on DUT"
        return 0
    }

    return 1
}


#20.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_reset_announce_timeout                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  announce_timeout  : (Optional)  announceReceiptTimeout.                     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If announceReceiptTimeout value is reset  #
#                                    on specified port of the DUT).            #
#                                                                              #
#                      0 on Failure (If announceReceiptTimeout value could not #
#                                    be reset on specified port of the DUT).   #
#                                                                              #
#  DEFINITION        : This function resets announceReceiptTimeout value to a  #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_reset_announce_timeout\                       #
#                             -port_num                 $port_num\             #
#                             -announce_timeout         $announce_timeout      #
#                                                                              #
################################################################################

proc ptp_ha::dut_reset_announce_timeout {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_reset_announce_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-announce_timeout)]} {
        set announce_timeout $param(-announce_timeout)
    } else {
        set announce_timeout $ptp_ha_env(DUT_PTP_HA_ANNOUNCE_TIMEOUT)
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset portDS.announceReceiptTimeout ($announce_timeout) on port ($port_num)"

    if {![ptp_ha::${config_mode}_reset_announce_timeout\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -announce_timeout $announce_timeout]} {

        ERRLOG -msg "ptp_ha::dut_reset_announce_timeout : Unable to reset portDS.announceReceiptTimeout\
                     on specified port on DUT"
        return 0
    }

    return 1
}


#21.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_reset_sync_interval                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  sync_interval     : (Optional)  logSyncInterval                             #
#                                  (e.g., 3)                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If logSyncInterval value is reset on      #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If logSyncInterval value could not be     #
#                                    reset on specified port of the DUT).      #
#                                                                              #
#  DEFINITION        : This function resets logSyncInterval value to a specific#
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_reset_sync_interval\                          #
#                          -port_num              $port_num\                   #
#                          -sync_interval         $sync_interval               #
#                                                                              #
################################################################################

proc ptp_ha::dut_reset_sync_interval {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter  0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_reset_sync_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-sync_interval)]} {
        set sync_interval $param(-sync_interval)
    } else {
        set sync_interval $ptp_ha_env(DUT_PTP_HA_SYNC_INTERVAL)
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset portDS.logSyncInterval ($sync_interval) on port ($port_num)"

    if {![ptp_ha::${config_mode}_reset_sync_interval\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -sync_interval    $sync_interval]} {

        ERRLOG -msg "ptp_ha::dut_reset_sync_interval : Unable to reset portDS.logSyncInterval\
                     on specified port on DUT"
        return 0
    }

    return 1
}


#22.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_reset_delayreq_interval                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  delayreq_interval : (Optional)  MinDelayReqInterval.                        #
#                                  (e.g., 1|2|4|8|16)                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If MinDelayReqInterval is reset on        #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If MinDelayReqInterval could not be reset #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets the MinDelayReqInterval to the     #
#                      specific PTP port on the DUT.                           #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_reset_delayreq_interval\                      #
#                             -port_num                 $port_num\             #
#                             -delayreq_interval        $delayreq_interval     #
#                                                                              #
################################################################################

proc ptp_ha::dut_reset_delayreq_interval {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG  -msg "ptp_ha::dut_reset_delayreq_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-delayreq_interval)]} {
        set delayreq_interval $param(-delayreq_interval)
    } else {
        set delayreq_interval $ptp_ha_env(DUT_PTP_HA_DELAY_REQ_INTERVAL)
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset portDS.logMinDelayReqInterval ($delayreq_interval) on port ($port_num)"

    if {![ptp_ha::${config_mode}_reset_delayreq_interval\
                           -session_handler    $session_handler\
                           -port_num           $port_num\
                           -delayreq_interval  $delayreq_interval]} {

        ERRLOG -msg "ptp_ha::dut_reset_delayreq_interval : Unable to reset portDS.logMinDelayReqInterval\
                    on specified port on DUT"
        return 0
    }

    return 1
}


#23.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_reset_pdelayreq_interval                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  pdelayreq_interval: (Optional)  minPdelayReqInterval.                       #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If minPdelayReqInterval is reset on       #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If minPdelayReqInterval could not be reset#
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets the minPdelayReqInterval to the    #
#                      specific PTP port on the DUT.                           #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_reset_pdelayreq_interval\                     #
#                             -port_num                 $port_num\             #
#                             -pdelayreq_interval       $pdelayreq_interval    #
#                                                                              #
################################################################################

proc ptp_ha::dut_reset_pdelayreq_interval {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_reset_pdelayreq_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-pdelayreq_interval)]} {
        set pdelayreq_interval $param(-pdelayreq_interval)
    } else {
        set pdelayreq_interval $ptp_ha_env(DUT_PTP_HA_PDELAY_REQ_INTERVAL)
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset portDS.logMinPdelayReqInterval ($pdelayreq_interval) on port ($port_num)"

    if {![ptp_ha::${config_mode}_reset_pdelayreq_interval\
                           -session_handler    $session_handler\
                           -port_num           $port_num\
                           -pdelayreq_interval $pdelayreq_interval]} {

        ERRLOG -msg "ptp_ha::dut_reset_pdelayreq_interval : Unable to reset portDS.logMinPdelayReqInterval\
                    on specified port on DUT\n"
        return 0
    }

    return 1
}


#24.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_reset_l1sync_interval                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  l1sync_interval   : (Optional)  logL1SyncInterval.                          #
#                                  (e.g.,3 )                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If logL1SyncInterval is reset on specified#
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If logL1SyncInterval could not be reset on#
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function resets the logL1SyncInterval to a specific#
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_reset_l1sync_interval\                        #
#                          -port_num              $port_num\                   #
#                          -l1sync_interval       $l1sync_interval             #
#                                                                              #
################################################################################

proc ptp_ha::dut_reset_l1sync_interval {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_reset_l1sync_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-l1sync_interval)]} {
        set l1sync_interval $param(-l1sync_interval)
    } else {
        set l1sync_interval $ptp_ha_env(DUT_PTP_HA_L1SYNC_INTERVAL)
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset L1SyncBasicPortDS.logL1SyncInterval ($l1sync_interval)\
                          on port ($port_num)"

    if {![ptp_ha::${config_mode}_reset_l1sync_interval\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -l1sync_interval  $l1sync_interval]} {

        ERRLOG -msg "ptp_ha::dut_reset_l1sync_interval : Unable to reset L1SyncBasicPortDS.logL1SyncInterval\
                     on specified port on DUT"
        return 0
    }

    return 1
}


#25.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_reset_l1sync_timeout                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  l1sync_timeout    : (Optional)  L1SyncReceiptTimeout.                       #
#                                  (e.g., 3)                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If L1SyncReceiptTimeout is reset on       #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If L1SyncReceiptTimeout could not be      #
#                                    reset on specified port of the DUT).      #
#                                                                              #
#  DEFINITION        : This function resets L1SyncReceiptTimeout to a specific #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_reset_l1sync_timeout\                         #
#                          -port_num              $port_num\                   #
#                          -l1sync_timeout        $l1sync_timeout              #
#                                                                              #
################################################################################

proc ptp_ha::dut_reset_l1sync_timeout {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_reset_l1sync_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-l1sync_timeout)]} {
        set l1sync_timeout $param(-l1sync_timeout)
    } else {
        set l1sync_timeout $ptp_ha_env(DUT_PTP_HA_L1SYNC_TIMEOUT)
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset L1SyncBasicPortDS.L1SyncReceiptTimeout ($l1sync_timeout)\
                          on port ($port_num)"

    if {![ptp_ha::${config_mode}_reset_l1sync_timeout\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -l1sync_timeout   $l1sync_timeout]} {

        ERRLOG -msg "ptp_ha::dut_reset_l1sync_timeout : Unable to reset L1SyncBasicPortDS.L1SyncReceiptTimeout\
                     on specified port on DUT"
        return 0
    }

    return 1
}


#26.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_reset_egress_latency                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Optional)  timestampCorrectionPortDS.egressLatency.    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If timestampCorrectionPortDS.egressLatency#
#                                    is reset on specified port of the DUT).   #
#                                                                              #
#                      0 on Failure (If timestampCorrectionPortDS.egressLatency#
#                                    could not be reset on specified port of   #
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function resets timestampCorrectionPortDS.         #
#                      egressLatency to a specific port on the DUT.            #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_reset_egress_latency\                         #
#                          -port_num              $port_num\                   #
#                          -latency               $latency                     #
#                                                                              #
################################################################################

proc ptp_ha::dut_reset_egress_latency {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_reset_egress_latency : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-latency)]} {
        set latency $param(-latency)
    } else {
        set latency $ptp_ha_env(DUT_PTP_HA_EGRESS_LATENCY)
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset timestampCorrectionPortDS.egressLatency ($latency ns) on port ($port_num)"

    if {![ptp_ha::${config_mode}_reset_egress_latency\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -latency          $latency]} {

        ERRLOG -msg "ptp_ha::dut_reset_egress_latency : Unable to reset timestampCorrectionPortDS.egressLatency\
                    on specified port on DUT"
        return 0
    }

    return 1
}


#27.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_reset_ingress_latency                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Optional)  timestampCorrectionPortDS.ingressLatency.   #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If timestampCorrectionPortDS.             #
#                                    ingressLatency is reset on specified port #
#                                    of the DUT).                              #
#                                                                              #
#                      0 on Failure (If timestampCorrectionPortDS.             #
#                                    ingressLatency could not be reset on      #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function resets timestampCorrectionPortDS.         #
#                      ingressLatency to a specific port on the DUT.           #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_reset_ingress_latency\                        #
#                          -port_num              $port_num\                   #
#                          -latency               $latency                     #
#                                                                              #
################################################################################

proc ptp_ha::dut_reset_ingress_latency {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_reset_ingress_latency : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-latency)]} {
        set latency $param(-latency)
    } else {
        set latency $ptp_ha_env(DUT_PTP_HA_INGRESS_LATENCY)
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset timestampCorrectionPortDS.ingressLatency ($latency ns) on port ($port_num)"

    if {![ptp_ha::${config_mode}_reset_ingress_latency\
                           -session_handler  $session_handler\
                           -port_num         $port_num\
                           -latency          $latency]} {

        ERRLOG -msg "ptp_ha::dut_reset_ingress_latency : Unable to reset timestampCorrectionPortDS.ingressLatency\
                     on specified port on DUT"
        return 0
    }

    return 1
}


#28.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_reset_constant_asymmetry                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  constant_asymmetry :(Optional)  asymmetryCorrectionPortDS.constantAsymmetry.#
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If asymmetryCorrectionPortDS.             #
#                                    constantAsymmetry is reset on specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If asymmetryCorrectionPortDS.             #
#                                    constantAsymmetry could not be reset on   #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function resets asymmetryCorrectionPortDS.         #
#                      constantAsymmetry to a specific port on the DUT.        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_reset_constant_asymmetry\                     #
#                          -port_num              $port_num\                   #
#                          -constant_asymmetry    $constant_asymmetry          #
#                                                                              #
################################################################################

proc ptp_ha::dut_reset_constant_asymmetry {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_reset_constant_asymmetry : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-constant_asymmetry)]} {
        set constant_asymmetry $param(-constant_asymmetry)
    } else {
        set constant_asymmetry $ptp_ha_env(DUT_PTP_HA_CONSTANT_ASYMMETRY)
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset asymmetryCorrectionPortDS.constantAsymmetry ($constant_asymmetry ns) on port ($port_num)"

    if {![ptp_ha::${config_mode}_reset_constant_asymmetry\
                    -session_handler      $session_handler\
                    -port_num             $port_num\
                    -constant_asymmetry   $constant_asymmetry]} {

        ERRLOG -msg "ptp_ha::dut_reset_l1sync_timeout : Unable to reset asymmetryCorrectionPortDS.constantAsymmetry\
        on specified port on DUT"
        return 0
    }

    return 1
}


#29.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_reset_scaled_delay_coefficient              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  delay_coefficient : (Optional)  asymmetryCorrectionPortDS.                  #
#                                  scaledDelayCoefficient .                    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If asymmetryCorrectionPortDS.             #
#                                    scaledDelayCoefficient is reset on        #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If asymmetryCorrectionPortDS.             #
#                                    scaledDelayCoefficient could not be reset #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets asymmetryCorrectionPortDS.         #
#                      scaledDelayCoefficient to a specific port on the DUT.   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_reset_scaled_delay_coefficient\               #
#                          -port_num              $port_num\                   #
#                          -delay_coefficient     $delay_coefficient           #
#                                                                              #
################################################################################

proc ptp_ha::dut_reset_scaled_delay_coefficient {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_reset_scaled_delay_coefficient : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-delay_coefficient)]} {
        set delay_coefficient $param(-delay_coefficient)
    } else {
        set delay_coefficient $ptp_ha_env(DUT_PTP_HA_SCALED_DELAY_COEFFICIENT)
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset asymmetryCorrectionPortDS.scaledDelayCoefficient\
                     ($delay_coefficient (fractional number)) on port ($port_num)"

    if {![ptp_ha::${config_mode}_reset_scaled_delay_coefficient\
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -delay_coefficient  $delay_coefficient]} {
    
        ERRLOG -msg "ptp_ha::dut_reset_scaled_delay_coefficient : Unable to reset\
                     asymmetryCorrectionPortDS.scaledDelayCoefficient\
                     on specified port on DUT"
        return 0
    }

    return 1
}


#30.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_reset_asymmetry_correction                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If asymmetryCorrectionPortDS.enable is    #
#                                    reset on specified port of the DUT).      #
#                                                                              #
#                      0 on Failure (If asymmetryCorrectionPortDS.enable could #
#                                    not be reset on specified port of the DUT)#
#                                                                              #
#  DEFINITION        : This function resets the asymmetryCorrectionPortDS.     #
#                      enable to the specific PTP port on the DUT.             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_reset_asymmetry_correction\                   #
#                        -port_num          $port_num                          #
#                                                                              #
################################################################################

proc ptp_ha::dut_reset_asymmetry_correction {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_reset_asymmetry_correction : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset asymmetryCorrectionPortDS.enable on port ($port_num)"

    if {![ptp_ha::${config_mode}_reset_asymmetry_correction\
                    -session_handler  $session_handler\
                    -port_num         $port_num]} { 
    
        ERRLOG -msg "ptp_ha::dut_reset_asymmetry_correction : Unable to reset asymmetryCorrectionPortDS.enable\
                     on specified port on DUT"
        return 0
    }

    return 1
}


#31.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_external_port_config_disable                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If external port configuration option is  #
#                                    disabled on the specified port at the DUT)#
#                                                                              #
#                      0 on Failure (If external port configuration option     #
#                                    could not be disabled on the specified    #
#                                    port at the DUT).                         #
#                                                                              #
#  DEFINITION        : This function disables external port configuration      #
#                      option on a specific port at the DUT.                   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_external_port_config_disable\                 #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::dut_external_port_config_disable {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_external_port_config_disable: Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Disable defaultDS.externalPortConfigurationEnabled on port ($port_num)"

    if {![ptp_ha::${config_mode}_external_port_config_disable\
                           -session_handler  $session_handler\
                           -port_num         $port_num]} {

        ERRLOG -msg "ptp_ha::dut_external_port_config_disable : Unable to disable\
                     defaultDS.externalPortConfigurationEnabled on specified port on DUT"
        return 0
    }

    return 1
}


#32.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_reset_epc_desired_state                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  desired_state     : (Optional)  externalPortConfigurationPortDS.desiredState#
#                                  (e.g., "INITIALIZING"|"FAULTY"|"DISABLED"|  #
#                                         "LISTENING"|"PRE_MASTER"|"MASTER"|   #
#                                         "PASSIVE"|"UNCALIBRATED"|"SLAVE"     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given externalPortConfigurationPortDS. #
#                                    desiredState is reset on specified port of#
#                                    the DUT).                                 #
#                                                                              #
#                      0 on Failure (If given externalPortConfigurationPortDS. #
#                                    desiredState could not be reset on        #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function resets given                              #
#                      externalPortConfigurationPortDS desiredState to a       #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_reset_scaled_delay_coefficient\               #
#                          -port_num              $port_num\                   #
#                          -delay_coefficient     $delay_coefficient           #
#                                                                              #
################################################################################

proc ptp_ha::dut_reset_epc_desired_state {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_reset_epc_desired_state : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-desired_state)]} {
        set desired_state $param(-desired_state)
    } else {
        set desired_state $ptp_ha_env(DUT_PTP_HA_DEFAULT_EPC_DESIRED_STATE)
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset externalPortConfigurationPortDS.desiredState ($desired_state)\
                          on port ($port_num)"

    if {![ptp_ha::${config_mode}_reset_epc_desired_state\
                           -session_handler    $session_handler\
                           -port_num           $port_num\
                           -desired_state      $desired_state]} {

        ERRLOG -msg "ptp_ha::dut_reset_epc_desired_state : Unable to reset\
                    externalPortConfigurationPortDS.desiredState\
                    on specified port on DUT"
        return 0
    }

    return 1
}


#33.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_reset_ipv4_address                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  ip_address        : (Mandatory) IPv4 address.                               #
#                                  (e.g., 192.168.200.1)                       #
#                                                                              #
#  net_mask          : (Mandatory) Subnet mask.                                #
#                                  (e.g., 255.255.255.0)                       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID.                                    #
#                                  (e.g., 100)                                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given IP address is reset on specified #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given IP address could not be reset on #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function resets given IP address to a specific port#
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_reset_ipv4_address\                           #
#                          -port_num              $port_num\                   #
#                          -ip_address            $ip_address\                 #
#                          -net_mask              $net_mask\                   #
#                          -vlan_id               $vlan_id                     #
#                                                                              #
################################################################################

proc ptp_ha::dut_reset_ipv4_address {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_reset_ipv4_address : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-ip_address)]} {
        set ip_address $param(-ip_address)
    } else {
        ERRLOG -msg "ptp_ha::dut_reset_ipv4_address : Missing Argument    -> ip_address"
        incr missedArgCounter
    }

    if {[info exists param(-net_mask)]} {
        set net_mask $param(-net_mask)
    } else {
        ERRLOG -msg "ptp_ha::dut_reset_ipv4_address : Missing Argument    -> net_mask"
        incr missedArgCounter
    }

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "ptp_ha::dut_reset_ipv4_address : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Reset IP address ($ip_address) on port ($port_num)"

    if {![ptp_ha::${config_mode}_reset_ipv4_address\
                    -session_handler    $session_handler\
                    -port_num           $port_num\
                    -ip_address         $ip_address\
                    -net_mask           $net_mask\
                    -vlan_id            $vlan_id]} {

        ERRLOG -msg "ptp_ha::dut_reset_ipv4_address : Unable to set\
                    IP address on specified port on DUT"
        return 0
    }

    return 1
}


#34.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_timestamp_corrected_tx_disable              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If L1SyncOptParamsPortDS.                 #
#                                    timestampsCorrectedTx option is           #
#                                    disabled on the specified port at the DUT)#
#                                                                              #
#                      0 on Failure (If L1SyncOptParamsPortDS.                 #
#                                    timestampsCorrectedTx option could not be #
#                                    disabled on the specified port at the DUT)#
#                                                                              #
#  DEFINITION        : This function disables L1SyncOptParamsPortDS.           #
#                      timestampsCorrectedTx option on a specific port at the  #
#                      DUT.                                                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_timestamp_corrected_tx_disable\               #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::dut_timestamp_corrected_tx_disable {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_timestamp_corrected_tx_disable: Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Disable L1SyncOptParamsPortDS.timestampsCorrectedTx option on port ($port_num)"

    if {![ptp_ha::${config_mode}_timestamp_corrected_tx_disable\
                    -session_handler  $session_handler\
                    -port_num         $port_num]} {

        ERRLOG -msg "ptp_ha::dut_timestamp_corrected_tx_disable: Unable to disable\
                     L1SyncOptParamsPortDS.timestampsCorrectedTx configuration option\
                     on specified port on DUT\n"

        return 0
    }

    return 1
}


################################################################################
#                           DUT CHECK FUNCTIONS                                #
################################################################################

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_check_l1sync_state                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  l1sync_state      : (Mandatory) L1 Sync State.                              #
#                                  (e.g., "DISABLED"|"IDLE"|"LINK_ALIVE"|      #
#                                         "CONFIG_MATCH"|"L1_SYNC_UP")         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given L1 Sync State is verified for the#
#                                    specified port).                          #
#                                                                              #
#                      0 on Failure (If given L1 Sync State could not be       #
#                                    verified for the specified port).         #
#                                                                              #
#  DEFINITION        : This function verifies the given L1 Sync State for a    #
#                      specific port.                                          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_check_l1sync_state\                           #
#                          -port_num           $port_num\                      #
#                          -l1sync_state       $l1sync_state                   #
#                                                                              #
################################################################################

proc ptp_ha::dut_check_l1sync_state {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_check_l1sync_state : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-l1sync_state)]} {
        set l1sync_state $param(-l1sync_state)
    } else {
        ERRLOG -msg "ptp_ha::dut_check_l1sync_state : Missing Argument    -> l1sync_state"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Check L1SyncBasicPortDS.L1SyncState ($l1sync_state)\
                          of port ($port_num)"

    if {![ptp_ha::${config_mode}_check_l1sync_state\
                    -session_handler  $session_handler\
                    -port_num         $port_num\
                    -l1sync_state     $l1sync_state]} {
    
        ERRLOG -msg "ptp_ha::dut_check_l1sync_state : Unable to check L1SyncBasicPortDS.L1SyncState\
                     of specified port in DUT"
        return 0
    }

    return 1
}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_check_ptp_port_state                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  port_state        : (Mandatory) portState.                                  #
#                                  (e.g., "INITIALIZING"|"FAULTY"|"DISABLED"|  #
#                                         "LISTENING"|"PRE_MASTER"|"MASTER"|   #
#                                         "PASSIVE"|"UNCALIBRATED"|"SLAVE"     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given portState is verified for the    #
#                                    specified port).                          #
#                                                                              #
#                      0 on Failure (If given portState could not be verified  #
#                                    for the specified port).                  #
#                                                                              #
#  DEFINITION        : This function verifies the given portState for a        #
#                      specific port.                                          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_check_ptp_port_state\                         #
#                          -port_num           $port_num\                      #
#                          -port_state         $port_state                     #
#                                                                              #
################################################################################

proc ptp_ha::dut_check_ptp_port_state {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_check_ptp_port_state : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-port_state)]} {
        set port_state $param(-port_state)
    } else {
        ERRLOG -msg "ptp_ha::dut_check_ptp_port_state : Missing Argument    -> port_state"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Check PTP portState ($port_state) on port ($port_num)"

    if {![ptp_ha::${config_mode}_check_ptp_port_state\
                    -session_handler  $session_handler\
                    -port_num         $port_num\
                    -port_state       $port_state]} {
    
        ERRLOG -msg "ptp_ha::dut_check_ptp_port_state : Unable to check portState\
                     on specified port on DUT"
        return 0
    }

    return 1
}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_check_asymmetry_correction                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  state             : (Mandatory) asymmetryCorrectionPortDS.enable.           #
#                                  (e.g., "ENABLED"|"DISABLED"                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given state of                         #
#                                    asymmetryCorrectionPortDS.enable is       #
#                                    verified for the specified port).         #
#                                                                              #
#                      0 on Failure (If given state of                         #
#                                    asymmetryCorrectionPortDS.enable could not#
#                                    be verified for the specified port).      #
#                                                                              #
#  DEFINITION        : This function verifies the given state of               #
#                      asymmetryCorrectionPortDS.enable for a specific port.   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_check_asymmetry_correction\                   #
#                          -port_num           $port_num\                      #
#                          -state              $state                          #
#                                                                              #
################################################################################

proc ptp_ha::dut_check_asymmetry_correction {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_check_asymmetry_correction : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-state)]} {
        set state $param(-state)
    } else {
        ERRLOG -msg "ptp_ha::dut_check_asymmetry_correction : Missing Argument    -> state"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Check state of asymmetryCorrectionPortDS.enable ($state) on port ($port_num)"

    if {![ptp_ha::${config_mode}_check_asymmetry_correction\
                    -session_handler  $session_handler\
                    -port_num         $port_num\
                    -state            $state]} {

        ERRLOG -msg "ptp_ha::dut_check_asymmetry_correction : Unable to check\
                    state of asymmetryCorrectionPortDS.enable on specified port\
                    on DUT"
        return 0
    }

    return 1
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_check_egress_latency                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Mandatory) timestampCorrectionPortDS.egressLatency.    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of                         #
#                                    timestampCorrectionPortDS.egressLatency   #
#                                    verified for the specified port).         #
#                                                                              #
#                      0 on Failure (If given value of                         #
#                                    timestampCorrectionPortDS.egressLatency   #
#                                    could not be verified for the specified   #
#                                    port).                                    #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      timestampCorrectionPortDS.egressLatency for a specific  #
#                      port.                                                   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_check_egress_latency\                         #
#                          -port_num              $port_num\                   #
#                          -latency               $latency                     #
#                                                                              #
################################################################################

proc ptp_ha::dut_check_egress_latency {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_check_egress_latency : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-latency)]} {
        set latency $param(-latency)
    } else {
        ERRLOG -msg "ptp_ha::dut_check_egress_latency : Missing Argument    -> latency"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Check timestampCorrectionPortDS.egressLatency ($latency ns) of port ($port_num)"

    if {![ptp_ha::${config_mode}_check_egress_latency\
                    -session_handler  $session_handler\
                    -port_num         $port_num\
                    -latency          $latency]} {

        ERRLOG -msg "ptp_ha::dut_check_egress_latency : Unable to check\
                    timestampCorrectionPortDS.egressLatency\
                    of specified port from DUT"
        return 0
    }

    return 1
}


#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_check_ingress_latency                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Mandatory) timestampCorrectionPortDS.ingressLatency.   #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of                         #
#                                    timestampCorrectionPortDS.ingressLatency  #
#                                    verified for the specified port).         #
#                                                                              #
#                      0 on Failure (If given value of                         #
#                                    timestampCorrectionPortDS.ingressLatency  #
#                                    could not be verified for the specified   #
#                                    port).                                    #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      timestampCorrectionPortDS.ingressLatency for a specific #
#                      port.                                                   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_check_ingress_latency\                        #
#                          -port_num              $port_num\                   #
#                          -latency               $latency                     #
#                                                                              #
################################################################################

proc ptp_ha::dut_check_ingress_latency {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_check_ingress_latency : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-latency)]} {
        set latency $param(-latency)
    } else {
        ERRLOG -msg "ptp_ha::dut_check_ingress_latency : Missing Argument    -> latency"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Check timestampCorrectionPortDS.ingressLatency ($latency ns) of port ($port_num)"

    if {![ptp_ha::${config_mode}_check_ingress_latency\
                    -session_handler  $session_handler\
                    -port_num         $port_num\
                    -latency          $latency]} {

        ERRLOG -msg "ptp_ha::dut_check_ingress_latency : Unable to check\
                    timestampCorrectionPortDS.ingressLatency\
                    of specified port from DUT"
        return 0
    }

    return 1
}


#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_check_constant_asymmetry                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  constant_asymmetry: (Mandatory) asymmetryCorrectionPortDS.constantAsymmetry.#
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of                         #
#                                    asymmetryCorrectionPortDS.                #
#                                    constantAsymmetry verified for the        #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If given value of                         #
#                                    asymmetryCorrectionPortDS.                #
#                                    constantAsymmetry could not be verified   #
#                                    for specified port at the DUT).           #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      asymmetryCorrectionPortDS.constantAsymmetry for a       #
#                      specific port.                                          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_check_constant_asymmetry\                     #
#                          -port_num              $port_num\                   #
#                          -constant_asymmetry    $constant_asymmetry          #
#                                                                              #
################################################################################

proc ptp_ha::dut_check_constant_asymmetry {args} {

    array set param $args
    global env ptp_ha_env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_check_constant_asymmetry : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    ### Checking Output Parameters ###

    if {[info exists param(-constant_asymmetry)]} {
        set constant_asymmetry $param(-constant_asymmetry)
    } else {
        ERRLOG -msg "ptp_ha::dut_check_constant_asymmetry : Missing Argument    -> constant_asymmetry"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Check asymmetryCorrectionPortDS.constantAsymmetry\
                        ($constant_asymmetry ns) of port ($port_num)"

    if {![ptp_ha::${config_mode}_check_constant_asymmetry\
                        -session_handler    $session_handler\
                        -port_num           $port_num\
                        -constant_asymmetry $constant_asymmetry]} {

        ERRLOG -msg "ptp_ha::dut_check_constant_asymmetry: Unable to check\
                    asymmetryCorrectionPortDS.constantAsymmetry\
                    of specified port from DUT"
        return 0
    }

    return 1
}


#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_check_scaled_delay_coefficient              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  delay_coefficient : (Mandatory) asymmetryCorrectionPortDS.                  #
#                                  scaledDelayCoefficient.                     #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of                         #
#                                    asymmetryCorrectionPortDS.                #
#                                    scaledDelayCoefficient verified for the   #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If given value of                         #
#                                    asymmetryCorrectionPortDS.                #
#                                    scaledDelayCoefficient could not be       #
#                                    verified for specified port at the DUT).  #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      asymmetryCorrectionPortDS.scaledDelayCoefficient for a  #
#                      specific port.                                          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_check_scaled_delay_coefficient\               #
#                          -port_num              $port_num\                   #
#                          -delay_coefficient     $delay_coefficient           #
#                                                                              #
################################################################################

proc ptp_ha::dut_check_scaled_delay_coefficient {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_check_scaled_delay_coefficient : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-delay_coefficient)]} {
        set delay_coefficient $param(-delay_coefficient)
    } else {
        ERRLOG -msg "ptp_ha::dut_check_scaled_delay_coefficient : Missing Argument    -> delay_coefficient"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Check asymmetryCorrectionPortDS.scaledDelayCoefficient\
                        ($delay_coefficient (fractional number)) of port ($port_num)"

    if {![ptp_ha::${config_mode}_check_scaled_delay_coefficient\
                        -session_handler    $session_handler\
                        -port_num           $port_num\
                        -delay_coefficient  $delay_coefficient]} {

        ERRLOG -msg "ptp_ha::dut_check_scaled_delay_coefficient : Unable to check\
                    asymmetryCorrectionPortDS.scaledDelayCoefficient\
                    of specified port from DUT"
        return 0
    }

    return 1
}


#8.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_check_external_port_config                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  state             : (Mandatory) defaultDS.externalPortConfigurationEnabled. #
#                                  (e.g., "ENABLED"|"DISABLED"                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given state of                         #
#                                    defaultDS.externalPortConfigurationEnabled#
#                                    is verified for the specified port).      #
#                                                                              #
#                      0 on Failure (If given state of                         #
#                                    defaultDS.externalPortConfigurationEnabled#
#                                    could not be verified for the specified   #
#                                    port).                                    #
#                                                                              #
#  DEFINITION        : This function verifies the given state of               #
#                      defaultDS.externalPortConfigurationEnabled for a        #
#                      specific port.                                          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_check_external_port_config\                   #
#                          -port_num           $port_num\                      #
#                          -state              $state                          #
#                                                                              #
################################################################################

proc ptp_ha::dut_check_external_port_config {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_check_external_port_config : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-state)]} {
        set state $param(-state)
    } else {
        ERRLOG -msg "ptp_ha::dut_check_external_port_config : Missing Argument    -> state"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Check state of defaultDS.externalPortConfigurationEnabled ($state) on port ($port_num)"

    if {![ptp_ha::${config_mode}_check_external_port_config\
                    -session_handler  $session_handler\
                    -port_num         $port_num\
                    -state            $state]} {

        ERRLOG -msg "ptp_ha::dut_check_external_port_config : Unable to check\
                    state of defaultDS.externalPortConfigurationEnabled on specified port\
                    on DUT"
        return 0
    }

    return 1
}


#9.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_check_slave_only                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  value             : (Mandatory) Value of defaultDS.slaveOnly                #
#                                  (e.g., "TRUE"|"FALSE")                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of defaultDS.slaveOnly     #
#                                    is verified at the DUT).                  #
#                                                                              #
#                      0 on Failure (If given value of defaultDS.slaveOnly     #
#                                    could not be verified at the DUT).        #
#                                                                              #
#  DEFINITION        : This function verifies the given value of defaultDS.    #
#                      slaveOnly at DUT.                                       #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_check_slave_only\                             #
#                          -value         $value                               #
#                                                                              #
################################################################################

proc ptp_ha::dut_check_slave_only {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-value)]} {
        set value $param(-value)
    } else {
        ERRLOG -msg "ptp_ha::dut_check_slave_only : Missing Argument    -> value"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Check value of defaultDS.slaveOnly ($value) in DUT"

    if {![ptp_ha::${config_mode}_check_slave_only\
                    -session_handler      $session_handler\
                    -value                $value]} {

        ERRLOG -msg "ptp_ha::dut_check_slave_only: Unable to check\
                     value of defaultDS.slaveOnly in DUT"
        return 0
    }

    return 1
}


#10.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_check_master_only                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  value             : (Mandatory) Value of portDS.masterOnly                  #
#                                  (e.g., "TRUE"|"FALSE")                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of portDS.masterOnly       #
#                                    is verified for the specified port).      #
#                                                                              #
#                      0 on Failure (If given value of portDS.masterOnly       #
#                                    could not be verified for the specified   #
#                                    port).                                    #
#                                                                              #
#  DEFINITION        : This function verifies the given value of portDS.       #
#                      masterOnly for a specific port.                         #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_check_master_only\                            #
#                          -port_num      $port_num\                           #
#                          -value         $value                               #
#                                                                              #
################################################################################

proc ptp_ha::dut_check_master_only {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_check_master_only : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-value)]} {
        set value $param(-value)
    } else {
        ERRLOG -msg "ptp_ha::dut_check_master_only : Missing Argument    -> value"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Check value of portDS.masterOnly ($value) on port ($port_num)"

    if {![ptp_ha::${config_mode}_check_master_only\
                    -session_handler      $session_handler\
                    -port_num             $port_num\
                    -value                $value]} {

        ERRLOG -msg "ptp_ha::dut_check_master_only: Unable to check\
                     value of portDS.masterOnly on specified port\
                     on DUT"
        return 0
    }

    return 1
}


#11.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_check_for_least_offset_from_master          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  offset_from_master: (Mandatory) currentDS.offsetFromMaster.                 #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  RETURNS           : 1 on Success (If the absolute value of currentDS.       #
#                                    offsetFromMaster in DUT becomes lesser    #
#                                    than the specified value to ensure that   #
#                                    the DUT synchronized it's time with       #
#                                    messages from TEE).                       #
#                                                                              #
#                      0 on Failure (If the absolute value of currentDS.       #
#                                    offsetFromMaster in DUT does not become   #
#                                    lesser than the specified value to ensure #
#                                    that the DUT synchronized it's time with  #
#                                    messages from TEE).                       #
#                                                                              #
#  DEFINITION        : This function invokes recursive procedure to check      #
#                      whether the absolute value of currentDS.offsetFromMaster#
#                      is lesser than the specified value to ensure that DUT   #
#                      synchronized it's time with messages from TEE.          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_check_for_least_offset_from_master\           #
#                          -offset_from_master    offset_from_master           #
#                                                                              #
################################################################################

proc ptp_ha::dut_check_for_least_offset_from_master {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-offset_from_master)]} {
        set offset_from_master $param(-offset_from_master)
    } else {
        ERRLOG -msg "ptp_ha::dut_check_for_least_offset_from_master : Missing Argument    -> offset_from_master"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Check absolute value of currentDS.offsetFromMaster\
                       in DUT is lesser than $offset_from_master ns"

    if {![ptp_ha::${config_mode}_check_for_least_offset_from_master\
                          -session_handler     $session_handler\
                          -offset_from_master  $offset_from_master]} {

        ERRLOG -msg "ptp_ha::dut_check_for_least_offset_from_master : Unable to\
                     check value of currentDS.offsetFromMaster in DUT"
        return 0
    }

    return 1
}



################################################################################
#                           DUT GET FUNCTIONS                                  #
################################################################################

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_get_timestamp                               #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  timestamp         : (Mandatory) Timestamp.                                  #
#                                  (e.g., xxxxx)                               #
#                                                                              #
#  RETURNS           : 1 on Success (If able to extract timestamp from DUT).   #
#                                                                              #
#                      0 on Failure (If could not able to extract timestamp    #
#                                   from DUT).                                 #
#                                                                              #
#  DEFINITION        : This function extracts timestamp from DUT.              #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_get_timestamp\                                #
#                             -timestamp       timestamp                       #
#                                                                              #
################################################################################

proc ptp_ha::dut_get_timestamp {args} {

    array set param $args
    global env

    set config_mode $env(DUT_CONFIG_MODE)
    set session_handler $::dut_session_id

    set missedArgCounter 0

    ### Checking Output Parameters ###

    if {[info exists param(-timestamp)]} {
        upvar $param(-timestamp) timestamp
    } else {
        ERRLOG -msg "ptp_ha::dut_get_timestamp : Missing Argument    -> timestamp"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Get timestamp from DUT"

    if {![ptp_ha::${config_mode}_get_timestamp\
                    -session_handler  $session_handler\
                    -timestamp        timestamp]} {
    
        ERRLOG -msg "ptp_ha::dut_get_timestamp : Unable to get timestamp from DUT"
        return 0
    }

    LOG -level 2 -msg "\n\t Extracted timestamp from DUT: $timestamp"

    return 1
}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_get_mean_delay                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  mean_delay        : (Mandatory) currentDS.meanDelay.                        #
#                                  (e.g., 3)                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If able to extract currentDS.meanDelay of #
#                                    specified port from the DUT).             #
#                                                                              #
#                      0 on Failure (If could not able to extract              #
#                                    currentDS.meanDelay of specified port from#
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function extracts currentDS.meanDelay of a specific#
#                      port from the DUT.                                      #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_get_mean_delay\                               #
#                          -port_num              $port_num\                   #
#                          -mean_delay            mean_delay                   #
#                                                                              #
################################################################################

proc ptp_ha::dut_get_mean_delay {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_get_mean_delay : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    ### Checking Output Parameters ###

    if {[info exists param(-mean_delay)]} {
        upvar $param(-mean_delay) mean_delay
    } else {
        ERRLOG -msg "ptp_ha::dut_get_mean_delay : Missing Argument    -> mean_delay"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Get currentDS.meanDelay of port ($port_num)"

    if {![ptp_ha::${config_mode}_get_mean_delay\
                          -session_handler  $session_handler\
                          -port_num         $port_num\
                          -mean_delay       mean_delay]} {

        ERRLOG -msg "ptp_ha::dut_get_mean_delay : Unable to get currentDS.meanDelay\
                    of specified port from DUT"
        return 0
    }

    LOG -level 2 -msg "\n\t Extracted currentDS.meanDelay from DUT: $mean_delay"

    return 1
}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_get_mean_link_delay                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  mean_link_delay   : (Mandatory) portDS.meanLinkDelay.                       #
#                                  (e.g., 3)                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If able to extract portDS.meanLinkDelay of#
#                                    specified port from the DUT).             #
#                                                                              #
#                      0 on Failure (If could not able to extract              #
#                                    portDS.meanLinkDelay of specified port    #
#                                    from the DUT).                            #
#                                                                              #
#  DEFINITION        : This function extracts portDS.meanLinkDelay of a        #
#                      specific port from the DUT.                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_get_mean_link_delay\                          #
#                          -port_num              $port_num\                   #
#                          -mean_link_delay       mean_link_delay              #
#                                                                              #
################################################################################

proc ptp_ha::dut_get_mean_link_delay {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_get_mean_link_delay : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    ### Checking Output Parameters ###

    if {[info exists param(-mean_link_delay)]} {
        upvar $param(-mean_link_delay) mean_link_delay
    } else {
        ERRLOG -msg "ptp_ha::dut_get_mean_link_delay : Missing Argument    -> mean_link_delay"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Get portDS.meanLinkDelay of port ($port_num)"

    if {![ptp_ha::${config_mode}_get_mean_link_delay\
                          -session_handler  $session_handler\
                          -port_num         $port_num\
                          -mean_link_delay       mean_link_delay]} {

        ERRLOG -msg "ptp_ha::dut_get_mean_link_delay : Unable to get portDS.meanLinkDelay\
                     of specified port from DUT"
        return 0
    }

    LOG -level 2 -msg "\n\t Extracted portDS.meanLinkDelay from DUT: $mean_link_delay"

    return 1
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_get_delay_asymmetry                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  delay_asymmetry   : (Mandatory) portDS.delayAsymmetry.                      #
#                                  (e.g., 3)                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If able to extract portDS.delayAsymmetry  #
#                                    of specified port from the DUT).          #
#                                                                              #
#                      0 on Failure (If could not able to extract              #
#                                    portDS.delayAsymmetry of specified port   #
#                                    from the DUT).                            #
#                                                                              #
#  DEFINITION        : This function extracts portDS.delayAsymmetry of a       #
#                      specific port from the DUT.                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::dut_get_delay_asymmetry\                          #
#                          -port_num              $port_num\                   #
#                          -delay_asymmetry       delay_asymmetry              #
#                                                                              #
################################################################################

proc ptp_ha::dut_get_delay_asymmetry {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::dut_get_delay_asymmetry : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    ### Checking Output Parameters ###

    if {[info exists param(-delay_asymmetry)]} {
        upvar $param(-delay_asymmetry) delay_asymmetry
    } else {
        ERRLOG -msg "ptp_ha::dut_get_delay_asymmetry : Missing Argument    -> delay_asymmetry"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg "\t Get portDS.delayAsymmetry of port ($port_num)"

    if {![ptp_ha::${config_mode}_get_delay_asymmetry\
                          -session_handler     $session_handler\
                          -port_num            $port_num\
                          -delay_asymmetry     delay_asymmetry]} {

        ERRLOG -msg "ptp_ha::dut_get_delay_asymmetry : Unable to get portDS.delayAsymmetry\
                    of specified port from DUT"
        return 0
    }

    LOG -level 2 -msg "\n\t Extracted portDS.delayAsymmetry from DUT: $delay_asymmetry"

    return 1
}



################################################################################
#                     DUT SETUP CONFIGURATION FUNCTIONS                        #
################################################################################


#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_configure_setup_001                         #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on success (If the DUT is configured for test setup 1)#
#                                                                              #
#                      0 on failure (If the DUT could not be configured for    #
#                                    test setup 1).                            #
#                                                                              #
#  DEFINITION        : This function configures the parameters required for the#
#                      test setup 1 at the DUT.                                #
#                                                                              #
#                      This function in turn calls below functions             #
#                                                                              #
#                         ptp_ha::dut_port_enable                              #
#                         ptp_ha::dut_global_ptp_enable                        #
#                         ptp_ha::dut_port_ptp_enable                          #
#                         ptp_ha::dut_set_domain                               #
#                         ptp_ha::dut_set_clock_mode                           #
#                         ptp_ha::dut_set_clock_step                           #
#                         ptp_ha::dut_set_delay_mechanism                      #
#                         ptp_ha::dut_set_communication_mode                   #
#                         ptp_ha::dut_set_network_protocol                     #
#                         ptp_ha::dut_set_vlan                                 #
#                         ptp_ha::dut_set_vlan_priority                        #
#                         ptp_ha::dut_set_ipv4_address                         #
#                         ptp_ha::dut_set_priority1                            #
#                         ptp_ha::dut_set_priority2                            #
#                         ptp_ha::dut_tcr_rcr_cr_enable                        #
#                         ptp_ha::dut_l1sync_enable                            #
#                         ptp_ha::dut_l1sync_opt_params_disable                #
#                         ptp_ha::dut_masteronly_disable                       #
#                         ptp_ha::dut_set_announce_interval                    #
#                         ptp_ha::dut_set_announce_timeout                     #
#                         ptp_ha::dut_set_sync_interval                        #
#                         ptp_ha::dut_set_delayreq_interval                    #
#                         ptp_ha::dut_set_pdelayreq_interval                   #
#                         ptp_ha::dut_set_l1sync_interval                      #
#                         ptp_ha::dut_set_l1sync_timeout                       #
#                         ptp_ha::dut_set_egress_latency                       #
#                         ptp_ha::dut_set_ingress_latency                      #
#                         ptp_ha::dut_set_constant_asymmetry                   #
#                         ptp_ha::dut_set_scaled_delay_coefficient             #
#                         ptp_ha::dut_set_asymmetry_correction                 #
#                         ptp_ha::dut_external_port_config_disable             #
#                         ptp_ha::dut_set_epc_desired_state                    #
#                                                                              #
# USAGE              :                                                         #
#                                                                              #
#     ptp_ha::dut_configure_setup_001                                          #
#                                                                              #
################################################################################

proc ptp_ha::dut_configure_setup_001 {args} {

    array set param $args

    global env ptp_ha_env

    set ip_address             $env(dut_TEST_ADDRESS1)
    set net_mask               $env(dut_TEST_MASK1)

    set vlan_priority          $ptp_ha_env(DUT_PTP_HA_VLAN_PRIORITY)

    set ptp_vlan_encap         $ptp_ha_env(DUT_PTP_HA_IS_VLAN_ENCAP)

    set clock_mode             $ptp_ha_env(DUT_PTP_HA_DEVCIE_TYPE)
    set clock_step             $ptp_ha_env(DUT_PTP_HA_CLOCK_STEP)
    set network_protocol       $ptp_ha_env(DUT_PTP_HA_TRANS_TYPE)
    set communication_mode     $ptp_ha_env(DUT_PTP_HA_COM_MODEL)
    set priority1              $ptp_ha_env(DUT_PTP_HA_PRIORITY1)
    set priority2              $ptp_ha_env(DUT_PTP_HA_PRIORITY2)
    set announce_interval      $ptp_ha_env(DUT_PTP_HA_ANNOUNCE_INTERVAL)
    set announce_timeout       $ptp_ha_env(DUT_PTP_HA_ANNOUNCE_TIMEOUT)
    set sync_interval          $ptp_ha_env(DUT_PTP_HA_SYNC_INTERVAL)
    set delayreq_interval      $ptp_ha_env(DUT_PTP_HA_DELAY_REQ_INTERVAL)
    set L1syncinterval         $ptp_ha_env(DUT_PTP_HA_L1SYNC_INTERVAL)
    set L1SyncReceiptTimeout   $ptp_ha_env(DUT_PTP_HA_L1SYNC_TIMEOUT)

    set egress_latency         $ptp_ha_env(DUT_PTP_HA_EGRESS_LATENCY)
    set ingress_latency        $ptp_ha_env(DUT_PTP_HA_INGRESS_LATENCY)
    set constant_asymmetry     $ptp_ha_env(DUT_PTP_HA_CONSTANT_ASYMMETRY)
    set delay_coefficient      $ptp_ha_env(DUT_PTP_HA_SCALED_DELAY_COEFFICIENT)

    set port_list              $::dut_port_num_1
    set domain                 $::ptp_dut_default_domain_number
    set delay_mechanism        $::DUT_PTP_DELAY_MECHANISM(E2E)

    set ptp_version            $::ptp_version

    set vlan_id                $::ptp_ha_vlan_id

    ### Check Input parameters ###

    set env(TEE_ATTEST_MANUAL_PROMPT)      0

    ###########################################################################
    #  1) Ensure the DUT port P1 admin status Up.                             #
    ###########################################################################

    if {![ptp_ha::dut_port_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###########################################################################
    #  2) Ensure PTP global on the DUT.                                       #
    ###########################################################################

    if {![ptp_ha::dut_global_ptp_enable]} {

        return 0
    }

    ###########################################################################
    #  3) Ensure PTP enabled on the port P1 on the DUT.                       #
    ###########################################################################

    if {![ptp_ha::dut_port_ptp_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    #  4) Configure Domain.                                                      #
    ##############################################################################

    if {![ptp_ha::dut_set_domain\
                    -clock_mode             $clock_mode\
                    -domain                 $domain ]} {

        return 0
    }

    ###########################################################################
    #  5) Ensure PTP set clock mode                                           #
    ###########################################################################

    if {![ptp_ha::dut_set_clock_mode\
                    -clock_mode             $clock_mode\
                    -port_num               [lindex $port_list 0] ]} {

        return 0
    }

    ###########################################################################
    #  6) Ensure PTP set clock step                                           #
    ###########################################################################

    if {![ptp_ha::dut_set_clock_step\
                    -port_num               [lindex $port_list 0]\
                    -clock_mode             $clock_mode\
                    -clock_step             $clock_step]} {

        return 0
    }

    ##############################################################################
    #  7) Configure Delay Mechanism.                                             #
    ##############################################################################

    if {![ptp_ha::dut_set_delay_mechanism\
                    -delay_mechanism        $delay_mechanism\
                    -clock_mode             $clock_mode\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    #  8) Configure Communication mode.                                          #
    ##############################################################################

    if {![ptp_ha::dut_set_communication_mode\
                   -port_num                [lindex $port_list 0]\
                   -clock_mode              $clock_mode\
                   -ptp_version             $ptp_version\
                   -communication_mode      $communication_mode]} {

        return 0
    }

    ###########################################################################
    #  9) Set Network protocol                                                #
    ###########################################################################

    if {![ptp_ha::dut_set_network_protocol\
                    -port_num               [lindex $port_list 0]\
                    -clock_mode             $clock_mode\
                    -network_protocol       $network_protocol]} {

        return 0
    }

    if {$ptp_vlan_encap == "true"} {

        ##############################################################################
        # 10) Create VLAN VLAN-A and associate port P1 to it.                        #
        ##############################################################################

        if {![ptp_ha::dut_set_vlan\
                        -vlan_id                $vlan_id\
                        -port_num               [lindex $port_list 0]]} {

            return 0
        }

        ##############################################################################
        # 11) Create VLAN VLAN-A and associate port P1 to it.                        #
        ##############################################################################

        if {![ptp_ha::dut_set_vlan_priority\
                        -vlan_id                $vlan_id\
                        -vlan_priority          $vlan_priority\
                        -port_num               [lindex $port_list 0]]} {

            return 0
        }
    }

    if {$::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4)} {

        ##############################################################################
        # 12) Set IP address.                                                        #
        ##############################################################################

        if {![ptp_ha::dut_set_ipv4_address\
                        -port_num              [lindex $port_list 0]\
                        -ip_address            $ip_address\
                        -net_mask              $net_mask\
                        -vlan_id               $vlan_id]} {

            return 0
        }
    }

    ##############################################################################
    # 13) Configure Priority1.                                                   #
    ##############################################################################

    if {![ptp_ha::dut_set_priority1\
                    -clock_mode             $clock_mode\
                    -priority1              $priority1]} {

        return 0
    }

    ##############################################################################
    # 14) Configure Priority2.                                                   #
    ##############################################################################

    if {![ptp_ha::dut_set_priority2\
                    -clock_mode             $clock_mode\
                    -priority2              $priority2]} {

        return 0
    }

    ###############################################################################
    # 15) Enable txcoherentisRequired, rxcoherentisRequired,                      #
    #     congruentIsRequired.                                                    #
    ###############################################################################

    if {![ptp_ha::dut_tcr_rcr_cr_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###############################################################################
    # 16) Enable L1 Sync on DUT.                                                  #
    ###############################################################################

    if {![ptp_ha::dut_l1sync_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###############################################################################
    # 17) Disable optParametersConfigured.                                        #
    ###############################################################################

    if {![ptp_ha::dut_l1sync_opt_params_disable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###############################################################################
    # 17) Disable masterOnly.                                                     #
    ###############################################################################

    if {![ptp_ha::dut_masteronly_disable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 18) Configure Announce Interval.                                           #
    ##############################################################################

    if {![ptp_ha::dut_set_announce_interval\
                    -announce_interval      $announce_interval\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 19) Configure Announce Timeout.                                            #
    ##############################################################################

    if {![ptp_ha::dut_set_announce_timeout\
                    -announce_timeout       $announce_timeout\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 20) Configure Sync Interval.                                               #
    ##############################################################################

    if {![ptp_ha::dut_set_sync_interval\
                    -sync_interval          $sync_interval\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 21) Configure delayRequestInterval                                         #
    ##############################################################################

    if {![ptp_ha::dut_set_delayreq_interval\
                    -delayreq_interval      $delayreq_interval\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 22) Configure logL1SyncInterval.                                           #
    ##############################################################################

    if {![ptp_ha::dut_set_l1sync_interval\
                    -port_num               [lindex $port_list 0]\
                    -l1sync_interval        $L1syncinterval]} {

        return 0
    }

    ##############################################################################
    # 23) Configure L1SyncReceiptTimeout.                                        #
    ##############################################################################

    if {![ptp_ha::dut_set_l1sync_timeout\
                    -port_num               [lindex $port_list 0]\
                    -l1sync_timeout         $L1SyncReceiptTimeout]} {

        return 0
    }

    ##############################################################################
    # 24) Configure timestampCorrectionPortDS.egressLatency.                     #
    ##############################################################################

    if {![ptp_ha::dut_set_egress_latency\
                    -port_num              [lindex $port_list 0]\
                    -latency               $egress_latency]} {

        return 0
    }

    ##############################################################################
    # 25) Configure timestampCorrectionPortDS.ingressLatency.                    #
    ##############################################################################

    if {![ptp_ha::dut_set_ingress_latency\
                    -port_num              [lindex $port_list 0]\
                    -latency               $ingress_latency]} {

        return 0
    }

    ##############################################################################
    # 26) Configure asymmetryCorrectionPortDS.constantAsymmetry.                 #
    ##############################################################################

    if {![ptp_ha::dut_set_constant_asymmetry\
                    -port_num              [lindex $port_list 0]\
                    -constant_asymmetry    $constant_asymmetry]} {

        return 0
    }

    ##############################################################################
    # 27) Configure asymmetryCorrectionPortDS.scaledDelayCoefficient.            #
    ##############################################################################

    if {![ptp_ha::dut_set_scaled_delay_coefficient\
                    -port_num              [lindex $port_list 0]\
                    -delay_coefficient     $delay_coefficient]} {

        return 0
    }

    ##############################################################################
    # 28) Configure asymmetryCorrectionPortDS.enable.                            #
    ##############################################################################

    if {![ptp_ha::dut_set_asymmetry_correction\
                    -port_num              [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 29) Disable externalPortConfigurationEnabled.                              #
    ##############################################################################

    if {![ptp_ha::dut_external_port_config_disable\
                    -port_num              [lindex $port_list 0]]} {

        return 0
    }

    set env(TEE_ATTEST_MANUAL_PROMPT)      1

    return 1
}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_configure_setup_002                         #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on success (If the DUT is configured for test setup 2)#
#                                                                              #
#                      0 on failure (If the DUT could not be configured for    #
#                                    test setup 2).                            #
#                                                                              #
#  DEFINITION        : This function configures the parameters required for the#
#                      test setup 2 at the DUT.                                #
#                                                                              #
#                      This function in turn calls below functions             #
#                                                                              #
#                         ptp_ha::dut_port_enable                              #
#                         ptp_ha::dut_global_ptp_enable                        #
#                         ptp_ha::dut_port_ptp_enable                          #
#                         ptp_ha::dut_set_domain                               #
#                         ptp_ha::dut_set_clock_mode                           #
#                         ptp_ha::dut_set_clock_step                           #
#                         ptp_ha::dut_set_delay_mechanism                      #
#                         ptp_ha::dut_set_communication_mode                   #
#                         ptp_ha::dut_set_network_protocol                     #
#                         ptp_ha::dut_set_vlan                                 #
#                         ptp_ha::dut_set_vlan_priority                        #
#                         ptp_ha::dut_set_ipv4_address                         #
#                         ptp_ha::dut_set_priority1                            #
#                         ptp_ha::dut_set_priority2                            #
#                         ptp_ha::dut_tcr_rcr_cr_enable                        #
#                         ptp_ha::dut_l1sync_enable                            #
#                         ptp_ha::dut_l1sync_opt_params_disable                #
#                         ptp_ha::dut_masteronly_disable                       #
#                         ptp_ha::dut_set_announce_interval                    #
#                         ptp_ha::dut_set_announce_timeout                     #
#                         ptp_ha::dut_set_sync_interval                        #
#                         ptp_ha::dut_set_delayreq_interval                    #
#                         ptp_ha::dut_set_pdelayreq_interval                   #
#                         ptp_ha::dut_set_l1sync_interval                      #
#                         ptp_ha::dut_set_l1sync_timeout                       #
#                         ptp_ha::dut_set_egress_latency                       #
#                         ptp_ha::dut_set_ingress_latency                      #
#                         ptp_ha::dut_set_constant_asymmetry                   #
#                         ptp_ha::dut_set_scaled_delay_coefficient             #
#                         ptp_ha::dut_set_asymmetry_correction                 #
#                         ptp_ha::dut_external_port_config_disable             #
#                         ptp_ha::dut_set_epc_desired_state                    #
#                                                                              #
# USAGE              :                                                         #
#                                                                              #
#     ptp_ha::dut_configure_setup_002                                          #
#                                                                              #
################################################################################

proc ptp_ha::dut_configure_setup_002 {args} {

    array set param $args

    global env ptp_ha_env

    set ip_address             $env(dut_TEST_ADDRESS1)
    set net_mask               $env(dut_TEST_MASK1)

    set vlan_priority          $ptp_ha_env(DUT_PTP_HA_VLAN_PRIORITY)

    set ptp_vlan_encap         $ptp_ha_env(DUT_PTP_HA_IS_VLAN_ENCAP)

    set clock_mode             $ptp_ha_env(DUT_PTP_HA_DEVCIE_TYPE)
    set clock_step             $ptp_ha_env(DUT_PTP_HA_CLOCK_STEP)
    set network_protocol       $ptp_ha_env(DUT_PTP_HA_TRANS_TYPE)
    set communication_mode     $ptp_ha_env(DUT_PTP_HA_COM_MODEL)
    set priority1              $ptp_ha_env(DUT_PTP_HA_PRIORITY1)
    set priority2              $ptp_ha_env(DUT_PTP_HA_PRIORITY2)
    set announce_interval      $ptp_ha_env(DUT_PTP_HA_ANNOUNCE_INTERVAL)
    set announce_timeout       $ptp_ha_env(DUT_PTP_HA_ANNOUNCE_TIMEOUT)
    set sync_interval          $ptp_ha_env(DUT_PTP_HA_SYNC_INTERVAL)
    set pdelayreq_interval     $ptp_ha_env(DUT_PTP_HA_PDELAY_REQ_INTERVAL)
    set L1syncinterval         $ptp_ha_env(DUT_PTP_HA_L1SYNC_INTERVAL)
    set L1SyncReceiptTimeout   $ptp_ha_env(DUT_PTP_HA_L1SYNC_TIMEOUT)

    set egress_latency         $ptp_ha_env(DUT_PTP_HA_EGRESS_LATENCY)
    set ingress_latency        $ptp_ha_env(DUT_PTP_HA_INGRESS_LATENCY)
    set constant_asymmetry     $ptp_ha_env(DUT_PTP_HA_CONSTANT_ASYMMETRY)
    set delay_coefficient      $ptp_ha_env(DUT_PTP_HA_SCALED_DELAY_COEFFICIENT)

    set port_list              $::dut_port_num_1
    set domain                 $::ptp_dut_default_domain_number
    set delay_mechanism        $::DUT_PTP_DELAY_MECHANISM(P2P)

    set ptp_version            $::ptp_version

    set vlan_id                $::ptp_ha_vlan_id

    ### Check Input parameters ###

    set env(TEE_ATTEST_MANUAL_PROMPT)      0

    ###########################################################################
    #  1) Ensure the DUT port P1 admin status Up.                             #
    ###########################################################################

    if {![ptp_ha::dut_port_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###########################################################################
    #  2) Ensure PTP global on the DUT.                                       #
    ###########################################################################

    if {![ptp_ha::dut_global_ptp_enable]} {

        return 0
    }

    ###########################################################################
    #  3) Ensure PTP enabled on the port P1 on the DUT.                       #
    ###########################################################################

    if {![ptp_ha::dut_port_ptp_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    #  4) Configure Domain.                                                      #
    ##############################################################################

    if {![ptp_ha::dut_set_domain\
                    -clock_mode             $clock_mode\
                    -domain                 $domain ]} {

        return 0
    }

    ###########################################################################
    #  5) Ensure PTP set clock mode                                           #
    ###########################################################################

    if {![ptp_ha::dut_set_clock_mode\
                    -clock_mode             $clock_mode\
                    -port_num               [lindex $port_list 0] ]} {

        return 0
    }

    ###########################################################################
    #  6) Ensure PTP set clock step                                           #
    ###########################################################################

    if {![ptp_ha::dut_set_clock_step\
                    -port_num               [lindex $port_list 0]\
                    -clock_mode             $clock_mode\
                    -clock_step             $clock_step]} {

        return 0
    }

    ##############################################################################
    #  7) Configure Delay Mechanism.                                             #
    ##############################################################################

    if {![ptp_ha::dut_set_delay_mechanism\
                    -delay_mechanism        $delay_mechanism\
                    -clock_mode             $clock_mode\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    #  8) Configure Communication mode.                                          #
    ##############################################################################

    if {![ptp_ha::dut_set_communication_mode\
                   -port_num                [lindex $port_list 0]\
                   -clock_mode              $clock_mode\
                   -ptp_version             $ptp_version\
                   -communication_mode      $communication_mode]} {

        return 0
    }

    ###########################################################################
    #  9) Set Network protocol                                                #
    ###########################################################################

    if {![ptp_ha::dut_set_network_protocol\
                    -port_num               [lindex $port_list 0]\
                    -clock_mode             $clock_mode\
                    -network_protocol       $network_protocol]} {

        return 0
    }

    if {$ptp_vlan_encap == "true"} {

        ##############################################################################
        # 10) Create VLAN VLAN-A and associate port P1 to it.                        #
        ##############################################################################

        if {![ptp_ha::dut_set_vlan\
                        -vlan_id                $vlan_id\
                        -port_num               [lindex $port_list 0]]} {

            return 0
        }

        ##############################################################################
        # 11) Create VLAN VLAN-A and associate port P1 to it.                        #
        ##############################################################################

        if {![ptp_ha::dut_set_vlan_priority\
                        -vlan_id                $vlan_id\
                        -vlan_priority          $vlan_priority\
                        -port_num               [lindex $port_list 0]]} {

            return 0
        }
    }

    if {$::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4)} {

        ##############################################################################
        # 12) Set IP address.                                                        #
        ##############################################################################

        if {![ptp_ha::dut_set_ipv4_address\
                        -port_num              [lindex $port_list 0]\
                        -ip_address            $ip_address\
                        -net_mask              $net_mask\
                        -vlan_id               $vlan_id]} {

            return 0
        }
    }

    ##############################################################################
    # 13) Configure Priority1.                                                   #
    ##############################################################################

    if {![ptp_ha::dut_set_priority1\
                    -clock_mode             $clock_mode\
                    -priority1              $priority1]} {

        return 0
    }

    ##############################################################################
    # 14) Configure Priority2.                                                   #
    ##############################################################################

    if {![ptp_ha::dut_set_priority2\
                    -clock_mode             $clock_mode\
                    -priority2              $priority2]} {

        return 0
    }

    ###############################################################################
    # 15) Enable txcoherentisRequired, rxcoherentisRequired,                      #
    #     congruentIsRequired.                                                    #
    ###############################################################################

    if {![ptp_ha::dut_tcr_rcr_cr_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###############################################################################
    # 16) Enable L1 Sync on DUT.                                                  #
    ###############################################################################

    if {![ptp_ha::dut_l1sync_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###############################################################################
    # 17) Disable optParametersConfigured.                                        #
    ###############################################################################

    if {![ptp_ha::dut_l1sync_opt_params_disable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###############################################################################
    # 17) Disable masterOnly.                                                     #
    ###############################################################################

    if {![ptp_ha::dut_masteronly_disable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 18) Configure Announce Interval.                                           #
    ##############################################################################

    if {![ptp_ha::dut_set_announce_interval\
                    -announce_interval      $announce_interval\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 19) Configure Announce Timeout.                                            #
    ##############################################################################

    if {![ptp_ha::dut_set_announce_timeout\
                    -announce_timeout       $announce_timeout\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 20) Configure Sync Interval.                                               #
    ##############################################################################

    if {![ptp_ha::dut_set_sync_interval\
                    -sync_interval          $sync_interval\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 21) Configure PdelayRequestInterval                                        #
    ##############################################################################

    if {![ptp_ha::dut_set_pdelayreq_interval\
                    -pdelayreq_interval     $pdelayreq_interval\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 22) Configure logL1SyncInterval.                                           #
    ##############################################################################

    if {![ptp_ha::dut_set_l1sync_interval\
                    -port_num               [lindex $port_list 0]\
                    -l1sync_interval        $L1syncinterval]} {

        return 0
    }

    ##############################################################################
    # 23) Configure L1SyncReceiptTimeout.                                        #
    ##############################################################################

    if {![ptp_ha::dut_set_l1sync_timeout\
                    -port_num               [lindex $port_list 0]\
                    -l1sync_timeout         $L1SyncReceiptTimeout]} {

        return 0
    }

    ##############################################################################
    # 24) Configure timestampCorrectionPortDS.egressLatency.                     #
    ##############################################################################

    if {![ptp_ha::dut_set_egress_latency\
                    -port_num              [lindex $port_list 0]\
                    -latency               0]} {

        return 0
    }

    ##############################################################################
    # 25) Configure timestampCorrectionPortDS.ingressLatency.                    #
    ##############################################################################

    if {![ptp_ha::dut_set_ingress_latency\
                    -port_num              [lindex $port_list 0]\
                    -latency               0]} {

        return 0
    }

    ##############################################################################
    # 26) Configure asymmetryCorrectionPortDS.constantAsymmetry.                 #
    ##############################################################################

    if {![ptp_ha::dut_set_constant_asymmetry\
                    -port_num              [lindex $port_list 0]\
                    -constant_asymmetry    0]} {

        return 0
    }

    ##############################################################################
    # 27) Configure asymmetryCorrectionPortDS.scaledDelayCoefficient.            #
    ##############################################################################

    if {![ptp_ha::dut_set_scaled_delay_coefficient\
                    -port_num              [lindex $port_list 0]\
                    -delay_coefficient     0]} {

        return 0
    }

    ##############################################################################
    # 28) Configure asymmetryCorrectionPortDS.enable.                            #
    ##############################################################################

    if {![ptp_ha::dut_set_asymmetry_correction\
                    -port_num              [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 29) Disable externalPortConfigurationEnabled.                              #
    ##############################################################################

    if {![ptp_ha::dut_external_port_config_disable\
                    -port_num              [lindex $port_list 0]]} {

        return 0
    }

    set env(TEE_ATTEST_MANUAL_PROMPT)      1

    return 1
}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_configure_setup_003                         #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on success (If the DUT is configured for test setup 3)#
#                                                                              #
#                      0 on failure (If the DUT could not be configured for    #
#                                    test setup 3).                            #
#                                                                              #
#  DEFINITION        : This function configures the parameters required for the#
#                      test setup 3 at the DUT.                                #
#                                                                              #
#                      This function in turn calls below functions             #
#                                                                              #
#                         ptp_ha::dut_port_enable                              #
#                         ptp_ha::dut_global_ptp_enable                        #
#                         ptp_ha::dut_port_ptp_enable                          #
#                         ptp_ha::dut_set_domain                               #
#                         ptp_ha::dut_set_clock_mode                           #
#                         ptp_ha::dut_set_clock_step                           #
#                         ptp_ha::dut_set_delay_mechanism                      #
#                         ptp_ha::dut_set_communication_mode                   #
#                         ptp_ha::dut_set_network_protocol                     #
#                         ptp_ha::dut_set_vlan                                 #
#                         ptp_ha::dut_set_vlan_priority                        #
#                         ptp_ha::dut_set_ipv4_address                         #
#                         ptp_ha::dut_set_priority1                            #
#                         ptp_ha::dut_set_priority2                            #
#                         ptp_ha::dut_tcr_rcr_cr_enable                        #
#                         ptp_ha::dut_l1sync_enable                            #
#                         ptp_ha::dut_l1sync_opt_params_disable                #
#                         ptp_ha::dut_masteronly_disable                       #
#                         ptp_ha::dut_set_announce_interval                    #
#                         ptp_ha::dut_set_announce_timeout                     #
#                         ptp_ha::dut_set_sync_interval                        #
#                         ptp_ha::dut_set_delayreq_interval                    #
#                         ptp_ha::dut_set_pdelayreq_interval                   #
#                         ptp_ha::dut_set_l1sync_interval                      #
#                         ptp_ha::dut_set_l1sync_timeout                       #
#                         ptp_ha::dut_set_egress_latency                       #
#                         ptp_ha::dut_set_ingress_latency                      #
#                         ptp_ha::dut_set_constant_asymmetry                   #
#                         ptp_ha::dut_set_scaled_delay_coefficient             #
#                         ptp_ha::dut_set_asymmetry_correction                 #
#                         ptp_ha::dut_external_port_config_disable             #
#                         ptp_ha::dut_set_epc_desired_state                    #
#                                                                              #
# USAGE              :                                                         #
#                                                                              #
#     ptp_ha::dut_configure_setup_003                                          #
#                                                                              #
################################################################################

proc ptp_ha::dut_configure_setup_003 {args} {

    array set param $args

    global env ptp_ha_env

    set ip_address_list        [list $env(dut_TEST_ADDRESS1) $env(dut_TEST_ADDRESS2)]
    set net_mask_list          [list $env(dut_TEST_MASK1) $env(dut_TEST_MASK2)]
    set vlan_id_1              $::ptp_ha_vlan_id
    set vlan_id_2              $::ptp_ha_vlan_id_2

    set vlan_id_list           [list $vlan_id_1 $vlan_id_2]

    set vlan_priority          $ptp_ha_env(DUT_PTP_HA_VLAN_PRIORITY)

    set ptp_vlan_encap         $ptp_ha_env(DUT_PTP_HA_IS_VLAN_ENCAP)

    set clock_mode             $::PTP_DEVICE_TYPE(BC)
    set clock_step             $ptp_ha_env(DUT_PTP_HA_CLOCK_STEP)
    set network_protocol       $ptp_ha_env(DUT_PTP_HA_TRANS_TYPE)
    set communication_mode     $ptp_ha_env(DUT_PTP_HA_COM_MODEL)
    set priority1              $ptp_ha_env(DUT_PTP_HA_PRIORITY1)
    set priority2              $ptp_ha_env(DUT_PTP_HA_PRIORITY2)
    set announce_interval      $ptp_ha_env(DUT_PTP_HA_ANNOUNCE_INTERVAL)
    set announce_timeout       $ptp_ha_env(DUT_PTP_HA_ANNOUNCE_TIMEOUT)
    set sync_interval          $ptp_ha_env(DUT_PTP_HA_SYNC_INTERVAL)
    set delayreq_interval      $ptp_ha_env(DUT_PTP_HA_DELAY_REQ_INTERVAL)
    set L1syncinterval         $ptp_ha_env(DUT_PTP_HA_L1SYNC_INTERVAL)
    set L1SyncReceiptTimeout   $ptp_ha_env(DUT_PTP_HA_L1SYNC_TIMEOUT)

    set egress_latency         $ptp_ha_env(DUT_PTP_HA_EGRESS_LATENCY)
    set ingress_latency        $ptp_ha_env(DUT_PTP_HA_INGRESS_LATENCY)
    set constant_asymmetry     $ptp_ha_env(DUT_PTP_HA_CONSTANT_ASYMMETRY)
    set delay_coefficient      $ptp_ha_env(DUT_PTP_HA_SCALED_DELAY_COEFFICIENT)

    set port_list              [list $::dut_port_num_1 $::dut_port_num_2]
    set domain                 $::ptp_dut_default_domain_number
    set delay_mechanism        $::DUT_PTP_DELAY_MECHANISM(E2E)

    set ptp_version            $::ptp_version

    ### Check Input parameters ###

    set env(TEE_ATTEST_MANUAL_PROMPT)      0

    ###########################################################################
    #  1) Ensure the DUT port P1 admin status Up.                             #
    ###########################################################################

    if {![ptp_ha::dut_port_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    if {![ptp_ha::dut_port_enable\
                    -port_num               [lindex $port_list 1]]} {

        return 0
    }

    ###########################################################################
    #  2) Ensure PTP global on the DUT.                                       #
    ###########################################################################

    if {![ptp_ha::dut_global_ptp_enable]} {

        return 0
    }

    ###########################################################################
    #  3) Ensure PTP enabled on the port P1 on the DUT.                       #
    ###########################################################################

    if {![ptp_ha::dut_port_ptp_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    if {![ptp_ha::dut_port_ptp_enable\
                    -port_num               [lindex $port_list 1]]} {

        return 0
    }

    ##############################################################################
    #  4) Configure Domain.                                                      #
    ##############################################################################

    if {![ptp_ha::dut_set_domain\
                    -clock_mode             $clock_mode\
                    -domain                 $domain ]} {

        return 0
    }

    ###########################################################################
    #  5) Ensure PTP set clock mode                                           #
    ###########################################################################

    if {![ptp_ha::dut_set_clock_mode\
                    -clock_mode             $clock_mode\
                    -port_num               [lindex $port_list 0] ]} {

        return 0
    }

    if {![ptp_ha::dut_set_clock_mode\
                    -clock_mode             $clock_mode\
                    -port_num               [lindex $port_list 1] ]} {

        return 0
    }

    ###########################################################################
    #  6) Ensure PTP set clock step                                           #
    ###########################################################################

    if {![ptp_ha::dut_set_clock_step\
                    -port_num               [lindex $port_list 0]\
                    -clock_mode             $clock_mode\
                    -clock_step             $clock_step]} {

        return 0
    }

    if {![ptp_ha::dut_set_clock_step\
                    -port_num               [lindex $port_list 1]\
                    -clock_mode             $clock_mode\
                    -clock_step             $clock_step]} {

        return 0
    }

    ##############################################################################
    #  7) Configure Delay Mechanism.                                             #
    ##############################################################################

    if {![ptp_ha::dut_set_delay_mechanism\
                    -delay_mechanism        $delay_mechanism\
                    -clock_mode             $clock_mode\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    if {![ptp_ha::dut_set_delay_mechanism\
                    -delay_mechanism        $delay_mechanism\
                    -clock_mode             $clock_mode\
                    -port_num               [lindex $port_list 1]]} {

        return 0
    }

    ##############################################################################
    #  8) Configure Communication mode.                                          #
    ##############################################################################

    if {![ptp_ha::dut_set_communication_mode\
                   -port_num                [lindex $port_list 0]\
                   -clock_mode              $clock_mode\
                   -ptp_version             $ptp_version\
                   -communication_mode      $communication_mode]} {

        return 0
    }

    if {![ptp_ha::dut_set_communication_mode\
                   -port_num                [lindex $port_list 1]\
                   -clock_mode              $clock_mode\
                   -ptp_version             $ptp_version\
                   -communication_mode      $communication_mode]} {

        return 0
    }

    ###########################################################################
    #  9) Set Network protocol                                                #
    ###########################################################################

    if {![ptp_ha::dut_set_network_protocol\
                    -port_num               [lindex $port_list 0]\
                    -clock_mode             $clock_mode\
                    -network_protocol       $network_protocol]} {

        return 0
    }

    if {![ptp_ha::dut_set_network_protocol\
                    -port_num               [lindex $port_list 1]\
                    -clock_mode             $clock_mode\
                    -network_protocol       $network_protocol]} {

        return 0
    }

    if {$ptp_vlan_encap == "true"} {

        ##############################################################################
        # 10) Create VLAN VLAN-A and VLAN-B and associate ports P1 and P2 to it.     #
        ##############################################################################

        if {![ptp_ha::dut_set_vlan\
                        -vlan_id                [lindex $vlan_id_list 0]\
                        -port_num               [lindex $port_list 0]]} {

            return 0
        }

        if {![ptp_ha::dut_set_vlan\
                        -vlan_id                [lindex $vlan_id_list 1]\
                        -port_num               [lindex $port_list 1]]} {

            return 0
        }

        ##############################################################################
        # 11) Create VLAN VLAN-A and VLAN-B and associate ports P1 and P2 to it.     #
        ##############################################################################

        if {![ptp_ha::dut_set_vlan_priority\
                        -vlan_id                [lindex $vlan_id_list 0]\
                        -vlan_priority          $vlan_priority\
                        -port_num               [lindex $port_list 0]]} {

            return 0
        }

        if {![ptp_ha::dut_set_vlan_priority\
                        -vlan_id                [lindex $vlan_id_list 1]\
                        -vlan_priority          $vlan_priority\
                        -port_num               [lindex $port_list 1]]} {

            return 0
        }
    }

    if {$::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4)} {

        ##############################################################################
        # 12) Set IP address.                                                        #
        ##############################################################################

        if {![ptp_ha::dut_set_ipv4_address\
                        -port_num              [lindex $port_list 0]\
                        -ip_address            [lindex $ip_address_list 0]\
                        -net_mask              [lindex $net_mask_list 0]\
                        -vlan_id               [lindex $vlan_id_list 0]]} {

            return 0
        }

        if {![ptp_ha::dut_set_ipv4_address\
                        -port_num              [lindex $port_list 1]\
                        -ip_address            [lindex $ip_address_list 1]\
                        -net_mask              [lindex $net_mask_list 1]\
                        -vlan_id               [lindex $vlan_id_list 1]]} {

            return 0
        }
    }

    ##############################################################################
    # 13) Configure Priority1.                                                   #
    ##############################################################################

    if {![ptp_ha::dut_set_priority1\
                    -clock_mode             $clock_mode\
                    -priority1              $priority1]} {

        return 0
    }

    ##############################################################################
    # 14) Configure Priority2.                                                   #
    ##############################################################################

    if {![ptp_ha::dut_set_priority2\
                    -clock_mode             $clock_mode\
                    -priority2              $priority2]} {

        return 0
    }

    ###############################################################################
    # 15) Enable txcoherentisRequired, rxcoherentisRequired,                      #
    #     congruentIsRequired.                                                    #
    ###############################################################################

    if {![ptp_ha::dut_tcr_rcr_cr_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    if {![ptp_ha::dut_tcr_rcr_cr_enable\
                    -port_num               [lindex $port_list 1]]} {

        return 0
    }

    ###############################################################################
    # 16) Enable L1 Sync on DUT.                                                  #
    ###############################################################################

    if {![ptp_ha::dut_l1sync_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    if {![ptp_ha::dut_l1sync_enable\
                    -port_num               [lindex $port_list 1]]} {

        return 0
    }

    ###############################################################################
    # 17) Disable optParametersConfigured.                                        #
    ###############################################################################

    if {![ptp_ha::dut_l1sync_opt_params_disable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###############################################################################
    # 17) Disable masterOnly.                                                     #
    ###############################################################################

    if {![ptp_ha::dut_masteronly_disable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    if {![ptp_ha::dut_masteronly_disable\
                    -port_num               [lindex $port_list 1]]} {

        return 0
    }

    ##############################################################################
    # 18) Configure Announce Interval.                                           #
    ##############################################################################

    if {![ptp_ha::dut_set_announce_interval\
                    -announce_interval      $announce_interval\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    if {![ptp_ha::dut_set_announce_interval\
                    -announce_interval      $announce_interval\
                    -port_num               [lindex $port_list 1]]} {

        return 0
    }

    ##############################################################################
    # 19) Configure Announce Timeout.                                            #
    ##############################################################################

    if {![ptp_ha::dut_set_announce_timeout\
                    -announce_timeout       $announce_timeout\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    if {![ptp_ha::dut_set_announce_timeout\
                    -announce_timeout       $announce_timeout\
                    -port_num               [lindex $port_list 1]]} {

        return 0
    }

    ##############################################################################
    # 20) Configure Sync Interval.                                               #
    ##############################################################################

    if {![ptp_ha::dut_set_sync_interval\
                    -sync_interval          $sync_interval\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    if {![ptp_ha::dut_set_sync_interval\
                    -sync_interval          $sync_interval\
                    -port_num               [lindex $port_list 1]]} {

        return 0
    }

    ##############################################################################
    # 21) Configure delayRequestInterval                                         #
    ##############################################################################

    if {![ptp_ha::dut_set_delayreq_interval\
                    -delayreq_interval      $delayreq_interval\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    if {![ptp_ha::dut_set_delayreq_interval\
                    -delayreq_interval      $delayreq_interval\
                    -port_num               [lindex $port_list 1]]} {

        return 0
    }

    ##############################################################################
    # 22) Configure logL1SyncInterval.                                           #
    ##############################################################################

    if {![ptp_ha::dut_set_l1sync_interval\
                    -port_num               [lindex $port_list 0]\
                    -l1sync_interval        $L1syncinterval]} {

        return 0
    }

    if {![ptp_ha::dut_set_l1sync_interval\
                    -port_num               [lindex $port_list 1]\
                    -l1sync_interval        $L1syncinterval]} {

        return 0
    }

    ##############################################################################
    # 23) Configure L1SyncReceiptTimeout.                                        #
    ##############################################################################

    if {![ptp_ha::dut_set_l1sync_timeout\
                    -port_num               [lindex $port_list 0]\
                    -l1sync_timeout         $L1SyncReceiptTimeout]} {

        return 0
    }

    if {![ptp_ha::dut_set_l1sync_timeout\
                    -port_num               [lindex $port_list 1]\
                    -l1sync_timeout         $L1SyncReceiptTimeout]} {

        return 0
    }

    ##############################################################################
    # 24) Configure timestampCorrectionPortDS.egressLatency.                     #
    ##############################################################################

    if {![ptp_ha::dut_set_egress_latency\
                    -port_num              [lindex $port_list 0]\
                    -latency               $egress_latency]} {

        return 0
    }

    if {![ptp_ha::dut_set_egress_latency\
                    -port_num              [lindex $port_list 1]\
                    -latency               $egress_latency]} {

        return 0
    }

    ##############################################################################
    # 25) Configure timestampCorrectionPortDS.ingressLatency.                    #
    ##############################################################################

    if {![ptp_ha::dut_set_ingress_latency\
                    -port_num              [lindex $port_list 0]\
                    -latency               $ingress_latency]} {

        return 0
    }

    if {![ptp_ha::dut_set_ingress_latency\
                    -port_num              [lindex $port_list 1]\
                    -latency               $ingress_latency]} {

        return 0
    }

    ##############################################################################
    # 26) Configure asymmetryCorrectionPortDS.constantAsymmetry.                 #
    ##############################################################################

    if {![ptp_ha::dut_set_constant_asymmetry\
                    -port_num              [lindex $port_list 0]\
                    -constant_asymmetry    $constant_asymmetry]} {

        return 0
    }

    if {![ptp_ha::dut_set_constant_asymmetry\
                    -port_num              [lindex $port_list 1]\
                    -constant_asymmetry    $constant_asymmetry]} {

        return 0
    }

    ##############################################################################
    # 27) Configure asymmetryCorrectionPortDS.scaledDelayCoefficient.            #
    ##############################################################################

    if {![ptp_ha::dut_set_scaled_delay_coefficient\
                    -port_num              [lindex $port_list 0]\
                    -delay_coefficient     $delay_coefficient]} {

        return 0
    }

    if {![ptp_ha::dut_set_scaled_delay_coefficient\
                    -port_num              [lindex $port_list 1]\
                    -delay_coefficient     $delay_coefficient]} {

        return 0
    }

    ##############################################################################
    # 28) Configure asymmetryCorrectionPortDS.enable.                            #
    ##############################################################################

    if {![ptp_ha::dut_set_asymmetry_correction\
                    -port_num              [lindex $port_list 0]]} {

        return 0
    }

    if {![ptp_ha::dut_set_asymmetry_correction\
                    -port_num              [lindex $port_list 1]]} {

        return 0
    }

    ##############################################################################
    # 29) Disable externalPortConfigurationEnabled.                              #
    ##############################################################################

    if {![ptp_ha::dut_external_port_config_disable\
                    -port_num              [lindex $port_list 0]]} {

        return 0
    }

    if {![ptp_ha::dut_external_port_config_disable\
                    -port_num              [lindex $port_list 1]]} {

        return 0
    }

    set env(TEE_ATTEST_MANUAL_PROMPT)      1

    return 1
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_configure_setup_004                         #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on success (If the DUT is configured for test setup 4)#
#                                                                              #
#                      0 on failure (If the DUT could not be configured for    #
#                                    test setup 4).                            #
#                                                                              #
#  DEFINITION        : This function configures the parameters required for the#
#                      test setup 4 at the DUT.                                #
#                                                                              #
#                      This function in turn calls below functions             #
#                                                                              #
#                         ptp_ha::dut_port_enable                              #
#                         ptp_ha::dut_global_ptp_enable                        #
#                         ptp_ha::dut_port_ptp_enable                          #
#                         ptp_ha::dut_set_domain                               #
#                         ptp_ha::dut_set_clock_mode                           #
#                         ptp_ha::dut_set_clock_step                           #
#                         ptp_ha::dut_l1sync_enable                            #
#                         ptp_ha::dut_set_vlan                                 #
#                         ptp_ha::dut_set_vlan_priority                        #
#                         ptp_ha::dut_set_ipv4_address                         #
#                                                                              #
# USAGE              :                                                         #
#                                                                              #
#     ptp_ha::dut_configure_setup_004                                          #
#                                                                              #
################################################################################

proc ptp_ha::dut_configure_setup_004 {args} {

    array set param $args

    global env ptp_ha_env

    set ip_address             $env(dut_TEST_ADDRESS1)
    set net_mask               $env(dut_TEST_MASK1)

    set vlan_priority          $ptp_ha_env(DUT_PTP_HA_VLAN_PRIORITY)

    set ptp_vlan_encap         $ptp_ha_env(DUT_PTP_HA_IS_VLAN_ENCAP)

    set clock_mode             $ptp_ha_env(DUT_PTP_HA_DEVCIE_TYPE)
    set clock_step             $ptp_ha_env(DUT_PTP_HA_CLOCK_STEP)
    set network_protocol       $ptp_ha_env(DUT_PTP_HA_TRANS_TYPE)
    set communication_mode     $ptp_ha_env(DUT_PTP_HA_COM_MODEL)

    set port_list              $::dut_port_num_1
    set domain                 $::ptp_dut_default_domain_number
    set delay_mechanism        $::DUT_PTP_DELAY_MECHANISM(E2E)

    set ptp_version            $::ptp_version

    set vlan_id                $::ptp_ha_vlan_id

    ### Check Input parameters ###

    set env(TEE_ATTEST_MANUAL_PROMPT)      0

    ###########################################################################
    #  1) Ensure the DUT port P1 admin status Up.                             #
    ###########################################################################

    if {![ptp_ha::dut_port_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###########################################################################
    #  2) Ensure PTP global on the DUT.                                       #
    ###########################################################################

    if {![ptp_ha::dut_global_ptp_enable]} {

        return 0
    }

    ###########################################################################
    #  3) Ensure PTP enabled on the port P1 on the DUT.                       #
    ###########################################################################

    if {![ptp_ha::dut_port_ptp_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    #  4) Configure Domain.                                                      #
    ##############################################################################

    if {![ptp_ha::dut_set_domain\
                    -clock_mode             $clock_mode\
                    -domain                 $domain ]} {

        return 0
    }

    ###########################################################################
    #  5) Ensure PTP set clock mode                                           #
    ###########################################################################

    if {![ptp_ha::dut_set_clock_mode\
                    -clock_mode             $clock_mode\
                    -port_num               [lindex $port_list 0] ]} {

        return 0
    }

    ###########################################################################
    #  6) Ensure PTP set clock step                                           #
    ###########################################################################

    if {![ptp_ha::dut_set_clock_step\
                    -port_num               [lindex $port_list 0]\
                    -clock_mode             $clock_mode\
                    -clock_step             $clock_step]} {

        return 0
    }

    ##############################################################################
    #  7) Configure Delay Mechanism.                                             #
    ##############################################################################

    if {![ptp_ha::dut_set_delay_mechanism\
                    -delay_mechanism        $delay_mechanism\
                    -clock_mode             $clock_mode\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    #  8) Configure Communication mode.                                          #
    ##############################################################################

    if {![ptp_ha::dut_set_communication_mode\
                   -port_num                [lindex $port_list 0]\
                   -clock_mode              $clock_mode\
                   -ptp_version             $ptp_version\
                   -communication_mode      $communication_mode]} {

        return 0
    }

    ###########################################################################
    #  9) Set Network protocol                                                #
    ###########################################################################

    if {![ptp_ha::dut_set_network_protocol\
                    -port_num               [lindex $port_list 0]\
                    -clock_mode             $clock_mode\
                    -network_protocol       $network_protocol]} {

        return 0
    }

    ###############################################################################
    # 10) Enable L1 Sync on DUT.                                                  #
    ###############################################################################

    if {![ptp_ha::dut_l1sync_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    if {$ptp_vlan_encap == "true"} {

        ##############################################################################
        # 11) Create VLAN VLAN-A and associate port P1 to it.                        #
        ##############################################################################

        if {![ptp_ha::dut_set_vlan\
                        -vlan_id                $vlan_id\
                        -port_num               [lindex $port_list 0]]} {

            return 0
        }

        ##############################################################################
        # 12) Create VLAN VLAN-A and associate port P1 to it.                        #
        ##############################################################################

        if {![ptp_ha::dut_set_vlan_priority\
                        -vlan_id                $vlan_id\
                        -vlan_priority          $vlan_priority\
                        -port_num               [lindex $port_list 0]]} {

            return 0
        }
    }

    if {$::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4)} {

        ##############################################################################
        # 13) Set IP address.                                                        #
        ##############################################################################

        if {![ptp_ha::dut_set_ipv4_address\
                        -port_num              [lindex $port_list 0]\
                        -ip_address            $ip_address\
                        -net_mask              $net_mask\
                        -vlan_id               $vlan_id]} {

            return 0
        }
    }

    set env(TEE_ATTEST_MANUAL_PROMPT)      1

    return 1
}


#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_configure_setup_005                         #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on success (If the DUT is configured for test setup 5)#
#                                                                              #
#                      0 on failure (If the DUT could not be configured for    #
#                                    test setup 5).                            #
#                                                                              #
#  DEFINITION        : This function configures the parameters required for the#
#                      test setup 5 at the DUT.                                #
#                                                                              #
#                      This function in turn calls below functions             #
#                                                                              #
#                         ptp_ha::dut_port_enable                              #
#                         ptp_ha::dut_global_ptp_enable                        #
#                         ptp_ha::dut_port_ptp_enable                          #
#                         ptp_ha::dut_set_domain                               #
#                         ptp_ha::dut_set_clock_mode                           #
#                         ptp_ha::dut_set_clock_step                           #
#                         ptp_ha::dut_set_delay_mechanism                      #
#                         ptp_ha::dut_set_communication_mode                   #
#                         ptp_ha::dut_set_network_protocol                     #
#                         ptp_ha::dut_set_vlan                                 #
#                         ptp_ha::dut_set_vlan_priority                        #
#                         ptp_ha::dut_set_ipv4_address                         #
#                         ptp_ha::dut_set_priority1                            #
#                         ptp_ha::dut_set_priority2                            #
#                         ptp_ha::dut_tcr_rcr_cr_enable                        #
#                         ptp_ha::dut_l1sync_enable                            #
#                         ptp_ha::dut_l1sync_opt_params_disable                #
#                         ptp_ha::dut_masteronly_disable                       #
#                         ptp_ha::dut_set_announce_interval                    #
#                         ptp_ha::dut_set_announce_timeout                     #
#                         ptp_ha::dut_set_sync_interval                        #
#                         ptp_ha::dut_set_delayreq_interval                    #
#                         ptp_ha::dut_set_pdelayreq_interval                   #
#                         ptp_ha::dut_set_l1sync_interval                      #
#                         ptp_ha::dut_set_l1sync_timeout                       #
#                         ptp_ha::dut_set_egress_latency                       #
#                         ptp_ha::dut_set_ingress_latency                      #
#                         ptp_ha::dut_set_constant_asymmetry                   #
#                         ptp_ha::dut_set_scaled_delay_coefficient             #
#                         ptp_ha::dut_set_asymmetry_correction                 #
#                         ptp_ha::dut_external_port_config_disable             #
#                         ptp_ha::dut_set_epc_desired_state                    #
#                                                                              #
# USAGE              :                                                         #
#                                                                              #
#     ptp_ha::dut_configure_setup_005                                          #
#                                                                              #
################################################################################

proc ptp_ha::dut_configure_setup_005 {args} {

    array set param $args

    global env ptp_ha_env

    set ip_address             $env(dut_TEST_ADDRESS1)
    set net_mask               $env(dut_TEST_MASK1)

    set vlan_priority          $ptp_ha_env(DUT_PTP_HA_VLAN_PRIORITY)

    set ptp_vlan_encap         $ptp_ha_env(DUT_PTP_HA_IS_VLAN_ENCAP)

    set clock_mode             $::PTP_DEVICE_TYPE(OC)
    set clock_step             $ptp_ha_env(DUT_PTP_HA_CLOCK_STEP)
    set network_protocol       $ptp_ha_env(DUT_PTP_HA_TRANS_TYPE)
    set communication_mode     $ptp_ha_env(DUT_PTP_HA_COM_MODEL)
    set priority1              $ptp_ha_env(DUT_PTP_HA_PRIORITY1)
    set priority2              $ptp_ha_env(DUT_PTP_HA_PRIORITY2)
    set announce_interval      $ptp_ha_env(DUT_PTP_HA_ANNOUNCE_INTERVAL)
    set announce_timeout       $ptp_ha_env(DUT_PTP_HA_ANNOUNCE_TIMEOUT)
    set sync_interval          $ptp_ha_env(DUT_PTP_HA_SYNC_INTERVAL)
    set delayreq_interval      $ptp_ha_env(DUT_PTP_HA_DELAY_REQ_INTERVAL)
    set L1syncinterval         $ptp_ha_env(DUT_PTP_HA_L1SYNC_INTERVAL)
    set L1SyncReceiptTimeout   $ptp_ha_env(DUT_PTP_HA_L1SYNC_TIMEOUT)

    set egress_latency         $ptp_ha_env(DUT_PTP_HA_EGRESS_LATENCY)
    set ingress_latency        $ptp_ha_env(DUT_PTP_HA_INGRESS_LATENCY)
    set constant_asymmetry     $ptp_ha_env(DUT_PTP_HA_CONSTANT_ASYMMETRY)
    set delay_coefficient      $ptp_ha_env(DUT_PTP_HA_SCALED_DELAY_COEFFICIENT)

    set port_list              $::dut_port_num_1
    set domain                 $::ptp_dut_default_domain_number
    set delay_mechanism        $::DUT_PTP_DELAY_MECHANISM(E2E)

    set ptp_version            $::ptp_version

    set vlan_id                $::ptp_ha_vlan_id

    ### Check Input parameters ###

    set env(TEE_ATTEST_MANUAL_PROMPT)      0

    ###########################################################################
    #  1) Ensure the DUT port P1 admin status Up.                             #
    ###########################################################################

    if {![ptp_ha::dut_port_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###########################################################################
    #  2) Ensure PTP global on the DUT.                                       #
    ###########################################################################

    if {![ptp_ha::dut_global_ptp_enable]} {

        return 0
    }

    ###########################################################################
    #  3) Ensure PTP enabled on the port P1 on the DUT.                       #
    ###########################################################################

    if {![ptp_ha::dut_port_ptp_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    #  4) Configure Domain.                                                      #
    ##############################################################################

    if {![ptp_ha::dut_set_domain\
                    -clock_mode             $clock_mode\
                    -domain                 $domain ]} {

        return 0
    }

    ###########################################################################
    #  5) Ensure PTP set clock mode                                           #
    ###########################################################################

    if {![ptp_ha::dut_set_clock_mode\
                    -clock_mode             $clock_mode\
                    -port_num               [lindex $port_list 0] ]} {

        return 0
    }

    ###########################################################################
    #  6) Ensure PTP set clock step                                           #
    ###########################################################################

    if {![ptp_ha::dut_set_clock_step\
                    -port_num               [lindex $port_list 0]\
                    -clock_mode             $clock_mode\
                    -clock_step             $clock_step]} {

        return 0
    }

    ##############################################################################
    #  7) Configure Delay Mechanism.                                             #
    ##############################################################################

    if {![ptp_ha::dut_set_delay_mechanism\
                    -delay_mechanism        $delay_mechanism\
                    -clock_mode             $clock_mode\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    #  8) Configure Communication mode.                                          #
    ##############################################################################

    if {![ptp_ha::dut_set_communication_mode\
                   -port_num                [lindex $port_list 0]\
                   -clock_mode              $clock_mode\
                   -ptp_version             $ptp_version\
                   -communication_mode      $communication_mode]} {

        return 0
    }

    ###########################################################################
    #  9) Set Network protocol                                                #
    ###########################################################################

    if {![ptp_ha::dut_set_network_protocol\
                    -port_num               [lindex $port_list 0]\
                    -clock_mode             $clock_mode\
                    -network_protocol       $network_protocol]} {

        return 0
    }

    if {$ptp_vlan_encap == "true"} {

        ##############################################################################
        # 10) Create VLAN VLAN-A and associate port P1 to it.                        #
        ##############################################################################

        if {![ptp_ha::dut_set_vlan\
                        -vlan_id                $vlan_id\
                        -port_num               [lindex $port_list 0]]} {

            return 0
        }

        ##############################################################################
        # 11) Create VLAN VLAN-A and associate port P1 to it.                        #
        ##############################################################################

        if {![ptp_ha::dut_set_vlan_priority\
                        -vlan_id                $vlan_id\
                        -vlan_priority          $vlan_priority\
                        -port_num               [lindex $port_list 0]]} {

            return 0
        }
    }

    if {$::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4)} {

        ##############################################################################
        # 12) Set IP address.                                                        #
        ##############################################################################

        if {![ptp_ha::dut_set_ipv4_address\
                        -port_num              [lindex $port_list 0]\
                        -ip_address            $ip_address\
                        -net_mask              $net_mask\
                        -vlan_id               $vlan_id]} {

            return 0
        }
    }

    ##############################################################################
    # 13) Configure Priority1.                                                   #
    ##############################################################################

    if {![ptp_ha::dut_set_priority1\
                    -clock_mode             $clock_mode\
                    -priority1              $priority1]} {

        return 0
    }

    ##############################################################################
    # 14) Configure Priority2.                                                   #
    ##############################################################################

    if {![ptp_ha::dut_set_priority2\
                    -clock_mode             $clock_mode\
                    -priority2              $priority2]} {

        return 0
    }

    ###############################################################################
    # 15) Enable txcoherentisRequired, rxcoherentisRequired,                      #
    #     congruentIsRequired.                                                    #
    ###############################################################################

    if {![ptp_ha::dut_tcr_rcr_cr_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###############################################################################
    # 16) Enable L1 Sync on DUT.                                                  #
    ###############################################################################

    if {![ptp_ha::dut_l1sync_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###############################################################################
    # 17) Disable optParametersConfigured.                                        #
    ###############################################################################

    if {![ptp_ha::dut_l1sync_opt_params_disable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###############################################################################
    # 17) Disable masterOnly.                                                     #
    ###############################################################################

    if {![ptp_ha::dut_masteronly_disable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 18) Configure Announce Interval.                                           #
    ##############################################################################

    if {![ptp_ha::dut_set_announce_interval\
                    -announce_interval      $announce_interval\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 19) Configure Announce Timeout.                                            #
    ##############################################################################

    if {![ptp_ha::dut_set_announce_timeout\
                    -announce_timeout       $announce_timeout\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 20) Configure Sync Interval.                                               #
    ##############################################################################

    if {![ptp_ha::dut_set_sync_interval\
                    -sync_interval          $sync_interval\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 21) Configure delayRequestInterval                                         #
    ##############################################################################

    if {![ptp_ha::dut_set_delayreq_interval\
                    -delayreq_interval      $delayreq_interval\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 22) Configure logL1SyncInterval.                                           #
    ##############################################################################

    if {![ptp_ha::dut_set_l1sync_interval\
                    -port_num               [lindex $port_list 0]\
                    -l1sync_interval        $L1syncinterval]} {

        return 0
    }

    ##############################################################################
    # 23) Configure L1SyncReceiptTimeout.                                        #
    ##############################################################################

    if {![ptp_ha::dut_set_l1sync_timeout\
                    -port_num               [lindex $port_list 0]\
                    -l1sync_timeout         $L1SyncReceiptTimeout]} {

        return 0
    }

    ##############################################################################
    # 24) Configure timestampCorrectionPortDS.egressLatency.                     #
    ##############################################################################

    if {![ptp_ha::dut_set_egress_latency\
                    -port_num              [lindex $port_list 0]\
                    -latency               $egress_latency]} {

        return 0
    }

    ##############################################################################
    # 25) Configure timestampCorrectionPortDS.ingressLatency.                    #
    ##############################################################################

    if {![ptp_ha::dut_set_ingress_latency\
                    -port_num              [lindex $port_list 0]\
                    -latency               $ingress_latency]} {

        return 0
    }

    ##############################################################################
    # 26) Configure asymmetryCorrectionPortDS.constantAsymmetry.                 #
    ##############################################################################

    if {![ptp_ha::dut_set_constant_asymmetry\
                    -port_num              [lindex $port_list 0]\
                    -constant_asymmetry    $constant_asymmetry]} {

        return 0
    }

    ##############################################################################
    # 27) Configure asymmetryCorrectionPortDS.scaledDelayCoefficient.            #
    ##############################################################################

    if {![ptp_ha::dut_set_scaled_delay_coefficient\
                    -port_num              [lindex $port_list 0]\
                    -delay_coefficient     $delay_coefficient]} {

        return 0
    }

    ##############################################################################
    # 28) Configure asymmetryCorrectionPortDS.enable.                            #
    ##############################################################################

    if {![ptp_ha::dut_set_asymmetry_correction\
                    -port_num              [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 29) Disable externalPortConfigurationEnabled.                              #
    ##############################################################################

    if {![ptp_ha::dut_external_port_config_disable\
                    -port_num              [lindex $port_list 0]]} {

        return 0
    }

    set env(TEE_ATTEST_MANUAL_PROMPT)      1

    return 1
}


#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_configure_setup_006                         #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on success (If the DUT is configured for test setup 6)#
#                                                                              #
#                      0 on failure (If the DUT could not be configured for    #
#                                    test setup 6).                            #
#                                                                              #
#  DEFINITION        : This function configures the parameters required for the#
#                      test setup 6 at the DUT.                                #
#                                                                              #
#                      This function in turn calls below functions             #
#                                                                              #
#                         ptp_ha::dut_port_enable                              #
#                         ptp_ha::dut_global_ptp_enable                        #
#                         ptp_ha::dut_port_ptp_enable                          #
#                         ptp_ha::dut_set_domain                               #
#                         ptp_ha::dut_set_clock_mode                           #
#                         ptp_ha::dut_set_clock_step                           #
#                         ptp_ha::dut_set_delay_mechanism                      #
#                         ptp_ha::dut_set_communication_mode                   #
#                         ptp_ha::dut_set_network_protocol                     #
#                         ptp_ha::dut_set_vlan                                 #
#                         ptp_ha::dut_set_vlan_priority                        #
#                         ptp_ha::dut_set_ipv4_address                         #
#                         ptp_ha::dut_set_priority1                            #
#                         ptp_ha::dut_set_priority2                            #
#                         ptp_ha::dut_tcr_rcr_cr_enable                        #
#                         ptp_ha::dut_l1sync_enable                            #
#                         ptp_ha::dut_l1sync_opt_params_disable                #
#                         ptp_ha::dut_masteronly_disable                       #
#                         ptp_ha::dut_set_announce_interval                    #
#                         ptp_ha::dut_set_announce_timeout                     #
#                         ptp_ha::dut_set_sync_interval                        #
#                         ptp_ha::dut_set_delayreq_interval                    #
#                         ptp_ha::dut_set_pdelayreq_interval                   #
#                         ptp_ha::dut_set_l1sync_interval                      #
#                         ptp_ha::dut_set_l1sync_timeout                       #
#                         ptp_ha::dut_set_egress_latency                       #
#                         ptp_ha::dut_set_ingress_latency                      #
#                         ptp_ha::dut_set_constant_asymmetry                   #
#                         ptp_ha::dut_set_scaled_delay_coefficient             #
#                         ptp_ha::dut_set_asymmetry_correction                 #
#                         ptp_ha::dut_external_port_config_enable              #
#                         ptp_ha::dut_set_epc_desired_state                    #
#                                                                              #
# USAGE              :                                                         #
#                                                                              #
#     ptp_ha::dut_configure_setup_006                                          #
#                                                                              #
################################################################################

proc ptp_ha::dut_configure_setup_006 {args} {

    array set param $args

    global env ptp_ha_env

    set ip_address             $env(dut_TEST_ADDRESS1)
    set net_mask               $env(dut_TEST_MASK1)

    set vlan_priority          $ptp_ha_env(DUT_PTP_HA_VLAN_PRIORITY)

    set ptp_vlan_encap         $ptp_ha_env(DUT_PTP_HA_IS_VLAN_ENCAP)

    set clock_mode             $ptp_ha_env(DUT_PTP_HA_DEVCIE_TYPE)
    set clock_step             $ptp_ha_env(DUT_PTP_HA_CLOCK_STEP)
    set network_protocol       $ptp_ha_env(DUT_PTP_HA_TRANS_TYPE)
    set communication_mode     $ptp_ha_env(DUT_PTP_HA_COM_MODEL)
    set priority1              $ptp_ha_env(DUT_PTP_HA_PRIORITY1)
    set priority2              $ptp_ha_env(DUT_PTP_HA_PRIORITY2)
    set announce_interval      $ptp_ha_env(DUT_PTP_HA_ANNOUNCE_INTERVAL)
    set announce_timeout       $ptp_ha_env(DUT_PTP_HA_ANNOUNCE_TIMEOUT)
    set sync_interval          $ptp_ha_env(DUT_PTP_HA_SYNC_INTERVAL)
    set delayreq_interval      $ptp_ha_env(DUT_PTP_HA_DELAY_REQ_INTERVAL)
    set L1syncinterval         $ptp_ha_env(DUT_PTP_HA_L1SYNC_INTERVAL)
    set L1SyncReceiptTimeout   $ptp_ha_env(DUT_PTP_HA_L1SYNC_TIMEOUT)

    set egress_latency         $ptp_ha_env(DUT_PTP_HA_EGRESS_LATENCY)
    set ingress_latency        $ptp_ha_env(DUT_PTP_HA_INGRESS_LATENCY)
    set constant_asymmetry     $ptp_ha_env(DUT_PTP_HA_CONSTANT_ASYMMETRY)
    set delay_coefficient      $ptp_ha_env(DUT_PTP_HA_SCALED_DELAY_COEFFICIENT)

    set port_list              $::dut_port_num_1
    set domain                 $::ptp_dut_default_domain_number
    set delay_mechanism        $::DUT_PTP_DELAY_MECHANISM(E2E)

    set ptp_version            $::ptp_version

    set vlan_id                $::ptp_ha_vlan_id

    ### Check Input parameters ###

    set env(TEE_ATTEST_MANUAL_PROMPT)      0

    ###########################################################################
    #  1) Ensure the DUT port P1 admin status Up.                             #
    ###########################################################################

    if {![ptp_ha::dut_port_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###########################################################################
    #  2) Ensure PTP global on the DUT.                                       #
    ###########################################################################

    if {![ptp_ha::dut_global_ptp_enable]} {

        return 0
    }

    ###########################################################################
    #  3) Ensure PTP enabled on the port P1 on the DUT.                       #
    ###########################################################################

    if {![ptp_ha::dut_port_ptp_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    #  4) Configure Domain.                                                      #
    ##############################################################################

    if {![ptp_ha::dut_set_domain\
                    -clock_mode             $clock_mode\
                    -domain                 $domain ]} {

        return 0
    }

    ###########################################################################
    #  5) Ensure PTP set clock mode                                           #
    ###########################################################################

    if {![ptp_ha::dut_set_clock_mode\
                    -clock_mode             $clock_mode\
                    -port_num               [lindex $port_list 0] ]} {

        return 0
    }

    ###########################################################################
    #  6) Ensure PTP set clock step                                           #
    ###########################################################################

    if {![ptp_ha::dut_set_clock_step\
                    -port_num               [lindex $port_list 0]\
                    -clock_mode             $clock_mode\
                    -clock_step             $clock_step]} {

        return 0
    }

    ##############################################################################
    #  7) Configure Delay Mechanism.                                             #
    ##############################################################################

    if {![ptp_ha::dut_set_delay_mechanism\
                    -delay_mechanism        $delay_mechanism\
                    -clock_mode             $clock_mode\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    #  8) Configure Communication mode.                                          #
    ##############################################################################

    if {![ptp_ha::dut_set_communication_mode\
                   -port_num                [lindex $port_list 0]\
                   -clock_mode              $clock_mode\
                   -ptp_version             $ptp_version\
                   -communication_mode      $communication_mode]} {

        return 0
    }

    ###########################################################################
    #  9) Set Network protocol                                                #
    ###########################################################################

    if {![ptp_ha::dut_set_network_protocol\
                    -port_num               [lindex $port_list 0]\
                    -clock_mode             $clock_mode\
                    -network_protocol       $network_protocol]} {

        return 0
    }

    if {$ptp_vlan_encap == "true"} {

        ##############################################################################
        # 10) Create VLAN VLAN-A and associate port P1 to it.                        #
        ##############################################################################

        if {![ptp_ha::dut_set_vlan\
                        -vlan_id                $vlan_id\
                        -port_num               [lindex $port_list 0]]} {

            return 0
        }

        ##############################################################################
        # 11) Create VLAN VLAN-A and associate port P1 to it.                        #
        ##############################################################################

        if {![ptp_ha::dut_set_vlan_priority\
                        -vlan_id                $vlan_id\
                        -vlan_priority          $vlan_priority\
                        -port_num               [lindex $port_list 0]]} {

            return 0
        }
    }

    if {$::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4)} {

        ##############################################################################
        # 12) Set IP address.                                                        #
        ##############################################################################

        if {![ptp_ha::dut_set_ipv4_address\
                        -port_num              [lindex $port_list 0]\
                        -ip_address            $ip_address\
                        -net_mask              $net_mask\
                        -vlan_id               $vlan_id]} {

            return 0
        }
    }

    ##############################################################################
    # 13) Configure Priority1.                                                   #
    ##############################################################################

    if {![ptp_ha::dut_set_priority1\
                    -clock_mode             $clock_mode\
                    -priority1              $priority1]} {

        return 0
    }

    ##############################################################################
    # 14) Configure Priority2.                                                   #
    ##############################################################################

    if {![ptp_ha::dut_set_priority2\
                    -clock_mode             $clock_mode\
                    -priority2              $priority2]} {

        return 0
    }

    ###############################################################################
    # 15) Enable txcoherentisRequired, rxcoherentisRequired,                      #
    #     congruentIsRequired.                                                    #
    ###############################################################################

    if {![ptp_ha::dut_tcr_rcr_cr_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###############################################################################
    # 16) Enable L1 Sync on DUT.                                                  #
    ###############################################################################

    if {![ptp_ha::dut_l1sync_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###############################################################################
    # 17) Disable optParametersConfigured.                                        #
    ###############################################################################

    if {![ptp_ha::dut_l1sync_opt_params_disable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###############################################################################
    # 17) Disable masterOnly.                                                     #
    ###############################################################################

    if {![ptp_ha::dut_masteronly_disable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 18) Configure Announce Interval.                                           #
    ##############################################################################

    if {![ptp_ha::dut_set_announce_interval\
                    -announce_interval      $announce_interval\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 19) Configure Announce Timeout.                                            #
    ##############################################################################

    if {![ptp_ha::dut_set_announce_timeout\
                    -announce_timeout       $announce_timeout\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 20) Configure Sync Interval.                                               #
    ##############################################################################

    if {![ptp_ha::dut_set_sync_interval\
                    -sync_interval          $sync_interval\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 21) Configure delayRequestInterval                                         #
    ##############################################################################

    if {![ptp_ha::dut_set_delayreq_interval\
                    -delayreq_interval      $delayreq_interval\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 22) Configure logL1SyncInterval.                                           #
    ##############################################################################

    if {![ptp_ha::dut_set_l1sync_interval\
                    -port_num               [lindex $port_list 0]\
                    -l1sync_interval        $L1syncinterval]} {

        return 0
    }

    ##############################################################################
    # 23) Configure L1SyncReceiptTimeout.                                        #
    ##############################################################################

    if {![ptp_ha::dut_set_l1sync_timeout\
                    -port_num               [lindex $port_list 0]\
                    -l1sync_timeout         $L1SyncReceiptTimeout]} {

        return 0
    }

    ##############################################################################
    # 24) Configure timestampCorrectionPortDS.egressLatency.                     #
    ##############################################################################

    if {![ptp_ha::dut_set_egress_latency\
                    -port_num              [lindex $port_list 0]\
                    -latency               $egress_latency]} {

        return 0
    }

    ##############################################################################
    # 25) Configure timestampCorrectionPortDS.ingressLatency.                    #
    ##############################################################################

    if {![ptp_ha::dut_set_ingress_latency\
                    -port_num              [lindex $port_list 0]\
                    -latency               $ingress_latency]} {

        return 0
    }

    ##############################################################################
    # 26) Configure asymmetryCorrectionPortDS.constantAsymmetry.                 #
    ##############################################################################

    if {![ptp_ha::dut_set_constant_asymmetry\
                    -port_num              [lindex $port_list 0]\
                    -constant_asymmetry    $constant_asymmetry]} {

        return 0
    }

    ##############################################################################
    # 27) Configure asymmetryCorrectionPortDS.scaledDelayCoefficient.            #
    ##############################################################################

    if {![ptp_ha::dut_set_scaled_delay_coefficient\
                    -port_num              [lindex $port_list 0]\
                    -delay_coefficient     $delay_coefficient]} {

        return 0
    }

    ##############################################################################
    # 28) Configure asymmetryCorrectionPortDS.enable.                            #
    ##############################################################################

    if {![ptp_ha::dut_set_asymmetry_correction\
                    -port_num              [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 29) Enable externalPortConfigurationEnabled.                               #
    ##############################################################################

    if {![ptp_ha::dut_external_port_config_enable\
                    -port_num              [lindex $port_list 0]]} {

        return 0
    }

    set env(TEE_ATTEST_MANUAL_PROMPT)      1

    return 1
}


#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_configure_setup_007                         #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on success (If the DUT is configured for test setup 7)#
#                                                                              #
#                      0 on failure (If the DUT could not be configured for    #
#                                    test setup 7).                            #
#                                                                              #
#  DEFINITION        : This function configures the parameters required for the#
#                      test setup 7 at the DUT.                                #
#                                                                              #
#                      This function in turn calls below functions             #
#                                                                              #
#                         ptp_ha::dut_port_enable                              #
#                         ptp_ha::dut_global_ptp_enable                        #
#                         ptp_ha::dut_port_ptp_enable                          #
#                         ptp_ha::dut_set_domain                               #
#                         ptp_ha::dut_set_clock_mode                           #
#                         ptp_ha::dut_set_clock_step                           #
#                         ptp_ha::dut_set_delay_mechanism                      #
#                         ptp_ha::dut_set_communication_mode                   #
#                         ptp_ha::dut_set_network_protocol                     #
#                         ptp_ha::dut_set_vlan                                 #
#                         ptp_ha::dut_set_vlan_priority                        #
#                         ptp_ha::dut_set_ipv4_address                         #
#                         ptp_ha::dut_set_priority1                            #
#                         ptp_ha::dut_set_priority2                            #
#                         ptp_ha::dut_tcr_rcr_cr_enable                        #
#                         ptp_ha::dut_l1sync_enable                            #
#                         ptp_ha::dut_l1sync_opt_params_disable                #
#                         ptp_ha::dut_masteronly_disable                       #
#                         ptp_ha::dut_set_announce_interval                    #
#                         ptp_ha::dut_set_announce_timeout                     #
#                         ptp_ha::dut_set_sync_interval                        #
#                         ptp_ha::dut_set_delayreq_interval                    #
#                         ptp_ha::dut_set_pdelayreq_interval                   #
#                         ptp_ha::dut_set_l1sync_interval                      #
#                         ptp_ha::dut_set_l1sync_timeout                       #
#                         ptp_ha::dut_set_egress_latency                       #
#                         ptp_ha::dut_set_ingress_latency                      #
#                         ptp_ha::dut_set_constant_asymmetry                   #
#                         ptp_ha::dut_set_scaled_delay_coefficient             #
#                         ptp_ha::dut_set_asymmetry_correction                 #
#                         ptp_ha::dut_external_port_config_enable              #
#                         ptp_ha::dut_set_epc_desired_state                    #
#                                                                              #
# USAGE              :                                                         #
#                                                                              #
#     ptp_ha::dut_configure_setup_007                                          #
#                                                                              #
################################################################################

proc ptp_ha::dut_configure_setup_007 {args} {

    array set param $args

    global env ptp_ha_env

    set ip_address_list        [list $env(dut_TEST_ADDRESS1) $env(dut_TEST_ADDRESS2)]
    set net_mask_list          [list $env(dut_TEST_MASK1) $env(dut_TEST_MASK2)]

    set vlan_id_1              $::ptp_ha_vlan_id
    set vlan_id_2              $::ptp_ha_vlan_id_2

    set vlan_id_list           [list $vlan_id_1 $vlan_id_2]
    set vlan_priority          $ptp_ha_env(DUT_PTP_HA_VLAN_PRIORITY)

    set ptp_vlan_encap         $ptp_ha_env(DUT_PTP_HA_IS_VLAN_ENCAP)

    set clock_mode             $::PTP_DEVICE_TYPE(BC)
    set clock_step             $ptp_ha_env(DUT_PTP_HA_CLOCK_STEP)
    set network_protocol       $ptp_ha_env(DUT_PTP_HA_TRANS_TYPE)
    set communication_mode     $ptp_ha_env(DUT_PTP_HA_COM_MODEL)
    set priority1              $ptp_ha_env(DUT_PTP_HA_PRIORITY1)
    set priority2              $ptp_ha_env(DUT_PTP_HA_PRIORITY2)
    set announce_interval      $ptp_ha_env(DUT_PTP_HA_ANNOUNCE_INTERVAL)
    set announce_timeout       $ptp_ha_env(DUT_PTP_HA_ANNOUNCE_TIMEOUT)
    set sync_interval          $ptp_ha_env(DUT_PTP_HA_SYNC_INTERVAL)
    set delayreq_interval      $ptp_ha_env(DUT_PTP_HA_DELAY_REQ_INTERVAL)
    set L1syncinterval         $ptp_ha_env(DUT_PTP_HA_L1SYNC_INTERVAL)
    set L1SyncReceiptTimeout   $ptp_ha_env(DUT_PTP_HA_L1SYNC_TIMEOUT)

    set egress_latency         $ptp_ha_env(DUT_PTP_HA_EGRESS_LATENCY)
    set ingress_latency        $ptp_ha_env(DUT_PTP_HA_INGRESS_LATENCY)
    set constant_asymmetry     $ptp_ha_env(DUT_PTP_HA_CONSTANT_ASYMMETRY)
    set delay_coefficient      $ptp_ha_env(DUT_PTP_HA_SCALED_DELAY_COEFFICIENT)

    set port_list              [list $::dut_port_num_1 $::dut_port_num_2]
    set domain                 $::ptp_dut_default_domain_number
    set delay_mechanism        $::DUT_PTP_DELAY_MECHANISM(E2E)

    set ptp_version            $::ptp_version

    ### Check Input parameters ###

    set env(TEE_ATTEST_MANUAL_PROMPT)      0

    ###########################################################################
    #  1) Ensure the DUT port P1 admin status Up.                             #
    ###########################################################################

    if {![ptp_ha::dut_port_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    if {![ptp_ha::dut_port_enable\
                    -port_num               [lindex $port_list 1]]} {

        return 0
    }

    ###########################################################################
    #  2) Ensure PTP global on the DUT.                                       #
    ###########################################################################

    if {![ptp_ha::dut_global_ptp_enable]} {

        return 0
    }

    ###########################################################################
    #  3) Ensure PTP enabled on the port P1 on the DUT.                       #
    ###########################################################################

    if {![ptp_ha::dut_port_ptp_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    if {![ptp_ha::dut_port_ptp_enable\
                    -port_num               [lindex $port_list 1]]} {

        return 0
    }

    ##############################################################################
    #  4) Configure Domain.                                                      #
    ##############################################################################

    if {![ptp_ha::dut_set_domain\
                    -clock_mode             $clock_mode\
                    -domain                 $domain ]} {

        return 0
    }

    ###########################################################################
    #  5) Ensure PTP set clock mode                                           #
    ###########################################################################

    if {![ptp_ha::dut_set_clock_mode\
                    -clock_mode             $clock_mode\
                    -port_num               [lindex $port_list 0] ]} {

        return 0
    }

    if {![ptp_ha::dut_set_clock_mode\
                    -clock_mode             $clock_mode\
                    -port_num               [lindex $port_list 1] ]} {

        return 0
    }

    ###########################################################################
    #  6) Ensure PTP set clock step                                           #
    ###########################################################################

    if {![ptp_ha::dut_set_clock_step\
                    -port_num               [lindex $port_list 0]\
                    -clock_mode             $clock_mode\
                    -clock_step             $clock_step]} {

        return 0
    }

    if {![ptp_ha::dut_set_clock_step\
                    -port_num               [lindex $port_list 1]\
                    -clock_mode             $clock_mode\
                    -clock_step             $clock_step]} {

        return 0
    }

    ##############################################################################
    #  7) Configure Delay Mechanism.                                             #
    ##############################################################################

    if {![ptp_ha::dut_set_delay_mechanism\
                    -delay_mechanism        $delay_mechanism\
                    -clock_mode             $clock_mode\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    if {![ptp_ha::dut_set_delay_mechanism\
                    -delay_mechanism        $delay_mechanism\
                    -clock_mode             $clock_mode\
                    -port_num               [lindex $port_list 1]]} {

        return 0
    }

    ##############################################################################
    #  8) Configure Communication mode.                                          #
    ##############################################################################

    if {![ptp_ha::dut_set_communication_mode\
                   -port_num                [lindex $port_list 0]\
                   -clock_mode              $clock_mode\
                   -ptp_version             $ptp_version\
                   -communication_mode      $communication_mode]} {

        return 0
    }

    if {![ptp_ha::dut_set_communication_mode\
                   -port_num                [lindex $port_list 1]\
                   -clock_mode              $clock_mode\
                   -ptp_version             $ptp_version\
                   -communication_mode      $communication_mode]} {

        return 0
    }

    ###########################################################################
    #  9) Set Network protocol                                                #
    ###########################################################################

    if {![ptp_ha::dut_set_network_protocol\
                    -port_num               [lindex $port_list 0]\
                    -clock_mode             $clock_mode\
                    -network_protocol       $network_protocol]} {

        return 0
    }

    if {![ptp_ha::dut_set_network_protocol\
                    -port_num               [lindex $port_list 1]\
                    -clock_mode             $clock_mode\
                    -network_protocol       $network_protocol]} {

        return 0
    }

    if {$ptp_vlan_encap == "true"} {

        ##############################################################################
        # 10) Create VLAN VLAN-A and VLAN-B and associate ports P1 and P2 to it.     #
        ##############################################################################

        if {![ptp_ha::dut_set_vlan\
                        -vlan_id                [lindex $vlan_id_list 0]\
                        -port_num               [lindex $port_list 0]]} {

            return 0
        }

        if {![ptp_ha::dut_set_vlan\
                        -vlan_id                [lindex $vlan_id_list 1]\
                        -port_num               [lindex $port_list 1]]} {

            return 0
        }

        ##############################################################################
        # 11) Create VLAN VLAN-A and VLAN-B and associate ports P1 and P2 to it.     #
        ##############################################################################

        if {![ptp_ha::dut_set_vlan_priority\
                        -vlan_id                [lindex $vlan_id_list 0]\
                        -vlan_priority          $vlan_priority\
                        -port_num               [lindex $port_list 0]]} {

            return 0
        }

        if {![ptp_ha::dut_set_vlan_priority\
                        -vlan_id                [lindex $vlan_id_list 1]\
                        -vlan_priority          $vlan_priority\
                        -port_num               [lindex $port_list 1]]} {

            return 0
        }
    }

    if {$::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4)} {

        ##############################################################################
        # 12) Set IP address.                                                        #
        ##############################################################################

        if {![ptp_ha::dut_set_ipv4_address\
                        -port_num              [lindex $port_list 0]\
                        -ip_address            [lindex $ip_address_list 0]\
                        -net_mask              [lindex $net_mask_list 0]\
                        -vlan_id               [lindex $vlan_id_list 0]]} {

            return 0
        }

        if {![ptp_ha::dut_set_ipv4_address\
                        -port_num              [lindex $port_list 1]\
                        -ip_address            [lindex $ip_address_list 1]\
                        -net_mask              [lindex $net_mask_list 1]\
                        -vlan_id               [lindex $vlan_id_list 1]]} {

            return 0
        }
    }

    ##############################################################################
    # 13) Configure Priority1.                                                   #
    ##############################################################################

    if {![ptp_ha::dut_set_priority1\
                    -clock_mode             $clock_mode\
                    -priority1              $priority1]} {

        return 0
    }

    ##############################################################################
    # 14) Configure Priority2.                                                   #
    ##############################################################################

    if {![ptp_ha::dut_set_priority2\
                    -clock_mode             $clock_mode\
                    -priority2              $priority2]} {

        return 0
    }

    ###############################################################################
    # 15) Enable txcoherentisRequired, rxcoherentisRequired,                      #
    #     congruentIsRequired.                                                    #
    ###############################################################################

    if {![ptp_ha::dut_tcr_rcr_cr_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    if {![ptp_ha::dut_tcr_rcr_cr_enable\
                    -port_num               [lindex $port_list 1]]} {

        return 0
    }

    ###############################################################################
    # 16) Enable L1 Sync on DUT.                                                  #
    ###############################################################################

    if {![ptp_ha::dut_l1sync_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    if {![ptp_ha::dut_l1sync_enable\
                    -port_num               [lindex $port_list 1]]} {

        return 0
    }

    ###############################################################################
    # 17) Disable optParametersConfigured.                                        #
    ###############################################################################

    if {![ptp_ha::dut_l1sync_opt_params_disable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    if {![ptp_ha::dut_l1sync_opt_params_disable\
                    -port_num               [lindex $port_list 1]]} {

        return 0
    }

    ###############################################################################
    # 17) Disable masterOnly.                                                     #
    ###############################################################################

    if {![ptp_ha::dut_masteronly_disable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    if {![ptp_ha::dut_masteronly_disable\
                    -port_num               [lindex $port_list 1]]} {

        return 0
    }

    ##############################################################################
    # 18) Configure Announce Interval.                                           #
    ##############################################################################

    if {![ptp_ha::dut_set_announce_interval\
                    -announce_interval      $announce_interval\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    if {![ptp_ha::dut_set_announce_interval\
                    -announce_interval      $announce_interval\
                    -port_num               [lindex $port_list 1]]} {

        return 0
    }

    ##############################################################################
    # 19) Configure Announce Timeout.                                            #
    ##############################################################################

    if {![ptp_ha::dut_set_announce_timeout\
                    -announce_timeout       $announce_timeout\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    if {![ptp_ha::dut_set_announce_timeout\
                    -announce_timeout       $announce_timeout\
                    -port_num               [lindex $port_list 1]]} {

        return 0
    }

    ##############################################################################
    # 20) Configure Sync Interval.                                               #
    ##############################################################################

    if {![ptp_ha::dut_set_sync_interval\
                    -sync_interval          $sync_interval\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    if {![ptp_ha::dut_set_sync_interval\
                    -sync_interval          $sync_interval\
                    -port_num               [lindex $port_list 1]]} {

        return 0
    }

    ##############################################################################
    # 21) Configure delayRequestInterval                                         #
    ##############################################################################

    if {![ptp_ha::dut_set_delayreq_interval\
                    -delayreq_interval      $delayreq_interval\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    if {![ptp_ha::dut_set_delayreq_interval\
                    -delayreq_interval      $delayreq_interval\
                    -port_num               [lindex $port_list 1]]} {

        return 0
    }

    ##############################################################################
    # 22) Configure logL1SyncInterval.                                           #
    ##############################################################################

    if {![ptp_ha::dut_set_l1sync_interval\
                    -port_num               [lindex $port_list 0]\
                    -l1sync_interval        $L1syncinterval]} {

        return 0
    }

    if {![ptp_ha::dut_set_l1sync_interval\
                    -port_num               [lindex $port_list 1]\
                    -l1sync_interval        $L1syncinterval]} {

        return 0
    }

    ##############################################################################
    # 23) Configure L1SyncReceiptTimeout.                                        #
    ##############################################################################

    if {![ptp_ha::dut_set_l1sync_timeout\
                    -port_num               [lindex $port_list 0]\
                    -l1sync_timeout         $L1SyncReceiptTimeout]} {

        return 0
    }

    if {![ptp_ha::dut_set_l1sync_timeout\
                    -port_num               [lindex $port_list 1]\
                    -l1sync_timeout         $L1SyncReceiptTimeout]} {

        return 0
    }

    ##############################################################################
    # 24) Configure timestampCorrectionPortDS.egressLatency.                     #
    ##############################################################################

    if {![ptp_ha::dut_set_egress_latency\
                    -port_num              [lindex $port_list 0]\
                    -latency               $egress_latency]} {

        return 0
    }

    if {![ptp_ha::dut_set_egress_latency\
                    -port_num              [lindex $port_list 1]\
                    -latency               $egress_latency]} {

        return 0
    }

    ##############################################################################
    # 25) Configure timestampCorrectionPortDS.ingressLatency.                    #
    ##############################################################################

    if {![ptp_ha::dut_set_ingress_latency\
                    -port_num              [lindex $port_list 0]\
                    -latency               $ingress_latency]} {

        return 0
    }

    if {![ptp_ha::dut_set_ingress_latency\
                    -port_num              [lindex $port_list 1]\
                    -latency               $ingress_latency]} {

        return 0
    }

    ##############################################################################
    # 26) Configure asymmetryCorrectionPortDS.constantAsymmetry.                 #
    ##############################################################################

    if {![ptp_ha::dut_set_constant_asymmetry\
                    -port_num              [lindex $port_list 0]\
                    -constant_asymmetry    $constant_asymmetry]} {

        return 0
    }

    if {![ptp_ha::dut_set_constant_asymmetry\
                    -port_num              [lindex $port_list 1]\
                    -constant_asymmetry    $constant_asymmetry]} {

        return 0
    }

    ##############################################################################
    # 27) Configure asymmetryCorrectionPortDS.scaledDelayCoefficient.            #
    ##############################################################################

    if {![ptp_ha::dut_set_scaled_delay_coefficient\
                    -port_num              [lindex $port_list 0]\
                    -delay_coefficient     $delay_coefficient]} {

        return 0
    }

    if {![ptp_ha::dut_set_scaled_delay_coefficient\
                    -port_num              [lindex $port_list 1]\
                    -delay_coefficient     $delay_coefficient]} {

        return 0
    }

    ##############################################################################
    # 28) Configure asymmetryCorrectionPortDS.enable.                            #
    ##############################################################################

    if {![ptp_ha::dut_set_asymmetry_correction\
                    -port_num              [lindex $port_list 0]]} {

        return 0
    }

    if {![ptp_ha::dut_set_asymmetry_correction\
                    -port_num              [lindex $port_list 1]]} {

        return 0
    }

    ##############################################################################
    # 29) Enable externalPortConfigurationEnabled.                               #
    ##############################################################################

    if {![ptp_ha::dut_external_port_config_enable\
                    -port_num              [lindex $port_list 0]]} {

        return 0
    }

    if {![ptp_ha::dut_external_port_config_enable\
                    -port_num              [lindex $port_list 1]]} {

        return 0
    }

    set env(TEE_ATTEST_MANUAL_PROMPT)      1

    return 1
}


#8.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_configure_setup_008                         #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on success (If the DUT is configured for test setup 8)#
#                                                                              #
#                      0 on failure (If the DUT could not be configured for    #
#                                    test setup 8).                            #
#                                                                              #
#  DEFINITION        : This function configures the parameters required for the#
#                      test setup 8 at the DUT.                                #
#                                                                              #
#                      This function in turn calls below functions             #
#                                                                              #
#                         ptp_ha::dut_port_enable                              #
#                         ptp_ha::dut_global_ptp_enable                        #
#                         ptp_ha::dut_port_ptp_enable                          #
#                         ptp_ha::dut_set_domain                               #
#                         ptp_ha::dut_set_clock_mode                           #
#                         ptp_ha::dut_set_clock_step                           #
#                         ptp_ha::dut_set_delay_mechanism                      #
#                         ptp_ha::dut_set_communication_mode                   #
#                         ptp_ha::dut_set_network_protocol                     #
#                         ptp_ha::dut_set_vlan                                 #
#                         ptp_ha::dut_set_vlan_priority                        #
#                         ptp_ha::dut_set_ipv4_address                         #
#                         ptp_ha::dut_set_priority1                            #
#                         ptp_ha::dut_set_priority2                            #
#                         ptp_ha::dut_tcr_rcr_cr_enable                        #
#                         ptp_ha::dut_l1sync_enable                            #
#                         ptp_ha::dut_l1sync_opt_params_disable                #
#                         ptp_ha::dut_masteronly_disable                       #
#                         ptp_ha::dut_set_announce_interval                    #
#                         ptp_ha::dut_set_announce_timeout                     #
#                         ptp_ha::dut_set_sync_interval                        #
#                         ptp_ha::dut_set_delayreq_interval                    #
#                         ptp_ha::dut_set_pdelayreq_interval                   #
#                         ptp_ha::dut_set_l1sync_interval                      #
#                         ptp_ha::dut_set_l1sync_timeout                       #
#                         ptp_ha::dut_set_egress_latency                       #
#                         ptp_ha::dut_set_ingress_latency                      #
#                         ptp_ha::dut_set_constant_asymmetry                   #
#                         ptp_ha::dut_set_scaled_delay_coefficient             #
#                         ptp_ha::dut_set_asymmetry_correction                 #
#                         ptp_ha::dut_set_epc_desired_state                    #
#                                                                              #
# USAGE              :                                                         #
#                                                                              #
#     ptp_ha::dut_configure_setup_008                                          #
#                                                                              #
################################################################################

proc ptp_ha::dut_configure_setup_008 {args} {

    array set param $args

    global env ptp_ha_env

    set ip_address             $env(dut_TEST_ADDRESS1)
    set net_mask               $env(dut_TEST_MASK1)

    set vlan_priority          $ptp_ha_env(DUT_PTP_HA_VLAN_PRIORITY)

    set ptp_vlan_encap         $ptp_ha_env(DUT_PTP_HA_IS_VLAN_ENCAP)

    set clock_mode             $::PTP_DEVICE_TYPE(OC)
    set clock_step             $ptp_ha_env(DUT_PTP_HA_CLOCK_STEP)
    set network_protocol       $ptp_ha_env(DUT_PTP_HA_TRANS_TYPE)
    set communication_mode     $ptp_ha_env(DUT_PTP_HA_COM_MODEL)
    set priority1              $ptp_ha_env(DUT_PTP_HA_PRIORITY1)
    set priority2              $ptp_ha_env(DUT_PTP_HA_PRIORITY2)
    set announce_interval      $ptp_ha_env(DUT_PTP_HA_ANNOUNCE_INTERVAL)
    set announce_timeout       $ptp_ha_env(DUT_PTP_HA_ANNOUNCE_TIMEOUT)
    set sync_interval          $ptp_ha_env(DUT_PTP_HA_SYNC_INTERVAL)
    set delayreq_interval      $ptp_ha_env(DUT_PTP_HA_DELAY_REQ_INTERVAL)
    set L1syncinterval         $ptp_ha_env(DUT_PTP_HA_L1SYNC_INTERVAL)
    set L1SyncReceiptTimeout   $ptp_ha_env(DUT_PTP_HA_L1SYNC_TIMEOUT)

    set egress_latency         $ptp_ha_env(DUT_PTP_HA_EGRESS_LATENCY)
    set ingress_latency        $ptp_ha_env(DUT_PTP_HA_INGRESS_LATENCY)
    set constant_asymmetry     $ptp_ha_env(DUT_PTP_HA_CONSTANT_ASYMMETRY)
    set delay_coefficient      $ptp_ha_env(DUT_PTP_HA_SCALED_DELAY_COEFFICIENT)

    set port_list              $::dut_port_num_1
    set domain                 $::ptp_dut_default_domain_number
    set delay_mechanism        $::DUT_PTP_DELAY_MECHANISM(E2E)

    set ptp_version            $::ptp_version

    set vlan_id                $::ptp_ha_vlan_id

    ### Check Input parameters ###

    set env(TEE_ATTEST_MANUAL_PROMPT)      0

    ###########################################################################
    #  1) Ensure the DUT port P1 admin status Up.                             #
    ###########################################################################

    if {![ptp_ha::dut_port_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###########################################################################
    #  2) Ensure PTP global on the DUT.                                       #
    ###########################################################################

    if {![ptp_ha::dut_global_ptp_enable]} {

        return 0
    }

    ###########################################################################
    #  3) Ensure PTP enabled on the port P1 on the DUT.                       #
    ###########################################################################

    if {![ptp_ha::dut_port_ptp_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    #  4) Configure Domain.                                                      #
    ##############################################################################

    if {![ptp_ha::dut_set_domain\
                    -clock_mode             $clock_mode\
                    -domain                 $domain ]} {

        return 0
    }

    ###########################################################################
    #  5) Ensure PTP set clock mode                                           #
    ###########################################################################

    if {![ptp_ha::dut_set_clock_mode\
                    -clock_mode             $clock_mode\
                    -port_num               [lindex $port_list 0] ]} {

        return 0
    }

    ###########################################################################
    #  6) Ensure PTP set clock step                                           #
    ###########################################################################

    if {![ptp_ha::dut_set_clock_step\
                    -port_num               [lindex $port_list 0]\
                    -clock_mode             $clock_mode\
                    -clock_step             $clock_step]} {

        return 0
    }

    ##############################################################################
    #  7) Configure Delay Mechanism.                                             #
    ##############################################################################

    if {![ptp_ha::dut_set_delay_mechanism\
                    -delay_mechanism        $delay_mechanism\
                    -clock_mode             $clock_mode\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    #  8) Configure Communication mode.                                          #
    ##############################################################################

    if {![ptp_ha::dut_set_communication_mode\
                   -port_num                [lindex $port_list 0]\
                   -clock_mode              $clock_mode\
                   -ptp_version             $ptp_version\
                   -communication_mode      $communication_mode]} {

        return 0
    }

    ###########################################################################
    #  9) Set Network protocol                                                #
    ###########################################################################

    if {![ptp_ha::dut_set_network_protocol\
                    -port_num               [lindex $port_list 0]\
                    -clock_mode             $clock_mode\
                    -network_protocol       $network_protocol]} {

        return 0
    }

    if {$ptp_vlan_encap == "true"} {

        ##############################################################################
        # 10) Create VLAN VLAN-A and associate port P1 to it.                        #
        ##############################################################################

        if {![ptp_ha::dut_set_vlan\
                        -vlan_id                $vlan_id\
                        -port_num               [lindex $port_list 0]]} {

            return 0
        }

        ##############################################################################
        # 11) Create VLAN VLAN-A and associate port P1 to it.                        #
        ##############################################################################

        if {![ptp_ha::dut_set_vlan_priority\
                        -vlan_id                $vlan_id\
                        -vlan_priority          $vlan_priority\
                        -port_num               [lindex $port_list 0]]} {

            return 0
        }
    }

    if {$::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4)} {

        ##############################################################################
        # 12) Set IP address.                                                        #
        ##############################################################################

        if {![ptp_ha::dut_set_ipv4_address\
                        -port_num              [lindex $port_list 0]\
                        -ip_address            $ip_address\
                        -net_mask              $net_mask\
                        -vlan_id               $vlan_id]} {

            return 0
        }
    }

    ##############################################################################
    # 13) Configure Priority1.                                                   #
    ##############################################################################

    if {![ptp_ha::dut_set_priority1\
                    -clock_mode             $clock_mode\
                    -priority1              $priority1]} {

        return 0
    }

    ##############################################################################
    # 14) Configure Priority2.                                                   #
    ##############################################################################

    if {![ptp_ha::dut_set_priority2\
                    -clock_mode             $clock_mode\
                    -priority2              $priority2]} {

        return 0
    }

    ###############################################################################
    # 15) Enable txcoherentisRequired, rxcoherentisRequired,                      #
    #     congruentIsRequired.                                                    #
    ###############################################################################

    if {![ptp_ha::dut_tcr_rcr_cr_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###############################################################################
    # 16) Enable L1 Sync on DUT.                                                  #
    ###############################################################################

    if {![ptp_ha::dut_l1sync_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###############################################################################
    # 17) Disable optParametersConfigured.                                        #
    ###############################################################################

    if {![ptp_ha::dut_l1sync_opt_params_disable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###############################################################################
    # 17) Disable masterOnly.                                                     #
    ###############################################################################

    if {![ptp_ha::dut_masteronly_disable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 18) Configure Announce Interval.                                           #
    ##############################################################################

    if {![ptp_ha::dut_set_announce_interval\
                    -announce_interval      $announce_interval\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 19) Configure Announce Timeout.                                            #
    ##############################################################################

    if {![ptp_ha::dut_set_announce_timeout\
                    -announce_timeout       $announce_timeout\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 20) Configure Sync Interval.                                               #
    ##############################################################################

    if {![ptp_ha::dut_set_sync_interval\
                    -sync_interval          $sync_interval\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 21) Configure delayRequestInterval                                         #
    ##############################################################################

    if {![ptp_ha::dut_set_delayreq_interval\
                    -delayreq_interval      $delayreq_interval\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 22) Configure logL1SyncInterval.                                           #
    ##############################################################################

    if {![ptp_ha::dut_set_l1sync_interval\
                    -port_num               [lindex $port_list 0]\
                    -l1sync_interval        $L1syncinterval]} {

        return 0
    }

    ##############################################################################
    # 23) Configure L1SyncReceiptTimeout.                                        #
    ##############################################################################

    if {![ptp_ha::dut_set_l1sync_timeout\
                    -port_num               [lindex $port_list 0]\
                    -l1sync_timeout         $L1SyncReceiptTimeout]} {

        return 0
    }

    ##############################################################################
    # 24) Configure timestampCorrectionPortDS.egressLatency.                     #
    ##############################################################################

    if {![ptp_ha::dut_set_egress_latency\
                    -port_num              [lindex $port_list 0]\
                    -latency               $egress_latency]} {

        return 0
    }

    ##############################################################################
    # 25) Configure timestampCorrectionPortDS.ingressLatency.                    #
    ##############################################################################

    if {![ptp_ha::dut_set_ingress_latency\
                    -port_num              [lindex $port_list 0]\
                    -latency               $ingress_latency]} {

        return 0
    }

    ##############################################################################
    # 26) Configure asymmetryCorrectionPortDS.constantAsymmetry.                 #
    ##############################################################################

    if {![ptp_ha::dut_set_constant_asymmetry\
                    -port_num              [lindex $port_list 0]\
                    -constant_asymmetry    $constant_asymmetry]} {

        return 0
    }

    ##############################################################################
    # 27) Configure asymmetryCorrectionPortDS.scaledDelayCoefficient.            #
    ##############################################################################

    if {![ptp_ha::dut_set_scaled_delay_coefficient\
                    -port_num              [lindex $port_list 0]\
                    -delay_coefficient     $delay_coefficient]} {

        return 0
    }

    ##############################################################################
    # 28) Configure asymmetryCorrectionPortDS.enable.                            #
    ##############################################################################

    if {![ptp_ha::dut_set_asymmetry_correction\
                    -port_num              [lindex $port_list 0]]} {

        return 0
    }

    set env(TEE_ATTEST_MANUAL_PROMPT)      1

    return 1
}


#9.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_configure_setup_009                         #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on success (If the DUT is configured for test setup 9)#
#                                                                              #
#                      0 on failure (If the DUT could not be configured for    #
#                                    test setup 9).                            #
#                                                                              #
#  DEFINITION        : This function configures the parameters required for the#
#                      test setup 9 at the DUT.                                #
#                                                                              #
#                      This function in turn calls below functions             #
#                                                                              #
#                         ptp_ha::dut_port_enable                              #
#                         ptp_ha::dut_global_ptp_enable                        #
#                         ptp_ha::dut_port_ptp_enable                          #
#                         ptp_ha::dut_set_domain                               #
#                         ptp_ha::dut_set_clock_mode                           #
#                         ptp_ha::dut_set_clock_step                           #
#                         ptp_ha::dut_set_delay_mechanism                      #
#                         ptp_ha::dut_set_communication_mode                   #
#                         ptp_ha::dut_set_network_protocol                     #
#                         ptp_ha::dut_set_vlan                                 #
#                         ptp_ha::dut_set_vlan_priority                        #
#                         ptp_ha::dut_set_ipv4_address                         #
#                         ptp_ha::dut_set_priority1                            #
#                         ptp_ha::dut_set_priority2                            #
#                         ptp_ha::dut_tcr_rcr_cr_enable                        #
#                         ptp_ha::dut_l1sync_enable                            #
#                         ptp_ha::dut_l1sync_opt_params_disable                #
#                         ptp_ha::dut_masteronly_disable                       #
#                         ptp_ha::dut_set_announce_interval                    #
#                         ptp_ha::dut_set_announce_timeout                     #
#                         ptp_ha::dut_set_sync_interval                        #
#                         ptp_ha::dut_set_delayreq_interval                    #
#                         ptp_ha::dut_set_pdelayreq_interval                   #
#                         ptp_ha::dut_set_l1sync_interval                      #
#                         ptp_ha::dut_set_l1sync_timeout                       #
#                         ptp_ha::dut_set_egress_latency                       #
#                         ptp_ha::dut_set_ingress_latency                      #
#                         ptp_ha::dut_set_constant_asymmetry                   #
#                         ptp_ha::dut_set_scaled_delay_coefficient             #
#                         ptp_ha::dut_set_asymmetry_correction                 #
#                         ptp_ha::dut_external_port_config_disable             #
#                         ptp_ha::dut_set_epc_desired_state                    #
#                                                                              #
# USAGE              :                                                         #
#                                                                              #
#     ptp_ha::dut_configure_setup_009                                          #
#                                                                              #
################################################################################

proc ptp_ha::dut_configure_setup_009 {args} {

    array set param $args

    global env ptp_ha_env

    set ip_address             $env(dut_TEST_ADDRESS1)
    set net_mask               $env(dut_TEST_MASK1)

    set vlan_priority          $ptp_ha_env(DUT_PTP_HA_VLAN_PRIORITY)

    set ptp_vlan_encap         $ptp_ha_env(DUT_PTP_HA_IS_VLAN_ENCAP)

    set clock_mode             $ptp_ha_env(DUT_PTP_HA_DEVCIE_TYPE)
    set clock_step             $ptp_ha_env(DUT_PTP_HA_CLOCK_STEP)
    set network_protocol       $ptp_ha_env(DUT_PTP_HA_TRANS_TYPE)
    set communication_mode     $ptp_ha_env(DUT_PTP_HA_COM_MODEL)
    set priority1              $ptp_ha_env(DUT_PTP_HA_PRIORITY1)
    set priority2              $ptp_ha_env(DUT_PTP_HA_PRIORITY2)
    set announce_interval      $ptp_ha_env(DUT_PTP_HA_ANNOUNCE_INTERVAL)
    set announce_timeout       $ptp_ha_env(DUT_PTP_HA_ANNOUNCE_TIMEOUT)
    set sync_interval          $ptp_ha_env(DUT_PTP_HA_SYNC_INTERVAL)
    set delayreq_interval      $ptp_ha_env(DUT_PTP_HA_DELAY_REQ_INTERVAL)
    set L1syncinterval         $ptp_ha_env(DUT_PTP_HA_L1SYNC_INTERVAL)
    set L1SyncReceiptTimeout   $ptp_ha_env(DUT_PTP_HA_L1SYNC_TIMEOUT)

    set egress_latency         $ptp_ha_env(DUT_PTP_HA_EGRESS_LATENCY)
    set ingress_latency        $ptp_ha_env(DUT_PTP_HA_INGRESS_LATENCY)
    set constant_asymmetry     $ptp_ha_env(DUT_PTP_HA_CONSTANT_ASYMMETRY)
    set delay_coefficient      $ptp_ha_env(DUT_PTP_HA_SCALED_DELAY_COEFFICIENT)

    set port_list              $::dut_port_num_1
    set domain                 $::ptp_dut_default_domain_number
    set delay_mechanism        $::DUT_PTP_DELAY_MECHANISM(E2E)

    set ptp_version            $::ptp_version

    set vlan_id                $::ptp_ha_vlan_id

    ### Check Input parameters ###

    set env(TEE_ATTEST_MANUAL_PROMPT)      0

    ###########################################################################
    #  1) Ensure the DUT port P1 admin status Up.                             #
    ###########################################################################

    if {![ptp_ha::dut_port_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###########################################################################
    #  2) Ensure PTP global on the DUT.                                       #
    ###########################################################################

    if {![ptp_ha::dut_global_ptp_enable]} {

        return 0
    }

    ###########################################################################
    #  3) Ensure PTP enabled on the port P1 on the DUT.                       #
    ###########################################################################

    if {![ptp_ha::dut_port_ptp_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    #  4) Configure Domain.                                                      #
    ##############################################################################

    if {![ptp_ha::dut_set_domain\
                    -clock_mode             $clock_mode\
                    -domain                 $domain ]} {

        return 0
    }

    ###########################################################################
    #  5) Ensure PTP set clock mode                                           #
    ###########################################################################

    if {![ptp_ha::dut_set_clock_mode\
                    -clock_mode             $clock_mode\
                    -port_num               [lindex $port_list 0] ]} {

        return 0
    }

    ###########################################################################
    #  6) Ensure PTP set clock step                                           #
    ###########################################################################

    if {![ptp_ha::dut_set_clock_step\
                    -port_num               [lindex $port_list 0]\
                    -clock_mode             $clock_mode\
                    -clock_step             $clock_step]} {

        return 0
    }

    ##############################################################################
    #  7) Configure Delay Mechanism.                                             #
    ##############################################################################

    if {![ptp_ha::dut_set_delay_mechanism\
                    -delay_mechanism        $delay_mechanism\
                    -clock_mode             $clock_mode\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    #  8) Configure Communication mode.                                          #
    ##############################################################################

    if {![ptp_ha::dut_set_communication_mode\
                   -port_num                [lindex $port_list 0]\
                   -clock_mode              $clock_mode\
                   -ptp_version             $ptp_version\
                   -communication_mode      $communication_mode]} {

        return 0
    }

    ###########################################################################
    #  9) Set Network protocol                                                #
    ###########################################################################

    if {![ptp_ha::dut_set_network_protocol\
                    -port_num               [lindex $port_list 0]\
                    -clock_mode             $clock_mode\
                    -network_protocol       $network_protocol]} {

        return 0
    }

    if {$ptp_vlan_encap == "true"} {

        ##############################################################################
        # 10) Create VLAN VLAN-A and associate port P1 to it.                        #
        ##############################################################################

        if {![ptp_ha::dut_set_vlan\
                        -vlan_id                $vlan_id\
                        -port_num               [lindex $port_list 0]]} {

            return 0
        }

        ##############################################################################
        # 11) Create VLAN VLAN-A and associate port P1 to it.                        #
        ##############################################################################

        if {![ptp_ha::dut_set_vlan_priority\
                        -vlan_id                $vlan_id\
                        -vlan_priority          $vlan_priority\
                        -port_num               [lindex $port_list 0]]} {

            return 0
        }
    }

    if {$::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4)} {

        ##############################################################################
        # 12) Set IP address.                                                        #
        ##############################################################################

        if {![ptp_ha::dut_set_ipv4_address\
                        -port_num              [lindex $port_list 0]\
                        -ip_address            $ip_address\
                        -net_mask              $net_mask\
                        -vlan_id               $vlan_id]} {

            return 0
        }
    }

    ##############################################################################
    # 13) Configure Priority1.                                                   #
    ##############################################################################

    if {![ptp_ha::dut_set_priority1\
                    -clock_mode             $clock_mode\
                    -priority1              $priority1]} {

        return 0
    }

    ##############################################################################
    # 14) Configure Priority2.                                                   #
    ##############################################################################

    if {![ptp_ha::dut_set_priority2\
                    -clock_mode             $clock_mode\
                    -priority2              $priority2]} {

        return 0
    }

    ###############################################################################
    # 15) Enable txcoherentisRequired, rxcoherentisRequired,                      #
    #     congruentIsRequired.                                                    #
    ###############################################################################

    if {![ptp_ha::dut_tcr_rcr_cr_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###############################################################################
    # 16) Enable L1 Sync on DUT.                                                  #
    ###############################################################################

    if {![ptp_ha::dut_l1sync_enable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###############################################################################
    # 17) Disable optParametersConfigured.                                        #
    ###############################################################################

    if {![ptp_ha::dut_l1sync_opt_params_disable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ###############################################################################
    # 17) Disable masterOnly.                                                     #
    ###############################################################################

    if {![ptp_ha::dut_masteronly_disable\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 18) Configure Announce Interval.                                           #
    ##############################################################################

    if {![ptp_ha::dut_set_announce_interval\
                    -announce_interval      $announce_interval\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 19) Configure Announce Timeout.                                            #
    ##############################################################################

    if {![ptp_ha::dut_set_announce_timeout\
                    -announce_timeout       $announce_timeout\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 20) Configure Sync Interval.                                               #
    ##############################################################################

    if {![ptp_ha::dut_set_sync_interval\
                    -sync_interval          $sync_interval\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 21) Configure delayRequestInterval                                         #
    ##############################################################################

    if {![ptp_ha::dut_set_delayreq_interval\
                    -delayreq_interval      $delayreq_interval\
                    -port_num               [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 22) Configure logL1SyncInterval.                                           #
    ##############################################################################

    if {![ptp_ha::dut_set_l1sync_interval\
                    -port_num               [lindex $port_list 0]\
                    -l1sync_interval        $L1syncinterval]} {

        return 0
    }

    ##############################################################################
    # 23) Configure L1SyncReceiptTimeout.                                        #
    ##############################################################################

    if {![ptp_ha::dut_set_l1sync_timeout\
                    -port_num               [lindex $port_list 0]\
                    -l1sync_timeout         $L1SyncReceiptTimeout]} {

        return 0
    }

    ##############################################################################
    # 24) Configure timestampCorrectionPortDS.egressLatency.                     #
    ##############################################################################

    if {![ptp_ha::dut_set_egress_latency\
                    -port_num              [lindex $port_list 0]\
                    -latency               0]} {

        return 0
    }

    ##############################################################################
    # 25) Configure timestampCorrectionPortDS.ingressLatency.                    #
    ##############################################################################

    if {![ptp_ha::dut_set_ingress_latency\
                    -port_num              [lindex $port_list 0]\
                    -latency               0]} {

        return 0
    }

    ##############################################################################
    # 26) Configure asymmetryCorrectionPortDS.constantAsymmetry.                 #
    ##############################################################################

    if {![ptp_ha::dut_set_constant_asymmetry\
                    -port_num              [lindex $port_list 0]\
                    -constant_asymmetry    0]} {

        return 0
    }

    ##############################################################################
    # 27) Configure asymmetryCorrectionPortDS.scaledDelayCoefficient.            #
    ##############################################################################

    if {![ptp_ha::dut_set_scaled_delay_coefficient\
                    -port_num              [lindex $port_list 0]\
                    -delay_coefficient     0]} {

        return 0
    }

    ##############################################################################
    # 28) Configure asymmetryCorrectionPortDS.enable.                            #
    ##############################################################################

    if {![ptp_ha::dut_set_asymmetry_correction\
                    -port_num              [lindex $port_list 0]]} {

        return 0
    }

    ##############################################################################
    # 29) Disable externalPortConfigurationEnabled.                              #
    ##############################################################################

    if {![ptp_ha::dut_external_port_config_disable\
                    -port_num              [lindex $port_list 0]]} {

        return 0
    }

    set env(TEE_ATTEST_MANUAL_PROMPT)      1

    return 1
}



################################################################################
#                       DUT SETUP CLEANUP FUNCTIONS                            #
################################################################################

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_cleanup_setup_001                           #
#                                                                              #
#  INPUT PARAMETERS   : NONE.                                                  #
#                                                                              #
#  OUTPUT PARAMETERS  : NONE.                                                  #
#                                                                              #
#  RETURNS           : 1 on success (if the configuration of test setup 1 is   #
#                                    cleared in DUT).                          #
#                                                                              #
#                      0 on failure (if the DUT could not be cleared for       #
#                                    test setup 1).                            #
#                                                                              #
#  DEFINITION        : This function clears the configurations made for test   #
#                      setup 1 at the DUT.                                     #
#                                                                              #
#                      This function in turn calls below functions             #
#                                                                              #
#                         ptp_ha::dut_reset_priority1                          #
#                         ptp_ha::dut_reset_priority2                          #
#                         ptp_ha::dut_tcr_rcr_cr_disable                       #
#                         ptp_ha::dut_l1sync_disable                           #
#                         ptp_ha::dut_reset_announce_interval                  #
#                         ptp_ha::dut_reset_announce_timeout                   #
#                         ptp_ha::dut_reset_sync_interval                      #
#                         ptp_ha::dut_reset_delayreq_interval                  #
#                         ptp_ha::dut_reset_l1sync_interval                    #
#                         ptp_ha::dut_reset_l1sync_timeout                     #
#                         ptp_ha::dut_reset_egress_latency                     #
#                         ptp_ha::dut_reset_ingress_latency                    #
#                         ptp_ha::dut_reset_constant_asymmetry                 #
#                         ptp_ha::dut_reset_scaled_delay_coefficient           #
#                         ptp_ha::dut_reset_asymmetry_correction               #
#                         ptp_ha::dut_reset_clock_mode                         #
#                         ptp_ha::dut_reset_clock_step                         #
#                         ptp_ha::dut_reset_delay_mechanism                    #
#                         ptp_ha::dut_reset_communication_mode                 #
#                         ptp_ha::dut_reset_network_protocol                   #
#                         ptp_ha::dut_reset_domain                             #
#                         ptp_ha::dut_port_ptp_disable                         #
#                         ptp_ha::dut_global_ptp_disable                       #
#                         ptp_ha::dut_reset_vlan_priority                      #
#                         ptp_ha::dut_reset_vlan                               #
#                         ptp_ha::dut_reset_ipv4_address                       #
#                         ptp_ha::dut_port_disable                             #
#                                                                              #
# USAGE              :                                                         #
#                                                                              #
#     ptp_ha::dut_cleanup_setup_001                                            #
#                                                                              #
################################################################################

proc ptp_ha::dut_cleanup_setup_001 {args} {

    array set param $args

    global env ptp_ha_env

    set ip_address             $env(dut_TEST_ADDRESS1)
    set net_mask               $env(dut_TEST_MASK1)

    set vlan_priority          $ptp_ha_env(DUT_PTP_HA_VLAN_PRIORITY)

    set ptp_vlan_encap         $ptp_ha_env(DUT_PTP_HA_IS_VLAN_ENCAP)

    set network_protocol       $ptp_ha_env(DUT_PTP_HA_TRANS_TYPE)
    set communication_mode     $ptp_ha_env(DUT_PTP_HA_COM_MODEL)

    set port_list              $::dut_port_num_1
    set domain                 $::ptp_dut_default_domain_number
    set delay_mechanism        $::DUT_PTP_DELAY_MECHANISM(E2E)

    set ptp_version            $::ptp_version

    set vlan_id                $::ptp_ha_vlan_id

    ### Check Input parameters ###

    LOG -level 0 -msg "Configuration Cleanup. Please ignore the ERRORS\n"

    ptp_ha::dut_reset_priority1

    ptp_ha::dut_reset_priority2

    ptp_ha::dut_tcr_rcr_cr_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_l1sync_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_announce_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_announce_timeout\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_sync_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_delayreq_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_l1sync_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_l1sync_timeout\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_egress_latency\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_ingress_latency\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_constant_asymmetry\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_scaled_delay_coefficient\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_asymmetry_correction\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_clock_mode\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_clock_step\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_delay_mechanism\
                    -port_num               [lindex $port_list 0]\
                    -delay_mechanism        $delay_mechanism

    ptp_ha::dut_reset_communication_mode\
                   -port_num                [lindex $port_list 0]

    ptp_ha::dut_reset_network_protocol\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_domain

    ptp_ha::dut_port_ptp_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_global_ptp_disable

    if {$ptp_vlan_encap == "true"} {

        ptp_ha::dut_reset_vlan_priority\
                        -vlan_id                $vlan_id\
                        -vlan_priority          $vlan_priority\
                        -port_num               [lindex $port_list 0]

        ptp_ha::dut_reset_vlan\
                        -vlan_id                $vlan_id\
                        -port_num               [lindex $port_list 0]

    }

    if {$::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4)} {

        ptp_ha::dut_reset_ipv4_address\
                        -port_num               [lindex $port_list 0]\
                        -ip_address             $ip_address\
                        -net_mask               $net_mask\
                        -vlan_id                $vlan_id
    }

    ptp_ha::dut_port_disable\
                    -port_num               [lindex $port_list 0]

    set env(TEE_ATTEST_MANUAL_PROMPT)      1

    return 1
}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_cleanup_setup_002                           #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on success (if the configuration of test setup 2 is   #
#                                    cleared in DUT).                          #
#                                                                              #
#                      0 on failure (if the DUT could not be cleared for       #
#                                    test setup 2).                            #
#                                                                              #
#  DEFINITION        : This function clears the configurations made for test   #
#                      setup 2 at the DUT.                                     #
#                                                                              #
#                      This function in turn calls below functions             #
#                                                                              #
#                         ptp_ha::dut_reset_priority1                          #
#                         ptp_ha::dut_reset_priority2                          #
#                         ptp_ha::dut_tcr_rcr_cr_disable                       #
#                         ptp_ha::dut_l1sync_disable                           #
#                         ptp_ha::dut_reset_announce_interval                  #
#                         ptp_ha::dut_reset_announce_timeout                   #
#                         ptp_ha::dut_reset_sync_interval                      #
#                         ptp_ha::dut_reset_delayreq_interval                  #
#                         ptp_ha::dut_reset_l1sync_interval                    #
#                         ptp_ha::dut_reset_l1sync_timeout                     #
#                         ptp_ha::dut_reset_egress_latency                     #
#                         ptp_ha::dut_reset_ingress_latency                    #
#                         ptp_ha::dut_reset_constant_asymmetry                 #
#                         ptp_ha::dut_reset_scaled_delay_coefficient           #
#                         ptp_ha::dut_reset_asymmetry_correction               #
#                         ptp_ha::dut_reset_clock_mode                         #
#                         ptp_ha::dut_reset_clock_step                         #
#                         ptp_ha::dut_reset_delay_mechanism                    #
#                         ptp_ha::dut_reset_communication_mode                 #
#                         ptp_ha::dut_reset_network_protocol                   #
#                         ptp_ha::dut_reset_domain                             #
#                         ptp_ha::dut_port_ptp_disable                         #
#                         ptp_ha::dut_global_ptp_disable                       #
#                         ptp_ha::dut_reset_vlan_priority                      #
#                         ptp_ha::dut_reset_vlan                               #
#                         ptp_ha::dut_reset_ipv4_address                       #
#                         ptp_ha::dut_port_disable                             #
#                                                                              #
# USAGE              :                                                         #
#                                                                              #
#     ptp_ha::dut_cleanup_setup_002                                            #
#                                                                              #
################################################################################

proc ptp_ha::dut_cleanup_setup_002 {args} {

    array set param $args

    global env ptp_ha_env

    set ip_address             $env(dut_TEST_ADDRESS1)
    set net_mask               $env(dut_TEST_MASK1)

    set vlan_priority          $ptp_ha_env(DUT_PTP_HA_VLAN_PRIORITY)

    set ptp_vlan_encap         $ptp_ha_env(DUT_PTP_HA_IS_VLAN_ENCAP)

    set network_protocol       $ptp_ha_env(DUT_PTP_HA_TRANS_TYPE)
    set communication_mode     $ptp_ha_env(DUT_PTP_HA_COM_MODEL)

    set port_list              $::dut_port_num_1
    set domain                 $::ptp_dut_default_domain_number
    set delay_mechanism        $::DUT_PTP_DELAY_MECHANISM(P2P)

    set ptp_version            $::ptp_version

    set vlan_id                $::ptp_ha_vlan_id

    ### Check Input parameters ###

    LOG -level 0 -msg "Configuration Cleanup. Please ignore the ERRORS\n"

    ptp_ha::dut_reset_priority1

    ptp_ha::dut_reset_priority2

    ptp_ha::dut_tcr_rcr_cr_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_l1sync_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_announce_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_announce_timeout\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_sync_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_delayreq_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_l1sync_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_l1sync_timeout\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_egress_latency\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_ingress_latency\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_constant_asymmetry\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_scaled_delay_coefficient\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_asymmetry_correction\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_clock_mode\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_clock_step\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_delay_mechanism\
                    -port_num               [lindex $port_list 0]\
                    -delay_mechanism        $delay_mechanism

    ptp_ha::dut_reset_communication_mode\
                   -port_num                [lindex $port_list 0]

    ptp_ha::dut_reset_network_protocol\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_domain

    ptp_ha::dut_port_ptp_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_global_ptp_disable

    if {$ptp_vlan_encap == "true"} {

        ptp_ha::dut_reset_vlan_priority\
                        -vlan_id                $vlan_id\
                        -vlan_priority          $vlan_priority\
                        -port_num               [lindex $port_list 0]

        ptp_ha::dut_reset_vlan\
                        -vlan_id                $vlan_id\
                        -port_num               [lindex $port_list 0]

    }

    if {$::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4)} {

        ptp_ha::dut_reset_ipv4_address\
                        -port_num               [lindex $port_list 0]\
                        -ip_address             $ip_address\
                        -net_mask               $net_mask\
                        -vlan_id                $vlan_id
    }

    ptp_ha::dut_port_disable\
                    -port_num               [lindex $port_list 0]

    return 1

}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_cleanup_setup_003                           #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on success (if the configuration of test setup 3 is   #
#                                    cleared in DUT).                          #
#                                                                              #
#                      0 on failure (if the DUT could not be cleared for       #
#                                    test setup 3).                            #
#                                                                              #
#  DEFINITION        : This function clears the configurations made for test   #
#                      setup 3 at the DUT.                                     #
#                                                                              #
#                      This function in turn calls below functions             #
#                                                                              #
#                         ptp_ha::dut_reset_priority1                          #
#                         ptp_ha::dut_reset_priority2                          #
#                         ptp_ha::dut_tcr_rcr_cr_disable                       #
#                         ptp_ha::dut_l1sync_disable                           #
#                         ptp_ha::dut_reset_announce_interval                  #
#                         ptp_ha::dut_reset_announce_timeout                   #
#                         ptp_ha::dut_reset_sync_interval                      #
#                         ptp_ha::dut_reset_delayreq_interval                  #
#                         ptp_ha::dut_reset_l1sync_interval                    #
#                         ptp_ha::dut_reset_l1sync_timeout                     #
#                         ptp_ha::dut_reset_egress_latency                     #
#                         ptp_ha::dut_reset_ingress_latency                    #
#                         ptp_ha::dut_reset_constant_asymmetry                 #
#                         ptp_ha::dut_reset_scaled_delay_coefficient           #
#                         ptp_ha::dut_reset_asymmetry_correction               #
#                         ptp_ha::dut_reset_clock_mode                         #
#                         ptp_ha::dut_reset_clock_step                         #
#                         ptp_ha::dut_reset_delay_mechanism                    #
#                         ptp_ha::dut_reset_communication_mode                 #
#                         ptp_ha::dut_reset_network_protocol                   #
#                         ptp_ha::dut_reset_domain                             #
#                         ptp_ha::dut_port_ptp_disable                         #
#                         ptp_ha::dut_global_ptp_disable                       #
#                         ptp_ha::dut_reset_vlan_priority                      #
#                         ptp_ha::dut_reset_vlan                               #
#                         ptp_ha::dut_reset_ipv4_address                       #
#                         ptp_ha::dut_port_disable                             #
#                                                                              #
# USAGE              :                                                         #
#                                                                              #
#     ptp_ha::dut_cleanup_setup_003                                            #
#                                                                              #
################################################################################

proc ptp_ha::dut_cleanup_setup_003 {args} {

    array set param $args

    global env ptp_ha_env

    set ip_address_list        [list $env(dut_TEST_ADDRESS1) $env(dut_TEST_ADDRESS2)]
    set net_mask_list          [list $env(dut_TEST_MASK1) $env(dut_TEST_MASK2)]
    set vlan_id_1              $::ptp_ha_vlan_id
    set vlan_id_2              $::ptp_ha_vlan_id_2

    set vlan_id_list           [list $vlan_id_1 $vlan_id_2]

    set vlan_priority          $ptp_ha_env(DUT_PTP_HA_VLAN_PRIORITY)

    set ptp_vlan_encap         $ptp_ha_env(DUT_PTP_HA_IS_VLAN_ENCAP)

    set network_protocol       $ptp_ha_env(DUT_PTP_HA_TRANS_TYPE)
    set communication_mode     $ptp_ha_env(DUT_PTP_HA_COM_MODEL)

    set port_list              [list $::dut_port_num_1 $::dut_port_num_2]
    set domain                 $::ptp_dut_default_domain_number
    set delay_mechanism        $::DUT_PTP_DELAY_MECHANISM(E2E)

    set ptp_version            $::ptp_version

    ### Check Input parameters ###

    LOG -level 0 -msg "Configuration Cleanup. Please ignore the ERRORS\n"

    ptp_ha::dut_reset_priority1

    ptp_ha::dut_reset_priority2

    ptp_ha::dut_tcr_rcr_cr_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_tcr_rcr_cr_disable\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_l1sync_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_l1sync_disable\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_announce_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_announce_interval\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_announce_timeout\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_announce_timeout\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_sync_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_sync_interval\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_delayreq_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_delayreq_interval\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_l1sync_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_l1sync_interval\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_l1sync_timeout\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_l1sync_timeout\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_egress_latency\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_egress_latency\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_ingress_latency\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_ingress_latency\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_constant_asymmetry\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_constant_asymmetry\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_scaled_delay_coefficient\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_scaled_delay_coefficient\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_asymmetry_correction\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_asymmetry_correction\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_clock_mode\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_clock_mode\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_clock_step\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_clock_step\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_delay_mechanism\
                    -port_num               [lindex $port_list 0]\
                    -delay_mechanism        $delay_mechanism

    ptp_ha::dut_reset_delay_mechanism\
                    -port_num               [lindex $port_list 1]\
                    -delay_mechanism        $delay_mechanism

    ptp_ha::dut_reset_communication_mode\
                   -port_num                [lindex $port_list 0]

    ptp_ha::dut_reset_communication_mode\
                   -port_num                [lindex $port_list 1]

    ptp_ha::dut_reset_network_protocol\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_network_protocol\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_domain

    ptp_ha::dut_port_ptp_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_port_ptp_disable\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_global_ptp_disable

    if {$ptp_vlan_encap == "true"} {

        ptp_ha::dut_reset_vlan_priority\
                        -vlan_id                [lindex $vlan_id_list 0]\
                        -vlan_priority          $vlan_priority\
                        -port_num               [lindex $port_list 0]

        ptp_ha::dut_reset_vlan_priority\
                        -vlan_id                [lindex $vlan_id_list 1]\
                        -vlan_priority          $vlan_priority\
                        -port_num               [lindex $port_list 1]

        ptp_ha::dut_reset_vlan\
                        -vlan_id                [lindex $vlan_id_list 0]\
                        -port_num               [lindex $port_list 0]

        ptp_ha::dut_reset_vlan\
                        -vlan_id                [lindex $vlan_id_list 1]\
                        -port_num               [lindex $port_list 1]
    }

    if {$::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4)} {

        ptp_ha::dut_reset_ipv4_address\
                        -port_num               [lindex $port_list 0]\
                        -ip_address             [lindex $ip_address_list 0]\
                        -net_mask               [lindex $net_mask_list 0]\
                        -vlan_id                [lindex $vlan_id_list 0]

        ptp_ha::dut_reset_ipv4_address\
                        -port_num               [lindex $port_list 1]\
                        -ip_address             [lindex $ip_address_list 1]\
                        -net_mask               [lindex $net_mask_list 1]\
                        -vlan_id                [lindex $vlan_id_list 1]
    }

    ptp_ha::dut_port_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_port_disable\
                    -port_num               [lindex $port_list 1]

    set env(TEE_ATTEST_MANUAL_PROMPT)      1

    return 1
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_cleanup_setup_004                           #
#                                                                              #
#  INPUT PARAMETERS   : NONE.                                                  #
#                                                                              #
#  OUTPUT PARAMETERS  : NONE.                                                  #
#                                                                              #
#  RETURNS           : 1 on success (if the configuration of test setup 4 is   #
#                                    cleared in DUT).                          #
#                                                                              #
#                      0 on failure (if the DUT could not be cleared for       #
#                                    test setup 4).                            #
#                                                                              #
#  DEFINITION        : This function clears the configurations made for test   #
#                      setup 4 at the DUT.                                     #
#                                                                              #
#                      This function in turn calls below functions             #
#                                                                              #
#                         ptp_ha::dut_reset_priority1                          #
#                         ptp_ha::dut_reset_priority2                          #
#                         ptp_ha::dut_tcr_rcr_cr_disable                       #
#                         ptp_ha::dut_l1sync_disable                           #
#                         ptp_ha::dut_reset_announce_interval                  #
#                         ptp_ha::dut_reset_announce_timeout                   #
#                         ptp_ha::dut_reset_sync_interval                      #
#                         ptp_ha::dut_reset_delayreq_interval                  #
#                         ptp_ha::dut_reset_l1sync_interval                    #
#                         ptp_ha::dut_reset_l1sync_timeout                     #
#                         ptp_ha::dut_reset_egress_latency                     #
#                         ptp_ha::dut_reset_ingress_latency                    #
#                         ptp_ha::dut_reset_constant_asymmetry                 #
#                         ptp_ha::dut_reset_scaled_delay_coefficient           #
#                         ptp_ha::dut_reset_asymmetry_correction               #
#                         ptp_ha::dut_reset_clock_mode                         #
#                         ptp_ha::dut_reset_clock_step                         #
#                         ptp_ha::dut_reset_delay_mechanism                    #
#                         ptp_ha::dut_reset_communication_mode                 #
#                         ptp_ha::dut_reset_network_protocol                   #
#                         ptp_ha::dut_reset_domain                             #
#                         ptp_ha::dut_port_ptp_disable                         #
#                         ptp_ha::dut_global_ptp_disable                       #
#                         ptp_ha::dut_reset_vlan_priority                      #
#                         ptp_ha::dut_reset_vlan                               #
#                         ptp_ha::dut_reset_ipv4_address                       #
#                         ptp_ha::dut_port_disable                             #
#                                                                              #
# USAGE              :                                                         #
#                                                                              #
#     ptp_ha::dut_cleanup_setup_004                                            #
#                                                                              #
################################################################################

proc ptp_ha::dut_cleanup_setup_004 {args} {

    array set param $args

    global env ptp_ha_env

    set ip_address             $env(dut_TEST_ADDRESS1)
    set net_mask               $env(dut_TEST_MASK1)

    set vlan_priority          $ptp_ha_env(DUT_PTP_HA_VLAN_PRIORITY)

    set ptp_vlan_encap         $ptp_ha_env(DUT_PTP_HA_IS_VLAN_ENCAP)

    set network_protocol       $ptp_ha_env(DUT_PTP_HA_TRANS_TYPE)
    set communication_mode     $ptp_ha_env(DUT_PTP_HA_COM_MODEL)

    set port_list              $::dut_port_num_1
    set domain                 $::ptp_dut_default_domain_number
    set delay_mechanism        $::DUT_PTP_DELAY_MECHANISM(E2E)

    set ptp_version            $::ptp_version

    set vlan_id                $::ptp_ha_vlan_id

    ### Check Input parameters ###

    LOG -level 0 -msg "Configuration Cleanup. Please ignore the ERRORS\n"

    ptp_ha::dut_l1sync_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_clock_mode\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_clock_step\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_delay_mechanism\
                    -port_num               [lindex $port_list 0]\
                    -delay_mechanism        $delay_mechanism

    ptp_ha::dut_reset_communication_mode\
                   -port_num                [lindex $port_list 0]

    ptp_ha::dut_reset_network_protocol\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_domain

    ptp_ha::dut_port_ptp_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_global_ptp_disable

    if {$ptp_vlan_encap == "true"} {

        ptp_ha::dut_reset_vlan_priority\
                        -vlan_id                $vlan_id\
                        -vlan_priority          $vlan_priority\
                        -port_num               [lindex $port_list 0]

        ptp_ha::dut_reset_vlan\
                        -vlan_id                $vlan_id\
                        -port_num               [lindex $port_list 0]

    }

    if {$::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4)} {

        ptp_ha::dut_reset_ipv4_address\
                        -port_num               [lindex $port_list 0]\
                        -ip_address             $ip_address\
                        -net_mask               $net_mask\
                        -vlan_id                $vlan_id
    }

    ptp_ha::dut_port_disable\
                    -port_num               [lindex $port_list 0]

    set env(TEE_ATTEST_MANUAL_PROMPT)      1

    return 1
}


#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_cleanup_setup_005                           #
#                                                                              #
#  INPUT PARAMETERS   : NONE.                                                  #
#                                                                              #
#  OUTPUT PARAMETERS  : NONE.                                                  #
#                                                                              #
#  RETURNS           : 1 on success (if the configuration of test setup 5 is   #
#                                    cleared in DUT).                          #
#                                                                              #
#                      0 on failure (if the DUT could not be cleared for       #
#                                    test setup 5).                            #
#                                                                              #
#  DEFINITION        : This function clears the configurations made for test   #
#                      setup 5 at the DUT.                                     #
#                                                                              #
#                      This function in turn calls below functions             #
#                                                                              #
#                         ptp_ha::dut_reset_priority1                          #
#                         ptp_ha::dut_reset_priority2                          #
#                         ptp_ha::dut_tcr_rcr_cr_disable                       #
#                         ptp_ha::dut_l1sync_disable                           #
#                         ptp_ha::dut_reset_announce_interval                  #
#                         ptp_ha::dut_reset_announce_timeout                   #
#                         ptp_ha::dut_reset_sync_interval                      #
#                         ptp_ha::dut_reset_delayreq_interval                  #
#                         ptp_ha::dut_reset_l1sync_interval                    #
#                         ptp_ha::dut_reset_l1sync_timeout                     #
#                         ptp_ha::dut_reset_egress_latency                     #
#                         ptp_ha::dut_reset_ingress_latency                    #
#                         ptp_ha::dut_reset_constant_asymmetry                 #
#                         ptp_ha::dut_reset_scaled_delay_coefficient           #
#                         ptp_ha::dut_reset_asymmetry_correction               #
#                         ptp_ha::dut_reset_clock_mode                         #
#                         ptp_ha::dut_reset_clock_step                         #
#                         ptp_ha::dut_reset_delay_mechanism                    #
#                         ptp_ha::dut_reset_communication_mode                 #
#                         ptp_ha::dut_reset_network_protocol                   #
#                         ptp_ha::dut_reset_domain                             #
#                         ptp_ha::dut_port_ptp_disable                         #
#                         ptp_ha::dut_global_ptp_disable                       #
#                         ptp_ha::dut_reset_vlan_priority                      #
#                         ptp_ha::dut_reset_vlan                               #
#                         ptp_ha::dut_reset_ipv4_address                       #
#                         ptp_ha::dut_port_disable                             #
#                                                                              #
# USAGE              :                                                         #
#                                                                              #
#     ptp_ha::dut_cleanup_setup_005                                            #
#                                                                              #
################################################################################

proc ptp_ha::dut_cleanup_setup_005 {args} {

    array set param $args

    global env ptp_ha_env

    set ip_address             $env(dut_TEST_ADDRESS1)
    set net_mask               $env(dut_TEST_MASK1)

    set vlan_priority          $ptp_ha_env(DUT_PTP_HA_VLAN_PRIORITY)

    set ptp_vlan_encap         $ptp_ha_env(DUT_PTP_HA_IS_VLAN_ENCAP)

    set network_protocol       $ptp_ha_env(DUT_PTP_HA_TRANS_TYPE)
    set communication_mode     $ptp_ha_env(DUT_PTP_HA_COM_MODEL)

    set port_list              $::dut_port_num_1
    set domain                 $::ptp_dut_default_domain_number
    set delay_mechanism        $::DUT_PTP_DELAY_MECHANISM(E2E)

    set ptp_version            $::ptp_version

    set vlan_id                $::ptp_ha_vlan_id

    ### Check Input parameters ###

    LOG -level 0 -msg "Configuration Cleanup. Please ignore the ERRORS\n"

    ptp_ha::dut_reset_priority1

    ptp_ha::dut_reset_priority2

    ptp_ha::dut_tcr_rcr_cr_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_l1sync_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_announce_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_announce_timeout\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_sync_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_delayreq_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_l1sync_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_l1sync_timeout\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_egress_latency\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_ingress_latency\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_constant_asymmetry\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_scaled_delay_coefficient\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_asymmetry_correction\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_clock_mode\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_clock_step\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_delay_mechanism\
                    -port_num               [lindex $port_list 0]\
                    -delay_mechanism        $delay_mechanism

    ptp_ha::dut_reset_communication_mode\
                   -port_num                [lindex $port_list 0]

    ptp_ha::dut_reset_network_protocol\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_domain

    ptp_ha::dut_port_ptp_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_global_ptp_disable

    if {$ptp_vlan_encap == "true"} {

        ptp_ha::dut_reset_vlan_priority\
                        -vlan_id                $vlan_id\
                        -vlan_priority          $vlan_priority\
                        -port_num               [lindex $port_list 0]

        ptp_ha::dut_reset_vlan\
                        -vlan_id                $vlan_id\
                        -port_num               [lindex $port_list 0]

    }

    if {$::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4)} {

        ptp_ha::dut_reset_ipv4_address\
                        -port_num               [lindex $port_list 0]\
                        -ip_address             $ip_address\
                        -net_mask               $net_mask\
                        -vlan_id                $vlan_id
    }

    ptp_ha::dut_port_disable\
                    -port_num               [lindex $port_list 0]

    set env(TEE_ATTEST_MANUAL_PROMPT)      1

    return 1
}


#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_cleanup_setup_006                           #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on success (if the configuration of test setup 6 is   #
#                                    cleared in DUT).                          #
#                                                                              #
#                      0 on failure (if the DUT could not be cleared for       #
#                                    test setup 6).                            #
#                                                                              #
#  DEFINITION        : This function clears the configurations made for test   #
#                      setup 6 at the DUT.                                     #
#                                                                              #
#                      This function in turn calls below functions             #
#                                                                              #
#                         ptp_ha::dut_reset_priority1                          #
#                         ptp_ha::dut_reset_priority2                          #
#                         ptp_ha::dut_tcr_rcr_cr_disable                       #
#                         ptp_ha::dut_l1sync_disable                           #
#                         ptp_ha::dut_reset_announce_interval                  #
#                         ptp_ha::dut_reset_announce_timeout                   #
#                         ptp_ha::dut_reset_sync_interval                      #
#                         ptp_ha::dut_reset_delayreq_interval                  #
#                         ptp_ha::dut_reset_l1sync_interval                    #
#                         ptp_ha::dut_reset_l1sync_timeout                     #
#                         ptp_ha::dut_reset_egress_latency                     #
#                         ptp_ha::dut_reset_ingress_latency                    #
#                         ptp_ha::dut_reset_constant_asymmetry                 #
#                         ptp_ha::dut_reset_scaled_delay_coefficient           #
#                         ptp_ha::dut_reset_asymmetry_correction               #
#                         ptp_ha::dut_reset_clock_mode                         #
#                         ptp_ha::dut_reset_clock_step                         #
#                         ptp_ha::dut_reset_delay_mechanism                    #
#                         ptp_ha::dut_reset_communication_mode                 #
#                         ptp_ha::dut_reset_network_protocol                   #
#                         ptp_ha::dut_reset_domain                             #
#                         ptp_ha::dut_port_ptp_disable                         #
#                         ptp_ha::dut_global_ptp_disable                       #
#                         ptp_ha::dut_reset_vlan_priority                      #
#                         ptp_ha::dut_reset_vlan                               #
#                         ptp_ha::dut_reset_ipv4_address                       #
#                         ptp_ha::dut_port_disable                             #
#                                                                              #
# USAGE              :                                                         #
#                                                                              #
#     ptp_ha::dut_cleanup_setup_006                                            #
#                                                                              #
################################################################################

proc ptp_ha::dut_cleanup_setup_006 {args} {

    array set param $args

    global env ptp_ha_env

    set ip_address             $env(dut_TEST_ADDRESS1)
    set net_mask               $env(dut_TEST_MASK1)

    set vlan_priority          $ptp_ha_env(DUT_PTP_HA_VLAN_PRIORITY)

    set ptp_vlan_encap         $ptp_ha_env(DUT_PTP_HA_IS_VLAN_ENCAP)

    set network_protocol       $ptp_ha_env(DUT_PTP_HA_TRANS_TYPE)
    set communication_mode     $ptp_ha_env(DUT_PTP_HA_COM_MODEL)

    set port_list              $::dut_port_num_1
    set domain                 $::ptp_dut_default_domain_number
    set delay_mechanism        $::DUT_PTP_DELAY_MECHANISM(E2E)

    set ptp_version            $::ptp_version

    set vlan_id                $::ptp_ha_vlan_id

    ### Check Input parameters ###

    LOG -level 0 -msg "Configuration Cleanup. Please ignore the ERRORS\n"

    ptp_ha::dut_reset_priority1

    ptp_ha::dut_reset_priority2

    ptp_ha::dut_external_port_config_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_tcr_rcr_cr_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_l1sync_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_announce_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_announce_timeout\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_sync_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_delayreq_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_l1sync_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_l1sync_timeout\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_egress_latency\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_ingress_latency\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_constant_asymmetry\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_scaled_delay_coefficient\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_asymmetry_correction\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_clock_mode\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_clock_step\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_delay_mechanism\
                    -port_num               [lindex $port_list 0]\
                    -delay_mechanism        $delay_mechanism

    ptp_ha::dut_reset_communication_mode\
                   -port_num                [lindex $port_list 0]

    ptp_ha::dut_reset_network_protocol\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_domain

    ptp_ha::dut_port_ptp_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_global_ptp_disable

    if {$ptp_vlan_encap == "true"} {

        ptp_ha::dut_reset_vlan_priority\
                        -vlan_id                $vlan_id\
                        -vlan_priority          $vlan_priority\
                        -port_num               [lindex $port_list 0]

        ptp_ha::dut_reset_vlan\
                        -vlan_id                $vlan_id\
                        -port_num               [lindex $port_list 0]

    }

    if {$::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4)} {

        ptp_ha::dut_reset_ipv4_address\
                        -port_num               [lindex $port_list 0]\
                        -ip_address             $ip_address\
                        -net_mask               $net_mask\
                        -vlan_id                $vlan_id
    }

    ptp_ha::dut_port_disable\
                    -port_num                   [lindex $port_list 0]

    return 1

}


#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_cleanup_setup_007                           #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on success (if the configuration of test setup 7 is   #
#                                    cleared in DUT).                          #
#                                                                              #
#                      0 on failure (if the DUT could not be cleared for       #
#                                    test setup 7).                            #
#                                                                              #
#  DEFINITION        : This function clears the configurations made for test   #
#                      setup 7 at the DUT.                                     #
#                                                                              #
#                      This function in turn calls below functions             #
#                                                                              #
#                         ptp_ha::dut_reset_priority1                          #
#                         ptp_ha::dut_reset_priority2                          #
#                         ptp_ha::dut_tcr_rcr_cr_disable                       #
#                         ptp_ha::dut_l1sync_disable                           #
#                         ptp_ha::dut_reset_announce_interval                  #
#                         ptp_ha::dut_reset_announce_timeout                   #
#                         ptp_ha::dut_reset_sync_interval                      #
#                         ptp_ha::dut_reset_delayreq_interval                  #
#                         ptp_ha::dut_reset_l1sync_interval                    #
#                         ptp_ha::dut_reset_l1sync_timeout                     #
#                         ptp_ha::dut_reset_egress_latency                     #
#                         ptp_ha::dut_reset_ingress_latency                    #
#                         ptp_ha::dut_reset_constant_asymmetry                 #
#                         ptp_ha::dut_reset_scaled_delay_coefficient           #
#                         ptp_ha::dut_reset_asymmetry_correction               #
#                         ptp_ha::dut_reset_clock_mode                         #
#                         ptp_ha::dut_reset_clock_step                         #
#                         ptp_ha::dut_reset_delay_mechanism                    #
#                         ptp_ha::dut_reset_communication_mode                 #
#                         ptp_ha::dut_reset_network_protocol                   #
#                         ptp_ha::dut_reset_domain                             #
#                         ptp_ha::dut_port_ptp_disable                         #
#                         ptp_ha::dut_global_ptp_disable                       #
#                         ptp_ha::dut_reset_vlan_priority                      #
#                         ptp_ha::dut_reset_vlan                               #
#                         ptp_ha::dut_reset_ipv4_address                       #
#                         ptp_ha::dut_port_disable                             #
#                                                                              #
# USAGE              :                                                         #
#                                                                              #
#     ptp_ha::dut_cleanup_setup_007                                            #
#                                                                              #
################################################################################

proc ptp_ha::dut_cleanup_setup_007 {args} {

    array set param $args

    global env ptp_ha_env

    set ip_address_list        [list $env(dut_TEST_ADDRESS1) $env(dut_TEST_ADDRESS2)]
    set net_mask_list          [list $env(dut_TEST_MASK1) $env(dut_TEST_MASK2)]
    set vlan_id_1              $::ptp_ha_vlan_id
    set vlan_id_2              $::ptp_ha_vlan_id_2

    set vlan_id_list           [list $vlan_id_1 $vlan_id_2]

    set vlan_priority          $ptp_ha_env(DUT_PTP_HA_VLAN_PRIORITY)

    set ptp_vlan_encap         $ptp_ha_env(DUT_PTP_HA_IS_VLAN_ENCAP)

    set network_protocol       $ptp_ha_env(DUT_PTP_HA_TRANS_TYPE)
    set communication_mode     $ptp_ha_env(DUT_PTP_HA_COM_MODEL)

    set port_list              [list $::dut_port_num_1 $::dut_port_num_2]
    set domain                 $::ptp_dut_default_domain_number
    set delay_mechanism        $::DUT_PTP_DELAY_MECHANISM(E2E)

    set ptp_version            $::ptp_version

    ### Check Input parameters ###

    LOG -level 0 -msg "Configuration Cleanup. Please ignore the ERRORS\n"

    ptp_ha::dut_reset_priority1

    ptp_ha::dut_reset_priority2

    ptp_ha::dut_external_port_config_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_external_port_config_disable\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_tcr_rcr_cr_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_tcr_rcr_cr_disable\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_l1sync_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_l1sync_disable\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_announce_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_announce_interval\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_announce_timeout\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_announce_timeout\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_sync_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_sync_interval\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_delayreq_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_delayreq_interval\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_l1sync_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_l1sync_interval\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_l1sync_timeout\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_l1sync_timeout\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_egress_latency\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_egress_latency\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_ingress_latency\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_ingress_latency\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_constant_asymmetry\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_constant_asymmetry\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_scaled_delay_coefficient\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_scaled_delay_coefficient\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_asymmetry_correction\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_asymmetry_correction\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_clock_mode\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_clock_mode\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_clock_step\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_clock_step\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_delay_mechanism\
                    -port_num               [lindex $port_list 0]\
                    -delay_mechanism        $delay_mechanism

    ptp_ha::dut_reset_delay_mechanism\
                    -port_num               [lindex $port_list 1]\
                    -delay_mechanism        $delay_mechanism

    ptp_ha::dut_reset_communication_mode\
                   -port_num                [lindex $port_list 0]

    ptp_ha::dut_reset_communication_mode\
                   -port_num                [lindex $port_list 1]

    ptp_ha::dut_reset_network_protocol\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_network_protocol\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_reset_domain

    ptp_ha::dut_port_ptp_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_port_ptp_disable\
                    -port_num               [lindex $port_list 1]

    ptp_ha::dut_global_ptp_disable

    if {$ptp_vlan_encap == "true"} {

        ptp_ha::dut_reset_vlan_priority\
                        -vlan_id                [lindex $vlan_id_list 0]\
                        -vlan_priority          $vlan_priority\
                        -port_num               [lindex $port_list 0]

        ptp_ha::dut_reset_vlan_priority\
                        -vlan_id                [lindex $vlan_id_list 1]\
                        -vlan_priority          $vlan_priority\
                        -port_num               [lindex $port_list 1]

        ptp_ha::dut_reset_vlan\
                        -vlan_id                [lindex $vlan_id_list 0]\
                        -port_num               [lindex $port_list 0]

        ptp_ha::dut_reset_vlan\
                        -vlan_id                [lindex $vlan_id_list 1]\
                        -port_num               [lindex $port_list 1]
    }

    if {$::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4)} {

        ptp_ha::dut_reset_ipv4_address\
                        -port_num               [lindex $port_list 0]\
                        -ip_address             [lindex $ip_address_list 0]\
                        -net_mask               [lindex $net_mask_list 0]\
                        -vlan_id                [lindex $vlan_id_list 0]

        ptp_ha::dut_reset_ipv4_address\
                        -port_num               [lindex $port_list 1]\
                        -ip_address             [lindex $ip_address_list 1]\
                        -net_mask               [lindex $net_mask_list 1]\
                        -vlan_id                [lindex $vlan_id_list 1]
    }

    ptp_ha::dut_port_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_port_disable\
                    -port_num               [lindex $port_list 1]

    set env(TEE_ATTEST_MANUAL_PROMPT)      1

    return 1
}


#8.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_cleanup_setup_008                           #
#                                                                              #
#  INPUT PARAMETERS   : NONE.                                                  #
#                                                                              #
#  OUTPUT PARAMETERS  : NONE.                                                  #
#                                                                              #
#  RETURNS           : 1 on success (if the configuration of test setup 8 is   #
#                                    cleared in DUT).                          #
#                                                                              #
#                      0 on failure (if the DUT could not be cleared for       #
#                                    test setup 8).                            #
#                                                                              #
#  DEFINITION        : This function clears the configurations made for test   #
#                      setup 8 at the DUT.                                     #
#                                                                              #
#                      This function in turn calls below functions             #
#                                                                              #
#                         ptp_ha::dut_reset_priority1                          #
#                         ptp_ha::dut_reset_priority2                          #
#                         ptp_ha::dut_tcr_rcr_cr_disable                       #
#                         ptp_ha::dut_l1sync_disable                           #
#                         ptp_ha::dut_reset_announce_interval                  #
#                         ptp_ha::dut_reset_announce_timeout                   #
#                         ptp_ha::dut_reset_sync_interval                      #
#                         ptp_ha::dut_reset_delayreq_interval                  #
#                         ptp_ha::dut_reset_l1sync_interval                    #
#                         ptp_ha::dut_reset_l1sync_timeout                     #
#                         ptp_ha::dut_reset_egress_latency                     #
#                         ptp_ha::dut_reset_ingress_latency                    #
#                         ptp_ha::dut_reset_constant_asymmetry                 #
#                         ptp_ha::dut_reset_scaled_delay_coefficient           #
#                         ptp_ha::dut_reset_asymmetry_correction               #
#                         ptp_ha::dut_reset_clock_mode                         #
#                         ptp_ha::dut_reset_clock_step                         #
#                         ptp_ha::dut_reset_delay_mechanism                    #
#                         ptp_ha::dut_reset_communication_mode                 #
#                         ptp_ha::dut_reset_network_protocol                   #
#                         ptp_ha::dut_reset_domain                             #
#                         ptp_ha::dut_port_ptp_disable                         #
#                         ptp_ha::dut_global_ptp_disable                       #
#                         ptp_ha::dut_reset_vlan_priority                      #
#                         ptp_ha::dut_reset_vlan                               #
#                         ptp_ha::dut_reset_ipv4_address                       #
#                         ptp_ha::dut_port_disable                             #
#                                                                              #
# USAGE              :                                                         #
#                                                                              #
#     ptp_ha::dut_cleanup_setup_008                                            #
#                                                                              #
################################################################################

proc ptp_ha::dut_cleanup_setup_008 {args} {

    array set param $args

    global env ptp_ha_env

    set ip_address             $env(dut_TEST_ADDRESS1)
    set net_mask               $env(dut_TEST_MASK1)

    set vlan_priority          $ptp_ha_env(DUT_PTP_HA_VLAN_PRIORITY)

    set ptp_vlan_encap         $ptp_ha_env(DUT_PTP_HA_IS_VLAN_ENCAP)

    set network_protocol       $ptp_ha_env(DUT_PTP_HA_TRANS_TYPE)
    set communication_mode     $ptp_ha_env(DUT_PTP_HA_COM_MODEL)

    set port_list              $::dut_port_num_1
    set domain                 $::ptp_dut_default_domain_number
    set delay_mechanism        $::DUT_PTP_DELAY_MECHANISM(E2E)

    set ptp_version            $::ptp_version

    set vlan_id                $::ptp_ha_vlan_id

    ### Check Input parameters ###

    LOG -level 0 -msg "Configuration Cleanup. Please ignore the ERRORS\n"

    ptp_ha::dut_reset_priority1

    ptp_ha::dut_reset_priority2

    ptp_ha::dut_tcr_rcr_cr_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_l1sync_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_announce_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_announce_timeout\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_sync_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_delayreq_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_l1sync_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_l1sync_timeout\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_egress_latency\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_ingress_latency\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_constant_asymmetry\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_scaled_delay_coefficient\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_asymmetry_correction\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_clock_mode\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_clock_step\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_delay_mechanism\
                    -port_num               [lindex $port_list 0]\
                    -delay_mechanism        $delay_mechanism

    ptp_ha::dut_reset_communication_mode\
                   -port_num                [lindex $port_list 0]

    ptp_ha::dut_reset_network_protocol\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_domain

    ptp_ha::dut_port_ptp_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_global_ptp_disable

    if {$ptp_vlan_encap == "true"} {

        ptp_ha::dut_reset_vlan_priority\
                        -vlan_id                $vlan_id\
                        -vlan_priority          $vlan_priority\
                        -port_num               [lindex $port_list 0]

        ptp_ha::dut_reset_vlan\
                        -vlan_id                $vlan_id\
                        -port_num               [lindex $port_list 0]

    }

    if {$::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4)} {

        ptp_ha::dut_reset_ipv4_address\
                        -port_num               [lindex $port_list 0]\
                        -ip_address             $ip_address\
                        -net_mask               $net_mask\
                        -vlan_id                $vlan_id
    }

    ptp_ha::dut_port_disable\
                    -port_num               [lindex $port_list 0]

    set env(TEE_ATTEST_MANUAL_PROMPT)      1

    return 1
}


#9.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::dut_cleanup_setup_009                           #
#                                                                              #
#  INPUT PARAMETERS   : NONE.                                                  #
#                                                                              #
#  OUTPUT PARAMETERS  : NONE.                                                  #
#                                                                              #
#  RETURNS           : 1 on success (if the configuration of test setup 9 is   #
#                                    cleared in DUT).                          #
#                                                                              #
#                      0 on failure (if the DUT could not be cleared for       #
#                                    test setup 9).                            #
#                                                                              #
#  DEFINITION        : This function clears the configurations made for test   #
#                      setup 9 at the DUT.                                     #
#                                                                              #
#                      This function in turn calls below functions             #
#                                                                              #
#                         ptp_ha::dut_reset_priority1                          #
#                         ptp_ha::dut_reset_priority2                          #
#                         ptp_ha::dut_tcr_rcr_cr_disable                       #
#                         ptp_ha::dut_l1sync_disable                           #
#                         ptp_ha::dut_reset_announce_interval                  #
#                         ptp_ha::dut_reset_announce_timeout                   #
#                         ptp_ha::dut_reset_sync_interval                      #
#                         ptp_ha::dut_reset_delayreq_interval                  #
#                         ptp_ha::dut_reset_l1sync_interval                    #
#                         ptp_ha::dut_reset_l1sync_timeout                     #
#                         ptp_ha::dut_reset_egress_latency                     #
#                         ptp_ha::dut_reset_ingress_latency                    #
#                         ptp_ha::dut_reset_constant_asymmetry                 #
#                         ptp_ha::dut_reset_scaled_delay_coefficient           #
#                         ptp_ha::dut_reset_asymmetry_correction               #
#                         ptp_ha::dut_reset_clock_mode                         #
#                         ptp_ha::dut_reset_clock_step                         #
#                         ptp_ha::dut_reset_delay_mechanism                    #
#                         ptp_ha::dut_reset_communication_mode                 #
#                         ptp_ha::dut_reset_network_protocol                   #
#                         ptp_ha::dut_reset_domain                             #
#                         ptp_ha::dut_port_ptp_disable                         #
#                         ptp_ha::dut_global_ptp_disable                       #
#                         ptp_ha::dut_reset_vlan_priority                      #
#                         ptp_ha::dut_reset_vlan                               #
#                         ptp_ha::dut_reset_ipv4_address                       #
#                         ptp_ha::dut_port_disable                             #
#                                                                              #
# USAGE              :                                                         #
#                                                                              #
#     ptp_ha::dut_cleanup_setup_009                                            #
#                                                                              #
################################################################################

proc ptp_ha::dut_cleanup_setup_009 {args} {

    array set param $args

    global env ptp_ha_env

    set ip_address             $env(dut_TEST_ADDRESS1)
    set net_mask               $env(dut_TEST_MASK1)

    set vlan_priority          $ptp_ha_env(DUT_PTP_HA_VLAN_PRIORITY)

    set ptp_vlan_encap         $ptp_ha_env(DUT_PTP_HA_IS_VLAN_ENCAP)

    set network_protocol       $ptp_ha_env(DUT_PTP_HA_TRANS_TYPE)
    set communication_mode     $ptp_ha_env(DUT_PTP_HA_COM_MODEL)

    set port_list              $::dut_port_num_1
    set domain                 $::ptp_dut_default_domain_number
    set delay_mechanism        $::DUT_PTP_DELAY_MECHANISM(E2E)

    set ptp_version            $::ptp_version

    set vlan_id                $::ptp_ha_vlan_id

    ### Check Input parameters ###

    LOG -level 0 -msg "Configuration Cleanup. Please ignore the ERRORS\n"

    ptp_ha::dut_reset_priority1

    ptp_ha::dut_reset_priority2

    ptp_ha::dut_tcr_rcr_cr_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_l1sync_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_announce_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_announce_timeout\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_sync_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_delayreq_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_l1sync_interval\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_l1sync_timeout\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_egress_latency\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_ingress_latency\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_constant_asymmetry\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_scaled_delay_coefficient\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_asymmetry_correction\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_clock_mode\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_clock_step\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_delay_mechanism\
                    -port_num               [lindex $port_list 0]\
                    -delay_mechanism        $delay_mechanism

    ptp_ha::dut_reset_communication_mode\
                   -port_num                [lindex $port_list 0]

    ptp_ha::dut_reset_network_protocol\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_reset_domain

    ptp_ha::dut_port_ptp_disable\
                    -port_num               [lindex $port_list 0]

    ptp_ha::dut_global_ptp_disable

    if {$ptp_vlan_encap == "true"} {

        ptp_ha::dut_reset_vlan_priority\
                        -vlan_id                $vlan_id\
                        -vlan_priority          $vlan_priority\
                        -port_num               [lindex $port_list 0]

        ptp_ha::dut_reset_vlan\
                        -vlan_id                $vlan_id\
                        -port_num               [lindex $port_list 0]

    }

    if {$::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4)} {

        ptp_ha::dut_reset_ipv4_address\
                        -port_num               [lindex $port_list 0]\
                        -ip_address             $ip_address\
                        -net_mask               $net_mask\
                        -vlan_id                $vlan_id
    }

    ptp_ha::dut_port_disable\
                    -port_num               [lindex $port_list 0]

    set env(TEE_ATTEST_MANUAL_PROMPT)      1

    return 1
}
