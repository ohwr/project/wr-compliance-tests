###############################################################################
# File Name         : manual_ptp-ha_functions.tcl                             #
# File Version      : 1.6                                                     #
# Component Name    : ATTEST DEVICE UNDER TEST (DUT)                          #
# Module Name       : Precision Time Protocol - High Accuracy                 #
###############################################################################
# History       Date       Author         Addition/ Alteration                #
###############################################################################
#                                                                             #
#  1.0       May/2018      CERN         Initial                               #
#  1.1       Jul/2018      CERN         Mentioned to convert timestamp into   #
#                                       epoch format                          #
#  1.2       Sep/2018      CERN         Added new procedures                  #
#                                       a) timestamp_corrected_tx_enable      #
#                                       b) timestamp_corrected_tx_disable     #
#                                       c) check_slave_only                   #
#                                       d) check_master_only                  #
#                                       d) Added units for Ingress/Egress     #
#                                          latencies, Constant Asymmetry and  #
#                                          scaledDelayCoefficient             #
#  1.3       Sep/2018      CERN         Added support to transmit PTP messages#
#                                       at background of manual pop-up.       #
#  1.4       Oct/2018      CERN         Added manual_get_mean_link_delay.     #
#  1.5       Nov/2018      CERN         a) Removed                            #
#                                          manual_get_offset_from_master.     #
#                                       b) Added                              #
#                                    manual_check_for_least_offset_from_master#
#  1.6       Jan/2019      CERN         Did editorial change in               #
#                                       check_for_least_offset_from_master.   #
#                                                                             #
###############################################################################
# Copyright (c) 2018 - 2019 CERN                                              #
###############################################################################

######################### PROCEDURES USED #####################################
#                                                                             #
#  DUT SET FUNCTIONS     :                                                    #
#                                                                             #
#  1) ptp_ha::manual_port_enable                                              #
#  2) ptp_ha::manual_port_ptp_enable                                          #
#  3) ptp_ha::manual_set_vlan                                                 #
#  4) ptp_ha::manual_set_vlan_priority                                        #
#  5) ptp_ha::manual_global_ptp_enable                                        #
#  6) ptp_ha::manual_set_communication_mode                                   #
#  7) ptp_ha::manual_set_domain                                               #
#  8) ptp_ha::manual_set_network_protocol                                     #
#  9) ptp_ha::manual_set_clock_mode                                           #
# 10) ptp_ha::manual_set_clock_step                                           #
# 11) ptp_ha::manual_set_delay_mechanism                                      #
# 12) ptp_ha::manual_set_priority1                                            #
# 13) ptp_ha::manual_set_priority2                                            #
# 14) ptp_ha::manual_tcr_rcr_cr_enable                                        #
# 15) ptp_ha::manual_l1sync_enable                                            #
# 16) ptp_ha::manual_l1sync_opt_params_enable                                 #
# 17) ptp_ha::manual_slaveonly_enable                                         #
# 18) ptp_ha::manual_masteronly_enable                                        #
# 19) ptp_ha::manual_set_announce_interval                                    #
# 20) ptp_ha::manual_set_announce_timeout                                     #
# 21) ptp_ha::manual_set_sync_interval                                        #
# 22) ptp_ha::manual_set_delayreq_interval                                    #
# 23) ptp_ha::manual_set_pdelayreq_interval                                   #
# 24) ptp_ha::manual_set_l1sync_interval                                      #
# 25) ptp_ha::manual_set_l1sync_timeout                                       #
# 26) ptp_ha::manual_set_egress_latency                                       #
# 27) ptp_ha::manual_set_ingress_latency                                      #
# 28) ptp_ha::manual_set_constant_asymmetry                                   #
# 29) ptp_ha::manual_set_scaled_delay_coefficient                             #
# 30) ptp_ha::manual_set_asymmetry_correction                                 #
# 31) ptp_ha::manual_external_port_config_enable                              #
# 32) ptp_ha::manual_set_epc_desired_state                                    #
# 33) ptp_ha::manual_set_ipv4_address                                         #
# 34) ptp_ha::manual_timestamp_corrected_tx_enable                            #
#                                                                             #
#  DUT DISABLE FUNCTIONS :                                                    #
#                                                                             #
#  1) ptp_ha::manual_port_disable                                             #
#  2) ptp_ha::manual_port_ptp_disable                                         #
#  3) ptp_ha::manual_reset_vlan                                               #
#  4) ptp_ha::manual_reset_vlan_priority                                      #
#  5) ptp_ha::manual_global_ptp_disable                                       #
#  6) ptp_ha::manual_reset_communication_mode                                 #
#  7) ptp_ha::manual_reset_domain                                             #
#  8) ptp_ha::manual_reset_network_protocol                                   #
#  9) ptp_ha::manual_reset_clock_mode                                         #
# 10) ptp_ha::manual_reset_clock_step                                         #
# 11) ptp_ha::manual_reset_delay_mechanism                                    #
# 12) ptp_ha::manual_reset_priority1                                          #
# 13) ptp_ha::manual_reset_priority2                                          #
# 14) ptp_ha::manual_tcr_rcr_cr_disable                                       #
# 15) ptp_ha::manual_l1sync_disable                                           #
# 16) ptp_ha::manual_l1sync_opt_params_disable                                #
# 17) ptp_ha::manual_slaveonly_disable                                        #
# 18) ptp_ha::manual_masteronly_disable                                       #
# 19) ptp_ha::manual_reset_announce_interval                                  #
# 20) ptp_ha::manual_reset_announce_timeout                                   #
# 21) ptp_ha::manual_reset_sync_interval                                      #
# 22) ptp_ha::manual_reset_delayreq_interval                                  #
# 23) ptp_ha::manual_reset_pdelayreq_interval                                 #
# 24) ptp_ha::manual_reset_l1sync_interval                                    #
# 25) ptp_ha::manual_reset_l1sync_timeout                                     #
# 26) ptp_ha::manual_reset_egress_latency                                     #
# 27) ptp_ha::manual_reset_ingress_latency                                    #
# 28) ptp_ha::manual_reset_constant_asymmetry                                 #
# 29) ptp_ha::manual_reset_scaled_delay_coefficient                           #
# 30) ptp_ha::manual_reset_asymmetry_correction                               #
# 31) ptp_ha::manual_external_port_config_disable                             #
# 32) ptp_ha::manual_reset_epc_desired_state                                  #
# 33) ptp_ha::manual_reset_ipv4_address                                       #
# 34) ptp_ha::manual_timestamp_corrected_tx_disable                           #
#                                                                             #
#  DUT CHECK FUNCTIONS   :                                                    #
#                                                                             #
#  1) ptp_ha::manual_check_l1sync_state                                       #
#  2) ptp_ha::manual_check_ptp_port_state                                     #
#  3) ptp_ha::manual_check_asymmetry_correction                               #
#  4) ptp_ha::manual_check_egress_latency                                     #
#  5) ptp_ha::manual_check_ingress_latency                                    #
#  6) ptp_ha::manual_check_constant_asymmetry                                 #
#  7) ptp_ha::manual_check_scaled_delay_coefficient                           #
#  8) ptp_ha::manual_check_external_port_config                               #
#  9) ptp_ha::manual_check_slave_only                                         #
#  10)ptp_ha::manual_check_master_only                                        #
#  11)ptp_ha::manual_check_for_least_offset_from_master                       #
#                                                                             #
#  DUT GET FUNCTIONS     :                                                    #
#                                                                             #
#  1) ptp_ha::manual_get_timestamp                                            #
#  2) ptp_ha::manual_get_mean_delay                                           #
#  3) ptp_ha::manual_get_mean_link_delay                                      #
#  4) ptp_ha::manual_get_delay_asymmetry                                      #
#                                                                             #
###############################################################################

################################################################################
#                           MANUAL SET FUNCTIONS                               #
################################################################################

package provide ptp_ha 1.0

namespace eval ptp_ha {

    namespace export *
}

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_port_enable                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) Port needs to be made administratively up.  #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If port is made administratively up)      #
#                                                                              #
#                      0 on Failure (If port could not be made                 #
#                                    administratively up)                      #
#                                                                              #
#  DEFINITION        : This function makes the specific port administratively  #
#                      up at the DUT.                                          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_port_enable\                               #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::manual_port_enable {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_port_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Enabling port ($port_num)                                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Enable port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        set answer [GetUserResponse]

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "        Assuming that user has enabled port ($port_num)           "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_port_ptp_enable                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) Port on which ptp is to be enabled.         #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is enabled on the specified port   #
#                                    at the DUT).                              #
#                                                                              #
#                      0 on Failure (If PTP could not be enabled on the        #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function enables PTP on a specific port on the DUT.#
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_port_ptp_enable\                           #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::manual_port_ptp_enable {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_port_ptp_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Enabling PTP on port ($port_num)                                 "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Enable PTP on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has enabled PTP on port ($port_num)          "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_set_vlan                                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID with which VLAN has to be created.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#  port_num          : (Mandatory) Port which has to be associated to VLAN ID. #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If VLAN ID is created and port is         #
#                                    associated to it on the DUT).             #
#                                                                              #
#                      0 on Failure (If VLAN ID could not be created or port   #
#                                    is not associated to it on the DUT).      #
#                                                                              #
#  DEFINITION        : This function creates VLAN ID and associates given port #
#                      to it on the DUT.                                       #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_set_vlan\                                  #
#                             -vlan_id                  $vlan_id\              #
#                             -port_num                 $port_num              #
#                                                                              #
################################################################################

proc ptp_ha::manual_set_vlan {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_vlan : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_vlan : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Creating VLAN ($vlan_id) and associating port ($port_num)        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Create VLAN ($vlan_id) and associating port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has created VLAN ($vlan_id) and associated   "
        LOG -level 2 -msg "  port ($port_num)                                                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_set_vlan_priority                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID for which the priority to be        #
#                                  associated (e.g., 100)                      #
#                                                                              #
#  vlan_priority     : (Mandatory) VLAN priority to be associated              #
#                                  (e.g., 1)                                   #
#                                                                              #
#  port_num          : (Mandatory) Port which has the VLAN ID configured       #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If VLAN Priority is associated)           #
#                                                                              #
#                      0 on Failure (If VLAN ID could not be associated)       #
#                                                                              #
#  DEFINITION        : This function associates the priority to the VLAN ID    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_set_vlan\                                  #
#                             -vlan_id                  $vlan_id\              #
#                             -vlan_priority            $vlan_priority\        #
#                             -port_num                 $port_num              #
#                                                                              #
################################################################################

proc ptp_ha::manual_set_vlan_priority {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_vlan_priority : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if {[info exists param(-vlan_priority)]} {
        set vlan_priority $param(-vlan_priority)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_vlan_priority : Missing Argument    -> vlan_priority"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_vlan_priority : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting priority ($vlan_priority) to the VLAN ($vlan_id)         "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set priority ($vlan_priority) to the VLAN ($vlan_id)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has set ($vlan_priority) to the VLAN ($vlan_id)"
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_global_ptp_enable                        #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is enabled on the DUT).            #
#                                                                              #
#                      0 on Failure (If PTP could not be enabled on the DUT).  #
#                                                                              #
#  DEFINITION        : This function enables PTP on the DUT.                   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_port_ptp_enable                            #
#                                                                              #
################################################################################

proc ptp_ha::manual_global_ptp_enable {args} {

    global env

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Enabling PTP globally                                            "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Enable PTP globally<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has enabled PTP globally                     "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_set_communication_mode                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  ptp_version       : (optional)  PTP version.                                #
#                                  (Default: 2)                                #
#                                                                              #
#  communication_mode : (Mandatory) Communication mode                         #
#                                  (e.g., "Unicast"|"Multicast")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If communication mode is set on the       #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If communication mode could not be        #
#                                    set on the specified port at the DUT).    #
#                                                                              #
#  DEFINITION        : This function sets the communication mode on a specific #
#                      port of the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_set_communication_mode\                    #
#                             -port_num                $port_num\              #
#                             -clock_mode              $clock_mode\            #
#                             -ptp_version             $ptp_version\           #
#                             -communication_mode      $communication_mode     #
#                                                                              #
################################################################################

proc ptp_ha::manual_set_communication_mode {args} {

    array set param $args
    global env

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
       set port_num $param(-port_num)
    } else {
       ERRLOG -msg "ptp_ha::manual_set_communication_mode : Missing Argument    -> port_num"
       incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_communication_mode : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-communication_mode)]} {
        set communication_mode $param(-communication_mode)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_communication_mode : Missing Argument  -> communication_mode"
        incr missedArgCounter
    }

    if {[info exists param(-ptp_version)]} {
        set ptp_version $param(-ptp_version)
    } else {
        set ptp_version "2"
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting communication mode on port ($port_num)                   "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "          PTP Version        : $ptp_version                       "
        LOG -level 2 -msg "          Communication Mode : $communication_mode                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set communication mode on port ($port_num)<br>\
                            \tClock Mode         : $clock_mode<br>\
                            \tPTP Version        : $ptp_version<br>\
                            \tCommunication Mode : $communication_mode<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has set communication mode on port ($port_num)"
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "          PTP Version        : $ptp_version                       "
        LOG -level 2 -msg "          Communication Mode : $communication_mode                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_set_domain                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  domain            : (Mandatory) Domain number.                              #
#                                  (e.g., 0..255)                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If domain number is set on the DUT).      #
#                                                                              #
#                      0 on Failure (If domain number could not able to set on #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function sets domain number on the DUT.            #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_set_domain\                                #
#                             -clock_mode      $clock_mode\                    #
#                             -domain          $domain                         #
#                                                                              #
################################################################################

proc ptp_ha::manual_set_domain {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_domain : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-domain)]} {
        set domain $param(-domain)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_domain : Missing Argument    -> domain"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting domain number ($domain)                                  "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "*             Click 'Yes'     If Success                          "
        LOG -level 2 -msg "*             Click 'No'      If Failed                           "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set domain number ($domain)<br>\
                            \tClock Mode         : $clock_mode<br><br>\
                            Click 'Yes' if Success<br>\
                            Click 'No' if Failed"\
                     -type yesno

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has set domain number ($domain)              "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#8.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_set_network_protocol                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) clock_mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  network_protocol  : (Mandatory) Network protocol.                           #
#                                  (e.g., "IEEE 802.3"|"UDP/IPv4")             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If network protocol is set on specified   #
#                                    port at the DUT).                         #
#                                                                              #
#                      0 on Failure (If network protocol could not be set      #
#                                    on specified port at the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets the network protocol on a specific   #
#                      port of the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                     ptp_ha::manual_set_network_protocol\                     #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -network_protocol $network_protocol              #
#                                                                              #
################################################################################

proc ptp_ha::manual_set_network_protocol {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_network_protocol : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_network_protocol : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-network_protocol)]} {
        set network_protocol $param(-network_protocol)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_network_protocol : Missing Argument    -> network_protocol"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting network protocol ($network_protocol) on port ($port_num) "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set network protocol ($network_protocol) on port ($port_num)<br>\
                            \tClock Mode         : $clock_mode<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has set network protocol ($network_protocol) "
        LOG -level 2 -msg "  on port ($port_num)                                             "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#9.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_set_clock_mode                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP clock mode is set on specified port#
#                                    of the DUT).                              #
#                                                                              #
#                      0 on Failure (If PTP clock mode could not be set on     #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function sets clock mode on a specific port of DUT.#
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_set_clock_mode\                            #
#                             -port_num        $port_num\                      #
#                             -clock_mode      $clock_mode                     #
#                                                                              #
################################################################################

proc ptp_ha::manual_set_clock_mode {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_clock_mode : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_clock_mode : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting clock mode ($clock_mode) on port ($port_num)             "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set clock mode ($clock_mode) on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has set clock mode ($clock_mode) on port ($port_num)"
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#10.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_set_clock_step                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  clock_step        : (Mandatory) Clock step.                                 #
#                                  (e.g., "One-step"|"Two-step")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If clock-step is set on the specified     #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If clock-step could not be able to set    #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets the clock-step on a specific port on #
#                      the DUT.                                                #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_set_clock_step\                            #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -clock_step       $clock_step                    #
#                                                                              #
################################################################################

proc ptp_ha::manual_set_clock_step {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_clock_step : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_clock_step : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-clock_step)]} {
        set clock_step $param(-clock_step)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_clock_step : Missing Argument    -> clock_step"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting clock step ($clock_step) on port ($port_num)             "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set clock step ($clock_step) on port ($port_num)<br>\
                            \tClock Mode         : $clock_mode<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has set clock step ($clock_step) on port ($port_num)"
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#11.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_set_delay_mechanism                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  delay_mechanism   : (Mandatory) Delay mechanism.                            #
#                                  (e.g., e2e|p2p|disable)                     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If delay mechanism is set on specified    #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If delay mechanism could not be set on    #
#                                    specified PTP port of the DUT).           #
#                                                                              #
#  DEFINITION        : This function sets the delay mechanism to a specific    #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_set_delay_mechanism\                       #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -delay_mechanism  $delay_mechanism               #
#                                                                              #
################################################################################

proc ptp_ha::manual_set_delay_mechanism {args} {

    array set param $args
    global env

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_delay_mechanism : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_delay_mechanism : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-delay_mechanism)]} {
        set delay_mechanism $param(-delay_mechanism)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_delay_mechanism : Missing Argument    -> delay_mechanism"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting delay mechanism ($delay_mechanism) on port ($port_num)   "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set delay mechanism ($delay_mechanism) on port ($port_num)<br>\
                            \tClock Mode         : $clock_mode<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has set delay mechanism ($delay_mechanism) on"
        LOG -level 2 -msg "  port ($port_num)                                                "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#12.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_set_priority1                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority1         : (Mandatory) Prority1                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority1 is set on the DUT).          #
#                                                                              #
#                      0 on Failure (If priority1 could not be able to set on  #
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function sets the priority1 on the DUT.            #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_set_priority1\                             #
#                             -clock_mode      $clock_mode\                    #
#                             -priority1       $priority1                      #
#                                                                              #
################################################################################

proc ptp_ha::manual_set_priority1 {args} {

    array set param $args
    global env

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_priority1 : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-priority1)]} {
        set priority1  $param(-priority1)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_priority1 : Missing Argument    -> priority1"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting priority1 ($priority1)                                   "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set priority1 ($priority1)<br>\
                            \tClock Mode         : $clock_mode<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has set priority1 ($priority1)               "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#13.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_set_priority2                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority1         : (Mandatory) Prority2                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority1 is set on the DUT).          #
#                                                                              #
#                      0 on Failure (If priority1 could not be able to set on  #
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function sets the priority1 on the DUT.            #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_set_priority2\                             #
#                             -clock_mode      $clock_mode\                    #
#                             -priority2       $priority2                      #
#                                                                              #
################################################################################

proc ptp_ha::manual_set_priority2 {args} {

    array set param $args
    global env

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_priority2 : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-priority2)]} {
        set priority2  $param(-priority2)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_priority2 : Missing Argument    -> priority2"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting priority2 ($priority2)                                   "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set priority2 ($priority2)<br>\
                            \tClock Mode         : $clock_mode<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has set priority2 ($priority2)               "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}




#14.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::manual_tcr_rcr_cr_enable                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If peerTxCoherentIsRequired,              #
#                                    peerRxCoherentIsRequired and              #
#                                    peerCongruentIsRequired is enabled on the #
#                                    DUT).                                     #
#                                                                              #
#                      0 on Failure (If peerTxCoherentIsRequired,              #
#                                    peerRxCoherentIsRequired and              #
#                                    peerCongruentIsRequired could not be      #
#                                    enabled on the DUT).                      #
#                                                                              #
#                                                                              #
#  DEFINITION        : This function enables peerTxCoherentIsRequired,         #
#                      peerRxCoherentIsRequired and peerCongruentIsRequired on #
#                      the DUT.                                                #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_tcr_rcr_cr_enable\                         #
#                             -port_num                 $port_num              #
#                                                                              #
################################################################################

proc ptp_ha::manual_tcr_rcr_cr_enable {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_tcr_rcr_cr_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
       return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Enabling peerTxCoherentIsRequired, peerRxCoherentIsRequired and "
        LOG -level 2 -msg "  peerCongruentIsRequired on port ($port_num)                     "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Enable peerTxCoherentIsRequired, peerRxCoherentIsRequired<br>\
                            and peerCongruentIsRequired on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has enabled peerTxCoherentIsRequired,        "
        LOG -level 2 -msg "  peerRxCoherentIsRequired and peerCongruentIsRequired            "
        LOG -level 2 -msg "  on port ($port_num)                                             "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#15.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_l1sync_enable                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If L1Sync option is enabled on the        #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If L1Sync option could not be enabled on  #
#                                    the specified port of the DUT).           #
#                                                                              #
#  DEFINITION        : This function enables L1Sync option on a specific port  #
#                      at the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_l1sync_enable\                             #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::manual_l1sync_enable {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_l1sync_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Enabling L1Sync option on port ($port_num)                       "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Enable L1Sync option on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has enabled L1Sync option on port($port_num) "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#16.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_l1sync_opt_params_enable                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If extended format of L1Sync TLV is       #
#                                    enabled on the specified port at the DUT).#
#                                                                              #
#                      0 on Failure (If extended format of L1Sync TLV could not#
#                                    be enabled on the specified port at the   #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function enables extended format of L1Sync TLV on  #
#                      the specified port at the DUT.                          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_l1sync_opt_params_enable\                  #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::manual_l1sync_opt_params_enable {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_l1sync_opt_params_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Enabling extended format of L1Sync TLV on port ($port_num)       "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Enable extended format of L1Sync TLV on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has enabled extended format of L1Sync TLV    "
        LOG -level 2 -msg "  on ($port_num)                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#17.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_slaveonly_enable                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If slave-only is enabled on the           #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If slave-only could not be enabled on the #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function enables slave-only on the specified port  #
#                      at the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_slaveonly_enable\                          #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::manual_slaveonly_enable {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_slaveonly_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Enabling slave-only on port ($port_num)                          "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Enable slave-only on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has enabled slave-only on port ($port_num)   "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#18.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_masteronly_enable                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If master-only is enabled on the          #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If master-only could not be enabled on the#
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function enables master-only on the specified port #
#                      at the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_masteronly_enable\                         #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::manual_masteronly_enable {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_masteronly_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Enabling master-only on port ($port_num)                         "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Enable master-only on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has enabled master-only on port ($port_num)  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#19.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::manual_set_announce_interval                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  announce_interval : (Mandatory) announceInterval.                           #
#                                  (e.g., 1|2|4|8|16)                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If announceInterval value is set on       #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If announceInterval value could not be    #
#                                    set on specified port of the DUT).        #
#                                                                              #
#  DEFINITION        : This function sets announceInterval value to a specific #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_set_announce_interval\                     #
#                             -port_num                 $port_num\             #
#                             -announce_interval        $announce_interval     #
#                                                                              #
################################################################################

proc ptp_ha::manual_set_announce_interval {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_announce_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-announce_interval)]} {
        set announce_interval $param(-announce_interval)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_announce_interval : Missing Argument    -> announce_interval"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting announceInterval ($announce_interval) on port ($port_num)"
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set announceInterval ($announce_interval) on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has set announceInterval ($announce_interval)"
        LOG -level 2 -msg "  on ($port_num)                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#20.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::manual_set_announce_timeout                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  announce_timeout  : (Mandatory) announceReceiptTimeout.                     #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If announceReceiptTimeout value is set on #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If announceReceiptTimeout value could not #
#                                    be set on specified port of the DUT).     #
#                                                                              #
#  DEFINITION        : This function sets announceReceiptTimeout value to a    #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_set_announce_timeout\                      #
#                             -port_num                 $port_num\             #
#                             -announce_timeout         $announce_timeout      #
#                                                                              #
################################################################################

proc ptp_ha::manual_set_announce_timeout {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_announce_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-announce_timeout)]} {
        set announce_timeout $param(-announce_timeout)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_announce_timeout : Missing Argument    -> announce_timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting announceReceiptTimeout ($announce_timeout) on port       "
        LOG -level 2 -msg " ($port_num)                                                      "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set announceReceiptTimeout ($announce_timeout) on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "-----------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                         "
        LOG -level 2 -msg "                                                                       "
        LOG -level 2 -msg "  Assuming that user has set announceReceiptTimeout ($announce_timeout)"
        LOG -level 2 -msg "  on ($port_num)                                                       "
        LOG -level 2 -msg "                                                                       "
        LOG -level 2 -msg "-----------------------------------------------------------------------"
    }

    return 1
}


#21.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_set_sync_interval                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  sync_interval     : (Mandatory) logSyncInterval                             #
#                                  (e.g., 3)                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If logSyncInterval value is set on        #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If logSyncInterval value could not be     #
#                                    set on specified port of the DUT).        #
#                                                                              #
#  DEFINITION        : This function sets logSyncInterval value to a specific  #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_set_sync_interval\                         #
#                          -port_num              $port_num\                   #
#                          -sync_interval         $sync_interval               #
#                                                                              #
################################################################################

proc ptp_ha::manual_set_sync_interval {args} {

    array set param $args
    global env

    set missedArgCounter  0

 ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_sync_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-sync_interval)]} {
        set sync_interval $param(-sync_interval)
    } else {
        ERRLOG  -msg "ptp_ha::manual_set_sync_interval : Missing Argument    -> sync_interval"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting logSyncInterval ($sync_interval) on port ($port_num)     "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set logSyncInterval ($sync_interval) on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has set logSyncInterval ($sync_interval)     "
        LOG -level 2 -msg "  on ($port_num)                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#22.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::manual_set_delayreq_interval                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  delayreq_interval : (Mandatory) MinDelayReqInterval.                        #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If MinDelayReqInterval is set on specified#
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If MinDelayReqInterval could not be set   #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets the MinDelayReqInterval to the       #
#                      specific PTP port on the DUT.                           #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_set_delayreq_interval\                     #
#                             -port_num                 $port_num\             #
#                             -delayreq_interval        $delayreq_interval     #
#                                                                              #
################################################################################

proc ptp_ha::manual_set_delayreq_interval {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
       set port_num $param(-port_num)
    } else {
       ERRLOG  -msg "ptp_ha::manual_set_delayreq_interval : Missing Argument    -> port_num"
       incr missedArgCounter
    }

    if {[info exists param(-delayreq_interval)]} {
       set delayreq_interval $param(-delayreq_interval)
    } else {
       ERRLOG  -msg "ptp_ha::manual_set_delayreq_interval : Missing Argument    -> delayreq_interval"
       incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
     }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "----------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                      "
        LOG -level 2 -msg "                                                                      "
        LOG -level 2 -msg " Setting MinDelayReqInterval ($delayreq_interval) on port ($port_num) "
        LOG -level 2 -msg "                                                                      "
        LOG -level 2 -msg "----------------------------------------------------------------------"

        PromptString -text "Set MinDelayReqInterval ($delayreq_interval) on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "---------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                       "
        LOG -level 2 -msg "                                                                     "
        LOG -level 2 -msg "  Assuming that user has set MinDelayReqInterval ($delayreq_interval)"
        LOG -level 2 -msg "  on port ($port_num)                                                "
        LOG -level 2 -msg "                                                                     "
        LOG -level 2 -msg "---------------------------------------------------------------------"
    }

    return 1
}


#23.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::manual_set_pdelayreq_interval                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  pdelayreq_interval: (Mandatory) minPdelayReqInterval.                       #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If minPdelayReqInterval is set on         #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If minPdelayReqInterval could not be set  #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets the minPdelayReqInterval to the      #
#                      specific PTP port on the DUT.                           #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_set_pdelayreq_interval\                    #
#                             -port_num                 $port_num\             #
#                             -pdelayreq_interval       $pdelayreq_interval    #
#                                                                              #
################################################################################

proc ptp_ha::manual_set_pdelayreq_interval {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_pdelayreq_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-pdelayreq_interval)]} {
        set pdelayreq_interval $param(-pdelayreq_interval)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_pdelayreq_interval : Missing Argument    -> pdelayreq_interval"
        incr missedArgCounter
   }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "-----------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                     "
        LOG -level 2 -msg "                                                                       "
        LOG -level 2 -msg "                                                                       "
        LOG -level 2 -msg " Setting minPdelayReqInterval ($pdelayreq_interval) on port ($port_num)"
        LOG -level 2 -msg "                                                                       "
        LOG -level 2 -msg "-----------------------------------------------------------------------"

        PromptString -text "Set minPdelayReqInterval ($pdelayreq_interval) on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "-----------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                         "
        LOG -level 2 -msg "                                                                       "
        LOG -level 2 -msg "  Assuming that user has set minPdelayReqInterval ($pdelayreq_interval)"
        LOG -level 2 -msg "  on ($port_num)                                                       "
        LOG -level 2 -msg "                                                                       "
        LOG -level 2 -msg "-----------------------------------------------------------------------"
    }

    return 1
}


#24.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_set_l1sync_interval                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  l1sync_interval   : (Mandatory) logL1SyncInterval.                          #
#                                  (e.g.,3 )                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If logL1SyncInterval is set on specified  #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If logL1SyncInterval could not be set on  #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function sets the logL1SyncInterval to a specific  #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_set_l1sync_interval\                       #
#                          -port_num              $port_num\                   #
#                          -l1sync_interval       $l1sync_interval             #
#                                                                              #
################################################################################

proc ptp_ha::manual_set_l1sync_interval {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_l1sync_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-l1sync_interval)]} {
        set l1sync_interval $param(-l1sync_interval)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_l1sync_interval : Missing Argument    -> l1sync_interval"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting logL1SyncInterval ($l1sync_interval) on port ($port_num) "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set logL1SyncInterval ($l1sync_interval) on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has set logL1SyncInterval ($l1sync_interval) "
        LOG -level 2 -msg "  on port ($port_num)                                             "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#25.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_set_l1sync_timeout                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  l1sync_timeout    : (Mandatory) L1SyncReceiptTimeout.                       #
#                                  (e.g., 3)                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If L1SyncReceiptTimeout is set on         #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If L1SyncReceiptTimeout could not be set  #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets L1SyncReceiptTimeout to a specific   #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_set_l1sync_timeout\                        #
#                          -port_num              $port_num\                   #
#                          -l1sync_timeout        $l1sync_timeout              #
#                                                                              #
################################################################################

proc ptp_ha::manual_set_l1sync_timeout {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_l1sync_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-l1sync_timeout)]} {
        set l1sync_timeout $param(-l1sync_timeout)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_l1sync_timeout : Missing Argument    -> l1sync_timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "-------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                 "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg " Setting L1SyncReceiptTimeout ($l1sync_timeout) on port ($port_num)"
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "-------------------------------------------------------------------"

        PromptString -text "Set L1SyncReceiptTimeout ($l1sync_timeout) on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "-------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                     "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "  Assuming that user has set L1SyncReceiptTimeout ($l1sync_timeout)"
        LOG -level 2 -msg "  on port ($port_num)                                              "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "-------------------------------------------------------------------"
    }

    return 1
}


#26.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_set_ingress_latency                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Mandatory) timestampCorrectionPortDS.ingressLatency.   #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If Ingress latency is set on specified    #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If Ingress latency could not be set       #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets ingress latency to a specific        #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_set_ingress_latency\                       #
#                          -port_num              $port_num\                   #
#                          -latency               $latency                     #
#                                                                              #
################################################################################

proc ptp_ha::manual_set_ingress_latency {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_ingress_latency : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-latency)]} {
        set latency $param(-latency)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_ingress_latency : Missing Argument    -> latency"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting Ingress latency ($latency ns) on port ($port_num)        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set Ingress latency ($latency ns) on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has set Ingress latency ($latency ns)        "
        LOG -level 2 -msg "  on port ($port_num)                                             "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#27.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_set_egress_latency                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Mandatory) timestampCorrectionPortDS.egressLatency.    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If egress latency is set on specified     #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If egress latency could not be set        #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets egress latency to a specific         #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_set_egress_latency\                        #
#                          -port_num              $port_num\                   #
#                          -latency               $latency                     #
#                                                                              #
################################################################################

proc ptp_ha::manual_set_egress_latency {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_egress_latency : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-latency)]} {
        set latency $param(-latency)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_egress_latency : Missing Argument    -> latency"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting Egress latency ($latency ns) on port ($port_num)         "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set Egress latency ($latency ns) on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has set Egress latency ($latency ns)         "
        LOG -level 2 -msg "  on port ($port_num)                                             "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#28.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_set_constant_asymmetry                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  constant_asymmetry :(Mandatory) asymmetryCorrectionPortDS.constantAsymmetry.#
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If constant asymmetry is set on specified #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If constant asymmetry could not be set    #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets constant asymmetry to a specific     #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_set_constant_asymmetry\                    #
#                          -port_num              $port_num\                   #
#                          -constant_asymmetry    $constant_asymmetry          #
#                                                                              #
################################################################################

proc ptp_ha::manual_set_constant_asymmetry {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_constant_asymmetry : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-constant_asymmetry)]} {
        set constant_asymmetry $param(-constant_asymmetry)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_constant_asymmetry : Missing Argument    -> constant_asymmetry
"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "--------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                  "
        LOG -level 2 -msg "                                                                    "
        LOG -level 2 -msg "                                                                    "
        LOG -level 2 -msg " Setting Constant Asymmetry constant_asymmetry ($constant_asymmetry ns)"
        LOG -level 2 -msg " on port ($port_num)                                                "
        LOG -level 2 -msg "                                                                    "
        LOG -level 2 -msg "--------------------------------------------------------------------"

        PromptString -text "Set Constant Asymmetry ($constant_asymmetry ns) on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "---------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                       "
        LOG -level 2 -msg "                                                                     "
        LOG -level 2 -msg "  Assuming that user has set Constant Asymmetry ($constant_asymmetry ns)"
        LOG -level 2 -msg "  on port ($port_num)                                                "
        LOG -level 2 -msg "                                                                     "
        LOG -level 2 -msg "---------------------------------------------------------------------"
    }

    return 1
}


#29.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_set_scaled_delay_coefficient             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  delay_coefficient : (Mandatory) asymmetryCorrectionPortDS.                  #
#                                  scaledDelayCoefficient .                    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If delay coefficient is set on specified  #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If delay coefficient could not be set     #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets delay coefficeint to a specific      #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_set_scaled_delay_coefficient               #
#                          -port_num              $port_num\                   #
#                          -delay_coefficient     $delay_coefficient           #
#                                                                              #
################################################################################

proc ptp_ha::manual_set_scaled_delay_coefficient {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_scaled_delay_coefficient : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-delay_coefficient)]} {
        set delay_coefficient $param(-delay_coefficient)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_scaled_delay_coefficient : Missing Argument    -> delay_coefficient"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting Constant Asymmetry delay_coefficient ($delay_coefficient (fractional number))"
        LOG -level 2 -msg " on port ($port_num)                                              "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set Delay Coefficient ($delay_coefficient (fractional number)) on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "-------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                     "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "  Assuming that user has set Delay Coefficient ($delay_coefficient (fractional number))"
        LOG -level 2 -msg "  on port ($port_num)                                              "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "-------------------------------------------------------------------"
    }

    return 1
}


#30.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_set_asymmetry_correction                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS          : 1 on Success (If asymmetry correction is set on specified#
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If asymmetry correction could not be set  #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets asymmetry correction to a specific   #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_set_asymmetry_correction                   #
#                          -port_num              $port_num\                   #
#                                                                              #
################################################################################

proc ptp_ha::manual_set_asymmetry_correction {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_asymmetry_correction : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Enabling asymmetryCorrectionPortDS.enable on port ($port_num)    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Enable asymmetryCorrectionPortDS.enable on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has enabled asymmetryCorrectionPortDS.enable "
        LOG -level 2 -msg "  on port ($port_num)                                             "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#31.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_external_port_config_enable              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If external port configuration option is  #
#                                    enabled on the specified port at the DUT).#
#                                                                              #
#                      0 on Failure (If external port configuration option     #
#                                    could not be enabled on the specified port#
#                                    at the DUT).                              #
#                                                                              #
#  DEFINITION        : This function enables external port configuration option#
#                      on a specific port at the DUT.                          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_external_port_config_enable\               #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::manual_external_port_config_enable {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_external_port_config_enable: Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Enabling external port configuration option on port ($port_num)  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Enable external port configuration option on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has enabled external port configuration      "
        LOG -level 2 -msg "  option on ($port_num)                                           "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#32.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_set_epc_desired_state                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  desired_state     : (Mandatory) externalPortConfigurationPortDS.desiredState#
#                                  (e.g., "INITIALIZING"|"FAULTY"|"DISABLED"|  #
#                                         "LISTENING"|"PRE_MASTER"|"MASTER"|   #
#                                         "PASSIVE"|"UNCALIBRATED"|"SLAVE"     #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given externalPortConfigurationPortDS. #
#                                    desiredState is set on specified port of  #
#                                    the DUT).                                 #
#                                                                              #
#                      0 on Failure (If given externalPortConfigurationPortDS. #
#                                    desiredState could not be set on specifed #
#                                    port of the DUT).                         #
#                                                                              #
#  DEFINITION        : This function sets given externalPortConfigurationPortDS#
#                      desiredState to a specific port on the DUT.             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_set_epc_desired_state                      #
#                          -port_num              $port_num\                   #
#                          -desired_state         $desired_state               #
#                                                                              #
################################################################################

proc ptp_ha::manual_set_epc_desired_state {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_epc_desired_state : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-desired_state)]} {
        set desired_state $param(-desired_state)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_epc_desired_state : Missing Argument    -> desired_state"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting the state ($desired_state) on port ($port_num)           "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set state ($desired_state) on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has set state ($desired_state) on port       "
        LOG -level 2 -msg "  ($port_num)                                                     "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#33.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_set_ipv4_address                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  ip_address        : (Mandatory) IPv4 address.                               #
#                                  (e.g., 192.168.200.1)                       #
#                                                                              #
#  net_mask          : (Mandatory) Subnet mask.                                #
#                                  (e.g., 255.255.255.0)                       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID.                                    #
#                                  (e.g., 100)                                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given IP address is set on specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given IP address could not be set on   #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function sets given IP address to a specific port  #
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_set_ipv4_address\                          #
#                          -port_num              $port_num\                   #
#                          -ip_address            $ip_address\                 #
#                          -net_mask              $net_mask\                   #
#                          -vlan_id               $vlan_id                     #
#                                                                              #
################################################################################

proc ptp_ha::manual_set_ipv4_address {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_ipv4_address : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-ip_address)]} {
        set ip_address $param(-ip_address)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_ipv4_address : Missing Argument    -> ip_address"
        incr missedArgCounter
    }

    if {[info exists param(-net_mask)]} {
        set net_mask $param(-net_mask)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_ipv4_address : Missing Argument    -> net_mask"
        incr missedArgCounter
    }

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_ipv4_address : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }


    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Setting IP addresses on port ($port_num)                         "
        LOG -level 2 -msg "          IP Address         : $ip_address                        "
        LOG -level 2 -msg "          Netmask            : $net_mask                          "
        LOG -level 2 -msg "          VLAN ID            : $vlan_id                           "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Set IP address on port ($port_num)<br>\
                            \tIP Address         : $ip_address<br>\
                            \tNetmask            : $net_mask<br>\
                            \tVLAN ID            : $vlan_id<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has set IP address on ($port_num)            "
        LOG -level 2 -msg "          IP Address         : $ip_address                        "
        LOG -level 2 -msg "          Netmask            : $net_mask                          "
        LOG -level 2 -msg "          VLAN ID            : $vlan_id                           "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#34.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_timestamp_corrected_tx_enable            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If L1SyncOptParamsPortDS.                 #
#                                    timestampsCorrectedTx option is enabled on#
#                                    the specified port at the DUT).           #
#                                                                              #
#                      0 on Failure (If  L1SyncOptParamsPortDS.                #
#                                    timestampsCorrectedTx option could not be #
#                                    enabled on the specified port at the DUT).#
#                                                                              #
#  DEFINITION        : This function enables L1SyncOptParamsPortDS.            #
#                      timestampsCorrectedTx option on a specific port at the  #
#                      DUT.                                                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_timestamp_corrected_tx_enable\             #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::manual_timestamp_corrected_tx_enable {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_timestamp_corrected_tx_enable: Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Enabling L1SyncOptParamsPortDS.timestampsCorrectedTx option on   "
        LOG -level 2 -msg " port ($port_num)                                                 "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Enable L1SyncOptParamsPortDS.timestampsCorrectedTx option on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has enabled L1SyncOptParamsPortDS.           "
        LOG -level 2 -msg "  timestampsCorrectedTx option on ($port_num)                     "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


################################################################################
#                       MANUAL DISABLING FUNCTIONS                             #
################################################################################


#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_port_disable                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) Port needs to be made administratively down.#
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If port is made administratively down)    #
#                                                                              #
#                      0 on Failure (If port could not be made                 #
#                                    administratively down)                    #
#                                                                              #
#  DEFINITION        : This function makes the specific port administratively  #
#                      down at the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_port_disable\                              #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::manual_port_disable {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_port_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Disabling port ($port_num)                                       "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Disable port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has disabled port ($port_num)                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_port_ptp_disable                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) Port on which ptp is to be disabled.        #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is disabled on the specified port  #
#                                    at the DUT).                              #
#                                                                              #
#                      0 on Failure (If PTP could not be disabled on the       #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function disables PTP on a specific port on the DUT#
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_port_ptp_disable\                          #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::manual_port_ptp_disable {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_port_ptp_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Disabling PTP on port ($port_num)                                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Disable PTP on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has disabled PTP on port ($port_num)         "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_reset_vlan                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID with which VLAN has to be deleted.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#  port_num          : (Mandatory) Port which has to be removed from VLAN ID.  #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If port is removed from VLAN ID and VLAN  #
#                                    ID is deleted from the DUT).              #
#                                                                              #
#                      0 on Failure (If port could not be removed from VLAN ID #
#                                    and VLAN ID is not deledted from the DUT).#
#                                                                              #
#  DEFINITION        : This function removes port from VLAN ID and deletes VLAN#
#                      ID from the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_reset_vlan\                                #
#                             -vlan_id                  $vlan_id\              #
#                             -port_num                 $port_num              #
#                                                                              #
################################################################################

proc ptp_ha::manual_reset_vlan {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_vlan : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_vlan : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Deleting VLAN ($vlan_id) and associating port ($port_num)        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Delete VLAN ($vlan_id) and associating port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has deleted VLAN ($vlan_id) and associating  "
        LOG -level 2 -msg "  port ($port_num)                                                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_reset_vlan_priority                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID                                     #
#                                  (e.g., 100)                                 #
#                                                                              #
#  vlan_priority     : (Optional)  VLAN priority                               #
#                                                                              #
#  port_num          : (Mandatory) Port which has to be associated to VLAN ID. #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If VLAN priority is reset on the VLAN ID  #
#                                    on the DUT).                              #
#                                                                              #
#                      0 on Failure (If VLAN priority could not reset on the   #
#                                    VLAN ID on the DUT).                      #
#                                                                              #
#  DEFINITION        : This function resets VLAN priority on the VLAN ID on the#
#                      DUT.                                                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_set_vlan\                                  #
#                             -vlan_id                  $vlan_id\              #
#                             -vlan_priority            $vlan_priority\        #
#                             -port_num                 $port_num              #
#                                                                              #
################################################################################

proc ptp_ha::manual_set_vlan_priority {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_vlan_priority : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if {[info exists param(-vlan_priority)]} {
        set vlan_priority $param(-vlan_priority)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_vlan_priority : Missing Argument    -> vlan_priority"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_set_vlan_priority : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Associating priority ($vlan_priority) to the VLAN ($vlan_id)     "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Associate priority ($vlan_priority) to the VLAN ($vlan_id)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has associated ($vlan_priority) to the VLAN  "
        LOG -level 2 -msg "  ($vlan_id)                                                      "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_global_ptp_disable                       #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is disabled on the DUT).           #
#                                                                              #
#                      0 on Failure (If PTP could not be disabled on the DUT). #
#                                                                              #
#  DEFINITION        : This function disables PTP on the DUT.                  #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_port_ptp_disable                           #
#                                                                              #
################################################################################

proc ptp_ha::manual_global_ptp_disable {} {

    global env

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Disabling PTP globally                                           "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Disable PTP globally<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has disabled PTP globally                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_reset_communication_mode                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  ptp_version       : (optional)  PTP version.                                #
#                                  (Default: 2)                                #
#                                                                              #
#  communication_mode : (Mandatory) Communication mode                         #
#                                  (e.g., "Unicast"|"Multicast")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If communication mode is reset on the     #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If communication mode could not be        #
#                                    reset on the specified port at the DUT).  #
#                                                                              #
#  DEFINITION        : This function resets the communication mode on a        #
#                      specific port of the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_reset_communication_mode\                  #
#                             -port_num                $port_num\              #
#                             -clock_mode              $clock_mode\            #
#                             -ptp_version             $ptp_version\           #
#                             -communication_mode      $communication_mode     #
#                                                                              #
################################################################################

proc ptp_ha::manual_reset_communication_mode {args} {

    array set param $args
    global env

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
       set port_num $param(-port_num)
    } else {
       ERRLOG -msg "ptp_ha::manual_reset_communication_mode : Missing Argument    -> port_num"
       incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_communication_mode : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-communication_mode)]} {
        set communication_mode $param(-communication_mode)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_communication_mode : Missing Argument  -> communication_mode"
        incr missedArgCounter
    }

    if {[info exists param(-ptp_version)]} {
        set ptp_version $param(-ptp_version)
    } else {
        set ptp_version "2"
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting communication mode on port ($port_num)                 "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "          PTP Version        : $ptp_version                       "
        LOG -level 2 -msg "          Communication Mode : $communication_mode                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset communication mode on port ($port_num)<br>\
                            \tClock Mode         : $clock_mode<br>\
                            \tPTP Version        : $ptp_version<br>\
                            \tCommunication Mode : $communication_mode<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "---------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                       "
        LOG -level 2 -msg "                                                                     "
        LOG -level 2 -msg "  Assuming that user has reset communication mode on port ($port_num)"
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                           "
        LOG -level 2 -msg "          PTP Version        : $ptp_version                          "
        LOG -level 2 -msg "          Communication Mode : $communication_mode                   "
        LOG -level 2 -msg "                                                                     "
        LOG -level 2 -msg "---------------------------------------------------------------------"
    }

    return 1
}


#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_reset_domain                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  domain            : (Mandatory) Domain number.                              #
#                                  (e.g., 0..255)                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If domain number is reset on the DUT).    #
#                                                                              #
#                      0 on Failure (If domain number could not able to reset  #
#                                    on DUT).                                  #
#                                                                              #
#  DEFINITION        : This function resets domain number on the DUT.          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_reset_domain\                              #
#                             -clock_mode      $clock_mode\                    #
#                             -domain          $domain                         #
#                                                                              #
################################################################################

proc ptp_ha::manual_reset_domain {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_domain : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-domain)]} {
        set domain $param(-domain)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_domain : Missing Argument    -> domain"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting domain number ($domain)                                "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset domain number ($domain)<br>\
                            \tClock Mode         : $clock_mode<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset domain number ($domain)            "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#8.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_reset_network_protocol                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) clock_mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  network_protocol  : (Mandatory) Network protocol.                           #
#                                  (e.g., "IEEE 802.3"|"UDP/IPv4")             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If network protocol is reset on specified #
#                                    port at the DUT).                         #
#                                                                              #
#                      0 on Failure (If network protocol could not be reset    #
#                                    on specified port at the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets the network protocol on a specific #
#                      port of the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                     ptp_ha::manual_reset_network_protocol\                   #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -network_protocol $network_protocol              #
#                                                                              #
################################################################################

proc ptp_ha::manual_reset_network_protocol {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_network_protocol : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_network_protocol : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-network_protocol)]} {
        set network_protocol $param(-network_protocol)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_network_protocol : Missing Argument    -> network_protocol"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "-------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                 "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg " Resetting network protocol ($network_protocol) on port ($port_num)"
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                         "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "-------------------------------------------------------------------"

        PromptString -text "Reset network protocol ($network_protocol) on port ($port_num)<br>\
                            \tClock Mode         : $clock_mode<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "-------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                     "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "  Assuming that user has reset network protocol ($network_protocol)"
        LOG -level 2 -msg "  on port ($port_num)                                              "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                         "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "-------------------------------------------------------------------"
    }

    return 1
}


#9.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_reset_clock_mode                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP clock mode is reset on specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If PTP clock mode could not be reset on   #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function resets clock mode on a specific port of   #
#                      DUT.                                                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_reset_clock_mode\                          #
#                             -port_num        $port_num\                      #
#                             -clock_mode      $clock_mode                     #
#                                                                              #
################################################################################

proc ptp_ha::manual_reset_clock_mode {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_clock_mode : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_clock_mode : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting clock mode ($clock_mode) on port ($port_num)           "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset clock mode ($clock_mode) on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset clock mode ($clock_mode) on port   "
        LOG -level 2 -msg "  ($port_num)                                                     "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#10.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_reset_clock_step                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  clock_step        : (Mandatory) Clock step.                                 #
#                                  (e.g., "One-step"|"Two-step")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If clock-step is reset on the specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If clock-step could not be able to reset  #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets the clock-step on a specific port  #
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_reset_clock_step\                          #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -clock_step       $clock_step                    #
#                                                                              #
################################################################################

proc ptp_ha::manual_reset_clock_step {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_clock_step : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_clock_step : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-clock_step)]} {
        set clock_step $param(-clock_step)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_clock_step : Missing Argument    -> clock_step"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting clock step ($clock_step) on port ($port_num)           "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset clock step ($clock_step) on port ($port_num)<br>\
                            \tClock Mode         : $clock_mode<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset clock step ($clock_step) on port   "
        LOG -level 2 -msg "  ($port_num)                                                     "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#11.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_reset_delay_mechanism                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  delay_mechanism   : (Mandatory) Delay mechanism.                            #
#                                  (e.g., e2e|p2p|disable)                     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If delay mechanism is reset on specified  #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If delay mechanism could not be reset on  #
#                                    specified PTP port of the DUT).           #
#                                                                              #
#  DEFINITION        : This function resets the delay mechanism to a specific  #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_reset_delay_mechanism\                     #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -delay_mechanism  $delay_mechanism               #
#                                                                              #
################################################################################

proc ptp_ha::manual_reset_delay_mechanism {args} {

    array set param $args
    global env

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_delay_mechanism : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_delay_mechanism : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-delay_mechanism)]} {
        set delay_mechanism $param(-delay_mechanism)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_delay_mechanism : Missing Argument    -> delay_mechanism"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting delay mechanism ($delay_mechanism) on port ($port_num) "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset delay mechanism ($delay_mechanism) on port ($port_num)<br>\
                            \tClock Mode         : $clock_mode<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset delay mechanism ($delay_mechanism) "
        LOG -level 2 -msg "  on port ($port_num)                                             "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#12.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_reset_priority1                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority1         : (Mandatory) Prority1                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority1 is reset on the DUT).        #
#                                                                              #
#                      0 on Failure (If priority1 could not be able to reset on#
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function resets the priority1 on the DUT.          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_reset_priority1\                           #
#                             -clock_mode      $clock_mode\                    #
#                             -priority1       $priority1                      #
#                                                                              #
################################################################################

proc ptp_ha::manual_reset_priority1 {args} {

    array set param $args
    global env

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_priority1 : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-priority1)]} {
        set priority1  $param(-priority1)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_priority1 : Missing Argument    -> priority1"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting priority1 ($priority1)                                 "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset priority1 ($priority1)<br>\
                            \tClock Mode         : $clock_mode<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset priority1 ($priority1)             "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#13.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_reset_priority2                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority2         : (Mandatory) Prority2                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority2 is reset on the DUT).        #
#                                                                              #
#                      0 on Failure (If priority2 could not be able to reset on#
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function resets the priority2 on the DUT.          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_reset_priority2\                           #
#                             -clock_mode      $clock_mode\                    #
#                             -priority2       $priority2                      #
#                                                                              #
################################################################################

proc ptp_ha::manual_reset_priority2 {args} {

    array set param $args
    global env

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_priority2 : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-priority1)]} {
        set priority1  $param(-priority1)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_priority2 : Missing Argument    -> priority1"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting priority2 ($priority2)                                 "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset priority2 ($priority2)<br>\
                            \tClock Mode         : $clock_mode<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset priority2 ($priority2)             "
        LOG -level 2 -msg "          Clock Mode         : $clock_mode                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#14.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::manual_tcr_rcr_cr_disable                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If peerTxCoherentIsRequired,              #
#                                    peerRxCoherentIsRequired and              #
#                                    peerCongruentIsRequired is disabled on the#
#                                    DUT).                                     #
#                                                                              #
#                      0 on Failure (If peerTxCoherentIsRequired,              #
#                                    peerRxCoherentIsRequired and              #
#                                    peerCongruentIsRequired could not be      #
#                                    disabled on the DUT).                     #
#                                                                              #
#                                                                              #
#  DEFINITION        : This function disables peerTxCoherentIsRequired,        #
#                      peerRxCoherentIsRequired and peerCongruentIsRequired on #
#                      the DUT.                                                #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_tcr_rcr_cr_disable\                        #
#                             -port_num                 $port_num              #
#                                                                              #
################################################################################

proc ptp_ha::manual_tcr_rcr_cr_disable {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_tcr_rcr_cr_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
       return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Disabling peerTxCoherentIsRequired, peerRxCoherentIsRequired and"
        LOG -level 2 -msg "  peerCongruentIsRequired on port ($port_num)                     "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Disable peerTxCoherentIsRequired, peerRxCoherentIsRequired<br>\
                            and peerCongruentIsRequired on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has disabled peerTxCoherentIsRequired,       "
        LOG -level 2 -msg "  peerRxCoherentIsRequired and peerCongruentIsRequired            "
        LOG -level 2 -msg "  on port ($port_num)                                             "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#15.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_l1sync_disable                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If L1Sync option is disabled on the       #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If L1Sync option could not be disabled on #
#                                    the specified port of the DUT).           #
#                                                                              #
#  DEFINITION        : This function disables L1Sync option on a specific port #
#                      at the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_l1sync_disable\                            #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::manual_l1sync_disable {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_l1sync_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Disabling L1Sync option on port ($port_num)                      "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Disable L1Sync option on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "-------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                     "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "  Assuming that user has disabled L1Sync option on port ($port_num)"
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "-------------------------------------------------------------------"
    }

    return 1
}


#16.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_l1sync_opt_params_disable                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If extended format of L1Sync TLV is       #
#                                    disabled on the specified port at the DUT)#
#                                                                              #
#                      0 on Failure (If extended format of L1Sync TLV could not#
#                                    be disabled on the specified port at the  #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function disables extended format of L1Sync TLV on #
#                      the specified port at the DUT.                          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_l1sync_opt_params_disable\                 #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::manual_l1sync_opt_params_disable {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_l1sync_opt_params_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Disabling extended format of L1Sync TLV on port ($port_num)      "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Disable extended format of L1Sync TLV on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has disabled extended format of L1Sync TLV   "
        LOG -level 2 -msg "  on port ($port_num)                                             "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#17.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_slaveonly_disable                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If slave-only is disabled on the          #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If slave-only could not be disabled on the#
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function disables slave-only on the specified port #
#                      at the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_slaveonly_disable\                         #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::manual_slaveonly_disable {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_slaveonly_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Disabling slave-only on port ($port_num)                         "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Disable slave-only on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has disabled slave-only on port ($port_num)  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#18.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_masteronly_disable                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If master-only is disabled on the         #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If master-only could not be disabled on   #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function disables master-only on a specific port   #
#                      at the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_masteronly_disable\                        #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::manual_masteronly_disable {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_masteronly_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Disabling master-only on port ($port_num)                        "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Disable master-only on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has disabled master-only on port ($port_num) "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#19.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::manual_reset_announce_interval                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  announce_interval : (Mandatory) announceInterval.                           #
#                                  (e.g., 1|2|4|8|16)                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If announceInterval value is reset on     #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If announceInterval value could not be    #
#                                    reset on specified port of the DUT).      #
#                                                                              #
#  DEFINITION        : This function resets announceInterval value to a        #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_reset_announce_interval\                   #
#                             -port_num                 $port_num\             #
#                             -announce_interval        $announce_interval     #
#                                                                              #
################################################################################

proc ptp_ha::manual_reset_announce_interval {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_announce_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-announce_interval)]} {
        set announce_interval $param(-announce_interval)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_announce_interval : Missing Argument    -> announce_interval"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "--------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                  "
        LOG -level 2 -msg "                                                                    "
        LOG -level 2 -msg "                                                                    "
        LOG -level 2 -msg " Resetting announceInterval ($announce_interval) on port ($port_num)"
        LOG -level 2 -msg "                                                                    "
        LOG -level 2 -msg "--------------------------------------------------------------------"

        PromptString -text "Reset announceInterval ($announce_interval) on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "--------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                      "
        LOG -level 2 -msg "                                                                    "
        LOG -level 2 -msg "  Assuming that user has reset announceInterval ($announce_interval)"
        LOG -level 2 -msg "  on port ($port_num)                                               "
        LOG -level 2 -msg "                                                                    "
        LOG -level 2 -msg "--------------------------------------------------------------------"
    }

    return 1
}


#20.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::manual_reset_announce_timeout                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  announce_timeout  : (Mandatory) announceReceiptTimeout.                     #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If announceReceiptTimeout value is reset  #
#                                    on specified port of the DUT).            #
#                                                                              #
#                      0 on Failure (If announceReceiptTimeout value could not #
#                                    be reset on specified port of the DUT).   #
#                                                                              #
#  DEFINITION        : This function resets announceReceiptTimeout value to a  #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_reset_announce_timeout\                    #
#                             -port_num                 $port_num\             #
#                             -announce_timeout         $announce_timeout      #
#                                                                              #
################################################################################

proc ptp_ha::manual_reset_announce_timeout {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_announce_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-announce_timeout)]} {
        set announce_timeout $param(-announce_timeout)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_announce_timeout : Missing Argument    -> announce_timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting announceReceiptTimeout ($announce_timeout) on port     "
        LOG -level 2 -msg " ($port_num)                                                      "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset announceReceiptTimeout ($announce_timeout) on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset announceReceiptTimeout             "
        LOG -level 2 -msg "  ($announce_timeout) on port ($port_num)                         "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#21.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_reset_sync_interval                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  sync_interval     : (Mandatory) logSyncInterval                             #
#                                  (e.g., 3)                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If logSyncInterval value is reset on      #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If logSyncInterval value could not be     #
#                                    reset on specified port of the DUT).      #
#                                                                              #
#  DEFINITION        : This function resets logSyncInterval value to a specific#
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_reset_sync_interval\                       #
#                          -port_num              $port_num\                   #
#                          -sync_interval         $sync_interval               #
#                                                                              #
################################################################################

proc ptp_ha::manual_reset_sync_interval {args} {

    array set param $args
    global env

    set missedArgCounter  0

 ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_sync_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-sync_interval)]} {
        set sync_interval $param(-sync_interval)
    } else {
        ERRLOG  -msg "ptp_ha::manual_reset_sync_interval : Missing Argument    -> sync_interval"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting logSyncInterval ($sync_interval) on port ($port_num)   "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset logSyncInterval ($sync_interval) on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset logSyncInterval ($sync_interval)   "
        LOG -level 2 -msg "  on port ($port_num)                                             "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#22.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::manual_reset_delayreq_interval                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  delayreq_interval : (Mandatory) MinDelayReqInterval.                        #
#                                  (e.g., 1|2|4|8|16)                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If MinDelayReqInterval is reset on        #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If MinDelayReqInterval could not be reset #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets the MinDelayReqInterval to the     #
#                      specific PTP port on the DUT.                           #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_reset_delayreq_interval\                   #
#                             -port_num                 $port_num\             #
#                             -delayreq_interval        $delayreq_interval     #
#                                                                              #
################################################################################

proc ptp_ha::manual_reset_delayreq_interval {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
       set port_num $param(-port_num)
    } else {
       ERRLOG  -msg "ptp_ha::manual_reset_delayreq_interval : Missing Argument    -> port_num"
       incr missedArgCounter
    }

    if {[info exists param(-delayreq_interval)]} {
       set delayreq_interval $param(-delayreq_interval)
    } else {
       ERRLOG  -msg "ptp_ha::manual_reset_delayreq_interval : Missing Argument    -> delayreq_interval"
       incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
     }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "-----------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                     "
        LOG -level 2 -msg "                                                                       "
        LOG -level 2 -msg "                                                                       "
        LOG -level 2 -msg " Resetting MinDelayReqInterval ($delayreq_interval) on port ($port_num)"
        LOG -level 2 -msg "                                                                       "
        LOG -level 2 -msg "-----------------------------------------------------------------------"

        PromptString -text "Reset MinDelayReqInterval ($delayreq_interval) on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "-----------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                         "
        LOG -level 2 -msg "                                                                       "
        LOG -level 2 -msg "  Assuming that user has reset MinDelayReqInterval ($delayreq_interval)"
        LOG -level 2 -msg "  on port ($port_num)                                                  "
        LOG -level 2 -msg "                                                                       "
        LOG -level 2 -msg "-----------------------------------------------------------------------"
    }

    return 1
}


#23.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::manual_reset_pdelayreq_interval                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  pdelayreq_interval: (Mandatory) minPdelayReqInterval.                       #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If minPdelayReqInterval is reset on       #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If minPdelayReqInterval could not be reset#
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets the minPdelayReqInterval to the    #
#                      specific PTP port on the DUT.                           #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_reset_pdelayreq_interval\                  #
#                             -port_num                 $port_num\             #
#                             -pdelayreq_interval       $pdelayreq_interval    #
#                                                                              #
################################################################################

proc ptp_ha::manual_reset_pdelayreq_interval {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_pdelayreq_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-pdelayreq_interval)]} {
        set pdelayreq_interval $param(-pdelayreq_interval)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_pdelayreq_interval : Missing Argument    -> pdelayreq_interval"
        incr missedArgCounter
   }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                      "
        LOG -level 2 -msg "                                                                        "
        LOG -level 2 -msg "                                                                        "
        LOG -level 2 -msg " Resetting minPdelayReqInterval ($pdelayreq_interval) on port($port_num)"
        LOG -level 2 -msg "                                                                        "
        LOG -level 2 -msg "------------------------------------------------------------------------"

        PromptString -text "Reset minPdelayReqInterval ($pdelayreq_interval) on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset minPdelayReqInterval               "
        LOG -level 2 -msg "  ($pdelayreq_interval) on port ($port_num)                       "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#24.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_reset_l1sync_interval                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  l1sync_interval   : (Mandatory) logL1SyncInterval.                          #
#                                  (e.g.,3 )                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If logL1SyncInterval is reset on specified#
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If logL1SyncInterval could not be reset on#
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function resets the logL1SyncInterval to a specific#
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_reset_l1sync_interval\                     #
#                          -port_num              $port_num\                   #
#                          -l1sync_interval       $l1sync_interval             #
#                                                                              #
################################################################################

proc ptp_ha::manual_reset_l1sync_interval {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_l1sync_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-l1sync_interval)]} {
        set l1sync_interval $param(-l1sync_interval)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_l1sync_interval : Missing Argument    -> l1sync_interval"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "-------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                 "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg " Resetting logL1SyncInterval ($l1sync_interval) on port ($port_num)"
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "-------------------------------------------------------------------"

        PromptString -text "Reset logL1SyncInterval ($l1sync_interval) on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "-------------------------------------------------------------------" 
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                     "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "  Assuming that user has reset logL1SyncInterval ($l1sync_interval)"
        LOG -level 2 -msg "  on port ($port_num)                                              "
        LOG -level 2 -msg "                                                                   "
        LOG -level 2 -msg "-------------------------------------------------------------------"
    }

    return 1
}


#25.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_reset_l1sync_timeout                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  l1sync_timeout    : (Mandatory) L1SyncReceiptTimeout.                       #
#                                  (e.g., 3)                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If L1SyncReceiptTimeout is reset on       #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If L1SyncReceiptTimeout could not be      #
#                                    reset on specified port of the DUT).      #
#                                                                              #
#  DEFINITION        : This function resets L1SyncReceiptTimeout to a specific #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_reset_l1sync_timeout\                      #
#                          -port_num              $port_num\                   #
#                          -l1sync_timeout        $l1sync_timeout              #
#                                                                              #
################################################################################

proc ptp_ha::manual_reset_l1sync_timeout {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_l1sync_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-l1sync_timeout)]} {
        set l1sync_timeout $param(-l1sync_timeout)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_l1sync_timeout : Missing Argument    -> l1sync_timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "---------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                   "
        LOG -level 2 -msg "                                                                     "
        LOG -level 2 -msg "                                                                     "
        LOG -level 2 -msg " Resetting L1SyncReceiptTimeout ($l1sync_timeout) on port ($port_num)"
        LOG -level 2 -msg "                                                                     "
        LOG -level 2 -msg "---------------------------------------------------------------------"

        PromptString -text "Reset L1SyncReceiptTimeout ($l1sync_timeout) on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "---------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                       "
        LOG -level 2 -msg "                                                                     "
        LOG -level 2 -msg "  Assuming that user has reset L1SyncReceiptTimeout ($l1sync_timeout)"
        LOG -level 2 -msg "  on port ($port_num)                                                "
        LOG -level 2 -msg "                                                                     "
        LOG -level 2 -msg "---------------------------------------------------------------------"
    }

    return 1
}


#26.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_reset_ingress_latency                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Mandatory) timestampCorrectionPortDS.ingressLatency.   #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If Ingress latency is reset on specified  #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If Ingress latency could not be reset     #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets ingress latency to a specific      #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_reset_ingress_latency\                     #
#                          -port_num              $port_num\                   #
#                          -latency               $latency                     #
#                                                                              #
################################################################################

proc ptp_ha::manual_reset_ingress_latency {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_ingress_latency : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-latency)]} {
        set latency $param(-latency)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_ingress_latency : Missing Argument    -> latency"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting Ingress latency ($latency ns) on port ($port_num)      "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset Ingress latency ($latency ns) on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset Ingress latency ($latency ns)      "
        LOG -level 2 -msg "  on port ($port_num)                                             "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#27.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_reset_egress_latency                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Mandatory) timestampCorrectionPortDS.egressLatency.    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If egress latency is reset on specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If egress latency could not be reset      #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets egress latency to a specific       #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_reset_egress_latency\                      #
#                          -port_num              $port_num\                   #
#                          -latency               $latency                     #
#                                                                              #
################################################################################

proc ptp_ha::manual_reset_egress_latency {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_egress_latency : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-latency)]} {
        set latency $param(-latency)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_egress_latency : Missing Argument    -> latency"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting Egress latency ($latency ns) on port ($port_num)       "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset Egress latency ($latency ns) on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset Egress latency ($latency ns)       "
        LOG -level 2 -msg "  on port ($port_num)                                             "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#28.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_reset_constant_asymmetry                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  constant_asymmetry :(Mandatory) asymmetryCorrectionPortDS.constantAsymmetry.#
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If constant asymmetry is reset on         #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If constant asymmetry could not be reset  #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets constant asymmetry to a specific   #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_reset_constant_asymmetry\                  #
#                          -port_num              $port_num\                   #
#                          -constant_asymmetry    $constant_asymmetry          #
#                                                                              #
################################################################################

proc ptp_ha::manual_reset_constant_asymmetry {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_constant_asymmetry : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-constant_asymmetry)]} {
        set constant_asymmetry $param(-constant_asymmetry)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_constant_asymmetry : Missing Argument    -> constant_asymmetry
"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "----------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                      "
        LOG -level 2 -msg "                                                                      "
        LOG -level 2 -msg " Resetting Constant Asymmetry constant_asymmetry ($constant_asymmetry ns)"
        LOG -level 2 -msg " on port ($port_num)                                                  "
        LOG -level 2 -msg "                                                                      "
        LOG -level 2 -msg "----------------------------------------------------------------------"

        PromptString -text "Reset Constant Asymmetry ($constant_asymmetry ns) on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "-----------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                         "
        LOG -level 2 -msg "                                                                       "
        LOG -level 2 -msg "  Assuming that user has reset Constant Asymmetry ($constant_asymmetry ns)"
        LOG -level 2 -msg "  on port ($port_num)                                                  "
        LOG -level 2 -msg "                                                                       "
        LOG -level 2 -msg "-----------------------------------------------------------------------"
    }

    return 1
}


#29.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_reset_scaled_delay_coefficient           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  delay_coefficient : (Mandatory) asymmetryCorrectionPortDS.                  #
#                                  scaledDelayCoefficient .                    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If delay coefficient is reset on specified#
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If delay coefficient could not be reset   #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets delay coefficeint to a specific    #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_reset_scaled_delay_coefficient             #
#                          -port_num              $port_num\                   #
#                          -delay_coefficient     $delay_coefficient           #
#                                                                              #
################################################################################

proc ptp_ha::manual_reset_scaled_delay_coefficient {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_scaled_delay_coefficient : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-delay_coefficient)]} {
        set delay_coefficient $param(-delay_coefficient)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_scaled_delay_coefficient : Missing Argument    -> delay_coefficient"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "--------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                  "
        LOG -level 2 -msg "                                                                    "
        LOG -level 2 -msg "                                                                    "
        LOG -level 2 -msg " Resetting Constant Asymmetry delay_coefficient ($delay_coefficient (fractional number))"
        LOG -level 2 -msg " on port ($port_num)                                                "
        LOG -level 2 -msg "                                                                    "
        LOG -level 2 -msg "--------------------------------------------------------------------"

        PromptString -text "Reset Delay Coefficient ($delay_coefficient (fractional number)) on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "---------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                       "
        LOG -level 2 -msg "                                                                     "
        LOG -level 2 -msg "  Assuming that user has reset Delay Coefficient ($delay_coefficient (fractional number))"
        LOG -level 2 -msg "  on port ($port_num)                                                "
        LOG -level 2 -msg "                                                                     "
        LOG -level 2 -msg "---------------------------------------------------------------------"
    }

    return 1
}


#30.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::manual_reset_asymmetry_correction              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If asymmetryCorrectionPortDS.enable is    #
#                                    reset on specified port of the DUT).      #
#                                                                              #
#                      0 on Failure (If asymmetryCorrectionPortDS.enable could #
#                                    not be reset on specified port of the DUT)#
#                                                                              #
#  DEFINITION        : This function resets the asymmetryCorrectionPortDS.     #
#                      enable to the specific PTP port on the DUT.             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_reset_asymmetry_correction\                #
#                        -port_num          $port_num                          #
#                                                                              #
################################################################################

proc ptp_ha::manual_reset_asymmetry_correction {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_asymmetry_correction : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Disabling asymmetryCorrectionPortDS.enable on port ($port_num)   "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Disable asymmetryCorrectionPortDS.enable on port($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has disabled asymmetryCorrectionPortDS.enable"
        LOG -level 2 -msg "  on port ($port_num)                                             "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#31.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_external_port_config_disable             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If external port configuration option is  #
#                                    disabled on the specified port at the DUT)#
#                                                                              #
#                      0 on Failure (If external port configuration option     #
#                                    could not be disabled on the specified    #
#                                    port at the DUT).                         #
#                                                                              #
#  DEFINITION        : This function disables external port configuration      #
#                      option on a specific port at the DUT.                   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_external_port_config_disable\              #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::manual_external_port_config_disable {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_external_port_config_disable: Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Disabling external port configuration option on port ($port_num) "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Disable external port configuration option on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has disabled external port configuration     "
        LOG -level 2 -msg "  option on ($port_num)                                           "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#32.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_reset_epc_desired_state                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  desired_state     : (Mandatory) externalPortConfigurationPortDS.desiredState#
#                                  (e.g., "INITIALIZING"|"FAULTY"|"DISABLED"|  #
#                                         "LISTENING"|"PRE_MASTER"|"MASTER"|   #
#                                         "PASSIVE"|"UNCALIBRATED"|"SLAVE"     #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given externalPortConfigurationPortDS. #
#                                    desiredState is reset on specified port of#
#                                    the DUT).                                 #
#                                                                              #
#                      0 on Failure (If given externalPortConfigurationPortDS. #
#                                    desiredState could not be reset on        #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION       : This function resets given externalPortConfigurationPortDS
#                      desiredState to a specific port on the DUT.             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_reset_epc_desired_state                    #
#                          -port_num              $port_num\                   #
#                          -desired_state         $desired_state               #
#                                                                              #
################################################################################

proc ptp_ha::manual_reset_epc_desired_state {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_epc_desired_state : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-desired_state)]} {
        set desired_state $param(-desired_state)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_epc_desired_state : Missing Argument    -> desired_state"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting the state ($desired_state) on port ($port_num)         "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset state ($desired_state) on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset state ($desired_state) on port     "
        LOG -level 2 -msg "  ($port_num)                                                     "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#33.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_reset_ipv4_address                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  ip_address        : (Mandatory) IPv4 address.                               #
#                                  (e.g., 192.168.200.1)                       #
#                                                                              #
#  net_mask          : (Mandatory) Subnet mask.                                #
#                                  (e.g., 255.255.255.0)                       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID.                                    #
#                                  (e.g., 100)                                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given IP address is reset on specified #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given IP address could not be reset on #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function resets given IP address to a specific port#
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_reset_ipv4_address\                        #
#                          -port_num              $port_num\                   #
#                          -ip_address            $ip_address\                 #
#                          -net_mask              $net_mask\                   #
#                          -vlan_id               $vlan_id                     #
#                                                                              #
################################################################################

proc ptp_ha::manual_reset_ipv4_address {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_ipv4_address : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-ip_address)]} {
        set ip_address $param(-ip_address)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_ipv4_address : Missing Argument    -> ip_address"
        incr missedArgCounter
    }

    if {[info exists param(-net_mask)]} {
        set net_mask $param(-net_mask)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_ipv4_address : Missing Argument    -> net_mask"
        incr missedArgCounter
    }

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "ptp_ha::manual_reset_ipv4_address : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Resetting IP addresses on port ($port_num)                       "
        LOG -level 2 -msg "          IP Address         : $ip_address                        "
        LOG -level 2 -msg "          Netmask            : $net_mask                          "
        LOG -level 2 -msg "          VLAN ID            : $vlan_id                           "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Reset IP address on port ($port_num)<br>\
                            \tIP Address         : $ip_address<br>\
                            \tNetmask            : $net_mask<br>\
                            \tVLAN ID            : $vlan_id<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has reset IP address on ($port_num)          "
        LOG -level 2 -msg "          IP Address         : $ip_address                        "
        LOG -level 2 -msg "          Netmask            : $net_mask                          "
        LOG -level 2 -msg "          VLAN ID            : $vlan_id                           "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


#34.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_timestamp_corrected_tx_disable           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If L1SyncOptParamsPortDS.                 #
#                                    timestampsCorrectedTx option is disabled  #
#                                    on the specified port at the DUT).        #
#                                                                              #
#                      0 on Failure (If  L1SyncOptParamsPortDS.                #
#                                    timestampsCorrectedTx option could not be #
#                                    disabled on the specified port at the DUT)#
#                                                                              #
#  DEFINITION        : This function disables L1SyncOptParamsPortDS.           #
#                      timestampsCorrectedTx option on a specific port at the  #
#                      DUT.                                                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_timestamp_corrected_tx_disable\            #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::manual_timestamp_corrected_tx_disable {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_timestamp_corrected_tx_disable: Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "          Running Under Manual Configuration Setup                "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg " Disabling L1SyncOptParamsPortDS.timestampsCorrectedTx option on  "
        LOG -level 2 -msg " port ($port_num)                                                 "
        LOG -level 2 -msg "------------------------------------------------------------------"

        PromptString -text "Disable L1SyncOptParamsPortDS.timestampsCorrectedTx option on port ($port_num)<br><br>\
                            Click 'Done'     to Continue<br>\
                            Click 'End Test' to Terminate"\
                     -type done

        
        set answer [GetUserResponse]
        

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg "------------------------------------------------------------------"
        LOG -level 2 -msg "      Running Under Manual Configuration Setup                    "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "  Assuming that user has disabled L1SyncOptParamsPortDS.          "
        LOG -level 2 -msg "  timestampsCorrectedTx option on ($port_num)                     "
        LOG -level 2 -msg "                                                                  "
        LOG -level 2 -msg "------------------------------------------------------------------"
    }

    return 1
}


################################################################################
#                           MANUAL CHECK FUNCTIONS                             #
################################################################################

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_check_l1sync_state                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  l1sync_state      : (Mandatory) L1 Sync State.                              #
#                                  (e.g., "DISABLED"|"IDLE"|"LINK_ALIVE"|      #
#                                         "CONFIG_MATCH"|"L1_SYNC_UP")         #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given L1 Sync State is verified in the #
#                                    specified port).                          #
#                                                                              #
#                      0 on Failure (If given L1 Sync State is not verified in #
#                                    the specified port).                      #
#                                                                              #
#  DEFINITION        : This function verifies the given L1 Sync State in a     #
#                      specific port.                                          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_check_l1sync_state\                        #
#                          -port_num           $port_num\                      #
#                          -l1sync_state       $l1sync_state                   #
#                                                                              #
################################################################################

proc ptp_ha::manual_check_l1sync_state {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_check_l1sync_state : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-l1sync_state)]} {
        set l1sync_state $param(-l1sync_state)
    } else {
        ERRLOG -msg "ptp_ha::manual_check_l1sync_state : Missing Argument    -> l1sync_state"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*           Running Under Manual Configuration Setup            "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Checking L1 Sync State ($l1sync_state) for port ($port_num)  "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*             Click 'Yes'     If Present                        "
        LOG -level 2 -msg  "*             Click 'No'      If Absent                         "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "****************************************************************"

        PromptString -text "Check L1 Sync State ($l1sync_state) for port ($port_num)<br><br>\
                                Click 'Yes'     If Present<br>\
                                Click 'No'      If Absent"\
                     -type  yesno

        set answer [GetUserResponse]

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg  "*********************************************************************"
        LOG -level 2 -msg  "*          Running Under Manual Configuration Setup                  "
        LOG -level 2 -msg  "*                                                                    "
        LOG -level 2 -msg  "*  Assuming user has successfully found L1 Sync State ($l1sync_state)"
        LOG -level 2 -msg  "*  for port ($port_num)                                              "
        LOG -level 2 -msg  "*********************************************************************"
    }

    return 1
}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_check_ptp_port_state                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  port_state        : (Mandatory) portState.                                  #
#                                  (e.g., "INITIALIZING"|"FAULTY"|"DISABLED"|  #
#                                         "LISTENING"|"PRE_MASTER"|"MASTER"|   #
#                                         "PASSIVE"|"UNCALIBRATED"|"SLAVE"     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given portState is verified in the     #
#                                    specified port).                          #
#                                                                              #
#                      0 on Failure (If given portState is not verified in the #
#                                    specified port).                          #
#                                                                              #
#  DEFINITION        : This function verifies the given portState in a specific#
#                      port.                                                   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_check_ptp_port_state\                      #
#                          -port_num           $port_num\                      #
#                          -port_state         $port_state                     #
#                                                                              #
################################################################################

proc ptp_ha::manual_check_ptp_port_state {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_check_ptp_port_state : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-port_state)]} {
        set port_state $param(-port_state)
    } else {
        ERRLOG -msg "ptp_ha::manual_check_ptp_port_state : Missing Argument    -> port_state"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*           Running Under Manual Configuration Setup            "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Checking portState ($port_state) for port ($port_num)        "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*             Click 'Yes'     If Present                        "
        LOG -level 2 -msg  "*             Click 'No'      If Absent                         "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "****************************************************************"

        PromptString -text "Check portState ($port_state) for port ($port_num)<br><br>\
                                Click 'Yes'     If Present<br>\
                                Click 'No'      If Absent"\
                     -type  yesno

        set answer [GetUserResponse]

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*          Running Under Manual Configuration Setup             "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Assuming user has successfully found portState ($port_state) "
        LOG -level 2 -msg  "*  for port ($port_num)                                         "
        LOG -level 2 -msg  "****************************************************************"
    }

    return 1
}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_check_asymmetry_correction               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  state             : (Mandatory) asymmetryCorrectionPortDS.enable.           #
#                                  (e.g., "ENABLED"|"DISABLED"                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given state of                         #
#                                    asymmetryCorrectionPortDS.enable is       #
#                                    verified for the specified port).         #
#                                                                              #
#                      0 on Failure (If given state of                         #
#                                    asymmetryCorrectionPortDS.enable could not#
#                                    be verified for the specified port).      #
#                                                                              #
#  DEFINITION        : This function verifies the given state of               #
#                      asymmetryCorrectionPortDS.enable for a specific port.   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_check_asymmetry_correction\                #
#                          -port_num           $port_num\                      #
#                          -state              $state                          #
#                                                                              #
################################################################################

proc ptp_ha::manual_check_asymmetry_correction {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_check_asymmetry_correction : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-state)]} {
        set state $param(-state)
    } else {
        ERRLOG -msg "ptp_ha::manual_check_asymmetry_correction : Missing Argument    -> state"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*           Running Under Manual Configuration Setup            "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Checking state of asymmetryCorrectionPortDS.enable ($state)  "
        LOG -level 2 -msg  "*  for port ($port_num)                                         "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*             Click 'Yes'     If Present                        "
        LOG -level 2 -msg  "*             Click 'No'      If Absent                         "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "****************************************************************"

        PromptString -text "Check asymmetryCorrectionPortDS.enable ($state) for port ($port_num)<br><br>\
                                Click 'Yes'     If Present<br>\
                                Click 'No'      If Absent"\
                     -type  yesno

        set answer [GetUserResponse]

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg  "*****************************************************************"
        LOG -level 2 -msg  "*          Running Under Manual Configuration Setup              "
        LOG -level 2 -msg  "*                                                                "
        LOG -level 2 -msg  "*  Assuming user has successfully found state of                 "
        LOG -level 2 -msg  "*  asymmetryCorrectionPortDS.enable ($state) for port ($port_num)"
        LOG -level 2 -msg  "*****************************************************************"
    }

    return 1
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_check_egress_latency                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Mandatory) timestampCorrectionPortDS.egressLatency.    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of                         #
#                                    timestampCorrectionPortDS.egressLatency   #
#                                    verified for the specified port).         #
#                                                                              #
#                      0 on Failure (If given value of                         #
#                                    timestampCorrectionPortDS.egressLatency   #
#                                    could not be verified for the specified   #
#                                    port).                                    #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      timestampCorrectionPortDS.egressLatency for a specific  #
#                      port.                                                   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_check_egress_latency\                      #
#                          -port_num           $port_num\                      #
#                          -latency            $latency                        #
#                                                                              #
################################################################################

proc ptp_ha::manual_check_egress_latency {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_check_egress_latency : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-latency)]} {
        set latency $param(-latency)
    } else {
        ERRLOG -msg "ptp_ha::manual_check_egress_latency : Missing Argument    -> latency"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*           Running Under Manual Configuration Setup            "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Checking value of timestampCorrectionPortDS.egressLatency ($latency ns)"
        LOG -level 2 -msg  "*  for port ($port_num)                                         "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*             Click 'Yes'     If Present                        "
        LOG -level 2 -msg  "*             Click 'No'      If Absent                         "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "****************************************************************"

        PromptString -text "Check timestampCorrectionPortDS.egressLatency ($latency ns) for port ($port_num)<br><br>\
                                Click 'Yes'     If Present<br>\
                                Click 'No'      If Absent"\
                     -type  yesno

        set answer [GetUserResponse]

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*          Running Under Manual Configuration Setup             "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Assuming user has successfully found value of                "
        LOG -level 2 -msg  "*  timestampCorrectionPortDS.egressLatency ($latency ns) for port"
        LOG -level 2 -msg  "*  ($port_num)                                                  "
        LOG -level 2 -msg  "****************************************************************"
    }

    return 1
}


#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_check_ingress_latency                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Mandatory) timestampCorrectionPortDS.ingressLatency.   #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of                         #
#                                    timestampCorrectionPortDS.ingressLatency  #
#                                    verified for the specified port).         #
#                                                                              #
#                      0 on Failure (If given value of                         #
#                                    timestampCorrectionPortDS.ingressLatency  #
#                                    could not be verified for the specified   #
#                                    port).                                    #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      timestampCorrectionPortDS.ingressLatency for a specific #
#                      port.                                                   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_check_ingress_latency\                     #
#                          -port_num           $port_num\                      #
#                          -latency            $latency                        #
#                                                                              #
################################################################################

proc ptp_ha::manual_check_ingress_latency {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_check_ingress_latency : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-latency)]} {
        set latency $param(-latency)
    } else {
        ERRLOG -msg "ptp_ha::manual_check_ingress_latency : Missing Argument    -> latency"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*           Running Under Manual Configuration Setup            "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Checking value of timestampCorrectionPortDS.ingressLatency ($latency ns)"
        LOG -level 2 -msg  "*  for port ($port_num)                                         "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*             Click 'Yes'     If Present                        "
        LOG -level 2 -msg  "*             Click 'No'      If Absent                         "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "****************************************************************"

        PromptString -text "Check timestampCorrectionPortDS.ingressLatency ($latency ns) for port ($port_num)<br><br>\
                                Click 'Yes'     If Present<br>\
                                Click 'No'      If Absent"\
                     -type  yesno

        set answer [GetUserResponse]

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*          Running Under Manual Configuration Setup             "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Assuming user has successfully found value of                "
        LOG -level 2 -msg  "*  timestampCorrectionPortDS.ingressLatency ($latency ns) for port ($port_num)"
        LOG -level 2 -msg  "****************************************************************"
    }

    return 1
}


#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_check_constant_asymmetry                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  constant_asymmetry: (Mandatory) asymmetryCorrectionPortDS.constantAsymmetry.#
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of                         #
#                                    asymmetryCorrectionPortDS.                #
#                                    constantAsymmetry verified for the        #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If given value of                         #
#                                    asymmetryCorrectionPortDS.                #
#                                    constantAsymmetry could not be verified   #
#                                    for specified port at the DUT).           #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      asymmetryCorrectionPortDS.constantAsymmetry for a       #
#                      specific port.                                          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_check_constant_asymmetry\                  #
#                          -port_num           $port_num\                      #
#                          -constant_asymmetry $constant_asymmetry             #
#                                                                              #
################################################################################

proc ptp_ha::manual_check_constant_asymmetry {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_check_constant_asymmetry : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-constant_asymmetry)]} {
        set constant_asymmetry $param(-constant_asymmetry)
    } else {
        ERRLOG -msg "ptp_ha::manual_check_constant_asymmetry : Missing Argument    -> constant_asymmetry"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*           Running Under Manual Configuration Setup            "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Checking value of asymmetryCorrectionPortDS.constantAsymmetry ($constant_asymmetry ns)"
        LOG -level 2 -msg  "*  for port ($port_num)                                         "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*             Click 'Yes'     If Present                        "
        LOG -level 2 -msg  "*             Click 'No'      If Absent                         "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "****************************************************************"

        PromptString -text "Check asymmetryCorrectionPortDS.constantAsymmetry ($constant_asymmetry ns) for port ($port_num)<br><br>\
                                Click 'Yes'     If Present<br>\
                                Click 'No'      If Absent"\
                     -type  yesno

        set answer [GetUserResponse]

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*          Running Under Manual Configuration Setup             "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Assuming user has successfully found value of                "
        LOG -level 2 -msg  "*  asymmetryCorrectionPortDS.constantAsymmetry ($constant_asymmetry ns)"
        LOG -level 2 -msg  "*  for port ($port_num)                                         "
        LOG -level 2 -msg  "****************************************************************"
    }

    return 1
}


#8.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_check_scaled_delay_coefficient           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  delay_coefficient : (Mandatory) asymmetryCorrectionPortDS.                  #
#                                  scaledDelayCoefficient.                     #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of                         #
#                                    asymmetryCorrectionPortDS.                #
#                                    scaledDelayCoefficient verified for the   #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If given value of                         #
#                                    asymmetryCorrectionPortDS.                #
#                                    scaledDelayCoefficient could not be       #
#                                    verified for specified port at the DUT).  #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      asymmetryCorrectionPortDS.scaledDelayCoefficient for a  #
#                      specific port.                                          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_check_scaled_delay_coefficient\            #
#                          -port_num           $port_num\                      #
#                          -constant_asymmetry $constant_asymmetry             #
#                                                                              #
################################################################################

proc ptp_ha::manual_check_scaled_delay_coefficient {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_check_scaled_delay_coefficient : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-delay_coefficient)]} {
        set delay_coefficient $param(-delay_coefficient)
    } else {
        ERRLOG -msg "ptp_ha::manual_check_scaled_delay_coefficient : Missing Argument    -> delay_coefficient"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*           Running Under Manual Configuration Setup            "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Checking value of asymmetryCorrectionPortDS.scaledDelayCoefficient"
        LOG -level 2 -msg  "*  ($delay_coefficient (fractional number)) for port ($port_num)"
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*             Click 'Yes'     If Present                        "
        LOG -level 2 -msg  "*             Click 'No'      If Absent                         "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "****************************************************************"

        PromptString -text "Check asymmetryCorrectionPortDS.scaledDelayCoefficient\
                            ($delay_coefficient (fractional number))\
                            for port ($port_num)<br><br>\
                                Click 'Yes'     If Present<br>\
                                Click 'No'      If Absent"\
                     -type  yesno

        set answer [GetUserResponse]

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*          Running Under Manual Configuration Setup             "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Assuming user has successfully found value of                "
        LOG -level 2 -msg  "*  asymmetryCorrectionPortDS.scaledDelayCoefficient             "
        LOG -level 2 -msg  "*  ($delay_coefficient (factional number)) on port ($port_num)  "
        LOG -level 2 -msg  "****************************************************************"
    }

    return 1
}

#8.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_check_external_port_config               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  state             : (Mandatory) defaultDS.externalPortConfigurationEnabled. #
#                                  (e.g., "ENABLED"|"DISABLED"                 #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  RETURNS           : 1 on Success (If given state of                         #
#                                    defaultDS.externalPortConfigurationEnabled#
#                                    is verified for the specified port).      #
#                                                                              #
#                      0 on Failure (If given state of                         #
#                                    defaultDS.externalPortConfigurationEnabled#
#                                    could not be verified for the specified   #
#                                    port).                                    #
#                                                                              #
#  DEFINITION        : This function verifies the given state of               #
#                      defaultDS.externalPortConfigurationEnabled for a        #
#                      specific port.                                          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_check_external_port_config\                #
#                          -port_num           $port_num\                      #
#                          -state              $state\                         #
#                                                                              #
################################################################################

proc ptp_ha::manual_check_external_port_config {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_check_external_port_config : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-state)]} {
        set state $param(-state)
    } else {
        ERRLOG -msg "ptp_ha::manual_check_external_port_config : Missing Argument    -> state"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*           Running Under Manual Configuration Setup            "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Checking value of defaultDS.externalPortConfigurationEnabled "
        LOG -level 2 -msg  "*  ($state) for port ($port_num)                                "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*             Click 'Yes'     If Present                        "
        LOG -level 2 -msg  "*             Click 'No'      If Absent                         "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "****************************************************************"

        PromptString -text "Check defaultDS.externalPortConfigurationEnabled ($state) \
                            for port ($port_num)<br><br>\
                                Click 'Yes'     If Present<br>\
                                Click 'No'      If Absent"\
                     -type  yesno

        set answer [GetUserResponse]

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*          Running Under Manual Configuration Setup             "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Assuming user has successfully found state of                "
        LOG -level 2 -msg  "*  defaultDS.externalPortConfigurationEnabled ($state)          "
        LOG -level 2 -msg  "*  for port ($port_num)                                         "
        LOG -level 2 -msg  "****************************************************************"
    }

    return 1
}


#9.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_check_slave_only                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  value             : (Mandatory) Value of defaultDS.slaveOnly                #
#                                  (e.g., "TRUE"|"FALSE")                      #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of defaultDS.slaveOnly     #
#                                    is verified at the DUT).                  #
#                                                                              #
#                      0 on Failure (If given value of defaultDS.slaveOnly     #
#                                    could not be verified at the DUT).        #
#                                                                              #
#  DEFINITION        : This function verifies the given value of defaultDS.    #
#                      slaveOnly at the DUT.                                   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_check_slave_only\                          #
#                          -value              $value                          #
#                                                                              #
################################################################################

proc ptp_ha::manual_check_slave_only {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-value)]} {
        set value $param(-value)
    } else {
        ERRLOG -msg "ptp_ha::manual_check_slave_only : Missing Argument    -> value"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*           Running Under Manual Configuration Setup            "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Checking value of defaultDS.slaveOnly ($value)               "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*             Click 'Yes'     If Present                        "
        LOG -level 2 -msg  "*             Click 'No'      If Absent                         "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "****************************************************************"

        PromptString -text "Check defaultDS.slaveOnly ($value) for port \
                            <br><br>\
                                Click 'Yes'     If Present<br>\
                                Click 'No'      If Absent"\
                     -type  yesno

        set answer [GetUserResponse]

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*          Running Under Manual Configuration Setup             "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Assuming user has successfully found value of                "
        LOG -level 2 -msg  "*  defaultDS.slaveOnly ($value) for port                        "
        LOG -level 2 -msg  "****************************************************************"
    }

    return 1
}


#10.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_check_master_only                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  value             : (Mandatory) Value of portDS.masterOnly                  #
#                                  (e.g., "TRUE"|"FALSE")                      #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of portDS.masterOnly       #
#                                    is verified for the specified port).      #
#                                                                              #
#                      0 on Failure (If given value of portDS.masterOnly       #
#                                    could not be verified for the specified   #
#                                    port).                                    #
#                                                                              #
#  DEFINITION        : This function verifies the given value of portDS.       #
#                      masterOnly for a specific port.                         #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_check_master_only\                         #
#                          -port_num           $port_num\                      #
#                          -value              $value\                         #
#                                                                              #
################################################################################

proc ptp_ha::manual_check_master_only {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_check_master_only : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-value)]} {
        set value $param(-value)
    } else {
        ERRLOG -msg "ptp_ha::manual_check_master_only : Missing Argument    -> value"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*           Running Under Manual Configuration Setup            "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Checking value of portDS.masterOnly ($value) for port        "
        LOG -level 2 -msg  "*  ($port_num)                                                  "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*             Click 'Yes'     If Present                        "
        LOG -level 2 -msg  "*             Click 'No'      If Absent                         "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "****************************************************************"

        PromptString -text "Check portDS.masterOnly ($value) for port \
                            ($port_num)<br><br>\
                                Click 'Yes'     If Present<br>\
                                Click 'No'      If Absent"\
                     -type  yesno

        set answer [GetUserResponse]

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*          Running Under Manual Configuration Setup             "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Assuming user has successfully found value of                "
        LOG -level 2 -msg  "*  portDS.masterOnly ($value) for port ($port_num)              "
        LOG -level 2 -msg  "****************************************************************"
    }

    return 1
}


#11.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_check_for_least_offset_from_master       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  offset_from_master: (Mandatory) currentDS.offsetFromMaster.                 #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  RETURNS           : 1 on Success (If the absolute value of currentDS.       #
#                                    offsetFromMaster in DUT becomes lesser    #
#                                    than the specified value to ensure that   #
#                                    the DUT synchronized it's time with       #
#                                    messages from TEE).                       #
#                                                                              #
#                      0 on Failure (If the absolute value of currentDS.       #
#                                    offsetFromMaster in DUT does not become   #
#                                    lesser than the specified value to ensure #
#                                    that the DUT synchronized it's time with  #
#                                    messages from TEE).                       #
#                                                                              #
#  DEFINITION        : This function is to check whether the absolute value of #
#                      currentDS.offsetFromMaster is lesser than the specified #
#                      value to ensure that DUT synchronized it's time with    #
#                      messages from TEE.                                      #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_check_for_least_offset_from_master\        #
#                          -offset_from_master    offset_from_master           #
#                                                                              #
################################################################################

proc ptp_ha::manual_check_for_least_offset_from_master {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-offset_from_master)]} {
        set offset_from_master  $param(-offset_from_master)
    } else {
        ERRLOG -msg "ptp_ha::manual_check_for_least_offset_from_master : Missing Argument    -> offset_from_master"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {$env(TEE_ATTEST_MANUAL_PROMPT)} {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*           Running Under Manual Configuration Setup            "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Checking the absolute value of currentDS.offsetFromMaster in "
        LOG -level 2 -msg  "*  DUT is lesser than $offset_from_master ns                    "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*             Click 'Yes'     If Present                        "
        LOG -level 2 -msg  "*             Click 'No'      If Absent                         "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "****************************************************************"

        PromptString -text "Check the absolute value of currentDS.offsetFromMaster in DUT is\
                            lesser than $offset_from_master ns to ensure that DUT synchronizes\
                            it's time with TEE<br><br>\
                                Click 'Yes'     If Present<br>\
                                Click 'No'      If Absent"\
                     -type  yesno

        set answer [GetUserResponse]

        if [string match -nocase "y" $answer] {
            set result 1
        } else {
            set result 0
        }

        return $result
    } else {

        LOG -level 2 -msg  "****************************************************************"
        LOG -level 2 -msg  "*          Running Under Manual Configuration Setup             "
        LOG -level 2 -msg  "*                                                               "
        LOG -level 2 -msg  "*  Assuming user has successfully checked the absolute value of "
        LOG -level 2 -msg  "*  currentDS.offsetFromMaster in DUT is lesser than $offset_from_master ns"
        LOG -level 2 -msg  "****************************************************************"
    }

    return 1
}



################################################################################
#                           MANUAL GET FUNCTIONS                               #
################################################################################

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_get_timestamp                            #
#                                                                              #
#  INPUT PARAMETERS  : NONE.                                                   #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  timestamp         : (Mandatory) Timestamp.                                  #
#                                  (e.g., xxxxx)                               #
#                                                                              #
#                                                                              #
#  RETURNS           : 1 on Success (If able to get timestamp from DUT).       #
#                                                                              #
#                      0 on Failure (If could not able to get timestamp from   #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function fetch timestamp from DUT.                 #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_get_timestamp\                             #
#                             -timestamp       timestamp                       #
#                                                                              #
################################################################################

proc ptp_ha::manual_get_timestamp {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-timestamp)]} {
        upvar $param(-timestamp) timestamp
    } else {
        ERRLOG -msg "ptp_ha::manual_get_timestamp : Missing Argument    -> timestamp"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg  "*******************************************************************"
    LOG -level 2 -msg  "*            Running Under Manual Configuration Setup              "
    LOG -level 2 -msg  "*                                                                  "
    LOG -level 2 -msg  "*  Get timestamp of DUT and convert it into epoch format           "
    LOG -level 2 -msg  "*                                                                  "
    LOG -level 2 -msg  "*             Press 'Done'     to Continue                         "
    LOG -level 2 -msg  "*             Press 'End Test' to Terminate                        "
    LOG -level 2 -msg  "*                                                                  "
    LOG -level 2 -msg  "*******************************************************************"

    PromptString_Get_Value -text "Enter the value of timestamp (in nanoseconds) from the DUT in\
                                  epoch format<br><br>\
                                  (If the device is Linux based, use below command <br><br>\
                                  'date +%s%N' - to get number of seconds and current\
                                  nanoseconds from epoch)"\
                           -type done

    set output_string [GetUserResponse]

    set list1             [split $output_string]
    set timestamp         [lindex $list1 0]
    set answer            [lindex $list1 1]

    if {![string match -nocase "y" $answer]} {
        return 0
    }

    return 1
}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_get_mean_delay                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  mean_delay        : (Mandatory) currentDS.meanDelay.                        #
#                                  (e.g., 3)                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If able to extract currentDS.meanDelay of #
#                                    specified port from the DUT).             #
#                                                                              #
#                      0 on Failure (If could not able to extract              #
#                                    currentDS.meanDelay of specified port from#
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function extracts currentDS.meanDelay of a specific#
#                      port from the DUT.                                      #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_get_mean_delay\                            #
#                          -port_num              $port_num\                   #
#                          -mean_delay            mean_delay                   #
#                                                                              #
################################################################################

proc ptp_ha::manual_get_mean_delay {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_get_mean_delay : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    ### Checking Output Parameters ###

    if {[info exists param(-mean_delay)]} {
        upvar $param(-mean_delay) mean_delay
    } else {
        ERRLOG -msg "ptp_ha::manual_get_mean_delay : Missing Argument    -> mean_delay"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg  "*******************************************************************"
    LOG -level 2 -msg  "*            Running Under Manual Configuration Setup              "
    LOG -level 2 -msg  "*                                                                  "
    LOG -level 2 -msg  "*  Get currentDS.meanDelay of port ($port_num) from DUT            "
    LOG -level 2 -msg  "*                                                                  "
    LOG -level 2 -msg  "*             Press 'Done'     to Continue                         "
    LOG -level 2 -msg  "*             Press 'End Test' to Terminate                        "
    LOG -level 2 -msg  "*                                                                  "
    LOG -level 2 -msg  "*******************************************************************"

    PromptString_Get_Value -text "Enter the value of currentDS.meanDelay of port ($port_num) in the DUT"\
                           -type done

    set output_string [GetUserResponse]

    set list1             [split $output_string]
    set mean_delay        [lindex $list1 0]
    set answer            [lindex $list1 1]

    if {![string match -nocase "y" $answer]} {
        return 0
    }

    return 1
}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_get_mean_link_delay                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  mean_link_delay   : (Mandatory) portDS.meanLinkDelay.                       #
#                                  (e.g., 3)                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If able to extract portDS.meanLinkDelay of#
#                                    specified port from the DUT).             #
#                                                                              #
#                      0 on Failure (If could not able to extract              #
#                                    portDS.meanLinkDelay of specified port    #
#                                    from the DUT).                            #
#                                                                              #
#  DEFINITION        : This function extracts portDS.meanLinkDelay of a        #
#                      specific port from the DUT.                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_get_mean_link_delay\                       #
#                          -port_num              $port_num\                   #
#                          -mean_link_delay       mean_link_delay              #
#                                                                              #
################################################################################

proc ptp_ha::manual_get_mean_link_delay {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_get_mean_link_delay : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    ### Checking Output Parameters ###

    if {[info exists param(-mean_link_delay)]} {
        upvar $param(-mean_link_delay) mean_link_delay
    } else {
        ERRLOG -msg "ptp_ha::manual_get_mean_link_delay : Missing Argument    -> mean_link_delay"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg  "*******************************************************************"
    LOG -level 2 -msg  "*            Running Under Manual Configuration Setup              "
    LOG -level 2 -msg  "*                                                                  "
    LOG -level 2 -msg  "*  Get portDS.meanLinkDelay of port ($port_num) from DUT           "
    LOG -level 2 -msg  "*                                                                  "
    LOG -level 2 -msg  "*             Press 'Done'     to Continue                         "
    LOG -level 2 -msg  "*             Press 'End Test' to Terminate                        "
    LOG -level 2 -msg  "*                                                                  "
    LOG -level 2 -msg  "*******************************************************************"

    PromptString_Get_Value -text "Enter the value of portDS.meanLinkDelay of port ($port_num) in the DUT"\
                           -type done

    set output_string [GetUserResponse]

    set list1             [split $output_string]
    set mean_link_delay   [lindex $list1 0]
    set answer            [lindex $list1 1]

    if {![string match -nocase "y" $answer]} {
        return 0
    }

    return 1
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::manual_get_delay_asymmetry                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  delay_asymmetry   : (Mandatory) portDS.delayAsymmetry.                      #
#                                  (e.g., 3)                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If able to extract portDS.delayAsymmetry  #
#                                    of specified port from the DUT).          #
#                                                                              #
#                      0 on Failure (If could not able to extract              #
#                                    portDS.delayAsymmetry of specified port   #
#                                    from the DUT).                            #
#                                                                              #
#  DEFINITION        : This function extracts portDS.delayAsymmetry of a       #
#                      specific port from the DUT.                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::manual_get_delay_asymmetry\                       #
#                          -port_num              $port_num\                   #
#                          -delay_asymmetry       delay_asymmetry              #
#                                                                              #
################################################################################

proc ptp_ha::manual_get_delay_asymmetry {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::manual_get_delay_asymmetry : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    ### Checking Output Parameters ###

    if {[info exists param(-delay_asymmetry)]} {
        upvar $param(-delay_asymmetry) delay_asymmetry
    } else {
        ERRLOG -msg "ptp_ha::manual_get_delay_asymmetry : Missing Argument    -> delay_asymmetry"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    LOG -level 2 -msg  "*******************************************************************"
    LOG -level 2 -msg  "*            Running Under Manual Configuration Setup              "
    LOG -level 2 -msg  "*                                                                  "
    LOG -level 2 -msg  "*  Get portDS.delayAsymmetry of port ($port_num) from DUT          "
    LOG -level 2 -msg  "*                                                                  "
    LOG -level 2 -msg  "*             Press 'Done'     to Continue                         "
    LOG -level 2 -msg  "*             Press 'End Test' to Terminate                        "
    LOG -level 2 -msg  "*                                                                  "
    LOG -level 2 -msg  "*******************************************************************"

    PromptString_Get_Value -text "Enter the value of portDS.delayAsymmetry of port ($port_num) in the DUT"\
                           -type done

    set output_string [GetUserResponse]

    set list1              [split $output_string]
    set delay_asymmetry    [lindex $list1 0]
    set answer             [lindex $list1 1]

    if {![string match -nocase "y" $answer]} {
        return 0
    }

    return 1
}

proc get_user_input {} {

    set ::_answer_ [gets stdin]
    return $::_answer_

}

proc GetUserResponse {} {

    puts "GETTING INPUT FROM USER:"

    Echo_Off
    fconfigure stdin -blocking 0
    fileevent stdin readable get_user_input
    fconfigure stdin -blocking 1
    vwait ::_answer_
    Echo_On

    set answer $::_answer_
    unset -nocomplain ::_answer_

    puts "GOT INPUT FROM USER: $answer"

    return $answer
}

