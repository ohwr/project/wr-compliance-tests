###############################################################################
# File Name         : ptp-ha_global.tcl                                       #
# File Version      : 1.4                                                     #
# Component Name    : Global Variables Initialization                         #
# Module Name       : Precision Time Protocol - High Accuracy                 #
###############################################################################
# History       Date       Author         Addition/ Alteration                #
###############################################################################
#                                                                             #
#  1.0       Mar/2018      CERN           Initial                             #
#  1.1       Sep/2018      CERN           Removed duplicate variables         #
#  1.2       Dec/2018      CERN           Added variables for                 #
#                                         L1SyncReceiptTimeout.               #
#  1.3       Jan/2019      CERN           Added variables for                 #
#                                         a) Globalizing timestamps in Sync   #
#                                            and Follow_Up messages.          #
#                                         b) Skipping return 0 in PAG 004, 005#
#                                            and 008.                         #
#  1.4       Feb/2019      CERN           Added margin of error.              #
#                                                                             #
###############################################################################
# Copyright (c) 2018 - 2019 CERN                                              #
###############################################################################

### CONFIGURATION VARIABLES FROM GUI ###

global env
global ptp_ha_env
global NETO2HOME

global ptp_sync_interval
global ptp_announce_interval ptp_announce_timeout
global ptp_l1sync_interval ptp_l1sync_timeout

global TEE_MAC TEE_MAC_1 TEE_MAC_2
global dut_session_id tee_session_id
global dut_port dut_address
global dut_test_port_ip tee_test_port_ip
global dut_test_port_ip_2 tee_test_port_ip_2
global dut_port_num_1 tee_port_num_1 
global dut_port_num_2 tee_port_num_2
global eth_type1 eth_type2
global tee_log_msg dut_log_msg
global ptp_comm_model
global unicast_flag
global ptp_trans_type

set ::XENA_GET_DELAY                0
set ::RX_POLLING_INTERVAL           500
set ::PRINT_AUTO_LOG                0

set ptp_device_type                 $ptp_ha_env(DUT_PTP_HA_DEVCIE_TYPE)
set ptp_p2p_delay_mechanism         $ptp_ha_env(DUT_PTP_HA_IS_P2P_DELAY)
set ptp_trans_type                  $ptp_ha_env(DUT_PTP_HA_TRANS_TYPE)
set ptp_comm_model                  $ptp_ha_env(DUT_PTP_HA_COM_MODEL)
set ptp_dut_clock_step              $ptp_ha_env(DUT_PTP_HA_CLOCK_STEP)
set ptp_ha_vlan_encap               $ptp_ha_env(DUT_PTP_HA_IS_VLAN_ENCAP)
set ptp_ha_vlan_priority            $ptp_ha_env(DUT_PTP_HA_VLAN_PRIORITY)
set ptp_user_delay                  $ptp_ha_env(DUT_PTP_HA_USER_DELAY)

set ptp_dut_default_domain_number   $ptp_ha_env(DUT_PTP_HA_DOMAIN_NUMBER)
set ptp_dut_default_priority1       $ptp_ha_env(DUT_PTP_HA_PRIORITY1)
set ptp_dut_default_priority2       $ptp_ha_env(DUT_PTP_HA_PRIORITY2)
set ptp_announce_interval           $ptp_ha_env(DUT_PTP_HA_ANNOUNCE_INTERVAL)
set ptp_announce_timeout            $ptp_ha_env(DUT_PTP_HA_ANNOUNCE_TIMEOUT)
set ptp_sync_interval               $ptp_ha_env(DUT_PTP_HA_SYNC_INTERVAL)
set ptp_delayreq_interval           $ptp_ha_env(DUT_PTP_HA_DELAY_REQ_INTERVAL)
set ptp_pdelayreq_interval          $ptp_ha_env(DUT_PTP_HA_PDELAY_REQ_INTERVAL)
set ptp_l1sync_interval             $ptp_ha_env(DUT_PTP_HA_L1SYNC_INTERVAL)
set ptp_l1sync_timeout              $ptp_ha_env(DUT_PTP_HA_L1SYNC_TIMEOUT)
set ptp_egress_latency              $ptp_ha_env(DUT_PTP_HA_EGRESS_LATENCY)
set ptp_ingress_latency             $ptp_ha_env(DUT_PTP_HA_INGRESS_LATENCY)
set ptp_constant_asymmetry          $ptp_ha_env(DUT_PTP_HA_CONSTANT_ASYMMETRY)
set ptp_delay_coefficient           $ptp_ha_env(DUT_PTP_HA_SCALED_DELAY_COEFFICIENT)
set offset_from_master              [expr abs($ptp_ha_env(DUT_PTP_HA_OFFSET_FROM_MASTER))]
set margin_of_error                 $ptp_ha_env(DUT_PTP_HA_MARGIN_OF_ERROR)

set ptp_vlan_encap                  $ptp_ha_vlan_encap
set ptp_transport_type              $ptp_trans_type
set ptp_communication_model         $ptp_comm_model

set ptp_ha_l1sync_timeout               [expr (int(pow(2,$ptp_l1sync_interval)) * $ptp_l1sync_timeout)]
set ptp_ha_l1sync_timeout_twice         [expr (2 * $ptp_ha_l1sync_timeout) + $ptp_user_delay]
set ptp_ha_l1sync_timeout_d             [expr (int(pow(2,$ptp_l1sync_interval)) * $ptp_l1sync_timeout) + $ptp_user_delay]
set ptp_ha_1_l1sync_timeout             [expr $ptp_ha_l1sync_timeout + $ptp_user_delay + 60]
set ptp_ha_announce_timeout             [expr (int(pow(2,$ptp_announce_interval)) * $ptp_announce_timeout)]
set ptp_ha_1_announce_timeout           [expr $ptp_ha_announce_timeout + $ptp_user_delay + 60]
set ptp_ha_sync_timeout                 [expr (int(pow(2,$ptp_sync_interval)) * 3) + $ptp_user_delay + 60]
set ptp_ha_followup_timeout             [expr (int(pow(2,$ptp_sync_interval)) * 3) + $ptp_user_delay + 60]
set ptp_ha_delayreq_timeout             [expr (int(pow(2,$ptp_delayreq_interval)) * 3) + $ptp_user_delay + 60]
set ptp_ha_delayresp_timeout            [expr (int(pow(2,$ptp_delayreq_interval)) * 3) + $ptp_user_delay + 60]
set ptp_ha_pdelayreq_timeout            [expr (int(pow(2,$ptp_pdelayreq_interval)) * 3) + $ptp_user_delay + 60]
set ptp_ha_pdelayresp_timeout           [expr (int(pow(2,$ptp_pdelayreq_interval)) * 3) + $ptp_user_delay + 60]
set ptp_ha_pdelayresp_followup_timeout  [expr (int(pow(2,$ptp_pdelayreq_interval)) * 3) + $ptp_user_delay + 10]
set ptp_ha_mgmt_timeout                 10
set ptp_ha_bmca                         [expr $ptp_ha_announce_timeout + $ptp_user_delay]

set ptp_ha_l1sync_wait                  10
set dut_config_delay                    3

set ptp_version                     1
set tcr                             1
set rcr                             1
set cr                              1
set ope                             1

set TEE_MAC                         "00:00:00:00:00:01"
set TEE_MAC_1                       "00:00:00:00:00:02"
set TEE_MAC_2                       "00:00:00:00:00:03"
set PTP_ETHTYPE                     "88F7"

set ptp_clock_identity_format       "EUI-48"; #EUI-48, IEEE EUI-64, Non-IEEE EUI-64

set DUT_PTP_L1_STATE(DISABLED)      "DISABLED"
set DUT_PTP_L1_STATE(IDLE)          "IDLE"

set reset_count_off                 "OFF"
set reset_count_on                  "ON"

set env(TEE_ATTEST_MANUAL_PROMPT)       0

set dut_port                            $env(dut_PORT)

set dut_address                         $env(TEST_DEVICE_IPV4_ADDRESS)

set dut_port_num_1                      $env(dut_to_tee_1_LOCATION)
set tee_port_num_1                      $env(tee_to_dut_1_LOCATION)
set dut_port_num_2                      $env(dut_to_tee_2_LOCATION)
set tee_port_num_2                      $env(tee_to_dut_2_LOCATION)

set ptp_vlan_id                         [set vlan_portmap($tee_port_num_1)]

set dut_test_port_ip                    $env(dut_TEST_ADDRESS1)
set tee_test_port_ip                    "1.1.1.1"


set dut_test_port_ip_2                  $env(dut_TEST_ADDRESS2)
set tee_test_port_ip_2                  "2.2.2.2"

set platform_type                       $env(TEE_PLATFORM_TYPE)

set ptp_ha_vlan_id                      [set vlan_portmap($tee_port_num_1)]
set ptp_ha_vlan_id_2                    [set vlan_portmap($tee_port_num_2)]

if {$ptp_comm_model == "Unicast" } {
    set unicast_flag 1
} else {
    set unicast_flag 0
}

### ETHERTYPE ###

global ETHERTYPE

set ETHERTYPE(PTP)                      "88F7"
set ETHERTYPE(VLAN)                     "8100"
set ETHERTYPE(IPV4)                     "0800"
set ETHERTYPE(ARP)                      "0806"

global PTP_UDP

set PTP_UDP(EVENT)                      "319"; # Sync, Delay_Req, Pdelay_Req, Pdelay_Resp
set PTP_UDP(GENERAL)                    "320"; # Announce, Follow_Up, Delay_Resp, Pdelay_Resp_Follow_Up, Management, Signaling

### PTP TRANSPORT TYPE ###

global PTP_TRANS_TYPE

set PTP_TRANS_TYPE(IEEE)                "IEEE 802.3"
set PTP_TRANS_TYPE(IPV4)                "UDP/IPv4"

global PTP_HA_MAC PTP_HA_IP

if {$::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4)} {

    array set PTP_HA_MAC {
        ANNOUNCE                "01:00:5E:00:01:81"
        SYNC                    "01:00:5E:00:01:81"
        FOLLOW_UP               "01:00:5E:00:01:81"
        DELAY_REQ               "01:00:5E:00:01:81"
        DELAY_RESP              "01:00:5E:00:01:81"
        PDELAY_REQ              "01:00:5E:00:00:6B"
        PDELAY_RESP             "01:00:5E:00:00:6B"
        PDELAY_RESP_FOLLOW_UP   "01:00:5E:00:00:6B"
        SIGNALING               "01:00:5E:00:00:6B"
        MANAGEMENT              "01:00:5E:00:01:81"
    }

    array set PTP_HA_IP {
        ANNOUNCE                "224.0.1.129"
        SYNC                    "224.0.1.129"
        FOLLOW_UP               "224.0.1.129"
        DELAY_REQ               "224.0.1.129"
        DELAY_RESP              "224.0.1.129"
        PDELAY_REQ              "224.0.0.107"
        PDELAY_RESP             "224.0.0.107"
        PDELAY_RESP_FOLLOW_UP   "224.0.0.107"
        SIGNALING               "224.0.0.107"
        MANAGEMENT              "224.0.1.129"
    }

} else {

    array set PTP_HA_MAC {
        ANNOUNCE                "01:1B:19:00:00:00"
        SYNC                    "01:1B:19:00:00:00"
        FOLLOW_UP               "01:1B:19:00:00:00"
        DELAY_REQ               "01:1B:19:00:00:00"
        DELAY_RESP              "01:1B:19:00:00:00"
        PDELAY_REQ              "01:80:C2:00:00:0E"
        PDELAY_RESP             "01:80:C2:00:00:0E"
        PDELAY_RESP_FOLLOW_UP   "01:80:C2:00:00:0E"
        SIGNALING               "01:80:C2:00:00:0E"
        MANAGEMENT              "01:1B:19:00:00:00"
    }

    array set PTP_HA_IP {
        ANNOUNCE                "0.0.0.0"
        SYNC                    "0.0.0.0"
        FOLLOW_UP               "0.0.0.0"
        DELAY_REQ               "0.0.0.0"
        DELAY_RESP              "0.0.0.0"
        PDELAY_REQ              "0.0.0.0"
        PDELAY_RESP             "0.0.0.0"
        PDELAY_RESP_FOLLOW_UP   "0.0.0.0"
        SIGNALING               "0.0.0.0"
        MANAGEMENT              "0.0.0.0"
    }

}

### PTP DEVICE TYPE ###

global PTP_DEVICE_TYPE

set PTP_DEVICE_TYPE(OC)                 "Ordinary Clock"
set PTP_DEVICE_TYPE(BC)                 "Boundary Clock"
set PTP_DEVICE_TYPE(E2E_TC)             "E2E Transparent Clock"
set PTP_DEVICE_TYPE(P2P_TC)             "P2P Transparent Clock"

### portDS.portState ###

global PTP_PORT_STATE

set PTP_PORT_STATE(INITIALIZING)        "INITIALIZING"
set PTP_PORT_STATE(FAULTY)              "FAULTY"
set PTP_PORT_STATE(DISABLED)            "DISABLED"
set PTP_PORT_STATE(LISTENING)           "LISTENING"
set PTP_PORT_STATE(PRE_MASTER)          "PRE_MASTER"
set PTP_PORT_STATE(MASTER)              "MASTER"
set PTP_PORT_STATE(PASSIVE)             "PASSIVE"
set PTP_PORT_STATE(UNCALIBRATED)        "UNCALIBRATED"
set PTP_PORT_STATE(SLAVE)               "SLAVE"


### portDS.delayMechanism ###

global PTP_DELAY_MECHANISM DUT_PTP_DELAY_MECHANISM

set PTP_DELAY_MECHANISM(E2E)            "01"
set PTP_DELAY_MECHANISM(P2P)            "02"

set DUT_PTP_DELAY_MECHANISM(E2E)        "Delay Request-Response"
set DUT_PTP_DELAY_MECHANISM(P2P)        "Peer-to-Peer Delay"

### PTP MESSAGE TYPES ###

global PTP_MESSAGE_TYPE

set PTP_MESSAGE_TYPE(SYNC)                  "0"
set PTP_MESSAGE_TYPE(DELAY_REQ)             "1"
set PTP_MESSAGE_TYPE(PDELAY_REQ)            "2"
set PTP_MESSAGE_TYPE(PDELAY_RESP)           "3"
set PTP_MESSAGE_TYPE(FOLLOW_UP)             "8"
set PTP_MESSAGE_TYPE(DELAY_RESP)            "9"
set PTP_MESSAGE_TYPE(PDELAY_RESP_FOLLOW_UP) "10"
set PTP_MESSAGE_TYPE(ANNOUNCE)              "11"
set PTP_MESSAGE_TYPE(SIGNALING)             "12"
set PTP_MESSAGE_TYPE(MANAGEMENT)            "13"


### PTP CONTROL FIELDS ###

global PTP_CONTROL_FIELD

set PTP_CONTROL_FIELD(SYNC)                     "00"
set PTP_CONTROL_FIELD(DELAY_REQ)                "01"
set PTP_CONTROL_FIELD(PDELAY_REQ)               "05"
set PTP_CONTROL_FIELD(PDELAY_RESP)              "05"
set PTP_CONTROL_FIELD(FOLLOW_UP)                "02"
set PTP_CONTROL_FIELD(DELAY_RESP)               "03"
set PTP_CONTROL_FIELD(PDELAY_RESP_FOLLOW_UP)    "05"
set PTP_CONTROL_FIELD(ANNOUNCE)                 "05"
set PTP_CONTROL_FIELD(SIGNALING)                "05"
set PTP_CONTROL_FIELD(MANAGEMENT)               "04"


### PTP MESSAGE LENGTH ###

global PTP_MESSAGE_LENGTH

set PTP_MESSAGE_LENGTH(SYNC)                    "44"
set PTP_MESSAGE_LENGTH(DELAY_REQ)               "44"
set PTP_MESSAGE_LENGTH(PDELAY_REQ)              "54"
set PTP_MESSAGE_LENGTH(PDELAY_RESP)             "54"
set PTP_MESSAGE_LENGTH(FOLLOW_UP)               "44"
set PTP_MESSAGE_LENGTH(DELAY_RESP)              "54"
set PTP_MESSAGE_LENGTH(PDELAY_RESP_FOLLOW_UP)   "54"
set PTP_MESSAGE_LENGTH(ANNOUNCE)                "64"


### L1SyncBasicPortDS.L1SyncState ###

global PTP_L1SYNC_STATE

set PTP_L1SYNC_STATE(DISABLED)              "DISABLED"
set PTP_L1SYNC_STATE(IDLE)                  "IDLE"
set PTP_L1SYNC_STATE(LINK_ALIVE)            "LINK_ALIVE"
set PTP_L1SYNC_STATE(CONFIG_MATCH)          "CONFIG_MATCH"
set PTP_L1SYNC_STATE(L1_SYNC_UP)            "L1_SYNC_UP"


### XENA STATISTICS RESET ###

global RESET_COUNT

set RESET_COUNT(ON)                     "ON"
set RESET_COUNT(OFF)                    "OFF"


global PTP_ETHTYPE ARP_TYPE ARP_TIMEOUT

set ARP_TIMEOUT                         "60"

set GRANDMASTER_ID                      "ece555fffef5d9e0"
set DEFAULT_DOMAIN                      "0"
set INFINITY                            "-1"
set TEE_CORRECTION_FIELD                "300000000"


### PTP CONSTANT VARIABLES ###

set TLV_TYPE(L1SYNC)                   "32769"

global gtx_origin_timestamp_sec gtx_origin_timestamp_ns gtx_follow_up_timestamp_sec gtx_follow_up_timestamp_ns

set SKIP_VALIDATION_FOR_DEBUG       "0"
set CURRENT_STEP                    "0"
set STOP_AT_STEP                    "100"
