###############################################################################
# File Name         : snmp_ptp_ha_functions.tcl                               #
# File Version      : 1.3                                                     #
# Component Name    : ATTEST DEVICE UNDER TEST (DUT)                          #
# Module Name       : Precision Time Protocol - High Accuracy                 #
###############################################################################
# History       Date       Author         Addition/ Alteration                #
###############################################################################
#                                                                             #
#  1.0       May/2018      CERN         Initial                               #
#  1.1       Sep/2018      CERN         a) Added new procedures               #
#                                       1) snmp_timestamp_corrected_tx_enable #
#                                       2) snmp_timestamp_corrected_tx_disable#
#                                       3) snmp_check_slave_only              #
#                                       4) snmp_check_master_only             #
#                                       b) Verification of attributes are done#
#                                          once                               #
#  1.2       Oct/2018      CERN         a) Resolved script errors.            #
#                                       b) Added snmp_get_mean_link_delay.    #
#  1.3       Nov/2018      CERN         a) Removed snmp_get_offset_from_master#
#                                       b) Added                              #
#                                      snmp_check_for_least_offset_from_master#
#                                                                             #
###############################################################################
# Copyright (c) 2018 CERN                                                     #
###############################################################################

######################### PROCEDURES USED #####################################
#                                                                             #
#  DUT SET FUNCTIONS     :                                                    #
#                                                                             #
#  1) ptp_ha::snmp_port_enable                                                #
#  2) ptp_ha::snmp_port_ptp_enable                                            #
#  3) ptp_ha::snmp_set_vlan                                                   #
#  4) ptp_ha::snmp_set_vlan_priority                                          #
#  5) ptp_ha::snmp_global_ptp_enable                                          #
#  6) ptp_ha::snmp_set_communication_mode                                     #
#  7) ptp_ha::snmp_set_domain                                                 #
#  8) ptp_ha::snmp_set_network_protocol                                       #
#  9) ptp_ha::snmp_set_clock_mode                                             #
# 10) ptp_ha::snmp_set_clock_step                                             #
# 11) ptp_ha::snmp_set_delay_mechanism                                        #
# 12) ptp_ha::snmp_set_priority1                                              #
# 13) ptp_ha::snmp_set_priority2                                              #
# 14) ptp_ha::snmp_tcr_rcr_cr_enable                                          #
# 15) ptp_ha::snmp_l1sync_enable                                              #
# 16) ptp_ha::snmp_l1sync_opt_params_enable                                   #
# 17) ptp_ha::snmp_slaveonly_enable                                           #
# 18) ptp_ha::snmp_masteronly_enable                                          #
# 19) ptp_ha::snmp_set_announce_interval                                      #
# 20) ptp_ha::snmp_set_announce_timeout                                       #
# 21) ptp_ha::snmp_set_sync_interval                                          #
# 22) ptp_ha::snmp_set_delayreq_interval                                      #
# 23) ptp_ha::snmp_set_pdelayreq_interval                                     #
# 24) ptp_ha::snmp_set_l1sync_interval                                        #
# 25) ptp_ha::snmp_set_l1sync_timeout                                         #
# 26) ptp_ha::snmp_set_egress_latency                                         #
# 27) ptp_ha::snmp_set_ingress_latency                                        #
# 28) ptp_ha::snmp_set_constant_asymmetry                                     #
# 29) ptp_ha::snmp_set_scaled_delay_coefficient                               #
# 30) ptp_ha::snmp_set_asymmetry_correction                                   #
# 31) ptp_ha::snmp_external_port_config_enable                                #
# 32) ptp_ha::snmp_set_epc_desired_state                                      #
# 33) ptp_ha::snmp_set_ipv4_address                                           #
# 34) ptp_ha::snmp_timestamp_corrected_tx_enable                              #
#                                                                             #
#  DUT DISABLE FUNCTIONS :                                                    #
#                                                                             #
#  1) ptp_ha::snmp_port_disable                                               #
#  2) ptp_ha::snmp_port_ptp_disable                                           #
#  3) ptp_ha::snmp_reset_vlan                                                 #
#  4) ptp_ha::snmp_reset_vlan_priority                                        #
#  5) ptp_ha::snmp_global_ptp_disable                                         #
#  6) ptp_ha::snmp_reset_communication_mode                                   #
#  7) ptp_ha::snmp_reset_domain                                               #
#  8) ptp_ha::snmp_reset_network_protocol                                     #
#  9) ptp_ha::snmp_reset_clock_mode                                           #
# 10) ptp_ha::snmp_reset_clock_step                                           #
# 11) ptp_ha::snmp_reset_delay_mechanism                                      #
# 12) ptp_ha::snmp_reset_priority1                                            #
# 13) ptp_ha::snmp_reset_priority2                                            #
# 14) ptp_ha::snmp_tcr_rcr_cr_disable                                         #
# 15) ptp_ha::snmp_l1sync_disable                                             #
# 16) ptp_ha::snmp_l1sync_opt_params_disable                                  #
# 17) ptp_ha::snmp_slaveonly_disable                                          #
# 18) ptp_ha::snmp_masteronly_disable                                         #
# 19) ptp_ha::snmp_reset_announce_interval                                    #
# 20) ptp_ha::snmp_reset_announce_timeout                                     #
# 21) ptp_ha::snmp_reset_sync_interval                                        #
# 22) ptp_ha::snmp_reset_delayreq_interval                                    #
# 23) ptp_ha::snmp_reset_pdelayreq_interval                                   #
# 24) ptp_ha::snmp_reset_l1sync_interval                                      #
# 25) ptp_ha::snmp_reset_l1sync_timeout                                       #
# 26) ptp_ha::snmp_reset_egress_latency                                       #
# 27) ptp_ha::snmp_reset_ingress_latency                                      #
# 28) ptp_ha::snmp_reset_constant_asymmetry                                   #
# 29) ptp_ha::snmp_reset_scaled_delay_coefficient                             #
# 30) ptp_ha::snmp_reset_asymmetry_correction                                 #
# 31) ptp_ha::snmp_external_port_config_disable                               #
# 32) ptp_ha::snmp_reset_epc_desired_state                                    #
# 33) ptp_ha::snmp_reset_ipv4_address                                         #
# 34) ptp_ha::snmp_timestamp_corrected_tx_disable                             #
#                                                                             #
#  DUT CHECK FUNCTIONS   :                                                    #
#                                                                             #
#  1) ptp_ha::snmp_check_l1sync_state                                         #
#  2) ptp_ha::snmp_check_ptp_port_state                                       #
#  3) ptp_ha::snmp_check_asymmetry_correction                                 #
#  4) ptp_ha::snmp_check_egress_latency                                       #
#  5) ptp_ha::snmp_check_ingress_latency                                      #
#  6) ptp_ha::snmp_check_constant_asymmetry                                   #
#  7) ptp_ha::snmp_check_scaled_delay_coefficient                             #
#  8) ptp_ha::snmp_check_external_port_config                                 #
#  9) ptp_ha::snmp_check_slave_only                                           #
# 10) ptp_ha::snmp_check_master_only                                          #
# 11) ptp_ha::snmp_check_for_least_offset_from_master                         #
#                                                                             #
#  DUT GET FUNCTIONS     :                                                    #
#                                                                             #
#  1) ptp_ha::snmp_get_timestamp                                              #
#  2) ptp_ha::snmp_get_mean_delay                                             #
#  3) ptp_ha::snmp_get_mean_link_delay                                        #
#  4) ptp_ha::snmp_get_delay_asymmetry                                        #
#                                                                             #
###############################################################################

################################################################################
#                          SNMP SET FUNCTIONS                                  #
################################################################################

package provide ptp_ha 1.0

namespace eval ptp_ha {

    namespace export *
}


#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_port_enable                                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) Port to be enabled.                         #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If port is enabled)                       #
#                                                                              #
#                      0 on Failure (If port could not be enabled)             #
#                                                                              #
#  DEFINITION        : This function enables a specific port on the DUT.       #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_port_enable\                                 #
#                             -session_handler $session_handler\               #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::snmp_port_enable {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_port_enable : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_port_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_port_enable\
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_PORT_ENABLE"\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_snmp_set_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_PORT_ENABLE"\
                        -parameter1              $port_num]} {

            return 0
        }
    }

    return $result

}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_port_ptp_enable                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) Port on which ptp is to be enabled.         #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is enabled on the specified port   #
#                                    at the DUT).                              #
#                                                                              #
#                      0 on Failure (If PTP could not be enabled on the        #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function enables PTP on a specific port on the DUT.#
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_port_ptp_enable\                             #
#                             -session_handler $session_handler\               #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::snmp_port_ptp_enable {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_port_ptp_enable : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_port_ptp_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_port_ptp_enable\
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_PORT_PTP_ENABLE"\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_PORT_PTP_ENABLE"\
                        -parameter1              $port_num]} {
            return 0
        }
    }

    return $result

}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_set_vlan                                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID with which VLAN has to be created.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#  port_num          : (Mandatory) Port which has to be associated to VLAN ID. #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If VLAN ID is created and port is         #
#                                    associated to it on the DUT).             #
#                                                                              #
#                      0 on Failure (If VLAN ID could not be created or port   #
#                                    is not associated to it on the DUT).      #
#                                                                              #
#  DEFINITION        : This function creates VLAN ID and associates given port #
#                      to it on the DUT.                                       #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_set_vlan\                                    #
#                             -session_handler $session_handler\               #
#                             -vlan_id         $vlan_id\                       #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::snmp_set_vlan {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_vlan : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_vlan : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_vlan : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::snmp_set_vlan\
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_VLAN"\
                        -vlan_id                $vlan_id\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler         $session_handler \
                        -cmd_type                "DUT_SET_VLAN"\
                        -parameter1              $vlan_id\
                        -parameter2              $port_num]} {
            return 0
        }
    }

    return $result
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_set_vlan_priority                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID with which VLAN has to be created.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#  vlan_priority     : (Mandatory) VLAN priority to be associated              #
#                                  (e.g., 1)                                   #
#                                                                              #
#  port_num          : (Mandatory) Port with VLAN ID configured.               #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If VLAN priority is associated0           #
#                                                                              #
#                      0 on Failure (If VLAN priority could not be associated) #
#                                                                              #
#  DEFINITION        : This function associates given priority to VLAN         #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_set_vlan_priority\                           #
#                             -session_handler     $session_handler\           #
#                             -vlan_id             $vlan_id\                   #
#                             -vlan_priority       $vlan_priority\             #
#                             -port_num            $port_num                   #
#                                                                              #
################################################################################

proc ptp_ha::snmp_set_vlan_priority {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_vlan_priority : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_vlan_priority : Missing argument    -> vlan_id"
        incr missedargcounter
    }

    if {[info exists param(-vlan_priority)]} {
        set vlan_priority $param(-vlan_priority)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_vlan_priority : Missing argument    -> vlan_priority"
        incr missedargcounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_vlan_priority : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_set_vlan_priority\
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_VLAN_PRIORITY"\
                        -vlan_id                $vlan_id\
                        -vlan_priority          $vlan_priority\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_snmp_set_command \
                        -session_handler         $session_handler \
                        -cmd_type                "DUT_SET_VLAN_PRIORITY"\
                        -parameter1              $vlan_priority\
                        -parameter2              $port_num]} {

            return 0
        }
    }

    return $result
}


#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_global_ptp_enable                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is enabled on the DUT).            #
#                                                                              #
#                      0 on Failure (If PTP could not be enabled on the DUT).  #
#                                                                              #
#  DEFINITION        : This function enables PTP on the DUT.                   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_global_ptp_enable\                           #
#                             -session_handler $session_handler                #
#                                                                              #
################################################################################

proc ptp_ha::snmp_global_ptp_enable {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_global_ptp_enable: Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_global_ptp_enable\
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_GLOBAL_PTP_ENABLE"]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_GLOBAL_PTP_ENABLE"]} {
            return 0
        }
    }

    return $result
}


#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_set_communication_mode                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  ptp_version       : (optional)  PTP version.                                #
#                                  (Default: 2)                                #
#                                                                              #
#  communication_mode : (Mandatory) Communication mode                         #
#                                  (e.g., "Unicast"|"Multicast")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If communication mode is set on the       #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If communication mode could not be        #
#                                    set on the specified port at the DUT).    #
#                                                                              #
#  DEFINITION        : This function sets the communication mode on a specific #
#                      port of the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_set_communication_mode\                      #
#                             -session_handler         $session_handler\       #
#                             -port_num                $port_num\              #
#                             -clock_mode              $clock_mode\            #
#                             -ptp_version             $ptp_version\           #
#                             -communication_mode      $communication_mode     #
#                                                                              #
################################################################################

proc ptp_ha::snmp_set_communication_mode {args} {

    array set param $args

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_communication_mode: Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
       set port_num $param(-port_num)
    } else {
       ERRLOG -msg "ptp_ha::snmp_set_communication_mode : Missing Argument    -> port_num"
       incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_communication_mode : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-communication_mode)]} {
        set communication_mode $param(-communication_mode)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_communication_mode : Missing Argument  -> communication_mode"
        incr missedArgCounter
    }

    if {[info exists param(-ptp_version)]} {
        set ptp_version $param(-ptp_version)
    } else {
        set ptp_version "2"
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_set_communication_mode\
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_COMMUNICATION_MODE" \
                        -port_num               $port_num\
                        -clock_mode             $clock_mode\
                        -ptp_version            $ptp_version\
                        -communication_mode     $communication_mode]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_COMMUNICATION_MODE"\
                        -parameter1             $port_num\
                        -parameter2             $clock_mode\
                        -parameter3             $ptp_version\
                        -parameter4             $communication_mode]} {
            return 0
        }
    }

    return $result
}


#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_set_domain                                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  domain            : (Mandatory) Domain number.                              #
#                                  (e.g., 0..255)                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If domain number is set on the DUT).      #
#                                                                              #
#                      0 on Failure (If domain number could not able to set on #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function sets domain number on the DUT.            #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_set_domain\                                  #
#                             -session_handler $session_handler\               #
#                             -clock_mode      $clock_mode\                    #
#                             -domain          $domain                         #
#                                                                              #
################################################################################

proc ptp_ha::snmp_set_domain {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_domain : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_domain : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-domain)]} {
        set domain $param(-domain)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_domain : Missing Argument    -> domain"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

   set result [ptp_ha::callback_snmp_set_domain\
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_DOMAIN" \
                        -clock_mode             $clock_mode\
                        -domain                 $domain] 

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_DOMAIN"\
                        -parameter1             $clock_mode\
                        -parameter2             $domain]} {
            return 0
        }
    }

    return $result
}


#8.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_set_network_protocol                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) clock_mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  network_protocol  : (Mandatory) Network protocol.                           #
#                                  (e.g., "IEEE 802.3"|"UDP/IPv4")             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If network protocol is set on specified   #
#                                    port at the DUT).                         #
#                                                                              #
#                      0 on Failure (If network protocol could not be set      #
#                                    on specified port at the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets the network protocol on a specific   #
#                      port of the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                     ptp_ha::snmp_set_network_protocol\                       #
#                             -session_handler  $session_handler\              #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -network_protocol $network_protocol              #
#                                                                              #
################################################################################

proc ptp_ha::snmp_set_network_protocol {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_network_protocol : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_network_protocol : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_network_protocol : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-network_protocol)]} {
        set network_protocol $param(-network_protocol)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_network_protocol : Missing Argument    -> network_protocol"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_set_network_protocol\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_NETWORK_PROTOCOL"\
                        -port_num               $port_num\
                        -clock_mode             $clock_mode\
                        -network_protocol       $network_protocol]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_NETWORK_PROTOCOL"\
                        -parameter1             $port_num\
                        -parameter2             $clock_mode\
                        -parameter3             $network_protocol]} {
            return 0
        }
    }

    return $result
}


#9.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_set_clock_mode                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP clock mode is set on specified port#
#                                    of the DUT).                              #
#                                                                              #
#                      0 on Failure (If PTP clock mode could not be set on     #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function sets clock mode on a specific port of DUT.#
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_set_clock_mode\                              #
#                             -session_handler $session_handler\               #
#                             -port_num        $port_num\                      #
#                             -clock_mode      $clock_mode                     #
#                                                                              #
################################################################################

proc ptp_ha::snmp_set_clock_mode {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_clock_mode : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_clock_mode : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_clock_mode : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_set_clock_mode\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_CLOCK_MODE"\
                        -port_num               $port_num\
                        -clock_mode             $clock_mode]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_CLOCK_MODE"\
                        -parameter1             $port_num\
                        -parameter2             $clock_mode]} {
            return 0
        }
    }

    return $result
}


#10.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_set_clock_step                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  clock_step        : (Mandatory) Clock step.                                 #
#                                  (e.g., "One-step"|"Two-step")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If clock-step is set on the specified     #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If clock-step could not be able to set    #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets the clock-step on a specific port on #
#                      the DUT.                                                #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_set_clock_step\                              #
#                             -session_handler  $session_handler\              #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -clock_step       $clock_step                    #
#                                                                              #
################################################################################

proc ptp_ha::snmp_set_clock_step {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_clock_step : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_clock_step : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_clock_step : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-clock_step)]} {
        set clock_step $param(-clock_step)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_clock_step : Missing Argument    -> clock_step"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

     set result [ptp_ha::callback_snmp_set_clock_step\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_CLOCK_STEP"\
                        -port_num               $port_num\
                        -clock_mode             $clock_mode\
                        -clock_step             $clock_step]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_CLOCK_STEP"\
                        -parameter1             $port_num\
                        -parameter1             $clock_mode\
                        -parameter3             $clock_step]} {
            return 0
        }
    }

    return $result
}


#11.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_set_delay_mechanism                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  delay_mechanism   : (Mandatory) Delay mechanism.                            #
#                                  (e.g., e2e|p2p|disable)                     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If delay mechanism is set on specified    #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If delay mechanism could not be set on    #
#                                    specified PTP port of the DUT).           #
#                                                                              #
#  DEFINITION        : This function sets the delay mechanism to a specific    #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_set_delay_mechanism\                         #
#                             -session_handler  $session_handler\              #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -delay_mechanism  $delay_mechanism               #
#                                                                              #
################################################################################

proc ptp_ha::snmp_set_delay_mechanism {args} {

    array set param $args

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_delay_mechanism : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_delay_mechanism : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_delay_mechanism : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-delay_mechanism)]} {
        set delay_mechanism $param(-delay_mechanism)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_delay_mechanism : Missing Argument    -> delay_mechanism"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_set_delay_mechanism\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_DELAY_MECHANISM"\
                        -port_num               $port_num\
                        -clock_mode             $clock_mode\
                        -delay_mechanism        $delay_mechanism]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_DELAY_MECHANISM"\
                        -parameter1             $port_num\
                        -parameter2             $clock_mode\
                        -parameter3             $delay_mechanism]} { 
            return 0
        }
    }

    return $result
}


#12.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_set_priority1                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority1         : (Mandatory) Prority1                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority1 is set on the DUT).          #
#                                                                              #
#                      0 on Failure (If priority1 could not be able to set on  #
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function sets the priority1 on the DUT.            #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_set_priority1\                               #
#                             -session_handler $session_handler\               #
#                             -clock_mode      $clock_mode\                    #
#                             -priority1       $priority1                      #
#                                                                              #
################################################################################

proc ptp_ha::snmp_set_priority1 {args} {

    array set param $args

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_priority1 : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_priority1 : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-priority1)]} {
        set priority1  $param(-priority1)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_priority1 : Missing Argument    -> priority1"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_set_priority1\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_PRIORITY1"\
                        -clock_mode             $clock_mode\
                        -priority1              $priority1]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_PRIORITY1"\
                        -parameter1             $clock_mode\
                        -parameter2             $priority1]} { 
            return 0
        }
    }

    return $result
}


#13.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_set_priority2                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority2         : (Mandatory) Prority2                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority2 is set on the DUT).          #
#                                                                              #
#                      0 on Failure (If priority2 could not be able to set on  #
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function sets the priority2 on the DUT.            #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_set_priority2\                               #
#                             -session_handler $session_handler\               #
#                             -clock_mode      $clock_mode\                    #
#                             -priority2       $priority2                      #
#                                                                              #
################################################################################

proc ptp_ha::snmp_set_priority2 {args} {

    array set param $args

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_priority2 : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_priority2 : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-priority2)]} {
        set priority2  $param(-priority2)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_priority2 : Missing Argument    -> priority2"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_set_priority2\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_PRIORITY2"\
                        -clock_mode             $clock_mode\
                        -priority2              $priority2]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_PRIORITY2"\
                        -parameter1             $clock_mode\
                        -parameter2             $priority2]} { 
            return 0
        }
    }

    return $result
}


#14.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::snmp_tcr_rcr_cr_enable                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If peerTxCoherentIsRequired,              #
#                                    peerRxCoherentIsRequired and              #
#                                    peerCongruentIsRequired is enabled on the #
#                                    DUT).                                     #
#                                                                              #
#                      0 on Failure (If peerTxCoherentIsRequired,              #
#                                    peerRxCoherentIsRequired and              #
#                                    peerCongruentIsRequired could not be      #
#                                    enabled on the DUT).                      #
#                                                                              #
#                                                                              #
#  DEFINITION        : This function enables peerTxCoherentIsRequired,         #
#                      peerRxCoherentIsRequired and peerCongruentIsRequired on #
#                      the DUT.                                                #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_tcr_rcr_cr_enable\                           #
#                             -session_handler $session_handler\               #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::snmp_tcr_rcr_cr_enable {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_tcr_rcr_cr_enable : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_tcr_rcr_cr_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
       return 0
    }

   set result [ptp_ha::callback_snmp_tcr_rcr_cr_enable\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_TCR_RCR_CR_ENABLE"\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_TCR_RCR_CR_ENABLE"\
                        -parameter1             $port_num]} {
            return 0
        }
    }

    return $result
}


#15.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_l1sync_enable                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If L1Sync option is enabled on the        #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If L1Sync option could not be enabled on  #
#                                    the specified port of the DUT).           #
#                                                                              #
#  DEFINITION        : This function enables L1Sync option on a specific port  #
#                      at the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_l1sync_enable\                               #
#                             -session_handler $session_handler\               #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::snmp_l1sync_enable {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_l1sync_enable : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_l1sync_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

   set result [ptp_ha::callback_snmp_l1sync_enable\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_L1SYNC_ENABLE"\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_L1SYNC_ENABLE"\
                        -parameter1             $port_num]} {
            return 0
        }
    }

    return $result
}


#16.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_l1sync_opt_params_enable                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If extended format of L1Sync TLV is       #
#                                    enabled on the specified port at the DUT).#
#                                                                              #
#                      0 on Failure (If extended format of L1Sync TLV could not#
#                                    be enabled on the specified port at the   #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function enables extended format of L1Sync TLV on  #
#                      the specified port at the DUT.                          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_l1sync_opt_params_enable\                    #
#                             -session_handler $session_handler\               #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::snmp_l1sync_opt_params_enable {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_l1sync_opt_params_enable : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_l1sync_opt_params_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

   set result [ptp_ha::callback_snmp_l1sync_opt_params_enable\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_L1SYNC_OPT_PARAMS_ENABLE"\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_L1SYNC_OPT_PARAMS_ENABLE"\
                        -parameter1             $port_num]} {
            return 0
        }
    }

   return $result
}


#17.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_slaveonly_enable                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If slave-only is enabled on the           #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If slave-only could not be enabled on the #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function enables slave-only on the specified port  #
#                      at the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_slaveonly_enable\                            #
#                             -session_handler $session_handler\               #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::snmp_slaveonly_enable {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_slaveonly_enable : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_slaveonly_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

   set result [ptp_ha::callback_snmp_slaveonly_enable\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SLAVEONLY_ENABLE"\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SLAVEONLY_ENABLE"\
                        -parameter1             $port_num]} {
            return 0
        }
    }

    return $result
}


#18.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_masteronly_enable                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If master-only is enabled on the          #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If master-only could not be enabled on the#
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function enables master-only on the specified port #
#                      at the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_masteronly_enable\                           #
#                             -session_handler $session_handler\               #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::snmp_masteronly_enable {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_masteronly_enable :  Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_masteronly_enable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

   set result [ptp_ha::callback_snmp_masteronly_enable\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_MASTERONLY_ENABLE"\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_MASTERONLY_ENABLE"\
                        -parameter1             $port_num]} {
            return 0
        }
    }

    return $result
}


#19.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::snmp_set_announce_interval                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  announce_interval : (Mandatory) announceInterval.                           #
#                                  (e.g., 1|2|4|8|16)                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If announceInterval value is set on       #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If announceInterval value could not be    #
#                                    set on specified port of the DUT).        #
#                                                                              #
#  DEFINITION        : This function sets announceInterval value to a specific #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_set_announce_interval\                       #
#                             -session_handler       $session_handler\         #
#                             -port_num              $port_num\                #
#                             -announce_interval     $announce_interval        #
#                                                                              #
################################################################################

proc ptp_ha::snmp_set_announce_interval {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_announce_interval :  Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_announce_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-announce_interval)]} {
        set announce_interval $param(-announce_interval)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_announce_interval : Missing Argument    -> announce_interval"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_set_announce_interval\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_ANNOUNCE_INTERVAL"\
                        -port_num               $port_num\
                        -announce_interval      $announce_interval]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_ANNOUNCE_INTERVAL"\
                        -parameter1             $port_num\
                        -parameter2             $announce_interval]} {
            return 0
        }
    }

    return $result
}


#20.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::snmp_set_announce_timeout                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  announce_timeout  : (Mandatory) announceReceiptTimeout.                     #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If announceReceiptTimeout value is set on #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If announceReceiptTimeout value could not #
#                                    be set on specified port of the DUT).     #
#                                                                              #
#  DEFINITION        : This function sets announceReceiptTimeout value to a    #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_set_announce_timeout\                        #
#                             -session_handler       $session_handler\         #
#                             -port_num              $port_num\                #
#                             -announce_timeout      $announce_timeout         #
#                                                                              #
################################################################################

proc ptp_ha::snmp_set_announce_timeout {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_announce_timeout : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_announce_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-announce_timeout)]} {
        set announce_timeout $param(-announce_timeout)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_announce_timeout : Missing Argument    -> announce_timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

   set result [ptp_ha::callback_snmp_set_announce_timeout\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_ANNOUNCE_TIMEOUT"\
                        -port_num               $port_num\
                        -announce_timeout       $announce_timeout]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_ANNOUNCE_TIMEOUT"\
                        -parameter1             $port_num\
                        -parameter2             $announce_timeout]} {
            return 0
        }
    }

    return $result
}


#21.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_set_sync_interval                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  sync_interval     : (Mandatory) logSyncInterval                             #
#                                  (e.g., 3)                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If logSyncInterval value is set on        #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If logSyncInterval value could not be     #
#                                    set on specified port of the DUT).        #
#                                                                              #
#  DEFINITION        : This function sets logSyncInterval value to a specific  #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_set_sync_interval\                           #
#                             -session_handler       $session_handler\         #
#                             -port_num              $port_num\                #
#                             -sync_interval         $sync_interval            #
#                                                                              #
################################################################################

proc ptp_ha::snmp_set_sync_interval {args} {

    array set param $args

    set missedArgCounter  0

 ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_sync_interval : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_sync_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-sync_interval)]} {
        set sync_interval $param(-sync_interval)
    } else {
        ERRLOG  -msg "ptp_ha::snmp_set_sync_interval : Missing Argument    -> sync_interval"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_set_sync_interval\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_SYNC_INTERVAL"\
                        -port_num               $port_num\
                        -sync_interval          $sync_interval]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_SYNC_INTERVAL"\
                        -parameter1             $port_num\
                        -parameter2             $sync_interval]} {
            return 0
        }
    }

    return $result
}


#22.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::snmp_set_delayreq_interval                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  delayreq_interval : (Mandatory) MinDelayReqInterval.                        #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If MinDelayReqInterval is set on specified#
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If MinDelayReqInterval could not be set   #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets the MinDelayReqInterval to the       #
#                      specific PTP port on the DUT.                           #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_set_delayreq_interval\                       #
#                             -session_handler       $session_handler\         #
#                             -port_num              $port_num\                #
#                             -delayreq_interval     $delayreq_interval        #
#                                                                              #
################################################################################

proc ptp_ha::snmp_set_delayreq_interval {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_delayreq_interval : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
       set port_num $param(-port_num)
    } else {
       ERRLOG  -msg "ptp_ha::snmp_set_delayreq_interval : Missing Argument    -> port_num"
       incr missedArgCounter
    }

    if {[info exists param(-delayreq_interval)]} {
       set delayreq_interval $param(-delayreq_interval)
    } else {
       ERRLOG  -msg "ptp_ha::snmp_set_delayreq_interval : Missing Argument    -> delayreq_interval"
       incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
     }

     set result [ptp_ha::callback_snmp_set_delayreq_interval\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_DELAYREQ_INTERVAL"\
                        -port_num               $port_num\
                        -delayreq_interval      $delayreq_interval]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_DELAYREQ_INTERVAL"\
                        -parameter1             $port_num\
                        -parameter2             $delayreq_interval]} {
            return 0
        }
    }

    return $result
}


#23.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::snmp_set_pdelayreq_interval                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  pdelayreq_interval: (Mandatory) minPdelayReqInterval.                       #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If minPdelayReqInterval is set on         #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If minPdelayReqInterval could not be set  #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets the minPdelayReqInterval to the      #
#                      specific PTP port on the DUT.                           #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_set_pdelayreq_interval\                      #
#                             -session_handler       $session_handler\         #
#                             -port_num              $port_num\                #
#                             -pdelayreq_interval    $pdelayreq_interval       #
#                                                                              #
################################################################################

proc ptp_ha::snmp_set_pdelayreq_interval {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_pdelayreq_interval : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
       set port_num $param(-port_num)
    } else {
       ERRLOG  -msg "ptp_ha::snmp_set_pdelayreq_interval : Missing Argument    -> port_num"
       incr missedArgCounter
    }

    if {[info exists param(-pdelayreq_interval)]} {
       set pdelayreq_interval $param(-pdelayreq_interval)
    } else {
       ERRLOG  -msg "ptp_ha::snmp_set_pdelayreq_interval : Missing Argument    -> delayreq_interval"
       incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
     }

     set result [ptp_ha::callback_snmp_set_pdelayreq_interval\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_PDELAYREQ_INTERVAL"\
                        -port_num               $port_num\
                        -pdelayreq_interval      $pdelayreq_interval]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_PDELAYREQ_INTERVAL"\
                        -parameter1             $port_num\
                        -parameter2             $pdelayreq_interval]} {
            return 0
        }
    }

    return $result
}


#24.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_set_l1sync_interval                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  l1sync_interval   : (Mandatory) logL1SyncInterval.                          #
#                                  (e.g.,3 )                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If logL1SyncInterval is set on specified  #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If logL1SyncInterval could not be set on  #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function sets the logL1SyncInterval to a specific  #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_set_l1sync_interval\                         #
#                             -session_handler     $session_handler\           #
#                             -port_num            $port_num\                  #
#                             -l1sync_interval     $l1sync_interval            #
#                                                                              #
################################################################################

proc ptp_ha::snmp_set_l1sync_interval {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_l1sync_interval : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_l1sync_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-l1sync_interval)]} {
        set l1sync_interval $param(-l1sync_interval)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_l1sync_interval : Missing Argument    -> l1sync_interval"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

   set result [ptp_ha::callback_snmp_set_l1sync_interval\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_L1SYNC_INTERVAL"\
                        -port_num               $port_num\
                        -l1sync_interval        $l1sync_interval]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_L1SYNC_INTERVAL"\
                        -parameter1             $port_num\
                        -parameter2             $l1sync_interval]} {
            return 0
        }
    }

     return $result
}


#25.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_set_l1sync_timeout                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  l1sync_timeout    : (Mandatory) L1SyncReceiptTimeout.                       #
#                                  (e.g., 3)                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If L1SyncReceiptTimeout is set on         #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If L1SyncReceiptTimeout could not be set  #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function sets L1SyncReceiptTimeout to a specific   #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_set_l1sync_timeout\                          #
#                             -session_handler       $session_handler\         #
#                             -port_num              $port_num\                #
#                             -l1sync_timeout        $l1sync_timeout           #
#                                                                              #
################################################################################

proc ptp_ha::snmp_set_l1sync_timeout {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_l1sync_timeout: Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_l1sync_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-l1sync_timeout)]} {
        set l1sync_timeout $param(-l1sync_timeout)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_l1sync_timeout : Missing Argument    -> l1sync_timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

   set result [ptp_ha::callback_snmp_set_l1sync_timeout\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_L1SYNC_TIMEOUT"\
                        -port_num               $port_num\
                        -l1sync_timeout         $l1sync_timeout]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_L1SYNC_TIMEOUT"\
                        -parameter1             $port_num\
                        -parameter2             $l1sync_timeout]} {
            return 0
        }
    }

    return $result
}


#26.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_set_egress_latency                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Mandatory) timestampCorrectionPortDS.egressLatency.    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If timestampCorrectionPortDS.egressLatency#
#                                    is set on specified port of the DUT).     #
#                                                                              #
#                      0 on Failure (If timestampCorrectionPortDS.egressLatency#
#                                    could not be set on specified port of the #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function sets timestampCorrectionPortDS.           #
#                      egressLatency to a specific port on the DUT.            #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_set_egress_latency\                          #
#                             -session_handler       $session_handler\         #
#                             -port_num              $port_num\                #
#                             -latency               $latency                  #
#                                                                              #
################################################################################

proc ptp_ha::snmp_set_egress_latency {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_egress_latency : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_egress_latency : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-latency)]} {
        set latency $param(-latency)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_egress_latency : Missing Argument    -> latency"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_set_egress_latency\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_EGRESS_LATENCY"\
                        -port_num               $port_num\
                        -latency                $latency]

    if {$result == 2} {

        if {![Send_snmp_set_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_EGRESS_LATENCY"\
                        -parameter1             $port_num\
                        -parameter2             $latency]} {

            return 0
        }
    }

    return $result
}


#27.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_set_ingress_latency                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Mandatory) timestampCorrectionPortDS.ingressLatency.   #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If timestampCorrectionPortDS.ingressLatency#
#                                    is set on specified port of the DUT).     #
#                                                                              #
#                      0 on Failure (If timestampCorrectionPortDS.ingressLatency#
#                                    could not be set on specified port of the #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function sets timestampCorrectionPortDS.           #
#                      ingressLatency to a specific port on the DUT.           #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_set_ingress_latency\                         #
#                             -session_handler       $session_handler\         #
#                             -port_num              $port_num\                #
#                             -latency               $latency                  #
#                                                                              #
################################################################################

proc ptp_ha::snmp_set_ingress_latency {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_ingress_latency : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_ingress_latency : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-latency)]} {
        set latency $param(-latency)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_ingress_latency : Missing Argument    -> latency"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_set_ingress_latency\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_INGRESS_LATENCY"\
                        -port_num               $port_num\
                        -latency                $latency]

    if {$result == 2} {

        if {![Send_snmp_set_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_INGRESS_LATENCY"\
                        -parameter1             $port_num\
                        -parameter2             $latency]} {

            return 0
        }
    }

    return $result
}


#28.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_set_constant_asymmetry                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  constant_asymmetry :(Mandatory) asymmetryCorrectionPortDS.constantAsymmetry.#
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If asymmetryCorrectionPortDS.             #
#                                    constantAsymmetry is set on specified port#
#                                    of the DUT).                              #
#                                                                              #
#                      0 on Failure (If asymmetryCorrectionPortDS.             #
#                                    constantAsymmetry could not be set on     #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function sets asymmetryCorrectionPortDS.           #
#                      constantAsymmetry to a specific port on the DUT.        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_set_constant_asymmetry\                      #
#                             -session_handler       $session_handler\         #
#                             -port_num              $port_num\                #
#                             -constant_asymmetry    $constant_asymmetry       #
#                                                                              #
################################################################################

proc ptp_ha::snmp_set_constant_asymmetry {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_constant_asymmetry : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_constant_asymmetry : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-constant_asymmetry)]} {
        set constant_asymmetry $param(-constant_asymmetry)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_constant_asymmetry : Missing Argument    -> constant_asymmetry"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_set_constant_asymmetry\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_CONSTANT_ASYMMETRY"\
                        -port_num               $port_num\
                        -constant_asymmetry     $constant_asymmetry]

    if {$result == 2} {

        if {![Send_snmp_set_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_CONSTANT_ASYMMETRY"\
                        -parameter1             $port_num\
                        -parameter2             $constant_asymmetry]} {

            return 0
        }
    }

    return $result
}


#29.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_set_scaled_delay_coefficient               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  delay_coefficient : (Mandatory) asymmetryCorrectionPortDS.                  #
#                                  scaledDelayCoefficient .                    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If asymmetryCorrectionPortDS.             #
#                                    scaledDelayCoefficient is set on specified#
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If asymmetryCorrectionPortDS.             #
#                                    scaledDelayCoefficient could not be set on#
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function sets asymmetryCorrectionPortDS.           #
#                      scaledDelayCoefficient to a specific port on the DUT.   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_set_scaled_delay_coefficient\                #
#                             -session_handler       $session_handler\         #
#                             -port_num              $port_num\                #
#                             -delay_coefficient     $delay_coefficient        #
#                                                                              #
################################################################################

proc ptp_ha::snmp_set_scaled_delay_coefficient {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_scaled_delay_coefficient : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_scaled_delay_coefficient : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-delay_coefficient)]} {
        set delay_coefficient $param(-delay_coefficient)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_scaled_delay_coefficient : Missing Argument    -> delay_coefficient"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_set_scaled_delay_coefficient\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_SCALED_DELAY_COEFFICIENT"\
                        -port_num               $port_num\
                        -delay_coefficient      $delay_coefficient]

    if {$result == 2} {

        if {![Send_snmp_set_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_SCALED_DELAY_COEFFICIENT"\
                        -parameter1             $port_num\
                        -parameter2             $delay_coefficient]} {

            return 0
        }
    }

    return $result
}


#30.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_set_asymmetry_correction                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If asymmetryCorrectionPortDS.enable is set#
#                                    on specified port of the DUT).            #
#                                                                              #
#                      0 on Failure (If asymmetryCorrectionPortDS.enable could #
#                                    not be set on specified port of the DUT). #
#                                                                              #
#  DEFINITION        : This function sets the asymmetryCorrectionPortDS.enable #
#                      to the specific PTP port on the DUT.                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_set_asymmetry_correction\                    #
#                             -session_handler       $session_handler\         #
#                             -port_num              $port_num                 #
#                                                                              #
################################################################################

proc ptp_ha::snmp_set_asymmetry_correction {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_asymmetry_correction : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_asymmetry_correction : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_set_asymmetry_correction\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_ASYMMETRY_CORRECTION"\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_snmp_set_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_ASYMMETRY_CORRECTION"\
                        -parameter1             $port_num]} {

            return 0
        }
    }

    return $result
}


#31.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_external_port_config_enable                #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If external port configuration option is  #
#                                    enabled on the specified port at the DUT).#
#                                                                              #
#                      0 on Failure (If external port configuration option     #
#                                    could not be enabled on the specified port#
#                                    at the DUT).                              #
#                                                                              #
#  DEFINITION        : This function enables external port configuration option#
#                      on a specific port at the DUT.                          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_external_port_config_enable\                 #
#                             -session_handler $session_handler\               #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::snmp_external_port_config_enable {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_external_port_config_enable : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_external_port_config_enable: Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_external_port_config_enable\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_EXTERNAL_PORT_CONFIG_ENABLE"\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_EXTERNAL_PORT_CONFIG_ENABLE"\
                        -parameter1             $port_num]} {
            return 0
        }
    }

    return $result
}


#32.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_set_epc_desired_state                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  desired_state     : (Mandatory) externalPortConfigurationPortDS.desiredState#
#                                  (e.g., "INITIALIZING"|"FAULTY"|"DISABLED"|  #
#                                         "LISTENING"|"PRE_MASTER"|"MASTER"|   #
#                                         "PASSIVE"|"UNCALIBRATED"|"SLAVE"     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given externalPortConfigurationPortDS. #
#                                    desiredState is set on specified port of  #
#                                    the DUT).                                 #
#                                                                              #
#                      0 on Failure (If given externalPortConfigurationPortDS. #
#                                    desiredState could not be set on specifed #
#                                    port of the DUT).                         #
#                                                                              #
#  DEFINITION        : This function sets given externalPortConfigurationPortDS#
#                      desiredState to a specific port on the DUT.             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_set_epc_desired_state\                       #
#                             -session_handler       $session_handler\         #
#                             -port_num              $port_num\                #
#                             -desired_state         $desired_state            #
#                                                                              #
################################################################################

proc ptp_ha::snmp_set_epc_desired_state {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_epc_desired_state : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_epc_desired_state : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-desired_state)]} {
        set desired_state $param(-desired_state)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_epc_desired_state : Missing Argument    -> desired_state"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_set_epc_desired_state\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_EPC_DESIRED_STATE"\
                        -port_num               $port_num\
                        -desired_state          $desired_state]

    if {$result == 2} {

        if {![Send_snmp_set_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_EPC_DESIRED_STATE"\
                        -parameter1             $port_num\
                        -parameter2             $desired_state]} {

            return 0
        }
    }

    return $result
}


#33.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_set_ipv4_address                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  ip_address        : (Mandatory) IPv4 address.                               #
#                                  (e.g., 192.168.200.1)                       #
#                                                                              #
#  net_mask          : (Mandatory) Subnet mask.                                #
#                                  (e.g., 255.255.255.0)                       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID.                                    #
#                                  (e.g., 100)                                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given IP address is set on specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given IP address could not be set on   #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function sets given IP address to a specific port  #
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_set_ipv4_address\                            #
#                             -session_handler     $session_handler\           #
#                             -port_num            $port_num\                  #
#                             -ip_address          $ip_address\                #
#                             -net_mask            $net_mask\                  #
#                             -vlan_id             $vlan_id                    #
#                                                                              #
################################################################################

proc ptp_ha::snmp_set_ipv4_address {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_ipv4_address : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_ipv4_address : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-ip_address)]} {
        set ip_address $param(-ip_address)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_ipv4_address : Missing Argument    -> ip_address"
        incr missedArgCounter
    }

    if {[info exists param(-net_mask)]} {
        set net_mask $param(-net_mask)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_ipv4_address : Missing Argument    -> net_mask"
        incr missedArgCounter
    }

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "ptp_ha::snmp_set_ipv4_address : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_set_ipv4_address\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SET_IPV4_ADDRESS"\
                        -port_num               $port_num\
                        -ip_address             $ip_address\
                        -net_mask               $net_mask\
                        -vlan_id                $vlan_id]

    if {$result == 2} {

        if {![Send_snmp_set_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SET_IPV4_ADDRESS"\
                        -parameter1             $port_num\
                        -parameter2             $ip_address\
                        -parameter3             $net_mask\
                        -parameter4             $vlan_id]} {

            return 0
        }
    }

    return $result
}


#34.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_timestamp_corrected_tx_enable              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If L1SyncOptParamsPortDS.                 #
#                                    timestampsCorrectedTx option is enabled on#
#                                    the specified port at the DUT).           #
#                                                                              #
#                      0 on Failure (If L1SyncOptParamsPortDS.                 #
#                                    timestampsCorrectedTx option could not be #
#                                    enabled on the specified port at the DUT).#
#                                                                              #
#  DEFINITION        : This function enables L1SyncOptParamsPortDS.            #
#                      timestampsCorrectedTx option on a specified port at the #
#                      DUT.                                                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_timestamp_corrected_tx_enable\               #
#                             -session_handler $session_handler\               #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::snmp_timestamp_corrected_tx_enable {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_timestamp_corrected_tx_enable : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_timestamp_corrected_tx_enable: Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_timestamp_corrected_tx_enable\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_TIMESTAMP_CORRECTED_TX_ENABLE"\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_TIMESTAMP_CORRECTED_TX_ENABLE"\
                        -parameter1             $port_num]} {
            return 0
        }
    }

    return $result
}

################################################################################
#                      SNMP DISABLING FUNCTIONS                                #
################################################################################


#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_port_disable                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) Port to be disabled.                        #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If port is disabled)                      #
#                                                                              #
#                      0 on Failure (If port could not be disabled)            #
#                                                                              #
#  DEFINITION        : This function disables a specific port on the DUT.      #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_port_disable\                                #
#                             -session_handler $session_handler\               #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::snmp_port_disable {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_port_disable : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_port_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_port_disable\
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_PORT_DISABLE"\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_snmp_set_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_PORT_DISABLE"\
                        -parameter1              $port_num]} {

            return 0
        }
    }

    return $result

}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_port_ptp_disable                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) Port on which ptp is to be disabled.        #
#                                  (e.g., fa0/1)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is disabled on the specified port  #
#                                    at the DUT).                              #
#                                                                              #
#                      0 on Failure (If PTP could not be disabled on the       #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function disables PTP on a specific port on the DUT#
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_port_ptp_disable\                            #
#                             -session_handler $session_handler\               #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::snmp_port_ptp_disable {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_port_ptp_disable : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_port_ptp_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

   set result [ptp_ha::callback_snmp_port_ptp_disable\
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_PORT_PTP_DISABLE"\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_PORT_PTP_DISABLE"\
                        -parameter1              $port_num]} {
            return 0
        }
    }

    return $result

}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_reset_vlan                                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID with which VLAN has to be deleted.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#  port_num          : (Mandatory) Port which has to be removed from VLAN ID.  #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If port is removed from VLAN ID and VLAN  #
#                                    ID is deleted from the DUT).              #
#                                                                              #
#                      0 on Failure (If port could not be removed from VLAN ID #
#                                    and VLAN ID is not deledted from the DUT).#
#                                                                              #
#  DEFINITION        : This function removes port from VLAN ID and deletes VLAN#
#                      ID from the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_reset_vlan\                                  #
#                             -session_handler   $session_handler\             #
#                             -vlan_id           $vlan_id\                     #
#                             -port_num          $port_num                     #
#                                                                              #
################################################################################

proc ptp_ha::snmp_reset_vlan {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_vlan : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_vlan : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_vlan : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

   set result [ptp_ha::snmp_reset_vlan\
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_VLAN"\
                        -vlan_id                $vlan_id\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_VLAN"\
                        -parameter1              $vlan_id\
                        -parameter2              $port_num]} {
            return 0
        }
    }

    return $result
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_reset_vlan_priority                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID with which VLAN has to be created.  #
#                                  (e.g., 100)                                 #
#                                                                              #
#  vlan_priority     : (Mandatory) VLAN priority to be associated              #
#                                  (e.g., 1)                                   #
#                                                                              #
#  port_num          : (Mandatory) Port with VLAN ID configured.               #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If VLAN priority is disassociated)        #
#                                                                              #
#                      0 on Failure (If VLAN priority could not be             #
#                                    disassociated)                            #
#                                                                              #
#  DEFINITION        : This function disassociates given priority from VLAN    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_reset_vlan_priority\                         #
#                             -session_handler     $session_handler\           #
#                             -vlan_id             $vlan_id\                   #
#                             -vlan_priority       $vlan_priority\             #
#                             -port_num            $port_num                   #
#                                                                              #
################################################################################

proc ptp_ha::snmp_reset_vlan_priority {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_vlan_priority : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_vlan_priority : Missing argument    -> vlan_id"
        incr missedargcounter
    }

    if {[info exists param(-vlan_priority)]} {
        set vlan_priority $param(-vlan_priority)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_vlan_priority : Missing argument    -> vlan_priority"
        incr missedargcounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_vlan_priority : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_set_vlan_priority\
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_VLAN_PRIORITY"\
                        -vlan_id                $vlan_id\
                        -vlan_priority          $vlan_priority\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_snmp_set_command \
                        -session_handler         $session_handler \
                        -cmd_type                "DUT_RESET_VLAN_PRIORITY"\
                        -parameter1              $vlan_priority\
                        -parameter2              $port_num]} {

            return 0
        }
    }

    return $result
}


#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_global_ptp_disable                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP is disabled on the DUT).           #
#                                                                              #
#                      0 on Failure (If PTP could not be disabled on the DUT). #
#                                                                              #
#  DEFINITION        : This function disables PTP on the DUT.                  #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_port_ptp_disable\                            #
#                             -session_handler $session_handler                #
#                                                                              #
################################################################################

proc ptp_ha::snmp_global_ptp_disable {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_global_ptp_disable : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

   set result [ptp_ha::callback_snmp_global_ptp_disable\
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_GLOBAL_PTP_DISABLE"]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_GLOBAL_PTP_DISABLE"]} {
            return 0
        }
    }

    return $result

}


#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_reset_communication_mode                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  ptp_version       : (optional)  PTP version.                                #
#                                  (Default: 2)                                #
#                                                                              #
#  communication_mode : (Mandatory) Communication mode                         #
#                                  (e.g., "Unicast"|"Multicast")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If communication mode is reset on the     #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If communication mode could not be        #
#                                    reset on the specified port at the DUT).  #
#                                                                              #
#  DEFINITION        : This function resets the communication mode on a        #
#                      specific port of the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_reset_communication_mode\                    #
#                             -session_handler         $session_handler\       #
#                             -port_num                $port_num\              #
#                             -clock_mode              $clock_mode\            #
#                             -ptp_version             $ptp_version\           #
#                             -communication_mode      $communication_mode     #
#                                                                              #
################################################################################

proc ptp_ha::snmp_reset_communication_mode {args} {

    array set param $args

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_communication_mode : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
       set port_num $param(-port_num)
    } else {
       ERRLOG -msg "ptp_ha::snmp_reset_communication_mode : Missing Argument    -> port_num"
       incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_communication_mode : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-communication_mode)]} {
        set communication_mode $param(-communication_mode)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_communication_mode : Missing Argument  -> communication_mode"
        incr missedArgCounter
    }

    if {[info exists param(-ptp_version)]} {
        set ptp_version $param(-ptp_version)
    } else {
        set ptp_version "2"
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

 set result [ptp_ha::callback_snmp_reset_communication_mode\
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_COMMUNICATION_MODE" \
                        -port_num               $port_num\
                        -clock_mode             $clock_mode\
                        -ptp_version            $ptp_version\
                        -communication_mode     $communication_mode]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_COMMUNICATION_MODE"\
                        -parameter1             $port_num\
                        -parameter2             $clock_mode\
                        -parameter3             $ptp_version\
                        -parameter4             $communication_mode]} {
            return 0
        }
    }

    return $result
}


#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_reset_domain                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  domain            : (Mandatory) Domain number.                              #
#                                  (e.g., 0..255)                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If domain number is reset on the DUT).    #
#                                                                              #
#                      0 on Failure (If domain number could not able to reset  #
#                                    on DUT).                                  #
#                                                                              #
#  DEFINITION        : This function resets domain number on the DUT.          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_reset_domain\                                #
#                             -session_handler $session_handler\               #
#                             -clock_mode      $clock_mode\                    #
#                             -domain          $domain                         #
#                                                                              #
################################################################################

proc ptp_ha::snmp_reset_domain {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_domain : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_domain : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-domain)]} {
        set domain $param(-domain)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_domain : Missing Argument    -> domain"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

   set result [ptp_ha::callback_snmp_reset_domain\
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_DOMAIN" \
                        -clock_mode             $clock_mode\
                        -domain                 $domain] 

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_DOMAIN"\
                        -parameter1             $clock_mode\
                        -parameter2             $domain]} {
            return 0
        }
    }

    return $result
}


#8.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_reset_network_protocol                     #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) clock_mode.                                 #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  network_protocol  : (Mandatory) Network protocol.                           #
#                                  (e.g., "IEEE 802.3"|"UDP/IPv4")             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If network protocol is reset on specified #
#                                    port at the DUT).                         #
#                                                                              #
#                      0 on Failure (If network protocol could not be reset    #
#                                    on specified port at the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets the network protocol on a specific #
#                      port of the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                     ptp_ha::snmp_reset_network_protocol\                     #
#                             -session_handler  $session_handler\              #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -network_protocol $network_protocol              #
#                                                                              #
################################################################################

proc ptp_ha::snmp_reset_network_protocol {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_network_protocol : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_network_protocol : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_network_protocol : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-network_protocol)]} {
        set network_protocol $param(-network_protocol)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_network_protocol : Missing Argument    -> network_protocol"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_reset_network_protocol\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_NETWORK_PROTOCOL"\
                        -port_num               $port_num\
                        -clock_mode             $clock_mode\
                        -network_protocol       $network_protocol]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_NETWORK_PROTOCOL"\
                        -parameter1             $port_num\
                        -parameter2             $clock_mode\
                        -parameter3             $network_protocol]} {
            return 0
        }
    }

    return $result
}


#9.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_reset_clock_mode                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If PTP clock mode is reset on specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If PTP clock mode could not be reset on   #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function resets clock mode on a specific port of   #
#                      DUT.                                                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_reset_clock_mode\                            #
#                             -session_handler $session_handler\               #
#                             -port_num        $port_num\                      #
#                             -clock_mode      $clock_mode                     #
#                                                                              #
################################################################################

proc ptp_ha::snmp_reset_clock_mode {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_clock_mode : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_clock_mode : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_clock_mode : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_reset_clock_mode\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_CLOCK_MODE"\
                        -port_num               $port_num\
                        -clock_mode             $clock_mode]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_CLOCK_MODE"\
                        -parameter1             $port_num\
                        -parameter2             $clock_mode]} {
            return 0
        }
    }

    return $result
}


#10.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_reset_clock_step                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  clock_step        : (Mandatory) Clock step.                                 #
#                                  (e.g., "One-step"|"Two-step")               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If clock-step is reset on the specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If clock-step could not be able to reset  #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets the clock-step on a specific port  #
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_reset_clock_step\                            #
#                             -session_handler  $session_handler\              #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -clock_step       $clock_step                    #
#                                                                              #
################################################################################

proc ptp_ha::snmp_reset_clock_step {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_clock_step : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_clock_step : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_clock_step : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-clock_step)]} {
        set clock_step $param(-clock_step)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_clock_step : Missing Argument    -> clock_step"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_reset_clock_step\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_CLOCK_STEP"\
                        -port_num               $port_num\
                        -clock_mode             $clock_mode\
                        -clock_step             $clock_step]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_CLOCK_STEP"\
                        -parameter1             $port_num\
                        -parameter1             $clock_mode\
                        -parameter3             $clock_step]} {
            return 0
        }
    }

    return $result
}


#11.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_reset_delay_mechanism                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                  .              #
#                                  (e.g., Ordinary Clock)                      #
#                                                                              #
#  delay_mechanism   : (Mandatory) Delay mechanism.                            #
#                                  (e.g., e2e|p2p|disable)                     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If delay mechanism is reset on specified  #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If delay mechanism could not be reset on  #
#                                    specified PTP port of the DUT).           #
#                                                                              #
#  DEFINITION        : This function resets the delay mechanism to a specific  #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_reset_delay_mechanism\                       #
#                             -session_handler  $session_handler\              #
#                             -port_num         $port_num\                     #
#                             -clock_mode       $clock_mode\                   #
#                             -delay_mechanism  $delay_mechanism               #
#                                                                              #
################################################################################

proc ptp_ha::snmp_reset_delay_mechanism {args} {

    array set param $args

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_delay_mechanism : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_delay_mechanism : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_delay_mechanism : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-delay_mechanism)]} {
        set delay_mechanism $param(-delay_mechanism)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_delay_mechanism : Missing Argument    -> delay_mechanism"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_reset_delay_mechanism\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_DELAY_MECHANISM"\
                        -port_num               $port_num\
                        -clock_mode             $clock_mode\
                        -delay_mechanism        $delay_mechanism]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_DELAY_MECHANISM"\
                        -parameter1             $port_num\
                        -parameter2             $clock_mode\
                        -parameter3             $delay_mechanism]} { 
            return 0
        }
    }

     return $result
}


#12.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_reset_priority1                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority1         : (Mandatory) Prority1                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority1 is reset on the DUT).        #
#                                                                              #
#                      0 on Failure (If priority1 could not be able to reset on#
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function resets the priority1 on the DUT.          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_reset_priority1\                             #
#                             -session_handler $session_handler\               #
#                             -clock_mode      $clock_mode\                    #
#                             -priority1       $priority1                      #
#                                                                              #
################################################################################

proc ptp_ha::snmp_reset_priority1 {args} {

    array set param $args

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_priority1 : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_priority1 : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-priority1)]} {
        set priority1  $param(-priority1)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_priority1 : Missing Argument    -> priority1"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

   set result [ptp_ha::callback_snmp_reset_priority1\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_PRIORITY1"\
                        -clock_mode             $clock_mode\
                        -priority1              $priority1]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_PRIORITY1"\
                        -parameter1             $clock_mode\
                        -parameter2             $priority1]} { 
            return 0
        }
    }

    return $result
}


#13.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_reset_priority2                            #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  clock_mode        : (Mandatory) Clock mode.                                 #
#                                  (e.g., Ordinary Clock).                     #
#                                                                              #
#  priority2         : (Mandatory) Prority2                                    #
#                                  (e.g., 0..255).                             #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If priority2 is reset on the DUT).        #
#                                                                              #
#                      0 on Failure (If priority2 could not be able to reset on#
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function resets the priority2 on the DUT.          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_reset_priority2\                             #
#                             -session_handler $session_handler\               #
#                             -clock_mode      $clock_mode\                    #
#                             -priority2       $priority2                      #
#                                                                              #
################################################################################

proc ptp_ha::snmp_reset_priority2 {args} {

    array set param $args

    set missedArgCounter 0

 ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_priority2 : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-clock_mode)]} {
        set clock_mode $param(-clock_mode)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_priority2 : Missing Argument    -> clock_mode"
        incr missedArgCounter
    }

    if {[info exists param(-priority2)]} {
        set priority2  $param(-priority2)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_priority2 : Missing Argument    -> priority2"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_reset_priority2\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_PRIORITY2"\
                        -clock_mode             $clock_mode\
                        -priority2              $priority2]

    if {$result == 2} {

        if {![Send_snmp_set_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_PRIORITY2"\
                        -parameter1             $clock_mode\
                        -parameter2             $priority2]} { 

            return 0
        }
    }

    return $result
}


#14.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::snmp_tcr_rcr_cr_disable                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If peerTxCoherentIsRequired,              #
#                                    peerRxCoherentIsRequired and              #
#                                    peerCongruentIsRequired is disabled on the#
#                                    DUT).                                     #
#                                                                              #
#                      0 on Failure (If peerTxCoherentIsRequired,              #
#                                    peerRxCoherentIsRequired and              #
#                                    peerCongruentIsRequired could not be      #
#                                    disabled on the DUT).                     #
#                                                                              #
#                                                                              #
#  DEFINITION        : This function disables peerTxCoherentIsRequired,        #
#                      peerRxCoherentIsRequired and peerCongruentIsRequired on #
#                      the DUT.                                                #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_tcr_rcr_cr_disable\                          #
#                             -session_handler  $session_handler\              #
#                             -port_num         $port_num                      #
#                                                                              #
################################################################################

proc ptp_ha::snmp_tcr_rcr_cr_disable {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_tcr_rcr_cr_disable : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_tcr_rcr_cr_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
       return 0
    }

    set result [ptp_ha::callback_snmp_tcr_rcr_cr_disable\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_TCR_RCR_CR_DISABLE"\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_TCR_RCR_CR_DISABLE"\
                        -parameter1             $port_num]} {
            return 0
        }
    }

    return $result
}


#15.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_l1sync_disable                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If L1Sync option is disabled on the       #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If L1Sync option could not be disabled on #
#                                    the specified port of the DUT).           #
#                                                                              #
#  DEFINITION        : This function disables L1Sync option on a specific port #
#                      at the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_l1sync_disable\                              #
#                             -session_handler $session_handler\               #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::snmp_l1sync_disable {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_l1sync_disable : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_l1sync_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

   set result [ptp_ha::callback_snmp_l1sync_disable\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_L1SYNC_DISABLE"\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_L1SYNC_DISABLE"\
                        -parameter1             $port_num]} {
            return 0
        }
    }

    return $result
}


#16.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_l1sync_opt_params_disable                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If extended format of L1Sync TLV is       #
#                                    disabled on the specified port at the DUT)#
#                                                                              #
#                      0 on Failure (If extended format of L1Sync TLV could not#
#                                    be disabled on the specified port at the  #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function disables extended format of L1Sync TLV on #
#                      the specified port at the DUT.                          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_l1sync_opt_params_disable\                   #
#                             -session_handler $session_handler\               #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::snmp_l1sync_opt_params_disable {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_l1sync_opt_params_disable : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_l1sync_opt_params_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

   set result [ptp_ha::callback_snmp_l1sync_opt_params_disable\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_L1SYNC_OPT_PARAMS_DISABLE"\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_L1SYNC_OPT_PARAMS_DISABLE"\
                        -parameter1             $port_num]} {
            return 0
        }
    }

    return $result
}


#17.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_slaveonly_disable                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If slave-only is disabled on the          #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If slave-only could not be disabled on the#
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function disables slave-only on the specified port #
#                      at the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_slaveonly_disable\                           #
#                             -session_handler $session_handler\               #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::snmp_slaveonly_disable {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_slaveonly_disable : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_slaveonly_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

   set result [ptp_ha::callback_snmp_slaveonly_disable\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_SLAVEONLY_DISABLE"\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_SLAVEONLY_DISABLE"\
                        -parameter1             $port_num]} {
            return 0
        }
    }

    return $result
}


#18.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_masteronly_disable                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If master-only is disabled on the         #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If master-only could not be disabled on   #
#                                    specified port at the DUT).               #
#                                                                              #
#  DEFINITION        : This function disables master-only on a specific port   #
#                      at the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_masteronly_disable\                          #
#                             -session_handler $session_handler\               #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::snmp_masteronly_disable {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_masteronly_disable : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_masteronly_disable : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

   set result [ptp_ha::callback_snmp_masteronly_disable\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_MASTERONLY_DISABLE"\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_MASTERONLY_DISABLE"\
                        -parameter1             $port_num]} {
            return 0
        }
    }

    return $result
}


#19.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::snmp_reset_announce_interval                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  announce_interval : (Mandatory) announceInterval.                           #
#                                  (e.g., 1|2|4|8|16)                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If announceInterval value is reset on     #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If announceInterval value could not be    #
#                                    reset on specified port of the DUT).      #
#                                                                              #
#  DEFINITION        : This function resets announceInterval value to a        #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_reset_announce_interval\                     #
#                             -session_handler     $session_handler\           #
#                             -port_num            $port_num\                  #
#                             -announce_interval   $announce_interval          #
#                                                                              #
################################################################################

proc ptp_ha::snmp_reset_announce_interval {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_announce_interval : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_announce_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-announce_interval)]} {
        set announce_interval $param(-announce_interval)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_announce_interval : Missing Argument    -> announce_interval"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

   set result [ptp_ha::callback_snmp_reset_announce_interval\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_ANNOUNCE_INTERVAL"\
                        -port_num               $port_num\
                        -announce_interval      $announce_interval]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_ANNOUNCE_INTERVAL"\
                        -parameter1             $port_num\
                        -parameter2             $announce_interval]} {
            return 0
        }
    }

    return $result
}


#20.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::snmp_reset_announce_timeout                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  announce_timeout  : (Mandatory) announceReceiptTimeout.                     #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If announceReceiptTimeout value is reset  #
#                                    on specified port of the DUT).            #
#                                                                              #
#                      0 on Failure (If announceReceiptTimeout value could not #
#                                    be reset on specified port of the DUT).   #
#                                                                              #
#  DEFINITION        : This function resets announceReceiptTimeout value to a  #
#                      specific port on the DUT.                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_reset_announce_timeout\                      #
#                             -session_handler    $session_handler\            #
#                             -port_num           $port_num\                   #
#                             -announce_timeout   $announce_timeout            #
#                                                                              #
################################################################################

proc ptp_ha::snmp_reset_announce_timeout {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_announce_timeout : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_announce_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-announce_timeout)]} {
        set announce_timeout $param(-announce_timeout)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_announce_timeout : Missing Argument    -> announce_timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

   set result [ptp_ha::callback_snmp_reset_announce_timeout\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_ANNOUNCE_TIMEOUT"\
                        -port_num               $port_num\
                        -announce_timeout       $announce_timeout]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_ANNOUNCE_TIMEOUT"\
                        -parameter1             $port_num\
                        -parameter2             $announce_timeout]} {
            return 0
        }
    }

    return $result
}


#21.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_reset_sync_interval                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  sync_interval     : (Mandatory) logSyncInterval                             #
#                                  (e.g., 3)                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If logSyncInterval value is reset on      #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If logSyncInterval value could not be     #
#                                    reset on specified port of the DUT).      #
#                                                                              #
#  DEFINITION        : This function resets logSyncInterval value to a specific#
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_reset_sync_interval\                         #
#                             -session_handler     $session_handler\           #
#                             -port_num            $port_num\                  #
#                             -sync_interval       $sync_interval              #
#                                                                              #
################################################################################

proc ptp_ha::snmp_reset_sync_interval {args} {

    array set param $args

    set missedArgCounter  0

 ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_sync_interval : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_sync_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-sync_interval)]} {
        set sync_interval $param(-sync_interval)
    } else {
        ERRLOG  -msg "ptp_ha::snmp_reset_sync_interval : Missing Argument    -> sync_interval"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

   set result [ptp_ha::callback_snmp_reset_sync_interval\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_SYNC_INTERVAL"\
                        -port_num               $port_num\
                        -sync_interval          $sync_interval]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_SYNC_INTERVAL"\
                        -parameter1             $port_num\
                        -parameter2             $sync_interval]} {
            return 0
        }
    }

    return $result
}


#22.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::snmp_reset_delayreq_interval                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  delayreq_interval : (Mandatory) MinDelayReqInterval.                        #
#                                  (e.g., 1|2|4|8|16)                          #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If MinDelayReqInterval is reset on        #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If MinDelayReqInterval could not be reset #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets the MinDelayReqInterval to the     #
#                      specific PTP port on the DUT.                           #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_reset_delayreq_interval\                     #
#                             -session_handler      $session_handler\          #
#                             -port_num             $port_num\                 #
#                             -delayreq_interval    $delayreq_interval         #
#                                                                              #
################################################################################

proc ptp_ha::snmp_reset_delayreq_interval {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_delayreq_interval : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
       set port_num $param(-port_num)
    } else {
       ERRLOG  -msg "ptp_ha::snmp_reset_delayreq_interval : Missing Argument    -> port_num"
       incr missedArgCounter
    }

    if {[info exists param(-delayreq_interval)]} {
       set delayreq_interval $param(-delayreq_interval)
    } else {
       ERRLOG  -msg "ptp_ha::snmp_reset_delayreq_interval : Missing Argument    -> delayreq_interval"
       incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
     }

   set result [ptp_ha::callback_snmp_reset_delayreq_interval\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_DELAYREQ_INTERVAL"\
                        -port_num               $port_num\
                        -delayreq_interval      $delayreq_interval]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_DELAYREQ_INTERVAL"\
                        -parameter1             $port_num\
                        -parameter2             $delayreq_interval]} {
            return 0
        }
    }

    return $result
}


#23.
################################################################################
#                                                                              #
#  PROCEDURE NAME    :  ptp_ha::snmp_reset_pdelayreq_interval                  #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  pdelayreq_interval: (Mandatory) minPdelayReqInterval.                       #
#                                  (e.g., 1|2|4|8|16).                         #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If minPdelayReqInterval is reset on       #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If minPdelayReqInterval could not be reset#
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets the minPdelayReqInterval to the    #
#                      specific PTP port on the DUT.                           #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_reset_pdelayreq_interval\                    #
#                             -session_handler      $session_handler\          #
#                             -port_num             $port_num\                 #
#                             -pdelayreq_interval   $pdelayreq_interval        #
#                                                                              #
################################################################################

proc ptp_ha::snmp_reset_pdelayreq_interval {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_pdelayreq_interval : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_pdelayreq_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-pdelayreq_interval)]} {
        set pdelayreq_interval $param(-pdelayreq_interval)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_pdelayreq_interval : Missing Argument    -> pdelayreq_interval"
        incr missedArgCounter
   }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_reset_pdelayreq_interval\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_PDELAYREQ_INTERVAL"\
                        -port_num                $port_num\
                        -pdelayreq_interval      $pdelayreq_interval]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_PDELAYREQ_INTERVAL"\
                        -parameter1             $port_num\
                        -parameter2             $pdelayreq_interval]} {
            return 0
        }
    }

    return $result
}


#24.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_reset_l1sync_interval                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  l1sync_interval   : (Mandatory) logL1SyncInterval.                          #
#                                  (e.g.,3 )                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If logL1SyncInterval is reset on specified#
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If logL1SyncInterval could not be reset on#
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function resets the logL1SyncInterval to a specific#
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_reset_l1sync_interval\                       #
#                             -session_handler     $session_handler\           #
#                             -port_num            $port_num\                  #
#                             -l1sync_interval     $l1sync_interval            #
#                                                                              #
################################################################################

proc ptp_ha::snmp_reset_l1sync_interval {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_l1sync_interval : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_l1sync_interval : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-l1sync_interval)]} {
        set l1sync_interval $param(-l1sync_interval)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_l1sync_interval : Missing Argument    -> l1sync_interval"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

   set result [ptp_ha::callback_snmp_reset_l1sync_interval\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_L1SYNC_INTERVAL"\
                        -port_num               $port_num\
                        -l1sync_interval        $l1sync_interval]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_L1SYNC_INTERVAL"\
                        -parameter1             $port_num\
                        -parameter2             $l1sync_interval]} {
            return 0
        }
    }

    return $result
}


#25.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_reset_l1sync_timeout                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  l1sync_timeout    : (Mandatory) L1SyncReceiptTimeout.                       #
#                                  (e.g., 3)                                   #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If L1SyncReceiptTimeout is reset on       #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If L1SyncReceiptTimeout could not be      #
#                                    reset on specified port of the DUT).      #
#                                                                              #
#  DEFINITION        : This function resets L1SyncReceiptTimeout to a specific #
#                      port on the DUT.                                        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_reset_l1sync_timeout\                        #
#                             -session_handler    $session_handler\            #
#                             -port_num           $port_num\                   #
#                             -l1sync_timeout     $l1sync_timeout              #
#                                                                              #
################################################################################

proc ptp_ha::snmp_reset_l1sync_timeout {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_l1sync_timeout : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_l1sync_timeout : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-l1sync_timeout)]} {
        set l1sync_timeout $param(-l1sync_timeout)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_l1sync_timeout : Missing Argument    -> l1sync_timeout"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

   set result [ptp_ha::callback_snmp_reset_l1sync_timeout\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_L1SYNC_TIMEOUT"\
                        -port_num               $port_num\
                        -l1sync_timeout         $l1sync_timeout]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_L1SYNC_TIMEOUT"\
                        -parameter1             $port_num\
                        -parameter2             $l1sync_timeout]} {
            return 0
        }
    }

    return $result
}


#26.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_reset_egress_latency                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Mandatory) timestampCorrectionPortDS.egressLatency.    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If timestampCorrectionPortDS.egressLatency#
#                                    is reset on specified port of the DUT).   #
#                                                                              #
#                      0 on Failure (If timestampCorrectionPortDS.egressLatency#
#                                    could not be reset on specified port of   #
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function resets timestampCorrectionPortDS.         #
#                      egressLatency to a specific port on the DUT.            #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_reset_egress_latency\                        #
#                             -session_handler       $session_handler\         #
#                             -port_num              $port_num\                #
#                             -latency               $latency                  #
#                                                                              #
################################################################################

proc ptp_ha::snmp_reset_egress_latency {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_egress_latency : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_egress_latency : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-latency)]} {
        set latency $param(-latency)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_egress_latency : Missing Argument    -> latency"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_reset_egress_latency\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_EGRESS_LATENCY"\
                        -port_num               $port_num\
                        -latency                $latency]

    if {$result == 2} {

        if {![Send_snmp_set_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_EGRESS_LATENCY"\
                        -parameter1             $port_num\
                        -parameter2             $latency]} {

            return 0
        }
    }

    return $result
}


#27.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_reset_ingress_latency                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Mandatory) timestampCorrectionPortDS.ingressLatency.   #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If timestampCorrectionPortDS.ingressLatency#
#                                    is reset on specified port of the DUT).   #
#                                                                              #
#                      0 on Failure (If timestampCorrectionPortDS.ingressLatency#
#                                    could not be reset on specified port of   #
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function resets timestampCorrectionPortDS.         #
#                      ingressLatency to a specific port on the DUT.           #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_reset_ingress_latency\                       #
#                             -session_handler       $session_handler\         #
#                             -port_num              $port_num\                #
#                             -latency               $latency                  #
#                                                                              #
################################################################################

proc ptp_ha::snmp_reset_ingress_latency {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_ingress_latency : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_ingress_latency : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-latency)]} {
        set latency $param(-latency)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_ingress_latency : Missing Argument    -> latency"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_reset_ingress_latency\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_INGRESS_LATENCY"\
                        -port_num               $port_num\
                        -latency                $latency]

    if {$result == 2} {

        if {![Send_snmp_set_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_INGRESS_LATENCY"\
                        -parameter1             $port_num\
                        -parameter2             $latency]} {

            return 0
        }
    }

    return $result
}


#28.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_reset_constant_asymmetry                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  constant_asymmetry :(Mandatory) asymmetryCorrectionPortDS.constantAsymmetry.#
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If asymmetryCorrectionPortDS.             #
#                                    constantAsymmetry is reset on specified   #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If asymmetryCorrectionPortDS.             #
#                                    constantAsymmetry could not be reset on   #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function resets asymmetryCorrectionPortDS.         #
#                      constantAsymmetry to a specific port on the DUT.        #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_reset_constant_asymmetry\                    #
#                             -session_handler       $session_handler\         #
#                             -port_num              $port_num\                #
#                             -constant_asymmetry    $constant_asymmetry       #
#                                                                              #
################################################################################

proc ptp_ha::snmp_reset_constant_asymmetry {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_constant_asymmetry : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_constant_asymmetry : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-constant_asymmetry)]} {
        set constant_asymmetry $param(-constant_asymmetry)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_constant_asymmetry : Missing Argument    -> constant_asymmetry"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_reset_constant_asymmetry\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_CONSTANT_ASYMMETRY"\
                        -port_num               $port_num\
                        -constant_asymmetry     $constant_asymmetry]

    if {$result == 2} {

        if {![Send_snmp_set_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_CONSTANT_ASYMMETRY"\
                        -parameter1             $port_num\
                        -parameter2             $constant_asymmetry]} {

            return 0
        }
    }

    return $result
}


#29.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_reset_scaled_delay_coefficient             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  delay_coefficient : (Mandatory) asymmetryCorrectionPortDS.                  #
#                                  scaledDelayCoefficient .                    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If asymmetryCorrectionPortDS.             #
#                                    scaledDelayCoefficient is reset on        #
#                                    specified port of the DUT).               #
#                                                                              #
#                      0 on Failure (If asymmetryCorrectionPortDS.             #
#                                    scaledDelayCoefficient could not be reset #
#                                    on specified port of the DUT).            #
#                                                                              #
#  DEFINITION        : This function resets asymmetryCorrectionPortDS.         #
#                      scaledDelayCoefficient to a specific port on the DUT.   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_reset_scaled_delay_coefficient\              #
#                             -session_handler       $session_handler\         #
#                             -port_num              $port_num\                #
#                             -delay_coefficient     $delay_coefficient        #
#                                                                              #
################################################################################

proc ptp_ha::snmp_reset_scaled_delay_coefficient {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_scaled_delay_coefficient : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_scaled_delay_coefficient : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-delay_coefficient)]} {
        set delay_coefficient $param(-delay_coefficient)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_scaled_delay_coefficient : Missing Argument    -> delay_coefficient"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_reset_scaled_delay_coefficient\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_SCALED_DELAY_COEFFICIENT"\
                        -port_num               $port_num\
                        -delay_coefficient      $delay_coefficient]

    if {$result == 2} {

        if {![Send_snmp_set_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_SCALED_DELAY_COEFFICIENT"\
                        -parameter1             $port_num\
                        -parameter2             $delay_coefficient]} {

            return 0
        }
    }

    return $result
}


#30.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_reset_asymmetry_correction                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If asymmetryCorrectionPortDS.enable is    #
#                                    reset on specified port of the DUT).      #
#                                                                              #
#                      0 on Failure (If asymmetryCorrectionPortDS.enable could #
#                                    not be reset on specified port of the DUT)#
#                                                                              #
#  DEFINITION        : This function resets the asymmetryCorrectionPortDS.enable#
#                      to the specific PTP port on the DUT.                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_reset_asymmetry_correction\                  #
#                             -session_handler       $session_handler\         #
#                             -port_num              $port_num                 #
#                                                                              #
################################################################################

proc ptp_ha::snmp_reset_asymmetry_correction {args} {

    array set param $args
    global env

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_asymmetry_correction : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_asymmetry_correction : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_reset_asymmetry_correction\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_ASYMMETRY_CORRECTION"\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_snmp_set_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_ASYMMETRY_CORRECTION"\
                        -parameter1             $port_num]} {

            return 0
        }
    }

    return $result
}


#31.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_external_port_config_disable               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If external port configuration option is  #
#                                    disabled on the specified port at the DUT)#
#                                                                              #
#                      0 on Failure (If external port configuration option     #
#                                    could not be disabled on the specified    #
#                                    port at the DUT).                         #
#                                                                              #
#  DEFINITION        : This function disables external port configuration      #
#                      option on a specific port at the DUT.                   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_external_port_config_disable\                #
#                             -session_handler $session_handler\               #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::snmp_external_port_config_disable {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_external_port_config_disable : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_external_port_config_disable: Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

   set result [ptp_ha::callback_snmp_external_port_config_disable\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_EXTERNAL_PORT_CONFIG_DISABLE"\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_EXTERNAL_PORT_CONFIG_DISABLE"\
                        -parameter1             $port_num]} {
            return 0
        }
    }

    return $result
}


#32.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_reset_epc_desired_state                    #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  desired_state     : (Mandatory) externalPortConfigurationPortDS.desiredState#
#                                  (e.g., "INITIALIZING"|"FAULTY"|"DISABLED"|  #
#                                         "LISTENING"|"PRE_MASTER"|"MASTER"|   #
#                                         "PASSIVE"|"UNCALIBRATED"|"SLAVE"     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given externalPortConfigurationPortDS. #
#                                    desiredState is reset on specified port of#
#                                    the DUT).                                 #
#                                                                              #
#                      0 on Failure (If given externalPortConfigurationPortDS. #
#                                    desiredState could not be reset on        #
#                                    specified port of the DUT).               #
#                                                                              #
#  DEFINITION        : This function resets given externalPortConfigurationPortDS#
#                      desiredState to a specific port on the DUT.             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_reset_epc_desired_state\                     #
#                             -session_handler       $session_handler\         #
#                             -port_num              $port_num\                #
#                             -desired_state         $desired_state            #
#                                                                              #
################################################################################

proc ptp_ha::snmp_reset_epc_desired_state {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_epc_desired_state : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_epc_desired_state : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-desired_state)]} {
        set desired_state $param(-desired_state)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_epc_desired_state : Missing Argument    -> desired_state"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_reset_epc_desired_state\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_EPC_DESIRED_STATE"\
                        -port_num               $port_num\
                        -desired_state          $desired_state]

    if {$result == 2} {

        if {![Send_snmp_set_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_EPC_DESIRED_STATE"\
                        -parameter1             $port_num\
                        -parameter2             $desired_state]} {

            return 0
        }
    }

    return $result
}


#33.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_reset_ipv4_address                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  ip_address        : (Mandatory) IPv4 address.                               #
#                                  (e.g., 192.168.200.1)                       #
#                                                                              #
#  net_mask          : (Mandatory) Subnet mask.                                #
#                                  (e.g., 255.255.255.0)                       #
#                                                                              #
#  vlan_id           : (Mandatory) VLAN ID.                                    #
#                                  (e.g., 100)                                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given IP address is reset on specified #
#                                    port of the DUT).                         #
#                                                                              #
#                      0 on Failure (If given IP address could not be reset on #
#                                    specifed port of the DUT).                #
#                                                                              #
#  DEFINITION        : This function resets given IP address to a specific port#
#                      on the DUT.                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_reset_ipv4_address\                          #
#                             -session_handler     $session_handler\           #
#                             -port_num            $port_num\                  #
#                             -ip_address          $ip_address\                #
#                             -net_mask            $net_mask\                  #
#                             -vlan_id             $vlan_id                    #
#                                                                              #
################################################################################

proc ptp_ha::snmp_reset_ipv4_address {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_ipv4_address : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_ipv4_address : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-ip_address)]} {
        set ip_address $param(-ip_address)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_ipv4_address : Missing Argument    -> ip_address"
        incr missedArgCounter
    }

    if {[info exists param(-net_mask)]} {
        set net_mask $param(-net_mask)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_ipv4_address : Missing Argument    -> net_mask"
        incr missedArgCounter
    }

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        ERRLOG -msg "ptp_ha::snmp_reset_ipv4_address : Missing Argument    -> vlan_id"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_reset_ipv4_address\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_RESET_IPV4_ADDRESS"\
                        -port_num               $port_num\
                        -ip_address             $ip_address\
                        -net_mask               $net_mask\
                        -vlan_id                $vlan_id]

    if {$result == 2} {

        if {![Send_snmp_set_command \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_RESET_IPV4_ADDRESS"\
                        -parameter1             $port_num\
                        -parameter2             $ip_address\
                        -parameter3             $net_mask\
                        -parameter4             $vlan_id]} {

            return 0
        }
    }

    return $result
}


#34.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_timestamp_corrected_tx_disable             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If L1SyncOptParamsPortDS.                 #
#                                    timestampsCorrectedTx option is disabled  #
#                                    on the specified port at the DUT).        #
#                                                                              #
#                      0 on Failure (If L1SyncOptParamsPortDS.                 #
#                                    timestampsCorrectedTx option could not be #
#                                    disabled on the specified port at the DUT)#
#                                                                              #
#  DEFINITION        : This function disables L1SyncOptParamsPortDS.           #
#                      timestampsCorrectedTx option on a specific port at the  #
#                      DUT.                                                    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_timestamp_corrected_tx_disable\              #
#                             -session_handler $session_handler\               #
#                             -port_num        $port_num                       #
#                                                                              #
################################################################################

proc ptp_ha::snmp_timestamp_corrected_tx_disable {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_timestamp_corrected_tx_disable : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_timestamp_corrected_tx_disable: Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    set result [ptp_ha::callback_snmp_timestamp_corrected_tx_disable\
                        -session_handler        $session_handler\
                        -cmd_type               "DUT_TIMESTAMP_CORRECTED_TX_DISABLE"\
                        -port_num               $port_num]

    if {$result == 2} {

        if {![Send_snmp_set_command  \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_TIMESTAMP_CORRECTED_TX_DISABLE"\
                        -parameter1             $port_num]} {
            return 0
        }
    }

    return $result
}


################################################################################
#                          SNMP CHECK FUNCTIONS                                #
################################################################################

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_check_l1sync_state                         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  l1sync_state      : (Mandatory) L1 Sync State.                              #
#                                  (e.g., "DISABLED"|"IDLE"|"LINK_ALIVE"|      #
#                                         "CONFIG_MATCH"|"L1_SYNC_UP")         #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given L1 Sync State is verified in the #
#                                    specified port).                          #
#                                                                              #
#                      0 on Failure (If given L1 Sync State is not verified in #
#                                    the specified port).                      #
#                                                                              #
#  DEFINITION        : This function verifies the given L1 Sync State in a     #
#                      specific port.                                          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_check_l1sync_state\                          #
#                             -session_handler    $session_handler\            #
#                             -port_num           $port_num\                   #
#                             -l1sync_state       $l1sync_state                #
#                                                                              #
################################################################################

proc ptp_ha::snmp_check_l1sync_state {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_port_ptp_enable : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_check_l1sync_state : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-l1sync_state)]} {
        set l1sync_state $param(-l1sync_state)
    } else {
        ERRLOG -msg "ptp_ha::snmp_check_l1sync_state : Missing Argument    -> l1sync_state"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {[info exists ::PTP_HA_DUT_CHECK_RETRY_LIMIT]} {
        set ::PTP_HA_DUT_CHECK_RETRY_LIMIT  $::PTP_HA_DUT_CHECK_RETRY_LIMIT
    } else {
        set ::PTP_HA_DUT_CHECK_RETRY_LIMIT  1
    }

    set i 0

    while { $i < $::PTP_HA_DUT_CHECK_RETRY_LIMIT} {

        incr i

        if {![Send_snmp_command_to_read \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_CHECK_L1SYNC_STATE"\
                        -parameter1             $port_num\
                        -parameter2             $l1sync_state\
                        -output                 buffer]} {

            LOG -level 4 -msg "ptp_ha::snmp_check_l1sync_state: Error while checking L1 Sync State"
            return 0
        }

        set result [callback_snmp_check_l1sync_state\
                        -session_handler        $session_handler\
                        -buffer                 $buffer\
                        -cmd_type               "DUT_CHECK_L1SYNC_STATE"\
                        -parameter1             $port_num\
                        -parameter2             $l1sync_state]

        if { $result == 1 } {
            return 1
        }
    }

    return 0
}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_check_ptp_port_state                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  port_state        : (Mandatory) portState.                                  #
#                                  (e.g., "INITIALIZING"|"FAULTY"|"DISABLED"|  #
#                                         "LISTENING"|"PRE_MASTER"|"MASTER"|   #
#                                         "PASSIVE"|"UNCALIBRATED"|"SLAVE"     #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given portState is verified in the     #
#                                    specified port).                          #
#                                                                              #
#                      0 on Failure (If given portState is not verified in the #
#                                    specified port).                          #
#                                                                              #
#  DEFINITION        : This function verifies the given portState in a specific#
#                      port.                                                   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_check_ptp_port_state\                        #
#                             -session_handler  $session_handler\              #
#                             -port_num         $port_num\                     #
#                             -port_state       $port_state                    #
#                                                                              #
################################################################################

proc ptp_ha::snmp_check_ptp_port_state {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_port_ptp_enable : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_check_ptp_port_state : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-port_state)]} {
        set port_state $param(-port_state)
    } else {
        ERRLOG -msg "ptp_ha::snmp_check_ptp_port_state : Missing Argument    -> port_state"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {[info exists ::PTP_HA_DUT_CHECK_RETRY_LIMIT]} {
        set ::PTP_HA_DUT_CHECK_RETRY_LIMIT  $::PTP_HA_DUT_CHECK_RETRY_LIMIT
    } else {
        set ::PTP_HA_DUT_CHECK_RETRY_LIMIT  1
    }

    set i 0

    while { $i < $::PTP_HA_DUT_CHECK_RETRY_LIMIT} {

        incr i

        if {![Send_snmp_command_to_read \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_CHECK_PTP_PORT_STATE"\
                        -parameter1             $port_num\
                        -parameter2             $port_state\
                        -output                 buffer]} {

            LOG -level 4 -msg "ptp_ha::snmp_check_ptp_port_state : Error while checking portState"
            return 0
        }

        set result [callback_snmp_check_ptp_port_state\
                        -session_handler        $session_handler\
                        -buffer                 $buffer\
                        -cmd_type               "DUT_CHECK_PTP_PORT_STATE"\
                        -parameter1             $port_num\
                        -parameter2             $port_state]

        if { $result == 1 } {
            return 1
        }
    }

    return 0
}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_check_asymmetry_correction                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  state             : (Mandatory) asymmetryCorrectionPortDS.enable.           #
#                                  (e.g., "ENABLED"|"DISABLED"                 #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given state of                         #
#                                    asymmetryCorrectionPortDS.enable is       #
#                                    verified for the specified port).         #
#                                                                              #
#                      0 on Failure (If given state of                         #
#                                    asymmetryCorrectionPortDS.enable could not#
#                                    be verified for the specified port).      #
#                                                                              #
#  DEFINITION        : This function verifies the given state of               #
#                      asymmetryCorrectionPortDS.enable for a specific port.   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_check_asymmetry_correction\                  #
#                             -session_handler    $session_handler\            #
#                             -port_num           $port_num\                   #
#                             -state              $state                       #
#                                                                              #
################################################################################

proc ptp_ha::snmp_check_asymmetry_correction {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_check_asymmetry_correction : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_check_asymmetry_correction : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-state)]} {
        set state $param(-state)
    } else {
        ERRLOG -msg "ptp_ha::snmp_check_asymmetry_correction : Missing Argument    -> state"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {[info exists ::PTP_HA_DUT_CHECK_RETRY_LIMIT]} {
        set ::PTP_HA_DUT_CHECK_RETRY_LIMIT  $::PTP_HA_DUT_CHECK_RETRY_LIMIT
    } else {
        set ::PTP_HA_DUT_CHECK_RETRY_LIMIT  1
    }

    set i 0

    while { $i < $::PTP_HA_DUT_CHECK_RETRY_LIMIT} {

        incr i

        if {![Send_snmp_command_to_read \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_CHECK_ASYMMETRY_CORRECTION"\
                        -parameter1             $port_num\
                        -parameter2             $state\
                        -output                 buffer]} {

            LOG -level 4 -msg "ptp_ha::snmp_check_asymmetry_correction : Error \
                              while checking asymmetryCorrectionPortDS.enable"

            return 0
        }

        set result [callback_snmp_check_asymmetry_correction\
                        -session_handler        $session_handler\
                        -buffer                 $buffer\
                        -cmd_type               "DUT_CHECK_ASYMMETRY_CORRECTION"\
                        -parameter1             $port_num\
                        -parameter2             $state]

        if { $result == 1 } {
            return 1
        }
    }

    return 0
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_check_egress_latency                       #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Mandatory) timestampCorrectionPortDS.egressLatency.    #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of                         #
#                                    timestampCorrectionPortDS.egressLatency   #
#                                    verified for the specified port).         #
#                                                                              #
#                      0 on Failure (If given value of                         #
#                                    timestampCorrectionPortDS.egressLatency   #
#                                    could not be verified for the specified   #
#                                    port).                                    #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      timestampCorrectionPortDS.egressLatency for a specific  #
#                      port.                                                   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_check_egress_latency\                        #
#                             -session_handler       $session_handler\         #
#                             -port_num              $port_num\                #
#                             -latency               $latency                  #
#                                                                              #
################################################################################

proc ptp_ha::snmp_check_egress_latency {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_check_egress_latency : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_check_egress_latency : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-latency)]} {
        set latency $param(-latency)
    } else {
        ERRLOG -msg "ptp_ha::snmp_check_egress_latency : Missing Argument    -> latency"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {[info exists ::PTP_HA_DUT_CHECK_RETRY_LIMIT]} {
        set ::PTP_HA_DUT_CHECK_RETRY_LIMIT  $::PTP_HA_DUT_CHECK_RETRY_LIMIT
    } else {
        set ::PTP_HA_DUT_CHECK_RETRY_LIMIT  1
    }

    set i 0

    while { $i < $::PTP_HA_DUT_CHECK_RETRY_LIMIT} {

        incr i

        if {![Send_snmp_command_to_read \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_CHECK_EGRESS_LATENCY"\
                        -parameter1             $port_num\
                        -parameter2             $latency\
                        -output                 buffer]} {

            LOG -level 4 -msg "ptp_ha::snmp_check_egress_latency : Error \
                              while checking timestampCorrectionPortDS.egressLatency"

            return 0
        }

        set result [callback_snmp_check_egress_latency\
                        -session_handler        $session_handler\
                        -buffer                 $buffer\
                        -cmd_type               "DUT_CHECK_EGRESS_LATENCY"\
                        -parameter1             $port_num\
                        -parameter2             $latency]

        if { $result == 1 } {
            return 1
        }
    }

    return 0
}


#5.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_check_ingress_latency                      #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  latency           : (Mandatory) timestampCorrectionPortDS.ingressLatency.   #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of                         #
#                                    timestampCorrectionPortDS.ingressLatency  #
#                                    verified for the specified port).         #
#                                                                              #
#                      0 on Failure (If given value of                         #
#                                    timestampCorrectionPortDS.ingressLatency  #
#                                    could not be verified for the specified   #
#                                    port).                                    #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      timestampCorrectionPortDS.ingressLatency for a specific #
#                      port.                                                   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_check_ingress_latency\                       #
#                             -session_handler       $session_handler\         #
#                             -port_num              $port_num\                #
#                             -latency               $latency                  #
#                                                                              #
################################################################################

proc ptp_ha::snmp_check_ingress_latency {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_check_ingress_latency : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_check_ingress_latency : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-latency)]} {
        set latency $param(-latency)
    } else {
        ERRLOG -msg "ptp_ha::snmp_check_ingress_latency : Missing Argument    -> latency"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {[info exists ::PTP_HA_DUT_CHECK_RETRY_LIMIT]} {
        set ::PTP_HA_DUT_CHECK_RETRY_LIMIT  $::PTP_HA_DUT_CHECK_RETRY_LIMIT
    } else {
        set ::PTP_HA_DUT_CHECK_RETRY_LIMIT  1
    }

    set i 0

    while { $i < $::PTP_HA_DUT_CHECK_RETRY_LIMIT} {

        incr i

        if {![Send_snmp_command_to_read \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_CHECK_INGRESS_LATENCY"\
                        -parameter1             $port_num\
                        -parameter2             $latency\
                        -output                 buffer]} {

            LOG -level 4 -msg "ptp_ha::snmp_check_ingress_latency : Error \
                              while checking timestampCorrectionPortDS.ingressLatency"

            return 0
        }

        set result [callback_snmp_check_ingress_latency\
                        -session_handler        $session_handler\
                        -buffer                 $buffer\
                        -cmd_type               "DUT_CHECK_INGRESS_LATENCY"\
                        -parameter1             $port_num\
                        -parameter2             $latency]

        if { $result == 1 } {
            return 1
        }
    }

    return 0
}


#6.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_check_constant_asymmetry                   #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  constant_asymmetry: (Mandatory) asymmetryCorrectionPortDS.constantAsymmetry.#
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of                         #
#                                    asymmetryCorrectionPortDS.                #
#                                    constantAsymmetry verified for the        #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If given value of                         #
#                                    asymmetryCorrectionPortDS.                #
#                                    constantAsymmetry could not be verified   #
#                                    for specified port at the DUT).           #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      asymmetryCorrectionPortDS.constantAsymmetry for a       #
#                      specific port.                                          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_check_constant_asymmetry\                    #
#                             -session_handler       $session_handler\         #
#                             -port_num              $port_num\                #
#                             -constant_asymmetry    $constant_asymmetry       #
#                                                                              #
################################################################################

proc ptp_ha::snmp_check_constant_asymmetry {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_check_constant_asymmetry : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_check_constant_asymmetry : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    ### Checking Output Parameters ###

    if {[info exists param(-constant_asymmetry)]} {
        set constant_asymmetry $param(-constant_asymmetry)
    } else {
        ERRLOG -msg "ptp_ha::snmp_check_constant_asymmetry : Missing Argument    -> constant_asymmetry"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {[info exists ::PTP_HA_DUT_CHECK_RETRY_LIMIT]} {
        set ::PTP_HA_DUT_CHECK_RETRY_LIMIT  $::PTP_HA_DUT_CHECK_RETRY_LIMIT
    } else {
        set ::PTP_HA_DUT_CHECK_RETRY_LIMIT  1
    }

    set i 0

    while { $i < $::PTP_HA_DUT_CHECK_RETRY_LIMIT} {

        incr i

        if {![Send_snmp_command_to_read \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_CHECK_CONSTANT_ASYMMETRY"\
                        -parameter1             $port_num\
                        -parameter2             $constant_asymmetry\
                        -output                 buffer]} {

            LOG -level 4 -msg "ptp_ha::snmp_check_constant_asymmetry : Error \
                              while checking asymmetryCorrectionPortDS.constantAsymmetry"

            return 0
        }

        set result [callback_snmp_check_constant_asymmetry\
                        -session_handler        $session_handler\
                        -buffer                 $buffer\
                        -cmd_type               "DUT_CHECK_CONSTANT_ASYMMETRY"\
                        -parameter1             $port_num\
                        -parameter2             $constant_asymmetry]

        if { $result == 1 } {
            return 1
        }
    }

    return 0
}


#7.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_check_scaled_delay_coefficient             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  delay_coefficient : (Mandatory) asymmetryCorrectionPortDS.                  #
#                                  scaledDelayCoefficient.                     #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of                         #
#                                    asymmetryCorrectionPortDS.                #
#                                    scaledDelayCoefficient verified for the   #
#                                    specified port at the DUT).               #
#                                                                              #
#                      0 on Failure (If given value of                         #
#                                    asymmetryCorrectionPortDS.                #
#                                    scaledDelayCoefficient could not be       #
#                                    verified for specified port at the DUT).  #
#                                                                              #
#  DEFINITION        : This function verifies the given value of               #
#                      asymmetryCorrectionPortDS.scaledDelayCoefficient for a  #
#                      specific port.                                          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_check_scaled_delay_coefficient\              #
#                             -session_handler       $session_handler\         #
#                             -port_num              $port_num\                #
#                             -delay_coefficient     $delay_coefficient        #
#                                                                              #
################################################################################

proc ptp_ha::snmp_check_scaled_delay_coefficient {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_check_scaled_delay_coefficient : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_check_scaled_delay_coefficient : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-delay_coefficient)]} {
        set delay_coefficient $param(-delay_coefficient)
    } else {
        ERRLOG -msg "ptp_ha::snmp_check_scaled_delay_coefficient : Missing Argument    -> delay_coefficient"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {[info exists ::PTP_HA_DUT_CHECK_RETRY_LIMIT]} {
        set ::PTP_HA_DUT_CHECK_RETRY_LIMIT  $::PTP_HA_DUT_CHECK_RETRY_LIMIT
    } else {
        set ::PTP_HA_DUT_CHECK_RETRY_LIMIT  1
    }

    set i 0

    while { $i < $::PTP_HA_DUT_CHECK_RETRY_LIMIT} {

        incr i

        if {![Send_snmp_command_to_read \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_CHECK_SCALED_DELAY_COEFFICIENT"\
                        -parameter1             $port_num\
                        -parameter2             $delay_coefficient\
                        -output                 buffer]} {

            LOG -level 4 -msg "ptp_ha::snmp_check_scaled_delay_coefficient : Error \
                              while checking asymmetryCorrectionPortDS.scaledDelayCoefficient"

            return 0
        }

        set result [callback_snmp_check_scaled_delay_coefficient \
                        -session_handler        $session_handler\
                        -buffer                 $buffer\
                        -cmd_type               "DUT_CHECK_SCALED_DELAY_COEFFICIENT"\
                        -parameter1             $port_num\
                        -parameter2             $delay_coefficient]

        if { $result == 1 } {
            return 1
        }
    }

    return 0
}


#8.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_check_external_port_config                 #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  state             : (Mandatory) State of external port configuration.       #
#                                  (e.g., "ENABLED"|"DISABLED")                #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given state of external port           #
#                                    configuration is verified for the         #
#                                    specified port).                          #
#                                                                              #
#                      0 on Failure (If given state of external port           #
#                                    configuration could not be verified for   #
#                                    the specified port).                      #
#                                                                              #
#  DEFINITION        : This function verifies the given state of external      #
#                      port configuration for a specific port.                 #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_check_external_port_config\                  #
#                             -session_handler    $session_handler\            #
#                             -port_num           $port_num\                   #
#                             -state              $state                       #
#                                                                              #
################################################################################

proc ptp_ha::snmp_check_external_port_config {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_check_external_port_config : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_check_external_port_config : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-state)]} {
        set state $param(-state)
    } else {
        ERRLOG -msg "ptp_ha::snmp_check_external_port_config : Missing Argument    -> state"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {[info exists ::PTP_HA_DUT_CHECK_RETRY_LIMIT]} {
        set ::PTP_HA_DUT_CHECK_RETRY_LIMIT  $::PTP_HA_DUT_CHECK_RETRY_LIMIT
    } else {
        set ::PTP_HA_DUT_CHECK_RETRY_LIMIT  1
    }

    set i 0

    while { $i < $::PTP_HA_DUT_CHECK_RETRY_LIMIT} {

        incr i

        if {![Send_snmp_command_to_read \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_CHECK_EXTERNAL_PORT_CONFIG"\
                        -parameter1             $port_num\
                        -parameter2             $state\
                        -output                 buffer]} {

            LOG -level 4 -msg "ptp_ha::snmp_check_external_port_config : Error \
                              while checking the state of external port configuration"

            return 0
        }

        set result [callback_snmp_check_external_port_config\
                        -session_handler        $session_handler\
                        -buffer                 $buffer\
                        -cmd_type               "DUT_CHECK_EXTERNAL_PORT_CONFIG"\
                        -parameter1             $port_num\
                        -parameter2             $state]

        if { $result == 1 } {
            return 1
        }
    }

    return 0
}


#9.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_check_slave_only                           #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  value             : (Mandatory) Value of defaultDS.slaveOnly                #
#                                  (e.g., "TRUE"|"FALSE")                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of defaultDS.slaveOnly     #
#                                    is verified at the DUT).                  #
#                                                                              #
#                      0 on Failure (If given value of defaultDS.slaveOnly     #
#                                    could not be verified at the DUT).        #
#                                                                              #
#  DEFINITION        : This function verifies the given value of defaultDS.    #
#                      slaveOnly at the DUT.                                   #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_check_slave_only\                            #
#                             -session_handler    $session_handler\            #
#                             -value              $value                       #
#                                                                              #
################################################################################

proc ptp_ha::snmp_check_slave_only {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_check_slave_only : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-value)]} {
        set value $param(-value)
    } else {
        ERRLOG -msg "ptp_ha::snmp_check_slave_only : Missing Argument    -> value"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {[info exists ::PTP_HA_DUT_CHECK_RETRY_LIMIT]} {
        set ::PTP_HA_DUT_CHECK_RETRY_LIMIT  $::PTP_HA_DUT_CHECK_RETRY_LIMIT
    } else {
        set ::PTP_HA_DUT_CHECK_RETRY_LIMIT  1
    }

    set i 0

    while { $i < $::PTP_HA_DUT_CHECK_RETRY_LIMIT} {

        incr i

        if {![Send_snmp_command_to_read \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_CHECK_SLAVE_ONLY"\
                        -parameter1             $value\
                        -output                 buffer]} {

            LOG -level 4 -msg "ptp_ha::snmp_check_slave_only : Error \
                               while checking the value of defaultDS.slaveOnly"

            return 0
        }

        set result [callback_snmp_check_slave_only\
                        -session_handler        $session_handler\
                        -buffer                 $buffer\
                        -cmd_type               "DUT_CHECK_SLAVE_ONLY"\
                        -value                  $value]

        if { $result == 1 } {
            return 1
        }
    }

    return 0
}


#10.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_check_master_only                          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  value             : (Mandatory) portDS.masterOnly                           #
#                                  (e.g., "TRUE"|"FALSE")                      #
#                                                                              #
#  OUTPUT PARAMETERS : NONE.                                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If given value of portDS.masterOnly       #
#                                    is verified for the specified port).      #
#                                                                              #
#                      0 on Failure (If given value of portDS.masterOnly       #
#                                    could not be verified for the specified   #
#                                    port).                                    #
#                                                                              #
#  DEFINITION        : This function verifies the given value of portDS.       #
#                      masterOnly for a specific port.                         #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_check_master_only\                           #
#                             -session_handler    $session_handler\            #
#                             -port_num           $port_num\                   #
#                             -value              $value                       #
#                                                                              #
################################################################################

proc ptp_ha::snmp_check_master_only {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_check_master_only : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_check_master_only : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    if {[info exists param(-value)]} {
        set value $param(-value)
    } else {
        ERRLOG -msg "ptp_ha::snmp_check_master_only : Missing Argument    -> value"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {[info exists ::PTP_HA_DUT_CHECK_RETRY_LIMIT]} {
        set ::PTP_HA_DUT_CHECK_RETRY_LIMIT  $::PTP_HA_DUT_CHECK_RETRY_LIMIT
    } else {
        set ::PTP_HA_DUT_CHECK_RETRY_LIMIT  1
    }

    set i 0

    while { $i < $::PTP_HA_DUT_CHECK_RETRY_LIMIT} {

        incr i

        if {![Send_snmp_command_to_read \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_CHECK_MASTER_ONLY"\
                        -parameter1             $port_num\
                        -parameter2             $value\
                        -output                 buffer]} {

            LOG -level 4 -msg "ptp_ha::snmp_check_master_only : Error \
                               while checking the value of portDS.masterOnly"

            return 0
        }

        set result [callback_snmp_check_master_only\
                        -session_handler        $session_handler\
                        -buffer                 $buffer\
                        -port_num               $port_num\
                        -cmd_type               "DUT_CHECK_MASTER_ONLY"\
                        -value                  $value]

        if { $result == 1 } {
            return 1
        }
    }

    return 0
}


#11.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_check_for_least_offset_from_master         #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  offset_from_master: (Mandatory) currentDS.offsetFromMaster.                 #
#                                  (e.g., 3)                                   #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  RETURNS           : 1 on Success (If the absolute value of currentDS.       #
#                                    offsetFromMaster in DUT becomes lesser    #
#                                    than the specified value to ensure that   #
#                                    the DUT synchronized it's time with       #
#                                    messages from TEE).                       #
#                                                                              #
#                      0 on Failure (If the absolute value of currentDS.       #
#                                    offsetFromMaster in DUT does not become   #
#                                    lesser than the specified value to ensure #
#                                    that the DUT synchronized it's time with  #
#                                    messages from TEE).                       #
#                                                                              #
#  DEFINITION        : This function invokes recursive procedure to check      #
#                      whether the absolute value of currentDS.offsetFromMaster#
#                      is lesser than the specified value to ensure that DUT   #
#                      synchronized it's time with messages from TEE.          #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_check_for_least_offset_from_master\          #
#                          -offset_from_master    offset_from_master           #
#                                                                              #
################################################################################

proc ptp_ha::snmp_check_for_least_offset_from_master {args} {

    array set param $args
    global env

    set config_mode      $env(DUT_CONFIG_MODE)
    set session_handler  $::dut_session_id

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-offset_from_master)]} {
        upvar $param(-offset_from_master) offset_from_master
    } else {
        ERRLOG -msg "ptp_ha::snmp_check_for_least_offset_from_master : Missing Argument    -> offset_from_master"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {[info exists ::PTP_HA_DUT_CHECK_RETRY_LIMIT]} {
        set ::PTP_HA_DUT_CHECK_RETRY_LIMIT  $::PTP_HA_DUT_CHECK_RETRY_LIMIT
    } else {
        set ::PTP_HA_DUT_CHECK_RETRY_LIMIT  1
    }

    set i 0

    while { $i < $::PTP_HA_DUT_CHECK_RETRY_LIMIT} {

        incr i

        if {![Send_snmp_command_to_read \
                        -session_handler        $session_handler \
                        -cmd_type               "DUT_CHECK_OFFSET_FROM_MASTER"\
                        -parameter1             $offset_from_master\
                        -output                 buffer]} {

            LOG -level 4 -msg "ptp_ha::snmp_check_for_least_offset_from_master: Error\
                               while checking the absolute value of currentDS.offsetFromMaster"

            return 0
        }

        set result [callback_snmp_check_for_least_offset_from_master\
                        -session_handler        $session_handler\
                        -buffer                 $buffer\
                        -cmd_type               "DUT_CHECK_OFFSET_FROM_MASTER"\
                        -offset_from_master     $offset_from_master]

        if { $result == 1 } {
            return 1
        }
    }

    return 0
}


################################################################################
#                          SNMP GET FUNCTIONS                                  #
################################################################################

#1.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_get_timestamp                              #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  timestamp         : (Mandatory) Timestamp.                                  #
#                                  (e.g., xxxxx)                               #
#                                                                              #
#                                                                              #
#  RETURNS           : 1 on Success (If able to get timestamp from DUT).       #
#                                                                              #
#                      0 on Failure (If could not able to get timestamp from   #
#                                    DUT).                                     #
#                                                                              #
#  DEFINITION        : This function fetch timestamp from DUT.                 #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_get_timestamp\                               #
#                             -session_handler $session_handler\               #
#                             -timestamp       timestamp                       #
#                                                                              #
################################################################################

proc ptp_ha::snmp_get_timestamp {args} {

    array set param $args

    set missedArgCounter 0

### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_port_ptp_enable : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-timestamp)]} {
        upvar $param(-timestamp) timestamp
    } else {
        ERRLOG -msg "ptp_ha::snmp_get_timestamp : Missing Argument    -> timestamp"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {![Send_snmp_command_to_read \
                    -session_handler        $session_handler \
                    -cmd_type               "DUT_GET_TIMESTAMP"\
                    -output                 buffer]} {

        LOG -level 4 -msg "ptp_ha::snmp_get_timestamp : Error while getting timestamp"
        return 0
    }

    set result [callback_snmp_get_timestamp\
                    -session_handler        $session_handler\
                    -buffer                 $buffer\
                    -cmd_type               "DUT_GET_TIMESTAMP"\
                    -timestamp              timestamp]

    return $result
}


#2.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_get_mean_delay                             #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  mean_delay        : (Mandatory) currentDS.meanDelay.                        #
#                                  (e.g., 3)                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If able to extract currentDS.meanDelay of #
#                                    specified port from the DUT).             #
#                                                                              #
#                      0 on Failure (If could not able to extract              #
#                                    currentDS.meanDelay of specified port from#
#                                    the DUT).                                 #
#                                                                              #
#  DEFINITION        : This function extracts currentDS.meanDelay of a specific#
#                      port from the DUT.                                      #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_get_mean_delay\                              #
#                             -session_handler       $session_handler\         #
#                             -port_num              $port_num\                #
#                             -mean_delay            mean_delay                #
#                                                                              #
################################################################################

proc ptp_ha::snmp_get_mean_delay {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_get_mean_delay : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_get_mean_delay : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    ### Checking Output Parameters ###

    if {[info exists param(-mean_delay)]} {
        upvar $param(-mean_delay) mean_delay
    } else {
        ERRLOG -msg "ptp_ha::snmp_get_mean_delay : Missing Argument    -> mean_delay"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {![Send_snmp_command_to_read \
                    -session_handler        $session_handler \
                    -port_num               $port_num\
                    -cmd_type               "DUT_GET_MEAN_DELAY"\
                    -output                 buffer]} {

        LOG -level 4 -msg "ptp_ha::snmp_get_mean_delay : Error while \
                          getting currentDS.meanDelay"

        return 0
    }

    set result [callback_snmp_get_mean_delay\
                    -session_handler        $session_handler\
                    -port_num               $port_num\
                    -buffer                 $buffer\
                    -cmd_type               "DUT_GET_MEAN_DELAY"\
                    -mean_delay             mean_delay]

    return $result
}


#3.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_get_mean_link_delay                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  mean_link_delay   : (Mandatory) portDS.meanLinkDelay.                       #
#                                  (e.g., 3)                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If able to extract portDS.meanLinkDelay of#
#                                    specified port from the DUT).             #
#                                                                              #
#                      0 on Failure (If could not able to extract              #
#                                    portDS.meanLinkDelay of specified port    #
#                                    from the DUT).                            #
#                                                                              #
#  DEFINITION        : This function extracts portDS.meanLinkDelay of a        #
#                      specific port from the DUT.                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_get_mean_link_delay\                         #
#                             -session_handler       $session_handler\         #
#                             -port_num              $port_num\                #
#                             -mean_link_delay       mean_link_delay           #
#                                                                              #
################################################################################

proc ptp_ha::snmp_get_mean_link_delay {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_get_mean_link_delay : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_get_mean_link_delay : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    ### Checking Output Parameters ###

    if {[info exists param(-mean_link_delay)]} {
        upvar $param(-mean_link_delay) mean_link_delay
    } else {
        ERRLOG -msg "ptp_ha::snmp_get_mean_link_delay : Missing Argument    -> mean_link_delay"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {![Send_snmp_command_to_read \
                    -session_handler        $session_handler \
                    -port_num               $port_num\
                    -cmd_type               "DUT_GET_MEAN_LINK_DELAY"\
                    -output                 buffer]} {

        LOG -level 4 -msg "ptp_ha::snmp_get_mean_link_delay : Error while \
                          getting portDS.meanLinkDelay"

        return 0
    }

    set result [callback_snmp_get_mean_delay\
                    -session_handler        $session_handler\
                    -port_num               $port_num\
                    -buffer                 $buffer\
                    -cmd_type               "DUT_GET_MEAN_LINK_DELAY"\
                    -mean_link_delay        mean_link_delay]

    return $result
}


#4.
################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::snmp_get_delay_asymmetry                        #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_handler   : (Mandatory) Session ID of the DUT Session opened.       #
#                                                                              #
#  port_num          : (Mandatory) DUT's port number.                          #
#                                  (e.g., fa0/0)                               #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  delay_asymmetry   : (Mandatory) portDS.delayAsymmetry.                      #
#                                  (e.g., 3)                                   #
#                                                                              #
#  RETURNS           : 1 on Success (If able to extract portDS.delayAsymmetry  #
#                                    of specified port from the DUT).          #
#                                                                              #
#                      0 on Failure (If could not able to extract              #
#                                    portDS.delayAsymmetry of specified port   #
#                                    from the DUT).                            #
#                                                                              #
#  DEFINITION        : This function extracts portDS.delayAsymmetry of a       #
#                      specific port from the DUT.                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                    ptp_ha::snmp_get_delay_asymmetry\                         #
#                          -port_num              $port_num\                   #
#                          -delay_asymmetry       delay_asymmetry              #
#                                                                              #
################################################################################

proc ptp_ha::snmp_get_delay_asymmetry {args} {

    array set param $args
    global env

    set missedArgCounter 0

    ### Checking Input Parameters ###

    if {[info exists param(-session_handler)]} {
        set session_handler $param(-session_handler)
    } else {
        ERRLOG -msg "ptp_ha::snmp_get_delay_asymmetry : Missing Argument    -> session_handler"
        incr missedArgCounter
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::snmp_get_delay_asymmetry : Missing Argument    -> port_num"
        incr missedArgCounter
    }

    ### Checking Output Parameters ###

    if {[info exists param(-delay_asymmetry)]} {
        upvar $param(-delay_asymmetry) delay_asymmetry
    } else {
        ERRLOG -msg "ptp_ha::snmp_get_delay_asymmetry : Missing Argument    -> delay_asymmetry"
        incr missedArgCounter
    }

    if { $missedArgCounter != 0 } {
        return 0
    }

    if {![Send_snmp_command_to_read \
                    -session_handler        $session_handler \
                    -port_num               $port_num\
                    -cmd_type               "DUT_GET_DELAY_ASYMMETRY"\
                    -output                 buffer]} {

        LOG -level 4 -msg "ptp_ha::snmp_get_delay_asymmetry : Error while getting portDS.delayAsymmetry"
        return 0
    }

    set result [callback_snmp_get_delay_asymmetry\
                    -session_handler        $session_handler\
                    -port_num               $port_num\
                    -buffer                 $buffer\
                    -cmd_type               "DUT_GET_DELAY_ASYMMETRY"\
                    -delay_asymmetry        delay_asymmetry]

    return $result
}
