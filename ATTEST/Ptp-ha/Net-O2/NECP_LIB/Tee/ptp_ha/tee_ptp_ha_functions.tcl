################################################################################
# File Name         : tee_ptp_ha_functions.tcl                                 #
# File Version      : 1.11                                                     #
# Component Name    : ATTEST TEST EXECUTION ENGINE (TEE)                       #
# Module Name       : Precision Time Protocol - High Accuracy                  #
################################################################################
# History       Date       Author        Addition/ Alteration                  #
################################################################################
#                                                                              #
#  1.0       May/2018      CERN          Initial                               #
#  1.1       Jun/2018      CERN          Changed default value for messageType #
#                                        in ptp_ha::send_mgmt to 13            #
#  1.2       Jul/2018      CERN          Added source mac validation in        #
#                                        ptp_ha:recv_signal                    #
#  1.3       Sep/2018      CERN          a) Added support for error_reason in  #
#                                           all ptp_ha:recv functions          #
#                                        b) Added support to get values of all #
#                                           the fields of the received ptp_ha  #
#                                           packet                             #
#                                        c) Changed timeout in recv functions  #
#                                           according to standard              #
#                                        d) Added support to take clockIdentity#
#                                           dynamically.                       #
#  1.4       Oct/2018      CERN          Modified default value of twoStepFlag #
#                                        in Sync according to the configuration#
#                                        of clock step in device.              #
#  1.5       Nov/2018      CERN          a) Support added to respond Delay_Req #
#                                           and Pdelay_Req messages            #
#                                           automatically.                     #
#                                        b) Corrected logMessageInterval value #
#                                           in Delay_Resp message.             #
#                                        c) Updated validation of below fields #
#                                           in check_signal                    #
#                                           1) tlv_length                      #
#                                           2) phase_offset_tx_timestamp_sec   #
#                                           3) phase_offset_tx_timestamp_ns    #
#                                           4) freq_offset_tx_timestamp_sec    #
#                                           5) freq_offset_tx_timestamp_ns     #
#  1.6       Dec/2018      CERN          Used actual timestamp for sending     #
#                                        Pdelay_Resp and Pdelay_Resp_Follow_Up.#
#  1.7       Jan/2019      CERN          Handled locking mechanism for         #
#                                        resetting capture buffer.             #
#  1.8       Jan/2019      CERN          Updated default value for             #
#                                        correctionField.                      #
#  1.9       Feb/2019      CERN          Added ptp_ha::stop_at_step.           #
#  1.10      Feb/2019      CERN          Updated ptp_ha::stop_at_step to work  #
#                                        based on configuration in CLI         #
#                                        integration file.                     #
#  1.11      Apr/2019      CERN          Added ptp_ha::value_range.            #
#                                                                              #
################################################################################
# Copyright (c) 2018 - 2019 CERN                                               #
################################################################################
#
######################### PROCEDURES USED ######################################
#                                                                              #
#  1. ptp_ha::send_sync                                                        #
#  2. ptp_ha::send_announce                                                    #
#  3. ptp_ha::send_delay_req                                                   #
#  4. ptp_ha::send_delay_resp                                                  #
#  5. ptp_ha::send_followup                                                    #
#  6. ptp_ha::send_pdelay_req                                                  #
#  7. ptp_ha::send_pdelay_resp                                                 #
#  8. ptp_ha::send_pdelay_resp_followup                                        #
#  9. ptp_ha::send_signal                                                      #
# 10. ptp_ha::send_mgmt                                                        #
# 11. ptp_ha::recv_sync                                                        #
# 12. ptp_ha::recv_announce                                                    #
# 13. ptp_ha::recv_delay_req                                                   #
# 14. ptp_ha::recv_delay_resp                                                  #
# 15. ptp_ha::recv_followup                                                    #
# 16. ptp_ha::recv_pdelay_req                                                  #
# 17. ptp_ha::recv_pdelay_resp                                                 #
# 18. ptp_ha::recv_pdelay_resp_followup                                        #
# 19. ptp_ha::recv_signal                                                      #
# 20. ptp_ha::recv_mgmt                                                        #
# 21. ptp_ha::stop_sync                                                        #
# 22. ptp_ha::stop_announce                                                    #
# 23. ptp_ha::stop_delay_req                                                   #
# 24. ptp_ha::stop_delay_resp                                                  #
# 25. ptp_ha::stop_followup                                                    #
# 26. ptp_ha::stop_pdelay_req                                                  #
# 27. ptp_ha::stop_pdelay_resp                                                 #
# 28. ptp_ha::stop_pdelay_resp_followup                                        #
# 29. ptp_ha::stop_signal                                                      #
# 30. ptp_ha::stop_mgmt                                                        #
# 31. ptp_ha::check_signal                                                     #
# 32. ptp_ha::resolve_ip_to_mac                                                #
# 33. ptp_ha::respond_to_arp_requests                                          #
# 35. ptp_ha::get_packet_content                                               #
# 36. ptp_ha::packet_capture                                                   #
# 37. ptp_ha::get_port_macaddress                                              #
# 38. ptp_ha::tee_get_interface_ip                                             #
# 39. ptp_ha::reset_capture_stats                                              #
#                                                                              #
################################################################################

namespace eval ptp_ha {}

#Initialization
set ::handler(PTP_HA)                         update_ptp_ha_dynamic_fields
set ::validation_handler(PTP_HA)              validate_ptp_ha_pkt
set ::validation_handler(ARP)                 validate_arp_pkt
set ::validation_handler(ARP_RESPONDER)       send_arp_response

################################################################################
#                                                                              #
#  PROCEDURE NAME         :  ptp_ha::send_sync                                 #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Transmission port              #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   dest_mac                      : (Mandatory) Destination MAC dddress.       #
#                                               (e.g., 01:80:C2:00:00:20)      #
#                                                                              #
#   src_mac                       : (Mandatory) Source MAC address.            #
#                                               (e.g., 00:80:C2:00:00:21)      #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                               (Default : 88F7)               #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                               (Default : 0)                  #
#                                                                              #
#   vlan_priority                 : (Optional)  Vlan Priority                  #
#                                               (Default : 0)                  #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                               (Default : 224.0.1.129)        #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                               (Default : 0.0.0.0)            #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                               (Default : auto)               #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                               (Default : 319)                #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                               (Default : 319)                #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                               (Default : 52)                 #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                               (Default : 0)                  #
#                                                                              #
#   major_sdo_id                  : (Optional)  Major sdo id                   #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default : 0)                  #
#                                                                              #
#   minor_version                 : (Optional)  PTP version number             #
#                                               (Default : 1)                  #
#                                                                              #
#   version_ptp                   : (Optional)  PTP minor version number       #
#                                               (Default : 2)                  #
#                                                                              #
#   message_length                : (Optional)  PTP Annouce message length     #
#                                               (Default : 44)                 #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                               (Default : 0)                  #
#                                                                              #
#   minor_sdo_id                  : (Optional)  Minor Sdo id                   #
#                                               (Default : 0)                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                               (Default : 0)                  #
#                                                                              #
#   two_step_flag                 : (Optional)  Two Step Flag                  #
#                                               (Default : 0)                  #
#                                                                              #
#   unicast_flag                  : (Optional)  Unicast Flag                   #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   secure                        : (Optional)  Secure Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                               (Default : 0)                  #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP TimeSclae Flag             #
#                                               (Default : 0)                  #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                               (Default : 0)                  #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                               (Default : 0)                  #
#                                                                              #
#   sync_uncertain                : (Optional)  Synchronization uncertain Flag #
#                                               (Default : 0)                  #
#                                                                              #
#   correction_field              : (Optional)  Correction Field value         #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type_specific         : (Optional)  Message type                   #
#                                               (Default : 0)                  #
#                                                                              #
#   src_port_number               : (Optional)  Source Port number             #
#                                               (Default : 0)                  #
#                                                                              #
#   src_clock_identity            : (Optional)  Source Clock identity          #
#                                               (Default : 0)                  #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                               (Default : 0)                  #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                               (Default : 0)                  #
#                                                                              #
#   log_message_interval          : (Optional)  Log message interval           #
#                                               (Default : 0)                  #
#                                                                              #
#   origin_timestamp_sec          : (Optional)  Origin Timestamp Sec           #
#                                               (Default : 0)                  #
#                                                                              #
#   origin_timestamp_ns           : (Optional)  Origin Timestamp Ns            #
#                                               (Default : 0)                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default : 1)                  #
#                                                                              #
#   interval                      : (Optional)  Frame interval (in seconds)    #
#                                               (Default : 1)                  #
#                                                                              #
#  OUTPUT PARAMETERS      :  NONE                                              #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is successfully send   #
#                                         with the given parameters)           #
#                                                                              #
#                         : 0 on failure (If the packet could not be send      #
#                                         with the given parameters)           #
#                                                                              #
#  DEFINITION             : This function is used to construct and send        #
#                           PTP sync message                                   #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       ptp_ha::send_sync\                                                     #
#           -port_num                         $port_num\                       #
#           -dest_mac                         $dest_mac\                       #
#           -src_mac                          $src_mac\                        #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -vlan_priority                    $vlan_priority\                  #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -major_sdo_id                     $major_sdo_id\                   #
#           -message_type                     $message_type\                   #
#           -minor_version                    $minor_version\                  #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -minor_sdo_id                     $minor_sdo_id\                   #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -two_step_flag                    $two_step_flag\                  #
#           -unicast_flag                     $unicast_flag\                   #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -secure                           $secure\                         #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -sync_uncertain                   $sync_uncertain\                 #
#           -correction_field                 $correction_field\               #
#           -message_type_specific            $message_type_specific\          #
#           -src_port_number                  $src_port_number\                #
#           -src_clock_identity               $src_clock_identity\             #
#           -sequence_id                      $sequence_id\                    #
#           -control_field                    $control_field\                  #
#           -log_message_interval             $log_message_interval\           #
#           -origin_timestamp_sec             $origin_timestamp_sec\           #
#           -origin_timestamp_ns              $origin_timestamp_ns\            #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#                                                                              #
################################################################################
proc ptp_ha::send_sync {args} {

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "ptp_ha::send_sync: Tee session is not initialized."
    }

    #OUTPUT PARAMETERS

    #Transmit timestamp sec
    if {[info exists param(-tx_origin_timestamp_sec)]} {

        upvar $param(-tx_origin_timestamp_sec) tx_origin_timestamp_sec 
        set tx_origin_timestamp_sec ""
    }

    #Transmit timestamp ns
    if {[info exists param(-tx_origin_timestamp_ns)]} {

        upvar $param(-tx_origin_timestamp_ns) tx_origin_timestamp_ns 
        set tx_origin_timestamp_ns ""
    }

    #INPUT PARAMETERS

    #MISCELLANEOUS
    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
        LOG -level 1 -msg "\t\tport_num                       -> $port_num"
    } else {

        ERRLOG -msg "ptp_ha::send_sync: Missing Argument -> port_num"
        return 0
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
        LOG -level 1 -msg "\t\tcount                          -> $count"
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
        LOG -level 1 -msg "\t\tinterval                       -> $interval"
    } else {

        set interval [expr pow(2, $::ptp_sync_interval)]
    }


    #ETHERNET HEADER OPTIONS
    LOG -level 1 -msg "\tEthernet Details"

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
        LOG -level 1 -msg "\t\tdest_mac                       -> $dest_mac"
    } else {

        get_dest_mac\
                     -port_num      $port_num\
                     -message       SYNC\
                     -mac           dest_mac

    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
        LOG -level 1 -msg "\t\tsrc_mac                        -> $src_mac"
    } else {

        ERRLOG -msg "ptp_ha::send_sync: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        LOG -level 1 -msg "\t\teth_type                       -> $eth_type"
    } else {

        if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
            set eth_type 0800
        } else {
            set eth_type 88F7
        }
    }

    #Vlan ID
    if { $::ptp_ha_vlan_encap == "true" } {

        LOG -level 1 -msg "\tVLAN Details"

        if {[info exists param(-vlan_id)]} {

            set vlan_id $param(-vlan_id)
            LOG -level 1 -msg "\t\tvlan_id                        -> $vlan_id"
        } else {

            set vlan_id $::ptp_ha_vlan_id
        }

        if {[info exists param(-vlan_priority)]} {

            set vlan_priority $param(-vlan_priority)
            LOG -level 1 -msg "\t\tvlan_priority                  -> $vlan_priority"
        } else {

            set vlan_priority $::ptp_ha_vlan_priority
        }
        
    } else {

        set vlan_id -1
        set vlan_priority 0
    }

    #IP HEADER OPTIONS
    if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {

        LOG -level 1 -msg "\tIP Details"

        #Destination IP address
        if {[info exists param(-dest_ip)]} {

            set dest_ip $param(-dest_ip)
            LOG -level 1 -msg "\t\tdest_ip                        -> $dest_ip"
        } else {

            get_dest_ip\
                         -port_num      $port_num\
                         -message       SYNC\
                         -ip            dest_ip
        }

        #Source IP address
        if {[info exists param(-src_ip)]} {

            set src_ip $param(-src_ip)
            LOG -level 1 -msg "\t\tsrc_ip                         -> $src_ip"
        } else {

            set src_ip 0.0.0.0
        }

        #IP checksum
        if {[info exists param(-ip_checksum)]} {

            set ip_checksum $param(-ip_checksum)
            LOG -level 1 -msg "\t\tip_checksum                    -> $ip_checksum"
        } else {

            set ip_checksum auto
        }

        #UDP HEADER OPTIONS

        #UDP destination port number
        if {[info exists param(-dest_port)]} {

            set dest_port $param(-dest_port)
            LOG -level 1 -msg "\t\tdest_port                      -> $dest_port"
        } else {

            set dest_port 319
        }

        #UDP source port number
        if {[info exists param(-src_port)]} {

            set src_port $param(-src_port)
            LOG -level 1 -msg "\t\tsrc_port                       -> $src_port"
        } else {

            set src_port 319
        }

        #UDP length
        if {[info exists param(-udp_length)]} {

            set udp_length $param(-udp_length)
            LOG -level 1 -msg "\t\tudp_length                     -> $udp_length"
        } else {

            set udp_length 52
        }

        #UDP checksum
        if {[info exists param(-udp_checksum)]} {

            set udp_checksum $param(-udp_checksum)
            LOG -level 1 -msg "\t\tudp_checksum                   -> $udp_checksum"
        } else {

            set udp_checksum auto
        }
    } else {
        set dest_ip 0.0.0.0
        set src_ip 0.0.0.0
        set ip_checksum 0
        set dest_port 0
        set src_port 0
        set udp_length 0
        set udp_checksum 0
    }

    #PTP HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Details"

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
        LOG -level 1 -msg "\t\tmajor_sdo_id                   -> $major_sdo_id"
    } else {

        set major_sdo_id 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        LOG -level 1 -msg "\t\tmessage_type                   -> $message_type"
    } else {

        set message_type 0
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
        LOG -level 1 -msg "\t\tminor_version                  -> $minor_version"
    } else {

        set minor_version 1
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        LOG -level 1 -msg "\t\tversion_ptp                    -> $version_ptp"
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        LOG -level 1 -msg "\t\tmessage_length                 -> $message_length"
    } else {

        set message_length 44
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        LOG -level 1 -msg "\t\tdomain_number                  -> $domain_number"
    } else {

        set domain_number 0
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
        LOG -level 1 -msg "\t\tminor_sdo_id                   -> $minor_sdo_id"
    } else {

        set minor_sdo_id 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
        LOG -level 1 -msg "\t\tcorrection_field               -> $correction_field"
    } else {

        set correction_field 0
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
        LOG -level 1 -msg "\t\tmessage_type_specific          -> $message_type_specific"
    } else {

        set message_type_specific 0
    }

    #Source Port Number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
        LOG -level 1 -msg "\t\tsrc_port_number                -> $src_port_number"
    } else {

        if {![regexp {.*?([0-9]+)$} $port_num match src_port_number]} {
            set src_port_number 0
        }

    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
        LOG -level 1 -msg "\t\tsrc_clock_identity             -> 0x[dec2hex $src_clock_identity 16]"
    } else {

        get_clock_identity\
                    -mac               $src_mac\
                    -format            $::ptp_clock_identity_format\
                    -clock_identity    src_clock_identity
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        LOG -level 1 -msg "\t\tsequence_id                    -> $sequence_id"
    } else {

        set sequence_id 0
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        LOG -level 1 -msg "\t\tcontrol_field                  -> $control_field"
    } else {

        set control_field 0
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
        LOG -level 1 -msg "\t\tlog_message_interval           -> $log_message_interval"
    } else {

        set log_message_interval 0
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    LOG -level 1 -msg "\tPTP Flag Details"
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        LOG -level 1 -msg "\t\talternate_master_flag          -> $alternate_master_flag"
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
        LOG -level 1 -msg "\t\ttwo_step_flag                  -> $two_step_flag"
    } else {

        if {($::ptp_dut_clock_step == "Two-Step")} {

            set two_step_flag 1
        } else {

            set two_step_flag 0
        }
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
        LOG -level 1 -msg "\t\tunicast_flag                   -> $unicast_flag"
    } else {

        set unicast_flag 0
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        LOG -level 1 -msg "\t\tprofile_specific1              -> $profile_specific1"
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        LOG -level 1 -msg "\t\tprofile_specific2              -> $profile_specific2"
    } else {

        set profile_specific2 0
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
        LOG -level 1 -msg "\t\tsecure                         -> $secure"
    } else {

        set secure 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        LOG -level 1 -msg "\t\tleap61                         -> $leap61"
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        LOG -level 1 -msg "\t\tleap59                         -> $leap59"
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        LOG -level 1 -msg "\t\tcurrent_utc_offset_valid       -> $current_utc_offset_valid"
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        LOG -level 1 -msg "\t\tptp_timescale                  -> $ptp_timescale"
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        LOG -level 1 -msg "\t\ttime_traceable                 -> $time_traceable"
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        LOG -level 1 -msg "\t\tfreq_traceable                 -> $freq_traceable"
    } else {

        set freq_traceable 0
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
        LOG -level 1 -msg "\t\tsync_uncertain                 -> $sync_uncertain"
    } else {

        set sync_uncertain 0
    }

    #PTP SYNC HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Sync Message Details"

    set timestamp_us                    [clock microseconds]
    set timestamp_s                     [expr $timestamp_us / 1000000]
    set timestamp_ns                    [expr ($timestamp_us % 1000000) * 1000]

    if {[info exists param(-origin_timestamp_sec)]} {

        set origin_timestamp_sec    $param(-origin_timestamp_sec)
        set tx_origin_timestamp_sec $origin_timestamp_sec

        LOG -level 1 -msg "\t\torigin_timestamp_sec           -> $origin_timestamp_sec"
    } else {

        set origin_timestamp_sec    $timestamp_s
        set tx_origin_timestamp_sec $origin_timestamp_sec
    }

    if {[info exists param(-origin_timestamp_ns)]} {

        set origin_timestamp_ns     $param(-origin_timestamp_ns)
        set tx_origin_timestamp_ns  $origin_timestamp_ns
        LOG -level 1 -msg "\t\torigin_timestamp_ns            -> $origin_timestamp_ns"
    } else {

        set origin_timestamp_ns     $timestamp_ns
        set tx_origin_timestamp_ns  $origin_timestamp_ns
    }

    #CALL PLATFORM FUNCTION
    if {![pltLib::send_ptp_ha_sync\
                -session_id                        $session_id\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -major_sdo_id                      $major_sdo_id\
                -message_type                      $message_type\
                -minor_version                     $minor_version\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -minor_sdo_id                      $minor_sdo_id\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -secure                            $secure\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_traceable                    $time_traceable\
                -freq_traceable                    $freq_traceable\
                -sync_uncertain                    $sync_uncertain\
                -correction_field                  $correction_field\
                -message_type_specific             $message_type_specific\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -origin_timestamp_sec              $origin_timestamp_sec\
                -origin_timestamp_ns               $origin_timestamp_ns\
                -count                             $count\
                -interval                          $interval]} {

        return 0

    }

    return 1

}

################################################################################
#                                                                              #
#  PROCEDURE NAME         :  ptp_ha::send_announce                             #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Transmission port              #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   dest_mac                      : (Optional)  Destination MAC dddress.       #
#                                               (e.g., 01:80:C2:00:00:20)      #
#                                                                              #
#   src_mac                       : (Mandatory) Source MAC address.            #
#                                               (e.g., 00:80:C2:00:00:21)      #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                               (Default : 88F7)               #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                               (Default : 0)                  #
#                                                                              #
#   vlan_priority                 : (Optional)  Vlan Priority                  #
#                                               (Default : 0)                  #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                               (Default : 224.0.1.129)        #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                               (Default : 0.0.0.0)            #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                               (Default : auto)               #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                               (Default : 320)                #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                               (Default : 320)                #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                               (Default : 72)                 #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                               (Default : 0)                  #
#                                                                              #
#   major_sdo_id                  : (Optional)  Major sdo id                   #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default : 11)                 #
#                                                                              #
#   minor_version                 : (Optional)  PTP version number             #
#                                               (Default : 1)                  #
#                                                                              #
#   version_ptp                   : (Optional)  PTP minor version number       #
#                                               (Default : 2)                  #
#                                                                              #
#   message_length                : (Optional)  PTP Annouce message length     #
#                                               (Default : 64)                 #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                               (Default : 0)                  #
#                                                                              #
#   minor_sdo_id                  : (Optional)  Minor Sdo id                   #
#                                               (Default : 0)                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                               (Default : 0)                  #
#                                                                              #
#   two_step_flag                 : (Optional)  Two Step Flag                  #
#                                               (Default : 0)                  #
#                                                                              #
#   unicast_flag                  : (Optional)  Unicast Flag                   #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   secure                        : (Optional)  Secure Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                               (Default : 0)                  #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP TimeSclae Flag             #
#                                               (Default : 0)                  #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                               (Default : 0)                  #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                               (Default : 0)                  #
#                                                                              #
#   sync_uncertain                : (Optional)  Synchronization uncertain Flag #
#                                               (Default : 0)                  #
#                                                                              #
#   correction_field              : (Optional)  Correction Field value         #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type_specific         : (Optional)  Message type                   #
#                                               (Default : 0)                  #
#                                                                              #
#   src_port_number               : (Optional)  Source Port number             #
#                                               (Default : 0)                  #
#                                                                              #
#   src_clock_identity            : (Optional)  Source Clock identity          #
#                                               (Default : 0)                  #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                               (Default : 0)                  #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                               (Default : 0)                  #
#                                                                              #
#   log_message_interval          : (Optional)  Log message interval           #
#                                               (Default : 0)                  #
#                                                                              #
#   origin_timestamp_sec          : (Optional)  Origin Timestamp Sec           #
#                                               (Default : 0)                  #
#                                                                              #
#   origin_timestamp_ns           : (Optional)  Origin Timestamp Ns            #
#                                               (Default : 0)                  #
#                                                                              #
#   current_utc_offset            : (Optional)  Current Utc Offset             #
#                                               (Default : 0)                  #
#                                                                              #
#   gm_priority1                  : (Optional)  Gm Priority1                   #
#                                               (Default : 0)                  #
#                                                                              #
#   gm_priority2                  : (Optional)  Gm Priority2                   #
#                                               (Default : 0)                  #
#                                                                              #
#   gm_identity                   : (Optional)  Gm Identity                    #
#                                               (Default : 0)                  #
#                                                                              #
#   steps_removed                 : (Optional)  Steps Removed                  #
#                                               (Default : 0)                  #
#                                                                              #
#   time_source                   : (Optional)  Time Source                    #
#                                               (Default : 0)                  #
#                                                                              #
#   gm_clock_classy               : (Optional)  Gm Clock Classy                #
#                                               (Default : 0)                  #
#                                                                              #
#   gm_clock_accuracy             : (Optional)  Gm Clock Accuracy              #
#                                               (Default : 0)                  #
#                                                                              #
#   gm_clock_variance             : (Optional)  Gm Clock Variance              #
#                                               (Default : 0)                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default : 1)                  #
#                                                                              #
#   interval                      : (Optional)  Frame interval (in seconds)    #
#                                               (Default : 1)                  #
#                                                                              #
#  OUTPUT PARAMETERS      :  NONE                                              #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is successfully send   #
#                                         with the given parameters)           #
#                                                                              #
#                         : 0 on failure (If the packet could not be send      #
#                                         with the given parameters)           #
#                                                                              #
#  DEFINITION             : This function is used to construct and send        #
#                           PTP announce message                               #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       ptp_ha::send_announce\                                                 #
#           -port_num                         $port_num\                       #
#           -dest_mac                         $dest_mac\                       #
#           -src_mac                          $src_mac\                        #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -vlan_priority                    $vlan_priority\                  #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -major_sdo_id                     $major_sdo_id\                   #
#           -message_type                     $message_type\                   #
#           -minor_version                    $minor_version\                  #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -minor_sdo_id                     $minor_sdo_id\                   #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -two_step_flag                    $two_step_flag\                  #
#           -unicast_flag                     $unicast_flag\                   #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -secure                           $secure\                         #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -sync_uncertain                   $sync_uncertain\                 #
#           -correction_field                 $correction_field\               #
#           -message_type_specific            $message_type_specific\          #
#           -src_port_number                  $src_port_number\                #
#           -src_clock_identity               $src_clock_identity\             #
#           -sequence_id                      $sequence_id\                    #
#           -control_field                    $control_field\                  #
#           -log_message_interval             $log_message_interval\           #
#           -origin_timestamp_sec             $origin_timestamp_sec\           #
#           -origin_timestamp_ns              $origin_timestamp_ns\            #
#           -current_utc_offset               $current_utc_offset\             #
#           -gm_priority1                     $gm_priority1\                   #
#           -gm_priority2                     $gm_priority2\                   #
#           -gm_identity                      $gm_identity\                    #
#           -steps_removed                    $steps_removed\                  #
#           -gm_clock_classy                  $gm_clock_classy\                #
#           -gm_clock_accuracy                $gm_clock_accuracy\              #
#           -gm_clock_variance                $gm_clock_variance\              #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#                                                                              #
################################################################################
proc ptp_ha::send_announce {args} {

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "ptp_ha::send_announce: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #MISCELLANEOUS
    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
        LOG -level 1 -msg "\t\tport_num                       -> $port_num"
    } else {

        ERRLOG -msg "ptp_ha::send_announce: Missing Argument -> port_num"
        return 0
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
        LOG -level 1 -msg "\t\tcount                          -> $count"
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
        LOG -level 1 -msg "\t\tinterval                       -> $interval"
    } else {

        set interval [expr pow(2, $::ptp_sync_interval)]
    }


    #ETHERNET HEADER OPTIONS
    LOG -level 1 -msg "\tEthernet Details"

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
        LOG -level 1 -msg "\t\tdest_mac                       -> $dest_mac"
    } else {

        get_dest_mac\
                     -port_num      $port_num\
                     -message       ANNOUNCE\
                     -mac           dest_mac
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
        LOG -level 1 -msg "\t\tsrc_mac                        -> $src_mac"
    } else {

        ERRLOG -msg "ptp_ha::send_announce: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        LOG -level 1 -msg "\t\teth_type                       -> $eth_type"
    } else {

        if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
            set eth_type 0800
        } else {
            set eth_type 88F7
        }
    }

    #Vlan ID
    if { $::ptp_ha_vlan_encap == "true" } {

        LOG -level 1 -msg "\tVLAN Details"

        if {[info exists param(-vlan_id)]} {

            set vlan_id $param(-vlan_id)
            LOG -level 1 -msg "\t\tvlan_id                        -> $vlan_id"
        } else {

            set vlan_id $::ptp_ha_vlan_id
        }

        if {[info exists param(-vlan_priority)]} {

            set vlan_priority $param(-vlan_priority)
            LOG -level 1 -msg "\t\tvlan_priority                  -> $vlan_priority"
        } else {

            set vlan_priority $::ptp_ha_vlan_priority
        }
        
    } else {

        set vlan_id -1
        set vlan_priority 0
    }

    #IP HEADER OPTIONS
    if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {

        LOG -level 1 -msg "\tIP Details"

        #Destination IP address
        if {[info exists param(-dest_ip)]} {

            set dest_ip $param(-dest_ip)
            LOG -level 1 -msg "\t\tdest_ip                        -> $dest_ip"
        } else {

            get_dest_ip\
                         -port_num      $port_num\
                         -message       ANNOUNCE\
                         -ip            dest_ip
        }

        #Source IP address
        if {[info exists param(-src_ip)]} {

            set src_ip $param(-src_ip)
            LOG -level 1 -msg "\t\tsrc_ip                         -> $src_ip"
        } else {

            set src_ip 0.0.0.0
        }

        #IP checksum
        if {[info exists param(-ip_checksum)]} {

            set ip_checksum $param(-ip_checksum)
            LOG -level 1 -msg "\t\tip_checksum                    -> $ip_checksum"
        } else {

            set ip_checksum auto
        }

        #UDP HEADER OPTIONS

        #UDP destination port number
        if {[info exists param(-dest_port)]} {

            set dest_port $param(-dest_port)
            LOG -level 1 -msg "\t\tdest_port                      -> $dest_port"
        } else {

            set dest_port 320
        }

        #UDP source port number
        if {[info exists param(-src_port)]} {

            set src_port $param(-src_port)
            LOG -level 1 -msg "\t\tsrc_port                       -> $src_port"
        } else {

            set src_port 320
        }

        #UDP length
        if {[info exists param(-udp_length)]} {

            set udp_length $param(-udp_length)
            LOG -level 1 -msg "\t\tudp_length                     -> $udp_length"
        } else {

            set udp_length 72
        }

        #UDP checksum
        if {[info exists param(-udp_checksum)]} {

            set udp_checksum $param(-udp_checksum)
            LOG -level 1 -msg "\t\tudp_checksum                   -> $udp_checksum"
        } else {

            set udp_checksum auto
        }
    } else {
        set dest_ip 0.0.0.0
        set src_ip 0.0.0.0
        set ip_checksum 0
        set dest_port 0
        set src_port 0
        set udp_length 0
        set udp_checksum 0
    }

    #PTP HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Details"

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
        LOG -level 1 -msg "\t\tmajor_sdo_id                   -> $major_sdo_id"
    } else {

        set major_sdo_id 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        LOG -level 1 -msg "\t\tmessage_type                   -> $message_type"
    } else {

        set message_type 11
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
        LOG -level 1 -msg "\t\tminor_version                  -> $minor_version"
    } else {

        set minor_version 1
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        LOG -level 1 -msg "\t\tversion_ptp                    -> $version_ptp"
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        LOG -level 1 -msg "\t\tmessage_length                 -> $message_length"
    } else {

        set message_length 64
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        LOG -level 1 -msg "\t\tdomain_number                  -> $domain_number"
    } else {

        set domain_number 0
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
        LOG -level 1 -msg "\t\tminor_sdo_id                   -> $minor_sdo_id"
    } else {

        set minor_sdo_id 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
        LOG -level 1 -msg "\t\tcorrection_field               -> $correction_field"
    } else {

        set correction_field 0
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
        LOG -level 1 -msg "\t\tmessage_type_specific          -> $message_type_specific"
    } else {

        set message_type_specific 0
    }

    #Source Port Number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
        LOG -level 1 -msg "\t\tsrc_port_number                -> $src_port_number"
    } else {

        if {![regexp {.*?([0-9]+)$} $port_num match src_port_number]} {
            set src_port_number 0
        }

    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
        LOG -level 1 -msg "\t\tsrc_clock_identity             -> 0x[dec2hex $src_clock_identity 16]"
    } else {

        get_clock_identity\
                    -mac               $src_mac\
                    -format            $::ptp_clock_identity_format\
                    -clock_identity    src_clock_identity
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        LOG -level 1 -msg "\t\tsequence_id                    -> $sequence_id"
    } else {

        set sequence_id 0
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        LOG -level 1 -msg "\t\tcontrol_field                  -> $control_field"
    } else {

        set control_field 5
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
        LOG -level 1 -msg "\t\tlog_message_interval           -> $log_message_interval"
    } else {

        set log_message_interval 127
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    LOG -level 1 -msg "\tPTP Flag Details"
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        LOG -level 1 -msg "\t\talternate_master_flag          -> $alternate_master_flag"
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
        LOG -level 1 -msg "\t\ttwo_step_flag                  -> $two_step_flag"
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
        LOG -level 1 -msg "\t\tunicast_flag                   -> $unicast_flag"
    } else {

        set unicast_flag 0
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        LOG -level 1 -msg "\t\tprofile_specific1              -> $profile_specific1"
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        LOG -level 1 -msg "\t\tprofile_specific2              -> $profile_specific2"
    } else {

        set profile_specific2 0
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
        LOG -level 1 -msg "\t\tsecure                         -> $secure"
    } else {

        set secure 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        LOG -level 1 -msg "\t\tleap61                         -> $leap61"
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        LOG -level 1 -msg "\t\tleap59                         -> $leap59"
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        LOG -level 1 -msg "\t\tcurrent_utc_offset_valid       -> $current_utc_offset_valid"
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        LOG -level 1 -msg "\t\tptp_timescale                  -> $ptp_timescale"
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        LOG -level 1 -msg "\t\ttime_traceable                 -> $time_traceable"
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        LOG -level 1 -msg "\t\tfreq_traceable                 -> $freq_traceable"
    } else {

        set freq_traceable 0
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
        LOG -level 1 -msg "\t\tsync_uncertain                 -> $sync_uncertain"
    } else {

        set sync_uncertain 0
    }

    #PTP ANNOUNCE HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Announce Message Details"

    set timestamp_us                    [clock microseconds]
    set timestamp_s                     [expr $timestamp_us / 1000000]
    set timestamp_ns                    [expr ($timestamp_us % 1000000) * 1000]

    if {[info exists param(-origin_timestamp_sec)]} {

        set origin_timestamp_sec $param(-origin_timestamp_sec)
        LOG -level 1 -msg "\t\torigin_timestamp_sec           -> $origin_timestamp_sec"
    } else {

        set origin_timestamp_sec $timestamp_s
    }

    if {[info exists param(-origin_timestamp_ns)]} {

        set origin_timestamp_ns $param(-origin_timestamp_ns)
        LOG -level 1 -msg "\t\torigin_timestamp_ns            -> $origin_timestamp_ns"
    } else {

        set origin_timestamp_ns $timestamp_ns
    }

    if {[info exists param(-current_utc_offset)]} {

        set current_utc_offset $param(-current_utc_offset)
        LOG -level 1 -msg "\t\tcurrent_utc_offset             -> $current_utc_offset"
    } else {

        set current_utc_offset  0
    }

    if {[info exists param(-gm_priority1)]} {

        set gm_priority1 $param(-gm_priority1)
        LOG -level 1 -msg "\t\tgm_priority1                   -> $gm_priority1"
    } else {

        set gm_priority1 0
    }

    if {[info exists param(-gm_priority2)]} {

        set gm_priority2 $param(-gm_priority2)
        LOG -level 1 -msg "\t\tgm_priority2                   -> $gm_priority2"
    } else {

        set gm_priority2 0
    }

    if {[info exists param(-gm_identity)]} {

        set gm_identity $param(-gm_identity)
        LOG -level 1 -msg "\t\tgm_identity                    -> $gm_identity"
    } else {

        set gm_identity 0
    }

    if {[info exists param(-steps_removed)]} {

        set steps_removed $param(-steps_removed)
        LOG -level 1 -msg "\t\tsteps_removed                  -> $steps_removed"
    } else {

        set steps_removed 0
    }

    if {[info exists param(-time_source)]} {

        set time_source $param(-time_source)
        LOG -level 1 -msg "\t\ttime_source                    -> $time_source"
    } else {

        set time_source 0
    }

    if {[info exists param(-gm_clock_classy)]} {

        set gm_clock_classy $param(-gm_clock_classy)
        LOG -level 1 -msg "\t\tgm_clock_classy                -> $gm_clock_classy"
    } else {

        set gm_clock_classy 0
    }

    if {[info exists param(-gm_clock_accuracy)]} {

        set gm_clock_accuracy $param(-gm_clock_accuracy)
        LOG -level 1 -msg "\t\tgm_clock_accuracy              -> $gm_clock_accuracy"
    } else {

        set gm_clock_accuracy 0
    }

    if {[info exists param(-gm_clock_variance)]} {

        set gm_clock_variance $param(-gm_clock_variance)
        LOG -level 1 -msg "\t\tgm_clock_variance              -> $gm_clock_variance"
    } else {

        set gm_clock_variance 0
    }

    #CALL PLATFORM FUNCTION
    if {![pltLib::send_ptp_ha_announce\
                -session_id                        $session_id\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -major_sdo_id                      $major_sdo_id\
                -message_type                      $message_type\
                -minor_version                     $minor_version\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -minor_sdo_id                      $minor_sdo_id\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -secure                            $secure\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_traceable                    $time_traceable\
                -freq_traceable                    $freq_traceable\
                -sync_uncertain                    $sync_uncertain\
                -correction_field                  $correction_field\
                -message_type_specific             $message_type_specific\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -origin_timestamp_sec              $origin_timestamp_sec\
                -origin_timestamp_ns               $origin_timestamp_ns\
                -current_utc_offset                $current_utc_offset\
                -gm_priority1                      $gm_priority1\
                -gm_priority2                      $gm_priority2\
                -gm_identity                       $gm_identity\
                -steps_removed                     $steps_removed\
                -time_source                       $time_source\
                -gm_clock_classy                   $gm_clock_classy\
                -gm_clock_accuracy                 $gm_clock_accuracy\
                -gm_clock_variance                 $gm_clock_variance\
                -count                             $count\
                -interval                          $interval]} {

        return 0

    }

    return 1

}

################################################################################
#                                                                              #
#  PROCEDURE NAME         :  ptp_ha::send_delay_req                            #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Transmission port              #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   dest_mac                      : (Optional)  Destination MAC dddress.       #
#                                               (e.g., 01:80:C2:00:00:20)      #
#                                                                              #
#   src_mac                       : (Mandatory) Source MAC address.            #
#                                               (e.g., 00:80:C2:00:00:21)      #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                               (Default : 88F7)               #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                               (Default : 0)                  #
#                                                                              #
#   vlan_priority                 : (Optional)  Vlan Priority                  #
#                                               (Default : 0)                  #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                               (Default : 224.0.1.129)        #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                               (Default : 0.0.0.0)            #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                               (Default : auto)               #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                               (Default : 319)                #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                               (Default : 319)                #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                               (Default : 52)                 #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                               (Default : 0)                  #
#                                                                              #
#   major_sdo_id                  : (Optional)  Major sdo id                   #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default : 1)                  #
#                                                                              #
#   minor_version                 : (Optional)  PTP version number             #
#                                               (Default : 2)                  #
#                                                                              #
#   version_ptp                   : (Optional)  PTP minor version number       #
#                                               (Default : 2)                  #
#                                                                              #
#   message_length                : (Optional)  PTP Annouce message length     #
#                                               (Default : 44)                 #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                               (Default : 0)                  #
#                                                                              #
#   minor_sdo_id                  : (Optional)  Minor Sdo id                   #
#                                               (Default : 0)                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                               (Default : 0)                  #
#                                                                              #
#   two_step_flag                 : (Optional)  Two Step Flag                  #
#                                               (Default : 0)                  #
#                                                                              #
#   unicast_flag                  : (Optional)  Unicast Flag                   #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   secure                        : (Optional)  Secure Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                               (Default : 0)                  #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP TimeSclae Flag             #
#                                               (Default : 0)                  #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                               (Default : 0)                  #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                               (Default : 0)                  #
#                                                                              #
#   sync_uncertain                : (Optional)  Synchronization uncertain Flag #
#                                               (Default : 0)                  #
#                                                                              #
#   correction_field              : (Optional)  Correction Field value         #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type_specific         : (Optional)  Message type                   #
#                                               (Default : 0)                  #
#                                                                              #
#   src_port_number               : (Optional)  Source Port number             #
#                                               (Default : 0)                  #
#                                                                              #
#   src_clock_identity            : (Optional)  Source Clock identity          #
#                                               (Default : 0)                  #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                               (Default : 0)                  #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                               (Default : 1)                  #
#                                                                              #
#   log_message_interval          : (Optional)  Log message interval           #
#                                               (Default : 127)                #
#                                                                              #
#   origin_timestamp_sec          : (Optional)  Origin Timestamp Sec           #
#                                               (Default : 0)                  #
#                                                                              #
#   origin_timestamp_ns           : (Optional)  Origin Timestamp Ns            #
#                                               (Default : 0)                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default : 1)                  #
#                                                                              #
#   interval                      : (Optional)  Frame interval (in seconds)    #
#                                               (Default : 1)                  #
#                                                                              #
#  OUTPUT PARAMETERS      :  NONE                                              #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is successfully send   #
#                                         with the given parameters)           #
#                                                                              #
#                         : 0 on failure (If the packet could not be send      #
#                                         with the given parameters)           #
#                                                                              #
#  DEFINITION             : This function is used to construct and send        #
#                           PTP delay req message                              #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       ptp_ha::send_delay_req\                                                #
#           -port_num                         $port_num\                       #
#           -dest_mac                         $dest_mac\                       #
#           -src_mac                          $src_mac\                        #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -vlan_priority                    $vlan_priority\                  #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -major_sdo_id                     $major_sdo_id\                   #
#           -message_type                     $message_type\                   #
#           -minor_version                    $minor_version\                  #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -minor_sdo_id                     $minor_sdo_id\                   #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -two_step_flag                    $two_step_flag\                  #
#           -unicast_flag                     $unicast_flag\                   #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -secure                           $secure\                         #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -sync_uncertain                   $sync_uncertain\                 #
#           -correction_field                 $correction_field\               #
#           -message_type_specific            $message_type_specific\          #
#           -src_port_number                  $src_port_number\                #
#           -src_clock_identity               $src_clock_identity\             #
#           -sequence_id                      $sequence_id\                    #
#           -control_field                    $control_field\                  #
#           -log_message_interval             $log_message_interval\           #
#           -origin_timestamp_sec             $origin_timestamp_sec\           #
#           -origin_timestamp_ns              $origin_timestamp_ns\            #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#                                                                              #
################################################################################
proc ptp_ha::send_delay_req {args} {

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "ptp_ha::send_delay_req: Tee session is not initialized."
    }

    #OUTPUT PARAMETERS

    #Transmit timestamp sec
    if {[info exists param(-tx_origin_timestamp_sec)]} {

        upvar $param(-tx_origin_timestamp_sec) tx_origin_timestamp_sec 
        set tx_origin_timestamp_sec ""
    }

    #Transmit timestamp ns
    if {[info exists param(-tx_origin_timestamp_ns)]} {

        upvar $param(-tx_origin_timestamp_ns) tx_origin_timestamp_ns 
        set tx_origin_timestamp_ns ""
    }

    #INPUT PARAMETERS

    #MISCELLANEOUS
    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
        LOG -level 1 -msg "\t\tport_num                       -> $port_num"
    } else {

        ERRLOG -msg "ptp_ha::send_delay_req: Missing Argument -> port_num"
        return 0
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
        LOG -level 1 -msg "\t\tcount                          -> $count"
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
        LOG -level 1 -msg "\t\tinterval                       -> $interval"
    } else {

        set interval [expr pow(2, $::ptp_sync_interval)]
    }


    #ETHERNET HEADER OPTIONS
    LOG -level 1 -msg "\tEthernet Details"

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
        LOG -level 1 -msg "\t\tdest_mac                       -> $dest_mac"
    } else {

        get_dest_mac\
                     -port_num      $port_num\
                     -message       DELAY_REQ\
                     -mac           dest_mac
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
        LOG -level 1 -msg "\t\tsrc_mac                        -> $src_mac"
    } else {

        ERRLOG -msg "ptp_ha::send_delay_req: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        LOG -level 1 -msg "\t\teth_type                       -> $eth_type"
    } else {

        if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
            set eth_type 0800
        } else {
            set eth_type 88F7
        }
    }

    #Vlan ID
    if { $::ptp_ha_vlan_encap == "true" } {

        LOG -level 1 -msg "\tVLAN Details"

        if {[info exists param(-vlan_id)]} {

            set vlan_id $param(-vlan_id)
            LOG -level 1 -msg "\t\tvlan_id                        -> $vlan_id"
        } else {

            set vlan_id $::ptp_ha_vlan_id
        }

        if {[info exists param(-vlan_priority)]} {

            set vlan_priority $param(-vlan_priority)
            LOG -level 1 -msg "\t\tvlan_priority                  -> $vlan_priority"
        } else {

            set vlan_priority $::ptp_ha_vlan_priority
        }
        
    } else {

        set vlan_id -1
        set vlan_priority 0
    }

    #IP HEADER OPTIONS
    if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {

        LOG -level 1 -msg "\tIP Details"

        #Destination IP address
        if {[info exists param(-dest_ip)]} {

            set dest_ip $param(-dest_ip)
            LOG -level 1 -msg "\t\tdest_ip                        -> $dest_ip"
        } else {

            get_dest_ip\
                         -port_num      $port_num\
                         -message       DELAY_REQ\
                         -ip            dest_ip
        }

        #Source IP address
        if {[info exists param(-src_ip)]} {

            set src_ip $param(-src_ip)
            LOG -level 1 -msg "\t\tsrc_ip                         -> $src_ip"
        } else {

            set src_ip 0.0.0.0
        }

        #IP checksum
        if {[info exists param(-ip_checksum)]} {

            set ip_checksum $param(-ip_checksum)
            LOG -level 1 -msg "\t\tip_checksum                    -> $ip_checksum"
        } else {

            set ip_checksum auto
        }

        #UDP HEADER OPTIONS

        #UDP destination port number
        if {[info exists param(-dest_port)]} {

            set dest_port $param(-dest_port)
            LOG -level 1 -msg "\t\tdest_port                      -> $dest_port"
        } else {

            set dest_port 319
        }

        #UDP source port number
        if {[info exists param(-src_port)]} {

            set src_port $param(-src_port)
            LOG -level 1 -msg "\t\tsrc_port                       -> $src_port"
        } else {

            set src_port 319
        }

        #UDP length
        if {[info exists param(-udp_length)]} {

            set udp_length $param(-udp_length)
            LOG -level 1 -msg "\t\tudp_length                     -> $udp_length"
        } else {

            set udp_length 52
        }

        #UDP checksum
        if {[info exists param(-udp_checksum)]} {

            set udp_checksum $param(-udp_checksum)
            LOG -level 1 -msg "\t\tudp_checksum                   -> $udp_checksum"
        } else {

            set udp_checksum auto

        }
    } else {
        set dest_ip 0.0.0.0
        set src_ip 0.0.0.0
        set ip_checksum 0
        set dest_port 0
        set src_port 0
        set udp_length 0
        set udp_checksum 0
    }

    #PTP HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Details"

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
        LOG -level 1 -msg "\t\tmajor_sdo_id                   -> $major_sdo_id"
    } else {

        set major_sdo_id 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        LOG -level 1 -msg "\t\tmessage_type                   -> $message_type"
    } else {

        set message_type 1
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
        LOG -level 1 -msg "\t\tminor_version                  -> $minor_version"
    } else {

        set minor_version 1
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        LOG -level 1 -msg "\t\tversion_ptp                    -> $version_ptp"
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        LOG -level 1 -msg "\t\tmessage_length                 -> $message_length"
    } else {

        set message_length 44
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        LOG -level 1 -msg "\t\tdomain_number                  -> $domain_number"
    } else {

        set domain_number 0
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
        LOG -level 1 -msg "\t\tminor_sdo_id                   -> $minor_sdo_id"
    } else {

        set minor_sdo_id 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
        LOG -level 1 -msg "\t\tcorrection_field               -> $correction_field"
    } else {

        set correction_field 0
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
        LOG -level 1 -msg "\t\tmessage_type_specific          -> $message_type_specific"
    } else {

        set message_type_specific 0
    }

    #Source Port Number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
        LOG -level 1 -msg "\t\tsrc_port_number                -> $src_port_number"
    } else {

        if {![regexp {.*?([0-9]+)$} $port_num match src_port_number]} {
            set src_port_number 0
        }

    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
        LOG -level 1 -msg "\t\tsrc_clock_identity             -> 0x[dec2hex $src_clock_identity 16]"
    } else {

        get_clock_identity\
                    -mac               $src_mac\
                    -format            $::ptp_clock_identity_format\
                    -clock_identity    src_clock_identity
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        LOG -level 1 -msg "\t\tsequence_id                    -> $sequence_id"
    } else {

        set sequence_id 0
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        LOG -level 1 -msg "\t\tcontrol_field                  -> $control_field"
    } else {

        set control_field 1
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
        LOG -level 1 -msg "\t\tlog_message_interval           -> $log_message_interval"
    } else {

        set log_message_interval 127
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    LOG -level 1 -msg "\tPTP Flag Details"
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        LOG -level 1 -msg "\t\talternate_master_flag          -> $alternate_master_flag"
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
        LOG -level 1 -msg "\t\ttwo_step_flag                  -> $two_step_flag"
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
        LOG -level 1 -msg "\t\tunicast_flag                   -> $unicast_flag"
    } else {

        set unicast_flag 0
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        LOG -level 1 -msg "\t\tprofile_specific1              -> $profile_specific1"
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        LOG -level 1 -msg "\t\tprofile_specific2              -> $profile_specific2"
    } else {

        set profile_specific2 0
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
        LOG -level 1 -msg "\t\tsecure                         -> $secure"
    } else {

        set secure 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        LOG -level 1 -msg "\t\tleap61                         -> $leap61"
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        LOG -level 1 -msg "\t\tleap59                         -> $leap59"
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        LOG -level 1 -msg "\t\tcurrent_utc_offset_valid       -> $current_utc_offset_valid"
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        LOG -level 1 -msg "\t\tptp_timescale                  -> $ptp_timescale"
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        LOG -level 1 -msg "\t\ttime_traceable                 -> $time_traceable"
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        LOG -level 1 -msg "\t\tfreq_traceable                 -> $freq_traceable"
    } else {

        set freq_traceable 0
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
        LOG -level 1 -msg "\t\tsync_uncertain                 -> $sync_uncertain"
    } else {

        set sync_uncertain 0
    }

    #PTP DELAY_REQ HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Delay_req Message Details"

    set timestamp_us                    [clock microseconds]
    set timestamp_s                     [expr $timestamp_us / 1000000]
    set timestamp_ns                    [expr ($timestamp_us % 1000000) * 1000]

    if {[info exists param(-origin_timestamp_sec)]} {

        set origin_timestamp_sec    $param(-origin_timestamp_sec)
        set tx_origin_timestamp_sec $origin_timestamp_sec

        LOG -level 1 -msg "\t\torigin_timestamp_sec           -> $origin_timestamp_sec"
    } else {

        set origin_timestamp_sec    $timestamp_s
        set tx_origin_timestamp_sec $origin_timestamp_sec
    }

    if {[info exists param(-origin_timestamp_ns)]} {

        set origin_timestamp_ns     $param(-origin_timestamp_ns)
        set tx_origin_timestamp_ns  $origin_timestamp_ns
        LOG -level 1 -msg "\t\torigin_timestamp_ns            -> $origin_timestamp_ns"
    } else {

        set origin_timestamp_ns     $timestamp_ns
        set tx_origin_timestamp_ns  $origin_timestamp_ns
    }

    #CALL PLATFORM FUNCTION
    if {![pltLib::send_ptp_ha_delay_req\
                -session_id                        $session_id\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -major_sdo_id                      $major_sdo_id\
                -message_type                      $message_type\
                -minor_version                     $minor_version\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -minor_sdo_id                      $minor_sdo_id\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -secure                            $secure\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_traceable                    $time_traceable\
                -freq_traceable                    $freq_traceable\
                -sync_uncertain                    $sync_uncertain\
                -correction_field                  $correction_field\
                -message_type_specific             $message_type_specific\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -origin_timestamp_sec              $origin_timestamp_sec\
                -origin_timestamp_ns               $origin_timestamp_ns\
                -count                             $count\
                -interval                          $interval]} {

        return 0

    }

    return 1

}

################################################################################
#                                                                              #
#  PROCEDURE NAME         :  ptp_ha::send_delay_resp                           #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Transmission port              #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   dest_mac                      : (Optional)  Destination MAC dddress.       #
#                                               (e.g., 01:80:C2:00:00:20)      #
#                                                                              #
#   src_mac                       : (Mandatory) Source MAC address.            #
#                                               (e.g., 00:80:C2:00:00:21)      #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                               (Default : 88F7)               #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                               (Default : 0)                  #
#                                                                              #
#   vlan_priority                 : (Optional)  Vlan Priority                  #
#                                               (Default : 0)                  #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                               (Default : 224.0.1.129)        #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                               (Default : 0.0.0.0)            #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                               (Default : auto)               #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                               (Default : 320)                #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                               (Default : 320)                #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                               (Default : 62)                 #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                               (Default : 0)                  #
#                                                                              #
#   major_sdo_id                  : (Optional)  Major sdo id                   #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default : 9)                  #
#                                                                              #
#   minor_version                 : (Optional)  PTP version number             #
#                                               (Default : 2)                  #
#                                                                              #
#   version_ptp                   : (Optional)  PTP minor version number       #
#                                               (Default : 2)                  #
#                                                                              #
#   message_length                : (Optional)  PTP message length             #
#                                               (Default : 54)                 #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                               (Default : 0)                  #
#                                                                              #
#   minor_sdo_id                  : (Optional)  Minor Sdo id                   #
#                                               (Default : 0)                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                               (Default : 0)                  #
#                                                                              #
#   two_step_flag                 : (Optional)  Two Step Flag                  #
#                                               (Default : 0)                  #
#                                                                              #
#   unicast_flag                  : (Optional)  Unicast Flag                   #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   secure                        : (Optional)  Secure Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                               (Default : 0)                  #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP TimeSclae Flag             #
#                                               (Default : 0)                  #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                               (Default : 0)                  #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                               (Default : 0)                  #
#                                                                              #
#   sync_uncertain                : (Optional)  Synchronization uncertain Flag #
#                                               (Default : 0)                  #
#                                                                              #
#   correction_field              : (Optional)  Correction Field value         #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type_specific         : (Optional)  Message type                   #
#                                               (Default : 0)                  #
#                                                                              #
#   src_port_number               : (Optional)  Source Port number             #
#                                               (Default : 0)                  #
#                                                                              #
#   src_clock_identity            : (Optional)  Source Clock identity          #
#                                               (Default : 0)                  #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                               (Default : 0)                  #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                               (Default : 3)                  #
#                                                                              #
#   log_message_interval          : (Optional)  Log message interval           #
#                                               (Default : 127)                #
#                                                                              #
#   receive_timestamp_sec         : (Optional)  Receive Timestamp Sec          #
#                                               (Default : 0)                  #
#                                                                              #
#   receive_timestamp_ns          : (Optional)  Receive Timestamp Ns           #
#                                               (Default : 0)                  #
#                                                                              #
#   requesting_port_number        : (Optional)  Requesting Port Number         #
#                                               (Default : 0)                  #
#                                                                              #
#   requesting_clock_identity     : (Optional)  Requesting Clock Identity      #
#                                               (Default : 0)                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default : 1)                  #
#                                                                              #
#   interval                      : (Optional)  Frame interval (in seconds)    #
#                                               (Default : 1)                  #
#                                                                              #
#  OUTPUT PARAMETERS      :  NONE                                              #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is successfully send   #
#                                         with the given parameters)           #
#                                                                              #
#                         : 0 on failure (If the packet could not be send      #
#                                         with the given parameters)           #
#                                                                              #
#  DEFINITION             : This function is used to construct and send        #
#                           PTP delay resp message                             #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       ptp_ha::send_delay_resp\                                               #
#           -port_num                         $port_num\                       #
#           -dest_mac                         $dest_mac\                       #
#           -src_mac                          $src_mac\                        #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -vlan_priority                    $vlan_priority\                  #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -major_sdo_id                     $major_sdo_id\                   #
#           -message_type                     $message_type\                   #
#           -minor_version                    $minor_version\                  #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -minor_sdo_id                     $minor_sdo_id\                   #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -two_step_flag                    $two_step_flag\                  #
#           -unicast_flag                     $unicast_flag\                   #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -secure                           $secure\                         #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -sync_uncertain                   $sync_uncertain\                 #
#           -correction_field                 $correction_field\               #
#           -message_type_specific            $message_type_specific\          #
#           -src_port_number                  $src_port_number\                #
#           -src_clock_identity               $src_clock_identity\             #
#           -sequence_id                      $sequence_id\                    #
#           -control_field                    $control_field\                  #
#           -log_message_interval             $log_message_interval\           #
#           -receive_timestamp_sec            $receive_timestamp_sec\          #
#           -receive_timestamp_ns             $receive_timestamp_ns\           #
#           -requesting_port_number           $requesting_port_number\         #
#           -requesting_clock_identity        $requesting_clock_identity\      #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#                                                                              #
################################################################################
proc ptp_ha::send_delay_resp {args} {

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "ptp_ha::send_delay_resp: Tee session is not initialized."
    }

    set ::PRINT_AUTO_LOG 0

    #INPUT PARAMETERS

    #MISCELLANEOUS
    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
        ptp_ha::print_auto_log "\t\tport_num                       -> $port_num"
    } else {

        ERRLOG -msg "ptp_ha::send_delay_resp: Missing Argument -> port_num"
        return 0
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
        ptp_ha::print_auto_log "\t\tcount                          -> $count"
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
        LOG -level 1 -msg "\t\tinterval                       -> $interval"
    } else {

        set interval [expr pow(2, $::ptp_sync_interval)]
    }


    #ETHERNET HEADER OPTIONS
    ptp_ha::print_auto_log "\tEthernet Details"

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
        LOG -level 1 -msg "\t\tdest_mac                       -> $dest_mac"
    } else {

        get_dest_mac\
                     -port_num      $port_num\
                     -message       DELAY_RESP\
                     -mac           dest_mac
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
        ptp_ha::print_auto_log "\t\tsrc_mac                        -> $src_mac"
    } else {

        ERRLOG -msg "ptp_ha::send_delay_resp: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        LOG -level 1 -msg "\t\teth_type                       -> $eth_type"
    } else {

        if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
            set eth_type 0800
        } else {
            set eth_type 88F7
        }
    }

    #Vlan ID
    if { $::ptp_ha_vlan_encap == "true" } {

        ptp_ha::print_auto_log "\tVLAN Details"

        if {[info exists param(-vlan_id)]} {

            set vlan_id $param(-vlan_id)
            LOG -level 1 -msg "\t\tvlan_id                        -> $vlan_id"
        } else {

            set vlan_id $::ptp_ha_vlan_id
        }

        if {[info exists param(-vlan_priority)]} {

            set vlan_priority $param(-vlan_priority)
            LOG -level 1 -msg "\t\tvlan_priority                  -> $vlan_priority"
        } else {

            set vlan_priority $::ptp_ha_vlan_priority
        }
        
    } else {

        set vlan_id -1
        set vlan_priority 0
    }

    #IP HEADER OPTIONS
    if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {

        ptp_ha::print_auto_log "\tIP Details"

        #Destination IP address
        if {[info exists param(-dest_ip)]} {

            set dest_ip $param(-dest_ip)
            ptp_ha::print_auto_log "\t\tdest_ip                        -> $dest_ip"
        } else {

            get_dest_ip\
                         -port_num      $port_num\
                         -message       DELAY_RESP\
                         -ip            dest_ip
        }

        #Source IP address
        if {[info exists param(-src_ip)]} {

            set src_ip $param(-src_ip)
            ptp_ha::print_auto_log "\t\tsrc_ip                         -> $src_ip"
        } else {

            set src_ip 0.0.0.0
        }

        #IP checksum
        if {[info exists param(-ip_checksum)]} {

            set ip_checksum $param(-ip_checksum)
            LOG -level 1 -msg "\t\tip_checksum                    -> $ip_checksum"
        } else {

            set ip_checksum auto
        }

        #UDP HEADER OPTIONS

        #UDP destination port number
        if {[info exists param(-dest_port)]} {

            set dest_port $param(-dest_port)
            LOG -level 1 -msg "\t\tdest_port                      -> $dest_port"
        } else {

            set dest_port 320
        }

        #UDP source port number
        if {[info exists param(-src_port)]} {

            set src_port $param(-src_port)
            LOG -level 1 -msg "\t\tsrc_port                       -> $src_port"
        } else {

            set src_port 320
        }

        #UDP length
        if {[info exists param(-udp_length)]} {

            set udp_length $param(-udp_length)
            LOG -level 1 -msg "\t\tudp_length                     -> $udp_length"
        } else {

            set udp_length 62
        }

        #UDP checksum
        if {[info exists param(-udp_checksum)]} {

            set udp_checksum $param(-udp_checksum)
            LOG -level 1 -msg "\t\tudp_checksum                   -> $udp_checksum"
        } else {

            set udp_checksum auto
        }
    } else {
        set dest_ip 0.0.0.0
        set src_ip 0.0.0.0
        set ip_checksum 0
        set dest_port 0
        set src_port 0
        set udp_length 0
        set udp_checksum 0
    }

    #PTP HEADER OPTIONS
    ptp_ha::print_auto_log "\tPTP Details"

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
        LOG -level 1 -msg "\t\tmajor_sdo_id                   -> $major_sdo_id"
    } else {

        set major_sdo_id 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        LOG -level 1 -msg "\t\tmessage_type                   -> $message_type"
    } else {

        set message_type 9
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
        LOG -level 1 -msg "\t\tminor_version                  -> $minor_version"
    } else {

        set minor_version 1
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        LOG -level 1 -msg "\t\tversion_ptp                    -> $version_ptp"
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        LOG -level 1 -msg "\t\tmessage_length                 -> $message_length"
    } else {

        set message_length 54
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        ptp_ha::print_auto_log "\t\tdomain_number                  -> $domain_number"
    } else {

        set domain_number 0
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
        LOG -level 1 -msg "\t\tminor_sdo_id                   -> $minor_sdo_id"
    } else {

        set minor_sdo_id 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
        ptp_ha::print_auto_log "\t\tcorrection_field               -> $correction_field"
    } else {

        set correction_field 0
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
        LOG -level 1 -msg "\t\tmessage_type_specific          -> $message_type_specific"
    } else {

        set message_type_specific 0
    }

    #Source Port Number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
        LOG -level 1 -msg "\t\tsrc_port_number                -> $src_port_number"
    } else {

        if {![regexp {.*?([0-9]+)$} $port_num match src_port_number]} {
            set src_port_number 0
        }

    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
        LOG -level 1 -msg "\t\tsrc_clock_identity             -> 0x[dec2hex $src_clock_identity 16]"
    } else {

        get_clock_identity\
                    -mac               $src_mac\
                    -format            $::ptp_clock_identity_format\
                    -clock_identity    src_clock_identity
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        ptp_ha::print_auto_log "\t\tsequence_id                    -> $sequence_id"
    } else {

        set sequence_id 0
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        LOG -level 1 -msg "\t\tcontrol_field                  -> $control_field"
    } else {

        set control_field 3
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
        LOG -level 1 -msg "\t\tlog_message_interval           -> $log_message_interval"
    } else {

        if {$::ptp_comm_model == "Unicast" } {
            set log_message_interval 127
        } else {
            set log_message_interval $::ptp_delayreq_interval
        }
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    ptp_ha::print_auto_log "\tPTP Flag Details"
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        LOG -level 1 -msg "\t\talternate_master_flag          -> $alternate_master_flag"
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
        LOG -level 1 -msg "\t\ttwo_step_flag                  -> $two_step_flag"
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
        ptp_ha::print_auto_log "\t\tunicast_flag                   -> $unicast_flag"
    } else {

        set unicast_flag 0
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        LOG -level 1 -msg "\t\tprofile_specific1              -> $profile_specific1"
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        LOG -level 1 -msg "\t\tprofile_specific2              -> $profile_specific2"
    } else {

        set profile_specific2 0
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
        LOG -level 1 -msg "\t\tsecure                         -> $secure"
    } else {

        set secure 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        LOG -level 1 -msg "\t\tleap61                         -> $leap61"
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        LOG -level 1 -msg "\t\tleap59                         -> $leap59"
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        LOG -level 1 -msg "\t\tcurrent_utc_offset_valid       -> $current_utc_offset_valid"
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        LOG -level 1 -msg "\t\tptp_timescale                  -> $ptp_timescale"
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        LOG -level 1 -msg "\t\ttime_traceable                 -> $time_traceable"
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        LOG -level 1 -msg "\t\tfreq_traceable                 -> $freq_traceable"
    } else {

        set freq_traceable 0
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
        LOG -level 1 -msg "\t\tsync_uncertain                 -> $sync_uncertain"
    } else {

        set sync_uncertain 0
    }

    #PTP DELAY_RESP HEADER OPTIONS
    ptp_ha::print_auto_log "\tPTP Delay_resp Message Details"

    set timestamp_us                    [clock microseconds]
    set timestamp_s                     [expr $timestamp_us / 1000000]
    set timestamp_ns                    [expr ($timestamp_us % 1000000) * 1000]

    if {[info exists param(-receive_timestamp_sec)]} {

        set receive_timestamp_sec $param(-receive_timestamp_sec)
        LOG -level 1 -msg "\t\treceive_timestamp_sec          -> $receive_timestamp_sec"
    } else {

        set receive_timestamp_sec   $timestamp_s
    }

    if {[info exists param(-receive_timestamp_ns)]} {

        set receive_timestamp_ns $param(-receive_timestamp_ns)
        LOG -level 1 -msg "\t\treceive_timestamp_ns           -> $receive_timestamp_ns"
    } else {

        set receive_timestamp_ns $timestamp_ns
    }

    if {[info exists param(-requesting_port_number)]} {

        set requesting_port_number $param(-requesting_port_number)
        ptp_ha::print_auto_log "\t\trequesting_port_number         -> $requesting_port_number"
    } else {

        set requesting_port_number 0
    }

    if {[info exists param(-requesting_clock_identity)]} {

        set requesting_clock_identity $param(-requesting_clock_identity)
        ptp_ha::print_auto_log "\t\trequesting_clock_identity      -> 0x[dec2hex $requesting_clock_identity 16]"
    } else {

        set requesting_clock_identity 0
    }

    set ::PRINT_AUTO_LOG 1

    #CALL PLATFORM FUNCTION
    if {![pltLib::send_ptp_ha_delay_resp\
                -session_id                        $session_id\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -major_sdo_id                      $major_sdo_id\
                -message_type                      $message_type\
                -minor_version                     $minor_version\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -minor_sdo_id                      $minor_sdo_id\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -secure                            $secure\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_traceable                    $time_traceable\
                -freq_traceable                    $freq_traceable\
                -sync_uncertain                    $sync_uncertain\
                -correction_field                  $correction_field\
                -message_type_specific             $message_type_specific\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -receive_timestamp_sec             $receive_timestamp_sec\
                -receive_timestamp_ns              $receive_timestamp_ns\
                -requesting_port_number            $requesting_port_number\
                -requesting_clock_identity         $requesting_clock_identity\
                -count                             $count\
                -interval                          $interval]} {

        return 0

    }

    return 1
}

################################################################################
#                                                                              #
#  PROCEDURE NAME         :  ptp_ha::send_followup                             #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Transmission port              #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   dest_mac                      : (Mandatory) Destination MAC dddress.       #
#                                               (e.g., 01:80:C2:00:00:20)      #
#                                                                              #
#   src_mac                       : (Mandatory) Source MAC address.            #
#                                               (e.g., 00:80:C2:00:00:21)      #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                               (Default : 88F7)               #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                               (Default : 0)                  #
#                                                                              #
#   vlan_priority                 : (Optional)  Vlan Priority                  #
#                                               (Default : 0)                  #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                               (Default : 224.0.1.129)        #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                               (Default : 0.0.0.0)            #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                               (Default : auto)               #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                               (Default : 320)                #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                               (Default : 320)                #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                               (Default : 52)                 #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                               (Default : 0)                  #
#                                                                              #
#   major_sdo_id                  : (Optional)  Major sdo id                   #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default : 8)                  #
#                                                                              #
#   minor_version                 : (Optional)  PTP version number             #
#                                               (Default : 2)                  #
#                                                                              #
#   version_ptp                   : (Optional)  PTP minor version number       #
#                                               (Default : 2)                  #
#                                                                              #
#   message_length                : (Optional)  PTP message length             #
#                                               (Default : 44)                 #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                               (Default : 0)                  #
#                                                                              #
#   minor_sdo_id                  : (Optional)  Minor Sdo id                   #
#                                               (Default : 0)                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                               (Default : 0)                  #
#                                                                              #
#   two_step_flag                 : (Optional)  Two Step Flag                  #
#                                               (Default : 0)                  #
#                                                                              #
#   unicast_flag                  : (Optional)  Unicast Flag                   #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   secure                        : (Optional)  Secure Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                               (Default : 0)                  #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP TimeSclae Flag             #
#                                               (Default : 0)                  #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                               (Default : 0)                  #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                               (Default : 0)                  #
#                                                                              #
#   sync_uncertain                : (Optional)  Synchronization uncertain Flag #
#                                               (Default : 0)                  #
#                                                                              #
#   correction_field              : (Optional)  Correction Field value         #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type_specific         : (Optional)  Message type                   #
#                                               (Default : 0)                  #
#                                                                              #
#   src_port_number               : (Optional)  Source Port number             #
#                                               (Default : 0)                  #
#                                                                              #
#   src_clock_identity            : (Optional)  Source Clock identity          #
#                                               (Default : 0)                  #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                               (Default : 0)                  #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                               (Default : 2)                  #
#                                                                              #
#   log_message_interval          : (Optional)  Log message interval           #
#                                               (Default : 127)                #
#                                                                              #
#   precise_origin_timestamp_sec  : (Optional)  Precise Origin Timestamp Sec   #
#                                               (Default : 0)                  #
#                                                                              #
#   precise_origin_timestamp_ns   : (Optional)  Precise Origin Timestamp Ns    #
#                                               (Default : 0)                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default : 1)                  #
#                                                                              #
#   interval                      : (Optional)  Frame interval (in seconds)    #
#                                               (Default : 1)                  #
#                                                                              #
#  OUTPUT PARAMETERS      :  NONE                                              #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is successfully send   #
#                                         with the given parameters)           #
#                                                                              #
#                         : 0 on failure (If the packet could not be send      #
#                                         with the given parameters)           #
#                                                                              #
#  DEFINITION             : This function is used to construct and send        #
#                           PTP followup message                               #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       ptp_ha::send_followup\                                                 #
#           -port_num                         $port_num\                       #
#           -dest_mac                         $dest_mac\                       #
#           -src_mac                          $src_mac\                        #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -vlan_priority                    $vlan_priority\                  #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -major_sdo_id                     $major_sdo_id\                   #
#           -message_type                     $message_type\                   #
#           -minor_version                    $minor_version\                  #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -minor_sdo_id                     $minor_sdo_id\                   #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -two_step_flag                    $two_step_flag\                  #
#           -unicast_flag                     $unicast_flag\                   #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -secure                           $secure\                         #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -sync_uncertain                   $sync_uncertain\                 #
#           -correction_field                 $correction_field\               #
#           -message_type_specific            $message_type_specific\          #
#           -src_port_number                  $src_port_number\                #
#           -src_clock_identity               $src_clock_identity\             #
#           -sequence_id                      $sequence_id\                    #
#           -control_field                    $control_field\                  #
#           -log_message_interval             $log_message_interval\           #
#           -precise_origin_timestamp_sec     $precise_origin_timestamp_sec\   #
#           -precise_origin_timestamp_ns      $precise_origin_timestamp_ns\    #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#                                                                              #
################################################################################
proc ptp_ha::send_followup {args} {

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "ptp_ha::send_followup: Tee session is not initialized."
    }

    #OUTPUT PARAMETERS

    #Transmit timestamp sec
    if {[info exists param(-tx_timestamp_sec)]} {

        upvar $param(-tx_timestamp_sec) tx_timestamp_sec 
        set tx_timestamp_sec ""
    }

    #Transmit timestamp ns
    if {[info exists param(-tx_timestamp_ns)]} {

        upvar $param(-tx_timestamp_ns) tx_timestamp_ns 
        set tx_timestamp_ns ""
    }

    #INPUT PARAMETERS

    #MISCELLANEOUS
    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
        LOG -level 1 -msg "\t\tport_num                       -> $port_num"
    } else {

        ERRLOG -msg "ptp_ha::send_followup: Missing Argument -> port_num"
        return 0
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
        LOG -level 1 -msg "\t\tcount                          -> $count"
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
        LOG -level 1 -msg "\t\tinterval                       -> $interval"
    } else {

        set interval [expr pow(2, $::ptp_sync_interval)]
    }


    #ETHERNET HEADER OPTIONS
    LOG -level 1 -msg "\tEthernet Details"

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
        LOG -level 1 -msg "\t\tdest_mac                       -> $dest_mac"
    } else {

        get_dest_mac\
                     -port_num      $port_num\
                     -message       FOLLOW_UP\
                     -mac           dest_mac
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
        LOG -level 1 -msg "\t\tsrc_mac                        -> $src_mac"
    } else {

        ERRLOG -msg "ptp_ha::send_followup: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        LOG -level 1 -msg "\t\teth_type                       -> $eth_type"
    } else {

        if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
            set eth_type 0800
        } else {
            set eth_type 88F7
        }
    }

    #Vlan ID
    if { $::ptp_ha_vlan_encap == "true" } {

        LOG -level 1 -msg "\tVLAN Details"

        if {[info exists param(-vlan_id)]} {

            set vlan_id $param(-vlan_id)
            LOG -level 1 -msg "\t\tvlan_id                        -> $vlan_id"
        } else {

            set vlan_id $::ptp_ha_vlan_id
        }

        if {[info exists param(-vlan_priority)]} {

            set vlan_priority $param(-vlan_priority)
            LOG -level 1 -msg "\t\tvlan_priority                  -> $vlan_priority"
        } else {

            set vlan_priority $::ptp_ha_vlan_priority
        }
        
    } else {

        set vlan_id -1
        set vlan_priority 0
    }

    #IP HEADER OPTIONS
    if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {

        LOG -level 1 -msg "\tIP Details"

        #Destination IP address
        if {[info exists param(-dest_ip)]} {

            set dest_ip $param(-dest_ip)
            LOG -level 1 -msg "\t\tdest_ip                        -> $dest_ip"
        } else {

            get_dest_ip\
                         -port_num      $port_num\
                         -message       FOLLOW_UP\
                         -ip            dest_ip
        }

        #Source IP address
        if {[info exists param(-src_ip)]} {

            set src_ip $param(-src_ip)
            LOG -level 1 -msg "\t\tsrc_ip                         -> $src_ip"
        } else {

            set src_ip 0.0.0.0
        }

        #IP checksum
        if {[info exists param(-ip_checksum)]} {

            set ip_checksum $param(-ip_checksum)
            LOG -level 1 -msg "\t\tip_checksum                    -> $ip_checksum"
        } else {

            set ip_checksum auto
        }

        #UDP HEADER OPTIONS

        #UDP destination port number
        if {[info exists param(-dest_port)]} {

            set dest_port $param(-dest_port)
            LOG -level 1 -msg "\t\tdest_port                      -> $dest_port"
        } else {

            set dest_port 320
        }

        #UDP source port number
        if {[info exists param(-src_port)]} {

            set src_port $param(-src_port)
            LOG -level 1 -msg "\t\tsrc_port                       -> $src_port"
        } else {

            set src_port 320
        }

        #UDP length
        if {[info exists param(-udp_length)]} {

            set udp_length $param(-udp_length)
            LOG -level 1 -msg "\t\tudp_length                     -> $udp_length"
        } else {

            set udp_length 52
        }

        #UDP checksum
        if {[info exists param(-udp_checksum)]} {

            set udp_checksum $param(-udp_checksum)
            LOG -level 1 -msg "\t\tudp_checksum                   -> $udp_checksum"
        } else {

            set udp_checksum auto
        }
    } else {
        set dest_ip 0.0.0.0
        set src_ip 0.0.0.0
        set ip_checksum 0
        set dest_port 0
        set src_port 0
        set udp_length 0
        set udp_checksum 0
    }

    #PTP HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Details"

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
        LOG -level 1 -msg "\t\tmajor_sdo_id                   -> $major_sdo_id"
    } else {

        set major_sdo_id 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        LOG -level 1 -msg "\t\tmessage_type                   -> $message_type"
    } else {

        set message_type 8
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
        LOG -level 1 -msg "\t\tminor_version                  -> $minor_version"
    } else {

        set minor_version 1
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        LOG -level 1 -msg "\t\tversion_ptp                    -> $version_ptp"
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        LOG -level 1 -msg "\t\tmessage_length                 -> $message_length"
    } else {

        set message_length 44
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        LOG -level 1 -msg "\t\tdomain_number                  -> $domain_number"
    } else {

        set domain_number 0
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
        LOG -level 1 -msg "\t\tminor_sdo_id                   -> $minor_sdo_id"
    } else {

        set minor_sdo_id 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
        LOG -level 1 -msg "\t\tcorrection_field               -> $correction_field"
    } else {

        set correction_field $::TEE_CORRECTION_FIELD
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
        LOG -level 1 -msg "\t\tmessage_type_specific          -> $message_type_specific"
    } else {

        set message_type_specific 0
    }

    #Source Port Number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
        LOG -level 1 -msg "\t\tsrc_port_number                -> $src_port_number"
    } else {

        if {![regexp {.*?([0-9]+)$} $port_num match src_port_number]} {
            set src_port_number 0
        }

    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
        LOG -level 1 -msg "\t\tsrc_clock_identity             -> 0x[dec2hex $src_clock_identity 16]"
    } else {

        get_clock_identity\
                    -mac               $src_mac\
                    -format            $::ptp_clock_identity_format\
                    -clock_identity    src_clock_identity
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        LOG -level 1 -msg "\t\tsequence_id                    -> $sequence_id"
    } else {

        set sequence_id 0
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        LOG -level 1 -msg "\t\tcontrol_field                  -> $control_field"
    } else {

        set control_field 2
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
        LOG -level 1 -msg "\t\tlog_message_interval           -> $log_message_interval"
    } else {

        set log_message_interval 127
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    LOG -level 1 -msg "\tPTP Flag Details"
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        LOG -level 1 -msg "\t\talternate_master_flag          -> $alternate_master_flag"
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
        LOG -level 1 -msg "\t\ttwo_step_flag                  -> $two_step_flag"
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
        LOG -level 1 -msg "\t\tunicast_flag                   -> $unicast_flag"
    } else {

        set unicast_flag 0
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        LOG -level 1 -msg "\t\tprofile_specific1              -> $profile_specific1"
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        LOG -level 1 -msg "\t\tprofile_specific2              -> $profile_specific2"
    } else {

        set profile_specific2 0
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
        LOG -level 1 -msg "\t\tsecure                         -> $secure"
    } else {

        set secure 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        LOG -level 1 -msg "\t\tleap61                         -> $leap61"
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        LOG -level 1 -msg "\t\tleap59                         -> $leap59"
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        LOG -level 1 -msg "\t\tcurrent_utc_offset_valid       -> $current_utc_offset_valid"
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        LOG -level 1 -msg "\t\tptp_timescale                  -> $ptp_timescale"
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        LOG -level 1 -msg "\t\ttime_traceable                 -> $time_traceable"
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        LOG -level 1 -msg "\t\tfreq_traceable                 -> $freq_traceable"
    } else {

        set freq_traceable 0
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
        LOG -level 1 -msg "\t\tsync_uncertain                 -> $sync_uncertain"
    } else {

        set sync_uncertain 0
    }

    #PTP FOLLOWUP HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Followup Message Details"

    set timestamp_us                    [clock microseconds]
    set timestamp_s                     [expr $timestamp_us / 1000000]
    set timestamp_ns                    [expr ($timestamp_us % 1000000) * 1000]

    if {[info exists param(-precise_origin_timestamp_sec)]} {

        set precise_origin_timestamp_sec $param(-precise_origin_timestamp_sec)
        LOG -level 1 -msg "\t\tprecise_origin_timestamp_sec   -> $precise_origin_timestamp_sec"
    } else {

        set precise_origin_timestamp_sec $timestamp_s
    }

    if {[info exists param(-precise_origin_timestamp_ns)]} {

        set precise_origin_timestamp_ns $param(-precise_origin_timestamp_ns)
        LOG -level 1 -msg "\t\tprecise_origin_timestamp_ns    -> $precise_origin_timestamp_ns"
    } else {

        set precise_origin_timestamp_ns $timestamp_ns
    }

    set tx_timestamp_sec    $timestamp_s
    set tx_timestamp_ns     $timestamp_ns

    #CALL PLATFORM FUNCTION
    if {![pltLib::send_ptp_ha_followup\
                -session_id                        $session_id\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -major_sdo_id                      $major_sdo_id\
                -message_type                      $message_type\
                -minor_version                     $minor_version\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -minor_sdo_id                      $minor_sdo_id\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -secure                            $secure\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_traceable                    $time_traceable\
                -freq_traceable                    $freq_traceable\
                -sync_uncertain                    $sync_uncertain\
                -correction_field                  $correction_field\
                -message_type_specific             $message_type_specific\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -precise_origin_timestamp_sec      $precise_origin_timestamp_sec\
                -precise_origin_timestamp_ns       $precise_origin_timestamp_ns\
                -count                             $count\
                -interval                          $interval]} {

        return 0

    }

    return 1

}

################################################################################
#                                                                              #
#  PROCEDURE NAME         :  ptp_ha::send_pdelay_req                           #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Transmission port              #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   dest_mac                      : (Mandatory) Destination MAC dddress.       #
#                                               (e.g., 01:80:C2:00:00:20)      #
#                                                                              #
#   src_mac                       : (Mandatory) Source MAC address.            #
#                                               (e.g., 00:80:C2:00:00:21)      #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                               (Default : 88F7)               #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                               (Default : 0)                  #
#                                                                              #
#   vlan_priority                 : (Optional)  Vlan Priority                  #
#                                               (Default : 0)                  #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                               (Default : 224.0.1.129)        #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                               (Default : 0.0.0.0)            #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                               (Default : auto)               #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                               (Default : 319)                #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                               (Default : 319)                #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                               (Default : 62)                 #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                               (Default : 0)                  #
#                                                                              #
#   major_sdo_id                  : (Optional)  Major sdo id                   #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default : 2)                  #
#                                                                              #
#   minor_version                 : (Optional)  PTP version number             #
#                                               (Default : 2)                  #
#                                                                              #
#   version_ptp                   : (Optional)  PTP minor version number       #
#                                               (Default : 2)                  #
#                                                                              #
#   message_length                : (Optional)  PTP message length             #
#                                               (Default : 54)                 #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                               (Default : 0)                  #
#                                                                              #
#   minor_sdo_id                  : (Optional)  Minor Sdo id                   #
#                                               (Default : 0)                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                               (Default : 0)                  #
#                                                                              #
#   two_step_flag                 : (Optional)  Two Step Flag                  #
#                                               (Default : 0)                  #
#                                                                              #
#   unicast_flag                  : (Optional)  Unicast Flag                   #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   secure                        : (Optional)  Secure Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                               (Default : 0)                  #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP TimeSclae Flag             #
#                                               (Default : 0)                  #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                               (Default : 0)                  #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                               (Default : 0)                  #
#                                                                              #
#   sync_uncertain                : (Optional)  Synchronization uncertain Flag #
#                                               (Default : 0)                  #
#                                                                              #
#   correction_field              : (Optional)  Correction Field value         #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type_specific         : (Optional)  Message type                   #
#                                               (Default : 0)                  #
#                                                                              #
#   src_port_number               : (Optional)  Source Port number             #
#                                               (Default : 0)                  #
#                                                                              #
#   src_clock_identity            : (Optional)  Source Clock identity          #
#                                               (Default : 0)                  #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                               (Default : 0)                  #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                               (Default : 5)                  #
#                                                                              #
#   log_message_interval          : (Optional)  Log message interval           #
#                                               (Default : 127)                #
#                                                                              #
#   origin_timestamp_sec          : (Optional)  Origin Timestamp Sec           #
#                                               (Default : 0)                  #
#                                                                              #
#   origin_timestamp_ns           : (Optional)  Origin Timestamp Ns            #
#                                               (Default : 0)                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default : 1)                  #
#                                                                              #
#   interval                      : (Optional)  Frame interval (in seconds)    #
#                                               (Default : 1)                  #
#                                                                              #
#  OUTPUT PARAMETERS      :  NONE                                              #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is successfully send   #
#                                         with the given parameters)           #
#                                                                              #
#                         : 0 on failure (If the packet could not be send      #
#                                         with the given parameters)           #
#                                                                              #
#  DEFINITION             : This function is used to construct and send        #
#                           PTP pdelay req message                             #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       ptp_ha::send_pdelay_req\                                               #
#           -port_num                         $port_num\                       #
#           -dest_mac                         $dest_mac\                       #
#           -src_mac                          $src_mac\                        #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -vlan_priority                    $vlan_priority\                  #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -major_sdo_id                     $major_sdo_id\                   #
#           -message_type                     $message_type\                   #
#           -minor_version                    $minor_version\                  #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -minor_sdo_id                     $minor_sdo_id\                   #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -two_step_flag                    $two_step_flag\                  #
#           -unicast_flag                     $unicast_flag\                   #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -secure                           $secure\                         #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -sync_uncertain                   $sync_uncertain\                 #
#           -correction_field                 $correction_field\               #
#           -message_type_specific            $message_type_specific\          #
#           -src_port_number                  $src_port_number\                #
#           -src_clock_identity               $src_clock_identity\             #
#           -sequence_id                      $sequence_id\                    #
#           -control_field                    $control_field\                  #
#           -log_message_interval             $log_message_interval\           #
#           -origin_timestamp_sec             $origin_timestamp_sec\           #
#           -origin_timestamp_ns              $origin_timestamp_ns\            #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#                                                                              #
################################################################################
proc ptp_ha::send_pdelay_req {args} {

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "ptp_ha::send_pdelay_req: Tee session is not initialized."
    }

    #OUTPUT PARAMETERS

    #Transmit timestamp sec
    if {[info exists param(-tx_origin_timestamp_sec)]} {

        upvar $param(-tx_origin_timestamp_sec) tx_origin_timestamp_sec 
        set tx_origin_timestamp_sec ""
    }

    #Transmit timestamp ns
    if {[info exists param(-tx_origin_timestamp_ns)]} {

        upvar $param(-tx_origin_timestamp_ns) tx_origin_timestamp_ns 
        set tx_origin_timestamp_ns ""
    }

    #INPUT PARAMETERS

    #MISCELLANEOUS
    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
        LOG -level 1 -msg "\t\tport_num                       -> $port_num"
    } else {

        ERRLOG -msg "ptp_ha::send_pdelay_req: Missing Argument -> port_num"
        return 0
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
        LOG -level 1 -msg "\t\tcount                          -> $count"
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
        LOG -level 1 -msg "\t\tinterval                       -> $interval"
    } else {

        set interval [expr pow(2, $::ptp_sync_interval)]
    }


    #ETHERNET HEADER OPTIONS
    LOG -level 1 -msg "\tEthernet Details"

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
        LOG -level 1 -msg "\t\tdest_mac                       -> $dest_mac"
    } else {

        get_dest_mac\
                     -port_num      $port_num\
                     -message       PDELAY_REQ\
                     -mac           dest_mac
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
        LOG -level 1 -msg "\t\tsrc_mac                        -> $src_mac"
    } else {

        ERRLOG -msg "ptp_ha::send_pdelay_req: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        LOG -level 1 -msg "\t\teth_type                       -> $eth_type"
    } else {

        if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
            set eth_type 0800
        } else {
            set eth_type 88f7
        }
    }

    #Vlan ID
    if { $::ptp_ha_vlan_encap == "true" } {

        LOG -level 1 -msg "\tVLAN Details"

        if {[info exists param(-vlan_id)]} {

            set vlan_id $param(-vlan_id)
            LOG -level 1 -msg "\t\tvlan_id                        -> $vlan_id"
        } else {

            set vlan_id $::ptp_ha_vlan_id
        }

        if {[info exists param(-vlan_priority)]} {

            set vlan_priority $param(-vlan_priority)
            LOG -level 1 -msg "\t\tvlan_priority                  -> $vlan_priority"
        } else {

            set vlan_priority $::ptp_ha_vlan_priority
        }
        
    } else {

        set vlan_id -1
        set vlan_priority 0
    }

    #IP HEADER OPTIONS
    if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {

        LOG -level 1 -msg "\tIP Details"

        #Destination IP address
        if {[info exists param(-dest_ip)]} {

            set dest_ip $param(-dest_ip)
            LOG -level 1 -msg "\t\tdest_ip                        -> $dest_ip"
        } else {

            get_dest_ip\
                         -port_num      $port_num\
                         -message       PDELAY_REQ\
                         -ip            dest_ip
        }

        #Source IP address
        if {[info exists param(-src_ip)]} {

            set src_ip $param(-src_ip)
            LOG -level 1 -msg "\t\tsrc_ip                         -> $src_ip"
        } else {

            set src_ip 0.0.0.0
        }

        #IP checksum
        if {[info exists param(-ip_checksum)]} {

            set ip_checksum $param(-ip_checksum)
            LOG -level 1 -msg "\t\tip_checksum                    -> $ip_checksum"
        } else {

            set ip_checksum auto
        }

        #UDP HEADER OPTIONS

        #UDP destination port number
        if {[info exists param(-dest_port)]} {

            set dest_port $param(-dest_port)
            LOG -level 1 -msg "\t\tdest_port                      -> $dest_port"
        } else {

            set dest_port 319
        }

        #UDP source port number
        if {[info exists param(-src_port)]} {

            set src_port $param(-src_port)
            LOG -level 1 -msg "\t\tsrc_port                       -> $src_port"
        } else {

            set src_port 319
        }

        #UDP length
        if {[info exists param(-udp_length)]} {

            set udp_length $param(-udp_length)
            LOG -level 1 -msg "\t\tudp_length                     -> $udp_length"
        } else {

            set udp_length 62
        }

        #UDP checksum
        if {[info exists param(-udp_checksum)]} {

            set udp_checksum $param(-udp_checksum)
            LOG -level 1 -msg "\t\tudp_checksum                   -> $udp_checksum"
        } else {

            set udp_checksum auto
        }
    } else {
        set dest_ip 0.0.0.0
        set src_ip 0.0.0.0
        set ip_checksum 0
        set dest_port 0
        set src_port 0
        set udp_length 0
        set udp_checksum 0
    }

    #PTP HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Details"

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
        LOG -level 1 -msg "\t\tmajor_sdo_id                   -> $major_sdo_id"
    } else {

        set major_sdo_id 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        LOG -level 1 -msg "\t\tmessage_type                   -> $message_type"
    } else {

        set message_type 2
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
        LOG -level 1 -msg "\t\tminor_version                  -> $minor_version"
    } else {

        set minor_version 1
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        LOG -level 1 -msg "\t\tversion_ptp                    -> $version_ptp"
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        LOG -level 1 -msg "\t\tmessage_length                 -> $message_length"
    } else {

        set message_length 54
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        LOG -level 1 -msg "\t\tdomain_number                  -> $domain_number"
    } else {

        set domain_number 0
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
        LOG -level 1 -msg "\t\tminor_sdo_id                   -> $minor_sdo_id"
    } else {

        set minor_sdo_id 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
        LOG -level 1 -msg "\t\tcorrection_field               -> $correction_field"
    } else {

        set correction_field 0
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
        LOG -level 1 -msg "\t\tmessage_type_specific          -> $message_type_specific"
    } else {

        set message_type_specific 0
    }

    #Source Port Number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
        LOG -level 1 -msg "\t\tsrc_port_number                -> $src_port_number"
    } else {

        if {![regexp {.*?([0-9]+)$} $port_num match src_port_number]} {
            set src_port_number 0
        }

    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
        LOG -level 1 -msg "\t\tsrc_clock_identity             -> 0x[dec2hex $src_clock_identity 16]"
    } else {

        get_clock_identity\
                    -mac               $src_mac\
                    -format            $::ptp_clock_identity_format\
                    -clock_identity    src_clock_identity
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        LOG -level 1 -msg "\t\tsequence_id                    -> $sequence_id"
    } else {

        set sequence_id 0
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        LOG -level 1 -msg "\t\tcontrol_field                  -> $control_field"
    } else {

        set control_field 5
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
        LOG -level 1 -msg "\t\tlog_message_interval           -> $log_message_interval"
    } else {

        set log_message_interval 127
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    LOG -level 1 -msg "\tPTP Flag Details"
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        LOG -level 1 -msg "\t\talternate_master_flag          -> $alternate_master_flag"
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
        LOG -level 1 -msg "\t\ttwo_step_flag                  -> $two_step_flag"
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
        LOG -level 1 -msg "\t\tunicast_flag                   -> $unicast_flag"
    } else {

        set unicast_flag 0
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        LOG -level 1 -msg "\t\tprofile_specific1              -> $profile_specific1"
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        LOG -level 1 -msg "\t\tprofile_specific2              -> $profile_specific2"
    } else {

        set profile_specific2 0
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
        LOG -level 1 -msg "\t\tsecure                         -> $secure"
    } else {

        set secure 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        LOG -level 1 -msg "\t\tleap61                         -> $leap61"
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        LOG -level 1 -msg "\t\tleap59                         -> $leap59"
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        LOG -level 1 -msg "\t\tcurrent_utc_offset_valid       -> $current_utc_offset_valid"
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        LOG -level 1 -msg "\t\tptp_timescale                  -> $ptp_timescale"
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        LOG -level 1 -msg "\t\ttime_traceable                 -> $time_traceable"
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        LOG -level 1 -msg "\t\tfreq_traceable                 -> $freq_traceable"
    } else {

        set freq_traceable 0
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
        LOG -level 1 -msg "\t\tsync_uncertain                 -> $sync_uncertain"
    } else {

        set sync_uncertain 0
    }

    #PTP PDELAY_REQ HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Pdelay_req Message Details"

    set timestamp_us                    [clock microseconds]
    set timestamp_s                     [expr $timestamp_us / 1000000]
    set timestamp_ns                    [expr ($timestamp_us % 1000000) * 1000]

    if {[info exists param(-origin_timestamp_sec)]} {

        set origin_timestamp_sec $param(-origin_timestamp_sec)
        set tx_origin_timestamp_sec $origin_timestamp_sec
        LOG -level 1 -msg "\t\torigin_timestamp_sec           -> $origin_timestamp_sec"
    } else {

        set origin_timestamp_sec    $timestamp_s
        set tx_origin_timestamp_sec $origin_timestamp_sec
    }

    if {[info exists param(-origin_timestamp_ns)]} {

        set origin_timestamp_ns $param(-origin_timestamp_ns)
        set tx_origin_timestamp_ns $origin_timestamp_ns
        LOG -level 1 -msg "\t\torigin_timestamp_ns            -> $origin_timestamp_ns"
    } else {

        set origin_timestamp_ns     $timestamp_ns
        set tx_origin_timestamp_ns  $origin_timestamp_ns
    }

    #CALL PLATFORM FUNCTION
    if {![pltLib::send_ptp_ha_pdelay_req\
                -session_id                        $session_id\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -major_sdo_id                      $major_sdo_id\
                -message_type                      $message_type\
                -minor_version                     $minor_version\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -minor_sdo_id                      $minor_sdo_id\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -secure                            $secure\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_traceable                    $time_traceable\
                -freq_traceable                    $freq_traceable\
                -sync_uncertain                    $sync_uncertain\
                -correction_field                  $correction_field\
                -message_type_specific             $message_type_specific\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -origin_timestamp_sec              $origin_timestamp_sec\
                -origin_timestamp_ns               $origin_timestamp_ns\
                -count                             $count\
                -interval                          $interval]} {

        return 0

    }

    return 1

}

################################################################################
#                                                                              #
#  PROCEDURE NAME         :  ptp_ha::send_pdelay_resp                          #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Transmission port              #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   dest_mac                      : (Mandatory) Destination MAC dddress.       #
#                                               (e.g., 01:80:C2:00:00:20)      #
#                                                                              #
#   src_mac                       : (Mandatory) Source MAC address.            #
#                                               (e.g., 00:80:C2:00:00:21)      #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                               (Default : 88F7)               #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                               (Default : 0)                  #
#                                                                              #
#   vlan_priority                 : (Optional)  Vlan Priority                  #
#                                               (Default : 0)                  #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                               (Default : 224.0.1.129)        #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                               (Default : 0.0.0.0)            #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                               (Default : auto)               #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                               (Default : 319)                #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                               (Default : 319)                #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                               (Default : 62)                 #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                               (Default : 0)                  #
#                                                                              #
#   major_sdo_id                  : (Optional)  Major sdo id                   #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default : 3)                  #
#                                                                              #
#   minor_version                 : (Optional)  PTP version number             #
#                                               (Default : 2)                  #
#                                                                              #
#   version_ptp                   : (Optional)  PTP minor version number       #
#                                               (Default : 2)                  #
#                                                                              #
#   message_length                : (Optional)  PTP message length             #
#                                               (Default : 54)                 #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                               (Default : 0)                  #
#                                                                              #
#   minor_sdo_id                  : (Optional)  Minor Sdo id                   #
#                                               (Default : 0)                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                               (Default : 0)                  #
#                                                                              #
#   two_step_flag                 : (Optional)  Two Step Flag                  #
#                                               (Default : 0)                  #
#                                                                              #
#   unicast_flag                  : (Optional)  Unicast Flag                   #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   secure                        : (Optional)  Secure Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                               (Default : 0)                  #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP TimeSclae Flag             #
#                                               (Default : 0)                  #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                               (Default : 0)                  #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                               (Default : 0)                  #
#                                                                              #
#   sync_uncertain                : (Optional)  Synchronization uncertain Flag #
#                                               (Default : 0)                  #
#                                                                              #
#   correction_field              : (Optional)  Correction Field value         #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type_specific         : (Optional)  Message type                   #
#                                               (Default : 0)                  #
#                                                                              #
#   src_port_number               : (Optional)  Source Port number             #
#                                               (Default : 0)                  #
#                                                                              #
#   src_clock_identity            : (Optional)  Source Clock identity          #
#                                               (Default : 0)                  #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                               (Default : 0)                  #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                               (Default : 5)                  #
#                                                                              #
#   log_message_interval          : (Optional)  Log message interval           #
#                                               (Default : 127)                #
#                                                                              #
#   request_receipt_timestamp_sec : (Optional)  Request Receipt Timestamp Sec  #
#                                               (Default : 0)                  #
#                                                                              #
#   request_receipt_timestamp_ns  : (Optional)  Request Receipt Timestamp Ns   #
#                                               (Default : 0)                  #
#                                                                              #
#   requesting_port_number        : (Optional)  Requesting Port Number         #
#                                               (Default : 0)                  #
#                                                                              #
#   requesting_clock_identity     : (Optional)  Requesting Clock Identity      #
#                                               (Default : 0)                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default : 1)                  #
#                                                                              #
#   interval                      : (Optional)  Frame interval (in seconds)    #
#                                               (Default : 1)                  #
#                                                                              #
#  OUTPUT PARAMETERS      :  NONE                                              #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is successfully send   #
#                                         with the given parameters)           #
#                                                                              #
#                         : 0 on failure (If the packet could not be send      #
#                                         with the given parameters)           #
#                                                                              #
#  DEFINITION             : This function is used to construct and send        #
#                           PTP pdelay resp message                            #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       ptp_ha::send_pdelay_resp\                                              #
#           -port_num                         $port_num\                       #
#           -dest_mac                         $dest_mac\                       #
#           -src_mac                          $src_mac\                        #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -vlan_priority                    $vlan_priority\                  #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -major_sdo_id                     $major_sdo_id\                   #
#           -message_type                     $message_type\                   #
#           -minor_version                    $minor_version\                  #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -minor_sdo_id                     $minor_sdo_id\                   #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -two_step_flag                    $two_step_flag\                  #
#           -unicast_flag                     $unicast_flag\                   #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -secure                           $secure\                         #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -sync_uncertain                   $sync_uncertain\                 #
#           -correction_field                 $correction_field\               #
#           -message_type_specific            $message_type_specific\          #
#           -src_port_number                  $src_port_number\                #
#           -src_clock_identity               $src_clock_identity\             #
#           -sequence_id                      $sequence_id\                    #
#           -control_field                    $control_field\                  #
#           -log_message_interval             $log_message_interval\           #
#           -request_receipt_timestamp_sec    $request_receipt_timestamp_sec\  #
#           -request_receipt_timestamp_ns     $request_receipt_timestamp_ns\   #
#           -requesting_port_number           $requesting_port_number\         #
#           -requesting_clock_identity        $requesting_clock_identity\      #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#                                                                              #
################################################################################
proc ptp_ha::send_pdelay_resp {args} {

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "ptp_ha::send_pdelay_resp: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #MISCELLANEOUS
    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
        ptp_ha::print_auto_log "\t\tport_num                       -> $port_num"
    } else {

        ERRLOG -msg "ptp_ha::send_pdelay_resp: Missing Argument -> port_num"
        return 0
    }

    #Instance
    if {[info exists param(-instance)]} {

        set instance $param(-instance)
    } else {

        set instance 7
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
        ptp_ha::print_auto_log "\t\tcount                          -> $count"
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
        LOG -level 1 -msg "\t\tinterval                       -> $interval"
    } else {

        set interval [expr pow(2, $::ptp_sync_interval)]
        set interval 0
    }


    #ETHERNET HEADER OPTIONS
    ptp_ha::print_auto_log "\tEthernet Details"

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
        LOG -level 1 -msg "\t\tdest_mac                       -> $dest_mac"
    } else {

        get_dest_mac\
                     -port_num      $port_num\
                     -message       PDELAY_RESP\
                     -mac           dest_mac
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
        ptp_ha::print_auto_log "\t\tsrc_mac                        -> $src_mac"
    } else {

        ERRLOG -msg "ptp_ha::send_pdelay_resp: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        LOG -level 1 -msg "\t\teth_type                       -> $eth_type"
    } else {

        if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
            set eth_type 0800
        } else {
            set eth_type 88F7
        }
    }

    #Vlan ID
    if { $::ptp_ha_vlan_encap == "true" } {

        ptp_ha::print_auto_log "\tVLAN Details"

        if {[info exists param(-vlan_id)]} {

            set vlan_id $param(-vlan_id)
            LOG -level 1 -msg "\t\tvlan_id                        -> $vlan_id"
        } else {

            set vlan_id $::ptp_ha_vlan_id
        }

        if {[info exists param(-vlan_priority)]} {

            set vlan_priority $param(-vlan_priority)
            LOG -level 1 -msg "\t\tvlan_priority                  -> $vlan_priority"
        } else {

            set vlan_priority $::ptp_ha_vlan_priority
        }
        
    } else {

        set vlan_id -1
        set vlan_priority 0
    }

    #IP HEADER OPTIONS
    if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {

        ptp_ha::print_auto_log "\tIP Details"

        #Destination IP address
        if {[info exists param(-dest_ip)]} {

            set dest_ip $param(-dest_ip)
            ptp_ha::print_auto_log "\t\tdest_ip                        -> $dest_ip"
        } else {

            get_dest_ip\
                         -port_num      $port_num\
                         -message       PDELAY_RESP\
                         -ip            dest_ip
        }

        #Source IP address
        if {[info exists param(-src_ip)]} {

            set src_ip $param(-src_ip)
            ptp_ha::print_auto_log "\t\tsrc_ip                         -> $src_ip"
        } else {

            set src_ip 0.0.0.0
        }

        #IP checksum
        if {[info exists param(-ip_checksum)]} {

            set ip_checksum $param(-ip_checksum)
            LOG -level 1 -msg "\t\tip_checksum                    -> $ip_checksum"
        } else {

            set ip_checksum auto
        }

        #UDP HEADER OPTIONS

        #UDP destination port number
        if {[info exists param(-dest_port)]} {

            set dest_port $param(-dest_port)
            LOG -level 1 -msg "\t\tdest_port                      -> $dest_port"
        } else {

            set dest_port 319
        }

        #UDP source port number
        if {[info exists param(-src_port)]} {

            set src_port $param(-src_port)
            LOG -level 1 -msg "\t\tsrc_port                       -> $src_port"
        } else {

            set src_port 319
        }

        #UDP length
        if {[info exists param(-udp_length)]} {

            set udp_length $param(-udp_length)
            LOG -level 1 -msg "\t\tudp_length                     -> $udp_length"
        } else {

            set udp_length 62
        }

        #UDP checksum
        if {[info exists param(-udp_checksum)]} {

            set udp_checksum $param(-udp_checksum)
            LOG -level 1 -msg "\t\tudp_checksum                   -> $udp_checksum"
        } else {

            set udp_checksum auto
        }
    } else {
        set dest_ip 0.0.0.0
        set src_ip 0.0.0.0
        set ip_checksum 0
        set dest_port 0
        set src_port 0
        set udp_length 0
        set udp_checksum 0
    }

    #PTP HEADER OPTIONS
    ptp_ha::print_auto_log "\tPTP Details"

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
        LOG -level 1 -msg "\t\tmajor_sdo_id                   -> $major_sdo_id"
    } else {

        set major_sdo_id 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        LOG -level 1 -msg "\t\tmessage_type                   -> $message_type"
    } else {

        set message_type 3
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
        LOG -level 1 -msg "\t\tminor_version                  -> $minor_version"
    } else {

        set minor_version 1
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        LOG -level 1 -msg "\t\tversion_ptp                    -> $version_ptp"
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        LOG -level 1 -msg "\t\tmessage_length                 -> $message_length"
    } else {

        set message_length 54
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        ptp_ha::print_auto_log "\t\tdomain_number                  -> $domain_number"
    } else {

        set domain_number 0
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
        LOG -level 1 -msg "\t\tminor_sdo_id                   -> $minor_sdo_id"
    } else {

        set minor_sdo_id 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
        ptp_ha::print_auto_log "\t\tcorrection_field               -> $correction_field"
    } else {

        set correction_field 0
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
        LOG -level 1 -msg "\t\tmessage_type_specific          -> $message_type_specific"
    } else {

        set message_type_specific 0
    }

    #Source Port Number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
        LOG -level 1 -msg "\t\tsrc_port_number                -> $src_port_number"
    } else {

        if {![regexp {.*?([0-9]+)$} $port_num match src_port_number]} {
            set src_port_number 0
        }

    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
        LOG -level 1 -msg "\t\tsrc_clock_identity             -> 0x[dec2hex $src_clock_identity 16]"
    } else {

        get_clock_identity\
                    -mac               $src_mac\
                    -format            $::ptp_clock_identity_format\
                    -clock_identity    src_clock_identity
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        ptp_ha::print_auto_log "\t\tsequence_id                    -> $sequence_id"
    } else {

        set sequence_id 0
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        LOG -level 1 -msg "\t\tcontrol_field                  -> $control_field"
    } else {

        set control_field 5
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
        LOG -level 1 -msg "\t\tlog_message_interval           -> $log_message_interval"
    } else {

        set log_message_interval 127
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    ptp_ha::print_auto_log "\tPTP Flag Details"
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        LOG -level 1 -msg "\t\talternate_master_flag          -> $alternate_master_flag"
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
        ptp_ha::print_auto_log "\t\ttwo_step_flag                  -> $two_step_flag"
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
        ptp_ha::print_auto_log "\t\tunicast_flag                   -> $unicast_flag"
    } else {

        set unicast_flag 0
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        LOG -level 1 -msg "\t\tprofile_specific1              -> $profile_specific1"
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        LOG -level 1 -msg "\t\tprofile_specific2              -> $profile_specific2"
    } else {

        set profile_specific2 0
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
        LOG -level 1 -msg "\t\tsecure                         -> $secure"
    } else {

        set secure 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        LOG -level 1 -msg "\t\tleap61                         -> $leap61"
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        LOG -level 1 -msg "\t\tleap59                         -> $leap59"
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        LOG -level 1 -msg "\t\tcurrent_utc_offset_valid       -> $current_utc_offset_valid"
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        LOG -level 1 -msg "\t\tptp_timescale                  -> $ptp_timescale"
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        LOG -level 1 -msg "\t\ttime_traceable                 -> $time_traceable"
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        LOG -level 1 -msg "\t\tfreq_traceable                 -> $freq_traceable"
    } else {

        set freq_traceable 0
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
        LOG -level 1 -msg "\t\tsync_uncertain                 -> $sync_uncertain"
    } else {

        set sync_uncertain 0
    }

    #PTP PDELAY_RESP HEADER OPTIONS
    ptp_ha::print_auto_log "\tPTP Pdelay_resp Message Details"

    if {[info exists param(-request_receipt_timestamp_sec)]} {

        set request_receipt_timestamp_sec $param(-request_receipt_timestamp_sec)
        ptp_ha::print_auto_log "\t\trequest_receipt_timestamp_sec  -> $request_receipt_timestamp_sec"
    } else {

        set request_receipt_timestamp_sec 0
    }

    if {[info exists param(-request_receipt_timestamp_ns)]} {

        set request_receipt_timestamp_ns $param(-request_receipt_timestamp_ns)
        ptp_ha::print_auto_log "\t\trequest_receipt_timestamp_ns   -> $request_receipt_timestamp_ns"
    } else {

        set request_receipt_timestamp_ns 0
    }

    if {[info exists param(-requesting_port_number)]} {

        set requesting_port_number $param(-requesting_port_number)
        ptp_ha::print_auto_log "\t\trequesting_port_number         -> $requesting_port_number"
    } else {

        set requesting_port_number 0
    }

    if {[info exists param(-requesting_clock_identity)]} {

        set requesting_clock_identity $param(-requesting_clock_identity)
        ptp_ha::print_auto_log "\t\trequesting_clock_identity      -> 0x[dec2hex $requesting_clock_identity 16]"
    } else {

        set requesting_clock_identity 0
    }

    #CALL PLATFORM FUNCTION
    if {![pltLib::send_ptp_ha_pdelay_resp\
                -session_id                        $session_id\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -major_sdo_id                      $major_sdo_id\
                -message_type                      $message_type\
                -minor_version                     $minor_version\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -minor_sdo_id                      $minor_sdo_id\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -secure                            $secure\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_traceable                    $time_traceable\
                -freq_traceable                    $freq_traceable\
                -sync_uncertain                    $sync_uncertain\
                -correction_field                  $correction_field\
                -message_type_specific             $message_type_specific\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -request_receipt_timestamp_sec     $request_receipt_timestamp_sec\
                -request_receipt_timestamp_ns      $request_receipt_timestamp_ns\
                -requesting_port_number            $requesting_port_number\
                -requesting_clock_identity         $requesting_clock_identity\
                -instance                          $instance\
                -count                             $count\
                -interval                          $interval]} {

        return 0

    }

    return 1

}

################################################################################
#                                                                              #
#  PROCEDURE NAME         :  ptp_ha::send_pdelay_resp_followup                 #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Transmission port              #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   dest_mac                      : (Mandatory) Destination MAC dddress.       #
#                                               (e.g., 01:80:C2:00:00:20)      #
#                                                                              #
#   src_mac                       : (Mandatory) Source MAC address.            #
#                                               (e.g., 00:80:C2:00:00:21)      #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                               (Default : 88F7)               #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                               (Default : 0)                  #
#                                                                              #
#   vlan_priority                 : (Optional)  Vlan Priority                  #
#                                               (Default : 0)                  #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                               (Default : 224.0.1.129)        #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                               (Default : 0.0.0.0)            #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                               (Default : auto)               #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                               (Default : 320)                #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                               (Default : 320)                #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                               (Default : 62)                 #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                               (Default : 0)                  #
#                                                                              #
#   major_sdo_id                  : (Optional)  Major sdo id                   #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default : 10)                 #
#                                                                              #
#   minor_version                 : (Optional)  PTP version number             #
#                                               (Default : 2)                  #
#                                                                              #
#   version_ptp                   : (Optional)  PTP minor version number       #
#                                               (Default : 2)                  #
#                                                                              #
#   message_length                : (Optional)  PTP message length             #
#                                               (Default : 54)                 #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                               (Default : 0)                  #
#                                                                              #
#   minor_sdo_id                  : (Optional)  Minor Sdo id                   #
#                                               (Default : 0)                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                               (Default : 0)                  #
#                                                                              #
#   two_step_flag                 : (Optional)  Two Step Flag                  #
#                                               (Default : 0)                  #
#                                                                              #
#   unicast_flag                  : (Optional)  Unicast Flag                   #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   secure                        : (Optional)  Secure Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                               (Default : 0)                  #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP TimeSclae Flag             #
#                                               (Default : 0)                  #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                               (Default : 0)                  #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                               (Default : 0)                  #
#                                                                              #
#   sync_uncertain                : (Optional)  Synchronization uncertain Flag #
#                                               (Default : 0)                  #
#                                                                              #
#   correction_field              : (Optional)  Correction Field value         #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type_specific         : (Optional)  Message type                   #
#                                               (Default : 0)                  #
#                                                                              #
#   src_port_number               : (Optional)  Source Port number             #
#                                               (Default : 0)                  #
#                                                                              #
#   src_clock_identity            : (Optional)  Source Clock identity          #
#                                               (Default : 0)                  #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                               (Default : 0)                  #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                               (Default : 5)                  #
#                                                                              #
#   log_message_interval          : (Optional)  Log message interval           #
#                                               (Default : 127)                #
#                                                                              #
#   response_origin_timestamp_sec : (Optional)  Response Origin Timestamp Sec  #
#                                               (Default : 0)                  #
#                                                                              #
#   response_origin_timestamp_ns  : (Optional)  Response Origin Timestamp Ns   #
#                                               (Default : 0)                  #
#                                                                              #
#   requesting_port_number        : (Optional)  Requesting Port Number         #
#                                               (Default : 0)                  #
#                                                                              #
#   requesting_clock_identity     : (Optional)  Requesting Clock Identity      #
#                                               (Default : 0)                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default : 1)                  #
#                                                                              #
#   interval                      : (Optional)  Frame interval (in seconds)    #
#                                               (Default : 1)                  #
#                                                                              #
#  OUTPUT PARAMETERS      :  NONE                                              #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is successfully send   #
#                                         with the given parameters)           #
#                                                                              #
#                         : 0 on failure (If the packet could not be send      #
#                                         with the given parameters)           #
#                                                                              #
#  DEFINITION             : This function is used to construct and send        #
#                           PTP pdelay resp followup message                   #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       ptp_ha::send_pdelay_resp_followup\                                     #
#           -port_num                         $port_num\                       #
#           -dest_mac                         $dest_mac\                       #
#           -src_mac                          $src_mac\                        #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -vlan_priority                    $vlan_priority\                  #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -major_sdo_id                     $major_sdo_id\                   #
#           -message_type                     $message_type\                   #
#           -minor_version                    $minor_version\                  #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -minor_sdo_id                     $minor_sdo_id\                   #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -two_step_flag                    $two_step_flag\                  #
#           -unicast_flag                     $unicast_flag\                   #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -secure                           $secure\                         #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -sync_uncertain                   $sync_uncertain\                 #
#           -correction_field                 $correction_field\               #
#           -message_type_specific            $message_type_specific\          #
#           -src_port_number                  $src_port_number\                #
#           -src_clock_identity               $src_clock_identity\             #
#           -sequence_id                      $sequence_id\                    #
#           -control_field                    $control_field\                  #
#           -log_message_interval             $log_message_interval\           #
#           -response_origin_timestamp_sec    $response_origin_timestamp_sec\  #
#           -response_origin_timestamp_ns     $response_origin_timestamp_ns\   #
#           -requesting_port_number           $requesting_port_number\         #
#           -requesting_clock_identity        $requesting_clock_identity\      #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#                                                                              #
################################################################################
proc ptp_ha::send_pdelay_resp_followup {args} {

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "ptp_ha::send_pdelay_resp_followup: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #MISCELLANEOUS
    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
        ptp_ha::print_auto_log "\t\tport_num                       -> $port_num"
    } else {

        ERRLOG -msg "ptp_ha::send_pdelay_resp_followup: Missing Argument -> port_num"
        return 0
    }

    #Instance
    if {[info exists param(-instance)]} {

        set instance $param(-instance)
    } else {

        set instance 8
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
        ptp_ha::print_auto_log "\t\tcount                          -> $count"
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
        LOG -level 1 -msg "\t\tinterval                       -> $interval"
    } else {

        set interval [expr pow(2, $::ptp_sync_interval)]
        set interval 0
    }


    #ETHERNET HEADER OPTIONS
    ptp_ha::print_auto_log "\tEthernet Details"

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
        LOG -level 1 -msg "\t\tdest_mac                       -> $dest_mac"
    } else {

        get_dest_mac\
                     -port_num      $port_num\
                     -message       PDELAY_RESP_FOLLOW_UP\
                     -mac           dest_mac
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
        ptp_ha::print_auto_log "\t\tsrc_mac                        -> $src_mac"
    } else {

        ERRLOG -msg "ptp_ha::send_pdelay_resp_followup: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        LOG -level 1 -msg "\t\teth_type                       -> $eth_type"
    } else {

        if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
            set eth_type 0800
        } else {
            set eth_type 88F7
        }
    }

    #Vlan ID
    if { $::ptp_ha_vlan_encap == "true" } {

        ptp_ha::print_auto_log "\tVLAN Details"

        if {[info exists param(-vlan_id)]} {

            set vlan_id $param(-vlan_id)
            LOG -level 1 -msg "\t\tvlan_id                        -> $vlan_id"
        } else {

            set vlan_id $::ptp_ha_vlan_id
        }

        if {[info exists param(-vlan_priority)]} {

            set vlan_priority $param(-vlan_priority)
            LOG -level 1 -msg "\t\tvlan_priority                  -> $vlan_priority"
        } else {

            set vlan_priority $::ptp_ha_vlan_priority
        }
        
    } else {

        set vlan_id -1
        set vlan_priority 0
    }

    #IP HEADER OPTIONS
    if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {

        ptp_ha::print_auto_log "\tIP Details"

        #Destination IP address
        if {[info exists param(-dest_ip)]} {

            set dest_ip $param(-dest_ip)
            ptp_ha::print_auto_log "\t\tdest_ip                        -> $dest_ip"
        } else {

            get_dest_ip\
                         -port_num      $port_num\
                         -message       PDELAY_RESP_FOLLOW_UP\
                         -ip            dest_ip
        }

        #Source IP address
        if {[info exists param(-src_ip)]} {

            set src_ip $param(-src_ip)
            ptp_ha::print_auto_log "\t\tsrc_ip                         -> $src_ip"
        } else {

            set src_ip 0.0.0.0
        }

        #IP checksum
        if {[info exists param(-ip_checksum)]} {

            set ip_checksum $param(-ip_checksum)
            LOG -level 1 -msg "\t\tip_checksum                    -> $ip_checksum"
        } else {

            set ip_checksum auto
        }

        #UDP HEADER OPTIONS

        #UDP destination port number
        if {[info exists param(-dest_port)]} {

            set dest_port $param(-dest_port)
            LOG -level 1 -msg "\t\tdest_port                      -> $dest_port"
        } else {

            set dest_port 320
        }

        #UDP source port number
        if {[info exists param(-src_port)]} {

            set src_port $param(-src_port)
            LOG -level 1 -msg "\t\tsrc_port                       -> $src_port"
        } else {

            set src_port 320
        }

        #UDP length
        if {[info exists param(-udp_length)]} {

            set udp_length $param(-udp_length)
            LOG -level 1 -msg "\t\tudp_length                     -> $udp_length"
        } else {

            set udp_length 62
        }

        #UDP checksum
        if {[info exists param(-udp_checksum)]} {

            set udp_checksum $param(-udp_checksum)
            LOG -level 1 -msg "\t\tudp_checksum                   -> $udp_checksum"
        } else {

            set udp_checksum auto
        }
    } else {
        set dest_ip 0.0.0.0
        set src_ip 0.0.0.0
        set ip_checksum 0
        set dest_port 0
        set src_port 0
        set udp_length 0
        set udp_checksum 0
    }

    #PTP HEADER OPTIONS
    ptp_ha::print_auto_log "\tPTP Details"

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
        LOG -level 1 -msg "\t\tmajor_sdo_id                   -> $major_sdo_id"
    } else {

        set major_sdo_id 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        LOG -level 1 -msg "\t\tmessage_type                   -> $message_type"
    } else {

        set message_type 10
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
        LOG -level 1 -msg "\t\tminor_version                  -> $minor_version"
    } else {

        set minor_version 1
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        LOG -level 1 -msg "\t\tversion_ptp                    -> $version_ptp"
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        LOG -level 1 -msg "\t\tmessage_length                 -> $message_length"
    } else {

        set message_length 54
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        ptp_ha::print_auto_log "\t\tdomain_number                  -> $domain_number"
    } else {

        set domain_number 0
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
        LOG -level 1 -msg "\t\tminor_sdo_id                   -> $minor_sdo_id"
    } else {

        set minor_sdo_id 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
        ptp_ha::print_auto_log "\t\tcorrection_field               -> $correction_field"
    } else {

        set correction_field 0
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
        LOG -level 1 -msg "\t\tmessage_type_specific          -> $message_type_specific"
    } else {

        set message_type_specific 0
    }

    #Source Port Number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
        LOG -level 1 -msg "\t\tsrc_port_number                -> $src_port_number"
    } else {

        if {![regexp {.*?([0-9]+)$} $port_num match src_port_number]} {
            set src_port_number 0
        }

    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
        LOG -level 1 -msg "\t\tsrc_clock_identity             -> 0x[dec2hex $src_clock_identity 16]"
    } else {

        get_clock_identity\
                    -mac               $src_mac\
                    -format            $::ptp_clock_identity_format\
                    -clock_identity    src_clock_identity
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        ptp_ha::print_auto_log "\t\tsequence_id                    -> $sequence_id"
    } else {

        set sequence_id 0
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        LOG -level 1 -msg "\t\tcontrol_field                  -> $control_field"
    } else {

        set control_field 5
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
        LOG -level 1 -msg "\t\tlog_message_interval           -> $log_message_interval"
    } else {

        set log_message_interval 127
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    ptp_ha::print_auto_log "\tPTP Flag Details"
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        LOG -level 1 -msg "\t\talternate_master_flag          -> $alternate_master_flag"
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
        LOG -level 1 -msg "\t\ttwo_step_flag                  -> $two_step_flag"
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
        ptp_ha::print_auto_log "\t\tunicast_flag                   -> $unicast_flag"
    } else {

        set unicast_flag 0
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        LOG -level 1 -msg "\t\tprofile_specific1              -> $profile_specific1"
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        LOG -level 1 -msg "\t\tprofile_specific2              -> $profile_specific2"
    } else {

        set profile_specific2 0
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
        LOG -level 1 -msg "\t\tsecure                         -> $secure"
    } else {

        set secure 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        LOG -level 1 -msg "\t\tleap61                         -> $leap61"
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        LOG -level 1 -msg "\t\tleap59                         -> $leap59"
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        LOG -level 1 -msg "\t\tcurrent_utc_offset_valid       -> $current_utc_offset_valid"
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        LOG -level 1 -msg "\t\tptp_timescale                  -> $ptp_timescale"
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        LOG -level 1 -msg "\t\ttime_traceable                 -> $time_traceable"
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        LOG -level 1 -msg "\t\tfreq_traceable                 -> $freq_traceable"
    } else {

        set freq_traceable 0
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
        LOG -level 1 -msg "\t\tsync_uncertain                 -> $sync_uncertain"
    } else {

        set sync_uncertain 0
    }

    #PTP PDELAY_RESP_FOLLOWUP HEADER OPTIONS
    ptp_ha::print_auto_log "\tPTP Pdelay_resp_followup Message Details"

    if {[info exists param(-response_origin_timestamp_sec)]} {

        set response_origin_timestamp_sec $param(-response_origin_timestamp_sec)
        ptp_ha::print_auto_log "\t\tresponse_origin_timestamp_sec  -> $response_origin_timestamp_sec"
    } else {

        set response_origin_timestamp_sec 0
    }

    if {[info exists param(-response_origin_timestamp_ns)]} {

        set response_origin_timestamp_ns $param(-response_origin_timestamp_ns)
        ptp_ha::print_auto_log "\t\tresponse_origin_timestamp_ns   -> $response_origin_timestamp_ns"
    } else {

        set response_origin_timestamp_ns 0
    }

    if {[info exists param(-requesting_port_number)]} {

        set requesting_port_number $param(-requesting_port_number)
        ptp_ha::print_auto_log "\t\trequesting_port_number         -> $requesting_port_number"
    } else {

        set requesting_port_number 0
    }

    if {[info exists param(-requesting_clock_identity)]} {

        set requesting_clock_identity $param(-requesting_clock_identity)
        ptp_ha::print_auto_log "\t\trequesting_clock_identity      -> $requesting_clock_identity"
    } else {

        set requesting_clock_identity 0
    }

    #CALL PLATFORM FUNCTION
    if {![pltLib::send_ptp_ha_pdelay_resp_followup\
                -session_id                        $session_id\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -major_sdo_id                      $major_sdo_id\
                -message_type                      $message_type\
                -minor_version                     $minor_version\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -minor_sdo_id                      $minor_sdo_id\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -secure                            $secure\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_traceable                    $time_traceable\
                -freq_traceable                    $freq_traceable\
                -sync_uncertain                    $sync_uncertain\
                -correction_field                  $correction_field\
                -message_type_specific             $message_type_specific\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -response_origin_timestamp_sec     $response_origin_timestamp_sec\
                -response_origin_timestamp_ns      $response_origin_timestamp_ns\
                -requesting_port_number            $requesting_port_number\
                -requesting_clock_identity         $requesting_clock_identity\
                -instance                          $instance\
                -count                             $count\
                -interval                          $interval]} {

        return 0

    }

    return 1

}

################################################################################
#                                                                              #
#  PROCEDURE NAME         :  ptp_ha::send_signal                               #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Transmission port              #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   dest_mac                      : (Mandatory) Destination MAC dddress.       #
#                                               (e.g., 01:80:C2:00:00:20)      #
#                                                                              #
#   src_mac                       : (Mandatory) Source MAC address.            #
#                                               (e.g., 00:80:C2:00:00:21)      #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                               (Default : 88F7)               #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                               (Default : 0)                  #
#                                                                              #
#   vlan_priority                 : (Optional)  Vlan Priority                  #
#                                               (Default : 0)                  #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                               (Default : 224.0.1.129)        #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                               (Default : 0.0.0.0)            #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                               (Default : auto)               #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                               (Default : 320)                #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                               (Default : 320)                #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                               (Default : 96)                 #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                               (Default : 0)                  #
#                                                                              #
#   major_sdo_id                  : (Optional)  Major sdo id                   #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default : 12)                 #
#                                                                              #
#   minor_version                 : (Optional)  PTP version number             #
#                                               (Default : 2)                  #
#                                                                              #
#   version_ptp                   : (Optional)  PTP minor version number       #
#                                               (Default : 2)                  #
#                                                                              #
#   message_length                : (Optional)  PTP message length             #
#                                               (Default : 88)                 #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                               (Default : 0)                  #
#                                                                              #
#   minor_sdo_id                  : (Optional)  Minor Sdo id                   #
#                                               (Default : 0)                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                               (Default : 0)                  #
#                                                                              #
#   two_step_flag                 : (Optional)  Two Step Flag                  #
#                                               (Default : 0)                  #
#                                                                              #
#   unicast_flag                  : (Optional)  Unicast Flag                   #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   secure                        : (Optional)  Secure Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                               (Default : 0)                  #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP TimeSclae Flag             #
#                                               (Default : 0)                  #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                               (Default : 0)                  #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                               (Default : 0)                  #
#                                                                              #
#   sync_uncertain                : (Optional)  Synchronization uncertain Flag #
#                                               (Default : 0)                  #
#                                                                              #
#   correction_field              : (Optional)  Correction Field value         #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type_specific         : (Optional)  Message type                   #
#                                               (Default : 0)                  #
#                                                                              #
#   src_port_number               : (Optional)  Source Port number             #
#                                               (Default : 0)                  #
#                                                                              #
#   src_clock_identity            : (Optional)  Source Clock identity          #
#                                               (Default : 0)                  #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                               (Default : 0)                  #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                               (Default : 5)                  #
#                                                                              #
#   log_message_interval          : (Optional)  Log message interval           #
#                                               (Default : 127)                #
#                                                                              #
#   target_port_number            : (Optional)  Target Port Number             #
#                                               (Default : 0)                  #
#                                                                              #
#   target_clock_identity         : (Optional)  Target Clock Identity          #
#                                               (Default : 0)                  #
#                                                                              #
#   tlv                           : (Optional)  Tlv                            #
#                                               (Default : L1_SYNC)            #
#                                                                              #
#   tlv_type                      : (Optional)  Tlv Type                       #
#                                               (Default : 32769)              #
#                                                                              #
#   tlv_length                    : (Optional)  Tlv Length                     #
#                                               (Default : 30)                 #
#                                                                              #
#   ope                           : (Optional)  Ope                            #
#                                               (Default : 0)                  #
#                                                                              #
#   cr                            : (Optional)  Cr                             #
#                                               (Default : 0)                  #
#                                                                              #
#   rcr                           : (Optional)  Rcr                            #
#                                               (Default : 0)                  #
#                                                                              #
#   tcr                           : (Optional)  Tcr                            #
#                                               (Default : 0)                  #
#                                                                              #
#   ic                            : (Optional)  Ic                             #
#                                               (Default : 0)                  #
#                                                                              #
#   irc                           : (Optional)  Irc                            #
#                                               (Default : 0)                  #
#                                                                              #
#   itc                           : (Optional)  Itc                            #
#                                               (Default : 0)                  #
#                                                                              #
#   fov                           : (Optional)  Fov                            #
#                                               (Default : 0)                  #
#                                                                              #
#   pov                           : (Optional)  Pov                            #
#                                               (Default : 0)                  #
#                                                                              #
#   tct                           : (Optional)  Tct                            #
#                                               (Default : 0)                  #
#                                                                              #
#   phase_offset_tx               : (Optional)  Phase Offset Tx                #
#                                               (Default : 0)                  #
#                                                                              #
#   phase_offset_tx_timestamp_sec : (Optional)  Phase Offset Tx Timestamp Sec  #
#                                               (Default : 0)                  #
#                                                                              #
#   phase_offset_tx_timestamp_ns  : (Optional)  Phase Offset Tx Timestamp Ns   #
#                                               (Default : 0)                  #
#                                                                              #
#   freq_offset_tx                : (Optional)  Freq Offset Tx                 #
#                                               (Default : 0)                  #
#                                                                              #
#   freq_offset_tx_timestamp_sec  : (Optional)  Freq Offset Tx Timestamp Sec   #
#                                               (Default : 0)                  #
#                                                                              #
#   freq_offset_tx_timestamp_ns   : (Optional)  Freq Offset Tx Timestamp Ns    #
#                                               (Default : 0)                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default : 1)                  #
#                                                                              #
#   interval                      : (Optional)  Frame interval (in seconds)    #
#                                               (Default : 1)                  #
#                                                                              #
#  OUTPUT PARAMETERS      :  NONE                                              #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is successfully send   #
#                                         with the given parameters)           #
#                                                                              #
#                         : 0 on failure (If the packet could not be send      #
#                                         with the given parameters)           #
#                                                                              #
#  DEFINITION             : This function is used to construct and send        #
#                           PTP signal message                                 #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       ptp_ha::send_signal\                                                   #
#           -port_num                         $port_num\                       #
#           -dest_mac                         $dest_mac\                       #
#           -src_mac                          $src_mac\                        #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -vlan_priority                    $vlan_priority\                  #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -major_sdo_id                     $major_sdo_id\                   #
#           -message_type                     $message_type\                   #
#           -minor_version                    $minor_version\                  #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -minor_sdo_id                     $minor_sdo_id\                   #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -two_step_flag                    $two_step_flag\                  #
#           -unicast_flag                     $unicast_flag\                   #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -secure                           $secure\                         #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -sync_uncertain                   $sync_uncertain\                 #
#           -correction_field                 $correction_field\               #
#           -message_type_specific            $message_type_specific\          #
#           -src_port_number                  $src_port_number\                #
#           -src_clock_identity               $src_clock_identity\             #
#           -sequence_id                      $sequence_id\                    #
#           -control_field                    $control_field\                  #
#           -log_message_interval             $log_message_interval\           #
#           -target_port_number               $target_port_number\             #
#           -target_clock_identity            $target_clock_identity\          #
#           -tlv                              $tlv\                            #
#           -tlv_type                         $tlv_type\                       #
#           -tlv_length                       $tlv_length\                     #
#           -ope                              $ope\                            #
#           -cr                               $cr\                             #
#           -rcr                              $rcr\                            #
#           -tcr                              $tcr\                            #
#           -ic                               $ic\                             #
#           -irc                              $irc\                            #
#           -itc                              $itc\                            #
#           -fov                              $fov\                            #
#           -pov                              $pov\                            #
#           -tct                              $tct\                            #
#           -phase_offset_tx                  $phase_offset_tx\                #
#           -phase_offset_tx_timestamp_sec    $phase_offset_tx_timestamp_sec\  #
#           -phase_offset_tx_timestamp_ns     $phase_offset_tx_timestamp_ns\   #
#           -freq_offset_tx                   $freq_offset_tx\                 #
#           -freq_offset_tx_timestamp_sec     $freq_offset_tx_timestamp_sec\   #
#           -freq_offset_tx_timestamp_ns      $freq_offset_tx_timestamp_ns\    #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#                                                                              #
################################################################################
proc ptp_ha::send_signal {args} {

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "ptp_ha::send_signal: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #MISCELLANEOUS
    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
        LOG -level 1 -msg "\t\tport_num                       -> $port_num"
    } else {

        ERRLOG -msg "ptp_ha::send_signal: Missing Argument -> port_num"
        return 0
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
        LOG -level 1 -msg "\t\tcount                          -> $count"
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
        LOG -level 1 -msg "\t\tinterval                       -> $interval"
    } else {

        set interval [expr pow(2, $::ptp_sync_interval)]
    }


    #ETHERNET HEADER OPTIONS
    LOG -level 1 -msg "\tEthernet Details"

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
        LOG -level 1 -msg "\t\tdest_mac                       -> $dest_mac"
    } else {

        get_dest_mac\
                     -port_num      $port_num\
                     -message       SIGNALING\
                     -mac           dest_mac
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
        LOG -level 1 -msg "\t\tsrc_mac                        -> $src_mac"
    } else {

        ERRLOG -msg "ptp_ha::send_signal: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        LOG -level 1 -msg "\t\teth_type                       -> $eth_type"
    } else {

        if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
            set eth_type 0800
        } else {
            set eth_type 88F7
        }
    }

    #Vlan ID
    if { $::ptp_ha_vlan_encap == "true" } {

        LOG -level 1 -msg "\tVLAN Details"

        if {[info exists param(-vlan_id)]} {

            set vlan_id $param(-vlan_id)
            LOG -level 1 -msg "\t\tvlan_id                        -> $vlan_id"
        } else {

            set vlan_id $::ptp_ha_vlan_id
        }

        if {[info exists param(-vlan_priority)]} {

            set vlan_priority $param(-vlan_priority)
            LOG -level 1 -msg "\t\tvlan_priority                  -> $vlan_priority"
        } else {

            set vlan_priority $::ptp_ha_vlan_priority
        }
        
    } else {

        set vlan_id -1
        set vlan_priority 0
    }

    #IP HEADER OPTIONS
    if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {

        LOG -level 1 -msg "\tIP Details"

        #Destination IP address
        if {[info exists param(-dest_ip)]} {

            set dest_ip $param(-dest_ip)
            LOG -level 1 -msg "\t\tdest_ip                        -> $dest_ip"
        } else {

            get_dest_ip\
                         -port_num      $port_num\
                         -message       SIGNALING\
                         -ip            dest_ip
        }

        #Source IP address
        if {[info exists param(-src_ip)]} {

            set src_ip $param(-src_ip)
            LOG -level 1 -msg "\t\tsrc_ip                         -> $src_ip"
        } else {

            set src_ip 0.0.0.0
        }

        #IP checksum
        if {[info exists param(-ip_checksum)]} {

            set ip_checksum $param(-ip_checksum)
            LOG -level 1 -msg "\t\tip_checksum                    -> $ip_checksum"
        } else {

            set ip_checksum auto
        }

        #UDP HEADER OPTIONS

        #UDP destination port number
        if {[info exists param(-dest_port)]} {

            set dest_port $param(-dest_port)
            LOG -level 1 -msg "\t\tdest_port                      -> $dest_port"
        } else {

            set dest_port 320
        }

        #UDP source port number
        if {[info exists param(-src_port)]} {

            set src_port $param(-src_port)
            LOG -level 1 -msg "\t\tsrc_port                       -> $src_port"
        } else {

            set src_port 320
        }

        #UDP length
        if {[info exists param(-udp_length)]} {

            set udp_length $param(-udp_length)
            LOG -level 1 -msg "\t\tudp_length                     -> $udp_length"
        } else {

            set udp_length 96
        }

        #UDP checksum
        if {[info exists param(-udp_checksum)]} {

            set udp_checksum $param(-udp_checksum)
            LOG -level 1 -msg "\t\tudp_checksum                   -> $udp_checksum"
        } else {

            set udp_checksum auto
        }
    } else {
        set dest_ip 0.0.0.0
        set src_ip 0.0.0.0
        set ip_checksum 0
        set dest_port 0
        set src_port 0
        set udp_length 0
        set udp_checksum 0
    }

    #PTP HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Details"

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
        LOG -level 1 -msg "\t\tmajor_sdo_id                   -> $major_sdo_id"
    } else {

        set major_sdo_id 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        LOG -level 1 -msg "\t\tmessage_type                   -> $message_type"
    } else {

        set message_type 12
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
        LOG -level 1 -msg "\t\tminor_version                  -> $minor_version"
    } else {

        set minor_version 1
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        LOG -level 1 -msg "\t\tversion_ptp                    -> $version_ptp"
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        LOG -level 1 -msg "\t\tmessage_length                 -> $message_length"
    } else {

        set message_length 88
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        LOG -level 1 -msg "\t\tdomain_number                  -> $domain_number"
    } else {

        set domain_number 0
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
        LOG -level 1 -msg "\t\tminor_sdo_id                   -> $minor_sdo_id"
    } else {

        set minor_sdo_id 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
        LOG -level 1 -msg "\t\tcorrection_field               -> $correction_field"
    } else {

        set correction_field 0
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
        LOG -level 1 -msg "\t\tmessage_type_specific          -> $message_type_specific"
    } else {

        set message_type_specific 0
    }

    #Source Port Number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
        LOG -level 1 -msg "\t\tsrc_port_number                -> $src_port_number"
    } else {

        if {![regexp {.*?([0-9]+)$} $port_num match src_port_number]} {
            set src_port_number 0
        }

    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
        LOG -level 1 -msg "\t\tsrc_clock_identity             -> 0x[dec2hex $src_clock_identity 16]"
    } else {

        get_clock_identity\
                    -mac               $src_mac\
                    -format            $::ptp_clock_identity_format\
                    -clock_identity    src_clock_identity
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        LOG -level 1 -msg "\t\tsequence_id                    -> $sequence_id"
    } else {

        set sequence_id 0
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        LOG -level 1 -msg "\t\tcontrol_field                  -> $control_field"
    } else {

        set control_field 5
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
        LOG -level 1 -msg "\t\tlog_message_interval           -> $log_message_interval"
    } else {

        set log_message_interval 127
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    LOG -level 1 -msg "\tPTP Flag Details"
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        LOG -level 1 -msg "\t\talternate_master_flag          -> $alternate_master_flag"
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
        LOG -level 1 -msg "\t\ttwo_step_flag                  -> $two_step_flag"
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
        LOG -level 1 -msg "\t\tunicast_flag                   -> $unicast_flag"
    } else {

        set unicast_flag 0
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        LOG -level 1 -msg "\t\tprofile_specific1              -> $profile_specific1"
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        LOG -level 1 -msg "\t\tprofile_specific2              -> $profile_specific2"
    } else {

        set profile_specific2 0
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
        LOG -level 1 -msg "\t\tsecure                         -> $secure"
    } else {

        set secure 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        LOG -level 1 -msg "\t\tleap61                         -> $leap61"
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        LOG -level 1 -msg "\t\tleap59                         -> $leap59"
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        LOG -level 1 -msg "\t\tcurrent_utc_offset_valid       -> $current_utc_offset_valid"
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        LOG -level 1 -msg "\t\tptp_timescale                  -> $ptp_timescale"
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        LOG -level 1 -msg "\t\ttime_traceable                 -> $time_traceable"
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        LOG -level 1 -msg "\t\tfreq_traceable                 -> $freq_traceable"
    } else {

        set freq_traceable 0
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
        LOG -level 1 -msg "\t\tsync_uncertain                 -> $sync_uncertain"
    } else {

        set sync_uncertain 0
    }

    #PTP SIGNAL HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Signal Message Details"

    if {[info exists param(-target_port_number)]} {

        set target_port_number $param(-target_port_number)
        LOG -level 1 -msg "\t\ttarget_port_number             -> $target_port_number"
    } else {

        set target_port_number 0
    }

    if {[info exists param(-target_clock_identity)]} {

        set target_clock_identity $param(-target_clock_identity)
        LOG -level 1 -msg "\t\ttarget_clock_identity          -> 0x[dec2hex $target_clock_identity 16]"
    } else {

        set target_clock_identity 0
    }

    if {[info exists param(-tlv)]} {

        set tlv $param(-tlv)
        LOG -level 1 -msg "\t\ttlv                            -> $tlv"
    } else {

        set tlv L1_SYNC
    }

    if {[info exists param(-tlv_type)]} {

        set tlv_type $param(-tlv_type)
        LOG -level 1 -msg "\t\ttlv_type                       -> $tlv_type"
    } else {

        set tlv_type 32769
    }

    if {[info exists param(-tlv_length)]} {

        set tlv_length $param(-tlv_length)
        LOG -level 1 -msg "\t\ttlv_length                     -> $tlv_length"
    } else {

        set tlv_length 30
    }

    if {[info exists param(-ope)]} {

        set ope $param(-ope)
        LOG -level 1 -msg "\t\tope                            -> $ope"
    } else {

        set ope 0
    }

    if {[info exists param(-cr)]} {

        set cr $param(-cr)
        LOG -level 1 -msg "\t\tcr                             -> $cr"
    } else {

        set cr 0
    }

    if {[info exists param(-rcr)]} {

        set rcr $param(-rcr)
        LOG -level 1 -msg "\t\trcr                            -> $rcr"
    } else {

        set rcr 0
    }

    if {[info exists param(-tcr)]} {

        set tcr $param(-tcr)
        LOG -level 1 -msg "\t\ttcr                            -> $tcr"
    } else {

        set tcr 0
    }

    if {[info exists param(-ic)]} {

        set ic $param(-ic)
        LOG -level 1 -msg "\t\tic                             -> $ic"
    } else {

        set ic 0
    }

    if {[info exists param(-irc)]} {

        set irc $param(-irc)
        LOG -level 1 -msg "\t\tirc                            -> $irc"
    } else {

        set irc 0
    }

    if {[info exists param(-itc)]} {

        set itc $param(-itc)
        LOG -level 1 -msg "\t\titc                            -> $itc"
    } else {

        set itc 0
    }

    if {[info exists param(-fov)]} {

        set fov $param(-fov)
        LOG -level 1 -msg "\t\tfov                            -> $fov"
    } else {

        set fov 0
    }

    if {[info exists param(-pov)]} {

        set pov $param(-pov)
        LOG -level 1 -msg "\t\tpov                            -> $pov"
    } else {

        set pov 0
    }

    if {[info exists param(-tct)]} {

        set tct $param(-tct)
        LOG -level 1 -msg "\t\ttct                            -> $tct"
    } else {

        set tct 0
    }

    if {[info exists param(-phase_offset_tx)]} {

        set phase_offset_tx $param(-phase_offset_tx)
        LOG -level 1 -msg "\t\tphase_offset_tx                -> $phase_offset_tx"
    } else {

        set phase_offset_tx 0
    }

    if {[info exists param(-phase_offset_tx_timestamp_sec)]} {

        set phase_offset_tx_timestamp_sec $param(-phase_offset_tx_timestamp_sec)
        LOG -level 1 -msg "\t\tphase_offset_tx_timestamp_sec  -> $phase_offset_tx_timestamp_sec"
    } else {

        set phase_offset_tx_timestamp_sec 0
    }

    if {[info exists param(-phase_offset_tx_timestamp_ns)]} {

        set phase_offset_tx_timestamp_ns $param(-phase_offset_tx_timestamp_ns)
        LOG -level 1 -msg "\t\tphase_offset_tx_timestamp_ns   -> $phase_offset_tx_timestamp_ns"
    } else {

        set phase_offset_tx_timestamp_ns 0
    }

    if {[info exists param(-freq_offset_tx)]} {

        set freq_offset_tx $param(-freq_offset_tx)
        LOG -level 1 -msg "\t\tfreq_offset_tx                 -> $freq_offset_tx"
    } else {

        set freq_offset_tx 0
    }

    if {[info exists param(-freq_offset_tx_timestamp_sec)]} {

        set freq_offset_tx_timestamp_sec $param(-freq_offset_tx_timestamp_sec)
        LOG -level 1 -msg "\t\tfreq_offset_tx_timestamp_sec   -> $freq_offset_tx_timestamp_sec"
    } else {

        set freq_offset_tx_timestamp_sec 0
    }

    if {[info exists param(-freq_offset_tx_timestamp_ns)]} {

        set freq_offset_tx_timestamp_ns $param(-freq_offset_tx_timestamp_ns)
        LOG -level 1 -msg "\t\tfreq_offset_tx_timestamp_ns    -> $freq_offset_tx_timestamp_ns"
    } else {

        set freq_offset_tx_timestamp_ns 0
    }

    #CALL PLATFORM FUNCTION
    if {![pltLib::send_ptp_ha_signal\
                -session_id                        $session_id\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -major_sdo_id                      $major_sdo_id\
                -message_type                      $message_type\
                -minor_version                     $minor_version\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -minor_sdo_id                      $minor_sdo_id\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -secure                            $secure\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_traceable                    $time_traceable\
                -freq_traceable                    $freq_traceable\
                -sync_uncertain                    $sync_uncertain\
                -correction_field                  $correction_field\
                -message_type_specific             $message_type_specific\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -target_port_number                $target_port_number\
                -target_clock_identity             $target_clock_identity\
                -tlv                               $tlv\
                -tlv_type                          $tlv_type\
                -tlv_length                        $tlv_length\
                -ope                               $ope\
                -cr                                $cr\
                -rcr                               $rcr\
                -tcr                               $tcr\
                -ic                                $ic\
                -irc                               $irc\
                -itc                               $itc\
                -fov                               $fov\
                -pov                               $pov\
                -tct                               $tct\
                -phase_offset_tx                   $phase_offset_tx\
                -phase_offset_tx_timestamp_sec     $phase_offset_tx_timestamp_sec\
                -phase_offset_tx_timestamp_ns      $phase_offset_tx_timestamp_ns\
                -freq_offset_tx                    $freq_offset_tx\
                -freq_offset_tx_timestamp_sec      $freq_offset_tx_timestamp_sec\
                -freq_offset_tx_timestamp_ns       $freq_offset_tx_timestamp_ns\
                -count                             $count\
                -interval                          $interval]} {

        return 0

    }

    return 1

}

################################################################################
#                                                                              #
#  PROCEDURE NAME         :  ptp_ha::send_mgmt                                 #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Transmission port              #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   dest_mac                      : (Mandatory) Destination MAC dddress.       #
#                                               (e.g., 01:80:C2:00:00:20)      #
#                                                                              #
#   src_mac                       : (Mandatory) Source MAC address.            #
#                                               (e.g., 00:80:C2:00:00:21)      #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                               (Default : 88F7)               #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                               (Default : 0)                  #
#                                                                              #
#   vlan_priority                 : (Optional)  Vlan Priority                  #
#                                               (Default : 0)                  #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                               (Default : 224.0.1.129)        #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                               (Default : 0.0.0.0)            #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                               (Default : auto)               #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                               (Default : 320)                #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                               (Default : 320)                #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                               (Default : 64)                 #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                               (Default : 0)                  #
#                                                                              #
#   major_sdo_id                  : (Optional)  Major sdo id                   #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default : 13)                 #
#                                                                              #
#   minor_version                 : (Optional)  PTP version number             #
#                                               (Default : 2)                  #
#                                                                              #
#   version_ptp                   : (Optional)  PTP minor version number       #
#                                               (Default : 2)                  #
#                                                                              #
#   message_length                : (Optional)  PTP message length             #
#                                               (Default : 56)                 #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                               (Default : 0)                  #
#                                                                              #
#   minor_sdo_id                  : (Optional)  Minor Sdo id                   #
#                                               (Default : 0)                  #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                               (Default : 0)                  #
#                                                                              #
#   two_step_flag                 : (Optional)  Two Step Flag                  #
#                                               (Default : 0)                  #
#                                                                              #
#   unicast_flag                  : (Optional)  Unicast Flag                   #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                               (Default : 0)                  #
#                                                                              #
#   secure                        : (Optional)  Secure Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                               (Default : 0)                  #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                               (Default : 0)                  #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP TimeSclae Flag             #
#                                               (Default : 0)                  #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                               (Default : 0)                  #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                               (Default : 0)                  #
#                                                                              #
#   sync_uncertain                : (Optional)  Synchronization uncertain Flag #
#                                               (Default : 0)                  #
#                                                                              #
#   correction_field              : (Optional)  Correction Field value         #
#                                               (Default : 0)                  #
#                                                                              #
#   message_type_specific         : (Optional)  Message type                   #
#                                               (Default : 0)                  #
#                                                                              #
#   src_port_number               : (Optional)  Source Port number             #
#                                               (Default : 0)                  #
#                                                                              #
#   src_clock_identity            : (Optional)  Source Clock identity          #
#                                               (Default : 0)                  #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                               (Default : 0)                  #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                               (Default : 4)                  #
#                                                                              #
#   log_message_interval          : (Optional)  Log message interval           #
#                                               (Default : 127)                #
#                                                                              #
#   target_port_number            : (Optional)  Target Port Number             #
#                                               (Default : 0)                  #
#                                                                              #
#   target_clock_identity         : (Optional)  Target Clock Identity          #
#                                               (Default : 0)                  #
#                                                                              #
#   starting_boundary_hops        : (Optional)  Starting Boundary Hops         #
#                                               (Default : 0)                  #
#                                                                              #
#   boundary_hops                 : (Optional)  Boundary Hops                  #
#                                               (Default : 0)                  #
#                                                                              #
#   action_field                  : (Optional)  Action Field                   #
#                                               (Default : 0)                  #
#                                                                              #
#   tlv                           : (Optional)  Tlv                            #
#                                               (Default : MASTER_ONLY)        #
#                                                                              #
#   tlv_type                      : (Optional)  Tlv Type                       #
#                                               (Default : 1)                  #
#                                                                              #
#   tlv_length                    : (Optional)  Tlv Length                     #
#                                               (Default : 4)                  #
#                                                                              #
#   management_id                 : (Optional)  Management Id                  #
#                                               (Default : 12288)              #
#                                                                              #
#   ext_port_config               : (Optional)  Ext Port Config                #
#                                               (Default : 0)                  #
#                                                                              #
#   master_only                   : (Optional)  Master Only                    #
#                                               (Default : 0)                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default : 1)                  #
#                                                                              #
#   interval                      : (Optional)  Frame interval (in seconds)    #
#                                               (Default : 1)                  #
#                                                                              #
#  OUTPUT PARAMETERS      :  NONE                                              #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is successfully send   #
#                                         with the given parameters)           #
#                                                                              #
#                         : 0 on failure (If the packet could not be send      #
#                                         with the given parameters)           #
#                                                                              #
#  DEFINITION             : This function is used to construct and send        #
#                           PTP mgmt message                                   #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       ptp_ha::send_mgmt\                                                     #
#           -port_num                         $port_num\                       #
#           -dest_mac                         $dest_mac\                       #
#           -src_mac                          $src_mac\                        #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -vlan_priority                    $vlan_priority\                  #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -major_sdo_id                     $major_sdo_id\                   #
#           -message_type                     $message_type\                   #
#           -minor_version                    $minor_version\                  #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -minor_sdo_id                     $minor_sdo_id\                   #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -two_step_flag                    $two_step_flag\                  #
#           -unicast_flag                     $unicast_flag\                   #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -secure                           $secure\                         #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -sync_uncertain                   $sync_uncertain\                 #
#           -correction_field                 $correction_field\               #
#           -message_type_specific            $message_type_specific\          #
#           -src_port_number                  $src_port_number\                #
#           -src_clock_identity               $src_clock_identity\             #
#           -sequence_id                      $sequence_id\                    #
#           -control_field                    $control_field\                  #
#           -log_message_interval             $log_message_interval\           #
#           -target_port_number               $target_port_number\             #
#           -target_clock_identity            $target_clock_identity\          #
#           -starting_boundary_hops           $starting_boundary_hops\         #
#           -boundary_hops                    $boundary_hops\                  #
#           -action_field                     $action_field\                   #
#           -tlv                              $tlv\                            #
#           -tlv_type                         $tlv_type\                       #
#           -tlv_length                       $tlv_length\                     #
#           -management_id                    $management_id\                  #
#           -ext_port_config                  $ext_port_config\                #
#           -master_only                      $master_only\                    #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#                                                                              #
################################################################################
proc ptp_ha::send_mgmt {args} {

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "ptp_ha::send_mgmt: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #MISCELLANEOUS
    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
        LOG -level 1 -msg "\t\tport_num                       -> $port_num"
    } else {

        ERRLOG -msg "ptp_ha::send_mgmt: Missing Argument -> port_num"
        return 0
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
        LOG -level 1 -msg "\t\tcount                          -> $count"
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
        LOG -level 1 -msg "\t\tinterval                       -> $interval"
    } else {

        set interval [expr pow(2, $::ptp_sync_interval)]
    }


    #ETHERNET HEADER OPTIONS
    LOG -level 1 -msg "\tEthernet Details"

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
        LOG -level 1 -msg "\t\tdest_mac                       -> $dest_mac"
    } else {

        get_dest_mac\
                     -port_num      $port_num\
                     -message       MANAGEMENT\
                     -mac           dest_mac
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
        LOG -level 1 -msg "\t\tsrc_mac                        -> $src_mac"
    } else {

        ERRLOG -msg "ptp_ha::send_mgmt: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        LOG -level 1 -msg "\t\teth_type                       -> $eth_type"
    } else {

        if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
            set eth_type 0800
        } else {
            set eth_type 88F7
        }
    }

    #Vlan ID
    if { $::ptp_ha_vlan_encap == "true" } {

        LOG -level 1 -msg "\tVLAN Details"

        if {[info exists param(-vlan_id)]} {

            set vlan_id $param(-vlan_id)
            LOG -level 1 -msg "\t\tvlan_id                        -> $vlan_id"
        } else {

            set vlan_id $::ptp_ha_vlan_id
        }

        if {[info exists param(-vlan_priority)]} {

            set vlan_priority $param(-vlan_priority)
            LOG -level 1 -msg "\t\tvlan_priority                  -> $vlan_priority"
        } else {

            set vlan_priority $::ptp_ha_vlan_priority
        }
        
    } else {

        set vlan_id -1
        set vlan_priority 0
    }

    #IP HEADER OPTIONS
    if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {

        LOG -level 1 -msg "\tIP Details"

        #Destination IP address
        if {[info exists param(-dest_ip)]} {

            set dest_ip $param(-dest_ip)
            LOG -level 1 -msg "\t\tdest_ip                        -> $dest_ip"
        } else {

            get_dest_ip\
                         -port_num      $port_num\
                         -message       MANAGEMENT\
                         -ip            dest_ip
        }

        #Source IP address
        if {[info exists param(-src_ip)]} {

            set src_ip $param(-src_ip)
            LOG -level 1 -msg "\t\tsrc_ip                         -> $src_ip"
        } else {

            set src_ip 0.0.0.0
        }

        #IP checksum
        if {[info exists param(-ip_checksum)]} {

            set ip_checksum $param(-ip_checksum)
            LOG -level 1 -msg "\t\tip_checksum                    -> $ip_checksum"
        } else {

            set ip_checksum auto
        }

        #UDP HEADER OPTIONS

        #UDP destination port number
        if {[info exists param(-dest_port)]} {

            set dest_port $param(-dest_port)
            LOG -level 1 -msg "\t\tdest_port                      -> $dest_port"
        } else {

            set dest_port 320
        }

        #UDP source port number
        if {[info exists param(-src_port)]} {

            set src_port $param(-src_port)
            LOG -level 1 -msg "\t\tsrc_port                       -> $src_port"
        } else {

            set src_port 320
        }

        #UDP length
        if {[info exists param(-udp_length)]} {

            set udp_length $param(-udp_length)
            LOG -level 1 -msg "\t\tudp_length                     -> $udp_length"
        } else {

            set udp_length 64
        }

        #UDP checksum
        if {[info exists param(-udp_checksum)]} {

            set udp_checksum $param(-udp_checksum)
            LOG -level 1 -msg "\t\tudp_checksum                   -> $udp_checksum"
        } else {

            set udp_checksum auto
        }
    } else {
        set dest_ip 0.0.0.0
        set src_ip 0.0.0.0
        set ip_checksum 0
        set dest_port 0
        set src_port 0
        set udp_length 0
        set udp_checksum 0
    }

    #PTP HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Details"

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
        LOG -level 1 -msg "\t\tmajor_sdo_id                   -> $major_sdo_id"
    } else {

        set major_sdo_id 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        LOG -level 1 -msg "\t\tmessage_type                   -> $message_type"
    } else {

        set message_type 13
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
        LOG -level 1 -msg "\t\tminor_version                  -> $minor_version"
    } else {

        set minor_version 1
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        LOG -level 1 -msg "\t\tversion_ptp                    -> $version_ptp"
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        LOG -level 1 -msg "\t\tmessage_length                 -> $message_length"
    } else {

        set message_length 56
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        LOG -level 1 -msg "\t\tdomain_number                  -> $domain_number"
    } else {

        set domain_number 0
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
        LOG -level 1 -msg "\t\tminor_sdo_id                   -> $minor_sdo_id"
    } else {

        set minor_sdo_id 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
        LOG -level 1 -msg "\t\tcorrection_field               -> $correction_field"
    } else {

        set correction_field 0
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
        LOG -level 1 -msg "\t\tmessage_type_specific          -> $message_type_specific"
    } else {

        set message_type_specific 0
    }

    #Source Port Number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
        LOG -level 1 -msg "\t\tsrc_port_number                -> $src_port_number"
    } else {

        if {![regexp {.*?([0-9]+)$} $port_num match src_port_number]} {
            set src_port_number 0
        }

    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
        LOG -level 1 -msg "\t\tsrc_clock_identity             -> 0x[dec2hex $src_clock_identity 16]"
    } else {

        get_clock_identity\
                    -mac               $src_mac\
                    -format            $::ptp_clock_identity_format\
                    -clock_identity    src_clock_identity
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        LOG -level 1 -msg "\t\tsequence_id                    -> $sequence_id"
    } else {

        set sequence_id 0
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        LOG -level 1 -msg "\t\tcontrol_field                  -> $control_field"
    } else {

        set control_field 0
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
        LOG -level 1 -msg "\t\tlog_message_interval           -> $log_message_interval"
    } else {

        set log_message_interval 127
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    LOG -level 1 -msg "\tPTP Flag Details"
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        LOG -level 1 -msg "\t\talternate_master_flag          -> $alternate_master_flag"
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
        LOG -level 1 -msg "\t\ttwo_step_flag                  -> $two_step_flag"
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
        LOG -level 1 -msg "\t\tunicast_flag                   -> $unicast_flag"
    } else {

        set unicast_flag 0
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        LOG -level 1 -msg "\t\tprofile_specific1              -> $profile_specific1"
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        LOG -level 1 -msg "\t\tprofile_specific2              -> $profile_specific2"
    } else {

        set profile_specific2 0
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
        LOG -level 1 -msg "\t\tsecure                         -> $secure"
    } else {

        set secure 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        LOG -level 1 -msg "\t\tleap61                         -> $leap61"
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        LOG -level 1 -msg "\t\tleap59                         -> $leap59"
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        LOG -level 1 -msg "\t\tcurrent_utc_offset_valid       -> $current_utc_offset_valid"
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        LOG -level 1 -msg "\t\tptp_timescale                  -> $ptp_timescale"
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        LOG -level 1 -msg "\t\ttime_traceable                 -> $time_traceable"
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        LOG -level 1 -msg "\t\tfreq_traceable                 -> $freq_traceable"
    } else {

        set freq_traceable 0
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
        LOG -level 1 -msg "\t\tsync_uncertain                 -> $sync_uncertain"
    } else {

        set sync_uncertain 0
    }

    #PTP MGMT HEADER OPTIONS
    LOG -level 1 -msg "\tPTP Mgmt Message Details"

    if {[info exists param(-target_port_number)]} {

        set target_port_number $param(-target_port_number)
        LOG -level 1 -msg "\t\ttarget_port_number             -> $target_port_number"
    } else {

        set target_port_number 0
    }

    if {[info exists param(-target_clock_identity)]} {

        set target_clock_identity $param(-target_clock_identity)
        LOG -level 1 -msg "\t\ttarget_clock_identity          -> 0x[dec2hex $target_clock_identity 16]"
    } else {

        set target_clock_identity 0
    }

    if {[info exists param(-starting_boundary_hops)]} {

        set starting_boundary_hops $param(-starting_boundary_hops)
        LOG -level 1 -msg "\t\tstarting_boundary_hops         -> $starting_boundary_hops"
    } else {

        set starting_boundary_hops 0
    }

    if {[info exists param(-boundary_hops)]} {

        set boundary_hops $param(-boundary_hops)
        LOG -level 1 -msg "\t\tboundary_hops                  -> $boundary_hops"
    } else {

        set boundary_hops 0
    }

    if {[info exists param(-action_field)]} {

        set action_field $param(-action_field)
        LOG -level 1 -msg "\t\taction_field                   -> $action_field"
    } else {

        set action_field 0
    }

    if {[info exists param(-tlv)]} {

        set tlv $param(-tlv)
        LOG -level 1 -msg "\t\ttlv                            -> $tlv"
    } else {

        set tlv MASTER_ONLY
    }

    if {[info exists param(-tlv_type)]} {

        set tlv_type $param(-tlv_type)
        LOG -level 1 -msg "\t\ttlv_type                       -> $tlv_type"
    } else {

        set tlv_type 1
    }

    if {[info exists param(-tlv_length)]} {

        set tlv_length $param(-tlv_length)
        LOG -level 1 -msg "\t\ttlv_length                     -> $tlv_length"
    } else {

        set tlv_length 4
    }

    if {[info exists param(-management_id)]} {

        set management_id $param(-management_id)
        LOG -level 1 -msg "\t\tmanagement_id                  -> $management_id"
    } else {

        set management_id 12288
    }

    if {[info exists param(-ext_port_config)]} {

        set ext_port_config $param(-ext_port_config)
        LOG -level 1 -msg "\t\text_port_config                -> $ext_port_config"
    } else {

        set ext_port_config 0
    }

    if {[info exists param(-master_only)]} {

        set master_only $param(-master_only)
        LOG -level 1 -msg "\t\tmaster_only                    -> $master_only"
    } else {

        set master_only 0
    }

    #CALL PLATFORM FUNCTION
    if {![pltLib::send_ptp_ha_mgmt\
                -session_id                        $session_id\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -major_sdo_id                      $major_sdo_id\
                -message_type                      $message_type\
                -minor_version                     $minor_version\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -minor_sdo_id                      $minor_sdo_id\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -secure                            $secure\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_traceable                    $time_traceable\
                -freq_traceable                    $freq_traceable\
                -sync_uncertain                    $sync_uncertain\
                -correction_field                  $correction_field\
                -message_type_specific             $message_type_specific\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -target_port_number                $target_port_number\
                -target_clock_identity             $target_clock_identity\
                -starting_boundary_hops            $starting_boundary_hops\
                -boundary_hops                     $boundary_hops\
                -action_field                      $action_field\
                -tlv                               $tlv\
                -tlv_type                          $tlv_type\
                -tlv_length                        $tlv_length\
                -management_id                     $management_id\
                -ext_port_config                   $ext_port_config\
                -master_only                       $master_only\
                -count                             $count\
                -interval                          $interval]} {

        return 0

    }

    return 1

}

################################################################################
#                                                                              #
#  PROCEDURE NAME         :  ptp_ha::recv_sync                                 #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Reception port                 #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   filter_id                     : (Mandatory) Filter id                      #
#                                                                              #
#   reset_count                   : (Optional)  Reset Count                    #
#                                               (Default: ON)                  #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                                                              #
#   major_sdo_id                  : (Optional)  Major sdo id                   #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default: 0)                   #
#                                                                              #
#   minor_version                 : (Optional)  PTP version number             #
#                                                                              #
#   version_ptp                   : (Optional)  PTP minor version number       #
#                                                                              #
#   message_length                : (Optional)  PTP Annouce message length     #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                                                              #
#   minor_sdo_id                  : (Optional)  Minor Sdo id                   #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                                                              #
#   secure                        : (Optional)  Secure Flag                    #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP Timescale Flag             #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                                                              #
#   sync_uncertain                : (Optional)  Synchronization uncertain Flag #
#                                                                              #
#   message_type_specific         : (Optional)  Message type                   #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default: 1)                   #
#                                                                              #
#   timeout                       : (Optional)  Frame timeout (in seconds)     #
#                                               (Default: 5)                   #
#                                                                              #
#  OUTPUT PARAMETERS      :                                                    #
#                                                                              #
#   recvd_dest_mac                : (Optional)  Destination MAC dddress.       #
#                                                                              #
#   recvd_src_mac                 : (Optional)  Source MAC address.            #
#                                                                              #
#   recvd_two_step_flag           : (Optional)  Two Step Flag                  #
#                                                                              #
#   recvd_unicast_flag            : (Optional)  Unicast Flag                   #
#                                                                              #
#   recvd_correction_field        : (Optional)  Correction Field value         #
#                                                                              #
#   recvd_src_port_number         : (Optional)  Source port number             #
#                                                                              #
#   recvd_src_clock_identity      : (Optional)  Source clock identity          #
#                                                                              #
#   recvd_sequence_id             : (Optional)  Sequence ID                    #
#                                                                              #
#   recvd_log_message_interval    : (Optional)  Log message interval           #
#                                                                              #
#   recvd_origin_timestamp_sec    : (Optional)  Recvd Origin Timestamp Sec     #
#                                                                              #
#   recvd_origin_timestamp_ns     : (Optional)  Recvd Origin Timestamp Ns      #
#                                                                              #
#   rx_timestamp_sec              : (Optional)  Packet's rx time in seconds    #
#                                                                              #
#   rx_timestamp_ns               : (Optional)  Packet's rx time in ns         #
#                                                                              #
#   error_reason                  : (Optional)  Error Reason                   #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is received as         #
#                                         expected)                            #
#                                                                              #
#                         : 0 on failure (If the packet is not received        #
#                                         as expected)                         #
#                                                                              #
#  DEFINITION             : This function is used to receive                   #
#                           PTP sync message                                   #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       ptp_ha::recv_sync\                                                     #
#           -port_num                         $port_num\                       #
#           -filter_id                        $filter_id\                      #
#           -reset_count                      $reset_count\                    #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -major_sdo_id                     $major_sdo_id\                   #
#           -message_type                     $message_type\                   #
#           -minor_version                    $minor_version\                  #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -minor_sdo_id                     $minor_sdo_id\                   #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -secure                           $secure\                         #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -sync_uncertain                   $sync_uncertain\                 #
#           -message_type_specific            $message_type_specific\          #
#           -control_field                    $control_field\                  #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#           -recvd_dest_mac                   recvd_dest_mac\                  #
#           -recvd_src_mac                    recvd_src_mac\                   #
#           -recvd_two_step_flag              recvd_two_step_flag\             #
#           -recvd_unicast_flag               recvd_unicast_flag\              #
#           -recvd_correction_field           recvd_correction_field\          #
#           -recvd_src_port_number            recvd_src_port_identity\         #
#           -recvd_src_clock_identity         recvd_src_clock_identity\        #
#           -recvd_sequence_id                recvd_sequence_id\               #
#           -recvd_log_message_interval       recvd_message_interval\          #
#           -recvd_origin_timestamp_sec       recvd_origin_timestamp_sec\      #
#           -recvd_origin_timestamp_ns        recvd_origin_timestamp_ns\       #
#           -rx_timestamp_sec                 rx_timestamp_sec\                #
#           -rx_timestamp_ns                  rx_timestamp_ns\                 #
#           -error_reason                     error_reason                     #
#                                                                              #
################################################################################
proc ptp_ha::recv_sync {args} {

    array unset ::validation_param
    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "ptp_ha::recv_sync: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #Reception port
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "ptp_ha::recv_sync: Missing Argument -> port_num"
        return 0
    }

    #Filter id
    if {[info exists param(-filter_id)]} {

        set filter_id $param(-filter_id)
    } else {

        ERRLOG -msg "ptp_ha::recv_sync: Missing Argument -> filter_id"
        return 0
    }

    #Reset Count
    if {[info exists param(-reset_count)]} {

        set reset_count $param(-reset_count)
    } else {

        set reset_count ON
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        set ::validation_param(eth_type) $param(-eth_type)
    } else {

        set ::validation_param(eth_type) "dont_care"
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
        set ::validation_param(vlan_id) $param(-vlan_id)
    } else {

        if { $::ptp_ha_vlan_encap == "true" } {
            set ::validation_param(vlan_id) $::ptp_ha_vlan_id
        } else {
            set ::validation_param(vlan_id) "dont_care"
        }
    }

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
        set ::validation_param(dest_ip) $param(-dest_ip)
    } else {

        set ::validation_param(dest_ip) "dont_care"
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
        set ::validation_param(src_ip) $param(-src_ip)
    } else {

        set ::validation_param(src_ip) "dont_care"
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
        set ::validation_param(ip_checksum) $param(-ip_checksum)
    } else {

        set ::validation_param(ip_checksum) "dont_care"
    }

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
        set ::validation_param(dest_port) $param(-dest_port)
    } else {

        set ::validation_param(dest_port) "dont_care"
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
        set ::validation_param(src_port) $param(-src_port)
    } else {

        set ::validation_param(src_port) "dont_care"
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
        set ::validation_param(udp_length) $param(-udp_length)
    } else {

        set ::validation_param(udp_length) "dont_care"
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
        set ::validation_param(udp_checksum) $param(-udp_checksum)
    } else {

        set ::validation_param(udp_checksum) "dont_care"
    }

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
        set ::validation_param(major_sdo_id) $param(-major_sdo_id)
    } else {

        set ::validation_param(major_sdo_id) "dont_care"
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        set ::validation_param(message_type) $param(-message_type)
    } else {

        set ::validation_param(message_type) 0
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
        set ::validation_param(minor_version) $param(-minor_version)
    } else {

        set ::validation_param(minor_version) "dont_care"
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        set ::validation_param(version_ptp) $param(-version_ptp)
    } else {

        set ::validation_param(version_ptp) "dont_care"
    }

    #PTP Annouce message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        set ::validation_param(message_length) $param(-message_length)
    } else {

        set ::validation_param(message_length) "dont_care"
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        set ::validation_param(domain_number) $param(-domain_number)
    } else {

        set ::validation_param(domain_number) "dont_care"
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
        set ::validation_param(minor_sdo_id) $param(-minor_sdo_id)
    } else {

        set ::validation_param(minor_sdo_id) "dont_care"
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        set ::validation_param(alternate_master_flag) $param(-alternate_master_flag)
    } else {

        set ::validation_param(alternate_master_flag) "dont_care"
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        set ::validation_param(profile_specific1) $param(-profile_specific1)
    } else {

        set ::validation_param(profile_specific1) "dont_care"
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        set ::validation_param(profile_specific2) $param(-profile_specific2)
    } else {

        set ::validation_param(profile_specific2) "dont_care"
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
        set ::validation_param(secure) $param(-secure)
    } else {

        set ::validation_param(secure) "dont_care"
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        set ::validation_param(leap61) $param(-leap61)
    } else {

        set ::validation_param(leap61) "dont_care"
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        set ::validation_param(leap59) $param(-leap59)
    } else {

        set ::validation_param(leap59) "dont_care"
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        set ::validation_param(current_utc_offset_valid) $param(-current_utc_offset_valid)
    } else {

        set ::validation_param(current_utc_offset_valid) "dont_care"
    }

    #PTP Timescale Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        set ::validation_param(ptp_timescale) $param(-ptp_timescale)
    } else {

        set ::validation_param(ptp_timescale) "dont_care"
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        set ::validation_param(time_traceable) $param(-time_traceable)
    } else {

        set ::validation_param(time_traceable) "dont_care"
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        set ::validation_param(freq_traceable) $param(-freq_traceable)
    } else {

        set ::validation_param(freq_traceable) "dont_care"
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
        set ::validation_param(sync_uncertain) $param(-sync_uncertain)
    } else {

        set ::validation_param(sync_uncertain) "dont_care"
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
        set ::validation_param(message_type_specific) $param(-message_type_specific)
    } else {

        set ::validation_param(message_type_specific) "dont_care"
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        set ::validation_param(sequence_id) $param(-sequence_id)
    } else {

        set ::validation_param(sequence_id) "dont_care"
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        set ::validation_param(control_field) $param(-control_field)
    } else {

        set ::validation_param(control_field) "dont_care"
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame timeout
    if {[info exists param(-timeout)]} {

        set timeout $param(-timeout)
    } else {

        set timeout $::ptp_ha_sync_timeout
    }

    #OUTPUT PARAMETERS
    #all the fields in the matched packet
    foreach key [array names param] {

        set var [string trimleft $key -]

        if [regexp {^recvd_.*} $var] {

            upvar [set param($key)] $var
            set $var ""
        }
    }

    #Received packet timestamp in seconds
    if {[info exists param(-rx_timestamp_sec)]} {

        upvar $param(-rx_timestamp_sec) rx_timestamp_sec
        set rx_timestamp_sec ""
    }

    #Received packet timestamp in nanoseconds
    if {[info exists param(-rx_timestamp_ns)]} {

        upvar $param(-rx_timestamp_ns) rx_timestamp_ns
        set rx_timestamp_ns ""
    }

    #Error Reason
    if {[info exists param(-error_reason)]} {

        upvar $param(-error_reason) error_reason
        set error_reason ""
    }

    #LOGIC GOES HERE
    #Timeout in millisec
    set timeout [expr int($timeout*1000)]
    set error_code 0

    printTimeout $timeout

    set result [pltLib::recv_periodic\
            -session_id           $session_id\
            -port_num             $port_num\
            -filter_id            $filter_id\
            -timeout              $timeout\
            -reset_count          $reset_count\
            -validation_handler   "PTP_HA"\
            -error_code           error_code\
            -rx_timestamp         rx_timestamp_sec\
            -rx_time_nanoseconds  rx_timestamp_ns\
            -pkt_content          pkt_content\
            -pkt_size             pkt_size]

    set error_reason [ptp_ha::tee_recv_error_reason $error_code]

    if {[info exists pkt_content]} {

        set hex $pkt_content

        pbLib::decode_ptp_ha_pkt\
                 -packet                         $hex\
                 -dest_mac                       recvd_dest_mac\
                 -src_mac                        recvd_src_mac\
                 -eth_type                       recvd_eth_type\
                 -vlan_id                        recvd_vlan_id\
                 -vlan_priority                  recvd_vlan_priority\
                 -vlan_dei                       recvd_vlan_dei\
                 -src_ip                         recvd_src_ip\
                 -dest_ip                        recvd_dest_ip\
                 -src_port                       recvd_src_port\
                 -dest_port                      recvd_dest_port\
                 -udp_length                     recvd_udp_length\
                 -udp_checksum                   recvd_udp_checksum\
                 -major_sdo_id                   recvd_major_sdo_id\
                 -message_type                   recvd_message_type\
                 -minor_version                  recvd_minor_version\
                 -version_ptp                    recvd_version_ptp\
                 -message_length                 recvd_message_length\
                 -domain_number                  recvd_domain_number\
                 -minor_sdo_id                   recvd_minor_sdo_id\
                 -alternate_master_flag          recvd_alternate_master_flag\
                 -two_step_flag                  recvd_two_step_flag\
                 -unicast_flag                   recvd_unicast_flag\
                 -profile_specific1              recvd_profile_specific1\
                 -profile_specific2              recvd_profile_specific2\
                 -secure                         recvd_secure\
                 -leap61                         recvd_leap61\
                 -leap59                         recvd_leap59\
                 -current_utc_offset_valid       recvd_current_utc_offset_valid\
                 -ptp_timescale                  recvd_ptp_timescale\
                 -time_traceable                 recvd_time_traceable\
                 -freq_traceable                 recvd_freq_traceable\
                 -sync_uncertain                 recvd_sync_uncertain\
                 -correction_field               recvd_correction_field\
                 -message_type_specific          recvd_message_type_specific\
                 -src_port_number                recvd_src_port_number\
                 -src_clock_identity             recvd_src_clock_identity\
                 -sequence_id                    recvd_sequence_id\
                 -control_field                  recvd_control_field\
                 -log_message_interval           recvd_log_message_interval\
                 -origin_timestamp_sec           recvd_origin_timestamp_sec\
                 -origin_timestamp_ns            recvd_origin_timestamp_ns

    }

    return $result

}


################################################################################
#                                                                              #
#  PROCEDURE NAME         :  ptp_ha::recv_announce                             #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Reception port                 #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   filter_id                     : (Mandatory) Filter id                      #
#                                                                              #
#   reset_count                   : (Optional)  Reset Count                    #
#                                               (Default: ON)                  #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                                                              #
#   major_sdo_id                  : (Optional)  Major sdo id                   #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default: 11)                  #
#                                                                              #
#   minor_version                 : (Optional)  PTP version number             #
#                                                                              #
#   version_ptp                   : (Optional)  PTP minor version number       #
#                                                                              #
#   message_length                : (Optional)  PTP Annouce message length     #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                                                              #
#   minor_sdo_id                  : (Optional)  Minor Sdo id                   #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                                                              #
#   unicast_flag                  : (Optional)  Unicast Flag                   #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                                                              #
#   secure                        : (Optional)  Secure Flag                    #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP Timescale Flag             #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                                                              #
#   sync_uncertain                : (Optional)  Synchronization uncertain Flag #
#                                                                              #
#   message_type_specific         : (Optional)  Message type                   #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                                                              #
#   log_message_interval          : (Optional)  Log Message Interval           #
#                                                                              #
#   current_utc_offset            : (Optional)  Current Utc Offset             #
#                                                                              #
#   steps_removed                 : (Optional)  Steps Removed                  #
#                                                                              #
#   time_source                   : (Optional)  Time Source                    #
#                                                                              #
#   gm_clock_classy               : (Optional)  Gm Clock Classy                #
#                                                                              #
#   gm_clock_accuracy             : (Optional)  Gm Clock Accuracy              #
#                                                                              #
#   gm_clock_variance             : (Optional)  Gm Clock Variance              #
#                                                                              #
#   gm_priority1                  : (Optional)  Gm Priority1                   #
#                                                                              #
#   gm_priority2                  : (Optional)  Gm Priority2                   #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default: 1)                   #
#                                                                              #
#   timeout                       : (Optional)  Frame timeout (in seconds)     #
#                                               (Default: 5)                   #
#                                                                              #
#  OUTPUT PARAMETERS      :                                                    #
#                                                                              #
#   recvd_dest_mac                : (Optional)  Destination MAC dddress.       #
#                                                                              #
#   recvd_src_mac                 : (Optional)  Source MAC address.            #
#                                                                              #
#   recvd_two_step_flag           : (Optional)  Two Step Flag                  #
#                                                                              #
#   recvd_unicast_flag            : (Optional)  Unicast Flag                   #
#                                                                              #
#   recvd_correction_field        : (Optional)  Correction Field value         #
#                                                                              #
#   recvd_src_port_number         : (Optional)  Source port number             #
#                                                                              #
#   recvd_src_clock_identity      : (Optional)  Source clock identity          #
#                                                                              #
#   recvd_sequence_id             : (Optional)  Sequence ID                    #
#                                                                              #
#   recvd_log_message_interval    : (Optional)  Log message interval           #
#                                                                              #
#   recvd_origin_timestamp_sec    : (Optional)  Recvd Origin Timestamp Sec     #
#                                                                              #
#   recvd_origin_timestamp_ns     : (Optional)  Recvd Origin Timestamp Ns      #
#                                                                              #
#   recvd_gm_priority1            : (Optional)  Recvd Gm Priority1             #
#                                                                              #
#   recvd_gm_priority2            : (Optional)  Recvd Gm Priority2             #
#                                                                              #
#   recvd_gm_identity             : (Optional)  Recvd Gm Identity              #
#                                                                              #
#   rx_timestamp_sec              : (Optional)  Packet's rx time in seconds    #
#                                                                              #
#   rx_timestamp_ns               : (Optional)  Packet's rx time in ns         #
#                                                                              #
#   error_reason                  : (Optional)  Error Reason                   #
#                                                                              #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is received as         #
#                                         expected)                            #
#                                                                              #
#                         : 0 on failure (If the packet is not received        #
#                                         as expected)                         #
#                                                                              #
#  DEFINITION             : This function is used to receive                   #
#                           PTP announce message                               #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       ptp_ha::recv_announce\                                                 #
#           -port_num                         $port_num\                       #
#           -filter_id                        $filter_id\                      #
#           -reset_count                      $reset_count\                    #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -major_sdo_id                     $major_sdo_id\                   #
#           -message_type                     $message_type\                   #
#           -minor_version                    $minor_version\                  #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -minor_sdo_id                     $minor_sdo_id\                   #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -unicast_flag                     $unicast_flag\                   #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -secure                           $secure\                         #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -sync_uncertain                   $sync_uncertain\                 #
#           -message_type_specific            $message_type_specific\          #
#           -control_field                    $control_field\                  #
#           -log_message_interval             $log_message_interval\           #
#           -gm_priority1                     $gm_priority1\                   #
#           -gm_priority2                     $gm_priority2\                   #
#           -current_utc_offset               $current_utc_offset\             #
#           -steps_removed                    $steps_removed\                  #
#           -time_source                      $time_source\                    #
#           -gm_clock_classy                  $gm_clock_classy\                #
#           -gm_clock_accuracy                $gm_clock_accuracy\              #
#           -gm_clock_variance                $gm_clock_variance\              #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#           -recvd_dest_mac                   recvd_dest_mac\                  #
#           -recvd_src_mac                    recvd_src_mac\                   #
#           -recvd_two_step_flag              recvd_two_step_flag\             #
#           -recvd_unicast_flag               recvd_unicast_flag\              #
#           -recvd_correction_field           recvd_correction_field\          #
#           -recvd_src_port_number            recvd_src_port_identity\         #
#           -recvd_src_clock_identity         recvd_src_clock_identity\        #
#           -recvd_sequence_id                recvd_sequence_id\               #
#           -recvd_log_message_interval       recvd_message_interval\          #
#           -recvd_origin_timestamp_sec       recvd_origin_timestamp_sec\      #
#           -recvd_origin_timestamp_ns        recvd_origin_timestamp_ns\       #
#           -recvd_gm_priority1               recvd_gm_priority1\              #
#           -recvd_gm_priority2               recvd_gm_priority2\              #
#           -recvd_gm_identity                recvd_gm_identity\               #
#           -rx_timestamp_sec                 rx_timestamp_sec\                #
#           -rx_timestamp_ns                  rx_timestamp_ns\                 #
#           -error_reason                     error_reason                     #
#                                                                              #
################################################################################
proc ptp_ha::recv_announce {args} {

    array unset ::validation_param

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "ptp_ha::recv_announce: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #Reception port
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "ptp_ha::recv_announce: Missing Argument -> port_num"
        return 0
    }

    #Filter id
    if {[info exists param(-filter_id)]} {

        set filter_id $param(-filter_id)
    } else {

        ERRLOG -msg "ptp_ha::recv_announce: Missing Argument -> filter_id"
        return 0
    }

    #Reset Count
    if {[info exists param(-reset_count)]} {

        set reset_count $param(-reset_count)
    } else {

        set reset_count ON
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        set ::validation_param(eth_type) $param(-eth_type)
    } else {

        set ::validation_param(eth_type) "dont_care"
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
        set ::validation_param(vlan_id) $param(-vlan_id)
    } else {

        if { $::ptp_ha_vlan_encap == "true" } {
            set ::validation_param(vlan_id) $::ptp_ha_vlan_id
        } else {
            set ::validation_param(vlan_id) "dont_care"
        }
    }

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
        set ::validation_param(dest_ip) $param(-dest_ip)
    } else {

        set ::validation_param(dest_ip) "dont_care"
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
        set ::validation_param(src_ip) $param(-src_ip)
    } else {

        set ::validation_param(src_ip) "dont_care"
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
        set ::validation_param(ip_checksum) $param(-ip_checksum)
    } else {

        set ::validation_param(ip_checksum) "dont_care"
    }

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
        set ::validation_param(dest_port) $param(-dest_port)
    } else {

        set ::validation_param(dest_port) "dont_care"
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
        set ::validation_param(src_port) $param(-src_port)
    } else {

        set ::validation_param(src_port) "dont_care"
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
        set ::validation_param(udp_length) $param(-udp_length)
    } else {

        set ::validation_param(udp_length) "dont_care"
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
        set ::validation_param(udp_checksum) $param(-udp_checksum)
    } else {

        set ::validation_param(udp_checksum) "dont_care"
    }

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
        set ::validation_param(major_sdo_id) $param(-major_sdo_id)
    } else {

        set ::validation_param(major_sdo_id) "dont_care"
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        set ::validation_param(message_type) $param(-message_type)
    } else {

        set ::validation_param(message_type) 11
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
        set ::validation_param(minor_version) $param(-minor_version)
    } else {

        set ::validation_param(minor_version) "dont_care"
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        set ::validation_param(version_ptp) $param(-version_ptp)
    } else {

        set ::validation_param(version_ptp) "dont_care"
    }

    #PTP Annouce message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        set ::validation_param(message_length) $param(-message_length)
    } else {

        set ::validation_param(message_length) "dont_care"
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        set ::validation_param(domain_number) $param(-domain_number)
    } else {

        set ::validation_param(domain_number) "dont_care"
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
        set ::validation_param(minor_sdo_id) $param(-minor_sdo_id)
    } else {

        set ::validation_param(minor_sdo_id) "dont_care"
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        set ::validation_param(alternate_master_flag) $param(-alternate_master_flag)
    } else {

        set ::validation_param(alternate_master_flag) "dont_care"
    }

    #Unicast flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
        set ::validation_param(unicast_flag) $param(-unicast_flag)
    } else {

        set ::validation_param(unicast_flag) "dont_care"
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        set ::validation_param(profile_specific1) $param(-profile_specific1)
    } else {

        set ::validation_param(profile_specific1) "dont_care"
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        set ::validation_param(profile_specific2) $param(-profile_specific2)
    } else {

        set ::validation_param(profile_specific2) "dont_care"
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
        set ::validation_param(secure) $param(-secure)
    } else {

        set ::validation_param(secure) "dont_care"
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        set ::validation_param(leap61) $param(-leap61)
    } else {

        set ::validation_param(leap61) "dont_care"
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        set ::validation_param(leap59) $param(-leap59)
    } else {

        set ::validation_param(leap59) "dont_care"
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        set ::validation_param(current_utc_offset_valid) $param(-current_utc_offset_valid)
    } else {

        set ::validation_param(current_utc_offset_valid) "dont_care"
    }

    #PTP Timescale Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        set ::validation_param(ptp_timescale) $param(-ptp_timescale)
    } else {

        set ::validation_param(ptp_timescale) "dont_care"
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        set ::validation_param(time_traceable) $param(-time_traceable)
    } else {

        set ::validation_param(time_traceable) "dont_care"
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        set ::validation_param(freq_traceable) $param(-freq_traceable)
    } else {

        set ::validation_param(freq_traceable) "dont_care"
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
        set ::validation_param(sync_uncertain) $param(-sync_uncertain)
    } else {

        set ::validation_param(sync_uncertain) "dont_care"
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
        set ::validation_param(message_type_specific) $param(-message_type_specific)
    } else {

        set ::validation_param(message_type_specific) "dont_care"
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        set ::validation_param(sequence_id) $param(-sequence_id)
    } else {

        set ::validation_param(sequence_id) "dont_care"
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        set ::validation_param(control_field) $param(-control_field)
    } else {

        set ::validation_param(control_field) "dont_care"
    }

    #Log Message Interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
        set ::validation_param(log_message_interval) $param(-log_message_interval)
    } else {

        set ::validation_param(log_message_interval) "dont_care"
    }

    #Current Utc Offset
    if {[info exists param(-current_utc_offset)]} {

        set current_utc_offset $param(-current_utc_offset)
        set ::validation_param(current_utc_offset) $param(-current_utc_offset)
    } else {

        set ::validation_param(current_utc_offset) "dont_care"
    }

    #Steps Removed
    if {[info exists param(-steps_removed)]} {

        set steps_removed $param(-steps_removed)
        set ::validation_param(steps_removed) $param(-steps_removed)
    } else {

        set ::validation_param(steps_removed) "dont_care"
    }

    #Time Source
    if {[info exists param(-time_source)]} {

        set time_source $param(-time_source)
        set ::validation_param(time_source) $param(-time_source)
    } else {

        set ::validation_param(time_source) "dont_care"
    }

    #Gm Clock Classy
    if {[info exists param(-gm_clock_classy)]} {

        set gm_clock_classy $param(-gm_clock_classy)
        set ::validation_param(gm_clock_classy) $param(-gm_clock_classy)
    } else {

        set ::validation_param(gm_clock_classy) "dont_care"
    }

    #Gm Clock Accuracy
    if {[info exists param(-gm_clock_accuracy)]} {

        set gm_clock_accuracy $param(-gm_clock_accuracy)
        set ::validation_param(gm_clock_accuracy) $param(-gm_clock_accuracy)
    } else {

        set ::validation_param(gm_clock_accuracy) "dont_care"
    }

    #Gm Clock Variance
    if {[info exists param(-gm_clock_variance)]} {

        set gm_clock_variance $param(-gm_clock_variance)
        set ::validation_param(gm_clock_variance) $param(-gm_clock_variance)
    } else {

        set ::validation_param(gm_clock_variance) "dont_care"
    }

    #Grandmaster priority 1
    if {[info exists param(-gm_priority1)]} {

        set gm_priority1 $param(-gm_priority1)
        set ::validation_param(gm_priority1) $param(-gm_priority1)
    } else {

        set ::validation_param(gm_priority1) "dont_care"
    }

    #Grandmaster priority 2
    if {[info exists param(-gm_priority2)]} {

        set gm_priority2 $param(-gm_priority2)
        set ::validation_param(gm_priority2) $param(-gm_priority2)
    } else {

        set ::validation_param(gm_priority2) "dont_care"
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame timeout
    if {[info exists param(-timeout)]} {

        set timeout $param(-timeout)
    } else {

        set timeout $::ptp_ha_1_announce_timeout
    }

    #OUTPUT PARAMETERS
    #all the fields in the matched packet
    foreach key [array names param] {

        set var [string trimleft $key -]

        if [regexp {^recvd_.*} $var] {

            upvar [set param($key)] $var
            set $var ""
        }
    }

    #Received packet timestamp in seconds
    if {[info exists param(-rx_timestamp_sec)]} {

        upvar $param(-rx_timestamp_sec) rx_timestamp_sec
        set rx_timestamp_sec ""
    }

    #Received packet timestamp in nanoseconds
    if {[info exists param(-rx_timestamp_ns)]} {

        upvar $param(-rx_timestamp_ns) rx_timestamp_ns
        set rx_timestamp_ns ""
    }

    #Error Reason
    if {[info exists param(-error_reason)]} {

        upvar $param(-error_reason) error_reason
        set error_reason ""
    }

    #LOGIC GOES HERE
    #Timeout in millisec
    set timeout [expr int($timeout*1000)]

    set error_code 0

    printTimeout $timeout

    set result [pltLib::recv_periodic\
            -session_id           $session_id\
            -port_num             $port_num\
            -filter_id            $filter_id\
            -timeout              $timeout\
            -reset_count          $reset_count\
            -validation_handler   "PTP_HA"\
            -error_code           error_code\
            -rx_timestamp         rx_timestamp_sec\
            -rx_time_nanoseconds  rx_timestamp_ns\
            -pkt_content          pkt_content\
            -pkt_size             pkt_size]

    set error_reason [ptp_ha::tee_recv_error_reason $error_code]

    if {[info exists pkt_content]} {

        set hex $pkt_content

        pbLib::decode_ptp_ha_pkt\
                 -packet                         $hex\
                 -dest_mac                       recvd_dest_mac\
                 -src_mac                        recvd_src_mac\
                 -eth_type                       recvd_eth_type\
                 -vlan_id                        recvd_vlan_id\
                 -vlan_priority                  recvd_vlan_priority\
                 -vlan_dei                       recvd_vlan_dei\
                 -src_ip                         recvd_src_ip\
                 -dest_ip                        recvd_dest_ip\
                 -src_port                       recvd_src_port\
                 -dest_port                      recvd_dest_port\
                 -udp_length                     recvd_udp_length\
                 -udp_checksum                   recvd_udp_checksum\
                 -major_sdo_id                   recvd_major_sdo_id\
                 -message_type                   recvd_message_type\
                 -minor_version                  recvd_minor_version\
                 -version_ptp                    recvd_version_ptp\
                 -message_length                 recvd_message_length\
                 -domain_number                  recvd_domain_number\
                 -minor_sdo_id                   recvd_minor_sdo_id\
                 -alternate_master_flag          recvd_alternate_master_flag\
                 -two_step_flag                  recvd_two_step_flag\
                 -unicast_flag                   recvd_unicast_flag\
                 -profile_specific1              recvd_profile_specific1\
                 -profile_specific2              recvd_profile_specific2\
                 -secure                         recvd_secure\
                 -leap61                         recvd_leap61\
                 -leap59                         recvd_leap59\
                 -current_utc_offset_valid       recvd_current_utc_offset_valid\
                 -ptp_timescale                  recvd_ptp_timescale\
                 -time_traceable                 recvd_time_traceable\
                 -freq_traceable                 recvd_freq_traceable\
                 -sync_uncertain                 recvd_sync_uncertain\
                 -correction_field               recvd_correction_field\
                 -message_type_specific          recvd_message_type_specific\
                 -src_port_number                recvd_src_port_number\
                 -src_clock_identity             recvd_src_clock_identity\
                 -sequence_id                    recvd_sequence_id\
                 -control_field                  recvd_control_field\
                 -log_message_interval           recvd_log_message_interval\
                 -origin_timestamp_sec           recvd_origin_timestamp_sec\
                 -origin_timestamp_ns            recvd_origin_timestamp_ns\
                 -current_utc_offset             recvd_current_utc_offset\
                 -gm_priority1                   recvd_gm_priority1\
                 -gm_priority2                   recvd_gm_priority2\
                 -gm_identity                    recvd_gm_identity\
                 -steps_removed                  recvd_steps_removed\
                 -time_source                    recvd_time_source\
                 -gm_clock_classy                recvd_gm_clock_classy\
                 -gm_clock_accuracy              recvd_gm_clock_accuracy\
                 -gm_clock_variance              recvd_gm_clock_variance

    }

    return $result

}


################################################################################
#                                                                              #
#  PROCEDURE NAME         :  ptp_ha::recv_delay_req                            #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Reception port                 #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   filter_id                     : (Mandatory) Filter id                      #
#                                                                              #
#   reset_count                   : (Optional)  Reset Count                    #
#                                               (Default: ON)                  #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                                                              #
#   major_sdo_id                  : (Optional)  Major sdo id                   #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default: 1)                   #
#                                                                              #
#   minor_version                 : (Optional)  PTP version number             #
#                                                                              #
#   version_ptp                   : (Optional)  PTP minor version number       #
#                                                                              #
#   message_length                : (Optional)  PTP Annouce message length     #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                                                              #
#   minor_sdo_id                  : (Optional)  Minor Sdo id                   #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                                                              #
#   secure                        : (Optional)  Secure Flag                    #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP Timescale Flag             #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                                                              #
#   sync_uncertain                : (Optional)  Synchronization uncertain Flag #
#                                                                              #
#   message_type_specific         : (Optional)  Message type                   #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default: 1)                   #
#                                                                              #
#   timeout                       : (Optional)  Frame timeout (in seconds)     #
#                                               (Default: 5)                   #
#                                                                              #
#  OUTPUT PARAMETERS      :                                                    #
#                                                                              #
#   recvd_dest_mac                : (Optional)  Destination MAC dddress.       #
#                                                                              #
#   recvd_src_mac                 : (Optional)  Source MAC address.            #
#                                                                              #
#   recvd_two_step_flag           : (Optional)  Two Step Flag                  #
#                                                                              #
#   recvd_unicast_flag            : (Optional)  Unicast Flag                   #
#                                                                              #
#   recvd_correction_field        : (Optional)  Correction Field value         #
#                                                                              #
#   recvd_src_port_number         : (Optional)  Source port number             #
#                                                                              #
#   recvd_src_clock_identity      : (Optional)  Source clock identity          #
#                                                                              #
#   recvd_sequence_id             : (Optional)  Sequence ID                    #
#                                                                              #
#   recvd_log_message_interval    : (Optional)  Log message interval           #
#                                                                              #
#   recvd_origin_timestamp_sec    : (Optional)  Recvd Origin Timestamp Sec     #
#                                                                              #
#   recvd_origin_timestamp_ns     : (Optional)  Recvd Origin Timestamp Ns      #
#                                                                              #
#   rx_timestamp_sec              : (Optional)  Packet's rx time in seconds    #
#                                                                              #
#   rx_timestamp_ns               : (Optional)  Packet's rx time in ns         #
#                                                                              #
#   error_reason                  : (Optional)  Error Reason                   #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is received as         #
#                                         expected)                            #
#                                                                              #
#                         : 0 on failure (If the packet is not received        #
#                                         as expected)                         #
#                                                                              #
#  DEFINITION             : This function is used to receive                   #
#                           PTP delay req message                              #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       ptp_ha::recv_delay_req\                                                #
#           -port_num                         $port_num\                       #
#           -filter_id                        $filter_id\                      #
#           -reset_count                      $reset_count\                    #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -major_sdo_id                     $major_sdo_id\                   #
#           -message_type                     $message_type\                   #
#           -minor_version                    $minor_version\                  #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -minor_sdo_id                     $minor_sdo_id\                   #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -secure                           $secure\                         #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -sync_uncertain                   $sync_uncertain\                 #
#           -message_type_specific            $message_type_specific\          #
#           -control_field                    $control_field\                  #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#           -recvd_dest_mac                   recvd_dest_mac\                  #
#           -recvd_src_mac                    recvd_src_mac\                   #
#           -recvd_two_step_flag              recvd_two_step_flag\             #
#           -recvd_unicast_flag               recvd_unicast_flag\              #
#           -recvd_correction_field           recvd_correction_field\          #
#           -recvd_src_port_number            recvd_src_port_identity\         #
#           -recvd_src_clock_identity         recvd_src_clock_identity\        #
#           -recvd_sequence_id                recvd_sequence_id\               #
#           -recvd_log_message_interval       recvd_message_interval\          #
#           -recvd_origin_timestamp_sec       recvd_origin_timestamp_sec\      #
#           -recvd_origin_timestamp_ns        recvd_origin_timestamp_ns\       #
#           -rx_timestamp_sec                 rx_timestamp_sec\                #
#           -rx_timestamp_ns                  rx_timestamp_ns\                 #
#           -error_reason                     error_reason                     #
#                                                                              #
################################################################################
proc ptp_ha::recv_delay_req {args} {

    array unset ::validation_param

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "ptp_ha::recv_delay_req: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #Reception port
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "ptp_ha::recv_delay_req: Missing Argument -> port_num"
        return 0
    }

    #Filter id
    if {[info exists param(-filter_id)]} {

        set filter_id $param(-filter_id)
    } else {

        ERRLOG -msg "ptp_ha::recv_delay_req: Missing Argument -> filter_id"
        return 0
    }

    #Reset Count
    if {[info exists param(-reset_count)]} {

        set reset_count $param(-reset_count)
    } else {

        set reset_count ON
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        set ::validation_param(eth_type) $param(-eth_type)
    } else {

        set ::validation_param(eth_type) "dont_care"
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
        set ::validation_param(vlan_id) $param(-vlan_id)
    } else {

        if { $::ptp_ha_vlan_encap == "true" } {
            set ::validation_param(vlan_id) $::ptp_ha_vlan_id
        } else {
            set ::validation_param(vlan_id) "dont_care"
        }
    }

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
        set ::validation_param(dest_ip) $param(-dest_ip)
    } else {

        set ::validation_param(dest_ip) "dont_care"
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
        set ::validation_param(src_ip) $param(-src_ip)
    } else {

        set ::validation_param(src_ip) "dont_care"
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
        set ::validation_param(ip_checksum) $param(-ip_checksum)
    } else {

        set ::validation_param(ip_checksum) "dont_care"
    }

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
        set ::validation_param(dest_port) $param(-dest_port)
    } else {

        set ::validation_param(dest_port) "dont_care"
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
        set ::validation_param(src_port) $param(-src_port)
    } else {

        set ::validation_param(src_port) "dont_care"
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
        set ::validation_param(udp_length) $param(-udp_length)
    } else {

        set ::validation_param(udp_length) "dont_care"
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
        set ::validation_param(udp_checksum) $param(-udp_checksum)
    } else {

        set ::validation_param(udp_checksum) "dont_care"
    }

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
        set ::validation_param(major_sdo_id) $param(-major_sdo_id)
    } else {

        set ::validation_param(major_sdo_id) "dont_care"
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        set ::validation_param(message_type) $param(-message_type)
    } else {

        set ::validation_param(message_type) 1
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
        set ::validation_param(minor_version) $param(-minor_version)
    } else {

        set ::validation_param(minor_version) "dont_care"
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        set ::validation_param(version_ptp) $param(-version_ptp)
    } else {

        set ::validation_param(version_ptp) "dont_care"
    }

    #PTP Annouce message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        set ::validation_param(message_length) $param(-message_length)
    } else {

        set ::validation_param(message_length) "dont_care"
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        set ::validation_param(domain_number) $param(-domain_number)
    } else {

        set ::validation_param(domain_number) "dont_care"
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
        set ::validation_param(minor_sdo_id) $param(-minor_sdo_id)
    } else {

        set ::validation_param(minor_sdo_id) "dont_care"
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        set ::validation_param(alternate_master_flag) $param(-alternate_master_flag)
    } else {

        set ::validation_param(alternate_master_flag) "dont_care"
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        set ::validation_param(profile_specific1) $param(-profile_specific1)
    } else {

        set ::validation_param(profile_specific1) "dont_care"
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        set ::validation_param(profile_specific2) $param(-profile_specific2)
    } else {

        set ::validation_param(profile_specific2) "dont_care"
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
        set ::validation_param(secure) $param(-secure)
    } else {

        set ::validation_param(secure) "dont_care"
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        set ::validation_param(leap61) $param(-leap61)
    } else {

        set ::validation_param(leap61) "dont_care"
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        set ::validation_param(leap59) $param(-leap59)
    } else {

        set ::validation_param(leap59) "dont_care"
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        set ::validation_param(current_utc_offset_valid) $param(-current_utc_offset_valid)
    } else {

        set ::validation_param(current_utc_offset_valid) "dont_care"
    }

    #PTP Timescale Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        set ::validation_param(ptp_timescale) $param(-ptp_timescale)
    } else {

        set ::validation_param(ptp_timescale) "dont_care"
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        set ::validation_param(time_traceable) $param(-time_traceable)
    } else {

        set ::validation_param(time_traceable) "dont_care"
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        set ::validation_param(freq_traceable) $param(-freq_traceable)
    } else {

        set ::validation_param(freq_traceable) "dont_care"
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
        set ::validation_param(sync_uncertain) $param(-sync_uncertain)
    } else {

        set ::validation_param(sync_uncertain) "dont_care"
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
        set ::validation_param(message_type_specific) $param(-message_type_specific)
    } else {

        set ::validation_param(message_type_specific) "dont_care"
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        set ::validation_param(sequence_id) $param(-sequence_id)
    } else {

        set ::validation_param(sequence_id) "dont_care"
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        set ::validation_param(control_field) $param(-control_field)
    } else {

        set ::validation_param(control_field) "dont_care"
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame timeout
    if {[info exists param(-timeout)]} {

        set timeout $param(-timeout)
    } else {

        set timeout $::ptp_ha_delayreq_timeout
    }

    #OUTPUT PARAMETERS
    #all the fields in the matched packet
    foreach key [array names param] {

        set var [string trimleft $key -]

        if [regexp {^recvd_.*} $var] {

            upvar [set param($key)] $var
            set $var ""
        }
    }

    #Received packet timestamp in seconds
    if {[info exists param(-rx_timestamp_sec)]} {

        upvar $param(-rx_timestamp_sec) rx_timestamp_sec
        set rx_timestamp_sec ""
    }

    #Received packet timestamp in nanoseconds
    if {[info exists param(-rx_timestamp_ns)]} {

        upvar $param(-rx_timestamp_ns) rx_timestamp_ns
        set rx_timestamp_ns ""
    }

    #Error Reason
    if {[info exists param(-error_reason)]} {

        upvar $param(-error_reason) error_reason
        set error_reason ""
    }

    #LOGIC GOES HERE
    #Timeout in millisec
    set timeout [expr int($timeout*1000)]
    set error_code 0

    printTimeout $timeout

    set result [pltLib::recv_periodic\
            -session_id           $session_id\
            -port_num             $port_num\
            -filter_id            $filter_id\
            -timeout              $timeout\
            -reset_count          $reset_count\
            -validation_handler   "PTP_HA"\
            -error_code           error_code\
            -rx_timestamp         rx_timestamp_sec\
            -rx_time_nanoseconds  rx_timestamp_ns\
            -pkt_content          pkt_content\
            -pkt_size             pkt_size]

    set error_reason [ptp_ha::tee_recv_error_reason $error_code]

    if {[info exists pkt_content]} {

        set hex $pkt_content

        pbLib::decode_ptp_ha_pkt\
                 -packet                         $hex\
                 -dest_mac                       recvd_dest_mac\
                 -src_mac                        recvd_src_mac\
                 -eth_type                       recvd_eth_type\
                 -vlan_id                        recvd_vlan_id\
                 -vlan_priority                  recvd_vlan_priority\
                 -vlan_dei                       recvd_vlan_dei\
                 -src_ip                         recvd_src_ip\
                 -dest_ip                        recvd_dest_ip\
                 -src_port                       recvd_src_port\
                 -dest_port                      recvd_dest_port\
                 -udp_length                     recvd_udp_length\
                 -udp_checksum                   recvd_udp_checksum\
                 -major_sdo_id                   recvd_major_sdo_id\
                 -message_type                   recvd_message_type\
                 -minor_version                  recvd_minor_version\
                 -version_ptp                    recvd_version_ptp\
                 -message_length                 recvd_message_length\
                 -domain_number                  recvd_domain_number\
                 -minor_sdo_id                   recvd_minor_sdo_id\
                 -alternate_master_flag          recvd_alternate_master_flag\
                 -two_step_flag                  recvd_two_step_flag\
                 -unicast_flag                   recvd_unicast_flag\
                 -profile_specific1              recvd_profile_specific1\
                 -profile_specific2              recvd_profile_specific2\
                 -secure                         recvd_secure\
                 -leap61                         recvd_leap61\
                 -leap59                         recvd_leap59\
                 -current_utc_offset_valid       recvd_current_utc_offset_valid\
                 -ptp_timescale                  recvd_ptp_timescale\
                 -time_traceable                 recvd_time_traceable\
                 -freq_traceable                 recvd_freq_traceable\
                 -sync_uncertain                 recvd_sync_uncertain\
                 -correction_field               recvd_correction_field\
                 -message_type_specific          recvd_message_type_specific\
                 -src_port_number                recvd_src_port_number\
                 -src_clock_identity             recvd_src_clock_identity\
                 -sequence_id                    recvd_sequence_id\
                 -control_field                  recvd_control_field\
                 -log_message_interval           recvd_log_message_interval\
                 -origin_timestamp_sec           recvd_origin_timestamp_sec\
                 -origin_timestamp_ns            recvd_origin_timestamp_ns

    }

    return $result

}


################################################################################
#                                                                              #
#  PROCEDURE NAME         :  ptp_ha::recv_delay_resp                           #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Reception port                 #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   filter_id                     : (Mandatory) Filter id                      #
#                                                                              #
#   reset_count                   : (Optional)  Reset Count                    #
#                                               (Default: ON)                  #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                                                              #
#   major_sdo_id                  : (Optional)  Major sdo id                   #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default: 9)                   #
#                                                                              #
#   minor_version                 : (Optional)  PTP version number             #
#                                                                              #
#   version_ptp                   : (Optional)  PTP minor version number       #
#                                                                              #
#   message_length                : (Optional)  PTP Annouce message length     #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                                                              #
#   minor_sdo_id                  : (Optional)  Minor Sdo id                   #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                                                              #
#   secure                        : (Optional)  Secure Flag                    #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP Timescale Flag             #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                                                              #
#   sync_uncertain                : (Optional)  Synchronization uncertain Flag #
#                                                                              #
#   message_type_specific         : (Optional)  Message type                   #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default: 1)                   #
#                                                                              #
#   timeout                       : (Optional)  Frame timeout (in seconds)     #
#                                               (Default: 5)                   #
#                                                                              #
#  OUTPUT PARAMETERS      :                                                    #
#                                                                              #
#   recvd_dest_mac                : (Optional)  Destination MAC dddress.       #
#                                                                              #
#   recvd_src_mac                 : (Optional)  Source MAC address.            #
#                                                                              #
#   recvd_two_step_flag           : (Optional)  Two Step Flag                  #
#                                                                              #
#   recvd_unicast_flag            : (Optional)  Unicast Flag                   #
#                                                                              #
#   recvd_correction_field        : (Optional)  Correction Field value         #
#                                                                              #
#   recvd_src_port_number         : (Optional)  Source port number             #
#                                                                              #
#   recvd_src_clock_identity      : (Optional)  Source clock identity          #
#                                                                              #
#   recvd_sequence_id             : (Optional)  Sequence ID                    #
#                                                                              #
#   recvd_log_message_interval    : (Optional)  Log message interval           #
#                                                                              #
#   recvd_receive_timestamp_sec   : (Optional)  Recvd Receive Timestamp Sec    #
#                                                                              #
#   recvd_receive_timestamp_ns    : (Optional)  Recvd Receive Timestamp Ns     #
#                                                                              #
#   rx_timestamp_sec              : (Optional)  Packet's rx time in seconds    #
#                                                                              #
#   rx_timestamp_ns               : (Optional)  Packet's rx time in ns         #
#                                                                              #
#   error_reason                  : (Optional)  Error Reason                   #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is received as         #
#                                         expected)                            #
#                                                                              #
#                         : 0 on failure (If the packet is not received        #
#                                         as expected)                         #
#                                                                              #
#  DEFINITION             : This function is used to receive                   #
#                           PTP delay resp message                             #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       ptp_ha::recv_delay_resp\                                               #
#           -port_num                         $port_num\                       #
#           -filter_id                        $filter_id\                      #
#           -reset_count                      $reset_count\                    #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -major_sdo_id                     $major_sdo_id\                   #
#           -message_type                     $message_type\                   #
#           -minor_version                    $minor_version\                  #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -minor_sdo_id                     $minor_sdo_id\                   #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -secure                           $secure\                         #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -sync_uncertain                   $sync_uncertain\                 #
#           -message_type_specific            $message_type_specific\          #
#           -control_field                    $control_field\                  #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#           -recvd_dest_mac                   recvd_dest_mac\                  #
#           -recvd_src_mac                    recvd_src_mac\                   #
#           -recvd_two_step_flag              recvd_two_step_flag\             #
#           -recvd_unicast_flag               recvd_unicast_flag\              #
#           -recvd_correction_field           recvd_correction_field\          #
#           -recvd_src_port_number            recvd_src_port_identity\         #
#           -recvd_src_clock_identity         recvd_src_clock_identity\        #
#           -recvd_sequence_id                recvd_sequence_id\               #
#           -recvd_log_message_interval       recvd_message_interval\          #
#           -recvd_receive_timestamp_sec      recvd_receive_timestamp_sec\     #
#           -recvd_receive_timestamp_ns       recvd_receive_timestamp_ns\      #
#           -recvd_requesting_clock_identity  recvd_requesting_clock_identity\ #
#           -recvd_requesting_port_number     recvd_requesting_port_number\    #
#           -rx_timestamp_sec                 rx_timestamp_sec\                #
#           -rx_timestamp_ns                  rx_timestamp_ns\                 #
#           -error_reason                     error_reason                     #
#                                                                              #
################################################################################
proc ptp_ha::recv_delay_resp {args} {

    array unset ::validation_param

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "ptp_ha::recv_delay_resp: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #Reception port
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "ptp_ha::recv_delay_resp: Missing Argument -> port_num"
        return 0
    }

    #Filter id
    if {[info exists param(-filter_id)]} {

        set filter_id $param(-filter_id)
    } else {

        ERRLOG -msg "ptp_ha::recv_delay_resp: Missing Argument -> filter_id"
        return 0
    }

    #Reset Count
    if {[info exists param(-reset_count)]} {

        set reset_count $param(-reset_count)
    } else {

        set reset_count ON
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        set ::validation_param(eth_type) $param(-eth_type)
    } else {

        set ::validation_param(eth_type) "dont_care"
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
        set ::validation_param(vlan_id) $param(-vlan_id)
    } else {

        if { $::ptp_ha_vlan_encap == "true" } {
            set ::validation_param(vlan_id) $::ptp_ha_vlan_id
        } else {
            set ::validation_param(vlan_id) "dont_care"
        }
    }

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
        set ::validation_param(dest_ip) $param(-dest_ip)
    } else {

        set ::validation_param(dest_ip) "dont_care"
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
        set ::validation_param(src_ip) $param(-src_ip)
    } else {

        set ::validation_param(src_ip) "dont_care"
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
        set ::validation_param(ip_checksum) $param(-ip_checksum)
    } else {

        set ::validation_param(ip_checksum) "dont_care"
    }

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
        set ::validation_param(dest_port) $param(-dest_port)
    } else {

        set ::validation_param(dest_port) "dont_care"
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
        set ::validation_param(src_port) $param(-src_port)
    } else {

        set ::validation_param(src_port) "dont_care"
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
        set ::validation_param(udp_length) $param(-udp_length)
    } else {

        set ::validation_param(udp_length) "dont_care"
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
        set ::validation_param(udp_checksum) $param(-udp_checksum)
    } else {

        set ::validation_param(udp_checksum) "dont_care"
    }

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
        set ::validation_param(major_sdo_id) $param(-major_sdo_id)
    } else {

        set ::validation_param(major_sdo_id) "dont_care"
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        set ::validation_param(message_type) $param(-message_type)
    } else {

        set ::validation_param(message_type) 9
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
        set ::validation_param(minor_version) $param(-minor_version)
    } else {

        set ::validation_param(minor_version) "dont_care"
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        set ::validation_param(version_ptp) $param(-version_ptp)
    } else {

        set ::validation_param(version_ptp) "dont_care"
    }

    #PTP Annouce message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        set ::validation_param(message_length) $param(-message_length)
    } else {

        set ::validation_param(message_length) "dont_care"
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        set ::validation_param(domain_number) $param(-domain_number)
    } else {

        set ::validation_param(domain_number) "dont_care"
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
        set ::validation_param(minor_sdo_id) $param(-minor_sdo_id)
    } else {

        set ::validation_param(minor_sdo_id) "dont_care"
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        set ::validation_param(alternate_master_flag) $param(-alternate_master_flag)
    } else {

        set ::validation_param(alternate_master_flag) "dont_care"
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        set ::validation_param(profile_specific1) $param(-profile_specific1)
    } else {

        set ::validation_param(profile_specific1) "dont_care"
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        set ::validation_param(profile_specific2) $param(-profile_specific2)
    } else {

        set ::validation_param(profile_specific2) "dont_care"
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
        set ::validation_param(secure) $param(-secure)
    } else {

        set ::validation_param(secure) "dont_care"
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        set ::validation_param(leap61) $param(-leap61)
    } else {

        set ::validation_param(leap61) "dont_care"
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        set ::validation_param(leap59) $param(-leap59)
    } else {

        set ::validation_param(leap59) "dont_care"
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        set ::validation_param(current_utc_offset_valid) $param(-current_utc_offset_valid)
    } else {

        set ::validation_param(current_utc_offset_valid) "dont_care"
    }

    #PTP Timescale Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        set ::validation_param(ptp_timescale) $param(-ptp_timescale)
    } else {

        set ::validation_param(ptp_timescale) "dont_care"
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        set ::validation_param(time_traceable) $param(-time_traceable)
    } else {

        set ::validation_param(time_traceable) "dont_care"
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        set ::validation_param(freq_traceable) $param(-freq_traceable)
    } else {

        set ::validation_param(freq_traceable) "dont_care"
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
        set ::validation_param(sync_uncertain) $param(-sync_uncertain)
    } else {

        set ::validation_param(sync_uncertain) "dont_care"
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
        set ::validation_param(message_type_specific) $param(-message_type_specific)
    } else {

        set ::validation_param(message_type_specific) "dont_care"
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        set ::validation_param(sequence_id) $param(-sequence_id)
    } else {

        set ::validation_param(sequence_id) "dont_care"
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        set ::validation_param(control_field) $param(-control_field)
    } else {

        set ::validation_param(control_field) "dont_care"
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame timeout
    if {[info exists param(-timeout)]} {

        set timeout $param(-timeout)
    } else {

        set timeout $::ptp_ha_delayresp_timeout
    }

    #OUTPUT PARAMETERS
    #all the fields in the matched packet
    foreach key [array names param] {

        set var [string trimleft $key -]

        if [regexp {^recvd_.*} $var] {

            upvar [set param($key)] $var
            set $var ""
        }
    }

    #Received packet timestamp in seconds
    if {[info exists param(-rx_timestamp_sec)]} {

        upvar $param(-rx_timestamp_sec) rx_timestamp_sec
        set rx_timestamp_sec ""
    }

    #Received packet timestamp in nanoseconds
    if {[info exists param(-rx_timestamp_ns)]} {

        upvar $param(-rx_timestamp_ns) rx_timestamp_ns
        set rx_timestamp_ns ""
    }

    #Error Reason
    if {[info exists param(-error_reason)]} {

        upvar $param(-error_reason) error_reason
        set error_reason ""
    }

    #LOGIC GOES HERE
    #Timeout in millisec
    set timeout [expr int($timeout*1000)]
    set error_code 0

    printTimeout $timeout

    set result [pltLib::recv_periodic\
            -session_id           $session_id\
            -port_num             $port_num\
            -filter_id            $filter_id\
            -timeout              $timeout\
            -reset_count          $reset_count\
            -validation_handler   "PTP_HA"\
            -error_code           error_code\
            -rx_timestamp         rx_timestamp_sec\
            -rx_time_nanoseconds  rx_timestamp_ns\
            -pkt_content          pkt_content\
            -pkt_size             pkt_size]

    set error_reason [ptp_ha::tee_recv_error_reason $error_code]

    if {[info exists pkt_content]} {

        set hex $pkt_content

        pbLib::decode_ptp_ha_pkt\
                 -packet                         $hex\
                 -dest_mac                       recvd_dest_mac\
                 -src_mac                        recvd_src_mac\
                 -eth_type                       recvd_eth_type\
                 -vlan_id                        recvd_vlan_id\
                 -vlan_priority                  recvd_vlan_priority\
                 -vlan_dei                       recvd_vlan_dei\
                 -src_ip                         recvd_src_ip\
                 -dest_ip                        recvd_dest_ip\
                 -src_port                       recvd_src_port\
                 -dest_port                      recvd_dest_port\
                 -udp_length                     recvd_udp_length\
                 -udp_checksum                   recvd_udp_checksum\
                 -major_sdo_id                   recvd_major_sdo_id\
                 -message_type                   recvd_message_type\
                 -minor_version                  recvd_minor_version\
                 -version_ptp                    recvd_version_ptp\
                 -message_length                 recvd_message_length\
                 -domain_number                  recvd_domain_number\
                 -minor_sdo_id                   recvd_minor_sdo_id\
                 -alternate_master_flag          recvd_alternate_master_flag\
                 -two_step_flag                  recvd_two_step_flag\
                 -unicast_flag                   recvd_unicast_flag\
                 -profile_specific1              recvd_profile_specific1\
                 -profile_specific2              recvd_profile_specific2\
                 -secure                         recvd_secure\
                 -leap61                         recvd_leap61\
                 -leap59                         recvd_leap59\
                 -current_utc_offset_valid       recvd_current_utc_offset_valid\
                 -ptp_timescale                  recvd_ptp_timescale\
                 -time_traceable                 recvd_time_traceable\
                 -freq_traceable                 recvd_freq_traceable\
                 -sync_uncertain                 recvd_sync_uncertain\
                 -correction_field               recvd_correction_field\
                 -message_type_specific          recvd_message_type_specific\
                 -src_port_number                recvd_src_port_number\
                 -src_clock_identity             recvd_src_clock_identity\
                 -sequence_id                    recvd_sequence_id\
                 -control_field                  recvd_control_field\
                 -log_message_interval           recvd_log_message_interval\
                 -receive_timestamp_sec          recvd_receive_timestamp_sec\
                 -receive_timestamp_ns           recvd_receive_timestamp_ns\
                 -requesting_port_number         recvd_requesting_port_number\
                 -requesting_clock_identity      recvd_requesting_clock_identity

    }

    return $result

}


################################################################################
#                                                                              #
#  PROCEDURE NAME         :  ptp_ha::recv_followup                             #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Reception port                 #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   filter_id                     : (Mandatory) Filter id                      #
#                                                                              #
#   reset_count                   : (Optional)  Reset Count                    #
#                                               (Default: ON)                  #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                                                              #
#   major_sdo_id                  : (Optional)  Major sdo id                   #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default: 8)                   #
#                                                                              #
#   minor_version                 : (Optional)  PTP version number             #
#                                                                              #
#   version_ptp                   : (Optional)  PTP minor version number       #
#                                                                              #
#   message_length                : (Optional)  PTP Annouce message length     #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                                                              #
#   minor_sdo_id                  : (Optional)  Minor Sdo id                   #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                                                              #
#   secure                        : (Optional)  Secure Flag                    #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP Timescale Flag             #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                                                              #
#   sync_uncertain                : (Optional)  Synchronization uncertain Flag #
#                                                                              #
#   message_type_specific         : (Optional)  Message type                   #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default: 1)                   #
#                                                                              #
#   timeout                       : (Optional)  Frame timeout (in seconds)     #
#                                               (Default: 5)                   #
#                                                                              #
#  OUTPUT PARAMETERS      :                                                    #
#                                                                              #
#   recvd_dest_mac                : (Optional)  Destination MAC dddress.       #
#                                                                              #
#   recvd_src_mac                 : (Optional)  Source MAC address.            #
#                                                                              #
#   recvd_two_step_flag           : (Optional)  Two Step Flag                  #
#                                                                              #
#   recvd_unicast_flag            : (Optional)  Unicast Flag                   #
#                                                                              #
#   recvd_correction_field        : (Optional)  Correction Field value         #
#                                                                              #
#   recvd_src_port_number         : (Optional)  Source port number             #
#                                                                              #
#   recvd_src_clock_identity      : (Optional)  Source clock identity          #
#                                                                              #
#   recvd_sequence_id             : (Optional)  Sequence ID                    #
#                                                                              #
#   recvd_log_message_interval    : (Optional)  Log message interval           #
#                                                                              #
#   rx_timestamp_sec              : (Optional)  Packet's rx time in seconds    #
#                                                                              #
#   rx_timestamp_ns               : (Optional)  Packet's rx time in ns         #
#                                                                              #
#   error_reason                  : (Optional)  Error Reason                   #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is received as         #
#                                         expected)                            #
#                                                                              #
#                         : 0 on failure (If the packet is not received        #
#                                         as expected)                         #
#                                                                              #
#  DEFINITION             : This function is used to receive                   #
#                           PTP followup message                               #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#   ptp_ha::recv_followup\                                                     #
#       -port_num                         $port_num\                           #
#       -filter_id                        $filter_id\                          #
#       -reset_count                      $reset_count\                        #
#       -eth_type                         $eth_type\                           #
#       -vlan_id                          $vlan_id\                            #
#       -dest_ip                          $dest_ip\                            #
#       -src_ip                           $src_ip\                             #
#       -ip_checksum                      $ip_checksum\                        #
#       -dest_port                        $dest_port\                          #
#       -src_port                         $src_port\                           #
#       -udp_length                       $udp_length\                         #
#       -udp_checksum                     $udp_checksum\                       #
#       -major_sdo_id                     $major_sdo_id\                       #
#       -message_type                     $message_type\                       #
#       -minor_version                    $minor_version\                      #
#       -version_ptp                      $version_ptp\                        #
#       -message_length                   $message_length\                     #
#       -domain_number                    $domain_number\                      #
#       -minor_sdo_id                     $minor_sdo_id\                       #
#       -alternate_master_flag            $alternate_master_flag\              #
#       -profile_specific1                $profile_specific1\                  #
#       -profile_specific2                $profile_specific2\                  #
#       -secure                           $secure\                             #
#       -leap61                           $leap61\                             #
#       -leap59                           $leap59\                             #
#       -current_utc_offset_valid         $current_utc_offset_valid\           #
#       -ptp_timescale                    $ptp_timescale\                      #
#       -time_traceable                   $time_traceable\                     #
#       -freq_traceable                   $freq_traceable\                     #
#       -sync_uncertain                   $sync_uncertain\                     #
#       -message_type_specific            $message_type_specific\              #
#       -control_field                    $control_field\                      #
#       -count                            $count\                              #
#       -interval                         $interval                            #
#       -recvd_dest_mac                   recvd_dest_mac\                      #
#       -recvd_src_mac                    recvd_src_mac\                       #
#       -recvd_two_step_flag              recvd_two_step_flag\                 #
#       -recvd_unicast_flag               recvd_unicast_flag\                  #
#       -recvd_correction_field           recvd_correction_field\              #
#       -recvd_src_port_number            recvd_src_port_identity\             #
#       -recvd_src_clock_identity         recvd_src_clock_identity\            #
#       -recvd_sequence_id                recvd_sequence_id\                   #
#       -recvd_log_message_interval       recvd_message_interval\              #
#       -recvd_precise_origin_timestamp_sec recvd_precise_origin_timestamp_sec\#
#       -recvd_precise_origin_timestamp_ns  recvd_precise_origin_timestamp_ns\ #
#       -rx_timestamp_sec                 rx_timestamp_sec\                    #
#       -rx_timestamp_ns                  rx_timestamp_ns\                     #
#       -error_reason                     error_reason                         #
#                                                                              #
################################################################################
proc ptp_ha::recv_followup {args} {

    array unset ::validation_param

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "ptp_ha::recv_followup: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #Reception port
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "ptp_ha::recv_followup: Missing Argument -> port_num"
        return 0
    }

    #Filter id
    if {[info exists param(-filter_id)]} {

        set filter_id $param(-filter_id)
    } else {

        ERRLOG -msg "ptp_ha::recv_followup: Missing Argument -> filter_id"
        return 0
    }

    #Reset Count
    if {[info exists param(-reset_count)]} {

        set reset_count $param(-reset_count)
    } else {

        set reset_count ON
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        set ::validation_param(eth_type) $param(-eth_type)
    } else {

        set ::validation_param(eth_type) "dont_care"
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
        set ::validation_param(vlan_id) $param(-vlan_id)
    } else {

        if { $::ptp_ha_vlan_encap == "true" } {
            set ::validation_param(vlan_id) $::ptp_ha_vlan_id
        } else {
            set ::validation_param(vlan_id) "dont_care"
        }
    }

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
        set ::validation_param(dest_ip) $param(-dest_ip)
    } else {

        set ::validation_param(dest_ip) "dont_care"
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
        set ::validation_param(src_ip) $param(-src_ip)
    } else {

        set ::validation_param(src_ip) "dont_care"
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
        set ::validation_param(ip_checksum) $param(-ip_checksum)
    } else {

        set ::validation_param(ip_checksum) "dont_care"
    }

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
        set ::validation_param(dest_port) $param(-dest_port)
    } else {

        set ::validation_param(dest_port) "dont_care"
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
        set ::validation_param(src_port) $param(-src_port)
    } else {

        set ::validation_param(src_port) "dont_care"
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
        set ::validation_param(udp_length) $param(-udp_length)
    } else {

        set ::validation_param(udp_length) "dont_care"
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
        set ::validation_param(udp_checksum) $param(-udp_checksum)
    } else {

        set ::validation_param(udp_checksum) "dont_care"
    }

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
        set ::validation_param(major_sdo_id) $param(-major_sdo_id)
    } else {

        set ::validation_param(major_sdo_id) "dont_care"
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        set ::validation_param(message_type) $param(-message_type)
    } else {

        set ::validation_param(message_type) 8
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
        set ::validation_param(minor_version) $param(-minor_version)
    } else {

        set ::validation_param(minor_version) "dont_care"
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        set ::validation_param(version_ptp) $param(-version_ptp)
    } else {

        set ::validation_param(version_ptp) "dont_care"
    }

    #PTP Annouce message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        set ::validation_param(message_length) $param(-message_length)
    } else {

        set ::validation_param(message_length) "dont_care"
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        set ::validation_param(domain_number) $param(-domain_number)
    } else {

        set ::validation_param(domain_number) "dont_care"
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
        set ::validation_param(minor_sdo_id) $param(-minor_sdo_id)
    } else {

        set ::validation_param(minor_sdo_id) "dont_care"
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        set ::validation_param(alternate_master_flag) $param(-alternate_master_flag)
    } else {

        set ::validation_param(alternate_master_flag) "dont_care"
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        set ::validation_param(profile_specific1) $param(-profile_specific1)
    } else {

        set ::validation_param(profile_specific1) "dont_care"
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        set ::validation_param(profile_specific2) $param(-profile_specific2)
    } else {

        set ::validation_param(profile_specific2) "dont_care"
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
        set ::validation_param(secure) $param(-secure)
    } else {

        set ::validation_param(secure) "dont_care"
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        set ::validation_param(leap61) $param(-leap61)
    } else {

        set ::validation_param(leap61) "dont_care"
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        set ::validation_param(leap59) $param(-leap59)
    } else {

        set ::validation_param(leap59) "dont_care"
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        set ::validation_param(current_utc_offset_valid) $param(-current_utc_offset_valid)
    } else {

        set ::validation_param(current_utc_offset_valid) "dont_care"
    }

    #PTP Timescale Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        set ::validation_param(ptp_timescale) $param(-ptp_timescale)
    } else {

        set ::validation_param(ptp_timescale) "dont_care"
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        set ::validation_param(time_traceable) $param(-time_traceable)
    } else {

        set ::validation_param(time_traceable) "dont_care"
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        set ::validation_param(freq_traceable) $param(-freq_traceable)
    } else {

        set ::validation_param(freq_traceable) "dont_care"
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
        set ::validation_param(sync_uncertain) $param(-sync_uncertain)
    } else {

        set ::validation_param(sync_uncertain) "dont_care"
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
        set ::validation_param(message_type_specific) $param(-message_type_specific)
    } else {

        set ::validation_param(message_type_specific) "dont_care"
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        set ::validation_param(sequence_id) $param(-sequence_id)
    } else {

        set ::validation_param(sequence_id) "dont_care"
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        set ::validation_param(control_field) $param(-control_field)
    } else {

        set ::validation_param(control_field) "dont_care"
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame timeout
    if {[info exists param(-timeout)]} {

        set timeout $param(-timeout)
    } else {

        set timeout $::ptp_ha_followup_timeout
    }

    #OUTPUT PARAMETERS
    #all the fields in the matched packet
    foreach key [array names param] {

        set var [string trimleft $key -]

        if [regexp {^recvd_.*} $var] {

            upvar [set param($key)] $var
            set $var ""
        }
    }

    #Received packet timestamp in seconds
    if {[info exists param(-rx_timestamp_sec)]} {

        upvar $param(-rx_timestamp_sec) rx_timestamp_sec
        set rx_timestamp_sec ""
    }

    #Received packet timestamp in nanoseconds
    if {[info exists param(-rx_timestamp_ns)]} {

        upvar $param(-rx_timestamp_ns) rx_timestamp_ns
        set rx_timestamp_ns ""
    }

    #Error Reason
    if {[info exists param(-error_reason)]} {

        upvar $param(-error_reason) error_reason
        set error_reason ""
    }

    #LOGIC GOES HERE
    #Timeout in millisec
    set timeout [expr int($timeout*1000)]
    set error_code 0

    printTimeout $timeout

    set result [pltLib::recv_periodic\
            -session_id           $session_id\
            -port_num             $port_num\
            -filter_id            $filter_id\
            -timeout              $timeout\
            -reset_count          $reset_count\
            -validation_handler   "PTP_HA"\
            -error_code           error_code\
            -rx_timestamp         rx_timestamp_sec\
            -rx_time_nanoseconds  rx_timestamp_ns\
            -pkt_content          pkt_content\
            -pkt_size             pkt_size]

    set error_reason [ptp_ha::tee_recv_error_reason $error_code]

    if {[info exists pkt_content]} {

        set hex $pkt_content

        pbLib::decode_ptp_ha_pkt\
                 -packet                         $hex\
                 -dest_mac                       recvd_dest_mac\
                 -src_mac                        recvd_src_mac\
                 -eth_type                       recvd_eth_type\
                 -vlan_id                        recvd_vlan_id\
                 -vlan_priority                  recvd_vlan_priority\
                 -vlan_dei                       recvd_vlan_dei\
                 -src_ip                         recvd_src_ip\
                 -dest_ip                        recvd_dest_ip\
                 -src_port                       recvd_src_port\
                 -dest_port                      recvd_dest_port\
                 -udp_length                     recvd_udp_length\
                 -udp_checksum                   recvd_udp_checksum\
                 -major_sdo_id                   recvd_major_sdo_id\
                 -message_type                   recvd_message_type\
                 -minor_version                  recvd_minor_version\
                 -version_ptp                    recvd_version_ptp\
                 -message_length                 recvd_message_length\
                 -domain_number                  recvd_domain_number\
                 -minor_sdo_id                   recvd_minor_sdo_id\
                 -alternate_master_flag          recvd_alternate_master_flag\
                 -two_step_flag                  recvd_two_step_flag\
                 -unicast_flag                   recvd_unicast_flag\
                 -profile_specific1              recvd_profile_specific1\
                 -profile_specific2              recvd_profile_specific2\
                 -secure                         recvd_secure\
                 -leap61                         recvd_leap61\
                 -leap59                         recvd_leap59\
                 -current_utc_offset_valid       recvd_current_utc_offset_valid\
                 -ptp_timescale                  recvd_ptp_timescale\
                 -time_traceable                 recvd_time_traceable\
                 -freq_traceable                 recvd_freq_traceable\
                 -sync_uncertain                 recvd_sync_uncertain\
                 -correction_field               recvd_correction_field\
                 -message_type_specific          recvd_message_type_specific\
                 -src_port_number                recvd_src_port_number\
                 -src_clock_identity             recvd_src_clock_identity\
                 -sequence_id                    recvd_sequence_id\
                 -control_field                  recvd_control_field\
                 -log_message_interval           recvd_log_message_interval\
                 -precise_origin_timestamp_sec   recvd_precise_origin_timestamp_sec\
                 -precise_origin_timestamp_ns    recvd_precise_origin_timestamp_ns

    }

    return $result

}


################################################################################
#                                                                              #
#  PROCEDURE NAME         :  ptp_ha::recv_pdelay_req                           #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Reception port                 #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   filter_id                     : (Mandatory) Filter id                      #
#                                                                              #
#   reset_count                   : (Optional)  Reset Count                    #
#                                               (Default: ON)                  #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                                                              #
#   major_sdo_id                  : (Optional)  Major sdo id                   #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default: 2)                   #
#                                                                              #
#   minor_version                 : (Optional)  PTP version number             #
#                                                                              #
#   version_ptp                   : (Optional)  PTP minor version number       #
#                                                                              #
#   message_length                : (Optional)  PTP Annouce message length     #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                                                              #
#   minor_sdo_id                  : (Optional)  Minor Sdo id                   #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                                                              #
#   secure                        : (Optional)  Secure Flag                    #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP Timescale Flag             #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                                                              #
#   sync_uncertain                : (Optional)  Synchronization uncertain Flag #
#                                                                              #
#   message_type_specific         : (Optional)  Message type                   #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default: 1)                   #
#                                                                              #
#   timeout                       : (Optional)  Frame timeout (in seconds)     #
#                                               (Default: 5)                   #
#                                                                              #
#  OUTPUT PARAMETERS      :                                                    #
#                                                                              #
#   recvd_dest_mac                : (Optional)  Destination MAC dddress.       #
#                                                                              #
#   recvd_src_mac                 : (Optional)  Source MAC address.            #
#                                                                              #
#   recvd_two_step_flag           : (Optional)  Two Step Flag                  #
#                                                                              #
#   recvd_unicast_flag            : (Optional)  Unicast Flag                   #
#                                                                              #
#   recvd_correction_field        : (Optional)  Correction Field value         #
#                                                                              #
#   recvd_src_port_number         : (Optional)  Source port number             #
#                                                                              #
#   recvd_src_clock_identity      : (Optional)  Source clock identity          #
#                                                                              #
#   recvd_sequence_id             : (Optional)  Sequence ID                    #
#                                                                              #
#   recvd_log_message_interval    : (Optional)  Log message interval           #
#                                                                              #
#   recvd_origin_timestamp_sec    : (Optional)  Recvd Origin Timestamp Sec     #
#                                                                              #
#   recvd_origin_timestamp_ns     : (Optional)  Recvd Origin Timestamp Ns      #
#                                                                              #
#   rx_timestamp_sec              : (Optional)  Packet's rx time in seconds    #
#                                                                              #
#   rx_timestamp_ns               : (Optional)  Packet's rx time in ns         #
#                                                                              #
#   error_reason                  : (Optional)  Error Reason                   #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is received as         #
#                                         expected)                            #
#                                                                              #
#                         : 0 on failure (If the packet is not received        #
#                                         as expected)                         #
#                                                                              #
#  DEFINITION             : This function is used to receive                   #
#                           PTP pdelay req message                             #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       ptp_ha::recv_pdelay_req\                                               #
#           -port_num                         $port_num\                       #
#           -filter_id                        $filter_id\                      #
#           -reset_count                      $reset_count\                    #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -major_sdo_id                     $major_sdo_id\                   #
#           -message_type                     $message_type\                   #
#           -minor_version                    $minor_version\                  #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -minor_sdo_id                     $minor_sdo_id\                   #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -secure                           $secure\                         #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -sync_uncertain                   $sync_uncertain\                 #
#           -message_type_specific            $message_type_specific\          #
#           -control_field                    $control_field\                  #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#           -recvd_dest_mac                   recvd_dest_mac\                  #
#           -recvd_src_mac                    recvd_src_mac\                   #
#           -recvd_two_step_flag              recvd_two_step_flag\             #
#           -recvd_unicast_flag               recvd_unicast_flag\              #
#           -recvd_correction_field           recvd_correction_field\          #
#           -recvd_src_port_number            recvd_src_port_identity\         #
#           -recvd_src_clock_identity         recvd_src_clock_identity\        #
#           -recvd_sequence_id                recvd_sequence_id\               #
#           -recvd_log_message_interval       recvd_message_interval\          #
#           -recvd_origin_timestamp_sec       recvd_origin_timestamp_sec\      #
#           -recvd_origin_timestamp_ns        recvd_origin_timestamp_ns\       #
#           -rx_timestamp_sec                 rx_timestamp_sec\                #
#           -rx_timestamp_ns                  rx_timestamp_ns\                 #
#           -error_reason                     error_reason                     #
#                                                                              #
################################################################################
proc ptp_ha::recv_pdelay_req {args} {

    array unset ::validation_param

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "ptp_ha::recv_pdelay_req: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #Reception port
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "ptp_ha::recv_pdelay_req: Missing Argument -> port_num"
        return 0
    }

    #Filter id
    if {[info exists param(-filter_id)]} {

        set filter_id $param(-filter_id)
    } else {

        ERRLOG -msg "ptp_ha::recv_pdelay_req: Missing Argument -> filter_id"
        return 0
    }

    #Reset Count
    if {[info exists param(-reset_count)]} {

        set reset_count $param(-reset_count)
    } else {

        set reset_count ON
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        set ::validation_param(eth_type) $param(-eth_type)
    } else {

        set ::validation_param(eth_type) "dont_care"
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
        set ::validation_param(vlan_id) $param(-vlan_id)
    } else {

        if { $::ptp_ha_vlan_encap == "true" } {
            set ::validation_param(vlan_id) $::ptp_ha_vlan_id
        } else {
            set ::validation_param(vlan_id) "dont_care"
        }
    }

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
        set ::validation_param(dest_ip) $param(-dest_ip)
    } else {

        set ::validation_param(dest_ip) "dont_care"
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
        set ::validation_param(src_ip) $param(-src_ip)
    } else {

        set ::validation_param(src_ip) "dont_care"
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
        set ::validation_param(ip_checksum) $param(-ip_checksum)
    } else {

        set ::validation_param(ip_checksum) "dont_care"
    }

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
        set ::validation_param(dest_port) $param(-dest_port)
    } else {

        set ::validation_param(dest_port) "dont_care"
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
        set ::validation_param(src_port) $param(-src_port)
    } else {

        set ::validation_param(src_port) "dont_care"
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
        set ::validation_param(udp_length) $param(-udp_length)
    } else {

        set ::validation_param(udp_length) "dont_care"
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
        set ::validation_param(udp_checksum) $param(-udp_checksum)
    } else {

        set ::validation_param(udp_checksum) "dont_care"
    }

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
        set ::validation_param(major_sdo_id) $param(-major_sdo_id)
    } else {

        set ::validation_param(major_sdo_id) "dont_care"
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        set ::validation_param(message_type) $param(-message_type)
    } else {

        set ::validation_param(message_type) 2
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
        set ::validation_param(minor_version) $param(-minor_version)
    } else {

        set ::validation_param(minor_version) "dont_care"
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        set ::validation_param(version_ptp) $param(-version_ptp)
    } else {

        set ::validation_param(version_ptp) "dont_care"
    }

    #PTP Annouce message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        set ::validation_param(message_length) $param(-message_length)
    } else {

        set ::validation_param(message_length) "dont_care"
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        set ::validation_param(domain_number) $param(-domain_number)
    } else {

        set ::validation_param(domain_number) "dont_care"
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
        set ::validation_param(minor_sdo_id) $param(-minor_sdo_id)
    } else {

        set ::validation_param(minor_sdo_id) "dont_care"
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        set ::validation_param(alternate_master_flag) $param(-alternate_master_flag)
    } else {

        set ::validation_param(alternate_master_flag) "dont_care"
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        set ::validation_param(profile_specific1) $param(-profile_specific1)
    } else {

        set ::validation_param(profile_specific1) "dont_care"
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        set ::validation_param(profile_specific2) $param(-profile_specific2)
    } else {

        set ::validation_param(profile_specific2) "dont_care"
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
        set ::validation_param(secure) $param(-secure)
    } else {

        set ::validation_param(secure) "dont_care"
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        set ::validation_param(leap61) $param(-leap61)
    } else {

        set ::validation_param(leap61) "dont_care"
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        set ::validation_param(leap59) $param(-leap59)
    } else {

        set ::validation_param(leap59) "dont_care"
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        set ::validation_param(current_utc_offset_valid) $param(-current_utc_offset_valid)
    } else {

        set ::validation_param(current_utc_offset_valid) "dont_care"
    }

    #PTP Timescale Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        set ::validation_param(ptp_timescale) $param(-ptp_timescale)
    } else {

        set ::validation_param(ptp_timescale) "dont_care"
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        set ::validation_param(time_traceable) $param(-time_traceable)
    } else {

        set ::validation_param(time_traceable) "dont_care"
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        set ::validation_param(freq_traceable) $param(-freq_traceable)
    } else {

        set ::validation_param(freq_traceable) "dont_care"
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
        set ::validation_param(sync_uncertain) $param(-sync_uncertain)
    } else {

        set ::validation_param(sync_uncertain) "dont_care"
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
        set ::validation_param(message_type_specific) $param(-message_type_specific)
    } else {

        set ::validation_param(message_type_specific) "dont_care"
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        set ::validation_param(sequence_id) $param(-sequence_id)
    } else {

        set ::validation_param(sequence_id) "dont_care"
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        set ::validation_param(control_field) $param(-control_field)
    } else {

        set ::validation_param(control_field) "dont_care"
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame timeout
    if {[info exists param(-timeout)]} {

        set timeout $param(-timeout)
    } else {

        set timeout $::ptp_ha_pdelayreq_timeout
    }

    #OUTPUT PARAMETERS
    #all the fields in the matched packet
    foreach key [array names param] {

        set var [string trimleft $key -]

        if [regexp {^recvd_.*} $var] {

            upvar [set param($key)] $var
            set $var ""
        }
    }

    #Received packet timestamp in seconds
    if {[info exists param(-rx_timestamp_sec)]} {

        upvar $param(-rx_timestamp_sec) rx_timestamp_sec
        set rx_timestamp_sec ""
    }

    #Received packet timestamp in nanoseconds
    if {[info exists param(-rx_timestamp_ns)]} {

        upvar $param(-rx_timestamp_ns) rx_timestamp_ns
        set rx_timestamp_ns ""
    }

    #Error Reason
    if {[info exists param(-error_reason)]} {

        upvar $param(-error_reason) error_reason
        set error_reason ""
    }

    #LOGIC GOES HERE
    #Timeout in millisec
    set timeout [expr int($timeout*1000)]
    set error_code 0

    printTimeout $timeout

    set result [pltLib::recv_periodic\
            -session_id           $session_id\
            -port_num             $port_num\
            -filter_id            $filter_id\
            -timeout              $timeout\
            -reset_count          $reset_count\
            -validation_handler   "PTP_HA"\
            -error_code           error_code\
            -rx_timestamp         rx_timestamp_sec\
            -rx_time_nanoseconds  rx_timestamp_ns\
            -pkt_content          pkt_content\
            -pkt_size             pkt_size]

    set error_reason [ptp_ha::tee_recv_error_reason $error_code]

    if {[info exists pkt_content]} {

        set hex $pkt_content

        pbLib::decode_ptp_ha_pkt\
                 -packet                         $hex\
                 -dest_mac                       recvd_dest_mac\
                 -src_mac                        recvd_src_mac\
                 -eth_type                       recvd_eth_type\
                 -vlan_id                        recvd_vlan_id\
                 -vlan_priority                  recvd_vlan_priority\
                 -vlan_dei                       recvd_vlan_dei\
                 -src_ip                         recvd_src_ip\
                 -dest_ip                        recvd_dest_ip\
                 -src_port                       recvd_src_port\
                 -dest_port                      recvd_dest_port\
                 -udp_length                     recvd_udp_length\
                 -udp_checksum                   recvd_udp_checksum\
                 -major_sdo_id                   recvd_major_sdo_id\
                 -message_type                   recvd_message_type\
                 -minor_version                  recvd_minor_version\
                 -version_ptp                    recvd_version_ptp\
                 -message_length                 recvd_message_length\
                 -domain_number                  recvd_domain_number\
                 -minor_sdo_id                   recvd_minor_sdo_id\
                 -alternate_master_flag          recvd_alternate_master_flag\
                 -two_step_flag                  recvd_two_step_flag\
                 -unicast_flag                   recvd_unicast_flag\
                 -profile_specific1              recvd_profile_specific1\
                 -profile_specific2              recvd_profile_specific2\
                 -secure                         recvd_secure\
                 -leap61                         recvd_leap61\
                 -leap59                         recvd_leap59\
                 -current_utc_offset_valid       recvd_current_utc_offset_valid\
                 -ptp_timescale                  recvd_ptp_timescale\
                 -time_traceable                 recvd_time_traceable\
                 -freq_traceable                 recvd_freq_traceable\
                 -sync_uncertain                 recvd_sync_uncertain\
                 -correction_field               recvd_correction_field\
                 -message_type_specific          recvd_message_type_specific\
                 -src_port_number                recvd_src_port_number\
                 -src_clock_identity             recvd_src_clock_identity\
                 -sequence_id                    recvd_sequence_id\
                 -control_field                  recvd_control_field\
                 -log_message_interval           recvd_log_message_interval\
                 -origin_timestamp_sec           recvd_origin_timestamp_sec\
                 -origin_timestamp_ns            recvd_origin_timestamp_ns

    }

    return $result

}


################################################################################
#                                                                              #
#  PROCEDURE NAME         :  ptp_ha::recv_pdelay_resp                          #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Reception port                 #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   filter_id                     : (Mandatory) Filter id                      #
#                                                                              #
#   reset_count                   : (Optional)  Reset Count                    #
#                                               (Default: ON)                  #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                                                              #
#   major_sdo_id                  : (Optional)  Major sdo id                   #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default: 3)                   #
#                                                                              #
#   minor_version                 : (Optional)  PTP version number             #
#                                                                              #
#   version_ptp                   : (Optional)  PTP minor version number       #
#                                                                              #
#   message_length                : (Optional)  PTP Annouce message length     #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                                                              #
#   minor_sdo_id                  : (Optional)  Minor Sdo id                   #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                                                              #
#   secure                        : (Optional)  Secure Flag                    #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP Timescale Flag             #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                                                              #
#   sync_uncertain                : (Optional)  Synchronization uncertain Flag #
#                                                                              #
#   message_type_specific         : (Optional)  Message type                   #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                                                              #
#   requesting_clock_identity     : (Optional)  Requesting Clock Identity      #
#                                                                              #
#   requesting_port_number        : (Optional)  Requesting Port Number         #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default: 1)                   #
#                                                                              #
#   timeout                       : (Optional)  Frame timeout (in seconds)     #
#                                               (Default: 5)                   #
#                                                                              #
#  OUTPUT PARAMETERS      :                                                    #
#                                                                              #
#   recvd_dest_mac                : (Optional)  Destination MAC dddress.       #
#                                                                              #
#   recvd_src_mac                 : (Optional)  Source MAC address.            #
#                                                                              #
#   recvd_two_step_flag           : (Optional)  Two Step Flag                  #
#                                                                              #
#   recvd_unicast_flag            : (Optional)  Unicast Flag                   #
#                                                                              #
#   recvd_correction_field        : (Optional)  Correction Field value         #
#                                                                              #
#   recvd_src_port_number         : (Optional)  Source port number             #
#                                                                              #
#   recvd_src_clock_identity      : (Optional)  Source clock identity          #
#                                                                              #
#   recvd_sequence_id             : (Optional)  Sequence ID                    #
#                                                                              #
#   recvd_log_message_interval    : (Optional)  Log message interval           #
#                                                                              #
#   rx_timestamp_sec              : (Optional)  Packet's rx time in seconds    #
#                                                                              #
#   rx_timestamp_ns               : (Optional)  Packet's rx time in ns         #
#                                                                              #
#   error_reason                  : (Optional)  Error Reason                   #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is received as         #
#                                         expected)                            #
#                                                                              #
#                         : 0 on failure (If the packet is not received        #
#                                         as expected)                         #
#                                                                              #
#  DEFINITION             : This function is used to receive                   #
#                           PTP pdelay resp message                            #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#  ptp_ha::recv_pdelay_resp\                                                   #
#   -port_num                         $port_num\                               #
#   -filter_id                        $filter_id\                              #
#   -reset_count                      $reset_count\                            #
#   -eth_type                         $eth_type\                               #
#   -vlan_id                          $vlan_id\                                #
#   -dest_ip                          $dest_ip\                                #
#   -src_ip                           $src_ip\                                 #
#   -ip_checksum                      $ip_checksum\                            #
#   -dest_port                        $dest_port\                              #
#   -src_port                         $src_port\                               #
#   -udp_length                       $udp_length\                             #
#   -udp_checksum                     $udp_checksum\                           #
#   -major_sdo_id                     $major_sdo_id\                           #
#   -message_type                     $message_type\                           #
#   -minor_version                    $minor_version\                          #
#   -version_ptp                      $version_ptp\                            #
#   -message_length                   $message_length\                         #
#   -domain_number                    $domain_number\                          #
#   -minor_sdo_id                     $minor_sdo_id\                           #
#   -alternate_master_flag            $alternate_master_flag\                  #
#   -profile_specific1                $profile_specific1\                      #
#   -profile_specific2                $profile_specific2\                      #
#   -secure                           $secure\                                 #
#   -leap61                           $leap61\                                 #
#   -leap59                           $leap59\                                 #
#   -current_utc_offset_valid         $current_utc_offset_valid\               #
#   -ptp_timescale                    $ptp_timescale\                          #
#   -time_traceable                   $time_traceable\                         #
#   -freq_traceable                   $freq_traceable\                         #
#   -sync_uncertain                   $sync_uncertain\                         #
#   -message_type_specific            $message_type_specific\                  #
#   -control_field                    $control_field\                          #
#   -count                            $count\                                  #
#   -interval                         $interval                                #
#   -requesting_clock_identity        $requesting_clock_identity\              #
#   -requesting_port_number           $requesting_port_number\                 #
#   -recvd_dest_mac                   recvd_dest_mac\                          #
#   -recvd_src_mac                    recvd_src_mac\                           #
#   -recvd_two_step_flag              recvd_two_step_flag\                     #
#   -recvd_unicast_flag               recvd_unicast_flag\                      #
#   -recvd_correction_field           recvd_correction_field\                  #
#   -recvd_src_port_number            recvd_src_port_identity\                 #
#   -recvd_src_clock_identity         recvd_src_clock_identity\                #
#   -recvd_sequence_id                recvd_sequence_id\                       #
#   -recvd_log_message_interval       recvd_message_interval\                  #
#   -recvd_requesting_clock_identity  recvd_requesting_clock_identity\         #
#   -recvd_requesting_port_number     recvd_requesting_port_number\            #
#   -rx_timestamp_sec                 rx_timestamp_sec\                        #
#   -rx_timestamp_ns                  rx_timestamp_ns\                         #
#   -error_reason                     error_reason                             #
#                                                                              #
################################################################################
proc ptp_ha::recv_pdelay_resp {args} {

    array unset ::validation_param

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "ptp_ha::recv_pdelay_resp: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #Reception port
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "ptp_ha::recv_pdelay_resp: Missing Argument -> port_num"
        return 0
    }

    #Filter id
    if {[info exists param(-filter_id)]} {

        set filter_id $param(-filter_id)
    } else {

        ERRLOG -msg "ptp_ha::recv_pdelay_resp: Missing Argument -> filter_id"
        return 0
    }

    #Reset Count
    if {[info exists param(-reset_count)]} {

        set reset_count $param(-reset_count)
    } else {

        set reset_count ON
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        set ::validation_param(eth_type) $param(-eth_type)
    } else {

        set ::validation_param(eth_type) "dont_care"
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
        set ::validation_param(vlan_id) $param(-vlan_id)
    } else {

        if { $::ptp_ha_vlan_encap == "true" } {
            set ::validation_param(vlan_id) $::ptp_ha_vlan_id
        } else {
            set ::validation_param(vlan_id) "dont_care"
        }
    }

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
        set ::validation_param(dest_ip) $param(-dest_ip)
    } else {

        set ::validation_param(dest_ip) "dont_care"
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
        set ::validation_param(src_ip) $param(-src_ip)
    } else {

        set ::validation_param(src_ip) "dont_care"
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
        set ::validation_param(ip_checksum) $param(-ip_checksum)
    } else {

        set ::validation_param(ip_checksum) "dont_care"
    }

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
        set ::validation_param(dest_port) $param(-dest_port)
    } else {

        set ::validation_param(dest_port) "dont_care"
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
        set ::validation_param(src_port) $param(-src_port)
    } else {

        set ::validation_param(src_port) "dont_care"
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
        set ::validation_param(udp_length) $param(-udp_length)
    } else {

        set ::validation_param(udp_length) "dont_care"
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
        set ::validation_param(udp_checksum) $param(-udp_checksum)
    } else {

        set ::validation_param(udp_checksum) "dont_care"
    }

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
        set ::validation_param(major_sdo_id) $param(-major_sdo_id)
    } else {

        set ::validation_param(major_sdo_id) "dont_care"
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        set ::validation_param(message_type) $param(-message_type)
    } else {

        set ::validation_param(message_type) 3
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
        set ::validation_param(minor_version) $param(-minor_version)
    } else {

        set ::validation_param(minor_version) "dont_care"
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        set ::validation_param(version_ptp) $param(-version_ptp)
    } else {

        set ::validation_param(version_ptp) "dont_care"
    }

    #PTP Annouce message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        set ::validation_param(message_length) $param(-message_length)
    } else {

        set ::validation_param(message_length) "dont_care"
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        set ::validation_param(domain_number) $param(-domain_number)
    } else {

        set ::validation_param(domain_number) "dont_care"
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
        set ::validation_param(minor_sdo_id) $param(-minor_sdo_id)
    } else {

        set ::validation_param(minor_sdo_id) "dont_care"
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        set ::validation_param(alternate_master_flag) $param(-alternate_master_flag)
    } else {

        set ::validation_param(alternate_master_flag) "dont_care"
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        set ::validation_param(profile_specific1) $param(-profile_specific1)
    } else {

        set ::validation_param(profile_specific1) "dont_care"
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        set ::validation_param(profile_specific2) $param(-profile_specific2)
    } else {

        set ::validation_param(profile_specific2) "dont_care"
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
        set ::validation_param(secure) $param(-secure)
    } else {

        set ::validation_param(secure) "dont_care"
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        set ::validation_param(leap61) $param(-leap61)
    } else {

        set ::validation_param(leap61) "dont_care"
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        set ::validation_param(leap59) $param(-leap59)
    } else {

        set ::validation_param(leap59) "dont_care"
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        set ::validation_param(current_utc_offset_valid) $param(-current_utc_offset_valid)
    } else {

        set ::validation_param(current_utc_offset_valid) "dont_care"
    }

    #PTP Timescale Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        set ::validation_param(ptp_timescale) $param(-ptp_timescale)
    } else {

        set ::validation_param(ptp_timescale) "dont_care"
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        set ::validation_param(time_traceable) $param(-time_traceable)
    } else {

        set ::validation_param(time_traceable) "dont_care"
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        set ::validation_param(freq_traceable) $param(-freq_traceable)
    } else {

        set ::validation_param(freq_traceable) "dont_care"
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
        set ::validation_param(sync_uncertain) $param(-sync_uncertain)
    } else {

        set ::validation_param(sync_uncertain) "dont_care"
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
        set ::validation_param(message_type_specific) $param(-message_type_specific)
    } else {

        set ::validation_param(message_type_specific) "dont_care"
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        set ::validation_param(sequence_id) $param(-sequence_id)
    } else {

        set ::validation_param(sequence_id) "dont_care"
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        set ::validation_param(control_field) $param(-control_field)
    } else {

        set ::validation_param(control_field) "dont_care"
    }

    #Requesting Clock Identity
    if {[info exists param(-requesting_clock_identity)]} {

        set ::validation_param(requesting_clock_identity) $param(-requesting_clock_identity)
    } else {

        set ::validation_param(requesting_clock_identity) "dont_care"
    }

    #Requesting Port Number
    if {[info exists param(-requesting_port_number)]} {

        set ::validation_param(requesting_port_number) $param(-requesting_port_number)
    } else {

        set ::validation_param(requesting_port_number) "dont_care"
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame timeout
    if {[info exists param(-timeout)]} {

        set timeout $param(-timeout)
    } else {

        set timeout $::ptp_ha_pdelayresp_timeout
    }

    #OUTPUT PARAMETERS
    #all the fields in the matched packet
    foreach key [array names param] {

        set var [string trimleft $key -]

        if [regexp {^recvd_.*} $var] {

            upvar [set param($key)] $var
            set $var ""
        }
    }

    #Received packet timestamp in seconds
    if {[info exists param(-rx_timestamp_sec)]} {

        upvar $param(-rx_timestamp_sec) rx_timestamp_sec
        set rx_timestamp_sec ""
    }

    #Received packet timestamp in nanoseconds
    if {[info exists param(-rx_timestamp_ns)]} {

        upvar $param(-rx_timestamp_ns) rx_timestamp_ns
        set rx_timestamp_ns ""
    }

    #Error Reason
    if {[info exists param(-error_reason)]} {

        upvar $param(-error_reason) error_reason
        set error_reason ""
    }

    #LOGIC GOES HERE
    #Timeout in millisec
    set timeout [expr int($timeout*1000)]
    set error_code 0

    printTimeout $timeout

    set result [pltLib::recv_periodic\
            -session_id           $session_id\
            -port_num             $port_num\
            -filter_id            $filter_id\
            -timeout              $timeout\
            -reset_count          $reset_count\
            -validation_handler   "PTP_HA"\
            -error_code           error_code\
            -rx_timestamp         rx_timestamp_sec\
            -rx_time_nanoseconds  rx_timestamp_ns\
            -pkt_content          pkt_content\
            -pkt_size             pkt_size]

    set error_reason [ptp_ha::tee_recv_error_reason $error_code]

    if {[info exists pkt_content]} {

        set hex $pkt_content

        pbLib::decode_ptp_ha_pkt\
                 -packet                         $hex\
                 -dest_mac                       recvd_dest_mac\
                 -src_mac                        recvd_src_mac\
                 -eth_type                       recvd_eth_type\
                 -vlan_id                        recvd_vlan_id\
                 -vlan_priority                  recvd_vlan_priority\
                 -vlan_dei                       recvd_vlan_dei\
                 -src_ip                         recvd_src_ip\
                 -dest_ip                        recvd_dest_ip\
                 -src_port                       recvd_src_port\
                 -dest_port                      recvd_dest_port\
                 -udp_length                     recvd_udp_length\
                 -udp_checksum                   recvd_udp_checksum\
                 -major_sdo_id                   recvd_major_sdo_id\
                 -message_type                   recvd_message_type\
                 -minor_version                  recvd_minor_version\
                 -version_ptp                    recvd_version_ptp\
                 -message_length                 recvd_message_length\
                 -domain_number                  recvd_domain_number\
                 -minor_sdo_id                   recvd_minor_sdo_id\
                 -alternate_master_flag          recvd_alternate_master_flag\
                 -two_step_flag                  recvd_two_step_flag\
                 -unicast_flag                   recvd_unicast_flag\
                 -profile_specific1              recvd_profile_specific1\
                 -profile_specific2              recvd_profile_specific2\
                 -secure                         recvd_secure\
                 -leap61                         recvd_leap61\
                 -leap59                         recvd_leap59\
                 -current_utc_offset_valid       recvd_current_utc_offset_valid\
                 -ptp_timescale                  recvd_ptp_timescale\
                 -time_traceable                 recvd_time_traceable\
                 -freq_traceable                 recvd_freq_traceable\
                 -sync_uncertain                 recvd_sync_uncertain\
                 -correction_field               recvd_correction_field\
                 -message_type_specific          recvd_message_type_specific\
                 -src_port_number                recvd_src_port_number\
                 -src_clock_identity             recvd_src_clock_identity\
                 -sequence_id                    recvd_sequence_id\
                 -control_field                  recvd_control_field\
                 -log_message_interval           recvd_log_message_interval\
                 -requesting_port_number         recvd_requesting_port_number\
                 -requesting_clock_identity      recvd_requesting_clock_identity\
                 -request_receipt_timestamp_sec  recvd_request_receipt_timestamp_sec\
                 -request_receipt_timestamp_ns   recvd_request_receipt_timestamp_ns

    }

    return $result

}


################################################################################
#                                                                              #
#  PROCEDURE NAME         :  ptp_ha::recv_pdelay_resp_followup                 #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Reception port                 #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   filter_id                     : (Mandatory) Filter id                      #
#                                                                              #
#   reset_count                   : (Optional)  Reset Count                    #
#                                               (Default: ON)                  #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                                                              #
#   major_sdo_id                  : (Optional)  Major sdo id                   #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default: 10)                  #
#                                                                              #
#   minor_version                 : (Optional)  PTP version number             #
#                                                                              #
#   version_ptp                   : (Optional)  PTP minor version number       #
#                                                                              #
#   message_length                : (Optional)  PTP Annouce message length     #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                                                              #
#   minor_sdo_id                  : (Optional)  Minor Sdo id                   #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                                                              #
#   secure                        : (Optional)  Secure Flag                    #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP Timescale Flag             #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                                                              #
#   sync_uncertain                : (Optional)  Synchronization uncertain Flag #
#                                                                              #
#   message_type_specific         : (Optional)  Message type                   #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                                                              #
#   requesting_clock_identity     : (Optional)  Requesting Clock Identity      #
#                                                                              #
#   requesting_port_number        : (Optional)  Requesting Port Number         #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default: 1)                   #
#                                                                              #
#   timeout                       : (Optional)  Frame timeout (in seconds)     #
#                                               (Default: 5)                   #
#                                                                              #
#  OUTPUT PARAMETERS      :                                                    #
#                                                                              #
#   recvd_dest_mac                : (Optional)  Destination MAC dddress.       #
#                                                                              #
#   recvd_src_mac                 : (Optional)  Source MAC address.            #
#                                                                              #
#   recvd_two_step_flag           : (Optional)  Two Step Flag                  #
#                                                                              #
#   recvd_unicast_flag            : (Optional)  Unicast Flag                   #
#                                                                              #
#   recvd_correction_field        : (Optional)  Correction Field value         #
#                                                                              #
#   recvd_src_port_number         : (Optional)  Source port number             #
#                                                                              #
#   recvd_src_clock_identity      : (Optional)  Source clock identity          #
#                                                                              #
#   recvd_sequence_id             : (Optional)  Sequence ID                    #
#                                                                              #
#   recvd_log_message_interval    : (Optional)  Log message interval           #
#                                                                              #
#   rx_timestamp_sec              : (Optional)  Packet's rx time in seconds    #
#                                                                              #
#   rx_timestamp_ns               : (Optional)  Packet's rx time in ns         #
#                                                                              #
#   error_reason                  : (Optional)  Error Reason                   #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is received as         #
#                                         expected)                            #
#                                                                              #
#                         : 0 on failure (If the packet is not received        #
#                                         as expected)                         #
#                                                                              #
#  DEFINITION             : This function is used to receive                   #
#                           PTP pdelay resp followup message                   #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#   ptp_ha::recv_pdelay_resp_followup\                                         #
#    -port_num                         $port_num\                              #
#    -filter_id                        $filter_id\                             #
#    -reset_count                      $reset_count\                           #
#    -eth_type                         $eth_type\                              #
#    -vlan_id                          $vlan_id\                               #
#    -dest_ip                          $dest_ip\                               #
#    -src_ip                           $src_ip\                                #
#    -ip_checksum                      $ip_checksum\                           #
#    -dest_port                        $dest_port\                             #
#    -src_port                         $src_port\                              #
#    -udp_length                       $udp_length\                            #
#    -udp_checksum                     $udp_checksum\                          #
#    -major_sdo_id                     $major_sdo_id\                          #
#    -message_type                     $message_type\                          #
#    -minor_version                    $minor_version\                         #
#    -version_ptp                      $version_ptp\                           #
#    -message_length                   $message_length\                        #
#    -domain_number                    $domain_number\                         #
#    -minor_sdo_id                     $minor_sdo_id\                          #
#    -alternate_master_flag            $alternate_master_flag\                 #
#    -profile_specific1                $profile_specific1\                     #
#    -profile_specific2                $profile_specific2\                     #
#    -secure                           $secure\                                #
#    -leap61                           $leap61\                                #
#    -leap59                           $leap59\                                #
#    -current_utc_offset_valid         $current_utc_offset_valid\              #
#    -ptp_timescale                    $ptp_timescale\                         #
#    -time_traceable                   $time_traceable\                        #
#    -freq_traceable                   $freq_traceable\                        #
#    -sync_uncertain                   $sync_uncertain\                        #
#    -message_type_specific            $message_type_specific\                 #
#    -control_field                    $control_field\                         #
#    -count                            $count\                                 #
#    -interval                         $interval                               #
#    -recvd_dest_mac                   recvd_dest_mac\                         #
#    -recvd_src_mac                    recvd_src_mac\                          #
#    -recvd_two_step_flag              recvd_two_step_flag\                    #
#    -recvd_unicast_flag               recvd_unicast_flag\                     #
#    -recvd_correction_field           recvd_correction_field\                 #
#    -recvd_src_port_number            recvd_src_port_identity\                #
#    -recvd_src_clock_identity         recvd_src_clock_identity\               #
#    -recvd_sequence_id                recvd_sequence_id\                      #
#    -recvd_log_message_interval       recvd_message_interval\                 #
#    -recvd_response_origin_timestamp_sec  recvd_response_origin_timestamp_sec\#
#    -recvd_response_origin_timestamp_ns  recvd_response_origin_timestamp_ns\  #
#    -recvd_requesting_clock_identity  recvd_requesting_clock_identity\        #
#    -recvd_requesting_port_number     recvd_requesting_port_number\           #
#    -rx_timestamp_sec                 rx_timestamp_sec\                       #
#    -rx_timestamp_ns                  rx_timestamp_ns\                        #
#    -error_reason                     error_reason                            #
#                                                                              #
################################################################################
proc ptp_ha::recv_pdelay_resp_followup {args} {

    array unset ::validation_param

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "ptp_ha::recv_pdelay_resp_followup: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #Reception port
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "ptp_ha::recv_pdelay_resp_followup: Missing Argument -> port_num"
        return 0
    }

    #Filter id
    if {[info exists param(-filter_id)]} {

        set filter_id $param(-filter_id)
    } else {

        ERRLOG -msg "ptp_ha::recv_pdelay_resp_followup: Missing Argument -> filter_id"
        return 0
    }

    #Reset Count
    if {[info exists param(-reset_count)]} {

        set reset_count $param(-reset_count)
    } else {

        set reset_count ON
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        set ::validation_param(eth_type) $param(-eth_type)
    } else {

        set ::validation_param(eth_type) "dont_care"
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
        set ::validation_param(vlan_id) $param(-vlan_id)
    } else {

        if { $::ptp_ha_vlan_encap == "true" } {
            set ::validation_param(vlan_id) $::ptp_ha_vlan_id
        } else {
            set ::validation_param(vlan_id) "dont_care"
        }
    }

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
        set ::validation_param(dest_ip) $param(-dest_ip)
    } else {

        set ::validation_param(dest_ip) "dont_care"
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
        set ::validation_param(src_ip) $param(-src_ip)
    } else {

        set ::validation_param(src_ip) "dont_care"
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
        set ::validation_param(ip_checksum) $param(-ip_checksum)
    } else {

        set ::validation_param(ip_checksum) "dont_care"
    }

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
        set ::validation_param(dest_port) $param(-dest_port)
    } else {

        set ::validation_param(dest_port) "dont_care"
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
        set ::validation_param(src_port) $param(-src_port)
    } else {

        set ::validation_param(src_port) "dont_care"
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
        set ::validation_param(udp_length) $param(-udp_length)
    } else {

        set ::validation_param(udp_length) "dont_care"
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
        set ::validation_param(udp_checksum) $param(-udp_checksum)
    } else {

        set ::validation_param(udp_checksum) "dont_care"
    }

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
        set ::validation_param(major_sdo_id) $param(-major_sdo_id)
    } else {

        set ::validation_param(major_sdo_id) "dont_care"
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        set ::validation_param(message_type) $param(-message_type)
    } else {

        set ::validation_param(message_type) 10
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
        set ::validation_param(minor_version) $param(-minor_version)
    } else {

        set ::validation_param(minor_version) "dont_care"
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        set ::validation_param(version_ptp) $param(-version_ptp)
    } else {

        set ::validation_param(version_ptp) "dont_care"
    }

    #PTP Annouce message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        set ::validation_param(message_length) $param(-message_length)
    } else {

        set ::validation_param(message_length) "dont_care"
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        set ::validation_param(domain_number) $param(-domain_number)
    } else {

        set ::validation_param(domain_number) "dont_care"
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
        set ::validation_param(minor_sdo_id) $param(-minor_sdo_id)
    } else {

        set ::validation_param(minor_sdo_id) "dont_care"
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        set ::validation_param(alternate_master_flag) $param(-alternate_master_flag)
    } else {

        set ::validation_param(alternate_master_flag) "dont_care"
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        set ::validation_param(profile_specific1) $param(-profile_specific1)
    } else {

        set ::validation_param(profile_specific1) "dont_care"
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        set ::validation_param(profile_specific2) $param(-profile_specific2)
    } else {

        set ::validation_param(profile_specific2) "dont_care"
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
        set ::validation_param(secure) $param(-secure)
    } else {

        set ::validation_param(secure) "dont_care"
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        set ::validation_param(leap61) $param(-leap61)
    } else {

        set ::validation_param(leap61) "dont_care"
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        set ::validation_param(leap59) $param(-leap59)
    } else {

        set ::validation_param(leap59) "dont_care"
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        set ::validation_param(current_utc_offset_valid) $param(-current_utc_offset_valid)
    } else {

        set ::validation_param(current_utc_offset_valid) "dont_care"
    }

    #PTP Timescale Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        set ::validation_param(ptp_timescale) $param(-ptp_timescale)
    } else {

        set ::validation_param(ptp_timescale) "dont_care"
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        set ::validation_param(time_traceable) $param(-time_traceable)
    } else {

        set ::validation_param(time_traceable) "dont_care"
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        set ::validation_param(freq_traceable) $param(-freq_traceable)
    } else {

        set ::validation_param(freq_traceable) "dont_care"
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
        set ::validation_param(sync_uncertain) $param(-sync_uncertain)
    } else {

        set ::validation_param(sync_uncertain) "dont_care"
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
        set ::validation_param(message_type_specific) $param(-message_type_specific)
    } else {

        set ::validation_param(message_type_specific) "dont_care"
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        set ::validation_param(sequence_id) $param(-sequence_id)
    } else {

        set ::validation_param(sequence_id) "dont_care"
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        set ::validation_param(control_field) $param(-control_field)
    } else {

        set ::validation_param(control_field) "dont_care"
    }

    #Requesting Clock Identity
    if {[info exists param(-requesting_clock_identity)]} {

        set ::validation_param(requesting_clock_identity) $param(-requesting_clock_identity)
    } else {

        set ::validation_param(requesting_clock_identity) "dont_care"
    }

    #Requesting Port Number
    if {[info exists param(-requesting_port_number)]} {

        set ::validation_param(requesting_port_number) $param(-requesting_port_number)
    } else {

        set ::validation_param(requesting_port_number) "dont_care"
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame timeout
    if {[info exists param(-timeout)]} {

        set timeout $param(-timeout)
    } else {

        set timeout $::ptp_ha_pdelayresp_followup_timeout
    }

    #OUTPUT PARAMETERS
    #all the fields in the matched packet
    foreach key [array names param] {

        set var [string trimleft $key -]

        if [regexp {^recvd_.*} $var] {

            upvar [set param($key)] $var
            set $var ""
        }
    }

    #Received packet timestamp in seconds
    if {[info exists param(-rx_timestamp_sec)]} {

        upvar $param(-rx_timestamp_sec) rx_timestamp_sec
        set rx_timestamp_sec ""
    }

    #Received packet timestamp in nanoseconds
    if {[info exists param(-rx_timestamp_ns)]} {

        upvar $param(-rx_timestamp_ns) rx_timestamp_ns
        set rx_timestamp_ns ""
    }

    #Error Reason
    if {[info exists param(-error_reason)]} {

        upvar $param(-error_reason) error_reason
        set error_reason ""
    }

    #LOGIC GOES HERE
    #Timeout in millisec
    set timeout [expr int($timeout*1000)]
    set error_code 0

    printTimeout $timeout

    set result [pltLib::recv_periodic\
            -session_id           $session_id\
            -port_num             $port_num\
            -filter_id            $filter_id\
            -timeout              $timeout\
            -reset_count          $reset_count\
            -validation_handler   "PTP_HA"\
            -error_code           error_code\
            -rx_timestamp         rx_timestamp_sec\
            -rx_time_nanoseconds  rx_timestamp_ns\
            -pkt_content          pkt_content\
            -pkt_size             pkt_size]

    set error_reason [ptp_ha::tee_recv_error_reason $error_code]

    if {[info exists pkt_content]} {

        set hex $pkt_content

        pbLib::decode_ptp_ha_pkt\
                 -packet                         $hex\
                 -dest_mac                       recvd_dest_mac\
                 -src_mac                        recvd_src_mac\
                 -eth_type                       recvd_eth_type\
                 -vlan_id                        recvd_vlan_id\
                 -vlan_priority                  recvd_vlan_priority\
                 -vlan_dei                       recvd_vlan_dei\
                 -src_ip                         recvd_src_ip\
                 -dest_ip                        recvd_dest_ip\
                 -src_port                       recvd_src_port\
                 -dest_port                      recvd_dest_port\
                 -udp_length                     recvd_udp_length\
                 -udp_checksum                   recvd_udp_checksum\
                 -major_sdo_id                   recvd_major_sdo_id\
                 -message_type                   recvd_message_type\
                 -minor_version                  recvd_minor_version\
                 -version_ptp                    recvd_version_ptp\
                 -message_length                 recvd_message_length\
                 -domain_number                  recvd_domain_number\
                 -minor_sdo_id                   recvd_minor_sdo_id\
                 -alternate_master_flag          recvd_alternate_master_flag\
                 -two_step_flag                  recvd_two_step_flag\
                 -unicast_flag                   recvd_unicast_flag\
                 -profile_specific1              recvd_profile_specific1\
                 -profile_specific2              recvd_profile_specific2\
                 -secure                         recvd_secure\
                 -leap61                         recvd_leap61\
                 -leap59                         recvd_leap59\
                 -current_utc_offset_valid       recvd_current_utc_offset_valid\
                 -ptp_timescale                  recvd_ptp_timescale\
                 -time_traceable                 recvd_time_traceable\
                 -freq_traceable                 recvd_freq_traceable\
                 -sync_uncertain                 recvd_sync_uncertain\
                 -correction_field               recvd_correction_field\
                 -message_type_specific          recvd_message_type_specific\
                 -src_port_number                recvd_src_port_number\
                 -src_clock_identity             recvd_src_clock_identity\
                 -sequence_id                    recvd_sequence_id\
                 -control_field                  recvd_control_field\
                 -log_message_interval           recvd_log_message_interval\
                 -requesting_port_number         recvd_requesting_port_number\
                 -requesting_clock_identity      recvd_requesting_clock_identity\
                 -response_origin_timestamp_sec  recvd_response_origin_timestamp_sec\
                 -response_origin_timestamp_ns   recvd_response_origin_timestamp_ns

    }

    return $result

}


################################################################################
#                                                                              #
#  PROCEDURE NAME         :  ptp_ha::recv_signal                               #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Reception port                 #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   filter_id                     : (Mandatory) Filter id                      #
#                                                                              #
#   reset_count                   : (Optional)  Reset Count                    #
#                                               (Default: ON)                  #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                                                              #
#   major_sdo_id                  : (Optional)  Major sdo id                   #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default: 12)                  #
#                                                                              #
#   minor_version                 : (Optional)  PTP version number             #
#                                                                              #
#   version_ptp                   : (Optional)  PTP minor version number       #
#                                                                              #
#   message_length                : (Optional)  PTP Annouce message length     #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                                                              #
#   minor_sdo_id                  : (Optional)  Minor Sdo id                   #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                                                              #
#   secure                        : (Optional)  Secure Flag                    #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP Timescale Flag             #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                                                              #
#   sync_uncertain                : (Optional)  Synchronization uncertain Flag #
#                                                                              #
#   message_type_specific         : (Optional)  Message type                   #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                                                              #
#   tlv_type                      : (Optional)  Tlv Type                       #
#                                                                              #
#   tlv_length                    : (Optional)  Tlv Length                     #
#                                                                              #
#   ope                           : (Optional)  Ope                            #
#                                                                              #
#   cr                            : (Optional)  Cr                             #
#                                                                              #
#   rcr                           : (Optional)  Rcr                            #
#                                                                              #
#   tcr                           : (Optional)  Tcr                            #
#                                                                              #
#   ic                            : (Optional)  Ic                             #
#                                                                              #
#   irc                           : (Optional)  Irc                            #
#                                                                              #
#   itc                           : (Optional)  Itc                            #
#                                                                              #
#   fov                           : (Optional)  Fov                            #
#                                                                              #
#   pov                           : (Optional)  Pov                            #
#                                                                              #
#   tct                           : (Optional)  Tct                            #
#                                                                              #
#   phase_offset_tx               : (Optional)  Phase Offset Tx                #
#                                                                              #
#   freq_offset_tx                : (Optional)  Freq Offset Tx                 #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default: 1)                   #
#                                                                              #
#   timeout                       : (Optional)  Frame timeout (in seconds)     #
#                                               (Default: 5)                   #
#                                                                              #
#  OUTPUT PARAMETERS      :                                                    #
#                                                                              #
#   recvd_dest_mac                : (Optional)  Destination MAC dddress.       #
#                                                                              #
#   recvd_src_mac                 : (Optional)  Source MAC address.            #
#                                                                              #
#   recvd_two_step_flag           : (Optional)  Two Step Flag                  #
#                                                                              #
#   recvd_unicast_flag            : (Optional)  Unicast Flag                   #
#                                                                              #
#   recvd_correction_field        : (Optional)  Correction Field value         #
#                                                                              #
#   recvd_src_port_number         : (Optional)  Source port number             #
#                                                                              #
#   recvd_src_clock_identity      : (Optional)  Source clock identity          #
#                                                                              #
#   recvd_sequence_id             : (Optional)  Sequence ID                    #
#                                                                              #
#   recvd_log_message_interval    : (Optional)  Log message interval           #
#                                                                              #
#   recvd_target_clock_identity   : (Optional)  Recvd Target Clock Identity    #
#                                                                              #
#   recvd_target_port_number      : (Optional)  Recvd Target Port Number       #
#                                                                              #
#   recvd_phase_offset_tx_timestamp_sec: (Optional) Recvd Phase Offset Tx Time #
#                                                                              #
#   recvd_phase_offset_tx_timestamp_ns: (Optional) Recvd Phase Offset Tx Time  #
#                                                                              #
#   recvd_freq_offset_tx_timestamp_sec: (Optional) Recvd Freq Offset Tx Time   #
#                                                                              #
#   recvd_freq_offset_tx_timestamp_ns: (Optional) Recvd Freq Offset Tx Time ns #
#                                                                              #
#   recvd_ic                      : (Optional)  Recvd Ic flag                  #
#                                                                              #
#   recvd_irc                     : (Optional)  Recvd Irc flag                 #
#                                                                              #
#   recvd_itc                     : (Optional)  Recvd Itc flag                 #
#                                                                              #
#   rx_timestamp_sec              : (Optional)  Packet's rx time in seconds    #
#                                                                              #
#   rx_timestamp_ns               : (Optional)  Packet's rx time in ns         #
#                                                                              #
#   error_reason                  : (Optional)  Error Reason                   #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is received as         #
#                                         expected)                            #
#                                                                              #
#                         : 0 on failure (If the packet is not received        #
#                                         as expected)                         #
#                                                                              #
#  DEFINITION             : This function is used to receive                   #
#                           PTP signal message                                 #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#   ptp_ha::recv_signal\                                                       #
#    -port_num                         $port_num\                              #
#    -filter_id                        $filter_id\                             #
#    -reset_count                      $reset_count\                           #
#    -eth_type                         $eth_type\                              #
#    -vlan_id                          $vlan_id\                               #
#    -dest_ip                          $dest_ip\                               #
#    -src_ip                           $src_ip\                                #
#    -ip_checksum                      $ip_checksum\                           #
#    -dest_port                        $dest_port\                             #
#    -src_port                         $src_port\                              #
#    -udp_length                       $udp_length\                            #
#    -udp_checksum                     $udp_checksum\                          #
#    -major_sdo_id                     $major_sdo_id\                          #
#    -message_type                     $message_type\                          #
#    -minor_version                    $minor_version\                         #
#    -version_ptp                      $version_ptp\                           #
#    -message_length                   $message_length\                        #
#    -domain_number                    $domain_number\                         #
#    -minor_sdo_id                     $minor_sdo_id\                          #
#    -alternate_master_flag            $alternate_master_flag\                 #
#    -profile_specific1                $profile_specific1\                     #
#    -profile_specific2                $profile_specific2\                     #
#    -secure                           $secure\                                #
#    -leap61                           $leap61\                                #
#    -leap59                           $leap59\                                #
#    -current_utc_offset_valid         $current_utc_offset_valid\              #
#    -ptp_timescale                    $ptp_timescale\                         #
#    -time_traceable                   $time_traceable\                        #
#    -freq_traceable                   $freq_traceable\                        #
#    -sync_uncertain                   $sync_uncertain\                        #
#    -message_type_specific            $message_type_specific\                 #
#    -control_field                    $control_field\                         #
#    -tlv_type                         $tlv_type\                              #
#    -tlv_length                       $tlv_length\                            #
#    -ope                              $ope\                                   #
#    -cr                               $cr\                                    #
#    -rcr                              $rcr\                                   #
#    -tcr                              $tcr\                                   #
#    -ic                               $ic\                                    #
#    -irc                              $irc\                                   #
#    -itc                              $itc\                                   #
#    -fov                              $fov\                                   #
#    -pov                              $pov\                                   #
#    -tct                              $tct\                                   #
#    -phase_offset_tx                  $phase_offset_tx\                       #
#    -freq_offset_tx                   $freq_offset_tx\                        #
#    -count                            $count\                                 #
#    -timeout                          $timeout\                               #
#    -recvd_dest_mac                   recvd_dest_mac\                         #
#    -recvd_src_mac                    recvd_src_mac\                          #
#    -recvd_two_step_flag              recvd_two_step_flag\                    #
#    -recvd_unicast_flag               recvd_unicast_flag\                     #
#    -recvd_correction_field           recvd_correction_field\                 #
#    -recvd_src_port_number            recvd_src_port_identity\                #
#    -recvd_src_clock_identity         recvd_src_clock_identity\               #
#    -recvd_sequence_id                recvd_sequence_id\                      #
#    -recvd_log_message_interval       recvd_message_interval\                 #
#    -recvd_target_clock_identity      recvd_target_clock_identity\            #
#    -recvd_target_port_number         recvd_target_port_number\               #
#    -recvd_phase_offset_tx_timestamp_sec recvd_phase_offset_tx_timestamp_sec\ #
#    -recvd_phase_offset_tx_timestamp_ns  recvd_phase_offset_tx_timestamp_ns\  #
#    -recvd_freq_offset_tx_timestamp_sec  recvd_freq_offset_tx_timestamp_sec\  #
#    -recvd_freq_offset_tx_timestamp_ns   recvd_freq_offset_tx_timestamp_ns\   #
#    -recvd_ic                         recvd_ic\                               #
#    -recvd_irc                        recvd_irc\                              #
#    -recvd_itc                        recvd_itc\                              #
#    -rx_timestamp_sec                 rx_timestamp_sec\                       #
#    -rx_timestamp_ns                  rx_timestamp_ns\                        #
#    -error_reason                     error_reason                            #
#                                                                              #
################################################################################
proc ptp_ha::recv_signal {args} {

    array unset ::validation_param

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "ptp_ha::recv_signal: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #Reception port
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "ptp_ha::recv_signal: Missing Argument -> port_num"
        return 0
    }

    #Filter id
    if {[info exists param(-filter_id)]} {

        set filter_id $param(-filter_id)
    } else {

        ERRLOG -msg "ptp_ha::recv_signal: Missing Argument -> filter_id"
        return 0
    }

    #Reset Count
    if {[info exists param(-reset_count)]} {

        set reset_count $param(-reset_count)
    } else {

        set reset_count ON
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
        set ::validation_param(src_mac) $param(-src_mac)
    } else {

        set ::validation_param(src_mac) "dont_care"
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        set ::validation_param(eth_type) $param(-eth_type)
    } else {

        set ::validation_param(eth_type) "dont_care"
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
        set ::validation_param(vlan_id) $param(-vlan_id)
    } else {

        if { $::ptp_ha_vlan_encap == "true" } {
            set ::validation_param(vlan_id) $::ptp_ha_vlan_id
        } else {
            set ::validation_param(vlan_id) "dont_care"
        }
    }

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
        set ::validation_param(dest_ip) $param(-dest_ip)
    } else {

        set ::validation_param(dest_ip) "dont_care"
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
        set ::validation_param(src_ip) $param(-src_ip)
    } else {

        set ::validation_param(src_ip) "dont_care"
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
        set ::validation_param(ip_checksum) $param(-ip_checksum)
    } else {

        set ::validation_param(ip_checksum) "dont_care"
    }

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
        set ::validation_param(dest_port) $param(-dest_port)
    } else {

        set ::validation_param(dest_port) "dont_care"
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
        set ::validation_param(src_port) $param(-src_port)
    } else {

        set ::validation_param(src_port) "dont_care"
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
        set ::validation_param(udp_length) $param(-udp_length)
    } else {

        set ::validation_param(udp_length) "dont_care"
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
        set ::validation_param(udp_checksum) $param(-udp_checksum)
    } else {

        set ::validation_param(udp_checksum) "dont_care"
    }

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
        set ::validation_param(major_sdo_id) $param(-major_sdo_id)
    } else {

        set ::validation_param(major_sdo_id) "dont_care"
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        set ::validation_param(message_type) $param(-message_type)
    } else {

        set ::validation_param(message_type) 12
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
        set ::validation_param(minor_version) $param(-minor_version)
    } else {

        set ::validation_param(minor_version) "dont_care"
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        set ::validation_param(version_ptp) $param(-version_ptp)
    } else {

        set ::validation_param(version_ptp) "dont_care"
    }

    #PTP Annouce message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        set ::validation_param(message_length) $param(-message_length)
    } else {

        set ::validation_param(message_length) "dont_care"
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        set ::validation_param(domain_number) $param(-domain_number)
    } else {

        set ::validation_param(domain_number) "dont_care"
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
        set ::validation_param(minor_sdo_id) $param(-minor_sdo_id)
    } else {

        set ::validation_param(minor_sdo_id) "dont_care"
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        set ::validation_param(alternate_master_flag) $param(-alternate_master_flag)
    } else {

        set ::validation_param(alternate_master_flag) "dont_care"
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        set ::validation_param(profile_specific1) $param(-profile_specific1)
    } else {

        set ::validation_param(profile_specific1) "dont_care"
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        set ::validation_param(profile_specific2) $param(-profile_specific2)
    } else {

        set ::validation_param(profile_specific2) "dont_care"
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
        set ::validation_param(secure) $param(-secure)
    } else {

        set ::validation_param(secure) "dont_care"
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        set ::validation_param(leap61) $param(-leap61)
    } else {

        set ::validation_param(leap61) "dont_care"
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        set ::validation_param(leap59) $param(-leap59)
    } else {

        set ::validation_param(leap59) "dont_care"
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        set ::validation_param(current_utc_offset_valid) $param(-current_utc_offset_valid)
    } else {

        set ::validation_param(current_utc_offset_valid) "dont_care"
    }

    #PTP Timescale Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        set ::validation_param(ptp_timescale) $param(-ptp_timescale)
    } else {

        set ::validation_param(ptp_timescale) "dont_care"
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        set ::validation_param(time_traceable) $param(-time_traceable)
    } else {

        set ::validation_param(time_traceable) "dont_care"
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        set ::validation_param(freq_traceable) $param(-freq_traceable)
    } else {

        set ::validation_param(freq_traceable) "dont_care"
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
        set ::validation_param(sync_uncertain) $param(-sync_uncertain)
    } else {

        set ::validation_param(sync_uncertain) "dont_care"
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
        set ::validation_param(message_type_specific) $param(-message_type_specific)
    } else {

        set ::validation_param(message_type_specific) "dont_care"
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        set ::validation_param(sequence_id) $param(-sequence_id)
    } else {

        set ::validation_param(sequence_id) "dont_care"
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        set ::validation_param(control_field) $param(-control_field)
    } else {

        set ::validation_param(control_field) "dont_care"
    }

    #Tlv Type
    if {[info exists param(-tlv_type)]} {

        set tlv_type $param(-tlv_type)
        set ::validation_param(tlv_type) $param(-tlv_type)
    } else {

        set ::validation_param(tlv_type) "dont_care"
    }

    #Tlv Length
    if {[info exists param(-tlv_length)]} {

        set tlv_length $param(-tlv_length)
        set ::validation_param(tlv_length) $param(-tlv_length)
    } else {

        set ::validation_param(tlv_length) "dont_care"
    }

    #Ope
    if {[info exists param(-ope)]} {

        set ope $param(-ope)
        set ::validation_param(ope) $param(-ope)
    } else {

        set ::validation_param(ope) "dont_care"
    }

    #Cr
    if {[info exists param(-cr)]} {

        set cr $param(-cr)
        set ::validation_param(cr) $param(-cr)
    } else {

        set ::validation_param(cr) "dont_care"
    }

    #Rcr
    if {[info exists param(-rcr)]} {

        set rcr $param(-rcr)
        set ::validation_param(rcr) $param(-rcr)
    } else {

        set ::validation_param(rcr) "dont_care"
    }

    #Tcr
    if {[info exists param(-tcr)]} {

        set tcr $param(-tcr)
        set ::validation_param(tcr) $param(-tcr)
    } else {

        set ::validation_param(tcr) "dont_care"
    }

    #Ic
    if {[info exists param(-ic)]} {

        set ic $param(-ic)
        set ::validation_param(ic) $param(-ic)
    } else {

        set ::validation_param(ic) "dont_care"
    }

    #Irc
    if {[info exists param(-irc)]} {

        set irc $param(-irc)
        set ::validation_param(irc) $param(-irc)
    } else {

        set ::validation_param(irc) "dont_care"
    }

    #Itc
    if {[info exists param(-itc)]} {

        set itc $param(-itc)
        set ::validation_param(itc) $param(-itc)
    } else {

        set ::validation_param(itc) "dont_care"
    }

    #Fov
    if {[info exists param(-fov)]} {

        set fov $param(-fov)
        set ::validation_param(fov) $param(-fov)
    } else {

        set ::validation_param(fov) "dont_care"
    }

    #Pov
    if {[info exists param(-pov)]} {

        set pov $param(-pov)
        set ::validation_param(pov) $param(-pov)
    } else {

        set ::validation_param(pov) "dont_care"
    }

    #Tct
    if {[info exists param(-tct)]} {

        set tct $param(-tct)
        set ::validation_param(tct) $param(-tct)
    } else {

        set ::validation_param(tct) "dont_care"
    }

    #Phase Offset Tx
    if {[info exists param(-phase_offset_tx)]} {

        set phase_offset_tx $param(-phase_offset_tx)
        set ::validation_param(phase_offset_tx) $param(-phase_offset_tx)
    } else {

        set ::validation_param(phase_offset_tx) "dont_care"
    }

    #Freq Offset Tx
    if {[info exists param(-freq_offset_tx)]} {

        set freq_offset_tx $param(-freq_offset_tx)
        set ::validation_param(freq_offset_tx) $param(-freq_offset_tx)
    } else {

        set ::validation_param(freq_offset_tx) "dont_care"
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame timeout
    if {[info exists param(-timeout)]} {

        set timeout $param(-timeout)
    } else {

        set timeout $::ptp_ha_1_l1sync_timeout
    }

    #OUTPUT PARAMETERS
    #all the fields in the matched packet
    foreach key [array names param] {

        set var [string trimleft $key -]

        if [regexp {^recvd_.*} $var] {

            upvar [set param($key)] $var
            set $var ""
        }
    }

    #Received packet timestamp in seconds
    if {[info exists param(-rx_timestamp_sec)]} {

        upvar $param(-rx_timestamp_sec) rx_timestamp_sec
        set rx_timestamp_sec ""
    }

    #Received packet timestamp in nanoseconds
    if {[info exists param(-rx_timestamp_ns)]} {

        upvar $param(-rx_timestamp_ns) rx_timestamp_ns
        set rx_timestamp_ns ""
    }

    #Error Reason
    if {[info exists param(-error_reason)]} {

        upvar $param(-error_reason) error_reason
        set error_reason ""
    }

    #LOGIC GOES HERE
    #Timeout in millisec
    set timeout [expr int($timeout*1000)]
    set error_code 0

    printTimeout $timeout

    set result [pltLib::recv_periodic\
            -session_id           $session_id\
            -port_num             $port_num\
            -filter_id            $filter_id\
            -timeout              $timeout\
            -reset_count          $reset_count\
            -validation_handler   "PTP_HA"\
            -error_code           error_code\
            -rx_timestamp         rx_timestamp_sec\
            -rx_time_nanoseconds  rx_timestamp_ns\
            -pkt_content          pkt_content\
            -pkt_size             pkt_size]

    set error_reason [ptp_ha::tee_recv_error_reason $error_code]

    if {[info exists pkt_content]} {

        set hex $pkt_content

        pbLib::decode_ptp_ha_pkt\
                 -packet                         $hex\
                 -dest_mac                       recvd_dest_mac\
                 -src_mac                        recvd_src_mac\
                 -eth_type                       recvd_eth_type\
                 -vlan_id                        recvd_vlan_id\
                 -vlan_priority                  recvd_vlan_priority\
                 -vlan_dei                       recvd_vlan_dei\
                 -src_ip                         recvd_src_ip\
                 -dest_ip                        recvd_dest_ip\
                 -src_port                       recvd_src_port\
                 -dest_port                      recvd_dest_port\
                 -udp_length                     recvd_udp_length\
                 -udp_checksum                   recvd_udp_checksum\
                 -major_sdo_id                   recvd_major_sdo_id\
                 -message_type                   recvd_message_type\
                 -minor_version                  recvd_minor_version\
                 -version_ptp                    recvd_version_ptp\
                 -message_length                 recvd_message_length\
                 -domain_number                  recvd_domain_number\
                 -minor_sdo_id                   recvd_minor_sdo_id\
                 -alternate_master_flag          recvd_alternate_master_flag\
                 -two_step_flag                  recvd_two_step_flag\
                 -unicast_flag                   recvd_unicast_flag\
                 -profile_specific1              recvd_profile_specific1\
                 -profile_specific2              recvd_profile_specific2\
                 -secure                         recvd_secure\
                 -leap61                         recvd_leap61\
                 -leap59                         recvd_leap59\
                 -current_utc_offset_valid       recvd_current_utc_offset_valid\
                 -ptp_timescale                  recvd_ptp_timescale\
                 -time_traceable                 recvd_time_traceable\
                 -freq_traceable                 recvd_freq_traceable\
                 -sync_uncertain                 recvd_sync_uncertain\
                 -correction_field               recvd_correction_field\
                 -message_type_specific          recvd_message_type_specific\
                 -src_port_number                recvd_src_port_number\
                 -src_clock_identity             recvd_src_clock_identity\
                 -sequence_id                    recvd_sequence_id\
                 -control_field                  recvd_control_field\
                 -log_message_interval           recvd_log_message_interval\
                 -target_port_number             recvd_target_port_number\
                 -target_clock_identity          recvd_target_clock_identity\
                 -tlv                            recvd_tlv\
                 -tlv_type                       recvd_tlv_type\
                 -tlv_length                     recvd_tlv_length\
                 -ope                            recvd_ope\
                 -cr                             recvd_cr\
                 -rcr                            recvd_rcr\
                 -tcr                            recvd_tcr\
                 -ic                             recvd_ic\
                 -irc                            recvd_irc\
                 -itc                            recvd_itc\
                 -fov                            recvd_fov\
                 -pov                            recvd_pov\
                 -tct                            recvd_tct\
                 -phase_offset_tx                recvd_phase_offset_tx\
                 -phase_offset_tx_timestamp_sec  recvd_phase_offset_tx_timestamp_sec\
                 -phase_offset_tx_timestamp_ns   recvd_phase_offset_tx_timestamp_ns\
                 -freq_offset_tx                 recvd_freq_offset_tx\
                 -freq_offset_tx_timestamp_sec   recvd_freq_offset_tx_timestamp_sec\
                 -freq_offset_tx_timestamp_ns    recvd_freq_offset_tx_timestamp_ns

    }

    return $result

}


################################################################################
#                                                                              #
#  PROCEDURE NAME         :  ptp_ha::recv_mgmt                                 #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Reception port                 #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   filter_id                     : (Mandatory) Filter id                      #
#                                                                              #
#   reset_count                   : (Optional)  Reset Count                    #
#                                               (Default: ON)                  #
#                                                                              #
#   eth_type                      : (Optional)  EtherType (0800 or 88F7)       #
#                                                                              #
#   vlan_id                       : (Optional)  Vlan ID                        #
#                                                                              #
#   dest_ip                       : (Optional)  Destination IP address         #
#                                                                              #
#   src_ip                        : (Optional)  Source IP address              #
#                                                                              #
#   ip_checksum                   : (Optional)  IP checksum                    #
#                                                                              #
#   dest_port                     : (Optional)  UDP destination port number    #
#                                                                              #
#   src_port                      : (Optional)  UDP source port number         #
#                                                                              #
#   udp_length                    : (Optional)  UDP length                     #
#                                                                              #
#   udp_checksum                  : (Optional)  UDP checksum                   #
#                                                                              #
#   major_sdo_id                  : (Optional)  Major sdo id                   #
#                                                                              #
#   message_type                  : (Optional)  Message Type                   #
#                                               (Default: 13)                  #
#                                                                              #
#   minor_version                 : (Optional)  PTP version number             #
#                                                                              #
#   version_ptp                   : (Optional)  PTP minor version number       #
#                                                                              #
#   message_length                : (Optional)  PTP Annouce message length     #
#                                                                              #
#   domain_number                 : (Optional)  Domain number                  #
#                                                                              #
#   minor_sdo_id                  : (Optional)  Minor Sdo id                   #
#                                                                              #
#   alternate_master_flag         : (Optional)  Alternate Master flag          #
#                                                                              #
#   profile_specific1             : (Optional)  Profile specific1 Flag         #
#                                                                              #
#   profile_specific2             : (Optional)  Profile specific2 Flag         #
#                                                                              #
#   secure                        : (Optional)  Secure Flag                    #
#                                                                              #
#   leap61                        : (Optional)  Leap61 Flag                    #
#                                                                              #
#   leap59                        : (Optional)  Leap59 Flag                    #
#                                                                              #
#   current_utc_offset_valid      : (Optional)  Current UTC Offset Flag        #
#                                                                              #
#   ptp_timescale                 : (Optional)  PTP Timescale Flag             #
#                                                                              #
#   time_traceable                : (Optional)  Time Traceable Flag            #
#                                                                              #
#   freq_traceable                : (Optional)  Frequency Traceable Flag       #
#                                                                              #
#   sync_uncertain                : (Optional)  Synchronization uncertain Flag #
#                                                                              #
#   message_type_specific         : (Optional)  Message type                   #
#                                                                              #
#   sequence_id                   : (Optional)  Sequence ID                    #
#                                                                              #
#   control_field                 : (Optional)  Control field                  #
#                                                                              #
#   starting_boundary_hops        : (Optional)  Starting Boundary Hops         #
#                                                                              #
#   boundary_hops                 : (Optional)  Boundary Hops                  #
#                                                                              #
#   action_field                  : (Optional)  Action Field                   #
#                                                                              #
#   tlv_type                      : (Optional)  Tlv Type                       #
#                                                                              #
#   tlv_length                    : (Optional)  Tlv Length                     #
#                                                                              #
#   management_id                 : (Optional)  Management Id                  #
#                                                                              #
#   ext_port_config               : (Optional)  Ext Port Config                #
#                                                                              #
#   master_only                   : (Optional)  Master Only                    #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default: 1)                   #
#                                                                              #
#   timeout                       : (Optional)  Frame timeout (in seconds)     #
#                                               (Default: 5)                   #
#                                                                              #
#  OUTPUT PARAMETERS      :                                                    #
#                                                                              #
#   recvd_dest_mac                : (Optional)  Destination MAC dddress.       #
#                                                                              #
#   recvd_src_mac                 : (Optional)  Source MAC address.            #
#                                                                              #
#   recvd_two_step_flag           : (Optional)  Two Step Flag                  #
#                                                                              #
#   recvd_unicast_flag            : (Optional)  Unicast Flag                   #
#                                                                              #
#   recvd_correction_field        : (Optional)  Correction Field value         #
#                                                                              #
#   recvd_src_port_number         : (Optional)  Source port number             #
#                                                                              #
#   recvd_src_clock_identity      : (Optional)  Source clock identity          #
#                                                                              #
#   recvd_sequence_id             : (Optional)  Sequence ID                    #
#                                                                              #
#   recvd_log_message_interval    : (Optional)  Log message interval           #
#                                                                              #
#   recvd_target_clock_identity   : (Optional)  Recvd Target Clock Identity    #
#                                                                              #
#   recvd_target_port_number      : (Optional)  Recvd Target Port Number       #
#                                                                              #
#   rx_timestamp_sec              : (Optional)  Packet's rx time in seconds    #
#                                                                              #
#   rx_timestamp_ns               : (Optional)  Packet's rx time in ns         #
#                                                                              #
#   error_reason                  : (Optional)  Error Reason                   #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is received as         #
#                                         expected)                            #
#                                                                              #
#                         : 0 on failure (If the packet is not received        #
#                                         as expected)                         #
#                                                                              #
#  DEFINITION             : This function is used to receive                   #
#                           PTP mgmt message                                   #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#       ptp_ha::recv_mgmt\                                                     #
#           -port_num                         $port_num\                       #
#           -filter_id                        $filter_id\                      #
#           -reset_count                      $reset_count\                    #
#           -eth_type                         $eth_type\                       #
#           -vlan_id                          $vlan_id\                        #
#           -dest_ip                          $dest_ip\                        #
#           -src_ip                           $src_ip\                         #
#           -ip_checksum                      $ip_checksum\                    #
#           -dest_port                        $dest_port\                      #
#           -src_port                         $src_port\                       #
#           -udp_length                       $udp_length\                     #
#           -udp_checksum                     $udp_checksum\                   #
#           -major_sdo_id                     $major_sdo_id\                   #
#           -message_type                     $message_type\                   #
#           -minor_version                    $minor_version\                  #
#           -version_ptp                      $version_ptp\                    #
#           -message_length                   $message_length\                 #
#           -domain_number                    $domain_number\                  #
#           -minor_sdo_id                     $minor_sdo_id\                   #
#           -alternate_master_flag            $alternate_master_flag\          #
#           -profile_specific1                $profile_specific1\              #
#           -profile_specific2                $profile_specific2\              #
#           -secure                           $secure\                         #
#           -leap61                           $leap61\                         #
#           -leap59                           $leap59\                         #
#           -current_utc_offset_valid         $current_utc_offset_valid\       #
#           -ptp_timescale                    $ptp_timescale\                  #
#           -time_traceable                   $time_traceable\                 #
#           -freq_traceable                   $freq_traceable\                 #
#           -sync_uncertain                   $sync_uncertain\                 #
#           -message_type_specific            $message_type_specific\          #
#           -control_field                    $control_field\                  #
#           -starting_boundary_hops           $starting_boundary_hops\         #
#           -boundary_hops                    $boundary_hops\                  #
#           -action_field                     $action_field\                   #
#           -tlv_type                         $tlv_type\                       #
#           -tlv_length                       $tlv_length\                     #
#           -management_id                    $management_id\                  #
#           -ext_port_config                  $ext_port_config\                #
#           -master_only                      $master_only\                    #
#           -count                            $count\                          #
#           -interval                         $interval                        #
#           -recvd_dest_mac                   recvd_dest_mac\                  #
#           -recvd_src_mac                    recvd_src_mac\                   #
#           -recvd_two_step_flag              recvd_two_step_flag\             #
#           -recvd_unicast_flag               recvd_unicast_flag\              #
#           -recvd_correction_field           recvd_correction_field\          #
#           -recvd_src_port_number            recvd_src_port_identity\         #
#           -recvd_src_clock_identity         recvd_src_clock_identity\        #
#           -recvd_sequence_id                recvd_sequence_id\               #
#           -recvd_log_message_interval       recvd_message_interval\          #
#           -recvd_target_clock_identity      recvd_target_clock_identity\     #
#           -recvd_target_port_number         recvd_target_port_number\        #
#           -rx_timestamp_sec                 rx_timestamp_sec\                #
#           -rx_timestamp_ns                  rx_timestamp_ns\                 #
#           -error_reason                     error_reason                     #
#                                                                              #
################################################################################
proc ptp_ha::recv_mgmt {args} {

    array unset ::validation_param

    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "ptp_ha::recv_mgmt: Tee session is not initialized."
    }

    #INPUT PARAMETERS

    #Reception port
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "ptp_ha::recv_mgmt: Missing Argument -> port_num"
        return 0
    }

    #Filter id
    if {[info exists param(-filter_id)]} {

        set filter_id $param(-filter_id)
    } else {

        ERRLOG -msg "ptp_ha::recv_mgmt: Missing Argument -> filter_id"
        return 0
    }

    #Reset Count
    if {[info exists param(-reset_count)]} {

        set reset_count $param(-reset_count)
    } else {

        set reset_count ON
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
        set ::validation_param(eth_type) $param(-eth_type)
    } else {

        set ::validation_param(eth_type) "dont_care"
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
        set ::validation_param(vlan_id) $param(-vlan_id)
    } else {

        if { $::ptp_ha_vlan_encap == "true" } {
            set ::validation_param(vlan_id) $::ptp_ha_vlan_id
        } else {
            set ::validation_param(vlan_id) "dont_care"
        }
    }

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
        set ::validation_param(dest_ip) $param(-dest_ip)
    } else {

        set ::validation_param(dest_ip) "dont_care"
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
        set ::validation_param(src_ip) $param(-src_ip)
    } else {

        set ::validation_param(src_ip) "dont_care"
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
        set ::validation_param(ip_checksum) $param(-ip_checksum)
    } else {

        set ::validation_param(ip_checksum) "dont_care"
    }

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
        set ::validation_param(dest_port) $param(-dest_port)
    } else {

        set ::validation_param(dest_port) "dont_care"
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
        set ::validation_param(src_port) $param(-src_port)
    } else {

        set ::validation_param(src_port) "dont_care"
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
        set ::validation_param(udp_length) $param(-udp_length)
    } else {

        set ::validation_param(udp_length) "dont_care"
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
        set ::validation_param(udp_checksum) $param(-udp_checksum)
    } else {

        set ::validation_param(udp_checksum) "dont_care"
    }

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
        set ::validation_param(major_sdo_id) $param(-major_sdo_id)
    } else {

        set ::validation_param(major_sdo_id) "dont_care"
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
        set ::validation_param(message_type) $param(-message_type)
    } else {

        set ::validation_param(message_type) 13
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
        set ::validation_param(minor_version) $param(-minor_version)
    } else {

        set ::validation_param(minor_version) "dont_care"
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
        set ::validation_param(version_ptp) $param(-version_ptp)
    } else {

        set ::validation_param(version_ptp) "dont_care"
    }

    #PTP Annouce message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
        set ::validation_param(message_length) $param(-message_length)
    } else {

        set ::validation_param(message_length) "dont_care"
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
        set ::validation_param(domain_number) $param(-domain_number)
    } else {

        set ::validation_param(domain_number) "dont_care"
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
        set ::validation_param(minor_sdo_id) $param(-minor_sdo_id)
    } else {

        set ::validation_param(minor_sdo_id) "dont_care"
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
        set ::validation_param(alternate_master_flag) $param(-alternate_master_flag)
    } else {

        set ::validation_param(alternate_master_flag) "dont_care"
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
        set ::validation_param(profile_specific1) $param(-profile_specific1)
    } else {

        set ::validation_param(profile_specific1) "dont_care"
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
        set ::validation_param(profile_specific2) $param(-profile_specific2)
    } else {

        set ::validation_param(profile_specific2) "dont_care"
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
        set ::validation_param(secure) $param(-secure)
    } else {

        set ::validation_param(secure) "dont_care"
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
        set ::validation_param(leap61) $param(-leap61)
    } else {

        set ::validation_param(leap61) "dont_care"
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
        set ::validation_param(leap59) $param(-leap59)
    } else {

        set ::validation_param(leap59) "dont_care"
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
        set ::validation_param(current_utc_offset_valid) $param(-current_utc_offset_valid)
    } else {

        set ::validation_param(current_utc_offset_valid) "dont_care"
    }

    #PTP Timescale Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
        set ::validation_param(ptp_timescale) $param(-ptp_timescale)
    } else {

        set ::validation_param(ptp_timescale) "dont_care"
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
        set ::validation_param(time_traceable) $param(-time_traceable)
    } else {

        set ::validation_param(time_traceable) "dont_care"
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
        set ::validation_param(freq_traceable) $param(-freq_traceable)
    } else {

        set ::validation_param(freq_traceable) "dont_care"
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
        set ::validation_param(sync_uncertain) $param(-sync_uncertain)
    } else {

        set ::validation_param(sync_uncertain) "dont_care"
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
        set ::validation_param(message_type_specific) $param(-message_type_specific)
    } else {

        set ::validation_param(message_type_specific) "dont_care"
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
        set ::validation_param(sequence_id) $param(-sequence_id)
    } else {

        set ::validation_param(sequence_id) "dont_care"
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
        set ::validation_param(control_field) $param(-control_field)
    } else {

        set ::validation_param(control_field) "dont_care"
    }

    #Starting Boundary Hops
    if {[info exists param(-starting_boundary_hops)]} {

        set starting_boundary_hops $param(-starting_boundary_hops)
        set ::validation_param(starting_boundary_hops) $param(-starting_boundary_hops)
    } else {

        set ::validation_param(starting_boundary_hops) "dont_care"
    }

    #Boundary Hops
    if {[info exists param(-boundary_hops)]} {

        set boundary_hops $param(-boundary_hops)
        set ::validation_param(boundary_hops) $param(-boundary_hops)
    } else {

        set ::validation_param(boundary_hops) "dont_care"
    }

    #Action Field
    if {[info exists param(-action_field)]} {

        set action_field $param(-action_field)
        set ::validation_param(action_field) $param(-action_field)
    } else {

        set ::validation_param(action_field) "dont_care"
    }

    #Tlv Type
    if {[info exists param(-tlv_type)]} {

        set tlv_type $param(-tlv_type)
        set ::validation_param(tlv_type) $param(-tlv_type)
    } else {

        set ::validation_param(tlv_type) "dont_care"
    }

    #Tlv Length
    if {[info exists param(-tlv_length)]} {

        set tlv_length $param(-tlv_length)
        set ::validation_param(tlv_length) $param(-tlv_length)
    } else {

        set ::validation_param(tlv_length) "dont_care"
    }

    #Management Id
    if {[info exists param(-management_id)]} {

        set management_id $param(-management_id)
        set ::validation_param(management_id) $param(-management_id)
    } else {

        set ::validation_param(management_id) "dont_care"
    }

    #Ext Port Config
    if {[info exists param(-ext_port_config)]} {

        set ext_port_config $param(-ext_port_config)
        set ::validation_param(ext_port_config) $param(-ext_port_config)
    } else {

        set ::validation_param(ext_port_config) "dont_care"
    }

    #Master Only
    if {[info exists param(-master_only)]} {

        set master_only $param(-master_only)
        set ::validation_param(master_only) $param(-master_only)
    } else {

        set ::validation_param(master_only) "dont_care"
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame timeout
    if {[info exists param(-timeout)]} {

        set timeout $param(-timeout)
    } else {

        set timeout $::ptp_ha_mgmt_timeout
    }

    #OUTPUT PARAMETERS
    #all the fields in the matched packet
    foreach key [array names param] {

        set var [string trimleft $key -]

        if [regexp {^recvd_.*} $var] {

            upvar [set param($key)] $var
            set $var ""
        }
    }

    #Received packet timestamp in seconds
    if {[info exists param(-rx_timestamp_sec)]} {

        upvar $param(-rx_timestamp_sec) rx_timestamp_sec
        set rx_timestamp_sec ""
    }

    #Received packet timestamp in nanoseconds
    if {[info exists param(-rx_timestamp_ns)]} {

        upvar $param(-rx_timestamp_ns) rx_timestamp_ns
        set rx_timestamp_ns ""
    }

    #Error Reason
    if {[info exists param(-error_reason)]} {

        upvar $param(-error_reason) error_reason
        set error_reason ""
    }

    #LOGIC GOES HERE
    #Timeout in millisec
    set timeout [expr int($timeout*1000)]
    set error_code 0

    printTimeout $timeout

    set result [pltLib::recv_periodic\
            -session_id           $session_id\
            -port_num             $port_num\
            -filter_id            $filter_id\
            -timeout              $timeout\
            -reset_count          $reset_count\
            -validation_handler   "PTP_HA"\
            -error_code           error_code\
            -rx_timestamp         rx_timestamp_sec\
            -rx_time_nanoseconds  rx_timestamp_ns\
            -pkt_content          pkt_content\
            -pkt_size             pkt_size]

    set error_reason [ptp_ha::tee_recv_error_reason $error_code]

    if {[info exists pkt_content]} {

        set hex $pkt_content

        pbLib::decode_ptp_ha_pkt\
                 -packet                         $hex\
                 -dest_mac                       recvd_dest_mac\
                 -src_mac                        recvd_src_mac\
                 -eth_type                       recvd_eth_type\
                 -vlan_id                        recvd_vlan_id\
                 -vlan_priority                  recvd_vlan_priority\
                 -vlan_dei                       recvd_vlan_dei\
                 -src_ip                         recvd_src_ip\
                 -dest_ip                        recvd_dest_ip\
                 -src_port                       recvd_src_port\
                 -dest_port                      recvd_dest_port\
                 -udp_length                     recvd_udp_length\
                 -udp_checksum                   recvd_udp_checksum\
                 -major_sdo_id                   recvd_major_sdo_id\
                 -message_type                   recvd_message_type\
                 -minor_version                  recvd_minor_version\
                 -version_ptp                    recvd_version_ptp\
                 -message_length                 recvd_message_length\
                 -domain_number                  recvd_domain_number\
                 -minor_sdo_id                   recvd_minor_sdo_id\
                 -alternate_master_flag          recvd_alternate_master_flag\
                 -two_step_flag                  recvd_two_step_flag\
                 -unicast_flag                   recvd_unicast_flag\
                 -profile_specific1              recvd_profile_specific1\
                 -profile_specific2              recvd_profile_specific2\
                 -secure                         recvd_secure\
                 -leap61                         recvd_leap61\
                 -leap59                         recvd_leap59\
                 -current_utc_offset_valid       recvd_current_utc_offset_valid\
                 -ptp_timescale                  recvd_ptp_timescale\
                 -time_traceable                 recvd_time_traceable\
                 -freq_traceable                 recvd_freq_traceable\
                 -sync_uncertain                 recvd_sync_uncertain\
                 -correction_field               recvd_correction_field\
                 -message_type_specific          recvd_message_type_specific\
                 -src_port_number                recvd_src_port_number\
                 -src_clock_identity             recvd_src_clock_identity\
                 -sequence_id                    recvd_sequence_id\
                 -control_field                  recvd_control_field\
                 -log_message_interval           recvd_log_message_interval\
                 -target_port_number             recvd_target_port_number\
                 -target_clock_identity          recvd_target_clock_identity\
                 -starting_boundary_hops         recvd_starting_boundary_hops\
                 -boundary_hops                  recvd_boundary_hops\
                 -action_field                   recvd_action_field\
                 -tlv                            recvd_tlv\
                 -tlv_type                       recvd_tlv_type\
                 -tlv_length                     recvd_tlv_length\
                 -management_id                  recvd_management_id\
                 -ext_port_config                recvd_ext_port_config\
                 -master_only                    recvd_master_only

    }
    return $result

}

################################################################################
#                                                                              #
#  PROCEDURE NAME         :  ptp_ha::check_signal                              #
#                                                                              #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   port_num                      : (Mandatory) Reception port                 #
#                                               (e.g., 0/1)                    #
#                                                                              #
#   filter_id                     : (Mandatory) Filter id                      #
#                                                                              #
#   count                         : (Optional)  Frame count                    #
#                                               (Default: 1)                   #
#                                                                              #
#   timeout                       : (Optional)  Frame timeout (in seconds)     #
#                                               (Default: 5)                   #
#                                                                              #
#  OUTPUT PARAMETERS      :                                                    #
#                                                                              #
#   rx_timestamp_sec              : (Optional)  Packet's rx time in seconds    #
#                                                                              #
#   rx_timestamp_ns               : (Optional)  Packet's rx time in ns         #
#                                                                              #
#  RETURNS                : 1 on success (If the packet is received as         #
#                                         expected)                            #
#                                                                              #
#                         : 0 on failure (If the packet is not received        #
#                                         as expected)                         #
#                                                                              #
#  DEFINITION             : This function is used to check PTP HA signal       #
#                           message format                                     #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#   ptp_ha::check_signal\                                                      #
#    -port_num                         $port_num\                              #
#    -filter_id                        $filter_id\                             #
#    -ope                              $ope\                                   #
#    -reset_count                      $reset_count\                           #
#    -count                            $count\                                 #
#    -timeout                          $timeout\                               #
#    -rx_timestamp_sec                 rx_timestamp_sec\                       #
#    -rx_timestamp_ns                  rx_timestamp_ns                         #
#                                                                              #
################################################################################

proc ptp_ha::check_signal {args} {

    array unset ::validation_param
    
    array set param $args

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "ptp_ha::check_signal: Tee session is not initialized."
    }

    #Port number on which frame is to be received
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
        LOG -level 1 -msg "\t\tport_num                       -> $port_num"
    } else {

        ERRLOG -msg "ptp_ha::check_signal: Missing Argument -> port_num"
        return 0
    }

    #Filter id
    if {[info exists param(-filter_id)]} {

        set filter_id $param(-filter_id)
    } else {

        ERRLOG -msg "ptp_ha::check_signal: Missing Argument -> filter_id"
        return 0
    }

    #Reset Count
    if {[info exists param(-reset_count)]} {

        set reset_count $param(-reset_count)
    } else {

        set reset_count ON
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame timeout
    if {[info exists param(-timeout)]} {

        set timeout $param(-timeout)
    } else {

        set timeout $::ptp_ha_1_l1sync_timeout
    }

    #Received packet timestamp in seconds
    if {[info exists param(-rx_timestamp_sec)]} {

        upvar $param(-rx_timestamp_sec) rx_timestamp_sec
        set rx_timestamp_sec ""
    }

    #Received packet timestamp in nanoseconds
    if {[info exists param(-rx_timestamp_ns)]} {

        upvar $param(-rx_timestamp_ns) rx_timestamp_ns
        set rx_timestamp_ns ""
    }

    #Ope
    if {[info exists param(-ope)]} {

        set ope $param(-ope)
    } else {

        set ope "0-1"
    }

    #LOGIC GOES HERE
    #Timeout in millisec
    set timeout [expr int($timeout*1000)]

    if {($::ptp_ha_vlan_encap == "true")} {
        set vlan_id  $::ptp_ha_vlan_id
    } else {
        set vlan_id  "dont_care"
    }

    if {[string match -nocase $::ptp_comm_model "Unicast"]} {
        set dest_mac "Unicast"
        set dest_ip  "Unicast"
    } else {
        set dest_mac $::PTP_HA_MAC(SIGNALING)
        set dest_ip  $::PTP_HA_IP(SIGNALING)
    }

    if { $::ptp_trans_type == $::PTP_TRANS_TYPE(IPV4) } {
        set ip_checksum  "Valid"
        set ip_protocol  17
        set src_ip       "Unicast"
        set dest_port    320
        set udp_checksum "Valid"
    } else {
        set ip_checksum  "dont_care"
        set ip_protocol  "dont_care"
        set src_ip       "dont_care"
        set dest_ip      "dont_care"
        set dest_port    "dont_care"
        set udp_checksum "dont_care"
    }

    #Receive only PTP HA signalling messages
    set ::validation_param(minor_version) 1
    set ::validation_param(version_ptp)   2
    set ::validation_param(message_type) 12

    printTimeout $timeout

    set result [pltLib::recv_periodic\
            -session_id           $session_id\
            -port_num             $port_num\
            -filter_id            $filter_id\
            -timeout              $timeout\
            -reset_count          $reset_count\
            -validation_handler   "PTP_HA"\
            -rx_timestamp         rx_timestamp_sec\
            -rx_time_nanoseconds  rx_timestamp_ns\
            -pkt_content          pkt_content\
            -pkt_size             pkt_size]

    if {$ope == 1} {
       set tlv_length "40"
       set fov "0-1"
       set pov "0-1"
       set tct "0-1"
       set phase_offset_tx               "non-zero"
       set phase_offset_tx_timestamp_sec "non-zero"
       set phase_offset_tx_timestamp_ns  "non-zero"
       set freq_offset_tx                "non-zero"
       set freq_offset_tx_timestamp_sec  "non-zero"
       set freq_offset_tx_timestamp_ns   "non-zero"
    } else {
       set tlv_length "2"
       set fov "dont_care"
       set pov "dont_care"
       set tct "dont_care"
       set phase_offset_tx               "dont_care"
       set phase_offset_tx_timestamp_sec "dont_care"
       set phase_offset_tx_timestamp_ns  "dont_care"
       set freq_offset_tx                "dont_care"
       set freq_offset_tx_timestamp_sec  "dont_care"
       set freq_offset_tx_timestamp_ns   "dont_care"
    }

    if {[info exists pkt_content]} {

        set result [check_ptp_ha_pkt\
                                 -packet                         $pkt_content\
                                 -dest_mac                       $dest_mac\
                                 -src_mac                        "Unicast"\
                                 -vlan_id                        $vlan_id\
                                 -ip_checksum                    $ip_checksum\
                                 -ip_protocol                    $ip_protocol\
                                 -src_ip                         $src_ip\
                                 -dest_ip                        $dest_ip\
                                 -dest_port                      $dest_port\
                                 -udp_checksum                   $udp_checksum\
                                 -major_sdo_id                   0\
                                 -message_type                   [expr 0xC]\
                                 -minor_version                  1\
                                 -version_ptp                    2\
                                 -message_length                 non-zero\
                                 -domain_number                  0-127\
                                 -minor_sdo_id                   0\
                                 -correction_field               0\
                                 -message_type_specific          0\
                                 -src_port_identity              non-zero\
                                 -sequence_id                    0-65535\
                                 -control_field                  5\
                                 -log_message_interval           127\
                                 -target_port_identity           non-zero\
                                 -tlv_type                       [expr 0x8001]\
                                 -tlv_length                     $tlv_length\
                                 -ope                            $ope\
                                 -cr                             0-1\
                                 -rcr                            0-1\
                                 -tcr                            0-1\
                                 -ic                             0-1\
                                 -irc                            0-1\
                                 -itc                            0-1\
                                 -fov                            $fov\
                                 -pov                            $pov\
                                 -tct                            $tct\
                                 -phase_offset_tx                $phase_offset_tx\
                                 -phase_offset_tx_timestamp_sec  $phase_offset_tx_timestamp_sec\
                                 -phase_offset_tx_timestamp_ns   $phase_offset_tx_timestamp_ns\
                                 -freq_offset_tx                 $freq_offset_tx\
                                 -freq_offset_tx_timestamp_sec   $freq_offset_tx_timestamp_sec\
                                 -freq_offset_tx_timestamp_ns    $freq_offset_tx_timestamp_ns]

    }

    return $result
}

################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::resolve_ip_to_mac                               #
#                                                                              #
#  DEFINITION        : This function resolves the ip to mac using ARP          #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#   port_num         : (Mandatory) Transmission port                           #
#                                  (e.g., 0/1)                                 #
#                                                                              #
#   dest_mac         : (Optional)  Destination MAC dddress.                    #
#                                  (e.g., FF:FF:FF:FF:FF:FF)                   #
#                                                                              #
#   src_mac          : (Mandatory) Source MAC address.                         #
#                                  (e.g., 00:80:C2:00:00:21)                   #
#                                                                              #
#   eth_type         : (Optional)  ARP EtherType                               #
#                                  (Default : 0806)                            #
#                                                                              #
#   vlan_id          : (Optional)  Vlan ID                                     #
#                                  (Default : 0)                               #
#                                                                              #
#   vlan_priority    : (Optional)  Vlan Priority                               #
#                                  (Default : 0)                               #
#                                                                              #
#   ip               : (Mandatory) IP address which must be resolved to mac    #
#                                                                              #
#   sender_ip        : (Optional)  Sender IP address                           #
#                                  (Default : 0.0.0.0)                         #
#                                                                              #
#   filter_id        : (Mandatory) Filter id                                   #
#                                                                              #
#   reset_count      : (Optional)  Reset Count  (Default: ON)                  #
#                                                                              #
#   timeout          : (Optional)  Frame timeout (in seconds)                  #
#                                  (Default: 5)                                #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#   mac              : (Mandatory) Resolved Mac address                        #
#                                                                              #
#  RETURNS           : 1 on success (If mac address is resolved)               #
#                                                                              #
#                      0 on failure (If mac address is not resolved)           #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#              ptp_ha::resolve_ip_to_mac\                                      #
#                                -port_num              $port_num\             #
#                                -dest_mac              $dest_mac\             #
#                                -src_mac               $src_mac\              #
#                                -eth_type              $eth_type\             #
#                                -vlan_id               $vlan_id\              #
#                                -vlan_priority         $vlan_priority\        #
#                                -ip                    $ip\                   #
#                                -sender_ip             $sender_ip\            #
#                                -filter_id             $filter_id\            #
#                                -reset_count           $reset_count\          #
#                                -timeout               $timeout\              #
#                                -mac                   mac                    #
#                                                                              #
################################################################################

proc ptp_ha::resolve_ip_to_mac {args} {

    array set param $args

    #INPUT PARAMETERS
    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "ptp_ha::resolve_ip_to_mac: Tee session is not initialized."
        return 0
    }

    #Transmission port
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "ptp_ha::resolve_ip_to_mac: Missing Argument -> port_num"
        return 0
    }

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
    } else {

        set dest_mac ff:ff:ff:ff:ff:ff
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
    } else {

        ERRLOG -msg "ptp_ha::resolve_ip_to_mac: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
    } else {

        set eth_type 0806
    }

    #Vlan ID
    if { $::ptp_ha_vlan_encap == "true" } {
        if {[info exists param(-vlan_id)]} {

            set vlan_id $param(-vlan_id)
        } else {

            set vlan_id $::ptp_ha_vlan_id
        }

        set ::validation_param(vlan_id) $vlan_id

        if {[info exists param(-vlan_priority)]} {

            set vlan_priority $param(-vlan_priority)
        } else {

            set vlan_priority $::ptp_ha_vlan_priority
        }
        
    } else {

        set vlan_id -1
        set vlan_priority 0
    }

    #IP address which must be resolved
    if {[info exists param(-ip)]} {

        set ip $param(-ip)
    } else {

        ERRLOG -msg "ptp_ha::resolve_ip_to_mac: Missing Argument -> ip"
        return 0
    }

    #Sender IP address
    if {[info exists param(-sender_ip)]} {

        set sender_ip $param(-sender_ip)
    } else {

        set sender_ip 0.0.0.0
    }

    #Filter id
    if {[info exists param(-filter_id)]} {

        set filter_id $param(-filter_id)
    } else {

        ERRLOG -msg "ptp_ha::resolve_ip_to_mac: Missing Argument -> filter_id"
        return 0
    }

    #Reset Count
    if {[info exists param(-reset_count)]} {

        set reset_count $param(-reset_count)
    } else {

        set reset_count ON
    }

    #Frame timeout
    if {[info exists param(-timeout)]} {

        set timeout $param(-timeout)
    } else {

        set timeout 5
    }

    #OUTPUT PARAMETERS

    #Resolved Mac address
    if {[info exists param(-mac)]} {

        upvar $param(-mac) mac
        set mac ""
    }

    #LOGIC GOES HERE
    #Timeout in millisec
    set timeout [expr int($timeout*1000)]

    #Send ARP REQUEST
    if {![pltLib::send_arp\
                -session_id                        $session_id\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -message_type                      1\
                -sender_mac                        $src_mac\
                -sender_ip                         $sender_ip\
                -target_mac                        00:00:00:00:00:00\
                -target_ip                         $ip\
                -count                             1\
                -interval                          1]} {

        return 0
    }

    set ::validation_param(dest_mac)         $src_mac
    set ::validation_param(sender_ip)        $ip
    set ::validation_param(target_mac)       $src_mac
    set ::validation_param(target_ip)        $sender_ip
    set ::validation_param(message_type)     2

    printTimeout $timeout

    #Receive ARP REQUEST
    set result [pltLib::recv_periodic\
            -session_id           $session_id\
            -port_num             $port_num\
            -filter_id            $filter_id\
            -timeout              $timeout\
            -reset_count          $reset_count\
            -validation_handler   "ARP"\
            -rx_timestamp         rx_timestamp_sec\
            -rx_time_nanoseconds  rx_timestamp_ns\
            -pkt_content          pkt_content\
            -pkt_size             pkt_size]

    if {[info exists pkt_content]} {
        pbLib::decode_arp_pkt\
                     -packet                      $pkt_content\
                     -sender_mac                  mac
    }

    return $result

}

#################################################################################
#                                                                               #
#  PROCEDURE NAME         : ptp_ha::respond_to_arp_requests                     #
#                                                                               #
#  INPUT PARAMETERS       :                                                     #
#                                                                               #
#   port_num              : (Mandatory) Port number on which frame is to send.  #
#                                       (e.g., 0/1)                             #
#                                                                               #
#   filter_id             : (Mandatory) Filter ID                               #
#                                                                               #
#   mac                   : (Mandatory) MAC address.                            #
#                                                                               #
#   ip                    : (Mandatory) IP address                              #
#                                                                               #
#   vlan_id               : (Optional)  Vlan ID                                 #
#                                       (Default : 0)                           #
#                                                                               #
#   vlan_priority         : (Optional)  Vlan Priority                           #
#                                       (Default : 0)                           #
#                                                                               #
#  OUTPUT PARAMETERS      :  NONE                                               #
#                                                                               #
#  RETURNS                :  1 on success                                       #
#                                                                               #
#                         :  0 on failure                                       #
#                                                                               #
#                                                                               #
#  DEFINITION             : This function is used to respond to arp requests    #
#                                                                               #
#  USAGE                  :                                                     #
#                                                                               #
#      ptp_ha::respond_to_arp_requests\                                         #
#                   -port_num                          $port_num\               #
#                   -filter_id                         $filter_id\              #
#                   -mac                               $mac\                    #
#                   -vlan_id                           $vlan_id\                #
#                   -vlan_priority                     $vlan_priority\          #
#                   -ip                                $ip                      #
#                                                                               #
#################################################################################

proc ptp_ha::respond_to_arp_requests {args} {

    array set param $args

    #INPUT PARAMETERS

    #Session identifier
    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "ptp_ha::respond_to_arp_requests: Tee session is not initialized."
        return 0
    }

    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "ptp_ha::respond_to_arp_requests: Missing Argument -> port_num"
        return 0
    }

    #MAC dddress
    if {[info exists param(-mac)]} {

        set mac $param(-mac)
    } else {

        ERRLOG -msg "ptp_ha::respond_to_arp_requests: Missing Argument -> mac"
        return 0
    }

    #Source MAC address
    if {[info exists param(-ip)]} {

        set ip $param(-ip)
    } else {

        ERRLOG -msg "ptp_ha::respond_to_arp_requests: Missing Argument -> ip"
        return 0
    }

    #Filter ID which captured matching packets
    if {[info exists param(-filter_id)]} {

        set filter_id $param(-filter_id)
    } else {

        ERRLOG -msg "ptp_ha::respond_to_arp_requests: Missing Argument -> filter_id"
        return 0
    }

    #Vlan ID
    if { $::ptp_ha_vlan_encap == "true" } {
        if {[info exists param(-vlan_id)]} {

            set vlan_id $param(-vlan_id)
        } else {

            set vlan_id $::ptp_ha_vlan_id
        }

        set ::validation_param(vlan_id)          $vlan_id

        if {[info exists param(-vlan_priority)]} {

            set vlan_priority $param(-vlan_priority)
        } else {

            set vlan_priority $::ptp_ha_vlan_priority
        }
        
    } else {

        set vlan_id -1
        set vlan_priority 0
    }

    #Source MAC address
    if {[info exists param(-timeout)]} {

        set timeout $param(-timeout)
    } else {

        set timeout 10
    }

    #Timeout in millisec
    set timeout [expr int($timeout*1000)]

    set ::validation_param(target_ip)        $ip
    set ::validation_param(message_type)     1

    set ::validation_param(session_id)       $session_id
    set ::validation_param(port_num)         $port_num
    set ::validation_param(tee_mac)          $mac

    printTimeout $timeout

    #Respond if ARP request is received
    set result [pltLib::recv_periodic\
            -session_id           $session_id\
            -port_num             $port_num\
            -filter_id            $filter_id\
            -timeout              $timeout\
            -reset_count          "OFF"\
            -validation_handler   "ARP_RESPONDER"\
            -rx_timestamp         rx_timestamp_sec\
            -rx_time_nanoseconds  rx_timestamp_ns\
            -pkt_content          pkt_content\
            -pkt_size             pkt_size]

    return 1

}


################################################################################
# PROCEDURE NAME    : ptp_ha::disable_port                                     #
#                                                                              #
# DEFINITION        : This procedure disables the given TEE port               #
#                                                                              #
# INPUT PARAMETERS  :                                                          #
#                                                                              #
# port_num          : (Mandatory) Interface that has to be disabled.           #
#                                                                              #
# USAGE             :                                                          #
#           ptp_ha::disable_port\                                              #
#                    -port_num                  $port_num\                     #
#                                                                              #
# RETURNS           : 1 on Success (If the port is disabled successfully).     #
#                     0 on Failure (If the port could not be disabled).        #
#                                                                              #
################################################################################

proc ptp_ha::disable_port {args} {

    array set param $args

    #Session identifier
    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "ptp_ha::disable_port: Tee session is not initialized."
        return 0
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "ptp_ha::disable_port: Missing Argument -> port_num"
        return 0
    }

    return [pltLib::disable_interface\
                        -session_id       $session_id\
                        -port_num         $port_num]

}

#################################################################################
#                                                                               #
#  PROCEDURE NAME    : ptp_ha::stop_$message                                    #
#                                                                               #
#  DEFINITION        : This function will stop periodic transmission of an      #
#                      message.                                                 #
#                                                                               #
#  INPUT PARAMETERS  :                                                          #
#                                                                               #
#  message           : message name which must be stopped                       #
#                                                                               #
#  USAGE             :                                                          #
#                                                                               #
#     ptp_ha::stop_$message                                                     #
#                                                                               #
#################################################################################
proc ptp_ha::stop_sync { args } {
     return [pltLib::cancel_send_periodic -instance      1]
}

proc ptp_ha::stop_announce { args } {
     return [pltLib::cancel_send_periodic -instance      2]
}

proc ptp_ha::stop_delay_req { args } {
     return [pltLib::cancel_send_periodic -instance      3]
}

proc ptp_ha::stop_delay_resp { args } {
     return [pltLib::cancel_send_periodic -instance      4]
}

proc ptp_ha::stop_followup { args } {
     return [pltLib::cancel_send_periodic -instance      5]
}

proc ptp_ha::stop_pdelay_req { args } {
     return [pltLib::cancel_send_periodic -instance      6]
}

proc ptp_ha::stop_pdelay_resp { args } {
     return [pltLib::cancel_send_periodic -instance      7]
}

proc ptp_ha::stop_pdelay_resp_followup { args } {
     return [pltLib::cancel_send_periodic -instance      8]
}

proc ptp_ha::stop_signal { args } {
     return [pltLib::cancel_send_periodic -instance      9]
}

proc ptp_ha::stop_mgmt { args } {
     return [pltLib::cancel_send_periodic -instance      10]
}

################################################################################
#                                                                              #
#  PROCEDURE NAME    : ptp_ha::get_port_macaddress                             #
#                                                                              #
#  DEFINITION        : This function get the mac address of a port from traffic#
#                      generator                                               #
#                                                                              #
#  INPUT PARAMETERS  :                                                         #
#                                                                              #
#  session_id        : (Optional) session id to which the command is sent      #
#                                                                              #
#  port_num          : (Mandatory) Port whose mac address has to obtain        #
#                                                                              #
#  OUTPUT PARAMETERS :                                                         #
#                                                                              #
#  mac_address       : (Mandatory) Mac address of the traffic generator port   #
#                                                                              #
#  RETURNS           : 1 on success (If the port is sync)                      #
#                                                                              #
#                      0 on failure (If the port is not sync)                  #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#             ptp_ha::get_port_macaddress\                                     #
#                                      -session_id      $session_id\           #
#                                      -port_num        $port_num\             #
#                                      -mac_address     mac_address            #
#                                                                              #
################################################################################

proc ptp_ha::get_port_macaddress {args} {

    array set param $args

    if {[info exists param(-session_id)]} {
        set session_id  $param(-session_id)
    } else {
        set session_id $::tee_session_id
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG  -msg "ptp_ha::get_port_macaddress :  Missing Argument -> port_num"
        return 0
    }

    if {[info exists param(-mac_address)]} {
        upvar  $param(-mac_address) mac_address
    } else {
        ERRLOG  -msg "ptp_ha::get_port_macaddress :  Missing Argument -> mac_address"
        return 0
    }


    if {![ pltLib::get_port_macaddress\
                 -session_id      $session_id\
                 -port_num        $port_num\
                 -mac_address     mac_address]} {

        ERRLOG  -msg "Getting of TEE port MAC is failed."
        return 0
    }

    return 1
}

proc TEE_CLEANUP {} {

    foreach after_info [after info] {
        after cancel $after_info
    }

}


#########################################################################
#  PROCEDURE NAME   : ptp_ha::tee_get_interface_ip                      #
#                                                                       #
#  INPUT PARAMETERS                                                     #
#                                                                       #
#  reference_ip     : (Mandatory) The Reference IP Address.             #
#                                                                       #
#  OUTPUT PARAMETERS                                                    #
#                                                                       #
#  ip               : The IP Address in given network from the          #
#                     reference IP Address.                             #
#                                                                       #
#  RETURNS          : 1 on success                                      #
#                     0 on failure                                      #
#                                                                       #
#  DEFINITION       : This function returns an ip address in the same   #
#                     network                                           #
#                                                                       #
#  USAGE            :                                                   #
#                                                                       #
#  ptp_ha::tee_get_interface_ip\                                        #
#                         -reference_ip    $reference_ip\               #
#                         -ip              ip                           #
#                                                                       #
#########################################################################

proc ptp_ha::tee_get_interface_ip {args} {
 
     array set param $args

    ### Check the Input Parameters ###
     if {[info exists param(-reference_ip)]} {
       set reference_ip $param(-reference_ip)
     } else {
          ERRLOG -msg "ptp_ha::tee_get_interface_ip: Missing Argument -> reference_ip\n"
          return 0
     }

     if {[info exists param(-ip)]} {
          upvar $param(-ip) ip
     }

     set ip_list [split $reference_ip .]

     if {[llength $ip_list] < 4} {
          ERRLOG -msg "ptp_ha::tee_get_interface_ip: Not a valid IP Address $reference_ip\n"
          return 0
     }

     set l1 [lindex $ip_list 0]
     set l2 [lindex $ip_list 1]
     set l3 [lindex $ip_list 2]
     set l4 [lindex $ip_list 3]


     if { $l4 >= 254 } {
       incr l4 -1
     } else {
       incr l4
     }

     set ip [join "$l1 $l2 $l3 $l4" .]

     return 1
}


#########################################################################
#  PROCEDURE NAME   : ptp_ha::tee_recv_error_reason                     #
#                                                                       #
#  INPUT PARAMETERS                                                     #
#                                                                       #
#  error_code       : (Mandatory) Error code                            #
#                                                                       #
#  OUTPUT PARAMETERS: None                                              #
#                                                                       #
#  RETURNS          : Error Reason                                      #
#                                                                       #
#  DEFINITION       : This function returns an error reason for the     #
#                     given error code                                  #
#                                                                       #
#  USAGE            :                                                   #
#                                                                       #
#               ptp_ha::tee_recv_error_reason $error_code               #
#                                                                       #
#########################################################################

proc ptp_ha::tee_recv_error_reason {error_code} {

    if [info exists ::tee_rx_errlog($error_code)] {
        set error_reason [set ::tee_rx_errlog($error_code)]
    } else {
        set error_reason ""
    }

    return $error_reason
}


proc printTimeout {timeout} {

#   puts "Maximum waiting time $timeout ms"
    return 1
}

#################################################################################
#                                                                               #
#  PROCEDURE NAME         : ptp_ha::respond_to_delay_requests                   #
#                                                                               #
#  INPUT PARAMETERS       :                                                     #
#                                                                               #
#   port_num              : (Mandatory) Port number on which frame is to send.  #
#                                       (e.g., 0/1)                             #
#                                                                               #
#   filter_id             : (Mandatory) Filter ID                               #
#                                                                               #
#  OUTPUT PARAMETERS      :  NONE                                               #
#                                                                               #
#  RETURNS                :  1 on success                                       #
#                                                                               #
#                         :  0 on failure                                       #
#                                                                               #
#                                                                               #
#  DEFINITION             : This function is used to respond to delay requests  #
#                                                                               #
#  USAGE                  :                                                     #
#                                                                               #
#      ptp_ha::respond_to_delay_requests\                                       #
#                   -port_num                          $port_num\               #
#                   -filter_id                         $filter_id\              #
#                                                                               #
#################################################################################

proc ptp_ha::respond_to_delay_requests {args} {


    if {![info exists ::PTP_HA_AUTO_RESPONSE_TO_DELAY_REQ]} {

        set ::PTP_HA_AUTO_RESPONSE_TO_DELAY_REQ     "OFF"
    }

    if {($::PTP_HA_AUTO_RESPONSE_TO_DELAY_REQ != "ON")} {return 1}

    array set param $args

    #INPUT PARAMETERS

    #Session identifier
    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "ptp_ha::respond_to_delay_requests: Tee session is not initialized."
        return 0
    }

    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "ptp_ha::respond_to_delay_requests: Missing Argument -> port_num"
        return 0
    }

    #Port number on which frame is to send
    if {[info exists param(-request)]} {

        set req_hex $param(-request)
    } else {

        ERRLOG -msg "ptp_ha::respond_to_delay_requests: Missing Argument -> request"
        return 0
    }

    if [pbLib::decode_ptp_ha_pkt\
                    -packet                         $req_hex\
                    -dest_mac                       dest_mac\
                    -src_mac                        src_mac\
                    -eth_type                       eth_type\
                    -vlan_id                        vlan_id\
                    -vlan_priority                  vlan_priority\
                    -vlan_dei                       vlan_dei\
                    -ip_version                     ip_version\
                    -ip_header_length               ip_header_length\
                    -ip_tos                         ip_tos\
                    -ip_total_length                ip_total_length\
                    -ip_identification              ip_identification\
                    -ip_flags                       ip_flags\
                    -ip_offset                      ip_offset\
                    -ip_ttl                         ip_ttl\
                    -ip_checksum                    ip_checksum\
                    -ip_protocol                    ip_protocol\
                    -src_ip                         src_ip\
                    -dest_ip                        dest_ip\
                    -src_port                       src_port\
                    -dest_port                      dest_port\
                    -udp_length                     udp_length\
                    -udp_checksum                   udp_checksum\
                    -major_sdo_id                   major_sdo_id\
                    -message_type                   message_type\
                    -minor_version                  minor_version\
                    -version_ptp                    version_ptp\
                    -message_length                 message_length\
                    -domain_number                  domain_number\
                    -minor_sdo_id                   minor_sdo_id\
                    -alternate_master_flag          alternate_master_flag\
                    -two_step_flag                  two_step_flag\
                    -unicast_flag                   unicast_flag\
                    -profile_specific1              profile_specific1\
                    -profile_specific2              profile_specific2\
                    -secure                         secure\
                    -leap61                         leap61\
                    -leap59                         leap59\
                    -current_utc_offset_valid       current_utc_offset_valid\
                    -ptp_timescale                  ptp_timescale\
                    -time_traceable                 time_traceable\
                    -freq_traceable                 freq_traceable\
                    -sync_uncertain                 sync_uncertain\
                    -correction_field               correction_field\
                    -message_type_specific          message_type_specific\
                    -src_port_number                src_port_number\
                    -src_clock_identity             src_clock_identity\
                    -sequence_id                    sequence_id\
                    -control_field                  control_field\
                    -log_message_interval           log_message_interval\
                    -origin_timestamp_sec           origin_timestamp_sec\
                    -origin_timestamp_ns            origin_timestamp_ns\
                    -current_utc_offset             current_utc_offset\
                    -gm_priority1                   gm_priority1\
                    -gm_priority2                   gm_priority2\
                    -gm_identity                    gm_identity\
                    -steps_removed                  steps_removed\
                    -time_source                    time_source\
                    -gm_clock_classy                gm_clock_classy\
                    -gm_clock_accuracy              gm_clock_accuracy\
                    -gm_clock_variance              gm_clock_variance\
                    -precise_origin_timestamp_sec   precise_origin_timestamp_sec\
                    -precise_origin_timestamp_ns    precise_origin_timestamp_ns\
                    -receive_timestamp_sec          receive_timestamp_sec\
                    -receive_timestamp_ns           receive_timestamp_ns\
                    -requesting_port_number         requesting_port_number\
                    -requesting_clock_identity      requesting_clock_identity\
                    -request_receipt_timestamp_sec  request_receipt_timestamp_sec\
                    -request_receipt_timestamp_ns   request_receipt_timestamp_ns\
                    -response_origin_timestamp_sec  response_origin_timestamp_sec\
                    -response_origin_timestamp_ns   response_origin_timestamp_ns\
                    -target_port_number             target_port_number\
                    -target_clock_identity          target_clock_identity\
                    -starting_boundary_hops         starting_boundary_hops\
                    -boundary_hops                  boundary_hops\
                    -action_field                   action_field\
                    -tlv                            tlv\
                    -tlv_type                       tlv_type\
                    -tlv_length                     tlv_length\
                    -ope                            ope\
                    -cr                             cr\
                    -rcr                            rcr\
                    -tcr                            tcr\
                    -ic                             ic\
                    -irc                            irc\
                    -itc                            itc\
                    -fov                            fov\
                    -pov                            pov\
                    -tct                            tct\
                    -phase_offset_tx                phase_offset_tx\
                    -phase_offset_tx_timestamp_sec  phase_offset_tx_timestamp_sec\
                    -phase_offset_tx_timestamp_ns   phase_offset_tx_timestamp_ns\
                    -freq_offset_tx                 freq_offset_tx\
                    -freq_offset_tx_timestamp_sec   freq_offset_tx_timestamp_sec\
                    -freq_offset_tx_timestamp_ns    freq_offset_tx_timestamp_ns\
                    -management_id                  management_id\
                    -ext_port_config                ext_port_config\
                    -master_only                    master_only] {

        if {$message_type != 1} {return 0}

        set correction_field        $correction_field
        set src_port_number         $src_port_number
        set src_clock_identity      $src_clock_identity
        set sequence_id             $sequence_id

        ptp_ha::send_delay_resp\
                        -port_num                       $port_num\
                        -src_mac                        $::TEE_MAC\
                        -src_ip                         $::tee_test_port_ip\
                        -unicast_flag                   $::unicast_flag\
                        -domain_number                  $::ptp_dut_default_domain_number\
                        -correction_field               $correction_field\
                        -requesting_port_number         $src_port_number\
                        -requesting_clock_identity      $src_clock_identity\
                        -sequence_id                    $sequence_id\
                        -count                          1
    }

    return 1

}


#################################################################################
#                                                                               #
#  PROCEDURE NAME         : ptp_ha::respond_to_pdelay_requests                  #
#                                                                               #
#  INPUT PARAMETERS       :                                                     #
#                                                                               #
#   port_num              : (Mandatory) Port number on which frame is to send.  #
#                                       (e.g., 0/1)                             #
#                                                                               #
#   filter_id             : (Mandatory) Filter ID                               #
#                                                                               #
#  OUTPUT PARAMETERS      :  NONE                                               #
#                                                                               #
#  RETURNS                :  1 on success                                       #
#                                                                               #
#                         :  0 on failure                                       #
#                                                                               #
#                                                                               #
#  DEFINITION             : This function is used to respond to pdelay requests #
#                                                                               #
#  USAGE                  :                                                     #
#                                                                               #
#      ptp_ha::respond_to_pdelay_requests\                                      #
#                   -port_num                          $port_num\               #
#                   -filter_id                         $filter_id\              #
#                                                                               #
#################################################################################

proc ptp_ha::respond_to_pdelay_requests {args} {


    if {![info exists ::PTP_HA_AUTO_RESPONSE_TO_PDELAY_REQ]} {

        set ::PTP_HA_AUTO_RESPONSE_TO_PDELAY_REQ     "OFF"
    }

    if {($::PTP_HA_AUTO_RESPONSE_TO_PDELAY_REQ != "ON")} {return 1}

    array set param $args

    #INPUT PARAMETERS

    #Session identifier
    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "ptp_ha::respond_to_pdelay_requests: Tee session is not initialized."
        return 0
    }

    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "ptp_ha::respond_to_pdelay_requests: Missing Argument -> port_num"
        return 0
    }

    #Port number on which frame is to send
    if {[info exists param(-request)]} {

        set req_hex $param(-request)
    } else {

        ERRLOG -msg "ptp_ha::respond_to_pdelay_requests: Missing Argument -> request"
        return 0
    }

    if [pbLib::decode_ptp_ha_pkt\
                    -packet                         $req_hex\
                    -dest_mac                       dest_mac\
                    -src_mac                        src_mac\
                    -eth_type                       eth_type\
                    -vlan_id                        vlan_id\
                    -vlan_priority                  vlan_priority\
                    -vlan_dei                       vlan_dei\
                    -ip_version                     ip_version\
                    -ip_header_length               ip_header_length\
                    -ip_tos                         ip_tos\
                    -ip_total_length                ip_total_length\
                    -ip_identification              ip_identification\
                    -ip_flags                       ip_flags\
                    -ip_offset                      ip_offset\
                    -ip_ttl                         ip_ttl\
                    -ip_checksum                    ip_checksum\
                    -ip_protocol                    ip_protocol\
                    -src_ip                         src_ip\
                    -dest_ip                        dest_ip\
                    -src_port                       src_port\
                    -dest_port                      dest_port\
                    -udp_length                     udp_length\
                    -udp_checksum                   udp_checksum\
                    -major_sdo_id                   major_sdo_id\
                    -message_type                   message_type\
                    -minor_version                  minor_version\
                    -version_ptp                    version_ptp\
                    -message_length                 message_length\
                    -domain_number                  domain_number\
                    -minor_sdo_id                   minor_sdo_id\
                    -alternate_master_flag          alternate_master_flag\
                    -two_step_flag                  two_step_flag\
                    -unicast_flag                   unicast_flag\
                    -profile_specific1              profile_specific1\
                    -profile_specific2              profile_specific2\
                    -secure                         secure\
                    -leap61                         leap61\
                    -leap59                         leap59\
                    -current_utc_offset_valid       current_utc_offset_valid\
                    -ptp_timescale                  ptp_timescale\
                    -time_traceable                 time_traceable\
                    -freq_traceable                 freq_traceable\
                    -sync_uncertain                 sync_uncertain\
                    -correction_field               correction_field\
                    -message_type_specific          message_type_specific\
                    -src_port_number                src_port_number\
                    -src_clock_identity             src_clock_identity\
                    -sequence_id                    sequence_id\
                    -control_field                  control_field\
                    -log_message_interval           log_message_interval\
                    -origin_timestamp_sec           origin_timestamp_sec\
                    -origin_timestamp_ns            origin_timestamp_ns\
                    -current_utc_offset             current_utc_offset\
                    -gm_priority1                   gm_priority1\
                    -gm_priority2                   gm_priority2\
                    -gm_identity                    gm_identity\
                    -steps_removed                  steps_removed\
                    -time_source                    time_source\
                    -gm_clock_classy                gm_clock_classy\
                    -gm_clock_accuracy              gm_clock_accuracy\
                    -gm_clock_variance              gm_clock_variance\
                    -precise_origin_timestamp_sec   precise_origin_timestamp_sec\
                    -precise_origin_timestamp_ns    precise_origin_timestamp_ns\
                    -receive_timestamp_sec          receive_timestamp_sec\
                    -receive_timestamp_ns           receive_timestamp_ns\
                    -requesting_port_number         requesting_port_number\
                    -requesting_clock_identity      requesting_clock_identity\
                    -request_receipt_timestamp_sec  request_receipt_timestamp_sec\
                    -request_receipt_timestamp_ns   request_receipt_timestamp_ns\
                    -response_origin_timestamp_sec  response_origin_timestamp_sec\
                    -response_origin_timestamp_ns   response_origin_timestamp_ns\
                    -target_port_number             target_port_number\
                    -target_clock_identity          target_clock_identity\
                    -starting_boundary_hops         starting_boundary_hops\
                    -boundary_hops                  boundary_hops\
                    -action_field                   action_field\
                    -tlv                            tlv\
                    -tlv_type                       tlv_type\
                    -tlv_length                     tlv_length\
                    -ope                            ope\
                    -cr                             cr\
                    -rcr                            rcr\
                    -tcr                            tcr\
                    -ic                             ic\
                    -irc                            irc\
                    -itc                            itc\
                    -fov                            fov\
                    -pov                            pov\
                    -tct                            tct\
                    -phase_offset_tx                phase_offset_tx\
                    -phase_offset_tx_timestamp_sec  phase_offset_tx_timestamp_sec\
                    -phase_offset_tx_timestamp_ns   phase_offset_tx_timestamp_ns\
                    -freq_offset_tx                 freq_offset_tx\
                    -freq_offset_tx_timestamp_sec   freq_offset_tx_timestamp_sec\
                    -freq_offset_tx_timestamp_ns    freq_offset_tx_timestamp_ns\
                    -management_id                  management_id\
                    -ext_port_config                ext_port_config\
                    -master_only                    master_only] {

        if {$message_type != 2} {return 0}

        set correction_field        [expr $correction_field + $::TEE_CORRECTION_FIELD]
        set src_port_number         $src_port_number
        set src_clock_identity      $src_clock_identity
        set sequence_id             $sequence_id

        set timestamp_us                    [clock microseconds]
        set request_receipt_timestamp_sec   [expr $timestamp_us / 1000000]
        set request_receipt_timestamp_ns    [expr ($timestamp_us % 1000000) * 1000]

        if {$::ptp_dut_clock_step == "Two-Step"} {

            set two_step_flag       1
        } else {

            set two_step_flag       0
        }

        ptp_ha::send_pdelay_resp\
                        -port_num                       $port_num\
                        -src_mac                        $::TEE_MAC\
                        -src_ip                         $::tee_test_port_ip\
                        -unicast_flag                   $::unicast_flag\
                        -domain_number                  $::ptp_dut_default_domain_number\
                        -two_step_flag                  $two_step_flag\
                        -correction_field               $correction_field\
                        -requesting_port_number         $src_port_number\
                        -requesting_clock_identity      $src_clock_identity\
                        -sequence_id                    $sequence_id\
                        -request_receipt_timestamp_sec  $request_receipt_timestamp_sec\
                        -request_receipt_timestamp_ns   $request_receipt_timestamp_ns\
                        -count                          1

        if {$two_step_flag == 1} {

            set timestamp_us                    [clock microseconds]
            set response_origin_timestamp_sec   [expr $timestamp_us / 1000000]
            set response_origin_timestamp_ns    [expr ($timestamp_us % 1000000) * 1000]

            ptp_ha::send_pdelay_resp_followup\
                            -port_num                       $port_num\
                            -src_mac                        $::TEE_MAC\
                            -src_ip                         $::tee_test_port_ip\
                            -unicast_flag                   $::unicast_flag\
                            -domain_number                  $::ptp_dut_default_domain_number\
                            -correction_field               $correction_field\
                            -requesting_port_number         $src_port_number\
                            -requesting_clock_identity      $src_clock_identity\
                            -sequence_id                    $sequence_id\
                            -response_origin_timestamp_sec  $response_origin_timestamp_sec\
                            -response_origin_timestamp_ns   $response_origin_timestamp_ns\
                            -count                          1
        }

        ptp_ha::get_tc_details -group_name group_name

        if { ($group_name == "peg") } {

            ptp_ha::send_pdelay_resp\
                        -port_num                       $port_num\
                        -src_mac                        $::TEE_MAC_1\
                        -src_ip                         $::tee_test_port_ip\
                        -unicast_flag                   $::unicast_flag\
                        -domain_number                  $::ptp_dut_default_domain_number\
                        -two_step_flag                  $two_step_flag\
                        -correction_field               $correction_field\
                        -requesting_port_number         $src_port_number\
                        -requesting_clock_identity      $src_clock_identity\
                        -sequence_id                    $sequence_id\
                        -request_receipt_timestamp_sec  $request_receipt_timestamp_sec\
                        -request_receipt_timestamp_ns   $request_receipt_timestamp_ns\
                        -instance                       12\
                        -count                          1

            if {$two_step_flag == 1} {

                set timestamp_us                    [clock microseconds]
                set response_origin_timestamp_sec   [expr $timestamp_us / 1000000]
                set response_origin_timestamp_ns    [expr ($timestamp_us % 1000000) * 1000]

                ptp_ha::send_pdelay_resp_followup\
                            -port_num                       $port_num\
                            -src_mac                        $::TEE_MAC_1\
                            -src_ip                         $::tee_test_port_ip\
                            -unicast_flag                   $::unicast_flag\
                            -domain_number                  $::ptp_dut_default_domain_number\
                            -correction_field               $correction_field\
                            -requesting_port_number         $src_port_number\
                            -requesting_clock_identity      $src_clock_identity\
                            -sequence_id                    $sequence_id\
                            -response_origin_timestamp_sec  $response_origin_timestamp_sec\
                            -response_origin_timestamp_ns   $response_origin_timestamp_ns\
                            -instance                       13\
                            -count                          1
            }
        }
    }

    return 1

}


#################################################################################
#                                                                               #
#  PROCEDURE NAME         : ptp_ha::enable_auto_response_to_delay_requests      #
#                                                                               #
#  INPUT PARAMETERS       :  NONE                                               #
#                                                                               #
#  OUTPUT PARAMETERS      :  NONE                                               #
#                                                                               #
#  RETURNS                :  1 on success                                       #
#                                                                               #
#                         :  0 on failure                                       #
#                                                                               #
#                                                                               #
#  DEFINITION             : This function is used to enable support to respond  #
#                           to delay requests.                                  #
#                                                                               #
#  USAGE                  :                                                     #
#                                                                               #
#      ptp_ha::enable_auto_response_to_delay_requests                           #
#                                                                               #
#################################################################################

proc ptp_ha::enable_auto_response_to_delay_requests {} {

    set ::PTP_HA_AUTO_RESPONSE_TO_DELAY_REQ     "ON"
    return 1
}


#################################################################################
#                                                                               #
#  PROCEDURE NAME         : ptp_ha::disable_auto_response_to_delay_requests     #
#                                                                               #
#  INPUT PARAMETERS       :  NONE                                               #
#                                                                               #
#  OUTPUT PARAMETERS      :  NONE                                               #
#                                                                               #
#  RETURNS                :  1 on success                                       #
#                                                                               #
#                         :  0 on failure                                       #
#                                                                               #
#                                                                               #
#  DEFINITION             : This function is used to disable the support to     #
#                           respond to delay requests.                          #
#                                                                               #
#  USAGE                  :                                                     #
#                                                                               #
#      ptp_ha::disable_auto_response_to_delay_requests                          #
#                                                                               #
#################################################################################

proc ptp_ha::disable_auto_response_to_delay_requests {} {

    set ::PTP_HA_AUTO_RESPONSE_TO_DELAY_REQ     "OFF"
    return 1
}


#################################################################################
#                                                                               #
#  PROCEDURE NAME         : ptp_ha::enable_auto_response_to_pdelay_requests     #
#                                                                               #
#  INPUT PARAMETERS       :  NONE                                               #
#                                                                               #
#  OUTPUT PARAMETERS      :  NONE                                               #
#                                                                               #
#  RETURNS                :  1 on success                                       #
#                                                                               #
#                         :  0 on failure                                       #
#                                                                               #
#                                                                               #
#  DEFINITION             : This function is used to enable support to respond  #
#                           to pdelay requests.                                 #
#                                                                               #
#  USAGE                  :                                                     #
#                                                                               #
#      ptp_ha::enable_auto_response_to_pdelay_requests                          #
#                                                                               #
#################################################################################

proc ptp_ha::enable_auto_response_to_pdelay_requests {} {

    set ::PTP_HA_AUTO_RESPONSE_TO_PDELAY_REQ     "ON"
    return 1
}


#################################################################################
#                                                                               #
#  PROCEDURE NAME         : ptp_ha::disable_auto_response_to_pdelay_requests    #
#                                                                               #
#  INPUT PARAMETERS       :  NONE                                               #
#                                                                               #
#  OUTPUT PARAMETERS      :  NONE                                               #
#                                                                               #
#  RETURNS                :  1 on success                                       #
#                                                                               #
#                         :  0 on failure                                       #
#                                                                               #
#                                                                               #
#  DEFINITION             : This function is used to disable the support to     #
#                           respond to pdelay requests.                         #
#                                                                               #
#  USAGE                  :                                                     #
#                                                                               #
#      ptp_ha::disable_auto_response_to_pdelay_requests                         #
#                                                                               #
#################################################################################

proc ptp_ha::disable_auto_response_to_pdelay_requests {} {

    set ::PTP_HA_AUTO_RESPONSE_TO_PDELAY_REQ     "OFF"
    return 1
}


#################################################################################
#                                                                               #
#  PROCEDURE NAME         : ptp_ha::print_auto_log                              #
#                                                                               #
#  INPUT PARAMETERS       :  NONE                                               #
#                                                                               #
#  OUTPUT PARAMETERS      :  NONE                                               #
#                                                                               #
#  RETURNS                :  1 on success                                       #
#                                                                               #
#                         :  0 on failure                                       #
#                                                                               #
#                                                                               #
#  DEFINITION             : This function hides the prints when receive         #
#                           functions are invoked from auto-responder module.   #
#                                                                               #
#  USAGE                  :                                                     #
#                                                                               #
#      ptp_ha::print_auto_log                                                   #
#                                                                               #
#################################################################################

proc ptp_ha::print_auto_log { message } {

    if {$::PRINT_AUTO_LOG == 1} {

        LOG -level 1 -msg $message
    }

    return 1
}


################################################################################
#                                                                              #
#  PROCEDURE NAME         : ptp_ha::get_tc_details                             #
#                                                                              #
#  INPUT PARAMETERS       :  NONE                                              #
#                                                                              #
#  OUTPUT PARAMETERS      :                                                    #
#                                                                              #
#   log_name                      : (Optional)  Running log file's name.       #
#                                                                              #
#   tc_name                       : (Optional)  Running test case's name.      #
#                                                                              #
#   tc_id                         : (Optional)  Running test case's id.        #
#                                                                              #
#   group_name                    : (Optional)  Running test case's group name.#
#                                                                              #
#  RETURNS                :  1 on success                                      #
#                                                                              #
#                         :  0 on failure                                      #
#                                                                              #
#                                                                              #
#  DEFINITION             : This function gives running test case's name, id   #
#                           and group name.                                    #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#      ptp_ha::get_tc_details                                                  #
#                                                                              #
################################################################################

proc ptp_ha::get_tc_details { args } {

    array set param $args

    if {[info exists param(-log_name)]} {
        upvar $param(-log_name) log_name
    }

    if {[info exists param(-tc_name)]} {
        upvar $param(-tc_name) tc_name
    }

    if {[info exists param(-group_name)]} {
        upvar $param(-group_name) group_name
    }

    if {[info exists param(-tc_id)]} {
        upvar $param(-tc_id) tc_id
    }

    global ATTEST_CORE

    set PktFileName     $ATTEST_CORE(-pktFileName)
    set log_name        [lindex [split $PktFileName "."] 0]
    set tc_name         [split $log_name "_"]
    set group_name      [lindex $tc_name 3]
    set id              [lindex $tc_name 4]

    return 1
}


#################################################################################
#                                                                               #
#  PROCEDURE NAME    : ptp_ha::reset_capture_stats                              #
#                                                                               #
#  DEFINITION        : This function reset the packet capture index & clear pkt #
#                      capture buffer.                                          #
#                                                                               #
#  INPUT PARAMETERS  :                                                          #
#                                                                               #
#  OUTPUT PARAMETERS :                                                          #
#                                                                               #
#      ptp_ha::reset_capture_stats\                                             #
#               -port_num           $port_num                                   #
#                                                                               #
#################################################################################

proc ptp_ha::reset_capture_stats { args } {

    array set param $args

    set port_num      $param(-port_num)

    if {[info exists ::tee_session_id]} {
        set session_id $::tee_session_id
    } else {
        ERRLOG -msg "ptp_ha::reset_capture_stats: TEE session is not initialized."
    }

    #Resource is in-use
    if { $::semlock == "LOCKED" } {

        after 5 ptp_ha::reset_capture_stats\
                        -port_num           $port_num

        return 0
    } else {

        set ::semlock "LOCKED"

        pltLib::reset_all_stats\
                -session_id         $session_id\
                -port_num           $port_num

        set ::semlock "UNLOCKED"
    }

    return 1
}


#################################################################################
#                                                                               #
#  PROCEDURE NAME    : ptp_ha::stop_at_step                                     #
#                                                                               #
#  DEFINITION        : This function stops at the step which is specified in CLI#
#                      integration file.                                        #
#                                                                               #
#  INPUT PARAMETERS  :                                                          #
#                                                                               #
#  OUTPUT PARAMETERS  :                                                         #
#                                                                               #
#      ptp_ha::stop_at_step                                                     #
#                                                                               #
#################################################################################

proc ptp_ha::stop_at_step {} {

    if {$::SKIP_VALIDATION_FOR_DEBUG != 0} {

        incr ::CURRENT_STEP

        set tc_def  "Debugging PTP Accuracy Group test cases."

        LOG -level 1 -msg "\tProceeded STEP $::CURRENT_STEP by ignoring validations in previous steps."

        if {$::CURRENT_STEP >= $::STOP_AT_STEP} {

            TEE_CLEANUP

            TC_CLEAN_AND_ABORT  -reason "Test case is gracefully stopped at STEP\
                                        $::CURRENT_STEP for debugging."\
                                -tc_def $tc_def

            return 0
        }
    }

    return 1
}


################################################################################
#                                                                              #
#  PROCEDURE NAME         : ptp_ha::value_range                                #
#                                                                              #
#  INPUT PARAMETERS       :                                                    #
#                                                                              #
#   value                 : (Mandatory) Value for which range to be calculated.#
#                                                                              #
#   tolerance             : (Mandatory) Tolerance for the range.               #
#                                                                              #
#  OUTPUT PARAMETERS      :                                                    #
#                                                                              #
#   minimum               : (Mandatory) Minimum value.                         #
#                                                                              #
#   maximum               : (Mandatory) Maximum value.                         #
#                                                                              #
#  RETURNS                :  1 on success                                      #
#                                                                              #
#                         :  0 on failure                                      #
#                                                                              #
#                                                                              #
#  DEFINITION             : This function calculates and gives minimum and     #
#                           maximum for the given value with tolerance limit.  #
#                                                                              #
#  USAGE                  :                                                    #
#                                                                              #
#                           ptp_ha::value_range 10 1 minimum maximum           #
#                                                                              #
################################################################################

proc ptp_ha::value_range {value tolerance minimum maximum} {

    set min_multiplier [format "%0.2f" [expr 1 - (($tolerance * 1.0) / 100)]]
    set max_multiplier [format "%0.2f" [expr 1 + (($tolerance * 1.0) / 100)]]

    upvar $minimum lminimum
    upvar $maximum lmaximum

    if {$value > 0} {
        set lminimum [expr $value * $min_multiplier]
        set lmaximum [expr $value * $max_multiplier]
    } else {
        set lminimum [expr $value * $max_multiplier]
        set lmaximum [expr $value * $min_multiplier]
    }

    set lminimum [format "%.3f" $lminimum]
    set lmaximum [format "%.3f" $lmaximum]

    return 1
}
