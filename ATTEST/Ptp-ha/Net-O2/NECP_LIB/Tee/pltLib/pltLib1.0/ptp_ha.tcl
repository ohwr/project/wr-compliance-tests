################################################################################
# File Name         : ptp_ha.tcl                                               #
# File Version      : 1.5                                                      #
# Component Name    : ATTEST Platform Library                                  #
# Module Name       : Precision Time Protocol - High Accuracy                  #
################################################################################
# History       Date       Author         Addition/ Alteration                 #
#                                                                              #
#  1.0         Apr/2018    CERN           Initial                              #
#  1.1         Sep/2018    CERN           Renamed messages in PSD.             #
#  1.2         Oct/2018    CERN           Added create_filter_ptp_ha_pkt.      #
#  1.3         Nov/2018    CERN           a) Support added to respond Delay_Req#
#                                            and Pdelay_Req messages           #
#                                            automatically in ptp-ha_psd_name. #
#                                         b) Fixed error in                    #
#                                            response_origin_timestamp_sec.    #
#  1.4         Dec/2018    CERN           a) Made instance value as argument in#
#                                            send_ptp_ha_pdelay_resp_followup. #
#                                         b) Added below procedures,           #
#                                            - ptp-ha_transmission_sequencing  #
#                                            - ptp-ha_transmission_queueing    #
#  1.5         Apr/2019    CERN           Added queueing support for One-Step  #
#                                         clock.                               #
#                                                                              #
################################################################################
#   Copyright (C) CERN 2018 - 2019                                             #
################################################################################

#########################PROCEDURES USED########################################
#                                                                              #
#  1. pltLib::send_ptp_ha_sync                                                 #
#  2. pltLib::send_ptp_ha_announce                                             #
#  3. pltLib::send_ptp_ha_delay_req                                            #
#  4. pltLib::send_ptp_ha_delay_resp                                           #
#  5. pltLib::send_ptp_ha_followup                                             #
#  6. pltLib::send_ptp_ha_pdelay_req                                           #
#  7. pltLib::send_ptp_ha_pdelay_resp                                          #
#  8. pltLib::send_ptp_ha_pdelay_resp_followup                                 #
#  9. pltLib::send_ptp_ha_signal                                               #
# 10. pltLib::send_ptp_ha_mgmt                                                 #
# 11. pltLib::send_arp                                                         #
# 12. pltLib::create_filter_ptp_ha_pkt                                         #
#                                                                              #
################################################################################

namespace eval pltLib {}

set ::ok_to_send_1           1; #Sync Message
set ::ok_to_send_4           0; #Delay_Resp Message
set ::ok_to_send_5           0; #Follow_Up Message
set ::ok_to_send_7           1; #Pdelay_Resp Message (with correct source MAC)
set ::ok_to_send_8           0; #Pdelay_Resp_Follow_Up Message (with correct source MAC)
set ::ok_to_send_12          1; #Pdelay_Resp Message (with incorrect source MAC)
set ::ok_to_send_13          0; #Pdelay_Resp_Follow_Up Message (with incorrect source MAC)

#1.
#################################################################################
#                                                                               #
#  PROCEDURE NAME         : pltLib::send_ptp_ha_sync                            #
#                                                                               #
#  INPUT PARAMETERS       :                                                     #
#                                                                               #
#   session_id            :  (Mandatory) Session identifier                     #
#                                                                               #
#   port_num              :  (Mandatory) Port number on which frame is to send. #
#                                        (e.g., 0/1)                            #
#                                                                               #
#   dest_mac              :  (Mandatory) Destination MAC dddress.               #
#                                        (e.g., 01:80:C2:00:00:20)              #
#                                                                               #
#   src_mac               :  (Mandatory) Source MAC address.                    #
#                                        (e.g., 00:80:C2:00:00:21)              #
#                                                                               #
#   eth_type              :  (Optional)  EtherType (0800 or 88F7)               #
#                                        (Default : 0800)                       #
#                                                                               #
#   vlan_id               :  (Optional)  Vlan ID                                #
#                                        (Default : 0)                          #
#                                                                               #
#   vlan_priority         :  (Optional)  Vlan Priority                          #
#                                        (Default : 0)                          #
#                                                                               #
#   dest_ip               :  (Optional)  Destination IP address                 #
#                                        (Default : 224.0.1.129)                #
#                                                                               #
#   src_ip                :  (Optional)  Source IP address                      #
#                                        (Default : 0.0.0.0)                    #
#                                                                               #
#   ip_checksum           :  (Optional)  IP checksum                            #
#                                        (Default : 0)                          #
#                                                                               #
#   dest_port             :  (Optional)  UDP destination port number            #
#                                        (Default : 319)                        #
#                                                                               #
#   src_port              :  (Optional)  UDP source port number                 #
#                                        (Default : 319)                        #
#                                                                               #
#   udp_length            :  (Optional)  UDP length                             #
#                                        (Default : 0)                          #
#                                                                               #
#   udp_checksum          :  (Optional)  UDP checksum                           #
#                                        (Default : 0)                          #
#                                                                               #
#   major_sdo_id          :  (Optional)  Major sdo id                           #
#                                        (Default : 0)                          #
#                                                                               #
#   message_type          :  (Optional)  Message Type                           #
#                                        (Default : 0)                          #
#                                                                               #
#   minor_version         :  (Optional)  PTP version number                     #
#                                        (Default : 2)                          #
#                                                                               #
#   version_ptp           :  (Optional)  PTP minor version number               #
#                                        (Default : 2)                          #
#                                                                               #
#   message_length        :  (Optional)  PTP Sync message length                #
#                                        (Default : 44)                         #
#                                                                               #
#   domain_number         :  (Optional)  Domain number                          #
#                                        (Default : 0)                          #
#                                                                               #
#   minor_sdo_id          :  (Optional)  Minor Sdo id                           #
#                                        (Default : 0)                          #
#                                                                               #
#   alternate_master_flag :  (Optional)  Alternate Master flag                  #
#                                        (Default : 0)                          #
#                                                                               #
#   two_step_flag         :  (Optional)  Two Step Flag                          #
#                                        (Default : 0)                          #
#                                                                               #
#   unicast_flag          :  (Optional)  Unicast Flag                           #
#                                        (Default : 0)                          #
#                                                                               #
#   profile_specific1     :  (Optional)  Profile specific1 Flag                 #
#                                        (Default : 0)                          #
#                                                                               #
#   profile_specific2     :  (Optional)  Profile specific2 Flag                 #
#                                        (Default : 0)                          #
#                                                                               #
#   secure                :  (Optional)  Secure Flag                            #
#                                        (Default : 0)                          #
#                                                                               #
#   leap61                :  (Optional)  Leap61 Flag                            #
#                                        (Default : 0)                          #
#                                                                               #
#   leap59                :  (Optional)  Leap59 Flag                            #
#                                        (Default : 0)                          #
#                                                                               #
#   current_utc_offset_valid:  (Optional)Current UTC Offset Flag                #
#                                        (Default : 0)                          #
#                                                                               #
#   ptp_timescale         :  (Optional)  PTP TimeSclae Flag                     #
#                                        (Default : 0)                          #
#                                                                               #
#   time_tracaeble        :  (Optional)  Time Traceable Flag                    #
#                                        (Default : 0)                          #
#                                                                               #
#   freq_tracaeble        :  (Optional)  Frequency Traceable Flag               #
#                                        (Default : 0)                          #
#                                                                               #
#   sync_uncertain        :  (Optional)  Synchronization uncertain Flag         #
#                                        (Default : 0)                          #
#                                                                               #
#   correction_field      :  (Optional)  Correction Field value                 #
#                                        (Default : 0)                          #
#                                                                               #
#   message_type_specific :  (Optional)  Message type                           #
#                                        (Default : 0)                          #
#                                                                               #
#   src_port_number       :  (Optional)  Source port number                     #
#                                        (Default : 0)                          #
#                                                                               #
#   src_clock_identity    :  (Optional)  Source Clock identity                  #
#                                        (Default : 0)                          #
#                                                                               #
#   sequence_id           :  (Optional)  Sequence ID                            #
#                                        (Default : 0)                          #
#                                                                               #
#   control_field         :  (Optional)  Control field                          #
#                                        (Default : 00)                         #
#                                                                               #
#   log_message_interval  :  (Optional)  Log message interval                   #
#                                        (Default : 0)                          #
#                                                                               #
#   origin_timestamp_sec  :  (Optional)  Origin timestamp in seconds            #
#                                        (Default : 0)                          #
#                                                                               #
#   origin_timestamp_ns   :  (Optional)  Origin timestamp in nanoseconds        #
#                                        (Default : 0)                          #
#                                                                               #
#   count                 :  (Optional)  Frame count                            #
#                                        (Default : 1)                          #
#                                                                               #
#   interval              :  (Optional)  Frame interval (in seconds)            #
#                                        (Default : 1)                          #
#                                                                               #
#  OUTPUT PARAMETERS      :  NONE                                               #
#                                                                               #
#  RETURNS                :  1 on success (If PTP Sync message is sent with the #
#                                          given parameters)                    #
#                                                                               #
#                                                                               #
#                         :  0 on success (If PTP Sync message could not be     #
#                                          sent with the given parameters)      #
#                                                                               #
#  DEFINITION             : This function is used to construct and send PTP     #
#                           sync message.                                       #
#                                                                               #
#  USAGE                  :                                                     #
#                                                                               #
#       pltLib::send_ptp_ha_sync\                                               #
#                   -session_id                        $session_id\             #
#                   -port_num                          $port_num\               #
#                   -dest_mac                          $dest_mac\               #
#                   -src_mac                           $src_mac\                #
#                   -eth_type                          $eth_type\               #
#                   -vlan_id                           $vlan_id\                #
#                   -vlan_priority                     $vlan_priority\          #
#                   -dest_ip                           $dest_ip\                #
#                   -src_ip                            $src_ip\                 #
#                   -ip_checksum                       $ip_checksum\            #
#                   -dest_port                         $dest_port\              #
#                   -src_port                          $src_port\               #
#                   -udp_length                        $udp_length\             #
#                   -udp_checksum                      $udp_checksum\           #
#                   -major_sdo_id                      $major_sdo_id\           #
#                   -message_type                      $message_type\           #
#                   -minor_version                     $minor_version\          #
#                   -version_ptp                       $version_ptp\            #
#                   -message_length                    $message_length\         #
#                   -domain_number                     $domain_number\          #
#                   -minor_sdo_id                      $minor_sdo_id\           #
#                   -alternate_master_flag             $alternate_master_flag\  #
#                   -two_step_flag                     $two_step_flag\          #
#                   -unicast_flag                      $unicast_flag\           #
#                   -profile_specific1                 $profile_specific1\      #
#                   -profile_specific2                 $profile_specific2\      #
#                   -secure                            $secure\                 #
#                   -leap61                            $leap61\                 #
#                   -leap59                            $leap59\                 #
#                   -current_utc_offset_valid         $current_utc_offset_valid\#
#                   -ptp_timescale                     $ptp_timescale\          #
#                   -time_tracaeble                    $time_traceable\         #
#                   -freq_tracaeble                    $freq_traceable\         #
#                   -sync_uncertain                    $sync_uncertain\         #
#                   -correction_field                  $correction_field\       #
#                   -message_type_specific             $message_type_specific\  #
#                   -src_port_number                   $src_port_number\        #
#                   -src_clock_identity                $src_clock_identity\     #
#                   -sequence_id                       $sequence_id\            #
#                   -control_field                     $control_field\          #
#                   -log_message_interval              $log_message_interval\   #
#                   -origin_timestamp_sec              $origin_timestamp_sec\   #
#                   -origin_timestamp_ns               $origin_timestamp_ns\    #
#                   -count                             $count\                  #
#                   -interval                          $interval                #
#                                                                               #
#################################################################################

proc pltLib::send_ptp_ha_sync {args} {

    array set param $args

    #INPUT PARAMETERS

    #Session identifier
    if {[info exists param(-session_id)]} {

        set session_id $param(-session_id)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_sync: Missing Argument -> session_id"
        return 0
    }

    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_sync: Missing Argument -> port_num"
        return 0
    }

    #ETHERNET HEADER OPTIONS

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_sync: Missing Argument -> dest_mac"
        return 0
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_sync: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
    } else {

        set eth_type 0800
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
    } else {

        set vlan_id 0
    }

    #Vlan Priority
    if {[info exists param(-vlan_priority)]} {

        set vlan_priority $param(-vlan_priority)
    } else {

        set vlan_priority 0
    }

    #IP HEADER OPTIONS

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
    } else {

        set dest_ip 224.0.1.129
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
    } else {

        set src_ip 0.0.0.0
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
    } else {

        set ip_checksum 0
    }

    #UDP HEADER OPTIONS

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
    } else {

        set dest_port 319
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
    } else {

        set src_port 319
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
    } else {

        set udp_length 0
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
    } else {

        set udp_checksum 0
    }

    #PTP HEADER OPTIONS

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
    } else {

        set major_sdo_id 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
    } else {

        set message_type 0
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
    } else {

        set minor_version 2
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
    } else {

        set message_length 44
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
    } else {

        set domain_number 0
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
    } else {

        set minor_sdo_id 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
    } else {

        set correction_field 0
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
    } else {

        set message_type_specific 0
    }

    #Source port number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
    } else {

        set src_port_number 0
    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
    } else {

        set src_clock_identity 0
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
    } else {

        set sequence_id 0
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
    } else {

        set control_field 00
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
    } else {

        set log_message_interval 0
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
    } else {

        set unicast_flag 0
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
    } else {

        set profile_specific2 0
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
    } else {

        set secure 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
    } else {

        set freq_traceable 0
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
    } else {

        set sync_uncertain 0
    }

    #PTP SYNC MESSAGE OPTIONS
    #Origin timestamp in seconds
    if {[info exists param(-origin_timestamp_sec)]} {

        set origin_timestamp_sec $param(-origin_timestamp_sec)
    } else {

        set origin_timestamp_sec 0
    }

    #Origin timestamp in nanoseconds
    if {[info exists param(-origin_timestamp_ns)]} {

        set origin_timestamp_ns $param(-origin_timestamp_ns)
    } else {

        set origin_timestamp_ns 0
    }

    #MISCELLANEOUS
    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
    } else {

        set interval 1
    }

    #Interval in millisec
    set interval [expr int($interval*1000)]

    #CONSTRUCT PTP HA SYNC PACKET DUMP
    if {![pbLib::encode_ptp_ha_pkt\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -major_sdo_id                      $major_sdo_id\
                -message_type                      $message_type\
                -minor_version                     $minor_version\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -minor_sdo_id                      $minor_sdo_id\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -secure                            $secure\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_tracaeble                    $time_traceable\
                -freq_tracaeble                    $freq_traceable\
                -sync_uncertain                    $sync_uncertain\
                -correction_field                  $correction_field\
                -message_type_specific             $message_type_specific\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -origin_timestamp_sec              $origin_timestamp_sec\
                -origin_timestamp_ns               $origin_timestamp_ns\
                -hexdump                           hexdump]} {

        return 0
    }

    pltLib::send_periodic\
                     -session_id     $session_id\
                     -port_num       $port_num\
                     -data           $hexdump\
                     -instance       1\
                     -interval       $interval\
                     -count          $count\
                     -handler_id     "PTP_HA"

    return 1

}


#2.
#################################################################################
#                                                                               #
#  PROCEDURE NAME         : pltLib::send_ptp_ha_announce                        #
#                                                                               #
#  INPUT PARAMETERS       :                                                     #
#                                                                               #
#   session_id            :  (Mandatory) Session identifier                     #
#                                                                               #
#   port_num              :  (Mandatory) Port number on which frame is to send. #
#                                        (e.g., 0/1)                            #
#                                                                               #
#   dest_mac              :  (Mandatory) Destination MAC dddress.               #
#                                        (e.g., 01:80:C2:00:00:20)              #
#                                                                               #
#   src_mac               :  (Mandatory) Source MAC address.                    #
#                                        (e.g., 00:80:C2:00:00:21)              #
#                                                                               #
#   eth_type              :  (Optional)  EtherType (0800 or 88F7)               #
#                                        (Default : 0800)                       #
#                                                                               #
#   vlan_id               :  (Optional)  Vlan ID                                #
#                                        (Default : 0)                          #
#                                                                               #
#   vlan_priority         :  (Optional)  Vlan Priority                          #
#                                        (Default : 0)                          #
#                                                                               #
#   dest_ip               :  (Optional)  Destination IP address                 #
#                                        (Default : 224.0.1.129)                #
#                                                                               #
#   src_ip                :  (Optional)  Source IP address                      #
#                                        (Default : 0.0.0.0)                    #
#                                                                               #
#   ip_checksum           :  (Optional)  IP checksum                            #
#                                        (Default : 0)                          #
#                                                                               #
#   dest_port             :  (Optional)  UDP destination port number            #
#                                        (Default : 319)                        #
#                                                                               #
#   src_port              :  (Optional)  UDP source port number                 #
#                                        (Default : 319)                        #
#                                                                               #
#   udp_length            :  (Optional)  UDP length                             #
#                                        (Default : 72)                         #
#                                                                               #
#   udp_checksum          :  (Optional)  UDP checksum                           #
#                                        (Default : 0)                          #
#                                                                               #
#   major_sdo_id          :  (Optional)  Major sdo id                           #
#                                        (Default : 0)                          #
#                                                                               #
#   message_type          :  (Optional)  Message Type                           #
#                                        (Default : 0)                          #
#                                                                               #
#   minor_version         :  (Optional)  PTP version number                     #
#                                        (Default : 2)                          #
#                                                                               #
#   version_ptp           :  (Optional)  PTP minor version number               #
#                                        (Default : 2)                          #
#                                                                               #
#   message_length        :  (Optional)  PTP Announce message length            #
#                                        (Default : 64)                         #
#                                                                               #
#   domain_number         :  (Optional)  Domain number                          #
#                                        (Default : 0)                          #
#                                                                               #
#   minor_sdo_id          :  (Optional)  Minor Sdo id                           #
#                                        (Default : 0)                          #
#                                                                               #
#   alternate_master_flag :  (Optional)  Alternate Master flag                  #
#                                        (Default : 0)                          #
#                                                                               #
#   two_step_flag         :  (Optional)  Two Step Flag                          #
#                                        (Default : 0)                          #
#                                                                               #
#   unicast_flag          :  (Optional)  Unicast Flag                           #
#                                        (Default : 0)                          #
#                                                                               #
#   profile_specific1     :  (Optional)  Profile specific1 Flag                 #
#                                        (Default : 0)                          #
#                                                                               #
#   profile_specific2     :  (Optional)  Profile specific2 Flag                 #
#                                        (Default : 0)                          #
#                                                                               #
#   secure                :  (Optional)  Secure Flag                            #
#                                        (Default : 0)                          #
#                                                                               #
#   leap61                :  (Optional)  Leap61 Flag                            #
#                                        (Default : 0)                          #
#                                                                               #
#   leap59                :  (Optional)  Leap59 Flag                            #
#                                        (Default : 0)                          #
#                                                                               #
#   current_utc_offset_valid:  (Optional)Current UTC Offset Flag                #
#                                        (Default : 0)                          #
#                                                                               #
#   ptp_timescale         :  (Optional)  PTP TimeSclae Flag                     #
#                                        (Default : 0)                          #
#                                                                               #
#   time_tracaeble        :  (Optional)  Time Traceable Flag                    #
#                                        (Default : 0)                          #
#                                                                               #
#   freq_tracaeble        :  (Optional)  Frequency Traceable Flag               #
#                                        (Default : 0)                          #
#                                                                               #
#   sync_uncertain        :  (Optional)  Synchronization uncertain Flag         #
#                                        (Default : 0)                          #
#                                                                               #
#   correction_field      :  (Optional)  Correction Field value                 #
#                                        (Default : 0)                          #
#                                                                               #
#   message_type_specific :  (Optional)  Message type                           #
#                                        (Default : 0)                          #
#                                                                               #
#   src_port_number       :  (Optional)  Source port number                     #
#                                        (Default : 0)                          #
#                                                                               #
#   src_clock_identity    :  (Optional)  Source Clock identity                  #
#                                        (Default : 0)                          #
#                                                                               #
#   sequence_id           :  (Optional)  Sequence ID                            #
#                                        (Default : 0)                          #
#                                                                               #
#   control_field         :  (Optional)  Control field                          #
#                                        (Default : 00)                         #
#                                                                               #
#   log_message_interval  :  (Optional)  Log message interval                   #
#                                        (Default : 0)                          #
#                                                                               #
#   origin_timestamp_sec  :  (Optional)  Origin timestamp in seconds            #
#                                        (Default : 0)                          #
#                                                                               #
#   origin_timestamp_ns   :  (Optional)  Origin timestamp in nanoseconds        #
#                                        (Default : 0)                          #
#                                                                               #
#   current_utc_offset    :  (Optional)  Current UTC Offset                     #
#                                        (Default : 0)                          #
#                                                                               #
#   gm_priority1          :  (Optional)  Grand Master Priority 1                #
#                                        (Default : 0)                          #
#                                                                               #
#   gm_priority2          :  (Optional)  Grand Master Priority 2                #
#                                        (Default : 128)                        #
#                                                                               #
#   gm_identity           :  (Optional)  Grand Master Identity                  #
#                                        (Default : 0)                          #
#                                                                               #
#   steps_removed         :  (Optional)  Steps Removed                          #
#                                        (Default : 0)                          #
#                                                                               #
#   time_source           :  (Optional)  Time Source                            #
#                                        (Default : 0)                          #
#                                                                               #
#   gm_clock_classy       :  (Optional)  Grand Master Clock classy              #
#                                        (Default : 0)                          #
#                                                                               #
#   gm_clock_accuracy     :  (Optional)  Grand Master Clock accuracy            #
#                                        (Default : 0)                          #
#                                                                               #
#   gm_clock_variance     :  (Optional)  Grand Master Clock offset scaled       #
#                                        log variance (Default : 0)             #
#                                                                               #
#   count                 :  (Optional)  Frame count                            #
#                                        (Default : 1)                          #
#                                                                               #
#   interval              :  (Optional)  Frame interval (in seconds)            #
#                                        (Default : 1)                          #
#                                                                               #
#  OUTPUT PARAMETERS      :  NONE                                               #
#                                                                               #
#  RETURNS                :  1 on success (If PTP Announce message is sent with #
#                                          the given parameters)                #
#                                                                               #
#                                                                               #
#                         :  0 on success (If PTP Announce message could not be #
#                                          sent with the given parameters)      #
#                                                                               #
#  DEFINITION             : This function is used to construct and send PTP     #
#                           announce message.                                   #
#                                                                               #
#  USAGE                  :                                                     #
#                                                                               #
#       pltLib::send_ptp_ha_announce\                                           #
#                   -session_id                        $session_id\             #
#                   -port_num                          $port_num\               #
#                   -dest_mac                          $dest_mac\               #
#                   -src_mac                           $src_mac\                #
#                   -eth_type                          $eth_type\               #
#                   -vlan_id                           $vlan_id\                #
#                   -vlan_priority                     $vlan_priority\          #
#                   -dest_ip                           $dest_ip\                #
#                   -src_ip                            $src_ip\                 #
#                   -ip_checksum                       $ip_checksum\            #
#                   -dest_port                         $dest_port\              #
#                   -src_port                          $src_port\               #
#                   -udp_length                        $udp_length\             #
#                   -udp_checksum                      $udp_checksum\           #
#                   -major_sdo_id                      $major_sdo_id\           #
#                   -message_type                      $message_type\           #
#                   -minor_version                     $minor_version\          #
#                   -version_ptp                       $version_ptp\            #
#                   -message_length                    $message_length\         #
#                   -domain_number                     $domain_number\          #
#                   -minor_sdo_id                      $minor_sdo_id\           #
#                   -alternate_master_flag             $alternate_master_flag\  #
#                   -two_step_flag                     $two_step_flag\          #
#                   -unicast_flag                      $unicast_flag\           #
#                   -profile_specific1                 $profile_specific1\      #
#                   -profile_specific2                 $profile_specific2\      #
#                   -secure                            $secure\                 #
#                   -leap61                            $leap61\                 #
#                   -leap59                            $leap59\                 #
#                   -current_utc_offset_valid         $current_utc_offset_valid\#
#                   -ptp_timescale                     $ptp_timescale\          #
#                   -time_tracaeble                    $time_traceable\         #
#                   -freq_tracaeble                    $freq_traceable\         #
#                   -sync_uncertain                    $sync_uncertain\         #
#                   -correction_field                  $correction_field\       #
#                   -message_type_specific             $message_type_specific\  #
#                   -src_port_number                   $src_port_number\        #
#                   -src_clock_identity                $src_clock_identity\     #
#                   -sequence_id                       $sequence_id\            #
#                   -control_field                     $control_field\          #
#                   -log_message_interval              $log_message_interval\   #
#                   -origin_timestamp_sec              $origin_timestamp_sec\   #
#                   -origin_timestamp_ns               $origin_timestamp_ns\    #
#                   -current_utc_offset                $current_utc_offset\     #
#                   -gm_priority1                      $gm_priority1\           #
#                   -gm_priority2                      $gm_priority2\           #
#                   -gm_identity                       $gm_identity\            #
#                   -steps_removed                     $steps_removed\          #
#                   -gm_clock_classy                   $gm_clock_classy\        #
#                   -gm_clock_accuracy                 $gm_clock_accuracy\      #
#                   -gm_clock_variance                 $gm_clock_variance\      #
#                   -count                             $count\                  #
#                   -interval                          $interval                #
#                                                                               #
#################################################################################

proc pltLib::send_ptp_ha_announce {args} {

    array set param $args

    #INPUT PARAMETERS

    #Session identifier
    if {[info exists param(-session_id)]} {

        set session_id $param(-session_id)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_announce: Missing Argument -> session_id"
        return 0
    }

    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_announce: Missing Argument -> port_num"
        return 0
    }

    #ETHERNET HEADER OPTIONS

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_announce: Missing Argument -> dest_mac"
        return 0
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_announce: Missing Argument -> src_mac"
        return 0
    }

    #PTP EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
    } else {

        set eth_type 0800
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
    } else {

        set vlan_id 0
    }

    #Vlan Priority
    if {[info exists param(-vlan_priority)]} {

        set vlan_priority $param(-vlan_priority)
    } else {

        set vlan_priority 0
    }

    #IP HEADER OPTIONS

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
    } else {

        set dest_ip 224.0.1.129
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
    } else {

        set src_ip 0.0.0.0
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
    } else {

        set ip_checksum 0
    }

    #UDP HEADER OPTIONS

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
    } else {

        set dest_port 319
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
    } else {

        set src_port 319
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
    } else {

        set udp_length 72
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
    } else {

        set udp_checksum 0
    }

    #PTP HEADER OPTIONS

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
    } else {

        set major_sdo_id 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
    } else {

        set message_type 11
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
    } else {

        set minor_version 2
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
    } else {

        set version_ptp 2
    }

    #PTP Announce message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
    } else {

        set message_length 64
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
    } else {

        set domain_number 0
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
    } else {

        set minor_sdo_id 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
    } else {

        set correction_field 0
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
    } else {

        set message_type_specific 0
    }

    #Source port number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
    } else {

        set src_port_number 0
    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
    } else {

        set src_clock_identity 0
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
    } else {

        set sequence_id 0
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
    } else {

        set control_field 00
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
    } else {

        set log_message_interval 0
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
    } else {

        set unicast_flag 0
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
    } else {

        set profile_specific2 0
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
    } else {

        set secure 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
    } else {

        set freq_traceable 0
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
    } else {

        set sync_uncertain 0
    }

    #PTP ANNOUNCE MESSAGE OPTIONS
    #Origin timestamp in seconds
    if {[info exists param(-origin_timestamp_sec)]} {

        set origin_timestamp_sec $param(-origin_timestamp_sec)
    } else {

        set origin_timestamp_sec 0
    }

    #Origin timestamp in nanoseconds
    if {[info exists param(-origin_timestamp_ns)]} {

        set origin_timestamp_ns $param(-origin_timestamp_ns)
    } else {

        set origin_timestamp_ns 0
    }

    #Current UTC Offset
    if {[info exists param(-current_utc_offset)]} {
        set current_utc_offset $param(-current_utc_offset)
    } else {
        set current_utc_offset 0
    }

    #Grand Master Priority 1
    if {[info exists param(-gm_priority1)]} {
        set gm_priority1 $param(-gm_priority1)
    } else {
        set gm_priority1 0
    }

    #Grand Master Priority 2
    if {[info exists param(-gm_priority2)]} {
        set gm_priority2 $param(-gm_priority2)
    } else {
        set gm_priority2 0
    }

    #Grand Master Identity
    if {[info exists param(-gm_identity)]} {
        set gm_identity $param(-gm_identity)
    } else {
        set gm_identity 0
    }

    #Steps Removed
    if {[info exists param(-steps_removed)]} {
        set steps_removed $param(-steps_removed)
    } else {
        set steps_removed 0
    }

    #Time Source
    if {[info exists param(-time_source)]} {
        set time_source $param(-time_source)
    } else {
        set time_source 0
    }

    #Grand Master Clock classy
    if {[info exists param(-gm_clock_classy)]} {
        set gm_clock_classy $param(-gm_clock_classy)
    } else {
        set gm_clock_classy 0
    }

    #Grand Master Clock accuracy
    if {[info exists param(-gm_clock_accuracy)]} {
        set gm_clock_accuracy $param(-gm_clock_accuracy)
    } else {
        set gm_clock_accuracy 0
    }

    #Grand Master Clock offset scaled
    if {[info exists param(-gm_clock_variance)]} {
        set gm_clock_variance $param(-gm_clock_variance)
    } else {
        set gm_clock_variance 0
    }

    #MISCELLANEOUS
    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
    } else {

        set interval 1
    }

    #Interval in millisec
    set interval [expr int($interval*1000)]


    #CONSTRUCT PTP HA ANNOUNCE PACKET DUMP
    if {![pbLib::encode_ptp_ha_pkt\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -major_sdo_id                      $major_sdo_id\
                -message_type                      $message_type\
                -minor_version                     $minor_version\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -minor_sdo_id                      $minor_sdo_id\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -secure                            $secure\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_tracaeble                    $time_traceable\
                -freq_tracaeble                    $freq_traceable\
                -sync_uncertain                    $sync_uncertain\
                -correction_field                  $correction_field\
                -message_type_specific             $message_type_specific\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -origin_timestamp_sec              $origin_timestamp_sec\
                -origin_timestamp_ns               $origin_timestamp_ns\
                -current_utc_offset                $current_utc_offset\
                -gm_priority1                      $gm_priority1\
                -gm_priority2                      $gm_priority2\
                -gm_identity                       $gm_identity\
                -steps_removed                     $steps_removed\
                -time_source                       $time_source\
                -gm_clock_classy                   $gm_clock_classy\
                -gm_clock_accuracy                 $gm_clock_accuracy\
                -gm_clock_variance                 $gm_clock_variance\
                -hexdump                           hexdump]} {

        return 0
    }

    pltLib::send_periodic\
                     -session_id     $session_id\
                     -port_num       $port_num\
                     -data           $hexdump\
                     -instance       2\
                     -interval       $interval\
                     -count          $count\
                     -handler_id     "PTP_HA"

    return 1

}


#3.
#################################################################################
#                                                                               #
#  PROCEDURE NAME         : pltLib::send_ptp_ha_delay_req                       #
#                                                                               #
#  INPUT PARAMETERS       :                                                     #
#                                                                               #
#   session_id            :  (Mandatory) Session identifier                     #
#                                                                               #
#   port_num              :  (Mandatory) Port number on which frame is to send. #
#                                        (e.g., 0/1)                            #
#                                                                               #
#   dest_mac              :  (Mandatory) Destination MAC dddress.               #
#                                        (e.g., 01:80:C2:00:00:20)              #
#                                                                               #
#   src_mac               :  (Mandatory) Source MAC address.                    #
#                                        (e.g., 00:80:C2:00:00:21)              #
#                                                                               #
#   eth_type              :  (Optional)  EtherType (0800 or 88F7)               #
#                                        (Default : 0800)                       #
#                                                                               #
#   vlan_id               :  (Optional)  Vlan ID                                #
#                                        (Default : 0)                          #
#                                                                               #
#   vlan_priority         :  (Optional)  Vlan Priority                          #
#                                        (Default : 0)                          #
#                                                                               #
#   dest_ip               :  (Optional)  Destination IP address                 #
#                                        (Default : 224.0.1.129)                #
#                                                                               #
#   src_ip                :  (Optional)  Source IP address                      #
#                                        (Default : 0.0.0.0)                    #
#                                                                               #
#   ip_checksum           :  (Optional)  IP checksum                            #
#                                        (Default : 0)                          #
#                                                                               #
#   dest_port             :  (Optional)  UDP destination port number            #
#                                        (Default : 319)                        #
#                                                                               #
#   src_port              :  (Optional)  UDP source port number                 #
#                                        (Default : 319)                        #
#                                                                               #
#   udp_length            :  (Optional)  UDP length                             #
#                                        (Default : 0)                          #
#                                                                               #
#   udp_checksum          :  (Optional)  UDP checksum                           #
#                                        (Default : 0)                          #
#                                                                               #
#   major_sdo_id          :  (Optional)  Major sdo id                           #
#                                        (Default : 0)                          #
#                                                                               #
#   message_type          :  (Optional)  Message Type                           #
#                                        (Default : 0)                          #
#                                                                               #
#   minor_version         :  (Optional)  PTP version number                     #
#                                        (Default : 2)                          #
#                                                                               #
#   version_ptp           :  (Optional)  PTP minor version number               #
#                                        (Default : 2)                          #
#                                                                               #
#   message_length        :  (Optional)  PTP Sync message length                #
#                                        (Default : 44)                         #
#                                                                               #
#   domain_number         :  (Optional)  Domain number                          #
#                                        (Default : 0)                          #
#                                                                               #
#   minor_sdo_id          :  (Optional)  Minor Sdo id                           #
#                                        (Default : 0)                          #
#                                                                               #
#   alternate_master_flag :  (Optional)  Alternate Master flag                  #
#                                        (Default : 0)                          #
#                                                                               #
#   two_step_flag         :  (Optional)  Two Step Flag                          #
#                                        (Default : 0)                          #
#                                                                               #
#   unicast_flag          :  (Optional)  Unicast Flag                           #
#                                        (Default : 0)                          #
#                                                                               #
#   profile_specific1     :  (Optional)  Profile specific1 Flag                 #
#                                        (Default : 0)                          #
#                                                                               #
#   profile_specific2     :  (Optional)  Profile specific2 Flag                 #
#                                        (Default : 0)                          #
#                                                                               #
#   secure                :  (Optional)  Secure Flag                            #
#                                        (Default : 0)                          #
#                                                                               #
#   leap61                :  (Optional)  Leap61 Flag                            #
#                                        (Default : 0)                          #
#                                                                               #
#   leap59                :  (Optional)  Leap59 Flag                            #
#                                        (Default : 0)                          #
#                                                                               #
#   current_utc_offset_valid:  (Optional)Current UTC Offset Flag                #
#                                        (Default : 0)                          #
#                                                                               #
#   ptp_timescale         :  (Optional)  PTP TimeSclae Flag                     #
#                                        (Default : 0)                          #
#                                                                               #
#   time_tracaeble        :  (Optional)  Time Traceable Flag                    #
#                                        (Default : 0)                          #
#                                                                               #
#   freq_tracaeble        :  (Optional)  Frequency Traceable Flag               #
#                                        (Default : 0)                          #
#                                                                               #
#   sync_uncertain        :  (Optional)  Synchronization uncertain Flag         #
#                                        (Default : 0)                          #
#                                                                               #
#   correction_field      :  (Optional)  Correction Field value                 #
#                                        (Default : 0)                          #
#                                                                               #
#   message_type_specific :  (Optional)  Message type                           #
#                                        (Default : 0)                          #
#                                                                               #
#   src_port_number       :  (Optional)  Source port number                     #
#                                        (Default : 0)                          #
#                                                                               #
#   src_clock_identity    :  (Optional)  Source Clock identity                  #
#                                        (Default : 0)                          #
#                                                                               #
#   sequence_id           :  (Optional)  Sequence ID                            #
#                                        (Default : 0)                          #
#                                                                               #
#   control_field         :  (Optional)  Control field                          #
#                                        (Default : 00)                         #
#                                                                               #
#   log_message_interval  :  (Optional)  Log message interval                   #
#                                        (Default : 0)                          #
#                                                                               #
#   origin_timestamp_sec  :  (Optional)  Origin timestamp in seconds            #
#                                        (Default : 0)                          #
#                                                                               #
#   origin_timestamp_ns   :  (Optional)  Origin timestamp in nanoseconds        #
#                                        (Default : 0)                          #
#                                                                               #
#   count                 :  (Optional)  Frame count                            #
#                                        (Default : 1)                          #
#                                                                               #
#   interval              :  (Optional)  Frame interval (in seconds)            #
#                                        (Default : 1)                          #
#                                                                               #
#  OUTPUT PARAMETERS      :  NONE                                               #
#                                                                               #
#  RETURNS                :  1 on success (If PTP HA delay request is sent with #
#                                          given parameters)                    #
#                                                                               #
#                                                                               #
#                         :  0 on success (If PTP HA delay req could not be     #
#                                          sent with the given parameters)      #
#                                                                               #
#  DEFINITION             : This function is used to construct and send PTP     #
#                           HA delay request message.                           #
#                                                                               #
#  USAGE                  :                                                     #
#                                                                               #
#       pltLib::send_ptp_ha_delay_req\                                          #
#                   -session_id                        $session_id\             #
#                   -port_num                          $port_num\               #
#                   -dest_mac                          $dest_mac\               #
#                   -src_mac                           $src_mac\                #
#                   -eth_type                          $eth_type\               #
#                   -vlan_id                           $vlan_id\                #
#                   -vlan_priority                     $vlan_priority\          #
#                   -dest_ip                           $dest_ip\                #
#                   -src_ip                            $src_ip\                 #
#                   -ip_checksum                       $ip_checksum\            #
#                   -dest_port                         $dest_port\              #
#                   -src_port                          $src_port\               #
#                   -udp_length                        $udp_length\             #
#                   -udp_checksum                      $udp_checksum\           #
#                   -major_sdo_id                      $major_sdo_id\           #
#                   -message_type                      $message_type\           #
#                   -minor_version                     $minor_version\          #
#                   -version_ptp                       $version_ptp\            #
#                   -message_length                    $message_length\         #
#                   -domain_number                     $domain_number\          #
#                   -minor_sdo_id                      $minor_sdo_id\           #
#                   -alternate_master_flag             $alternate_master_flag\  #
#                   -two_step_flag                     $two_step_flag\          #
#                   -unicast_flag                      $unicast_flag\           #
#                   -profile_specific1                 $profile_specific1\      #
#                   -profile_specific2                 $profile_specific2\      #
#                   -secure                            $secure\                 #
#                   -leap61                            $leap61\                 #
#                   -leap59                            $leap59\                 #
#                   -current_utc_offset_valid         $current_utc_offset_valid\#
#                   -ptp_timescale                     $ptp_timescale\          #
#                   -time_tracaeble                    $time_traceable\         #
#                   -freq_tracaeble                    $freq_traceable\         #
#                   -sync_uncertain                    $sync_uncertain\         #
#                   -correction_field                  $correction_field\       #
#                   -message_type_specific             $message_type_specific\  #
#                   -src_port_number                   $src_port_number\        #
#                   -src_clock_identity                $src_clock_identity\     #
#                   -sequence_id                       $sequence_id\            #
#                   -control_field                     $control_field\          #
#                   -log_message_interval              $log_message_interval\   #
#                   -origin_timestamp_sec              $origin_timestamp_sec\   #
#                   -origin_timestamp_ns               $origin_timestamp_ns\    #
#                   -count                             $count\                  #
#                   -interval                          $interval                #
#                                                                               #
#################################################################################

proc pltLib::send_ptp_ha_delay_req {args} {

    array set param $args

    #INPUT PARAMETERS

    #Session identifier
    if {[info exists param(-session_id)]} {

        set session_id $param(-session_id)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_delay_req: Missing Argument -> session_id"
        return 0
    }

    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_delay_req: Missing Argument -> port_num"
        return 0
    }

    #ETHERNET HEADER OPTIONS

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_delay_req: Missing Argument -> dest_mac"
        return 0
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_delay_req: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
    } else {

        set eth_type 0800
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
    } else {

        set vlan_id 0
    }

    #Vlan Priority
    if {[info exists param(-vlan_priority)]} {

        set vlan_priority $param(-vlan_priority)
    } else {

        set vlan_priority 0
    }

    #IP HEADER OPTIONS

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
    } else {

        set dest_ip 224.0.1.129
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
    } else {

        set src_ip 0.0.0.0
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
    } else {

        set ip_checksum 0
    }

    #UDP HEADER OPTIONS

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
    } else {

        set dest_port 319
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
    } else {

        set src_port 319
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
    } else {

        set udp_length 0
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
    } else {

        set udp_checksum 0
    }

    #PTP HEADER OPTIONS

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
    } else {

        set major_sdo_id 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
    } else {

        set message_type 0
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
    } else {

        set minor_version 2
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
    } else {

        set message_length 44
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
    } else {

        set domain_number 0
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
    } else {

        set minor_sdo_id 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
    } else {

        set correction_field 0
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
    } else {

        set message_type_specific 0
    }

    #Source port number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
    } else {

        set src_port_number 0
    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
    } else {

        set src_clock_identity 0
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
    } else {

        set sequence_id 0
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
    } else {

        set control_field 00
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
    } else {

        set log_message_interval 0
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
    } else {

        set unicast_flag 0
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
    } else {

        set profile_specific2 0
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
    } else {

        set secure 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
    } else {

        set freq_traceable 0
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
    } else {

        set sync_uncertain 0
    }

    #PTP SYNC MESSAGE OPTIONS
    #Origin timestamp in seconds
    if {[info exists param(-origin_timestamp_sec)]} {

        set origin_timestamp_sec $param(-origin_timestamp_sec)
    } else {

        set origin_timestamp_sec 0
    }

    #Origin timestamp in nanoseconds
    if {[info exists param(-origin_timestamp_ns)]} {

        set origin_timestamp_ns $param(-origin_timestamp_ns)
    } else {

        set origin_timestamp_ns 0
    }

    #MISCELLANEOUS
    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
    } else {

        set interval 1
    }

    #Interval in millisec
    set interval [expr int($interval*1000)]

    #CONSTRUCT PTP HA SYNC PACKET DUMP
    if {![pbLib::encode_ptp_ha_pkt\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -major_sdo_id                      $major_sdo_id\
                -message_type                      $message_type\
                -minor_version                     $minor_version\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -minor_sdo_id                      $minor_sdo_id\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -secure                            $secure\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_tracaeble                    $time_traceable\
                -freq_tracaeble                    $freq_traceable\
                -sync_uncertain                    $sync_uncertain\
                -correction_field                  $correction_field\
                -message_type_specific             $message_type_specific\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -origin_timestamp_sec              $origin_timestamp_sec\
                -origin_timestamp_ns               $origin_timestamp_ns\
                -hexdump                           hexdump]} {

        return 0
    }

    pltLib::send_periodic\
                     -session_id     $session_id\
                     -port_num       $port_num\
                     -data           $hexdump\
                     -instance       3\
                     -interval       $interval\
                     -count          $count\
                     -handler_id     "PTP_HA"

    return 1

}

#4.
########################################################################################
#                                                                                      #
#  PROCEDURE NAME                : pltLib::send_ptp_ha_delay_resp                      #
#                                                                                      #
#  INPUT PARAMETERS              :                                                     #
#                                                                                      #
#   session_id                   :  (Mandatory) Session identifier                     #
#                                                                                      #
#   port_num                     :  (Mandatory) Port number on which frame is to send. #
#                                               (e.g., 0/1)                            #
#                                                                                      #
#   dest_mac                     :  (Mandatory) Destination MAC dddress.               #
#                                               (e.g., 01:80:C2:00:00:20)              #
#                                                                                      #
#   src_mac                      :  (Mandatory) Source MAC address.                    #
#                                               (e.g., 00:80:C2:00:00:21)              #
#                                                                                      #
#   eth_type                     :  (Optional)  EtherType (0800 or 88F7)               #
#                                               (Default : 0800)                       #
#                                                                                      #
#   vlan_id                      :  (Optional)  Vlan ID                                #
#                                               (Default : 0)                          #
#                                                                                      #
#   vlan_priority                :  (Optional)  Vlan Priority                          #
#                                               (Default : 0)                          #
#                                                                                      #
#   dest_ip                      :  (Optional)  Destination IP address                 #
#                                               (Default : 224.0.1.129)                #
#                                                                                      #
#   src_ip                       :  (Optional)  Source IP address                      #
#                                               (Default : 0.0.0.0)                    #
#                                                                                      #
#   ip_checksum                  :  (Optional)  IP checksum                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   dest_port                    :  (Optional)  UDP destination port number            #
#                                               (Default : 319)                        #
#                                                                                      #
#   src_port                     :  (Optional)  UDP source port number                 #
#                                               (Default : 319)                        #
#                                                                                      #
#   udp_length                   :  (Optional)  UDP length                             #
#                                               (Default : 0)                          #
#                                                                                      #
#   udp_checksum                 :  (Optional)  UDP checksum                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   major_sdo_id                 :  (Optional)  Major sdo id                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   message_type                 :  (Optional)  Message Type                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   minor_version                :  (Optional)  PTP version number                     #
#                                               (Default : 2)                          #
#                                                                                      #
#   version_ptp                  :  (Optional)  PTP minor version number               #
#                                               (Default : 2)                          #
#                                                                                      #
#   message_length               :  (Optional)  PTP Sync message length                #
#                                               (Default : 44)                         #
#                                                                                      #
#   domain_number                :  (Optional)  Domain number                          #
#                                               (Default : 0)                          #
#                                                                                      #
#   minor_sdo_id                 :  (Optional)  Minor Sdo id                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   alternate_master_flag        :  (Optional)  Alternate Master flag                  #
#                                               (Default : 0)                          #
#                                                                                      #
#   two_step_flag                :  (Optional)  Two Step Flag                          #
#                                               (Default : 0)                          #
#                                                                                      #
#   unicast_flag                 :  (Optional)  Unicast Flag                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   profile_specific1            :  (Optional)  Profile specific1 Flag                 #
#                                               (Default : 0)                          #
#                                                                                      #
#   profile_specific2            :  (Optional)  Profile specific2 Flag                 #
#                                               (Default : 0)                          #
#                                                                                      #
#   secure                       :  (Optional)  Secure Flag                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   leap61                       :  (Optional)  Leap61 Flag                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   leap59                       :  (Optional)  Leap59 Flag                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   current_utc_offset_valid     :  (Optional)  Current UTC Offset Flag                #
#                                               (Default : 0)                          #
#                                                                                      #
#   ptp_timescale                :  (Optional)  PTP TimeSclae Flag                     #
#                                               (Default : 0)                          #
#                                                                                      #
#   time_tracaeble               :  (Optional)  Time Traceable Flag                    #
#                                               (Default : 0)                          #
#                                                                                      #
#   freq_tracaeble               :  (Optional)  Frequency Traceable Flag               #
#                                               (Default : 0)                          #
#                                                                                      #
#   sync_uncertain               :  (Optional)  Synchronization uncertain Flag         #
#                                               (Default : 0)                          #
#                                                                                      #
#   correction_field             :  (Optional)  Correction Field value                 #
#                                               (Default : 0)                          #
#                                                                                      #
#   message_type_specific        :  (Optional)  Message type                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   src_port_number              :  (Optional)  Source port number                     #
#                                               (Default : 0)                          #
#                                                                                      #
#   src_clock_identity           :  (Optional)  Source Clock identity                  #
#                                               (Default : 0)                          #
#                                                                                      #
#   sequence_id                  :  (Optional)  Sequence ID                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   control_field                :  (Optional)  Control field                          #
#                                               (Default : 00)                         #
#                                                                                      #
#   log_message_interval         :  (Optional)  Log message interval                   #
#                                               (Default : 0)                          #
#                                                                                      #
#   receive_timestamp_sec         :  (Optional) Receive timestamp in seconds           #
#                                               (Default : 0)                          #
#                                                                                      #
#   receive_timestamp_ns          :  (Optional) Receive timestamp in nanoseconds       #
#                                               (Default : 0)                          #
#                                                                                      #
#   requesting_port_number        : (Optional)  Requesting Port Number                 #
#                                               (Default : 0)                          #
#                                                                                      #
#   requesting_clock_identity     : (Optional)  Requesting Clock Identity              #
#                                               (Default : 0)                          #
#                                                                                      #
#   count                         :  (Optional)  Frame count                           #
#                                                (Default : 1)                         #
#                                                                                      #
#   interval                      :  (Optional)  Frame interval (in seconds)           #
#                                                (Default : 1)                         #
#                                                                                      #
#  OUTPUT PARAMETERS              :  NONE                                              #
#                                                                                      #
#  RETURNS                        :  1 on success (If PTP HA delay response is sent    #
#                                                 with the given parameters)           #
#                                                                                      #
#                                                                                      #
#                                 :  0 on success (If PTP HA delay resp could not be   #
#                                                  sent with the given parameters)     #
#                                                                                      #
#  DEFINITION                     : This function is used to construct and send PTP    #
#                                   HA delay request message.                          #
#                                                                                      #
#  USAGE                          :                                                    #
#                                                                                      #
#       pltLib::send_ptp_ha_delay_resp\                                                #
#                   -session_id                        $session_id\                    #
#                   -port_num                          $port_num\                      #
#                   -dest_mac                          $dest_mac\                      #
#                   -src_mac                           $src_mac\                       #
#                   -eth_type                          $eth_type\                      #
#                   -vlan_id                           $vlan_id\                       #
#                   -vlan_priority                     $vlan_priority\                 #
#                   -dest_ip                           $dest_ip\                       #
#                   -src_ip                            $src_ip\                        #
#                   -ip_checksum                       $ip_checksum\                   #
#                   -dest_port                         $dest_port\                     #
#                   -src_port                          $src_port\                      #
#                   -udp_length                        $udp_length\                    #
#                   -udp_checksum                      $udp_checksum\                  #
#                   -major_sdo_id                      $major_sdo_id\                  #
#                   -message_type                      $message_type\                  #
#                   -minor_version                     $minor_version\                 #
#                   -version_ptp                       $version_ptp\                   #
#                   -message_length                    $message_length\                #
#                   -domain_number                     $domain_number\                 #
#                   -minor_sdo_id                      $minor_sdo_id\                  #
#                   -alternate_master_flag             $alternate_master_flag\         #
#                   -two_step_flag                     $two_step_flag\                 #
#                   -unicast_flag                      $unicast_flag\                  #
#                   -profile_specific1                 $profile_specific1\             #
#                   -profile_specific2                 $profile_specific2\             #
#                   -secure                            $secure\                        #
#                   -leap61                            $leap61\                        #
#                   -leap59                            $leap59\                        #
#                   -current_utc_offset_valid         $current_utc_offset_valid\       #
#                   -ptp_timescale                     $ptp_timescale\                 #
#                   -time_tracaeble                    $time_traceable\                #
#                   -freq_tracaeble                    $freq_traceable\                #
#                   -sync_uncertain                    $sync_uncertain\                #
#                   -correction_field                  $correction_field\              #
#                   -message_type_specific             $message_type_specific\         #
#                   -src_port_number                   $src_port_number\               #
#                   -src_clock_identity                $src_clock_identity\            #
#                   -sequence_id                       $sequence_id\                   #
#                   -control_field                     $control_field\                 #
#                   -log_message_interval              $log_message_interval\          #
#                   -receive_timestamp_sec             $receive_timestamp_sec\         #
#                   -receive_timestamp_ns              $receive_timestamp_ns\          #
#                   -requesting_port_number            $requesting_port_number\        #
#                   -requesting_clock_identity         $requesting_clock_identity\     #
#                   -count                             $count\                         #
#                   -interval                          $interval                       #
#                                                                                      #
########################################################################################

proc pltLib::send_ptp_ha_delay_resp {args} {

    array set param $args

    #INPUT PARAMETERS

    #Session identifier
    if {[info exists param(-session_id)]} {

        set session_id $param(-session_id)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_delay_resp: Missing Argument -> session_id"
        return 0
    }

    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_delay_resp: Missing Argument -> port_num"
        return 0
    }

    #ETHERNET HEADER OPTIONS

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_delay_resp: Missing Argument -> dest_mac"
        return 0
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_delay_resp: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
    } else {

        set eth_type 0800
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
    } else {

        set vlan_id 0
    }

    #Vlan Priority
    if {[info exists param(-vlan_priority)]} {

        set vlan_priority $param(-vlan_priority)
    } else {

        set vlan_priority 0
    }

    #IP HEADER OPTIONS

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
    } else {

        set dest_ip 224.0.1.129
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
    } else {

        set src_ip 0.0.0.0
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
    } else {

        set ip_checksum 0
    }

    #UDP HEADER OPTIONS

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
    } else {

        set dest_port 319
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
    } else {

        set src_port 319
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
    } else {

        set udp_length 0
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
    } else {

        set udp_checksum 0
    }

    #PTP HEADER OPTIONS

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
    } else {

        set major_sdo_id 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
    } else {

        set message_type 0
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
    } else {

        set minor_version 2
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
    } else {

        set message_length 44
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
    } else {

        set domain_number 0
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
    } else {

        set minor_sdo_id 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
    } else {

        set correction_field 0
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
    } else {

        set message_type_specific 0
    }

    #Source port number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
    } else {

        set src_port_number 0
    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
    } else {

        set src_clock_identity 0
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
    } else {

        set sequence_id 0
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
    } else {

        set control_field 00
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
    } else {

        set log_message_interval 0
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
    } else {

        set unicast_flag 0
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
    } else {

        set profile_specific2 0
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
    } else {

        set secure 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
    } else {

        set freq_traceable 0
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
    } else {

        set sync_uncertain 0
    }

    #Receive timestamp in seconds
    if {[info exists param(-receive_timestamp_sec)]} {

        set receive_timestamp_sec $param(-receive_timestamp_sec)
    } else {

        set receive_timestamp_sec 0
    }

    #Receive timestamp in nanoseconds
    if {[info exists param(-receive_timestamp_ns)]} {

        set receive_timestamp_ns $param(-receive_timestamp_ns)
    } else {

        set receive_timestamp_ns 0
    }

    if {[info exists param(-requesting_port_number)]} {

        set requesting_port_number $param(-requesting_port_number)
    } else {

        set requesting_port_number 0
    }

    if {[info exists param(-requesting_clock_identity)]} {

        set requesting_clock_identity $param(-requesting_clock_identity)
    } else {

        set requesting_port_number 0
    }

    #MISCELLANEOUS
    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
    } else {

        set interval 1
    }

    #Interval in millisec
    set interval [expr int($interval*1000)]

    #CONSTRUCT PTP HA SYNC PACKET DUMP
    if {![pbLib::encode_ptp_ha_pkt\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -major_sdo_id                      $major_sdo_id\
                -message_type                      $message_type\
                -minor_version                     $minor_version\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -minor_sdo_id                      $minor_sdo_id\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -secure                            $secure\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_tracaeble                    $time_traceable\
                -freq_tracaeble                    $freq_traceable\
                -sync_uncertain                    $sync_uncertain\
                -correction_field                  $correction_field\
                -message_type_specific             $message_type_specific\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -receive_timestamp_sec             $receive_timestamp_sec\
                -receive_timestamp_ns              $receive_timestamp_ns\
                -requesting_port_number            $requesting_port_number\
                -requesting_clock_identity         $requesting_clock_identity\
                -hexdump                           hexdump]} {

        return 0
    }

    pltLib::send_periodic\
                     -session_id     $session_id\
                     -port_num       $port_num\
                     -data           $hexdump\
                     -instance       4\
                     -interval       $interval\
                     -count          $count\
                     -handler_id     "PTP_HA"

    return 1

}

#5.
########################################################################################
#                                                                                      #
#  PROCEDURE NAME                : pltLib::send_ptp_ha_followup                        #
#                                                                                      #
#  INPUT PARAMETERS              :                                                     #
#                                                                                      #
#   session_id                   :  (Mandatory) Session identifier                     #
#                                                                                      #
#   port_num                     :  (Mandatory) Port number on which frame is to send. #
#                                               (e.g., 0/1)                            #
#                                                                                      #
#   dest_mac                     :  (Mandatory) Destination MAC dddress.               #
#                                               (e.g., 01:80:C2:00:00:20)              #
#                                                                                      #
#   src_mac                      :  (Mandatory) Source MAC address.                    #
#                                               (e.g., 00:80:C2:00:00:21)              #
#                                                                                      #
#   eth_type                     :  (Optional)  EtherType (0800 or 88F7)               #
#                                               (Default : 0800)                       #
#                                                                                      #
#   vlan_id                      :  (Optional)  Vlan ID                                #
#                                               (Default : 0)                          #
#                                                                                      #
#   vlan_priority                :  (Optional)  Vlan Priority                          #
#                                               (Default : 0)                          #
#                                                                                      #
#   dest_ip                      :  (Optional)  Destination IP address                 #
#                                               (Default : 224.0.1.129)                #
#                                                                                      #
#   src_ip                       :  (Optional)  Source IP address                      #
#                                               (Default : 0.0.0.0)                    #
#                                                                                      #
#   ip_checksum                  :  (Optional)  IP checksum                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   dest_port                    :  (Optional)  UDP destination port number            #
#                                               (Default : 319)                        #
#                                                                                      #
#   src_port                     :  (Optional)  UDP source port number                 #
#                                               (Default : 319)                        #
#                                                                                      #
#   udp_length                   :  (Optional)  UDP length                             #
#                                               (Default : 0)                          #
#                                                                                      #
#   udp_checksum                 :  (Optional)  UDP checksum                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   major_sdo_id                 :  (Optional)  Major sdo id                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   message_type                 :  (Optional)  Message Type                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   minor_version                :  (Optional)  PTP version number                     #
#                                               (Default : 2)                          #
#                                                                                      #
#   version_ptp                  :  (Optional)  PTP minor version number               #
#                                               (Default : 2)                          #
#                                                                                      #
#   message_length               :  (Optional)  PTP Sync message length                #
#                                               (Default : 44)                         #
#                                                                                      #
#   domain_number                :  (Optional)  Domain number                          #
#                                               (Default : 0)                          #
#                                                                                      #
#   minor_sdo_id                 :  (Optional)  Minor Sdo id                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   alternate_master_flag        :  (Optional)  Alternate Master flag                  #
#                                               (Default : 0)                          #
#                                                                                      #
#   two_step_flag                :  (Optional)  Two Step Flag                          #
#                                               (Default : 0)                          #
#                                                                                      #
#   unicast_flag                 :  (Optional)  Unicast Flag                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   profile_specific1            :  (Optional)  Profile specific1 Flag                 #
#                                               (Default : 0)                          #
#                                                                                      #
#   profile_specific2            :  (Optional)  Profile specific2 Flag                 #
#                                               (Default : 0)                          #
#                                                                                      #
#   secure                       :  (Optional)  Secure Flag                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   leap61                       :  (Optional)  Leap61 Flag                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   leap59                       :  (Optional)  Leap59 Flag                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   current_utc_offset_valid     :  (Optional)  Current UTC Offset Flag                #
#                                               (Default : 0)                          #
#                                                                                      #
#   ptp_timescale                :  (Optional)  PTP TimeSclae Flag                     #
#                                               (Default : 0)                          #
#                                                                                      #
#   time_tracaeble               :  (Optional)  Time Traceable Flag                    #
#                                               (Default : 0)                          #
#                                                                                      #
#   freq_tracaeble               :  (Optional)  Frequency Traceable Flag               #
#                                               (Default : 0)                          #
#                                                                                      #
#   sync_uncertain               :  (Optional)  Synchronization uncertain Flag         #
#                                               (Default : 0)                          #
#                                                                                      #
#   correction_field             :  (Optional)  Correction Field value                 #
#                                               (Default : 0)                          #
#                                                                                      #
#   message_type_specific        :  (Optional)  Message type                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   src_port_number              :  (Optional)  Source port number                     #
#                                               (Default : 0)                          #
#                                                                                      #
#   src_clock_identity           :  (Optional)  Source Clock identity                  #
#                                               (Default : 0)                          #
#                                                                                      #
#   sequence_id                  :  (Optional)  Sequence ID                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   control_field                :  (Optional)  Control field                          #
#                                               (Default : 00)                         #
#                                                                                      #
#   log_message_interval         :  (Optional)  Log message interval                   #
#                                               (Default : 0)                          #
#                                                                                      #
#   precise_origin_timestamp_sec  : (Optional)  Precise Origin Timestamp Sec           #
#                                               (Default : 0)                          #
#                                                                                      #
#   precise_origin_timestamp_ns   : (Optional)  Precise Origin Timestamp Ns            #
#                                               (Default : 0)                          #
#                                                                                      #
#   count                         : (Optional)  Frame count                            #
#                                               (Default : 1)                          #
#                                                                                      #
#   interval                      : (Optional)  Frame interval (in seconds)            #
#                                               (Default : 1)                          #
#                                                                                      #
#  OUTPUT PARAMETERS              :  NONE                                              #
#                                                                                      #
#  RETURNS                        :  1 on success (If PTP HA delay response is sent    #
#                                                 with the given parameters)           #
#                                                                                      #
#                                                                                      #
#                                 :  0 on success (If PTP HA delay resp could not be   #
#                                                  sent with the given parameters)     #
#                                                                                      #
#  DEFINITION                     : This function is used to construct and send PTP    #
#                                   HA delay request message.                          #
#                                                                                      #
#  USAGE                          :                                                    #
#                                                                                      #
#       pltLib::send_ptp_ha_followup\                                                  #
#                   -session_id                        $session_id\                    #
#                   -port_num                          $port_num\                      #
#                   -dest_mac                          $dest_mac\                      #
#                   -src_mac                           $src_mac\                       #
#                   -eth_type                          $eth_type\                      #
#                   -vlan_id                           $vlan_id\                       #
#                   -vlan_priority                     $vlan_priority\                 #
#                   -dest_ip                           $dest_ip\                       #
#                   -src_ip                            $src_ip\                        #
#                   -ip_checksum                       $ip_checksum\                   #
#                   -dest_port                         $dest_port\                     #
#                   -src_port                          $src_port\                      #
#                   -udp_length                        $udp_length\                    #
#                   -udp_checksum                      $udp_checksum\                  #
#                   -major_sdo_id                      $major_sdo_id\                  #
#                   -message_type                      $message_type\                  #
#                   -minor_version                     $minor_version\                 #
#                   -version_ptp                       $version_ptp\                   #
#                   -message_length                    $message_length\                #
#                   -domain_number                     $domain_number\                 #
#                   -minor_sdo_id                      $minor_sdo_id\                  #
#                   -alternate_master_flag             $alternate_master_flag\         #
#                   -two_step_flag                     $two_step_flag\                 #
#                   -unicast_flag                      $unicast_flag\                  #
#                   -profile_specific1                 $profile_specific1\             #
#                   -profile_specific2                 $profile_specific2\             #
#                   -secure                            $secure\                        #
#                   -leap61                            $leap61\                        #
#                   -leap59                            $leap59\                        #
#                   -current_utc_offset_valid         $current_utc_offset_valid\       #
#                   -ptp_timescale                     $ptp_timescale\                 #
#                   -time_tracaeble                    $time_traceable\                #
#                   -freq_tracaeble                    $freq_traceable\                #
#                   -sync_uncertain                    $sync_uncertain\                #
#                   -correction_field                  $correction_field\              #
#                   -message_type_specific             $message_type_specific\         #
#                   -src_port_number                   $src_port_number\               #
#                   -src_clock_identity                $src_clock_identity\            #
#                   -sequence_id                       $sequence_id\                   #
#                   -control_field                     $control_field\                 #
#                   -log_message_interval              $log_message_interval\          #
#                   -precise_origin_timestamp_sec      $precise_origin_timestamp_sec\  #
#                   -precise_origin_timestamp_ns       $precise_origin_timestamp_ns\   #
#                   -count                             $count\                         #
#                   -interval                          $interval                       #
#                                                                                      #
########################################################################################

proc pltLib::send_ptp_ha_followup {args} {

    array set param $args

    #INPUT PARAMETERS

    #Session identifier
    if {[info exists param(-session_id)]} {

        set session_id $param(-session_id)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_followup: Missing Argument -> session_id"
        return 0
    }

    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_followup: Missing Argument -> port_num"
        return 0
    }

    #ETHERNET HEADER OPTIONS

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_followup: Missing Argument -> dest_mac"
        return 0
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_followup: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
    } else {

        set eth_type 0800
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
    } else {

        set vlan_id 0
    }

    #Vlan Priority
    if {[info exists param(-vlan_priority)]} {

        set vlan_priority $param(-vlan_priority)
    } else {

        set vlan_priority 0
    }

    #IP HEADER OPTIONS

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
    } else {

        set dest_ip 224.0.1.129
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
    } else {

        set src_ip 0.0.0.0
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
    } else {

        set ip_checksum 0
    }

    #UDP HEADER OPTIONS

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
    } else {

        set dest_port 319
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
    } else {

        set src_port 319
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
    } else {

        set udp_length 0
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
    } else {

        set udp_checksum 0
    }

    #PTP HEADER OPTIONS

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
    } else {

        set major_sdo_id 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
    } else {

        set message_type 0
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
    } else {

        set minor_version 2
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
    } else {

        set message_length 44
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
    } else {

        set domain_number 0
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
    } else {

        set minor_sdo_id 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
    } else {

        set correction_field 0
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
    } else {

        set message_type_specific 0
    }

    #Source port number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
    } else {

        set src_port_number 0
    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
    } else {

        set src_clock_identity 0
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
    } else {

        set sequence_id 0
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
    } else {

        set control_field 00
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
    } else {

        set log_message_interval 0
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
    } else {

        set unicast_flag 0
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
    } else {

        set profile_specific2 0
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
    } else {

        set secure 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
    } else {

        set freq_traceable 0
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
    } else {

        set sync_uncertain 0
    }

    #Precise Origin timestamp in seconds
    if {[info exists param(-precise_origin_timestamp_sec)]} {

        set precise_origin_timestamp_sec $param(-precise_origin_timestamp_sec)
    } else {

        set precise_origin_timestamp_sec 0
    }

    #Precise Origin timestamp in nanoseconds
    if {[info exists param(-precise_origin_timestamp_ns)]} {

        set precise_origin_timestamp_ns $param(-precise_origin_timestamp_ns)
    } else {

        set precise_origin_timestamp_ns 0
    }

    #MISCELLANEOUS
    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
    } else {

        set interval 1
    }

    #Interval in millisec
    set interval [expr int($interval*1000)]

    #CONSTRUCT PTP HA SYNC PACKET DUMP
    if {![pbLib::encode_ptp_ha_pkt\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -major_sdo_id                      $major_sdo_id\
                -message_type                      $message_type\
                -minor_version                     $minor_version\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -minor_sdo_id                      $minor_sdo_id\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -secure                            $secure\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_tracaeble                    $time_traceable\
                -freq_tracaeble                    $freq_traceable\
                -sync_uncertain                    $sync_uncertain\
                -correction_field                  $correction_field\
                -message_type_specific             $message_type_specific\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -precise_origin_timestamp_sec      $precise_origin_timestamp_sec\
                -precise_origin_timestamp_ns       $precise_origin_timestamp_ns\
                -hexdump                           hexdump]} {

        return 0
    }

    pltLib::send_periodic\
                     -session_id     $session_id\
                     -port_num       $port_num\
                     -data           $hexdump\
                     -instance       5\
                     -interval       $interval\
                     -count          $count\
                     -handler_id     "PTP_HA"

    return 1

}

#6.
#################################################################################
#                                                                               #
#  PROCEDURE NAME         : pltLib::send_ptp_ha_pdelay_req                      #
#                                                                               #
#  INPUT PARAMETERS       :                                                     #
#                                                                               #
#   session_id            :  (Mandatory) Session identifier                     #
#                                                                               #
#   port_num              :  (Mandatory) Port number on which frame is to send. #
#                                        (e.g., 0/1)                            #
#                                                                               #
#   dest_mac              :  (Mandatory) Destination MAC dddress.               #
#                                        (e.g., 01:80:C2:00:00:20)              #
#                                                                               #
#   src_mac               :  (Mandatory) Source MAC address.                    #
#                                        (e.g., 00:80:C2:00:00:21)              #
#                                                                               #
#   eth_type              :  (Optional)  EtherType (0800 or 88F7)               #
#                                        (Default : 0800)                       #
#                                                                               #
#   vlan_id               :  (Optional)  Vlan ID                                #
#                                        (Default : 0)                          #
#                                                                               #
#   vlan_priority         :  (Optional)  Vlan Priority                          #
#                                        (Default : 0)                          #
#                                                                               #
#   dest_ip               :  (Optional)  Destination IP address                 #
#                                        (Default : 224.0.1.129)                #
#                                                                               #
#   src_ip                :  (Optional)  Source IP address                      #
#                                        (Default : 0.0.0.0)                    #
#                                                                               #
#   ip_checksum           :  (Optional)  IP checksum                            #
#                                        (Default : 0)                          #
#                                                                               #
#   dest_port             :  (Optional)  UDP destination port number            #
#                                        (Default : 319)                        #
#                                                                               #
#   src_port              :  (Optional)  UDP source port number                 #
#                                        (Default : 319)                        #
#                                                                               #
#   udp_length            :  (Optional)  UDP length                             #
#                                        (Default : 0)                          #
#                                                                               #
#   udp_checksum          :  (Optional)  UDP checksum                           #
#                                        (Default : 0)                          #
#                                                                               #
#   major_sdo_id          :  (Optional)  Major sdo id                           #
#                                        (Default : 0)                          #
#                                                                               #
#   message_type          :  (Optional)  Message Type                           #
#                                        (Default : 0)                          #
#                                                                               #
#   minor_version         :  (Optional)  PTP version number                     #
#                                        (Default : 2)                          #
#                                                                               #
#   version_ptp           :  (Optional)  PTP minor version number               #
#                                        (Default : 2)                          #
#                                                                               #
#   message_length        :  (Optional)  PTP Sync message length                #
#                                        (Default : 44)                         #
#                                                                               #
#   domain_number         :  (Optional)  Domain number                          #
#                                        (Default : 0)                          #
#                                                                               #
#   minor_sdo_id          :  (Optional)  Minor Sdo id                           #
#                                        (Default : 0)                          #
#                                                                               #
#   alternate_master_flag :  (Optional)  Alternate Master flag                  #
#                                        (Default : 0)                          #
#                                                                               #
#   two_step_flag         :  (Optional)  Two Step Flag                          #
#                                        (Default : 0)                          #
#                                                                               #
#   unicast_flag          :  (Optional)  Unicast Flag                           #
#                                        (Default : 0)                          #
#                                                                               #
#   profile_specific1     :  (Optional)  Profile specific1 Flag                 #
#                                        (Default : 0)                          #
#                                                                               #
#   profile_specific2     :  (Optional)  Profile specific2 Flag                 #
#                                        (Default : 0)                          #
#                                                                               #
#   secure                :  (Optional)  Secure Flag                            #
#                                        (Default : 0)                          #
#                                                                               #
#   leap61                :  (Optional)  Leap61 Flag                            #
#                                        (Default : 0)                          #
#                                                                               #
#   leap59                :  (Optional)  Leap59 Flag                            #
#                                        (Default : 0)                          #
#                                                                               #
#   current_utc_offset_valid:  (Optional)Current UTC Offset Flag                #
#                                        (Default : 0)                          #
#                                                                               #
#   ptp_timescale         :  (Optional)  PTP TimeSclae Flag                     #
#                                        (Default : 0)                          #
#                                                                               #
#   time_tracaeble        :  (Optional)  Time Traceable Flag                    #
#                                        (Default : 0)                          #
#                                                                               #
#   freq_tracaeble        :  (Optional)  Frequency Traceable Flag               #
#                                        (Default : 0)                          #
#                                                                               #
#   sync_uncertain        :  (Optional)  Synchronization uncertain Flag         #
#                                        (Default : 0)                          #
#                                                                               #
#   correction_field      :  (Optional)  Correction Field value                 #
#                                        (Default : 0)                          #
#                                                                               #
#   message_type_specific :  (Optional)  Message type                           #
#                                        (Default : 0)                          #
#                                                                               #
#   src_port_number       :  (Optional)  Source port number                     #
#                                        (Default : 0)                          #
#                                                                               #
#   src_clock_identity    :  (Optional)  Source Clock identity                  #
#                                        (Default : 0)                          #
#                                                                               #
#   sequence_id           :  (Optional)  Sequence ID                            #
#                                        (Default : 0)                          #
#                                                                               #
#   control_field         :  (Optional)  Control field                          #
#                                        (Default : 00)                         #
#                                                                               #
#   log_message_interval  :  (Optional)  Log message interval                   #
#                                        (Default : 0)                          #
#                                                                               #
#   origin_timestamp_sec  :  (Optional)  Origin timestamp in seconds            #
#                                        (Default : 0)                          #
#                                                                               #
#   origin_timestamp_ns   :  (Optional)  Origin timestamp in nanoseconds        #
#                                        (Default : 0)                          #
#                                                                               #
#   count                 :  (Optional)  Frame count                            #
#                                        (Default : 1)                          #
#                                                                               #
#   interval              :  (Optional)  Frame interval (in seconds)            #
#                                        (Default : 1)                          #
#                                                                               #
#  OUTPUT PARAMETERS      :  NONE                                               #
#                                                                               #
#  RETURNS                :  1 on success (If PTP HA peer delay request is sent #
#                                          with the given parameters)           #
#                                                                               #
#                                                                               #
#                         :  0 on success (If PTP HA peer delay req could not be#
#                                          sent with the given parameters)      #
#                                                                               #
#  DEFINITION             : This function is used to construct and send PTP     #
#                           HA peer delay request message.                      #
#                                                                               #
#  USAGE                  :                                                     #
#                                                                               #
#       pltLib::send_ptp_ha_pdelay_req\                                         #
#                   -session_id                        $session_id\             #
#                   -port_num                          $port_num\               #
#                   -dest_mac                          $dest_mac\               #
#                   -src_mac                           $src_mac\                #
#                   -eth_type                          $eth_type\               #
#                   -vlan_id                           $vlan_id\                #
#                   -vlan_priority                     $vlan_priority\          #
#                   -dest_ip                           $dest_ip\                #
#                   -src_ip                            $src_ip\                 #
#                   -ip_checksum                       $ip_checksum\            #
#                   -dest_port                         $dest_port\              #
#                   -src_port                          $src_port\               #
#                   -udp_length                        $udp_length\             #
#                   -udp_checksum                      $udp_checksum\           #
#                   -major_sdo_id                      $major_sdo_id\           #
#                   -message_type                      $message_type\           #
#                   -minor_version                     $minor_version\          #
#                   -version_ptp                       $version_ptp\            #
#                   -message_length                    $message_length\         #
#                   -domain_number                     $domain_number\          #
#                   -minor_sdo_id                      $minor_sdo_id\           #
#                   -alternate_master_flag             $alternate_master_flag\  #
#                   -two_step_flag                     $two_step_flag\          #
#                   -unicast_flag                      $unicast_flag\           #
#                   -profile_specific1                 $profile_specific1\      #
#                   -profile_specific2                 $profile_specific2\      #
#                   -secure                            $secure\                 #
#                   -leap61                            $leap61\                 #
#                   -leap59                            $leap59\                 #
#                   -current_utc_offset_valid         $current_utc_offset_valid\#
#                   -ptp_timescale                     $ptp_timescale\          #
#                   -time_tracaeble                    $time_traceable\         #
#                   -freq_tracaeble                    $freq_traceable\         #
#                   -sync_uncertain                    $sync_uncertain\         #
#                   -correction_field                  $correction_field\       #
#                   -message_type_specific             $message_type_specific\  #
#                   -src_port_number                   $src_port_number\        #
#                   -src_clock_identity                $src_clock_identity\     #
#                   -sequence_id                       $sequence_id\            #
#                   -control_field                     $control_field\          #
#                   -log_message_interval              $log_message_interval\   #
#                   -origin_timestamp_sec              $origin_timestamp_sec\   #
#                   -origin_timestamp_ns               $origin_timestamp_ns\    #
#                   -count                             $count\                  #
#                   -interval                          $interval                #
#                                                                               #
#################################################################################

proc pltLib::send_ptp_ha_pdelay_req {args} {

    array set param $args

    #INPUT PARAMETERS

    #Session identifier
    if {[info exists param(-session_id)]} {

        set session_id $param(-session_id)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_pdelay_req: Missing Argument -> session_id"
        return 0
    }

    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_pdelay_req: Missing Argument -> port_num"
        return 0
    }

    #ETHERNET HEADER OPTIONS

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_pdelay_req: Missing Argument -> dest_mac"
        return 0
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_pdelay_req: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
    } else {

        set eth_type 0800
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
    } else {

        set vlan_id 0
    }

    #Vlan Priority
    if {[info exists param(-vlan_priority)]} {

        set vlan_priority $param(-vlan_priority)
    } else {

        set vlan_priority 0
    }

    #IP HEADER OPTIONS

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
    } else {

        set dest_ip 224.0.1.129
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
    } else {

        set src_ip 0.0.0.0
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
    } else {

        set ip_checksum 0
    }

    #UDP HEADER OPTIONS

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
    } else {

        set dest_port 319
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
    } else {

        set src_port 319
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
    } else {

        set udp_length 0
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
    } else {

        set udp_checksum 0
    }

    #PTP HEADER OPTIONS

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
    } else {

        set major_sdo_id 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
    } else {

        set message_type 0
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
    } else {

        set minor_version 2
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
    } else {

        set message_length 44
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
    } else {

        set domain_number 0
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
    } else {

        set minor_sdo_id 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
    } else {

        set correction_field 0
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
    } else {

        set message_type_specific 0
    }

    #Source port number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
    } else {

        set src_port_number 0
    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
    } else {

        set src_clock_identity 0
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
    } else {

        set sequence_id 0
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
    } else {

        set control_field 00
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
    } else {

        set log_message_interval 0
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
    } else {

        set unicast_flag 0
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
    } else {

        set profile_specific2 0
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
    } else {

        set secure 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
    } else {

        set freq_traceable 0
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
    } else {

        set sync_uncertain 0
    }

    #PTP SYNC MESSAGE OPTIONS
    #Origin timestamp in seconds
    if {[info exists param(-origin_timestamp_sec)]} {

        set origin_timestamp_sec $param(-origin_timestamp_sec)
    } else {

        set origin_timestamp_sec 0
    }

    #Origin timestamp in nanoseconds
    if {[info exists param(-origin_timestamp_ns)]} {

        set origin_timestamp_ns $param(-origin_timestamp_ns)
    } else {

        set origin_timestamp_ns 0
    }

    #MISCELLANEOUS
    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
    } else {

        set interval 1
    }

    #Interval in millisec
    set interval [expr int($interval*1000)]

    #CONSTRUCT PTP HA SYNC PACKET DUMP
    if {![pbLib::encode_ptp_ha_pkt\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -major_sdo_id                      $major_sdo_id\
                -message_type                      $message_type\
                -minor_version                     $minor_version\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -minor_sdo_id                      $minor_sdo_id\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -secure                            $secure\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_tracaeble                    $time_traceable\
                -freq_tracaeble                    $freq_traceable\
                -sync_uncertain                    $sync_uncertain\
                -correction_field                  $correction_field\
                -message_type_specific             $message_type_specific\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -origin_timestamp_sec              $origin_timestamp_sec\
                -origin_timestamp_ns               $origin_timestamp_ns\
                -hexdump                           hexdump]} {

        return 0
    }

    pltLib::send_periodic\
                     -session_id     $session_id\
                     -port_num       $port_num\
                     -data           $hexdump\
                     -instance       6\
                     -interval       $interval\
                     -count          $count\
                     -handler_id     "PTP_HA"

    return 1

}

#7.
########################################################################################
#                                                                                      #
#  PROCEDURE NAME                : pltLib::send_ptp_ha_pdelay_resp                     #
#                                                                                      #
#  INPUT PARAMETERS              :                                                     #
#                                                                                      #
#   session_id                   :  (Mandatory) Session identifier                     #
#                                                                                      #
#   port_num                     :  (Mandatory) Port number on which frame is to send. #
#                                               (e.g., 0/1)                            #
#                                                                                      #
#   dest_mac                     :  (Mandatory) Destination MAC dddress.               #
#                                               (e.g., 01:80:C2:00:00:20)              #
#                                                                                      #
#   src_mac                      :  (Mandatory) Source MAC address.                    #
#                                               (e.g., 00:80:C2:00:00:21)              #
#                                                                                      #
#   eth_type                     :  (Optional)  EtherType (0800 or 88F7)               #
#                                               (Default : 0800)                       #
#                                                                                      #
#   vlan_id                      :  (Optional)  Vlan ID                                #
#                                               (Default : 0)                          #
#                                                                                      #
#   vlan_priority                :  (Optional)  Vlan Priority                          #
#                                               (Default : 0)                          #
#                                                                                      #
#   dest_ip                      :  (Optional)  Destination IP address                 #
#                                               (Default : 224.0.1.129)                #
#                                                                                      #
#   src_ip                       :  (Optional)  Source IP address                      #
#                                               (Default : 0.0.0.0)                    #
#                                                                                      #
#   ip_checksum                  :  (Optional)  IP checksum                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   dest_port                    :  (Optional)  UDP destination port number            #
#                                               (Default : 319)                        #
#                                                                                      #
#   src_port                     :  (Optional)  UDP source port number                 #
#                                               (Default : 319)                        #
#                                                                                      #
#   udp_length                   :  (Optional)  UDP length                             #
#                                               (Default : 0)                          #
#                                                                                      #
#   udp_checksum                 :  (Optional)  UDP checksum                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   major_sdo_id                 :  (Optional)  Major sdo id                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   message_type                 :  (Optional)  Message Type                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   minor_version                :  (Optional)  PTP version number                     #
#                                               (Default : 2)                          #
#                                                                                      #
#   version_ptp                  :  (Optional)  PTP minor version number               #
#                                               (Default : 2)                          #
#                                                                                      #
#   message_length               :  (Optional)  PTP Sync message length                #
#                                               (Default : 44)                         #
#                                                                                      #
#   domain_number                :  (Optional)  Domain number                          #
#                                               (Default : 0)                          #
#                                                                                      #
#   minor_sdo_id                 :  (Optional)  Minor Sdo id                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   alternate_master_flag        :  (Optional)  Alternate Master flag                  #
#                                               (Default : 0)                          #
#                                                                                      #
#   two_step_flag                :  (Optional)  Two Step Flag                          #
#                                               (Default : 0)                          #
#                                                                                      #
#   unicast_flag                 :  (Optional)  Unicast Flag                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   profile_specific1            :  (Optional)  Profile specific1 Flag                 #
#                                               (Default : 0)                          #
#                                                                                      #
#   profile_specific2            :  (Optional)  Profile specific2 Flag                 #
#                                               (Default : 0)                          #
#                                                                                      #
#   secure                       :  (Optional)  Secure Flag                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   leap61                       :  (Optional)  Leap61 Flag                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   leap59                       :  (Optional)  Leap59 Flag                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   current_utc_offset_valid     :  (Optional)  Current UTC Offset Flag                #
#                                               (Default : 0)                          #
#                                                                                      #
#   ptp_timescale                :  (Optional)  PTP TimeSclae Flag                     #
#                                               (Default : 0)                          #
#                                                                                      #
#   time_tracaeble               :  (Optional)  Time Traceable Flag                    #
#                                               (Default : 0)                          #
#                                                                                      #
#   freq_tracaeble               :  (Optional)  Frequency Traceable Flag               #
#                                               (Default : 0)                          #
#                                                                                      #
#   sync_uncertain               :  (Optional)  Synchronization uncertain Flag         #
#                                               (Default : 0)                          #
#                                                                                      #
#   correction_field             :  (Optional)  Correction Field value                 #
#                                               (Default : 0)                          #
#                                                                                      #
#   message_type_specific        :  (Optional)  Message type                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   src_port_number              :  (Optional)  Source port number                     #
#                                               (Default : 0)                          #
#                                                                                      #
#   src_clock_identity           :  (Optional)  Source Clock identity                  #
#                                               (Default : 0)                          #
#                                                                                      #
#   sequence_id                  :  (Optional)  Sequence ID                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   control_field                :  (Optional)  Control field                          #
#                                               (Default : 00)                         #
#                                                                                      #
#   log_message_interval         :  (Optional)  Log message interval                   #
#                                               (Default : 0)                          #
#                                                                                      #
#   request_receipt_timestamp_sec:  (Optional) Request receipt timestamp in seconds    #
#                                              (Default : 0)                           #
#                                                                                      #
#   request_receipt_timestamp_ns :  (Optional) Request receipt timestamp in nanoseconds#
#                                              (Default : 0)                           #
#                                                                                      #
#   requesting_port_number        : (Optional)  Requesting Port Number                 #
#                                               (Default : 0)                          #
#                                                                                      #
#   requesting_clock_identity     : (Optional)  Requesting Clock Identity              #
#                                               (Default : 0)                          #
#                                                                                      #
#   count                         :  (Optional)  Frame count                           #
#                                                (Default : 1)                         #
#                                                                                      #
#   interval                      :  (Optional)  Frame interval (in seconds)           #
#                                                (Default : 1)                         #
#                                                                                      #
#  OUTPUT PARAMETERS              :  NONE                                              #
#                                                                                      #
#  RETURNS                        :  1 on success (If PTP HA delay response is sent    #
#                                                 with the given parameters)           #
#                                                                                      #
#                                                                                      #
#                                 :  0 on success (If PTP HA delay resp could not be   #
#                                                  sent with the given parameters)     #
#                                                                                      #
#  DEFINITION                     : This function is used to construct and send PTP    #
#                                   HA delay request message.                          #
#                                                                                      #
#  USAGE                          :                                                    #
#                                                                                      #
#       pltLib::send_ptp_ha_pdelay_resp\                                               #
#                   -session_id                        $session_id\                    #
#                   -port_num                          $port_num\                      #
#                   -dest_mac                          $dest_mac\                      #
#                   -src_mac                           $src_mac\                       #
#                   -eth_type                          $eth_type\                      #
#                   -vlan_id                           $vlan_id\                       #
#                   -vlan_priority                     $vlan_priority\                 #
#                   -dest_ip                           $dest_ip\                       #
#                   -src_ip                            $src_ip\                        #
#                   -ip_checksum                       $ip_checksum\                   #
#                   -dest_port                         $dest_port\                     #
#                   -src_port                          $src_port\                      #
#                   -udp_length                        $udp_length\                    #
#                   -udp_checksum                      $udp_checksum\                  #
#                   -major_sdo_id                      $major_sdo_id\                  #
#                   -message_type                      $message_type\                  #
#                   -minor_version                     $minor_version\                 #
#                   -version_ptp                       $version_ptp\                   #
#                   -message_length                    $message_length\                #
#                   -domain_number                     $domain_number\                 #
#                   -minor_sdo_id                      $minor_sdo_id\                  #
#                   -alternate_master_flag             $alternate_master_flag\         #
#                   -two_step_flag                     $two_step_flag\                 #
#                   -unicast_flag                      $unicast_flag\                  #
#                   -profile_specific1                 $profile_specific1\             #
#                   -profile_specific2                 $profile_specific2\             #
#                   -secure                            $secure\                        #
#                   -leap61                            $leap61\                        #
#                   -leap59                            $leap59\                        #
#                   -current_utc_offset_valid         $current_utc_offset_valid\       #
#                   -ptp_timescale                     $ptp_timescale\                 #
#                   -time_tracaeble                    $time_traceable\                #
#                   -freq_tracaeble                    $freq_traceable\                #
#                   -sync_uncertain                    $sync_uncertain\                #
#                   -correction_field                  $correction_field\              #
#                   -message_type_specific             $message_type_specific\         #
#                   -src_port_number                   $src_port_number\               #
#                   -src_clock_identity                $src_clock_identity\            #
#                   -sequence_id                       $sequence_id\                   #
#                   -control_field                     $control_field\                 #
#                   -log_message_interval              $log_message_interval\          #
#                   -request_receipt_timestamp_sec     $request_receipt_timestamp_sec\ #
#                   -request_receipt_timestamp_ns      $request_receipt_timestamp_ns\  #
#                   -requesting_port_number            $requesting_port_number\        #
#                   -requesting_clock_identity         $requesting_clock_identity\     #
#                   -count                             $count\                         #
#                   -interval                          $interval                       #
#                                                                                      #
########################################################################################

proc pltLib::send_ptp_ha_pdelay_resp {args} {

    array set param $args

    #INPUT PARAMETERS

    #Session identifier
    if {[info exists param(-session_id)]} {

        set session_id $param(-session_id)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_pdelay_resp: Missing Argument -> session_id"
        return 0
    }

    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_pdelay_resp: Missing Argument -> port_num"
        return 0
    }

    #ETHERNET HEADER OPTIONS

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_pdelay_resp: Missing Argument -> dest_mac"
        return 0
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_pdelay_resp: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
    } else {

        set eth_type 0800
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
    } else {

        set vlan_id 0
    }

    #Vlan Priority
    if {[info exists param(-vlan_priority)]} {

        set vlan_priority $param(-vlan_priority)
    } else {

        set vlan_priority 0
    }

    #IP HEADER OPTIONS

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
    } else {

        set dest_ip 224.0.1.129
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
    } else {

        set src_ip 0.0.0.0
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
    } else {

        set ip_checksum 0
    }

    #UDP HEADER OPTIONS

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
    } else {

        set dest_port 319
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
    } else {

        set src_port 319
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
    } else {

        set udp_length 0
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
    } else {

        set udp_checksum 0
    }

    #PTP HEADER OPTIONS

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
    } else {

        set major_sdo_id 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
    } else {

        set message_type 0
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
    } else {

        set minor_version 2
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
    } else {

        set message_length 44
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
    } else {

        set domain_number 0
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
    } else {

        set minor_sdo_id 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
    } else {

        set correction_field 0
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
    } else {

        set message_type_specific 0
    }

    #Source port number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
    } else {

        set src_port_number 0
    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
    } else {

        set src_clock_identity 0
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
    } else {

        set sequence_id 0
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
    } else {

        set control_field 00
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
    } else {

        set log_message_interval 0
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
    } else {

        set unicast_flag 0
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
    } else {

        set profile_specific2 0
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
    } else {

        set secure 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
    } else {

        set freq_traceable 0
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
    } else {

        set sync_uncertain 0
    }

    #Request Receipt timestamp in seconds
    if {[info exists param(-request_receipt_timestamp_sec)]} {

        set request_receipt_timestamp_sec $param(-request_receipt_timestamp_sec)
    } else {

        set request_receipt_timestamp_sec 0
    }

    #Request receipt timestamp in nanoseconds
    if {[info exists param(-request_receipt_timestamp_ns)]} {

        set request_receipt_timestamp_ns $param(-request_receipt_timestamp_ns)
    } else {

        set request_receipt_timestamp_ns 0
    }

    if {[info exists param(-requesting_port_number)]} {

        set requesting_port_number $param(-requesting_port_number)
    } else {

        set requesting_port_number 0
    }

    if {[info exists param(-requesting_clock_identity)]} {

        set requesting_clock_identity $param(-requesting_clock_identity)
    } else {

        set requesting_port_number 0
    }

    #MISCELLANEOUS
    #Instance
    if {[info exists param(-instance)]} {

        set instance $param(-instance)
    } else {

        set instance 7
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
    } else {

        set interval 1
    }

    #Interval in millisec
    set interval [expr int($interval*1000)]

    #CONSTRUCT PTP HA PEER DELAY PACKET DUMP
    if {![pbLib::encode_ptp_ha_pkt\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -major_sdo_id                      $major_sdo_id\
                -message_type                      $message_type\
                -minor_version                     $minor_version\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -minor_sdo_id                      $minor_sdo_id\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -secure                            $secure\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_tracaeble                    $time_traceable\
                -freq_tracaeble                    $freq_traceable\
                -sync_uncertain                    $sync_uncertain\
                -correction_field                  $correction_field\
                -message_type_specific             $message_type_specific\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -request_receipt_timestamp_sec     $request_receipt_timestamp_sec\
                -request_receipt_timestamp_ns      $request_receipt_timestamp_ns\
                -requesting_port_number            $requesting_port_number\
                -requesting_clock_identity         $requesting_clock_identity\
                -hexdump                           hexdump]} {

        return 0
    }

    pltLib::send_periodic\
                     -session_id     $session_id\
                     -port_num       $port_num\
                     -data           $hexdump\
                     -instance       $instance\
                     -interval       $interval\
                     -count          $count\
                     -handler_id     "PTP_HA"

    return 1

}

#8.
########################################################################################
#                                                                                      #
#  PROCEDURE NAME                : pltLib::send_ptp_ha_pdelay_resp_followup            #
#                                                                                      #
#  INPUT PARAMETERS              :                                                     #
#                                                                                      #
#   session_id                   :  (Mandatory) Session identifier                     #
#                                                                                      #
#   port_num                     :  (Mandatory) Port number on which frame is to send. #
#                                               (e.g., 0/1)                            #
#                                                                                      #
#   dest_mac                     :  (Mandatory) Destination MAC dddress.               #
#                                               (e.g., 01:80:C2:00:00:20)              #
#                                                                                      #
#   src_mac                      :  (Mandatory) Source MAC address.                    #
#                                               (e.g., 00:80:C2:00:00:21)              #
#                                                                                      #
#   eth_type                     :  (Optional)  EtherType (0800 or 88F7)               #
#                                               (Default : 0800)                       #
#                                                                                      #
#   vlan_id                      :  (Optional)  Vlan ID                                #
#                                               (Default : 0)                          #
#                                                                                      #
#   vlan_priority                :  (Optional)  Vlan Priority                          #
#                                               (Default : 0)                          #
#                                                                                      #
#   dest_ip                      :  (Optional)  Destination IP address                 #
#                                               (Default : 224.0.1.129)                #
#                                                                                      #
#   src_ip                       :  (Optional)  Source IP address                      #
#                                               (Default : 0.0.0.0)                    #
#                                                                                      #
#   ip_checksum                  :  (Optional)  IP checksum                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   dest_port                    :  (Optional)  UDP destination port number            #
#                                               (Default : 319)                        #
#                                                                                      #
#   src_port                     :  (Optional)  UDP source port number                 #
#                                               (Default : 319)                        #
#                                                                                      #
#   udp_length                   :  (Optional)  UDP length                             #
#                                               (Default : 0)                          #
#                                                                                      #
#   udp_checksum                 :  (Optional)  UDP checksum                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   major_sdo_id                 :  (Optional)  Major sdo id                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   message_type                 :  (Optional)  Message Type                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   minor_version                :  (Optional)  PTP version number                     #
#                                               (Default : 2)                          #
#                                                                                      #
#   version_ptp                  :  (Optional)  PTP minor version number               #
#                                               (Default : 2)                          #
#                                                                                      #
#   message_length               :  (Optional)  PTP Sync message length                #
#                                               (Default : 44)                         #
#                                                                                      #
#   domain_number                :  (Optional)  Domain number                          #
#                                               (Default : 0)                          #
#                                                                                      #
#   minor_sdo_id                 :  (Optional)  Minor Sdo id                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   alternate_master_flag        :  (Optional)  Alternate Master flag                  #
#                                               (Default : 0)                          #
#                                                                                      #
#   two_step_flag                :  (Optional)  Two Step Flag                          #
#                                               (Default : 0)                          #
#                                                                                      #
#   unicast_flag                 :  (Optional)  Unicast Flag                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   profile_specific1            :  (Optional)  Profile specific1 Flag                 #
#                                               (Default : 0)                          #
#                                                                                      #
#   profile_specific2            :  (Optional)  Profile specific2 Flag                 #
#                                               (Default : 0)                          #
#                                                                                      #
#   secure                       :  (Optional)  Secure Flag                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   leap61                       :  (Optional)  Leap61 Flag                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   leap59                       :  (Optional)  Leap59 Flag                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   current_utc_offset_valid     :  (Optional)  Current UTC Offset Flag                #
#                                               (Default : 0)                          #
#                                                                                      #
#   ptp_timescale                :  (Optional)  PTP TimeSclae Flag                     #
#                                               (Default : 0)                          #
#                                                                                      #
#   time_tracaeble               :  (Optional)  Time Traceable Flag                    #
#                                               (Default : 0)                          #
#                                                                                      #
#   freq_tracaeble               :  (Optional)  Frequency Traceable Flag               #
#                                               (Default : 0)                          #
#                                                                                      #
#   sync_uncertain               :  (Optional)  Synchronization uncertain Flag         #
#                                               (Default : 0)                          #
#                                                                                      #
#   correction_field             :  (Optional)  Correction Field value                 #
#                                               (Default : 0)                          #
#                                                                                      #
#   message_type_specific        :  (Optional)  Message type                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   src_port_number              :  (Optional)  Source port number                     #
#                                               (Default : 0)                          #
#                                                                                      #
#   src_clock_identity           :  (Optional)  Source Clock identity                  #
#                                               (Default : 0)                          #
#                                                                                      #
#   sequence_id                  :  (Optional)  Sequence ID                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   control_field                :  (Optional)  Control field                          #
#                                               (Default : 00)                         #
#                                                                                      #
#   log_message_interval         :  (Optional)  Log message interval                   #
#                                               (Default : 0)                          #
#                                                                                      #
#   response_origin_timestamp_sec:  (Optional) Response origin timestamp in seconds    #
#                                              (Default : 0)                           #
#                                                                                      #
#   response_origin_timestamp_ns :  (Optional) Response origin timestamp in nanoseconds#
#                                              (Default : 0)                           #
#                                                                                      #
#   requesting_port_number        : (Optional)  Requesting Port Number                 #
#                                               (Default : 0)                          #
#                                                                                      #
#   requesting_clock_identity     : (Optional)  Requesting Clock Identity              #
#                                               (Default : 0)                          #
#                                                                                      #
#   count                         :  (Optional)  Frame count                           #
#                                                (Default : 1)                         #
#                                                                                      #
#   interval                      :  (Optional)  Frame interval (in seconds)           #
#                                                (Default : 1)                         #
#                                                                                      #
#  OUTPUT PARAMETERS              :  NONE                                              #
#                                                                                      #
#  RETURNS                        :  1 on success (If PTP HA delay response is sent    #
#                                                 with the given parameters)           #
#                                                                                      #
#                                                                                      #
#                                 :  0 on success (If PTP HA delay resp could not be   #
#                                                  sent with the given parameters)     #
#                                                                                      #
#  DEFINITION                     : This function is used to construct and send PTP    #
#                                   HA delay request message.                          #
#                                                                                      #
#  USAGE                          :                                                    #
#                                                                                      #
#       pltLib::send_ptp_ha_pdelay_resp\                                               #
#                   -session_id                        $session_id\                    #
#                   -port_num                          $port_num\                      #
#                   -dest_mac                          $dest_mac\                      #
#                   -src_mac                           $src_mac\                       #
#                   -eth_type                          $eth_type\                      #
#                   -vlan_id                           $vlan_id\                       #
#                   -vlan_priority                     $vlan_priority\                 #
#                   -dest_ip                           $dest_ip\                       #
#                   -src_ip                            $src_ip\                        #
#                   -ip_checksum                       $ip_checksum\                   #
#                   -dest_port                         $dest_port\                     #
#                   -src_port                          $src_port\                      #
#                   -udp_length                        $udp_length\                    #
#                   -udp_checksum                      $udp_checksum\                  #
#                   -major_sdo_id                      $major_sdo_id\                  #
#                   -message_type                      $message_type\                  #
#                   -minor_version                     $minor_version\                 #
#                   -version_ptp                       $version_ptp\                   #
#                   -message_length                    $message_length\                #
#                   -domain_number                     $domain_number\                 #
#                   -minor_sdo_id                      $minor_sdo_id\                  #
#                   -alternate_master_flag             $alternate_master_flag\         #
#                   -two_step_flag                     $two_step_flag\                 #
#                   -unicast_flag                      $unicast_flag\                  #
#                   -profile_specific1                 $profile_specific1\             #
#                   -profile_specific2                 $profile_specific2\             #
#                   -secure                            $secure\                        #
#                   -leap61                            $leap61\                        #
#                   -leap59                            $leap59\                        #
#                   -current_utc_offset_valid         $current_utc_offset_valid\       #
#                   -ptp_timescale                     $ptp_timescale\                 #
#                   -time_tracaeble                    $time_traceable\                #
#                   -freq_tracaeble                    $freq_traceable\                #
#                   -sync_uncertain                    $sync_uncertain\                #
#                   -correction_field                  $correction_field\              #
#                   -message_type_specific             $message_type_specific\         #
#                   -src_port_number                   $src_port_number\               #
#                   -src_clock_identity                $src_clock_identity\            #
#                   -sequence_id                       $sequence_id\                   #
#                   -control_field                     $control_field\                 #
#                   -log_message_interval              $log_message_interval\          #
#                   -response_origin_timestamp_sec     $response_origin_timestamp_sec\ #
#                   -response-origin_timestamp_ns      $response_origin_timestamp_ns\  #
#                   -requesting_port_number            $requesting_port_number\        #
#                   -requesting_clock_identity         $requesting_clock_identity\     #
#                   -count                             $count\                         #
#                   -interval                          $interval                       #
#                                                                                      #
########################################################################################

proc pltLib::send_ptp_ha_pdelay_resp_followup {args} {

    array set param $args

    #INPUT PARAMETERS

    #Session identifier
    if {[info exists param(-session_id)]} {

        set session_id $param(-session_id)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_pdelay_resp_followup: Missing Argument -> session_id"
        return 0
    }

    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_pdelay_resp_followup: Missing Argument -> port_num"
        return 0
    }

    #ETHERNET HEADER OPTIONS

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_pdelay_resp_followup: Missing Argument -> dest_mac"
        return 0
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_pdelay_resp_followup: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
    } else {

        set eth_type 0800
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
    } else {

        set vlan_id 0
    }

    #Vlan Priority
    if {[info exists param(-vlan_priority)]} {

        set vlan_priority $param(-vlan_priority)
    } else {

        set vlan_priority 0
    }

    #IP HEADER OPTIONS

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
    } else {

        set dest_ip 224.0.1.129
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
    } else {

        set src_ip 0.0.0.0
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
    } else {

        set ip_checksum 0
    }

    #UDP HEADER OPTIONS

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
    } else {

        set dest_port 319
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
    } else {

        set src_port 319
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
    } else {

        set udp_length 0
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
    } else {

        set udp_checksum 0
    }

    #PTP HEADER OPTIONS

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
    } else {

        set major_sdo_id 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
    } else {

        set message_type 0
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
    } else {

        set minor_version 2
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
    } else {

        set message_length 44
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
    } else {

        set domain_number 0
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
    } else {

        set minor_sdo_id 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
    } else {

        set correction_field 0
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
    } else {

        set message_type_specific 0
    }

    #Source port number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
    } else {

        set src_port_number 0
    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
    } else {

        set src_clock_identity 0
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
    } else {

        set sequence_id 0
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
    } else {

        set control_field 00
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
    } else {

        set log_message_interval 0
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
    } else {

        set unicast_flag 0
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
    } else {

        set profile_specific2 0
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
    } else {

        set secure 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
    } else {

        set freq_traceable 0
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
    } else {

        set sync_uncertain 0
    }

    #Request Receipt timestamp in seconds
    if {[info exists param(-response_origin_timestamp_sec)]} {

        set response_origin_timestamp_sec $param(-response_origin_timestamp_sec)
    } else {

        set response_origin_timestamp_sec 0
    }

    #Request receipt timestamp in nanoseconds
    if {[info exists param(-response_origin_timestamp_ns)]} {

        set response_origin_timestamp_ns $param(-response_origin_timestamp_ns)
    } else {

        set response_origin_timestamp_ns 0
    }

    if {[info exists param(-requesting_port_number)]} {

        set requesting_port_number $param(-requesting_port_number)
    } else {

        set requesting_port_number 0
    }

    if {[info exists param(-requesting_clock_identity)]} {

        set requesting_clock_identity $param(-requesting_clock_identity)
    } else {

        set requesting_clock_identity 0
    }

    #MISCELLANEOUS
    #Instance
    if {[info exists param(-instance)]} {

        set instance $param(-instance)
    } else {

        set instance 8
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
    } else {

        set interval 1
    }

    #Interval in millisec
    set interval [expr int($interval*1000)]

    #CONSTRUCT PTP HA PEER DELAY PACKET DUMP
    if {![pbLib::encode_ptp_ha_pkt\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -major_sdo_id                      $major_sdo_id\
                -message_type                      $message_type\
                -minor_version                     $minor_version\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -minor_sdo_id                      $minor_sdo_id\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -secure                            $secure\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_tracaeble                    $time_traceable\
                -freq_tracaeble                    $freq_traceable\
                -sync_uncertain                    $sync_uncertain\
                -correction_field                  $correction_field\
                -message_type_specific             $message_type_specific\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -response_origin_timestamp_sec     $response_origin_timestamp_sec\
                -response_origin_timestamp_ns      $response_origin_timestamp_ns\
                -requesting_port_number            $requesting_port_number\
                -requesting_clock_identity         $requesting_clock_identity\
                -hexdump                           hexdump]} {

        return 0
    }

    pltLib::send_periodic\
                     -session_id     $session_id\
                     -port_num       $port_num\
                     -data           $hexdump\
                     -instance       $instance\
                     -interval       $interval\
                     -count          $count\
                     -handler_id     "PTP_HA"

    return 1

}

#9.
########################################################################################
#                                                                                      #
#  PROCEDURE NAME                : pltLib::send_ptp_ha_signal                          #
#                                                                                      #
#  INPUT PARAMETERS              :                                                     #
#                                                                                      #
#   session_id                   :  (Mandatory) Session identifier                     #
#                                                                                      #
#   port_num                     :  (Mandatory) Port number on which frame is to send. #
#                                               (e.g., 0/1)                            #
#                                                                                      #
#   dest_mac                     :  (Mandatory) Destination MAC dddress.               #
#                                               (e.g., 01:80:C2:00:00:20)              #
#                                                                                      #
#   src_mac                      :  (Mandatory) Source MAC address.                    #
#                                               (e.g., 00:80:C2:00:00:21)              #
#                                                                                      #
#   eth_type                     :  (Optional)  EtherType (0800 or 88F7)               #
#                                               (Default : 0800)                       #
#                                                                                      #
#   vlan_id                      :  (Optional)  Vlan ID                                #
#                                               (Default : 0)                          #
#                                                                                      #
#   vlan_priority                :  (Optional)  Vlan Priority                          #
#                                               (Default : 0)                          #
#                                                                                      #
#   dest_ip                      :  (Optional)  Destination IP address                 #
#                                               (Default : 224.0.1.129)                #
#                                                                                      #
#   src_ip                       :  (Optional)  Source IP address                      #
#                                               (Default : 0.0.0.0)                    #
#                                                                                      #
#   ip_checksum                  :  (Optional)  IP checksum                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   dest_port                    :  (Optional)  UDP destination port number            #
#                                               (Default : 319)                        #
#                                                                                      #
#   src_port                     :  (Optional)  UDP source port number                 #
#                                               (Default : 319)                        #
#                                                                                      #
#   udp_length                   :  (Optional)  UDP length                             #
#                                               (Default : 0)                          #
#                                                                                      #
#   udp_checksum                 :  (Optional)  UDP checksum                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   major_sdo_id                 :  (Optional)  Major sdo id                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   message_type                 :  (Optional)  Message Type                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   minor_version                :  (Optional)  PTP version number                     #
#                                               (Default : 2)                          #
#                                                                                      #
#   version_ptp                  :  (Optional)  PTP minor version number               #
#                                               (Default : 2)                          #
#                                                                                      #
#   message_length               :  (Optional)  PTP Sync message length                #
#                                               (Default : 44)                         #
#                                                                                      #
#   domain_number                :  (Optional)  Domain number                          #
#                                               (Default : 0)                          #
#                                                                                      #
#   minor_sdo_id                 :  (Optional)  Minor Sdo id                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   alternate_master_flag        :  (Optional)  Alternate Master flag                  #
#                                               (Default : 0)                          #
#                                                                                      #
#   two_step_flag                :  (Optional)  Two Step Flag                          #
#                                               (Default : 0)                          #
#                                                                                      #
#   unicast_flag                 :  (Optional)  Unicast Flag                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   profile_specific1            :  (Optional)  Profile specific1 Flag                 #
#                                               (Default : 0)                          #
#                                                                                      #
#   profile_specific2            :  (Optional)  Profile specific2 Flag                 #
#                                               (Default : 0)                          #
#                                                                                      #
#   secure                       :  (Optional)  Secure Flag                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   leap61                       :  (Optional)  Leap61 Flag                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   leap59                       :  (Optional)  Leap59 Flag                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   current_utc_offset_valid     :  (Optional)  Current UTC Offset Flag                #
#                                               (Default : 0)                          #
#                                                                                      #
#   ptp_timescale                :  (Optional)  PTP TimeSclae Flag                     #
#                                               (Default : 0)                          #
#                                                                                      #
#   time_tracaeble               :  (Optional)  Time Traceable Flag                    #
#                                               (Default : 0)                          #
#                                                                                      #
#   freq_tracaeble               :  (Optional)  Frequency Traceable Flag               #
#                                               (Default : 0)                          #
#                                                                                      #
#   sync_uncertain               :  (Optional)  Synchronization uncertain Flag         #
#                                               (Default : 0)                          #
#                                                                                      #
#   correction_field             :  (Optional)  Correction Field value                 #
#                                               (Default : 0)                          #
#                                                                                      #
#   message_type_specific        :  (Optional)  Message type                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   src_port_number              :  (Optional)  Source port number                     #
#                                               (Default : 0)                          #
#                                                                                      #
#   src_clock_identity           :  (Optional)  Source Clock identity                  #
#                                               (Default : 0)                          #
#                                                                                      #
#   sequence_id                  :  (Optional)  Sequence ID                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   control_field                :  (Optional)  Control field                          #
#                                               (Default : 00)                         #
#                                                                                      #
#   log_message_interval         :  (Optional)  Log message interval                   #
#                                               (Default : 0)                          #
#                                                                                      #
#   target_port_number            : (Optional)  Target Port Number                     #
#                                               (Default : 0)                          #
#                                                                                      #
#   target_clock_identity         : (Optional)  Target Clock Identity                  #
#                                               (Default : 0)                          #
#                                                                                      #
#   tlv                           : (Optional)  Tlv                                    #
#                                               (Default : L1_SYNC)                    #
#                                                                                      #
#   tlv_type                      : (Optional)  Tlv Type                               #
#                                               (Default : 32769)                      #
#                                                                                      #
#   tlv_length                    : (Optional)  Tlv Length                             #
#                                               (Default : 30)                         #
#                                                                                      #
#   ope                           : (Optional)  Ope                                    #
#                                               (Default : 0)                          #
#                                                                                      #
#   cr                            : (Optional)  Cr                                     #
#                                               (Default : 0)                          #
#                                                                                      #
#   rcr                           : (Optional)  Rcr                                    #
#                                               (Default : 0)                          #
#                                                                                      #
#   tcr                           : (Optional)  Tcr                                    #
#                                               (Default : 0)                          #
#                                                                                      #
#   ic                            : (Optional)  Ic                                     #
#                                               (Default : 0)                          #
#                                                                                      #
#   irc                           : (Optional)  Irc                                    #
#                                               (Default : 0)                          #
#                                                                                      #
#   itc                           : (Optional)  Itc                                    #
#                                               (Default : 0)                          #
#                                                                                      #
#   fov                           : (Optional)  Fov                                    #
#                                               (Default : 0)                          #
#                                                                                      #
#   pov                           : (Optional)  Pov                                    #
#                                               (Default : 0)                          #
#                                                                                      #
#   tct                           : (Optional)  Tct                                    #
#                                               (Default : 0)                          #
#                                                                                      #
#   phase_offset_tx               : (Optional)  Phase Offset Tx                        #
#                                               (Default : 0)                          #
#                                                                                      #
#   phase_offset_tx_timestamp_sec : (Optional)  Phase Offset Tx Timestamp Sec          #
#                                               (Default : 0)                          #
#                                                                                      #
#   phase_offset_tx_timestamp_ns  : (Optional)  Phase Offset Tx Timestamp Ns           #
#                                               (Default : 0)                          #
#                                                                                      #
#   freq_offset_tx                : (Optional)  Freq Offset Tx                         #
#                                               (Default : 0)                          #
#                                                                                      #
#   freq_offset_tx_timestamp_sec  : (Optional)  Freq Offset Tx Timestamp Sec           #
#                                               (Default : 0)                          #
#                                                                                      #
#   freq_offset_tx_timestamp_ns   : (Optional)  Freq Offset Tx Timestamp Ns            #
#                                               (Default : 0)                          #
#                                                                                      #
#                                                                                      #
#   count                         :  (Optional)  Frame count                           #
#                                                (Default : 1)                         #
#                                                                                      #
#   interval                      :  (Optional)  Frame interval (in seconds)           #
#                                                (Default : 1)                         #
#                                                                                      #
#  OUTPUT PARAMETERS              :  NONE                                              #
#                                                                                      #
#  RETURNS                        :  1 on success (If PTP HA signal message is sent    #
#                                                 with the given parameters)           #
#                                                                                      #
#                                                                                      #
#                                 :  0 on success (If PTP HA signal message count not  #
#                                                  be sent with the given parameters)  #
#                                                                                      #
#  DEFINITION                     : This function is used to construct and send PTP    #
#                                   HA signal message.                                 #
#                                                                                      #
#  USAGE                          :                                                    #
#                                                                                      #
#       pltLib::send_ptp_ha_signal\                                                    #
#                   -session_id                        $session_id\                    #
#                   -port_num                          $port_num\                      #
#                   -dest_mac                          $dest_mac\                      #
#                   -src_mac                           $src_mac\                       #
#                   -eth_type                          $eth_type\                      #
#                   -vlan_id                           $vlan_id\                       #
#                   -vlan_priority                     $vlan_priority\                 #
#                   -dest_ip                           $dest_ip\                       #
#                   -src_ip                            $src_ip\                        #
#                   -ip_checksum                       $ip_checksum\                   #
#                   -dest_port                         $dest_port\                     #
#                   -src_port                          $src_port\                      #
#                   -udp_length                        $udp_length\                    #
#                   -udp_checksum                      $udp_checksum\                  #
#                   -major_sdo_id                      $major_sdo_id\                  #
#                   -message_type                      $message_type\                  #
#                   -minor_version                     $minor_version\                 #
#                   -version_ptp                       $version_ptp\                   #
#                   -message_length                    $message_length\                #
#                   -domain_number                     $domain_number\                 #
#                   -minor_sdo_id                      $minor_sdo_id\                  #
#                   -alternate_master_flag             $alternate_master_flag\         #
#                   -two_step_flag                     $two_step_flag\                 #
#                   -unicast_flag                      $unicast_flag\                  #
#                   -profile_specific1                 $profile_specific1\             #
#                   -profile_specific2                 $profile_specific2\             #
#                   -secure                            $secure\                        #
#                   -leap61                            $leap61\                        #
#                   -leap59                            $leap59\                        #
#                   -current_utc_offset_valid         $current_utc_offset_valid\       #
#                   -ptp_timescale                     $ptp_timescale\                 #
#                   -time_tracaeble                    $time_traceable\                #
#                   -freq_tracaeble                    $freq_traceable\                #
#                   -sync_uncertain                    $sync_uncertain\                #
#                   -correction_field                  $correction_field\              #
#                   -message_type_specific             $message_type_specific\         #
#                   -src_port_number                   $src_port_number\               #
#                   -src_clock_identity                $src_clock_identity\            #
#                   -sequence_id                       $sequence_id\                   #
#                   -control_field                     $control_field\                 #
#                   -log_message_interval              $log_message_interval\          #
#                   -origin_timestamp_sec             $origin_timestamp_sec\           #
#                   -origin_timestamp_ns              $origin_timestamp_ns\            #
#                   -current_utc_offset_valid         $current_utc_offset_valid\       #
#                   -gm_priority1                     $gm_priority1\                   #
#                   -gm_priority2                     $gm_priority2\                   #
#                   -gm_identity                      $gm_identity\                    #
#                   -steps_removed                    $steps_removed\                  #
#                   -gm_clock_classy                  $gm_clock_classy\                #
#                   -gm_clock_accuracy                $gm_clock_accuracy\              #
#                   -gm_clock_variance                $gm_clock_variance\              #
#                   -target_port_number               $target_port_number\             #
#                   -target_clock_identity            $target_clock_identity\          #
#                   -tlv                              $tlv\                            #
#                   -tlv_type                         $tlv_type\                       #
#                   -tlv_length                       $tlv_length\                     #
#                   -ope                              $ope\                            #
#                   -cr                               $cr\                             #
#                   -rcr                              $rcr\                            #
#                   -tcr                              $tcr\                            #
#                   -ic                               $ic\                             #
#                   -irc                              $irc\                            #
#                   -itc                              $itc\                            #
#                   -fov                              $fov\                            #
#                   -pov                              $pov\                            #
#                   -tct                              $tct\                            #
#                   -phase_offset_tx                  $phase_offset_tx\                #
#                   -phase_offset_tx_timestamp_sec    $phase_offset_tx_timestamp_sec\  #
#                   -phase_offset_tx_timestamp_ns     $phase_offset_tx_timestamp_ns\   #
#                   -freq_offset_tx                   $freq_offset_tx\                 #
#                   -freq_offset_tx_timestamp_sec     $freq_offset_tx_timestamp_sec\   #
#                   -freq_offset_tx_timestamp_ns      $freq_offset_tx_timestamp_ns\    #
#                   -count                             $count\                         #
#                   -interval                          $interval                       #
#                                                                                      #
########################################################################################

proc pltLib::send_ptp_ha_signal {args} {

    array set param $args

    #INPUT PARAMETERS

    #Session identifier
    if {[info exists param(-session_id)]} {

        set session_id $param(-session_id)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_signal: Missing Argument -> session_id"
        return 0
    }

    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_signal: Missing Argument -> port_num"
        return 0
    }

    #ETHERNET HEADER OPTIONS

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_signal: Missing Argument -> dest_mac"
        return 0
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_signal: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
    } else {

        set eth_type 0800
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
    } else {

        set vlan_id 0
    }

    #Vlan Priority
    if {[info exists param(-vlan_priority)]} {

        set vlan_priority $param(-vlan_priority)
    } else {

        set vlan_priority 0
    }

    #IP HEADER OPTIONS

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
    } else {

        set dest_ip 224.0.1.129
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
    } else {

        set src_ip 0.0.0.0
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
    } else {

        set ip_checksum 0
    }

    #UDP HEADER OPTIONS

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
    } else {

        set dest_port 319
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
    } else {

        set src_port 319
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
    } else {

        set udp_length 0
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
    } else {

        set udp_checksum 0
    }

    #PTP HEADER OPTIONS

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
    } else {

        set major_sdo_id 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
    } else {

        set message_type 0
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
    } else {

        set minor_version 2
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
    } else {

        set message_length 44
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
    } else {

        set domain_number 0
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
    } else {

        set minor_sdo_id 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
    } else {

        set correction_field 0
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
    } else {

        set message_type_specific 0
    }

    #Source port number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
    } else {

        set src_port_number 0
    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
    } else {

        set src_clock_identity 0
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
    } else {

        set sequence_id 0
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
    } else {

        set control_field 00
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
    } else {

        set log_message_interval 0
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
    } else {

        set unicast_flag 0
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
    } else {

        set profile_specific2 0
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
    } else {

        set secure 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
    } else {

        set freq_traceable 0
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
    } else {

        set sync_uncertain 0
    }

    if {[info exists param(-target_port_number)]} {

        set target_port_number $param(-target_port_number)
    } else {

        set target_port_number 0
    }

    if {[info exists param(-target_clock_identity)]} {

        set target_clock_identity $param(-target_clock_identity)
    } else {

        set target_clock_identity 0
    }

    if {[info exists param(-tlv)]} {

        set tlv $param(-tlv)
    } else {

        set tlv L1_SYNC
    }

    if {[info exists param(-tlv_type)]} {

        set tlv_type $param(-tlv_type)
    } else {

        set tlv_type 32769
    }

    if {[info exists param(-tlv_length)]} {

        set tlv_length $param(-tlv_length)
    } else {

        set tlv_length 30
    }

    if {[info exists param(-ope)]} {

        set ope $param(-ope)
    } else {

        set ope 0
    }

    if {[info exists param(-cr)]} {

        set cr $param(-cr)
    } else {

        set cr 0
    }

    if {[info exists param(-rcr)]} {

        set rcr $param(-rcr)
    } else {

        set rcr 0
    }

    if {[info exists param(-tcr)]} {

        set tcr $param(-tcr)
    } else {

        set tcr 0
    }

    if {[info exists param(-ic)]} {

        set ic $param(-ic)
    } else {

        set ic 0
    }

    if {[info exists param(-irc)]} {

        set irc $param(-irc)
    } else {

        set irc 0
    }

    if {[info exists param(-itc)]} {

        set itc $param(-itc)
    } else {

        set itc 0
    }

    if {[info exists param(-fov)]} {

        set fov $param(-fov)
    } else {

        set fov 0
    }

    if {[info exists param(-pov)]} {

        set pov $param(-pov)
    } else {

        set pov 0
    }

    if {[info exists param(-tct)]} {

        set tct $param(-tct)
    } else {

        set tct 0
    }

    if {[info exists param(-phase_offset_tx)]} {

        set phase_offset_tx $param(-phase_offset_tx)
    } else {

        set phase_offset_tx 0
    }

    if {[info exists param(-phase_offset_tx_timestamp_sec)]} {

        set phase_offset_tx_timestamp_sec $param(-phase_offset_tx_timestamp_sec)
    } else {

        set phase_offset_tx_timestamp_sec 0
    }

    if {[info exists param(-phase_offset_tx_timestamp_ns)]} {

        set phase_offset_tx_timestamp_ns $param(-phase_offset_tx_timestamp_ns)
    } else {

        set phase_offset_tx_timestamp_ns 0
    }

    if {[info exists param(-freq_offset_tx)]} {

        set freq_offset_tx $param(-freq_offset_tx)
    } else {

        set freq_offset_tx 0
    }

    if {[info exists param(-freq_offset_tx_timestamp_sec)]} {

        set freq_offset_tx_timestamp_sec $param(-freq_offset_tx_timestamp_sec)
    } else {

        set freq_offset_tx_timestamp_sec 0
    }

    if {[info exists param(-freq_offset_tx_timestamp_ns)]} {

        set freq_offset_tx_timestamp_ns $param(-freq_offset_tx_timestamp_ns)
    } else {

        set freq_offset_tx_timestamp_ns 0
    }

    #MISCELLANEOUS
    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
    } else {

        set interval 1
    }

    #Interval in millisec
    set interval [expr int($interval*1000)]

    #CONSTRUCT PTP HA PEER DELAY PACKET DUMP
    if {![pbLib::encode_ptp_ha_pkt\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -major_sdo_id                      $major_sdo_id\
                -message_type                      $message_type\
                -minor_version                     $minor_version\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -minor_sdo_id                      $minor_sdo_id\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -secure                            $secure\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_tracaeble                    $time_traceable\
                -freq_tracaeble                    $freq_traceable\
                -sync_uncertain                    $sync_uncertain\
                -correction_field                  $correction_field\
                -message_type_specific             $message_type_specific\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -target_port_number                $target_port_number\
                -target_clock_identity             $target_clock_identity\
                -tlv                               $tlv\
                -tlv_type                          $tlv_type\
                -tlv_length                        $tlv_length\
                -ope                               $ope\
                -cr                                $cr\
                -rcr                               $rcr\
                -tcr                               $tcr\
                -ic                                $ic\
                -irc                               $irc\
                -itc                               $itc\
                -fov                               $fov\
                -pov                               $pov\
                -tct                               $tct\
                -phase_offset_tx                   $phase_offset_tx\
                -phase_offset_tx_timestamp_sec     $phase_offset_tx_timestamp_sec\
                -phase_offset_tx_timestamp_ns      $phase_offset_tx_timestamp_ns\
                -freq_offset_tx                    $freq_offset_tx\
                -freq_offset_tx_timestamp_sec      $freq_offset_tx_timestamp_sec\
                -freq_offset_tx_timestamp_ns       $freq_offset_tx_timestamp_ns\
                -hexdump                           hexdump]} {

        return 0
    }

    pltLib::send_periodic\
                     -session_id     $session_id\
                     -port_num       $port_num\
                     -data           $hexdump\
                     -instance       9\
                     -interval       $interval\
                     -count          $count\
                     -handler_id     "PTP_HA"

    return 1

}

#10.
########################################################################################
#                                                                                      #
#  PROCEDURE NAME                : pltLib::send_ptp_ha_mgmt                            #
#                                                                                      #
#  INPUT PARAMETERS              :                                                     #
#                                                                                      #
#   session_id                   :  (Mandatory) Session identifier                     #
#                                                                                      #
#   port_num                     :  (Mandatory) Port number on which frame is to send. #
#                                               (e.g., 0/1)                            #
#                                                                                      #
#   dest_mac                     :  (Mandatory) Destination MAC dddress.               #
#                                               (e.g., 01:80:C2:00:00:20)              #
#                                                                                      #
#   src_mac                      :  (Mandatory) Source MAC address.                    #
#                                               (e.g., 00:80:C2:00:00:21)              #
#                                                                                      #
#   eth_type                     :  (Optional)  EtherType (0800 or 88F7)               #
#                                               (Default : 0800)                       #
#                                                                                      #
#   vlan_id                      :  (Optional)  Vlan ID                                #
#                                               (Default : 0)                          #
#                                                                                      #
#   vlan_priority                :  (Optional)  Vlan Priority                          #
#                                               (Default : 0)                          #
#                                                                                      #
#   dest_ip                      :  (Optional)  Destination IP address                 #
#                                               (Default : 224.0.1.129)                #
#                                                                                      #
#   src_ip                       :  (Optional)  Source IP address                      #
#                                               (Default : 0.0.0.0)                    #
#                                                                                      #
#   ip_checksum                  :  (Optional)  IP checksum                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   dest_port                    :  (Optional)  UDP destination port number            #
#                                               (Default : 319)                        #
#                                                                                      #
#   src_port                     :  (Optional)  UDP source port number                 #
#                                               (Default : 319)                        #
#                                                                                      #
#   udp_length                   :  (Optional)  UDP length                             #
#                                               (Default : 0)                          #
#                                                                                      #
#   udp_checksum                 :  (Optional)  UDP checksum                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   major_sdo_id                 :  (Optional)  Major sdo id                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   message_type                 :  (Optional)  Message Type                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   minor_version                :  (Optional)  PTP version number                     #
#                                               (Default : 2)                          #
#                                                                                      #
#   version_ptp                  :  (Optional)  PTP minor version number               #
#                                               (Default : 2)                          #
#                                                                                      #
#   message_length               :  (Optional)  PTP Sync message length                #
#                                               (Default : 44)                         #
#                                                                                      #
#   domain_number                :  (Optional)  Domain number                          #
#                                               (Default : 0)                          #
#                                                                                      #
#   minor_sdo_id                 :  (Optional)  Minor Sdo id                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   alternate_master_flag        :  (Optional)  Alternate Master flag                  #
#                                               (Default : 0)                          #
#                                                                                      #
#   two_step_flag                :  (Optional)  Two Step Flag                          #
#                                               (Default : 0)                          #
#                                                                                      #
#   unicast_flag                 :  (Optional)  Unicast Flag                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   profile_specific1            :  (Optional)  Profile specific1 Flag                 #
#                                               (Default : 0)                          #
#                                                                                      #
#   profile_specific2            :  (Optional)  Profile specific2 Flag                 #
#                                               (Default : 0)                          #
#                                                                                      #
#   secure                       :  (Optional)  Secure Flag                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   leap61                       :  (Optional)  Leap61 Flag                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   leap59                       :  (Optional)  Leap59 Flag                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   current_utc_offset_valid     :  (Optional)  Current UTC Offset Flag                #
#                                               (Default : 0)                          #
#                                                                                      #
#   ptp_timescale                :  (Optional)  PTP TimeSclae Flag                     #
#                                               (Default : 0)                          #
#                                                                                      #
#   time_tracaeble               :  (Optional)  Time Traceable Flag                    #
#                                               (Default : 0)                          #
#                                                                                      #
#   freq_tracaeble               :  (Optional)  Frequency Traceable Flag               #
#                                               (Default : 0)                          #
#                                                                                      #
#   sync_uncertain               :  (Optional)  Synchronization uncertain Flag         #
#                                               (Default : 0)                          #
#                                                                                      #
#   correction_field             :  (Optional)  Correction Field value                 #
#                                               (Default : 0)                          #
#                                                                                      #
#   message_type_specific        :  (Optional)  Message type                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   src_port_number              :  (Optional)  Source port number                     #
#                                               (Default : 0)                          #
#                                                                                      #
#   src_clock_identity           :  (Optional)  Source Clock identity                  #
#                                               (Default : 0)                          #
#                                                                                      #
#   sequence_id                  :  (Optional)  Sequence ID                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   control_field                :  (Optional)  Control field                          #
#                                               (Default : 00)                         #
#                                                                                      #
#   log_message_interval         :  (Optional)  Log message interval                   #
#                                               (Default : 0)                          #
#                                                                                      #
#   target_port_number            : (Optional)  Target Port Number                     #
#                                               (Default : 0)                          #
#                                                                                      #
#   target_clock_identity         : (Optional)  Target Clock Identity                  #
#                                               (Default : 0)                          #
#                                                                                      #
#   starting_boundary_hops        : (Optional)  Starting Boundary Hops                 #
#                                               (Default : 0)                          #
#                                                                                      #
#   boundary_hops                 : (Optional)  Boundary Hops                          #
#                                               (Default : 0)                          #
#                                                                                      #
#   action_field                  : (Optional)  Action Field                           #
#                                               (Default : 0)                          #
#                                                                                      #
#   tlv                           : (Optional)  Tlv                                    #
#                                               (Default : MASTER_ONLY)                #
#                                                                                      #
#   tlv_type                      : (Optional)  Tlv Type                               #
#                                               (Default : 1)                          #
#                                                                                      #
#   tlv_length                    : (Optional)  Tlv Length                             #
#                                               (Default : 4)                          #
#                                                                                      #
#   management_id                 : (Optional)  Management Id                          #
#                                               (Default : 12288)                      #
#                                                                                      #
#   ext_port_config               : (Optional)  Ext Port Config                        #
#                                               (Default : 0)                          #
#                                                                                      #
#   master_only                   : (Optional)  Master Only                            #
#                                               (Default : 0)                          #
#                                                                                      #
#   count                         :  (Optional)  Frame count                           #
#                                                (Default : 1)                         #
#                                                                                      #
#   interval                      :  (Optional)  Frame interval (in seconds)           #
#                                                (Default : 1)                         #
#                                                                                      #
#  OUTPUT PARAMETERS              :  NONE                                              #
#                                                                                      #
#  RETURNS                        :  1 on success (If PTP HA delay response is sent    #
#                                                 with the given parameters)           #
#                                                                                      #
#                                                                                      #
#                                 :  0 on success (If PTP HA delay resp could not be   #
#                                                  sent with the given parameters)     #
#                                                                                      #
#  DEFINITION                     : This function is used to construct and send PTP    #
#                                   HA delay request message.                          #
#                                                                                      #
#  USAGE                          :                                                    #
#                                                                                      #
#       pltLib::send_ptp_ha_mgmt\                                                      #
#                   -session_id                        $session_id\                    #
#                   -port_num                          $port_num\                      #
#                   -dest_mac                          $dest_mac\                      #
#                   -src_mac                           $src_mac\                       #
#                   -eth_type                          $eth_type\                      #
#                   -vlan_id                           $vlan_id\                       #
#                   -vlan_priority                     $vlan_priority\                 #
#                   -dest_ip                           $dest_ip\                       #
#                   -src_ip                            $src_ip\                        #
#                   -ip_checksum                       $ip_checksum\                   #
#                   -dest_port                         $dest_port\                     #
#                   -src_port                          $src_port\                      #
#                   -udp_length                        $udp_length\                    #
#                   -udp_checksum                      $udp_checksum\                  #
#                   -major_sdo_id                      $major_sdo_id\                  #
#                   -message_type                      $message_type\                  #
#                   -minor_version                     $minor_version\                 #
#                   -version_ptp                       $version_ptp\                   #
#                   -message_length                    $message_length\                #
#                   -domain_number                     $domain_number\                 #
#                   -minor_sdo_id                      $minor_sdo_id\                  #
#                   -alternate_master_flag             $alternate_master_flag\         #
#                   -two_step_flag                     $two_step_flag\                 #
#                   -unicast_flag                      $unicast_flag\                  #
#                   -profile_specific1                 $profile_specific1\             #
#                   -profile_specific2                 $profile_specific2\             #
#                   -secure                            $secure\                        #
#                   -leap61                            $leap61\                        #
#                   -leap59                            $leap59\                        #
#                   -current_utc_offset_valid         $current_utc_offset_valid\       #
#                   -ptp_timescale                     $ptp_timescale\                 #
#                   -time_tracaeble                    $time_traceable\                #
#                   -freq_tracaeble                    $freq_traceable\                #
#                   -sync_uncertain                    $sync_uncertain\                #
#                   -correction_field                  $correction_field\              #
#                   -message_type_specific             $message_type_specific\         #
#                   -src_port_number                   $src_port_number\               #
#                   -src_clock_identity                $src_clock_identity\            #
#                   -sequence_id                       $sequence_id\                   #
#                   -control_field                     $control_field\                 #
#                   -log_message_interval              $log_message_interval\          #
#                   -origin_timestamp_sec              $origin_timestamp_sec\          #
#                   -origin_timestamp_ns               $origin_timestamp_ns\           #
#                   -current_utc_offset_valid         $current_utc_offset_valid\       #
#                   -gm_priority1                      $gm_priority1\                  #
#                   -gm_priority2                      $gm_priority2\                  #
#                   -gm_identity                       $gm_identity\                   #
#                   -steps_removed                     $steps_removed\                 #
#                   -gm_clock_classy                   $gm_clock_classy\               #
#                   -gm_clock_accuracy                 $gm_clock_accuracy\             #
#                   -gm_clock_variance                 $gm_clock_variance\             #
#                   -target_port_number                $target_port_number\            #
#                   -target_clock_identity             $target_clock_identity\         #
#                   -starting_boundary_hops            $starting_boundary_hops\        #
#                   -boundary_hops                     $boundary_hops\                 #
#                   -action_field                      $action_field\                  #
#                   -tlv                               $tlv\                           #
#                   -tlv_type                          $tlv_type\                      #
#                   -tlv_length                        $tlv_length\                    #
#                   -management_id                     $management_id\                 #
#                   -ext_port_config                   $ext_port_config\               #
#                   -master_only                       $master_only\                   #
#                   -count                             $count\                         #
#                   -interval                          $interval                       #
#                                                                                      #
########################################################################################

proc pltLib::send_ptp_ha_mgmt {args} {

    array set param $args

    #INPUT PARAMETERS

    #Session identifier
    if {[info exists param(-session_id)]} {

        set session_id $param(-session_id)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_mgmt: Missing Argument -> session_id"
        return 0
    }

    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_mgmt: Missing Argument -> port_num"
        return 0
    }

    #ETHERNET HEADER OPTIONS

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_mgmt: Missing Argument -> dest_mac"
        return 0
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
    } else {

        ERRLOG -msg "pltLib::send_ptp_ha_mgmt: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
    } else {

        set eth_type 0800
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
    } else {

        set vlan_id 0
    }

    #Vlan Priority
    if {[info exists param(-vlan_priority)]} {

        set vlan_priority $param(-vlan_priority)
    } else {

        set vlan_priority 0
    }

    #IP HEADER OPTIONS

    #Destination IP address
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
    } else {

        set dest_ip 224.0.1.129
    }

    #Source IP address
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
    } else {

        set src_ip 0.0.0.0
    }

    #IP checksum
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
    } else {

        set ip_checksum 0
    }

    #UDP HEADER OPTIONS

    #UDP destination port number
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
    } else {

        set dest_port 319
    }

    #UDP source port number
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
    } else {

        set src_port 319
    }

    #UDP length
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
    } else {

        set udp_length 0
    }

    #UDP checksum
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
    } else {

        set udp_checksum 0
    }

    #PTP HEADER OPTIONS

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
    } else {

        set major_sdo_id 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
    } else {

        set message_type 0
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
    } else {

        set minor_version 2
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
    } else {

        set message_length 44
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
    } else {

        set domain_number 0
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
    } else {

        set minor_sdo_id 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
    } else {

        set correction_field 0
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
    } else {

        set message_type_specific 0
    }

    #Source port number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
    } else {

        set src_port_number 0
    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
    } else {

        set src_clock_identity 0
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
    } else {

        set sequence_id 0
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
    } else {

        set control_field 00
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
    } else {

        set log_message_interval 0
    }

    #PTP HEADER - FLAGS
    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
    } else {

        set unicast_flag 0
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
    } else {

        set profile_specific2 0
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
    } else {

        set secure 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
    } else {

        set freq_traceable 0
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
    } else {

        set sync_uncertain 0
    }

    if {[info exists param(-target_port_number)]} {

        set target_port_number $param(-target_port_number)
    } else {

        set target_port_number 0
    }

    if {[info exists param(-target_clock_identity)]} {

        set target_clock_identity $param(-target_clock_identity)
    } else {

        set target_clock_identity 0
    }

    if {[info exists param(-starting_boundary_hops)]} {

        set starting_boundary_hops $param(-starting_boundary_hops)
    } else {

        set starting_boundary_hops 0
    }

    if {[info exists param(-boundary_hops)]} {

        set boundary_hops $param(-boundary_hops)
    } else {

        set boundary_hops 0
    }

    if {[info exists param(-action_field)]} {

        set action_field $param(-action_field)
    } else {

        set action_field 0
    }

    if {[info exists param(-tlv)]} {

        set tlv $param(-tlv)
    } else {

        set tlv MASTER_ONLY
    }

    if {[info exists param(-tlv_type)]} {

        set tlv_type $param(-tlv_type)
    } else {

        set tlv_type 1
    }

    if {[info exists param(-tlv_length)]} {

        set tlv_length $param(-tlv_length)
    } else {

        set tlv_length 4
    }

    if {[info exists param(-management_id)]} {

        set management_id $param(-management_id)
    } else {

        set management_id 12288
    }

    if {[info exists param(-ext_port_config)]} {

        set ext_port_config $param(-ext_port_config)
    } else {

        set ext_port_config 0
    }

    if {[info exists param(-master_only)]} {

        set master_only $param(-master_only)
    } else {

        set master_only 0
    }
    #MISCELLANEOUS
    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
    } else {

        set interval 1
    }

    #Interval in millisec
    set interval [expr int($interval*1000)]

    #CONSTRUCT PTP HA PEER DELAY PACKET DUMP
    if {![pbLib::encode_ptp_ha_pkt\
                -port_num                          $port_num\
                -dest_mac                          $dest_mac\
                -src_mac                           $src_mac\
                -eth_type                          $eth_type\
                -vlan_id                           $vlan_id\
                -vlan_priority                     $vlan_priority\
                -dest_ip                           $dest_ip\
                -src_ip                            $src_ip\
                -ip_checksum                       $ip_checksum\
                -dest_port                         $dest_port\
                -src_port                          $src_port\
                -udp_length                        $udp_length\
                -udp_checksum                      $udp_checksum\
                -major_sdo_id                      $major_sdo_id\
                -message_type                      $message_type\
                -minor_version                     $minor_version\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -minor_sdo_id                      $minor_sdo_id\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -secure                            $secure\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_tracaeble                    $time_traceable\
                -freq_tracaeble                    $freq_traceable\
                -sync_uncertain                    $sync_uncertain\
                -correction_field                  $correction_field\
                -message_type_specific             $message_type_specific\
                -src_port_number                   $src_port_number\
                -src_clock_identity                $src_clock_identity\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -target_port_number                $target_port_number\
                -target_clock_identity             $target_clock_identity\
                -starting_boundary_hops            $starting_boundary_hops\
                -boundary_hops                     $boundary_hops\
                -action_field                      $action_field\
                -tlv                               $tlv\
                -tlv_type                          $tlv_type\
                -tlv_length                        $tlv_length\
                -management_id                     $management_id\
                -ext_port_config                   $ext_port_config\
                -master_only                       $master_only\
                -hexdump                           hexdump]} {

        return 0
    }

    pltLib::send_periodic\
                     -session_id     $session_id\
                     -port_num       $port_num\
                     -data           $hexdump\
                     -instance       10\
                     -interval       $interval\
                     -count          $count\
                     -handler_id     "PTP_HA"

    return 1

}

#################################################################################
#                                                                               #
#  PROCEDURE NAME         : pltLib::send_arp                                    #
#                                                                               #
#  INPUT PARAMETERS       :                                                     #
#                                                                               #
#   session_id            : (Mandatory) Session identifier                      #
#                                                                               #
#   port_num              : (Mandatory) Port number on which frame is to send.  #
#                                       (e.g., 0/1)                             #
#                                                                               #
#   dest_mac              : (Mandatory) Destination MAC dddress.                #
#                                       (e.g., FF:FF:FF:FF:FF:FF)               #
#                                                                               #
#   src_mac               : (Mandatory) Source MAC address.                     #
#                                       (e.g., 00:80:C2:00:00:21)               #
#                                                                               #
#   eth_type              : (Optional)  EtherType                               #
#                                       (Default : 0806)                        #
#                                                                               #
#   vlan_id               : (Optional)  Vlan ID                                 #
#                                       (Default : 0)                           #
#                                                                               #
#   vlan_priority         : (Optional)  Vlan Priority                           #
#                                       (Default : 0)                           #
#                                                                               #
#   vlan_dei              : (Optional)  Vlan DEI Bit                            #
#                                       (Default : 0)                           #
#                                                                               #
#   hardware_type         : (Optional) Specifies the network protocol type      #
#                                      (Default : 1)                            #
#                                                                               #
#   protocol_type         : (Optional) Specifies the internetwork protocol for  #
#                                      which the ARP request is intended.       #
#                                      (Default : 0800)                         #
#                                                                               #
#   hardware_size         : (Optional) Length of a hardware address             #
#                                      (Default : 6)                            #
#                                                                               #
#   protocol_size         : (Optional) Length addresses used in the upper layer #
#                                      protocol. (Default : 4)                  #
#                                                                               #
#   message_type          : (Optional) Specifies the operation that the sender  #
#                                      is performing: 1 - request, 2 - reply.   #
#                                      (Default : 1)                            #
#                                                                               #
#   sender_mac            : (Optional) Sender hardware address                  #
#                                      (Default : 00:00:00:00:00:00)            #
#                                                                               #
#   sender_ip             : (Optional) Sender protocol address                  #
#                                      (Default : 0.0.0.0)                      #
#                                                                               #
#   target_mac            : (Optional) Target hardware address                  #
#                                      (Default : 00:00:00:00:00:00)            #
#                                                                               #
#   target_ip             : (Optional) Target protocol address                  #
#                                      (Default : 0.0.0.0)                      #
#                                                                               #
#   count                 : (Optional) Frame count                              #
#                                      (Default : 1)                            #
#                                                                               #
#   interval              : (Optional) Frame interval (in seconds)              #
#                                      (Default : 1)                            #
#                                                                               #
#  OUTPUT PARAMETERS      :  NONE                                               #
#                                                                               #
#  RETURNS                :  1 on success (If ARP packet is sent with the       #
#                                          given parameters)                    #
#                                                                               #
#                                                                               #
#                         :  0 on success (If ARP packet could not be           #
#                                          sent with the given parameters)      #
#                                                                               #
#  DEFINITION             : This function is used to construct and send ARP     #
#                           packet.                                             #
#                                                                               #
#  USAGE                  :                                                     #
#                                                                               #
#       pltLib::send_arp\                                                       #
#                   -session_id                        $session_id\             #
#                   -port_num                          $port_num\               #
#                   -dest_mac                          $dest_mac\               #
#                   -src_mac                           $src_mac\                #
#                   -eth_type                          $eth_type\               #
#                   -vlan_id                           $vlan_id\                #
#                   -vlan_priority                     $vlan_priority\          #
#                   -hardware_type                     $hardware_type\          #
#                   -protocol_type                     $protocol_type\          #
#                   -hardware_size                     $hardware_size\          #
#                   -protocol_size                     $protocol_size\          #
#                   -message_type                      $message_type\           #
#                   -sender_mac                        $sender_mac\             #
#                   -sender_ip                         $sender_ip\              #
#                   -target_mac                        $target_mac\             #
#                   -target_ip                         $target_ip\              #
#                   -count                             $count\                  #
#                   -interval                          $interval                #
#                                                                               #
#################################################################################

proc pltLib::send_arp {args} {

    array set param $args

    #INPUT PARAMETERS

    #Session identifier
    if {[info exists param(-session_id)]} {

        set session_id $param(-session_id)
    } else {

        ERRLOG -msg "pltLib::send_arp: Missing Argument -> session_id"
        return 0
    }

    #Port number on which frame is to send
    if {[info exists param(-port_num)]} {

        set port_num $param(-port_num)
    } else {

        ERRLOG -msg "pltLib::send_arp: Missing Argument -> port_num"
        return 0
    }

    #Destination MAC dddress
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
    } else {

        ERRLOG -msg "pltLib::send_arp: Missing Argument -> dest_mac"
        return 0
    }

    #Source MAC address
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
    } else {

        ERRLOG -msg "pltLib::send_arp: Missing Argument -> src_mac"
        return 0
    }

    #EtherType
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
    } else {

        set eth_type 0806
    }

    #Vlan ID
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
    } else {

        set vlan_id 0
    }

    #Vlan Priority
    if {[info exists param(-vlan_priority)]} {

        set vlan_priority $param(-vlan_priority)
    } else {

        set vlan_priority 0
    }

    #Vlan Dei
    if {[info exists param(-vlan_dei)]} {

        set vlan_dei $param(-vlan_dei)
    } else {

        set vlan_dei 0
    }

    #Specifies the network protocol type
    if {[info exists param(-hardware_type)]} {

        set hardware_type $param(-hardware_type)
    } else {

        set hardware_type 1
    }

    #Specifies the internetwork protocol for
    if {[info exists param(-protocol_type)]} {

        set protocol_type $param(-protocol_type)
    } else {

        set protocol_type 0800
    }

    #Length of a hardware address
    if {[info exists param(-hardware_size)]} {

        set hardware_size $param(-hardware_size)
    } else {

        set hardware_size 6
    }

    #Length addresses used in the upper layer
    if {[info exists param(-protocol_size)]} {

        set protocol_size $param(-protocol_size)
    } else {

        set protocol_size 4
    }

    #Specifies the operation that the sender
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
    } else {

        set message_type 1
    }

    #Sender hardware address
    if {[info exists param(-sender_mac)]} {

        set sender_mac $param(-sender_mac)
    } else {

        set sender_mac 00:00:00:00:00:00
    }

    #Sender protocol address
    if {[info exists param(-sender_ip)]} {

        set sender_ip $param(-sender_ip)
    } else {

        set sender_ip 0.0.0.0
    }

    #Target hardware address
    if {[info exists param(-target_mac)]} {

        set target_mac $param(-target_mac)
    } else {

        set target_mac 00:00:00:00:00:00
    }

    #Target protocol address
    if {[info exists param(-target_ip)]} {

        set target_ip $param(-target_ip)
    } else {

        set target_ip 0.0.0.0
    }

    #Frame count
    if {[info exists param(-count)]} {

        set count $param(-count)
    } else {

        set count 1
    }

    #Frame interval
    if {[info exists param(-interval)]} {

        set interval $param(-interval)
    } else {

        set interval 1
    }

    #Interval in millisec
    set interval [expr int($interval*1000)]

    #Construct ARP Packet
    if {![pbLib::encode_arp_pkt\
                     -dest_mac        $dest_mac\
                     -src_mac         $src_mac\
                     -eth_type        $eth_type\
                     -hardware_type   $hardware_type\
                     -protocol_type   $protocol_type\
                     -hardware_size   $hardware_size\
                     -protocol_size   $protocol_size\
                     -message_type    $message_type\
                     -sender_mac      $sender_mac\
                     -sender_ip       $sender_ip\
                     -target_mac      $target_mac\
                     -target_ip       $target_ip\
                     -hexdump         packet]} {

        return 0
    }

    #Add VLAN Tag
    if {[string is integer $vlan_id] && ($vlan_id >= 0) && ($vlan_id <= 4095) } {
        set packet [pbLib::push_vlan_tag\
                        -packet          $packet\
                        -vlan_id         $vlan_id\
                        -vlan_priority   $vlan_priority]
    }

    pltLib::send_periodic\
                     -session_id     $session_id\
                     -port_num       $port_num\
                     -data           $packet\
                     -instance       11\
                     -interval       $interval\
                     -count          $count\
                     -handler_id     "DEFAULT"

    return 1

}

#################################################################################
#  PROCEDURE NAME    : pltLib::ptp-ha_psd_name                                  #
#                                                                               #
#  DEFINITION        : This function is used to get the psd name from the       #
#                      hexdump                                                  #
#                                                                               #
#  INPUT PARAMETERS  :                                                          #
#                                                                               #
#  dump              : hex dump                                                 #
#                                                                               #
#  RETURNS           : packet name if hexdump is WRPTP packet or Unknown        # 
#                                                                               #
#  USAGE             :                                                          #
#                                                                               #
#                       pltLib::ptp-ha_psd_name\                                #
#                           -hexdump                 $dump                      #
#                                                                               #
#################################################################################
proc pltLib::ptp-ha_psd_name {args} {

    array set param $args

    set packet $param(-hexdump)
    set type $param(-type)
    set port $param(-port)

    set pkt_name ""

    if {![pbLib::decode_ptp_ha_pkt\
            -packet                         $packet\
            -vlan_id                        vlan_id\
            -ip_protocol                    ip_protocol\
            -src_port                       src_port\
            -dest_port                      dest_port\
            -message_type                   message_type\
            -version_ptp                    version_ptp\
            -tlv_type                       tlv_type]} {
        return "Unknown"
    }

    if {$vlan_id != ""} {
        lappend pkt_name "VLAN"
    }

    if {$ip_protocol != ""} {
        lappend pkt_name "IP"
    }

    if {$message_type != ""} {
        switch $message_type {
            0  {lappend pkt_name Sync          }
            1  {lappend pkt_name Delay_Req     }
            2  {lappend pkt_name Pdelay_Req    }
            3  {lappend pkt_name Pdelay_Resp   }
            8  {lappend pkt_name Follow_Up     }
            9  {lappend pkt_name Delay_Resp    }
            10 {lappend pkt_name Pdelay_Resp_Follow_Up }
            11 {lappend pkt_name Announce      }
            12 {
                if {$tlv_type == 0x8001} {
                    lappend pkt_name L1Sync
                } else {
                    lappend pkt_name Signaling
                }
            }
            13 {lappend pkt_name Management    }
            default {lappend pkt_name Reserved }
        }
    }

    if {$type == "R"} {
        switch $message_type {
            1  {
                   after 0 ptp_ha::respond_to_delay_requests\
                                             -request    $packet\
                                             -port_num   $port
            }
            2  {
                   after 0 ptp_ha::respond_to_pdelay_requests\
                                             -request    $packet\
                                             -port_num   $port
            }
        }
    }

    return [join $pkt_name +]

}


#################################################################################
#  PROCEDURE NAME    : pltLib::create_filter_ptp_ha_pkt                         #
#                                                                               #
#  DEFINITION        : This function is used to create filter to receive PTP    #
#                      packet                          .                        #
#                                                                               #
#  INPUT PARAMETERS  :                                                          #
#                                                                               #
#  session_id        : (Optional)  session id to handle session with the Tee    #
#                                                                               #
#  port_num          : (Mandatory) Port Number in which filter is created       #
#                                                                               #
#  vlan_id           : (Mandatory)  VLAN ID                                     #
#                                                                               #
#  src   _mac        : (Mandatory)  source MAC                                  #
#                                                                               #
#  src_port          : (Mandatory)  UDP source port                             #
#                                                                               #
#  USAGE             :                                                          #
#                                                                               #
#                       pltLib::create_filter_ptp_ha_pkt\                       #
#                           -session_id              $session_id\               #
#                           -port_num                $port_num\                 #
#                           -source_mac              $source_mac\               #
#                           -ethertype               $ether_type                #
#                           -vlan_id                 $vlan_id\                  #
#                           -ids                     ids                        #
#                                                                               #
#################################################################################

proc pltLib::create_filter_ptp_ha_pkt {args} {

    array set param $args
    set message_type_flag 0

    if {[info exists param(-session_id)]} {
        set session_id $param(-session_id)
    } else {
        set session_id $::session_id
    }

    if {[info exists param(-port_num)]} {
        set port_num $param(-port_num)
    } else {
        ERRLOG -msg "pltLib::create_filter_ptp_ha_pkt : Missing Argument -> Port Number\n"
        return 0
    }

    if {[info exists param(-vlan_id)]} {
        set vlan_id $param(-vlan_id)
    } else {
        set vlan_id 4096
    }

    if {[info exists param(-source_mac)]} {
        set source_mac $param(-source_mac)
    } else {
        set source_mac "00:00:00:00:00:00"
    }

    if {[info exists param(-ether_type)]} {
        set ether_type $param(-ether_type)
    } else {
        ERRLOG -msg "pltLib::create_filter_ptp_ha_pkt : Missing Argument ->Ether Type \n"
        return 0
    }

    if {[info exists param(-message_type)]} {
        set message_type $param(-message_type)
        set message_type_flag 1
    } else {
        set message_type 0
    }

    set message_type        [format %.2x $message_type]
    set message_type_len    [llength $message_type]
    set message_type1        ""

    for {set i $message_type_len} {$i < 4} {incr i} {
        append message_type1 0
    }
    append message_type $message_type1

    if {[info exists param(-ids)]} {
        upvar $param(-ids) ids
    } else {
        ERRLOG -msg "pltLib::create_filter_ptp_ha_pkt : Missing Argument -> ids\n"
        return 0
    }

    set vlan_id        [format %x $vlan_id]
    set vlanidlen      [llength [split $vlan_id ""]]
    set vlanid1        ""

    for {set i $vlanidlen} {$i < 4} {incr i} {
        append vlanid1 0
    }

    append vlanid1 $vlan_id

    set source_mac       [join [split $source_mac ":"] ""]
    set filter_mode      on
    set pkt_to_keep      FILTER
    set bytes_to_keep    0
    set capture_flag     on
    set ether_type       $ether_type

    if {$ether_type == "88F7"} {
        if {$::ptp_ha_vlan_encap == "false" || $vlan_id == 0} {
            if {$::ptp_trans_type == "IEEE 802.3"} {
                set mask                " FFFF0000 FFFF0000"
                set position            " 12 12"
                set value               " $ether_type 8100"
                set match_term_id       " 1 2"
                set filter_id           1
                set filter_condition    " 1 | 2"
            } else {
                if { $::ptp_comm_model == "Multicast" } {

                    set mask                " FFFF0000 "
                    set position            " 12 "
                    set value               " 0800 "
                    set match_term_id       " 1 "
                    set filter_id           1
                    set filter_condition    "1"

                } else {
                    set mask                " FFFF0000 FFFF0000"
                    set position            " 12  12"
                    set value               " 0800 0806"
                    set match_term_id       " 1 2"
                    set filter_id           1
                    set filter_condition    " 1 | 2"
                }
            }
        } else {
            if {$::ptp_trans_type == "IEEE 802.3"} {
                set mask                " FFFF0000 0FFF0000 FFFF0000"
                set position            " 12 14 16"
                set value               " 8100 $vlanid1 88F7"
                set match_term_id       " 1 2 3"
                set filter_id           1
                set filter_condition    "[join [string trim $match_term_id] " & "]"
            } else {
                if { $::ptp_comm_model == "Multicast" } {
                    set mask                " FFFF0000 0FFF0000 FFFF0000 "
                    set position            " 12 14 16 "
                    set value               " 8100 $vlanid1 0800"
                    set match_term_id       " 1 2 3 "
                    set filter_id           1
                    set filter_condition    "1 & 2 & 3"
                } else {
                    set mask                " FFFF0000 0FFF0000 FFFF0000 FFFF0000"
                    set position            " 12 14 16 16"
                    set value               " 8100 $vlanid1 0800 0806"
                    set match_term_id       " 1 2 3 4"
                    set filter_id           1
                    set filter_condition    " ( 1|4 ) & ( 2|4 ) & ( 3|4 ) "
                }
            }
        }

    } else {

        if {$::ptp_ha_vlan_encap == "false"} {

            if {$::ptp_trans_type == "IEEE 802.3"} {
                set mask                " FFFF0000"
                set position            " 12"
                set value               " $ether_type "
                set match_term_id       " 1"
                set filter_id           1
                set filter_condition    "[join [string trim $match_term_id] " & "]"
            } elseif { $::ptp_comm_model == "Multicast" } {

                set mask                " FFFF0000 "
                set position            " 12 "
                set value               " 0800 "
                set match_term_id       " 1 "
                set filter_id           1
                set filter_condition    "1"

            } else {
                set mask                " FFFF0000 FFFF0000"
                set position            " 12  12"
                set value               " 0800 0806"
                set match_term_id       " 1 2"
                set filter_id           1
                set filter_condition    " 1 | 2"
            }
        } else {
            if {$::ptp_trans_type == "IEEE 802.3"} {
                set mask                " FFFF0000 0FFF0000 FFFF0000"
                set position            " 12 14 16"
                set value               " 8100 $vlanid1 88F7"
                set match_term_id       " 1 2 3"
                set filter_id           1
                set filter_condition    "[join [string trim $match_term_id] " & "]"
            } else {
                set mask                " FFFF0000 0FFF0000 FFFF0000 FFFF0000"
                set position            " 12 14 16 12"
                set value                " 8100 $vlanid1 0800 0806"
                set match_term_id       " 1 2 3 4"
                set filter_id           1
                set filter_condition    "( 1|4 ) & ( 2|4 ) &  ( 3|4 )"
            }
        }
    }

    for {set i 0} {$i < [llength $mask]} {incr i} {

        pltLib::create_filter_match_term\
                    -session_id         $::session_id\
                    -port_num           $port_num\
                    -match_term_id      [lindex $match_term_id $i]\
                    -mask               [lindex $mask $i]\
                    -byte_position      [lindex $position $i]\
                    -value              [lindex $value $i]
    }

    pltLib::create_filter_condition\
                -session_id         $::session_id\
                -port_num           $port_num\
                -filter_id          $filter_id\
                -filter_condition   $filter_condition\
                -filter_mode        $filter_mode

    pltLib::set_packet_capture_criteria\
                -session_id         $::session_id\
                -port_num           $port_num\
                -pkt_to_keep        $pkt_to_keep\
                -index              $filter_id\
                -bytes_to_keep      $bytes_to_keep

    pltLib::set_port_capture_mode\
                -session_id         $::session_id\
                -port_num           $port_num\
                -capture_flag       $capture_flag

    set ids "[string trim $match_term_id] $filter_id"

    return 1

}


#################################################################################
#                                                                               #
#  PROCEDURE NAME    : pltLib::ptp-ha_transmission_sequencing                   #
#                                                                               #
#  DEFINITION        :  This function delays the transmission of PTP messages to#
#                       which tries to over-take the queue.                     #
#                                                                               #
#  INPUT PARAMETERS  :                                                          #
#                                                                               #
#  session_id        :  TEE session id                                          #
#                                                                               #
#  port_num          :  Port Number                                             #
#                                                                               #
#  data              :  hex dump that need to transmited periodically.          #
#                                                                               #
#  interval          :  Interval (in millisec)                                  #
#                                                                               #
#  count             :  Number of packets to be send                            #
#                                                                               #
#  handler           :  function which modify the data after each send          #
#                                                                               #
#  instance          : Instance ID on which the data is to be transmitted.      #
#                                                                               #
#                                                                               #
#  USAGE             :                                                          #
#                                                                               #
#      pltLib::ptp-ha_transmission_sequencing\                                  #
#                session_id         $session_id\                                #
#                port_num           $port_num\                                  #
#                data               $data\                                      #
#                instance           $instance\                                  #
#                handler            $handler\                                   #
#                count              $count                                      #
#                                                                               #
#################################################################################

proc pltLib::ptp-ha_transmission_sequencing { args } {

    array set param $args
    set session_id  $param(-session_id)
    set port_num    $param(-port_num)
    set data        $param(-data)
    set instance    $param(-instance)
    set interval    $param(-interval)
    set count       $param(-count)

    if {[info exists param(-handler)]} {
        set handler $param(-handler)
    } else {
        set handler     "dont_care"
    }

    if {(($instance == 1) && ($::ok_to_send_1 == 0))} {

        set ::send_after_id$instance [after 50 pltLib::send_periodic_wrapper\
                -session_id     $session_id\
                -port_num       $port_num\
                -data           $data\
                -instance       $instance\
                -interval       $interval\
                -count          $count\
                -handler        $handler]
        return 0
    }

    if {(($instance == 5) && ($::ok_to_send_5 == 0))} {

        set ::send_after_id$instance [after 50 pltLib::send_periodic_wrapper\
                -session_id     $session_id\
                -port_num       $port_num\
                -data           $data\
                -instance       $instance\
                -interval       $interval\
                -count          $count\
                -handler        $handler]
        return 0
    }

    if {(($instance == 7) && ($::ok_to_send_7 == 0))} {

        set ::send_after_id$instance [after 50 pltLib::send_periodic_wrapper\
                -session_id     $session_id\
                -port_num       $port_num\
                -data           $data\
                -instance       $instance\
                -interval       $interval\
                -count          $count\
                -handler        $handler]
        return 0
    }

    if {(($instance == 8) && ($::ok_to_send_8 == 0))} {

        set ::send_after_id$instance [after 50 pltLib::send_periodic_wrapper\
                -session_id     $session_id\
                -port_num       $port_num\
                -data           $data\
                -instance       $instance\
                -interval       $interval\
                -count          $count\
                -handler        $handler]
        return 0
    }

    if {(($instance == 12) && ($::ok_to_send_12 == 0))} {

        set ::send_after_id$instance [after 50 pltLib::send_periodic_wrapper\
                -session_id     $session_id\
                -port_num       $port_num\
                -data           $data\
                -instance       $instance\
                -interval       $interval\
                -count          $count\
                -handler        $handler]
        return 0
    }

    if {(($instance == 13) && ($::ok_to_send_13 == 0))} {

        set ::send_after_id$instance [after 50 pltLib::send_periodic_wrapper\
                -session_id     $session_id\
                -port_num       $port_num\
                -data           $data\
                -instance       $instance\
                -interval       $interval\
                -count          $count\
                -handler        $handler]
        return 0
    }

    return 1
}


#################################################################################
#                                                                               #
#  PROCEDURE NAME    : pltLib::ptp-ha_transmission_queueing                     #
#                                                                               #
#  DEFINITION        :  This function creates queue for the transmission of PTP #
#                       messages with instances.                                #
#                                                                               #
#  INPUT PARAMETERS  :                                                          #
#                                                                               #
#  instance          : Instance ID on which the data is to be transmitted.      #
#                                                                               #
#                                                                               #
#  USAGE             :                                                          #
#                                                                               #
#      pltLib::ptp-ha_transmission_queueing\                                    #
#                instance           $instance                                   #
#                                                                               #
#################################################################################

proc pltLib::ptp-ha_transmission_queueing { args } {

    array set param $args
    set instance    $param(-instance)

    if {$::ptp_dut_clock_step == "Two-Step"} {

        set two_step_flag       1
    } else {

        set two_step_flag       0
    }

    if {$two_step_flag == 1} {

        if {$instance == 1} {
            set ::ok_to_send_1    0
            set ::ok_to_send_5    1
        }

        if {$instance == 4} {
            set ::ok_to_send_4    [expr $::ok_to_send_4 + 1]
        }

        if {$instance == 5} {
            set ::ok_to_send_1    1
            set ::ok_to_send_5    0
        }

        if {$instance == 7} {
            set ::ok_to_send_7    0
            set ::ok_to_send_8    1
        }

        if {$instance == 8} {
            set ::ok_to_send_7    1
            set ::ok_to_send_8    0
        }

        if {$instance == 12} {
            set ::ok_to_send_12    0
            set ::ok_to_send_13    1
        }

        if {$instance == 13} {
            set ::ok_to_send_12    1
            set ::ok_to_send_13    0
        }
    } else {

        if {$instance == 1} {
            set ::ok_to_send_1    1
            set ::ok_to_send_5    1
        }

        if {$instance == 4} {
            set ::ok_to_send_4    [expr $::ok_to_send_4 + 1]
        }

        if {$instance == 5} {
            set ::ok_to_send_1    1
            set ::ok_to_send_5    1
        }

        if {$instance == 7} {
            set ::ok_to_send_7    1
            set ::ok_to_send_8    1
        }

        if {$instance == 8} {
            set ::ok_to_send_7    1
            set ::ok_to_send_8    1
        }

        if {$instance == 12} {
            set ::ok_to_send_12    1
            set ::ok_to_send_13    1
        }

        if {$instance == 13} {
            set ::ok_to_send_12    1
            set ::ok_to_send_13    1
        }
    }

    return 1
}
