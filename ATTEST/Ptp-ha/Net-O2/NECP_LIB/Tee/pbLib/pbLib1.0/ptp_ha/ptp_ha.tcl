################################################################################
# File Name           : ptp_ha.tcl                                             #
# File Version        : 1.1                                                    #
# Component Name      : ATTEST Packet Library                                  #
# Module Name         : Precision Time Protocol - High Accuracy                #
################################################################################
# History    Date       Author     Addition/ Alteration                        #
#                                                                              #
#  1.0       Apr/2018   CERN       Initial                                     #
#  1.1       Apr/2019   CERN       Added conversion of Time Interval to        #
#                                  Nanoseconds and vice-versa for              #
#                                  correctionField.                            #
#                                                                              #
################################################################################
# Copyright (C) 2018 - 2019 CERN                                               #
################################################################################

############################# LIST OF PROCEDURES ###############################
#                                                                              #
#  1. pbLib::encode_eth_header                                                 #
#  2. pbLib::decode_eth_header                                                 #
#  3. pbLib::encode_vlan_header                                                #
#  4. pbLib::decode_vlan_header                                                #
#  5. pbLib::encode_arp_header                                                 #
#  6. pbLib::decode_arp_header                                                 #
#  7. pbLib::encode_ipv4_header                                                #
#  8. pbLib::decode_ipv4_header                                                #
#  9. pbLib::encode_udp_header                                                 #
# 10. pbLib::decode_udp_header                                                 #
# 11. pbLib::encode_ptp_ha_common_header                                       #
# 12. pbLib::decode_ptp_ha_common_header                                       #
# 13. pbLib::encode_ptp_ha_sync_header                                         #
# 14. pbLib::decode_ptp_ha_sync_header                                         #
# 15. pbLib::encode_ptp_ha_announce_header                                     #
# 16. pbLib::decode_ptp_ha_announce_header                                     #
# 17. pbLib::encode_ptp_ha_delay_req_header                                    #
# 18. pbLib::decode_ptp_ha_delay_req_header                                    #
# 19. pbLib::encode_ptp_ha_delay_resp_header                                   #
# 20. pbLib::decode_ptp_ha_delay_resp_header                                   #
# 21. pbLib::encode_ptp_ha_followup_header                                     #
# 22. pbLib::decode_ptp_ha_followup_header                                     #
# 23. pbLib::encode_ptp_ha_pdelay_req_header                                   #
# 24. pbLib::decode_ptp_ha_pdelay_req_header                                   #
# 25. pbLib::encode_ptp_ha_pdelay_resp_header                                  #
# 26. pbLib::decode_ptp_ha_pdelay_resp_header                                  #
# 27. pbLib::encode_ptp_ha_pdelay_resp_followup_header                         #
# 28. pbLib::decode_ptp_ha_pdelay_resp_followup_header                         #
# 29. pbLib::encode_ptp_ha_signal_header                                       #
# 30. pbLib::decode_ptp_ha_signal_header                                       #
# 31. pbLib::encode_ptp_ha_mgmt_header                                         #
# 32. pbLib::decode_ptp_ha_mgmt_header                                         #
#                                                                              #
# 33. pbLib::encode_arp_pkt                                                    #
# 34. pbLib::decode_arp_pkt                                                    #
# 35. pbLib::encode_ptp_ha_pkt                                                 #
# 36. pbLib::decode_ptp_ha_pkt                                                 #
#                                                                              #
# 37. pbLib::encode_port_identity                                              #
# 38. pbLib::push_vlan_tag                                                     #
#                                                                              #
################################################################################

namespace eval pbLib {}

################################################################################
#  PROCEDURE NAME       : pbLib::encode_eth_header                             #
#                                                                              #
#  DEFINITION           : This function encodes ethernet header.               #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  dest_mac             : (Optional) Destination MAC address of eth header     #
#                                    ( Default : 00:00:00:00:00:AA )           #
#                                                                              #
#  src_mac              : (Optional) Source MAC address of eth header          #
#                                    ( Default : 00:00:00:00:00:BB )           #
#                                                                              #
#  eth_type             : (Optional) Ethernet type/length of eth header        #
#                                    ( Default : ffff )                        #
#                                                                              #
#  OUTPUT PARAMETERS    : NONE                                                 #
#                                                                              #
#  RETURNS              : Ethernet header hex dump                             #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#             pbLib::encode_eth_header\                                        #
#                                      -dest_mac      $dest_mac \              #
#                                      -src_mac       $src_mac  \              #
#                                      -eth_type      $eth_type                #
#                                      -hexdump       hexdump                  #
#                                                                              #
################################################################################

proc pbLib::encode_eth_header {args} {

    array set param $args

    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)

    } else {

        set dest_mac "00:00:00:00:00:AA"
    }

    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)

    } else {

        set src_mac "00:00:00:00:00:BB"
    }

    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)

    } else {

        set eth_type FFFF
    }

    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    ethernet eth

    set eth::dmac     [mac2hex $dest_mac]
    set eth::smac     [mac2hex $src_mac]
    set eth::type     $eth_type


    set hexdump [eth::pkt]

    return $hexdump

}


################################################################################
#  PROCEDURE NAME       : pbLib::encode_vlan_header                            #
#                                                                              #
#  DEFINITION           : This function encodes vlan header.                   #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  vlan_id              : (Optional) Vlan id of vlan header                    #
#                                    ( Default : 0 )                           #
#                                                                              #
#  priority             : (Optional) Priority of vlan header                   #
#                                    ( Default : 1 )                           #
#                                                                              #
#  dei                  : (Optional) Eligible indicator of vlan header         #
#                                    ( Default : 0 )                           #
#                                                                              #
#  next_protocol        : (Optional) Next protocol of vlan header              #
#                                    ( Default : ffff )                        #
#                                                                              #
#  OUTPUT PARAMETERS    : NONE                                                 #
#                                                                              #
#  RETURNS              : Vlan header hex dump                                 #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#          pbLib::encode_vlan_header\                                          #
#                                    -vlan_id         $vlan_id  \              #
#                                    -priority        $priority \              #
#                                    -dei             $dei      \              #
#                                    -next_protocol   $next_protocol           #
#                                    -hexdump         hexdump                  #
#                                                                              #
################################################################################

proc pbLib::encode_vlan_header {args} {

    array set param $args

    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)

    } else {

        set vlan_id 0
    }

    if {[info exists param(-priority)]} {

        set priority $param(-priority)

    } else {

        set priority 1
    }


    if {[info exists param(-dei)]} {

        set dei $param(-dei)

    } else {

        set dei 0
    }

    if {[info exists param(-next_protocol)]} {

        set next_protocol $param(-next_protocol)

    } else {

        set next_protocol "FFFF"
    }

    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }


    vlan virtual

    set virtual::priority [dec2hex $priority 2]
    set virtual::cfi      [dec2hex $dei 2]
    set virtual::vlan     [dec2hex $vlan_id 4]
    set virtual::typelen  $next_protocol

    set hexdump [virtual::pkt]

    return $hexdump

}


################################################################################
#  PROCEDURE NAME       : pbLib::encode_arp_header                             #
#                                                                              #
#  DEFINITION           : This function encodes arp header.                    #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  hardware_type        : (Optional) Specifies the network protocol type       #
#                                    ( Default : 1 )                           #
#                                                                              #
#  protocol_type        : (Optional) Specifies the internetwork protocol for   #
#                                    which the ARP request is intended.        #
#                                    ( Default : 0800 )                        #
#                                                                              #
#  hardware_size        : (Optional) Length of a hardware address              #
#                                    ( Default : 6 )                           #
#                                                                              #
#  protocol_size        : (Optional) Length addresses used in the upper layer  #
#                                    protocol. ( Default : 4 )                 #
#                                                                              #
#  message_type         : (Optional) Specifies the operation that the sender   #
#                                    is performing: 1 - request, 2 - reply.    #
#                                    ( Default : 1 )                           #
#                                                                              #
#  sender_mac           : (Optional) Sender hardware address                   #
#                                    ( Default : 00:00:00:00:00:00 )           #
#                                                                              #
#  sender_ip            : (Optional) Sender protocol address                   #
#                                    ( Default : 0.0.0.0 )                     #
#                                                                              #
#  target_mac           : (Optional) Target hardware address                   #
#                                    ( Default : 00:00:00:00:00:00 )           #
#                                                                              #
#  target_ip            : (Optional) Target protocol address                   #
#                                    ( Default : 0.0.0.0 )                     #
#                                                                              #
#  OUTPUT PARAMETERS    : NONE                                                 #
#                                                                              #
#  RETURNS              : Arp header hex dump                                  #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#             pbLib::encode_arp_header      -hardware_type   $hardware_typ  \  #
#                                    -protocol_type   $protocol_type \         #
#                                    -hardware_size   $hardware_size \         #
#                                    -protocol_size   $protocol_size \         #
#                                    -message_type    $message_type \          #
#                                    -sender_mac      $sender_mac \            #
#                                    -sender_ip       $sender_ip \             #
#                                    -target_mac      $target_mac \            #
#                                    -target_ip       $target_ip               #
#                                    -hexdump         hexdump                  #
#                                                                              #
################################################################################

proc pbLib::encode_arp_header {args} {

    array set param $args

    if {[info exists param(-hardware_type)]} {

        set hardware_type $param(-hardware_type)

    } else {

        set hardware_type 1

    }

    if {[info exists param(-protocol_type)]} {

        set protocol_type $param(-protocol_type)

    } else {

        set protocol_type 0800

    }

    if {[info exists param(-hardware_size)]} {

        set hardware_size $param(-hardware_size)

    } else {

        set hardware_size 06

    }

    if {[info exists param(-protocol_size)]} {

        set protocol_size $param(-protocol_size)

    } else {

        set protocol_size 04
    }

    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)

    } else {

        set message_type 1
    }

    if {[info exists param(-sender_mac)]} {

        set sender_mac $param(-sender_mac)

    } else {

        set sender_mac "00:00:00:00:00:00"
    }


    if {[info exists param(-sender_ip)]} {

        set sender_ip $param(-sender_ip)

    } else {

        set sender_ip "0.0.0.0"
    }


    if {[info exists param(-target_mac)]} {

        set target_mac $param(-target_mac)

    } else {

        set target_mac "00:00:00:00:00:00"
    }


    if {[info exists param(-target_ip)]} {

        set target_ip $param(-target_ip)

    } else {

        set target_ip "0.0.0.0"
    }

    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    arp Arp

    set Arp::hw_type     [dec2hex $hardware_type 2]
    set Arp::proto_type  $protocol_type
    set Arp::hw_size     [dec2hex $hardware_size 2]
    set Arp::proto_size  [dec2hex $protocol_size 2]
    set Arp::opcode      [dec2hex $message_type 2]
    set Arp::src_mac     [mac2hex $sender_mac]
    set Arp::src_ip      [ip2hex $sender_ip]
    set Arp::dest_mac    [mac2hex $target_mac]
    set Arp::dest_ip     [ip2hex $target_ip]

    set hexdump [Arp::pkt]

    return $hexdump

}


################################################################################
#  PROCEDURE NAME       : pbLib::encode_ipv4_header                            #
#                                                                              #
#  DEFINITION           : This function encodes ipv4 header.                   #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  version              : (Optional) Version of ip                             #
#                                    ( Default : 4 )                           #
#                                                                              #
#  header_length        : (Optional) Header length  of ipv4                    #
#                                    ( Default : 20)                           #
#                                                                              #
#  tos                  : (Optional) Type of service (hex value)               #
#                                    ( Default : 00 )                          #
#                                                                              #
#  total_length         : (Optional) Total length  of ipv4                     #
#                                    ( Default : 20 )                          #
#                                                                              #
#  identification       : (Optional) Unique id of ipv4                         #
#                                    ( Default : 0 )                           #
#                                                                              #
#  flags                : (Optional) Flags represents the control or identify  #
#                                    fragments. ( Default - 0 )                #
#                                    ( 0 - Reserved , 1 - Don't fragmetn ,     #
#                                      2 - More fragment )                     #
#                                                                              #
#  offset               : (Optional) Fragment offset                           #
#                                    ( Default - 0 )                           #
#                                                                              #
#  ttl                  : (Optional) Time to live                              #
#                                    ( Default - 64 )                          #
#                                                                              #
#  checksum             : (Optional) Error-checking of header(16-bit checksum) #
#                                    ( Default - auto )                        #
#                                                                              #
#  next_protocol        : (Optional) Protocol in the data portion of the       #
#                                    IP datagram. ( Default - ff )             #
#                                                                              #
#  src_ip               : (Optional) IPv4 address of the sender of the packet  #
#                                    ( Default - 0.0.0.0 )                     #
#                                                                              #
#  dest_ip              : (Optional) IPv4 address of the reciever of the packet#
#                                    ( Default - 0.0.0.0 )                     #
#                                                                              #
#  OUTPUT PARAMETERS    : NONE                                                 #
#                                                                              #
#  RETURNS              : Ipv4 header hex dump                                 #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#             pbLib::encode_ipv4_header     -version         $version  \       #
#                                    -header_length   $header_length \         #
#                                    -tos             $tos \                   #
#                                    -total_length    $total_length \          #
#                                    -identification  $id \                    #
#                                    -flags           $flags \                 #
#                                    -ttl             $ttl \                   #
#                                    -offset          $offset \                #
#                                    -checksum        $checksum \              #
#                                    -next_protocol   $next_protocol \         #
#                                    -src_ip          $src_ip \                #
#                                    -dest_ip         $dest_ip                 #
#                                    -hexdump         hexdump                  #
#                                                                              #
################################################################################

proc pbLib::encode_ipv4_header {args} {

    array set param $args

    if {[info exists param(-version)]} {

        set version $param(-version)

    } else {

        set version 4

    }

    if {[info exists param(-header_length)]} {

        set header_length $param(-header_length)

    } else {

        set header_length 20
    }

    if {[info exists param(-tos)]} {

        set tos $param(-tos)

    } else {

        set tos "00"
    }

    if {[info exists param(-total_length)]} {

        set total_length $param(-total_length)

    } else {

        set total_length 20

    }

    if {[info exists param(-identification)]} {

        set identification $param(-identification)

    } else {

        set identification 0
    }

    if {[info exists param(-flags)]} {

        set flags $param(-flags)

    } else {

        set flags 0

    }

    if {[info exists param(-offset)]} {

        set offset $param(-offset)

    } else {

        set offset 0
    }

    if {[info exists param(-ttl)]} {

        set ttl $param(-ttl)

    } else {

        set ttl 64

    }


    if {[info exists param(-checksum)]} {

        set checksum $param(-checksum)

    } else {

        set checksum "auto"
    }

    if {[info exists param(-next_protocol)]} {

        set next_protocol $param(-next_protocol)

    } else {

        set next_protocol "ff"

    }

    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)

    } else {

        set src_ip 0.0.0.0
    }

    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)

    } else {

        set dest_ip 0.0.0.0

    }

    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    set auto_checksum 0
    if { [string match -nocase "auto" $checksum] } {
        set checksum 0000
        set auto_checksum 1
    }

    set hdr_len [expr $header_length/4]

    ip ipv4

    set ipv4::version        [dec2hex $version 1]
    set ipv4::hdrlen         [dec2hex $hdr_len 1]
    set ipv4::tos            $tos
    set ipv4::len            [dec2hex $total_length 4]
    set ipv4::id             [dec2hex $identification 2]
    set ipv4::flags          [dec2hex $flags 2]
    set ipv4::offset         [dec2hex $offset 2]
    set ipv4::ttl            [dec2hex $ttl 2]
    set ipv4::proto          [dec2hex $next_protocol 2]
    set ipv4::checksum       $checksum
    set ipv4::srcip          [ip2hex  $src_ip]
    set ipv4::dstip          [ip2hex  $dest_ip]

    set hexdump [ipv4::pkt]

    if {$auto_checksum} {

        #Hexdump with calculating checksum
        set ipv4::checksum [calculate_checksum $hexdump]
        set hexdump [ipv4::pkt]
    }

    return $hexdump

}

################################################################################
#  PROCEDURE NAME       : pbLib::encode_udp_header                             #
#                                                                              #
#  DEFINITION           : This function encodes udp header.                    #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  src_port             : (Optional) Sender's port number                      #
#                                    ( Default - 0 )                           #
#                                                                              #
#  dest_port            : (Optional) Receiver's port number                    #
#                                    ( Default - 0 )                           #
#                                                                              #
#  payload_length       : (Optional) Length in bytes of UDP header and data    #
#                                    ( Default - 8 )                           #
#                                                                              #
#  checksum             : (Optional) Error-checking of the header              #
#                                    ( Default - auto )                        #
#                                                                              #
#  pseudo_header        : (Optional) Pseudo header for checksum calculation    #
#                                                                              #
#  OUTPUT PARAMETERS    : NONE                                                 #
#                                                                              #
#  RETURNS              : Udp header hex dump                                  #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#             pbLib::encode_udp_header\                                        #
#                                    -src_port       $src_port \               #
#                                    -dest_port      $dest_port \              #
#                                    -payload_length $length \                 #
#                                    -checksum       $checksum                 #
#                                    -pseudo_header  $pseudo_header\           #
#                                    -hexdump        hexdump                   #
#                                                                              #
################################################################################

proc pbLib::encode_udp_header {args} {

    array set param $args

    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)

    } else {

        set src_port 0
    }

    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)

    } else {

        set dest_port 0
    }

    if {[info exists param(-payload_length)]} {

        set payload_length $param(-payload_length)

    } else {

        set payload_length 8
    }

    if {[info exists param(-checksum)]} {

        set checksum $param(-checksum)

    } else {

        set checksum "auto"
    }

    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    if {[info exists param(-pseudo_header)]} {

        set pseudo_header $param(-pseudo_header)
    } else {
        set pseudo_header ""
    }

    if {[info exists param(-data)]} {

        set data $param(-data)
    } else {
        set data ""
    }

    set auto_checksum 0
    if { [string match -nocase "auto" $checksum] } {
        set checksum 0000
        set auto_checksum 1
    }

    if {$pseudo_header == ""} {
        set auto_checksum 0
    }

    udp Udp

    set Udp::srcport  [dec2hex $src_port 2]
    set Udp::dstport  [dec2hex $dest_port 2]
    set Udp::length   [dec2hex $payload_length 4]
    set Udp::checksum $checksum

    set hexdump [Udp::pkt]

    if {$auto_checksum} {
        append pseudo_header $Udp::length
        append pseudo_header $hexdump
        append pseudo_header $data

        set Udp::checksum [calculate_checksum $pseudo_header]
        set hexdump [Udp::pkt]
    }

    return $hexdump

}

################################################################################
#  PROCEDURE NAME       : pbLib::decode_eth_header                             #
#                                                                              #
#  DEFINITION           : This function decodes ethernet header.               #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  hex                  : (Mandatory) Ethernet hex dump                        #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  dest_mac             : (Optional) Destination MAC address of eth header     #
#                                                                              #
#  src_mac              : (Optional) Source MAC address of eth header          #
#                                                                              #
#  eth_type/next_protocol : (Optional) Ethernet type/length of eth header      #
#                                                                              #
#  RETURNS              : None                                                 #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#             pbLib::decode_eth_header        -hex           $hex \            #
#                                      -dest_mac      dest_mac \               #
#                                      -src_mac       src_mac  \               #
#                                      -eth_type      eth_type                 #
#                                                                              #
################################################################################

proc pbLib::decode_eth_header {args} {

    array set param $args

    if {[info exists param(-hex)]} {

        set hex $param(-hex)

    } else {

        return -code error "Mandatory parameter missing - hex"
    }


    if {[info exists param(-dest_mac)]} {

        upvar $param(-dest_mac) dmac
    }

    if {[info exists param(-src_mac)]} {
        upvar $param(-src_mac) smac
    }

    if {[info exists param(-next_protocol)]} {
        upvar $param(-next_protocol) next
    }

    if {[info exists param(-eth_type)]} {
        upvar $param(-eth_type) next
    }

    ethernet eth = $hex

    set dmac [hex2mac $eth::dmac]
    set smac [hex2mac $eth::smac]
    set next $eth::type

}

################################################################################
#  PROCEDURE NAME       : pbLib::decode_vlan_header                            #
#                                                                              #
#  DEFINITION           : This function decodes vlan header.                   #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  hex                  : (Mandatory) vlan hex dump                            #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  vlan_id              : (Optional) Vlan id of vlan header                    #
#                                                                              #
#  priority             : (Optional) Priority of vlan header                   #
#                                                                              #
#  dei                  : (Optional) Eligible indicator of vlan header         #
#                                                                              #
#  next_protocol        : (Optional) Next protocol of vlan header              #
#                                                                              #
#  RETURNS              : None                                                 #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#          pbLib::decode_vlan_header        -hex             $hex \            #
#                                    -vlan_id         vlan_id  \               #
#                                    -priority        priority \               #
#                                    -dei             dei      \               #
#                                    -next_protocol   next_protocol            #
#                                                                              #
################################################################################

proc pbLib::decode_vlan_header {args} {

    array set param $args

    if {[info exists param(-hex)]} {

        set hex $param(-hex)

    } else {

        return -code error "Mandatory parameter missing - hex"
    }


    if {[info exists param(-priority)]} {

        upvar $param(-priority) priority
    }

    if {[info exists param(-dei)]} {

        upvar $param(-dei) dei
    }

    if {[info exists param(-vlan_id)]} {

        upvar $param(-vlan_id) vlan_id
    }

    if {[info exists param(-next_protocol)]} {

        upvar $param(-next_protocol) next_protocol
    }


    vlan virtual = $hex

    set priority      [hex2dec $virtual::priority]
    set dei           [hex2dec $virtual::cfi]
    set vlan_id       [hex2dec $virtual::vlan]
    set next_protocol $virtual::typelen

}


################################################################################
#  PROCEDURE NAME       : pbLib::decode_arp_header                             #
#                                                                              #
#  DEFINITION           : This function decodes arp header.                    #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  hex                  : (Mandatory) Arp header hex dump                      #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  hardware_type        : (Optional) Specifies the network protocol type       #
#                                                                              #
#  protocol_type        : (Optional) Specifies the internetwork protocol for   #
#                         which the ARP request is intended                    #
#                                                                              #
#  hardware_size        : (Optional) Length of a hardware address              #
#                                                                              #
#  protocol_size        : (Optional) Length addresses used in the upper layer  #
#                                    protocol                                  #
#                                                                              #
#  message_type         : (Optional) Specifies the operation that the sender   #
#                                    is performing: 1 - request, 2 - reply.    #
#                                                                              #
#  sender_mac           : (Optional) Sender hardware address                   #
#                                                                              #
#  sender_ip            : (Optional) Sender protocol address                   #
#                                                                              #
#  target_mac           : (Optional) Target hardware address                   #
#                                                                              #
#  target_ip            : (Optional) Target protocol address                   #
#                                                                              #
#  RETURNS              : None                                                 #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#             pbLib::decode_arp_header\                                        #
#                                    -hex             $hex \                   #
#                                    -hardware_type   hardware_typ  \          #
#                                    -protocol_type   protocol_type \          #
#                                    -hardware_size   hardware_size \          #
#                                    -protocol_size   protocol_size \          #
#                                    -message_type    message_type \           #
#                                    -sender_mac      sender_mac \             #
#                                    -sender_ip       sender_ip \              #
#                                    -target_mac      target_mac \             #
#                                    -target_ip       target_ip                #
#                                                                              #
################################################################################

proc pbLib::decode_arp_header {args} {

    array set param $args

    if {[info exists param(-hex)]} {

        set hex $param(-hex)

    } else {

        return -code error "Mandatory parameter missing - hex"
    }


    if {[info exists param(-hardware_type)]} {

        upvar $param(-hardware_type) hardware_type
        set hardware_type ""
    }

    if {[info exists param(-protocol_type)]} {

        upvar $param(-protocol_type) protocol_type
        set protocol_type ""
    }

    if {[info exists param(-hardware_size)]} {

        upvar $param(-hardware_size) hardware_size
        set hardware_size ""
    }

    if {[info exists param(-protocol_size)]} {

        upvar $param(-protocol_size) protocol_size
        set protocol_size ""
    }

    if {[info exists param(-message_type)]} {

        upvar $param(-message_type) message_type
        set message_type ""
    }

    if {[info exists param(-sender_mac)]} {

        upvar $param(-sender_mac) sender_mac
        set sender_mac ""
    }

    if {[info exists param(-sender_ip)]} {

        upvar $param(-sender_ip) sender_ip
        set sender_ip ""
    }

    if {[info exists param(-target_mac)]} {

        upvar $param(-target_mac) target_mac
        set target_mac ""
    }

    if {[info exists param(-target_ip)]} {

        upvar $param(-target_ip) target_ip
        set target_ip ""
    }

    arp Arp = $hex

    set hardware_type  [hex2dec $Arp::hw_type]
    set protocol_type  $Arp::proto_type
    set hardware_size  [hex2dec $Arp::hw_size]
    set protocol_size  [hex2dec $Arp::proto_size]
    set message_type   [hex2dec $Arp::opcode]
    set sender_mac     [hex2mac $Arp::src_mac]
    set sender_ip      [hex2ip $Arp::src_ip]
    set target_mac     [hex2mac $Arp::dest_mac]
    set target_ip      [hex2ip $Arp::dest_ip]
}

################################################################################
#  PROCEDURE NAME       : pbLib::decode_ipv4_header                            #
#                                                                              #
#  DEFINITION           : This function decodes ipv4 header.                   #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  hex                  : (Mandatory) Ipv4 hex dump                            #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  version              : (Optional) Version of ip                             #
#                                                                              #
#  header_length        : (Optional) Header length  of ipv4                    #
#                                                                              #
#  tos                  : (Optional) Type of service                           #
#                                                                              #
#  total_length         : (Optional) Total length  of ipv4                     #
#                                                                              #
#  identification       : (Optional) Unique id of ipv4                         #
#                                                                              #
#  flags                : (Optional) Flags represents the control or identify  #
#                                    fragments.                                #
#                                    ( 0 - Reserved , 1 - Don't fragmetn ,     #
#                                      2 - More fragment )                     #
#                                                                              #
#  offset               : (Optional) Fragment offset                           #
#                                                                              #
#  ttl                  : (Optional) Time to live                              #
#                                                                              #
#  checksum             : (Optional) Error-checking of the header              #
#                                                                              #
#  next_protocol        : (Optional) Protocol in the data portion of the       #
#                                    IP datagram                               #
#                                                                              #
#  src_ip               : (Optional) IPv4 address of the sender of the packet  #
#                                                                              #
#  dest_ip              : (Optional) IPv4 address of the reciever of the packet#
#                                                                              #
#  RETURNS              : None                                                 #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#             pbLib::decode_ipv4_header\                                       #
#                            -hex                     $hex\                    #
#                            -version                 version\                 #
#                            -header_length           header_length\           #
#                            -tos                     tos\                     #
#                            -total_length            total_length\            #
#                            -identification          id\                      #
#                            -flags                   flags\                   #
#                            -ttl                     ttl\                     #
#                            -offset                  offset\                  #
#                            -checksum                checksum\                #
#                            -next_protocol           next_protocol\           #
#                            -src_ip                  src_ip\                  #
#                            -dest_ip                 dest_ip\                 #
#                            -checksum_status         checksum_status\         #
#                            -checksum_expected       checksum_expected        #
#                                                                              #
################################################################################

proc pbLib::decode_ipv4_header {args} {

    array set param $args

    if {[info exists param(-hex)]} {

        set hex $param(-hex)

    } else {

        return -code error "Mandatory parameter missing - hex"
    }


    if {[info exists param(-type)]} {

        upvar $param(-type) type
        set type ""
    }

    if {[info exists param(-version)]} {

        upvar $param(-version) version
        set version ""
    }


    if {[info exists param(-header_length)]} {

        upvar $param(-header_length) header_length
        set header_length ""
    }

    if {[info exists param(-tos)]} {

        upvar $param(-tos) tos
        set tos ""
    }

    if {[info exists param(-total_length)]} {

        upvar $param(-total_length) total_length
        set total_length ""
    }

    if {[info exists param(-identification)]} {

        upvar $param(-identification) identification
        set identification ""
    }

    if {[info exists param(-flags)]} {

        upvar $param(-flags) flags
        set flags ""
    }

    if {[info exists param(-offset)]} {

        upvar $param(-offset) offset
        set offset ""
    }

    if {[info exists param(-ttl)]} {

        upvar $param(-ttl) ttl
        set ttl ""
    }


    if {[info exists param(-checksum)]} {

        upvar $param(-checksum) checksum
        set checksum ""
    }

    if {[info exists param(-next_protocol)]} {

        upvar $param(-next_protocol) next_protocol
        set next_protocol ""
    }

    if {[info exists param(-src_ip)]} {

        upvar $param(-src_ip) src_ip
        set src_ip ""
    }

    if {[info exists param(-dest_ip)]} {

        upvar $param(-dest_ip) dest_ip
        set dest_ip ""
    }

    if {[info exists param(-checksum_status)]} {

        upvar $param(-checksum_status) checksum_status
        set checksum_status ""
    }

    if {[info exists param(-checksum_expected)]} {

        upvar $param(-checksum_expected) checksum_expected
        set checksum_expected ""
    }
    
    ip ipv4 = $hex
    
    set version         [hex2dec $ipv4::version]
    set header_length   [expr [hex2dec $ipv4::hdrlen] * 4]
    set tos             $ipv4::tos
    set total_length    [hex2dec $ipv4::len]
    set identification  [hex2dec $ipv4::id]
    set flags           [hex2dec $ipv4::flags]
    set offset          [hex2dec $ipv4::offset]
    set ttl             [hex2dec $ipv4::ttl]
    set next_protocol   [hex2dec $ipv4::proto]
    set checksum        $ipv4::checksum
    set src_ip          [hex2ip $ipv4::srcip]
    set dest_ip         [hex2ip $ipv4::dstip]

    #Checksum validation
    set ipv4::checksum      0000
    set checksum_status     [expr !0x[calculate_checksum $hex]]
    set checksum_expected   [calculate_checksum [ipv4::pkt]]

}

################################################################################
#  PROCEDURE NAME       : pbLib::decode_udp_header                             #
#                                                                              #
#  DEFINITION           : This function decodes udp header.                    #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  hex                  : Udp hex dump                                         #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  src_port             : (Optional) Sender's port number                      #
#                                                                              #
#  dest_port            : (Optional) Receiver's port number                    #
#                                                                              #
#  payload_length       : (Optional) Length in bytes of UDP header and data    #
#                                                                              #
#  checksum             : (Optional) Error-checking of the header              #
#                                                                              #
#  pseudo_header        : (Optional) Pseudo header for checksum calculation    #
#                                                                              #
#  checksum_status      : (Optional) Status of UDP Checksum                    #
#                                                                              #
#  checksum_expected    : (Optional) Expected UDP Checksum                     #
#                                                                              #
#  RETURNS              : None                                                 #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#             pbLib::decode_udp_header\                                        #
#                            -hex                    $hex\                     #
#                            -src_port               src_port\                 #
#                            -dest_port              dest_port\                #
#                            -payload_length         length\                   #
#                            -checksum               checksum\                 #
#                            -pseudo_header          $pseudo_header\           #
#                            -checksum_status        checksum_status\          #
#                            -checksum_expected      checksum_expected         #
#                                                                              #
################################################################################

proc pbLib::decode_udp_header {args} {

    array set param $args

    if {[info exists param(-hex)]} {

        set hex $param(-hex)

    } else {

        return -code error "Mandatory parameter missing - hex"
    }

    if {[info exists param(-src_port)]} {

        upvar $param(-src_port) src_port
        set src_port ""
    }

    if {[info exists param(-dest_port)]} {

        upvar $param(-dest_port) dest_port
        set dest_port ""
    }

    if {[info exists param(-payload_length)]} {

        upvar $param(-payload_length) payload_length
        set payload_length ""
    }

    if {[info exists param(-checksum)]} {

        upvar $param(-checksum) checksum
        set checksum ""
    }

    if {[info exists param(-pseudo_header)]} {

        set pseudo_header $param(-pseudo_header)
    } else {
        set pseudo_header ""
    }

    if {[info exists param(-data)]} {

        set data $param(-data)
    } else {
        set data ""
    }

    if {[info exists param(-checksum_status)]} {

        upvar $param(-checksum_status) checksum_status
        set checksum_status ""
    }

    if {[info exists param(-checksum_expected)]} {

        upvar $param(-checksum_expected) checksum_expected
        set checksum_expected ""
    }

    udp Udp = $hex

    set src_port         [hex2dec $Udp::srcport]
    set dest_port        [hex2dec $Udp::dstport]
    set payload_length   [hex2dec $Udp::length]
    set checksum         $Udp::checksum

    #Checksum validation
    if {$checksum == "0000"} {
        set checksum_status 1
    } else {
        append pseudo_header $Udp::length
        set checksum_status [expr !0x[calculate_checksum $pseudo_header[Udp::pkt]$data]]
    }

    set Udp::checksum       0000
    set checksum_expected   [calculate_checksum $pseudo_header[Udp::pkt]$data]

}

################################################################################
#  PROCEDURE NAME       : push_vlan_tag                                        #
#                                                                              #
#  DEFINITION           : This function used to push vlan in given packet      #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  packet               : (Mandatory) packet hex dump                          #
#                                                                              #
#  vlan_id              : (Optional) Vlan id of vlan header                    #
#                                    ( Default : 0 )                           #
#                                                                              #
#  priority             : (Optional) Priority of vlan header                   #
#                                    ( Default : 1 )                           #
#                                                                              #
#  dei                  : (Optional) Eligible indicator of vlan header         #
#                                    ( Default : 0 )                           #
#                                                                              #
#  RETURNS              : Vlan tagged packet hex dump                          #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#          push_vlan_tag             -packet          $packet \                #
#                                    -vlan_id         $vlan_id  \              #
#                                    -priority        $priority \              #
#                                    -dei             $dei \                   #
#                                    -next_protocol   $next_protocol           #
#                                                                              #
################################################################################

proc pbLib::push_vlan_tag {args} {

    array set param $args

    if {[info exists param(-packet)]} {

        set hex $param(-packet)

    } else {

        return -code error "Mandatory parameter missing - packet"
    }


    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)

    } else {

        set vlan_id 0
    }

    if {[info exists param(-type)]} {

        set type $param(-type)

    } else {

        set type "8100"
    }

    if {[info exists param(-priority)]} {

        set priority $param(-priority)

    } else {

        set priority 1
    }


    if {[info exists param(-dei)]} {

        set dei $param(-dei)

    } else {

        set dei 0
    }

    if {[info exists param(-next_protocol)]} {

        set next_protocol $param(-next_protocol)

    } else {

        set next_protocol "FFFF"
    }


    set vlan_hex [pbLib::encode_vlan_header \
                        -vlan_id        $vlan_id\
                        -next_protocol  $next_protocol \
                        -priority       $priority\
                        -dei            $dei]

    set vlan_hex [string range $type$vlan_hex 0 7]

    set eth_dump [string range $hex 0 23]
    set rem_dump [string range $hex 24 end]

    return $eth_dump$vlan_hex$rem_dump

}

################################################################################
#  PROCEDURE NAME       : pbLib::encode_arp_pkt                                #
#                                                                              #
#  DEFINITION           : This function encodes arp packet                     #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  ( ethernet )                                                                #
#                                                                              #
#  dest_mac             : (Optional) Destination MAC address of eth header     #
#                                    ( Default : 00:00:00:00:00:AA )           #
#                                                                              #
#  src_mac              : (Optional) Source MAC address of eth header          #
#                                    ( Default : 00:00:00:00:00:BB )           #
#                                                                              #
#  eth_type             : (Optional) Ethernet type/length of eth header        #
#                                    ( Default : 0806 )                        #
#                                                                              #
#  ( arp )                                                                     #
#                                                                              #
#  hardware_type        : (Optional) Specifies the network protocol type       #
#                                    ( Default : 1 )                           #
#                                                                              #
#  protocol_type        : (Optional) Specifies the internetwork protocol for   #
#                                    which the ARP request is intended.        #
#                                    ( Default : 0800 )                        #
#                                                                              #
#  hardware_size        : (Optional) Length of a hardware address              #
#                                    ( Default : 6 )                           #
#                                                                              #
#  protocol_size        : (Optional) Length addresses used in the upper layer  #
#                                    protocol. ( Default : 4 )                 #
#                                                                              #
#  message_type         : (Optional) Specifies the operation that the sender   #
#                                    is performing: 1 - request, 2 - reply.    #
#                                    ( Default : 1 )                           #
#                                                                              #
#  sender_mac           : (Optional) Sender hardware address                   #
#                                    ( Default : 00:00:00:00:00:00 )           #
#                                                                              #
#  sender_ip            : (Optional) Sender protocol address                   #
#                                    ( Default : 0.0.0.0 )                     #
#                                                                              #
#  target_mac           : (Optional) Target hardware address                   #
#                                    ( Default : 00:00:00:00:00:00 )           #
#                                                                              #
#  target_ip            : (Optional) Target protocol address                   #
#                                    ( Default : 0.0.0.0 )                     #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  hexdump               :  (Optional)  Hex dump of ARP packet                 #
#                                                                              #
#  RETURNS              : Arp packet hex dump                                  #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#                   pbLib::encode_arp_pkt\                                     #
#                                    -dest_mac        $dest_mac \              #
#                                    -src_mac         $src_mac  \              #
#                                    -eth_type        $eth_type \              #
#                                    -hardware_type   $hardware_typ  \         #
#                                    -protocol_type   $protocol_type \         #
#                                    -hardware_size   $hardware_size \         #
#                                    -protocol_size   $protocol_size \         #
#                                    -message_type    $message_type \          #
#                                    -sender_mac      $sender_mac \            #
#                                    -sender_ip       $sender_ip \             #
#                                    -target_mac      $target_mac \            #
#                                    -target_ip       $target_ip               #
#                                                                              #
################################################################################

proc pbLib::encode_arp_pkt {args} {

    array set param $args

    #ETHERNET PARAMETERS
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)

    } else {

        set dest_mac "00:00:00:00:00:AA"
    }

    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)

    } else {

        set src_mac "00:00:00:00:00:BB"
    }

    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)

    } else {

        set eth_type "0806"
    }


    #ETHERNET CONSTRUCT
    set eth_hex [pbLib::encode_eth_header \
                -dest_mac  $dest_mac\
                -src_mac   $src_mac\
                -eth_type  $eth_type]


    #ARP PARAMETERS
    if {[info exists param(-hardware_type)]} {

        set hardware_type $param(-hardware_type)

    } else {

        set hardware_type 1

    }


    if {[info exists param(-protocol_type)]} {

        set protocol_type $param(-protocol_type)

    } else {

        set protocol_type 0800

    }

    if {[info exists param(-hardware_size)]} {

        set hardware_size $param(-hardware_size)

    } else {

        set hardware_size 06

    }

    if {[info exists param(-protocol_size)]} {

        set protocol_size $param(-protocol_size)

    } else {

        set protocol_size 04
    }

    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)

    } else {

        set message_type 1
    }

    if {[info exists param(-sender_mac)]} {

        set sender_mac $param(-sender_mac)

    } else {

        set sender_mac "00:00:00:00:00:00"
    }


    if {[info exists param(-sender_ip)]} {

        set sender_ip $param(-sender_ip)

    } else {

        set sender_ip "0.0.0.0"
    }


    if {[info exists param(-target_mac)]} {

        set target_mac $param(-target_mac)

    } else {

        set target_mac "00:00:00:00:00:00"
    }


    if {[info exists param(-target_ip)]} {

        set target_ip $param(-target_ip)

    } else {

        set target_ip "0.0.0.0"
    }

    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    set arp_hex [pbLib::encode_arp_header\
                -hardware_type $hardware_type\
                -protocol_type $protocol_type\
                -hardware_size $hardware_size\
                -protocol_size $protocol_size\
                -message_type  $message_type\
                -sender_mac    $sender_mac\
                -sender_ip     $sender_ip\
                -target_mac    $target_mac\
                -target_ip     $target_ip]

    set hexdump $eth_hex$arp_hex
    return 1

}

################################################################################
#  PROCEDURE NAME       : pbLib::decode_arp_pkt                                #
#                                                                              #
#  DEFINITION           : This function decodes arp packet                     #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  packet               : (Mandatory) arp packet hex dump                      #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  ( ethernet )                                                                #
#                                                                              #
#  dest_mac             : (Optional) Destination MAC address of eth header     #
#                                                                              #
#  src_mac              : (Optional) Source MAC address of eth header          #
#                                                                              #
#  eth_type             : (Optional) Ethernet type/length of eth header        #
#                                                                              #
#  ( if packet is single tagged )                                              #
#                                                                              #
#  vlan_id              : (Optional) Vlan id of vlan header                    #
#                                                                              #
#  vlan_priority        : (Optional) Priority of vlan header                   #
#                                                                              #
#  vlan_ dei            : (Optional) Eligible indicator of vlan header         #
#                                                                              #
#  vlan_next            : (Optional) Next protocol of vlan header              #
#                                                                              #
#  ( if packet is double tagged )                                              #
#                                                                              #
#  outer_vlan_id        : (Optional) Vlan id of outer vlan                     #
#                                                                              #
#  outer_vlan_priority  : (Optional) Priority of outer vlan                    #
#                                                                              #
#  outer_vlan_ dei      : (Optional) Eligible indicator of outer vlan          #
#                                                                              #
#  outer_vlan_next      : (Optional) Next protocol of outer vlan header        #
#                                                                              #
#  inner_vlan_id        : (Optional) Vlan id of inner vlan                     #
#                                                                              #
#  inner_vlan_priority  : (Optional) Priority of inner vlan                    #
#                                                                              #
#  inner_vlan_ dei      : (Optional) Eligible indicator of inner vlan          #
#                                                                              #
#  inner_vlan_next      : (Optional) Next protocol of inner vlan header        #
#                                                                              #
#  ( arp )                                                                     #
#                                                                              #
#  hardware_type        : (Optional) Specifies the network protocol type       #
#                                                                              #
#  protocol_type        : (Optional) Specifies the internetwork protocol for   #
#                         which the ARP request is intended                    #
#                                                                              #
#  hardware_size        : (Optional) Length of a hardware address              #
#                                                                              #
#  protocol_size        : (Optional) Length addresses used in the upper layer  #
#                                    protocol                                  #
#                                                                              #
#  message_type         : (Optional) Specifies the operation that the sender   #
#                                    is performing: 1 - request, 2 - reply.    #
#                                                                              #
#  sender_mac           : (Optional) Sender hardware address                   #
#                                                                              #
#  sender_ip            : (Optional) Sender protocol address                   #
#                                                                              #
#  target_mac           : (Optional) Target hardware address                   #
#                                                                              #
#  target_ip            : (Optional) Target protocol address                   #
#                                                                              #
#  RETURNS              : None                                                 #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#             pbLib::decode_arp_pkt\                                           #
#                                    -packet           $packet                 #
#                                    -dest_mac         dest_mac \              #
#                                    -src_mac          src_mac  \              #
#                                    -eth_type         eth_type \              #
#                                    -version          version  \              #
#                                    -hex              hex \                   #
#                                    -hardware_type    hardware_typ  \         #
#                                    -protocol_type    protocol_type \         #
#                                    -hardware_size    hardware_size \         #
#                                    -protocol_size    protocol_size \         #
#                                    -message_type     message_type \          #
#                                    -sender_mac       sender_mac \            #
#                                    -sender_ip        sender_ip \             #
#                                    -target_mac       target_mac \            #
#                                    -target_ip        target_ip               #
#                                                                              #
################################################################################

proc pbLib::decode_arp_pkt {args} {

    array set param $args

    if {[info exists param(-packet)]} {

        set packet $param(-packet)

    } else {

        return -code error "Mandatory parameter missing - packet"
    }

    # ETHERNET HEADER
    if {[info exists param(-dest_mac)]} {

        upvar $param(-dest_mac) dest_mac
        set dest_mac ""
    }

    if {[info exists param(-src_mac)]} {

        upvar $param(-src_mac) src_mac
        set src_mac ""
    }

    if {[info exists param(-eth_type)]} {

        upvar $param(-eth_type) eth_type
        set eth_type ""
    }

    # VLAN HEADER
    if { [info exists param(-vlan_priority)] } {

        upvar $param(-vlan_priority) vlan_priority
        set vlan_priority ""
    }

    if { [info exists param(-vlan_dei)] } {

        upvar $param(-vlan_dei) vlan_dei
        set vlan_dei ""
    }

    if {[info exists param(-vlan_id)]} {

        upvar $param(-vlan_id) vlan_id
        set vlan_id ""
    }

    if {[info exists param(-vlan_next)]} {

        upvar $param(-vlan_next) vlan_next
        set vlan_next ""
    }

    #upvaring ARP
    if {[info exists param(-hardware_type)]} {

        upvar $param(-hardware_type) hardware_type
        set hardware_type ""
    }

    if {[info exists param(-protocol_type)]} {

        upvar $param(-protocol_type) protocol_type
        set protocol_type ""
    }

    if {[info exists param(-hardware_size)]} {

        upvar $param(-hardware_size) hardware_size
        set hardware_size ""
    }

    if {[info exists param(-protocol_size)]} {

        upvar $param(-protocol_size) protocol_size
        set protocol_size ""
    }

    if {[info exists param(-message_type)]} {

        upvar $param(-message_type) message_type
        set message_type ""
    }

    if {[info exists param(-sender_mac)]} {

        upvar $param(-sender_mac) sender_mac
        set sender_mac ""
    }

    if {[info exists param(-sender_ip)]} {

        upvar $param(-sender_ip) sender_ip
        set sender_ip ""
    }

    if {[info exists param(-target_mac)]} {

        upvar $param(-target_mac) target_mac
        set target_mac ""
    }

    if {[info exists param(-target_ip)]} {

        upvar $param(-target_ip) target_ip
        set target_ip ""
    }

    set hex [string tolower $packet]

    #DECODING ETHERNET
    pbLib::decode_eth_header\
                 -hex               $hex\
                 -dest_mac          dest_mac\
                 -src_mac           src_mac\
                 -next_protocol     eth_type

    set next_protocol $eth_type
    set hex [string range $hex 28 end]

    while 1 {
        switch $next_protocol {

            "8100" {
                pbLib::decode_vlan_header\
                            -hex            $hex\
                            -priority       vlan_priority\
                            -dei            vlan_dei\
                            -vlan_id        vlan_id\
                            -next_protocol  vlan_next

                set next_protocol $vlan_next
                set hex [string range $hex 8 end]
                continue
            }

            "0806" {
                pbLib::decode_arp_header\
                        -hex            $hex\
                        -hardware_type  hardware_type\
                        -protocol_type  protocol_type\
                        -hardware_size  hardware_size\
                        -protocol_size  protocol_size\
                        -message_type   message_type\
                        -sender_mac     sender_mac\
                        -sender_ip      sender_ip\
                        -target_mac     target_mac\
                        -target_ip      target_ip
                break
            }

            default {
                return 0
            }
        }
    }

    return 1
}

################################################################################
#  PROCEDURE NAME       : pbLib::encode_ptp_ha_common_header                   #
#                                                                              #
#  DEFINITION           : This function encodes ptp ha common header.          #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  major_sdo_id          :  (Optional)  Major sdo id                           #
#                                       (Default : 0)                          #
#                                                                              #
#  message_type          :  (Optional)  Message Type                           #
#                                       (Default : 0)                          #
#                                                                              #
#  minor_version         :  (Optional)  PTP version number                     #
#                                       (Default : 2)                          #
#                                                                              #
#  version_ptp           :  (Optional)  PTP minor version number               #
#                                       (Default : 2)                          #
#                                                                              #
#  message_length        :  (Optional)  PTP HA message length                  #
#                                       (Default : 44)                         #
#                                                                              #
#  domain_number         :  (Optional)  Domain number                          #
#                                       (Default : 0)                          #
#                                                                              #
#  minor_sdo_id          :  (Optional)  Minor Sdo id                           #
#                                       (Default : 0)                          #
#                                                                              #
#  alternate_master_flag :  (Optional)  Alternate Master flag                  #
#                                       (Default : 0)                          #
#                                                                              #
#  two_step_flag         :  (Optional)  Two Step Flag                          #
#                                       (Default : 0)                          #
#                                                                              #
#  unicast_flag          :  (Optional)  Unicast Flag                           #
#                                       (Default : 0)                          #
#                                                                              #
#  profile_specific1     :  (Optional)  Profile specific1 Flag                 #
#                                       (Default : 0)                          #
#                                                                              #
#  profile_specific2     :  (Optional)  Profile specific2 Flag                 #
#                                       (Default : 0)                          #
#                                                                              #
#  secure                :  (Optional)  Secure Flag                            #
#                                       (Default : 0)                          #
#                                                                              #
#  leap61                :  (Optional)  Leap61 Flag                            #
#                                       (Default : 0)                          #
#                                                                              #
#  leap59                :  (Optional)  Leap59 Flag                            #
#                                       (Default : 0)                          #
#                                                                              #
#  current_utc_offset_valid:(Optional)  Current UTC Offset Flag                #
#                                       (Default : 0)                          #
#                                                                              #
#  ptp_timescale         :  (Optional)  PTP TimeSclae Flag                     #
#                                       (Default : 0)                          #
#                                                                              #
#  time_traceable        :  (Optional)  Time Traceable Flag                    #
#                                       (Default : 0)                          #
#                                                                              #
#  freq_traceable        :  (Optional)  Frequency Traceable Flag               #
#                                       (Default : 0)                          #
#                                                                              #
#  sync_uncertain        :  (Optional)  Synchronization uncertain Flag         #
#                                       (Default : 0)                          #
#                                                                              #
#  correction_field      :  (Optional)  Correction Field value                 #
#                                       (Default : 0)                          #
#                                                                              #
#  message_type_specific :  (Optional)  Message type                           #
#                                       (Default : 0)                          #
#                                                                              #
#  src_port_number       :  (Optional)  Source port identity                   #
#                                       (Default : 0)                          #
#                                                                              #
#  src_clock_identity    :  (Optional)  Source Clock identity                  #
#                                       (Default : 0)                          #
#                                                                              #
#  sequence_id           :  (Optional)  Sequence ID                            #
#                                       (Default : 0)                          #
#                                                                              #
#  control_field         :  (Optional)  Control field                          #
#                                       (Default : 0)                          #
#                                                                              #
#  log_message_interval  :  (Optional)  Log message interval                   #
#                                       (Default : 0)                          #
#                                                                              #
#  OUTPUT PARAMETERS    : NONE                                                 #
#                                                                              #
#  hexdump               :  (Optional)  Hex dump of PTP HA Commom Header       #
#                                       (Default : 0)                          #
#                                                                              #
#                                                                              #
#  RETURNS              : PTP HA header hex dump                               #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#     pbLib::encode_ptp_ha_common_header\                                      #
#                 -major_sdo_id                      $major_sdo_id\            #
#                 -message_type                      $message_type\            #
#                 -minor_version                     $minor_version\           #
#                 -version_ptp                       $version_ptp\             #
#                 -message_length                    $message_length\          #
#                 -domain_number                     $domain_number\           #
#                 -minor_sdo_id                      $minor_sdo_id\            #
#                 -alternate_master_flag             $alternate_master_flag\   #
#                 -two_step_flag                     $two_step_flag\           #
#                 -unicast_flag                      $unicast_flag\            #
#                 -profile_specific1                 $profile_specific1\       #
#                 -profile_specific2                 $profile_specific2\       #
#                 -secure                            $secure\                  #
#                 -leap61                            $leap61\                  #
#                 -leap59                            $leap59\                  #
#                 -current_utc_offset_valid          $current_utc_offset_valid\#
#                 -ptp_timescale                     $ptp_timescale\           #
#                 -time_traceable                    $time_traceable\          #
#                 -freq_traceable                    $freq_traceable\          #
#                 -sync_uncertain                    $sync_uncertain\          #
#                 -correction_field                  $correction_field\        #
#                 -message_type_specific             $message_type_specific\   #
#                 -src_port_number                   $src_port_number\         #
#                 -src_clock_identity                $src_clock_identity\      #
#                 -sequence_id                       $sequence_id\             #
#                 -control_field                     $control_field\           #
#                 -log_message_interval              $log_message_interval\    #
#                 -hexdump                           hexdump                   #
#                                                                              #
################################################################################

proc pbLib::encode_ptp_ha_common_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
    } else {

        set major_sdo_id 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
    } else {

        set message_type 0
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
    } else {

        set minor_version 2
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
    } else {

        set version_ptp 2
    }

    #PTP HA message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
    } else {

        set message_length 44
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
    } else {

        set domain_number 0
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
    } else {

        set minor_sdo_id 0
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
    } else {

        set unicast_flag 0
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
    } else {

        set profile_specific2 0
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
    } else {

        set secure 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
    } else {

        set current_utc_offset_valid 0
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
    } else {

        set freq_traceable 0
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
    } else {

        set sync_uncertain 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
    } else {

        set correction_field 0
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
    } else {

        set message_type_specific 0
    }

    #Source port number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
    } else {

        set src_port_number 0
    }

    #Source clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
    } else {

        set src_clock_identity 0
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
    } else {

        set sequence_id 0
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
    } else {

        set control_field 0
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
    } else {

        set log_message_interval 0
    }

    #OUTPUT PARAMETERS

    #Hex dump of PTP HA Commom Header
    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    #LOGIC GOES HERE
    #FLAGS
    ptp_ha_flags flags

    set flags::alternateMasterFlag         $alternate_master_flag
    set flags::twoStepFlag                 $two_step_flag
    set flags::unicastFlag                 $unicast_flag
    set flags::reserved                    0
    set flags::profileSpecific1            $profile_specific1
    set flags::profileSpecific2            $profile_specific2
    set flags::secure                      $secure
    set flags::leap61                      $leap61
    set flags::leap59                      $leap59
    set flags::currentUtcOffsetValid       $current_utc_offset_valid
    set flags::ptpTimescale                $ptp_timescale
    set flags::timeTraceable               $time_traceable
    set flags::frequencyTraceable          $freq_traceable
    set flags::synchronizationUncertain    $sync_uncertain
    set flags::reserved2                   0

    set flag_field [flags::pkt]

    #Source port Identity
    ptp_ha_port_identity port_identity

    set port_identity::clockIdentity   [dec2hex $src_clock_identity 16]
    set port_identity::portNumber      [dec2hex $src_port_number 4]

    set source_port_identity [port_identity::pkt]


    #Header
    ptp_ha_common hdr

    set hdr::majorSdoId           [dec2hex $major_sdo_id 1]
    set hdr::messageType          [dec2hex $message_type 1]
    set hdr::minorVersionPTP      [dec2hex $minor_version 1]
    set hdr::versionPTP           [dec2hex $version_ptp 1]
    set hdr::messageLength        [dec2hex $message_length 4]
    set hdr::domainNumber         [dec2hex $domain_number 2]
    set hdr::minorSdoId           [dec2hex $minor_sdo_id 2]
    set hdr::flagField            $flag_field
    set hdr::correctionField      [dec2hex [expr round($correction_field * pow(2,16))] 16]
    set hdr::messageTypeSpecific  [dec2hex $message_type_specific 8]
    set hdr::sourcePortIdentity   $source_port_identity
    set hdr::sequenceId           [dec2hex $sequence_id 4]
    set hdr::controlField         [dec2hex $control_field 2]
    set hdr::logMessageInterval   [dec2hex $log_message_interval 2]

    set hexdump [hdr::pkt]

    return $hexdump

}

################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::encode_ptp_ha_sync_header                     #
#                                                                              #
#  DEFINITION           : This function encodes ptp ha sync header.            #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  origin_timestamp_sec :  (Optional)  Origin timestamp in seconds             #
#                                      (Default : 0)                           #
#                                                                              #
#  origin_timestamp_ns  :  (Optional)  Origin timestamp in nanoseconds         #
#                                      (Default : 0)                           #
#                                                                              #
#  OUTPUT PARAMETERS    : NONE                                                 #
#                                                                              #
#  hexdump              :  (Optional)  Hex dump of PTP HA Sync Header          #
#                                                                              #
#                                                                              #
#  RETURNS              : PTP HA header hex dump                               #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#    pbLib::encode_ptp_ha_sync_header\                                         #
#                -origin_timestamp_sec              $origin_timestamp_sec\     #
#                -origin_timestamp_ns               $origin_timestamp_ns\      #
#                -hexdump                           hexdump                    #
#                                                                              #
################################################################################

proc pbLib::encode_ptp_ha_sync_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #Origin timestamp in seconds
    if {[info exists param(-origin_timestamp_sec)]} {

        set origin_timestamp_sec $param(-origin_timestamp_sec)
    } else {

        set origin_timestamp_sec 0
    }

    #Origin timestamp in nanoseconds
    if {[info exists param(-origin_timestamp_ns)]} {

        set origin_timestamp_ns $param(-origin_timestamp_ns)
    } else {

        set origin_timestamp_ns 0
    }

    #OUTPUT PARAMETERS

    #Hex dump of PTP HA Sync Header
    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    #LOGIC GOES HERE

    #Timestamp
    ptp_ha_timestamp timestamp

    set timestamp::secondsField     [dec2hex $origin_timestamp_sec 12]
    set timestamp::nanosecondsField [dec2hex $origin_timestamp_ns 8]

    set origin_timestamp [timestamp::pkt]


    #Header
    ptp_ha_sync hdr

    set hdr::originTimestamp $origin_timestamp

    set hexdump [hdr::pkt]

    return $hexdump

}

################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::encode_ptp_ha_announce_header                 #
#                                                                              #
#  DEFINITION           : This function encodes ptp ha announce header         #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  origin_timestamp_sec :  (Optional)  Origin timestamp in seconds             #
#                                      (Default : 0)                           #
#                                                                              #
#  origin_timestamp_ns  :  (Optional)  Origin timestamp in nanoseconds         #
#                                      (Default : 0)                           #
#                                                                              #
#  current_utc_offset   :  (Optional)  Current UTC Offset                      #
#                                      (Default : 0)                           #
#                                                                              #
#  gm_priority1         :  (Optional)  Grand Master Priority 1                 #
#                                      (Default : 0)                           #
#                                                                              #
#  gm_priority2         :  (Optional)  Grand Master Priority 2                 #
#                                      (Default : 128)                         #
#                                                                              #
#  gm_identity          :  (Optional)  Grand Master Identity                   #
#                                      (Default : 0)                           #
#                                                                              #
#  steps_removed        :  (Optional)  Steps Removed                           #
#                                      (Default : 0)                           #
#                                                                              #
#  time_source          :  (Optional)  Time Source                             #
#                                      (Default : 0)                           #
#                                                                              #
#  gm_clock_classy      :  (Optional)  Grand Master Clock classy               #
#                                      (Default : 0)                           #
#                                                                              #
#  gm_clock_accuracy    :  (Optional)  Grand Master Clock accuracy             #
#                                      (Default : 0)                           #
#                                                                              #
#  gm_clock_variance    :  (Optional)  Grand Master Clock offset scaled        #
#                                        log variance (Default : 0)            #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS    : NONE                                                 #
#                                                                              #
#  hexdump              :  (Optional)  PTP HA Announce header hex dump         #
#                                                                              #
#  RETURNS              : PTP HA packet hex dump                               #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#   pbLib::encode_ptp_ha_announce_header\                                      #
#               -origin_timestamp_sec              $origin_timestamp_sec\      #
#               -origin_timestamp_ns               $origin_timestamp_ns\       #
#               -current_utc_offset                $current_utc_offset\        #
#               -gm_priority1                      $gm_priority1\              #
#               -gm_priority2                      $gm_priority2\              #
#               -gm_identity                       $gm_identity\               #
#               -steps_removed                     $steps_removed\             #
#               -time_source                       $time_source\               #
#               -gm_clock_classy                   $gm_clock_classy\           #
#               -gm_clock_accuracy                 $gm_clock_accuracy\         #
#               -gm_clock_variance                 $gm_clock_variance\         #
#               -hexdump                           hexdump                     #
#                                                                              #
################################################################################

proc pbLib::encode_ptp_ha_announce_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #Origin timestamp in seconds
    if {[info exists param(-origin_timestamp_sec)]} {

        set origin_timestamp_sec $param(-origin_timestamp_sec)
    } else {

        set origin_timestamp_sec 0
    }

    #Origin timestamp in nanoseconds
    if {[info exists param(-origin_timestamp_ns)]} {

        set origin_timestamp_ns $param(-origin_timestamp_ns)
    } else {

        set origin_timestamp_ns 0
    }

    #Current UTC Offset
    if {[info exists param(-current_utc_offset)]} {

        set current_utc_offset $param(-current_utc_offset)
    } else {

        set current_utc_offset 0
    }

    #Grand Master Priority 1
    if {[info exists param(-gm_priority1)]} {

        set gm_priority1 $param(-gm_priority1)
    } else {

        set gm_priority1 0
    }

    #Grand Master Priority 2
    if {[info exists param(-gm_priority2)]} {

        set gm_priority2 $param(-gm_priority2)
    } else {

        set gm_priority2 128
    }

    #Grand Master Identity
    if {[info exists param(-gm_identity)]} {

        set gm_identity $param(-gm_identity)
    } else {

        set gm_identity 0
    }

    #Steps Removed
    if {[info exists param(-steps_removed)]} {

        set steps_removed $param(-steps_removed)
    } else {

        set steps_removed 0
    }

    #Time Source
    if {[info exists param(-time_source)]} {

        set time_source $param(-time_source)
    } else {

        set time_source 0
    }

    #Grand Master Clock classy
    if {[info exists param(-gm_clock_classy)]} {

        set gm_clock_classy $param(-gm_clock_classy)
    } else {

        set gm_clock_classy 0
    }

    #Grand Master Clock accuracy
    if {[info exists param(-gm_clock_accuracy)]} {

        set gm_clock_accuracy $param(-gm_clock_accuracy)
    } else {

        set gm_clock_accuracy 0
    }

    #Grand Master Clock offset scaled
    if {[info exists param(-gm_clock_variance)]} {

        set gm_clock_variance $param(-gm_clock_variance)
    } else {

        set gm_clock_variance 0
    }

    #OUTPUT PARAMETERS

    #PTP HA Announce header hex dump
    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    #LOGIC GOES HERE

    #Timestamp
    ptp_ha_timestamp timestamp

    set timestamp::secondsField     [dec2hex $origin_timestamp_sec 12]
    set timestamp::nanosecondsField [dec2hex $origin_timestamp_ns 8]

    set originTimestamp [timestamp::pkt]


    #Clock Quality
    ptp_ha_clock_quality clock_quality

    set clock_quality::clockClass              [dec2hex $gm_clock_classy 2]
    set clock_quality::clockAccuracy           [dec2hex $gm_clock_accuracy 2]
    set clock_quality::offsetScaledLogVariance [dec2hex $gm_clock_variance 4]

    set grandmasterClockQuality [clock_quality::pkt]


    #Header
    ptp_ha_announce hdr

    set hdr::originTimestamp         $originTimestamp
    set hdr::currentUtcOffset        [dec2hex $current_utc_offset 4]
    set hdr::reserved                [dec2hex 0 2]
    set hdr::grandmasterPriority1    [dec2hex $gm_priority1 2]
    set hdr::grandmasterClockQuality $grandmasterClockQuality
    set hdr::grandmasterPriority2    [dec2hex $gm_priority2 2]
    set hdr::grandmasterIdentity     [dec2hex $gm_identity 16]
    set hdr::stepsRemoved            [dec2hex $steps_removed 4]
    set hdr::timeSource              [dec2hex $time_source 2]

    set hexdump [hdr::pkt]

    return $hexdump

}

################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::encode_ptp_ha_delay_req_header                #
#                                                                              #
#  DEFINITION           : This function encodes ptp ha delay req header.       #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  origin_timestamp_sec :  (Optional)  Origin timestamp in seconds             #
#                                      (Default : 0)                           #
#                                                                              #
#  origin_timestamp_ns  :  (Optional)  Origin timestamp in nanoseconds         #
#                                      (Default : 0)                           #
#                                                                              #
#  OUTPUT PARAMETERS    : NONE                                                 #
#                                                                              #
#  hexdump              :  (Optional)  Hex dump of PTP HA Delay request Header #
#                                                                              #
#                                                                              #
#  RETURNS              : PTP HA delay request header hex dump                 #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#    pbLib::encode_ptp_ha_delay_req_header\                                    #
#                -origin_timestamp_sec              $origin_timestamp_sec\     #
#                -origin_timestamp_ns               $origin_timestamp_ns\      #
#                -hexdump                           hexdump                    #
#                                                                              #
################################################################################

proc pbLib::encode_ptp_ha_delay_req_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #Origin timestamp in seconds
    if {[info exists param(-origin_timestamp_sec)]} {

        set origin_timestamp_sec $param(-origin_timestamp_sec)
    } else {

        set origin_timestamp_sec 0
    }

    #Origin timestamp in nanoseconds
    if {[info exists param(-origin_timestamp_ns)]} {

        set origin_timestamp_ns $param(-origin_timestamp_ns)
    } else {

        set origin_timestamp_ns 0
    }

    #OUTPUT PARAMETERS

    #Hex dump of PTP HA Sync Header
    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    #LOGIC GOES HERE

    #Timestamp
    ptp_ha_timestamp timestamp

    set timestamp::secondsField     [dec2hex $origin_timestamp_sec 12]
    set timestamp::nanosecondsField [dec2hex $origin_timestamp_ns 8]

    set origin_timestamp [timestamp::pkt]


    #Header
    ptp_ha_delay_req hdr

    set hdr::originTimestamp $origin_timestamp

    set hexdump [hdr::pkt]

    return $hexdump

}

################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::encode_ptp_ha_delay_resp_header               #
#                                                                              #
#  DEFINITION           : This function encodes ptp ha delay response header.  #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  receive_timestamp_sec         :  (Optional)  Receive Timestamp              #
#                                     (Default : 0)                            #
#                                                                              #
#  receive_timestamp_ns          :  (Optional)  Receive Timestamp              #
#                                     (Default : 0)                            #
#                                                                              #
#  requesting_port_number        :  (Optional)  Requesting Port Number         #
#                                       (Default : 0)                          #
#                                                                              #
#  requesting_clock_identity     :  (Optional)  Requesting Clock Identity      #
#                                       (Default : 0)                          #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  hexdump              :  (Optional) Hex dump of PTP HA Delay response header #
#                                                                              #
#                                                                              #
#  RETURNS              : PTP HA delay reponse header hex dump                 #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#   pbLib::encode_ptp_ha_delay_resp_header\                                    #
#               -receive_timestamp_sec         $receive_timestamp_sec\         #
#               -receive_timestamp_ns          $receive_timestamp_ns\          #
#               -requesting_port_number        $requesting_port_number\        #
#               -requesting_clock_identity     $requesting_clock_identity\     #
#               -hexdump                       hexdump                         #
#                                                                              #
################################################################################

proc pbLib::encode_ptp_ha_delay_resp_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #Receive Timestamp
    if {[info exists param(-receive_timestamp_sec)]} {

        set receive_timestamp_sec $param(-receive_timestamp_sec)
    } else {

        set receive_timestamp_sec 0
    }

    #Receive Timestamp
    if {[info exists param(-receive_timestamp_ns)]} {

        set receive_timestamp_ns $param(-receive_timestamp_ns)
    } else {

        set receive_timestamp_ns 0
    }

    #Requesting Port Number
    if {[info exists param(-requesting_port_number)]} {

        set requesting_port_number $param(-requesting_port_number)
    } else {

        set requesting_port_number 0
    }

    #Requesting Clock Identity
    if {[info exists param(-requesting_clock_identity)]} {

        set requesting_clock_identity $param(-requesting_clock_identity)
    } else {

        set requesting_clock_identity 0
    }

    #OUTPUT PARAMETERS

    #Hex dump of PTP HA Delay response Header
    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }


    #LOGIC GOES HERE

    #Receive Timestamp
    ptp_ha_timestamp timestamp

    set timestamp::secondsField     [dec2hex $receive_timestamp_sec 12]
    set timestamp::nanosecondsField [dec2hex $receive_timestamp_ns 8]

    set receive_timestamp [timestamp::pkt]

    #Header
    ptp_ha_delay_resp hdr

    set hdr::receiveTimestamp        $receive_timestamp
    set hdr::requestingPortIdentity  [pbLib::encode_port_identity $requesting_clock_identity $requesting_port_number]

    set hexdump [hdr::pkt]

    return $hexdump

}

################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::encode_ptp_ha_followup_header                 #
#                                                                              #
#  DEFINITION           : This function encodes ptp ha follow up header.       #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  precise_origin_timestamp_sec :  (Optional)  Precise Origin timestamp in sec #
#                                      (Default : 0)                           #
#                                                                              #
#  precise_origin_timestamp_ns  :  (Optional)  Precise Origin timestamp in ns  #
#                                      (Default : 0)                           #
#                                                                              #
#  OUTPUT PARAMETERS    : NONE                                                 #
#                                                                              #
#  hexdump              :  (Optional)  Hex dump of PTP HA Follow up Header     #
#                                                                              #
#                                                                              #
#  RETURNS              : PTP HA follow up header hex dump                     #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#    pbLib::encode_ptp_ha_followup_header\                                     #
#                -precise_origin_timestamp_sec  $precise_origin_timestamp_sec\ #
#                -precise_origin_timestamp_ns   $precise_origin_timestamp_ns\  #
#                -hexdump                       hexdump                        #
#                                                                              #
################################################################################

proc pbLib::encode_ptp_ha_followup_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #Precise Origin timestamp in seconds
    if {[info exists param(-precise_origin_timestamp_sec)]} {

        set precise_origin_timestamp_sec $param(-precise_origin_timestamp_sec)
    } else {

        set precise_origin_timestamp_sec 0
    }

    #Precise Origin timestamp in nanoseconds
    if {[info exists param(-precise_origin_timestamp_ns)]} {

        set precise_origin_timestamp_ns $param(-precise_origin_timestamp_ns)
    } else {

        set precise_origin_timestamp_ns 0
    }

    #OUTPUT PARAMETERS

    #Hex dump of PTP HA Sync Header
    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    #LOGIC GOES HERE

    #Timestamp
    ptp_ha_timestamp timestamp

    set timestamp::secondsField     [dec2hex $precise_origin_timestamp_sec 12]
    set timestamp::nanosecondsField [dec2hex $precise_origin_timestamp_ns 8]

    set precise_origin_timestamp [timestamp::pkt]


    #Header
    ptp_ha_followup hdr

    set hdr::preciseOriginTimestamp $precise_origin_timestamp

    set hexdump [hdr::pkt]

    return $hexdump

}

################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::encode_ptp_ha_pdelay_req_header               #
#                                                                              #
#  DEFINITION           : This function encodes ptp ha peer delay request      #
#                         header                                               #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  origin_timestamp_sec :  (Optional)  Origin timestamp in seconds             #
#                                      (Default : 0)                           #
#                                                                              #
#  origin_timestamp_ns  :  (Optional)  Origin timestamp in nanoseconds         #
#                                            (Default : 0)                     #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  hexdump              :  (Optional)  PTP HA peer delay request header        #
#                                      hex dump                                #
#                                                                              #
#  RETURNS              : PTP HA peer delay request header hex dump            #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#   pbLib::encode_ptp_ha_pdelay_req_header\                                    #
#               -origin_timestamp_sec              $origin_timestamp_sec\      #
#               -origin_timestamp_ns               $origin_timestamp_ns\       #
#               -hexdump                           hexdump                     #
#                                                                              #
################################################################################

proc pbLib::encode_ptp_ha_pdelay_req_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #Origin timestamp in seconds
    if {[info exists param(-origin_timestamp_sec)]} {

        set origin_timestamp_sec $param(-origin_timestamp_sec)
    } else {

        set origin_timestamp_sec 0
    }

    #Origin timestamp in nanoseconds
    if {[info exists param(-origin_timestamp_ns)]} {

        set origin_timestamp_ns $param(-origin_timestamp_ns)
    } else {

        set origin_timestamp_ns 0
    }

    #OUTPUT PARAMETERS

    #PTP HA peer delay request header
    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    #LOGIC GOES HERE

    #Orgin Timestamp
    ptp_ha_timestamp timestamp

    set timestamp::secondsField     [dec2hex $origin_timestamp_sec 12]
    set timestamp::nanosecondsField [dec2hex $origin_timestamp_ns 8]

    set originTimestamp [timestamp::pkt]


    #Header
    ptp_ha_pdelay_req hdr

    set hdr::originTimestamp  $originTimestamp
    set hdr::reserved         [dec2hex 0 20]

    set hexdump [hdr::pkt]

    return $hexdump

}

################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::encode_ptp_ha_pdelay_resp_header              #
#                                                                              #
#  DEFINITION           : This function encodes ptp ha peer delay response     #
#                         header                                               #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  requesting_port_number        :  (Optional)  Requesting Port Number         #
#                                       (Default : 0)                          #
#                                                                              #
#  requesting_clock_identity     :  (Optional)  Requesting Clock Identity      #
#                                       (Default : 0)                          #
#                                                                              #
#  request_receipt_timestamp_sec :  (Optional)  Request Receipt Timestamp      #
#                                       (Default : 0)                          #
#                                                                              #
#  request_receipt_timestamp_ns  :  (Optional)  Request Receipt Timestamp      #
#                                       (Default : 0)                          #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  hexdump              :  (Optional)  PTP HA peer delay response header       #
#                                      hex dump                                #
#                                                                              #
#  RETURNS              : PTP HA peer delay response header hex dump           #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#   pbLib::encode_ptp_ha_pdelay_resp_header\                                   #
#           -requesting_port_number         $requesting_port_number\           #
#           -requesting_clock_identity      $requesting_clock_identity\        #
#           -request_receipt_timestamp_sec  $request_receipt_timestamp_sec\    #
#           -request_receipt_timestamp_ns   $request_receipt_timestamp_ns\     #
#           -hexdump                        hexdump                            #
#                                                                              #
################################################################################

proc pbLib::encode_ptp_ha_pdelay_resp_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #Requesting Port Number
    if {[info exists param(-requesting_port_number)]} {

        set requesting_port_number $param(-requesting_port_number)
    } else {

        set requesting_port_number 0
    }

    #Requesting Clock Identity
    if {[info exists param(-requesting_clock_identity)]} {

        set requesting_clock_identity $param(-requesting_clock_identity)
    } else {

        set requesting_clock_identity 0
    }

    #Request Receipt Timestamp
    if {[info exists param(-request_receipt_timestamp_sec)]} {

        set request_receipt_timestamp_sec $param(-request_receipt_timestamp_sec)
    } else {

        set request_receipt_timestamp_sec 0
    }

    #Request Receipt Timestamp
    if {[info exists param(-request_receipt_timestamp_ns)]} {

        set request_receipt_timestamp_ns $param(-request_receipt_timestamp_ns)
    } else {

        set request_receipt_timestamp_ns 0
    }

    #OUTPUT PARAMETERS

    #PTP HA peer delay response header
    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    #LOGIC GOES HERE

    #Request Receipt Timestamp
    ptp_ha_timestamp timestamp

    set timestamp::secondsField     [dec2hex $request_receipt_timestamp_sec 12]
    set timestamp::nanosecondsField [dec2hex $request_receipt_timestamp_ns 8]

    set requestReceiptTimestamp [timestamp::pkt]


    #Requesting port Identity
    ptp_ha_port_identity port_identity

    set port_identity::clockIdentity   [dec2hex $requesting_clock_identity 16]
    set port_identity::portNumber      [dec2hex $requesting_port_number 4]

    set requestingPortIdentity [port_identity::pkt]


    #Header
    ptp_ha_pdelay_resp hdr

    set hdr::requestReceiptTimestamp $requestReceiptTimestamp
    set hdr::requestingPortIdentity  $requestingPortIdentity

    set hexdump [hdr::pkt]

    return $hexdump

}

################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::encode_ptp_ha_pdelay_resp_followup_header     #
#                                                                              #
#  DEFINITION           : This function encodes ptp ha peer delay response     #
#                         followup header                                      #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  requesting_port_number        :  (Optional)  Requesting Port Number         #
#                                       (Default : 0)                          #
#                                                                              #
#  requesting_clock_identity     :  (Optional)  Requesting Clock Identity      #
#                                       (Default : 0)                          #
#                                                                              #
#  response_origin_timestamp_sec :  (Optional)  Response Origin Timestamp      #
#                                       (Default : 0)                          #
#                                                                              #
#  response_origin_timestamp_ns  :  (Optional)  Response Origin Timestamp      #
#                                      (Default : 0)                           #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  hexdump              :  (Optional)  PTP HA Peer delay response followup     #
#                                      header hex dump                         #
#                                                                              #
#  RETURNS              : PTP HA Peer delay response followup header hexdump   #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#   pbLib::encode_ptp_ha_pdelay_resp_followup_header\                          #
#           -requesting_port_number            $requesting_port_number\        #
#           -requesting_clock_identity         $requesting_clock_identity\     #
#           -response_origin_timestamp_sec     $response_origin_timestamp_sec\ #
#           -response_origin_timestamp_ns      $response_origin_timestamp_ns\  #
#           -hexdump                           hexdump                         #
#                                                                              #
################################################################################

proc pbLib::encode_ptp_ha_pdelay_resp_followup_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #Requesting Port Number
    if {[info exists param(-requesting_port_number)]} {

        set requesting_port_number $param(-requesting_port_number)
    } else {

        set requesting_port_number 0
    }

    #Requesting Clock Identity
    if {[info exists param(-requesting_clock_identity)]} {

        set requesting_clock_identity $param(-requesting_clock_identity)
    } else {

        set requesting_clock_identity 0
    }

    #Response Origin Timestamp
    if {[info exists param(-response_origin_timestamp_sec)]} {

        set response_origin_timestamp_sec $param(-response_origin_timestamp_sec)
    } else {

        set response_origin_timestamp_sec 0
    }

    #Response Origin Timestamp
    if {[info exists param(-response_origin_timestamp_ns)]} {

        set response_origin_timestamp_ns $param(-response_origin_timestamp_ns)
    } else {

        set response_origin_timestamp_ns 0
    }

    #OUTPUT PARAMETERS

    #PTP HA Peer delay response followup
    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    #LOGIC GOES HERE

    #Response Origin Timestamp
    ptp_ha_timestamp timestamp

    set timestamp::secondsField     [dec2hex $response_origin_timestamp_sec 12]
    set timestamp::nanosecondsField [dec2hex $response_origin_timestamp_ns 8]

    set responseOriginTimestamp [timestamp::pkt]


    #Requesting port Identity
    ptp_ha_port_identity port_identity

    set port_identity::clockIdentity   [dec2hex $requesting_clock_identity 16]
    set port_identity::portNumber      [dec2hex $requesting_port_number 4]

    set requestingPortIdentity [port_identity::pkt]


    #Header
    ptp_ha_pdelay_resp_followup hdr

    set hdr::responseOriginTimestamp $responseOriginTimestamp
    set hdr::requestingPortIdentity  $requestingPortIdentity

    set hexdump [hdr::pkt]

    return $hexdump

}

################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::encode_ptp_ha_signal_header                   #
#                                                                              #
#  DEFINITION           : This function encodes ptp ha signal header           #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  target_port_number            :  (Optional)  Target Port Number             #
#                                       (Default : 0)                          #
#                                                                              #
#  target_clock_identity         :  (Optional)  Target Clock Identity          #
#                                       (Default : 0)                          #
#                                                                              #
#  tlv                           :  (Optional)  TLV                            #
#                                       (Default : L1_SYNC)                    #
#                                                                              #
#  tlv_type                      :  (Optional)  TLV Type                       #
#                                       (Default : 32769)                      #
#                                                                              #
#  tlv_length                    :  (Optional)  TLV length                     #
#                                       (Default : 30)                         #
#                                                                              #
#  ope                           :  (Optional)  OPE Flag                       #
#                                       (Default : 0)                          #
#                                                                              #
#  cr                            :  (Optional)  CR Flag                        #
#                                       (Default : 0)                          #
#                                                                              #
#  rcr                           :  (Optional)  RCR Flag                       #
#                                       (Default : 0)                          #
#                                                                              #
#  tcr                           :  (Optional)  TCR Flag                       #
#                                       (Default : 0)                          #
#                                                                              #
#  ic                            :  (Optional)  IC Flag                        #
#                                       (Default : 0)                          #
#                                                                              #
#  irc                           :  (Optional)  IRC Flag                       #
#                                       (Default : 0)                          #
#                                                                              #
#  itc                           :  (Optional)  ITC Flag                       #
#                                       (Default : 0)                          #
#                                                                              #
#  fov                           :  (Optional)  FOV Flag                       #
#                                       (Default : 0)                          #
#                                                                              #
#  pov                           :  (Optional)  POV Flag                       #
#                                       (Default : 0)                          #
#                                                                              #
#  tct                           :  (Optional)  TCT Flag                       #
#                                       (Default : 0)                          #
#                                                                              #
#  phase_offset_tx               :  (Optional)  Phase offset tx                #
#                                       (Default : 0)                          #
#                                                                              #
#  phase_offset_tx_timestamp_sec :  (Optional)  Phase offset tx timestamp Sec  #
#                                       (Default : 0)                          #
#                                                                              #
#  phase_offset_tx_timestamp_ns  :  (Optional)  Phase offset tx timsstamp Ns   #
#                                       (Default : 0)                          #
#                                                                              #
#  freq_offset_tx                :  (Optional)  Frequency offset tx            #
#                                       (Default : 0)                          #
#                                                                              #
#  freq_offset_tx_timestamp_sec  :  (Optional)  Frequency offset tx timestamp  #
#                                       (Default : 0)                          #
#                                                                              #
#  freq_offset_tx_timestamp_ns   :  (Optional)  Frequency offset tx timstamp   #
#                                       (Default : 0)                          #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  hexdump              :  (Optional)  PTP HA Signal header hex dump           #
#                                                                              #
#  RETURNS              : PTP HA Signal header hex dump                        #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#   pbLib::encode_ptp_ha_signal_header\                                        #
#              -target_port_number             $target_port_number\            #
#              -target_clock_identity          $target_clock_identity\         #
#              -tlv                            $tlv\                           #
#              -tlv_type                       $tlv_type\                      #
#              -tlv_length                     $tlv_length\                    #
#              -ope                            $ope\                           #
#              -cr                             $cr\                            #
#              -rcr                            $rcr\                           #
#              -tcr                            $tcr\                           #
#              -ic                             $ic\                            #
#              -irc                            $irc\                           #
#              -itc                            $itc\                           #
#              -fov                            $fov\                           #
#              -pov                            $pov\                           #
#              -tct                            $tct\                           #
#              -phase_offset_tx                $phase_offset_tx\               #
#              -phase_offset_tx_timestamp_sec  $phase_offset_tx_timestamp_sec\ #
#              -phase_offset_tx_timestamp_ns   $phase_offset_tx_timestamp_ns\  #
#              -freq_offset_tx                 $freq_offset_tx\                #
#              -freq_offset_tx_timestamp_sec   $freq_offset_tx_timestamp_sec\  #
#              -freq_offset_tx_timestamp_ns    $freq_offset_tx_timestamp_ns\   #
#              -hexdump                        hexdump                         #
#                                                                              #
################################################################################

proc pbLib::encode_ptp_ha_signal_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #Target Port Number
    if {[info exists param(-target_port_number)]} {

        set target_port_number $param(-target_port_number)
    } else {

        set target_port_number 0
    }

    #Target Clock Identity
    if {[info exists param(-target_clock_identity)]} {

        set target_clock_identity $param(-target_clock_identity)
    } else {

        set target_clock_identity 0
    }

    #TLV
    if {[info exists param(-tlv)]} {

        set tlv $param(-tlv)
    } else {

        set tlv "L1_SYNC"
    }

    #TLV Type
    if {[info exists param(-tlv_type)]} {

        set tlv_type $param(-tlv_type)
    } else {

        set tlv_type [expr 0x8001]
    }

    #TLV length
    if {[info exists param(-tlv_length)]} {

        set tlv_length $param(-tlv_length)
    } else {

        set tlv_length 30
    }

    #OPE Flag
    if {[info exists param(-ope)]} {

        set ope $param(-ope)
    } else {

        set ope 0
    }

    #CR Flag
    if {[info exists param(-cr)]} {

        set cr $param(-cr)
    } else {

        set cr 0
    }

    #RCR Flag
    if {[info exists param(-rcr)]} {

        set rcr $param(-rcr)
    } else {

        set rcr 0
    }

    #TCR Flag
    if {[info exists param(-tcr)]} {

        set tcr $param(-tcr)
    } else {

        set tcr 0
    }

    #IC Flag
    if {[info exists param(-ic)]} {

        set ic $param(-ic)
    } else {

        set ic 0
    }

    #IRC Flag
    if {[info exists param(-irc)]} {

        set irc $param(-irc)
    } else {

        set irc 0
    }

    #ITC Flag
    if {[info exists param(-itc)]} {

        set itc $param(-itc)
    } else {

        set itc 0
    }

    #FOV Flag
    if {[info exists param(-fov)]} {

        set fov $param(-fov)
    } else {

        set fov 0
    }

    #POV Flag
    if {[info exists param(-pov)]} {

        set pov $param(-pov)
    } else {

        set pov 0
    }

    #TCT Flag
    if {[info exists param(-tct)]} {

        set tct $param(-tct)
    } else {

        set tct 0
    }

    #Phase offset tx
    if {[info exists param(-phase_offset_tx)]} {

        set phase_offset_tx $param(-phase_offset_tx)
    } else {

        set phase_offset_tx 0
    }

    #Phase offset tx timestamp Sec
    if {[info exists param(-phase_offset_tx_timestamp_sec)]} {

        set phase_offset_tx_timestamp_sec $param(-phase_offset_tx_timestamp_sec)
    } else {

        set phase_offset_tx_timestamp_sec 0
    }

    #Phase offset tx timsstamp Ns
    if {[info exists param(-phase_offset_tx_timestamp_ns)]} {

        set phase_offset_tx_timestamp_ns $param(-phase_offset_tx_timestamp_ns)
    } else {

        set phase_offset_tx_timestamp_ns 0
    }

    #Frequency offset tx
    if {[info exists param(-freq_offset_tx)]} {

        set freq_offset_tx $param(-freq_offset_tx)
    } else {

        set freq_offset_tx 0
    }

    #Frequency offset tx timestamp
    if {[info exists param(-freq_offset_tx_timestamp_sec)]} {

        set freq_offset_tx_timestamp_sec $param(-freq_offset_tx_timestamp_sec)
    } else {

        set freq_offset_tx_timestamp_sec 0
    }

    #Frequency offset tx timstamp
    if {[info exists param(-freq_offset_tx_timestamp_ns)]} {

        set freq_offset_tx_timestamp_ns $param(-freq_offset_tx_timestamp_ns)
    } else {

        set freq_offset_tx_timestamp_ns 0
    }

    #OUTPUT PARAMETERS

    #PTP HA Signal header hex dump
    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    #LOGIC GOES HERE

    #Target Port Identity
    ptp_ha_port_identity port_identity

    set port_identity::clockIdentity   [dec2hex $target_clock_identity 16]
    set port_identity::portNumber      [dec2hex $target_port_number 4]

    set targetPortIdentity [port_identity::pkt]

    #Header
    ptp_ha_signal hdr

    set hdr::targetPortIdentity $targetPortIdentity

    set hexdump [hdr::pkt]

    #L1 Sync
    if {$tlv == "L1_SYNC"} {

        if {$ope} {
            #L1 SYNC EXTENDED FORMAT
            ptp_ha_l1_sync_tlv_ext l1_sync

            #Phase Offset Tx Timestamp
            ptp_ha_timestamp phase_timestamp
            
            set phase_timestamp::secondsField     [dec2hex $phase_offset_tx_timestamp_sec 12]
            set phase_timestamp::nanosecondsField [dec2hex $phase_offset_tx_timestamp_sec 8]
            
            set phaseOffsetTxTimestamp [phase_timestamp::pkt]
            
            #Frequency Offset Tx Timestamp
            ptp_ha_timestamp freq_timestamp
            
            set freq_timestamp::secondsField     [dec2hex $freq_offset_tx_timestamp_sec 12]
            set freq_timestamp::nanosecondsField [dec2hex $freq_offset_tx_timestamp_sec 8]
            
            set freqOffsetTxTimestamp [freq_timestamp::pkt]

            set l1_sync::reserved3               0
            set l1_sync::FOV                     $fov
            set l1_sync::POV                     $pov
            set l1_sync::TCT                     $tct
            set l1_sync::phaseOffsetTx           [dec2hex $phase_offset_tx 16]
            set l1_sync::phaseOffsetTxTimestamp  $phaseOffsetTxTimestamp
            set l1_sync::freqOffsetTx            [dec2hex $freq_offset_tx 16]
            set l1_sync::freqOffsetTxTimestamp   $freqOffsetTxTimestamp
            set l1_sync::reserved4               0

        } else {
            #L1 SYNC
            ptp_ha_l1_sync_tlv l1_sync
        }

        set l1_sync::reserved1               0
        set l1_sync::OPE                     $ope
        set l1_sync::CR                      $cr
        set l1_sync::RCR                     $rcr
        set l1_sync::TCR                     $tcr
        set l1_sync::reserved2               0
        set l1_sync::IC                      $ic
        set l1_sync::IRC                     $irc
        set l1_sync::ITC                     $itc

        set l1_sync_hex [l1_sync::pkt]

        ptp_ha_signal_tlv signal_tlv

        set tlv_length [size_of $l1_sync_hex]

        set signal_tlv::tlvType         [dec2hex $tlv_type 4]
        set signal_tlv::lengthField     [dec2hex $tlv_length 4]

        append hexdump [signal_tlv::pkt]

        append hexdump $l1_sync_hex
    }

    return $hexdump

}

################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::encode_ptp_ha_mgmt_header                     #
#                                                                              #
#  DEFINITION           : This function encodes ptp ha management header       #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  target_port_number        :  (Optional)  Target Port Number                 #
#                                   (Default : 0)                              #
#                                                                              #
#  target_clock_identity     :  (Optional)  Target Clock Identity              #
#                                   (Default : 0)                              #
#                                                                              #
#  starting_boundary_hops    :  (Optional)  Starting Boundary Hops             #
#                                   (Default : 0)                              #
#                                                                              #
#  boundary_hops             :  (Optional)  Boundary Hops                      #
#                                   (Default : 0)                              #
#                                                                              #
#  action_field              :  (Optional)  Action Field                       #
#                                   (Default : 0)                              #
#                                                                              #
#  tlv_type                  :  (Optional)  TLV Type                           #
#                                   (Default : 1)                              #
#                                                                              #
#  tlv_length                :  (Optional)  TLV length                         #
#                                   (Default : 4)                              #
#                                                                              #
#  management_id             :  (Optional)  Management ID                      #
#                                   (Default : 12288)                          #
#                                                                              #
#  ext_port_config           :  (Optional)  External Port Configuration        #
#                                   (Default : 0)                              #
#                                                                              #
#  master_only               :  (Optional)  Master only                        #
#                                   (Default : 0)                              #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  hexdump              :  (Optional)  PTP HA Management header hex dump       #
#                                                                              #
#  RETURNS              : PTP HA Management header hex dump                    #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#   pbLib::encode_ptp_ha_mgmt_header\                                          #
#               -target_port_number                $target_port_number\        #
#               -target_clock_identity             $target_clock_identity\     #
#               -starting_boundary_hops            $starting_boundary_hops\    #
#               -boundary_hops                     $boundary_hops\             #
#               -action_field                      $action_field\              #
#               -tlv_type                          $tlv_type\                  #
#               -tlv_length                        $tlv_length\                #
#               -management_id                     $management_id\             #
#               -ext_port_config                   $ext_port_config\           #
#               -master_only                       $master_only\               #
#               -hexdump                           hexdump                     #
#                                                                              #
################################################################################

proc pbLib::encode_ptp_ha_mgmt_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #Target Port Number
    if {[info exists param(-target_port_number)]} {

        set target_port_number $param(-target_port_number)
    } else {

        set target_port_number 0
    }

    #Target Clock Identity
    if {[info exists param(-target_clock_identity)]} {

        set target_clock_identity $param(-target_clock_identity)
    } else {

        set target_clock_identity 0
    }

    #Starting Boundary Hops
    if {[info exists param(-starting_boundary_hops)]} {

        set starting_boundary_hops $param(-starting_boundary_hops)
    } else {

        set starting_boundary_hops 0
    }

    #Boundary Hops
    if {[info exists param(-boundary_hops)]} {

        set boundary_hops $param(-boundary_hops)
    } else {

        set boundary_hops 0
    }

    #Action Field
    if {[info exists param(-action_field)]} {

        set action_field $param(-action_field)
    } else {

        set action_field 0
    }

    #TLV Type
    if {[info exists param(-tlv_type)]} {

        set tlv_type $param(-tlv_type)
    } else {

        set tlv_type 1
    }

    #TLV length
    if {[info exists param(-tlv_length)]} {

        set tlv_length $param(-tlv_length)
    } else {

        set tlv_length 4
    }

    #Management ID
    if {[info exists param(-management_id)]} {

        set management_id $param(-management_id)
    } else {

        set management_id 12288
    }

    #External Port Configuration
    if {[info exists param(-ext_port_config)]} {

        set ext_port_config $param(-ext_port_config)
    } else {

        set ext_port_config 0
    }

    #Master only
    if {[info exists param(-master_only)]} {

        set master_only $param(-master_only)
    } else {

        set master_only 0
    }

    #OUTPUT PARAMETERS

    #PTP HA Management header hex dump
    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }

    #LOGIC GOES HERE

    #Target Port Identity
    ptp_ha_port_identity target_port_identity

    set target_port_identity::clockIdentity   [dec2hex $target_clock_identity 16]
    set target_port_identity::portNumber      [dec2hex $target_port_number 4]

    set targetPortIdentity [target_port_identity::pkt]


    #Header
    ptp_ha_mgmt hdr

    set hdr::targetPortIdentity      $targetPortIdentity

    set hdr::startingBoundaryHops    [dec2hex $starting_boundary_hops 2]
    set hdr::boundaryHops            [dec2hex $boundary_hops 2]
    set hdr::reserved1               [dec2hex 0 1]
    set hdr::actionField             [dec2hex $action_field 1]
    set hdr::reserved2               [dec2hex 0 1]

    set hexdump [hdr::pkt]

    #TLV VALUE FIELD
    switch $management_id {

        12288 {
            ptp_ha_master_only_tlv tlv_value

            set tlv_value::reserved1  0
            set tlv_value::MO         $master_only
            set tlv_value::reserved2  0

            set valueField [tlv_value::pkt]

        }

        12289 {
            ptp_ha_ext_port_conf_enabled_tlv tlv_value

            set tlv_value::reserved1  0
            set tlv_value::EPC        $ext_port_config
            set tlv_value::reserved2  0

            set valueField [tlv_value::pkt]
        }

        default {
            set valueField ""
        }

    }

    #TLV HEADER
    ptp_ha_mgmt_tlv mgmt_tlv
    set mgmt_tlv::tlvType       [dec2hex $tlv_type 4]
    set mgmt_tlv::lengthField   [dec2hex $tlv_length 4]
    set mgmt_tlv::managementId  [dec2hex $management_id 2]

    append hexdump [mgmt_tlv::pkt]

    append hexdump $valueField

    return $hexdump

}

################################################################################
#  PROCEDURE NAME       : pbLib::decode_ptp_ha_pkt                             #
#                                                                              #
#  DEFINITION           : This function decodes ptp ha packet                  #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  packet               : (Mandatory) ptp ha packet hex dump                   #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  (Ethernet HEADER)                                                           #
#                                                                              #
#  dest_mac             : (Optional) Destination MAC address of eth header     #
#                                                                              #
#  src_mac              : (Optional) Source MAC address of eth header          #
#                                                                              #
#  eth_type             : (Optional) Ethernet type/length of eth header        #
#                                                                              #
#                                                                              #
#  (VLAN HEADER)                                                               #
#                                                                              #
#  vlan_id              : (Optional) Vlan id of vlan header                    #
#                                                                              #
#  vlan_priority        : (Optional) Priority of vlan header                   #
#                                                                              #
#  vlan_dei             : (Optional) Eligible indicator of vlan header         #
#                                                                              #
#                                                                              #
#  (IPv4 HEADER)                                                               #
#                                                                              #
#  ip_version           : (Optional) Version of ip                             #
#                                                                              #
#  ip_header_length     : (Optional) Header length  of ipv4                    #
#                                                                              #
#  ip_tos               : (Optional) Type of service (hex value)               #
#                                                                              #
#  ip_total_length      : (Optional) Total length  of ipv4                     #
#                                                                              #
#  ip_identification    : (Optional) Unique id of ipv4                         #
#                                                                              #
#  ip_flags             : (Optional) Flags represents the control or identify  #
#                                 fragments.                                   #
#                                                                              #
#  ip_offset            : (Optional) Fragment offset                           #
#                                                                              #
#  ip_ttl               : (Optional) Time to live                              #
#                                                                              #
#  ip_checksum          : (Optional) IP Checksum                               #
#                                                                              #
#  ip_protocol          : (Optional) Protocol in the data portion of the       #
#                                    IP datagram.                              #
#                                                                              #
#  src_ip               : (Optional) IPv4 address of the sender of the packet  #
#                                                                              #
#  dest_ip              : (Optional) IPv4 address of the reciever of the packet#
#                                                                              #
#                                                                              #
#  (UDP HEADER)                                                                #
#                                                                              #
#  src_port             : (Optional) Sender's port number                      #
#                                                                              #
#  dest_port            : (Optional) Receiver's port number                    #
#                                                                              #
#  udp_length           : (Optional) Length in bytes of UDP header and data    #
#                                                                              #
#  udp_checksum         : (Optional) UDP Checksum                              #
#                                                                              #
#                                                                              #
#  (PTP HA COMMON HEADER)                                                      #
#                                                                              #
#  major_sdo_id          :  (Optional)  Major sdo id                           #
#                                                                              #
#  message_type          :  (Optional)  Message Type                           #
#                                                                              #
#  minor_version         :  (Optional)  PTP version number                     #
#                                                                              #
#  version_ptp           :  (Optional)  PTP minor version number               #
#                                                                              #
#  message_length        :  (Optional)  PTP Sync message length                #
#                                                                              #
#  domain_number         :  (Optional)  Domain number                          #
#                                                                              #
#  minor_sdo_id          :  (Optional)  Minor Sdo id                           #
#                                                                              #
#  alternate_master_flag :  (Optional)  Alternate Master flag                  #
#                                                                              #
#  two_step_flag         :  (Optional)  Two Step Flag                          #
#                                                                              #
#  unicast_flag          :  (Optional)  Unicast Flag                           #
#                                                                              #
#  profile_specific1     :  (Optional)  Profile specific1 Flag                 #
#                                                                              #
#  profile_specific2     :  (Optional)  Profile specific2 Flag                 #
#                                                                              #
#  secure                :  (Optional)  Secure Flag                            #
#                                                                              #
#  leap61                :  (Optional)  Leap61 Flag                            #
#                                                                              #
#  leap59                :  (Optional)  Leap59 Flag                            #
#                                                                              #
#  current_utc_offset_valid:(Optional)  Current UTC Offset Flag                #
#                                                                              #
#  ptp_timescale         :  (Optional)  PTP TimeSclae Flag                     #
#                                                                              #
#  time_traceable        :  (Optional)  Time Traceable Flag                    #
#                                                                              #
#  freq_traceable        :  (Optional)  Frequency Traceable Flag               #
#                                                                              #
#  sync_uncertain        :  (Optional)  Synchronization uncertain Flag         #
#                                                                              #
#  correction_field      :  (Optional)  Correction Field value                 #
#                                                                              #
#  message_type_specific :  (Optional)  Message type                           #
#                                                                              #
#  src_port_number       :  (Optional)  Source port number                     #
#                                                                              #
#  src_clock_identity    :  (Optional)  Source Clock identity                  #
#                                                                              #
#  sequence_id           :  (Optional)  Sequence ID                            #
#                                                                              #
#  control_field         :  (Optional)  Control field                          #
#                                                                              #
#  log_message_interval  :  (Optional)  Log message interval                   #
#                                                                              #
#  (PTP HA SYNC/DELAY REQUEST HEADER)                                          #
#                                                                              #
#  origin_timestamp_sec       :  (Optional)  Origin timestamp in seconds       #
#                                                                              #
#  origin_timestamp_ns        :  (Optional)  Origin timestamp in nanoseconds   #
#                                                                              #
#  (PTP HA ANNOUNCE HEADER)                                                    #
#                                                                              #
#  current_utc_offset         :  (Optional)  Current UTC Offset                #
#                                                                              #
#  gm_priority1               :  (Optional)  Grand Master Priority 1           #
#                                                                              #
#  gm_priority2               :  (Optional)  Grand Master Priority 2           #
#                                                                              #
#  gm_identity                :  (Optional)  Grand Master Identity             #
#                                                                              #
#  steps_removed              :  (Optional)  Steps Removed                     #
#                                                                              #
#  time_source                :  (Optional)  Time Source                       #
#                                                                              #
#  gm_clock_classy            :  (Optional)  Grand Master Clock classy         #
#                                                                              #
#  gm_clock_accuracy          :  (Optional)  Grand Master Clock accuracy       #
#                                                                              #
#  gm_clock_variance          :  (Optional)  Grand Master Clock offset scaled  #
#                                                                              #
#  (PTP HA OTHER HEADERS)                                                      #
#                                                                              #
#  precise_origin_timestamp_sec  :  (Optional)  Precise Origin Timestamp       #
#                                                                              #
#  precise_origin_timestamp_ns   :  (Optional)  Precise Origin Timestamp       #
#                                                                              #
#  receive_timestamp_sec         :  (Optional)  Receive Timestamp              #
#                                                                              #
#  receive_timestamp_ns          :  (Optional)  Receive Timestamp              #
#                                                                              #
#  requesting_port_number        :  (Optional)  Requesting Port Number         #
#                                                                              #
#  requesting_clock_identity     :  (Optional)  Requesting Clock Identity      #
#                                                                              #
#  request_receipt_timestamp_sec :  (Optional)  Request Receipt Timestamp      #
#                                                                              #
#  request_receipt_timestamp_ns  :  (Optional)  Request Receipt Timestamp      #
#                                                                              #
#  response_origin_timestamp_sec :  (Optional)  Response Origin Timestamp      #
#                                                                              #
#  response_origin_timestamp_ns  :  (Optional)  Response Origin Timestamp      #
#                                                                              #
#  target_port_number            :  (Optional)  Target Port Number             #
#                                                                              #
#  target_clock_identity         :  (Optional)  Target Clock Identity          #
#                                                                              #
#  starting_boundary_hops        :  (Optional)  Starting Boundary Hops         #
#                                                                              #
#  boundary_hops                 :  (Optional)  Boundary Hops                  #
#                                                                              #
#  action_field                  :  (Optional)  Action Field                   #
#                                                                              #
#  (PTP HA TLVs)                                                               #
#                                                                              #
#  tlv                           :  (Optional)  TLV Name                       #
#                                                                              #
#  tlv_type                      :  (Optional)  TLV Type                       #
#                                                                              #
#  tlv_length                    :  (Optional)  TLV length                     #
#                                                                              #
#  ope                           :  (Optional)  OPE Flag                       #
#                                                                              #
#  cr                            :  (Optional)  CR Flag                        #
#                                                                              #
#  rcr                           :  (Optional)  RCR Flag                       #
#                                                                              #
#  tcr                           :  (Optional)  TCR Flag                       #
#                                                                              #
#  ic                            :  (Optional)  IC Flag                        #
#                                                                              #
#  irc                           :  (Optional)  IRC Flag                       #
#                                                                              #
#  itc                           :  (Optional)  ITC Flag                       #
#                                                                              #
#  fov                           :  (Optional)  FOV Flag                       #
#                                                                              #
#  pov                           :  (Optional)  POV Flag                       #
#                                                                              #
#  tct                           :  (Optional)  TCT Flag                       #
#                                                                              #
#  phase_offset_tx               :  (Optional)  Phase offset tx                #
#                                                                              #
#  phase_offset_tx_timestamp_sec :  (Optional)  Phase offset tx timestamp Sec  #
#                                                                              #
#  phase_offset_tx_timestamp_ns  :  (Optional)  Phase offset tx timsstamp Ns   #
#                                                                              #
#  freq_offset_tx                :  (Optional)  Frequency offset tx            #
#                                                                              #
#  freq_offset_tx_timestamp_sec  :  (Optional)  Frequency offset tx timestamp  #
#                                                                              #
#  freq_offset_tx_timestamp_ns   :  (Optional)  Frequency offset tx timstamp   #
#                                                                              #
#  management_id                 :  (Optional)  Management ID                  #
#                                                                              #
#  ext_port_config               :  (Optional)  External Port Configuration    #
#                                                                              #
#  master_only                   :  (Optional)  Master only                    #
#                                                                              #
#  ip_checksum_expected          :  (Optional)  Expected IP Checksum           #
#                                                                              #
#  ip_checksum_status            :  (Optional)  Status of IP Checksum          #
#                                                                              #
#  udp_checksum_expected         :  (Optional)  Expected UDP Checksum          #
#                                                                              #
#  udp_checksum_status           :  (Optional)  Status of UDP Checksum         #
#                                                                              #
#  RETURNS              : 0 on failure (If the packet is not PTP HA packet)    #
#                                                                              #
#                         1 on success (If the packet is PTP HA packet)        #
#                                                                              #
#  USAGE                :                                                      #
#    pbLib::decode_ptp_ha_pkt\                                                 #
#             -dest_mac                       dest_mac\                        #
#             -src_mac                        src_mac\                         #
#             -eth_type                       eth_type\                        #
#             -vlan_id                        vlan_id\                         #
#             -vlan_priority                  vlan_priority\                   #
#             -vlan_dei                       vlan_dei\                        #
#             -ip_version                     ip_version\                      #
#             -ip_header_length               ip_header_length\                #
#             -ip_tos                         ip_tos\                          #
#             -ip_total_length                ip_total_length\                 #
#             -ip_identification              ip_identification\               #
#             -ip_flags                       ip_flags\                        #
#             -ip_offset                      ip_offset\                       #
#             -ip_ttl                         ip_ttl\                          #
#             -ip_checksum                    ip_checksum\                     #
#             -ip_protocol                    ip_protocol\                     #
#             -src_ip                         src_ip\                          #
#             -dest_ip                        dest_ip\                         #
#             -src_port                       src_port\                        #
#             -dest_port                      dest_port\                       #
#             -udp_length                     udp_length\                      #
#             -udp_checksum                   udp_checksum\                    #
#             -major_sdo_id                   major_sdo_id\                    #
#             -message_type                   message_type\                    #
#             -minor_version                  minor_version\                   #
#             -version_ptp                    version_ptp\                     #
#             -message_length                 message_length\                  #
#             -domain_number                  domain_number\                   #
#             -minor_sdo_id                   minor_sdo_id\                    #
#             -alternate_master_flag          alternate_master_flag\           #
#             -two_step_flag                  two_step_flag\                   #
#             -unicast_flag                   unicast_flag\                    #
#             -profile_specific1              profile_specific1\               #
#             -profile_specific2              profile_specific2\               #
#             -secure                         secure\                          #
#             -leap61                         leap61\                          #
#             -leap59                         leap59\                          #
#             -current_utc_offset_valid       current_utc_offset_valid\        #
#             -ptp_timescale                  ptp_timescale\                   #
#             -time_traceable                 time_traceable\                  #
#             -freq_traceable                 freq_traceable\                  #
#             -sync_uncertain                 sync_uncertain\                  #
#             -correction_field               correction_field\                #
#             -message_type_specific          message_type_specific\           #
#             -src_port_number                src_port_number\                 #
#             -src_clock_identity             src_clock_identity\              #
#             -sequence_id                    sequence_id\                     #
#             -control_field                  control_field\                   #
#             -log_message_interval           log_message_interval\            #
#             -origin_timestamp_sec           origin_timestamp_sec\            #
#             -origin_timestamp_ns            origin_timestamp_ns\             #
#             -current_utc_offset             current_utc_offset\              #
#             -gm_priority1                   gm_priority1\                    #
#             -gm_priority2                   gm_priority2\                    #
#             -gm_identity                    gm_identity\                     #
#             -steps_removed                  steps_removed\                   #
#             -time_source                    time_source\                     #
#             -gm_clock_classy                gm_clock_classy\                 #
#             -gm_clock_accuracy              gm_clock_accuracy\               #
#             -gm_clock_variance              gm_clock_variance\               #
#             -precise_origin_timestamp_sec   precise_origin_timestamp_sec\    #
#             -precise_origin_timestamp_ns    precise_origin_timestamp_ns\     #
#             -receive_timestamp_sec          receive_timestamp_sec            #
#             -receive_timestamp_ns           receive_timestamp_ns\            #
#             -requesting_port_number         requesting_port_number\          #
#             -requesting_clock_identity      requesting_clock_identity\       #
#             -request_receipt_timestamp_sec  request_receipt_timestamp_sec\   #
#             -request_receipt_timestamp_ns   request_receipt_timestamp_ns\    #
#             -response_origin_timestamp_sec  response_origin_timestamp_sec\   #
#             -response_origin_timestamp_ns   response_origin_timestamp_ns\    #
#             -target_port_number             target_port_number\              #
#             -target_clock_identity          target_clock_identity\           #
#             -starting_boundary_hops         starting_boundary_hops\          #
#             -boundary_hops                  boundary_hops\                   #
#             -action_field                   action_field\                    #
#             -ip_checksum_expected           ip_checksum_expected\            #
#             -ip_checksum_status             ip_checksum_status\              #
#             -udp_checksum_expected          udp_checksum_expected\           #
#             -udp_checksum_status            udp_checksum_status              #
#                                                                              #
################################################################################

proc pbLib::decode_ptp_ha_pkt {args} {

    array set param $args

    #INPUT PARAMETERS

    #ptp ha packet hex dump
    if {[info exists param(-packet)]} {

        set packet $param(-packet)
    } else {

        ERRLOG -msg "pbLib::decode_ptp_ha_pkt: Missing Argument -> packet"
        return 0
    }

    #OUTPUT PARAMETERS

    #Destination MAC address of eth header
    if {[info exists param(-dest_mac)]} {

        upvar $param(-dest_mac) dest_mac
        set dest_mac ""
    }

    #Source MAC address of eth header
    if {[info exists param(-src_mac)]} {

        upvar $param(-src_mac) src_mac
        set src_mac ""
    }

    #Ethernet type
    if {[info exists param(-eth_type)]} {

        upvar $param(-eth_type) eth_type
        set eth_type ""
    }

    #Vlan id of vlan header
    if {[info exists param(-vlan_id)]} {

        upvar $param(-vlan_id) vlan_id
        set vlan_id ""
    }

    #Priority of vlan header
    if {[info exists param(-vlan_priority)]} {

        upvar $param(-vlan_priority) vlan_priority
        set vlan_priority ""
    }

    #Eligible indicator of vlan header
    if {[info exists param(-vlan_dei)]} {

        upvar $param(-vlan_dei) vlan_dei
        set vlan_dei ""
    }

    #Version of ip
    if {[info exists param(-ip_version)]} {

        upvar $param(-ip_version) ip_version
        set ip_version ""
    }

    #Header length  of ipv4
    if {[info exists param(-ip_header_length)]} {

        upvar $param(-ip_header_length) ip_header_length
        set ip_header_length ""
    }

    #Type of service
    if {[info exists param(-ip_tos)]} {

        upvar $param(-ip_tos) ip_tos
        set ip_tos ""
    }

    #Total length  of ipv4
    if {[info exists param(-ip_total_length)]} {

        upvar $param(-ip_total_length) ip_total_length
        set ip_total_length ""
    }

    #Unique id of ipv4
    if {[info exists param(-ip_identification)]} {

        upvar $param(-ip_identification) ip_identification
        set ip_identification ""
    }

    #Flags represents the control or identify
    if {[info exists param(-ip_flags)]} {

        upvar $param(-ip_flags) ip_flags
        set ip_flags ""
    }

    #Fragment offset
    if {[info exists param(-ip_offset)]} {

        upvar $param(-ip_offset) ip_offset
        set ip_offset ""
    }

    #Time to live
    if {[info exists param(-ip_ttl)]} {

        upvar $param(-ip_ttl) ip_ttl
        set ip_ttl ""
    }

    #IP Checksum
    if {[info exists param(-ip_checksum)]} {

        upvar $param(-ip_checksum) ip_checksum
        set ip_checksum ""
    }

    #Protocol in the data portion of the
    if {[info exists param(-ip_protocol)]} {

        upvar $param(-ip_protocol) ip_protocol
        set ip_protocol ""
    }

    #IPv4 address of the sender of the packet
    if {[info exists param(-src_ip)]} {

        upvar $param(-src_ip) src_ip
        set src_ip ""
    }

    #IPv4 address of the reciever of the packet
    if {[info exists param(-dest_ip)]} {

        upvar $param(-dest_ip) dest_ip
        set dest_ip ""
    }

    #Sender
    if {[info exists param(-src_port)]} {

        upvar $param(-src_port) src_port
        set src_port ""
    }

    #Receiver
    if {[info exists param(-dest_port)]} {

        upvar $param(-dest_port) dest_port
        set dest_port ""
    }

    #Length in bytes of UDP header and data
    if {[info exists param(-udp_length)]} {

        upvar $param(-udp_length) udp_length
        set udp_length ""
    }

    #UDP Checksum
    if {[info exists param(-udp_checksum)]} {

        upvar $param(-udp_checksum) udp_checksum
        set udp_checksum ""
    }

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        upvar $param(-major_sdo_id) major_sdo_id
        set major_sdo_id ""
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        upvar $param(-message_type) message_type
        set message_type ""
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        upvar $param(-minor_version) minor_version
        set minor_version ""
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        upvar $param(-version_ptp) version_ptp
        set version_ptp ""
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        upvar $param(-message_length) message_length
        set message_length ""
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        upvar $param(-domain_number) domain_number
        set domain_number ""
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        upvar $param(-minor_sdo_id) minor_sdo_id
        set minor_sdo_id ""
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        upvar $param(-alternate_master_flag) alternate_master_flag
        set alternate_master_flag ""
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        upvar $param(-two_step_flag) two_step_flag
        set two_step_flag ""
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        upvar $param(-unicast_flag) unicast_flag
        set unicast_flag ""
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        upvar $param(-profile_specific1) profile_specific1
        set profile_specific1 ""
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        upvar $param(-profile_specific2) profile_specific2
        set profile_specific2 ""
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        upvar $param(-secure) secure
        set secure ""
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        upvar $param(-leap61) leap61
        set leap61 ""
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        upvar $param(-leap59) leap59
        set leap59 ""
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        upvar $param(-current_utc_offset_valid) current_utc_offset_valid
        set current_utc_offset_valid ""
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        upvar $param(-ptp_timescale) ptp_timescale
        set ptp_timescale ""
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        upvar $param(-time_traceable) time_traceable
        set time_traceable ""
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        upvar $param(-freq_traceable) freq_traceable
        set freq_traceable ""
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        upvar $param(-sync_uncertain) sync_uncertain
        set sync_uncertain ""
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        upvar $param(-correction_field) correction_field
        set correction_field ""
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        upvar $param(-message_type_specific) message_type_specific
        set message_type_specific ""
    }

    #Source port number
    if {[info exists param(-src_port_number)]} {

        upvar $param(-src_port_number) src_port_number
        set src_port_number ""
    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        upvar $param(-src_clock_identity) src_clock_identity
        set src_clock_identity ""
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        upvar $param(-sequence_id) sequence_id
        set sequence_id ""
    }

    #Control field
    if {[info exists param(-control_field)]} {

        upvar $param(-control_field) control_field
        set control_field ""
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        upvar $param(-log_message_interval) log_message_interval
        set log_message_interval ""
    }

    #Origin timestamp in seconds
    if {[info exists param(-origin_timestamp_sec)]} {

        upvar $param(-origin_timestamp_sec) origin_timestamp_sec
        set origin_timestamp_sec ""
    }

    #Origin timestamp in nanoseconds
    if {[info exists param(-origin_timestamp_ns)]} {

        upvar $param(-origin_timestamp_ns) origin_timestamp_ns
        set origin_timestamp_ns ""
    }

    #Current UTC Offset
    if {[info exists param(-current_utc_offset)]} {

        upvar $param(-current_utc_offset) current_utc_offset
        set current_utc_offset ""
    }

    #Grand Master Priority 1
    if {[info exists param(-gm_priority1)]} {

        upvar $param(-gm_priority1) gm_priority1
        set gm_priority1 ""
    }

    #Grand Master Priority 2
    if {[info exists param(-gm_priority2)]} {

        upvar $param(-gm_priority2) gm_priority2
        set gm_priority2 ""
    }

    #Grand Master Identity
    if {[info exists param(-gm_identity)]} {

        upvar $param(-gm_identity) gm_identity
        set gm_identity ""
    }

    #Steps Removed
    if {[info exists param(-steps_removed)]} {

        upvar $param(-steps_removed) steps_removed
        set steps_removed ""
    }

    #Time Source
    if {[info exists param(-time_source)]} {

        upvar $param(-time_source) time_source
        set time_source ""
    }

    #Grand Master Clock classy
    if {[info exists param(-gm_clock_classy)]} {

        upvar $param(-gm_clock_classy) gm_clock_classy
        set gm_clock_classy ""
    }

    #Grand Master Clock accuracy
    if {[info exists param(-gm_clock_accuracy)]} {

        upvar $param(-gm_clock_accuracy) gm_clock_accuracy
        set gm_clock_accuracy ""
    }

    #Grand Master Clock offset scaled
    if {[info exists param(-gm_clock_variance)]} {

        upvar $param(-gm_clock_variance) gm_clock_variance
        set gm_clock_variance ""
    }

    #Precise Origin Timestamp
    if {[info exists param(-precise_origin_timestamp_sec)]} {

        upvar $param(-precise_origin_timestamp_sec) precise_origin_timestamp_sec
        set precise_origin_timestamp_sec ""
    }

    #Precise Origin Timestamp
    if {[info exists param(-precise_origin_timestamp_ns)]} {

        upvar $param(-precise_origin_timestamp_ns) precise_origin_timestamp_ns
        set precise_origin_timestamp_ns ""
    }

    #Receive Timestamp
    if {[info exists param(-receive_timestamp_sec)]} {

        upvar $param(-receive_timestamp_sec) receive_timestamp_sec
        set receive_timestamp_sec ""
    }

    #Receive Timestamp
    if {[info exists param(-receive_timestamp_ns)]} {

        upvar $param(-receive_timestamp_ns) receive_timestamp_ns
        set receive_timestamp_ns ""
    }

    #Requesting Port Number
    if {[info exists param(-requesting_port_number)]} {

        upvar $param(-requesting_port_number) requesting_port_number
        set requesting_port_number ""
    }

    #Requesting Clock Identity
    if {[info exists param(-requesting_clock_identity)]} {

        upvar $param(-requesting_clock_identity) requesting_clock_identity
        set requesting_clock_identity ""
    }

    #Request Receipt Timestamp
    if {[info exists param(-request_receipt_timestamp_sec)]} {

        upvar $param(-request_receipt_timestamp_sec) request_receipt_timestamp_sec
        set request_receipt_timestamp_sec ""
    }

    #Request Receipt Timestamp
    if {[info exists param(-request_receipt_timestamp_ns)]} {

        upvar $param(-request_receipt_timestamp_ns) request_receipt_timestamp_ns
        set request_receipt_timestamp_ns ""
    }

    #Response Origin Timestamp
    if {[info exists param(-response_origin_timestamp_sec)]} {

        upvar $param(-response_origin_timestamp_sec) response_origin_timestamp_sec
        set response_origin_timestamp_sec ""
    }

    #Response Origin Timestamp
    if {[info exists param(-response_origin_timestamp_ns)]} {

        upvar $param(-response_origin_timestamp_ns) response_origin_timestamp_ns
        set response_origin_timestamp_ns ""
    }

    #Target Port Number
    if {[info exists param(-target_port_number)]} {

        upvar $param(-target_port_number) target_port_number
        set target_port_number ""
    }

    #Target Clock Identity
    if {[info exists param(-target_clock_identity)]} {

        upvar $param(-target_clock_identity) target_clock_identity
        set target_clock_identity ""
    }

    #Starting Boundary Hops
    if {[info exists param(-starting_boundary_hops)]} {

        upvar $param(-starting_boundary_hops) starting_boundary_hops
        set starting_boundary_hops ""
    }

    #Boundary Hops
    if {[info exists param(-boundary_hops)]} {

        upvar $param(-boundary_hops) boundary_hops
        set boundary_hops ""
    }

    #Action Field
    if {[info exists param(-action_field)]} {

        upvar $param(-action_field) action_field
        set action_field ""
    }

    #TLV Name
    if {[info exists param(-tlv)]} {

        upvar $param(-tlv) tlv
        set tlv "NONE"
    }

    #TLV Type
    if {[info exists param(-tlv_type)]} {

        upvar $param(-tlv_type) tlv_type
        set tlv_type ""
    }

    #TLV length
    if {[info exists param(-tlv_length)]} {

        upvar $param(-tlv_length) tlv_length
        set tlv_length ""
    }

    #OPE Flag
    if {[info exists param(-ope)]} {

        upvar $param(-ope) ope
        set ope ""
    }

    #CR Flag
    if {[info exists param(-cr)]} {

        upvar $param(-cr) cr
        set cr ""
    }

    #RCR Flag
    if {[info exists param(-rcr)]} {

        upvar $param(-rcr) rcr
        set rcr ""
    }

    #TCR Flag
    if {[info exists param(-tcr)]} {

        upvar $param(-tcr) tcr
        set tcr ""
    }

    #IC Flag
    if {[info exists param(-ic)]} {

        upvar $param(-ic) ic
        set ic ""
    }

    #IRC Flag
    if {[info exists param(-irc)]} {

        upvar $param(-irc) irc
        set irc ""
    }

    #ITC Flag
    if {[info exists param(-itc)]} {

        upvar $param(-itc) itc
        set itc ""
    }

    #FOV Flag
    if {[info exists param(-fov)]} {

        upvar $param(-fov) fov
        set fov ""
    }

    #POV Flag
    if {[info exists param(-pov)]} {

        upvar $param(-pov) pov
        set pov ""
    }

    #TCT Flag
    if {[info exists param(-tct)]} {

        upvar $param(-tct) tct
        set tct ""
    }

    #Phase offset tx
    if {[info exists param(-phase_offset_tx)]} {

        upvar $param(-phase_offset_tx) phase_offset_tx
        set phase_offset_tx ""
    }

    #Phase offset tx timestamp Sec
    if {[info exists param(-phase_offset_tx_timestamp_sec)]} {

        upvar $param(-phase_offset_tx_timestamp_sec) phase_offset_tx_timestamp_sec
        set phase_offset_tx_timestamp_sec ""
    }

    #Phase offset tx timsstamp Ns
    if {[info exists param(-phase_offset_tx_timestamp_ns)]} {

        upvar $param(-phase_offset_tx_timestamp_ns) phase_offset_tx_timestamp_ns
        set phase_offset_tx_timestamp_ns ""
    }

    #Frequency offset tx
    if {[info exists param(-freq_offset_tx)]} {

        upvar $param(-freq_offset_tx) freq_offset_tx
        set freq_offset_tx ""
    }

    #Frequency offset tx timestamp
    if {[info exists param(-freq_offset_tx_timestamp_sec)]} {

        upvar $param(-freq_offset_tx_timestamp_sec) freq_offset_tx_timestamp_sec
        set freq_offset_tx_timestamp_sec ""
    }

    #Frequency offset tx timstamp
    if {[info exists param(-freq_offset_tx_timestamp_ns)]} {

        upvar $param(-freq_offset_tx_timestamp_ns) freq_offset_tx_timestamp_ns
        set freq_offset_tx_timestamp_ns ""
    }

    #Management ID
    if {[info exists param(-management_id)]} {

        upvar $param(-management_id) management_id
        set management_id ""
    }

    #External Port Configuration
    if {[info exists param(-ext_port_config)]} {

        upvar $param(-ext_port_config) ext_port_config
        set ext_port_config ""
    }

    #Master only
    if {[info exists param(-master_only)]} {

        upvar $param(-master_only) master_only
        set master_only ""
    }

    #IP Checksum status
    if {[info exists param(-ip_checksum_status)]} {

        upvar $param(-ip_checksum_status) ip_checksum_status
        set ip_checksum_status ""
    }

    #Expected IP Checksum
    if {[info exists param(-ip_checksum_expected)]} {

        upvar $param(-ip_checksum_expected) ip_checksum_expected
        set ip_checksum_expected ""
    }

    #IP Checksum status
    if {[info exists param(-udp_checksum_status)]} {

        upvar $param(-udp_checksum_status) udp_checksum_status
        set udp_checksum_status ""
    }

    #Expected UDP Checksum
    if {[info exists param(-udp_checksum_expected)]} {

        upvar $param(-udp_checksum_expected) udp_checksum_expected
        set udp_checksum_expected ""
    }

    #LOGIC GOES HERE
    set hex [string tolower $packet]
    set decode_ptp 0

    #DECODE ETH HEADER
    pbLib::decode_eth_header\
                -hex            $hex\
                -dest_mac       dest_mac\
                -src_mac        src_mac\
                -next_protocol  eth_type

    set hex [string range $hex 28 end]

    while 1 {
        switch $eth_type {

            "8100" {
                #DECODE VLAN HEADER
                pbLib::decode_vlan_header\
                             -hex           $hex\
                             -priority      vlan_priority\
                             -dei           vlan_dei\
                             -vlan_id       vlan_id\
                             -next_protocol eth_type

                set hex [string range $hex 8 end]
                continue
            }

            "0800" {

                #DECODE IPv4 HEADER
                pbLib::decode_ipv4_header\
                             -hex                     $hex\
                             -version                 ip_version\
                             -header_length           ip_header_length\
                             -tos                     ip_tos\
                             -total_length            ip_total_length\
                             -identification          ip_identification\
                             -flags                   ip_flags\
                             -offset                  ip_offset\
                             -ttl                     ip_ttl\
                             -next_protocol           ip_protocol\
                             -checksum                ip_checksum\
                             -src_ip                  src_ip\
                             -dest_ip                 dest_ip\
                             -checksum_status         ip_checksum_status\
                             -checksum_expected       ip_checksum_expected

                set hex [string range $hex [expr $ip_header_length*2] end]

                if {$ip_protocol == 17} {
                    
                    set ip_pseudo_header ""
                    append ip_pseudo_header [dec2hex $ip_protocol 4]
                    append ip_pseudo_header [ip2hex $src_ip]
                    append ip_pseudo_header [ip2hex $dest_ip]

                    set data [string range $hex 16 end]

                    #DECODE UDP HEADER
                    pbLib::decode_udp_header\
                                      -hex                 $hex\
                                      -src_port            src_port\
                                      -dest_port           dest_port\
                                      -payload_length      udp_length\
                                      -checksum            udp_checksum\
                                      -pseudo_header       $ip_pseudo_header\
                                      -data                $data\
                                      -checksum_status     udp_checksum_status\
                                      -checksum_expected   udp_checksum_expected

                    set hex $data
                    set decode_ptp 1
                }

                break
            }

            "88f7" {

                set decode_ptp 1
                break

            }

            default {
                break
            }
        }
    }

    #DECODE AS PTP HA
    if {$decode_ptp == 1} {
        #DECODE PTP COMMON HEADER
        pbLib::decode_ptp_ha_common_header\
                    -hex                               $hex\
                    -major_sdo_id                      major_sdo_id\
                    -message_type                      message_type\
                    -minor_version                     minor_version\
                    -version_ptp                       version_ptp\
                    -message_length                    message_length\
                    -domain_number                     domain_number\
                    -minor_sdo_id                      minor_sdo_id\
                    -correction_field                  correction_field\
                    -message_type_specific             message_type_specific\
                    -sequence_id                       sequence_id\
                    -control_field                     control_field\
                    -log_message_interval              log_message_interval\
                    -src_clock_identity                src_clock_identity\
                    -src_port_number                   src_port_number\
                    -alternate_master_flag             alternate_master_flag\
                    -two_step_flag                     two_step_flag\
                    -unicast_flag                      unicast_flag\
                    -profile_specific1                 profile_specific1\
                    -profile_specific2                 profile_specific2\
                    -secure                            secure\
                    -leap61                            leap61\
                    -leap59                            leap59\
                    -current_utc_offset_valid          current_utc_offset_valid\
                    -ptp_timescale                     ptp_timescale\
                    -time_traceable                    time_traceable\
                    -freq_traceable                    freq_traceable\
                    -sync_uncertain                    sync_uncertain

        set hex [string range $hex 68 end]

        switch $message_type {
            0 {
                #Sync
                pbLib::decode_ptp_ha_sync_header\
                                             -hex                        $hex\
                                             -origin_timestamp_sec       origin_timestamp_sec\
                                             -origin_timestamp_ns        origin_timestamp_ns
            }
            1 {
                #Delay Request
                pbLib::decode_ptp_ha_delay_req_header\
                                             -hex                        $hex\
                                             -origin_timestamp_sec       origin_timestamp_sec\
                                             -origin_timestamp_ns        origin_timestamp_ns
            }
            2 {
                #Peer Delay Request
                pbLib::decode_ptp_ha_pdelay_req_header\
                                             -hex                        $hex\
                                             -origin_timestamp_sec       origin_timestamp_sec\
                                             -origin_timestamp_ns        origin_timestamp_ns
            }
            3 {
                #Peer Delay Response
                pbLib::decode_ptp_ha_pdelay_resp_header\
                                             -hex                            $hex\
                                             -request_receipt_timestamp_sec  request_receipt_timestamp_sec\
                                             -request_receipt_timestamp_ns   request_receipt_timestamp_ns\
                                             -requesting_port_number         requesting_port_number\
                                             -requesting_clock_identity      requesting_clock_identity
            }
            8 {
                #Follow up
                pbLib::decode_ptp_ha_followup_header\
                                             -hex                           $hex\
                                             -precise_origin_timestamp_sec  precise_origin_timestamp_sec\
                                             -precise_origin_timestamp_ns   precise_origin_timestamp_ns\
                                             -hexdump                       header
            }
            9 {
                #Delay Response
                pbLib::decode_ptp_ha_delay_resp_header\
                                             -hex                          $hex\
                                             -receive_timestamp_sec        receive_timestamp_sec\
                                             -receive_timestamp_ns         receive_timestamp_ns\
                                             -requesting_port_number       requesting_port_number\
                                             -requesting_clock_identity    requesting_clock_identity
            }
           10 {
                #Peer Delay Response Follow up
                pbLib::decode_ptp_ha_pdelay_resp_followup_header\
                                             -hex                           $hex\
                                             -response_origin_timestamp_sec response_origin_timestamp_sec\
                                             -response_origin_timestamp_ns  response_origin_timestamp_ns\
                                             -requesting_port_number        requesting_port_number\
                                             -requesting_clock_identity     requesting_clock_identity
            }
           11 {
                #Announce
                pbLib::decode_ptp_ha_announce_header\
                                             -hex                        $hex\
                                             -origin_timestamp_sec       origin_timestamp_sec\
                                             -origin_timestamp_ns        origin_timestamp_ns\
                                             -current_utc_offset_valid   current_utc_offset_valid\
                                             -gm_priority1               gm_priority1\
                                             -gm_priority2               gm_priority2\
                                             -gm_identity                gm_identity\
                                             -steps_removed              steps_removed\
                                             -time_source                time_source\
                                             -gm_clock_classy            gm_clock_classy\
                                             -gm_clock_accuracy          gm_clock_accuracy\
                                             -gm_clock_variance          gm_clock_variance
            }
           12 {
                #Signalling
                pbLib::decode_ptp_ha_signal_header\
                                             -hex                               $hex\
                                             -target_port_number                target_port_number\
                                             -target_clock_identity             target_clock_identity\
                                             -tlv                               tlv\
                                             -tlv_type                          tlv_type\
                                             -tlv_length                        tlv_length\
                                             -ope                               ope\
                                             -cr                                cr\
                                             -rcr                               rcr\
                                             -tcr                               tcr\
                                             -ic                                ic\
                                             -irc                               irc\
                                             -itc                               itc\
                                             -fov                               fov\
                                             -pov                               pov\
                                             -tct                               tct\
                                             -phase_offset_tx                   phase_offset_tx\
                                             -phase_offset_tx_timestamp_sec     phase_offset_tx_timestamp_sec\
                                             -phase_offset_tx_timestamp_ns      phase_offset_tx_timestamp_ns\
                                             -freq_offset_tx                    freq_offset_tx\
                                             -freq_offset_tx_timestamp_sec      freq_offset_tx_timestamp_sec\
                                             -freq_offset_tx_timestamp_ns       freq_offset_tx_timestamp_ns
            }
           13 {
                #Management
                pbLib::decode_ptp_ha_mgmt_header\
                                          -hex                        $hex\
                                          -target_port_number         target_port_number\
                                          -target_clock_identity      target_clock_identity\
                                          -starting_boundary_hops     starting_boundary_hops\
                                          -boundary_hops              boundary_hops\
                                          -action_field               action_field\
                                          -management_id              management_id\
                                          -ext_port_config            ext_port_config\
                                          -master_only                master_only
            }
           default {
                #RESERVED
            }
        }

    } else {
        return 0
    }

    return 1

}

################################################################################
#  PROCEDURE NAME        : pbLib::decode_ptp_ha_common_header                  #
#                                                                              #
#  DEFINITION            : This function decodes ptp ha header.                #
#                                                                              #
#  INPUT PARAMETERS      : NONE                                                #
#                                                                              #
#  hex                   :  (Mandatory)  Hex dump of PTP HA Commom Header      #
#                                                                              #
#  OUTPUT PARAMETERS     :                                                     #
#                                                                              #
#  major_sdo_id          :  (Optional)  Major sdo id                           #
#                                                                              #
#  message_type          :  (Optional)  Message Type                           #
#                                                                              #
#  minor_version         :  (Optional)  PTP version number                     #
#                                                                              #
#  version_ptp           :  (Optional)  PTP minor version number               #
#                                                                              #
#  message_length        :  (Optional)  PTP HA message length                  #
#                                                                              #
#  domain_number         :  (Optional)  Domain number                          #
#                                                                              #
#  minor_sdo_id          :  (Optional)  Minor Sdo id                           #
#                                                                              #
#  alternate_master_flag :  (Optional)  Alternate Master flag                  #
#                                                                              #
#  two_step_flag         :  (Optional)  Two Step Flag                          #
#                                                                              #
#  unicast_flag          :  (Optional)  Unicast Flag                           #
#                                                                              #
#  profile_specific1     :  (Optional)  Profile specific1 Flag                 #
#                                                                              #
#  profile_specific2     :  (Optional)  Profile specific2 Flag                 #
#                                                                              #
#  secure                :  (Optional)  Secure Flag                            #
#                                                                              #
#  leap61                :  (Optional)  Leap61 Flag                            #
#                                                                              #
#  leap59                :  (Optional)  Leap59 Flag                            #
#                                                                              #
#  current_utc_offset_valid:(Optional)  Current UTC Offset Flag                #
#                                                                              #
#  ptp_timescale         :  (Optional)  PTP TimeSclae Flag                     #
#                                                                              #
#  time_traceable        :  (Optional)  Time Traceable Flag                    #
#                                                                              #
#  freq_traceable        :  (Optional)  Frequency Traceable Flag               #
#                                                                              #
#  sync_uncertain        :  (Optional)  Synchronization uncertain Flag         #
#                                                                              #
#  correction_field      :  (Optional)  Correction Field value                 #
#                                                                              #
#  message_type_specific :  (Optional)  Message type                           #
#                                                                              #
#  src_port_number       :  (Optional)  Source port number                     #
#                                                                              #
#  src_clock_identity    :  (Optional)  Source clock identity                  #
#                                                                              #
#  sequence_id           :  (Optional)  Sequence ID                            #
#                                                                              #
#  control_field         :  (Optional)  Control field                          #
#                                                                              #
#  log_message_interval  :  (Optional)  Log message interval                   #
#                                                                              #
#                                                                              #
#  RETURNS              : PTP HA common header hex dump                        #
#                                                                              #
#  USAGE                :                                                      #
#   pbLib::decode_ptp_ha_common_header\                                        #
#              -hexdump                           $hexdump\                    #
#              -major_sdo_id                      major_sdo_id\                #
#              -message_type                      message_type\                #
#              -minor_version                     minor_version\               #
#              -version_ptp                       version_ptp\                 #
#              -message_length                    message_length\              #
#              -domain_number                     domain_number\               #
#              -minor_sdo_id                      minor_sdo_id\                #
#              -alternate_master_flag             alternate_master_flag\       #
#              -two_step_flag                     two_step_flag\               #
#              -unicast_flag                      unicast_flag\                #
#              -profile_specific1                 profile_specific1\           #
#              -profile_specific2                 profile_specific2\           #
#              -secure                            secure\                      #
#              -leap61                            leap61\                      #
#              -leap59                            leap59\                      #
#              -current_utc_offset_valid          current_utc_offset_valid\    #
#              -ptp_timescale                     ptp_timescale\               #
#              -time_traceable                    time_traceable\              #
#              -freq_traceable                    freq_traceable\              #
#              -sync_uncertain                    sync_uncertain\              #
#              -correction_field                  correction_field\            #
#              -message_type_specific             message_type_specific\       #
#              -src_port_number                   src_port_number\             #
#              -src_clock_identity                src_clock_identity           #
#              -sequence_id                       sequence_id\                 #
#              -control_field                     control_field\               #
#              -log_message_interval              log_message_interval         #
#                                                                              #
################################################################################

proc pbLib::decode_ptp_ha_common_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #Hex dump of PTP HA Commom Header
    if {[info exists param(-hex)]} {

        set hex $param(-hex)
    } else {
        ERRLOG -msg "pbLib::decode_ptp_ha_common_header: Missing Argument -> hex"
        return 0
    }

    #OUTPUT PARAMETERS

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        upvar $param(-major_sdo_id) major_sdo_id
        set major_sdo_id ""
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        upvar $param(-message_type) message_type
        set message_type ""
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        upvar $param(-minor_version) minor_version
        set minor_version ""
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        upvar $param(-version_ptp) version_ptp
        set version_ptp ""
    }

    #PTP HA message length
    if {[info exists param(-message_length)]} {

        upvar $param(-message_length) message_length
        set message_length ""
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        upvar $param(-domain_number) domain_number
        set domain_number ""
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        upvar $param(-minor_sdo_id) minor_sdo_id
        set minor_sdo_id ""
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        upvar $param(-alternate_master_flag) alternate_master_flag
        set alternate_master_flag ""
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        upvar $param(-two_step_flag) two_step_flag
        set two_step_flag ""
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        upvar $param(-unicast_flag) unicast_flag
        set unicast_flag ""
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        upvar $param(-profile_specific1) profile_specific1
        set profile_specific1 ""
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        upvar $param(-profile_specific2) profile_specific2
        set profile_specific2 ""
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        upvar $param(-secure) secure
        set secure ""
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        upvar $param(-leap61) leap61
        set leap61 ""
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        upvar $param(-leap59) leap59
        set leap59 ""
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        upvar $param(-current_utc_offset_valid) current_utc_offset_valid
        set current_utc_offset_valid ""
    }

    #PTP TimeSclae Flag
    if {[info exists param(-ptp_timescale)]} {

        upvar $param(-ptp_timescale) ptp_timescale
        set ptp_timescale ""
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        upvar $param(-time_traceable) time_traceable
        set time_traceable ""
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        upvar $param(-freq_traceable) freq_traceable
        set freq_traceable ""
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        upvar $param(-sync_uncertain) sync_uncertain
        set sync_uncertain ""
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        upvar $param(-correction_field) correction_field
        set correction_field ""
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        upvar $param(-message_type_specific) message_type_specific
        set message_type_specific ""
    }

    #Source port number
    if {[info exists param(-src_port_number)]} {

        upvar $param(-src_port_number) src_port_number
        set src_port_number ""
    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        upvar $param(-src_clock_identity) src_clock_identity
        set src_clock_identity ""
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        upvar $param(-sequence_id) sequence_id
        set sequence_id ""
    }

    #Control field
    if {[info exists param(-control_field)]} {

        upvar $param(-control_field) control_field
        set control_field ""
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        upvar $param(-log_message_interval) log_message_interval
        set log_message_interval ""
    }

    #LOGIC GOES HERE
    ptp_ha_common hdr = $hex

    set major_sdo_id                     [hex2dec $hdr::majorSdoId]
    set message_type                     [hex2dec $hdr::messageType]
    set minor_version                    [hex2dec $hdr::minorVersionPTP]
    set version_ptp                      [hex2dec $hdr::versionPTP]
    set message_length                   [hex2dec $hdr::messageLength]
    set domain_number                    [hex2dec $hdr::domainNumber]
    set minor_sdo_id                     [hex2dec $hdr::minorSdoId]

    ptp_ha_flags flag = $hdr::flagField

    set alternate_master_flag            $flag::alternateMasterFlag
    set two_step_flag                    $flag::twoStepFlag
    set unicast_flag                     $flag::unicastFlag
    set profile_specific1                $flag::profileSpecific1
    set profile_specific2                $flag::profileSpecific2
    set secure                           $flag::secure
    set leap61                           $flag::leap61
    set leap59                           $flag::leap59
    set current_utc_offset_valid         $flag::currentUtcOffsetValid
    set ptp_timescale                    $flag::ptpTimescale
    set time_traceable                   $flag::timeTraceable
    set freq_traceable                   $flag::frequencyTraceable
    set sync_uncertain                   $flag::synchronizationUncertain
    set correction_field                 [format "%.3f" [expr [format %i 0x$hdr::correctionField] / pow(2,16)]]
    set message_type_specific            [hex2dec $hdr::messageTypeSpecific]

    ptp_ha_port_identity port_identity = $hdr::sourcePortIdentity

    set src_port_number                  [hex2dec $port_identity::portNumber]
    set src_clock_identity               [hex2dec $port_identity::clockIdentity]

    set sequence_id                      [hex2dec $hdr::sequenceId]
    set control_field                    [hex2dec $hdr::controlField]
    set log_message_interval             [hex2dec $hdr::logMessageInterval]

    return 1

}

################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::decode_ptp_ha_sync_header                     #
#                                                                              #
#  DEFINITION           : This function decodes ptp ha sync header             #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  hex                  : (Mandatory) hex dump                                 #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  origin_timestamp_sec :  (Optional)  Origin timestamp in seconds             #
#                                                                              #
#  origin_timestamp_ns  :  (Optional)  Origin timestamp in nanoseconds         #
#                                                                              #
#  RETURNS              : 0 on failure (If the hexdump is not PTP HA SYNC)     #
#                                                                              #
#                         1 on success (If the hexdump is PTP HA SYNC)         #
#                                                                              #
#  USAGE                :                                                      #
#    pbLib::decode_ptp_ha_sync_header\                                         #
#               -hex                               $hex\                       #
#               -origin_timestamp_sec              origin_timestamp_sec\       #
#               -origin_timestamp_ns               origin_timestamp_ns         #
#                                                                              #
################################################################################

proc pbLib::decode_ptp_ha_sync_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #hex dump
    if {[info exists param(-hex)]} {

        set hex $param(-hex)
    } else {

        ERRLOG -msg "pbLib::decode_ptp_ha_sync_header: Missing Argument -> hex"
        return 0
    }

    #OUTPUT PARAMETERS

    #Origin timestamp in seconds
    if {[info exists param(-origin_timestamp_sec)]} {

        upvar $param(-origin_timestamp_sec) origin_timestamp_sec
        set origin_timestamp_sec ""
    }

    #Origin timestamp in nanoseconds
    if {[info exists param(-origin_timestamp_ns)]} {

        upvar $param(-origin_timestamp_ns) origin_timestamp_ns
        set origin_timestamp_ns ""
    }


    #LOGIC GOES HERE
    ptp_ha_sync hdr = $hex

    ptp_ha_timestamp timestamp =  $hdr::originTimestamp

    set origin_timestamp_sec [hex2dec $timestamp::secondsField]
    set origin_timestamp_ns  [hex2dec $timestamp::nanosecondsField]

    return 1

}


################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::decode_ptp_ha_announce_header                 #
#                                                                              #
#  DEFINITION           : This function decodes ptp ha announce header         #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  hex                  : (Mandatory) hex dump                                 #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  origin_timestamp_sec :  (Optional)  Origin timestamp in seconds             #
#                                                                              #
#  origin_timestamp_ns  :  (Optional)  Origin timestamp in nanoseconds         #
#                                                                              #
#  current_utc_offset   :  (Optional)  Current UTC Offset                      #
#                                                                              #
#  gm_priority1         :  (Optional)  Grand Master Priority 1                 #
#                                                                              #
#  gm_priority2         :  (Optional)  Grand Master Priority 2                 #
#                                                                              #
#  gm_identity          :  (Optional)  Grand Master Identity                   #
#                                                                              #
#  steps_removed        :  (Optional)  Steps Removed                           #
#                                                                              #
#  time_source          :  (Optional)  Time Source                             #
#                                                                              #
#  gm_clock_classy      :  (Optional)  Grand Master Clock classy               #
#                                                                              #
#  gm_clock_accuracy    :  (Optional)  Grand Master Clock accuracy             #
#                                                                              #
#  gm_clock_variance    :  (Optional)  Grand Master Clock offset scaled        #
#                                                                              #
#  RETURNS              : 0 on failure (If the hexdump is not PTP HA ANNOUNCE) #
#                                                                              #
#                         1 on success (If the hexdump is PTP HA ANNOUNCE)     #
#                                                                              #
#  #USAGE                                                                      #
#  pbLib::decode_ptp_ha_announce_header\                                       #
#             -hex                               $hex\                         #
#             -origin_timestamp_sec              origin_timestamp_sec\         #
#             -origin_timestamp_ns               origin_timestamp_ns\          #
#             -current_utc_offset                current_utc_offset\           #
#             -gm_priority1                      gm_priority1\                 #
#             -gm_priority2                      gm_priority2\                 #
#             -gm_identity                       gm_identity\                  #
#             -steps_removed                     steps_removed\                #
#             -time_source                       time_source\                  #
#             -gm_clock_classy                   gm_clock_classy\              #
#             -gm_clock_accuracy                 gm_clock_accuracy\            #
#             -gm_clock_variance                 gm_clock_variance             #
#                                                                              #
################################################################################

proc pbLib::decode_ptp_ha_announce_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #hex dump
    if {[info exists param(-hex)]} {

        set hex $param(-hex)
    } else {

        ERRLOG -msg "pbLib::decode_ptp_ha_announce_header: Missing Argument -> hex"
        return 0
    }

    #OUTPUT PARAMETERS

    #Origin timestamp in seconds
    if {[info exists param(-origin_timestamp_sec)]} {

        upvar $param(-origin_timestamp_sec) origin_timestamp_sec
        set origin_timestamp_sec ""
    }

    #Origin timestamp in nanoseconds
    if {[info exists param(-origin_timestamp_ns)]} {

        upvar $param(-origin_timestamp_ns) origin_timestamp_ns
        set origin_timestamp_ns ""
    }

    #Current UTC Offset
    if {[info exists param(-current_utc_offset)]} {

        upvar $param(-current_utc_offset) current_utc_offset
        set current_utc_offset ""
    }

    #Grand Master Priority 1
    if {[info exists param(-gm_priority1)]} {

        upvar $param(-gm_priority1) gm_priority1
        set gm_priority1 ""
    }

    #Grand Master Priority 2
    if {[info exists param(-gm_priority2)]} {

        upvar $param(-gm_priority2) gm_priority2
        set gm_priority2 ""
    }

    #Grand Master Identity
    if {[info exists param(-gm_identity)]} {

        upvar $param(-gm_identity) gm_identity
        set gm_identity ""
    }

    #Steps Removed
    if {[info exists param(-steps_removed)]} {

        upvar $param(-steps_removed) steps_removed
        set steps_removed ""
    }

    #Time Source
    if {[info exists param(-time_source)]} {

        upvar $param(-time_source) time_source
        set time_source ""
    }

    #Grand Master Clock classy
    if {[info exists param(-gm_clock_classy)]} {

        upvar $param(-gm_clock_classy) gm_clock_classy
        set gm_clock_classy ""
    }

    #Grand Master Clock accuracy
    if {[info exists param(-gm_clock_accuracy)]} {

        upvar $param(-gm_clock_accuracy) gm_clock_accuracy
        set gm_clock_accuracy ""
    }

    #Grand Master Clock offset scaled
    if {[info exists param(-gm_clock_variance)]} {

        upvar $param(-gm_clock_variance) gm_clock_variance
        set gm_clock_variance ""
    }


    #LOGIC GOES HERE

    ptp_ha_announce hdr = $hex

    ptp_ha_timestamp timestamp =  $hdr::originTimestamp

    set origin_timestamp_sec [hex2dec $timestamp::secondsField]
    set origin_timestamp_ns  [hex2dec $timestamp::nanosecondsField]

    set current_utc_offset_valid   [hex2dec $hdr::currentUtcOffset]
    set gm_priority1         [hex2dec $hdr::grandmasterPriority1]
    set gm_priority2         [hex2dec $hdr::grandmasterPriority2]
    set gm_identity          [hex2dec $hdr::grandmasterIdentity]
    set steps_removed        [hex2dec $hdr::stepsRemoved]
    set time_source          [hex2dec $hdr::timeSource]

    ptp_ha_clock_quality clock_quality = $hdr::grandmasterClockQuality

    set gm_clock_classy      [hex2dec $clock_quality::clockClass]
    set gm_clock_accuracy    [hex2dec $clock_quality::clockAccuracy]
    set gm_clock_variance    [hex2dec $clock_quality::offsetScaledLogVariance]


    return 1

}


################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::decode_ptp_ha_delay_req_header                #
#                                                                              #
#  DEFINITION           : This function decodes ptp ha delay request header    #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  hex                  : (Mandatory) hex dump                                 #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  origin_timestamp_sec :  (Optional)  Origin timestamp in seconds             #
#                                                                              #
#  origin_timestamp_ns  :  (Optional)  Origin timestamp in nanoseconds         #
#                                                                              #
#  RETURNS              : 0 on failure (If the hexdump is not PTP HA DELAY     #
#                                       REQUEST)                               #
#                                                                              #
#                         1 on success (If the hexdump is PTP HA DELAY REQUEST)#
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#   pbLib::decode_ptp_ha_delay_req_header\                                     #
#              -hex                               $hex\                        #
#              -origin_timestamp_sec              origin_timestamp_sec\        #
#              -origin_timestamp_ns               origin_timestamp_ns          #
#                                                                              #
################################################################################

proc pbLib::decode_ptp_ha_delay_req_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #hex dump
    if {[info exists param(-hex)]} {

        set hex $param(-hex)
    } else {

        ERRLOG -msg "pbLib::decode_ptp_ha_delay_req_header: Missing Argument -> hex"
        return 0
    }

    #OUTPUT PARAMETERS

    #Origin timestamp in seconds
    if {[info exists param(-origin_timestamp_sec)]} {

        upvar $param(-origin_timestamp_sec) origin_timestamp_sec
        set origin_timestamp_sec ""
    }

    #Origin timestamp in nanoseconds
    if {[info exists param(-origin_timestamp_ns)]} {

        upvar $param(-origin_timestamp_ns) origin_timestamp_ns
        set origin_timestamp_ns ""
    }

    #LOGIC GOES HERE
    ptp_ha_delay_req hdr = $hex

    ptp_ha_timestamp timestamp =  $hdr::originTimestamp

    set origin_timestamp_sec [hex2dec $timestamp::secondsField]
    set origin_timestamp_ns  [hex2dec $timestamp::nanosecondsField]

    return 1

}


####################################################################################
#                                                                                  #
#  PROCEDURE NAME           : pbLib::decode_ptp_ha_delay_resp_header               #
#                                                                                  #
#  DEFINITION               : This function decodes ptp ha delay response header   #
#                                                                                  #
#  INPUT PARAMETERS         :                                                      #
#                                                                                  #
#  hex                      : (Mandatory) hex dump                                 #
#                                                                                  #
#  OUTPUT PARAMETERS        :                                                      #
#                                                                                  #
#  receive_timestamp_sec    : (Optional)  Receive Timestamp                        #
#                                                                                  #
#  receive_timestamp_ns     : (Optional)  Receive Timestamp                        #
#                                                                                  #
#  requesting_port_number   : (Optional)  Requesting Port Number                   #
#                                                                                  #
#  requesting_clock_identity: (Optional)  Requesting Clock Identity                #
#                                                                                  #
#  RETURNS                  : 0 on failure (If the hexdump is not PTP HA DELAY     #
#                             REQUEST)                                             #
#                                                                                  #
#                             1 on success (If the hexdump is PTP HA DELAY         #
#                             REQUEST)                                             #
#                                                                                  #
#  USAGE                    :                                                      #
#                                                                                  #
#   pbLib::decode_ptp_ha_delay_resp_header\                                        #
#              -hex                               $hex\                            #
#              -receive_timestamp_sec             receive_timestamp_sec\           #
#              -receive_timestamp_ns              receive_timestamp_ns\            #
#              -requesting_port_number            requesting_port_number\          #
#              -requesting_clock_identity         requesting_clock_identity        #
#                                                                                  #
####################################################################################

proc pbLib::decode_ptp_ha_delay_resp_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #hex dump
    if {[info exists param(-hex)]} {

        set hex $param(-hex)
    } else {

        ERRLOG -msg "pbLib::decode_ptp_ha_delay_resp_header: Missing Argument -> hex"
        return 0    }

    #OUTPUT PARAMETERS

    #Receive Timestamp
    if {[info exists param(-receive_timestamp_sec)]} {

        upvar $param(-receive_timestamp_sec) receive_timestamp_sec
        set receive_timestamp_sec ""
    }

    #Receive Timestamp
    if {[info exists param(-receive_timestamp_ns)]} {

        upvar $param(-receive_timestamp_ns) receive_timestamp_ns
        set receive_timestamp_ns ""
    }

    #Requesting Port Number
    if {[info exists param(-requesting_port_number)]} {

        upvar $param(-requesting_port_number) requesting_port_number
        set requesting_port_number ""
    }

    #Requesting Port Identity
    if {[info exists param(-requesting_clock_identity)]} {

        upvar $param(-requesting_clock_identity) requesting_clock_identity
        set requesting_clock_identity ""
    }

    #LOGIC GOES HERE
    ptp_ha_delay_resp hdr = $hex

    ptp_ha_timestamp timestamp =  $hdr::receiveTimestamp

    set receive_timestamp_sec [hex2dec $timestamp::secondsField]
    set receive_timestamp_ns  [hex2dec $timestamp::nanosecondsField]

    ptp_ha_port_identity port_identity = $hdr::requestingPortIdentity

    set requesting_port_number     [hex2dec $port_identity::portNumber]
    set requesting_clock_identity  [hex2dec $port_identity::clockIdentity]


    return 1

}


#########################################################################################
#                                                                                       #
#  PROCEDURE NAME                : pbLib::decode_ptp_ha_followup_header                 #
#                                                                                       #
#  DEFINITION                    : This function decodes ptp ha followup header         #
#                                                                                       #
#  INPUT PARAMETERS              :                                                      #
#                                                                                       #
#  hex                           : (Mandatory) hex dump                                 #
#                                                                                       #
#  OUTPUT PARAMETERS             :                                                      #
#                                                                                       #
#  precise_origin_timestamp_sec  : (Optional)  Precise Origin Timestamp                 #
#                                                                                       #
#  precise_origin_timestamp_ns   : (Optional)  Precise Origin Timestamp                 #
#                                                                                       #
#  RETURNS                       : 0 on failure (If the hexdump is not PTP HA FOLLOWUP  #
#                                                header)                                #
#                                                                                       #
#                                  1 on success (If the hexdump is PTP HA FOLLOWUP      #
#                                                header)                                #
#                                                                                       #
#  USAGE                         :                                                      #
#                                                                                       #
#   pbLib::decode_ptp_ha_followup_header\                                               #
#              -hex                               $hex\                                 #
#              -precise_origin_timestamp_sec      precise_origin_timestamp_sec\         #
#              -precise_origin_timestamp_ns       precise_origin_timestamp_ns           #
#                                                                                       #
#########################################################################################

proc pbLib::decode_ptp_ha_followup_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #hex dump
    if {[info exists param(-hex)]} {

        set hex $param(-hex)
    } else {

        ERRLOG -msg "pbLib::decode_ptp_ha_followup_header: Missing Argument -> hex"
        return 0    }

    #OUTPUT PARAMETERS

    #Precise Origin Timestamp
    if {[info exists param(-precise_origin_timestamp_sec)]} {

        upvar $param(-precise_origin_timestamp_sec) precise_origin_timestamp_sec
        set precise_origin_timestamp_sec ""
    }

    #Precise Origin Timestamp
    if {[info exists param(-precise_origin_timestamp_ns)]} {

        upvar $param(-precise_origin_timestamp_ns) precise_origin_timestamp_ns
        set precise_origin_timestamp_ns ""
    }

    #LOGIC GOES HERE
    ptp_ha_followup hdr = $hex

    ptp_ha_timestamp timestamp =  $hdr::preciseOriginTimestamp

    set precise_origin_timestamp_sec [hex2dec $timestamp::secondsField]
    set precise_origin_timestamp_ns  [hex2dec $timestamp::nanosecondsField]


    return 1

}


################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::decode_ptp_ha_pdelay_req_header               #
#                                                                              #
#  DEFINITION           : This function decodes ptp ha peer delay request hdr  #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  hex                  : (Mandatory) hex dump                                 #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  origin_timestamp_sec : (Optional)  Origin timestamp in seconds              #
#                                                                              #
#  origin_timestamp_ns  : (Optional)  Origin timestamp in nanoseconds          #
#                                                                              #
#  RETURNS              : 0 on failure (If the hexdump is not PTP HA PDELAY    #
#                                       REQUEST)                               #
#                                                                              #
#                         1 on success (If the hexdump is PTP HA PDELAY REQUEST#
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#   pbLib::decode_ptp_ha_pdelay_req_header\                                    #
#              -hex                               $hex\                        #
#              -origin_timestamp_sec              origin_timestamp_sec\        #
#              -origin_timestamp_ns               origin_timestamp_ns          #
#                                                                              #
################################################################################

proc pbLib::decode_ptp_ha_pdelay_req_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #hex dump
    if {[info exists param(-hex)]} {

        set hex $param(-hex)
    } else {

        ERRLOG -msg "pbLib::decode_ptp_ha_pdelay_req_header: Missing Argument -> hex"
        return 0    }

    #OUTPUT PARAMETERS

    #Origin timestamp in seconds
    if {[info exists param(-origin_timestamp_sec)]} {

        upvar $param(-origin_timestamp_sec) origin_timestamp_sec
        set origin_timestamp_sec ""
    }

    #Origin timestamp in nanoseconds
    if {[info exists param(-origin_timestamp_ns)]} {

        upvar $param(-origin_timestamp_ns) origin_timestamp_ns
        set origin_timestamp_ns ""
    }

    #LOGIC GOES HERE
    ptp_ha_pdelay_req hdr = $hex

    ptp_ha_timestamp timestamp =  $hdr::originTimestamp

    set origin_timestamp_sec [hex2dec $timestamp::secondsField]
    set origin_timestamp_ns  [hex2dec $timestamp::nanosecondsField]

    return 1

}


#########################################################################################
#                                                                                       #
#  PROCEDURE NAME                : pbLib::decode_ptp_ha_pdelay_resp_header              #
#                                                                                       #
#  DEFINITION                    : This function decodes ptp ha peer delay response hdr #
#                                                                                       #
#  INPUT PARAMETERS              :                                                      #
#                                                                                       #
#  hex                           : (Mandatory) hex dump                                 #
#                                                                                       #
#  OUTPUT PARAMETERS             :                                                      #
#                                                                                       #
#  request_receipt_timestamp_sec : (Optional)  Request Receipt Timestamp                #
#                                                                                       #
#  request_receipt_timestamp_ns  : (Optional)  Request Receipt Timestamp                #
#                                                                                       #
#  requesting_port_number        : (Optional)  Requesting Port Number                   #
#                                                                                       #
#  requesting_clock_identity     : (Optional)  Requesting Clock Identity                #
#                                                                                       #
#                                                                                       #
#  RETURNS                       : 0 on failure (If the hexdump is not PTP HA PDELAY    #
#                                       RESPONSE)                                       #
#                                                                                       #
#                                  1 on success (If the hexdump is PTP HA PDELAY        #
#                                       RESPONSE)                                       #
#                                                                                       #
#  USAGE                         :                                                      #
#                                                                                       #
#   pbLib::decode_ptp_ha_pdelay_resp_header\                                            #
#              -hex                               $hex\                                 #
#              -request_receipt_timestamp_sec     request_receipt_timestamp_sec\        #
#              -request_receipt_timestamp_ns      request_receipt_timestamp_ns\         #
#              -requesting_port_number            requesting_port_number\               #
#              -requesting_clock_identity         requesting_clock_identity             #
#                                                                                       #
#########################################################################################

proc pbLib::decode_ptp_ha_pdelay_resp_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #hex dump
    if {[info exists param(-hex)]} {

        set hex $param(-hex)
    } else {

        ERRLOG -msg "pbLib::decode_ptp_ha_pdelay_resp_header: Missing Argument -> hex"
        return 0    }

    #OUTPUT PARAMETERS

    #Request Receipt Timestamp
    if {[info exists param(-request_receipt_timestamp_sec)]} {

        upvar $param(-request_receipt_timestamp_sec) request_receipt_timestamp_sec
        set request_receipt_timestamp_sec ""
    }

    #Request Receipt Timestamp
    if {[info exists param(-request_receipt_timestamp_ns)]} {

        upvar $param(-request_receipt_timestamp_ns) request_receipt_timestamp_ns
        set request_receipt_timestamp_ns ""
    }

    #Requesting Port Number
    if {[info exists param(-requesting_port_number)]} {

        upvar $param(-requesting_port_number) requesting_port_number
        set requesting_port_number ""
    }

    #Requesting Clock Identity
    if {[info exists param(-requesting_clock_identity)]} {

        upvar $param(-requesting_clock_identity) requesting_clock_identity
        set requesting_clock_identity ""
    }

    #LOGIC GOES HERE
    ptp_ha_pdelay_resp hdr = $hex

    ptp_ha_timestamp timestamp =  $hdr::requestReceiptTimestamp

    set request_receipt_timestamp_sec  [hex2dec $timestamp::secondsField]
    set request_receipt_timestamp_ns   [hex2dec $timestamp::nanosecondsField]

    ptp_ha_port_identity port_identity =  $hdr::requestingPortIdentity

    set requesting_port_number       [hex2dec $port_identity::portNumber]
    set requesting_clock_identity    [hex2dec $port_identity::clockIdentity]


    return 1

}


##########################################################################################
#                                                                                        #
#  PROCEDURE NAME                : pbLib::decode_ptp_ha_pdelay_resp_followup_header      #
#                                                                                        #
#  DEFINITION                    : This function decodes ptp ha peer delay response      #
#                                  followup header                                       #
#                                                                                        #
#  INPUT PARAMETERS              :                                                       #
#                                                                                        #
#  hex                           : (Mandatory) hex dump                                  #
#                                                                                        #
#  OUTPUT PARAMETERS             :                                                       #
#                                                                                        #
#  response_origin_timestamp_sec :  (Optional)  Response Origin Timestamp                #
#                                                                                        #
#  response_origin_timestamp_ns  :  (Optional)  Response Origin Timestamp                #
#                                                                                        #
#  requesting_port_number        :  (Optional)  Requesting Port Number                   #
#                                                                                        #
#  requesting_clock_identity     :  (Optional)  Requesting Clock Identity                #
#                                                                                        #
#  RETURNS                       : 0 on failure                                          #
#                                                                                        #
#                                                                                        #
#                                  1 on success                                          #
#                                                                                        #
#                                                                                        #
#  USAGE                         :                                                       #
#                                                                                        #
#   pbLib::decode_ptp_ha_pdelay_resp_followup_header\                                    #
#              -hex                               $hex\                                  #
#              -response_origin_timestamp_sec     response_origin_timestamp_sec\         #
#              -response_origin_timestamp_ns      response_origin_timestamp_ns\          #
#              -requesting_port_number            requesting_port_number\                #
#              -requesting_clock_identity         requesting_clock_identity              #
#                                                                                        #
##########################################################################################
#
proc pbLib::decode_ptp_ha_pdelay_resp_followup_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #hex dump
    if {[info exists param(-hex)]} {

        set hex $param(-hex)
    } else {

        ERRLOG -msg "pbLib::decode_ptp_ha_pdelay_resp_followup_header: Missing Argument -> hex"
        return 0
    }

    #OUTPUT PARAMETERS

    #Response Origin Timestamp
    if {[info exists param(-response_origin_timestamp_sec)]} {

        upvar $param(-response_origin_timestamp_sec) response_origin_timestamp_sec
        set response_origin_timestamp_sec ""
    }

    #Response Origin Timestamp
    if {[info exists param(-response_origin_timestamp_ns)]} {

        upvar $param(-response_origin_timestamp_ns) response_origin_timestamp_ns
        set response_origin_timestamp_ns ""
    }

    #Requesting Port Number
    if {[info exists param(-requesting_port_number)]} {

        upvar $param(-requesting_port_number) requesting_port_number
        set requesting_port_number ""
    }

    #Requesting Clock Identity
    if {[info exists param(-requesting_clock_identity)]} {

        upvar $param(-requesting_clock_identity) requesting_clock_identity
        set requesting_clock_identity ""
    }

    #LOGIC GOES HERE

    ptp_ha_pdelay_resp_followup hdr = $hex

    ptp_ha_timestamp timestamp = $hdr::responseOriginTimestamp

    set response_origin_timestamp_sec [hex2dec $timestamp::secondsField]
    set response_origin_timestamp_ns  [hex2dec $timestamp::nanosecondsField]

    ptp_ha_port_identity port_identity = $hdr::requestingPortIdentity

    set requesting_clock_identity [hex2dec $port_identity::clockIdentity]
    set requesting_port_number    [hex2dec $port_identity::portNumber]

    return 1

}


#################################################################################
#                                                                               #
#  PROCEDURE NAME                : pbLib::decode_ptp_ha_signal_header           #
#                                                                               #
#  DEFINITION                    : This function decodes ptp ha signal header   #
#                                                                               #
#  INPUT PARAMETERS              :                                              #
#                                                                               #
#  hex                           : (Mandatory) hex dump                         #
#                                                                               #
#  OUTPUT PARAMETERS             :                                              #
#                                                                               #
#  target_port_number            : (Optional)  Target Port Number               #
#                                                                               #
#  target_clock_identity         : (Optional)  Target Clock Identity            #
#                                                                               #
#  tlv                           : (Optional)  TLV                              #
#                                                                               #
#  tlv_type                      : (Optional)  TLV Type                         #
#                                                                               #
#  tlv_length                    : (Optional)  TLV length                       #
#                                                                               #
#  ope                           : (Optional)  OPE Flag                         #
#                                                                               #
#  cr                            : (Optional)  CR Flag                          #
#                                                                               #
#  rcr                           : (Optional)  RCR Flag                         #
#                                                                               #
#  tcr                           : (Optional)  TCR Flag                         #
#                                                                               #
#  ic                            : (Optional)  IC Flag                          #
#                                                                               #
#  irc                           : (Optional)  IRC Flag                         #
#                                                                               #
#  itc                           : (Optional)  ITC Flag                         #
#                                                                               #
#  fov                           : (Optional)  FOV Flag                         #
#                                                                               #
#  pov                           : (Optional)  POV Flag                         #
#                                                                               #
#  tct                           : (Optional)  TCT Flag                         #
#                                                                               #
#  phase_offset_tx               : (Optional)  Phase offset tx                  #
#                                                                               #
#  phase_offset_tx_timestamp_sec : (Optional)  Phase offset tx timestamp Sec    #
#                                                                               #
#  phase_offset_tx_timestamp_ns  : (Optional)  Phase offset tx timsstamp Ns     #
#                                                                               #
#  freq_offset_tx                : (Optional)  Frequency offset tx              #
#                                                                               #
#  freq_offset_tx_timestamp_sec  : (Optional)  Frequency offset tx timestamp    #
#                                                                               #
#  freq_offset_tx_timestamp_ns   : (Optional)  Frequency offset tx timstamp     #
#                                                                               #
#  RETURNS                       : 0 on failure (If the hexdump is not PTP HA   #
#                                                SIGNAL)                        #
#                                                                               #
#                                  1 on success (If the hexdump is PTP HA SIGNAL#
#                                                                               #
#   #USAGE                                                                      #
#   pbLib::decode_ptp_ha_signal_header\                                         #
#              -hex                               $hex\                         #
#              -target_port_number                target_port_number\           #
#              -target_clock_identity             target_clock_identity\        #
#              -tlv_type                          tlv_type\                     #
#              -tlv_length                        tlv_length\                   #
#              -ope                               ope\                          #
#              -cr                                cr\                           #
#              -rcr                               rcr\                          #
#              -tcr                               tcr\                          #
#              -ic                                ic\                           #
#              -irc                               irc\                          #
#              -itc                               itc\                          #
#              -fov                               fov\                          #
#              -pov                               pov\                          #
#              -tct                               tct\                          #
#              -phase_offset_tx                   phase_offset_tx\              #
#              -phase_offset_tx_timestamp_sec     phase_offset_tx_timestamp_sec\#
#              -phase_offset_tx_timestamp_ns      phase_offset_tx_timestamp_ns\ #
#              -freq_offset_tx                    freq_offset_tx\               #
#              -freq_offset_tx_timestamp_sec      freq_offset_tx_timestamp_sec\ #
#              -freq_offset_tx_timestamp_ns       freq_offset_tx_timestamp_ns   #
#                                                                               #
#################################################################################

proc pbLib::decode_ptp_ha_signal_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #hex dump
    if {[info exists param(-hex)]} {

        set hex $param(-hex)
    } else {

        ERRLOG -msg "pbLib::decode_ptp_ha_signal_header: Missing Argument -> hex"
        return 0    }

    #OUTPUT PARAMETERS

    #Target Port Number
    if {[info exists param(-target_port_number)]} {

        upvar $param(-target_port_number) target_port_number
        set target_port_number ""
    }

    #Target Clock Identity
    if {[info exists param(-target_clock_identity)]} {

        upvar $param(-target_clock_identity) target_clock_identity
        set target_clock_identity ""
    }

    #TLV
    if {[info exists param(-tlv)]} {

        upvar $param(-tlv) tlv
        set tlv "NONE"
    }

    #TLV Type
    if {[info exists param(-tlv_type)]} {

        upvar $param(-tlv_type) tlv_type
        set tlv_type ""
    }

    #TLV length
    if {[info exists param(-tlv_length)]} {

        upvar $param(-tlv_length) tlv_length
        set tlv_length ""
    }

    #OPE Flag
    if {[info exists param(-ope)]} {

        upvar $param(-ope) ope
        set ope ""
    }

    #CR Flag
    if {[info exists param(-cr)]} {

        upvar $param(-cr) cr
        set cr ""
    }

    #RCR Flag
    if {[info exists param(-rcr)]} {

        upvar $param(-rcr) rcr
        set rcr ""
    }

    #TCR Flag
    if {[info exists param(-tcr)]} {

        upvar $param(-tcr) tcr
        set tcr ""
    }

    #IC Flag
    if {[info exists param(-ic)]} {

        upvar $param(-ic) ic
        set ic ""
    }

    #IRC Flag
    if {[info exists param(-irc)]} {

        upvar $param(-irc) irc
        set irc ""
    }

    #ITC Flag
    if {[info exists param(-itc)]} {

        upvar $param(-itc) itc
        set itc ""
    }

    #FOV Flag
    if {[info exists param(-fov)]} {

        upvar $param(-fov) fov
        set fov ""
    }

    #POV Flag
    if {[info exists param(-pov)]} {

        upvar $param(-pov) pov
        set pov ""
    }

    #TCT Flag
    if {[info exists param(-tct)]} {

        upvar $param(-tct) tct
        set tct ""
    }

    #Phase offset tx
    if {[info exists param(-phase_offset_tx)]} {

        upvar $param(-phase_offset_tx) phase_offset_tx
        set phase_offset_tx ""
    }

    #Phase offset tx timestamp Sec
    if {[info exists param(-phase_offset_tx_timestamp_sec)]} {

        upvar $param(-phase_offset_tx_timestamp_sec) phase_offset_tx_timestamp_sec
        set phase_offset_tx_timestamp_sec ""
    }

    #Phase offset tx timsstamp Ns
    if {[info exists param(-phase_offset_tx_timestamp_ns)]} {

        upvar $param(-phase_offset_tx_timestamp_ns) phase_offset_tx_timestamp_ns
        set phase_offset_tx_timestamp_ns ""
    }

    #Frequency offset tx
    if {[info exists param(-freq_offset_tx)]} {

        upvar $param(-freq_offset_tx) freq_offset_tx
        set freq_offset_tx ""
    }

    #Frequency offset tx timestamp
    if {[info exists param(-freq_offset_tx_timestamp_sec)]} {

        upvar $param(-freq_offset_tx_timestamp_sec) freq_offset_tx_timestamp_sec
        set freq_offset_tx_timestamp_sec ""
    }

    #Frequency offset tx timstamp
    if {[info exists param(-freq_offset_tx_timestamp_ns)]} {

        upvar $param(-freq_offset_tx_timestamp_ns) freq_offset_tx_timestamp_ns
        set freq_offset_tx_timestamp_ns ""
    }


    #LOGIC GOES HERE
    ptp_ha_signal hdr = $hex

    ptp_ha_port_identity port_identity =  $hdr::targetPortIdentity

    set target_clock_identity [hex2dec $port_identity::clockIdentity]
    set target_port_number    [hex2dec $port_identity::portNumber]

    set hex [string range $hex 20 end]

    #Signalling Message - TLV
    ptp_ha_signal_tlv signal_tlv = $hex

    set tlv_type   [hex2dec $signal_tlv::tlvType]
    set tlv_length [hex2dec $signal_tlv::lengthField]

    set hex [string range $hex 8 end]

    #L1 SYNC
    if {$tlv_type == 32769} {
        set tlv L1_SYNC
        
        ptp_ha_l1_sync_tlv  l1sync = $hex

        #L1 SYNC EXTENDED FORMAT
        if {($l1sync::OPE && ($tlv_length >= 40))} {

            ptp_ha_l1_sync_tlv_ext  l1sync = $hex

            set fov                         $l1sync::FOV
            set pov                         $l1sync::POV
            set tct                         $l1sync::TCT
            set phase_offset_tx             [hex2dec $l1sync::phaseOffsetTx]
            set phase_offset_tx_timestamp   $l1sync::phaseOffsetTxTimestamp
            set freq_offset_tx              [hex2dec $l1sync::freqOffsetTx]
            set freq_offset_tx_timestamp    $l1sync::freqOffsetTxTimestamp
            
            #Phase Offset Tx Timestamp
            ptp_ha_timestamp phase_timestamp =  $phase_offset_tx_timestamp
            set phase_offset_tx_timestamp_sec [hex2dec $phase_timestamp::secondsField]
            set phase_offset_tx_timestamp_ns  [hex2dec $phase_timestamp::nanosecondsField]
            
            #Frequency Tx Timestamp
            ptp_ha_timestamp freq_timestamp =  $freq_offset_tx_timestamp
            set freq_offset_tx_timestamp_sec [hex2dec $freq_timestamp::secondsField]
            set freq_offset_tx_timestamp_ns  [hex2dec $freq_timestamp::nanosecondsField]

        }

        set ope                         $l1sync::OPE
        set cr                          $l1sync::CR
        set rcr                         $l1sync::RCR
        set tcr                         $l1sync::TCR
        set ic                          $l1sync::IC
        set irc                         $l1sync::IRC
        set itc                         $l1sync::ITC
    }

    return 1

}


####################################################################################
#                                                                                  #
#  PROCEDURE NAME           : pbLib::decode_ptp_ha_mgmt_header                     #
#                                                                                  #
#  DEFINITION               : This function decodes ptp ha management header       #
#                                                                                  #
#  INPUT PARAMETERS         :                                                      #
#                                                                                  #
#  hex                      : (Mandatory) hex dump                                 #
#                                                                                  #
#  OUTPUT PARAMETERS        :                                                      #
#                                                                                  #
#  target_port_number       : (Optional)  Target Port Number                       #
#                                                                                  #
#  target_clock_identity    : (Optional)  Target Clock Identity                    #
#                                                                                  #
#  starting_boundary_hops   : (Optional)  Starting Boundary Hops                   #
#                                                                                  #
#  boundary_hops            : (Optional)  Boundary Hops                            #
#                                                                                  #
#  action_field             : (Optional)  Action Field                             #
#                                                                                  #
#  tlv_type                 : (Optional)  TLV Type                                 #
#                                                                                  #
#  tlv_length               : (Optional)  TLV length                               #
#                                                                                  #
#  management_id            : (Optional)  Management ID                            #
#                                                                                  #
#  ext_port_config          : (Optional)  External Port Configuration              #
#                                                                                  #
#  master_only              : (Optional)  Master only                              #
#                                                                                  #
#                                                                                  #
#  RETURNS                  : 0 on failure (If the hexdump is not PTP HA MANAGEMENT#
#                                                                                  #
#                             1 on success (If the hexdump is PTP HA MANAGEMENT    #
#                                                                                  #
#  USAGE                    :                                                      #
#                                                                                  #
#   pbLib::decode_ptp_ha_mgmt_header\                                              #
#              -hex                               $hex\                            #
#              -target_port_number                target_port_number\              #
#              -target_clock_identity             target_clock_identity\           #
#              -starting_boundary_hops            starting_boundary_hops\          #
#              -boundary_hops                     boundary_hops\                   #
#              -action_field                      action_field\                    #
#              -tlv_type                          tlv_type\                        #
#              -tlv_length                        tlv_length\                      #
#              -management_id                     management_id\                   #
#              -ext_port_config                   ext_port_config\                 #
#              -master_only                       master_only                      #
#                                                                                  #
####################################################################################

proc pbLib::decode_ptp_ha_mgmt_header {args} {

    array set param $args

    #INPUT PARAMETERS

    #hex dump
    if {[info exists param(-hex)]} {

        set hex $param(-hex)
    } else {

        ERRLOG -msg "pbLib::decode_ptp_ha_mgmt_header: Missing Argument -> hex"
        return 0
    }

    #OUTPUT PARAMETERS

    #Target Port Number
    if {[info exists param(-target_port_number)]} {

        upvar $param(-target_port_number) target_port_number
        set target_port_number ""
    }

    #Target Clock Identity
    if {[info exists param(-target_clock_identity)]} {

        upvar $param(-target_clock_identity) target_clock_identity
        set target_clock_identity ""
    }

    #Starting Boundary Hops
    if {[info exists param(-starting_boundary_hops)]} {

        upvar $param(-starting_boundary_hops) starting_boundary_hops
        set starting_boundary_hops ""
    }

    #Boundary Hops
    if {[info exists param(-boundary_hops)]} {

        upvar $param(-boundary_hops) boundary_hops
        set boundary_hops ""
    }

    #Action Field
    if {[info exists param(-action_field)]} {

        upvar $param(-action_field) action_field
        set action_field ""
    }

    #TLV Type
    if {[info exists param(-tlv_type)]} {

        upvar $param(-tlv_type) tlv_type
        set tlv_type ""
    }

    #TLV length
    if {[info exists param(-tlv_length)]} {

        upvar $param(-tlv_length) tlv_length
        set tlv_length ""
    }

    #Management ID
    if {[info exists param(-management_id)]} {

        upvar $param(-management_id) management_id
        set management_id ""
    }

    #External Port Configuration
    if {[info exists param(-ext_port_config)]} {

        upvar $param(-ext_port_config) ext_port_config
        set ext_port_config ""
    }

    #Master only
    if {[info exists param(-master_only)]} {

        upvar $param(-master_only) master_only
        set master_only ""
    }

    #LOGIC GOES HERE

    #Management Header
    ptp_ha_mgmt hdr = $hex

    set target_port_identity     $hdr::targetPortIdentity
    set starting_boundary_hops   [hex2dec $hdr::startingBoundaryHops]
    set boundary_hops            [hex2dec $hdr::boundaryHops]
    set action_field             [hex2dec $hdr::actionField]

    #Target Port Identity
    ptp_ha_port_identity port_identity = $target_port_identity

    set target_clock_identity [hex2dec $port_identity::clockIdentity]
    set target_port_number    [hex2dec $port_identity::portNumber]

    set hex [string range $hex 28 end]

    #Management TLV Header
    ptp_ha_mgmt_tlv mgmt_tlv = $hex

    set tlv_type        [hex2dec $mgmt_tlv::tlvType]
    set tlv_length      [hex2dec $mgmt_tlv::lengthField]
    set management_id   [hex2dec $mgmt_tlv::managementId]

    set hex [string range $hex 12 end]

    switch $management_id {

        12288 {
            ptp_ha_master_only_tlv tlv = $hex
            set master_only $tlv::MO
        }

        12289 {
            ptp_ha_ext_port_conf_enabled_tlv tlv = $hex
            set ext_port_config $tlv::EPC
        }

    }

    return 1

}

################################################################################
#                                                                              #
#  PROCEDURE NAME       : pbLib::encode_ptp_ha_pkt                             #
#                                                                              #
#  DEFINITION           : This function constructs ptp ha packet               #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  (Ethernet HEADER)                                                           #
#                                                                              #
#  dest_mac             : (Optional) Destination MAC address of eth header     #
#                                    ( Default : 00:00:00:00:00:AA )           #
#                                                                              #
#  src_mac              : (Optional) Source MAC address of eth header          #
#                                    ( Default : 00:00:00:00:00:BB )           #
#                                                                              #
#  eth_type             : (Optional) Ethernet type/length of eth header        #
#                                    ( Default : 0800 )                        #
#                                                                              #
#                                                                              #
#  (VLAN HEADER)                                                               #
#                                                                              #
#  vlan_id              : (Optional) Vlan id of vlan header                    #
#                                    ( Default : -1 )                          #
#                                                                              #
#  vlan_priority        : (Optional) Priority of vlan header                   #
#                                    ( Default : 1 )                           #
#                                                                              #
#  vlan_dei             : (Optional) Eligible indicator of vlan header         #
#                                    ( Default : 0 )                           #
#                                                                              #
#                                                                              #
#  (IPv4 HEADER)                                                               #
#                                                                              #
#  ip_version           : (Optional) Version of ip                             #
#                                    ( Default : 4 )                           #
#                                                                              #
#  ip_header_length     : (Optional) Header length  of ipv4                    #
#                                    ( Default : 20)                           #
#                                                                              #
#  ip_tos               : (Optional) Type of service (hex value)               #
#                                    ( Default : 00 )                          #
#                                                                              #
#  ip_total_length      : (Optional) Total length  of ipv4                     #
#                                    ( Default : 72 )                          #
#                                                                              #
#  ip_identification    : (Optional) Unique id of ipv4                         #
#                                    ( Default : 0 )                           #
#                                                                              #
#  ip_flags             : (Optional) Flags represents the control or identify  #
#                                    fragments. ( Default : 0 )                #
#                                    ( 0 - Reserved , 1 - Don't fragment ,     #
#                                      2 - More fragment )                     #
#                                                                              #
#  ip_offset            : (Optional) Fragment offset                           #
#                                    ( Default : 0 )                           #
#                                                                              #
#  ip_ttl               : (Optional) Time to live                              #
#                                    ( Default : 64 )                          #
#                                                                              #
#  ip_checksum          : (Optional) Error-checking of header(16-bit checksum) #
#                                    ( Default : auto )                        #
#                                                                              #
#  ip_protocol          : (Optional) Protocol in the data portion of the       #
#                                    IP datagram. ( Default : 17 )             #
#                                                                              #
#  src_ip               : (Optional) IPv4 address of the sender of the packet  #
#                                    ( Default : 0.0.0.0 )                     #
#                                                                              #
#  dest_ip              : (Optional) IPv4 address of the reciever of the packet#
#                                    ( Default : 0.0.0.0 )                     #
#                                                                              #
#                                                                              #
#  (UDP HEADER)                                                                #
#                                                                              #
#  src_port             : (Optional) Sender's port number                      #
#                                    ( Default : 319 )                         #
#                                                                              #
#  dest_port            : (Optional) Receiver's port number                    #
#                                    ( Default : 319 )                         #
#                                                                              #
#  udp_length           : (Optional) Length in bytes of UDP header and data    #
#                                    ( Default : 52 )                          #
#                                                                              #
#  udp_checksum         : (Optional) Error-checking of the header              #
#                                    ( Default : auto )                        #
#                                                                              #
#                                                                              #
#  (PTP HA COMMON HEADER)                                                      #
#                                                                              #
#  major_sdo_id          :  (Optional)  Major sdo id                           #
#                                       (Default : 0)                          #
#                                                                              #
#  message_type          :  (Optional)  Message Type                           #
#                                       (Default : 0)                          #
#                                                                              #
#  minor_version         :  (Optional)  PTP version number                     #
#                                       (Default : 2)                          #
#                                                                              #
#  version_ptp           :  (Optional)  PTP minor version number               #
#                                       (Default : 2)                          #
#                                                                              #
#  message_length        :  (Optional)  PTP Sync message length                #
#                                       (Default : 44)                         #
#                                                                              #
#  domain_number         :  (Optional)  Domain number                          #
#                                       (Default : 0)                          #
#                                                                              #
#  minor_sdo_id          :  (Optional)  Minor Sdo id                           #
#                                       (Default : 0)                          #
#                                                                              #
#  alternate_master_flag :  (Optional)  Alternate Master flag                  #
#                                       (Default : 0)                          #
#                                                                              #
#  two_step_flag         :  (Optional)  Two Step Flag                          #
#                                       (Default : 0)                          #
#                                                                              #
#  unicast_flag          :  (Optional)  Unicast Flag                           #
#                                       (Default : 0)                          #
#                                                                              #
#  profile_specific1     :  (Optional)  Profile specific1 Flag                 #
#                                       (Default : 0)                          #
#                                                                              #
#  profile_specific2     :  (Optional)  Profile specific2 Flag                 #
#                                       (Default : 0)                          #
#                                                                              #
#  secure                :  (Optional)  Secure Flag                            #
#                                       (Default : 0)                          #
#                                                                              #
#  leap61                :  (Optional)  Leap61 Flag                            #
#                                       (Default : 0)                          #
#                                                                              #
#  leap59                :  (Optional)  Leap59 Flag                            #
#                                       (Default : 0)                          #
#                                                                              #
#  current_utc_offset_valid:(Optional)  Current UTC Offset Flag                #
#                                       (Default : 0)                          #
#                                                                              #
#  ptp_timescale         :  (Optional)  PTP Timescale Flag                     #
#                                       (Default : 0)                          #
#                                                                              #
#  time_traceable        :  (Optional)  Time Traceable Flag                    #
#                                       (Default : 0)                          #
#                                                                              #
#  freq_traceable        :  (Optional)  Frequency Traceable Flag               #
#                                       (Default : 0)                          #
#                                                                              #
#  sync_uncertain        :  (Optional)  Synchronization uncertain Flag         #
#                                       (Default : 0)                          #
#                                                                              #
#  correction_field      :  (Optional)  Correction Field value                 #
#                                       (Default : 0)                          #
#                                                                              #
#  message_type_specific :  (Optional)  Message type                           #
#                                       (Default : 0)                          #
#                                                                              #
#  src_port_number       :  (Optional)  Source port number                     #
#                                       (Default : 0)                          #
#                                                                              #
#  src_clock_identity    :  (Optional)  Source Clock identity                  #
#                                       (Default : 0)                          #
#                                                                              #
#  sequence_id           :  (Optional)  Sequence ID                            #
#                                       (Default : 0)                          #
#                                                                              #
#  control_field         :  (Optional)  Control field                          #
#                                       (Default : 0)                          #
#                                                                              #
#  log_message_interval  :  (Optional)  Log message interval                   #
#                                       (Default : 127)                        #
#                                                                              #
#  (PTP HA SYNC/DELAY REQUEST HEADER)                                          #
#                                                                              #
#  origin_timestamp_sec       :  (Optional)  Origin timestamp in seconds       #
#                                            (Default : 0)                     #
#                                                                              #
#  origin_timestamp_ns        :  (Optional)  Origin timestamp in nanoseconds   #
#                                            (Default : 0)                     #
#                                                                              #
#  (PTP HA ANNOUNCE HEADER)                                                    #
#                                                                              #
#  current_utc_offset_valid   :  (Optional)  Current UTC Offset                #
#                                            (Default : 0)                     #
#                                                                              #
#  gm_priority1               :  (Optional)  Grand Master Priority 1           #
#                                            (Default : 0)                     #
#                                                                              #
#  gm_priority2               :  (Optional)  Grand Master Priority 2           #
#                                            (Default : 128)                   #
#                                                                              #
#  gm_identity                :  (Optional)  Grand Master Identity             #
#                                            (Default : 0)                     #
#                                                                              #
#  steps_removed              :  (Optional)  Steps Removed                     #
#                                            (Default : 0)                     #
#                                                                              #
#  time_source                :  (Optional)  Time Source                       #
#                                            (Default : 0)                     #
#                                                                              #
#  gm_clock_classy            :  (Optional)  Grand Master Clock classy         #
#                                            (Default : 0)                     #
#                                                                              #
#  gm_clock_accuracy          :  (Optional)  Grand Master Clock accuracy       #
#                                            (Default : 0)                     #
#                                                                              #
#  gm_clock_variance          :  (Optional)  Grand Master Clock offset scaled  #
#                                        log variance (Default : 0)            #
#                                                                              #
#  (PTP HA OTHER HEADERS)                                                      #
#                                                                              #
#  precise_origin_timestamp_sec  :  (Optional)  Precise Origin Timestamp       #
#                                     (Default : 0)                            #
#                                                                              #
#  precise_origin_timestamp_ns   :  (Optional)  Precise Origin Timestamp       #
#                                     (Default : 0)                            #
#                                                                              #
#  receive_timestamp_sec         :  (Optional)  Receive Timestamp              #
#                                     (Default : 0)                            #
#                                                                              #
#  receive_timestamp_ns          :  (Optional)  Receive Timestamp              #
#                                     (Default : 0)                            #
#                                                                              #
#  requesting_port_number        :  (Optional)  Requesting Port Number         #
#                                       (Default : 0)                          #
#                                                                              #
#  requesting_clock_identity     :  (Optional)  Requesting Clock Identity      #
#                                       (Default : 0)                          #
#                                                                              #
#  request_receipt_timestamp_sec :  (Optional)  Request Receipt Timestamp      #
#                                       (Default : 0)                          #
#                                                                              #
#  request_receipt_timestamp_ns  :  (Optional)  Request Receipt Timestamp      #
#                                       (Default : 0)                          #
#                                                                              #
#  response_origin_timestamp_sec :  (Optional)  Response Origin Timestamp      #
#                                       (Default : 0)                          #
#                                                                              #
#  response_origin_timestamp_ns  :  (Optional)  Response Origin Timestamp      #
#                                      (Default : 0)                           #
#                                                                              #
#  target_port_number            :  (Optional)  Target Port number             #
#                                       (Default : 0)                          #
#                                                                              #
#  target_clock_identity         :  (Optional)  Target clock identity          #
#                                       (Default : 0)                          #
#                                                                              #
#  starting_boundary_hops        :  (Optional)  Starting Boundary Hops         #
#                                       (Default : 0)                          #
#                                                                              #
#  boundary_hops                 :  (Optional)  Boundary Hops                  #
#                                       (Default : 0)                          #
#                                                                              #
#  action_field                  :  (Optional)  Action Field                   #
#                                       (Default : 0)                          #
#                                                                              #
#  (PTP HA TLVs)                                                               #
#                                                                              #
#  tlv                           :  (Optional)  TLV                            #
#                                       (Default : NONE)                       #
#                                                                              #
#  tlv_type                      :  (Optional)  TLV Type                       #
#                                       (Default : 32769)                      #
#                                                                              #
#  tlv_length                    :  (Optional)  TLV length                     #
#                                       (Default : 30)                         #
#                                                                              #
#  ope                           :  (Optional)  OPE Flag                       #
#                                       (Default : 0)                          #
#                                                                              #
#  cr                            :  (Optional)  CR Flag                        #
#                                       (Default : 0)                          #
#                                                                              #
#  rcr                           :  (Optional)  RCR Flag                       #
#                                       (Default : 0)                          #
#                                                                              #
#  tcr                           :  (Optional)  TCR Flag                       #
#                                       (Default : 0)                          #
#                                                                              #
#  ic                            :  (Optional)  IC Flag                        #
#                                       (Default : 0)                          #
#                                                                              #
#  irc                           :  (Optional)  IRC Flag                       #
#                                       (Default : 0)                          #
#                                                                              #
#  itc                           :  (Optional)  ITC Flag                       #
#                                       (Default : 0)                          #
#                                                                              #
#  fov                           :  (Optional)  FOV Flag                       #
#                                       (Default : 0)                          #
#                                                                              #
#  pov                           :  (Optional)  POV Flag                       #
#                                       (Default : 0)                          #
#                                                                              #
#  tct                           :  (Optional)  TCT Flag                       #
#                                       (Default : 0)                          #
#                                                                              #
#  phase_offset_tx               :  (Optional)  Phase offset tx                #
#                                       (Default : 0)                          #
#                                                                              #
#  phase_offset_tx_timestamp_sec :  (Optional)  Phase offset tx timestamp Sec  #
#                                       (Default : 0)                          #
#                                                                              #
#  phase_offset_tx_timestamp_ns  :  (Optional)  Phase offset tx timsstamp Ns   #
#                                       (Default : 0)                          #
#                                                                              #
#  freq_offset_tx                :  (Optional)  Frequency offset tx            #
#                                       (Default : 0)                          #
#                                                                              #
#  freq_offset_tx_timestamp_sec  :  (Optional)  Frequency offset tx timestamp  #
#                                       (Default : 0)                          #
#                                                                              #
#  freq_offset_tx_timestamp_ns   :  (Optional)  Frequency offset tx timstamp   #
#                                       (Default : 0)                          #
#                                                                              #
#  management_id                 :  (Optional)  Management ID                  #
#                                       (Default : 12288)                      #
#                                                                              #
#  ext_port_config               :  (Optional)  External Port Configuration    #
#                                       (Default : 0)                          #
#                                                                              #
#  master_only                   :  (Optional)  Master only                    #
#                                       (Default : 0)                          #
#                                                                              #
#                                                                              #
#  OUTPUT PARAMETERS    :                                                      #
#                                                                              #
#  hexdump                       :  (Optional)  PTP HA packet hex dump         #
#                                                                              #
#  RETURNS              : 1 on success, 0 on failure                           #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#   pbLib::encode_ptp_ha_pkt\                                                  #
#              -dest_mac                       $dest_mac\                      #
#              -src_mac                        $src_mac\                       #
#              -eth_type                       $eth_type\                      #
#              -vlan_id                        $vlan_id\                       #
#              -vlan_priority                  $vlan_priority\                 #
#              -vlan_dei                       $vlan_dei\                      #
#              -ip_version                     $ip_version\                    #
#              -ip_header_length               $ip_header_length\              #
#              -ip_tos                         $ip_tos\                        #
#              -ip_total_length                $ip_total_length\               #
#              -ip_identification              $ip_identification\             #
#              -ip_flags                       $ip_flags\                      #
#              -ip_offset                      $ip_offset\                     #
#              -ip_ttl                         $ip_ttl\                        #
#              -ip_checksum                    $ip_checksum\                   #
#              -ip_protocol                    $ip_protocol\                   #
#              -src_ip                         $src_ip\                        #
#              -dest_ip                        $dest_ip\                       #
#              -src_port                       $src_port\                      #
#              -dest_port                      $dest_port\                     #
#              -udp_length                     $udp_length\                    #
#              -udp_checksum                   $udp_checksum\                  #
#              -major_sdo_id                   $major_sdo_id\                  #
#              -message_type                   $message_type\                  #
#              -minor_version                  $minor_version\                 #
#              -version_ptp                    $version_ptp\                   #
#              -message_length                 $message_length\                #
#              -domain_number                  $domain_number\                 #
#              -minor_sdo_id                   $minor_sdo_id\                  #
#              -alternate_master_flag          $alternate_master_flag\         #
#              -two_step_flag                  $two_step_flag\                 #
#              -unicast_flag                   $unicast_flag\                  #
#              -profile_specific1              $profile_specific1\             #
#              -profile_specific2              $profile_specific2\             #
#              -secure                         $secure\                        #
#              -leap61                         $leap61\                        #
#              -leap59                         $leap59\                        #
#              -current_utc_offset_valid       $current_utc_offset_valid\      #
#              -ptp_timescale                  $ptp_timescale\                 #
#              -time_traceable                 $time_traceable\                #
#              -freq_traceable                 $freq_traceable\                #
#              -sync_uncertain                 $sync_uncertain\                #
#              -correction_field               $correction_field\              #
#              -message_type_specific          $message_type_specific\         #
#              -src_port_number                $src_port_number\               #
#              -src_clock_identity             $src_clock_identity\            #
#              -sequence_id                    $sequence_id\                   #
#              -control_field                  $control_field\                 #
#              -log_message_interval           $log_message_interval\          #
#              -origin_timestamp_sec           $origin_timestamp_sec\          #
#              -origin_timestamp_ns            $origin_timestamp_ns\           #
#              -current_utc_offset_valid       $current_utc_offset_valid\      #
#              -gm_priority1                   $gm_priority1\                  #
#              -gm_priority2                   $gm_priority2\                  #
#              -gm_identity                    $gm_identity\                   #
#              -steps_removed                  $steps_removed\                 #
#              -time_source                    $time_source\                   #
#              -gm_clock_classy                $gm_clock_classy\               #
#              -gm_clock_accuracy              $gm_clock_accuracy\             #
#              -gm_clock_variance              $gm_clock_variance\             #
#              -precise_origin_timestamp_sec   $precise_origin_timestamp_sec\  #
#              -precise_origin_timestamp_ns    $precise_origin_timestamp_ns\   #
#              -receive_timestamp_sec          $receive_timestamp_sec\         #
#              -receive_timestamp_ns           $receive_timestamp_ns\          #
#              -requesting_port_number         $requesting_port_number\        #
#              -requesting_clock_identity      $requesting_clock_identity\     #
#              -request_receipt_timestamp_sec  $request_receipt_timestamp_sec\ #
#              -request_receipt_timestamp_ns   $request_receipt_timestamp_ns\  #
#              -response_origin_timestamp_sec  $response_origin_timestamp_sec\ #
#              -response_origin_timestamp_ns   $response_origin_timestamp_ns\  #
#              -target_port_number             $target_port_number\            #
#              -target_clock_identity          $target_clock_identity\         #
#              -starting_boundary_hops         $starting_boundary_hops\        #
#              -boundary_hops                  $boundary_hops\                 #
#              -action_field                   $action_field\                  #
#              -tlv                            $tlv\                           #
#              -tlv_type                       $tlv_type\                      #
#              -tlv_length                     $tlv_length\                    #
#              -ope                            $ope\                           #
#              -cr                             $cr\                            #
#              -rcr                            $rcr\                           #
#              -tcr                            $tcr\                           #
#              -ic                             $ic\                            #
#              -irc                            $irc\                           #
#              -itc                            $itc\                           #
#              -fov                            $fov\                           #
#              -pov                            $pov\                           #
#              -tct                            $tct\                           #
#              -phase_offset_tx                $phase_offset_tx\               #
#              -phase_offset_tx_timestamp_sec  $phase_offset_tx_timestamp_sec\ #
#              -phase_offset_tx_timestamp_ns   $phase_offset_tx_timestamp_ns\  #
#              -freq_offset_tx                 $freq_offset_tx\                #
#              -freq_offset_tx_timestamp_sec   $freq_offset_tx_timestamp_sec\  #
#              -freq_offset_tx_timestamp_ns    $freq_offset_tx_timestamp_ns\   #
#              -management_id                  $management_id\                 #
#              -ext_port_config                $ext_port_config\               #
#              -master_only                    $master_only\                   #
#              -hexdump                        hexdump                         #
#                                                                              #
################################################################################
#
proc pbLib::encode_ptp_ha_pkt {args} {

    array set param $args

    #INPUT PARAMETERS

    #Destination MAC address of eth header
    if {[info exists param(-dest_mac)]} {

        set dest_mac $param(-dest_mac)
    } else {

        set dest_mac 00:00:00:00:00:AA
    }

    #Source MAC address of eth header
    if {[info exists param(-src_mac)]} {

        set src_mac $param(-src_mac)
    } else {

        set src_mac 00:00:00:00:00:BB
    }

    #Ethernet type
    if {[info exists param(-eth_type)]} {

        set eth_type $param(-eth_type)
    } else {

        set eth_type 0800
    }

    #Vlan id of vlan header
    if {[info exists param(-vlan_id)]} {

        set vlan_id $param(-vlan_id)
    } else {

        set vlan_id -1
    }

    #Priority of vlan header
    if {[info exists param(-vlan_priority)]} {

        set vlan_priority $param(-vlan_priority)
    } else {

        set vlan_priority 1
    }

    #Eligible indicator of vlan header
    if {[info exists param(-vlan_dei)]} {

        set vlan_dei $param(-vlan_dei)
    } else {

        set vlan_dei 0
    }

    #Version of ip
    if {[info exists param(-ip_version)]} {

        set ip_version $param(-ip_version)
    } else {

        set ip_version 4
    }

    #Header length  of ipv4
    if {[info exists param(-ip_header_length)]} {

        set ip_header_length $param(-ip_header_length)
    } else {

        set ip_header_length 20
    }

    #Type of service
    if {[info exists param(-ip_tos)]} {

        set ip_tos $param(-ip_tos)
    } else {

        set ip_tos 00
    }

    #Total length  of ipv4
    if {[info exists param(-ip_total_length)]} {

        set ip_total_length $param(-ip_total_length)
    } else {

        set ip_total_length 72
    }

    #Unique id of ipv4
    if {[info exists param(-ip_identification)]} {

        set ip_identification $param(-ip_identification)
    } else {

        set ip_identification 0
    }

    #Flags represents the control or identify
    if {[info exists param(-ip_flags)]} {

        set ip_flags $param(-ip_flags)
    } else {

        set ip_flags 0
    }

    #Fragment offset
    if {[info exists param(-ip_offset)]} {

        set ip_offset $param(-ip_offset)
    } else {

        set ip_offset 0
    }

    #Time to live
    if {[info exists param(-ip_ttl)]} {

        set ip_ttl $param(-ip_ttl)
    } else {

        set ip_ttl 64
    }

    #Error
    if {[info exists param(-ip_checksum)]} {

        set ip_checksum $param(-ip_checksum)
    } else {

        set ip_checksum "auto"
    }

    #Protocol in the data portion of the
    if {[info exists param(-ip_protocol)]} {

        set ip_protocol $param(-ip_protocol)
    } else {

        set ip_protocol 17
    }

    #IPv4 address of the sender of the packet
    if {[info exists param(-src_ip)]} {

        set src_ip $param(-src_ip)
    } else {

        set src_ip 0.0.0.0
    }

    #IPv4 address of the reciever of the packet
    if {[info exists param(-dest_ip)]} {

        set dest_ip $param(-dest_ip)
    } else {

        set dest_ip 0.0.0.0
    }

    #Sender
    if {[info exists param(-src_port)]} {

        set src_port $param(-src_port)
    } else {

        set src_port 319
    }

    #Receiver
    if {[info exists param(-dest_port)]} {

        set dest_port $param(-dest_port)
    } else {

        set dest_port 319
    }

    #Length in bytes of UDP header and data
    if {[info exists param(-udp_length)]} {

        set udp_length $param(-udp_length)
    } else {

        set udp_length 52
    }

    #Error
    if {[info exists param(-udp_checksum)]} {

        set udp_checksum $param(-udp_checksum)
    } else {

        set udp_checksum auto
    }

    #Major sdo id
    if {[info exists param(-major_sdo_id)]} {

        set major_sdo_id $param(-major_sdo_id)
    } else {

        set major_sdo_id 0
    }

    #Message Type
    if {[info exists param(-message_type)]} {

        set message_type $param(-message_type)
    } else {

        set message_type 0
    }

    #PTP version number
    if {[info exists param(-minor_version)]} {

        set minor_version $param(-minor_version)
    } else {

        set minor_version 2
    }

    #PTP minor version number
    if {[info exists param(-version_ptp)]} {

        set version_ptp $param(-version_ptp)
    } else {

        set version_ptp 2
    }

    #PTP Sync message length
    if {[info exists param(-message_length)]} {

        set message_length $param(-message_length)
    } else {

        set message_length 44
    }

    #Domain number
    if {[info exists param(-domain_number)]} {

        set domain_number $param(-domain_number)
    } else {

        set domain_number 0
    }

    #Minor Sdo id
    if {[info exists param(-minor_sdo_id)]} {

        set minor_sdo_id $param(-minor_sdo_id)
    } else {

        set minor_sdo_id 0
    }

    #Alternate Master flag
    if {[info exists param(-alternate_master_flag)]} {

        set alternate_master_flag $param(-alternate_master_flag)
    } else {

        set alternate_master_flag 0
    }

    #Two Step Flag
    if {[info exists param(-two_step_flag)]} {

        set two_step_flag $param(-two_step_flag)
    } else {

        set two_step_flag 0
    }

    #Unicast Flag
    if {[info exists param(-unicast_flag)]} {

        set unicast_flag $param(-unicast_flag)
    } else {

        set unicast_flag 0
    }

    #Profile specific1 Flag
    if {[info exists param(-profile_specific1)]} {

        set profile_specific1 $param(-profile_specific1)
    } else {

        set profile_specific1 0
    }

    #Profile specific2 Flag
    if {[info exists param(-profile_specific2)]} {

        set profile_specific2 $param(-profile_specific2)
    } else {

        set profile_specific2 0
    }

    #Secure Flag
    if {[info exists param(-secure)]} {

        set secure $param(-secure)
    } else {

        set secure 0
    }

    #Leap61 Flag
    if {[info exists param(-leap61)]} {

        set leap61 $param(-leap61)
    } else {

        set leap61 0
    }

    #Leap59 Flag
    if {[info exists param(-leap59)]} {

        set leap59 $param(-leap59)
    } else {

        set leap59 0
    }

    #Current UTC Offset Flag
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
    } else {

        set current_utc_offset_valid 0
    }

    #PTP Timescale Flag
    if {[info exists param(-ptp_timescale)]} {

        set ptp_timescale $param(-ptp_timescale)
    } else {

        set ptp_timescale 0
    }

    #Time Traceable Flag
    if {[info exists param(-time_traceable)]} {

        set time_traceable $param(-time_traceable)
    } else {

        set time_traceable 0
    }

    #Frequency Traceable Flag
    if {[info exists param(-freq_traceable)]} {

        set freq_traceable $param(-freq_traceable)
    } else {

        set freq_traceable 0
    }

    #Synchronization uncertain Flag
    if {[info exists param(-sync_uncertain)]} {

        set sync_uncertain $param(-sync_uncertain)
    } else {

        set sync_uncertain 0
    }

    #Correction Field value
    if {[info exists param(-correction_field)]} {

        set correction_field $param(-correction_field)
    } else {

        set correction_field 0
    }

    #Message type
    if {[info exists param(-message_type_specific)]} {

        set message_type_specific $param(-message_type_specific)
    } else {

        set message_type_specific 0
    }

    #Source port number
    if {[info exists param(-src_port_number)]} {

        set src_port_number $param(-src_port_number)
    } else {

        set src_port_number 0
    }

    #Source Clock identity
    if {[info exists param(-src_clock_identity)]} {

        set src_clock_identity $param(-src_clock_identity)
    } else {

        set src_clock_identity 0
    }

    #Sequence ID
    if {[info exists param(-sequence_id)]} {

        set sequence_id $param(-sequence_id)
    } else {

        set sequence_id 0
    }

    #Control field
    if {[info exists param(-control_field)]} {

        set control_field $param(-control_field)
    } else {

        set control_field 0
    }

    #Log message interval
    if {[info exists param(-log_message_interval)]} {

        set log_message_interval $param(-log_message_interval)
    } else {

        set log_message_interval 127
    }

    #Origin timestamp in seconds
    if {[info exists param(-origin_timestamp_sec)]} {

        set origin_timestamp_sec $param(-origin_timestamp_sec)
    } else {

        set origin_timestamp_sec 0
    }

    #Origin timestamp in nanoseconds
    if {[info exists param(-origin_timestamp_ns)]} {

        set origin_timestamp_ns $param(-origin_timestamp_ns)
    } else {

        set origin_timestamp_ns 0
    }

    #Current UTC Offset
    if {[info exists param(-current_utc_offset_valid)]} {

        set current_utc_offset_valid $param(-current_utc_offset_valid)
    } else {

        set current_utc_offset_valid 0
    }

    #Grand Master Priority 1
    if {[info exists param(-gm_priority1)]} {

        set gm_priority1 $param(-gm_priority1)
    } else {

        set gm_priority1 0
    }

    #Grand Master Priority 2
    if {[info exists param(-gm_priority2)]} {

        set gm_priority2 $param(-gm_priority2)
    } else {

        set gm_priority2 128
    }

    #Grand Master Identity
    if {[info exists param(-gm_identity)]} {

        set gm_identity $param(-gm_identity)
    } else {

        set gm_identity 0
    }

    #Steps Removed
    if {[info exists param(-steps_removed)]} {

        set steps_removed $param(-steps_removed)
    } else {

        set steps_removed 0
    }

    #Time Source
    if {[info exists param(-time_source)]} {

        set time_source $param(-time_source)
    } else {

        set time_source 0
    }

    #Grand Master Clock classy
    if {[info exists param(-gm_clock_classy)]} {

        set gm_clock_classy $param(-gm_clock_classy)
    } else {

        set gm_clock_classy 0
    }

    #Grand Master Clock accuracy
    if {[info exists param(-gm_clock_accuracy)]} {

        set gm_clock_accuracy $param(-gm_clock_accuracy)
    } else {

        set gm_clock_accuracy 0
    }

    #Grand Master Clock offset scaled
    if {[info exists param(-gm_clock_variance)]} {

        set gm_clock_variance $param(-gm_clock_variance)
    } else {

        set gm_clock_variance 0
    }

    #Precise Origin Timestamp
    if {[info exists param(-precise_origin_timestamp_sec)]} {

        set precise_origin_timestamp_sec $param(-precise_origin_timestamp_sec)
    } else {

        set precise_origin_timestamp_sec 0
    }

    #Precise Origin Timestamp
    if {[info exists param(-precise_origin_timestamp_ns)]} {

        set precise_origin_timestamp_ns $param(-precise_origin_timestamp_ns)
    } else {

        set precise_origin_timestamp_ns 0
    }

    #Receive Timestamp
    if {[info exists param(-receive_timestamp_sec)]} {

        set receive_timestamp_sec $param(-receive_timestamp_sec)
    } else {

        set receive_timestamp_sec 0
    }

    #Receive Timestamp
    if {[info exists param(-receive_timestamp_ns)]} {

        set receive_timestamp_ns $param(-receive_timestamp_ns)
    } else {

        set receive_timestamp_ns 0
    }

    #Requesting Port Number
    if {[info exists param(-requesting_port_number)]} {

        set requesting_port_number $param(-requesting_port_number)
    } else {

        set requesting_port_number 0
    }

    #Requesting Clock Identity
    if {[info exists param(-requesting_clock_identity)]} {

        set requesting_clock_identity $param(-requesting_clock_identity)
    } else {

        set requesting_clock_identity 0
    }

    #Request Receipt Timestamp
    if {[info exists param(-request_receipt_timestamp_sec)]} {

        set request_receipt_timestamp_sec $param(-request_receipt_timestamp_sec)
    } else {

        set request_receipt_timestamp_sec 0
    }

    #Request Receipt Timestamp
    if {[info exists param(-request_receipt_timestamp_ns)]} {

        set request_receipt_timestamp_ns $param(-request_receipt_timestamp_ns)
    } else {

        set request_receipt_timestamp_ns 0
    }

    #Response Origin Timestamp
    if {[info exists param(-response_origin_timestamp_sec)]} {

        set response_origin_timestamp_sec $param(-response_origin_timestamp_sec)
    } else {

        set response_origin_timestamp_sec 0
    }

    #Response Origin Timestamp
    if {[info exists param(-response_origin_timestamp_ns)]} {

        set response_origin_timestamp_ns $param(-response_origin_timestamp_ns)
    } else {

        set response_origin_timestamp_ns 0
    }

    #Target Port number
    if {[info exists param(-target_port_number)]} {

        set target_port_number $param(-target_port_number)
    } else {

        set target_port_number 0
    }

    #Target clock identity
    if {[info exists param(-target_clock_identity)]} {

        set target_clock_identity $param(-target_clock_identity)
    } else {

        set target_clock_identity 0
    }

    #Starting Boundary Hops
    if {[info exists param(-starting_boundary_hops)]} {

        set starting_boundary_hops $param(-starting_boundary_hops)
    } else {

        set starting_boundary_hops 0
    }

    #Boundary Hops
    if {[info exists param(-boundary_hops)]} {

        set boundary_hops $param(-boundary_hops)
    } else {

        set boundary_hops 0
    }

    #Action Field
    if {[info exists param(-action_field)]} {

        set action_field $param(-action_field)
    } else {

        set action_field 0
    }

    #TLV
    if {[info exists param(-tlv)]} {

        set tlv $param(-tlv)
    } else {

        set tlv NONE
    }

    #TLV Type
    if {[info exists param(-tlv_type)]} {

        set tlv_type $param(-tlv_type)
    } else {

        set tlv_type 32769
    }

    #TLV length
    if {[info exists param(-tlv_length)]} {

        set tlv_length $param(-tlv_length)
    } else {

        set tlv_length 30
    }

    #OPE Flag
    if {[info exists param(-ope)]} {

        set ope $param(-ope)
    } else {

        set ope 0
    }

    #CR Flag
    if {[info exists param(-cr)]} {

        set cr $param(-cr)
    } else {

        set cr 0
    }

    #RCR Flag
    if {[info exists param(-rcr)]} {

        set rcr $param(-rcr)
    } else {

        set rcr 0
    }

    #TCR Flag
    if {[info exists param(-tcr)]} {

        set tcr $param(-tcr)
    } else {

        set tcr 0
    }

    #IC Flag
    if {[info exists param(-ic)]} {

        set ic $param(-ic)
    } else {

        set ic 0
    }

    #IRC Flag
    if {[info exists param(-irc)]} {

        set irc $param(-irc)
    } else {

        set irc 0
    }

    #ITC Flag
    if {[info exists param(-itc)]} {

        set itc $param(-itc)
    } else {

        set itc 0
    }

    #FOV Flag
    if {[info exists param(-fov)]} {

        set fov $param(-fov)
    } else {

        set fov 0
    }

    #POV Flag
    if {[info exists param(-pov)]} {

        set pov $param(-pov)
    } else {

        set pov 0
    }

    #TCT Flag
    if {[info exists param(-tct)]} {

        set tct $param(-tct)
    } else {

        set tct 0
    }

    #Phase offset tx
    if {[info exists param(-phase_offset_tx)]} {

        set phase_offset_tx $param(-phase_offset_tx)
    } else {

        set phase_offset_tx 0
    }

    #Phase offset tx timestamp Sec
    if {[info exists param(-phase_offset_tx_timestamp_sec)]} {

        set phase_offset_tx_timestamp_sec $param(-phase_offset_tx_timestamp_sec)
    } else {

        set phase_offset_tx_timestamp_sec 0
    }

    #Phase offset tx timsstamp Ns
    if {[info exists param(-phase_offset_tx_timestamp_ns)]} {

        set phase_offset_tx_timestamp_ns $param(-phase_offset_tx_timestamp_ns)
    } else {

        set phase_offset_tx_timestamp_ns 0
    }

    #Frequency offset tx
    if {[info exists param(-freq_offset_tx)]} {

        set freq_offset_tx $param(-freq_offset_tx)
    } else {

        set freq_offset_tx 0
    }

    #Frequency offset tx timestamp
    if {[info exists param(-freq_offset_tx_timestamp_sec)]} {

        set freq_offset_tx_timestamp_sec $param(-freq_offset_tx_timestamp_sec)
    } else {

        set freq_offset_tx_timestamp_sec 0
    }

    #Frequency offset tx timstamp
    if {[info exists param(-freq_offset_tx_timestamp_ns)]} {

        set freq_offset_tx_timestamp_ns $param(-freq_offset_tx_timestamp_ns)
    } else {

        set freq_offset_tx_timestamp_ns 0
    }

    #Management ID
    if {[info exists param(-management_id)]} {

        set management_id $param(-management_id)
    } else {
        #EPC
        set management_id [expr 0x3000]
    }

    #External Port Configuration
    if {[info exists param(-ext_port_config)]} {

        set ext_port_config $param(-ext_port_config)
    } else {

        set ext_port_config 0
    }

    #Master only
    if {[info exists param(-master_only)]} {

        set master_only $param(-master_only)
    } else {

        set master_only 0
    }

    #OUTPUT PARAMETERS

    #PTP HA packet hex dump
    if {[info exists param(-hexdump)]} {

        upvar $param(-hexdump) hexdump
        set hexdump ""
    }


    #LOGIC GOES HERE
    set packet ""

    if {(($src_ip == "0.0.0.0") && ($dest_ip == "0.0.0.0")) || $src_ip == "" || $dest_ip == ""} {
        set transport_type "Ethernet"
    } else {
        set transport_type "IPv4/UDP"
        set eth_type 0800
    }

    #PTP Message Header
    switch $message_type {
        0 {
            #Sync
            pbLib::encode_ptp_ha_sync_header\
                                         -origin_timestamp_sec       $origin_timestamp_sec\
                                         -origin_timestamp_ns        $origin_timestamp_ns\
                                         -hexdump                    header
        }
        1 {
            #Delay Request
            pbLib::encode_ptp_ha_delay_req_header\
                                         -origin_timestamp_sec       $origin_timestamp_sec\
                                         -origin_timestamp_ns        $origin_timestamp_ns\
                                         -hexdump                    header
        }
        2 {
            #Peer Delay Request
            pbLib::encode_ptp_ha_pdelay_req_header\
                                         -origin_timestamp_sec       $origin_timestamp_sec\
                                         -origin_timestamp_ns        $origin_timestamp_ns\
                                         -hexdump                    header
        }
        3 {
            #Peer Delay Response
            pbLib::encode_ptp_ha_pdelay_resp_header\
                                         -request_receipt_timestamp_sec  $request_receipt_timestamp_sec\
                                         -request_receipt_timestamp_ns   $request_receipt_timestamp_ns\
                                         -requesting_port_number         $requesting_port_number\
                                         -requesting_clock_identity      $requesting_clock_identity\
                                         -hexdump                        header
        }
        8 {
            #Follow up
            pbLib::encode_ptp_ha_followup_header\
                                         -precise_origin_timestamp_sec  $precise_origin_timestamp_sec\
                                         -precise_origin_timestamp_ns   $precise_origin_timestamp_ns\
                                         -hexdump                       header
        }
        9 {
            #Delay Response
            pbLib::encode_ptp_ha_delay_resp_header\
                                         -receive_timestamp_sec        $receive_timestamp_sec\
                                         -receive_timestamp_ns         $receive_timestamp_ns\
                                         -requesting_port_number       $requesting_port_number\
                                         -requesting_clock_identity    $requesting_clock_identity\
                                         -hexdump                      header
        }
       10 {
            #Peer Delay Response Follow up
            pbLib::encode_ptp_ha_pdelay_resp_followup_header\
                                         -response_origin_timestamp_sec $response_origin_timestamp_sec\
                                         -response_origin_timestamp_ns  $response_origin_timestamp_ns\
                                         -requesting_port_number        $requesting_port_number\
                                         -requesting_clock_identity     $requesting_clock_identity\
                                         -hexdump                       header
        }
       11 {
            #Announce
            pbLib::encode_ptp_ha_announce_header\
                                         -origin_timestamp_sec       $origin_timestamp_sec\
                                         -origin_timestamp_ns        $origin_timestamp_ns\
                                         -current_utc_offset_valid   $current_utc_offset_valid\
                                         -gm_priority1               $gm_priority1\
                                         -gm_priority2               $gm_priority2\
                                         -gm_identity                $gm_identity\
                                         -steps_removed              $steps_removed\
                                         -time_source                $time_source\
                                         -gm_clock_classy            $gm_clock_classy\
                                         -gm_clock_accuracy          $gm_clock_accuracy\
                                         -gm_clock_variance          $gm_clock_variance\
                                         -hexdump                    header
        }
       12 {
            #Signalling
            pbLib::encode_ptp_ha_signal_header\
                                         -target_port_number                $target_port_number\
                                         -target_clock_identity             $target_clock_identity\
                                         -tlv                               $tlv\
                                         -tlv_type                          $tlv_type\
                                         -tlv_length                        $tlv_length\
                                         -ope                               $ope\
                                         -cr                                $cr\
                                         -rcr                               $rcr\
                                         -tcr                               $tcr\
                                         -ic                                $ic\
                                         -irc                               $irc\
                                         -itc                               $itc\
                                         -fov                               $fov\
                                         -pov                               $pov\
                                         -tct                               $tct\
                                         -phase_offset_tx                   $phase_offset_tx\
                                         -phase_offset_tx_timestamp_sec     $phase_offset_tx_timestamp_sec\
                                         -phase_offset_tx_timestamp_ns      $phase_offset_tx_timestamp_ns\
                                         -freq_offset_tx                    $freq_offset_tx\
                                         -freq_offset_tx_timestamp_sec      $freq_offset_tx_timestamp_sec\
                                         -freq_offset_tx_timestamp_ns       $freq_offset_tx_timestamp_ns\
                                         -hexdump                           header
        }
       13 {
            #Management
            pbLib::encode_ptp_ha_mgmt_header\
                                      -target_port_number         $target_port_number\
                                      -target_clock_identity      $target_clock_identity\
                                      -starting_boundary_hops     $starting_boundary_hops\
                                      -boundary_hops              $boundary_hops\
                                      -action_field               $action_field\
                                      -management_id              $management_id\
                                      -ext_port_config            $ext_port_config\
                                      -master_only                $master_only\
                                      -hexdump                    header
        }
       default {
            #reserved
            set header ""
        }
    }

    set packet $header

    set message_length [size_of $header 34]

    #ENCODE PTP HEADER
    pbLib::encode_ptp_ha_common_header\
                -major_sdo_id                      $major_sdo_id\
                -message_type                      $message_type\
                -minor_version                     $minor_version\
                -version_ptp                       $version_ptp\
                -message_length                    $message_length\
                -domain_number                     $domain_number\
                -minor_sdo_id                      $minor_sdo_id\
                -correction_field                  $correction_field\
                -message_type_specific             $message_type_specific\
                -sequence_id                       $sequence_id\
                -control_field                     $control_field\
                -log_message_interval              $log_message_interval\
                -src_clock_identity                $src_clock_identity\
                -src_port_number                   $src_port_number\
                -alternate_master_flag             $alternate_master_flag\
                -two_step_flag                     $two_step_flag\
                -unicast_flag                      $unicast_flag\
                -profile_specific1                 $profile_specific1\
                -profile_specific2                 $profile_specific2\
                -secure                            $secure\
                -leap61                            $leap61\
                -leap59                            $leap59\
                -current_utc_offset_valid          $current_utc_offset_valid\
                -ptp_timescale                     $ptp_timescale\
                -time_traceable                    $time_traceable\
                -freq_traceable                    $freq_traceable\
                -sync_uncertain                    $sync_uncertain\
                -hexdump                           ptp_ha_common_header

    prepend packet $ptp_ha_common_header

    if {$transport_type == "IPv4/UDP"} {

        set udp_length [expr $message_length + 8]

        set ip_pseudo_header ""
        append ip_pseudo_header [dec2hex $ip_protocol 4]
        append ip_pseudo_header [ip2hex $src_ip]
        append ip_pseudo_header [ip2hex $dest_ip]

        #ENCODE UDP HEADER
        pbLib::encode_udp_header\
                    -src_port        $src_port\
                    -dest_port       $dest_port\
                    -payload_length  $udp_length\
                    -checksum        $udp_checksum\
                    -pseudo_header   $ip_pseudo_header\
                    -data            $packet\
                    -hexdump         udp_header

        prepend packet $udp_header
        set ip_total_length [expr $udp_length + $ip_header_length]

        #ENCODE IPv4 HEADER
        pbLib::encode_ipv4_header\
                    -version         $ip_version\
                    -header_length   $ip_header_length\
                    -tos             $ip_tos\
                    -total_length    $ip_total_length\
                    -identification  $ip_identification\
                    -flags           $ip_flags\
                    -ttl             $ip_ttl\
                    -offset          $ip_offset\
                    -next_protocol   $ip_protocol\
                    -checksum        $ip_checksum\
                    -src_ip          $src_ip\
                    -dest_ip         $dest_ip\
                    -hexdump         ipv4_header

        prepend packet $ipv4_header
    }

    #ENCODE ETHERNET HEADER
    pbLib::encode_eth_header\
                -dest_mac    $dest_mac\
                -src_mac     $src_mac\
                -eth_type    $eth_type\
                -hexdump     eth_header

    prepend packet $eth_header

    if {[string is integer $vlan_id] && ($vlan_id >= 0) && ($vlan_id <= 4095) } {
        set packet [pbLib::push_vlan_tag\
                        -packet          $packet\
                        -vlan_id         $vlan_id\
                        -priority        $vlan_priority\
                        -dei             $vlan_dei]
    }

    set hexdump $packet
    return 1

}


################################################################################
#  PROCEDURE NAME       : pbLib::encode_port_identity                          #
#                                                                              #
#  DEFINITION           : This function encodes port identity.                 #
#                                                                              #
#  INPUT PARAMETERS     :                                                      #
#                                                                              #
#  clock_identity       : (Mandatory) Clock Identity                           #
#                                                                              #
#  port_number          : (Mandatory) Port Number                              #
#                                                                              #
#  OUTPUT PARAMETERS    : NONE                                                 #
#                                                                              #
#  RETURNS              : Port identity hex                                    #
#                                                                              #
#  USAGE                :                                                      #
#                                                                              #
#     pbLib::encode_port_identity $clock_identity $port_number                 #
#                                                                              #
################################################################################

proc pbLib::encode_port_identity {clock_identity port_number} {

    if {($clock_identity == "") || ($port_number == "")} {
        return ""
    }

    ptp_ha_port_identity port_identity

    set port_identity::clockIdentity   [dec2hex $clock_identity 16]
    set port_identity::portNumber      [dec2hex $port_number 4]

    return [port_identity::pkt]

}
