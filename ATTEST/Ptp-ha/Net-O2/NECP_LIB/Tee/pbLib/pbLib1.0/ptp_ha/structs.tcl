#ETHERNET
struct ethernet {
    
    dmac    6
    smac    6
    type    2
    
}


#VLAN
struct vlan {
    
    priority    :3
    cfi         :1
    vlan        :12
    typelen     2
}


#IP
struct ip {
    
    version     :4
    hdrlen      :4
    tos         1
    len         2
    id          2
    flags       :3
    offset      :13
    ttl         1
    proto       1
    checksum    2
    srcip       4
    dstip       4
    
}


#ARP
struct arp {
    
    hw_type     2
    proto_type  2
    hw_size     1
    proto_size  1
    opcode      2
    src_mac     6
    src_ip      4
    dest_mac    6
    dest_ip     4
    
}


#UDP
struct udp {
    
    srcport    2
    dstport    2
    length     2
    checksum   2
}


#PTP HA - Timestamp [10]
struct ptp_ha_timestamp {

    secondsField        6
    nanosecondsField    4

}


#PTP HA - Port Identity [10]
struct ptp_ha_port_identity {

    clockIdentity   8
    portNumber      2

}


#PTP HA - Clock Quality [4]
struct ptp_ha_clock_quality {

    clockClass              1
    clockAccuracy           1
    offsetScaledLogVariance 2

}


#PTP High Accuracy - Common [34]
struct ptp_ha_common {

    majorSdoId          :4
    messageType         :4
    minorVersionPTP     :4
    versionPTP          :4
    messageLength       2
    domainNumber        1
    minorSdoId          1
    flagField           2
    correctionField     8
    messageTypeSpecific 4
    sourcePortIdentity  10
    sequenceId          2
    controlField        1
    logMessageInterval  1

}


#PTP HA - flags [2]
struct ptp_ha_flags {

    secure                      :1
    profileSpecific2            :1
    profileSpecific1            :1
    reserved                    :2
    unicastFlag                 :1
    twoStepFlag                 :1
    alternateMasterFlag         :1

    reserved2                   :1
    synchronizationUncertain    :1
    frequencyTraceable          :1
    timeTraceable               :1
    ptpTimescale                :1
    currentUtcOffsetValid       :1
    leap59                      :1
    leap61                      :1

}


#PTP HA - Announce message [30]
struct ptp_ha_announce {
    
    originTimestamp         10
    currentUtcOffset        2
    reserved                1
    grandmasterPriority1    1
    grandmasterClockQuality 4
    grandmasterPriority2    1
    grandmasterIdentity     8
    stepsRemoved            2
    timeSource              1
    
}


#PTP HA - Sync message [10] 
struct ptp_ha_sync {

    originTimestamp         10

}


#PTP HA - Delay_Req messages [10]
struct ptp_ha_delay_req {
    
    originTimestamp         10

}


#PTP HA - Follow_Up message [10]
struct ptp_ha_followup {

    preciseOriginTimestamp  10

}


#PTP HA - Delay_Resp message [20]
struct ptp_ha_delay_resp {

    receiveTimestamp        10
    requestingPortIdentity  10

}


#PTP HA - Pdelay_Req message [20]
struct ptp_ha_pdelay_req {
    
    originTimestamp         10
    reserved                10
    
}


#PTP HA - Pdelay_Resp message [20]
struct ptp_ha_pdelay_resp {

    requestReceiptTimestamp 10
    requestingPortIdentity  10

}


#PTP HA - Pdelay_Resp_Follow_Up message [20]
struct ptp_ha_pdelay_resp_followup {

    responseOriginTimestamp 10
    requestingPortIdentity  10

}


#PTP HA - Signaling message [+TLVs] [10]
struct ptp_ha_signal {

    targetPortIdentity      10

}


#PTP HA - Management message [+managementTLV] [14]
struct ptp_ha_mgmt {

    targetPortIdentity      10
    startingBoundaryHops    1
    boundaryHops            1
    reserved1               :4
    actionField             :4
    reserved2               1

}


#PTP HA - Management TLV [+dataField] [6 + M]
struct ptp_ha_mgmt_tlv {

    tlvType         2
    lengthField     2
    managementId    2

}


#PTP HA - TLV [+valueField]  [4 + N]
struct ptp_ha_signal_tlv {

    tlvType         2
    lengthField     2

}

#PTP HA - L1_SYNC TLV format [2]
struct ptp_ha_l1_sync_tlv {

    reserved1               :4
    OPE                     :1
    CR                      :1
    RCR                     :1
    TCR                     :1
    reserved2               :5
    IC                      :1
    IRC                     :1
    ITC                     :1

}


#PTP HA - L1_SYNC TLV extended format [40]
struct ptp_ha_l1_sync_tlv_ext {

    reserved1               :4
    OPE                     :1
    CR                      :1
    RCR                     :1
    TCR                     :1
    reserved2               :5
    IC                      :1
    IRC                     :1
    ITC                     :1
    reserved3               :5
    FOV                     :1
    POV                     :1
    TCT                     :1
    phaseOffsetTx           8
    phaseOffsetTxTimestamp  10
    freqOffsetTx            8
    freqOffsetTxTimestamp   10
    reserved4               1

}


#PTP HA - EXTERNAL_PORT_CONFIGURATION_ENABLED management TLV [2]
struct ptp_ha_ext_port_conf_enabled_tlv {

    reserved1               :7
    EPC                     :1
    reserved2               1

}


#PTP HA - MASTER_ONLY management TLV [2]
struct ptp_ha_master_only_tlv {

    reserved1               :7
    MO                      :1
    reserved2               1

}
