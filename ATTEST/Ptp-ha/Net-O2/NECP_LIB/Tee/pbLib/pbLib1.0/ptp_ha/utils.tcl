################################################################################
# File Name           :     utils.tcl                                          #
# File Version        :     1.6                                                #
# Component Name      :     UTILITIES                                          #
# Module Name         :     UTILS API                                          #
################################################################################
# History      Date     Author     Addition/ Alteration                        #
#                                                                              #
#  1.0       Apr/2018   CERN       Initial                                     #
#  1.1       Jun/2018   CERN       a) Change MAC address to same case          #
#                                  b) Fixed error in correction_field          #
#                                  c) Fixed error in current_offset_valid flag #
#  1.2       Sep/2018   CERN       a) Enhancements to support error_code while #
#                                     receiving ptp_ha packet                  #
#                                  b) Improved log print for checking message  #
#                                     format                                   #
#                                  c) Added procedure to get destination MAC   #
#                                     and IP for PTP messages in different     #
#                                     modes                                    #
#  1.3       Oct/2018   CERN       Added TLV name for TLVs in log.             #
#  1.4       Nov/2018   CERN       a) sequenceId in PAG group is made fixed.   #
#                                  b) Updated validation of below fields in    #
#                                     check_ptp_ha_pkt                         #
#                                     1) phase_offset_tx_timestamp_sec         #
#                                     2) phase_offset_tx_timestamp_ns          #
#                                     3) freq_offset_tx_timestamp_sec          #
#                                     4) freq_offset_tx_timestamp_ns           #
#  1.5       Dec/2018   CERN       a) Updated timestamp for Pdelay_Resp and    #
#                                     Pdelay_Resp_Follow_Up messages.          #
#                                  b) Added support for incrementing sequenceId#
#                                     in Sync and Follow_Up messages.          #
#  1.6       Jan/2019   CERN       Updated default value for correctionField.  #
#                                                                              #
################################################################################
# Copyright (c) 2018 - 2019 CERN                                               #
################################################################################

################################################################################
#  PROCEDURE NAME    : dec2hex                                                 #
#                                                                              #
#  DEFINITION        : This function is used to convert  decimal to hex.       #
#                                                                              #
#  INPUT             : 15                                                      #
#                                                                              #
#  OUTPUT            : e.g 0F                                                  #
#                                                                              #
#  USAGE             :                                                         #
#                       dec2hex $decimal                                       #
#                                                                              #
################################################################################

proc dec2hex {decimal {digit 1}} {

    if {$decimal == ""} {
        return ""
    }

    if {$decimal < 0} {
        return -code error "dec2hex: invalid integer, $decimal"
    }

    #input is already hex
    if {[regexp {0x([0-9aA-fF]+)} $decimal match hex]} {
        return $hex
    }
 
    set num [expr wide($decimal)]
    set res {}
    set hex_list {0 1 2 3 4 5 6 7 8 9 a b c d e f}

    while {$num/16 != 0} {
      set rest [expr {$num%16}]
      set res [lindex $hex_list $rest]$res
      set num [expr {$num/16}]
    }
    set res [lindex $hex_list $num]$res

    #prepend zeroes
    set len [string length $res]
    set diff [expr $digit-$len]

    set res [string repeat 0 $diff]$res

    return $res

}

################################################################################
#  PROCEDURE NAME    : mac2hex                                                 #
#                                                                              #
#  DEFINITION        : This function is used to convert MAC to Hex.            #
#                                                                              #
#  INPUT             : 00:11:11:11:11:11                                       #
#                                                                              #
#  OUTPUT            : e.g 001111111111                                        #
#                                                                              #
#  USAGE             :                                                         #
#                       mac2hex $mac_address                                   #
#                                                                              #
################################################################################

proc mac2hex { mac_address } {
    
    set hex  [join [split $mac_address ":"] ""]
    
    return $hex
    
}

################################################################################
#  PROCEDURE NAME    : ip2hex                                                  #
#                                                                              #
#  DEFINITION        : This function is used to convert IP to Hex.             #
#                                                                              #
#  INPUT             : 1.1.1.1                                                 #
#                                                                              #
#  OUTPUT            : e.g 01010101                                            #
#                                                                              #
#  USAGE             :                                                         #
#                       ip2hex $ip_address                                     #
#                                                                              #
################################################################################

proc ip2hex { ip_address } {
    
    set ip1 [split $ip_address "\."]
    
    set FirstHex  [dec2hex [lindex $ip1 0] 2]
    
    set SecondHex [dec2hex [lindex $ip1 1] 2]
    
    set ThirdHex  [dec2hex [lindex $ip1 2] 2]
    
    set FourthHex [dec2hex [lindex $ip1 3] 2]
    
    return "${FirstHex}${SecondHex}${ThirdHex}${FourthHex}"
    
}

################################################################################
#  PROCEDURE NAME    : mac_address_format                                      #
#                                                                              #
#  DEFINITION        : This function formats the input into mac format         #
#                                                                              #
#  INPUT             : aabbccddeeff                                            #
#                                                                              #
#  OUTPUT            : e.g AA:BB:CC:DD:EE:FF                                   #
#                                                                              #
#  USAGE             :                                                         #
#                        mac_address_format $mac_address                       #
#                                                                              #
################################################################################

proc mac_address_format {mac_address} {
    
    set mac_address [string toupper $mac_address]
    
    scan $mac_address "%2s%2s%2s%2s%2s%2s" a b c d e f
    
    set mac_address "$a $b $c $d $e $f"
    
    set result_mac_address [string map { " " ":" } $mac_address]
    
    return $result_mac_address
    
}

################################################################################
#  PROCEDURE NAME    : ip_address_format                                       #
#                                                                              #
#  DEFINITION        : This function formats the input into ip format          #
#                                                                              #
#  INPUT             : 01010101                                                #
#                                                                              #
#  OUTPUT            : e.g 1.1.1.1                                             #
#                                                                              #
#  USAGE             :                                                         #
#                      ip_address_format $ip_address                           #
#                                                                              #
################################################################################

proc ip_address_format { hexval } {
    
    set ip_address [string toupper $hexval]
    
    scan $ip_address "%2s%2s%2s%2s" a b c d
    
    set a [hex2dec $a]
    set b [hex2dec $b]
    set c [hex2dec $c]
    set d [hex2dec $d]
    
    set ip_address "$a $b $c $d"
    
    set result_ip_address [string map { " " "." } $ip_address]
    
    return $result_ip_address
    
}

################################################################################
#  PROCEDURE NAME    : hex2mac                                                 #
#                                                                              #
#  DEFINITION        : This function is used to convert Hex to MAC.            #
#                                                                              #
#  INPUT             : 001111111111                                            #
#                                                                              #
#  OUTPUT            : e.g 00:11:11:11:11:11                                   #
#                                                                              #
#  USAGE             :                                                         #
#                       hex2mac $mac                                           #
#                                                                              #
################################################################################

proc hex2mac {mac} {
    
    regsub -all {[0-9aA-fF]{2}} $mac "& " output
    
    return [ join $output : ]
    
}


################################################################################
#  PROCEDURE NAME    : hex2bin                                                 #
#                                                                              #
#  DEFINITION        : This function is used to convert Hex to bin             #
#                                                                              #
#  INPUT             : e.g AA                                                  #
#                                                                              #
#  OUTPUT            : e.g 1010 1010                                           #
#                                                                              #
#  USAGE             :                                                         #
#                       hex2bin $hex                                           #
#                                                                              #
################################################################################

proc hex2bin {args} {
    
    set hex [ string tolower $args ]
    
    regsub -all {[0-9a-f]{2}} $hex "& " hex
    
    set binary ""
    
    foreach hexval $hex {
        
        binary scan [binary format H* [ string map {{ } 0} [ format %2s $hexval] ] ] B* bin
        
        append binary $bin
        
    }
    
    return $binary
    
}

################################################################################
#  PROCEDURE NAME    : calculate_checksum                                      #
#                                                                              #
#  DEFINITION        : This function is used to calculate checksum             #
#                                                                              #
#  INPUT             : hex                                                     #
#                                                                              #
#  OUTPUT            : checksum value                                          #
#                                                                              #
#  USAGE             :                                                         #
#                       calculate_check $hex                                   #
#                                                                              #
################################################################################

proc calculate_checksum {hex} {
    
    #CheckSum Calculation:
    
    #includes: version, header length, dscp, total length, id, flags, fragment offset, ttl, protocol, checksum(as 0000), src & dst IP
    
    set str [binary format H* $hex]
    
    # compute the crc of bytes found in 'str'
    
    set crc 0
    
    binary scan $str S* data
    
    foreach part $data {
        
        set crc [expr {$crc + (($part + 0x10000) % 0x10000)}]
        
    }
    
    set crc [expr {~(($crc >> 16) + ($crc & 0xFFFF)) & 0xFFFF}]
    
    return [format %04x $crc]
    
}


################################################################################
#  PROCEDURE NAME    : hex2dec                                                 #
#                                                                              #
#  DEFINITION        : This function is used to convert hex to decimal.        #
#                                                                              #
#  INPUT             : e.g 0F                                                  #
#                                                                              #
#  OUTPUT            : e.g 15                                                  #
#                                                                              #
#  USAGE             :                                                         #
#                       hex2dec $hex                                           #
#                                                                              #
################################################################################

proc hex2dec {largeHex} {
    
    set res [expr wide(0)]
    foreach hexDigit [split $largeHex {}] {
        
        set new 0x$hexDigit
        set res [expr {(16*$res) + $new} ]
        
    }
    
    return $res
}


################################################################################
#  PROCEDURE NAME    : is_ipv4_address                                         #
#                                                                              #
#  DEFINITION        : This function is used to find whether the given hex is  #
#                      ipv4 address or not.                                    #
#                                                                              #
#  INPUT             : e.g C0A80A2D                                            #
#                                                                              #
#  OUTPUT            : returns 1 - if hex is an ip address                     #
#                      returns 0 - if hex is not an ip address                 #
#                                                                              #
#  USAGE             :                                                         #
#                       is_ipv4_address $hex                                   #
#                                                                              #
################################################################################

proc is_ipv4_address { hex } {
    
    set A [string range $hex 0 1]
    set B [string range $hex 2 3]
    set C [string range $hex 4 5]
    set D [string range $hex 6 7]
    
    set hex_list "$A $B $C $D"
    
    set ip1 [hex2dec [lindex $hex_list 0]]
    set ip2 [hex2dec [lindex $hex_list 1]]
    set ip3 [hex2dec [lindex $hex_list 2]]
    set ip4 [hex2dec [lindex $hex_list 3]]
    
    set ip_address "$ip1 $ip2 $ip3 $ip4"
    
    set result_ip_address [string map { " " "." } $ip_address]
    
    set octet {(?:\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])}
    
    set pattern "^[join [list $octet $octet $octet $octet] {\.}]\$"
    
    return [regexp -- $pattern $result_ip_address]
    
}


################################################################################
#  PROCEDURE NAME    : hex2ip                                                  #
#                                                                              #
#  DEFINITION        : This function is used to convert hex to ip address.     #
#                                                                              #
#  INPUT             : e.g C0A80A2D                                            #
#                                                                              #
#  OUTPUT            : e.g 192.168.10.45                                       #
#                                                                              #
#  USAGE             :                                                         #
#                       hex2ip $hex                                            #
#                                                                              #
################################################################################

proc hex2ip { hex } {
    
    set FirstHex  [hex2dec [string range $hex 0 1]]
    set SecondHex [hex2dec [string range $hex 2 3]]
    set ThirdHex  [hex2dec [string range $hex 4 5]]
    set FourthHex [hex2dec [string range $hex 6 7]]
    
    return "$FirstHex.$SecondHex.$ThirdHex.$FourthHex"
    
}

################################################################################
#  PROCEDURE NAME    : print_match_log                                         #
#                                                                              #
#  INPUT             :                                                         #
#                                                                              #
#    expected*       : expected value                                          #
#                                                                              #
#    received*       : received value                                          #
#                                                                              #
#    description*    : description to be printed in log messages               #
#                                                                              #
#    status*         : 0 if failure; 1 if success                              #
#                                                                              #
#  RETURNS           : NONE                                                    #
#                                                                              #
#  DEFINITION        : This function is used to print ATTEST Exp/Rcvd log      #
#                                                                              #
#  USAGE             :                                                         #
#                       print_match_log\                                       #
#                                   -expected       $expected                  #
#                                   -received       $received                  #
#                                   -description    $description               #
#                                   -status         $status                    #
#                                                                              #
################################################################################

proc print_match_log {args} {

    array set param $args
    set expected_value $param(-expected)
    set received_value $param(-received)
    set description $param(-description)
    set status $param(-status)

    if {$status == 1} {
        set log_level 3
        set match "matching"
    } else {
        set log_level 0
        set match "not matching"
    }

    LOG -level $log_level -msg "$description $match Exp-> $expected_value, Rcvd-> $received_value"

}

################################################################################
#  PROCEDURE NAME    : print_check_log                                         #
#                                                                              #
#  INPUT             :                                                         #
#                                                                              #
#    expected*       : expected value                                          #
#                                                                              #
#    received*       : received value                                          #
#                                                                              #
#    description*    : description to be printed in log messages               #
#                                                                              #
#    status*         : 0 if failure; 1 if success                              #
#                                                                              #
#  RETURNS           : NONE                                                    #
#                                                                              #
#  DEFINITION        : This function is used to print ATTEST Exp/Rcvd log      #
#                                                                              #
#  USAGE             :                                                         #
#                       print_check_log\                                       #
#                                   -expected       $expected                  #
#                                   -received       $received                  #
#                                   -description    $description               #
#                                   -status         $status                    #
#                                                                              #
################################################################################

proc print_check_log {args} {

    array set param $args
    set description    $param(-description)
    set status         $param(-status)
    set expected_value [string tolower $param(-expected)]
    set received_value [string tolower $param(-received)]

    if {$status == 1} {
        set log_level 3
        set msg "Ok (Exp: $expected_value, Rcvd: $received_value)"
    } else {
        set log_level 0
        set msg "Not ok (Exp: $expected_value, Rcvd: $received_value)"
        if {$status == "warn"} {
            set msg "Warning (Exp: $expected_value, Rcvd: $received_value)"
        }
    }

    LOG -level $log_level -msg "[format %-30s $description]: $msg"
}

################################################################################
#  PROCEDURE NAME    : validate_field                                          #
#                                                                              #
#  INPUT             :                                                         #
#                                                                              #
#    expected*       : expected parameter in global array 'validation_param'   #
#                                                                              #
#    received*       : received value                                          #
#                                                                              #
#    description*    : description to be printed in log messages               #
#                                                                              #
#    type*           : data type of value to represent in log messages         #
#                                                                              #
#  RETURNS           : 0 on failure to next level function                     #
#                    : 1 on success                                            #
#                                                                              #
#  DEFINITION        : This function is used to compare expected and received  #
#                      value and print relevant logs                           #
#                                                                              #
#  USAGE             :                                                         #
#                       validate_field\                                        #
#                                   -expected       $expected                  #
#                                   -received       $received                  #
#                                   -description    $description               #
#                                   -type           $type                      #
#                                                                              #
################################################################################
proc validate_field {args} {

    array set param $args

    set expected_param $param(-expected)
    set received_value $param(-received)
    set description $param(-description)
    set type $param(-type)

    if {![info exists ::validation_param($expected_param)]} {
        set expected_value "dont_care"
    } else {
        set expected_value [set ::validation_param($expected_param)]
    }

    if [info exists param(-default)] {
        set default_name $param(-default)
    } else {
        set default_name "Unknown"
    }
    
    if {[string match -nocase $expected_value "dont_care"]} {
        return 1
    }

#    set status [string match -nocase $expd $recvd]
    set status [string match -nocase $expected_value $received_value]

    switch $type {
        "mac" {
            #pbLib::mac_address_format
            set expected $expected_value
            set received $received_value
        }

        "ip" {
            #pbLib::ip_address_format
            set expected $expected_value
            set received $received_value
        }

        "hex" {
            set expected 0x$expected_value
            set received 0x$received_value
        }

        "dec2hex" {
            set expected 0x[dec2hex $expected_value]
            set received 0x[dec2hex $received_value]
        }

        default {
            set expected $expected_value
            set received $received_value
        }
    }

    if [info exists param(-array)] {

        array set arr $param(-array)

        get_value_from_array\
            -list       $param(-array)\
            -key        $expected\
            -value      expected\
            -default    $default_name

        get_value_from_array\
            -list       $param(-array)\
            -key        $received\
            -value      received\
            -default    $default_name
    }

    print_match_log\
                -expected       $expected\
                -received       $received\
                -description    $description\
                -status         $status

    if {$status} {
        return $status
    } else {
        #list of fields in ptp_ha packet
        set ptp_ha_pkt_fields [get_ptp_ha_fields]
        set field_id [expr [lsearch $ptp_ha_pkt_fields $expected_param] + 1]

        uplevel set mismatched_field $field_id
        return -code return 0
    }

}

################################################################################
#  PROCEDURE NAME    : validate_ptp_ha_pkt                                     #
#                                                                              #
#  INPUT             :                                                         #
#                                                                              #
#    pkt_content     : packet hex dump                                         #
#                                                                              #
#  RETURNS           : 0 on failure                                            #
#                    : 1 on success                                            #
#                                                                              #
#  DEFINITION        : This function is used to validate the ptp ha packet     #
#                                                                              #
#  USAGE             :                                                         #
#                       validate_ptp_ha_pkt\                                   #
#                                   -pkt_content       $pkt_content            #
#                                                                              #
################################################################################

proc validate_ptp_ha_pkt {args} {

    array set param $args
    set packet $param(-pkt_content)
    set packet [join $packet ""]

    if {[info exists param(-error_code)]} {
        upvar $param(-error_code) mismatched_field
        set mismatched_field 0
    }

    pbLib::decode_ptp_ha_pkt\
                 -packet                         $packet\
                 -dest_mac                       dest_mac\
                 -src_mac                        src_mac\
                 -eth_type                       eth_type\
                 -vlan_id                        vlan_id\
                 -vlan_priority                  vlan_priority\
                 -vlan_dei                       vlan_dei\
                 -ip_checksum                    ip_checksum\
                 -ip_protocol                    ip_protocol\
                 -src_ip                         src_ip\
                 -dest_ip                        dest_ip\
                 -src_port                       src_port\
                 -dest_port                      dest_port\
                 -udp_length                     udp_length\
                 -udp_checksum                   udp_checksum\
                 -major_sdo_id                   major_sdo_id\
                 -message_type                   message_type\
                 -minor_version                  minor_version\
                 -version_ptp                    version_ptp\
                 -message_length                 message_length\
                 -domain_number                  domain_number\
                 -minor_sdo_id                   minor_sdo_id\
                 -alternate_master_flag          alternate_master_flag\
                 -two_step_flag                  two_step_flag\
                 -unicast_flag                   unicast_flag\
                 -profile_specific1              profile_specific1\
                 -profile_specific2              profile_specific2\
                 -secure                         secure\
                 -leap61                         leap61\
                 -leap59                         leap59\
                 -current_utc_offset_valid       current_utc_offset_valid\
                 -ptp_timescale                  ptp_timescale\
                 -time_traceable                 time_traceable\
                 -freq_traceable                 freq_traceable\
                 -sync_uncertain                 sync_uncertain\
                 -correction_field               correction_field\
                 -message_type_specific          message_type_specific\
                 -src_port_number                src_port_number\
                 -src_clock_identity             src_clock_identity\
                 -sequence_id                    sequence_id\
                 -control_field                  control_field\
                 -log_message_interval           log_message_interval\
                 -origin_timestamp_sec           origin_timestamp_sec\
                 -origin_timestamp_ns            origin_timestamp_ns\
                 -current_utc_offset             current_utc_offset\
                 -gm_priority1                   gm_priority1\
                 -gm_priority2                   gm_priority2\
                 -gm_identity                    gm_identity\
                 -steps_removed                  steps_removed\
                 -time_source                    time_source\
                 -gm_clock_classy                gm_clock_classy\
                 -gm_clock_accuracy              gm_clock_accuracy\
                 -gm_clock_variance              gm_clock_variance\
                 -precise_origin_timestamp_sec   precise_origin_timestamp_sec\
                 -precise_origin_timestamp_ns    precise_origin_timestamp_ns\
                 -receive_timestamp_sec          receive_timestamp_sec\
                 -receive_timestamp_ns           receive_timestamp_ns\
                 -requesting_port_number         requesting_port_number\
                 -requesting_clock_identity      requesting_clock_identity\
                 -request_receipt_timestamp_sec  request_receipt_timestamp_sec\
                 -request_receipt_timestamp_ns   request_receipt_timestamp_ns\
                 -response_origin_timestamp_sec  response_origin_timestamp_sec\
                 -response_origin_timestamp_ns   response_origin_timestamp_ns\
                 -target_port_number             target_port_number\
                 -target_clock_identity          target_clock_identity\
                 -starting_boundary_hops         starting_boundary_hops\
                 -boundary_hops                  boundary_hops\
                 -action_field                   action_field\
                 -tlv                            tlv\
                 -tlv_type                       tlv_type\
                 -tlv_length                     tlv_length\
                 -ope                            ope\
                 -cr                             cr\
                 -rcr                            rcr\
                 -tcr                            tcr\
                 -ic                             ic\
                 -irc                            irc\
                 -itc                            itc\
                 -fov                            fov\
                 -pov                            pov\
                 -tct                            tct\
                 -phase_offset_tx                phase_offset_tx\
                 -phase_offset_tx_timestamp_sec  phase_offset_tx_timestamp_sec\
                 -phase_offset_tx_timestamp_ns   phase_offset_tx_timestamp_ns\
                 -freq_offset_tx                 freq_offset_tx\
                 -freq_offset_tx_timestamp_sec   freq_offset_tx_timestamp_sec\
                 -freq_offset_tx_timestamp_ns    freq_offset_tx_timestamp_ns\
                 -management_id                  management_id\
                 -ext_port_config                ext_port_config\
                 -master_only                    master_only\

    validate_field\
                -expected       dest_mac\
                -received       $dest_mac\
                -description    "Destination MAC"\
                -type           "mac"

    validate_field\
                -expected       src_mac\
                -received       $src_mac\
                -description    "Source MAC"\
                -type           "mac"

    validate_field\
                -expected       eth_type\
                -received       $eth_type\
                -description    "Ethtype"\
                -type           "hex"

    validate_field\
                -expected       vlan_id\
                -received       $vlan_id\
                -description    "VLAN ID"\
                -type           "dec"


    validate_field\
                -expected       vlan_priority\
                -received       $vlan_priority\
                -description    "VLAN Priority"\
                -type           "dec"


    validate_field\
                -expected       vlan_dei\
                -received       $vlan_dei\
                -description    "VLAN DEI bit"\
                -type           "bit"

    validate_field\
                -expected       ip_checksum\
                -received       $ip_checksum\
                -description    "IP Checksum"\
                -type           "hex"

    validate_field\
                -expected       ip_protocol\
                -received       $ip_protocol\
                -description    "IP Protocol ID"\
                -type           "dec"

    validate_field\
                -expected       src_ip\
                -received       $src_ip\
                -description    "Source IP"\
                -type           "ip"

    validate_field\
                -expected       dest_ip\
                -received       $dest_ip\
                -description    "Destination IP"\
                -type           "ip"

    validate_field\
                -expected       src_port\
                -received       $src_port\
                -description    "Source Port"\
                -type           "dec"

    validate_field\
                -expected       dest_port\
                -received       $dest_port\
                -description    "Destination Port"\
                -type           "dec"

    validate_field\
                -expected       udp_length\
                -received       $udp_length\
                -description    "UDP Length"\
                -type           "dec"

    validate_field\
                -expected       udp_checksum\
                -received       $udp_checksum\
                -description    "UDP Checksum"\
                -type           "hex"

    validate_field\
                -expected       major_sdo_id\
                -received       $major_sdo_id\
                -description    "PTP Major SDO ID"\
                -type           "dec"

    set message_type_array {
        0x0 "Sync"
        0x1 "Delay_Req"
        0x2 "Pdelay_Req"
        0x3 "Pdelay_Resp"
        0x8 "Follow_Up"
        0x9 "Delay_Resp"
        0xa "Pdelay_Resp_Follow_Up"
        0xb "Announce"
        0xc "Signaling"
        0xd "Management"
    }

    validate_field\
                -expected       message_type\
                -received       $message_type\
                -description    "PTP Message Type"\
                -type           "dec2hex"\
                -array          $message_type_array\
                -default        Reserved

    validate_field\
                -expected       minor_version\
                -received       $minor_version\
                -description    "PTP Minor version"\
                -type           "dec"

    validate_field\
                -expected       version_ptp\
                -received       $version_ptp\
                -description    "PTP version"\
                -type           "dec"

    validate_field\
                -expected       message_length\
                -received       $message_length\
                -description    "PTP Message Length"\
                -type           "dec"

    validate_field\
                -expected       domain_number\
                -received       $domain_number\
                -description    "PTP Domain Number"\
                -type           "dec"

    validate_field\
                -expected       minor_sdo_id\
                -received       $minor_sdo_id\
                -description    "PTP Minor SDO ID"\
                -type           "dec"

    validate_field\
                -expected       alternate_master_flag\
                -received       $alternate_master_flag\
                -description    "PTP Alternate Master Flag"\
                -type           "bit"

    validate_field\
                -expected       two_step_flag\
                -received       $two_step_flag\
                -description    "PTP Two Step Flag"\
                -type           "bit"

    validate_field\
                -expected       unicast_flag\
                -received       $unicast_flag\
                -description    "PTP Unicast Flag"\
                -type           "bit"

    validate_field\
                -expected       profile_specific1\
                -received       $profile_specific1\
                -description    "PTP Profile specific 1 Flag"\
                -type           "bit"

    validate_field\
                -expected       profile_specific2\
                -received       $profile_specific2\
                -description    "PTP Profile specific 2 Flag"\
                -type           "bit"

    validate_field\
                -expected       secure\
                -received       $secure\
                -description    "PTP Secure Flag"\
                -type           "bit"

    validate_field\
                -expected       leap61\
                -received       $leap61\
                -description    "PTP leap61 Flag"\
                -type           "bit"

    validate_field\
                -expected       leap59\
                -received       $leap59\
                -description    "PTP leap59 Flag"\
                -type           "bit"

    validate_field\
                -expected       current_utc_offset_valid\
                -received       $current_utc_offset_valid\
                -description    "PTP current utc offset valid Flag"\
                -type           "bit"

    validate_field\
                -expected       ptp_timescale\
                -received       $ptp_timescale\
                -description    "PTP Time scale Flag"\
                -type           "bit"

    validate_field\
                -expected       time_traceable\
                -received       $time_traceable\
                -description    "PTP Time traceable Flag"\
                -type           "bit"

    validate_field\
                -expected       freq_traceable\
                -received       $freq_traceable\
                -description    "PTP Frequency traceable Flag"\
                -type           "bit"

    validate_field\
                -expected       sync_uncertain\
                -received       $sync_uncertain\
                -description    "PTP synchronization uncertain Flag"\
                -type           "bit"

    validate_field\
                -expected       correction_field\
                -received       $correction_field\
                -description    "PTP Correction Field"\
                -type           "dec"

    validate_field\
                -expected       message_type_specific\
                -received       $message_type_specific\
                -description    "PTP Message Type Specific"\
                -type           "dec"

    validate_field\
                -expected       src_port_number\
                -received       $src_port_number\
                -description    "PTP Source port number"\
                -type           "dec"

    validate_field\
                -expected       src_clock_identity\
                -received       $src_clock_identity\
                -description    "PTP Source clock identity"\
                -type           "dec"

    validate_field\
                -expected       sequence_id\
                -received       $sequence_id\
                -description    "PTP Sequence ID"\
                -type           "dec"

    validate_field\
                -expected       control_field\
                -received       $control_field\
                -description    "PTP Control field"\
                -type           "dec"

    validate_field\
                -expected       log_message_interval\
                -received       $log_message_interval\
                -description    "PTP Log Message interval"\
                -type           "dec"

    validate_field\
                -expected       current_utc_offset\
                -received       $current_utc_offset\
                -description    "PTP Current UTC Offset"\
                -type           "dec"

    validate_field\
                -expected       gm_priority1\
                -received       $gm_priority1\
                -description    "PTP Grandmaster priority 1"\
                -type           "dec"

    validate_field\
                -expected       gm_priority2\
                -received       $gm_priority2\
                -description    "PTP Grandmaster priority 2"\
                -type           "dec"

    validate_field\
                -expected       gm_identity\
                -received       $gm_identity\
                -description    "PTP Grandmaster Identity"\
                -type           "dec"

    validate_field\
                -expected       steps_removed\
                -received       $steps_removed\
                -description    "PTP Steps removed"\
                -type           "dec"

    validate_field\
                -expected       time_source\
                -received       $time_source\
                -description    "PTP Time source"\
                -type           "dec"

    validate_field\
               -expected       steps_removed\
               -received       $steps_removed\
               -description    "PTP Steps removed"\
               -type           "dec"

    validate_field\
               -expected       time_source\
               -received       $time_source\
               -description    "PTP Time source"\
               -type           "dec"

    validate_field\
               -expected       gm_clock_classy\
               -received       $gm_clock_classy\
               -description    "PTP Grandmaster clock class"\
               -type           "dec"

    validate_field\
               -expected       gm_clock_accuracy\
               -received       $gm_clock_accuracy\
               -description    "PTP Grandmaster clock accuracy"\
               -type           "dec"

    validate_field\
               -expected       gm_clock_variance\
               -received       $gm_clock_variance\
               -description    "PTP Grandmaster clock variance"\
               -type           "dec"

    validate_field\
               -expected       requesting_port_number\
               -received       $requesting_port_number\
               -description    "PTP Requesting port number"\
               -type           "dec"

    validate_field\
               -expected       requesting_clock_identity\
               -received       $requesting_clock_identity\
               -description    "PTP Requesting clock identity"\
               -type           "dec"

    validate_field\
               -expected       target_port_number\
               -received       $target_port_number\
               -description    "PTP Target port number"\
               -type           "dec"

    validate_field\
               -expected       target_clock_identity\
               -received       $target_clock_identity\
               -description    "PTP Target clock identity"\
               -type           "dec"

    validate_field\
               -expected       starting_boundary_hops\
               -received       $starting_boundary_hops\
               -description    "PTP Starting boundary hops"\
               -type           "dec"

    validate_field\
               -expected       boundary_hops\
               -received       $boundary_hops\
               -description    "PTP Boundary hops"\
               -type           "dec"

    validate_field\
               -expected       action_field\
               -received       $action_field\
               -description    "PTP Action field"\
               -type           "dec"

    validate_field\
               -expected       tlv\
               -received       $tlv\
               -description    "PTP TLV Flag"\
               -type           "dec"

    set tlv_type_array {
        0x0000                                 Reserved
        0x0001                               MANAGEMENT
        0x0002                  MANAGEMENT_ERROR_STATUS
        0x0003                   ORGANIZATION_EXTENSION
        0x0004             REQUEST_UNICAST_TRANSMISSION
        0x0005               GRANT_UNICAST_TRANSMISSION
        0x0006              CANCEL_UNICAST_TRANSMISSION
        0x0007  ACKNOWLEDGE_CANCEL_UNICAST_TRANSMISSION
        0x0008                               PATH_TRACE
        0x0009          ALTERNATE_TIME_OFFSET_INDICATOR
        0x000A                    CUMULATIVE_RATE_RATIO
        0x000B                ENHANCED_ACCURACY_METRICS
        0x000C                                      PAD
        0x000D                           AUTHENTICATION
        0x4000        ORGANIZATION_EXTENSION_FORWARDING
        0x8001                                  L1_SYNC
        0x8002          PORT_COMMUNICATION_AVAILABILITY
        0x8003                         PROTOCOL_ADDRESS
        0x8004                SLAVE_RX_SYNC_TIMING_DATA
        0x8005              SLAVE_RX_SYNC_COMPUTED_DATA
        0x8006                SLAVE_TX_EVENT_TIMESTAMPS
    }

    validate_field\
                -expected       tlv_type\
                -received       $tlv_type\
                -description    "PTP TLV Type"\
                -type           "dec2hex"\
                -array          $tlv_type_array\
                -default        Reserved

    validate_field\
               -expected       tlv_length\
               -received       $tlv_length\
               -description    "PTP TLV length"\
               -type           "dec"

    validate_field\
               -expected       ope\
               -received       $ope\
               -description    "PTP OPE Flag"\
               -type           "dec"

    validate_field\
               -expected       cr\
               -received       $cr\
               -description    "PTP CR Flag"\
               -type           "dec"

    validate_field\
               -expected       rcr\
               -received       $rcr\
               -description    "PTP RCR Flag"\
               -type           "dec"

    validate_field\
               -expected       tcr\
               -received       $tcr\
               -description    "PTP TCR Flag"\
               -type           "dec"

    validate_field\
               -expected       ic\
               -received       $ic\
               -description    "PTP IC Flag"\
               -type           "dec"

    validate_field\
               -expected       irc\
               -received       $irc\
               -description    "PTP IRC Flag"\
               -type           "dec"

    validate_field\
               -expected       itc\
               -received       $itc\
               -description    "PTP ITC Flag"\
               -type           "dec"

    validate_field\
               -expected       fov\
               -received       $fov\
               -description    "PTP FOV Flag"\
               -type           "dec"

    validate_field\
               -expected       pov\
               -received       $pov\
               -description    "PTP POV Flag"\
               -type           "dec"

    validate_field\
               -expected       tct\
               -received       $tct\
               -description    "PTP TCT Flag"\
               -type           "dec"

    validate_field\
               -expected       phase_offset_tx\
               -received       $phase_offset_tx\
               -description    "PTP Phase offset tx"\
               -type           "dec"

    validate_field\
               -expected       phase_offset_tx_timestamp_sec\
               -received       $phase_offset_tx_timestamp_sec\
               -description    "PTP Phase offset tx timestamp (s)"\
               -type           "dec"

    validate_field\
               -expected       phase_offset_tx_timestamp_ns\
               -received       $phase_offset_tx_timestamp_ns\
               -description    "PTP Phase offset tx timestamp (ns)"\
               -type           "dec"

    validate_field\
               -expected       freq_offset_tx\
               -received       $freq_offset_tx\
               -description    "PTP Freq offset tx"\
               -type           "dec"

    validate_field\
               -expected       freq_offset_tx_timestamp_sec\
               -received       $freq_offset_tx_timestamp_sec\
               -description    "PTP Freq offset tx timestamp (s)"\
               -type           "dec"

    validate_field\
               -expected       freq_offset_tx_timestamp_ns\
               -received       $freq_offset_tx_timestamp_ns\
               -description    "PTP Freq offset tx timestamp (ns)"\
               -type           "dec"

    validate_field\
               -expected       management_id\
               -received       $management_id\
               -description    "PTP Management id"\
               -type           "dec"

    validate_field\
               -expected       ext_port_config\
               -received       $ext_port_config\
               -description    "PTP Ext port config"\
               -type           "dec"

    validate_field\
               -expected       master_only\
               -received       $master_only\
               -description    "PTP Master only"\
               -type           "dec"

    return 1

}

################################################################################
#  PROCEDURE NAME    : ptp_ha_default_pkt_handler                              #
#                                                                              #
#  INPUT             :                                                         #
#                                                                              #
#    data            : packet hex dump                                         #
#                                                                              #
#  RETURNS           : packet hex dump                                         #
#                                                                              #
#  DEFINITION        : This function is used to do following operation         #
#                              -make no changes in the hex dump                #
#                                                                              #
#  USAGE             :                                                         #
#                    ptp_ha_default_pkt_handler\                               #
#                                   -data       $data                          #
#                                                                              #
################################################################################

proc ptp_ha_default_pkt_handler {args} {
     array set param $args
     return $param(-data)
}

################################################################################
#  PROCEDURE NAME    : update_ptp_ha_dynamic_fields                            #
#                                                                              #
#  INPUT             :                                                         #
#                                                                              #
#    data            : packet hex dump                                         #
#                                                                              #
#  RETURNS           : updated packet hex dump                                 #
#                                                                              #
#  DEFINITION        : This function is used to do following operation         #
#                     a) Increment the sequence id                             #
#                     b) Update the packet with current timestamp              #
#                            - as origin timestamp                             #
#                                                                              #
#  USAGE             :                                                         #
#                        update_ptp_ha_dynamic_fields\                         #
#                                   -data       $data                          #
#                                                                              #
################################################################################

proc update_ptp_ha_dynamic_fields {args} {

    array set param $args
    set packet $param(-data)
    set packet [join $packet ""]

    if [pbLib::decode_ptp_ha_pkt\
                    -packet                         $packet\
                    -dest_mac                       dest_mac\
                    -src_mac                        src_mac\
                    -eth_type                       eth_type\
                    -vlan_id                        vlan_id\
                    -vlan_priority                  vlan_priority\
                    -vlan_dei                       vlan_dei\
                    -ip_version                     ip_version\
                    -ip_header_length               ip_header_length\
                    -ip_tos                         ip_tos\
                    -ip_total_length                ip_total_length\
                    -ip_identification              ip_identification\
                    -ip_flags                       ip_flags\
                    -ip_offset                      ip_offset\
                    -ip_ttl                         ip_ttl\
                    -ip_checksum                    ip_checksum\
                    -ip_protocol                    ip_protocol\
                    -src_ip                         src_ip\
                    -dest_ip                        dest_ip\
                    -src_port                       src_port\
                    -dest_port                      dest_port\
                    -udp_length                     udp_length\
                    -udp_checksum                   udp_checksum\
                    -major_sdo_id                   major_sdo_id\
                    -message_type                   message_type\
                    -minor_version                  minor_version\
                    -version_ptp                    version_ptp\
                    -message_length                 message_length\
                    -domain_number                  domain_number\
                    -minor_sdo_id                   minor_sdo_id\
                    -alternate_master_flag          alternate_master_flag\
                    -two_step_flag                  two_step_flag\
                    -unicast_flag                   unicast_flag\
                    -profile_specific1              profile_specific1\
                    -profile_specific2              profile_specific2\
                    -secure                         secure\
                    -leap61                         leap61\
                    -leap59                         leap59\
                    -current_utc_offset_valid       current_utc_offset_valid\
                    -ptp_timescale                  ptp_timescale\
                    -time_traceable                 time_traceable\
                    -freq_traceable                 freq_traceable\
                    -sync_uncertain                 sync_uncertain\
                    -correction_field               correction_field\
                    -message_type_specific          message_type_specific\
                    -src_port_number                src_port_number\
                    -src_clock_identity             src_clock_identity\
                    -sequence_id                    sequence_id\
                    -control_field                  control_field\
                    -log_message_interval           log_message_interval\
                    -origin_timestamp_sec           origin_timestamp_sec\
                    -origin_timestamp_ns            origin_timestamp_ns\
                    -current_utc_offset             current_utc_offset\
                    -gm_priority1                   gm_priority1\
                    -gm_priority2                   gm_priority2\
                    -gm_identity                    gm_identity\
                    -steps_removed                  steps_removed\
                    -time_source                    time_source\
                    -gm_clock_classy                gm_clock_classy\
                    -gm_clock_accuracy              gm_clock_accuracy\
                    -gm_clock_variance              gm_clock_variance\
                    -precise_origin_timestamp_sec   precise_origin_timestamp_sec\
                    -precise_origin_timestamp_ns    precise_origin_timestamp_ns\
                    -receive_timestamp_sec          receive_timestamp_sec\
                    -receive_timestamp_ns           receive_timestamp_ns\
                    -requesting_port_number         requesting_port_number\
                    -requesting_clock_identity      requesting_clock_identity\
                    -request_receipt_timestamp_sec  request_receipt_timestamp_sec\
                    -request_receipt_timestamp_ns   request_receipt_timestamp_ns\
                    -response_origin_timestamp_sec  response_origin_timestamp_sec\
                    -response_origin_timestamp_ns   response_origin_timestamp_ns\
                    -target_port_number             target_port_number\
                    -target_clock_identity          target_clock_identity\
                    -starting_boundary_hops         starting_boundary_hops\
                    -boundary_hops                  boundary_hops\
                    -action_field                   action_field\
                    -tlv                            tlv\
                    -tlv_type                       tlv_type\
                    -tlv_length                     tlv_length\
                    -ope                            ope\
                    -cr                             cr\
                    -rcr                            rcr\
                    -tcr                            tcr\
                    -ic                             ic\
                    -irc                            irc\
                    -itc                            itc\
                    -fov                            fov\
                    -pov                            pov\
                    -tct                            tct\
                    -phase_offset_tx                phase_offset_tx\
                    -phase_offset_tx_timestamp_sec  phase_offset_tx_timestamp_sec\
                    -phase_offset_tx_timestamp_ns   phase_offset_tx_timestamp_ns\
                    -freq_offset_tx                 freq_offset_tx\
                    -freq_offset_tx_timestamp_sec   freq_offset_tx_timestamp_sec\
                    -freq_offset_tx_timestamp_ns    freq_offset_tx_timestamp_ns\
                    -management_id                  management_id\
                    -ext_port_config                ext_port_config\
                    -master_only                    master_only] {

        #update dynamic fields

        incr sequence_id

        set timestamp_us                    [clock microseconds]
        set timestamp_s                     [expr $timestamp_us / 1000000]
        set timestamp_ns                    [expr ($timestamp_us % 1000000) * 1000]

        if { $two_step_flag == 1 } {
            set origin_timestamp_sec        0
            set origin_timestamp_ns         0
        } else {
            set origin_timestamp_sec        $timestamp_s
            set origin_timestamp_ns         $timestamp_ns
        }

#       set origin_timestamp_sec            $timestamp_s
#       set origin_timestamp_ns             $timestamp_ns
        set precise_origin_timestamp_sec    $timestamp_s
        set precise_origin_timestamp_ns     $timestamp_ns
        set receive_timestamp_sec           $timestamp_s
        set receive_timestamp_ns            $timestamp_ns
        set request_receipt_timestamp_sec   $timestamp_s
        set request_receipt_timestamp_ns    $timestamp_ns
        set response_origin_timestamp_sec   $timestamp_s
        set response_origin_timestamp_ns    $timestamp_ns

        set ::gtx_origin_timestamp_sec      $origin_timestamp_sec
        set ::gtx_origin_timestamp_ns       $origin_timestamp_ns

        switch $message_type {

            1 {
                set correction_field        $::TEE_CORRECTION_FIELD
            }

            3 {
                set correction_field        $::TEE_CORRECTION_FIELD
            }

            8 {
                set ::gtx_follow_up_timestamp_sec   $timestamp_s
                set ::gtx_follow_up_timestamp_ns    $timestamp_ns
                set correction_field        $::TEE_CORRECTION_FIELD
            }

            10 {
                set correction_field        $::TEE_CORRECTION_FIELD
            }
        }

        pbLib::encode_ptp_ha_pkt\
                        -hexdump                        hexdump\
                        -dest_mac                       $dest_mac\
                        -src_mac                        $src_mac\
                        -eth_type                       $eth_type\
                        -vlan_id                        $vlan_id\
                        -vlan_priority                  $vlan_priority\
                        -vlan_dei                       $vlan_dei\
                        -ip_version                     $ip_version\
                        -ip_header_length               $ip_header_length\
                        -ip_tos                         $ip_tos\
                        -ip_total_length                $ip_total_length\
                        -ip_identification              $ip_identification\
                        -ip_flags                       $ip_flags\
                        -ip_offset                      $ip_offset\
                        -ip_ttl                         $ip_ttl\
                        -ip_checksum                    $ip_checksum\
                        -ip_protocol                    $ip_protocol\
                        -src_ip                         $src_ip\
                        -dest_ip                        $dest_ip\
                        -src_port                       $src_port\
                        -dest_port                      $dest_port\
                        -udp_length                     $udp_length\
                        -udp_checksum                   "auto"\
                        -major_sdo_id                   $major_sdo_id\
                        -message_type                   $message_type\
                        -minor_version                  $minor_version\
                        -version_ptp                    $version_ptp\
                        -message_length                 $message_length\
                        -domain_number                  $domain_number\
                        -minor_sdo_id                   $minor_sdo_id\
                        -alternate_master_flag          $alternate_master_flag\
                        -two_step_flag                  $two_step_flag\
                        -unicast_flag                   $unicast_flag\
                        -profile_specific1              $profile_specific1\
                        -profile_specific2              $profile_specific2\
                        -secure                         $secure\
                        -leap61                         $leap61\
                        -leap59                         $leap59\
                        -current_utc_offset_valid       $current_utc_offset_valid\
                        -ptp_timescale                  $ptp_timescale\
                        -time_traceable                 $time_traceable\
                        -freq_traceable                 $freq_traceable\
                        -sync_uncertain                 $sync_uncertain\
                        -correction_field               $correction_field\
                        -message_type_specific          $message_type_specific\
                        -src_port_number                $src_port_number\
                        -src_clock_identity             $src_clock_identity\
                        -sequence_id                    $sequence_id\
                        -control_field                  $control_field\
                        -log_message_interval           $log_message_interval\
                        -origin_timestamp_sec           $origin_timestamp_sec\
                        -origin_timestamp_ns            $origin_timestamp_ns\
                        -current_utc_offset             $current_utc_offset\
                        -gm_priority1                   $gm_priority1\
                        -gm_priority2                   $gm_priority2\
                        -gm_identity                    $gm_identity\
                        -steps_removed                  $steps_removed\
                        -time_source                    $time_source\
                        -gm_clock_classy                $gm_clock_classy\
                        -gm_clock_accuracy              $gm_clock_accuracy\
                        -gm_clock_variance              $gm_clock_variance\
                        -precise_origin_timestamp_sec   $precise_origin_timestamp_sec\
                        -precise_origin_timestamp_ns    $precise_origin_timestamp_ns\
                        -receive_timestamp_sec          $receive_timestamp_sec\
                        -receive_timestamp_ns           $receive_timestamp_ns\
                        -requesting_port_number         $requesting_port_number\
                        -requesting_clock_identity      $requesting_clock_identity\
                        -request_receipt_timestamp_sec  $request_receipt_timestamp_sec\
                        -request_receipt_timestamp_ns   $request_receipt_timestamp_ns\
                        -response_origin_timestamp_sec  $response_origin_timestamp_sec\
                        -response_origin_timestamp_ns   $response_origin_timestamp_ns\
                        -target_port_number             $target_port_number\
                        -target_clock_identity          $target_clock_identity\
                        -starting_boundary_hops         $starting_boundary_hops\
                        -boundary_hops                  $boundary_hops\
                        -action_field                   $action_field\
                        -tlv                            $tlv\
                        -tlv_type                       $tlv_type\
                        -tlv_length                     $tlv_length\
                        -ope                            $ope\
                        -cr                             $cr\
                        -rcr                            $rcr\
                        -tcr                            $tcr\
                        -ic                             $ic\
                        -irc                            $irc\
                        -itc                            $itc\
                        -fov                            $fov\
                        -pov                            $pov\
                        -tct                            $tct\
                        -phase_offset_tx                $phase_offset_tx\
                        -phase_offset_tx_timestamp_sec  $phase_offset_tx_timestamp_sec\
                        -phase_offset_tx_timestamp_ns   $phase_offset_tx_timestamp_ns\
                        -freq_offset_tx                 $freq_offset_tx\
                        -freq_offset_tx_timestamp_sec   $freq_offset_tx_timestamp_sec\
                        -freq_offset_tx_timestamp_ns    $freq_offset_tx_timestamp_ns\
                        -management_id                  $management_id\
                        -ext_port_config                $ext_port_config\
                        -master_only                    $master_only

        return $hexdump
    } else {
        ERRLOG  -msg "update_ptp_ha_dynamic_fields: Given Hexdump is not a ptp ha packet"
    }
}

################################################################################
#  PROCEDURE NAME    : validate_arp_pkt                                        #
#                                                                              #
#  INPUT             :                                                         #
#                                                                              #
#    pkt_content     : packet hex dump                                         #
#                                                                              #
#  RETURNS           : 0 on failure                                            #
#                    : 1 on success                                            #
#                                                                              #
#  DEFINITION        : This function is used to validate the arp packet        #
#                                                                              #
#  USAGE             :                                                         #
#                       validate_arp_pkt\                                      #
#                                   -pkt_content       $pkt_content            #
#                                                                              #
################################################################################

proc validate_arp_pkt {args} {

    array set param $args
    set packet $param(-pkt_content)
    set packet [join $packet ""]

    pbLib::decode_arp_pkt\
                 -packet                      $packet\
                 -dest_mac                    dest_mac\
                 -src_mac                     src_mac\
                 -eth_type                    eth_type\
                 -vlan_id                     vlan_id\
                 -vlan_priority               vlan_priority\
                 -vlan_dei                    vlan_dei\
                 -vlan_next                   vlan_next\
                 -message_type                message_type\
                 -sender_mac                  sender_mac\
                 -sender_ip                   sender_ip\
                 -target_mac                  target_mac\
                 -target_ip                   target_ip

    validate_field\
                -expected       dest_mac\
                -received       $dest_mac\
                -description    "Destination Mac"\
                -type           "mac"

    validate_field\
                -expected       src_mac\
                -received       $src_mac\
                -description    "Source Mac"\
                -type           "mac"

    validate_field\
                -expected       eth_type\
                -received       $eth_type\
                -description    "Ethtype"\
                -type           "hex"

    validate_field\
                -expected       vlan_id\
                -received       $vlan_id\
                -description    "VLAN ID"\
                -type           "dec"

    validate_field\
                -expected       vlan_priority\
                -received       $vlan_priority\
                -description    "VLAN Priority"\
                -type           "dec"

    validate_field\
                -expected       vlan_dei\
                -received       $vlan_dei\
                -description    "VLAN DEI bit"\
                -type           "bit"

    validate_field\
                -expected       vlan_next\
                -received       $vlan_next\
                -description    "Ethertype"\
                -type           "hex"

    validate_field\
                -expected       message_type\
                -received       $message_type\
                -description    "ARP Message Type"\
                -type           "dec"

    validate_field\
                -expected       sender_mac\
                -received       $sender_mac\
                -description    "Sender MAC address"\
                -type           "mac"

    validate_field\
                -expected       sender_ip\
                -received       $sender_ip\
                -description    "Sender IP address"\
                -type           "ip"

    validate_field\
                -expected       target_mac\
                -received       $target_mac\
                -description    "Target MAC address"\
                -type           "mac"

    validate_field\
                -expected       target_ip\
                -received       $target_ip\
                -description    "Target IP address"\
                -type           "ip"

    return 1

}

################################################################################
#  PROCEDURE NAME    : send_arp_response                                       #
#                                                                              #
#  INPUT             :                                                         #
#                                                                              #
#    pkt_content     : packet hex dump                                         #
#                                                                              #
#  RETURNS           : 0 on failure                                            #
#                    : 1 on success                                            #
#                                                                              #
#  DEFINITION        : This function is used to validate the arp packet and    #
#                      respond with arp reply                                  #
#                                                                              #
#  USAGE             :                                                         #
#                       send_arp_response\                                     #
#                                   -pkt_content       $pkt_content            #
#                                                                              #
################################################################################

proc send_arp_response {args} {
    
    array set param $args
    set packet $param(-pkt_content)
    set packet [join $packet ""]

    if {[validate_arp_pkt -pkt_content $packet]} {

        pbLib::decode_arp_pkt\
                     -packet           $packet\
                     -dest_mac         recvd_dest_mac\
                     -src_mac          recvd_src_mac\
                     -eth_type         recvd_eth_type\
                     -vlan_id          recvd_vlan_id\
                     -vlan_priority    recvd_vlan_priority\
                     -vlan_dei         recvd_vlan_dei\
                     -vlan_next        recvd_vlan_next\
                     -message_type     recvd_message_type\
                     -sender_mac       recvd_sender_mac\
                     -sender_ip        recvd_sender_ip\
                     -target_mac       recvd_target_mac\
                     -target_ip        recvd_target_ip


        set session_id $::validation_param(session_id)
        set port_num   $::validation_param(port_num)

        set dest_mac   $recvd_src_mac
        set target_mac $recvd_src_mac
 
        set src_mac    $::validation_param(tee_mac)
        set sender_mac $::validation_param(tee_mac)

        set sender_ip $recvd_target_ip
        set target_ip $recvd_sender_ip

        if {![pltLib::send_arp\
                     -session_id       $session_id\
                     -port_num         $port_num\
                     -dest_mac         $dest_mac\
                     -src_mac          $src_mac\
                     -eth_type         $recvd_eth_type\
                     -vlan_id          $recvd_vlan_id\
                     -vlan_priority    $recvd_vlan_priority\
                     -vlan_dei         $recvd_vlan_dei\
                     -message_type     2\
                     -sender_mac       $sender_mac\
                     -sender_ip        $sender_ip\
                     -target_mac       $target_mac\
                     -target_ip        $target_ip\
                     -count            1\
                     -interval         1]} {
 
            return 0
        }

    }

    return 1
}

################################################################################
#  PROCEDURE NAME    : check_mac                                               #
#                                                                              #
#  INPUT             :                                                         #
#                                                                              #
#    mac             : mac address                                             #
#                                                                              #
#    type            : type of mac address [UNICAST | MULTICAST | BROADCAST]   #
#                      or value to be compared with                            #
#                                                                              #
#  RETURNS           : 0 on failure                                            #
#                    : 1 on success                                            #
#                                                                              #
#  DEFINITION        : This function is used to check if the given mac is      #
#                      of given type                                           #
#                                                                              #
#  USAGE             :                                                         #
#                       check_mac\                                             #
#                             -mac         $mac\                               #
#                             -type        $type                               #
#                                                                              #
################################################################################

proc check_mac {args} {

    array set param $args
    set mac $param(-mac)

    if {[info exists param(-type)]} {
        set type $param(-type)
    } else {
        set type "dont_care"
    }

    switch -nocase $type {

        "UNICAST" {
            return [expr !((0x[regexp -inline {[0-9aA-zZ]{2}} $mac])%2)]
        }

        "MULTICAST" {
            return [expr (0x[regexp -inline {[0-9aA-zZ]{2}} $mac])%2]
        }

        "BROADCAST" {
            return [string match -nocase "ff:ff:ff:ff:ff:ff" $mac]
        }

        default {
            return [string match -nocase $type $mac]
        }

    }

    return 1

}

################################################################################
#  PROCEDURE NAME    : check_ip                                                #
#                                                                              #
#  INPUT             :                                                         #
#                                                                              #
#    ip              : ip address                                              #
#                                                                              #
#    type            : type of ip address [UNICAST | MULTICAST | BROADCAST]    #
#                      or value to be compared with                            #
#                                                                              #
#  RETURNS           : 0 on failure                                            #
#                    : 1 on success                                            #
#                                                                              #
#  DEFINITION        : This function is used to check if the given ip is       #
#                      of given type                                           #
#                                                                              #
#  USAGE             :                                                         #
#                       check_ip\                                              #
#                             -ip          $ip\                                #
#                             -type        $type                               #
#                                                                              #
################################################################################

proc check_ip {args} {

    array set param $args
    set ip $param(-ip)

    if {[info exists param(-type)]} {
        set type $param(-type)
    } else {
        set type "dont_care"
    }

    if {$ip == ""} {return 0}

    set first_byte [lindex [split $ip .] 0]

    switch -nocase $type {

        "UNICAST" {
            return [expr $first_byte < 224]
        }

        "MULTICAST" {
            return [expr $first_byte >= 224]
        }

        "BROADCAST" {
            return [string match -nocase "255.255.255.255" $ip]
        }

        default {
            return [string match -nocase $type $ip]
        }

    }

    return 1

}

################################################################################
#  PROCEDURE NAME    : check_value                                             #
#                                                                              #
#  INPUT             :                                                         #
#                                                                              #
#    value           : value which should be checked                           #
#                                                                              #
#    range           : range in which values existance must be checked         #
#                                                                              #
#  RETURNS           : 0 if value is not in range                              #
#                    : 1 if value is in range                                  #
#                                                                              #
#  DEFINITION        : This function is used to check if the given value is in #
#                      the given range                                         #
#                                                                              #
#  USAGE             :                                                         #
#                       check_value\                                           #
#                             -value       $value\                             #
#                             -range       $range                              #
#                                                                              #
################################################################################

proc check_value {args} {

    array set param $args
    set value $param(-value)
    set range $param(-range)
    
    if {[info exists param(-type)]} {
        set type [set param(-type)]
    } else {
        set type "dec"
    }

    if {$value == ""} {return 0}

    #Convert value to dec
    if {$type == "hex"} {
        set value [hex2dec $value]
    }

    #non-zero condition
    if {$range == "non-zero"} {

        return [expr $value != 0]

    } else {
        set range_values [split $range -]

        #non range
        if {[llength $range_values] == 1} {

            #Convert range to dec
            if {$type == "hex"} {
                set range [hex2dec $range]
            }

            return [expr $value == $range]

        }
    }

    #non range
    set min [lindex $range_values 0]
    set max [lindex $range_values 1]

    #Convert range to dec
    if {$type == "hex"} {
        set min [hex2dec $min]
        set max [hex2dec $max]
    }

    return [expr ($value >= $min) && ($value <=$max)]

}

################################################################################
#  PROCEDURE NAME    : check_field                                             #
#                                                                              #
#  INPUT             :                                                         #
#                                                                              #
#    expected*       : expected parameter in global array 'validation_param'   #
#                                                                              #
#    received*       : received value                                          #
#                                                                              #
#    description*    : description to be printed in log messages               #
#                                                                              #
#    type*           : data type of value to represent in log messages         #
#                                                                              #
#  RETURNS           : 0 on failure to next level function                     #
#                    : 1 on success                                            #
#                                                                              #
#  DEFINITION        : This function is used to compare expected and received  #
#                      value and print relevant logs                           #
#                                                                              #
#  USAGE             :                                                         #
#                       validate_field\                                        #
#                                   -expected       $expected                  #
#                                   -received       $received                  #
#                                   -description    $description               #
#                                   -type           $type                      #
#                                                                              #
################################################################################
proc check_field {args} {

    array set param $args

    set expected $param(-expected)
    set received_value $param(-received)
    set description $param(-description)
    set type $param(-type)

    if {[string match -nocase $expected "dont_care"]} {
        return 1
    }

    switch $type {
        "mac" {
            set status [check_mac\
                              -mac    $received_value\
                              -type   $expected]
        }

        "ip" {
            set status [check_ip\
                              -ip     $received_value\
                              -type   $expected]
        }

        "hex" {
            set status [check_value\
                              -value  $received_value\
                              -range  $expected\
                              -type   $type]

            #non-zero hex values
            if {[lsearch -nocase "non-zero" $expected] == -1} {
                set expected    0x$expected
            }
            set received_value  0x$received_value
        }

        default {
            set status [check_value\
                              -value  $received_value\
                              -range  $expected]

            if {$type == "dec2hex"} {
                set expected       0x[dec2hex $expected]
                set received_value 0x[dec2hex $received_value]
            }
        }
    }

    #update status when failed
    if {$status == 0} {
        uplevel set _check_field_status_ 0
    }

    print_check_log\
                -expected       $expected\
                -received       $received_value\
                -description    $description\
                -status         $status

    return $status

}


################################################################################
#  PROCEDURE NAME    : get_ptp_ha_fields                                       #
#                                                                              #
#  INPUT             :                                                         #
#                                                                              #
#  RETURNS           : list of fields                                          #
#                                                                              #
#  DEFINITION        : This function is used to get ptp ha packet fields       #
#                                                                              #
#  USAGE             :                                                         #
#                       get_ptp_ha_fields                                      #
#                                                                              #
################################################################################

proc get_ptp_ha_fields {args} {

    set ptp_ha_pkt_fields {dest_mac src_mac eth_type vlan_id vlan_priority vlan_dei ip_checksum ip_protocol src_ip dest_ip src_port dest_port udp_length udp_checksum major_sdo_id message_type minor_version version_ptp message_length domain_number minor_sdo_id alternate_master_flag two_step_flag unicast_flag profile_specific1 profile_specific2 secure leap61 leap59 current_utc_offset_valid ptp_timescale time_traceable freq_traceable sync_uncertain correction_field message_type_specific src_port_number src_clock_identity sequence_id control_field log_message_interval origin_timestamp_sec origin_timestamp_ns current_utc_offset gm_priority1 gm_priority2 gm_identity steps_removed time_source gm_clock_classy gm_clock_accuracy gm_clock_variance precise_origin_timestamp_sec precise_origin_timestamp_ns receive_timestamp_sec receive_timestamp_ns requesting_port_number requesting_clock_identity request_receipt_timestamp_sec request_receipt_timestamp_ns response_origin_timestamp_sec response_origin_timestamp_ns target_port_number target_clock_identity starting_boundary_hops boundary_hops action_field tlv tlv_type tlv_length ope cr rcr tcr ic irc itc fov pov tct phase_offset_tx phase_offset_tx_timestamp_sec phase_offset_tx_timestamp_ns freq_offset_tx freq_offset_tx_timestamp_sec freq_offset_tx_timestamp_ns management_id ext_port_config master_only source_port_identity target_port_identity}

    return $ptp_ha_pkt_fields
}

################################################################################
#  PROCEDURE NAME    : check_ptp_ha_pkt                                        #
#                                                                              #
#  INPUT             :                                                         #
#                                                                              #
#    pkt_content     : packet hex dump                                         #
#                                                                              #
#  RETURNS           : 0 on failure                                            #
#                    : 1 on success                                            #
#                                                                              #
#  DEFINITION        : This function is used to check the ptp ha packet        #
#                                                                              #
#  USAGE             :                                                         #
#                       check_ptp_ha_pkt\                                      #
#                                   -pkt_content       $pkt_content            #
#                                                                              #
################################################################################

proc check_ptp_ha_pkt {args} {

    array set param $args
    set packet $param(-packet)
    set packet [join $packet ""]

    #list of fields in ptp ha packet
    set ptp_ha_pkt_fields [get_ptp_ha_fields]

    foreach field $ptp_ha_pkt_fields {

        if {[info exists param(-$field)]} {
            set $field [set param(-$field)]
        } else {
            set $field "dont_care"
        }

    }

    pbLib::decode_ptp_ha_pkt\
                 -packet                         $packet\
                 -dest_mac                       recvd_dest_mac\
                 -src_mac                        recvd_src_mac\
                 -eth_type                       recvd_eth_type\
                 -vlan_id                        recvd_vlan_id\
                 -vlan_priority                  recvd_vlan_priority\
                 -vlan_dei                       recvd_vlan_dei\
                 -ip_checksum                    recvd_ip_checksum\
                 -ip_protocol                    recvd_ip_protocol\
                 -src_ip                         recvd_src_ip\
                 -dest_ip                        recvd_dest_ip\
                 -src_port                       recvd_src_port\
                 -dest_port                      recvd_dest_port\
                 -udp_length                     recvd_udp_length\
                 -udp_checksum                   recvd_udp_checksum\
                 -major_sdo_id                   recvd_major_sdo_id\
                 -message_type                   recvd_message_type\
                 -minor_version                  recvd_minor_version\
                 -version_ptp                    recvd_version_ptp\
                 -message_length                 recvd_message_length\
                 -domain_number                  recvd_domain_number\
                 -minor_sdo_id                   recvd_minor_sdo_id\
                 -alternate_master_flag          recvd_alternate_master_flag\
                 -two_step_flag                  recvd_two_step_flag\
                 -unicast_flag                   recvd_unicast_flag\
                 -profile_specific1              recvd_profile_specific1\
                 -profile_specific2              recvd_profile_specific2\
                 -secure                         recvd_secure\
                 -leap61                         recvd_leap61\
                 -leap59                         recvd_leap59\
                 -current_utc_offset_valid       recvd_current_utc_offset_valid\
                 -ptp_timescale                  recvd_ptp_timescale\
                 -time_traceable                 recvd_time_traceable\
                 -freq_traceable                 recvd_freq_traceable\
                 -sync_uncertain                 recvd_sync_uncertain\
                 -correction_field               recvd_correction_field\
                 -message_type_specific          recvd_message_type_specific\
                 -src_port_number                recvd_src_port_number\
                 -src_clock_identity             recvd_src_clock_identity\
                 -sequence_id                    recvd_sequence_id\
                 -control_field                  recvd_control_field\
                 -log_message_interval           recvd_log_message_interval\
                 -origin_timestamp_sec           recvd_origin_timestamp_sec\
                 -origin_timestamp_ns            recvd_origin_timestamp_ns\
                 -current_utc_offset             recvd_current_utc_offset\
                 -gm_priority1                   recvd_gm_priority1\
                 -gm_priority2                   recvd_gm_priority2\
                 -gm_identity                    recvd_gm_identity\
                 -steps_removed                  recvd_steps_removed\
                 -time_source                    recvd_time_source\
                 -gm_clock_classy                recvd_gm_clock_classy\
                 -gm_clock_accuracy              recvd_gm_clock_accuracy\
                 -gm_clock_variance              recvd_gm_clock_variance\
                 -precise_origin_timestamp_sec   recvd_precise_origin_timestamp_sec\
                 -precise_origin_timestamp_ns    recvd_precise_origin_timestamp_ns\
                 -receive_timestamp_sec          recvd_receive_timestamp_sec\
                 -receive_timestamp_ns           recvd_receive_timestamp_ns\
                 -requesting_port_number         recvd_requesting_port_number\
                 -requesting_clock_identity      recvd_requesting_clock_identity\
                 -request_receipt_timestamp_sec  recvd_request_receipt_timestamp_sec\
                 -request_receipt_timestamp_ns   recvd_request_receipt_timestamp_ns\
                 -response_origin_timestamp_sec  recvd_response_origin_timestamp_sec\
                 -response_origin_timestamp_ns   recvd_response_origin_timestamp_ns\
                 -target_port_number             recvd_target_port_number\
                 -target_clock_identity          recvd_target_clock_identity\
                 -starting_boundary_hops         recvd_starting_boundary_hops\
                 -boundary_hops                  recvd_boundary_hops\
                 -action_field                   recvd_action_field\
                 -tlv                            recvd_tlv\
                 -tlv_type                       recvd_tlv_type\
                 -tlv_length                     recvd_tlv_length\
                 -ope                            recvd_ope\
                 -cr                             recvd_cr\
                 -rcr                            recvd_rcr\
                 -tcr                            recvd_tcr\
                 -ic                             recvd_ic\
                 -irc                            recvd_irc\
                 -itc                            recvd_itc\
                 -fov                            recvd_fov\
                 -pov                            recvd_pov\
                 -tct                            recvd_tct\
                 -phase_offset_tx                recvd_phase_offset_tx\
                 -phase_offset_tx_timestamp_sec  recvd_phase_offset_tx_timestamp_sec\
                 -phase_offset_tx_timestamp_ns   recvd_phase_offset_tx_timestamp_ns\
                 -freq_offset_tx                 recvd_freq_offset_tx\
                 -freq_offset_tx_timestamp_sec   recvd_freq_offset_tx_timestamp_sec\
                 -freq_offset_tx_timestamp_ns    recvd_freq_offset_tx_timestamp_ns\
                 -management_id                  recvd_management_id\
                 -ext_port_config                recvd_ext_port_config\
                 -master_only                    recvd_master_only\
                 -ip_checksum_expected           ip_checksum_expected\
                 -ip_checksum_status             ip_checksum_status\
                 -udp_checksum_expected          udp_checksum_expected\
                 -udp_checksum_status            udp_checksum_status

    #This field is updated as 0 when any of the field check has failed
    set _check_field_status_ 1

    check_field\
                -expected       $dest_mac\
                -received       $recvd_dest_mac\
                -description    "Destination MAC"\
                -type           "mac"

    check_field\
                -expected       $src_mac\
                -received       $recvd_src_mac\
                -description    "Source MAC"\
                -type           "mac"

    check_field\
                -expected       $eth_type\
                -received       $recvd_eth_type\
                -description    "Ethtype"\
                -type           "hex"

    check_field\
                -expected       $vlan_id\
                -received       $recvd_vlan_id\
                -description    "VLAN ID"\
                -type           "dec"


    check_field\
                -expected       $vlan_priority\
                -received       $recvd_vlan_priority\
                -description    "VLAN Priority"\
                -type           "dec"


    check_field\
                -expected       $vlan_dei\
                -received       $recvd_vlan_dei\
                -description    "VLAN DEI bit"\
                -type           "bit"

    if [string match -nocase $ip_checksum "valid"] {
        set expected_checksum $ip_checksum_expected
    } else {
        set expected_checksum $ip_checksum
    }

    check_field\
                -expected       $expected_checksum\
                -received       $recvd_ip_checksum\
                -description    "IP Checksum"\
                -type           "hex"

    check_field\
                -expected       $ip_protocol\
                -received       $recvd_ip_protocol\
                -description    "IP Protocol ID"\
                -type           "dec"

    check_field\
                -expected       $src_ip\
                -received       $recvd_src_ip\
                -description    "Source IP"\
                -type           "ip"

    check_field\
                -expected       $dest_ip\
                -received       $recvd_dest_ip\
                -description    "Destination IP"\
                -type           "ip"

    check_field\
                -expected       $src_port\
                -received       $recvd_src_port\
                -description    "Source Port"\
                -type           "dec"

    check_field\
                -expected       $dest_port\
                -received       $recvd_dest_port\
                -description    "Destination Port"\
                -type           "dec"

    check_field\
                -expected       $udp_length\
                -received       $recvd_udp_length\
                -description    "UDP Length"\
                -type           "dec"

    if [string match -nocase $udp_checksum "valid"] {

        print_check_log\
                -expected       0x$udp_checksum_expected\
                -received       0x$recvd_udp_checksum\
                -description    "UDP Checksum"\
                -status         $udp_checksum_status

    } else {

        check_field\
                -expected       $udp_checksum\
                -received       $recvd_udp_checksum\
                -description    "UDP Checksum"\
                -type           "hex"

    }

    check_field\
                -expected       $major_sdo_id\
                -received       $recvd_major_sdo_id\
                -description    "PTP Major SDO ID"\
                -type           "dec"

    check_field\
                -expected       $message_type\
                -received       $recvd_message_type\
                -description    "PTP Message Type"\
                -type           "dec"

    check_field\
                -expected       $minor_version\
                -received       $recvd_minor_version\
                -description    "PTP Minor version"\
                -type           "dec"

    check_field\
                -expected       $version_ptp\
                -received       $recvd_version_ptp\
                -description    "PTP version"\
                -type           "dec"

    check_field\
                -expected       $message_length\
                -received       $recvd_message_length\
                -description    "PTP Message Length"\
                -type           "dec"

    check_field\
                -expected       $domain_number\
                -received       $recvd_domain_number\
                -description    "PTP Domain Number"\
                -type           "dec"

    check_field\
                -expected       $minor_sdo_id\
                -received       $recvd_minor_sdo_id\
                -description    "PTP Minor SDO ID"\
                -type           "dec"

    check_field\
                -expected       $alternate_master_flag\
                -received       $recvd_alternate_master_flag\
                -description    "PTP Alternate Master Flag"\
                -type           "bit"

    check_field\
                -expected       $two_step_flag\
                -received       $recvd_two_step_flag\
                -description    "PTP Two Step Flag"\
                -type           "bit"

    check_field\
                -expected       $unicast_flag\
                -received       $recvd_unicast_flag\
                -description    "PTP Unicast Flag"\
                -type           "bit"

    check_field\
                -expected       $profile_specific1\
                -received       $recvd_profile_specific1\
                -description    "PTP Profile specific 1 Flag"\
                -type           "bit"

    check_field\
                -expected       $profile_specific2\
                -received       $recvd_profile_specific2\
                -description    "PTP Profile specific 2 Flag"\
                -type           "bit"

    check_field\
                -expected       $secure\
                -received       $recvd_secure\
                -description    "PTP Secure Flag"\
                -type           "bit"

    check_field\
                -expected       $leap61\
                -received       $recvd_leap61\
                -description    "PTP leap61 Flag"\
                -type           "bit"

    check_field\
                -expected       $leap59\
                -received       $recvd_leap59\
                -description    "PTP leap59 Flag"\
                -type           "bit"

    check_field\
                -expected       $current_utc_offset_valid\
                -received       $recvd_current_utc_offset_valid\
                -description    "PTP current utc offset valid Flag"\
                -type           "bit"

    check_field\
                -expected       $ptp_timescale\
                -received       $recvd_ptp_timescale\
                -description    "PTP Time scale Flag"\
                -type           "bit"

    check_field\
                -expected       $time_traceable\
                -received       $recvd_time_traceable\
                -description    "PTP Time traceable Flag"\
                -type           "bit"

    check_field\
                -expected       $freq_traceable\
                -received       $recvd_freq_traceable\
                -description    "PTP Frequency traceable Flag"\
                -type           "bit"

    check_field\
                -expected       $sync_uncertain\
                -received       $recvd_sync_uncertain\
                -description    "PTP synchronization uncertain Flag"\
                -type           "bit"

    check_field\
                -expected       $correction_field\
                -received       $recvd_correction_field\
                -description    "PTP Correction Field"\
                -type           "dec"

    check_field\
                -expected       $message_type_specific\
                -received       $recvd_message_type_specific\
                -description    "PTP Message Type Specific"\
                -type           "dec"

    check_field\
                -expected       $src_port_number\
                -received       $recvd_src_port_number\
                -description    "PTP Source port number"\
                -type           "dec"

    check_field\
                -expected       $src_clock_identity\
                -received       $recvd_src_clock_identity\
                -description    "PTP Source clock identity"\
                -type           "dec"

    check_field\
                -expected       $source_port_identity\
                -received       [pbLib::encode_port_identity $recvd_src_clock_identity $recvd_src_port_number]\
                -description    "PTP Source Port identity"\
                -type           "hex"

    check_field\
                -expected       $sequence_id\
                -received       $recvd_sequence_id\
                -description    "PTP Sequence ID"\
                -type           "dec"

    check_field\
                -expected       $control_field\
                -received       $recvd_control_field\
                -description    "PTP Control field"\
                -type           "dec"

    check_field\
                -expected       $log_message_interval\
                -received       $recvd_log_message_interval\
                -description    "PTP Log Message interval"\
                -type           "dec"

    check_field\
                -expected       $current_utc_offset\
                -received       $recvd_current_utc_offset\
                -description    "PTP Current UTC Offset"\
                -type           "dec"

    check_field\
                -expected       $gm_priority1\
                -received       $recvd_gm_priority1\
                -description    "PTP Grandmaster priority 1"\
                -type           "dec"

    check_field\
                -expected       $gm_priority2\
                -received       $recvd_gm_priority2\
                -description    "PTP Grandmaster priority 2"\
                -type           "dec"

    check_field\
                -expected       $gm_identity\
                -received       $recvd_gm_identity\
                -description    "PTP Grandmaster Identity"\
                -type           "dec"

    check_field\
                -expected       $steps_removed\
                -received       $recvd_steps_removed\
                -description    "PTP Steps removed"\
                -type           "dec"

    check_field\
                -expected       $time_source\
                -received       $recvd_time_source\
                -description    "PTP Time source"\
                -type           "dec"

    check_field\
                -expected       $steps_removed\
                -received       $recvd_steps_removed\
                -description    "PTP Steps removed"\
                -type           "dec"

    check_field\
                -expected       $time_source\
                -received       $recvd_time_source\
                -description    "PTP Time source"\
                -type           "dec"

    check_field\
                -expected       $gm_clock_classy\
                -received       $recvd_gm_clock_classy\
                -description    "PTP Grandmaster clock class"\
                -type           "dec"

    check_field\
                -expected       $gm_clock_accuracy\
                -received       $recvd_gm_clock_accuracy\
                -description    "PTP Grandmaster clock accuracy"\
                -type           "dec"

    check_field\
                -expected       $gm_clock_variance\
                -received       $recvd_gm_clock_variance\
                -description    "PTP Grandmaster clock variance"\
                -type           "dec"

    check_field\
                -expected       $requesting_port_number\
                -received       $recvd_requesting_port_number\
                -description    "PTP Requesting port number"\
                -type           "dec"

    check_field\
                -expected       $requesting_clock_identity\
                -received       $recvd_requesting_clock_identity\
                -description    "PTP Requesting clock identity"\
                -type           "dec"

    check_field\
                -expected       $target_port_number\
                -received       $recvd_target_port_number\
                -description    "PTP Target port number"\
                -type           "dec"

    check_field\
                -expected       $target_clock_identity\
                -received       $recvd_target_clock_identity\
                -description    "PTP Target clock identity"\
                -type           "dec"

    check_field\
                -expected       $target_port_identity\
                -received       [pbLib::encode_port_identity $recvd_target_clock_identity $recvd_target_port_number]\
                -description    "PTP Target Port identity"\
                -type           "hex"

    check_field\
                -expected       $starting_boundary_hops\
                -received       $recvd_starting_boundary_hops\
                -description    "PTP Starting boundary hops"\
                -type           "dec"

    check_field\
                -expected       $boundary_hops\
                -received       $recvd_boundary_hops\
                -description    "PTP Boundary hops"\
                -type           "dec"

    check_field\
                -expected       $action_field\
                -received       $recvd_action_field\
                -description    "PTP Action field"\
                -type           "dec"

    check_field\
                -expected       $tlv\
                -received       $recvd_tlv\
                -description    "PTP TLV Flag"\
                -type           "dec"

    check_field\
                -expected       $tlv_type\
                -received       $recvd_tlv_type\
                -description    "PTP Tlv type"\
                -type           "dec"

    check_field\
                -expected       $tlv_length\
                -received       $recvd_tlv_length\
                -description    "PTP Tlv length"\
                -type           "dec"

    check_field\
                -expected       $ope\
                -received       $recvd_ope\
                -description    "PTP OPE Flag"\
                -type           "dec"

    check_field\
                -expected       $cr\
                -received       $recvd_cr\
                -description    "PTP CR Flag"\
                -type           "dec"

    check_field\
                -expected       $rcr\
                -received       $recvd_rcr\
                -description    "PTP RCR Flag"\
                -type           "dec"

    check_field\
                -expected       $tcr\
                -received       $recvd_tcr\
                -description    "PTP TCR Flag"\
                -type           "dec"

    check_field\
                -expected       $ic\
                -received       $recvd_ic\
                -description    "PTP IC Flag"\
                -type           "dec"

    check_field\
                -expected       $irc\
                -received       $recvd_irc\
                -description    "PTP IRC Flag"\
                -type           "dec"

    check_field\
                -expected       $itc\
                -received       $recvd_itc\
                -description    "PTP ITC Flag"\
                -type           "dec"

    check_field\
                -expected       $fov\
                -received       $recvd_fov\
                -description    "PTP FOV Flag"\
                -type           "dec"

    check_field\
                -expected       $pov\
                -received       $recvd_pov\
                -description    "PTP POV Flag"\
                -type           "dec"

    check_field\
                -expected       $tct\
                -received       $recvd_tct\
                -description    "PTP TCT Flag"\
                -type           "dec"

    check_field\
                -expected       $phase_offset_tx\
                -received       $recvd_phase_offset_tx\
                -description    "PTP Phase offset tx"\
                -type           "dec"

    check_field\
                -expected       $phase_offset_tx_timestamp_sec\
                -received       $recvd_phase_offset_tx_timestamp_sec\
                -description    "PTP Phase offset tx timestamp (s)"\
                -type           "dec"

    check_field\
                -expected       $phase_offset_tx_timestamp_ns\
                -received       $recvd_phase_offset_tx_timestamp_ns\
                -description    "PTP Phase offset tx timestamp (ns)"\
                -type           "dec"

    check_field\
                -expected       $freq_offset_tx\
                -received       $recvd_freq_offset_tx\
                -description    "PTP Freq offset tx"\
                -type           "dec"

    check_field\
                -expected       $freq_offset_tx_timestamp_sec\
                -received       $recvd_freq_offset_tx_timestamp_sec\
                -description    "PTP Freq offset tx timestamp (s)"\
                -type           "dec"

    check_field\
                -expected       $freq_offset_tx_timestamp_ns\
                -received       $recvd_freq_offset_tx_timestamp_ns\
                -description    "PTP Freq offset tx timestamp (ns)"\
                -type           "dec"

    check_field\
                -expected       $management_id\
                -received       $recvd_management_id\
                -description    "PTP Management id"\
                -type           "dec"

    check_field\
                -expected       $ext_port_config\
                -received       $recvd_ext_port_config\
                -description    "PTP Ext port config"\
                -type           "dec"

    check_field\
                -expected       $master_only\
                -received       $recvd_master_only\
                -description    "PTP Master only"\
                -type           "dec"


    return $_check_field_status_

}

################################################################################
#  PROCEDURE NAME    : size_of                                                 #
#                                                                              #
#  INPUT             :                                                         #
#                                                                              #
#    pkt_content     : packet hex dump                                         #
#    extra           : additional bytes to add (optional)                      #
#                                                                              #
#  RETURNS           : size of hex in bytes                                    #
#                                                                              #
#  DEFINITION        : This function is used to return size of packet in bytes #
#                                                                              #
#  USAGE             :                                                         #
#                       size_of $hex                                           #
#                                                                              #
################################################################################

proc size_of {hex {extra 0}} {

    set hex [join $hex ""]
    return [expr ([string length $hex] / 2) + $extra]

}

################################################################################
#  PROCEDURE NAME    : prepend                                                 #
#                                                                              #
#  INPUT             :                                                         #
#                                                                              #
#    varName         : variable name                                           #
#    value           : value to be prepended to the variable                   #
#                                                                              #
#  RETURNS           : prepended value                                         #
#                                                                              #
#  DEFINITION        : prepends strings to the value stored in a variable      #
#                                                                              #
#  USAGE             :                                                         #
#                       append varName ?value?                                 #
#                                                                              #
################################################################################

proc prepend {s_var txt} {

    upvar 1 $s_var s
    if ![info exists s] {set s ""}
    set s "${txt}${s}"

    return $s
}

################################################################################
#  PROCEDURE NAME    : get_value_from_array                                    #
#                                                                              #
#  INPUT             :                                                         #
#                                                                              #
#    list            : list which has key value pair                           #
#                                                                              #
#    key             : key whose value should be returned                      #
#                                                                              #
#    default         : default value if the key is not present (Unknown)       #
#                                                                              #
#  OUTPUT            :                                                         #
#                                                                              #
#    value           : value of the given key                                  #
#                                                                              #
#  RETURNS           : value                                                   #
#                                                                              #
#  DEFINITION        : returns value of the given key in the given array       #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#           get_value_from_array\                                              #
#                           -list       $List\                                 #
#                           -key        $key\                                  #
#                           -value      value\                                 #
#                           -default    "reserved"                             #
#                                                                              #
################################################################################

proc get_value_from_array {args} {

    array set param $args

    array set arr $param(-list)
    set key $param(-key)

    if [info exists param(-value)] {
        upvar $param(-value) value
    }

    if [info exists param(-default)] {
        set value $param(-default)
    } else {
        set value "Unknown"
    }

    if [info exists arr($key)] {
        set value "[set arr($key)]"
    }

    set value "$value ($key)"

    return $value
}

################################################################################
#  PROCEDURE NAME    : get_clock_identity                                      #
#                                                                              #
#  INPUT             :                                                         #
#     mac_address    : (Mandatory) Source mac address                          #
#                                                                              #
#     method         : (Optional)  Format used to derive Clock identity.       #
#                                  Should be "EUI-48", "IEEE EUI-64" or        #
#                                  "Non-IEEE EUI-64"  (Default: EUI-48)        #
#                                                                              #
#  OUTPUT            : clock_identity                                          #
#                                                                              #
#  DEFINITION        : This function is used to get the clock_identity of the  #
#                      mac_address                                             #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#               get_clock_identity\                                            #
#                           -mac            $src_mac\                          #
#                           -format         $::ptp_clock_identity_format\      #
#                           -clock_identity src_clock_identity                 #
#                                                                              #
################################################################################

proc get_clock_identity {args} {

    array set param $args

    set mac [mac2hex $param(-mac)]

    if [info exists param(-format)] {
        set method $param(-format)
    } else {
        set method "EUI-48"
    }

    if [info exists param(-clock_identity)] {
        upvar $param(-clock_identity) clock_identity
        set clock_identity ""
    }

    set supported_methods [list "EUI-48" "IEEE EUI-64" "Non-IEEE EUI-64"]

    if {[lsearch $supported_methods $method] == -1} {
        return 0
    }

    switch $method {

        "EUI-48" {
            set id_1 [string range $mac 0 5] 
            set id_2 fffe
            set id_3 [string range $mac 6 11]
            append clock_identity 0x $id_1 $id_2 $id_3
        }

        "IEEE EUI-64" {
            set id_1 [string range $mac 0 5]
            append clock_identity 0x $id_1 0123456789
        }

        "Non-IEEE EUI-64" {
            set id_1 ff05
            append clock_identity 0x ff05 $mac
        }

    }
    return 1
}


################################################################################
#  PROCEDURE NAME    : get_dest_mac                                            #
#                                                                              #
#  INPUT             :                                                         #
#     port           : (Mandatory) Source port number                          #
#                                                                              #
#     message        : (Mandatory) PTP Message type                            #
#                                                                              #
#                                                                              #
#  OUTPUT            :                                                         #
#     mac            : (Optional)  Destination MAC address                     #
#                                                                              #
#  DEFINITION        : This function is used to get the destination MAC address#
#                      to be used in testscripts                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#               get_dest_mac\                                                  #
#                           -port           $port\                             #
#                           -message        $message\                          #
#                           -mac            dest_mac                           #
#                                                                              #
################################################################################
proc get_dest_mac {args} {

    array set param $args

    set port_num $param(-port_num)
    set message  $param(-message)

    if {[info exists param(-mac)]} {
        upvar $param(-mac) dest_mac
    }

    if {[string match -nocase $::ptp_comm_model "Unicast"]} {
        set dest_mac [set ::NEXT_HOP_MAC($port_num)]

    } else {
        set dest_mac [set ::PTP_HA_MAC($message)]

    }

    return $dest_mac 
}


################################################################################
#  PROCEDURE NAME    : get_dest_ip                                             #
#                                                                              #
#  INPUT             :                                                         #
#     port           : (Mandatory) Source port number                          #
#                                                                              #
#     message        : (Mandatory) PTP Message type                            #
#                                                                              #
#                                                                              #
#  OUTPUT            :                                                         #
#     ip             : (Optional)  Destination IP address                      #
#                                                                              #
#  DEFINITION        : This function is used to get the destination IP address #
#                      to be used in testscripts                               #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#               get_dest_ip\                                                   #
#                           -port           $port\                             #
#                           -message        $message\                          #
#                           -ip             dest_ip                            #
#                                                                              #
################################################################################
proc get_dest_ip {args} {

    array set param $args

    set port_num $param(-port_num)
    set message  $param(-message)

    if {[info exists param(-ip)]} {
        upvar $param(-ip) dest_ip
    }

    if {[string match -nocase $::ptp_comm_model "Unicast"]} {
        set dest_ip [set ::NEXT_HOP_IP($port_num)]

    } else {
        set dest_ip [set ::PTP_HA_IP($message)]

    }

    return $dest_ip 
}

################################################################################
#  PROCEDURE NAME    : digit_separator                                         #
#                                                                              #
#  INPUT             :                                                         #
#     number         : (Mandatory) Number                                      #
#                                                                              #
#     separator      : (Optional)  Character as separator.                     #
#                                  Default: ","                                #
#                                                                              #
#  RETURN            : Number with separator.                                  #
#                                                                              #
#  DEFINITION        : This function returns number with separator.            #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      digit_separator $number $separator                      #
#                                                                              #
################################################################################

proc digit_separator {number {separator ,}} {
    while {[regsub {^([-+]?\d+)(\d\d\d)} $number "\\1$separator\\2" number]} {}
    return $number
}

################################################################################
#  PROCEDURE NAME    : nano                                                    #
#                                                                              #
#  INPUT             :                                                         #
#     number         : (Mandatory) Number                                      #
#                                                                              #
#  RETURN            : Number in nano format with space in left side.          #
#                                                                              #
#  DEFINITION        : This function returns number with space in left side    #
#                                                                              #
#  USAGE             :                                                         #
#                                                                              #
#                      nano $number                                            #
#                                                                              #
################################################################################

proc nano {number} {
    set number [format "%27s" [digit_separator $number]]
    return $number
}

