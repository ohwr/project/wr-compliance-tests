#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_ptp-ha_pcg_013                                  #
# Test Case Version : 1.3                                                     #
# Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    #
# Module Name       : PTP-HA Configuration Group (PCG)                        #
#                                                                             #
# Title             : No-updation of data set based on Announce message when  #
#                     portstate is masterOnly                                 #
#                                                                             #
# Purpose           : To verify that a PTP enabled device does not update data#
#                     set from received Announce message when port state is   #
#                     configured as masterOnly.                               #
#                                                                             #
# Reference         : P1588/D1.3, February 2018 V3.01 Section 9.2.2.2 Page 139#
#                                                                             #
# Conformance Type  : SHALL                                                   #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 003                                                     #
# Test Topology     : 004                                                     #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                                        <Enable PTP> | P2        #
#           |                          <Enable PTP with BC clock> |           #
#           |                    <Clock mode - one-step/two-step> |           #
#           |                     <Configure logAnnounceInterval, | P1        #
#           |                             announceReceiptTimeout> |           #
#           |                         <Configure logSyncInterval> | P1        #
#           |                  <Configure logMinDelayReqInterval> | P1        #
#           |                    <Configure Priority1, Priority2> | P1        #
#           |                                     <Enable L1SYNC> | P1        #
#           |                                     <Enable L1SYNC> | P2        #
#           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        #
#           |                     <Disable L1SynOptParams option> | P1        #
#           |           <Enable asymmetryCorrectionPortDS.enable> | P1        #
#           | <Configure timestampCorrectionPortDS.egressLatency, | P1        #
#           |           timestampCorrectionPortDS.ingressLatency, |           #
#           |        asymmetryCorrectionPortDS.constantAsymmetry, |           #
#           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           #
#           |                                                     |           #
#           |             <Configure PTP Port State = masterOnly> | P1        #
#           |                                                     |           #
#           |                       <Check Port Status = MASTER>  | P1        #
#           |                                                     |           #
#           |                         ANNOUNCE [MSG_TYPE = 0x0B,  |           #
#           |                       PRI1 = X, PRI2 = Y, DN = DN1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | ANNOUNCE [MSG_TYPE = 0x0B,                          |           #
#           |  PRI1 = X-1, PRI2 = Y-1, DN = DN1]                  |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |          < Wait for 6s to complete BMCA >           |           #
#           |                                                     |           #
#           | PTP SIGNALING with L1 Sync TLV                      |           #
#           | [MSG_TYPE = 0xC, DN = DN1,                          |           #
#           | TLV_TYPE = 0x8001, TCR = 1,                         |           #
#           | RCR = 1, CR = 1, ITC = 1,                           |           #
#           | IRC = 1, IC = 1]                                    |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                         ANNOUNCE [MSG_TYPE = 0x0B,  |           #
#           |                       PRI1 = X, PRI2 = Y, DN = DN1] |           #
#        T2 |-------------------------------------------------<<--| P2        #
#           |                                                     |           #
#           |             <Disable the master Only configuration> | P1        #
#           |                                                     |           #
#           |          < Wait for 6s to complete BMCA >           |           #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                   PRI1 = X-1, PRI2 = Y-1, DN = DN1] |           #
#        T2 |-------------------------------------------------<<--| P2        #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     MSG_TYPE  = Message Type                                                #
#     DN        = Domain Number                                               #
#     PRI       = Priority                                                    #
#                                                                             #
# NOTE :                                                                      #
#                                                                             #
#  1. This objective is verified using the High Accuracy Delay Request-       #
#     Response Default PTP Profile                                            #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's ports P1 and P2.                                      #
#    ii.   Enable PTP on ports P1 and P2.                                     #
#   iii.   Enable PTP globally with device type as Boundary clock.            #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's ports P1 and P2.                            #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.ingressLatency,           #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add ports T1 and T2 at TEE.                                        #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Configure PTP port state as masterOnly on Port P1.                 #
#                                                                             #
# Step 4 : Observe that the port status of P1 in DUT is in MASTER state.      #
#                                                                             #
# Step 5 : Observe that DUT transmits ANNOUNCE message on port P1             #
#          with following parameters.                                         #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type  = 0x0B                                      #
#                   Domain Number = DN1                                       #
#                   Priority1     = X                                         #
#                   Priority2     = Y                                         #
#                                                                             #
# Step 6 : Send periodic ANNOUNCE message with following parameters.          #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  Priority1          = X-1                                   #
#                  Priority2          = Y-1                                   #
#                  ANN_INT            = 1                                     #
#                                                                             #
# Step 7 : Wait for 6s for completing BMCA.                                   #
#                                                                             #
# Step 8 : Send periodic PTP SIGNALING message with L1 Sync TLV on the port P1#
#          with following parameters:                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 1                                          #
#                  RCR           = 1                                          #
#                  CR            = 1                                          #
#                  ITC           = 1                                          #
#                  IRC           = 1                                          #
#                  IC            = 1                                          #
#                                                                             #
# Step 9 : Observe that DUT transmits ANNOUNCE message on port P2 with        #
#          following parameters.                                              #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type  = 0x0B                                      #
#                   Domain Number = DN1                                       #
#                   Priority1     = X                                         #
#                   Priority2     = Y                                         #
#                                                                             #
# Step 10: Disable masteronly configuration on port P1.                       #
#                                                                             #
# Step 11: Verify that DUT transmits ANNOUNCE message on port P2 with         #
#          following parameters.                                              #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type  = 0x0B                                      #
#                   Domain Number = DN1                                       #
#                   Priority1     = X-1                                       #
#                   Priority2     = Y-1                                       #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          May/2018      CERN          Initial                            #
# 1.1          Jul/2018      CERN          Added ptp_ha::display_port_details #
#                                          to print port details              #
# 1.2          Sep/2018      CERN          a) Removed destination MAC and IP  #
#                                             in send functions.              #
#                                          b) Removed timeout in recv         #
#                                             functions.                      #
#                                          c) Moved timeout calculation to    #
#                                             PTP-HA global file.             #
#                                          d) Corrected fields in Announce    #
#                                             message sent from TEE.          #
# 1.3          Oct/2018      CERN          a) Added steps to make sure L1Sync #
#                                             state L1_SYNC_UP.               #
#                                          b) Added wait to complete BMCA     #
#                                             before checking the state is    #
#                                             MASTER.                         #
#                                                                             #
###############################################################################
# Copyright (c) 2018 CERN                                                     #
###############################################################################

global dut_log_msg
global tee_log_msg
global session_id

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def "To verify that a PTP enabled device does not update data\
            set from received Announce message when port state is\
            configured as masterOnly."

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set tee_port_2                     $::tee_port_num_2
set dut_port_1                     $::dut_port_num_1
set dut_port_2                     $::dut_port_num_2
set vlan_id_1                      $::ptp_ha_vlan_id
set vlan_id_2                      $::ptp_ha_vlan_id_2
set port_list                      "$::tee_port_num_1 $::tee_port_num_2" 

set dut_ip_1                       $::dut_test_port_ip

set dut_ip_2                       $::dut_test_port_ip_2

set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT

set domain                         $::DEFAULT_DOMAIN
set gmid                           $::GRANDMASTER_ID
set infinity                       $::INFINITY
set tlv_type(l1sync)               $::TLV_TYPE(L1SYNC)
set master                         $::PTP_PORT_STATE(MASTER)

set ann_int                        1
set sequence_id_ann                1
set sequence_id_l1sync             43692
set ann_int                        1

set tcr_true        1
set rcr_true        1
set cr_true         1
set itc_true        1
set irc_true        1
set ic_true         1

set tcr_false       0
set rcr_false       0
set cr_false        0
set itc_false       0
set irc_false       0
set ic_false        0

########################### END - INITIALIZATION ##############################

if {$::ptp_device_type != "Boundary Clock"} {

    TC_ABORT -reason "This test case is applicable only for Boundary clock"\
             -tc_def  $tc_def

}

ptp_ha::display_port_details\
                -port_list  $port_list 

ptp_ha::dut_Initialization

proc cleanup {} {

    ptp_ha::dut_masteronly_disable \
              -port_num        $::dut_port_num_1

    ptp_ha::dut_cleanup_setup_003

}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's ports P1 and P2.                                      #
#    ii.   Enable PTP on ports P1 and P2.                                     #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's ports P1 and P2.                            #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.ingressLatency,           #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
#                                                                             #
###############################################################################

STEP 1

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP3)"

if {![ptp_ha::dut_configure_setup_003]} {

    LOG -msg "$dut_log_msg(INIT_SETUP3_F)"

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP3_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 2 : Initialization of TEE                                              #
#     i.   Add ports T1 and T2 at TEE.                                        #
#                                                                             #
###############################################################################

STEP 2

LOG -level 0 -msg "$tee_log_msg(INIT)"

ptp_ha::Initialization\
        -port_list   $port_list

set tee_mac_1       $::TEE_MAC
set tee_ip_1        $::tee_test_port_ip

set tee_mac_2       $::TEE_MAC_2
set tee_ip_2        $::tee_test_port_ip_2

for {set i 1} {$i<=2} {incr i} {

    if {$ptp_comm_model == "Unicast"} {

        if {![PTP_HA_UNICAST_PRE_REQUISITE_CONFIGURATION\
                -port_num       [set tee_port_$i]\
                -tee_mac        [set tee_mac_$i]\
                -tee_ip         [set tee_ip_$i]\
                -dut_ip         [set dut_ip_$i]\
                -vlan_id        [set vlan_id_$i]\
                -dut_mac        tee_dest_mac_$i\
                -timeout        $arp_timeout]} {


                TEE_CLEANUP
                TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
                                   -tc_def $tc_def

                return 0
        }

    }

}

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_1\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id_1\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def $tc_def

    return 0
}

set filter_id1 [lindex $id1 end]

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_2\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id_2\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def $tc_def

    return 0
}


set filter_id2 [lindex $id1 end]

if { $::ptp_ha_vlan_encap != "true" } {
    set vlan_id_2 "dont_care"
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_2

#(Part 1)#

###############################################################################
# Step 3 : Configure PTP port state as masterOnly on Port P1.                 #
#                                                                             #
###############################################################################

STEP 3

LOG -level 0 -msg "$dut_log_msg(CONFIG_MO)"

  if {![ptp_ha::dut_masteronly_enable \
                -port_num             $dut_port_1]} {

      TEE_CLEANUP

      TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_MO_F)"\
                         -tc_def  $tc_def

      return 0
}

###############################################################################
# Step 4 : Observe that the port status of P1 in DUT is in MASTER state.      #
#                                                                             #
###############################################################################

STEP 4

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_MASTER_P1_O)"

sleep [expr $::dut_config_delay + $::ptp_ha_bmca]

if {![ptp_ha::dut_check_ptp_port_state \
              -port_num                $dut_port_1\
              -port_state              $master] } {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_MASTER_P1_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 5 : Observe that DUT transmits ANNOUNCE message on port P1             #
#          with following parameters.                                         #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type  = 0x0B                                      #
#                   Domain Number = DN1                                       #
#                   Priority1     = X                                         #
#                   Priority2     = Y                                         #
###############################################################################

STEP 5

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
             -port_num               $tee_port_1\
             -filter_id              $filter_id1\
             -domain_number          $domain\
             -recvd_gm_priority1     priority11\
             -recvd_gm_priority2     priority12]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_2

###############################################################################
#                                                                             #
# Step 6 : Send periodic ANNOUNCE message with following parameters.          #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  Priority1          = X-1                                   #
#                  Priority2          = Y-1                                   #
#                  ANN_INT            = 1                                     #
###############################################################################

STEP 6

set priority11 [ expr $priority11 - 1 ]
set priority12 [ expr $priority12 - 1 ]

LOG -level 0 -msg "$tee_log_msg(ANN_TX_T1)"

if {![ptp_ha::send_announce \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac_1\
           -src_ip                  $tee_ip_1\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -count                   $infinity\
           -gm_priority1            $priority11\
           -gm_priority2            $priority12\
           -gmid                    $gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANN_TX_T1_F)"\
                        -tc_def $tc_def
    return 0
}

###############################################################################
#                                                                             #
# Step 7 : Wait for 6s for completing BMCA.                                   #
#                                                                             #
###############################################################################

STEP 7

LOG -level 0 -msg "$dut_log_msg(WAIT_BMCA)"

sleep $::ptp_ha_bmca

###############################################################################
# Step 8 : Send periodic PTP SIGNALING message with L1 Sync TLV on the port P1#
#          with following parameters:                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 1                                          #
#                  RCR           = 1                                          #
#                  CR            = 1                                          #
#                  ITC           = 1                                          #
#                  IRC           = 1                                          #
#                  IC            = 1                                          #
###############################################################################

STEP 8

LOG -level 0 -msg   "$tee_log_msg(L1SYNC_TX_T1)"

if {![ptp_ha::send_signal \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac_1\
           -tlv_type                $tlv_type(l1sync)\
           -src_ip                  $tee_ip_1\
           -domain_number           $domain\
           -tcr                     $tcr_true\
           -rcr                     $rcr_true\
           -cr                      $cr_true\
           -itc                     $itc_true\
           -irc                     $irc_true\
           -ic                      $ic_true\
           -count                   $infinity\
           -sequence_id             $sequence_id_l1sync]} {

    TEE_CLEANUP
    
    TC_CLEAN_AND_ABORT   -reason "$tee_log_msg(L1SYNC_TX_T1_F)"\
                         -tc_def  $tc_def

    return 0
}

sleep $::ptp_ha_l1sync_wait

###############################################################################
# Step 9 : Observe that DUT transmits ANNOUNCE message on port P2 with        #
#          following parameters.                                              #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type  = 0x0B                                      #
#                   Domain Number = DN1                                       #
#                   Priority1     = X                                         #
#                   Priority2     = Y                                         #
###############################################################################

STEP 9

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P2_O)"

if {![ptp_ha::recv_announce \
             -port_num             $tee_port_2\
             -vlan_id              $vlan_id_2\
             -filter_id            $filter_id2\
             -domain_number        $domain\
             -recvd_gm_priority1   priority21\
             -recvd_gm_priority2   priority22]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_RX_P2_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
#                                                                             #
# Step 10: Disable masteronly configuration on port P1.                       #
#                                                                             #
###############################################################################

STEP 10

LOG -level 0 -msg "$dut_log_msg(DISABLE_MO)"

if {![ptp_ha::dut_masteronly_disable \
              -port_num        $dut_port_1]} {
      
      TEE_CLEANUP

      TC_CLEAN_AND_ABORT -reason   "$dut_log_msg(DISABLE_MO_F)"\
                         -tc_def    $tc_def

      return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_2

###############################################################################
# Step 11: Verify that DUT transmits ANNOUNCE message on port P2 with         #
#          following parameters.                                              #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type  = 0x0B                                      #
#                   Domain Number = DN1                                       #
#                   Priority1     = X-1                                       #
#                   Priority2     = Y-1                                       #
#                                                                             #
###############################################################################

STEP 11

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P2_V)"

if {![ptp_ha::recv_announce \
             -port_num              $tee_port_2\
             -vlan_id               $vlan_id_2\
             -filter_id             $filter_id2\
             -domain_number         $domain\
             -gm_priority1          $priority11\
             -gm_priority2          $priority12]} {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL -reason "PTP enabled device updates dataset from\
                               received Announce message when port state is\
                               configured as masterOnly."\
                      -tc_def $tc_def

    return 0
}

TEE_CLEANUP

TC_CLEAN_AND_PASS -reason "PTP enabled device does not update dataset\
                           from received Announce message\
                           when port state is configured as masterOnly."\
                  -tc_def  $tc_def

return 1

############################## END OF TEST CASE ###################################

