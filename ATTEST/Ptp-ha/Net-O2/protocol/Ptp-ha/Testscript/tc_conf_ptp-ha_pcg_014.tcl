#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_ptp-ha_pcg_014                                  #
# Test Case Version : 1.3                                                     #
# Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    #
# Module Name       : PTP-HA Configuration Group (PCG)                        #
#                                                                             #
# Title             : timestampCorrectionPortDS.egressLatency                 #
#                                                                             #
# Purpose           : To verify that a PTP enabled device supports to         #
#                     configure timestampCorrectionPortDS.egressLatency       #
#                     (allowable range: -2^63 to 2^63-1).                     #
#                                                                             #
# Reference         : P1588/D1.3, February 2018 V3.01 Clause 7.3.4.2 Page 68, #
#                     Clause 8.2.16.3 Page 129, Clause 16.7 Page 301,         #
#                     Clause J.5.3 Table 150 Page 413                         #
#                                                                             #
# Conformance Type  : SHALL                                                   #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 001                                                     #
# Test Topology     : 001                                                     #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode - one-step/two-step> |           #
#           |                     <Configure logAnnounceInterval, | P1        #
#           |                             announceReceiptTimeout> |           #
#           |                         <Configure logSyncInterval> | P1        #
#           |                  <Configure logMinDelayReqInterval> | P1        #
#           |                    <Configure Priority1, Priority2> | P1        #
#           |                                     <Enable L1SYNC> | P1        #
#           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        #
#           |                     <Disable L1SynOptParams option> | P1        #
#           |           <Enable asymmetryCorrectionPortDS.enable> | P1        #
#           | <Configure timestampCorrectionPortDS.egressLatency, | P1        #
#           |           timestampCorrectionPortDS.ingressLatency, |           #
#           |        asymmetryCorrectionPortDS.constantAsymmetry, |           #
#           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           #
#           |                                                     |           #
#           |    <Check timestampCorrectionPortDS.egressLatency = | P1        #
#           |                                            Default> |           #
#           |                                                     |           #
#           | !<Configure timestampCorrectionPortDS.egressLatency | P1        #
#           |                                     value to -2^64> |           #
#           |                                                     |           #
#           |    <Check timestampCorrectionPortDS.egressLatency = | P1        #
#           |                                    Default or zero> |           #
#           |                                                     |           #
#           |  <Configure timestampCorrectionPortDS.egressLatency | P1        #
#           |                                     value to -2^63> |           #
#           |                                                     |           #
#           |    <Check timestampCorrectionPortDS.egressLatency = | P1        #
#           |                                              -2^63> |           #
#           |                                                     |           #
#           | !<Configure timestampCorrectionPortDS.egressLatency | P1        #
#           |                                      value to 2^63> |           #
#           |                                                     |           #
#           |    <Check timestampCorrectionPortDS.egressLatency = | P1        #
#           |                                      -2^63 or zero> |           #
#           |                                                     |           #
#           |  <Configure timestampCorrectionPortDS.egressLatency | P1        #
#           |                                    value to 2^63-1> |           #
#           |                                                     |           #
#           |    <Check timestampCorrectionPortDS.egressLatency = | P1        #
#           |                                             2^63-1> |           #
#           |                                                     |           #
#           |  <Configure timestampCorrectionPortDS.egressLatency | P1        #
#           |                                   value to Default> |           #
#           |                                                     |           #
#           |    <Check timestampCorrectionPortDS.egressLatency = | P1        #
#           |                                            Default> |           #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     MSG_TYPE  = Message Type                                                #
#     DN        = Domain Number                                               #
#                                                                             #
# NOTE :                                                                      #
#                                                                             #
#  1. This objective is verified using the High Accuracy Delay Request-       #
#     Response Default PTP Profile                                            #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for egressLatency, ingressLatency,        #
#          constantAsymmetry and scaledDelayCoefficient.                      #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Check whether egressLatency has default value on port P1.          #
#                                                                             #
# Step 4 : Observe that DUT does not allow to configure out-of-range value of #
#          egressLatency on port P1 by trying to set egressLatency to         #
#          - 281 474 976 710 656 ns (i.e., the value of dataset expressed     #
#          in TimeInterval timestampCorrectionPortDS.egressLatency = -2^64).  #
#                                                                             #
# Step 5 : If DUT allows to configure egressLatency in step 4, check whether  #
#          egressLatency has still the default value or zero on port P1.      #
#                                                                             #
# Step 6 : Observe that DUT allows to configure the minimum allowed value     #
#          of the egressLatency on port P1 by setting egressLatency to        #
#          - 140 737 488 355 328 ns (i.e., the value of dataset expressed     #
#          in TimeInterval timestampCorrectionPortDS.egressLatency = -2^63).  #
#                                                                             #
# Step 7 : Check whether egressLatency on port P1 is                          #
#          - 140 737 488 355 328 ns (i.e., timestampCorrectionPortDS.         #
#          egressLatency = -2^63 expressed in TimeInterval).                  #
#                                                                             #
# Step 8 : Observe that DUT does not allow to configure out-of-range value    #
#          of egressLatency on port P1 by trying to set egressLatency to      #
#          140 737 488 355 328 ns (i.e., the value of dataset expressed in    #
#          TimeInterval timestampCorrectionPortDS.egressLatency = 2^63).      #
#                                                                             #
# Step 9 : If DUT allows to configure egressLatency in step 8, check whether  #
#          egressLatency on port P1 is still - 140 737 488 355 328 ns (i.e.,  #
#          (timestampCorrectionPortDS. egressLatency = -2^63 expressed in     #
#          TimeInterval) or zero.                                             #
#                                                                             #
# Step 10: Observe that DUT allows to configure maximum allowed value of the  #
#          egressLatency on port P1 by trying to set egressLatency to         #
#          140 737 488 355 327 ns (i.e., the value of dataset expressed       #
#          in TimeInterval timestampCorrectionPortDS.egressLatency = 2^63-1). #
#                                                                             #
# Step 11: Check whether egressLatency on port P1 is                          #
#          140 737 488 355 327 ns (i.e., timestampCorrectionPortDS.           #
#          egressLatency = 2^63-1 expressed in TimeInterval).                 #
#                                                                             #
# Step 12: Observe that DUT allows to configure egressLatency to the default  #
#          value on port P1.                                                  #
#                                                                             #
# Step 13: Verify whether egressLatency has the default value on port P1.     #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          May/2018      CERN          Initial                            #
# 1.1          Jul/2018      CERN          Added ptp_ha::display_port_details #
#                                          to print port details              #
# 1.2          Sep/2018      CERN          Re-worded header and log prints    #
#                                          to improve understandability.      #
# 1.3          Oct/2018      CERN          Added validation with zero value   #
#                                          for the configuration with         #
#                                          unallowed values.                  #
#                                                                             #
###############################################################################
# Copyright (c) 2018 CERN                                                     #
###############################################################################

global dut_log_msg
global tee_log_msg
global session_id

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def  "To verify that a PTP enabled device supports to
             configure timestampCorrectionPortDS.egressLatency
             (allowable range: -2^63 to 2^63-1)."

########################### START - INITIALIZATION ############################

set tee_port_1          $::tee_port_num_1
set dut_port_1          $::dut_port_num_1
set vlan_id             $::ptp_ha_vlan_id

set dut_ip              $::dut_test_port_ip
set ptp_comm_model      $::ptp_comm_model
set ptp_ethtype         $::PTP_ETHTYPE
set arp_timeout         $::ARP_TIMEOUT
set zero                0

########################### END - INITIALIZATION ##############################

ptp_ha::display_port_details

ptp_ha::dut_Initialization

proc cleanup {} {

    ptp_ha::dut_cleanup_setup_001

}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for egressLatency, ingressLatency,        #
#          constantAsymmetry and scaledDelayCoefficient.                      #
#                                                                             #
###############################################################################

STEP 1

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP1)"

if {![ptp_ha::dut_configure_setup_001]} {

    LOG -msg "$dut_log_msg(INIT_SETUP1_F)"

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP1_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
###############################################################################

STEP 2

LOG -level 0 -msg "$tee_log_msg(INIT)"

ptp_ha::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

    if {![PTP_HA_UNICAST_PRE_REQUISITE_CONFIGURATION\
                        -port_num       $tee_port_1\
                        -tee_mac        $tee_mac\
                        -tee_ip         $tee_ip\
                        -dut_ip         $dut_ip\
                        -vlan_id        $vlan_id\
                        -dut_mac        tee_dest_mac\
                        -timeout        $arp_timeout]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
                           -tc_def  $tc_def

        return 0
    }

}

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_1\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def  $tc_def

    return 0
}

set filter_id [lindex $id1 end]

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

# (Part 1)#

###############################################################################
# Step 3 : Check whether egressLatency has default value on port P1.          #
#                                                                             #
###############################################################################

STEP 3

LOG -level 0 -msg "Check egressLatency of port P1 ($dut_port_1) is\
                   $::ptp_egress_latency ns"

sleep $::dut_config_delay

if {![ptp_ha::dut_check_egress_latency \
               -port_num        $dut_port_1\
               -latency         $::ptp_egress_latency]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "egressLatency of port P1 ($dut_port_1) is not\
                                 $::ptp_egress_latency ns"\
                        -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 4 : Observe that DUT does not allow to configure out-of-range value of #
#          egressLatency on port P1 by trying to set egressLatency to         #
#          - 281 474 976 710 656 000 ps (i.e., the value of dataset expressed #
#          in TimeInterval timestampCorrectionPortDS.egressLatency = -2^64).  #
#                                                                             #
###############################################################################

STEP 4

set latency "-[format "%.f" [expr pow(2,48)]]"

LOG -level 0 -msg "$dut_log_msg(TXPORTDS.EGRESSLATENCY_N_O)"

if {[ptp_ha::dut_set_egress_latency\
             -port_num      $dut_port_1\
             -latency       $latency]} {

    set step4_pass 1

} else {

    set step4_pass 0
}

###############################################################################
# Step 5 : If DUT allows to configure egressLatency in step 4, check whether  #
#          egressLatency has still the default value or zero on port P1.      #
#                                                                             #
###############################################################################

STEP 5

LOG -level 0 -msg "Check egressLatency of port P1 ($dut_port_1) is\
                   $::ptp_egress_latency ns"

if {$step4_pass} {

    sleep $::dut_config_delay

    if {![ptp_ha::dut_check_egress_latency\
                    -port_num     $dut_port_1\
                    -latency      $::ptp_egress_latency]} {

STEP 5a

        LOG -level 0 -msg "Check egressLatency of port P1 ($dut_port_1) is\
                           $zero ns"

        if {![ptp_ha::dut_check_egress_latency\
                        -port_num     $dut_port_1\
                        -latency      $zero]} {

            TEE_CLEANUP

            TC_CLEAN_AND_ABORT  -reason "egressLatency of port P1 ($dut_port_1) is neither\
                                        $::ptp_egress_latency ns nor $zero ns"\
                                -tc_def  $tc_def
            return 0
        }
    }

} else {

    LOG -level 0 -msg "$tee_log_msg(SKIP_STEP)"
}

###############################################################################
# Step 6 : Observe that DUT allows to configure the minimum allowed value     #
#          of the egressLatency on port P1 by setting egressLatency to        #
#          - 140 737 488 355 328 000 ps (i.e., the value of dataset expressed #
#          in TimeInterval timestampCorrectionPortDS.egressLatency = -2^63).  #
#                                                                             #
###############################################################################

STEP 6

set latency [format "%.f" [expr pow(-2,47)]]

LOG -level 0 -msg "$dut_log_msg(TXPORTDS.EGRESSLATENCY_O)"

if {![ptp_ha::dut_set_egress_latency\
             -port_num     $dut_port_1\
             -latency      $latency]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(TXPORTDS.EGRESSLATENCY_F)"\
                        -tc_def  $tc_def

     return 0
}

###############################################################################
# Step 7 : Check whether egressLatency on port P1 is                          #
#          - 140 737 488 355 328 000 ps (i.e., timestampCorrectionPortDS.     #
#          egressLatency = -2^63 expressed in TimeInterval).                  #
#                                                                             #
###############################################################################

STEP 7

LOG -level 0 -msg "Check egressLatency of port P1 ($dut_port_1) is\
                   $latency ns"

sleep $::dut_config_delay

if {![ptp_ha::dut_check_egress_latency\
             -port_num      $dut_port_1\
             -latency       $latency]} {

     TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "egressLatency of port P1 ($dut_port_1) is not\
                                $latency ns"\
                        -tc_def  $tc_def

     return 0
}

###############################################################################
# Step 8 : Observe that DUT does not allow to configure out-of-range value    #
#          of egressLatency on port P1 by trying to set egressLatency to      #
#          140 737 488 355 328 000 ps (i.e., the value of dataset expressed   #
#          in TimeInterval timestampCorrectionPortDS.egressLatency = 2^63).   #
#                                                                             #
###############################################################################

STEP 8

set latency [format "%.f" [expr pow(2,47)]]

LOG -level 0 -msg "$dut_log_msg(TXPORTDS.EGRESSLATENCY_N_O)"

if {[ptp_ha::dut_set_egress_latency\
            -port_num      $dut_port_1\
            -latency       $latency]} {

    set step8_pass 1

} else {

    set step8_pass 0
}

###############################################################################
# Step 9 : If DUT allows to configure egressLatency in step 8, check whether  #
#          egressLatency on port P1 is still - 140 737 488 355 328 000 ps     #
#          (i.e., timestampCorrectionPortDS. egressLatency = -2^63 expressed  #
#          in TimeInterval) or zero.                                          #
#                                                                             #
###############################################################################

STEP 9

set latency [format "%.f" [expr pow(-2,47)]]

LOG -level 0 -msg "Check egressLatency of port P1 ($dut_port_1) is\
                   $latency ns"

if {$step8_pass} {

    sleep $::dut_config_delay

    if {![ptp_ha::dut_check_egress_latency\
                    -port_num    $dut_port_1\
                    -latency     $latency]} {

STEP 9a

        LOG -level 0 -msg "Check egressLatency of port P1 ($dut_port_1) is\
                           $zero ns"

        if {![ptp_ha::dut_check_egress_latency\
                        -port_num    $dut_port_1\
                        -latency     $zero]} {

            TEE_CLEANUP

            TC_CLEAN_AND_ABORT  -reason "egressLatency of port P1 ($dut_port_1) is neither\
                                        $latency ns nor $zero ns"\
                                -tc_def  $tc_def

            return 0
        }
    }

} else {

    LOG -level 0 -msg "$tee_log_msg(SKIP_STEP)"
}


###############################################################################
# Step 10: Observe that DUT allows to configure maximum allowed value of the  #
#          egressLatency on port P1 by trying to set egressLatency to         #
#          140 737 488 355 327 000 ps (i.e., the value of dataset expressed   #
#          in TimeInterval timestampCorrectionPortDS.egressLatency = 2^63-1). #
#                                                                             #
###############################################################################

STEP 10

set latency [format "%.f" [expr pow(2,47) - 1]]

LOG -level 0 -msg "$dut_log_msg(TXPORTDS.EGRESSLATENCY_O)"

if {![ptp_ha::dut_set_egress_latency\
              -port_num     $dut_port_1\
              -latency      $latency]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(TXPORTDS.EGRESSLATENCY_F)"\
                        -tc_def  $tc_def
     return 0
}

###############################################################################
# Step 11: Check whether egressLatency on port P1 is                          #
#          140 737 488 355 327 000 ps (i.e., timestampCorrectionPortDS.       #
#          egressLatency = 2^63-1 expressed in TimeInterval).                 #
#                                                                             #
###############################################################################

STEP 11

LOG -level 0 -msg "Check egressLatency of port P1 ($dut_port_1) is\
                   $latency ns"

sleep $::dut_config_delay

if {![ptp_ha::dut_check_egress_latency\
              -port_num     $dut_port_1\
              -latency      $latency]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "egressLatency of port P1 ($dut_port_1) is not\
                                 $latency ns"\
                        -tc_def  $tc_def 
     return 0
}

###############################################################################
# Step 12: Observe that DUT allows to configure egressLatency to the default  #
#          value on port P1.                                                  #
#                                                                             #
###############################################################################

STEP 12

LOG -level 0 -msg "$dut_log_msg(TXPORTDS.EGRESSLATENCY_O)"

if {![ptp_ha::dut_set_egress_latency\
              -port_num        $dut_port_1\
              -latency         $::ptp_egress_latency]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(TXPORTDS.EGRESSLATENCY_F)"\
                        -tc_def  $tc_def
     return 0
}

###############################################################################
# Step 13: Verify whether egressLatency has the default value on port P1.     #
#                                                                             #
###############################################################################

STEP 13

LOG -level 0 -msg "Check egressLatency of port P1 ($dut_port_1) is\
                   $::ptp_egress_latency ns"

sleep $::dut_config_delay

if {![ptp_ha::dut_check_egress_latency \
               -port_num        $dut_port_1\
               -latency         $::ptp_egress_latency]} {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL  -reason "PTP enabled device does not support to\
                                configure timestampCorrectionPortDS.egressLatency\
                                (allowable range: -2^63 to 2^63-1)"\
                       -tc_def  $tc_def
    return 0
}

TEE_CLEANUP

TC_CLEAN_AND_PASS  -reason "PTP enabled device supports to\
                           configure timestampCorrectionPortDS.egressLatency\
                           (allowable range: -2^63 to 2^63-1)"\
                   -tc_def  $tc_def
return 1

############################## END OF TEST CASE ###############################
