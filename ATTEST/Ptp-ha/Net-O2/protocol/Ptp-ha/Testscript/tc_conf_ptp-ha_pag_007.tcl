#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_ptp-ha_pag_007                                  #
# Test Case Version : 1.1                                                     #
# Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    #
# Module Name       : PTP Accuracy Group (PAG)                                #
#                                                                             #
# Title             : Egress timestamp in Pdelay_Resp message                 #
#                                                                             #
# Purpose           : To verify that a PTP enabled device generates Egress    #
#                     timestamp in Pdelay_Resp (event) messages from          #
#                     timestampCorrectionPortDS.egressLatency when using      #
#                     Peer to Peer Delay mechanism.                           #
#                                                                             #
# Reference         : IEEE 1588-2017 Clause 16.7.1 Page 301, Clause 7.3.4.2   #
#                     Page 68, Clause 8.2.16.2 Page 128                       #
#                                                                             #
# Conformance Type  : SHALL                                                   #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 002                                                     #
# Test Topology     : 002                                                     #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode - one-step/two-step> |           #
#           |                        <Set delay mechanism as P2P> |           #
#           |                     <Configure logAnnounceInterval, | P1        #
#           |                             announceReceiptTimeout> |           #
#           |                         <Configure logSyncInterval> | P1        #
#           |                  <Configure logMinDelayReqInterval> | P1        #
#           |                    <Configure Priority1, Priority2> | P1        #
#           |                                     <Enable L1SYNC> | P1        #
#           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        #
#           |                     <Disable L1SynOptParams option> | P1        #
#           |           <Enable asymmetryCorrectionPortDS.enable> | P1        #
#           | <Configure timestampCorrectionPortDS.egressLatency, | P1        #
#           |           timestampCorrectionPortDS.ingressLatency, |           #
#           |        asymmetryCorrectionPortDS.constantAsymmetry, |           #
#           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                          DN = DN1, PRI1 = X]        |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | ANNOUNCE [MSG_TYPE = 0x0B,                          |           #
#           |   DN = DN1, PRI1 = X-1]                             |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           | <Send Sync/ Sync & Follow-up messages               |           #
#           | based on clock step>                                |           #
#           |                                                     |           #
#           | PTP SIGNALING with L1 Sync TLV                      |           #
#           | [MSG_TYPE = 0xC, DN = DN1,                          |           #
#           | TLV_TYPE = 0x8001, TCR = 1,                         |           #
#           | RCR = 1, CR = 1, ITC = 1,                           |           #
#           | IRC = 1, IC = 1]                                    |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |             <Check L1SYNC port status - L1_SYNC_UP> | P1        #
#           |                                                     |           #
#           | PDELAY_REQ [MSG_TYPE = 0x01, DN = DN1]              |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                       PDELAY_RESP [MSG_TYPE = 0x09, |           #
#           |                                           DN = DN1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | <Check non-zero absolute value of currentDS.        |           #
#           |             offsetFromMaster is lowest as possible> |           #
#           |                                                     |           #
#           | <Calculate meanDelay (MD1)>                         |           #
#           |                                                     |           #
#           |  <Configure timestampCorrectionPortDS.egressLatency |           #
#           |                                 value to 2^(32+16)> |           #
#           |                                                     |           #
#           | PDELAY_REQ [MSG_TYPE = 0x01, DN = DN1]              |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                       PDELAY_RESP [MSG_TYPE = 0x09, |           #
#           |                                           DN = DN1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | <Check non-zero absolute value of currentDS.        |           #
#           |             offsetFromMaster is lowest as possible> |           #
#           |                                                     |           #
#           | <Calculate meanDelay (MD2)>                         |           #
#           |                                                     |           #
#           |                       MD2 < MD1                     |           #
#           |                                                     |           #
#           |  <Configure timestampCorrectionPortDS.egressLatency |           #
#           |                                value to -2^(32+16)> |           #
#           |                                                     |           #
#           | PDELAY_REQ [MSG_TYPE = 0x01, DN = DN1]              |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                       PDELAY_RESP [MSG_TYPE = 0x09, |           #
#           |                                           DN = DN1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | <Check non-zero absolute value of currentDS.        |           #
#           |             offsetFromMaster is lowest as possible> |           #
#           |                                                     |           #
#           | <Calculate meanDelay (MD3)>                         |           #
#           |                                                     |           #
#           |                       MD3 > MD1                     |           #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     MSG_TYPE  = Message Type                                                #
#     DN        = Domain Number                                               #
#     PRT1      = priority1                                                   #
#     DTS       = DUT's Timestamp                                             #
#                                                                             #
# NOTE :                                                                      #
#                                                                             #
#  1. This objective is verified using the High Accuracy Peer to Peer Default #
#     PTP Profile                                                             #
#                                                                             #
#  2. meanDelay is calculated with below formulae:                            #
#     * For one-step clock:                                                   #
#                                                                             #
#        [(t4 - t1) - <correctedPdelayRespCorrectionField>]/2                 #
#                                                                             #
#     * For two-step clock:                                                   #
#        [(t4 - t1) - (responseOriginTimestamp - requestReceiptTimestamp) -   #
#        <correctedPdelayRespCorrectionField> - correctionField of            #
#        Pdelay_Resp_Follow_Up]/2                                             #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure delaymechanism as Peer to peer.                          #
#    vi.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#   vii.   Enable L1SYNC on DUT's port P1.                                    #
#  viii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#    ix.   Disable L1SynOptParams on DUT.                                     #
#     x.   Enable asymmetryCorrectionPortDS.enable.                           #
#    xi.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency = 0, timestampCorrectionPortDS.ingressLatency = 0,   #
#          asymmetryCorrectionPortDS.constantAsymmetry = 0 and                #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient = 0.              #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x0B                                    #
#                  Domain Number    = DN1                                     #
#                  Priority1        = X                                       #
#                                                                             #
# Step 4 : Send periodic ANNOUNCE message on port T1 with following parameters#
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x0B                                    #
#                  Domain Number    = DN1                                     #
#                  Priority1        = X-1                                     #
#                                                                             #
# Step 5 : Send periodic SYNC message on the port P1 with following           #
#          parameters:                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x00                                    #
#                  Domain Number    = DN1                                     #
#                                                                             #
# Step 5a: If the clock is two-step clock, send periodic FOLLOW_UP message on #
#          port T1 with following parameters:                                 #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x08                                    #
#                  Domain Number    = DN1                                     #
#                                                                             #
# Step 6 : Send periodic PTP SIGNALING message with L1 Sync TLV on the port P1#
#          with following parameters:                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 1                                          #
#                  RCR           = 1                                          #
#                  CR            = 1                                          #
#                  ITC           = 1                                          #
#                  IRC           = 1                                          #
#                  IC            = 1                                          #
#                                                                             #
# Step 7 : Observe that the DUT's L1SYNC port status of P1 is L1_SYNC_UP.     #
#                                                                             #
# Step 8 :  Send periodic PDELAY_REQ message on port T1 with following        #
#           parameters.                                                       #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x01                                    #
#                  Domain Number    = DN1                                     #
#                                                                             #
# Step 9 : Observe that the DUT transmits PDELAY_RESP message on the port P1  #
#          with following parameters.                                         #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type    = 0x09                                    #
#                   Domain Number   = DN1                                     #
#                                                                             #
# Step 10: Check whether the non-zero absolute value of currentDS.            #
#          offsetFromMaster in DUT becomes lowest as possible to ensure that  #
#          the DUT synchronizes it's time with TEE.                           #
#                                                                             #
# Step 11: Calculate meanDelay (MD1) at TEE.                                  #
#                                                                             #
# Step 12: Configure egressLatency on port P1 by setting egressLatency to     #
#          4 294 967 296 ns (i.e., the value of dataset expressed in          #
#          TimeInterval asymmetryCorrectionPortDS.egressLatency = 2^48).      #
#                                                                             #
# Step 13: Send periodic PDELAY_REQ message on port T1 with following         #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x01                                    #
#                  Domain Number    = DN1                                     #
#                                                                             #
# Step 14: Observe that the DUT transmits PDELAY_RESP message on the port P1  #
#          with following parameters.                                         #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type    = 0x09                                    #
#                   Domain Number   = DN1                                     #
#                                                                             #
# Step 15: Check whether the non-zero absolute value of currentDS.            #
#          offsetFromMaster in DUT becomes lowest as possible to ensure that  #
#          the DUT synchronizes it's time with TEE.                           #
#                                                                             #
# Step 16: Calculate meanDelay (MD2) at TEE.                                  #
#                                                                             #
# Step 17: Observe that MD2 is lesser than MD1.                               #
#                                                                             #
# Step 18: Configure egressLatency on port P1 by setting egressLatency to     #
#          -4 294 967 296 ns (i.e., the value of dataset expressed in         #
#          TimeInterval asymmetryCorrectionPortDS.egressLatency = -2^48).     #
#                                                                             #
# Step 19: Send periodic PDELAY_REQ message on port T1 with following         #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x01                                    #
#                  Domain Number    = DN1                                     #
#                                                                             #
# Step 20: Observe that the DUT transmits PDELAY_RESP message on the port P1  #
#          with following parameters.                                         #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type    = 0x09                                    #
#                   Domain Number   = DN1                                     #
#                                                                             #
# Step 21: Check whether the non-zero absolute value of currentDS.            #
#          offsetFromMaster in DUT becomes lowest as possible to ensure that  #
#          the DUT synchronizes it's time with TEE.                           #
#                                                                             #
# Step 22: Calculate meanDelay (MD3) at TEE.                                  #
#                                                                             #
# Step 23: Verify that MD3 is greater than MD1.                               #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          Oct/2018      CERN          Initial                            #
# 1.1          Apr/2019      CERN          a) Added steps to check whether    #
#                                             offsetFromMaster becomes least  #
#                                             non-zero value to ensure DUT    #
#                                             synchronizes its time with TEE. #
#                                          b) Added support to proceed test   #
#                                             until last step and gracefully  #
#                                             abort the test for debugging.   #
#                                                                             #
###############################################################################
# Copyright (c) 2018 - 2019 CERN                                              #
###############################################################################

global dut_log_msg
global tee_log_msg
global session_id

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def   "To verify that a PTP enabled device generates Egress\
              timestamp in Pdelay_Resp (event) messages from\
              timestampCorrectionPortDS.egressLatency when using\
              Peer to Peer Delay mechanism."

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set dut_port_1                     $::dut_port_num_1
set vlan_id                        $::ptp_ha_vlan_id
set dut_ip                         $::dut_test_port_ip
set dut_dest_ip                    $::dut_test_port_ip
set tee_dest_ip                    $::tee_test_port_ip
set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT

set domain                         $::DEFAULT_DOMAIN
set infinity                       $::INFINITY
set clock_step                     $::ptp_dut_clock_step
set offset_from_master             $::offset_from_master

set sequence_id_ann                1
set sequence_id_sync               1
set sequence_id_l1sync             43692

set tlv_type(l1sync)               $::TLV_TYPE(L1SYNC)
set l1_sync_up                     $::PTP_L1SYNC_STATE(L1_SYNC_UP)

set tcr_true        1
set rcr_true        1
set cr_true         1
set itc_true        1
set irc_true        1
set ic_true         1

set SKIP_VALIDATION $::SKIP_VALIDATION_FOR_DEBUG

########################### END - INITIALIZATION ##############################

if {($env(TEST_DEVICE_MANAGEMENT_MODE) == "Manual")} {
    
    puts ".\n"
    puts "???????????????????????????????????????????????????????????????????????????"
    puts "???????????????????????????????????????????????????????????????????????????"
    puts "?? WARNING: Execution of this test in manual management mode may give    ??"
    puts "??          in-correct result.                                           ??"
    puts "??                                                                       ??"
    puts "?? The timestamps from the device should be taken faster and hence it is ??"
    puts "?? advised to execute this test case in automated mode.                  ??"
    puts "??                                                                       ??"
    puts "???????????????????????????????????????????????????????????????????????????"
    puts "???????????????????????????????????????????????????????????????????????????"
    puts ".\n"
}

if { $::ptp_p2p_delay_mechanism != "true" } {

    TC_ABORT -reason "$tee_log_msg(P2P_DLY_MECHANISM)"\
             -tc_def $tc_def

    return 0
}

ptp_ha::display_port_details

ptp_ha::dut_Initialization

proc cleanup {} {

    ptp_ha::dut_cleanup_setup_002

    ptp_ha::dut_reset_egress_latency\
             -port_num      $::dut_port_num_1

}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

### (Initial Part) ###

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure delaymechanism as Peer to peer.                          #
#    vi.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#   vii.   Enable L1SYNC on DUT's port P1.                                    #
#  viii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#    ix.   Disable L1SynOptParams on DUT.                                     #
#     x.   Enable asymmetryCorrectionPortDS.enable.                           #
#    xi.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency = 0, timestampCorrectionPortDS.ingressLatency = 0,   #
#          asymmetryCorrectionPortDS.constantAsymmetry = 0 and                #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient = 0.              #
###############################################################################

STEP 1

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP2)"

if {![ptp_ha::dut_configure_setup_002]} {

    LOG -msg "$dut_log_msg(INIT_SETUP2_F)"

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP2_F)"\
                       -tc_def  $tc_def
    return 0
}

ptp_ha::stop_at_step

###############################################################################
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
###############################################################################

STEP 2

LOG -level 0 -msg "$tee_log_msg(INIT)"

ptp_ha::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

    if {![PTP_HA_UNICAST_PRE_REQUISITE_CONFIGURATION\
                        -port_num       $tee_port_1\
                        -tee_mac        $tee_mac\
                        -tee_ip         $tee_ip\
                        -dut_ip         $dut_ip\
                        -vlan_id        $vlan_id\
                        -dut_mac        tee_dest_mac\
                        -timeout        $arp_timeout]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
                           -tc_def $tc_def

        return 0
    }

}

ptp_ha::stop_at_step

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_1\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def  $tc_def

    return 0
}

set filter_id [lindex $id1 end]

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

ptp_ha::enable_auto_response_to_pdelay_requests

#(Part 1) #

###############################################################################
#                                                                             #
# Step 3 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x0B                                    #
#                  Domain Number    = DN1                                     #
#                  Priority1        = X                                       #
###############################################################################

STEP 3

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
           -port_num                $tee_port_1\
           -domain_number           $domain\
           -error_reason            error_reason\
           -filter_id               $filter_id\
           -recvd_gm_priority1      priority1]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANNOUNCE_RX_P1_F) $error_reason"\
                        -tc_def  $tc_def

    return 0
}

ptp_ha::stop_at_step

###############################################################################
#                                                                             #
# Step 4 : Send periodic ANNOUNCE message on port T1 with following parameters#
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x0B                                    #
#                  Domain Number    = DN1                                     #
#                  Priority1        = X-1                                     #
###############################################################################

STEP 4

set priority [expr $priority1 - 1]

LOG -level 0 -msg "$tee_log_msg(ANN_TX_T1)"

if {![ptp_ha::send_announce \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -gm_priority1            $priority\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -count                   $infinity]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANN_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

ptp_ha::stop_at_step

###############################################################################
#                                                                             #
# Step 5 : Send periodic SYNC message on the port P1 with following           #
#          parameters:                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x00                                    #
#                  Domain Number    = DN1                                     #
###############################################################################

STEP 5

LOG -level 0 -msg "$tee_log_msg(SYNC_TX_T1)"

if {![ptp_ha::send_sync \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -count                   $infinity]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
#                                                                             #
# Step 5a: If the clock is two-step clock, send periodic FOLLOW_UP message on #
#          port T1 with following parameters:                                 #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x08                                    #
#                  Domain Number    = DN1                                     #
###############################################################################

if { $clock_step == "Two-Step" } {

STEP 5a

LOG -level 0 -msg "$tee_log_msg(FOLLOWUP_TX_T1)"

    if {![ptp_ha::send_followup \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -count                   $infinity]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FOLLOWUP_TX_T1_F)"\
                           -tc_def  $tc_def

        return 0
    }
}

ptp_ha::stop_at_step

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 6 : Send periodic PTP SIGNALING message with L1 Sync TLV on the port P1#
#          with following parameters:                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 1                                          #
#                  RCR           = 1                                          #
#                  CR            = 1                                          #
#                  ITC           = 1                                          #
#                  IRC           = 1                                          #
#                  IC            = 1                                          #
###############################################################################

STEP 6

LOG -level 0 -msg   "$tee_log_msg(L1SYNC_TX_T1)"

if {![ptp_ha::send_signal \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -tlv_type                $tlv_type(l1sync)\
           -src_ip                  $tee_ip\
           -domain_number           $domain\
           -tcr                     $tcr_true\
           -rcr                     $rcr_true\
           -cr                      $cr_true\
           -itc                     $itc_true\
           -irc                     $irc_true\
           -ic                      $ic_true\
           -count                   $infinity]} {

    TEE_CLEANUP
    
    TC_CLEAN_AND_ABORT   -reason "$tee_log_msg(L1SYNC_TX_T1_F)"\
                         -tc_def  $tc_def

    return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 7 : Observe that the DUT's L1SYNC port status of P1 is L1_SYNC_UP.     #
###############################################################################

STEP 7

LOG -level 0 -msg  "$dut_log_msg(CHECK_STATE_L1_SYNC_UP_P1_V)"

sleep $::ptp_ha_l1sync_wait

if {![ptp_ha::dut_check_l1sync_state \
              -port_num                $dut_port_1\
              -l1sync_state            $l1_sync_up] } {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_L1_SYNC_UP_P1_F)"\
                       -tc_def $tc_def
    return 0 
}

ptp_ha::stop_at_step

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
#                                                                             #
# Step 8 :  Send periodic PDELAY_REQ message on port T1 with following        #
#           parameters.                                                       #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x01                                    #
#                  Domain Number    = DN1                                     #
###############################################################################

STEP 8

LOG -level 0 -msg "$tee_log_msg(PDELAY_REQ_TX)"

if {![ptp_ha::send_pdelay_req \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -tx_origin_timestamp_sec t1_sec\
           -tx_origin_timestamp_ns  t1_ns\
           -count                   $infinity]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(PDELAY_REQ_TX_F)"\
                       -tc_def  $tc_def

    return 0
}

ptp_ha::stop_at_step

###############################################################################
#                                                                             #
# Step 9 : Observe that the DUT transmits PDELAY_RESP message on the port P1  #
#          with following parameters.                                         #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type    = 0x09                                    #
#                   Domain Number   = DN1                                     #
###############################################################################

STEP 9

LOG -level 0 -msg "$tee_log_msg(PDELAY_RESP_RX_P1_O)"

if {![ptp_ha::recv_pdelay_resp \
            -port_num                               $tee_port_1\
            -domain_number                          $domain\
            -recvd_correction_field                 cf_pdr\
            -rx_timestamp_ns                        t4\
            -recvd_request_receipt_timestamp_sec    rrt_sec\
            -recvd_request_receipt_timestamp_ns     rrt_ns\
            -error_reason                           error_reason\
            -filter_id                              $filter_id]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(PDELAY_RESP_RX_P1_F) $error_reason"\
                        -tc_def  $tc_def

    return 0
}

if { $clock_step == "Two-Step" } {

STEP 9a

LOG -level 0 -msg "$tee_log_msg(PDELAY_RESP_FOLLOW_UP_RX_P1_O)"

    if {![ptp_ha::recv_pdelay_resp_followup \
                -port_num                               $tee_port_1\
                -domain_number                          $domain\
                -recvd_correction_field                 cf_pdrf\
                -recvd_response_origin_timestamp_sec    rot_sec\
                -recvd_response_origin_timestamp_ns     rot_ns\
                -rx_timestamp_ns                        t4\
                -error_reason                           error_reason\
                -filter_id                              $filter_id]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(PDELAY_RESP_FOLLOW_UP_RX_P1_F) $error_reason"\
                            -tc_def  $tc_def

        return 0
    }
}

ptp_ha::stop_at_step


###############################################################################
# Step 10: Check whether the non-zero absolute value of currentDS.            #
#          offsetFromMaster in DUT becomes lowest as possible to ensure that  #
#          the DUT synchronizes it's time with TEE.                           #
###############################################################################

STEP 10

LOG -level 0 -msg  "$dut_log_msg(CHECK_LOWEST_OFM_V)"

if {![ptp_ha::dut_check_for_least_offset_from_master\
                -offset_from_master         $offset_from_master]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_LOWEST_OFM_F)"\
                       -tc_def $tc_def
    return 0 
}

ptp_ha::stop_at_step

###############################################################################
#                                                                             #
# Step 11: Calculate meanDelay (MD1) at TEE.                                  #
###############################################################################

STEP 11

LOG -level 0 -msg "$dut_log_msg(GET_CDS.MEANDELAY_MD_TEE)"

#  2. meanDelay is calculated with below formulae:                            #
#     * For one-step clock:                                                   #
#                                                                             #
#                                                                             #
#     * For two-step clock:                                                   #

if { $clock_step == "Two-Step" } {

#        [(TS2 - TS1) - (responseOriginTimestamp - requestReceiptTimestamp) - #
#        <correctedPdelayRespCorrectionField> - correctionField of            #
#        Pdelay_Resp_Follow_Up]/2                                             #

    set t4_sec      [expr $t4 / 1000000000]
    set t4_ns       [expr $t4 % 1000000000]

    LOG -level 3 -msg "t1 (in sec)                       = [nano $t1_sec] s"
    LOG -level 3 -msg "t1 (in ns)                        = [nano $t1_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "t4 (in sec)                       = [nano $t4_sec] s"
    LOG -level 3 -msg "t4 (in ns)                        = [nano $t4_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "responseOriginTimestamp (in sec)  = [nano $rot_sec] s"
    LOG -level 3 -msg "responseOriginTimestamp (in ns)   = [nano $rot_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "requestReceiptTimestamp (in sec)  = [nano $rrt_sec] s"
    LOG -level 3 -msg "requestReceiptTimestamp (in ns)   = [nano $rrt_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "CF in pDelay_Resp                 = [nano $cf_pdr] ns"
    LOG -level 3 -msg "CF in pDelay_Resp_Follow_up       = [nano $cf_pdrf] ns"
    LOG -level 3 -msg "------------------------------------------------------------------------"

    set MD1 [expr ((($t4_sec * pow(10,9)) + $t4_ns) -\
                   (($t1_sec * pow(10,9)) + $t1_ns) -\
                  ((($rot_sec * pow(10,9)) + $rot_ns) -\
                   (($rrt_sec * pow(10,9)) + $rrt_ns)) -\
                     $cf_pdr - $cf_pdrf)/2]

    LOG -level 3 -msg "MD1                               = [nano $MD1] ns"

} else {

#        [(TS2 - TS1) - <correctedPdelayRespCorrectionField>]/2               #

    set t4_sec      [expr $t4 / 1000000000]
    set t4_ns       [expr $t4 % 1000000000]

    LOG -level 3 -msg "t1 (in sec)                       = [nano $t1_sec] s"
    LOG -level 3 -msg "t1 (in ns)                        = [nano $t1_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "t4 (in sec)                       = [nano $t4_sec] s"
    LOG -level 3 -msg "t4 (in ns)                        = [nano $t4_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "CF in pDelay_Resp                 = [nano $cf_pdr]"
    LOG -level 3 -msg "------------------------------------------------------------------------"

    set MD1 [expr ((($t4_sec * pow(10,9)) + $t4_ns) -\
                   (($t1_sec * pow(10,9)) + $t1_ns) -\
                     $cf_pdr)/2]

    LOG -level 3 -msg "MD1                               = [nano $MD1] ns"
}

ptp_ha::stop_at_step

###############################################################################
# Step 12: Configure egressLatency on port P1 by setting egressLatency to     #
#          4 294 967 296 ns (i.e., the value of dataset expressed in          #
#          TimeInterval asymmetryCorrectionPortDS.egressLatency = 2^48).      #
###############################################################################

STEP 12

set latency 4294967296

LOG -level 0 -msg "$dut_log_msg(CONFIG_TXPORTDS.EGRESSLATENCY)"

if {![ptp_ha::dut_set_egress_latency\
             -port_num       $dut_port_1\
             -latency        $latency]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_TXPORTDS.EGRESSLATENCY_F)"\
                        -tc_def  $tc_def

     return 0
}

ptp_ha::stop_at_step

sleep $::ptp_ha_l1sync_wait

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
#                                                                             #
# Step 13: Send periodic PDELAY_REQ message on port T1 with following         #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x01                                    #
#                  Domain Number    = DN1                                     #
###############################################################################

STEP 13

LOG -level 0 -msg "$tee_log_msg(PDELAY_REQ_TX)"

if {![ptp_ha::send_pdelay_req \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -tx_origin_timestamp_sec t1_sec\
           -tx_origin_timestamp_ns  t1_ns\
           -count                   $infinity]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(PDELAY_REQ_TX_F)"\
                       -tc_def  $tc_def

    return 0
}

ptp_ha::stop_at_step

###############################################################################
#                                                                             #
# Step 14: Observe that the DUT transmits PDELAY_RESP message on the port P1  #
#          with following parameters.                                         #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type    = 0x09                                    #
#                   Domain Number   = DN1                                     #
###############################################################################

STEP 14

LOG -level 0 -msg "$tee_log_msg(PDELAY_RESP_RX_P1_O)"

if {![ptp_ha::recv_pdelay_resp \
            -port_num                               $tee_port_1\
            -domain_number                          $domain\
            -recvd_correction_field                 cf_pdr\
            -rx_timestamp_ns                        t4\
            -recvd_request_receipt_timestamp_sec    rrt_sec\
            -recvd_request_receipt_timestamp_ns     rrt_ns\
            -error_reason                           error_reason\
            -filter_id                              $filter_id]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(PDELAY_RESP_RX_P1_F) $error_reason"\
                        -tc_def  $tc_def

    return 0
}

if { $clock_step == "Two-Step" } {

STEP 13a

LOG -level 0 -msg "$tee_log_msg(PDELAY_RESP_FOLLOW_UP_RX_P1_O)"

    if {![ptp_ha::recv_pdelay_resp_followup \
                -port_num                               $tee_port_1\
                -domain_number                          $domain\
                -recvd_correction_field                 cf_pdrf\
                -recvd_response_origin_timestamp_sec    rot_sec\
                -recvd_response_origin_timestamp_ns     rot_ns\
                -rx_timestamp_ns                        t4\
                -error_reason                           error_reason\
                -filter_id                              $filter_id]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(PDELAY_RESP_FOLLOW_UP_RX_P1_F) $error_reason"\
                            -tc_def  $tc_def

        return 0
    }
}

ptp_ha::stop_at_step

###############################################################################
# Step 15: Check whether the non-zero absolute value of currentDS.            #
#          offsetFromMaster in DUT becomes lowest as possible to ensure that  #
#          the DUT synchronizes it's time with TEE.                           #
###############################################################################

STEP 15

LOG -level 0 -msg  "$dut_log_msg(CHECK_LOWEST_OFM_V)"

if {![ptp_ha::dut_check_for_least_offset_from_master\
                -offset_from_master         $offset_from_master]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_LOWEST_OFM_F)"\
                       -tc_def $tc_def
    return 0 
}

ptp_ha::stop_at_step

###############################################################################
#                                                                             #
# Step 16: Calculate meanDelay (MD2) at TEE.                                  #
###############################################################################

STEP 16

LOG -level 0 -msg "$dut_log_msg(GET_CDS.MEANDELAY_MD_TEE)"

#  2. meanDelay is calculated with below formulae:                            #
#     * For one-step clock:                                                   #
#                                                                             #
#                                                                             #
#     * For two-step clock:                                                   #

if { $clock_step == "Two-Step" } {

#        [(TS2 - TS1) - (responseOriginTimestamp - requestReceiptTimestamp) - #
#        <correctedPdelayRespCorrectionField> - correctionField of            #
#        Pdelay_Resp_Follow_Up]/2                                             #

    set t4_sec      [expr $t4 / 1000000000]
    set t4_ns       [expr $t4 % 1000000000]

    LOG -level 3 -msg "t1 (in sec)                       = [nano $t1_sec] s"
    LOG -level 3 -msg "t1 (in ns)                        = [nano $t1_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "t4 (in sec)                       = [nano $t4_sec] s"
    LOG -level 3 -msg "t4 (in ns)                        = [nano $t4_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "responseOriginTimestamp (in sec)  = [nano $rot_sec] s"
    LOG -level 3 -msg "responseOriginTimestamp (in ns)   = [nano $rot_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "requestReceiptTimestamp (in sec)  = [nano $rrt_sec] s"
    LOG -level 3 -msg "requestReceiptTimestamp (in ns)   = [nano $rrt_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "CF in pDelay_Resp                 = [nano $cf_pdr] ns"
    LOG -level 3 -msg "CF in pDelay_Resp_Follow_up       = [nano $cf_pdrf] ns"
    LOG -level 3 -msg "------------------------------------------------------------------------"


    set MD2 [expr ((($t4_sec * pow(10,9)) + $t4_ns) -\
                   (($t1_sec * pow(10,9)) + $t1_ns) -\
                  ((($rot_sec * pow(10,9)) + $rot_ns) -\
                   (($rrt_sec * pow(10,9)) + $rrt_ns)) -\
                     $cf_pdr - $cf_pdrf)/2]

    LOG -level 3 -msg "MD2                               = [nano $MD2] ns"
} else {

#        [(TS2 - TS1) - <correctedPdelayRespCorrectionField>]/2               #

    set t4_sec      [expr $t4 / 1000000000]
    set t4_ns       [expr $t4 % 1000000000]

    LOG -level 3 -msg "t1 (in sec)                       = [nano $t1_sec] s"
    LOG -level 3 -msg "t1 (in ns)                        = [nano $t1_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "t4 (in sec)                       = [nano $t4_sec] s"
    LOG -level 3 -msg "t4 (in ns)                        = [nano $t4_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "CF in pDelay_Resp                 = [nano $cf_pdr]"
    LOG -level 3 -msg "------------------------------------------------------------------------"

    set MD2 [expr ((($t4_sec * pow(10,9)) + $t4_ns) -\
                   (($t1_sec * pow(10,9)) + $t1_ns) -\
                     $cf_pdr)/2]

    LOG -level 3 -msg "MD2                               = [nano $MD2] ns"
}

ptp_ha::stop_at_step

###############################################################################
#                                                                             #
# Step 17: Observe that MD2 is lesser than MD1.                               #
###############################################################################

STEP 17

LOG -level 0 -msg "$dut_log_msg(MD2_LESSER_MD1_O)"
LOG -level 3 -msg "Value of meanDelay (MD1) = [nano $MD1] ns"
LOG -level 3 -msg "Value of meanDelay (MD2) = [nano $MD2] ns"

if {!( $MD2 < $MD1 )} {

if {$SKIP_VALIDATION != 1} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(MD2_LESSER_MD1_F)"\
                       -tc_def  $tc_def
    return 0
}
}

ptp_ha::stop_at_step

###############################################################################
#                                                                             #
# Step 18: Configure egressLatency on port P1 by setting egressLatency to     #
#          -4 294 967 296 ns (i.e., the value of dataset expressed in         #
#          TimeInterval asymmetryCorrectionPortDS.egressLatency = -2^48).     #
###############################################################################

STEP 18

set latency -4294967296

LOG -level 0 -msg "$dut_log_msg(CONFIG_TXPORTDS.EGRESSLATENCY)"

if {![ptp_ha::dut_set_egress_latency\
             -port_num      $dut_port_1\
             -latency       $latency]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_TXPORTDS.EGRESSLATENCY_F)"\
                        -tc_def  $tc_def

     return 0
}

ptp_ha::stop_at_step

sleep $::ptp_ha_l1sync_wait

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
#                                                                             #
# Step 19: Send periodic PDELAY_REQ message on port T1 with following         #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x01                                    #
#                  Domain Number    = DN1                                     #
###############################################################################

STEP 19

LOG -level 0 -msg "$tee_log_msg(PDELAY_REQ_TX)"

if {![ptp_ha::send_pdelay_req \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -tx_origin_timestamp_sec t1_sec\
           -tx_origin_timestamp_ns  t1_ns\
           -count                   $infinity]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(PDELAY_REQ_TX_F)"\
                       -tc_def  $tc_def

    return 0
}

ptp_ha::stop_at_step

###############################################################################
#                                                                             #
# Step 20: Observe that the DUT transmits PDELAY_RESP message on the port P1  #
#          with following parameters.                                         #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type    = 0x09                                    #
#                   Domain Number   = DN1                                     #
###############################################################################

STEP 20

LOG -level 0 -msg "$tee_log_msg(PDELAY_RESP_RX_P1_O)"

if {![ptp_ha::recv_pdelay_resp \
            -port_num                               $tee_port_1\
            -domain_number                          $domain\
            -recvd_correction_field                 cf_pdr\
            -rx_timestamp_ns                        t4\
            -recvd_request_receipt_timestamp_sec    rrt_sec\
            -recvd_request_receipt_timestamp_ns     rrt_ns\
            -error_reason                           error_reason\
            -filter_id                              $filter_id]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(PDELAY_RESP_RX_P1_F) $error_reason"\
                        -tc_def  $tc_def

    return 0
}

if { $clock_step == "Two-Step" } {

STEP 20a

LOG -level 0 -msg "$tee_log_msg(PDELAY_RESP_FOLLOW_UP_RX_P1_O)"

    if {![ptp_ha::recv_pdelay_resp_followup \
                -port_num                               $tee_port_1\
                -domain_number                          $domain\
                -recvd_correction_field                 cf_pdrf\
                -recvd_response_origin_timestamp_sec    rot_sec\
                -recvd_response_origin_timestamp_ns     rot_ns\
                -rx_timestamp_ns                        t4\
                -error_reason                           error_reason\
                -filter_id                              $filter_id]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(PDELAY_RESP_FOLLOW_UP_RX_P1_F) $error_reason"\
                            -tc_def  $tc_def

        return 0
    }
}

ptp_ha::stop_at_step

###############################################################################
# Step 21: Check whether the non-zero absolute value of currentDS.            #
#          offsetFromMaster in DUT becomes lowest as possible to ensure that  #
#          the DUT synchronizes it's time with TEE.                           #
###############################################################################

STEP 21

LOG -level 0 -msg  "$dut_log_msg(CHECK_LOWEST_OFM_V)"

if {![ptp_ha::dut_check_for_least_offset_from_master\
                -offset_from_master         $offset_from_master]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_LOWEST_OFM_F)"\
                       -tc_def $tc_def
    return 0 
}

ptp_ha::stop_at_step

###############################################################################
#                                                                             #
# Step 22: Calculate meanDelay (MD3) at TEE.                                  #
###############################################################################

STEP 22

LOG -level 0 -msg "$dut_log_msg(GET_CDS.MEANDELAY_MD_TEE)"

#  2. meanDelay is calculated with below formulae:                            #
#     * For one-step clock:                                                   #
#                                                                             #
#                                                                             #
#     * For two-step clock:                                                   #

if { $clock_step == "Two-Step" } {

#        [(TS2 - TS1) - (responseOriginTimestamp - requestReceiptTimestamp) - #
#        <correctedPdelayRespCorrectionField> - correctionField of            #
#        Pdelay_Resp_Follow_Up]/2                                             #

    set t4_sec      [expr $t4 / 1000000000]
    set t4_ns       [expr $t4 % 1000000000]

    LOG -level 3 -msg "t1 (in sec)                       = [nano $t1_sec] s"
    LOG -level 3 -msg "t1 (in ns)                        = [nano $t1_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "t4 (in sec)                       = [nano $t4_sec] s"
    LOG -level 3 -msg "t4 (in ns)                        = [nano $t4_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "responseOriginTimestamp (in sec)  = [nano $rot_sec] s"
    LOG -level 3 -msg "responseOriginTimestamp (in ns)   = [nano $rot_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "requestReceiptTimestamp (in sec)  = [nano $rrt_sec] s"
    LOG -level 3 -msg "requestReceiptTimestamp (in ns)   = [nano $rrt_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "CF in pDelay_Resp                 = [nano $cf_pdr] ns"
    LOG -level 3 -msg "CF in pDelay_Resp_Follow_up       = [nano $cf_pdrf] ns"
    LOG -level 3 -msg "------------------------------------------------------------------------"

    set MD3 [expr ((($t4_sec * pow(10,9)) + $t4_ns) -\
                   (($t1_sec * pow(10,9)) + $t1_ns) -\
                  ((($rot_sec * pow(10,9)) + $rot_ns) -\
                   (($rrt_sec * pow(10,9)) + $rrt_ns)) -\
                     $cf_pdr - $cf_pdrf)/2]

    LOG -level 3 -msg "MD3                               = [nano $MD3] ns"
} else {

#        [(TS2 - TS1) - <correctedPdelayRespCorrectionField>]/2               #

    set t4_sec      [expr $t4 / 1000000000]
    set t4_ns       [expr $t4 % 1000000000]

    LOG -level 3 -msg "t1 (in sec)                       = [nano $t1_sec] s"
    LOG -level 3 -msg "t1 (in ns)                        = [nano $t1_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "t4 (in sec)                       = [nano $t4_sec] s"
    LOG -level 3 -msg "t4 (in ns)                        = [nano $t4_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "CF in pDelay_Resp                 = [nano $cf_pdr]"
    LOG -level 3 -msg "------------------------------------------------------------------------"

    set MD3 [expr ((($t4_sec * pow(10,9)) + $t4_ns) -\
                   (($t1_sec * pow(10,9)) + $t1_ns) -\
                     $cf_pdr)/2]

    LOG -level 3 -msg "MD3                               = [nano $MD3] ns"
}

ptp_ha::stop_at_step

###############################################################################
#                                                                             #
# Step 23: Verify that MD3 is greater than MD1.                               #
#                                                                             #
###############################################################################

STEP 23

LOG -level 0 -msg "$dut_log_msg(MD3_GREATER_MD1_V)"

LOG -level 3 -msg "Value of meanDelay (MD1) = [nano $MD1] ns"
LOG -level 3 -msg "Value of meanDelay (MD3) = [nano $MD3] ns"

if {!( $MD3 > $MD1 )} {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL -reason  "Value of MD3 is not greater than MD1."\
                      -tc_def  $tc_def
    return 0
}

TEE_CLEANUP

TC_CLEAN_AND_PASS -reason  "DUT generates Egress timestamp in\
                            Pdelay_Resp (event) messages from\
                            timestampCorrectionPortDS.egressLatency when\
                            using peer to peer delay mechanism."\
                  -tc_def   $tc_def

return 1

########################### END OF TEST CASE ##################################
