#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_ptp-ha_pag_008                                  #
# Test Case Version : 1.8                                                     #
# Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    #
# Module Name       : PTP Accuracy Group (PAG)                                #
#                                                                             #
# Title             : Ingress timestamp in Sync message                       #
#                                                                             #
# Purpose           : To verify that a PTP enabled device generates Ingress   #
#                     timestamp in Sync (event) messages from                 #
#                     timestampCorrectionPortDS.ingressLatency when using     #
#                     Delay Request-Response mechanism.                       #
#                                                                             #
# Reference         : IEEE 1588-2017 Clause 16.7.1 Page 301, Clause 7.3.4.2   #
#                     Page 68, Clause 8.2.16.2 Page 128                       #
#                                                                             #
# Conformance Type  : SHALL                                                   #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 009                                                     #
# Test Topology     : 001                                                     #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode - one-step/two-step> |           #
#           |                     <Configure logAnnounceInterval, | P1        #
#           |                             announceReceiptTimeout> |           #
#           |                         <Configure logSyncInterval> | P1        #
#           |                  <Configure logMinDelayReqInterval> | P1        #
#           |                    <Configure Priority1, Priority2> | P1        #
#           |                                     <Enable L1SYNC> | P1        #
#           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        #
#           |                     <Disable L1SynOptParams option> | P1        #
#           |           <Enable asymmetryCorrectionPortDS.enable> | P1        #
#           | <Configure timestampCorrectionPortDS.egressLatency, | P1        #
#           |           timestampCorrectionPortDS.ingressLatency, |           #
#           |        asymmetryCorrectionPortDS.constantAsymmetry, |           #
#           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                          DN = DN1, PRI1 = X]        |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | ANNOUNCE [MSG_TYPE = 0x0B,                          |           #
#           |   DN = DN1, PRI1 = X-1]                             |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#        T1 | <Enable auto responder to Delay_Req messages>       | P1        #
#           |                                                     |           #
#           | <Send Sync/ Sync & Follow-up msgs                   |           #
#           | based on clock step>                                |           #
#           |                                                     |           #
#           |          < Wait for 6s to complete BMCA >           |           #
#           |                                                     |           #
#           | PTP SIGNALING with L1 Sync TLV                      |           #
#           | [MSG_TYPE = 0xC, DN = DN1,                          |           #
#           | TLV_TYPE = 0x8001, TCR = 1,                         |           #
#           | RCR = 1, CR = 1, ITC = 1,                           |           #
#           | IRC = 1, IC = 1]                                    |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |             <Check L1SYNC port status - L1_SYNC_UP> | P1        #
#           |                                                     |           #
#           | <Check non-zero absolute value of currentDS.        |           #
#           |             offsetFromMaster is lowest as possible> |           #
#           |                                                     |           #
#           | SYNC [MSG_TYPE = 0x00, DN = DN1,                    |           #
#           | SEET1(ORG_TS = OTS1, CF = SYNC_CF1)]                |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           | <If Clock Step = Two Step, then                     |           #
#           |      send FOLLOW_UP and include CF = FU_CF1>        |           #
#           |                                                     |           #
#           |                             <Get DUT's time (DTS1)> |           #
#           |                                                     |           #
#           |                  DTS1 - SEET1 < 1                   |           #
#           |                                                     |           #
#           | <Configure timestampCorrectionPortDS.ingressLatency |           #
#           |                                 value to 2^(32+16)> |           #
#           |                                                     |           #
#           |     PTP SIGNALING with L1 Sync TLV [MSG_TYPE = 0xC, |           #
#           |                           DN = DN1,TLV_TYPE=0x8001] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | <Send Sync/ Sync & Follow-up msgs                   |           #
#           | based on clock step>                                |           #
#           |                                                     |           #
#           | <Check non-zero absolute value of currentDS.        |           #
#           |             offsetFromMaster is lowest as possible> |           #
#           |                                                     |           #
#           | SYNC [MSG_TYPE = 0x00, DN = DN1,                    |           #
#           | SEET2(ORG_TS = OTS2, CF = SYNC_CF2)]                |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           | <If Clock Step = Two Step, then                     |           #
#           |      send FOLLOW_UP and include CF = FU_CF2>        |           #
#           |                                                     |           #
#           |                             <Get DUT's time (DTS2)> |           #
#           |                                                     |           #
#           |                  DTS2 - SEET2 > 1                   |           #
#           |                                                     |           #
#           | <Configure timestampCorrectionPortDS.ingressLatency |           #
#           |                                value to -2^(32+16)> |           #
#           |                                                     |           #
#           |     PTP SIGNALING with L1 Sync TLV [MSG_TYPE = 0xC, |           #
#           |                           DN = DN1,TLV_TYPE=0x8001] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | <Send Sync/ Sync & Follow-up msgs                   |           #
#           | based on clock step>                                |           #
#           |                                                     |           #
#           | <Check non-zero absolute value of currentDS.        |           #
#           |             offsetFromMaster is lowest as possible> |           #
#           |                                                     |           #
#           | SYNC [MSG_TYPE = 0x00, DN = DN1,                    |           #
#           | SEET3(ORG_TS = OTS3, CF = SYNC_CF3)]                |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           | <If Clock Step = Two Step, then                     |           #
#           |      send FOLLOW_UP and include CF = FU_CF3>        |           #
#           |                                                     |           #
#           |                             <Get DUT's time (DTS3)> |           #
#           |                                                     |           #
#           |                  SEET3 - DTS3 > 1                   |           #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     MSG_TYPE  = Message Type                                                #
#     DN        = Domain Number                                               #
#     PRT1      = priority1                                                   #
#     ORG_TS    = Origin Timestamp                                            #
#     CF        = CorrectionField                                             #
#     DTS       = DUT's Timestamp                                             #
#     SEET      = SyncEventEgressTimestamp                                    #
#                                                                             #
# NOTE :                                                                      #
#                                                                             #
#  1. This objective is verified using the High Accuracy Delay Request-       #
#     Response Default PTP Profile                                            #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency = 0, timestampCorrectionPortDS.ingressLatency = 0,   #
#          asymmetryCorrectionPortDS.constantAsymmetry = 0 and                #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient = 0.              #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x0B                                #
#                  Domain Number        = DN1                                 #
#                  Priority1            = X                                   #
#                                                                             #
# Step 4 : Send periodic ANNOUNCE message on port T1 with following parameters#
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x0B                                #
#                  Domain Number        = DN1                                 #
#                  Priority1            = X-1                                 #
#                                                                             #
# Step 5 : Enable auto responder to respond every Delay_Req messages received #
#          on port T1.                                                        #
#                                                                             #
# Step 6 : Send periodic SYNC message on port T1 with following parameters.   #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x00                                #
#                  Domain Number        = DN1                                 #
#                  Source Port Identity = E                                   #
#                  Sequence ID          = B                                   #
#                                                                             #
# Step 6a: If the clock is two step, send periodic FOLLOW_UP message on port  #
#          T1 with following parameters.                                      #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x08                                #
#                  Domain Number        = DN1                                 #
#                  Source Port Identity = E                                   #
#                  Sequence ID          = B                                   #
#                                                                             #
# Step 7 : Wait for 6s to complete BMCA.                                      #
#                                                                             #
# Step 8 : Send periodic PTP SIGNALING message with L1 Sync TLV on the port P1#
#          with following parameters:                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 1                                          #
#                  RCR           = 1                                          #
#                  CR            = 1                                          #
#                  ITC           = 1                                          #
#                  IRC           = 1                                          #
#                  IC            = 1                                          #
#                                                                             #
# Step 9 : Observe that the DUT's L1SYNC port status of P1 is L1_SYNC_UP.     #
#                                                                             #
# Step 10: Check whether the non-zero absolute value of currentDS.            #
#          offsetFromMaster in DUT becomes lowest as possible to ensure that  #
#          the DUT synchronizes it's time with TEE.                           #
#                                                                             #
# Step 11: Send SYNC message on port T1 with following parameters.            #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x00                                #
#                  Domain Number        = DN1                                 #
#                  Source Port Identity = E                                   #
#                  Sequence ID          = B                                   #
#                  Origin Timestamp     = OTS1                                #
#                  Correction Field     = SYNC_CF1                            #
#                                                                             #
# Step 11a:If the clock is two step, send FOLLOW_UP message on port T1 with   #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x08                                #
#                  Domain Number        = DN1                                 #
#                  Source Port Identity = E                                   #
#                  Sequence ID          = B                                   #
#                  Correction Field     = FU_CF1                              #
#                                                                             #
# Note: Calculate SyncEventEgressTimestamp (SEET1) from SYNC and FOLLOW_UP    #
#       messages.                                                             #
#                                                                             #
# Step 12: Get DUT's time and convert it into epoch Timestamp (DTS1).         #
#                                                                             #
# Step 13: Observe that time difference of DTS1 and SEET1 is lesser than 1    #
#          (i.e., DTS1 - SEET1 < 1)                                           #
#                                                                             #
# Step 14: Configure ingressLatency on port P1 by setting ingressLatency to   #
#          4 294 967 296 ns (i.e., the value of dataset expressed in          #
#          TimeInterval asymmetryCorrectionPortDS.ingressLatency = 2^48).     #
#                                                                             #
# Step 15: Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV  #
#          on the port P1 with following parameters:                          #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                                                                             #
# Step 16: Send periodic SYNC message on port T1 with following parameters.   #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x00                                #
#                  Domain Number        = DN1                                 #
#                  Source Port Identity = E                                   #
#                  Sequence ID          = B                                   #
#                                                                             #
# Step 16a:If the clock is two step, send periodic FOLLOW_UP message on port  #
#          T1 with following parameters.                                      #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x08                                #
#                  Domain Number        = DN1                                 #
#                  Source Port Identity = E                                   #
#                  Sequence ID          = B                                   #
#                                                                             #
# Step 17: Check whether the non-zero absolute value of currentDS.            #
#          offsetFromMaster in DUT becomes lowest as possible to ensure that  #
#          the DUT synchronizes it's time with TEE.                           #
#                                                                             #
# Step 18: Send SYNC message on port T1 with following parameters.            #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x00                                #
#                  Domain Number        = DN1                                 #
#                  Source Port Identity = E                                   #
#                  Sequence ID          = B                                   #
#                  Origin Timestamp     = OTS2                                #
#                  Correction Field     = SYNC_CF2                            #
#                                                                             #
# Step 18a:If the clock is two step, send FOLLOW_UP message on port T1 with   #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x08                                #
#                  Domain Number        = DN1                                 #
#                  Source Port Identity = E                                   #
#                  Sequence ID          = B                                   #
#                  Correction Field     = FU_CF2                              #
#                                                                             #
# Note: Calculate SyncEventEgressTimestamp (SEET2) from SYNC and FOLLOW_UP    #
#       messages.                                                             #
#                                                                             #
# Step 19: Get DUT's time and convert it into epoch Timestamp (DTS2).         #
#                                                                             #
# Step 20: Observe that time difference of DTS2 and SEET2 is greater than 1.  #
#          (i.e., DTS2 - SEET2 > 1)                                           #
#                                                                             #
# Step 21: Configure ingressLatency on port P1 by setting ingressLatency to   #
#          -4 294 967 296 ns (i.e., the value of dataset expressed in         #
#          TimeInterval asymmetryCorrectionPortDS.ingressLatency = -2^48).    #
#                                                                             #
# Step 22: Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV  #
#          on the port P1 with following parameters:                          #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                                                                             #
# Step 23: Send periodic SYNC message on port T1 with following parameters.   #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x00                                #
#                  Domain Number        = DN1                                 #
#                  Source Port Identity = E                                   #
#                  Sequence ID          = B                                   #
#                                                                             #
# Step 23a:If the clock is two step, send periodic FOLLOW_UP message on port  #
#          T1 with following parameters.                                      #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x08                                #
#                  Domain Number        = DN1                                 #
#                  Source Port Identity = E                                   #
#                  Sequence ID          = B                                   #
#                                                                             #
# Step 24: Check whether the non-zero absolute value of currentDS.            #
#          offsetFromMaster in DUT becomes lowest as possible to ensure that  #
#          the DUT synchronizes it's time with TEE.                           #
#                                                                             #
# Step 25: Send SYNC message on port T1 with following parameters.            #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x00                                #
#                  Domain Number        = DN1                                 #
#                  Source Port Identity = E                                   #
#                  Sequence ID          = B                                   #
#                  Origin Timestamp     = OTS3                                #
#                  Correction Field     = SYNC_CF3                            #
#                                                                             #
# Step 25a:If the clock is two step, send FOLLOW_UP message on port T1 with   #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x08                                #
#                  Domain Number        = DN1                                 #
#                  Source Port Identity = E                                   #
#                  Sequence ID          = B                                   #
#                  Correction Field     = FU_CF3                              #
#                                                                             #
# Note: Calculate SyncEventEgressTimestamp (SEET3) from SYNC and FOLLOW_UP    #
#       messages.                                                             #
#                                                                             #
# Step 26: Get DUT's time and convert it into epoch Timestamp (DTS3).         #
#                                                                             #
# Step 27: Verify that time difference of SEET3 and DTS3 is greater than 1.   #
#          (i.e., SEET3 - DTS3 > 1)                                           #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          May/2018      CERN          Initial                            #
# 1.1          Jun/2018      CERN          Fixed dut_port in                  #
#                                          dut_reset_egress_latency           #
# 1.2          Jul/2018      CERN          Added ptp_ha::display_port_details #
#                                          to print port details              #
# 1.3          Sep/2018      CERN          a) Removed destination MAC and IP  #
#                                             in send functions.              #
#                                          b) Removed timeout in recv         #
#                                             functions.                      #
#                                          c) Moved timeout calculation to    #
#                                             PTP-HA global file.             #
#                                          d) Enabled log prints for          #
#                                             calculating differences in      #
#                                             timestamps.                     #
#                                          e) Re-worded header and log prints #
#                                             to improve understandability.   #
# 1.4          Oct/2018      CERN          L1Sync messages are made to        #
#                                          transmit periodically from TEE.    #
# 1.5          Nov/2018      CERN          a) Replaced                        #
#                                             SyncEventIngressTimestamp with  #
#                                             SyncEventEgressTimestamp and    #
#                                             SEIT with SEET.                 #
#                                          b) Added step to check whether     #
#                                             offsetFromMaster becomes least  #
#                                             value to ensure DUT synchronizes#
#                                             its time with TEE.              #
#                                          c) Added auto-respond module to    #
#                                             respond every Delay_Req messages#
# 1.6          Jan/2019      CERN          a) Changed value of offsetFromMater#
#                                             as absolute value.              #
#                                          b) Added condition before getting  #
#                                             the timestamp from DUT to ensure#
#                                             that Sync and Follow_Up messages#
#                                             are sent from TEE.              #
#                                          c) Added support to proceed test   #
#                                             until last step and gracefully  #
#                                             abort the test for debugging.   #
# 1.7          Feb/2019      CERN          a) Mentioned non-zero for          #
#                                             offsetFromMaster in the steps.  #
#                                          b) Changed validation of           #
#                                             differences between SEET and DTS#
#                                             values.                         #
#                                                                             #
###############################################################################
# Copyright (c) 2018 - 2019 CERN                                              #
###############################################################################

global dut_log_msg
global tee_log_msg
global session_id

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def  "To verify that a PTP enabled device generates Ingress\
             timestamp in Sync (event) messages from\
             timestampCorrectionPortDS.ingressLatency when using\
             Delay Request-Response mechanism"

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set dut_port_1                     $::dut_port_num_1
set vlan_id                        $::ptp_ha_vlan_id
set dut_ip                         $::dut_test_port_ip
set tee_ip                         $::tee_test_port_ip
set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT

set domain                         $::DEFAULT_DOMAIN
set gmid                           $::GRANDMASTER_ID
set infinity                       $::INFINITY
set clock_step                     $::ptp_dut_clock_step
set offset_from_master             $::offset_from_master

set sequence_id_followup           1
set sequence_id_sync               1
set sequence_id_ann                1
set sequence_id_l1sync             43692

set constant_asymmetry             0

set tlv_type(l1sync)               $::TLV_TYPE(L1SYNC)
set l1_sync_up                     $::PTP_L1SYNC_STATE(L1_SYNC_UP)

set tcr_true        1
set rcr_true        1
set cr_true         1
set itc_true        1
set irc_true        1
set ic_true         1

set SKIP_VALIDATION $::SKIP_VALIDATION_FOR_DEBUG

########################### END - INITIALIZATION ##############################

if {($env(TEST_DEVICE_MANAGEMENT_MODE) == "Manual")} {
    
    puts ".\n"
    puts "???????????????????????????????????????????????????????????????????????????"
    puts "???????????????????????????????????????????????????????????????????????????"
    puts "?? WARNING: Execution of this test in manual management mode may give    ??"
    puts "??          in-correct result.                                           ??"
    puts "??                                                                       ??"
    puts "?? The timestamps from the device should be taken faster and hence it is ??"
    puts "?? advised to execute this test case in automated mode.                  ??"
    puts "??                                                                       ??"
    puts "???????????????????????????????????????????????????????????????????????????"
    puts "???????????????????????????????????????????????????????????????????????????"
    puts ".\n"
}

ptp_ha::display_port_details

ptp_ha::dut_Initialization

proc cleanup {} {

    ptp_ha::dut_cleanup_setup_009

}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

### (Initial Part) ###

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency = 0, timestampCorrectionPortDS.ingressLatency = 0,   #
#          asymmetryCorrectionPortDS.constantAsymmetry = 0 and                #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient = 0.              #
###############################################################################

STEP 1

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP9)"

if {![ptp_ha::dut_configure_setup_009]} {

    LOG -msg "$dut_log_msg(INIT_SETUP9_F)"

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP9_F)"\
                       -tc_def  $tc_def
    return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
###############################################################################

STEP 2

LOG -level 0 -msg "$tee_log_msg(INIT)"

ptp_ha::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

    if {![PTP_HA_UNICAST_PRE_REQUISITE_CONFIGURATION\
                        -port_num       $tee_port_1\
                        -tee_mac        $tee_mac\
                        -tee_ip         $tee_ip\
                        -dut_ip         $dut_ip\
                        -vlan_id        $vlan_id\
                        -dut_mac        tee_dest_mac\
                        -timeout        $arp_timeout]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
                           -tc_def  $tc_def

        return 0
    }

}

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_1\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def  $tc_def

    return 0
}

set filter_id [lindex $id1 end]

ptp_ha::stop_at_step

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1


#(Part 1)#

###############################################################################
# Step 3 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x0B                                #
#                  Domain Number        = DN1                                 #
#                  Priority1            = X                                   #
###############################################################################

STEP 3

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
           -port_num                $tee_port_1\
           -domain_number           $domain\
           -filter_id               $filter_id\
           -recvd_gm_priority1      priority1]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                        -tc_def  $tc_def

    return 0
}

ptp_ha::stop_at_step

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 4 : Send periodic ANNOUNCE message on port T1 with following parameters#
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x0B                                #
#                  Domain Number        = DN1                                 #
#                  Priority1            = X-1                                 #
###############################################################################

STEP 4

set priority [expr $priority1 - 1]

LOG -level 0 -msg "$tee_log_msg(ANN_TX_T1)"

if {![ptp_ha::send_announce \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -gm_priority1            $priority\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -count                   $infinity]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANN_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 5 : Enable auto responder to respond every Delay_Req messages received #
#          on port T1.                                                        #
#                                                                             #
###############################################################################

STEP 5

LOG -level 0 -msg "$tee_log_msg(ENABLE_RESPONDER_TO_DELAY_REQ)"

ptp_ha::enable_auto_response_to_delay_requests

ptp_ha::stop_at_step

###############################################################################
# Step 6 : Send periodic SYNC message on port T1 with following parameters.   #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x00                                #
#                  Domain Number        = DN1                                 #
#                  Source Port Identity = E                                   #
#                  Sequence ID          = B                                   #
###############################################################################

STEP 6

LOG -level 0 -msg "$tee_log_msg(SYNC_TX_T1)"

if {![ptp_ha::send_sync \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -count                   $infinity]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 6a: If the clock is two step, send periodic FOLLOW_UP message on port  #
#          T1 with following parameters.                                      #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x08                                #
#                  Domain Number        = DN1                                 #
#                  Source Port Identity = E                                   #
#                  Sequence ID          = B                                   #
###############################################################################

if { $clock_step == "Two-Step" } {

STEP 6a

LOG -level 0 -msg "$tee_log_msg(FOLLOWUP_TX_T1)"

if {![ptp_ha::send_followup \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -count                   $infinity]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FOLLOWUP_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}
 }

ptp_ha::stop_at_step

###############################################################################
# Step 7 : Wait for 6s to complete BMCA.                                      #
#                                                                             #
###############################################################################

STEP 7

LOG -level 0 -msg "$dut_log_msg(WAIT_BMCA)"

sleep $::ptp_ha_bmca

ptp_ha::stop_at_step

###############################################################################
# Step 8 : Send periodic PTP SIGNALING message with L1 Sync TLV on the port P1#
#          with following parameters:                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 1                                          #
#                  RCR           = 1                                          #
#                  CR            = 1                                          #
#                  ITC           = 1                                          #
#                  IRC           = 1                                          #
#                  IC            = 1                                          #
###############################################################################

STEP 8

LOG -level 0 -msg   "$tee_log_msg(L1SYNC_TX_T1)"

if {![ptp_ha::send_signal \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -tlv_type                $tlv_type(l1sync)\
           -src_ip                  $tee_ip\
           -domain_number           $domain\
           -tcr                     $tcr_true\
           -rcr                     $rcr_true\
           -cr                      $cr_true\
           -itc                     $itc_true\
           -irc                     $irc_true\
           -ic                      $ic_true\
           -count                   $infinity]} {

    TEE_CLEANUP
    
    TC_CLEAN_AND_ABORT   -reason "$tee_log_msg(L1SYNC_TX_T1_F)"\
                         -tc_def  $tc_def

    return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 9 : Observe that the DUT's L1SYNC port status of P1 is L1_SYNC_UP.     #
###############################################################################

STEP 9

LOG -level 0 -msg  "$dut_log_msg(CHECK_STATE_L1_SYNC_UP_P1_V)"

sleep $::ptp_ha_l1sync_wait

if {![ptp_ha::dut_check_l1sync_state \
              -port_num                $dut_port_1\
              -l1sync_state            $l1_sync_up] } {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_L1_SYNC_UP_P1_F)"\
                       -tc_def $tc_def
    return 0 
}

ptp_ha::stop_at_step

###############################################################################
# Step 10: Check whether the non-zero absolute value of currentDS.            #
#          offsetFromMaster in DUT becomes lowest as possible to ensure that  #
#          the DUT synchronizes it's time with TEE.                           #
###############################################################################

STEP 10

LOG -level 0 -msg  "$dut_log_msg(CHECK_LOWEST_OFM_V)"

if {![ptp_ha::dut_check_for_least_offset_from_master\
                -offset_from_master         $offset_from_master]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_LOWEST_OFM_F)"\
                       -tc_def $tc_def
    return 0 
}

ptp_ha::stop_at_step

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 11: Send SYNC message on port T1 with following parameters.            #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x00                                #
#                  Domain Number        = DN1                                 #
#                  Source Port Identity = E                                   #
#                  Sequence ID          = B                                   #
#                  Origin Timestamp     = OTS1                                #
#                  Correction Field     = SYNC_CF1                            #
###############################################################################

STEP 11

LOG -level 0 -msg "$tee_log_msg(SYNC_TX_T1)"

set SYNC_CF1 0

if {![ptp_ha::send_sync \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -correction_field        $SYNC_CF1\
           -tx_origin_timestamp_sec tx_origin_timestamp_sec\
           -tx_origin_timestamp_ns  tx_origin_timestamp_ns\
           -domain_number           $domain\
           -count                   $infinity]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

while {$::ok_to_send_1 != 0} {

    LOG -level 3 -msg "Waiting to send Sync message."
}

###############################################################################
# Step 11a:If the clock is two step, send FOLLOW_UP message on port T1 with   #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x08                                #
#                  Domain Number        = DN1                                 #
#                  Source Port Identity = E                                   #
#                  Sequence ID          = B                                   #
#                  Correction Field     = FU_CF1                              #
###############################################################################

if { $clock_step == "Two-Step" } {

    STEP 11a

    LOG -level 0 -msg "$tee_log_msg(FOLLOWUP_TX_T1)"

    set FU_CF1 0

    if {![ptp_ha::send_followup \
                    -port_num                        $tee_port_1\
                    -src_mac                         $tee_mac\
                    -src_ip                          $tee_ip\
                    -unicast_flag                    $::unicast_flag\
                    -correction_field                $FU_CF1\
                    -domain_number                   $domain\
                    -precise_origin_timestamp_sec    $tx_origin_timestamp_sec\
                    -precise_origin_timestamp_ns     $tx_origin_timestamp_ns\
                    -tx_timestamp_sec                tx_timestamp_sec\
                    -tx_timestamp_ns                 tx_timestamp_ns\
                    -count                           $infinity]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(FOLLOWUP_TX_T1_F)"\
                            -tc_def  $tc_def

        return 0
    }

    while {$::ok_to_send_5 != 0} {

        LOG -level 3 -msg "Waiting to send Follow_Up message."
    }
}

ptp_ha::stop_at_step

###############################################################################
# Step 12: Get DUT's time and convert it into epoch Timestamp (DTS1).         #
###############################################################################

STEP 12

LOG -level 0 -msg "$dut_log_msg(CONVERT_DUT_EPOCH_DTS1)"

if {![ptp_ha::dut_get_timestamp \
              -timestamp         DTS1]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONVERT_DUT_EPOCH_DTS1_F)"\
                       -tc_def  $tc_def
    return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 13: Observe that time difference of DTS1 and SEET1 is lesser than 1    #
#          (i.e., DTS1 - SEET1 < 1)                                           #
###############################################################################

STEP 13

LOG -level 0 -msg "$dut_log_msg(TIME_DIFF_DTS1_SEET1_LESSER_1_O)"

if { $clock_step == "One-Step" } {

    set ots_s   [format "%.f" [expr $::gtx_origin_timestamp_sec * 1000000000.0]]
    set ots_ns  [format "%.f" [expr $::gtx_origin_timestamp_ns * 1.0]]

    set OTS1 [expr $ots_s + $ots_ns]

    set OTS1        [format "%.f" [expr $OTS1 * 1.0]]
    set SYNC_CF1    [format "%.f" [expr $SYNC_CF1 * 1.0]]

    set SEET1       [expr $OTS1 + $SYNC_CF1]

    LOG -level 3 -msg "OTS sec  = [nano $::gtx_origin_timestamp_sec]"
    LOG -level 3 -msg "OTS nsec = [nano $::gtx_origin_timestamp_ns]"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "OTS1     = [nano $OTS1] ns"
    LOG -level 3 -msg "SYNC_CF1 = [nano $SYNC_CF1] ns"
    LOG -level 3 -msg "------------------------------------------"
    LOG -level 3 -msg "SEET1    = [nano $SEET1] (formula: OTS1 + SYNC_CF1)"

} else {

    set st_s    [format "%.f" [expr $::gtx_follow_up_timestamp_sec * 1000000000.0]]
    set st_ns   [format "%.f" [expr $::gtx_follow_up_timestamp_ns * 1.0]]

    set systemTime1 [expr $st_s + $st_ns]

    set SYNC_CF1    [format "%.f" [expr $SYNC_CF1 * 1.0]]
    set FU_CF1      [format "%.f" [expr $FU_CF1 * 1.0]]
    set systemTime1 [format "%.f" [expr $systemTime1 * 1.0]]

    set SEET1        [format "%.f" [expr ($SYNC_CF1 + $FU_CF1 + $systemTime1) * 1.0]]

    LOG -level 3 -msg "ST sec   = [nano $::gtx_follow_up_timestamp_sec]"
    LOG -level 3 -msg "ST nsec  = [nano $::gtx_follow_up_timestamp_ns]"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "SYNC_CF1 = [nano $SYNC_CF1] ns"
    LOG -level 3 -msg "FU_CF1   = [nano $FU_CF1] ns"
    LOG -level 3 -msg "ST1      = [nano $systemTime1] ns"
    LOG -level 3 -msg "------------------------------------------"
    LOG -level 3 -msg "SEET1    = [nano $SEET1] ns (formula: SYNC_CF1 + FU_CF1 + ST1)"
}

LOG -level 3 -msg ""
LOG -level 3 -msg "DTS1         = [nano $DTS1] ns"
LOG -level 3 -msg "SEET1        = [nano $SEET1] ns"
LOG -level 3 -msg "----------------------------------------------"
LOG -level 0 -msg "DTS1 - SEET1 = [nano [expr  $DTS1 - $SEET1]] ns (formula: DTS1 - SEET1)"

if {!([expr $DTS1 - $SEET1] < 1)} {

if {$SKIP_VALIDATION != 1} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(TIME_DIFF_DTS1_SEET1_LESSER_1_F)"\
                       -tc_def  $tc_def
    return 0
}
}

ptp_ha::stop_at_step

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 14: Configure ingressLatency on port P1 by setting ingressLatency to   #
#          4 294 967 296 ns (i.e., the value of dataset expressed in          #
#          TimeInterval asymmetryCorrectionPortDS.ingressLatency = 2^48).     #
#                                                                             #
###############################################################################

STEP 14

set latency 4294967296

LOG -level 0 -msg "$dut_log_msg(CONFIG_TXPORTDS.INGRESSLATENCY)"

if {![ptp_ha::dut_set_ingress_latency\
             -port_num     $dut_port_1\
             -latency      $latency]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_TXPORTDS.INGRESSLATENCY_F)"\
                        -tc_def  $tc_def

     return 0
}

ptp_ha::stop_at_step

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 15: Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV  #
#          on the port P1 with following parameters:                          #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                                                                             #
###############################################################################

STEP 15

LOG -level 0 -msg "$tee_log_msg(L1SYNC_RX_P1_O)"

if {![ptp_ha::recv_signal \
              -port_num               $tee_port_1\
              -domain_number          $domain\
              -error_reason           error_reason\
              -tlv_type               $tlv_type(l1sync)\
              -filter_id              $filter_id]} {

      TEE_CLEANUP

      TC_CLEAN_AND_ABORT -reason "$tee_log_msg(L1SYNC_RX_P1_F) $error_reason"\
                         -tc_def  $tc_def

      return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 16: Send periodic SYNC message on port T1 with following parameters.   #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x00                                #
#                  Domain Number        = DN1                                 #
#                  Source Port Identity = E                                   #
#                  Sequence ID          = B                                   #
###############################################################################

STEP 16

LOG -level 0 -msg "$tee_log_msg(SYNC_TX_T1)"

if {![ptp_ha::send_sync \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -count                   $infinity]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 16a:If the clock is two step, send periodic FOLLOW_UP message on port  #
#          T1 with following parameters.                                      #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x08                                #
#                  Domain Number        = DN1                                 #
#                  Source Port Identity = E                                   #
#                  Sequence ID          = B                                   #
###############################################################################

if { $clock_step == "Two-Step" } {

STEP 16a

LOG -level 0 -msg "$tee_log_msg(FOLLOWUP_TX_T1)"

if {![ptp_ha::send_followup \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -count                   $infinity]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FOLLOWUP_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}
 }

ptp_ha::stop_at_step

###############################################################################
# Step 17: Check whether the non-zero absolute value of currentDS.            #
#          offsetFromMaster in DUT becomes lowest as possible to ensure that  #
#          the DUT synchronizes it's time with TEE.                           #
###############################################################################

STEP 17

LOG -level 0 -msg  "$dut_log_msg(CHECK_LOWEST_OFM_V)"

if {![ptp_ha::dut_check_for_least_offset_from_master \
                -offset_from_master         $offset_from_master]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_LOWEST_OFM_F)"\
                       -tc_def $tc_def
    return 0 
}

ptp_ha::stop_at_step

###############################################################################
# Step 18: Send SYNC message on port T1 with following parameters.            #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x00                                #
#                  Domain Number        = DN1                                 #
#                  Source Port Identity = E                                   #
#                  Sequence ID          = B                                   #
#                  Origin Timestamp     = OTS2                                #
#                  Correction Field     = SYNC_CF2                            #
###############################################################################

STEP 18

LOG -level 0 -msg "$tee_log_msg(SYNC_TX_T1)"

set SYNC_CF2 0

if {![ptp_ha::send_sync \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -correction_field        $SYNC_CF2\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -tx_origin_timestamp_sec tx_origin_timestamp_sec\
           -tx_origin_timestamp_ns  tx_origin_timestamp_ns\
           -count                   $infinity]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

while {$::ok_to_send_1 != 0} {

    LOG -level 3 -msg "Waiting to send Sync message."
}

###############################################################################
# Step 18a:If the clock is two step, send FOLLOW_UP message on port T1 with   #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x08                                #
#                  Domain Number        = DN1                                 #
#                  Source Port Identity = E                                   #
#                  Sequence ID          = B                                   #
#                  Correction Field     = FU_CF2                              #
###############################################################################

if { $clock_step == "Two-Step" } {

    STEP 18a

    LOG -level 0 -msg "$tee_log_msg(FOLLOWUP_TX_T1)"

    set FU_CF2 0

    if {![ptp_ha::send_followup \
                    -port_num                        $tee_port_1\
                    -src_mac                         $tee_mac\
                    -src_ip                          $tee_ip\
                    -unicast_flag                    $::unicast_flag\
                    -correction_field                $FU_CF2\
                    -domain_number                   $domain\
                    -precise_origin_timestamp_sec    $tx_origin_timestamp_sec\
                    -precise_origin_timestamp_ns     $tx_origin_timestamp_ns\
                    -tx_timestamp_sec                tx_timestamp_sec\
                    -tx_timestamp_ns                 tx_timestamp_ns\
                    -count                           $infinity]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(FOLLOWUP_TX_T1_F)"\
                            -tc_def  $tc_def

        return 0
    }

    while {$::ok_to_send_5 != 0} {

        LOG -level 3 -msg "Waiting to send Follow_Up message."
    }
}

ptp_ha::stop_at_step

###############################################################################
# Step 19: Get DUT's time and convert it into epoch Timestamp (DTS2).         #
###############################################################################

STEP 19

LOG -level 0 -msg "$dut_log_msg(CONVERT_DUT_EPOCH_DTS2)"

if {![ptp_ha::dut_get_timestamp \
              -timestamp      DTS2]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONVERT_DUT_EPOCH_DTS2_F)"\
                       -tc_def  $tc_def
    return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 20: Observe that time difference of DTS2 and SEET2 is greater than 1.  #
#          (i.e., DTS2 - SEET2 > 1)                                           #
###############################################################################

STEP 20

LOG -level 0 -msg "$dut_log_msg(TIME_DIFF_DTS2_SEET2_GREATER_1_O)"

if { $clock_step == "One-Step" } {

    set ots_s   [format "%.f" [expr $::gtx_origin_timestamp_sec * 1000000000.0]]
    set ots_ns  [format "%.f" [expr $::gtx_origin_timestamp_ns * 1.0]]

    set OTS2 [expr $ots_s + $ots_ns]

    set OTS2        [format "%.f" [expr $OTS2 * 1.0]]
    set SYNC_CF2    [format "%.f" [expr $SYNC_CF2 * 1.0]]

    set SEET2       [expr $OTS2 + $SYNC_CF2]

    LOG -level 3 -msg "OTS sec  = [nano $::gtx_origin_timestamp_sec]"
    LOG -level 3 -msg "OTS nsec = [nano $::gtx_origin_timestamp_ns]"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "OTS2     = [nano $OTS2] ns"
    LOG -level 3 -msg "SYNC_CF2 = [nano $SYNC_CF2] ns"
    LOG -level 3 -msg "------------------------------------------"
    LOG -level 3 -msg "SEET2    = [nano $SEET2] ns (formula: OTS2 + SYNC_CF2)"

} else {

    set st_s    [format "%.f" [expr $::gtx_follow_up_timestamp_sec * 1000000000.0]]
    set st_ns   [format "%.f" [expr $::gtx_follow_up_timestamp_ns * 1.0]]

    set systemTime2 [expr $st_s + $st_ns]

    set SYNC_CF2    [format "%.f" [expr $SYNC_CF2 * 1.0]]
    set FU_CF2      [format "%.f" [expr $FU_CF2 * 1.0]]
    set systemTime2 [format "%.f" [expr $systemTime2 * 1.0]]

    set SEET2        [expr $SYNC_CF2 + $FU_CF2 + $systemTime2]

    LOG -level 3 -msg "ST sec   = [nano $::gtx_follow_up_timestamp_sec]"
    LOG -level 3 -msg "ST nsec  = [nano $::gtx_follow_up_timestamp_ns]"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "SYNC_CF2 = [nano $SYNC_CF2] ns"
    LOG -level 3 -msg "FU_CF2   = [nano $FU_CF2] ns"
    LOG -level 3 -msg "ST2      = [nano $systemTime2] ns"
    LOG -level 3 -msg "------------------------------------------"
    LOG -level 3 -msg "SEET2    = [nano $SEET2] ns (formula: SYNC_CF2 + FU_CF2 + ST2)"
}

LOG -level 3 -msg ""
LOG -level 3 -msg "DTS2         = [nano $DTS2] ns"
LOG -level 3 -msg "SEET2        = [nano $SEET2] ns"
LOG -level 3 -msg "---------------------------------------------"
LOG -level 0 -msg "DTS2 - SEET2 = [nano [expr  $DTS2 - $SEET2]] ns (formula: DTS2 - SEET2)"


if {! ([expr $DTS2 - $SEET2] > 1)} {

if {$SKIP_VALIDATION != 1} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(TIME_DIFF_DTS2_SEET2_GREATER_1_F)"\
                       -tc_def  $tc_def
    return 0
}
}

ptp_ha::stop_at_step

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 21: Configure ingressLatency on port P1 by setting ingressLatency to   #
#          -4 294 967 296 ns (i.e., the value of dataset expressed in         #
#          TimeInterval asymmetryCorrectionPortDS.ingressLatency = -2^48).    #
#                                                                             #
###############################################################################

STEP 21

set latency -4294967296

LOG -level 0 -msg "$dut_log_msg(CONFIG_TXPORTDS.INGRESSLATENCY)"

if {![ptp_ha::dut_set_ingress_latency\
             -port_num     $dut_port_1\
             -latency      $latency]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_TXPORTDS.INGRESSLATENCY_F)"\
                        -tc_def  $tc_def

     return 0
}

ptp_ha::stop_at_step

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 22: Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV  #
#          on the port P1 with following parameters:                          #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                                                                             #
###############################################################################

STEP 22

LOG -level 0 -msg "$tee_log_msg(L1SYNC_RX_P1_O)"

if {![ptp_ha::recv_signal \
              -port_num               $tee_port_1\
              -domain_number          $domain\
              -error_reason           error_reason\
              -tlv_type               $tlv_type(l1sync)\
              -filter_id              $filter_id]} {

      TEE_CLEANUP

      TC_CLEAN_AND_ABORT -reason "$tee_log_msg(L1SYNC_RX_P1_F) $error_reason"\
                         -tc_def  $tc_def

      return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 23: Send periodic SYNC message on port T1 with following parameters.   #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x00                                #
#                  Domain Number        = DN1                                 #
#                  Source Port Identity = E                                   #
#                  Sequence ID          = B                                   #
###############################################################################

STEP 23

LOG -level 0 -msg "$tee_log_msg(SYNC_TX_T1)"

if {![ptp_ha::send_sync \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -count                   $infinity]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 23a:If the clock is two step, send periodic FOLLOW_UP message on port  #
#          T1 with following parameters.                                      #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x08                                #
#                  Domain Number        = DN1                                 #
#                  Source Port Identity = E                                   #
#                  Sequence ID          = B                                   #
###############################################################################

if { $clock_step == "Two-Step" } {

STEP 23a

LOG -level 0 -msg "$tee_log_msg(FOLLOWUP_TX_T1)"

if {![ptp_ha::send_followup \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -count                   $infinity]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FOLLOWUP_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}
 }

ptp_ha::stop_at_step

###############################################################################
# Step 24: Check whether the non-zero absolute value of currentDS.            #
#          offsetFromMaster in DUT becomes lowest as possible to ensure that  #
#          the DUT synchronizes it's time with TEE.                           #
###############################################################################

STEP 24

LOG -level 0 -msg  "$dut_log_msg(CHECK_LOWEST_OFM_V)"

if {![ptp_ha::dut_check_for_least_offset_from_master \
                -offset_from_master         $offset_from_master]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_LOWEST_OFM_F)"\
                       -tc_def $tc_def
    return 0 
}

ptp_ha::stop_at_step

###############################################################################
# Step 25: Send SYNC message on port T1 with following parameters.            #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x00                                #
#                  Domain Number        = DN1                                 #
#                  Source Port Identity = E                                   #
#                  Sequence ID          = B                                   #
#                  Origin Timestamp     = OTS3                                #
#                  Correction Field     = SYNC_CF3                            #
#                                                                             #
###############################################################################

STEP 25

LOG -level 0 -msg "$tee_log_msg(SYNC_TX_T1)"

set SYNC_CF3 0

if {![ptp_ha::send_sync \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -correction_field        $SYNC_CF3\
           -domain_number           $domain\
           -tx_origin_timestamp_sec tx_origin_timestamp_sec\
           -tx_origin_timestamp_ns  tx_origin_timestamp_ns\
           -count                   $infinity]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

while {$::ok_to_send_1 != 0} {

    LOG -level 3 -msg "Waiting to send Sync message."
}

###############################################################################
# Step 25a:If the clock is two step, send FOLLOW_UP message on port T1 with   #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x08                                #
#                  Domain Number        = DN1                                 #
#                  Source Port Identity = E                                   #
#                  Sequence ID          = B                                   #
#                  Correction Field     = FU_CF3                              #
#                                                                             #
###############################################################################

if { $clock_step == "Two-Step" } {

    STEP 25a

    LOG -level 0 -msg "$tee_log_msg(FOLLOWUP_TX_T1)"

    set FU_CF3 0

    if {![ptp_ha::send_followup \
                    -port_num                        $tee_port_1\
                    -src_mac                         $tee_mac\
                    -src_ip                          $tee_ip\
                    -unicast_flag                    $::unicast_flag\
                    -correction_field                $FU_CF3\
                    -domain_number                   $domain\
                    -precise_origin_timestamp_sec    $tx_origin_timestamp_sec\
                    -precise_origin_timestamp_ns     $tx_origin_timestamp_ns\
                    -tx_timestamp_sec                tx_timestamp_sec\
                    -tx_timestamp_ns                 tx_timestamp_ns\
                    -count                           $infinity]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(FOLLOWUP_TX_T1_F)"\
                            -tc_def  $tc_def

        return 0
    }

    while {$::ok_to_send_5 != 0} {

        LOG -level 3 -msg "Waiting to send Follow_Up message."
    }
}

ptp_ha::stop_at_step

###############################################################################
# Step 26: Get DUT's time and convert it into epoch Timestamp (DTS3).         #
###############################################################################

STEP 26

LOG -level 0 -msg "$dut_log_msg(CONVERT_DUT_EPOCH_DTS3)"

if {![ptp_ha::dut_get_timestamp \
              -timestamp        DTS3]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONVERT_DUT_EPOCH_DTS3_F)"\
                       -tc_def  $tc_def
    return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 27: Verify that time difference of SEET3 and DTS3 is greater than 1.   #
#          (i.e., SEET3 - DTS3 > 1)                                           #
###############################################################################

STEP 27

LOG -level 0 -msg "$dut_log_msg(TIME_DIFF_SEET3_DTS3_GREATER_1_V)"

if { $clock_step == "One-Step" } {

    set ots_s   [format "%.f" [expr $::gtx_origin_timestamp_sec * 1000000000.0]]
    set ots_ns  [format "%.f" [expr $::gtx_origin_timestamp_ns * 1.0]]

    set OTS3 [expr $ots_s + $ots_ns]

    set OTS3        [format "%.f" [expr $OTS3 * 1.0]]
    set SYNC_CF3    [format "%.f" [expr $SYNC_CF3 * 1.0]]

    set SEET3       [expr $OTS3 + $SYNC_CF3]

    LOG -level 3 -msg "OTS sec  = [nano $::gtx_origin_timestamp_sec]"
    LOG -level 3 -msg "OTS nsec = [nano $::gtx_origin_timestamp_ns]"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "OTS3     = [nano $OTS3] ns"
    LOG -level 3 -msg "SYNC_CF3 = [nano $SYNC_CF3] ns"
    LOG -level 3 -msg "------------------------------------------"
    LOG -level 3 -msg "SEET3    = [nano $SEET3] ns (formula: OTS3 + SYNC_CF3)"

} else {

    set st_s    [format "%.f" [expr $::gtx_follow_up_timestamp_sec * 1000000000.0]]
    set st_ns   [format "%.f" [expr $::gtx_follow_up_timestamp_ns * 1.0]]

    set systemTime3 [expr $st_s + $st_ns]

    set SYNC_CF3    [format "%.f" [expr $SYNC_CF3 * 1.0]]
    set FU_CF3      [format "%.f" [expr $FU_CF3 * 1.0]]
    set systemTime3 [format "%.f" [expr $systemTime3 * 1.0]]

    set SEET3        [expr $SYNC_CF3 + $FU_CF3 + $systemTime3]

    LOG -level 3 -msg "ST sec   = [nano $::gtx_follow_up_timestamp_sec]"
    LOG -level 3 -msg "ST nsec  = [nano $::gtx_follow_up_timestamp_ns]"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "SYNC_CF3 = [nano $SYNC_CF3] ns"
    LOG -level 3 -msg "FU_CF3   = [nano $FU_CF3] ns"
    LOG -level 3 -msg "ST3      = [nano $systemTime3] ns"
    LOG -level 3 -msg "------------------------------------------"
    LOG -level 3 -msg "SEET3    = [nano $SEET3] ns (formula: SYNC_CF3 + FU_CF3 + ST3)"

}

LOG -level 3 -msg ""
LOG -level 3 -msg "SEET3        = [nano $SEET3] ns"
LOG -level 3 -msg "DTS3         = [nano $DTS3] ns"
LOG -level 3 -msg "---------------------------------------------"
LOG -level 0 -msg "SEET3 - DTS3 = [nano [expr $SEET3 - $DTS3]] ns (formula: SEET3 - DTS3)"

if {! ([expr $SEET3 - $DTS3] > 1) } {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL -reason  "PTP enabled device does not generate Ingress\
                                timestamp in Sync (event) messages from\
                                timestampCorrectionPortDS.ingressLatency when using\
                                Delay Request-Response mechanism"\
                       -tc_def  $tc_def
    return 0
}

TEE_CLEANUP

TC_CLEAN_AND_PASS -reason  "PTP enabled device generates Ingress\
                            timestamp in Sync (event) messages from\
                            timestampCorrectionPortDS.ingressLatency when using\
                            Delay Request-Response mechanism"\
                  -tc_def   $tc_def

return 1

############################### END OF TEST CASE ###############################
