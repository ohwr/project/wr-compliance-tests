#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_ptp-ha_pag_010                                  #
# Test Case Version : 1.0                                                     #
# Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    #
# Module Name       : PTP Accuracy Group (PAG)                                #
#                                                                             #
# Title             : Ingress timestamp in Pdelay_Req message                 #
#                                                                             #
# Purpose           : To verify that a PTP enabled device generates Ingress   #
#                     timestamp in Pdelay_Req (event) messages from           #
#                     timestampCorrectionPortDS.ingressLatency when using     #
#                     Peer to Peer Delay mechanism.                           #
#                                                                             #
# Reference         : IEEE 1588-2017 Clause 16.7.1 Page 301, Clause 7.3.4.2   #
#                     Page 68, Clause 8.2.16.2 Page 128, Clause 11.3.2        #
#                     Page 192                                                #
#                                                                             #
# Conformance Type  : SHALL                                                   #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 002                                                     #
# Test Topology     : 001                                                     #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode - one-step/two-step> |           #
#           |                        <Set delay mechanism as P2P> |           #
#           |                     <Configure logAnnounceInterval, | P1        #
#           |                             announceReceiptTimeout> |           #
#           |                         <Configure logSyncInterval> | P1        #
#           |                  <Configure logMinDelayReqInterval> | P1        #
#           |                    <Configure Priority1, Priority2> | P1        #
#           |                                     <Enable L1SYNC> | P1        #
#           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        #
#           |                     <Disable L1SynOptParams option> | P1        #
#           |           <Enable asymmetryCorrectionPortDS.enable> | P1        #
#           | <Configure timestampCorrectionPortDS.egressLatency, | P1        #
#           |           timestampCorrectionPortDS.ingressLatency, |           #
#           |        asymmetryCorrectionPortDS.constantAsymmetry, |           #
#           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                          DN = DN1, PRI1 = X]        |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | ANNOUNCE [MSG_TYPE = 0x0B,                          |           #
#           |   DN = DN1, PRI1 = X+1]                             |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           | PDELAY_REQ [MSG_TYPE = 0x02, DN = DN1,              |           #
#           |  SRC_PRT_ID = E, SEQ_ID = D,                        |           #
#           |  ORG_TS = 0, CF = 0](TS1)                           |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |             PDELAY_RESP [MSG_TYPE = 0x03, DN = DN1, |           #
#           |                      SRC_MAC = SRC1, CLK_ID = CLK1, |           #
#           |                          REQ_PRT_ID = E, SEQ_ID = D |           #
#           |                   RRT = RRT1, CF = PDRESP_CF1](TS2) |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                     <If Clock Step = Two Step, then |           #
#           |           receive Pdelay_Resp_Follow_Up and include |           #
#           |                     ROT = ROT1, CF = PDRESP_FU_CF1> |           #
#           |                                                     |           #
#           | <Calculate meanDelay (MD1)>                         |           #
#           |                                                     |           #
#           | <Configure timestampCorrectionPortDS.ingressLatency |           #
#           |                                      value to 2^32> |           #
#           |                                                     |           #
#           | PDELAY_REQ [MSG_TYPE = 0x02, DN = DN1,              |           #
#           |  CLK_ID = CLK1, SRC_PRT_ID = E, SEQ_ID = D,         |           #
#           |  ORG_TS = 0, CF = 0](TS1)                           |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |             PDELAY_RESP [MSG_TYPE = 0x03, DN = DN1, |           #
#           |                      SRC_MAC = SRC1, CLK_ID = CLK1, |           #
#           |                          REQ_PRT_ID = E, SEQ_ID = D |           #
#           |                   RRT = RRT2, CF = PDRESP_CF2](TS2) |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                     <If Clock Step = Two Step, then |           #
#           |           receive Pdelay_Resp_Follow_Up and include |           #
#           |                     ROT = ROT2, CF = PDRESP_FU_CF2> |           #
#           |                                                     |           #
#           | <Calculate meanDelay (MD2)>                         |           #
#           |                                                     |           #
#           | <Configure timestampCorrectionPortDS.ingressLatency |           #
#           |                                value to -2^(32+16)> |           #
#           |                                                     |           #
#           | PDELAY_REQ [MSG_TYPE = 0x02, DN = DN1,              |           #
#           |  SRC_PRT_ID = E, SEQ_ID = D,                        |           #
#           |  ORG_TS = 0, CF = 0](TS1)                           |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |             PDELAY_RESP [MSG_TYPE = 0x03, DN = DN1, |           #
#           |                      SRC_MAC = SRC1, CLK_ID = CLK1, |           #
#           |                          REQ_PRT_ID = E, SEQ_ID = D |           #
#           |                   RRT = RRT3, CF = PDRESP_CF3](TS2) |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                     <If Clock Step = Two Step, then |           #
#           |           receive Pdelay_Resp_Follow_Up and include |           #
#           |                     ROT = ROT3, CF = PDRESP_FU_CF3> |           #
#           |                                                     |           #
#           | <Calculate meanDelay (MD3)>                         |           #
#           |                                                     |           #
#           |                MD2 < MD1 & MD3 > MD1                |           #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     MSG_TYPE  = Message Type                                                #
#     DN        = Domain Number                                               #
#     PRT1      = priority1                                                   #
#                                                                             #
# NOTE :                                                                      #
#                                                                             #
#  1. This objective is verified using the High Accuracy Peer to Peer Default #
#     PTP Profile                                                             #
#                                                                             #
#  2. meanDelay is calculated with below formulae:                            #
#     * For one-step clock:                                                   #
#                                                                             #
#        [(t4 - t1) - <correctedPdelayRespCorrectionField>]/2                 #
#                                                                             #
#     * For two-step clock:                                                   #
#        [(t4 - t1) - (responseOriginTimestamp - requestReceiptTimestamp) -   #
#        <correctedPdelayRespCorrectionField> - correctionField of            #
#        Pdelay_Resp_Follow_Up]/2                                             #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure delaymechanism as Peer to peer.                          #
#    vi.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#   vii.   Enable L1SYNC on DUT's port P1.                                    #
#  viii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#    ix.   Disable L1SynOptParams on DUT.                                     #
#     x.   Enable asymmetryCorrectionPortDS.enable.                           #
#    xi.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency = 0, timestampCorrectionPortDS.egressLatency = 0,    #
#          asymmetryCorrectionPortDS.constantAsymmetry = 0 and                #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient = 0.              #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x0B                                    #
#                  Domain Number    = DN1                                     #
#                  Priority1        = X                                       #
#                                                                             #
# Step 4 : Send periodic ANNOUNCE message on port T1 with following parameters#
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x0B                                    #
#                  Domain Number    = DN1                                     #
#                  Priority1        = X+1                                     #
#                                                                             #
# Step 5 : Send PDELAY_REQ message on the port T1 with following parameters   #
#          and store timestamp TS1.                                           #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x02                                #
#                  Domain Number        = DN1                                 #
#                  Clock ID             = CLK1                                #
#                  Sequence ID          = D                                   #
#                  Source Port Identity = E                                   #
#                  Origin Timestamp     = 0                                   #
#                  Correction Field     = 0                                   #
#                                                                             #
# Step 6 : Observe that the DUT transmits PDELAY_RESP on port T1 and with     #
#          following parameters and store timestamp TS2.                      #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type              = 0x03                           #
#                  Domain Number             = DN1                            #
#                  Source Mac                = SRC1                           #
#                  Clock ID                  = CLK1                           #
#                  Sequence Id               = D                              #
#                  Requesting Port Identity  = E                              #
#                  Request Receipt Timestamp = RRT1                           #
#                  Correction Field          = PDRESP_CF1                     #
#                                                                             #
# Step 6a: If the clock is two step, observe that DUT transmits               #
#          PDELAY_RESP_FOLLOW_UP message on port P1 with following parameters.#
#                                                                             #
#              PTP Header                                                     #
#                  Message Type              = 0x0A                           #
#                  Domain Number             = DN1                            #
#                  Sequence Id               = D                              #
#                  Source Port Identity      = F                              #
#                  Requesting Port Identity  = E                              #
#                  Correction Field          = PDRESP_FU_CF1                  #
#                                                                             #
# Step 7 : Calculate meanDelay (MD1) at TEE.                                  #
#                                                                             #
# Step 8 : Configure ingressLatency on port P1 by setting ingressLatency to   #
#          4 294 967 296 ns (i.e., the value of dataset expressed in          #
#          TimeInterval asymmetryCorrectionPortDS.ingressLatency = 2^48).     #
#                                                                             #
# Step 9 : Send PDELAY_REQ message on the port T1 with following parameters   #
#          and store timestamp TS1.                                           #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x02                                #
#                  Domain Number        = DN1                                 #
#                  Sequence ID          = D                                   #
#                  Source Port Identity = E                                   #
#                  Origin Timestamp     = 0                                   #
#                  Correction Field     = 0                                   #
#                                                                             #
# Step 10: Observe that the DUT transmits PDELAY_RESP on port T1 and with     #
#          following parameters and store timestamp TS2.                      #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type              = 0x03                           #
#                  Domain Number             = DN1                            #
#                  Source Mac                = SRC1                           #
#                  Clock ID                  = CLK1                           #
#                  Sequence Id               = D                              #
#                  Requesting Port Identity  = E                              #
#                  Request Receipt Timestamp = RRT2                           #
#                  Correction Field          = PDRESP_CF2                     #
#                                                                             #
# Step 10a:If the clock is two step, observe that DUT transmits               #
#          PDELAY_RESP_FOLLOW_UP message on port P1 with following parameters.#
#                                                                             #
#              PTP Header                                                     #
#                  Message Type              = 0x0A                           #
#                  Domain Number             = DN1                            #
#                  Sequence Id               = D                              #
#                  Source Port Identity      = F                              #
#                  Requesting Port Identity  = E                              #
#                  Correction Field          = PDRESP_FU_CF2                  #
#                                                                             #
# Step 11: Calculate meanDelay (MD2) at TEE.                                  #
#                                                                             #
# Step 12: Configure ingressLatency on port P1 by setting ingressLatency to   #
#          -4 294 967 296 ns (i.e., the value of dataset expressed in         #
#          TimeInterval asymmetryCorrectionPortDS.ingressLatency = -2^48).    #
#                                                                             #
# Step 13: Send PDELAY_REQ message on the port T1 with following parameters   #
#          and store timestamp TS1.                                           #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x02                                #
#                  Domain Number        = DN1                                 #
#                  Sequence ID          = D                                   #
#                  Source Port Identity = E                                   #
#                  Origin Timestamp     = 0                                   #
#                  Correction Field     = 0                                   #
#                                                                             #
# Step 14: Observe that the DUT transmits PDELAY_RESP on port T1 and with     #
#          following parameters and store timestamp TS2.                      #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type              = 0x03                           #
#                  Domain Number             = DN1                            #
#                  Source Mac                = SRC1                           #
#                  Clock ID                  = CLK1                           #
#                  Sequence Id               = D                              #
#                  Requesting Port Identity  = E                              #
#                  Request Receipt Timestamp = RRT3                           #
#                  Correction Field          = PDRESP_CF3                     #
#                                                                             #
# Step 14a:If the clock is two step, observe that DUT transmits               #
#          PDELAY_RESP_FOLLOW_UP message on port P1 with following parameters.#
#                                                                             #
#              PTP Header                                                     #
#                  Message Type              = 0x0A                           #
#                  Domain Number             = DN1                            #
#                  Sequence Id               = D                              #
#                  Source Port Identity      = F                              #
#                  Requesting Port Identity  = E                              #
#                  Correction Field          = PDRESP_FU_CF3                  #
#                                                                             #
# Step 15: Calculate meanDelay (MD3) at TEE.                                  #
#                                                                             #
# Step 16: Verify that MD2 < MD1 & MD3 > MD1.                                 #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          Oct/2018      CERN          Initial                            #
#                                                                             #
###############################################################################
# Copyright (c) 2018 CERN                                                     #
###############################################################################

global dut_log_msg
global tee_log_msg
global session_id

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def  "To verify that a PTP enabled device generates Ingress\
             timestamp in Pdelay_Req (event) messages from\
             timestampCorrectionPortDS.ingressLatency when using\
             Peer to Peer Delay mechanism."

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set dut_port_1                     $::dut_port_num_1
set vlan_id                        $::ptp_ha_vlan_id
set dut_ip                         $::dut_test_port_ip
set dut_dest_ip                    $::dut_test_port_ip
set tee_dest_ip                    $::tee_test_port_ip
set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT

set domain                         $::DEFAULT_DOMAIN
set infinity                       $::INFINITY
set clock_step                     $::ptp_dut_clock_step
set sequence_id_ann                1

set sequence_id                    1
set correction_field               0

########################### END - INITIALIZATION ##############################

if {($env(TEST_DEVICE_MANAGEMENT_MODE) == "Manual")} {
    
    puts ".\n"
    puts "???????????????????????????????????????????????????????????????????????????"
    puts "???????????????????????????????????????????????????????????????????????????"
    puts "?? WARNING: Execution of this test in manual management mode may give    ??"
    puts "??          in-correct result.                                           ??"
    puts "??                                                                       ??"
    puts "?? The timestamps from the device should be taken faster and hence it is ??"
    puts "?? advised to execute this test case in automated mode.                  ??"
    puts "??                                                                       ??"
    puts "???????????????????????????????????????????????????????????????????????????"
    puts "???????????????????????????????????????????????????????????????????????????"
    puts ".\n"
}

if { $::ptp_p2p_delay_mechanism != "true" } {

    TC_ABORT -reason "$tee_log_msg(P2P_DLY_MECHANISM)"\
             -tc_def $tc_def

    return 0
}

ptp_ha::display_port_details

ptp_ha::dut_Initialization

proc cleanup {} {

    ptp_ha::dut_cleanup_setup_002

    ptp_ha::dut_reset_ingress_latency\
             -port_num      $::dut_port_num_1

}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

### (Initial Part) ###

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure delaymechanism as Peer to peer.                          #
#    vi.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#   vii.   Enable L1SYNC on DUT's port P1.                                    #
#  viii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#    ix.   Disable L1SynOptParams on DUT.                                     #
#     x.   Enable asymmetryCorrectionPortDS.enable.                           #
#    xi.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency = 0, timestampCorrectionPortDS.egressLatency = 0,    #
#          asymmetryCorrectionPortDS.constantAsymmetry = 0 and                #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient = 0.              #
###############################################################################

STEP 1

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP2)"

if {![ptp_ha::dut_configure_setup_002]} {

    LOG -msg "$dut_log_msg(INIT_SETUP2_F)"

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP2_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
###############################################################################

STEP 2

LOG -level 0 -msg "$tee_log_msg(INIT)"

ptp_ha::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

    if {![PTP_HA_UNICAST_PRE_REQUISITE_CONFIGURATION\
                        -port_num       $tee_port_1\
                        -tee_mac        $tee_mac\
                        -tee_ip         $tee_ip\
                        -dut_ip         $dut_ip\
                        -vlan_id        $vlan_id\
                        -dut_mac        tee_dest_mac\
                        -timeout        $arp_timeout]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
                           -tc_def $tc_def

        return 0
    }

}

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_1\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def  $tc_def

    return 0
}

set filter_id [lindex $id1 end]

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

ptp_ha::enable_auto_response_to_pdelay_requests

# (Part 1) # 

###############################################################################
#                                                                             #
# Step 3 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x0B                                    #
#                  Domain Number    = DN1                                     #
#                  Priority1        = X                                       #
###############################################################################

STEP 3

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
           -port_num                $tee_port_1\
           -domain_number           $domain\
           -filter_id               $filter_id\
           -error_reason            error_reason\
           -recvd_gm_priority1      priority1]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANNOUNCE_RX_P1_F) $error_reason"\
                        -tc_def  $tc_def

    return 0
}

###############################################################################
#                                                                             #
# Step 4 : Send periodic ANNOUNCE message on port T1 with following parameters#
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x0B                                    #
#                  Domain Number    = DN1                                     #
#                  Priority1        = X+1                                     #
###############################################################################

STEP 4

set priority [expr $priority1 + 1]

LOG -level 0 -msg "$tee_log_msg(ANN_TX_T1)"

if {![ptp_ha::send_announce \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -gm_priority1            $priority\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -count                   $infinity]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANN_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
#                                                                             #
# Step 5 : Send PDELAY_REQ message on the port T1 with following parameters   #
#          and store timestamp TS1.                                           #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x02                                #
#                  Domain Number        = DN1                                 #
#                  Sequence ID          = D                                   #
#                  Source Port Identity = E                                   #
#                  Origin Timestamp     = 0                                   #
#                  Correction Field     = 0                                   #
###############################################################################

STEP 5

LOG -level 0 -msg "$tee_log_msg(PDELAY_REQ_TX)"

if {![ptp_ha::send_pdelay_req \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -correction_field        $correction_field\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -tx_origin_timestamp_sec t1_sec\
           -tx_origin_timestamp_ns  t1_ns\
           -count                   $infinity]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(PDELAY_REQ_TX_F)"\
                       -tc_def  $tc_def

    return 0
}


###############################################################################
#                                                                             #
# Step 6 : Observe that the DUT transmits PDELAY_RESP on port T1 and with     #
#          following parameters and store timestamp TS2.                      #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type              = 0x03                           #
#                  Domain Number             = DN1                            #
#                  Source Mac                = SRC1                           #
#                  Clock ID                  = CLK1                           #
#                  Sequence Id               = D                              #
#                  Requesting Port Identity  = E                              #
#                  Request Receipt Timestamp = RRT1                           #
#                  Correction Field          = PDRESP_CF1                     #
###############################################################################

STEP 6

LOG -level 0 -msg "$tee_log_msg(PDELAY_RESP_RX_P1_O)"

if {![ptp_ha::recv_pdelay_resp \
            -port_num                               $tee_port_1\
            -domain_number                          $domain\
            -recvd_correction_field                 cf_pdr\
            -rx_timestamp_ns                        t4\
            -recvd_request_receipt_timestamp_sec    rrt_sec\
            -recvd_request_receipt_timestamp_ns     rrt_ns\
            -error_reason                           error_reason\
            -filter_id                              $filter_id]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(PDELAY_RESP_RX_P1_F) $error_reason"\
                        -tc_def  $tc_def

    return 0
}

###############################################################################
#                                                                             #
# Step 6a: If the clock is two step, observe that DUT transmits               #
#          PDELAY_RESP_FOLLOW_UP message on port P1 with following parameters.#
#                                                                             #
#              PTP Header                                                     #
#                  Message Type              = 0x0A                           #
#                  Domain Number             = DN1                            #
#                  Sequence Id               = D                              #
#                  Source Port Identity      = F                              #
#                  Requesting Port Identity  = E                              #
#                  Correction Field          = PDRESP_FU_CF1                  #
###############################################################################

if { $clock_step == "Two-Step" } {

STEP 6a

LOG -level 0 -msg "$tee_log_msg(PDELAY_RESP_FOLLOW_UP_RX_P1_O)"

    if {![ptp_ha::recv_pdelay_resp_followup \
                -port_num                               $tee_port_1\
                -domain_number                          $domain\
                -recvd_correction_field                 cf_pdrf\
                -recvd_response_origin_timestamp_sec    rot_sec\
                -recvd_response_origin_timestamp_ns     rot_ns\
                -rx_timestamp_ns                        t4\
                -error_reason                           error_reason\
                -filter_id                              $filter_id]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(PDELAY_RESP_FOLLOW_UP_RX_P1_F) $error_reason"\
                            -tc_def  $tc_def

        return 0
    }
}

###############################################################################
#                                                                             #
# Step 7 : Calculate meanDelay (MD1) at TEE.                                  #
###############################################################################

STEP 7

LOG -level 0 -msg "$dut_log_msg(GET_CDS.MEANDELAY_MD_TEE)"

#  2. meanDelay is calculated with below formulae:                            #
#     * For one-step clock:                                                   #
#                                                                             #
#                                                                             #
#     * For two-step clock:                                                   #

if { $clock_step == "Two-Step" } {

#        [(TS2  TS1)  (responseOriginTimestamp  requestReceiptTimestamp)  #
#        <correctedPdelayRespCorrectionField>  correctionField of            #
#        Pdelay_Resp_Follow_Up]/2                                             #

    set t4_sec      [expr $t4 / 1000000000]
    set t4_ns       [expr $t4 % 1000000000]

    LOG -level 3 -msg "t1 (in sec)                       = [nano $t1_sec] s"
    LOG -level 3 -msg "t1 (in ns)                        = [nano $t1_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "t4 (in sec)                       = [nano $t4_sec] s"
    LOG -level 3 -msg "t4 (in ns)                        = [nano $t4_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "responseOriginTimestamp (in sec)  = [nano $rot_sec] s"
    LOG -level 3 -msg "responseOriginTimestamp (in ns)   = [nano $rot_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "requestReceiptTimestamp (in sec)  = [nano $rrt_sec] s"
    LOG -level 3 -msg "requestReceiptTimestamp (in ns)   = [nano $rrt_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "CF in pDelay_Resp                 = [nano $cf_pdr] ns"
    LOG -level 3 -msg "CF in pDelay_Resp_Follow_up       = [nano $cf_pdrf] ns"
    LOG -level 3 -msg "------------------------------------------------------------------------"

    set MD1 [expr ((($t4_sec * pow(10,9)) + $t4_ns) -\
                   (($t1_sec * pow(10,9)) + $t1_ns) -\
                  ((($rot_sec * pow(10,9)) + $rot_ns) -\
                   (($rrt_sec * pow(10,9)) + $rrt_ns)) -\
                     $cf_pdr - $cf_pdrf)/2]

    LOG -level 3 -msg "MD1                               = [nano $MD1] ns"

} else {

#        [(TS2  TS1)  <correctedPdelayRespCorrectionField>]/2               #

    set t4_sec      [expr $t4 / 1000000000]
    set t4_ns       [expr $t4 % 1000000000]

    LOG -level 3 -msg "t1 (in sec)                       = [nano $t1_sec] s"
    LOG -level 3 -msg "t1 (in ns)                        = [nano $t1_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "t4 (in sec)                       = [nano $t4_sec] s"
    LOG -level 3 -msg "t4 (in ns)                        = [nano $t4_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "CF in pDelay_Resp                 = [nano $cf_pdr]"
    LOG -level 3 -msg "------------------------------------------------------------------------"

    set MD1 [expr ((($t4_sec * pow(10,9)) + $t4_ns) -\
                   (($t1_sec * pow(10,9)) + $t1_ns) -\
                     $cf_pdr)/2]

    LOG -level 3 -msg "MD1                               = [nano $MD1] ns"
}

###############################################################################
#                                                                             #
# Step 8 : Configure ingressLatency on port P1 by setting ingressLatency to   #
#          4 294 967 296 ns (i.e., the value of dataset expressed in          #
#          TimeInterval asymmetryCorrectionPortDS.ingressLatency = 2^48).     #
###############################################################################

STEP 8

set latency 4294967296

LOG -level 0 -msg "$dut_log_msg(CONFIG_TXPORTDS.INGRESSLATENCY)"

if {![ptp_ha::dut_set_ingress_latency\
             -port_num      $dut_port_1\
             -latency       $latency]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_TXPORTDS.INGRESSLATENCY_F)"\
                        -tc_def  $tc_def

     return 0
}

sleep $::ptp_ha_l1sync_wait

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
#                                                                             #
# Step 9 : Send PDELAY_REQ message on the port T1 with following parameters   #
#          and store timestamp TS1.                                           #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x02                                #
#                  Domain Number        = DN1                                 #
#                  Sequence ID          = D                                   #
#                  Source Port Identity = E                                   #
#                  Origin Timestamp     = 0                                   #
#                  Correction Field     = 0                                   #
###############################################################################

STEP 9

LOG -level 0 -msg "$tee_log_msg(PDELAY_REQ_TX)"

if {![ptp_ha::send_pdelay_req \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -correction_field        $correction_field\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -tx_origin_timestamp_sec t1_sec\
           -tx_origin_timestamp_ns  t1_ns\
           -count                   $infinity]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(PDELAY_REQ_TX_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
#                                                                             #
# Step 10: Observe that the DUT transmits PDELAY_RESP on port T1 and with     #
#          following parameters and store timestamp TS2.                      #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type              = 0x03                           #
#                  Domain Number             = DN1                            #
#                  Source Mac                = SRC1                           #
#                  Clock ID                  = CLK1                           #
#                  Sequence Id               = D                              #
#                  Requesting Port Identity  = E                              #
#                  Request Receipt Timestamp = RRT2                           #
#                  Correction Field          = PDRESP_CF2                     #
###############################################################################

STEP 10

LOG -level 0 -msg "$tee_log_msg(PDELAY_RESP_RX_P1_O)"

if {![ptp_ha::recv_pdelay_resp \
            -port_num                               $tee_port_1\
            -domain_number                          $domain\
            -recvd_correction_field                 cf_pdr\
            -rx_timestamp_ns                        t4\
            -recvd_request_receipt_timestamp_sec    rrt_sec\
            -recvd_request_receipt_timestamp_ns     rrt_ns\
            -error_reason                           error_reason\
            -filter_id                              $filter_id]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(PDELAY_RESP_RX_P1_F) $error_reason"\
                        -tc_def  $tc_def

    return 0
}

###############################################################################
#                                                                             #
# Step 10a:If the clock is two step, observe that DUT transmits               #
#          PDELAY_RESP_FOLLOW_UP message on port P1 with following parameters.#
#                                                                             #
#              PTP Header                                                     #
#                  Message Type              = 0x0A                           #
#                  Domain Number             = DN1                            #
#                  Sequence Id               = D                              #
#                  Source Port Identity      = F                              #
#                  Requesting Port Identity  = E                              #
#                  Correction Field          = PDRESP_FU_CF1                  #
###############################################################################

if { $clock_step == "Two-Step" } {

STEP 10a

LOG -level 0 -msg "$tee_log_msg(PDELAY_RESP_FOLLOW_UP_RX_P1_O)"

    if {![ptp_ha::recv_pdelay_resp_followup \
                -port_num                               $tee_port_1\
                -domain_number                          $domain\
                -recvd_correction_field                 cf_pdrf\
                -recvd_response_origin_timestamp_sec    rot_sec\
                -recvd_response_origin_timestamp_ns     rot_ns\
                -rx_timestamp_ns                        t4\
                -error_reason                           error_reason\
                -filter_id                              $filter_id]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(PDELAY_RESP_FOLLOW_UP_RX_P1_F) $error_reason"\
                            -tc_def  $tc_def

        return 0
    }
}

###############################################################################
#                                                                             #
# Step 11: Calculate meanDelay (MD2) at TEE.                                  #
###############################################################################

STEP 11

LOG -level 0 -msg "$dut_log_msg(GET_CDS.MEANDELAY_MD_TEE)"

#  2. meanDelay is calculated with below formulae:                            #
#     * For one-step clock:                                                   #
#                                                                             #
#                                                                             #
#     * For two-step clock:                                                   #

if { $clock_step == "Two-Step" } {

#        [(TS2  TS1)  (responseOriginTimestamp  requestReceiptTimestamp)  #
#        <correctedPdelayRespCorrectionField>  correctionField of            #
#        Pdelay_Resp_Follow_Up]/2                                             #

    set t4_sec      [expr $t4 / 1000000000]
    set t4_ns       [expr $t4 % 1000000000]

    LOG -level 3 -msg "t1 (in sec)                       = [nano $t1_sec] s"
    LOG -level 3 -msg "t1 (in ns)                        = [nano $t1_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "t4 (in sec)                       = [nano $t4_sec] s"
    LOG -level 3 -msg "t4 (in ns)                        = [nano $t4_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "responseOriginTimestamp (in sec)  = [nano $rot_sec] s"
    LOG -level 3 -msg "responseOriginTimestamp (in ns)   = [nano $rot_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "requestReceiptTimestamp (in sec)  = [nano $rrt_sec] s"
    LOG -level 3 -msg "requestReceiptTimestamp (in ns)   = [nano $rrt_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "CF in pDelay_Resp                 = [nano $cf_pdr] ns"
    LOG -level 3 -msg "CF in pDelay_Resp_Follow_up       = [nano $cf_pdrf] ns"
    LOG -level 3 -msg "------------------------------------------------------------------------"

    set MD2 [expr ((($t4_sec * pow(10,9)) + $t4_ns) -\
                   (($t1_sec * pow(10,9)) + $t1_ns) -\
                  ((($rot_sec * pow(10,9)) + $rot_ns) -\
                   (($rrt_sec * pow(10,9)) + $rrt_ns)) -\
                     $cf_pdr - $cf_pdrf)/2]

    LOG -level 3 -msg "MD2                               = [nano $MD2] ns"

} else {

#        [(TS2  TS1)  <correctedPdelayRespCorrectionField>]/2               #

    set t4_sec      [expr $t4 / 1000000000]
    set t4_ns       [expr $t4 % 1000000000]

    LOG -level 3 -msg "t1 (in sec)                       = [nano $t1_sec] s"
    LOG -level 3 -msg "t1 (in ns)                        = [nano $t1_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "t4 (in sec)                       = [nano $t4_sec] s"
    LOG -level 3 -msg "t4 (in ns)                        = [nano $t4_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "CF in pDelay_Resp                 = [nano $cf_pdr]"
    LOG -level 3 -msg "------------------------------------------------------------------------"

    set MD2 [expr ((($t4_sec * pow(10,9)) + $t4_ns) -\
                   (($t1_sec * pow(10,9)) + $t1_ns) -\
                     $cf_pdr)/2]

    LOG -level 3 -msg "MD2                               = [nano $MD2] ns"
}

###############################################################################
#                                                                             #
# Step 12: Configure ingressLatency on port P1 by setting ingressLatency to   #
#          -4 294 967 296 ns (i.e., the value of dataset expressed in         #
#          TimeInterval asymmetryCorrectionPortDS.ingressLatency = -2^48).    #
###############################################################################

STEP 12

set latency -4294967296

LOG -level 0 -msg "$dut_log_msg(CONFIG_TXPORTDS.INGRESSLATENCY)"

if {![ptp_ha::dut_set_ingress_latency\
             -port_num      $dut_port_1\
             -latency       $latency]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_TXPORTDS.INGRESSLATENCY_F)"\
                        -tc_def  $tc_def

     return 0
}

sleep $::ptp_ha_l1sync_wait

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
#                                                                             #
# Step 13: Send PDELAY_REQ message on the port T1 with following parameters   #
#          and store timestamp TS1.                                           #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x02                                #
#                  Domain Number        = DN1                                 #
#                  Sequence ID          = D                                   #
#                  Source Port Identity = E                                   #
#                  Origin Timestamp     = 0                                   #
#                  Correction Field     = 0                                   #
###############################################################################

STEP 13

LOG -level 0 -msg "$tee_log_msg(PDELAY_REQ_TX)"

if {![ptp_ha::send_pdelay_req \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -correction_field        $correction_field\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -tx_origin_timestamp_sec t1_sec\
           -tx_origin_timestamp_ns  t1_ns\
           -count                   $infinity]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(PDELAY_REQ_TX_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
#                                                                             #
# Step 14: Observe that the DUT transmits PDELAY_RESP on port T1 and with     #
#          following parameters and store timestamp TS2.                      #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type              = 0x03                           #
#                  Domain Number             = DN1                            #
#                  Source Mac                = SRC1                           #
#                  Clock ID                  = CLK1                           #
#                  Sequence Id               = D                              #
#                  Requesting Port Identity  = E                              #
#                  Request Receipt Timestamp = RRT2                           #
#                  Correction Field          = PDRESP_CF2                     #
###############################################################################

STEP 14

LOG -level 0 -msg "$tee_log_msg(PDELAY_RESP_RX_P1_O)"

if {![ptp_ha::recv_pdelay_resp \
            -port_num                               $tee_port_1\
            -domain_number                          $domain\
            -recvd_correction_field                 cf_pdr\
            -rx_timestamp_ns                        t4\
            -recvd_request_receipt_timestamp_sec    rrt_sec\
            -recvd_request_receipt_timestamp_ns     rrt_ns\
            -error_reason                           error_reason\
            -filter_id                              $filter_id]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(PDELAY_RESP_RX_P1_F) $error_reason"\
                        -tc_def  $tc_def

    return 0
}

###############################################################################
#                                                                             #
# Step 14a:If the clock is two step, observe that DUT transmits               #
#          PDELAY_RESP_FOLLOW_UP message on port P1 with following parameters.#
#                                                                             #
#              PTP Header                                                     #
#                  Message Type              = 0x0A                           #
#                  Domain Number             = DN1                            #
#                  Sequence Id               = D                              #
#                  Source Port Identity      = F                              #
#                  Requesting Port Identity  = E                              #
#                  Correction Field          = PDRESP_FU_CF1                  #
###############################################################################

if { $clock_step == "Two-Step" } {

STEP 14a

LOG -level 0 -msg "$tee_log_msg(PDELAY_RESP_FOLLOW_UP_RX_P1_O)"

    if {![ptp_ha::recv_pdelay_resp_followup \
                -port_num                               $tee_port_1\
                -domain_number                          $domain\
                -recvd_correction_field                 cf_pdrf\
                -recvd_response_origin_timestamp_sec    rot_sec\
                -recvd_response_origin_timestamp_ns     rot_ns\
                -rx_timestamp_ns                        t4\
                -error_reason                           error_reason\
                -filter_id                              $filter_id]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(PDELAY_RESP_FOLLOW_UP_RX_P1_F) $error_reason"\
                            -tc_def  $tc_def

        return 0
    }
}

###############################################################################
#                                                                             #
# Step 15: Calculate meanDelay (MD3) at TEE.                                  #
###############################################################################

STEP 15

LOG -level 0 -msg "$dut_log_msg(GET_CDS.MEANDELAY_MD_TEE)"

#  2. meanDelay is calculated with below formulae:                            #
#     * For one-step clock:                                                   #
#                                                                             #
#                                                                             #
#     * For two-step clock:                                                   #

if { $clock_step == "Two-Step" } {

#        [(TS2  TS1)  (responseOriginTimestamp  requestReceiptTimestamp)  #
#        <correctedPdelayRespCorrectionField>  correctionField of            #
#        Pdelay_Resp_Follow_Up]/2                                             #

    set t4_sec      [expr $t4 / 1000000000]
    set t4_ns       [expr $t4 % 1000000000]

    LOG -level 3 -msg "t1 (in sec)                       = [nano $t1_sec] s"
    LOG -level 3 -msg "t1 (in ns)                        = [nano $t1_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "t4 (in sec)                       = [nano $t4_sec] s"
    LOG -level 3 -msg "t4 (in ns)                        = [nano $t4_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "responseOriginTimestamp (in sec)  = [nano $rot_sec] s"
    LOG -level 3 -msg "responseOriginTimestamp (in ns)   = [nano $rot_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "requestReceiptTimestamp (in sec)  = [nano $rrt_sec] s"
    LOG -level 3 -msg "requestReceiptTimestamp (in ns)   = [nano $rrt_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "CF in pDelay_Resp                 = [nano $cf_pdr] ns"
    LOG -level 3 -msg "CF in pDelay_Resp_Follow_up       = [nano $cf_pdrf] ns"
    LOG -level 3 -msg "------------------------------------------------------------------------"

    set MD3 [expr ((($t4_sec * pow(10,9)) + $t4_ns) -\
                   (($t1_sec * pow(10,9)) + $t1_ns) -\
                  ((($rot_sec * pow(10,9)) + $rot_ns) -\
                   (($rrt_sec * pow(10,9)) + $rrt_ns)) -\
                     $cf_pdr - $cf_pdrf)/2]

    LOG -level 3 -msg "MD3                               = [nano $MD3] ns"

} else {

#        [(TS2  TS1)  <correctedPdelayRespCorrectionField>]/2               #

    set t4_sec      [expr $t4 / 1000000000]
    set t4_ns       [expr $t4 % 1000000000]

    LOG -level 3 -msg "t1 (in sec)                       = [nano $t1_sec] s"
    LOG -level 3 -msg "t1 (in ns)                        = [nano $t1_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "t4 (in sec)                       = [nano $t4_sec] s"
    LOG -level 3 -msg "t4 (in ns)                        = [nano $t4_ns] ns"
    LOG -level 3 -msg ""
    LOG -level 3 -msg "CF in pDelay_Resp                 = [nano $cf_pdr]"
    LOG -level 3 -msg "------------------------------------------------------------------------"

    set MD3 [expr ((($t4_sec * pow(10,9)) + $t4_ns) -\
                   (($t1_sec * pow(10,9)) + $t1_ns) -\
                     $cf_pdr)/2]

    LOG -level 3 -msg "MD3                               = [nano $MD3] ns"
}

###############################################################################
#                                                                             #
# Step 16: Verify that MD2 < MD1 & MD3 > MD1.                                 #
#                                                                             #
###############################################################################

STEP 16

LOG -level 0 -msg "$dut_log_msg(VERIFY_MD1)"

LOG -level 0 -msg "Value of meanDelay (MD1)= $MD1 ns"
LOG -level 0 -msg "Value of meanDelay (MD2)= $MD2 ns"
LOG -level 0 -msg "Value of meanDelay (MD3)= $MD3 ns"

if {!(($MD2 < $MD1) && ($MD3 > $MD1))} {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL -reason "DUT generates Ingress timestamp in\
                               Pdelay_Req (event) messages from\
                               timestampCorrectionPortDS.ingressLatency when\
                               using Peer to Peer Delay mechanism."\
                      -tc_def  $tc_def

    return 0

}

TEE_CLEANUP

TC_CLEAN_AND_PASS -reason "DUT generates Ingress timestamp in\
                           Pdelay_Req (event) messages from\
                           timestampCorrectionPortDS.ingressLatency when\
                           using Peer to Peer Delay mechanism."\
                  -tc_def  $tc_def

return 1

############################## END OF TESTCASE #################################
