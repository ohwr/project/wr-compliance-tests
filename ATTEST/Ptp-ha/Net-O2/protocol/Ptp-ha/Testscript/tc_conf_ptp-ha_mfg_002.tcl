#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_ptp-ha_mfg_002                                  #
# Test Case Version : 1.0                                                     #
# Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    #
# Module Name       : Message Format Group (MFG)                              #
#                                                                             #
# Title             : L1Sync message with optParamsEnabled is set to TRUE     #
#                     - transport UDP over IP                                 #
#                                                                             #
# Purpose           : To verify that a PTP enabled device sends L1Sync        #
#                     signaling message in correct format when                #
#                     optParamsEnabled is set to TRUE and transport over UDP  #
#                     over IP.                                                #
#                                                                             #
# Reference         : P1588/D1.3, February 2018 V3.01 Clause O.6.1 Page 447,  #
#                     Clause O.6.2 Page 447, Clause O.6.4 Pages 448 and 449,  #
#                     Clause O.8.5 Page 455 and 456,Clause 13.12.2 Page 225   #
#                                                                             #
# Conformance Type  : SHALL                                                   #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 001                                                     #
# Test Topology     : 001                                                     #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode - one-step/two-step> |           #
#           |                     <Configure logAnnounceInterval, | P1        #
#           |                             announceReceiptTimeout> |           #
#           |                         <Configure logSyncInterval> | P1        #
#           |                  <Configure logMinDelayReqInterval> | P1        #
#           |                    <Configure Priority1, Priority2> | P1        #
#           |                                     <Enable L1SYNC> | P1        #
#           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        #
#           |                     <Disable L1SynOptParams option> | P1        #
#           |           <Enable asymmetryCorrectionPortDS.enable> | P1        #
#           | <Configure timestampCorrectionPortDS.egressLatency, | P1        #
#           |           timestampCorrectionPortDS.ingressLatency, |           #
#           |        asymmetryCorrectionPortDS.constantAsymmetry, |           #
#           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           #
#           |                                                     |           #
#           |                      <Enable L1SynOptParams option> | P1        #
#           |                                                     |           #
#           |     PTP SIGNALING with L1 Sync TLV [MSG_TYPE = 0x0C,|           #
#           |                                  DN = DN1, OPE = 1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     MSG_TYPE  = Message Type                                                #
#     DN        = Domain Number                                               #
#     OPE       = Optional Parameters Enabled                                 #
#                                                                             #
# NOTE :                                                                      #
#                                                                             #
#  1. This objective is verified using the High Accuracy Delay Request-       #
#     Response Default PTP Profile                                            #
#                                                                             #
#  2. This objective is applicable only for device implementation supports    #
#     transport over UDP over IP                                              #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.ingressLatency,           #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Enable L1SynOptParams on DUT.                                      #
#                                                                             #
# Step 4 : Verify that DUT transmits L1 Sync Signaling message on the port P1 #
#          when optParamsEnabled is set to true. Checking that the following  #
#          PTP message fields have correct information.                       #
#                                                                             #
#                Ethernet Fields                                              #
#                   1) Source MAC           = Unicast MAC                     #
#                   2) Destination MAC      = Multicast MAC                   #
#                                             (01:00:5E:00:00:6B)             #
#                   3) Ether Type           = 0x0800 (IP)                     #
#                                                                             #
#                IPv4 Fields                                                  #
#                   4) IP Protocol          = 17 (UDP)                        #
#                   5) Destination IP       = 224.0.0.107                     #
#                                             (non-forwardable address)       #
#                   6) Source IP            = Unicast IP                      #
#                   7) Checksum             = Valid                           #
#                                                                             #
#                UDP Fields                                                   #
#                   8) UDP Destination Port = 320 (General Message)           #
#                   9) Checksum             = Valid                           #
#                                                                             #
#                PTP Fields                                                   #
#                  10) messageType          = 0xC (Signaling message) (4 bits)#
#                  11) majorSdoId           = 0x000 (4 bits)                  #
#                  12) versionPTP           = 2 (4 bits)                      #
#                  13) minorVersionPTP      = 1 (4 bits)                      #
#                  14) messageLength        = non-zero (2 octets)             #
#                  15) domainNumber         = 0 - 127 (1 octet)               #
#                  16) minorSdoId           = 0 (1 octet)                     #
#                  17) flagField            = 0x0000 - 0xFFFF (2 octets)      #
#                  18) correctionField      = 0 (8 octets)                    #
#                  19) messageTypeSpecific  = (4 octets)                      #
#                  20) sourcePortIdentity   = non-zero (10 octets)            #
#                  21) sequenceId           = 0 - 65535 (2 octets)            #
#                  22) controlField         = 05 (1 octet)                    #
#                  23) logMessageInterval   = 0x7F (1 octet)                  #
#                  24) targetPortIdentity   = non-zero (10 octets)            #
#                                                                             #
#                L1Sync Details                                               #
#                  25) tlvType              = 0x8001 (L1_SYNC) (2 octets)     #
#                  26) length field         = 2 (2 octets)                    #
#                  27) TCR                  = 0 or 1 (1 bit)                  #
#                  28) RCR                  = 0 or 1 (1 bit)                  #
#                  29) CR                   = 0 or 1 (1 bit)                  #
#                  30) OPE                  = 1 (1 bit)                       #
#                  31) Reserved             = 4 bits                          #
#                  32) ITC                  = 0 or 1 (1 bit)                  #
#                  33) IRC                  = 0 or 1 (1 bit)                  #
#                  34) IC                   = 0 or 1 (1 bit)                  #
#                  35) Reserved             = 5 bits                          #
#                                                                             #
#                L1Sync TLV Extended Details                                  #
#                   36) TCT                 = 0 or 1 (1 bit)                  #
#                   37) POV                 = 0 or 1 (1 bit)                  #
#                   38) FOV                 = 0 or 1 (1 bit)                  #
#                   39) Reserved            = (5 bits)                        #
#                   40) phaseOffsetTx       = non-zero (8 octets)             #
#                   41) phaseOffsetTxTimestamp  = non-zero (10 octets)        #
#                   42) freqOffsetTx        = non-zero (8 octets)             #
#                   43) freqOffsetTxTimestamp   = non-zero (10 octets)        #
#                   44) Reserved            = (1 octet)                       #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          Oct/2018      CERN          Initial                            #
#                                                                             #
###############################################################################
# Copyright (c) 2018 CERN                                                     #
###############################################################################

global dut_log_msg
global tee_log_msg
global session_id

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def      "To verify that a PTP enabled device sends L1Sync\
                 signaling message in correct format when\
                 optParamsEnabled is set to TRUE and transport over UDP\
                 over IP."

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set dut_port_1                     $::dut_port_num_1
set vlan_id                        $::ptp_ha_vlan_id
set dut_ip                         $::dut_test_port_ip
set dut_dest_ip                    $::dut_test_port_ip
set tee_dest_ip                    $::tee_test_port_ip
set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT

set tlv_type(l1sync)               $::TLV_TYPE(L1SYNC)

set ope_true    1

########################### END - INITIALIZATION ##############################

if { $::ptp_trans_type == "IEEE 802.3" } {

       ERRLOG -msg "$tee_log_msg(UDP)" 

       TC_ABORT -reason "$tee_log_msg(UDP)"\
                -tc_def  $tc_def

  return 0

}

ptp_ha::display_port_details

ptp_ha::dut_Initialization

proc cleanup { } {

ptp_ha::dut_cleanup_setup_001

}
##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

### (Initial Part) ###
###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Ensure that DUT port P1 is operational.                            #
#    ii.   Enable PTP globally with Device Type as Boundary clock.            #
#   iii.   Configure clock step as One-step/ two-step.                        #
#    iv.   Configure Network Transport Protocol as UDP/IPv4.                  #
#     v.   Configure PTP on port P1.                                          #
###############################################################################

STEP 1

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP1)"

if {![ptp_ha::dut_configure_setup_001]} {

    LOG -msg "$dut_log_msg(INIT_SETUP1_F)"

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP1_F)"\
                       -tc_def $tc_def
    return 0
}

###############################################################################
# Step 2 : Initialization of TEE                                              #
#     i.   Add ports T1 at TEE.                                               #
###############################################################################

STEP 2

LOG -level 0 -msg "$tee_log_msg(INIT)"

ptp_ha::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

    if {![PTP_HA_UNICAST_PRE_REQUISITE_CONFIGURATION\
                        -port_num       $tee_port_1\
                        -tee_mac        $tee_mac\
                        -tee_ip         $tee_ip\
                        -dut_ip         $dut_ip\
                        -vlan_id        $vlan_id\
                        -dut_mac        tee_dest_mac\
                        -timeout        $arp_timeout]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
                           -tc_def $tc_def

        return 0
    }

}

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_1\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def $tc_def

    return 0
}

set filter_id [lindex $id1 end]

###############################################################################
# Step 3 : Enable L1SynOptParams on DUT.                                      #
###############################################################################

STEP 3

LOG -level 0 -msg "$dut_log_msg(ENABLE_L1SYN_OPT_PARAMS)"

if {![ptp_ha::dut_l1sync_opt_params_enable\
              -port_num      $dut_port_1]} {

TEE_CLEANUP

TC_CLEAN_AND_ABORT -reason "$dut_log_msg(ENABLE_L1SYN_OPT_PARAMS_F)"\
                   -tc_def  $tc_def

return 0

}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 4 : Verify that DUT transmits L1 Sync Signaling message on the port P1 #
#          when optParamsEnabled is set to true. Checking that the following  #
#          PTP message fields have correct information.                       #
#                                                                             #
#                Ethernet Fields                                              #
#                   1) Source MAC           = Unicast MAC                     #
#                   2) Destination MAC      = Multicast MAC                   #
#                                             (01:00:5E:00:00:6B)             #
#                   3) Ether Type           = 0x0800 (IP)                     #
#                                                                             #
#                IPv4 Fields                                                  #
#                   4) IP Protocol          = 17 (UDP)                        #
#                   5) Destination IP       = 224.0.0.107                     #
#                                             (non-forwardable address)       #
#                   6) Source IP            = Unicast IP                      #
#                   7) Checksum             = Valid                           #
#                                                                             #
#                UDP Fields                                                   #
#                   8) UDP Destination Port = 320 (General Message)           #
#                   9) Checksum             = Valid                           #
#                                                                             #
#                PTP Fields                                                   #
#                  10) messageType          = 0xC (Signaling message) (4 bits)#
#                  11) majorSdoId           = 0x000 (4 bits)                  #
#                  12) versionPTP           = 2 (4 bits)                      #
#                  13) minorVersionPTP      = 1 (4 bits)                      #
#                  14) messageLength        = non-zero (2 octets)             #
#                  15) domainNumber         = 0 - 127 (1 octet)               #
#                  16) minorSdoId           = 0 (1 octet)                     #
#                  17) flagField            = 0x0000 - 0xFFFF (2 octets)      #
#                  18) correctionField      = 0 (8 octets)                    #
#                  19) messageTypeSpecific  = (4 octets)                      #
#                  20) sourcePortIdentity   = non-zero (10 octets)            #
#                  21) sequenceId           = 0 - 65535 (2 octets)            #
#                  22) controlField         = 05 (1 octet)                    #
#                  23) logMessageInterval   = 0x7F (1 octet)                  #
#                  24) targetPortIdentity   = non-zero (10 octets)            #
#                                                                             #
#                L1Sync Details                                               #
#                  25) tlvType              = 0x8001 (L1_SYNC) (2 octets)     #
#                  26) length field         = 2 (2 octets)                    #
#                  27) TCR                  = 0 or 1 (1 bit)                  #
#                  28) RCR                  = 0 or 1 (1 bit)                  #
#                  29) CR                   = 0 or 1 (1 bit)                  #
#                  30) OPE                  = 1 (1 bit)                       #
#                  31) Reserved             = 4 bits                          #
#                  32) ITC                  = 0 or 1 (1 bit)                  #
#                  33) IRC                  = 0 or 1 (1 bit)                  #
#                  34) IC                   = 0 or 1 (1 bit)                  #
#                  35) Reserved             = 5 bits                          #
#                                                                             #
#                L1Sync TLV Extended Details                                  #
#                   36) TCT                 = 0 or 1 (1 bit)                  #
#                   37) POV                 = 0 or 1 (1 bit)                  #
#                   38) FOV                 = 0 or 1 (1 bit)                  #
#                   39) Reserved            = (5 bits)                        #
#                   40) phaseOffsetTx       = non-zero (8 octets)             #
#                   41) phaseOffsetTxTimestamp  = non-zero (10 octets)        #
#                   42) freqOffsetTx        = non-zero (8 octets)             #
#                   43) freqOffsetTxTimestamp   = non-zero (10 octets)        #
#                   44) Reserved            = (1 octet)                       #
#                                                                             #
#                                                                             #
###############################################################################

STEP 4

LOG -level 0 -msg "$dut_log_msg(L1SYNC_RX_P1_V)"

if {![ptp_ha::check_signal\
              -port_num                $tee_port_1\
              -tlv_type                $tlv_type(l1sync)\
              -ope                     $ope_true\
              -filter_id               $filter_id]} {

     TEE_CLEANUP

     TC_CLEAN_AND_FAIL -reason "DUT does not send L1Sync signaling message\
                                in correct format when optParamsEnabled is set\
                                to TRUE and transport over UDP over IP."\
                       -tc_def  $tc_def

     return 0
}

TEE_CLEANUP

TC_CLEAN_AND_PASS  -reason "DUT sends L1Sync signaling message\
                            in correct format when optParamsEnabled is set to\
                            TRUE and transport over UDP over IP."\
                   -tc_def  $tc_def

return 1

########################## END OF TEST CASE ###################################
