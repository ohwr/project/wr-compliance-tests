#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_ptp-ha_peg_009                                  #
# Test Case Version : 1.3                                                     #
# Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    #
# Module Name       : PTP ExternalPortConfiguration Group (PEG)               #
#                                                                             #
# Title             : portDS.portState remains in slave state - expiry of     #
#                     Announcereceipttimeout                                  #
#                                                                             #
# Purpose           : To verify that a PTP enabled device remains in SLAVE    #
#                     state even after the expiry of Announcereceipttimeout   #
#                     when defaultDS.externalPortConfigurationEnabled is set  #
#                     to TRUE and externalPortConfigurationPortDS.desiredState#
#                     is set to SLAVE.                                        #
#                                                                             #
# Reference         : P1588/D1.3,February 2018 V3.01 Clause 17.6.5.3 Page 355 #
#                                                                             #
# Conformance Type  : SHALL                                                   #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 006                                                     #
# Test Topology     : 001                                                     #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode - one-step/two-step> |           #
#           |                     <Configure logAnnounceInterval, | P1        #
#           |                             announceReceiptTimeout> |           #
#           |                         <Configure logSyncInterval> | P1        #
#           |                  <Configure logMinDelayReqInterval> | P1        #
#           |                    <Configure Priority1, Priority2> | P1        #
#           |                                     <Enable L1SYNC> | P1        #
#           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        #
#           |                     <Disable L1SynOptParams option> | P1        #
#           |           <Enable asymmetryCorrectionPortDS.enable> | P1        #
#           | <Configure timestampCorrectionPortDS.egressLatency, | P1        #
#           |           timestampCorrectionPortDS.ingressLatency, |           #
#           |        asymmetryCorrectionPortDS.constantAsymmetry, |           #
#           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           #
#           |                                            <Enable  |           #
#           |         defaultDS.externalPortConfigurationEnabled> | P1        #
#           |                                                     |           #
#           |                                          <Configure |           #
#           | externalPortConfigurationPortDS.desiredState=SLAVE> |           #
#           |                                                     |           #
#           | < Wait for the expiry of AnnounceReceipt timeout>   |           #
#           |                                                     |           #
#           |                        <Check Port Status = SLAVE>  | P1        #
#           |                                                     |           #
#           |                                            <Disable |           #
#           |         defaultDS.externalPortConfigurationEnabled> | P1        #
#           |                                                     |           #
#           | < Wait for the expiry of AnnounceReceipt timeout>   |           #
#           |                                                     |           #
#           |                        <Check Port Status = MASTER> | P1        #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     PRI       = Priority                                                    #
#                                                                             #
# NOTE :                                                                      #
#                                                                             #
#  1. This objective is verified using the High Accuracy Delay Request-       #
#     Response Default PTP Profile                                            #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.egressLatency,            #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
#    xi.   Enable defaultDS.externalPortConfigurationEnabled on port P1.      #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Configure externalPortConfigurationPortDS.desiredState as SLAVE    #
#                                                                             #
# Step 4 : Wait for the expiry of AnnounceReceiptTimeout                      #
#                                                                             #
# Step 5 : Observe that the port status of P1 in DUT is in SLAVE state.       #
#                                                                             #
# Step 6 : Disable defaultDS.externalPortConfigurationEnabled on port P1 in   #
#           DUT.                                                              #
#                                                                             #
# Step 7 : Wait for the expiry of AnnounceReceiptTimeout                      #
#                                                                             #
# Step 8 : Verify that the port status of P1 in DUT is in MASTER state.       #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          May/2018      CERN          Initial                            #
# 1.1          Jun/2018      CERN          Fixed dut_port in                  #
#                                          dut_set_epc_desired_state          #
# 1.2          Jul/2018      CERN          Added ptp_ha::display_port_details #
#                                          to print port details              #
# 1.3          Sep/2018      CERN          a) Removed destination MAC and IP  #
#                                             in send functions.              #
#                                          b) Removed timeout in recv         #
#                                             functions.                      #
#                                          c) Moved timeout calculation to    #
#                                             PTP-HA global file.             #
#                                                                             #
###############################################################################
# Copyright (c) 2018 CERN                                                     #
###############################################################################

global dut_log_msg
global tee_log_msg
global session_id

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def "To verify that a PTP enabled device remains in SLAVE\
            state even after the expiry of Announcereceipttimeout\
            when defaultDS.externalPortConfigurationEnabled is set\
            to TRUE and externalPortConfigurationPortDS.desiredState\
            is set to SLAVE."

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set dut_port_1                     $::dut_port_num_1
set vlan_id                        $::ptp_ha_vlan_id
set dut_ip                         $::dut_test_port_ip
set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT
set master                         $::PTP_PORT_STATE(MASTER)
set slave                          $::PTP_PORT_STATE(SLAVE)
set passive                        $::PTP_PORT_STATE(PASSIVE)

########################### END - INITIALIZATION ##############################

ptp_ha::display_port_details

ptp_ha::dut_Initialization

proc cleanup {} {

    ptp_ha::dut_cleanup_setup_006

    ptp_ha::dut_set_epc_desired_state\
             -port_num         $::dut_port_num_1\
             -desired_state    $::PTP_PORT_STATE(PASSIVE)

}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

### (Initial Part) ###

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.egressLatency,            #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
#    xi.   Enable defaultDS.externalPortConfigurationEnabled on port P1.      #
###############################################################################

STEP 1

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP6)"

if {![ptp_ha::dut_configure_setup_006]} {

    LOG -msg "$dut_log_msg(INIT_SETUP6_F)"

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP6_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
###############################################################################

STEP 2

LOG -level 0 -msg "$tee_log_msg(INIT)"

ptp_ha::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

    if {![PTP_HA_UNICAST_PRE_REQUISITE_CONFIGURATION\
                        -port_num       $tee_port_1\
                        -tee_mac        $tee_mac\
                        -tee_ip         $tee_ip\
                        -dut_ip         $dut_ip\
                        -vlan_id        $vlan_id\
                        -dut_mac        tee_dest_mac\
                        -timeout        $arp_timeout]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
                           -tc_def  $tc_def

        return 0
    }

}

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_1\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def  $tc_def

    return 0
}

set filter_id [lindex $id1 end]

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 3 : Configure externalPortConfigurationPortDS.desiredState as SLAVE    #
###############################################################################

STEP 3

LOG -level 0 -msg "$dut_log_msg(CONFIGURE_SLAVE)"

if {![ptp_ha::dut_set_epc_desired_state\
            -port_num         $dut_port_1\
            -desired_state    $slave]} {
   TEE_CLEANUP 

   TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIGURE_SLAVE_F)"\
                      -tc_def  $tc_def
   return 0
}

###############################################################################
# Step 4 : Wait for the expiry of AnnounceReceiptTimeout                      #
###############################################################################

STEP 4

LOG -level 0 -msg "$dut_log_msg(WAIT_ANNOUNCE_RECEIPT_TIMEOUT)"

sleep $::ptp_ha_bmca

###############################################################################
# Step 5 : Observe that the port status of P1 in DUT is in SLAVE state.       #
###############################################################################

STEP 5

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_SLAVE_P1_O)"

if {![ptp_ha::dut_check_ptp_port_state \
              -port_num                $dut_port_1\
              -port_state              $slave] } {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_SLAVE_P1_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 6 : Disable defaultDS.externalPortConfigurationEnabled on port P1 in   #
#           DUT.                                                              #
###############################################################################

STEP 6

LOG -level 0 -msg "$dut_log_msg(DISABLE_EXTERNAL_PORT_CONFIG)"

if {![ptp_ha::dut_external_port_config_disable\
           -port_num      $dut_port_1] } {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT   -reason "$dut_log_msg(DISABLE_EXTERNAL_PORT_CONFIG_F)"\
                         -tc_def  $tc_def
    return 0
}

###############################################################################
#                                                                             #
# Step 7 : Wait for the expiry of AnnounceReceiptTimeout                      #
###############################################################################

STEP 7

LOG -level 0 -msg "$dut_log_msg(WAIT_ANNOUNCE_RECEIPT_TIMEOUT)"

sleep [expr $::ptp_ha_bmca + 1]

###############################################################################
# Step 8 : Verify that the port status of P1 in DUT is in MASTER state.       #
#                                                                             #
###############################################################################

STEP 8

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_MASTER_P1_V)"

if {![ptp_ha::dut_check_ptp_port_state \
              -port_num                $dut_port_1\
              -port_state              $master] } {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL -reason "PTP enabled device does not remain in SLAVE\
                               state even after the expiry of Announcereceipttimeout\
                               when defaultDS.externalPortConfigurationEnabled is set\
                               to TRUE and externalPortConfigurationPortDS.desiredState\
                               is set to SLAVE."\
                      -tc_def  $tc_def
    return 0
}

TEE_CLEANUP

TC_CLEAN_AND_PASS -reason "PTP enabled device remains in SLAVE\
                           state even after the expiry of Announcereceipttimeout\
                           when defaultDS.externalPortConfigurationEnabled is set\
                           to TRUE and externalPortConfigurationPortDS.desiredState\
                           is set to SLAVE."\
                  -tc_def  $tc_def

return 1
######################### END OF TEST CASE ####################################
