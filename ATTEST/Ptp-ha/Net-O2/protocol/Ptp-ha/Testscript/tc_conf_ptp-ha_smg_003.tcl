#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_ptp-ha_smg_003                                  #
# Test Case Version : 1.3                                                     #
# Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    #
# Module Name       : PTP-HA State Machine Group (SMG)                        #
#                                                                             #
# Title             : L1SYNC port state continues to be in IDLE State         #
#                                                                             #
# Purpose           : To verify that L1 SYNC port continues to be in IDLE     #
#                     state if no L1 Sync TLV is received.                    #
#                                                                             #
# Reference         : IEEE 1588-2017 Clause O.7.2 Table 157 Page 449,         #
#                     Clause O.7.3 Figure 70 Page 450                         #
#                                                                             #
# Conformance Type  : MUST                                                    #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 001                                                     #
# Test Topology     : 001                                                     #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#          TEE                                                  DUT           #
#        [slave]                                              [master]        #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode - one-step/two-step> |           #
#           |                     <Configure logAnnounceInterval, | P1        #
#           |                             announceReceiptTimeout> |           #
#           |                         <Configure logSyncInterval> | P1        #
#           |                  <Configure logMinDelayReqInterval> | P1        #
#           |                    <Configure Priority1, Priority2> | P1        #
#           |                                     <Enable L1SYNC> | P1        #
#           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        #
#           |                     <Disable L1SynOptParams option> | P1        #
#           |           <Enable asymmetryCorrectionPortDS.enable> | P1        #
#           | <Configure timestampCorrectionPortDS.egressLatency, | P1        #
#           |           timestampCorrectionPortDS.ingressLatency, |           #
#           |        asymmetryCorrectionPortDS.constantAsymmetry, |           #
#           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           #
#           |                                                     |           #
#           |                           ANNOUNCE [MSG_TYPE = 0x0B,|           #
#           |                                    PRI=X, DN = DN1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | ANNOUNCE [MSG_TYPE = 0x0B,                          |           #
#           | PRI=X+1, DN = DN1]                                  |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                   <Check L1SYNC port status - IDLE> | P1        #
#           |                                                     |           #
#           |                      PTP SIGNALING with L1 Sync TLV |           #
#           |                          [MSG_TYPE = 0xC, DN = DN1, |           #
#           |                                  TLV_TYPE = 0x8001] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |          <Wait for L1Sync Receipt Timeout Interval  |           #
#           |            (L1SyncReceiptTimeout * L1SyncInterval)> |           #
#           |                                                     |           #
#           |                   <Check L1SYNC port status - IDLE> | P1        #
#           |                                                     |           #
#           |                      PTP SIGNALING with L1 Sync TLV |           #
#           |                          [MSG_TYPE = 0xC, DN = DN1, |           #
#           |                                  TLV_TYPE = 0x8001] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     MSG_TYPE  = Message Type                                                #
#     DN        = Domain Number                                               #
#     BC        = Boundary Clock                                              #
#     OC        = Ordinary Clock                                              #
#     SEQ_ID    = Sequence ID                                                 #
#     PRI       = Priority                                                    #
#                                                                             #
# NOTE :                                                                      #
#                                                                             #
#  1. This objective is verified using the High Accuracy Delay Request-       #
#     Response Default PTP Profile                                            #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.egressLatency,            #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
#                  Sequence ID   = Y                                          #
#                  Priority1     = X                                          #
#                                                                             #
# Step 4 : Send periodic ANNOUNCE message with Priority1 value incremented    #
#          from the Priority1 value of received Announce message on port T1   #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
#                  Sequence ID   = A                                          #
#                  Priority1     = X+1                                        #
#                                                                             #
# Step 5 : Observe that the DUT's L1SYNC port status P1 is in IDLE state.     #
#                                                                             #
# Step 6 : Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV  #
#          on the port P1 with following parameters:                          #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                                                                             #
# Step 7 : Wait for expiry of L1 sync receipt timeout interval                #
#         (L1SyncReceiptTimeout * L1SyncInterval).                            #
#                                                                             #
# Step 8 : Verify that the DUT's L1SYNC port status P1 continues to be in     #
#          IDLE state.                                                        #
#                                                                             #
# Step 9 : Verify that DUT transmits PTP SIGNALING message with L1 Sync TLV   #
#          on the port P1 with following parameters:                          #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          Apr/2018      CERN          Initial                            #
# 1.1          Jun/2018      CERN          Fixed log message                  #
#                                          CHECK_STATE_IDLE_P1_F              #
# 1.2          Jul/2018      CERN          Added ptp_ha::display_port_details #
#                                          to print port details              #
# 1.3          Sep/2018      CERN          a) Removed destination MAC and IP  #
#                                             in send functions.              #
#                                          b) Removed timeout in recv         #
#                                             functions.                      #
#                                          c) Moved timeout calculation to    #
#                                             PTP-HA global file.             #
#                                          d) Added Announce messages         #
#                                             exchange in ladder diagram.     #
#                                                                             #
###############################################################################
# Copyright (c) 2018 CERN                                                     #
###############################################################################

global dut_log_msg
global tee_log_msg
global session_id

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def "To verify that L1 SYNC port continues to be in IDLE state if no\
            L1 Sync TLV is received."

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set dut_port_1                     $::dut_port_num_1
set vlan_id                        $::ptp_ha_vlan_id
set dut_ip                         $::dut_test_port_ip
set tee_ip                         $::tee_test_port_ip
set ptp_comm_model                 $::ptp_comm_model
set tee_mac                        $::TEE_MAC
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT
set l1sync_type                    $::TLV_TYPE(L1SYNC)
set reset_count_on                 $::RESET_COUNT(ON)
set domain                         $::DEFAULT_DOMAIN
set infinity                       $::INFINITY
set gmid                           $::GRANDMASTER_ID
set sequence_id_ann                1
set idle                           $::PTP_L1SYNC_STATE(IDLE)

########################### END - INITIALIZATION ##############################

ptp_ha::display_port_details

### Initializing session with DUT ###
ptp_ha::dut_Initialization

proc cleanup {} {

    ptp_ha::dut_cleanup_setup_001

}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

### (Initial Part) ###

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.egressLatency,            #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
###############################################################################

STEP 1

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP1)"

if {![ptp_ha::dut_configure_setup_001]} {

    LOG -msg "$dut_log_msg(INIT_SETUP1_F)"

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP1_F)"\
                       -tc_def $tc_def
    return 0
}


###############################################################################
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
###############################################################################

STEP 2

LOG -level 0 -msg "$tee_log_msg(INIT)"

ptp_ha::Initialization

set tee_mac                        $::TEE_MAC
set tee_ip                         $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

    if {![PTP_HA_UNICAST_PRE_REQUISITE_CONFIGURATION\
                        -port_num       $tee_port_1\
                        -tee_mac        $tee_mac\
                        -tee_ip         $tee_ip\
                        -dut_ip         $dut_ip\
                        -vlan_id        $vlan_id\
                        -dut_mac        tee_dest_mac\
                        -timeout        $arp_timeout]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
                           -tc_def $tc_def

        return 0
    }
}

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_1\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def $tc_def

    return 0
}

set filter_id [lindex $id1 end]

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 3 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
#                  Sequence ID   = Y                                          #
#                  Priority1     = X                                          #
#                                                                             #
###############################################################################

STEP 3

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
           -port_num                $tee_port_1\
           -domain_number           $domain\
           -filter_id               $filter_id\
           -recvd_gm_priority1      priority1]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                        -tc_def $tc_def

    return 0
}



###############################################################################
# Step 4 : Send periodic ANNOUNCE message with Priority1 value incremented    #
#          from the Priority1 value of received Announce message on port T1   #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
#                  Sequence ID   = A                                          #
#                  Priority1     = X+1                                        #
#                                                                             #
###############################################################################

STEP 4

set priority [expr $priority1 + 1]

LOG -level 0 -msg "$tee_log_msg(ANN_TX_T1)"

if {![ptp_ha::send_announce \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -gm_priority1            $priority\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -sequence_id             $sequence_id_ann\
           -count                   $infinity\
           -gm_identity             0x$gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANN_TX_T1_F)"\
                       -tc_def $tc_def

    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1


###############################################################################
# Step 5 : Observe that the DUT's L1SYNC port status P1 is in IDLE state.     #
###############################################################################

STEP 5

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_IDLE_P1_O)"

if {![ptp_ha::dut_check_l1sync_state \
              -port_num                $dut_port_1\
              -l1sync_state            $idle] } {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_IDLE_P1_F)"\
                       -tc_def  $tc_def
    return 0
}


###############################################################################
# Step 6 : Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV  #
#          on the port P1 with following parameters:                          #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                                                                             #
###############################################################################

STEP 6

LOG -level 0 -msg "$tee_log_msg(L1SYNC_RX_P1_O)"

if {![ptp_ha::recv_signal \
              -port_num                $tee_port_1\
              -domain_number           $domain\
              -tlv_type                $l1sync_type\
              -filter_id               $filter_id]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(L1SYNC_RX_P1_F)"\
                       -tc_def $tc_def

    return 0
}

###############################################################################
# Step 7 : Wait for expiry of L1 sync receipt timeout interval.               #
###############################################################################

STEP 7

LOG -level 0 -msg "$tee_log_msg(WAIT_L1SYNC_RX_INTR)"

sleep $::ptp_ha_l1sync_timeout_d


ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 8 : Verify that the DUT's L1SYNC port status P1 continues to be in     #
#          IDLE state.                                                        #
###############################################################################

STEP 8

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_IDLE_P1_V)"\

if {![ptp_ha::dut_check_l1sync_state \
              -port_num                $dut_port_1\
              -l1sync_state            $idle] } {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL  -reason "$dut_log_msg(CHECK_STATE_IDLE_P1_F)"\
                       -tc_def $tc_def
    return 0 
}


###############################################################################
#Step 9 : Verify that DUT transmits PTP SIGNALING message with L1 Sync TLV    # 
#          on the port P1 with following parameters:                          #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
###############################################################################

STEP 9

LOG -level 0 -msg "$tee_log_msg(L1SYNC_RX_P1_V)"

if {![ptp_ha::recv_signal \
           -port_num                $tee_port_1\
           -tlv_type                $l1sync_type\
           -domain_number           $domain\
           -filter_id               $filter_id]} {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL -reason "DUT does not maintain L1 SYNC port state in IDLE state\
                               if no signaling message with L1 Sync TLV is\
                               received."\
                      -tc_def $tc_def

    return 0
}

TEE_CLEANUP

TC_CLEAN_AND_PASS -reason "DUT maintains L1 SYNC port states in IDLE state\
                           if no signaling message with L1 Sync TLV is\
                           received."\
                  -tc_def $tc_def

return 1

########################## END OF TEST CASE #################################
