#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_ptp-ha_pcg_003                                  #
# Test Case Version : 1.3                                                     #
# Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    #
# Module Name       : PTP-HA Configuration Group (PCG)                        #
#                                                                             #
# Title             : logAnnounceInterval                                     #
#                                                                             #
# Purpose           : To verify that a PTP enabled device transmits Announce  #
#                     messages at configured logAnnounceInterval              #
#                     (allowable range: 0 to 4).                              #
#                                                                             #
# Reference         : P1588/D1.3, February 2018 V3.01 Clause J.5.2 Page 412   #
#                     Clause 7.7.2.2 Page 96                                  #
#                                                                             #
# Conformance Type  : SHALL                                                   #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 001                                                     #
# Test Topology     : 001                                                     #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode - one-step/two-step> |           #
#           |                     <Configure logAnnounceInterval, | P1        #
#           |                             announceReceiptTimeout> |           #
#           |                         <Configure logSyncInterval> | P1        #
#           |                  <Configure logMinDelayReqInterval> | P1        #
#           |                    <Configure Priority1, Priority2> | P1        #
#           |                                     <Enable L1SYNC> | P1        #
#           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        #
#           |                     <Disable L1SynOptParams option> | P1        #
#           |           <Enable asymmetryCorrectionPortDS.enable> | P1        #
#           | <Configure timestampCorrectionPortDS.egressLatency, | P1        #
#           |           timestampCorrectionPortDS.ingressLatency, |           #
#           |        asymmetryCorrectionPortDS.constantAsymmetry, |           #
#           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                           DN = DN1,Timestamp = Ta]  |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                           DN = DN1,Timestamp = Tb]  |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                           DN = DN1,Timestamp = Tc]  |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |              <Check ((Tb - Ta) + (Tc - Tb))/2 = 2s> |           #
#           |                                                     |           #
#           |                   <Configure announce interval = 0> |           #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                          DN = DN1,Timestamp = Ta]   |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                           DN = DN1,Timestamp = Tb]  |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                           DN = DN1,Timestamp = Tc]  |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |              <Check ((Tb - Ta) + (Tc - Tb))/2 = 1s> |           #
#           |                                                     |           #
#           |                   <Configure announce interval = 4> |           #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                          DN = DN1,Timestamp = Ta]   |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                           DN = DN1,Timestamp = Tb]  |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                           DN = DN1,Timestamp = Tc]  |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |             <Check ((Tb - Ta) + (Tc - Tb))/2 = 16s> |           #
#           |                                                     |           #
#           |                   <Configure announce interval = 1> |           #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                          DN = DN1,Timestamp = Ta]   |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                           DN = DN1,Timestamp = Tb]  |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                           DN = DN1,Timestamp = Tc]  |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |              <Check ((Tb - Ta) + (Tc - Tb))/2 = 2s> |           #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     MSG_TYPE  = Message Type                                                #
#     DN        = Domain Number                                               #
#                                                                             #
# NOTE :                                                                      #
#                                                                             #
#  1. This objective is verified using the High Accuracy Delay Request-       #
#     Response Default PTP Profile                                            #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.ingressLatency,           #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Check whether that the DUT transmits three consecutive ANNOUNCE    #
#          messages on the port P1 with following parameters and store        #
#          timestamps Ta, Tb and Tc for messages respectively.                #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                                                                             #
# Step 4 : Check whether ((Tb - Ta) + (Tc - Tb))/2 = 2s                       #
#                                                                             #
# Step 5 : Configure Log Announce Interval as 0 on Port P1 in DUT.            #
#                                                                             #
# Step 6 : Check whether that the DUT transmits three consecutive ANNOUNCE    #
#          messages on the port P1 with following parameters and store        #
#          timestamps Ta, Tb and Tc for messages respectively.                #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                                                                             #
# Step 7 : Check whether ((Tb - Ta) + (Tc - Tb))/2 = 1s                       #
#                                                                             #
# Step 8 : Configure Log Announce Interval as 4 on Port P1 in DUT.            #
#                                                                             #
# Step 9 : Check whether that the DUT transmits three consecutive ANNOUNCE    #
#          messages on the port P1 with following parameters and store        #
#          timestamps Ta, Tb and Tc for messages respectively.                #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                                                                             #
# Step 10: Check whether ((Tb - Ta) + (Tc - Tb))/2 = 16s                      #
#                                                                             #
# Step 11: Configure Log Announce Interval as 1 on Port P1 in DUT.            #
#                                                                             #
# Step 12: Check whether that the DUT transmits three consecutive ANNOUNCE    #
#          messages on the port P1 with following parameters and store        #
#          timestamps Ta, Tb and Tc for messages respectively.                #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                                                                             #
# Step 13: Verify whether ((Tb - Ta) + (Tc - Tb))/2 = 2s                      #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          May/2018      CERN          Initial                            #
# 1.1          Jun/2018      CERN          Fixed dut_port in                  #
#                                          dut_reset_announce_interval        #
# 1.2          Jul/2018      CERN          Updated tolerance to 30% in        #
#                                          interval measurement               #
# 1.3          Sep/2018      CERN          a) Removed destination MAC and IP  #
#                                             in send functions.              #
#                                          b) Removed timeout in recv         #
#                                             functions.                      #
#                                          c) Moved timeout calculation to    #
#                                             PTP-HA global file.             #
#                                          d) Capture buffer is cleared after #
#                                             every configuration step.       #
#                                                                             #
###############################################################################
# Copyright (c) 2018 CERN                                                     #
###############################################################################

global dut_log_msg
global tee_log_msg
global session_id

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def "To verify that a PTP enabled device transmits Announce\
            messages at configured logAnnounceInterval\
            (allowable range: 0 to 4)." 

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set dut_port_1                     $::dut_port_num_1
set vlan_id                        $::ptp_ha_vlan_id

set dut_ip                         $::dut_test_port_ip
set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT

set domain                         $::DEFAULT_DOMAIN
set gmid                           $::GRANDMASTER_ID
set infinity                       $::INFINITY

set sequence_id_ann                1
set sequence_id_l1sync             43692

set ann_int                        0

########################### END - INITIALIZATION ##############################

ptp_ha::display_port_details

ptp_ha::dut_Initialization

proc cleanup {} {

    ptp_ha::dut_cleanup_setup_001

    ptp_ha::dut_reset_announce_interval \
            -port_num        $::dut_port_num_1

}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.ingressLatency,           #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
###############################################################################

STEP 1

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP1)"

if {![ptp_ha::dut_configure_setup_001]} {

    LOG -msg "$dut_log_msg(INIT_SETUP1_F)"

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP1_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
###############################################################################

STEP 2

LOG -level 0 -msg "$tee_log_msg(INIT)"

ptp_ha::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

    if {![PTP_HA_UNICAST_PRE_REQUISITE_CONFIGURATION\
                        -port_num       $tee_port_1\
                        -tee_mac        $tee_mac\
                        -tee_ip         $tee_ip\
                        -dut_ip         $dut_ip\
                        -vlan_id        $vlan_id\
                        -dut_mac        tee_dest_mac\
                        -timeout        $arp_timeout]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
                           -tc_def  $tc_def

        return 0
    }
}

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_1\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def  $tc_def

    return 0
}

set filter_id [lindex $id1 end]

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

# (Part 1)#

###############################################################################
# Step 3 : Check whether that the DUT transmits three consecutive ANNOUNCE    #
#          messages on the port P1 with following parameters and store        #
#          timestamps Ta, Tb and Tc for messages respectively.                #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
###############################################################################

STEP 3a

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -reset_count       $::reset_count_off\
             -rx_timestamp_ns   Ta]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}


STEP 3b

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -reset_count       $::reset_count_off\
             -rx_timestamp_ns   Tb]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

STEP 3c

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -reset_count       $::reset_count_off\
             -rx_timestamp_ns   Tc]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
#                                                                             #
# Step 4 : Check whether ((Tb - Ta) + (Tc - Tb))/2 = 2s                       #
###############################################################################

STEP 4

set b [ expr (((($Tb-$Ta) + ($Tc-$Tb))/2)/1000000000.0) ]
set b [format %.1f $b]

LOG -level 0 -msg "$dut_log_msg(CHECK_EXPR)"
LOG -level 0 -msg "\nAnnounce Interval:"
LOG -level 0 -msg "               Expected = 2 secs (Allowable range: 1.4 s - 2.6 s)"
LOG -level 0 -msg "               Observed = $b secs"

if {!($b >= 1.4 && $b <= 2.6)} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$dut_log_msg(CHECK_EXPR_F)"\
                        -tc_def  $tc_def
    return 0
}

###################################################################################
#                                                                                 #
# Step 5 : Configure Log Announce Interval as 0 on Port P1 in DUT.                #
###################################################################################

STEP 5

LOG -level 0 -msg "$dut_log_msg(CONFIG_ANN_INTVL_0)"

  if {![ptp_ha::dut_set_announce_interval \
                -port_num                $dut_port_1\
                -announce_interval       $ann_int]} {

      TEE_CLEANUP

      TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_ANN_INTVL_F)"\
                         -tc_def  $tc_def

      return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
#                                                                             #
# Step 6 : Check whether that the DUT transmits three consecutive ANNOUNCE    #
#          messages on the port P1 with following parameters and store        #
#          timestamps Ta, Tb and Tc for messages respectively.                #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
###############################################################################

STEP 6a

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -reset_count       $::reset_count_off\
             -rx_timestamp_ns   Ta]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}


STEP 6b

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -reset_count       $::reset_count_off\
             -rx_timestamp_ns   Tb]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}


STEP 6c

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -reset_count       $::reset_count_off\
             -rx_timestamp_ns   Tc]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}


###############################################################################
#                                                                             #
# Step 7 : Check whether ((Tb - Ta) + (Tc - Tb))/2 = 1s                       #
###############################################################################

STEP 7

set b [ expr (((($Tb-$Ta) + ($Tc-$Tb))/2)/1000000000.0) ]
set b [format %.1f $b]

LOG -level 0 -msg "$dut_log_msg(CHECK_EXPR)"
LOG -level 0 -msg "\nAnnounce Interval:"
LOG -level 0 -msg "               Expected = 1 sec (Allowable range: 0.7 s - 1.3 s)"
LOG -level 0 -msg "               Observed = $b secs"

if {!($b >= 0.7 && $b <= 1.3)} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$dut_log_msg(CHECK_EXPR_F)"\
                        -tc_def  $tc_def
    return 0
}

###################################################################################
#                                                                                 #
# Step 8 : Configure Log Announce Interval as 4 on Port P1 in DUT.                #
###################################################################################

STEP 8

LOG -level 0 -msg "$dut_log_msg(CONFIG_ANN_INTVL_4)"

  if {![ptp_ha::dut_set_announce_interval \
                -port_num                $dut_port_1\
                -announce_interval       4]} {

      TEE_CLEANUP

      TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_ANN_INTVL_F)"\
                         -tc_def  $tc_def

      return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
#                                                                             #
# Step 9 : Check whether that the DUT transmits three consecutive ANNOUNCE    #
#          messages on the port P1 with following parameters and store        #
#          timestamps Ta, Tb and Tc for messages respectively.                #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
###############################################################################

STEP 9a

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -reset_count       $::reset_count_off\
             -rx_timestamp_ns   Ta]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}


STEP 9b

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -reset_count       $::reset_count_off\
             -rx_timestamp_ns   Tb]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}


STEP 9c

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -reset_count       $::reset_count_off\
             -rx_timestamp_ns   Tc]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}


###############################################################################
#                                                                             #
# Step 10: Check whether ((Tb - Ta) + (Tc - Tb))/2 = 16s                      #
###############################################################################

STEP 10

set b [ expr (((($Tb-$Ta) + ($Tc-$Tb))/2)/1000000000.0) ]
set b [format %.1f $b]

LOG -level 0 -msg " $dut_log_msg(CHECK_EXPR)"
LOG -level 0 -msg "\nAnnounce Interval:"
LOG -level 0 -msg "               Expected = 16 secs (Allowable range: 11.2 s - 20.8 s)"
LOG -level 0 -msg "               Observed = $b secs"

if {!($b >= 11.2 && $b <= 20.8)} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$dut_log_msg(CHECK_EXPR_F)"\
                        -tc_def  $tc_def
    return 0
}

###################################################################################
#                                                                                 #
# Step 11: Configure Log Announce Interval as 1 on Port P1 in DUT.                #
###################################################################################

STEP 11

LOG -level 0 -msg "$dut_log_msg(CONFIG_ANN_INTVL_1)"

  if {![ptp_ha::dut_set_announce_interval \
                -port_num                $dut_port_1\
                -announce_interval       1]} {

      TEE_CLEANUP

      TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_ANN_INTVL_F)"\
                         -tc_def  $tc_def

      return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 12: Check whether that the DUT transmits three consecutive ANNOUNCE    #
#          messages on the port P1 with following parameters and store        #
#          timestamps Ta, Tb and Tc for messages respectively.                #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
###############################################################################

STEP 12a

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -reset_count       $::reset_count_off\
             -rx_timestamp_ns   Ta]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}


STEP 12b

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -reset_count       $::reset_count_off\
             -rx_timestamp_ns   Tb]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}


STEP 12c

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -reset_count       $::reset_count_off\
             -rx_timestamp_ns   Tc]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}


###############################################################################
#                                                                             #
# Step 13: Verify whether ((Tb - Ta) + (Tc - Tb))/2 = 2s                      #
#                                                                             #
###############################################################################

STEP 13


set b [ expr (((($Tb-$Ta) + ($Tc-$Tb))/2)/1000000000.0) ]
set b [format %.1f $b]

LOG -level 0 -msg " $dut_log_msg(CHECK_EXPR)"
LOG -level 0 -msg "\nAnnounce Interval:"
LOG -level 0 -msg "               Expected = 2 secs (Allowable range: 1.4 s - 2.6 s)"
LOG -level 0 -msg "               Observed = $b secs"

if {!($b >= 1.4 && $b <= 2.6)} {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL   -reason "PTP enabled device does not transmits Announce\
                                 messages at configured logAnnounceInterval\
                                 (allowable range: 0 to 4)"\
                        -tc_def  $tc_def
    return 0
}

TEE_CLEANUP

TC_CLEAN_AND_PASS    -reason "PTP enabled device transmits Announce\
                             messages at configured logAnnounceInterval\
                             (allowable range: 0 to 4)"\
                    -tc_def  $tc_def
return 1

################################## END OF TEST CASE ############################
