#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_ptp-ha_idp_002                                  #
# Test Case Version : 1.0                                                     #
# Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    #
# Module Name       : Inter-operation with the Peer-to-peer Default PTP       #
#                     profile when DUT is master                              #
#                                                                             #
# Title             : Inter-operation with the Peer-to-peer Default           #
#                     PTP profile when DUT is master                          #
#                                                                             #
# Purpose           : To verify that a PTP enabled device using Peer-to-Peer  #
#                     delay mechanism synchronizes its High Accuracy          #
#                     Peer-to-Peer Delay PTP profile to Peer-to-Peer Default  #
#                     PTP profile when it is master.                          #
#                                                                             #
# Reference         : P1588/D1.3, February 2018 V3.01 Clause J.5.4 Page 414   #
#                                                                             #
# Conformance Type  : SHALL                                                   #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 002                                                     #
# Test Topology     : 001                                                     #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#                                                                             #
#          TEE                                                   DUT          #
#        [slave]                                              [master]        #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode - one-step/two-step> |           #
#           |                        <Set delay mechanism as P2P> |           #
#           |                     <Configure logAnnounceInterval, | P1        #
#           |                             announceReceiptTimeout> |           #
#           |                         <Configure logSyncInterval> | P1        #
#           |                  <Configure logMinDelayReqInterval> | P1        #
#           |                    <Configure Priority1, Priority2> | P1        #
#           |                                     <Enable L1SYNC> | P1        #
#           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        #
#           |                     <Disable L1SynOptParams option> | P1        #
#           |           <Enable asymmetryCorrectionPortDS.enable> | P1        #
#           | <Configure timestampCorrectionPortDS.egressLatency, | P1        #
#           |           timestampCorrectionPortDS.ingressLatency, |           #
#           |        asymmetryCorrectionPortDS.constantAsymmetry, |           #
#           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           #
#           |                                                     |           #
#           |                           ANNOUNCE [MSG_TYPE = 0x0B,|           #
#           |                                    PRI=X, DN = DN1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | ANNOUNCE [MSG_TYPE = 0x0B,                          |           #
#           | PRI=X+1, DN = DN1]                                  |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |    PTP SIGNALLING with L1 Sync TLV [MSG_TYPE = 0x0C,|           #
#           |                                           DN = DN1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | PDELAY_REQ [MSG_TYPE = 0x02,                        |           #
#           | SRC_PORT_IDEN = E, DN = DN1]                        |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                        PDELAY_RESP [MSG_TYPE = 0x03,|           #
#           |                         SRC_PORT_IDEN = F, DN = DN1 |           #
#           |                                  REQ_PORT_IDEN = E] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |              PDELAY_RESP_FOLLOW_UP [MSG_TYPE = 0x0A,|           #
#           |                         SRC_PORT_IDEN = F, DN = DN1 |           #
#           |                                  REQ_PORT_IDEN = E] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                   <Check L1SYNC port status - IDLE> | P1        #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     MSG_TYPE       = Message Type                                           #
#     DN             = Domain Number                                          #
#     PRI            = Priority                                               #
#     BC             = Boundary Clock                                         #
#     OC             = Ordinary Clock                                         #
#     P2P            = Peer to Peer                                           #
#     SRC_PORT_IDEN  = Source Port Identity                                   #
#     REQ_PORT_IDEN  = Requesting Port Identity                               #
#                                                                             #
# NOTE :                                                                      #
#                                                                             #
#  1. This objective is verified using the High Accuracy Peer to Peer Default #
#     PTP Profile                                                             #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure delaymechanism as Peer to peer.                          #
#    vi.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#   vii.   Enable L1SYNC on DUT's port P1.                                    #
#  viii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#    ix.   Disable L1SynOptParams on DUT.                                     #
#     x.   Enable asymmetryCorrectionPortDS.enable.                           #
#    xi.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.ingressLatency,           #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
#                  Priority      = X                                          #
#                                                                             #
# Step 4 : Send ANNOUNCE message on the port T1 with following parameters.    #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
#                  Priority      = X+1                                        #
#                                                                             #
# Step 5 : Observe that DUT transmits PTP message with L1 Sync TLV on the     #
#          port P1 with the following parameters.                             #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0C                                       #
#                  Domain Number = DN1                                        #
#                                                                             #
# Step 6 : Send PDELAY_REQ message on the port T1 with following parameters.  #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type          = 0x02                               #
#                  Domain Number         = DN1                                #
#                  Source Port Identity  = E                                  #
#                                                                             #
# Step 7 : Observe that DUT transmits PDELAY_RESP message on the port P1 with #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type              = 0x03                           #
#                  Domain Number             = DN1                            #
#                  Source Port Identity      = F                              #
#                  Requesting Port Identity  = E                              #
#                                                                             #
# Step 7a: If the clock is two-step clock, observe that DUT transmits         #
#          PDELAY_RESP_FOLLOW_UP message on the port P1 with following        #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type              = 0x0A                           #
#                  Domain Number             = DN1                            #
#                  Source Port Identity      = F                              #
#                  Requesting Port Identity  = E                              #
#                                                                             #
# Step 8 : Verify that DUT's L1SYNC port status P1 is in IDLE state.          #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          Oct/2018      CERN          Initial                            #
#                                                                             #
###############################################################################
# Copyright (c) 2018 CERN                                                     #
###############################################################################

global dut_log_msg
global tee_log_msg
global session_id

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def    "To verify that a PTP enabled device using Peer-to-Peer\
               delay mechanism synchronizes its High Accuracy\
               Peer-to-Peer Delay PTP profile to Peer-to-Peer\
               Default PTP profile when it is master."

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set dut_port_1                     $::dut_port_num_1
set vlan_id                        $::ptp_ha_vlan_id
set dut_ip                         $::dut_test_port_ip
set dut_dest_ip                    $::dut_test_port_ip
set tee_dest_ip                    $::tee_test_port_ip
set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT
set domain                         $::DEFAULT_DOMAIN
set infinity                       $::INFINITY
set idle                           $::PTP_L1SYNC_STATE(IDLE)
set tlv_type(l1sync)               $::TLV_TYPE(L1SYNC)
set sequence_id_ann                1

set clock_step                     $::ptp_dut_clock_step
set src_port_number                0
set src_clock_identity             0

########################### END - INITIALIZATION ##############################

if { $::ptp_p2p_delay_mechanism != "true" } {

    TC_ABORT -reason "$tee_log_msg(P2P_DLY_MECHANISM)"\
             -tc_def $tc_def

    return 0
}

ptp_ha::display_port_details

ptp_ha::dut_Initialization

proc cleanup {} {

    ptp_ha::dut_cleanup_setup_002

}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

### (Initial Part) ###

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure delaymechanism as Peer to peer.                          #
#    vi.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#   vii.   Enable L1SYNC on DUT's port P1.                                    #
#  viii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#    ix.   Disable L1SynOptParams on DUT.                                     #
#     x.   Enable asymmetryCorrectionPortDS.enable.                           #
#    xi.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.ingressLatency,           #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
###############################################################################

STEP 1

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP2)"

if {![ptp_ha::dut_configure_setup_002]} {

    LOG -msg "$dut_log_msg(INIT_SETUP2_F)"

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP2_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
###############################################################################

STEP 2

LOG -level 0 -msg "$tee_log_msg(INIT)"

ptp_ha::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

    if {![PTP_HA_UNICAST_PRE_REQUISITE_CONFIGURATION\
                        -port_num       $tee_port_1\
                        -tee_mac        $tee_mac\
                        -tee_ip         $tee_ip\
                        -dut_ip         $dut_ip\
                        -vlan_id        $vlan_id\
                        -dut_mac        tee_dest_mac\
                        -timeout        $arp_timeout]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
                           -tc_def  $tc_def

        return 0
    }

}

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_1\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def  $tc_def

    return 0
}

set filter_id [lindex $id1 end]

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 3 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
#                  Priority1     = X                                          #
#                                                                             #
###############################################################################

STEP 3

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
           -port_num                $tee_port_1\
           -domain_number           $domain\
           -filter_id               $filter_id\
           -error_reason            error_reason\
           -recvd_gm_priority1      priority1]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANNOUNCE_RX_P1_F) $error_reason"\
                        -tc_def  $tc_def

    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 4 : Send periodic ANNOUNCE message with Priority1 value incremented    #
#          from the Priority1 value of received Announce message on port T1   #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
#                  Priority1     = X+1                                        #
#                                                                             #
###############################################################################

STEP 4

set priority [ expr $priority1 + 1 ]

LOG -level 0 -msg "$tee_log_msg(ANN_TX_T1)"

if {![ptp_ha::send_announce \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -gm_priority1            $priority\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -sequence_id             $sequence_id_ann\
           -count                   $infinity]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANN_TX_T1_F)"\
                       -tc_def $tc_def

    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 5 : Observe that DUT transmits PTP message with L1 Sync TLV            #
#          on the port P1 with following parameters:                          #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#                                                                             #
###############################################################################

STEP 5

LOG -level 0 -msg "$tee_log_msg(L1SYNC_RX_P1_O)"

if {![ptp_ha::recv_signal \
              -port_num                $tee_port_1\
              -domain_number           $domain\
              -tlv_type                $tlv_type(l1sync)\
              -error_reason            error_reason\
              -filter_id               $filter_id]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason  "$tee_log_msg(L1SYNC_RX_P1_F) $error_reason"\
                       -tc_def   $tc_def

    return 0
}

###############################################################################
# Step 6 : Send PDELAY_REQ message on the port T1 with following parameters.  #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type          = 0x02                               #
#                  Domain Number         = DN1                                #
#                  Source Port Identity  = E                                  #
###############################################################################

STEP 6

LOG -level 0 -msg "$tee_log_msg(PDELAY_REQ_TX)"

if {![ptp_ha::send_pdelay_req\
           -port_num            $tee_port_1\
           -src_mac             $tee_mac\
           -src_ip              $tee_ip\
           -domain_number       $domain\
           -src_port_number     $src_port_number\
           -src_clock_identity  $src_clock_identity]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason  "$tee_log_msg(PDELAY_REQ_TX_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 7 : Observe that DUT transmits PDELAY_RESP message on the port P1 with #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type              = 0x03                           #
#                  Domain Number             = DN1                            #
#                  Source Port Identity      = F                              #
#                  Requesting Port Identity  = E                              #
###############################################################################

STEP 7

LOG -level 0 -msg "$tee_log_msg(PDELAY_RESP_RX_P1_O)"

if {![ptp_ha::recv_pdelay_resp \
           -port_num                         $tee_port_1\
           -domain_number                    $domain\
           -requesting_port_number           $src_port_number\
           -requesting_clock_identity        $src_clock_identity\
           -filter_id                        $filter_id\
           -recvd_src_port_number            recvd_src_port_number\
           -recvd_src_clock_identity         recvd_src_clock_identity\
           -error_reason                     error_reason]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(PDELAY_RESP_RX_P1_F) $error_reason"\
                        -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 7a: If the clock is two-step clock, observe that DUT transmits         #
#          PDELAY_RESP_FOLLOW_UP message on the port P1 with following        #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type              = 0x0A                           #
#                  Domain Number             = DN1                            #
#                  Source Port Identity      = F                              #
#                  Requesting Port Identity  = E                              #
###############################################################################

if { $clock_step == "Two-Step" } {

STEP "7a"

LOG -level 0 -msg "$tee_log_msg(PDELAY_RESP_FOLLOW_UP_RX_P1_O)"

    if {![ptp_ha::recv_pdelay_resp_followup\
           -port_num                         $tee_port_1\
           -domain_number                    $domain\
           -filter_id                        $filter_id\
           -error_reason                     error_reason\
           -requesting_port_number           $src_port_number\
           -requesting_clock_identity        $src_clock_identity]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(PDELAY_RESP_FOLLOW_UP_RX_P1_F) $error_reason"\
                            -tc_def  $tc_def

        return 0
    }

}

###############################################################################
# Step 8 : Verify that DUT's L1SYNC port status P1 is in IDLE state.          #
#                                                                             #
###############################################################################

STEP 8

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_IDLE_P1_V)"

if {![ptp_ha::dut_check_l1sync_state \
              -port_num                $dut_port_1\
              -l1sync_state            $idle] } {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL   -reason "DUT's L1SYNC port status is not in\
                                 IDLE state."\
                        -tc_def  $tc_def
    return 0
}

TEE_CLEANUP

TC_CLEAN_AND_PASS -reason  "DUT using Peer-to-Peerdelay mechanism\
                            synchronizeis its High Accuracy Peer-to-Peer\
                            Delay PTP profile to Peer-to-Peer\
                            Default PTP profile when it is master."\
                  -tc_def   $tc_def
return 1

############################ END OF TEST CASE #################################
