#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_ptp-ha_pag_002                                  #
# Test Case Version : 1.6                                                     #
# Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    #
# Module Name       : PTP Accuracy Group (PAG)                                #
#                                                                             #
# Title             : Egress timestamp in Delay_Req message                   #
#                                                                             #
# Purpose           : To verify that a PTP enabled device generates Egress    #
#                     timestamp in Delay_Req (event) messages from            #
#                     timestampCorrectionPortDS.egressLatency when using      #
#                     Delay Request-Response mechanism.                       #
#                                                                             #
# Reference         : IEEE 1588-2017 Clause 16.7.1 Page 301, Clause 7.3.4.2   #
#                     Page 68, Clause 8.2.16.2 Page 128                       #
#                                                                             #
# Conformance Type  : SHALL                                                   #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 009                                                     #
# Test Topology     : 001                                                     #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode - one-step/two-step> |           #
#           |                     <Configure logAnnounceInterval, | P1        #
#           |                             announceReceiptTimeout> |           #
#           |                         <Configure logSyncInterval> | P1        #
#           |                  <Configure logMinDelayReqInterval> | P1        #
#           |                    <Configure Priority1, Priority2> | P1        #
#           |                                     <Enable L1SYNC> | P1        #
#           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        #
#           |                     <Disable L1SynOptParams option> | P1        #
#           |           <Enable asymmetryCorrectionPortDS.enable> | P1        #
#           | <Configure timestampCorrectionPortDS.egressLatency, | P1        #
#           |           timestampCorrectionPortDS.ingressLatency, |           #
#           |        asymmetryCorrectionPortDS.constantAsymmetry, |           #
#           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                                 DN = DN1, PRI1 = X] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | ANNOUNCE [MSG_TYPE = 0x0B,                          |           #
#           | DN = DN1, PRI1 = X-1]                               |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#        T1 | <Enable auto responder to Delay_Req messages>       | P1        #
#           |                                                     |           #
#           | <Send Sync/ Sync & Follow-up messages               |           #
#           | based on clock step>                                |           #
#           |                                                     |           #
#           |          < Wait for 6s to complete BMCA >           |           #
#           |                                                     |           #
#           |                      PTP SIGNALING with L1 Sync TLV |           #
#           |                          [MSG_TYPE = 0xC, DN = DN1, |           #
#           |                                  TLV_TYPE = 0x8001] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | PTP SIGNALING with L1 Sync TLV                      |           #
#           | [MSG_TYPE = 0xC, DN = DN1,                          |           #
#           | TLV_TYPE = 0x8001, TCR = 1,                         |           #
#           | RCR = 1, CR = 1, ITC = 1,                           |           #
#           | IRC = 1, IC = 1]                                    |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |             <Check L1SYNC port status - L1_SYNC_UP> | P1        #
#           |                                                     |           #
#           | <Check non-zero absolute value of currentDS.        |           #
#           |             offsetFromMaster is lowest as possible> |           #
#           |                                                     |           #
#           |                     <Get currentDS.meanDelay (MD1)> |           #
#           |                                                     |           #
#           |  <Configure timestampCorrectionPortDS.egressLatency |           #
#           |                                 value to 2^(32+16)> |           #
#           |                                                     |           #
#           |               DELAY_REQ [MSG_TYPE = 0x01, DN = DN1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | <Check non-zero absolute value of currentDS.        |           #
#           |             offsetFromMaster is lowest as possible> |           #
#           |                                                     |           #
#           |                     <Get currentDS.meanDelay (MD2)> |           #
#           |                                                     |           #
#           |                       MD2 < MD1                     |           #
#           |                                                     |           #
#           |  <Configure timestampCorrectionPortDS.egressLatency |           #
#           |                                value to -2^(32+16)> |           #
#           |                                                     |           #
#           |               DELAY_REQ [MSG_TYPE = 0x01, DN = DN1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | <Check non-zero absolute value of currentDS.        |           #
#           |             offsetFromMaster is lowest as possible> |           #
#           |                                                     |           #
#           |                     <Get currentDS.meanDelay (MD3)> |           #
#           |                                                     |           #
#           |                       MD3 > MD1                     |           #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     MSG_TYPE  = Message Type                                                #
#     DN        = Domain Number                                               #
#     PRT1      = priority1                                                   #
#     MD1 - MD3 = currentDS.meanDelay                                         #
#                                                                             #
# NOTE :                                                                      #
#                                                                             #
#  1. This objective is verified using the High Accuracy Delay Request-       #
#     Response Default PTP Profile                                            #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency = 0, timestampCorrectionPortDS.egressLatency = 0,    #
#          asymmetryCorrectionPortDS.constantAsymmetry = 0 and                #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient = 0.              #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x0B                                    #
#                  Domain Number    = DN1                                     #
#                  Priority1        = X                                       #
#                                                                             #
# Step 4 : Send periodic ANNOUNCE message on port T1 with following parameters#
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x0B                                    #
#                  Domain Number    = DN1                                     #
#                  Priority1        = X-1                                     #
#                                                                             #
# Step 5 : Enable auto responder to respond every Delay_Req messages received #
#          on port T1.                                                        #
#                                                                             #
# Step 6 : Send periodic SYNC message on the port P1 with with following      #
#          parameters:                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x00                                    #
#                  Domain Number    = DN1                                     #
#                                                                             #
# Step 6a: If the clock is two-step clock, send periodic FOLLOW_UP message on #
#          port T1 with following parameters:                                 #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x08                                    #
#                  Domain Number    = DN1                                     #
#                                                                             #
# Step 7 : Wait for 6s for completing BMCA.                                   #
#                                                                             #
# Step 8 : Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV  #
#          on the port P1 with following parameters:                          #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                                                                             #
# Step 9 : Send periodic PTP SIGNALING message with L1 Sync TLV on the port P1#
#          with following parameters:                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 1                                          #
#                  RCR           = 1                                          #
#                  CR            = 1                                          #
#                  ITC           = 1                                          #
#                  IRC           = 1                                          #
#                  IC            = 1                                          #
#                                                                             #
# Step 10: Observe that the DUT's L1SYNC port status of P1 is L1_SYNC_UP.     #
#                                                                             #
# Step 11: Check whether the non-zero absolute value of currentDS.            #
#          offsetFromMaster in DUT becomes lowest as possible to ensure that  #
#          the DUT synchronizes it's time with TEE.                           #
#                                                                             #
# Step 12: Get currentDS.meanDelay (MD1) of DUT.                              #
#                                                                             #
# Step 13: Configure egressLatency on port P1 by setting egressLatency to     #
#          4 294 967 296 ns (i.e., the value of dataset expressed in          #
#          TimeInterval asymmetryCorrectionPortDS.egressLatency = 2^48).      #
#                                                                             #
# Step 14: Observe that the DUT transmits DELAY_REQ message on port P1 with   #
#          following parameters to ensure the DUT is ready with configured    #
#          asymmetryCorrectionPortDS.egressLatency value.                     #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x01                                    #
#                  Domain Number    = DN1                                     #
#                                                                             #
# Step 15: Check whether the non-zero absolute value of currentDS.            #
#          offsetFromMaster in DUT becomes lowest as possible to ensure that  #
#          the DUT synchronizes it's time with TEE.                           #
#                                                                             #
# Step 16: Get currentDS.meanDelay (MD2) of DUT.                              #
#                                                                             #
# Step 17: Observe that MD2 is lesser than MD1.                               #
#                                                                             #
# Step 18: Configure egressLatency on port P1 by setting egressLatency to     #
#          -4 294 967 296 ns (i.e., the value of dataset expressed in         #
#          TimeInterval asymmetryCorrectionPortDS.egressLatency = -2^48).     #
#                                                                             #
# Step 19: Observe that the DUT transmits DELAY_REQ message on port P1 with   #
#          following parameters to ensure the DUT is ready with configured    #
#          asymmetryCorrectionPortDS.egressLatency value.                     #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x01                                    #
#                  Domain Number    = DN1                                     #
#                                                                             #
# Step 20: Check whether the non-zero absolute value of currentDS.            #
#          offsetFromMaster in DUT becomes lowest as possible to ensure that  #
#          the DUT synchronizes it's time with TEE.                           #
#                                                                             #
# Step 21: Get currentDS.meanDelay (MD3) of DUT.                              #
#                                                                             #
# Step 22: Verify that MD3 is greater than MD1.                               #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          May/2018      CERN          Initial                            #
# 1.1          Jun/2018      CERN          Fixed dut_port in                  #
#                                          dut_reset_egress_latency           #
# 1.2          Jul/2018      CERN          Updated delay request timeout      #
# 1.3          Sep/2018      CERN          a) Removed destination MAC and IP  #
#                                             in send functions.              #
#                                          b) Removed timeout in recv         #
#                                             functions.                      #
#                                          c) Moved timeout calculation to    #
#                                             PTP-HA global file.             #
#                                          d) Added wait to complete BMCA.    #
#                                          e) Copied appropriate fields from  #
#                                             Delay_Req to Delay_Resp message.#
#                                          f) Re-worded header and log prints #
#                                             to improve understandability.   #
# 1.4          Sep/2018      CERN          Added steps to make L1Sync state UP#
# 1.5          Nov/2018      CERN          Re-ordered message exchanges in    #
#                                          Ladder Diagram and Procedure to    #
#                                          include the auto-responder module  #
#                                          and avoid timing issues while      #
#                                          extracting meanDelay values.       #
# 1.6          Mar/2019      CERN          a) Added steps to check whether    #
#                                             offsetFromMaster becomes least  #
#                                             non-zero value to ensure DUT    #
#                                             synchronizes its time with TEE. #
#                                          b) Added support to proceed test   #
#                                             until last step and gracefully  #
#                                             abort the test for debugging.   #
#                                                                             #
###############################################################################
# Copyright (c) 2018 - 2019 CERN                                              #
###############################################################################

global dut_log_msg
global tee_log_msg
global session_id

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def  "To verify that a PTP enabled device generates Egress\
             timestamp in Delay_Req (event) messages from\
             timestampCorrectionPortDS.egressLatency when using\
             Delay Request-Response mechanism."

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set dut_port_1                     $::dut_port_num_1
set vlan_id                        $::ptp_ha_vlan_id
set dut_ip                         $::dut_test_port_ip
set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT

set domain                         $::DEFAULT_DOMAIN
set gmid                           $::GRANDMASTER_ID
set infinity                       $::INFINITY
set clock_step                     $::ptp_dut_clock_step
set sequence_id_ann                1
set sequence_id_l1sync             43692
set offset_from_master             $::offset_from_master

set tlv_type(l1sync)               $::TLV_TYPE(L1SYNC)
set idle                           $::PTP_L1SYNC_STATE(IDLE)
set link_alive                     $::PTP_L1SYNC_STATE(LINK_ALIVE)
set config_match                   $::PTP_L1SYNC_STATE(CONFIG_MATCH)
set l1_sync_up                     $::PTP_L1SYNC_STATE(L1_SYNC_UP)

set tcr_true        1
set rcr_true        1
set cr_true         1
set itc_true        1
set irc_true        1
set ic_true         1

set tcr_false       0
set rcr_false       0
set cr_false        0
set itc_false       0
set irc_false       0
set ic_false        0

set SKIP_VALIDATION $::SKIP_VALIDATION_FOR_DEBUG

########################### END - INITIALIZATION ##############################

if {($env(TEST_DEVICE_MANAGEMENT_MODE) == "Manual")} {
    
    puts ".\n"
    puts "???????????????????????????????????????????????????????????????????????????"
    puts "???????????????????????????????????????????????????????????????????????????"
    puts "?? WARNING: Execution of this test in manual management mode may give    ??"
    puts "??          in-correct result.                                           ??"
    puts "??                                                                       ??"
    puts "?? The timestamps from the device should be taken faster and hence it is ??"
    puts "?? advised to execute this test case in automated mode.                  ??"
    puts "??                                                                       ??"
    puts "???????????????????????????????????????????????????????????????????????????"
    puts "???????????????????????????????????????????????????????????????????????????"
    puts ".\n"
}

ptp_ha::display_port_details

ptp_ha::dut_Initialization

proc cleanup {} {

    ptp_ha::dut_cleanup_setup_009

    ptp_ha::dut_reset_egress_latency\
             -port_num      $::dut_port_num_1

}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

### (Initial Part) ###

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency = 0, timestampCorrectionPortDS.egressLatency = 0,    #
#          asymmetryCorrectionPortDS.constantAsymmetry = 0 and                #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient = 0.              #
#                                                                             #
###############################################################################

STEP 1

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP9)"

if {![ptp_ha::dut_configure_setup_009]} {

    LOG -msg "$dut_log_msg(INIT_SETUP9_F)"

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP9_F)"\
                       -tc_def  $tc_def
    return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
###############################################################################

STEP 2

LOG -level 0 -msg "$tee_log_msg(INIT)"

ptp_ha::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

    if {![PTP_HA_UNICAST_PRE_REQUISITE_CONFIGURATION\
                        -port_num       $tee_port_1\
                        -tee_mac        $tee_mac\
                        -tee_ip         $tee_ip\
                        -dut_ip         $dut_ip\
                        -vlan_id        $vlan_id\
                        -dut_mac        tee_dest_mac\
                        -timeout        $arp_timeout]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
                           -tc_def $tc_def

        return 0
    }

}

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_1\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def  $tc_def

    return 0
}

set filter_id [lindex $id1 end]

ptp_ha::stop_at_step

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

#(Part 1)#

###############################################################################
# Step 3 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x0B                                    #
#                  Domain Number    = DN1                                     #
#                  Priority1        = X                                       #
#                                                                             #
###############################################################################

STEP 3

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
           -port_num                $tee_port_1\
           -domain_number           $domain\
           -filter_id               $filter_id\
           -recvd_gm_priority1      priority1]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                        -tc_def  $tc_def

    return 0
}

ptp_ha::stop_at_step

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 4 : Send periodic ANNOUNCE message on port T1 with following parameters#
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x0B                                    #
#                  Domain Number    = DN1                                     #
#                  Priority1        = X-1                                     #
#                                                                             #
###############################################################################

STEP 4

set priority [expr $priority1 - 1]

LOG -level 0 -msg "$tee_log_msg(ANN_TX_T1)"

if {![ptp_ha::send_announce \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -gm_priority1            $priority\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -count                   $infinity\
           -gmid                    0x$gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANN_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 5 : Enable auto responder to respond every Delay_Req messages received #
#          on port T1.                                                        #
#                                                                             #
###############################################################################

STEP 5

LOG -level 0 -msg "$tee_log_msg(ENABLE_RESPONDER_TO_DELAY_REQ)"

ptp_ha::enable_auto_response_to_delay_requests

ptp_ha::stop_at_step


###############################################################################
# Step 6 : Send periodic SYNC message on the port P1 with with following      #
#          parameters:                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x00                                    #
#                  Domain Number    = DN1                                     #
#                                                                             #
###############################################################################

STEP 6

LOG -level 0 -msg "$tee_log_msg(SYNC_TX_T1)"

if {![ptp_ha::send_sync \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -count                   $infinity\
           -gmid                    0x$gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 6a: If the clock is two-step clock, send periodic FOLLOW_UP message on #
#          port T1 with following parameters:                                 #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x08                                    #
#                  Domain Number    = DN1                                     #
#                                                                             #
###############################################################################

if { $clock_step == "Two-Step" } {

STEP 6a

LOG -level 0 -msg "$tee_log_msg(FOLLOWUP_TX_T1)"

if {![ptp_ha::send_followup \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -count                   $infinity\
           -gmid                    0x$gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FOLLOWUP_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}
 }

ptp_ha::stop_at_step

###############################################################################
# Step 7 : Wait for 6s for completing BMCA.                                   #
#                                                                             #
###############################################################################

STEP 7

LOG -level 0 -msg "$dut_log_msg(WAIT_BMCA)"

sleep $::ptp_ha_bmca

ptp_ha::stop_at_step

###############################################################################
# Step 8 : Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV  #
#          on the port P1 with following parameters:                          #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
###############################################################################
 
STEP 8

LOG -level 0 -msg "$tee_log_msg(L1SYNC_RX_P1_O)"

  if {![ptp_ha::recv_signal \
                -port_num                $tee_port_1\
                -domain_number           $domain\
                -tlv_type                $tlv_type(l1sync)\
                -filter_id               $filter_id]} {

      TEE_CLEANUP

      TC_CLEAN_AND_ABORT -reason "$tee_log_msg(L1SYNC_RX_P1_F)"\
                         -tc_def  $tc_def

      return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 9 : Send periodic PTP SIGNALING message with L1 Sync TLV on the port P1#
#          with following parameters:                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 1                                          #
#                  RCR           = 1                                          #
#                  CR            = 1                                          #
#                  ITC           = 1                                          #
#                  IRC           = 1                                          #
#                  IC            = 1                                          #
###############################################################################

STEP 9

LOG -level 0 -msg   "$tee_log_msg(L1SYNC_TX_T1)"

if {![ptp_ha::send_signal \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -tlv_type                $tlv_type(l1sync)\
           -src_ip                  $tee_ip\
           -domain_number           $domain\
           -tcr                     $tcr_true\
           -rcr                     $rcr_true\
           -cr                      $cr_true\
           -itc                     $itc_true\
           -irc                     $irc_true\
           -ic                      $ic_true\
           -count                   $infinity]} {

    TEE_CLEANUP
    
    TC_CLEAN_AND_ABORT   -reason "$tee_log_msg(L1SYNC_TX_T1_F)"\
                         -tc_def  $tc_def

    return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 10: Observe that the DUT's L1SYNC port status of P1 is L1_SYNC_UP.     #
###############################################################################

STEP 10

LOG -level 0 -msg  "$dut_log_msg(CHECK_STATE_L1_SYNC_UP_P1_V)"

if {![info exists ::PTP_HA_DEBUG_CLI]} {
    set ::PTP_HA_DEBUG_CLI 1
}

if {$::PTP_HA_DEBUG_CLI != 0} {

    sleep $::ptp_ha_l1sync_wait

    if {![ptp_ha::dut_check_l1sync_state \
                    -port_num                $dut_port_1\
                    -l1sync_state            $l1_sync_up] } {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT  -reason "$dut_log_msg(CHECK_STATE_L1_SYNC_UP_P1_F)"\
                            -tc_def $tc_def
        return 0 
    }
} else {

    LOG -level 0 -msg   "Checking of L1Sync state is skipped by user to avoid\
                        save the time to check the state."
}

ptp_ha::stop_at_step

###############################################################################
# Step 11: Check whether the non-zero absolute value of currentDS.            #
#          offsetFromMaster in DUT becomes lowest as possible to ensure that  #
#          the DUT synchronizes it's time with TEE.                           #
###############################################################################

STEP 11

LOG -level 0 -msg  "$dut_log_msg(CHECK_LOWEST_OFM_V)"

if {![ptp_ha::dut_check_for_least_offset_from_master\
                -offset_from_master         $offset_from_master]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_LOWEST_OFM_F)"\
                       -tc_def $tc_def
    return 0 
}

ptp_ha::stop_at_step

###############################################################################
# Step 12: Get currentDS.meanDelay (MD1) of DUT.                              #
#                                                                             #
###############################################################################

STEP 12

LOG -level 0 -msg "$dut_log_msg(GET_CDS.MEANDELAY_MD1)"

if {![ptp_ha::dut_get_mean_delay \
              -port_num      $dut_port_1\
              -mean_delay    MD1]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(GET_CDS.MEANDELAY_MD1_F)"\
                       -tc_def  $tc_def
    return 0
}

ptp_ha::stop_at_step

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 13: Configure egressLatency on port P1 by setting egressLatency to     #
#          4 294 967 296 ns (i.e., the value of dataset expressed in          #
#          TimeInterval asymmetryCorrectionPortDS.egressLatency = 2^48).      #
###############################################################################

STEP 13

set latency 4294967296

LOG -level 0 -msg "$dut_log_msg(CONFIG_TXPORTDS.EGRESSLATENCY)"

if {![ptp_ha::dut_set_egress_latency\
             -port_num       $dut_port_1\
             -latency        $latency]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_TXPORTDS.EGRESSLATENCY_F)"\
                        -tc_def  $tc_def

     return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 14: Observe that the DUT transmits DELAY_REQ message on port P1 with   #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x01                                    #
#                  Domain Number    = DN1                                     #
#                                                                             #
###############################################################################

STEP 14

LOG -level 0 -msg "$tee_log_msg(DELAY_REQ_RX_O)"

if {![ptp_ha::recv_delay_req \
           -port_num                 $tee_port_1\
           -domain_number            $domain\
           -recvd_correction_field   recvd_correction_field\
           -recvd_src_port_number    recvd_src_port_number\
           -recvd_src_clock_identity recvd_src_clock_identity\
           -recvd_sequence_id        recvd_sequence_id\
           -filter_id                $filter_id]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(DELAY_REQ_RX_F)"\
                        -tc_def  $tc_def

    return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 15: Check whether the non-zero absolute value of currentDS.            #
#          offsetFromMaster in DUT becomes lowest as possible to ensure that  #
#          the DUT synchronizes it's time with TEE.                           #
###############################################################################

STEP 15

LOG -level 0 -msg  "$dut_log_msg(CHECK_LOWEST_OFM_V)"

if {![ptp_ha::dut_check_for_least_offset_from_master\
                -offset_from_master         $offset_from_master]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_LOWEST_OFM_F)"\
                       -tc_def $tc_def
    return 0 
}

ptp_ha::stop_at_step

###############################################################################
# Step 16: Get currentDS.meanDelay (MD2) of DUT.                              #
#                                                                             #
###############################################################################

STEP 16

LOG -level 0 -msg "$dut_log_msg(GET_CDS.MEANDELAY_MD2)"

if {![ptp_ha::dut_get_mean_delay \
              -port_num      $dut_port_1\
              -mean_delay    MD2]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(GET_CDS.MEANDELAY_MD2_F)"\
                       -tc_def  $tc_def
    return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 17: Observe that MD2 is lesser than MD1.                               #
#                                                                             #
###############################################################################

STEP 17

LOG -level 0 -msg "$dut_log_msg(MD2_LESSER_MD1_O)"

LOG -level 3 -msg "Value of meanDelay (MD1) = $MD1 ns"
LOG -level 3 -msg "Value of meanDelay (MD2) = $MD2 ns"

if {!( $MD2 < $MD1 )} {

if {$SKIP_VALIDATION != 1} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(MD2_LESSER_MD1_F)"\
                       -tc_def  $tc_def
    return 0
}
}

ptp_ha::stop_at_step

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 18: Configure egressLatency on port P1 by setting egressLatency to     #
#          4 294 967 296 ns (i.e., the value of dataset expressed in          #
#          TimeInterval asymmetryCorrectionPortDS.egressLatency = 2^48).      #
###############################################################################

STEP 18

set latency -4294967296

LOG -level 0 -msg "$dut_log_msg(CONFIG_TXPORTDS.EGRESSLATENCY)"

if {![ptp_ha::dut_set_egress_latency\
             -port_num      $dut_port_1\
             -latency       $latency]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_TXPORTDS.EGRESSLATENCY_F)"\
                        -tc_def  $tc_def

     return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 19: Observe that the DUT transmits DELAY_REQ message on port P1 with   #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x01                                    #
#                  Domain Number    = DN1                                     #
#                                                                             #
###############################################################################

STEP 19

LOG -level 0 -msg "$tee_log_msg(DELAY_REQ_RX_O)"

if {![ptp_ha::recv_delay_req \
           -port_num                 $tee_port_1\
           -domain_number            $domain\
           -recvd_correction_field   recvd_correction_field\
           -recvd_src_port_number    recvd_src_port_number\
           -recvd_src_clock_identity recvd_src_clock_identity\
           -recvd_sequence_id        recvd_sequence_id\
           -filter_id                $filter_id]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(DELAY_REQ_RX_F)"\
                        -tc_def  $tc_def

    return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 20: Check whether the non-zero absolute value of currentDS.            #
#          offsetFromMaster in DUT becomes lowest as possible to ensure that  #
#          the DUT synchronizes it's time with TEE.                           #
###############################################################################

STEP 20

LOG -level 0 -msg  "$dut_log_msg(CHECK_LOWEST_OFM_V)"

if {![ptp_ha::dut_check_for_least_offset_from_master\
                -offset_from_master         $offset_from_master]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_LOWEST_OFM_F)"\
                       -tc_def $tc_def
    return 0 
}

ptp_ha::stop_at_step

###############################################################################
# Step 21: Get currentDS.meanDelay (MD3) of DUT.                              #
#                                                                             #
###############################################################################

STEP 21

LOG -level 0 -msg "$dut_log_msg(GET_CDS.MEANDELAY_MD3)"

if {![ptp_ha::dut_get_mean_delay \
              -port_num      $dut_port_1\
              -mean_delay    MD3]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(GET_CDS.MEANDELAY_MD3_F)"\
                       -tc_def  $tc_def
    return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 22: Verify that MD3 is greater than MD1.                               #
#                                                                             #
###############################################################################

STEP 22

LOG -level 0 -msg "$dut_log_msg(MD3_GREATER_MD1_V)"

LOG -level 3 -msg "Value of meanDelay (MD1) = $MD1 ns"
LOG -level 3 -msg "Value of meanDelay (MD3) = $MD3 ns"

if {!( $MD3 > $MD1 )} {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL -reason "$dut_log_msg(MD3_GREATER_MD1_F)"\
                      -tc_def  $tc_def

    return 0
}

TEE_CLEANUP

TC_CLEAN_AND_PASS -reason  "PTP enabled device generates Egress\
                            timestamp in Delay_Req (event) messages from\
                            timestampCorrectionPortDS.egressLatency when using\
                            delay request-response mechanism"\
                  -tc_def   $tc_def

return 1

########################### END OF TEST CASE ##################################
