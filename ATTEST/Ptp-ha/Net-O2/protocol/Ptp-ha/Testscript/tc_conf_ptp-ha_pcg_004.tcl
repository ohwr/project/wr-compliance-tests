#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_ptp-ha_pcg_004                                  #
# Test Case Version : 1.3                                                     #
# Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    #
# Module Name       : PTP-HA Configuration Group (PCG)                        #
#                                                                             #
# Title             : logSyncInterval                                         #
#                                                                             #
# Purpose           : To verify that a PTP enabled device transmits Sync      #
#                     messages at configured logSyncInterval (allowable       #
#                     range: -1 to +1).                                       #
#                                                                             #
# Reference         : P1588/D1.3, February 2018 V3.01 Clause J.5.2 Page 412   #
#                     Clause 7.7.2.3 Page 96                                  #
#                                                                             #
# Conformance Type  : SHALL                                                   #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 001                                                     #
# Test Topology     : 001                                                     #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode - one-step/two-step> |           #
#           |                     <Configure logAnnounceInterval, | P1        #
#           |                             announceReceiptTimeout> |           #
#           |                         <Configure logSyncInterval> | P1        #
#           |                  <Configure logMinDelayReqInterval> | P1        #
#           |                    <Configure Priority1, Priority2> | P1        #
#           |                                     <Enable L1SYNC> | P1        #
#           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        #
#           |                     <Disable L1SynOptParams option> | P1        #
#           |           <Enable asymmetryCorrectionPortDS.enable> | P1        #
#           | <Configure timestampCorrectionPortDS.egressLatency, | P1        #
#           |           timestampCorrectionPortDS.ingressLatency, |           #
#           |        asymmetryCorrectionPortDS.constantAsymmetry, |           #
#           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                                  DN = DN1,PRT1 = Z] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | ANNOUNCE [MSG_TYPE = 0x0B,                          |           #
#           |         DN = DN_1,PRT1=Z+1]                         |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                     SYNC [MSG_TYPE = 0x00,DN = DN1, |           #
#           |                                    Timestamp = Ta]  |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                     SYNC [MSG_TYPE = 0x00,DN = DN1, |           #
#           |                                    Timestamp = Tb]  |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                     SYNC [MSG_TYPE = 0x00,DN = DN1, |           #
#           |                                    Timestamp = Tc]  |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |              <Check ((Tb - Ta) + (Tc - Tb))/2 = 1s> |           #
#           |                                                     |           #
#           |                      <Configure sync interval = -1> |           #
#           |                                                     |           #
#           |                     SYNC [MSG_TYPE = 0x00,DN = DN1, |           #
#           |                                    Timestamp = Ta]  |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                     SYNC [MSG_TYPE = 0x00,DN = DN1, |           #
#           |                                    Timestamp = Tb]  |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                     SYNC [MSG_TYPE = 0x00,DN = DN1, |           #
#           |                                    Timestamp = Tc]  |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |            <Check ((Tb - Ta) + (Tc - Tb))/2 = 0.5s> |           #
#           |                                                     |           #
#           |                      <Configure sync interval =  1> |           #
#           |                                                     |           #
#           |                     SYNC [MSG_TYPE = 0x00,DN = DN1, |           #
#           |                                    Timestamp = Ta]  |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                     SYNC [MSG_TYPE = 0x00,DN = DN1, |           #
#           |                                    Timestamp = Tb]  |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                     SYNC [MSG_TYPE = 0x00,DN = DN1, |           #
#           |                                    Timestamp = Tc]  |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |              <Check ((Tb - Ta) + (Tc - Tb))/2 = 2s> |           #
#           |                                                     |           #
#           |                       <Configure sync interval = 0> |           #
#           |                                                     |           #
#           |                     SYNC [MSG_TYPE = 0x00,DN = DN1, |           #
#           |                                    Timestamp = Ta]  |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                     SYNC [MSG_TYPE = 0x00,DN = DN1, |           #
#           |                                    Timestamp = Tb]  |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                     SYNC [MSG_TYPE = 0x00,DN = DN1, |           #
#           |                                    Timestamp = Tc]  |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |              <Check ((Tb - Ta) + (Tc - Tb))/2 = 1s> |           #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     MSG_TYPE  = Message Type                                                #
#     DN        = Domain Number                                               #
#     PRT1      = Priority1                                                   #
#                                                                             #
# NOTE :                                                                      #
#                                                                             #
#  1. This objective is verified using the High Accuracy Delay Request-       #
#     Response Default PTP Profile                                            #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.ingressLatency,           #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters:                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type    = 0x0B                                     #
#                  Domain Number   = DN1                                      #
#                  Priority1       = Z                                        #
#                                                                             #
# Step 4 : Send ANNOUNCE message on port T1 with following parameters:        #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type    = 0x0B                                     #
#                  Domain Number   = DN1                                      #
#                  Priority1       = Z+1                                      #
#                                                                             #
# Step 5 : Check whether that the DUT transmits three consecutive SYNC        #
#          messages on the port P1 with following parameters and store        #
#          timestamps Ta, Tb and Tc for messages respectively.                #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x00                                  #
#                  Domain Number      = DN1                                   #
#                                                                             #
# Step 6 : Check whether ((Tb - Ta) + (Tc - Tb))/2 = 1s                       #
#                                                                             #
# Step 7 :Configure Sync Interval as -1 on Port P1 in DUT.                    #
#                                                                             #
# Step 8 : Check whether that the DUT transmits three consecutive SYNC        #
#          messages on the port P1 with following parameters and store        #
#          timestamps Ta, Tb and Tc for messages respectively.                #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x00                                  #
#                  Domain Number      = DN1                                   #
#                                                                             #
# Step 9 : Check whether ((Tb - Ta) + (Tc - Tb))/2 = 0.5s                     #
#                                                                             #
# Step 10: Configure Sync Interval as 1 on Port P1 in DUT.                    #
#                                                                             #
# Step 11: Check whether that the DUT transmits three consecutive SYNC        #
#          messages on the port P1 with following parameters and store        #
#          timestamps Ta, Tb and Tc for messages respectively.                #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x00                                  #
#                  Domain Number      = DN1                                   #
#                                                                             #
# Step 12: Check whether ((Tb - Ta) + (Tc - Tb))/2 = 2s                       #
#                                                                             #
# Step 13: Configure Sync Interval as 0 on Port P1 in DUT.                    #
#                                                                             #
# Step 14:Check whether that the DUT transmits three consecutive SYNC         #
#         messages on the port P1 with following parameters and store         #
#         timestamps Ta, Tb and Tc for messages respectively.                 #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x00                                  #
#                  Domain Number      = DN1                                   #
#                                                                             #
# Step 15:Verify whether ((Tb - Ta) + (Tc - Tb))/2 = 1s                       #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          May/2018      CERN          Initial                            #
# 1.1          Jun/2018      CERN          Fixed dut_port in                  #
#                                          dut_reset_sync_interval            #
# 1.2          Jul/2018      CERN          Updated tolerance to 30% in        #
#                                          interval measurement               #
# 1.3          Sep/2018      CERN          a) Removed destination MAC and IP  #
#                                             in send functions.              #
#                                          b) Removed timeout in recv         #
#                                             functions.                      #
#                                          c) Moved timeout calculation to    #
#                                             PTP-HA global file.             #
#                                          e) Capture buffer is cleared after #
#                                             every configuration step.       #
#                                                                             #
###############################################################################
# Copyright (c) 2018 CERN                                                     #
###############################################################################

global dut_log_msg
global tee_log_msg
global session_id

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def "To verify that a PTP enabled device transmits Sync\
            messages at configured logSyncInterval (allowable\
            range: -1 to +1)."

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set dut_port_1                     $::dut_port_num_1
set vlan_id                        $::ptp_ha_vlan_id

set dut_ip                         $::dut_test_port_ip
set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT

set domain                         $::DEFAULT_DOMAIN
set gmid                           $::GRANDMASTER_ID
set infinity                       $::INFINITY

set sequence_id_ann                1
set sequence_id_l1sync             43692

########################### END - INITIALIZATION ##############################

ptp_ha::display_port_details

ptp_ha::dut_Initialization

proc cleanup {} {

    ptp_ha::dut_cleanup_setup_001

    ptp_ha::dut_reset_sync_interval \
             -port_num     $::dut_port_num_1

}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.ingressLatency,           #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
###############################################################################

STEP 1

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP1)"

if {![ptp_ha::dut_configure_setup_001]} {

    LOG -msg "$dut_log_msg(INIT_SETUP1_F)"

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP1_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
###############################################################################

STEP 2

LOG -level 0 -msg "$tee_log_msg(INIT)"

ptp_ha::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

    if {![PTP_HA_UNICAST_PRE_REQUISITE_CONFIGURATION\
                        -port_num       $tee_port_1\
                        -tee_mac        $tee_mac\
                        -tee_ip         $tee_ip\
                        -dut_ip         $dut_ip\
                        -vlan_id        $vlan_id\
                        -dut_mac        tee_dest_mac\
                        -timeout        $arp_timeout]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
                           -tc_def  $tc_def

        return 0
    }

}

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_1\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def  $tc_def

    return 0
}

set filter_id [lindex $id1 end]

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

# (Part 1)#

###############################################################################
# Step 3 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters:                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type    = 0x0B                                     #
#                  Domain Number   = DN1                                      #
#                  Priority1       = Z                                        #
###############################################################################

STEP 3

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
           -port_num                $tee_port_1\
           -domain_number           $domain\
           -filter_id               $filter_id\
           -recvd_gm_priority1      priority1]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                        -tc_def  $tc_def

    return 0
}

###############################################################################
#                                                                             #
# Step 4 : Send ANNOUNCE message on port T1 with following parameters:        #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type    = 0x0B                                     #
#                  Domain Number   = DN1                                      #
#                  Priority1       = Z+1                                      #
###############################################################################

STEP 4

set priority [expr $priority1 + 1]

LOG -level 0 -msg "$tee_log_msg(ANN_TX_T1)"

if {![ptp_ha::send_announce \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -gm_priority1            $priority\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -sequence_id             $sequence_id_ann\
           -count                   $infinity\
           -gmid                    $gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANN_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 5 : Check whether that the DUT transmits three consecutive SYNC        #
#          messages on the port P1 with following parameters and store        #
#          timestamps Ta, Tb and Tc for messages respectively.                #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x00                                  #
#                  Domain Number      = DN1                                   #
###############################################################################

STEP 5a

LOG -level 0 -msg "$tee_log_msg(SYNC_RX_P1_O)"

if {![ptp_ha::recv_sync \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -reset_count       $::reset_count_off\
             -rx_timestamp_ns   Ta]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}


STEP 5b

LOG -level 0 -msg "$tee_log_msg(SYNC_RX_P1_O)"

if {![ptp_ha::recv_sync \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -reset_count       $::reset_count_off\
             -rx_timestamp_ns   Tb]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}


STEP 5c

LOG -level 0 -msg "$tee_log_msg(SYNC_RX_P1_O)"

if {![ptp_ha::recv_sync \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -reset_count       $::reset_count_off\
             -rx_timestamp_ns   Tc]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}


###############################################################################
# Step 6 : Check whether ((Tb - Ta) + (Tc - Tb))/2 = 1s                       #
#                                                                             #
###############################################################################

STEP 6


set b [ expr (((($Tb-$Ta) + ($Tc-$Tb))/2)/1000000000.0) ]

set b [format %.1f $b]


LOG -level 0 -msg "$dut_log_msg(CHECK_EXPR)"
LOG -level 0 -msg "\nSync Interval:"
LOG -level 0 -msg "               Expected = 1 sec (Allowable range: 0.7 s - 1.3 s)"
LOG -level 0 -msg "               Observed = $b secs"

if {!($b >= 0.7 && $b <= 1.3)} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$dut_log_msg(CHECK_EXPR_F)"\
                        -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 7 :Configure Sync Interval as -1 on Port P1 in DUT.                    #
###############################################################################

STEP 7

LOG -level 0 -msg "$dut_log_msg(CONFIG_SYNC_INTVL_1)"

  if {![ptp_ha::dut_set_sync_interval \
                -port_num                $dut_port_1\
                -sync_interval           -1]} {

      TEE_CLEANUP

      TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_SYNC_INTVL_F)"\
                         -tc_def $tc_def

      return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
#                                                                             #
# Step 8 : Check whether that the DUT transmits three consecutive SYNC        #
#          messages on the port P1 with following parameters and store        #
#          timestamps Ta, Tb and Tc for messages respectively.                #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x00                                  #
#                  Domain Number      = DN1                                   #
###############################################################################

STEP 8a

LOG -level 0 -msg "$tee_log_msg(SYNC_RX_P1_O)"

if {![ptp_ha::recv_sync \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -reset_count       $::reset_count_off\
             -rx_timestamp_ns   Ta]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}


STEP 8b

LOG -level 0 -msg "$tee_log_msg(SYNC_RX_P1_O)"

if {![ptp_ha::recv_sync \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -reset_count       $::reset_count_off\
             -rx_timestamp_ns   Tb]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}


STEP 8c

LOG -level 0 -msg "$tee_log_msg(SYNC_RX_P1_O)"

if {![ptp_ha::recv_sync \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -reset_count       $::reset_count_off\
             -rx_timestamp_ns   Tc]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}


###############################################################################
#                                                                             #
# Step 9 : Check whether ((Tb - Ta) + (Tc - Tb))/2 = 0.5s                     #
###############################################################################

STEP 9


set b [ expr (((($Tb-$Ta) + ($Tc-$Tb))/2)/1000000000.0) ]

set b [format %.1f $b]


LOG -level 0 -msg "$dut_log_msg(CHECK_EXPR)"
LOG -level 0 -msg "\nSync Interval:"
LOG -level 0 -msg "               Expected = 0.5 sec (Allowable range: 0.35 s - 0.65 s)"
LOG -level 0 -msg "               Observed = $b secs"

if {!($b >= 0.35 && $b <= 0.65)} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$dut_log_msg(CHECK_EXPR_F)"\
                        -tc_def  $tc_def
    return 0
}

###############################################################################
#                                                                             #
# Step 10: Configure Sync Interval as 1 on Port P1 in DUT.                    #
###############################################################################

STEP 10

LOG -level 0 -msg "$dut_log_msg(CONFIG_SYNC_INTVL_1_P1)"

  if {![ptp_ha::dut_set_sync_interval \
                -port_num                $dut_port_1\
                -sync_interval           1]} {

      TEE_CLEANUP

      TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_SYNC_INTVL_F)"\
                         -tc_def  $tc_def

      return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
#                                                                             #
# Step 11: Check whether that the DUT transmits three consecutive SYNC        #
#          messages on the port P1 with following parameters and store        #
#          timestamps Ta, Tb and Tc for messages respectively.                #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x00                                  #
#                  Domain Number      = DN1                                   #
###############################################################################

STEP 11a

LOG -level 0 -msg "$tee_log_msg(SYNC_RX_P1_O)"

if {![ptp_ha::recv_sync \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -reset_count       $::reset_count_off\
             -rx_timestamp_ns   Ta]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}


STEP 11b

LOG -level 0 -msg "$tee_log_msg(SYNC_RX_P1_O)"

if {![ptp_ha::recv_sync \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -reset_count       $::reset_count_off\
             -rx_timestamp_ns   Tb]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}


STEP 11c

LOG -level 0 -msg "$tee_log_msg(SYNC_RX_P1_O)"

if {![ptp_ha::recv_sync \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -reset_count       $::reset_count_off\
             -rx_timestamp_ns   Tc]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}


###############################################################################
#                                                                             #
# Step 12: Check whether ((Tb - Ta) + (Tc - Tb))/2 = 2s                       #
###############################################################################

STEP 12


set b [ expr (((($Tb-$Ta) + ($Tc-$Tb))/2)/1000000000.0) ]
set b [format %.1f $b]


LOG -level 0 -msg "$dut_log_msg(CHECK_EXPR)"
LOG -level 0 -msg "\nSync Interval:"
LOG -level 0 -msg "               Expected = 2 sec (Allowable range: 1.4 s - 2.6 s)"
LOG -level 0 -msg "               Observed = $b secs"

if {!($b >= 1.4 && $b <= 2.6)} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$dut_log_msg(CHECK_EXPR_F)"\
                        -tc_def  $tc_def
    return 0
}

###############################################################################
#                                                                             #
# Step 13: Configure Sync Interval as 0 on Port P1 in DUT.                    #
###############################################################################

STEP 13

LOG -level 0 -msg "$dut_log_msg(CONFIG_SYNC_INTVL_0)"

  if {![ptp_ha::dut_set_sync_interval \
                -port_num                $dut_port_1\
                -sync_interval           0]} {

      TEE_CLEANUP

      TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_SYNC_INTVL_F)"\
                         -tc_def $tc_def

      return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
#                                                                             #
# Step 14:Check whether that the DUT transmits three consecutive SYNC         #
#         messages on the port P1 with following parameters and store         #
#         timestamps Ta, Tb and Tc for messages respectively.                 #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x00                                  #
#                  Domain Number      = DN1                                   #
###############################################################################

STEP 14a

LOG -level 0 -msg "$tee_log_msg(SYNC_RX_P1_O)"

if {![ptp_ha::recv_sync \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -reset_count       $::reset_count_off\
             -rx_timestamp_ns   Ta]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}


STEP 14b

LOG -level 0 -msg "$tee_log_msg(SYNC_RX_P1_O)"

if {![ptp_ha::recv_sync \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -reset_count       $::reset_count_off\
             -rx_timestamp_ns   Tb]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}


STEP 14c

LOG -level 0 -msg "$tee_log_msg(SYNC_RX_P1_O)"

if {![ptp_ha::recv_sync \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -reset_count       $::reset_count_off\
             -rx_timestamp_ns   Tc]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}


###############################################################################
# Step 15:Verify whether ((Tb - Ta) + (Tc - Tb))/2 = 1s                       #
#                                                                             #
###############################################################################

STEP 15


set b [ expr (((($Tb-$Ta) + ($Tc-$Tb))/2)/1000000000.0) ]
set b [format %.1f $b]

LOG -level 0 -msg "$dut_log_msg(CHECK_EXPR)"
LOG -level 0 -msg "\nSync Interval:"
LOG -level 0 -msg "               Expected = 1 sec (Allowable range: 0.7 s - 1.3 s)"
LOG -level 0 -msg "               Observed = $b secs"

if {!($b >= 0.7 && $b <= 1.3)} {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL  -reason "PTP enabled device doe not transmits Sync\
                                messages at configured logSyncInterval (allowable\
                                range: -1 to +1)"\
                        -tc_def $tc_def
    return 0
}

TEE_CLEANUP

TC_CLEAN_AND_PASS  -reason "PTP enabled device transmits Sync\
                             messages at configured logSyncInterval (allowable\
                             range: -1 to +1)"\
                    -tc_def  $tc_def
return 1

############################### END OF TEST CASE ##############################
