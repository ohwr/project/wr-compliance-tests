#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_ptp-ha_peg_014                                  #
# Test Case Version : 1.0                                                     #
# Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    #
# Module Name       : PTP ExternalPortConfiguration Group (PEG)               #
#                                                                             #
# Title             : slaveOnly is FALSE when defaultDS.                      #
#                     externalPortConfigurationEnabled is TRUE.               #
#                                                                             #
# Purpose           : To verify that an Ordinary Clock does not allow to set  #
#                     slaveOnly to TRUE when defaultDS.                       #
#                     externalPortConfigurationEnabled is set to TRUE.        #
#                                                                             #
# Reference         : P1588/D1.4, July 2018 Clause 17.6.5.3 Page 361          #
#                                                                             #
# Conformance Type  : SHALL                                                   #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 008                                                     #
# Test Topology     : 001                                                     #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                          <Enable PTP with OC clock> |           #
#           |                    <Clock mode - one-step/two-step> |           #
#           |                     <Configure logAnnounceInterval, | P1        #
#           |                             announceReceiptTimeout> |           #
#           |                         <Configure logSyncInterval> | P1        #
#           |                  <Configure logMinDelayReqInterval> | P1        #
#           |                <Configure Priority1 (X), Priority2> | P1        #
#           |                                     <Enable L1SYNC> | P1        #
#           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        #
#           |                     <Disable L1SynOptParams option> | P1        #
#           |           <Enable asymmetryCorrectionPortDS.enable> | P1        #
#           | <Configure timestampCorrectionPortDS.egressLatency, | P1        #
#           |           timestampCorrectionPortDS.ingressLatency, |           #
#           |        asymmetryCorrectionPortDS.constantAsymmetry, |           #
#           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           #
#           |                                                     |           #
#           |                                             <Enable |           #
#           |         defaultDS.externalPortConfigurationEnabled> |           #
#           |                                                     |           #
#           |             !<Configure defaultDS.slaveOnly = TRUE> |           #
#           |                                                     |           #
#           |                 <Check defaultDS.slaveOnly = FALSE> |           #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     MSG_TYPE  = Message Type                                                #
#     DN        = Domain Number                                               #
#     OC        = Ordinary Clock                                              #
#                                                                             #
# NOTE :                                                                      #
#                                                                             #
#  1. This objective is verified using the High Accuracy Delay Request-       #
#     Response Default PTP Profile                                            #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Ordinary clock.            #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1 (X), Priority2,             #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.egressLatency,            #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Enable defaultDS.externalPortConfigurationEnabled on DUT.          #
#                                                                             #
# Step 4 : Verify that DUT does not allow to configure defaultDS.slaveOnly    #
#          = TRUE.                                                            #
#                                                                             #
# Step 5 : If DUT allows to configure in Step 4, verify that                  #
#          defaultDS.slaveOnly is set to FALSE.                               #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          Oct/2018      CERN          Initial                            #
#                                                                             #
###############################################################################
# Copyright (c) 2018 CERN                                                     #
###############################################################################

global dut_log_msg
global tee_log_msg
global session_id

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def  "To verify that an Ordinary Clock does not allow to set\
             slaveOnly to TRUE when defaultDS.\
             externalPortConfigurationEnabled is set to TRUE."

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set dut_port_1                     $::dut_port_num_1
set vlan_id                        $::ptp_ha_vlan_id
set dut_ip                         $::dut_test_port_ip
set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT
set domain                         $::DEFAULT_DOMAIN
set infinity                       $::INFINITY
set gmid                           $::GRANDMASTER_ID

set sequence_id_ann                1

set value_true                    1
set value_false                   0

set DUT_BOOLEAN(0)  FALSE
set DUT_BOOLEAN(1)  TRUE

########################### END - INITIALIZATION ##############################

ptp_ha::display_port_details

ptp_ha::dut_Initialization

proc cleanup {} {

    ptp_ha::dut_cleanup_setup_008

}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

### (Initial Part) ###
###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Ordinary clock.            #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1 (X), Priority2,             #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.egressLatency,            #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
###############################################################################

STEP 1

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP8)"

if {![ptp_ha::dut_configure_setup_008]} {

    LOG -msg "$dut_log_msg(INIT_SETUP8_F)"

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP8_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
###############################################################################

STEP 2

LOG -level 0 -msg "$tee_log_msg(INIT)"

ptp_ha::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

    if {![PTP_HA_UNICAST_PRE_REQUISITE_CONFIGURATION\
                        -port_num       $tee_port_1\
                        -tee_mac        $tee_mac\
                        -tee_ip         $tee_ip\
                        -dut_ip         $dut_ip\
                        -vlan_id        $vlan_id\
                        -dut_mac        tee_dest_mac\
                        -timeout        $arp_timeout]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
                           -tc_def  $tc_def

        return 0
    }

}

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_1\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def  $tc_def

    return 0
}

set filter_id [lindex $id1 end]

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1


###############################################################################
# Step 3 : Enable defaultDS.externalPortConfigurationEnabled on DUT.          #
#                                                                             #
###############################################################################

STEP 3

LOG -level 0 -msg "$dut_log_msg(ENABLE_EXTERNAL_PORT_CONFIG)"

if {![ptp_ha::dut_external_port_config_enable\
              -port_num       $dut_port_1]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(ENABLE_EXTERNAL_PORT_CONFIG_F)"\
                       -tc_def  $tc_def

    return 0

}

###############################################################################
# Step 4 : Verify that DUT does not allow to configure defaultDS.slaveOnly    #
#          = TRUE.                                                            #
#                                                                             #
###############################################################################

STEP 4

LOG -level 0 -msg "$dut_log_msg(DEFAULTDS.SLAVEONLY_NOT_V)"

if {[ptp_ha::dut_slaveonly_enable\
              -port_num       $dut_port_1]} {

    set step4_pass 1

} else {

    set step4_pass 0

}

###############################################################################
# Step 5 : If DUT allows to configure in Step 4, verify that                  #
#          defaultDS.slaveOnly is set to FALSE.                               #
#                                                                             #
###############################################################################

STEP 5

if {$step4_pass} {

    LOG -level 0 -msg "$dut_log_msg(DEFAULTDS.SLAVEONLY_FALSE_V)"

    sleep $::dut_config_delay

    if {![ptp_ha::dut_check_slave_only\
              -port_num       $dut_port_1\
              -value          [set DUT_BOOLEAN($value_false)]]} {

        TEE_CLEANUP

        TC_CLEAN_AND_FAIL -reason "defaultDS.slaveOnly is not set to FALSE."\
                          -tc_def  $tc_def

        return 0
    }

} else {

    LOG -level 0 -msg "$tee_log_msg(SKIP_STEP)"

}

TEE_CLEANUP

TC_CLEAN_AND_PASS -reason "Ordinary Clock does not allow to set\
                           slaveOnly to TRUE when defaultDS.\
                           externalPortConfigurationEnabled is set to TRUE."\
                  -tc_def  $tc_def

return 1

################################ END OF TESTCASE ##############################

