#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_ptp-ha_mhg_002                                  #
# Test case Version : 1.4                                                     #
# Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    #
# Module Name       : PTP-HA Message Handling Group (MHG)                     #
#                                                                             #
# Title             : Non-forwarding of L1Sync messages with                  #
#                     non-forwardable address on transport over               #
#                     IEEE 802.3/Ethernet                                     #
#                                                                             #
# Purpose           : To verify that a PTP enabled device does not forward PTP#
#                     signaling message with L1 Sync TLV destined with        #
#                     non-forwardable address (01:80:C2:00:00:0E) on transport#
#                     over IEEE 802.3/Ethernet.                               #
#                                                                             #
# Reference         : P1588/D1.3, February 2018 V3.01 Clause O.6.1 Page 447   #
#                                                                             #
# Conformance Type  : SHALL                                                   #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 003                                                     #
# Test Topology     : 004                                                     #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                                        <Enable PTP> | P2        #
#           |                          <Enable PTP with BC clock> |           #
#           |                    <Clock mode - one-step/two-step> |           #
#           |                     <Configure logAnnounceInterval, | P1        #
#           |                             announceReceiptTimeout> |           #
#           |                         <Configure logSyncInterval> | P1        #
#           |                  <Configure logMinDelayReqInterval> | P1        #
#           |                    <Configure Priority1, Priority2> | P1        #
#           |                                     <Enable L1SYNC> | P1        #
#           |                                     <Enable L1SYNC> | P2        #
#           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        #
#           |                     <Disable L1SynOptParams option> | P1        #
#           |           <Enable asymmetryCorrectionPortDS.enable> | P1        #
#           | <Configure timestampCorrectionPortDS.egressLatency, | P1        #
#           |           timestampCorrectionPortDS.ingressLatency, |           #
#           |        asymmetryCorrectionPortDS.constantAsymmetry, |           #
#           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                                           DN = DN1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                                           DN = DN1] |           #
#        T2 |-------------------------------------------------<<--| P2        #
#           |                                                     |           #
#           |PTP SIGNALING with L1 Sync TLV [MSG_TYPE = 0x0C,     |           #
#           |DN = DN1, DEST_MAC = 01:80:C2:00:00:0E]              |           #
#        T1 |------>>---------------------------------------------| P1        #
#           |                                                     |           #
#           |     PTP SIGNALING with L1 Sync TLV [MSG_TYPE = 0x0C,|           #
#           |            DN = DN1, DEST_MAC = Y,SRC_MAC!=TEE_MAC1]|           #
#        T2 |                xxx------------------------------<<--| P2        #
#           |                                                     |           #
#           |                                    <Disable L1SYNC> | P2        #
#           |                                                     |           #
#           |PTP SIGNALLING with L1 Sync TLV [MSG_TYPE = 0x0C,    |           #
#           |DN = DN1, DEST_MAC = 01:80:C2:00:00:0E]              |           #
#        T1 |------>>---------------------------------------------| P1        #
#           |                                                     |           #
#           |    PTP SIGNALLING with L1 Sync TLV [MSG_TYPE = 0x0C,|           #
#           |                                           DN = DN1] |           #
#        T2 |                xxx------------------------------<<--| P2        #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     MSG_TYPE  = Message Type                                                #
#     DN        = Domain Number                                               #
#     BC        = Boundary Clock                                              #
#     OC        = Ordinary Clock                                              #
#     DEST_MAC  = Destination MAC                                             #
#     TEE_MAC1  = TEE MAC of port1                                            #
#                                                                             #
# NOTE :                                                                      #
#                                                                             #
#  1. This objective is verified using the High Accuracy Delay Request-       #
#     Response Default PTP Profile                                            #
#                                                                             #
#  2. This objective is applicable only for device implementation supports    #
#     transport over IEEE 802.3/Ethernet                                      #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's ports P1 and P2.                                      #
#    ii.   Enable PTP on ports P1 and P2.                                     #
#   iii.   Enable PTP globally with device type as Boundary clock.            #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's ports P1 and P2.                            #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.ingressLatency,           #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add ports T1 and T2 at TEE.                                        #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
#                                                                             #
# Step 4 : Observe that DUT transmits ANNOUNCE message on the port P2 with    #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
#                                                                             #
# Step 5 : Send periodic L1SYNC Signaling message on port T1 with             #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type    = 0x0C                                     #
#                  Domain Number   = DN1                                      #
#                  Destination MAC = 01:80:C2:00:00:0E                        #
#                                                                             #
# Step 6 : Verify that DUT transmits only its own L1 Sync Signaling messages  #
#          and does not re-transmit L1 Sync Signaling messages from TEE port  #
#          T1.Observe that DUT does not transmits L1 Sync Signaling messages  #
#          with non-forwardable address and source mac address of the TEE port#
#          MAC(T2) on Port P2.                                                #
#                                                                             #
# Step 7 : Disable L1SYNC on port P2.                                         #
#                                                                             #
# Step 8 : Send periodic L1SYNC Signaling message on port T1 with             #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0C                                       #
#                  Domain Number = DN1                                        #
#                  Destination MAC = 01:80:C2:00:00:0E                        #
#                                                                             #
# Step 9 : Verify that DUT does not transmit any L1 Sync Signaling messages on#
#          port P2.                                                           #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          May/2018      CERN          Initial                            #
# 1.1          Jun/2018      CERN          Updated Step 4 and 6 to receive on #
#                                          Port 2                             # 
# 1.2          Jul/2018      CERN          Added two steps to receive announce#
#                                          msg in port 1 and 2.               # 
# 1.3          Sep/2018      CERN          a) Removed destination MAC and IP  #
#                                             in send functions.              #
#                                          b) Removed timeout in recv         #
#                                             functions.                      #
#                                          c) Moved timeout calculation to    #
#                                             PTP-HA global file.             #
# 1.4          Oct/2018      CERN          Modified to gracefully abort the   #
#                                          test if transport type is not      #
#                                          IEEE 802.3/Ethernet.               #
#                                                                             #
###############################################################################
# Copyright (c) 2018 CERN                                                     #
###############################################################################

global dut_log_msg
global tee_log_msg
global session_id

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def "To verify that a PTP enabled does not forward PTP\
            signaling message with L1 Sync TLV destined with\
            non-forwardable address (01:80:C2:00:00:0E) on transport\
            over IEEE 802.3/Ethernet."

########################### START - INITIALIZATION ############################


set tee_port_1                     $::tee_port_num_1
set tee_port_2                     $::tee_port_num_2
set dut_port_1                     $::dut_port_num_1
set dut_port_2                     $::dut_port_num_2
set vlan_id_1                      $::ptp_ha_vlan_id
set vlan_id_2                      $::ptp_ha_vlan_id_2
set port_list                      "$::tee_port_num_1 $::tee_port_num_2" 

set dut_ip_1                       $::dut_test_port_ip

set dut_ip_2                       $::dut_test_port_ip_2

set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT

set domain                         $::DEFAULT_DOMAIN
set infinity                       $::INFINITY
set gmid                           $::GRANDMASTER_ID
set tlv_type(l1sync)               $::TLV_TYPE(L1SYNC)

########################### END - INITIALIZATION ##############################

if { $::ptp_trans_type == "UDP/IPv4" } {

       ERRLOG -msg "$tee_log_msg(IEEE)" 

       TC_ABORT -reason "$tee_log_msg(IEEE)"\
                -tc_def  $tc_def

  return 0

}

ptp_ha::display_port_details\
                -port_list  $port_list 

ptp_ha::dut_Initialization

proc cleanup {} {

    ptp_ha::dut_cleanup_setup_003

}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

### (Initial Part) ###

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's ports P1 and P2.                                      #
#    ii.   Enable PTP on ports P1 and P2.                                     #
#   iii.   Enable PTP globally with device type as Boundary clock.            #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's ports P1 and P2.                            #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.ingressLatency,           #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
###############################################################################

STEP 1

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP3)"

if {![ptp_ha::dut_configure_setup_003]} {

    LOG -msg "$dut_log_msg(INIT_SETUP3_F)"

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP3_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 2 : Initialization of TEE                                              #
#     i.   Add ports T1 and T2 at TEE.                                        #
###############################################################################

STEP 2

LOG -level 0 -msg "$tee_log_msg(INIT)"

ptp_ha::Initialization\
        -port_list   $port_list

set tee_mac_1       $::TEE_MAC
set tee_ip_1        $::tee_test_port_ip

set tee_mac_2       $::TEE_MAC_2
set tee_ip_2        $::tee_test_port_ip_2

for {set i 1} {$i<=2} {incr i} {

    if {$ptp_comm_model == "Unicast"} {

        if {![PTP_HA_UNICAST_PRE_REQUISITE_CONFIGURATION\
                -port_num       [set tee_port_$i]\
                -tee_mac        [set tee_mac_$i]\
                -tee_ip         [set tee_ip_$i]\
                -dut_ip         [set dut_ip_$i]\
                -vlan_id        [set vlan_id_$i]\
                -dut_mac        tee_dest_mac_$i\
                -timeout        $arp_timeout]} {

                TEE_CLEANUP
                TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
                                   -tc_def $tc_def

                return 0
        }

    }

}

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_1\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id_1\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def $tc_def

    return 0
}

set filter_id1 [lindex $id1 end]

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_2\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id_2\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def $tc_def

    return 0
}


set filter_id2 [lindex $id1 end]

if { $::ptp_ha_vlan_encap != "true" } {
    set vlan_id_2 "dont_care"
}

if { $::ptp_trans_type != "UDP/IPv4" } {
    set tee_ip_1_rx  "dont_care"
} else {
    set tee_ip_1_rx  $tee_ip_1
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_2

###############################################################################
# Step 3 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
###############################################################################

STEP 3

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
           -port_num                $tee_port_1\
           -domain_number           $domain\
           -filter_id               $filter_id1]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                        -tc_def $tc_def

    return 0
}

###############################################################################
# Step 4 : Observe that DUT transmits ANNOUNCE message on the port P2 with    #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
###############################################################################

STEP 4

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P2_O)"

if {![ptp_ha::recv_announce \
           -port_num                $tee_port_2\
           -vlan_id                 $vlan_id_2\
           -domain_number           $domain\
           -filter_id               $filter_id2]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANNOUNCE_RX_P2_F)"\
                        -tc_def $tc_def

    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_2

###############################################################################
# Step 5 : Send periodic L1SYNC Signaling message on port T1 with             #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0C                                       #
#                  Domain Number = DN1                                        #
#                  Destination MAC = 01:80:C2:00:00:0E                        #
###############################################################################

STEP 5

LOG -level 0 -msg   "$tee_log_msg(L1SYNC_TX_T1)"

if {![ptp_ha::send_signal \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac_1\
           -src_ip                  $tee_ip_1\
           -count                   $infinity\
           -domain_number           $domain]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(L1SYNC_TX_T1_F)"\
                        -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 6 : Verify that DUT transmits only its own L1 Sync Signaling messages  #
#          and does not re-transmit L1 Sync Signaling messages from TEE port  #
#          T1.Observe that DUT does not transmits L1 Sync Signaling messages  #
#          with non-forwardable address and source ip is not equal to TEE port#
#          IP (T1) on Port P2.                                                #
###############################################################################

STEP 6

LOG -level 0 -msg "$tee_log_msg(L1SYNC_NRX_P2_O)"

if {[ptp_ha::recv_signal \
           -port_num                $tee_port_2\
           -vlan_id                 $vlan_id_2\
           -src_mac                 $tee_mac_1\
           -src_ip                  $tee_ip_1_rx\
           -domain_number           $domain\
           -tlv_type                $tlv_type(l1sync)\
           -timeout                 $::ptp_ha_l1sync_timeout\
           -filter_id               $filter_id2]} {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL  -reason   "PTP enabled device has forwarded PTP\
                                 signaling message with L1 Sync TLV destined with\
                                 non-forwardable address(01:80:C2:00:00:0E) on transport\
                                 over IEEE 802.3/Ethernet."\
                        -tc_def  $tc_def

    return 0

}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_2

###############################################################################
# Step 7 : Disable L1SYNC on port P2.                                         #
###############################################################################

STEP 7

LOG -level 0 -msg "$dut_log_msg(DISABLE_L1SYNC_P2)"

if {![ptp_ha::dut_l1sync_disable\
              -port_num     $dut_port_2]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(DISABLE_L1SYNC_P2_F)"
                       -tc_def  $tc_def

    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_2

###############################################################################
#                                                                             #
# Step 8 : Send periodic L1SYNC Signaling message on port T1 with             #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type    = 0x0C                                     #
#                  Domain Number   = DN1                                      #
#                  Destination MAC = 01:80:C2:00:00:0E                        #
###############################################################################

STEP 8

LOG -level 0 -msg   "$tee_log_msg(L1SYNC_TX_T1)"

if {![ptp_ha::send_signal \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac_1\
           -src_ip                  $tee_ip_1\
           -count                   $infinity\
           -domain_number           $domain]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(L1SYNC_TX_T1_F)"\
                        -tc_def  $tc_def

    return 0
}

###############################################################################
#                                                                             #
# Step 9 : Verify that DUT does not transmit any L1 Sync Signaling messages on#
#          port P2.                                                           #
#                                                                             #
###############################################################################

STEP 9

LOG -level 0 -msg "$tee_log_msg(L1SYNC_NRX_P2_V)"

if {[ptp_ha::recv_signal \
           -port_num                $tee_port_2\
           -vlan_id                 $vlan_id_2\
           -domain_number           $domain\
           -tlv_type                $tlv_type(l1sync)\
           -timeout                 $::ptp_ha_l1sync_timeout\
           -filter_id               $filter_id2]} {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL  -reason   "PTP enabled device has forwarded PTP\
                                 signaling message with L1 Sync TLV destined with\
                                 non-forwardable address (01:80:C2:00:00:0E) on transport\
                                 over IEEE 802.3/Ethernet."\
                        -tc_def  $tc_def

    return 0
}

TEE_CLEANUP

TC_CLEAN_AND_PASS -reason  "PTP enabled device has not forwarded PTP\
                            signaling message with L1 Sync TLV destined with\
                            non-forwardable address (01:80:C2:00:00:0E) on transport\
                            over IEEE 802.3/Ethernet."\
                  -tc_def   $tc_def

return 1
############################ END OF TEST CASE ###################################
