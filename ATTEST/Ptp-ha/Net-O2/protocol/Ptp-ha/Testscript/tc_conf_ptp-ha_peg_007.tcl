#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_ptp-ha_peg_007                                  #
# Test Case Version : 1.3                                                     #
# Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    #
# Module Name       : PTP ExternalPortConfiguration Group (PEG)               #
#                                                                             #
# Title             : External Configuration: portDS.portState is FAULTY      #
#                                                                             #
# Purpose           : To verify that a PTP enabled device sets                #
#                     portDS.portState to FAULTY state when                   #
#                     defaultDS.externalPortConfigurationEnabled is set to    #
#                     TRUE and externalPortConfigurationPortDS.desiredState is#
#                     set to FAULTY.                                          #
#                                                                             #
# Reference         : P1588/D1.3, February 2018 V3.01 Clause 17.6.3.2 Page 354#
#                                                                             #
# Conformance Type  : SHALL                                                   #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 006                                                     #
# Test Topology     : 001                                                     #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode - one-step/two-step> |           #
#           |                     <Configure logAnnounceInterval, | P1        #
#           |                             announceReceiptTimeout> |           #
#           |                         <Configure logSyncInterval> | P1        #
#           |                  <Configure logMinDelayReqInterval> | P1        #
#           |                <Configure Priority1 (X), Priority2> | P1        #
#           |                                     <Enable L1SYNC> | P1        #
#           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        #
#           |                     <Disable L1SynOptParams option> | P1        #
#           |           <Enable asymmetryCorrectionPortDS.enable> | P1        #
#           | <Configure timestampCorrectionPortDS.egressLatency, | P1        #
#           |           timestampCorrectionPortDS.ingressLatency, |           #
#           |        asymmetryCorrectionPortDS.constantAsymmetry, |           #
#           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           #
#           |                                            <Enable  |           #
#           |        <defaultDS.externalPortConfigurationEnabled> |           #
#           |                                                     |           #
#           |                                          <Configure |           #
#           | externalPortConfigurationPortDS.desiredState=FAULTY>|           #
#           |                                                     |           #
#           |                        <Check Port Status = FAULTY> | P1        #
#           |                                                     |           #
#           |                                      <Disable port> | P1        #
#           |                                                     |           #
#           |                                       <Enable port> | P1        #
#           |                                                     |           #
#           | ANNOUNCE [MSG_TYPE = 0x0B, DN = DN1,                |           #
#           |           SEQ_ID = A, PRI1 = X+1]                   |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |          < Wait for 6s to complete BMCA >           |           #
#           |                                                     |           #
#           |                         <Check Port Status = FAULTY>| P1        #
#           |                                                     |           #
#           |                                           <Disable  |           #
#           |        <defaultDS.externalPortConfigurationEnabled> |           #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                     DN = DN1, SEQ_ID = Y, PRI1 = X] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | ANNOUNCE [MSG_TYPE = 0x0B, DN = DN1,                |           #
#           |           SEQ_ID = A, PRI1 = X+1]                   |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |          < Wait for 6s to complete BMCA >           |           #
#           |                                                     |           #
#           |                         <Check port state =  MASTER>| P1        #
#           |                                                     |           #
#                                                                             #
# NOTE :                                                                      #
#                                                                             #
#  1. This objective is verified using the High Accuracy Delay Request-       #
#     Response Default PTP Profile                                            #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1 (X), Priority2,             #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.egressLatency,            #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
#    xi.   Enable defaultDS.externalPortConfigurationEnabled on port P1.      #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Configure externalPortConfigurationPortDS.desiredState as FAULTY.  #
#                                                                             #
# Step 4 : Verify that the port status of P1 in DUT is in FAULTY state.       #
#                                                                             #
# Step 5 : Send periodic ANNOUNCE message with Priority1 value on port T1     #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
#                  Sequence ID   = A                                          #
#                  Priority1     = X+1                                        #
#                                                                             #
# Step 6 : Wait for 6s for completing BMCA.                                   #
#                                                                             #
# Step 7 : Verify that the port status of P1 in DUT is in FAULTY state.       #
#                                                                             #
# Step 8 : Disable defaultDS.externalPortConfigurationEnabled on port P1 in   #
#          DUT.                                                               #
#                                                                             #
# Step 9 : Disable DUT's port P1.                                             #
#                                                                             #
# Step 10: Enable DUT's port P1.                                              #
#                                                                             #
# Step 11: Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
#                  Sequence ID   = Y                                          #
#                  Priority1     = X                                          #
#                                                                             #
# Step 12: Send periodic ANNOUNCE message with Priority1 value on port T1     #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
#                  Sequence ID   = A                                          #
#                  Priority1     = X+1                                        #
#                                                                             #
# Step 13: Wait for 6s for completing BMCA.                                   #
#                                                                             #
# Step 14: Verify that the port status of P1 in DUT is in MASTER state.       #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          May/2018      CERN          Initial                            #
# 1.1          Jun/2018      CERN          Fixed dut_port in                  #
#                                          dut_set_epc_desired_state          #
# 1.2          Jul/2018      CERN          Added Step 6 - Wait for BMCA       #
# 1.3          Sep/2018      CERN          a) Removed destination MAC and IP  #
#                                             in send functions.              #
#                                          b) Removed timeout in recv         #
#                                             functions.                      #
#                                          c) Moved timeout calculation to    #
#                                             PTP-HA global file.             #
#                                                                             #
###############################################################################
# Copyright (c) 2018 CERN                                                     #
###############################################################################

global dut_log_msg
global tee_log_msg
global session_id

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def  "To verify that a PTP enabled device sets\
             portDS.portState to FAULTY state when\
             defaultDS.externalPortConfigurationEnabled is set to\
             TRUE and externalPortConfigurationPortDS.desiredState is\
             set to FAULTY."

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set dut_port_1                     $::dut_port_num_1
set vlan_id                        $::ptp_ha_vlan_id
set dut_ip                         $::dut_test_port_ip
set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT
set domain                         $::DEFAULT_DOMAIN
set infinity                       $::INFINITY
set gmid                           $::GRANDMASTER_ID
set faulty                         $::PTP_PORT_STATE(FAULTY)
set master                         $::PTP_PORT_STATE(MASTER)
set passive                        $::PTP_PORT_STATE(PASSIVE)
set priority1                      $::ptp_dut_default_priority1

set sequence_id_ann                1

########################### END - INITIALIZATION ##############################

ptp_ha::display_port_details

ptp_ha::dut_Initialization

proc cleanup {} {

    ptp_ha::dut_cleanup_setup_006

    ptp_ha::dut_set_epc_desired_state\
             -port_num         $::dut_port_num_1\
             -desired_state    $::PTP_PORT_STATE(PASSIVE)

}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

### (Initial Part) ###

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1 (X), Priority2,             #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.egressLatency,            #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
#    xi.   Enable defaultDS.externalPortConfigurationEnabled on port P1.      #
#                                                                             #
###############################################################################

STEP 1

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP6)"

if {![ptp_ha::dut_configure_setup_006]} {

    LOG -msg "$dut_log_msg(INIT_SETUP6_F)"

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP6_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
###############################################################################

STEP 2

LOG -level 0 -msg "$tee_log_msg(INIT)"

ptp_ha::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

    if {![PTP_HA_UNICAST_PRE_REQUISITE_CONFIGURATION\
                        -port_num       $tee_port_1\
                        -tee_mac        $tee_mac\
                        -tee_ip         $tee_ip\
                        -dut_ip         $dut_ip\
                        -vlan_id        $vlan_id\
                        -dut_mac        tee_dest_mac\
                        -timeout        $arp_timeout]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
                           -tc_def  $tc_def

        return 0
    }

}

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_1\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def  $tc_def

    return 0
}

set filter_id [lindex $id1 end]

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

#(Part 1)#

###############################################################################
#                                                                             #
# Step 3 : Configure externalPortConfigurationPortDS.desiredState as FAULTY.  #
#                                                                             #
###############################################################################

STEP 3

LOG -level 0 -msg "$dut_log_msg(CONFIGURE_FAULTY)"

if {![ptp_ha::dut_set_epc_desired_state\
             -port_num         $dut_port_1\
             -desired_state    $faulty]} {
    TEE_CLEANUP 

    TC_CLEAN_AND_ABORT    -reason "$dut_log_msg(CONFIGURE_FAULTY_F)"\
                          -tc_def  $tc_def
        return 0
}

###############################################################################
# Step 4 : Verify that the port status of P1 in DUT is in FAULTY state.       #
#                                                                             #
###############################################################################

sleep $::ptp_ha_bmca

STEP 4

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_FAULTY_P1_V)"

if {![ptp_ha::dut_check_ptp_port_state \
              -port_num                $dut_port_1\
              -port_state              $faulty] } {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL -reason "$dut_log_msg(CHECK_STATE_FAULTY_P1_F)"\
                      -tc_def  $tc_def
    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 5 : Send periodic ANNOUNCE message with Priority1 value on port T1     #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
#                  Sequence ID   = A                                          #
#                  Priority1     = X+1                                        #
#                                                                             #
###############################################################################

STEP 5

set priority [expr $priority1 + 1]

LOG -level 0 -msg "$tee_log_msg(ANN_TX_T1)"

if {![ptp_ha::send_announce \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -gm_priority1            $priority\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -sequence_id             $sequence_id_ann\
           -count                   $infinity\
           -gmid                    $gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANN_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 6: Wait for 6s for completing BMCA.                                    #
###############################################################################

STEP 6

LOG -level 0 -msg "$dut_log_msg(WAIT_BMCA)"

sleep $::ptp_ha_bmca

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 7 : Verify that the port status of P1 in DUT is in FAULTY state.       #
#                                                                             #
###############################################################################

STEP 7

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_FAULTY_P1_V)"

if {![ptp_ha::dut_check_ptp_port_state \
              -port_num            $dut_port_1\
              -port_state          $faulty] } {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL -reason "$dut_log_msg(CHECK_STATE_FAULTY_P1_F)"\
                      -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 8 : Disable defaultDS.externalPortConfigurationEnabled on port P1 in   #
#          DUT.                                                               #
#                                                                             #
###############################################################################

STEP 8

LOG -level 0 -msg "$dut_log_msg(DISABLE_EXTERNAL_PORT_CONFIG)"

if {![ptp_ha::dut_external_port_config_disable\
             -port_num      $dut_port_1] } {

     TEE_CLEANUP
   
     TC_CLEAN_AND_ABORT  -reason "$dut_log_msg(DISABLE_EXTERNAL_PORT_CONFIG_F)"\
                         -tc_def  $tc_def
     return 0
}

###############################################################################
# Step 9 : Disable DUT's port P1.                                             #
#                                                                             #
###############################################################################

STEP 9

LOG -level 0 -msg "$dut_log_msg(DISABLE_PORT)"

if {![ptp_ha::dut_port_disable \
             -port_num    $dut_port_1]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason  "$dut_log_msg(DISABLE_PORT_F)"\
                       -tc_def   $tc_def
    return 0

}

###############################################################################
# Step 10: Enable DUT's port P1.                                              #
#                                                                             #
###############################################################################

STEP 10

LOG -level 0 -msg "$dut_log_msg(ENABLE_PORT)"


if {![ptp_ha::dut_port_enable \
           -port_num    $dut_port_1]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason  "$dut_log_msg(ENABLE_PORT_F)"\
                       -tc_def   $tc_def
    return 0

}

###############################################################################
# Step 11: Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
#                  Sequence ID   = Y                                          #
#                  Priority1     = X                                          #
#                                                                             #
###############################################################################

STEP 11

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
           -port_num                $tee_port_1\
           -domain_number           $domain\
           -filter_id               $filter_id\
           -recvd_gm_priority1      priority1]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                        -tc_def  $tc_def

    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 12: Send periodic ANNOUNCE message with Priority1 value on port T1     #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
#                  Sequence ID   = A                                          #
#                  Priority1     = X+1                                        #
#                                                                             #
###############################################################################

STEP 12

set priority [expr $priority1 + 1]

LOG -level 0 -msg "$tee_log_msg(ANN_TX_T1)"

if {![ptp_ha::send_announce \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -gm_priority1            $priority\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -sequence_id             $sequence_id_ann\
           -count                   $infinity\
           -gmid                    $gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANN_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 13: Wait for 6s for completing BMCA.                                   #
#                                                                             #
###############################################################################

STEP 13

LOG -level 0 -msg "$dut_log_msg(WAIT_BMCA)"

sleep $::ptp_ha_bmca

###############################################################################
# Step 14: Verify that the port status of P1 in DUT is in MASTER state.       #
#                                                                             #
###############################################################################

STEP 14

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_MASTER_P1_V)"

if {![ptp_ha::dut_check_ptp_port_state \
              -port_num                $dut_port_1\
              -port_state              $master] } {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL -reason "PTP enabled device does not set\
                               portDS.portState to FAULTY state when\
                               defaultDS.externalPortConfigurationEnabled is set to\
                               TRUE and externalPortConfigurationPortDS.desiredState is\
                               set to FAULTY."\
                      -tc_def  $tc_def
    return 0
}

TEE_CLEANUP

TC_CLEAN_AND_PASS -reason "PTP enabled device sets\
                           portDS.portState to FAULTY state when\
                           defaultDS.externalPortConfigurationEnabled is set to\
                           TRUE and externalPortConfigurationPortDS.desiredState is\
                           set to FAULTY."\
                  -tc_def  $tc_def

return 1
############################ END OF TEST CASE ################################
