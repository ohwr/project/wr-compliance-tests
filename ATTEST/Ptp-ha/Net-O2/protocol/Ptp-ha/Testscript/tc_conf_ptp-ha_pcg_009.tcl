#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_ptp-ha_pcg_009                                  #
# Test Case Version : 1.4                                                     #
# Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    #
# Module Name       : PTP-HA Configuration Group (PCG)                        #
#                                                                             #
# Title             : L1SyncReceiptTimeout                                    #
#                                                                             #
# Purpose           : To verify that a PTP enabled device supports to         #
#                     configure L1SyncReceiptTimeout in range of value 2 to 10#
#                                                                             #
# Reference         : P1588/D1.3, February 2018 V3.01 Clause J.5.3 Table 150, #
#                     Page 413 Clause O.4.7 Page 443                          #
#                                                                             #
# Conformance Type  : SHALL                                                   #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 001                                                     #
# Test Topology     : 001                                                     #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode - one-step/two-step> |           #
#           |                     <Configure logAnnounceInterval, | P1        #
#           |                             announceReceiptTimeout> |           #
#           |                         <Configure logSyncInterval> | P1        #
#           |                  <Configure logMinDelayReqInterval> | P1        #
#           |                    <Configure Priority1, Priority2> | P1        #
#           |                                     <Enable L1SYNC> | P1        #
#           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        #
#           |                     <Disable L1SynOptParams option> | P1        #
#           |           <Enable asymmetryCorrectionPortDS.enable> | P1        #
#           | <Configure timestampCorrectionPortDS.egressLatency, | P1        #
#           |           timestampCorrectionPortDS.ingressLatency, |           #
#           |        asymmetryCorrectionPortDS.constantAsymmetry, |           #
#           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                     DN = DN1, SEQ_ID = Y, PRI1 = Z] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | ANNOUNCE [MSG_TYPE = 0x0B, DN = DN1,                |           #
#           |           SEQ_ID = A, PRI1 = Z+1]                   |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                   <Check L1SYNC port status - IDLE> | P1        #
#           |                                                     |           #
#           |                      PTP SIGNALING with L1 Sync TLV |           #
#           |                          [MSG_TYPE = 0xC, DN = DN1, |           #
#           |                         TLV_TYPE = 0x8001, TCR = 1, |           #
#           |                                    RCR = 1, CR = 1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | PTP SIGNALING with L1 Sync TLV                      |           #
#           | [MSG_TYPE = 0xC, DN = DN1,                          |           #
#           | TLV_TYPE=0x8001, TCR = 0,                           |           #
#           | RCR = 0, CR = 0]                                    |           #
#        T1 |---->>-----------------------------------------------| P1        #
#           |                                                     |           #
#           |             <Check L1SYNC port status - LINK_ALIVE> | P1        #
#           |                                                     |           #
#           | PTP SIGNALING with L1 Sync TLV                      |           #
#           | [MSG_TYPE = 0xC, DN = DN1,                          |           #
#           | TLV_TYPE=0x8001, TCR = 0,                           |           #
#           | RCR = 0, CR = 0]                                    |           #
#        T1 |---->>----XX                                         | P1        #
#           |                                                     |           #
#           |                    <Wait for 3s>                    |           #
#           |                                                     |           #
#           |                   <Check L1SYNC port status - IDLE> | P1        #
#           |                                                     |           #
#           |                <Configure L1SyncReceiptTimeout = 2> |           #
#           |                                                     |           #
#           |                      PTP SIGNALING with L1 Sync TLV |           #
#           |                          [MSG_TYPE = 0xC, DN = DN1, |           #
#           |                         TLV_TYPE = 0x8001, TCR = 1, |           #
#           |                                    RCR = 1, CR = 1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | PTP SIGNALING with L1 Sync TLV                      |           #
#           | [MSG_TYPE = 0xC, DN = DN1,                          |           #
#           | TLV_TYPE=0x8001, TCR = 0,                           |           #
#           | RCR = 0, CR = 0]                                    |           #
#        T1 |---->>-----------------------------------------------| P1        #
#           |                                                     |           #
#           |             <Check L1SYNC port status - LINK_ALIVE> | P1        #
#           |                                                     |           #
#           | PTP SIGNALING with L1 Sync TLV                      |           #
#           | [MSG_TYPE = 0xC, DN = DN1,                          |           #
#           | TLV_TYPE=0x8001, TCR = 0,                           |           #
#           | RCR = 0, CR = 0]                                    |           #
#        T1 |---->>----XX                                         | P1        #
#           |                                                     |           #
#           |                    <Wait for 2s>                    |           #
#           |                                                     |           #
#           |                   <Check L1SYNC port status - IDLE> | P1        #
#           |                                                     |           #
#           |               <Configure L1SyncReceiptTimeout = 10> |           #
#           |                                                     |           #
#           |                      PTP SIGNALING with L1 Sync TLV |           #
#           |                          [MSG_TYPE = 0xC, DN = DN1, |           #
#           |                         TLV_TYPE = 0x8001, TCR = 1, |           #
#           |                                    RCR = 1, CR = 1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | PTP SIGNALING with L1 Sync TLV                      |           #
#           | [MSG_TYPE = 0xC, DN = DN1,                          |           #
#           | TLV_TYPE=0x8001, TCR = 0,                           |           #
#           | RCR = 0, CR = 0]                                    |           #
#        T1 |---->>-----------------------------------------------| P1        #
#           |                                                     |           #
#           |             <Check L1SYNC port status - LINK_ALIVE> | P1        #
#           |                                                     |           #
#           | PTP SIGNALING with L1 Sync TLV                      |           #
#           | [MSG_TYPE = 0xC, DN = DN1,                          |           #
#           | TLV_TYPE=0x8001, TCR = 0,                           |           #
#           | RCR = 0, CR = 0]                                    |           #
#        T1 |---->>----XX                                         | P1        #
#           |                                                     |           #
#           |                    <Wait for 5s>                    |           #
#           |                                                     |           #
#           |             <Check L1SYNC port status - LINK_ALIVE> | P1        #
#           |                                                     |           #
#           |                    <Wait for 5s>                    |           #
#           |                                                     |           #
#           |                   <Check L1SYNC port status - IDLE> | P1        #
#           |                                                     |           #
#           |                <Configure L1SyncReceiptTimeout = 3> |           #
#           |                                                     |           #
#           |                      PTP SIGNALING with L1 Sync TLV |           #
#           |                          [MSG_TYPE = 0xC, DN = DN1, |           #
#           |                         TLV_TYPE = 0x8001, TCR = 1, |           #
#           |                                    RCR = 1, CR = 1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | PTP SIGNALING with L1 Sync TLV                      |           #
#           | [MSG_TYPE = 0xC, DN = DN1,                          |           #
#           | TLV_TYPE=0x8001, TCR = 0,                           |           #
#           | RCR = 0, CR = 0]                                    |           #
#        T1 |---->>-----------------------------------------------| P1        #
#           |                                                     |           #
#           |             <Check L1SYNC port status - LINK_ALIVE> | P1        #
#           |                                                     |           #
#           | PTP SIGNALING with L1 Sync TLV                      |           #
#           | [MSG_TYPE = 0xC, DN = DN1,                          |           #
#           | TLV_TYPE=0x8001, TCR = 0,                           |           #
#           | RCR = 0, CR = 0]                                    |           #
#        T1 |---->>----XX                                         | P1        #
#           |                                                     |           #
#           |                    <Wait for 3s>                    |           #
#           |                                                     |           #
#           |                   <Check L1SYNC port status - IDLE> | P1        #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     MSG_TYPE  = Message Type                                                #
#     DN        = Domain Number                                               #
#     BC        = Boundary Clock                                              #
#     OC        = Ordinary Clock                                              #
#                                                                             #
# NOTE :                                                                      #
#                                                                             #
#  1. This objective is verified using the High Accuracy Delay Request-       #
#     Response Default PTP Profile                                            #
#                                                                             #
#  2. Timeout = L1SyncInterval * L1SyncReceiptTimeout                         #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.ingressLatency,           #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters:                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  Priority1          = Z                                     #
#                  ANN_INT            = 1                                     #
#                                                                             #
# Step 4 : Send periodic ANNOUNCE message with Priority1 value incremented    #
#          from the Priority1 value of received Announce message on port T1   #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  Priority1          = Z+1                                   #
#                  ANN_INT            = 1                                     #
#                                                                             #
# Step 5 : Observe that DUT's L1SYNC port status P1 is in IDLE state.         #
#                                                                             #
# Step 6 : Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV  #
#          on the port P1 with following parameters:                          #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 1                                          #
#                  RCR           = 1                                          #
#                  CR            = 1                                          #
#                                                                             #
# Step 7 : Send periodic PTP SIGNALING message with L1 Sync TLV on the port P1#
#          with following parameters:                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 0                                          #
#                  RCR           = 0                                          #
#                  CR            = 0                                          #
#                  ITC           = 0                                          #
#                  IRC           = 0                                          #
#                  IC            = 0                                          #
#                                                                             #
# Step 8 : Observe that DUT's L1SYNC port status P1 is in LINK_ALIVE state.   #
#                                                                             #
# Step 9 : Stop sending L1SYNC SIGNALLING message on the port T1.             #
#                                                                             #
# Step 10: Wait for expiry of 3s.                                             #
#                                                                             #
# Step 11: Observe that DUT's L1SYNC port status P1 is in IDLE state.         #
#                                                                             #
# Step 12: Configure L1SyncReceiptTimeout as 2 on DUT.                        #
#                                                                             #
# Step 13: Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV  #
#          on the port P1 with following parameters:                          #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 1                                          #
#                  RCR           = 1                                          #
#                  CR            = 1                                          #
#                                                                             #
# Step 14: Send periodic PTP SIGNALING message with L1 Sync TLV on the port P1#
#          with following parameters:                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 0                                          #
#                  RCR           = 0                                          #
#                  CR            = 0                                          #
#                  ITC           = 0                                          #
#                  IRC           = 0                                          #
#                  IC            = 0                                          #
#                                                                             #
# Step 15: Observe that DUT's L1SYNC port status P1 is in LINK_ALIVE state.   #
#                                                                             #
# Step 16: Stop sending L1SYNC SIGNALLING message on the port T1.             #
#                                                                             #
# Step 17: Wait for expiry of 2s.                                             #
#                                                                             #
# Step 18: Observe that DUT's L1SYNC port status P1 is in IDLE state.         #
#                                                                             #
# Step 19: Configure L1SyncReceiptTimeout as 10 on DUT.                       #
#                                                                             #
# Step 20: Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV  #
#          on the port P1 with following parameters:                          #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 1                                          #
#                  RCR           = 1                                          #
#                  CR            = 1                                          #
#                                                                             #
# Step 21: Send periodic PTP SIGNALING message with L1 Sync TLV on the port P1#
#          with following parameters:                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 0                                          #
#                  RCR           = 0                                          #
#                  CR            = 0                                          #
#                  ITC           = 0                                          #
#                  IRC           = 0                                          #
#                  IC            = 0                                          #
#                                                                             #
# Step 22: Observe that DUT's L1SYNC port status P1 is in LINK_ALIVE state.   #
#                                                                             #
# Step 23: Stop sending L1SYNC SIGNALLING message on the port T1.             #
#                                                                             #
# Step 24: Wait for expiry of 5s.                                             #
#                                                                             #
# Step 25: Observe that DUT's L1SYNC port status P1 is in LINK_ALIVE state.   #
#                                                                             #
# Step 26: Wait for expiry of 5s.                                             #
#                                                                             #
# Step 27: Observe that DUT's L1SYNC port status P1 is in IDLE state.         #
#                                                                             #
# Step 28: Configure L1SyncReceiptTimeout as 3 on DUT.                        #
#                                                                             #
# Step 29: Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV  #
#          on the port P1 with following parameters:                          #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 1                                          #
#                  RCR           = 1                                          #
#                  CR            = 1                                          #
#                                                                             #
# Step 30: Send periodic PTP SIGNALING message with L1 Sync TLV on the port P1#
#          with following parameters:                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 0                                          #
#                  RCR           = 0                                          #
#                  CR            = 0                                          #
#                  ITC           = 0                                          #
#                  IRC           = 0                                          #
#                  IC            = 0                                          #
#                                                                             #
# Step 31: Observe that DUT's L1SYNC port status P1 is in LINK_ALIVE state.   #
#                                                                             #
# Step 32: Stop sending L1SYNC SIGNALLING message on the port T1.             #
#                                                                             #
# Step 33: Wait for expiry of 3s.                                             #
#                                                                             #
# Step 34: Verify that DUT's L1SYNC port status P1 is in IDLE state.          #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          May/2018      CERN          Initial                            #
# 1.1          Jun/2018      CERN          Fixed dut_port in                  #
#                                          dut_reset_l1sync_timeout           #
# 1.2          Jul/2018      CERN          Removed ITC, IRC and IC in Step 6  #
# 1.3          Sep/2018      CERN          a) Removed destination MAC and IP  #
#                                             in send functions.              #
#                                          b) Removed timeout in recv         #
#                                             functions.                      #
#                                          c) Moved timeout calculation to    #
#                                             PTP-HA global file.             #
#                                          d) Capture buffer is cleared after #
#                                             every configuration step.       #
# 1.4          Sep/2018      CERN          Added steps to receive L1Sync      #
#                                          message after configuration in DUT.#
#                                                                             #
###############################################################################
# Copyright (c) 2018 CERN                                                     #
###############################################################################

global dut_log_msg
global tee_log_msg
global session_id

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def "To verify that a PTP enabled device supports to\
            configure L1SyncReceiptTimeout interval to range of\
            value 2 to 10."

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set dut_port_1                     $::dut_port_num_1
set vlan_id                        $::ptp_ha_vlan_id

set dut_ip                         $::dut_test_port_ip
set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT

set domain                         $::DEFAULT_DOMAIN
set gmid                           $::GRANDMASTER_ID
set infinity                       $::INFINITY
set tlv_type(l1sync)               $::TLV_TYPE(L1SYNC)
set idle                           $::PTP_L1SYNC_STATE(IDLE)
set link_alive                     $::PTP_L1SYNC_STATE(LINK_ALIVE)

set sequence_id_ann                1
set sequence_id_l1sync             43692
set l1sync_receipttimeout          0
set ann_int                        1

set tcr_true        1
set rcr_true        1
set cr_true         1

set tcr_false       0
set rcr_false       0
set cr_false        0
set itc_false       0
set irc_false       0
set ic_false        0

########################### END - INITIALIZATION ##############################

ptp_ha::display_port_details

ptp_ha::dut_Initialization

proc cleanup {} {

    ptp_ha::dut_cleanup_setup_001

    ptp_ha::dut_reset_l1sync_timeout \
              -port_num         $::dut_port_num_1

}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.ingressLatency,           #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
#                                                                             #
###############################################################################

STEP 1

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP1)"

if {![ptp_ha::dut_configure_setup_001]} {

    LOG -msg "$dut_log_msg(INIT_SETUP1_F)"

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP1_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
###############################################################################

STEP 2

LOG -level 0 -msg "$tee_log_msg(INIT)"

ptp_ha::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

    if {![PTP_HA_UNICAST_PRE_REQUISITE_CONFIGURATION\
                        -port_num       $tee_port_1\
                        -tee_mac        $tee_mac\
                        -tee_ip         $tee_ip\
                        -dut_ip         $dut_ip\
                        -vlan_id        $vlan_id\
                        -dut_mac        tee_dest_mac\
                        -timeout        $arp_timeout]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
                           -tc_def $tc_def

        return 0
    }

}

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_1\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def $tc_def

    return 0
}

set filter_id [lindex $id1 end]

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

#(Part 1)#

###############################################################################
# Step 3 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters:                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  Priority1          = Z                                     #
#                  ANN_INT            = 1                                     #
###############################################################################

STEP 3

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
             -port_num                $tee_port_1\
             -filter_id               $filter_id\
             -domain_number           $domain\
             -recvd_gm_priority1      priority1\
             -log_message_interval    $ann_int]} {
    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
#                                                                             #
# Step 4 : Send periodic ANNOUNCE message with Priority1 value incremented    #
#          from the Priority1 value of received Announce message on port T1   #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  Priority1          = Z+1                                   #
#                  ANN_INT            = 1                                     #
###############################################################################

STEP 4

set priority [ expr $priority1 + 1 ]

LOG -level 0 -msg "$tee_log_msg(ANN_TX_T1)"

if {![ptp_ha::send_announce \
             -port_num               $tee_port_1\
             -src_mac                $tee_mac\
             -sequence_id            $sequence_id_ann\
             -filter_id              $filter_id\
             -domain_number          $domain\
             -count                  $infinity\
             -gm_priority1           $priority\
             -log_message_interval   $ann_int]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANN_TX_T1_F)"\
                        -tc_def  $tc_def
    return 0
}

###############################################################################
#                                                                             #
# Step 5 : Observe that DUT's L1SYNC port status P1 is in IDLE state.         #
#                                                                             #
###############################################################################

STEP 5

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_IDLE_P1_V)"

if {![ptp_ha::dut_check_l1sync_state \
              -port_num                $dut_port_1\
              -l1sync_state            $idle] } {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL -reason "$dut_log_msg(CHECK_STATE_IDLE_P1_F)"\
                      -tc_def  $tc_def
    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 6 : Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV  #
#          on the port P1 with following parameters:                          #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 1                                          #
#                  RCR           = 1                                          #
#                  CR            = 1                                          #
#                                                                             #
###############################################################################

STEP 6

LOG -level 0 -msg "$tee_log_msg(L1SYNC_RX_P1_O)"

if {![ptp_ha::recv_signal \
              -port_num          $tee_port_1\
              -filter_id         $filter_id\
              -domain_number     $domain\
              -tlv_type          $tlv_type(l1sync)\
              -tcr               $tcr_true\
              -rcr               $rcr_true\
              -cr                $cr_true]} {
    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(L1SYNC_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 7 : Send periodic PTP SIGNALING message with L1 Sync TLV on the port P1#
#          with following parameters:                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 0                                          #
#                  RCR           = 0                                          #
#                  CR            = 0                                          #
#                  ITC           = 0                                          #
#                  IRC           = 0                                          #
#                  IC            = 0                                          #
###############################################################################

STEP 7

LOG -level 0 -msg "$tee_log_msg(L1SYNC_TX_T1)"

if {![ptp_ha::send_signal \
        -port_num          $tee_port_1\
        -src_mac           $tee_mac\
        -sequence_id       $sequence_id_ann\
        -filter_id         $filter_id\
        -domain_number     $domain\
        -tlv_type          $tlv_type(l1sync)\
        -count             $infinity\
        -tcr               $tcr_false\
        -rcr               $rcr_false\
        -cr                $cr_false\
        -itc               $itc_false\
        -irc               $irc_false\
        -ic                $ic_false]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(L1SYNC_TX_T1_F)"\
                        -tc_def  $tc_def
    return 0
}

sleep 2

###############################################################################
#                                                                             #
# Step 8 : Observe that DUT's L1SYNC port status P1 is in LINK_ALIVE state.   #
#                                                                             #
###############################################################################

STEP 8

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_LINK_ALIVE_P1_O)"

sleep $::ptp_ha_l1sync_wait

if {![ptp_ha::dut_check_l1sync_state \
              -port_num                $dut_port_1\
              -l1sync_state            $link_alive] } {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_LINK_ALIVE_P1_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 9 : Stop sending L1SYNC SIGNALLING message on the port T1.             #
#                                                                             #
###############################################################################

STEP 9

LOG -level 0 -msg "$tee_log_msg(STOP_L1SYNC_TX_T1)"

if {![ptp_ha::stop_signal]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT   -reason "$tee_log_msg(STOP_L1SYNC_TX_T1_F)"\
                         -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 10: Wait for expiry of 3s.                                             #
#                                                                             #
###############################################################################

STEP 10

LOG -level 0 -msg "$dut_log_msg(WAIT_3)"

sleep 3

###############################################################################
# Step 11: Observe that DUT's L1SYNC port status P1 is in IDLE state.         #
#                                                                             #
###############################################################################

STEP 11

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_IDLE_P1_O)"

if {![ptp_ha::dut_check_l1sync_state \
              -port_num                $dut_port_1\
              -l1sync_state            $idle] } {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_IDLE_P1_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 12: Configure L1SyncReceiptTimeout as 2 on DUT.                        #
#                                                                             #
###############################################################################

STEP 12

set l1sync_timeout [ expr $l1sync_receipttimeout + 2 ]

LOG -level 0 -msg "$dut_log_msg(CONFIG_L1SYNC_TIMEOUT_2)"

  if {![ptp_ha::dut_set_l1sync_timeout \
                -port_num                $dut_port_1\
                -l1sync_timeout          $l1sync_timeout]} {

      TEE_CLEANUP

      TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_L1SYNC_TIMEOUT_F)"\
                         -tc_def  $tc_def

      return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 13: Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV  #
#          on the port P1 with following parameters:                          #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 1                                          #
#                  RCR           = 1                                          #
#                  CR            = 1                                          #
#                                                                             #
###############################################################################

STEP 13

LOG -level 0 -msg "$tee_log_msg(L1SYNC_RX_P1_O)"

if {![ptp_ha::recv_signal \
              -port_num          $tee_port_1\
              -filter_id         $filter_id\
              -domain_number     $domain\
              -tlv_type          $tlv_type(l1sync)\
              -tcr               $tcr_true\
              -rcr               $rcr_true\
              -cr                $cr_true]} {
    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(L1SYNC_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 14: Send periodic PTP SIGNALING message with L1 Sync TLV on the port P1#
#          with following parameters:                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 0                                          #
#                  RCR           = 0                                          #
#                  CR            = 0                                          #
#                  ITC           = 0                                          #
#                  IRC           = 0                                          #
#                  IC            = 0                                          #
###############################################################################

STEP 14

LOG -level 0 -msg "$tee_log_msg(L1SYNC_TX_T1)"

if {![ptp_ha::send_signal \
        -port_num          $tee_port_1\
        -src_mac           $tee_mac\
        -sequence_id       $sequence_id_ann\
        -filter_id         $filter_id\
        -domain_number     $domain\
        -tlv_type          $tlv_type(l1sync)\
        -count             $infinity\
        -tcr               $tcr_false\
        -rcr               $rcr_false\
        -cr                $cr_false\
        -itc               $itc_false\
        -irc               $irc_false\
        -ic                $ic_false]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(L1SYNC_TX_T1_F)"\
                        -tc_def  $tc_def
    return 0
}

sleep 2

###############################################################################
#                                                                             #
# Step 15: Observe that DUT's L1SYNC port status P1 is in LINK_ALIVE state.   #
#                                                                             #
###############################################################################

STEP 15

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_LINK_ALIVE_P1_O)"

sleep $::ptp_ha_l1sync_wait

if {![ptp_ha::dut_check_l1sync_state \
              -port_num                $dut_port_1\
              -l1sync_state            $link_alive] } {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_LINK_ALIVE_P1_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 16: Stop sending L1SYNC SIGNALLING message on the port T1.             #
#                                                                             #
###############################################################################

STEP 16

LOG -level 0 -msg "$tee_log_msg(STOP_L1SYNC_TX_T1)"

if {![ptp_ha::stop_signal]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT   -reason "$tee_log_msg(STOP_L1SYNC_TX_T1_F)"\
                         -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 17: Wait for expiry of 2s.                                             #
#                                                                             #
###############################################################################

STEP 17

LOG -level 0 -msg "$dut_log_msg(WAIT_2)"

sleep 2

###############################################################################
# Step 18: Observe that DUT's L1SYNC port status P1 is in IDLE state.         #
#                                                                             #
###############################################################################

STEP 18

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_IDLE_P1_O)"

if {![ptp_ha::dut_check_l1sync_state \
              -port_num                $dut_port_1\
              -l1sync_state            $idle] } {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_IDLE_P1_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 19: Configure L1SyncReceiptTimeout as 10 on DUT.                       #
#                                                                             #
###############################################################################

STEP 19

set l1sync_timeout [ expr $l1sync_receipttimeout + 10 ]

LOG -level 0 -msg "$dut_log_msg(CONFIG_L1SYNC_TIMEOUT_10)"

  if {![ptp_ha::dut_set_l1sync_timeout \
                -port_num                $dut_port_1\
                -l1sync_timeout          $l1sync_timeout]} {

      TEE_CLEANUP

      TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_L1SYNC_TIMEOUT_F)"\
                         -tc_def  $tc_def

      return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 20: Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV  #
#          on the port P1 with following parameters:                          #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 1                                          #
#                  RCR           = 1                                          #
#                  CR            = 1                                          #
#                                                                             #
###############################################################################

STEP 20

LOG -level 0 -msg "$tee_log_msg(L1SYNC_RX_P1_O)"

if {![ptp_ha::recv_signal \
              -port_num          $tee_port_1\
              -filter_id         $filter_id\
              -domain_number     $domain\
              -tlv_type          $tlv_type(l1sync)\
              -tcr               $tcr_true\
              -rcr               $rcr_true\
              -cr                $cr_true]} {
    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(L1SYNC_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 21: Send periodic PTP SIGNALING message with L1 Sync TLV on the port P1#
#          with following parameters:                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 0                                          #
#                  RCR           = 0                                          #
#                  CR            = 0                                          #
#                  ITC           = 0                                          #
#                  IRC           = 0                                          #
#                  IC            = 0                                          #
###############################################################################

STEP 21

LOG -level 0 -msg "$tee_log_msg(L1SYNC_TX_T1)"

if {![ptp_ha::send_signal \
         -port_num          $tee_port_1\
         -src_mac           $tee_mac\
         -sequence_id       $sequence_id_ann\
         -filter_id         $filter_id\
         -domain_number     $domain\
         -tlv_type          $tlv_type(l1sync)\
         -count             $infinity\
         -tcr               $tcr_false\
         -rcr               $rcr_false\
         -cr                $cr_false\
         -itc               $itc_false\
         -irc               $irc_false\
         -ic                $ic_false]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(L1SYNC_TX_T1_F)"\
                        -tc_def  $tc_def
    return 0
}

sleep 2

###############################################################################
# Step 22: Observe that DUT's L1SYNC port status P1 is in LINK_ALIVE state.   #
#                                                                             #
###############################################################################

STEP 22

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_LINK_ALIVE_P1_O)"

sleep $::ptp_ha_l1sync_wait

if {![ptp_ha::dut_check_l1sync_state \
              -port_num                $dut_port_1\
              -l1sync_state            $link_alive] } {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_LINK_ALIVE_P1_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 23: Stop sending L1SYNC SIGNALLING message on the port T1.             #
#                                                                             #
###############################################################################

STEP 23

LOG -level 0 -msg "$tee_log_msg(STOP_L1SYNC_TX_T1)"

if {![ptp_ha::stop_signal]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT   -reason "$tee_log_msg(STOP_L1SYNC_TX_T1_F)"\
                         -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 24: Wait for expiry of 5s.                                             #
#                                                                             #
###############################################################################

STEP 24

LOG -level 0 -msg "$dut_log_msg(WAIT_5)"

sleep 5

###############################################################################
# Step 25: Observe that DUT's L1SYNC port status P1 is in LINK_ALIVE state.   #
#                                                                             #
###############################################################################

STEP 25

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_LINK_ALIVE_P1_O)"

if {![ptp_ha::dut_check_l1sync_state \
              -port_num                $dut_port_1\
              -l1sync_state            $link_alive] } {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_LINK_ALIVE_P1_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 26: Wait for expiry of 5s.                                             #
#                                                                             #
###############################################################################

STEP 26

LOG -level 0 -msg "$dut_log_msg(WAIT_5)"

sleep 5

###############################################################################
# Step 27: Observe that DUT's L1SYNC port status P1 is in IDLE state.         #
#                                                                             #
###############################################################################

STEP 27

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_IDLE_P1_O)"

if {![ptp_ha::dut_check_l1sync_state \
              -port_num                $dut_port_1\
              -l1sync_state            $idle] } {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_IDLE_P1_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 28: Configure L1SyncReceiptTimeout as 3 on DUT.                        #
#                                                                             #
###############################################################################

STEP 28

set l1sync_timeout [ expr $l1sync_receipttimeout + 3 ]

LOG -level 0 -msg "$dut_log_msg(CONFIG_L1SYNC_TIMEOUT_3)"

  if {![ptp_ha::dut_set_l1sync_timeout \
                -port_num                $dut_port_1\
                -l1sync_timeout          $l1sync_timeout]} {

      TEE_CLEANUP

      TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_L1SYNC_TIMEOUT_F)"\
                         -tc_def  $tc_def

      return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 29: Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV  #
#          on the port P1 with following parameters:                          #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 1                                          #
#                  RCR           = 1                                          #
#                  CR            = 1                                          #
#                                                                             #
###############################################################################

STEP 29

LOG -level 0 -msg "$tee_log_msg(L1SYNC_RX_P1_O)"

if {![ptp_ha::recv_signal \
              -port_num          $tee_port_1\
              -filter_id         $filter_id\
              -domain_number     $domain\
              -tlv_type          $tlv_type(l1sync)\
              -tcr               $tcr_true\
              -rcr               $rcr_true\
              -cr                $cr_true]} {
    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(L1SYNC_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 30: Send periodic PTP SIGNALING message with L1 Sync TLV on the port P1#
#          with following parameters:                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 0                                          #
#                  RCR           = 0                                          #
#                  CR            = 0                                          #
#                  ITC           = 0                                          #
#                  IRC           = 0                                          #
#                  IC            = 0                                          #
###############################################################################

STEP 30

LOG -level 0 -msg "$tee_log_msg(L1SYNC_TX_T1)"

if {![ptp_ha::send_signal \
         -port_num          $tee_port_1\
         -src_mac           $tee_mac\
         -sequence_id       $sequence_id_ann\
         -filter_id         $filter_id\
         -domain_number     $domain\
         -tlv_type          $tlv_type(l1sync)\
         -count             $infinity\
         -tcr               $tcr_false\
         -rcr               $rcr_false\
         -cr                $cr_false\
         -itc               $itc_false\
         -irc               $irc_false\
         -ic                $ic_false]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(L1SYNC_TX_T1_F)"\
                        -tc_def  $tc_def
    return 0
}

sleep 2

###############################################################################
#                                                                             #
# Step 31: Observe that DUT's L1SYNC port status P1 is in LINK_ALIVE state.   #
#                                                                             #
###############################################################################

STEP 31

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_LINK_ALIVE_P1_O)"

sleep $::ptp_ha_l1sync_wait

if {![ptp_ha::dut_check_l1sync_state \
              -port_num                $dut_port_1\
              -l1sync_state            $link_alive] } {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_LINK_ALIVE_P1_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 32: Stop sending L1SYNC SIGNALLING message on the port T1.             #
#                                                                             #
###############################################################################

STEP 32

LOG -level 0 -msg "$tee_log_msg(STOP_L1SYNC_TX_T1)"

if {![ptp_ha::stop_signal]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT   -reason "$tee_log_msg(STOP_L1SYNC_TX_T1_F)"\
                         -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 33: Wait for expiry of 3s.                                             #
#                                                                             #
###############################################################################

STEP 33

LOG -level 0 -msg "$dut_log_msg(WAIT_3)"

sleep 3

###############################################################################
# Step 34: Verify that DUT's L1SYNC port status P1 is in IDLE state.          #
#                                                                             #
###############################################################################

STEP 34

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_IDLE_P1_O)"

if {![ptp_ha::dut_check_l1sync_state \
              -port_num                $dut_port_1\
              -l1sync_state            $idle] } {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL  -reason "PTP enabled device does not support to\
                                configure L1SyncReceiptTimeout interval to range of\
                                value 2 to 10"\
                       -tc_def  $tc_def
    return 0
}

TEE_CLEANUP

TC_CLEAN_AND_PASS   -reason "PTP enabled device supports to\
                             configure L1SyncReceiptTimeout interval to range of\
                             value 2 to 10"\
                    -tc_def  $tc_def
return 1

################################ END OF TEST CASE ###############################
