#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_ptp-ha_pag_006                                  #
# Test Case Version : 1.4                                                     #
# Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    #
# Module Name       : PTP Accuracy Group (PAG)                                #
#                                                                             #
# Title             : Calculation of delayAsymmetry in Peer to Peer Delay     #
#                     mechanism when DUT is master                            #
#                                                                             #
# Purpose           : To verify that a PTP enabled device performs computation#
#                     of delayAsymmetry each time the value of meanDelay is   #
#                     updated in Peer to Peer Delay mechanism when DUT is     #
#                     master.                                                 #
#                                                                             #
# Reference         : P1588/D1.3, February 2018 V3.01 clause 16.8.3 page 302  #
#                     Clause 7.4.2 Page 73                                    #
#                                                                             #
# Conformance Type  : SHALL                                                   #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 002                                                     #
# Test Topology     : 001                                                     #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode - one-step/two-step> |           #
#           |                        <Set delay mechanism as P2P> |           #
#           |                     <Configure logAnnounceInterval, | P1        #
#           |                             announceReceiptTimeout> |           #
#           |                         <Configure logSyncInterval> | P1        #
#           |                  <Configure logMinDelayReqInterval> | P1        #
#           |                    <Configure Priority1, Priority2> | P1        #
#           |                                     <Enable L1SYNC> | P1        #
#           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        #
#           |                     <Disable L1SynOptParams option> | P1        #
#           |           <Enable asymmetryCorrectionPortDS.enable> | P1        #
#           | <Configure timestampCorrectionPortDS.egressLatency, | P1        #
#           |           timestampCorrectionPortDS.ingressLatency, |           #
#           |        asymmetryCorrectionPortDS.constantAsymmetry, |           #
#           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           #
#           |                                                     |           #
#           |  <Configure timestampCorrectionPortDS.egressLatency |           #
#           |                         value to -4 000 000 000 ns> |           #
#           |                                                     |           #
#           | <Configure timestampCorrectionPortDS.ingressLatency |           #
#           |                         value to -4 000 000 000 ns> |           #
#           |                                                     |           #
#           |           <Enable asymmetryCorrectionPortDS.enable> | P1        #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                                 DN = DN1, PRI1 = X] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | ANNOUNCE [MSG_TYPE = 0x0B,                          |           #
#           | DN = DN1, PRI1 = X+1]                               |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#        T1 | <Enable auto responder to Pdelay_Req messages>      | P1        #
#           |                                                     |           #
#           |              PDELAY_REQ [MSG_TYPE = 0x02, DN = DN1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                   <Get portDS.meanLinkDelay (MLD1)> |           #
#           |                                                     |           #
#           |                          <Get delayAsymmetry (DA1)> |           #
#           |                                                     |           #
#           |                                     <Check DA1 = 0> |           #
#           |                                                     |           #
#           |    PDELAY_REQ [MSG_TYPE = 0x02, DN = DN1, CF = CF1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                                     <Check CF1 = 0> |           #
#           |                                                     |           #
#           |               <Configure asymmetryCorrectionPortDS. |           #
#           |                              scaledDelayCoefficient |           #
#           |                                            to 2^62> |           #
#           |                                                     |           #
#           |              PDELAY_REQ [MSG_TYPE = 0x02, DN = DN1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                   <Get portDS.meanLinkDelay (MLD2)> |           #
#           |                                                     |           #
#           |                          <Get delayAsymmetry (DA2)> |           #
#           |                                                     |           #
#           |                          <Check DA2 = (1/3) * MLD2> |           #
#           |                                                     |           #
#           |    PDELAY_REQ [MSG_TYPE = 0x02, DN = DN1, CF = CF2] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                            <Check CF2 = (-1) * DA2> |           #
#           |                                                     |           #
#           |               <Configure asymmetryCorrectionPortDS. |           #
#           |                                   constantAsymmetry |           #
#           |                                       to 2^(32+16)> |           #
#           |                                                     |           #
#           |              PDELAY_REQ [MSG_TYPE = 0x02, DN = DN1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                   <Get portDS.meanLinkDelay (MLD3)> |           #
#           |                                                     |           #
#           |                          <Get delayAsymmetry (DA3)> |           #
#           |                                                     |           #
#           |              <Check DA2 = (1/3) * MLD3 + 2^(32+16)> |           #
#           |                                                     |           #
#           |    PDELAY_REQ [MSG_TYPE = 0x02, DN = DN1, CF = CF3] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                            <Check CF3 = (-1) * DA3> |           #
#           |                                                     |           #
#           |               <Configure asymmetryCorrectionPortDS. |           #
#           |                              scaledDelayCoefficient |           #
#           |                                           to -2^62> |           #
#           |                                                     |           #
#           |               <Configure asymmetryCorrectionPortDS. |           #
#           |                                   constantAsymmetry |           #
#           |                                               to 0> |           #
#           |                                                     |           #
#           |              PDELAY_REQ [MSG_TYPE = 0x02, DN = DN1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                   <Get portDS.meanLinkDelay (MLD4)> |           #
#           |                                                     |           #
#           |                          <Get delayAsymmetry (DA4)> |           #
#           |                                                     |           #
#           |                           <Check DA4 = (-1) * MLD4> |           #
#           |                                                     |           #
#           |    PDELAY_REQ [MSG_TYPE = 0x02, DN = DN1, CF = CF4] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                            <Check CF4 = (-1) * DA4> |           #
#           |                                                     |           #
#           |          <Disable asymmetryCorrectionPortDS.enable> | P1        #
#           |                                                     |           #
#           |              PDELAY_REQ [MSG_TYPE = 0x02, DN = DN1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                   <Get portDS.meanLinkDelay (MLD5)> |           #
#           |                                                     |           #
#           |                          <Get delayAsymmetry (DA5)> |           #
#           |                                                     |           #
#           |                           <Check DA5 = (-1) * MLD5> |           #
#           |                                                     |           #
#           |      PDELAY_REQ [MSG_TYPE=0X02, DN = DN1, CF = CF5] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                            <Check CF5 = (-1) * DA5> |           #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     MSG_TYPE         = Message Type                                         #
#     DN               = Domain Number                                        #
#     PRT1             = priority1                                            #
#     SEQ_ID           = Sequence ID                                          #
#     CF1 - CF5        = Correction Field                                     #
#     DA1 - DA5        = currentDS.delayAsymmetry                             #
#     MLD1 - MLD5      = currentDS.meanLinkDelay                              #
#                                                                             #
# NOTE :                                                                      #
#                                                                             #
#  1. This objective is verified using the High Accuracy Peer to Peer Default #
#     PTP Profile                                                             #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure delaymechanism as Peer to peer.                          #
#    vi.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#   vii.   Enable L1SYNC on DUT's port P1.                                    #
#  viii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#    ix.   Disable L1SynOptParams on DUT.                                     #
#     x.   Enable asymmetryCorrectionPortDS.enable.                           #
#    xi.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency = 0, timestampCorrectionPortDS.egressLatency = 0,    #
#          asymmetryCorrectionPortDS.constantAsymmetry = 0 and                #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient = 0.              #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Configure egressLatency and ingressLatency on port P1 by setting   #
#          egressLatency and ingressLatency to -4 000 000 000 ns.             #
#                                                                             #
# Step 4 : Enable asymmetryCorrectionPortDS.enable.                           #
#                                                                             #
# Step 5 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
#                  Priority1     = X                                          #
#                                                                             #
# Step 6 : Send periodic ANNOUNCE message with Priority1 value decremented    #
#          from the Priority1 value of received Announce message on port T1   #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
#                  Priority1     = X+1                                        #
#                                                                             #
# Step 7 : Enable auto responder to respond every Pdelay_Req messages received#
#          on port T1.                                                        #
#                                                                             #
# Step 8 : Observe that DUT transmits PDELAY_REQ message on the port P1 with  #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type              = 0x02                           #
#                  Domain Number             = DN1                            #
#                                                                             #
# Step 9 : Get portDS.meanLinkDelay (MLD1) value for Port P1 in DUT.          #
#                                                                             #
# Step 10: Get delayAsymmetry (DA1) value for Port P1 in DUT.                 #
#                                                                             #
# Step 10: Check whether DA1 = 0.                                             #
#                                                                             #
# Step 11: Observe that DUT transmits PDELAY_REQ on the port P1 with following#
#          parameters and record correction field (CF1).                      #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type             = 0x01                            #
#                  Domain Number            = DN1                             #
#                                                                             #
# Step 12: Check whether CF1 = 0.                                             #
#                                                                             #
# Step 13: Configure scaledDelayCoefficient on port P1 by setting             #
#          scaledDelayCoefficient to 1 (i.e., the value of dataset expressed  #
#          in RelativeDifference asymmetryCorrectionPortDS.                   #
#          scaledDelayCoefficient = 2^62).                                    #
#                                                                             #
# Step 14: Observe that DUT transmits PDELAY_REQ message on the port P1 with  #
#          following parameters to ensure the DUT is ready with configured    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient value.            #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type              = 0x02                           #
#                  Domain Number             = DN1                            #
#                                                                             #
# Step 15: Get portDS.meanLinkDelay (MLD2) value for Port P1 in DUT.          #
#                                                                             #
# Step 16: Get delayAsymmetry (DA2) value for Port P1 in DUT.                 #
#                                                                             #
# Step 17: Check whether DA2 = (1/3)*MLD2 (with margin of error).             #
#                                                                             #
# Step 18: Observe that DUT transmits PDELAY_REQ on the port P1 with following#
#          parameters and record correction field (CF2).                      #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type             = 0x01                            #
#                  Domain Number            = DN1                             #
#                                                                             #
# Step 19: Check whether CF2 = (-1) * DA2 (with margin of error).             #
#                                                                             #
# Step 20: Configure constantAsymmetry on port P1 by setting constantAsymmetry#
#          to 4 294 967 296 ns (i.e., the value of dataset expressed in       #
#          TimeInterval asymmetryCorrectionPortDS.constantAsymmetry = 2^48).  #
#                                                                             #
# Step 21: Observe that DUT transmits PDELAY_REQ message on the port P1 with  #
#          following parameters to ensure the DUT is ready with configured    #
#          asymmetryCorrectionPortDS.constantAsymmetry value.                 #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type              = 0x02                           #
#                  Domain Number             = DN1                            #
#                                                                             #
# Step 22: Get portDS.meanLinkDelay (MLD3) value for Port P1 in DUT.          #
#                                                                             #
# Step 23: Get delayAsymmetry (DA3) value for Port P1 in DUT.                 #
#                                                                             #
# Step 24: Check whether DA3 = (1/3)*MLD2 + 4 294 967 296 ns (i.e., 2^(32+16) #
#          in TimeInterval) (with margin of error).                           #
#                                                                             #
# Step 25: Observe that DUT transmits PDELAY_REQ on the port P1 with following#
#          parameters and record correction field (CF3).                      #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type             = 0x01                            #
#                  Domain Number            = DN1                             #
#                                                                             #
# Step 26: Check whether CF3 = (-1) * DA3 (with margin of error).             #
#                                                                             #
# Step 27: Configure scaledDelayCoefficient on port P1 by setting             #
#          scaledDelayCoefficient to -1 (i.e., the value of dataset expressed #
#          in RelativeDifference asymmetryCorrectionPortDS.                   #
#          scaledDelayCoefficient = -2^62).                                   #
#                                                                             #
# Step 28: Configure constantAsymmetry on port P1 by setting constantAsymmetry#
#          to 0 (i.e., the value of dataset expressed in TimeInterval         #
#          asymmetryCorrectionPortDS.constantAsymmetry = 0).                  #
#                                                                             #
# Step 29: Observe that DUT transmits PDELAY_REQ message on the port P1 with  #
#          following parameters to ensure the DUT is ready with configured    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient and               #
#          asymmetryCorrectionPortDS.constantAsymmetry values.                #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type              = 0x02                           #
#                  Domain Number             = DN1                            #
#                                                                             #
# Step 30: Get portDS.meanLinkDelay (MLD4) value for Port P1 in DUT.          #
#                                                                             #
# Step 31: Get delayAsymmetry (DA4) value for Port P1 in DUT.                 #
#                                                                             #
# Step 32: Check whether DA4 = (-1) * MLD4 (with margin of error).            #
#                                                                             #
# Step 33: Observe that DUT transmits PDELAY_REQ on the port P1 with following#
#          parameters and record correction field (CF4).                      #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type             = 0x01                            #
#                  Domain Number            = DN1                             #
#                                                                             #
# Step 34: Check whether CF4 = (-1) * DA4 (with margin of error).             #
#                                                                             #
# Step 35: Disable asymmetryCorrectionPortDS.enable.                          #
#                                                                             #
# Note: The asymmetryCorrectionPortDS.enable is not allowed to be set to FALSE#
#       in the High Accuracy Default PTP Profile. Despite providing FALSE     #
#       value of this data set member to the DUT, it is expected that the     #
#       value of asymmetryCorrectionPortDS.enable remains TRUE and the        #
#       measurements of meanDelay and delayAsymmetry are made accordingly.    #
#                                                                             #
# Step 36: Observe that DUT transmits PDELAY_REQ message on the port P1 with  #
#          following parameters to ensure the DUT is ready with configured    #
#          asymmetryCorrectionPortDS.enable value.                            #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type              = 0x02                           #
#                  Domain Number             = DN1                            #
#                                                                             #
# Step 37: Get portDS.meanLinkDelay (MLD5) value for Port P1 in DUT.          #
#                                                                             #
# Step 38: Get delayAsymmetry (DA5) value for Port P1 in DUT.                 #
#                                                                             #
# Step 39: Check whether DA5 = (-1) * MLD5 (with margin of error).            #
#                                                                             #
# Step 40: Observe that DUT transmits PDELAY_REQ on the port P1 with following#
#          parameters and record correction field (CF5).                      #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type             = 0x02                            #
#                  Domain Number            = DN1                             #
#                                                                             #
# Step 41: Verify whether CF5 = (-1) * DA5 (with margin of error).            #
#                                                                             #
# Note: The default value of margin of error can be changed through ATTEST GUI#
#       (i.e., Go to Configuration Manager and select desired configuration,  #
#       go to Protocol Options > PTP-HA > Global).                            #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          Oct/2018      CERN          Initial                            #
# 1.1          Nov/2018      CERN          Re-ordered message exchanges in    #
#                                          Ladder Diagram and Procedure to    #
#                                          include the auto-responder module  #
#                                          and avoid timing issues while      #
#                                          extracting meanLinkDelay values.   #
# 1.2          Jan/2019      CERN          Resolved the error in using        #
#                                          floating values less than 1.       #
# 1.3          Feb/2019      CERN          a) Added support to proceed test   #
#                                             until last step and gracefully  #
#                                             abort the test for debugging.   #
#                                          b) Added margin of error for       #
#                                             validating meanDelay and        #
#                                             delayAsymmetry.                 #
# 1.4          Apr/2019      CERN          a) Removed comparison of meanDelays#
#                                          b) Added step to initialize        #
#                                             egressLatency and ingressLatency#
#                                             to -4 000 000 000 ns.           #
#                                          c) Fixed error in margin of error  #
#                                             calculation for negative values.#
#                                          d) asymmetryCorrectionPortDS.enable#
#                                             is not allowed to set FALSE and #
#                                             verification of meanDelay and   #
#                                             delayAsymmetry is modified      #
#                                             accordingly.                    #
#                                                                             #
###############################################################################
# Copyright (c) 2018 - 2019 CERN                                              #
###############################################################################

global dut_log_msg
global tee_log_msg
global session_id

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def      "To verify that a PTP enabled device performs computation\
                 of delayAsymmetry each time the value of meanDelay is\
                 updated in Peer to Peer Delay mechanism when DUT is\
                 master."

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set dut_port_1                     $::dut_port_num_1
set vlan_id                        $::ptp_ha_vlan_id
set dut_ip                         $::dut_test_port_ip
set dut_dest_ip                    $::dut_test_port_ip
set tee_dest_ip                    $::tee_test_port_ip
set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT

set domain                         $::DEFAULT_DOMAIN
set infinity                       $::INFINITY
set clock_step                     $::ptp_dut_clock_step
set offset_from_master             $::offset_from_master
set ptp_ha_margin_of_error         [expr 1.0 * $::margin_of_error]

set sequence_id_ann                 1
set sequence_id_sync                1
set constant_asymmetry              0

set SKIP_VALIDATION $::SKIP_VALIDATION_FOR_DEBUG

########################### END - INITIALIZATION ##############################

if {($env(TEST_DEVICE_MANAGEMENT_MODE) == "Manual")} {
    
    puts ".\n"
    puts "???????????????????????????????????????????????????????????????????????????"
    puts "???????????????????????????????????????????????????????????????????????????"
    puts "?? WARNING: Execution of this test in manual management mode may give    ??"
    puts "??          in-correct result.                                           ??"
    puts "??                                                                       ??"
    puts "?? The timestamps from the device should be taken faster and hence it is ??"
    puts "?? advised to execute this test case in automated mode.                  ??"
    puts "??                                                                       ??"
    puts "???????????????????????????????????????????????????????????????????????????"
    puts "???????????????????????????????????????????????????????????????????????????"
    puts ".\n"
}

if { $::ptp_p2p_delay_mechanism != "true" } {

    TC_ABORT -reason "$tee_log_msg(P2P_DLY_MECHANISM)"\
             -tc_def $tc_def

    return 0
}

ptp_ha::display_port_details

ptp_ha::dut_Initialization

proc cleanup {} {

    ptp_ha::dut_cleanup_setup_002

    ptp_ha::dut_reset_egress_latency\
                -port_num           $::dut_port_num_1

    ptp_ha::dut_reset_ingress_latency\
                -port_num           $::dut_port_num_1

    ptp_ha::dut_reset_scaled_delay_coefficient\
                -port_num           $::dut_port_num_1

    ptp_ha::dut_reset_constant_asymmetry\
                -port_num           $::dut_port_num_1

    ptp_ha::dut_reset_asymmetry_correction\
                -port_num           $::dut_port_num_1
}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

### (Initial Part) ###

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure delaymechanism as Peer to peer.                          #
#    vi.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#   vii.   Enable L1SYNC on DUT's port P1.                                    #
#  viii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#    ix.   Disable L1SynOptParams on DUT.                                     #
#     x.   Enable asymmetryCorrectionPortDS.enable.                           #
#    xi.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency = 0, timestampCorrectionPortDS.egressLatency = 0,    #
#          asymmetryCorrectionPortDS.constantAsymmetry = 0 and                #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient = 0.              #
###############################################################################

STEP 1

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP2)"

if {![ptp_ha::dut_configure_setup_002]} {

    LOG -msg "$dut_log_msg(INIT_SETUP2_F)"

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP2_F)"\
                       -tc_def  $tc_def
    return 0
}

ptp_ha::stop_at_step

###############################################################################
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
###############################################################################

STEP 2

LOG -level 0 -msg "$tee_log_msg(INIT)"

ptp_ha::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

    if {![PTP_HA_UNICAST_PRE_REQUISITE_CONFIGURATION\
                        -port_num       $tee_port_1\
                        -tee_mac        $tee_mac\
                        -tee_ip         $tee_ip\
                        -dut_ip         $dut_ip\
                        -vlan_id        $vlan_id\
                        -dut_mac        tee_dest_mac\
                        -timeout        $arp_timeout]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
                           -tc_def  $tc_def

        return 0
    }

}

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_1\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def  $tc_def

    return 0
}

set filter_id [lindex $id1 end]

ptp_ha::stop_at_step

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

# (Part 1)   #

###############################################################################
# Step 3 : Configure egressLatency and ingressLatency on port P1 by setting   #
#          egressLatency and ingressLatency to -4 000 000 000 ns.             #
###############################################################################

STEP 3

set latency -4000000000

LOG -level 0 -msg "$dut_log_msg(CONFIG_TXPORTDS.EGRESSLATENCY)"

if {![ptp_ha::dut_set_egress_latency\
             -port_num       $dut_port_1\
             -latency        $latency]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_TXPORTDS.EGRESSLATENCY_F)"\
                        -tc_def  $tc_def

     return 0
}

LOG -level 0 -msg "$dut_log_msg(CONFIG_TXPORTDS.INGRESSLATENCY)"

if {![ptp_ha::dut_set_ingress_latency\
             -port_num       $dut_port_1\
             -latency        $latency]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_TXPORTDS.INGRESSLATENCY_F)"\
                        -tc_def  $tc_def

     return 0
}

ptp_ha::stop_at_step


###############################################################################
# Step 4 : Enable asymmetryCorrectionPortDS.enable.                           #
###############################################################################

STEP 4

LOG -level 0 -msg "$dut_log_msg(ACP.ENABLE)"

if {![ptp_ha::dut_set_asymmetry_correction\
                -port_num      $dut_port_1]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(ACP.ENABLE_F)"\
                       -tc_def  $tc_def

    return 0
}

ptp_ha::stop_at_step

###############################################################################
#                                                                             #
# Step 5 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
#                  Priority1     = X                                          #
###############################################################################

STEP 5

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
           -port_num                $tee_port_1\
           -domain_number           $domain\
           -filter_id               $filter_id\
           -error_reason            error_reason\
           -recvd_gm_priority1      priority1]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANNOUNCE_RX_P1_F) $error_reason"\
                        -tc_def  $tc_def

    return 0
}

ptp_ha::stop_at_step

###############################################################################
#                                                                             #
# Step 6 : Send periodic ANNOUNCE message with Priority1 value decremented    #
#          from the Priority1 value of received Announce message on port T1   #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
#                  Priority1     = X+1                                        #
###############################################################################

STEP 6

set priority [expr $priority1 + 1]

LOG -level 0 -msg "$tee_log_msg(ANN_TX_T1)"

if {![ptp_ha::send_announce \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -gm_priority1            $priority\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -count                   $infinity]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANN_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 7 : Enable auto responder to respond every Pdelay_Req messages received#
#          on port T1.                                                        #
#                                                                             #
###############################################################################

STEP 7

LOG -level 0 -msg "$tee_log_msg(ENABLE_RESPONDER_TO_PDELAY_REQ)"

ptp_ha::enable_auto_response_to_pdelay_requests

ptp_ha::stop_at_step

################################################################################
# Step 8 : Observe that DUT transmits PDELAY_REQ message on the port P1 with   #
#           following parameters. Store Correction Field, Sequence ID, Source  #
#           Port Identity as PDREQ_CF, D and E respectively.                   #
#                                                                              #
#               PTP Header                                                     #
#                   Message Type              = 0x02                           #
#                   Domain Number             = DN1                            #
#                   Correction Field          = PDREQ_CF                       #
#                   Sequence ID               = D                              #
#                   Source Port Identity      = E                              #
################################################################################

STEP 8

LOG -level 0 -msg "$tee_log_msg(PDELAY_REQ_RX_P1_O)"

if {![ptp_ha::recv_pdelay_req \
           -port_num                  $tee_port_1\
           -domain_number             $domain\
           -filter_id                 $filter_id\
           -error_reason              error_reason\
           -recvd_correction_field    PDREQ_CF\
           -recvd_sequence_id         recvd_sequence_id\
           -recvd_src_port_number     recvd_src_port_number\
           -recvd_src_clock_identity  recvd_src_clock_identity]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(PDELAY_REQ_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 9: Get portDS.meanLinkDelay (MLD1) value for Port P1 in DUT.           #
###############################################################################

STEP 9

LOG -level 0 -msg "$dut_log_msg(GET_MEAN_LINK_DELAY_MLD1)"

if {![ptp_ha::dut_get_mean_link_delay\
          -port_num         $dut_port_1\
          -mean_link_delay  MLD1]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(GET_MEAN_LINK_DELAY_MLD1_F)"\
                       -tc_def  $tc_def

    return 0

}

ptp_ha::stop_at_step

set MLD1 [format "%.3f" $MLD1]

###############################################################################
# Step 10: Get delayAsymmetry (DA1) value for Port P1 in DUT.                 #
###############################################################################

STEP 10

LOG -level 0 -msg "$dut_log_msg(GET_DELAY_ASYMMETRY_DA1)"

if {![ptp_ha::dut_get_delay_asymmetry \
              -port_num          $dut_port_1\
              -delay_asymmetry   DA1]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(GET_DELAY_ASYMMETRY_DA1_F)"\
                        -tc_def  $tc_def
     return 0
}

ptp_ha::stop_at_step

set DA1 [format "%.3f" $DA1]

###############################################################################
# Step 11: Check whether DA1 = 0.                                             #
###############################################################################

STEP 11

LOG -level 0 -msg "$dut_log_msg(DA1_V)"

LOG -level 3 -msg "Value of delayAsymmetry (DA1) = $DA1 ns"

if {!($DA1 == 0)} {

if {$SKIP_VALIDATION != 1} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(DA1_F)"\
                       -tc_def  $tc_def
    return 0
}
}

ptp_ha::stop_at_step

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 12: Observe that DUT transmits PDELAY_REQ on the port P1 with following#
#          parameters and record correction field (CF1).                      #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type             = 0x01                            #
#                  Domain Number            = DN1                             #
###############################################################################

STEP 12

LOG -level 0 -msg "$tee_log_msg(PDELAY_REQ_RX_O)"

if {![ptp_ha::recv_pdelay_req \
           -port_num                 $tee_port_1\
           -domain_number            $domain\
           -recvd_correction_field   CF1\
           -error_reason             error_reason\
           -filter_id                $filter_id]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(PDELAY_REQ_RX_F) $error_reason"\
                        -tc_def  $tc_def

    return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 13: Check whether CF1 = 0.                                             #
###############################################################################

STEP 13

LOG -level 0 -msg "$dut_log_msg(CF1_V)"

LOG -level 3 -msg "Value of correctionField (CF1) = $CF1 ns"

if {!($CF1 == 0)} {

if {$SKIP_VALIDATION != 1} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$dut_log_msg(CF1_F)"

    return 0
}
}

ptp_ha::stop_at_step

###############################################################################
# Step 14: Configure scaledDelayCoefficient on port P1 by setting             #
#          scaledDelayCoefficient to 1 (i.e., the value of dataset expressed  #
#          in RelativeDifference asymmetryCorrectionPortDS.                   #
#          scaledDelayCoefficient = 2^62).                                    #
###############################################################################

STEP 14

set delay_coefficient 1

LOG -level 0 -msg "$dut_log_msg(CONFIG_ASC.DELAYCOEFFICIENT)"

if {![ptp_ha::dut_set_scaled_delay_coefficient\
             -port_num               $dut_port_1\
             -delay_coefficient      $delay_coefficient]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_ASC.DELAYCOEFFICIENT_F)"\
                        -tc_def  $tc_def

     return 0
}

ptp_ha::stop_at_step

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

################################################################################
# Step 15: Observe that DUT transmits PDELAY_REQ message on the port P1 with   #
#           following parameters. Store Correction Field, Sequence ID, Source  #
#           Port Identity as PDREQ_CF, D and E respectively.                   #
#                                                                              #
#               PTP Header                                                     #
#                   Message Type              = 0x02                           #
#                   Domain Number             = DN1                            #
#                   Correction Field          = PDREQ_CF                       #
#                   Sequence ID               = D                              #
#                   Source Port Identity      = E                              #
#                                                                              #
################################################################################

STEP 15

LOG -level 0 -msg "$tee_log_msg(PDELAY_REQ_RX_O)"

if {![ptp_ha::recv_pdelay_req \
           -port_num                 $tee_port_1\
           -domain_number            $domain\
           -filter_id                $filter_id\
           -recvd_correction_field   PDREQ_CF\
           -error_reason             error_reason\
           -recvd_src_port_number    src_port_number\
           -recvd_src_clock_identity src_clock_identity\
           -recvd_sequence_id        sequence_id]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(PDELAY_REQ_RX_F) $error_reason"\
                        -tc_def  $tc_def

    return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 16: Get portDS.meanLinkDelay (MLD2) value for Port P1 in DUT.          #
###############################################################################

STEP 16

LOG -level 0 -msg "$dut_log_msg(GET_MEAN_LINK_DELAY_MLD2)"

if {![ptp_ha::dut_get_mean_link_delay\
          -port_num         $dut_port_1\
          -mean_link_delay  MLD2]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(GET_MEAN_LINK_DELAY_MLD2_F)"\
                       -tc_def  $tc_def

    return 0

}

ptp_ha::stop_at_step

set MLD2 [format "%.3f" $MLD2]

###############################################################################
# Step 17: Get delayAsymmetry (DA2) value for Port P1 in DUT.                 #
###############################################################################

STEP 17

LOG -level 0 -msg "$dut_log_msg(GET_DELAY_ASYMMETRY_DA2)"

if {![ptp_ha::dut_get_delay_asymmetry \
              -port_num          $dut_port_1\
              -delay_asymmetry   DA2]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(GET_DELAY_ASYMMETRY_DA2_F)"\
                        -tc_def  $tc_def
     return 0
}

ptp_ha::stop_at_step

set DA2 [format "%.3f" $DA2]

###############################################################################
# Step 18: Check whether DA2 = (1/3)*MLD2 (with margin of error).             #
###############################################################################

STEP 18

LOG -level 0 -msg "$dut_log_msg(CHECK_EXPRESSION_DA2)"

set DA_CAL  [format "%.3f" [expr (1.0/3.0)*$MLD2]]

ptp_ha::value_range $DA2 $ptp_ha_margin_of_error da_cal_min da_cal_max

LOG -level 3 -msg "Value of DA2      = $DA2 ns"
LOG -level 3 -msg "Value of DA_CAL   = $DA_CAL ns (Acceptable Range: $da_cal_min ns to $da_cal_max ns)"

if {!(($DA2 >= $da_cal_min) && ($DA2 <= $da_cal_max))} {

if {$SKIP_VALIDATION != 1} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_EXPRESSION_F)"\
                       -tc_def  $tc_def
     return 0
}
}

ptp_ha::stop_at_step

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 19: Observe that DUT transmits PDELAY_REQ on the port P1 with following#
#          parameters and record correction field (CF2).                      #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type             = 0x01                            #
#                  Domain Number            = DN1                             #
###############################################################################

STEP 19

LOG -level 0 -msg "$tee_log_msg(PDELAY_REQ_RX_O)"

if {![ptp_ha::recv_pdelay_req \
           -port_num                 $tee_port_1\
           -domain_number            $domain\
           -filter_id                $filter_id\
           -error_reason             error_reason\
           -recvd_correction_field   CF2]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(PDELAY_REQ_RX_F) $error_reason"\
                        -tc_def  $tc_def

    return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 20: Check whether CF2 = -DA2 (with margin of error).                   #
###############################################################################

STEP 20

LOG -level 0 -msg "$dut_log_msg(CF2_DA2_V)"

set DA_CAL [format "%.3f" [expr (-1.0 * $DA2)]]

ptp_ha::value_range $CF2 $ptp_ha_margin_of_error da_cal_min da_cal_max

LOG -level 3 -msg "Value of correctionField (CF2)       = $CF2 ns"
LOG -level 3 -msg "Value of delayAsymmetry ((-1) * DA2) = $DA_CAL ns (Acceptable Range: $da_cal_min ns to $da_cal_max ns)"

if {!(($CF2 >= $da_cal_min) && ($CF2 <= $da_cal_max))} {

if {$SKIP_VALIDATION != 1} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CF2_DA2_F)"
                       -tc_def  $tc_def
    return 0
}
}

ptp_ha::stop_at_step

###############################################################################
# Step 21: Configure constantAsymmetry on port P1 by setting constantAsymmetry#
#          to 4 294 967 296 ns (i.e., the value of dataset expressed in       #
#          TimeInterval asymmetryCorrectionPortDS.constantAsymmetry = 2^48).  #
###############################################################################

STEP 21

set constant_asymmetry 4294967296

LOG -level 0 -msg "$dut_log_msg(CONFIG_ASC.CONSTANTASYMMETRY)"

if {![ptp_ha::dut_set_constant_asymmetry \
             -port_num            $dut_port_1\
             -constant_asymmetry  $constant_asymmetry]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_ASC.CONSTANTASYMMETRY_F)"\
                        -tc_def  $tc_def

     return 0
}

ptp_ha::stop_at_step

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 22: Observe that DUT transmits PDELAY_REQ message on the port P1 with  #
#          following parameters. Store Correction Field, Sequence ID, Source  #
#          Port Identity as PDREQ_CF, D and E respectively.                   #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type              = 0x02                           #
#                  Domain Number             = DN1                            #
#                  Correction Field          = PDREQ_CF                       #
#                  Sequence ID               = D                              #
#                  Source Port Identity      = E                              #
#                                                                             #
###############################################################################

STEP 22

LOG -level 0 -msg "$tee_log_msg(PDELAY_REQ_RX_O)"

if {![ptp_ha::recv_pdelay_req \
           -port_num                  $tee_port_1\
           -domain_number             $domain\
           -filter_id                 $filter_id\
           -recvd_correction_field    PDREQ_CF\
           -recvd_src_port_number     src_port_number\
           -error_reason              error_reason\
           -recvd_src_clock_identity  src_clock_identity\
           -recvd_sequence_id         sequence_id]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(PDELAY_REQ_RX_F) $error_reason"\
                        -tc_def  $tc_def

    return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 23: Get portDS.meanLinkDelay (MLD3) value for Port P1 in DUT.          #
###############################################################################

STEP 23

LOG -level 0 -msg "$dut_log_msg(GET_MEAN_LINK_DELAY_MLD3)"

if {![ptp_ha::dut_get_mean_link_delay\
          -port_num         $dut_port_1\
          -mean_link_delay  MLD3]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(GET_MEAN_LINK_DELAY_MLD3_F)"\
                       -tc_def  $tc_def

    return 0

}

ptp_ha::stop_at_step

set MLD3 [format "%.3f" $MLD3]

###############################################################################
# Step 24: Get delayAsymmetry (DA3) value for Port P1 in DUT.                 #
###############################################################################

STEP 24

LOG -level 0 -msg "$dut_log_msg(GET_DELAY_ASYMMETRY_DA3)"

if {![ptp_ha::dut_get_delay_asymmetry \
              -port_num          $dut_port_1\
              -delay_asymmetry   DA3]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(GET_DELAY_ASYMMETRY_DA3_F)"\
                        -tc_def  $tc_def
     return 0
}

ptp_ha::stop_at_step

set DA3 [format "%.3f" $DA3]

###############################################################################
# Step 25: Check whether DA3 = (1/3)*MLD2 + 4 294 967 296 ns (i.e., 2^(32+16) #
#          in TimeInterval) (with margin of error).                           #
###############################################################################

STEP 25

LOG -level 0 -msg "$dut_log_msg(CHECK_EXPRESSION_DA3)"

set DA_CAL  [format "%.3f" [expr (1.0/3.0)*$MLD2]]
set DA_CAL  [format "%.3f" [expr $DA_CAL + 4294967296]]

ptp_ha::value_range $DA3 $ptp_ha_margin_of_error da_cal_min da_cal_max

LOG -level 3 -msg "Value of DA3       = $DA3 ns"
LOG -level 3 -msg "Value of (1/3)*MD2 + 4294967296 = $DA_CAL ns (Acceptable Range: $da_cal_min ns to $da_cal_max ns)"

if {!(($DA3 >= $da_cal_min) && ($DA3 <= $da_cal_max))} {

if {$SKIP_VALIDATION != 1} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_EXPRESSION_F)"\
                       -tc_def  $tc_def
     return 0
}
}

ptp_ha::stop_at_step

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 26: Observe that DUT transmits PDELAY_REQ on the port P1 with following#
#          parameters and record correction field (CF3).                      #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type             = 0x01                            #
#                  Domain Number            = DN1                             #
###############################################################################

STEP 26

LOG -level 0 -msg "$tee_log_msg(PDELAY_REQ_RX_O)"

if {![ptp_ha::recv_pdelay_req \
           -port_num                $tee_port_1\
           -domain_number           $domain\
           -filter_id               $filter_id\
           -error_reason            error_reason\
           -recvd_correction_field  CF3]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(PDELAY_REQ_RX_F) $error_reason"\
                        -tc_def  $tc_def

    return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 27: Check whether CF3 = -DA3 (with margin of error).                   #
###############################################################################

STEP 27

LOG -level 0 -msg "$dut_log_msg(CF3_DA3_V)"

set DA_CAL [format "%.3f" [expr (-1.0 * $DA3)]]

ptp_ha::value_range $CF3 $ptp_ha_margin_of_error da_cal_min da_cal_max

LOG -level 3 -msg "Value of correctionField (CF3)       = $CF3 ns"
LOG -level 3 -msg "Value of delayAsymmetry ((-1) * DA3) = $DA_CAL ns (Acceptable Range: $da_cal_min ns to $da_cal_max ns)"

if {!(($CF3 >= $da_cal_min) && ($CF3 <= $da_cal_max))} {

if {$SKIP_VALIDATION != 1} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CF3_DA3_F)"
                       -tc_def  $tc_def
    return 0
}
}

ptp_ha::stop_at_step

###############################################################################
# Step 28: Configure scaledDelayCoefficient on port P1 by setting             #
#          scaledDelayCoefficient to -1 (i.e., the value of dataset expressed #
#          in RelativeDifference asymmetryCorrectionPortDS.                   #
#          scaledDelayCoefficient = -2^62).                                   #
###############################################################################

STEP 28

set delay_coefficient -1

LOG -level 0 -msg "$dut_log_msg(CONFIG_ASC.DELAYCOEFFICIENT)"

if {![ptp_ha::dut_set_scaled_delay_coefficient\
             -port_num             $dut_port_1\
             -delay_coefficient    $delay_coefficient]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_ASC.DELAYCOEFFICIENT_F)"\
                        -tc_def  $tc_def

     return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 29: Configure constantAsymmetry on port P1 by setting constantAsymmetry#
#          to 0 (i.e., the value of dataset expressed in TimeInterval         #
#          asymmetryCorrectionPortDS.constantAsymmetry = 0).                  #
###############################################################################

STEP 29

LOG -level 0 -msg "$dut_log_msg(CONFIG_ASC.CONSTANTASYMMETRY)"

set constant_asymmetry 0

if {![ptp_ha::dut_set_constant_asymmetry\
              -port_num             $dut_port_1\
              -constant_asymmetry   $constant_asymmetry]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_ASC.CONSTANTASYMMETRY_F)"\
                       -tc_def  $tc_def
    return 0
}

ptp_ha::stop_at_step

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 30: Observe that DUT transmits PDELAY_REQ message on the port P1 with  #
#          following parameters. Store Correction Field, Sequence ID, Source  #
#          Port Identity as PDREQ_CF, D and E respectively.                   #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type              = 0x02                           #
#                  Domain Number             = DN1                            #
#                  Correction Field          = PDREQ_CF                       #
#                  Sequence ID               = D                              #
#                  Source Port Identity      = E                              #
###############################################################################

STEP 30

LOG -level 0 -msg "$tee_log_msg(PDELAY_REQ_RX_O)"

if {![ptp_ha::recv_pdelay_req \
           -port_num                  $tee_port_1\
           -domain_number             $domain\
           -recvd_src_port_number     src_port_number\
           -recvd_src_clock_identity  src_clock_identity\
           -recvd_sequence_id         sequence_id\
           -error_reason              error_reason\
           -recvd_correction_field    PDREQ_CF\
           -filter_id                 $filter_id]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(PDELAY_REQ_RX_F) $error_reason"\
                        -tc_def  $tc_def

    return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 31: Get portDS.meanLinkDelay (MLD4) value for Port P1 in DUT.          #
###############################################################################

STEP 31

LOG -level 0 -msg "$dut_log_msg(GET_MEAN_LINK_DELAY_MLD4)"

if {![ptp_ha::dut_get_mean_link_delay\
          -port_num         $dut_port_1\
          -mean_link_delay  MLD4]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(GET_MEAN_LINK_DELAY_MLD4_F)"\
                       -tc_def  $tc_def

    return 0

}

ptp_ha::stop_at_step

set MLD4 [format "%.3f" $MLD4]

###############################################################################
# Step 32: Get delayAsymmetry (DA4) value for Port P1 in DUT.                 #
###############################################################################

STEP 32

LOG -level 0 -msg "$dut_log_msg(GET_DELAY_ASYMMETRY_DA4)"

if {![ptp_ha::dut_get_delay_asymmetry \
              -port_num          $dut_port_1\
              -delay_asymmetry   DA4]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(GET_DELAY_ASYMMETRY_DA4_F)"\
                        -tc_def  $tc_def
     return 0
}

ptp_ha::stop_at_step

set DA4 [format "%.3f" $DA4]

###############################################################################
# Step 33: Check whether DA4 = (-1)*MLD4 (with margin of error).              #
###############################################################################

STEP 33

LOG -level 0 -msg "$dut_log_msg(CHECK_EXPRESSION_DA4)"

set DA_CAL [format "%.3f" [expr (-1.0 * $MLD4)]]

ptp_ha::value_range $DA4 $ptp_ha_margin_of_error da_cal_min da_cal_max

LOG -level 3 -msg "Value of DA4      = $DA4 ns"
LOG -level 3 -msg "Value of DA_CAL   = $DA_CAL ns (Acceptable Range: $da_cal_min ns to $da_cal_max ns)"

if {!(($DA4 >= $da_cal_min) && ($DA4 <= $da_cal_max))} {

if {$SKIP_VALIDATION != 1} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_EXPRESSION_F)"\
                       -tc_def  $tc_def
     return 0
}
}

ptp_ha::stop_at_step

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 34: Observe that DUT transmits PDELAY_REQ on the port P1 with following#
#          parameters and record correction field (CF4).                      #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type             = 0x01                            #
#                  Domain Number            = DN1                             #
###############################################################################

STEP 34

LOG -level 0 -msg "$tee_log_msg(PDELAY_REQ_RX_O)"

if {![ptp_ha::recv_pdelay_req \
           -port_num                 $tee_port_1\
           -domain_number            $domain\
           -recvd_correction_field   CF4\
           -error_reason             error_reason\
           -filter_id                $filter_id]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(PDELAY_REQ_RX_F) $error_reason"\
                        -tc_def  $tc_def

    return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 35: Check whether CF4 = -DA4 (with margin of error).                   #
###############################################################################

STEP 35

LOG -level 0 -msg "$dut_log_msg(CF4_DA4_V)"

set DA_CAL [format "%.3f" [expr (-1.0 * $DA4)]]

ptp_ha::value_range $CF4 $ptp_ha_margin_of_error da_cal_min da_cal_max

LOG -level 3 -msg "Value of correctionField (CF4)       = $CF4 ns"
LOG -level 3 -msg "Value of delayAsymmetry ((-1) * DA4) = $DA_CAL ns (Acceptable Range: $da_cal_min ns to $da_cal_max ns)"

if {!(($CF4 >= $da_cal_min) && ($CF4 <= $da_cal_max))} {

if {$SKIP_VALIDATION != 1} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CF4_DA4_F)"\
                       -tc_def  $tc_def
    return 0
}
}

ptp_ha::stop_at_step

###############################################################################
# Step 36: Disable asymmetryCorrectionPortDS.enable.                          #
#                                                                             #
# Note: The asymmetryCorrectionPortDS.enable is not allowed to be set to FALSE#
#       in the High Accuracy Default PTP Profile. Despite providing FALSE     #
#       value of this data set member to the DUT, it is expected that the     #
#       value of asymmetryCorrectionPortDS.enable remains TRUE and the        #
#       measurements of meanDelay and delayAsymmetry are made accordingly.    #
###############################################################################

STEP 36

LOG -level 0 -msg "$dut_log_msg(ACP.DISABLE)"

if {[ptp_ha::dut_reset_asymmetry_correction\
                -port_num      $dut_port_1]} {

    puts ".\n"
    puts "???????????????????????????????????????????????????????????????????????????"
    puts "???????????????????????????????????????????????????????????????????????????"
    puts "?? NOTE: asymmetryCorrectionPortDS.enable is not allowed to set to FALSE.??"
    puts "??                                                                       ??"
    puts "?? The asymmetryCorrectionPortDS.enable is not allowed to be set to FALSE??"
    puts "?? in the High Accuracy Default PTP Profile. Despite providing FALSE     ??"
    puts "?? value of this data set member to the DUT, it is expected that the     ??"
    puts "?? value of asymmetryCorrectionPortDS.enable remains TRUE and the        ??"
    puts "?? measurements of meanDelay and delayAsymmetry are made accordingly.    ??"
    puts "??                                                                       ??"
    puts "???????????????????????????????????????????????????????????????????????????"
    puts "???????????????????????????????????????????????????????????????????????????"
    puts ".\n"
}

ptp_ha::stop_at_step

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 37: Observe that DUT transmits PDELAY_REQ message on the port P1 with  #
#          following parameters. Store Correction Field, Sequence ID, Source  #
#          Port Identity as PDREQ_CF, D and E respectively.                   #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type              = 0x02                           #
#                  Domain Number             = DN1                            #
#                  Correction Field          = PDREQ_CF                       #
#                  Sequence ID               = D                              #
#                  Source Port Identity      = E                              #
#                                                                             #
###############################################################################

STEP 37

LOG -level 0 -msg "$tee_log_msg(PDELAY_REQ_RX_O)"

if {![ptp_ha::recv_pdelay_req \
           -port_num                  $tee_port_1\
           -domain_number             $domain\
           -filter_id                 $filter_id\
           -error_reason              error_reason\
           -recvd_src_port_number     src_port_number\
           -recvd_correction_field    PDREQ_CF\
           -recvd_src_clock_identity  src_clock_identity\
           -recvd_sequence_id         sequence_id]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(PDELAY_REQ_RX_F) $error_reason"\
                        -tc_def  $tc_def

    return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 38: Get portDS.meanLinkDelay (MLD5) value for Port P1 in DUT.          #
###############################################################################

STEP 38

LOG -level 0 -msg "$dut_log_msg(GET_MEAN_LINK_DELAY_MLD5)"

if {![ptp_ha::dut_get_mean_link_delay\
          -port_num         $dut_port_1\
          -mean_link_delay  MLD5]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(GET_MEAN_LINK_DELAY_MLD5_F)"\
                       -tc_def  $tc_def

    return 0

}

ptp_ha::stop_at_step

set MLD4 [format "%.3f" $MLD4]

###############################################################################
# Step 39: Get delayAsymmetry (DA5) value for Port P1 in DUT.                 #
###############################################################################

STEP 39

LOG -level 0 -msg "$dut_log_msg(GET_DELAY_ASYMMETRY_DA5)"

if {![ptp_ha::dut_get_delay_asymmetry \
              -port_num          $dut_port_1\
              -delay_asymmetry   DA5]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(GET_DELAY_ASYMMETRY_DA5_F)"\
                        -tc_def  $tc_def
     return 0
}

ptp_ha::stop_at_step

set DA5 [format "%.3f" $DA5]

###############################################################################
# Step 40: Check whether DA5 = (-1)*MLD5 (with margin of error).              #
###############################################################################

STEP 40

LOG -level 0 -msg "$dut_log_msg(CHECK_EXPR_DA5_MLD)"

set DA_CAL [format "%.3f" [expr (-1.0 * $MLD5)]]

ptp_ha::value_range $DA5 $ptp_ha_margin_of_error da_cal_min da_cal_max

LOG -level 3 -msg "Value of DA5       = $DA5 ns"
LOG -level 3 -msg "Value of (-1)*MLD5 = $DA_CAL ns (Acceptable Range: $da_cal_min ns to $da_cal_max ns)"

if {!(($DA5 >= $da_cal_min) && ($DA5 <= $da_cal_max))} {

if {$SKIP_VALIDATION != 1} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_EXPR_DA5_MLD_F)"\
                       -tc_def  $tc_def
     return 0
}
}

ptp_ha::stop_at_step

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 41: Observe that DUT transmits PDELAY_REQ on the port P1 with following#
#          parameters and record correction field (CF5).                      #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type             = 0x01                            #
#                  Domain Number            = DN1                             #
###############################################################################

STEP 41

LOG -level 0 -msg "$tee_log_msg(PDELAY_REQ_RX_O)"

if {![ptp_ha::recv_pdelay_req \
           -port_num                 $tee_port_1\
           -domain_number            $domain\
           -recvd_correction_field   CF5\
           -error_reason             error_reason\
           -filter_id                $filter_id]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(PDELAY_REQ_RX_F) $error_reason"\
                        -tc_def  $tc_def

    return 0
}

ptp_ha::stop_at_step

###############################################################################
# Step 42: Verify whether CF5 = (-1)*DA5 (with margin of error).              #
###############################################################################

STEP 42

LOG -level 0 -msg "$dut_log_msg(CF5_DA5_V)"

set DA_CAL [format "%.3f" [expr (-1.0 * $DA5)]]

ptp_ha::value_range $CF5 $ptp_ha_margin_of_error da_cal_min da_cal_max

LOG -level 3 -msg "Value of CF5      = $CF5 ns"
LOG -level 3 -msg "Value of (-1)*DA5 = $DA_CAL ns (Acceptable Range: $da_cal_min ns to $da_cal_max ns)"

if {!(($CF5 >= $da_cal_min) && ($CF5 <= $da_cal_max))} {


    TEE_CLEANUP

    TC_CLEAN_AND_FAIL   -reason "$dut_log_msg(CF5_DA5_F)"\
                        -tc_def $tc_def
    return 0
}

TEE_CLEANUP

TC_CLEAN_AND_PASS -reason  "DUT performs computation of delayAsymmetry\
                            each time the value of meanDelay is updated in\
                            Peer to Peer Delay mechanism when DUT is master."\
                  -tc_def   $tc_def

return 1

################################ END OF TEST CASE #############################
