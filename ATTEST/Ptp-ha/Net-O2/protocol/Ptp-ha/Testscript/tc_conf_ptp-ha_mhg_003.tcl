#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_ptp-ha_mhg_003                                  #
# Test Case Version : 1.4                                                     #
# Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    #
# Module Name       : PTP-HA Message Handling Group (MHG)                     #
#                                                                             #
# Title             : Discarding of PTP signaling message with L1 Sync TLV    #
#                     with invalid domain number.                             #
#                                                                             #
# Purpose           : To verify that a PTP enabled device does not accept PTP #
#                     signaling message with L1 Sync TLV and invalid domain   #
#                     number.                                                 #
#                                                                             #
# Reference         : P1588/D1.3, February 2018 V3.01 Clause O.7.2 Page 449   #
#                     Clause J.5.2 Page 412.                                  #
#                                                                             #
# Conformance Type  : SHALL                                                   #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 001                                                     #
# Test Topology     : 001                                                     #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#          TEE                                                   DUT          #
#        [slave]                                              [master]        #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode - one-step/two-step> |           #
#           |                     <Configure logAnnounceInterval, | P1        #
#           |                             announceReceiptTimeout> |           #
#           |                         <Configure logSyncInterval> | P1        #
#           |                  <Configure logMinDelayReqInterval> | P1        #
#           |                    <Configure Priority1, Priority2> | P1        #
#           |                                     <Enable L1SYNC> | P1        #
#           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        #
#           |                     <Disable L1SynOptParams option> | P1        #
#           |           <Enable asymmetryCorrectionPortDS.enable> | P1        #
#           | <Configure timestampCorrectionPortDS.egressLatency, | P1        #
#           |           timestampCorrectionPortDS.ingressLatency, |           #
#           |        asymmetryCorrectionPortDS.constantAsymmetry, |           #
#           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                     DN = DN1, SEQ_ID = Y, PRI1 = X] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | ANNOUNCE [MSG_TYPE = 0x0B,                          |           #
#           | DN = DN1, SEQ_ID = A, PRI1 = X+1]                   |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                   <Check L1SYNC port status - IDLE> | P1        #
#           |                                                     |           #
#           |                      PTP SIGNALING with L1 Sync TLV |           #
#           |                          [MSG_TYPE = 0xC, DN = DN1, |           #
#           |                                  TLV_TYPE = 0x8001] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | PTP SIGNALING with L1 Sync TLV                      |           #
#           | [MSG_TYPE = 0xC, DN = DN1,                          |           #
#           | TLV_TYPE = 0x8001, TCR = 0, RCR = 0,                |           #
#           | CR = 0, ITC = 0, IRC = 0, IC = 0]                   |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |   <Wait for the expiry of L1SyncReceiptTimeout>     |           #
#           |                                                     |           #
#           |             <Check L1SYNC port status - LINK_ALIVE> | P1        #
#           |                                                     |           #
#           | PTP SIGNALING with L1 Sync TLV                      |           #
#           | [MSG_TYPE = 0xC, DN = 128,                          |           #
#           | TLV_TYPE = 0x8001, TCR = 0, RCR = 0,                |           #
#           | CR = 0, ITC = 0, IRC = 0, IC = 0]                   |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |       <Wait for the expiry of twice                 |           #
#           |                    the L1SyncReceiptTimeout>        |           #
#           |                                                     |           #
#           |                   <Check L1SYNC port status - IDLE> | P1        #
#           |                                                     |           #
#           |                      <Configure Domain Number = 127>|           #
#           |                                                     |           #
#           | PTP SIGNALING with L1 Sync TLV                      |           #
#           | [MSG_TYPE = 0xC, DN = 127,                          |           #
#           | TLV_TYPE = 0x8001, TCR = 0, RCR = 0,                |           #
#           | CR = 0, ITC = 0, IRC = 0, IC = 0]                   |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |   <Wait for the expiry of L1SyncReceiptTimeout>     |           #
#           |                                                     |           #
#           |             <Check L1SYNC port status - LINK_ALIVE> | P1        #
#           |                                                     |           #
#           | PTP SIGNALING with L1 Sync TLV                      |           #
#           | [MSG_TYPE = 0xC, DN = 128,                          |           #
#           | TLV_TYPE = 0x8001, TCR = 0, RCR = 0,                |           #
#           | CR = 0, ITC = 0, IRC = 0, IC = 0]                   |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |       <Wait for the expiry of twice                 |           #
#           |                    the L1SyncReceiptTimeout>        |           #
#           |                                                     |           #
#           |                   <Check L1SYNC port status - IDLE> | P1        #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     MSG_TYPE  = Message Type                                                #
#     DN        = Domain Number                                               #
#     TCR       = txCoherentIsRequired                                        #
#     RCR       = rxCoherentIsRequired                                        #
#     CR        = congruentIsRequired                                         #
#     ITC       = peerIsTxCoherent                                            #
#     IRC       = peerIsRxCoherent                                            #
#     IC        = peerIsCongruent                                             #
#                                                                             #
# NOTE :                                                                      #
#                                                                             #
#  1. This objective is verified using the High Accuracy Delay Request-       #
#     Response Default PTP Profile                                            #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.ingressLatency,           #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
#                  Sequence ID   = Y                                          #
#                  Priority1     = X                                          #
#                                                                             #
# Step 4 : Send periodic ANNOUNCE message with Priority1 value incremented    #
#          from the Priority1 value of received Announce message on port T1   #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
#                  Sequence ID   = A                                          #
#                  Priority1     = X+1                                        #
#                                                                             #
# Step 5 : Observe that the DUT's L1SYNC port status P1 is in IDLE state.     #
#                                                                             #
# Step 6 : Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV  #
#          on the port P1 with following parameters:                          #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                                                                             #
# Step 7 : Send PTP SIGNALING message with L1 Sync TLV on the port P1 with    #
#          following parameters:                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 0                                          #
#                  RCR           = 0                                          #
#                  CR            = 0                                          #
#                  ITC           = 0                                          #
#                  IRC           = 0                                          #
#                  IC            = 0                                          #
#                                                                             #
# Step 8 : Wait for the expiry of L1SyncReceiptTimeout.                       #
#                                                                             #
# Step 9 : Observe that the DUT's L1SYNC port status P1 is in LINK_ALIVE state#
#                                                                             #
# Step 10: Send PTP SIGNALING message with L1 Sync TLV on the port P1 with    #
#          following parameters:                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = 128                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 0                                          #
#                  RCR           = 0                                          #
#                  CR            = 0                                          #
#                  ITC           = 0                                          #
#                  IRC           = 0                                          #
#                  IC            = 0                                          #
#                                                                             #
# Step 11: Wait for the expiry of twice the L1SyncReceiptTimeout.             #
#                                                                             #
# Step 12: Verify that the DUT's L1SYNC port status P1 is in IDLE state.      #
#                                                                             #
# Step 13: Configure domain number as 127 on Port P1 in DUT.                  #
#                                                                             #
# Step 14: Send PTP SIGNALING message with L1 Sync TLV on the port P1 with    #
#          following parameters:                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = 127                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 0                                          #
#                  RCR           = 0                                          #
#                  CR            = 0                                          #
#                  ITC           = 0                                          #
#                  IRC           = 0                                          #
#                  IC            = 0                                          #
#                                                                             #
# Step 15: Wait for the expiry of L1SyncReceiptTimeout.                       #
#                                                                             #
# Step 16: Observe that the DUT's L1SYNC port status P1 is in LINK_ALIVE state#
#                                                                             #
# Step 17: Send PTP SIGNALING message with L1 Sync TLV on the port P1 with    #
#          following parameters:                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = 128                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 0                                          #
#                  RCR           = 0                                          #
#                  CR            = 0                                          #
#                  ITC           = 0                                          #
#                  IRC           = 0                                          #
#                  IC            = 0                                          #
#                                                                             #
# Step 18: Wait for the expiry of twice the L1SyncReceiptTimeout.             #
#                                                                             #
# Step 19 : Verify that the DUT's L1SYNC port status P1 is in IDLE state.     #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          May/2018      CERN          Initial                            #
# 1.1          Jun/2018      CERN          Fixed domain in dut_reset_domain   #
# 1.2          Jul/2018      CERN          Added ptp_ha::display_port_details #
#                                          to print port details              #
# 1.3          Sep/2018      CERN          a) Removed destination MAC and IP  #
#                                             in send functions.              #
#                                          b) Removed timeout in recv         #
#                                             functions.                      #
#                                          c) Moved timeout calculation to    #
#                                             PTP-HA global file.             #
#                                          d) Added wait to ensure state      #
#                                             transitions.                    #
# 1.4          Dec/2018      CERN          Reduced timeouts for ensuring the  #
#                                          change in domain numbers.          #
#                                                                             #
###############################################################################
# Copyright (c) 2018 CERN                                                     #
###############################################################################

global dut_log_msg
global tee_log_msg
global session_id

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def   "To verify that a PTP enabled device does not accept PTP\
              signaling message with L1 Sync TLV and invalid domain\
              number"

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set dut_port_1                     $::dut_port_num_1
set vlan_id                        $::ptp_ha_vlan_id
set dut_ip                         $::dut_test_port_ip
set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT
set tlv_type(l1sync)               $::TLV_TYPE(L1SYNC)

set domain                         $::DEFAULT_DOMAIN
set infinity                       $::INFINITY
set gmid                           $::GRANDMASTER_ID

set link_alive                     $::PTP_L1SYNC_STATE(LINK_ALIVE)
set idle                           $::PTP_L1SYNC_STATE(IDLE)
set sequence_id_ann                1
set sequence_id_l1sync             43692

set tcr_false       0
set rcr_false       0
set cr_false        0
set itc_false       0
set irc_false       0
set ic_false        0

########################### END - INITIALIZATION ##############################

ptp_ha::display_port_details

ptp_ha::dut_Initialization

proc cleanup {} {

    ptp_ha::dut_cleanup_setup_001

    ptp_ha::dut_reset_domain \
           -clock_mode     $::ptp_dut_clock_step\
           -domain         $::DEFAULT_DOMAIN\

}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

### (Initial Part) ###

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.egressLatency,            #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
###############################################################################

STEP 1

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP1)"

if {![ptp_ha::dut_configure_setup_001]} {

    LOG -msg "$dut_log_msg(INIT_SETUP1_F)"

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP1_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
###############################################################################

STEP 2

LOG -level 0 -msg "$tee_log_msg(INIT)"

ptp_ha::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

    if {![PTP_HA_UNICAST_PRE_REQUISITE_CONFIGURATION\
                        -port_num       $tee_port_1\
                        -tee_mac        $tee_mac\
                        -tee_ip         $tee_ip\
                        -dut_ip         $dut_ip\
                        -vlan_id        $vlan_id\
                        -dut_mac        tee_dest_mac\
                        -timeout        $arp_timeout]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
                           -tc_def  $tc_def

        return 0
    }

}

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_1\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def  $tc_def

    return 0
}

set filter_id [lindex $id1 end]

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 3 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
#                  Sequence ID   = Y                                          #
#                  Priority1     = X                                          #
###############################################################################

STEP 3

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
           -port_num                $tee_port_1\
           -domain_number           $domain\
           -filter_id               $filter_id\
           -recvd_gm_priority1      priority1]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                        -tc_def  $tc_def

    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 4 : Send periodic ANNOUNCE message with Priority1 value incremented    #
#          from the Priority1 value of received Announce message on port T1   #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
#                  Sequence ID   = A                                          #
#                  Priority1     = X+1                                        #
###############################################################################

STEP 4

set priority [expr $priority1 + 1]

LOG -level 0 -msg "$tee_log_msg(ANN_TX_T1)"

if {![ptp_ha::send_announce \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -gm_priority1            $priority\
           -src_ip                  $tee_ip\
           -domain_number           $domain\
           -sequence_id             $sequence_id_ann\
           -count                   10\
           -gmid                    $gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANN_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

sleep $::ptp_ha_bmca

###############################################################################
# Step 5 : Observe that the DUT's L1SYNC port status P1 is in IDLE state.     #
###############################################################################

STEP 5

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_IDLE_P1_O)"

if {![ptp_ha::dut_check_l1sync_state \
              -port_num                $dut_port_1\
              -l1sync_state            $idle] } {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_IDLE_P1_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 6 : Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV  #
#          on the port P1 with following parameters:                          #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
###############################################################################

STEP 6

LOG -level 0 -msg "$dut_log_msg(L1SYNC_RX_P1_O)"

  if {![ptp_ha::recv_signal \
                -port_num                $tee_port_1\
                -domain_number           $domain\
                -tlv_type                $tlv_type(l1sync)\
                -filter_id               $filter_id]} {

      TEE_CLEANUP

      TC_CLEAN_AND_ABORT -reason "$dut_log_msg(L1SYNC_RX_P1_F)"\
                         -tc_def  $tc_def

      return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 7 : Send PTP SIGNALING message with L1 Sync TLV on the port P1 with    #
#          following parameters:                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 0                                          #
#                  RCR           = 0                                          #
#                  CR            = 0                                          #
#                  ITC           = 0                                          #
#                  IRC           = 0                                          #
#                  IC            = 0                                          #
###############################################################################

STEP 7

LOG -level 0 -msg  "$dut_log_msg(L1SYNC_TX_T1)"

if {![ptp_ha::send_signal \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -tlv_type                $tlv_type(l1sync)\
           -domain_number           $domain\
           -tcr                     $tcr_false\
           -rcr                     $rcr_false\
           -cr                      $cr_false\
           -itc                     $itc_false\
           -irc                     $irc_false\
           -ic                      $ic_false\
           -count                   $infinity\
           -sequence_id             $sequence_id_l1sync]} {

    TEE_CLEANUP
    
    TC_CLEAN_AND_ABORT   -reason "$dut_log_msg(L1SYNC_TX_T1_F)"\
                         -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 8 : Wait for the expiry of L1SyncReceiptTimeout.                       #
###############################################################################

STEP 8

LOG -level 0 -msg "$dut_log_msg(WAIT_L1SYNCRECEIPTTIMEOUT)"

sleep $::ptp_ha_l1sync_timeout_d

###############################################################################
# Step 9 : Observe that the DUT's L1SYNC port status P1 is in LINK_ALIVE state#
###############################################################################

STEP 9

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_LINK_ALIVE_P1_O)"

if {![ptp_ha::dut_check_l1sync_state \
              -port_num                $dut_port_1\
              -l1sync_state            $link_alive]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_LINK_ALIVE_P1_F)"\
                       -tc_def  $tc_def
    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 10: Send PTP SIGNALING message with L1 Sync TLV on the port P1 with    #
#          following parameters:                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = 128                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 0                                          #
#                  RCR           = 0                                          #
#                  CR            = 0                                          #
#                  ITC           = 0                                          #
#                  IRC           = 0                                          #
#                  IC            = 0                                          #
###############################################################################

STEP 10

LOG -level 0 -msg  "$dut_log_msg(L1SYNC_TX_T1)"

if {![ptp_ha::send_signal \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -tlv_type                $tlv_type(l1sync)\
           -domain_number           128\
           -tcr                     $tcr_false\
           -rcr                     $rcr_false\
           -cr                      $cr_false\
           -itc                     $itc_false\
           -irc                     $irc_false\
           -ic                      $ic_false\
           -count                   $infinity\
           -sequence_id             $sequence_id_l1sync]} {
    TEE_CLEANUP

    TC_CLEAN_AND_ABORT   -reason "$dut_log_msg(L1SYNC_TX_T1_F)"\
                         -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 11: Wait for the expiry of twice the L1SyncReceiptTimeout.             #
###############################################################################

STEP 11

LOG -level 0 -msg "$dut_log_msg(WAIT_TWICE_L1SYNCRECEIPTTIMEOUT)"

sleep $::ptp_ha_l1sync_timeout_twice

###############################################################################
# Step 12 : Verify that the DUT's L1SYNC port status P1 is in IDLE state.     #
#                                                                             #
###############################################################################

STEP 12

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_IDLE_P1_V)"

if {![ptp_ha::dut_check_l1sync_state \
              -port_num                $dut_port_1\
              -l1sync_state            $idle] } {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_IDLE_P1_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 13: Configure domain number as 127 on Port P1 in DUT.                  #
#                                                                             #
###############################################################################

STEP 13

LOG -level 0 -msg "$dut_log_msg(CONFIGURE_DN)"

if {![ptp_ha::dut_set_domain \
           -clock_mode     $::ptp_dut_clock_step\
           -domain         127]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIGURE_DN_F)"\
                        -tc_def  $tc_def
     return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 14: Send PTP SIGNALING message with L1 Sync TLV on the port P1 with    #
#          following parameters:                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = 127                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 0                                          #
#                  RCR           = 0                                          #
#                  CR            = 0                                          #
#                  ITC           = 0                                          #
#                  IRC           = 0                                          #
#                  IC            = 0                                          #
#                                                                             #
###############################################################################

STEP 14

LOG -level 0 -msg  "$dut_log_msg(L1SYNC_TX_T1)"

if {![ptp_ha::send_signal \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -tlv_type                $tlv_type(l1sync)\
           -domain_number           127\
           -tcr                     $tcr_false\
           -rcr                     $rcr_false\
           -cr                      $cr_false\
           -itc                     $itc_false\
           -irc                     $irc_false\
           -ic                      $ic_false\
           -count                   $infinity\
           -sequence_id             $sequence_id_l1sync]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT   -reason "$dut_log_msg(L1SYNC_TX_T1_F)"\
                         -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 15: Wait for the expiry of L1SyncReceiptTimeout.                       #
###############################################################################

STEP 15

LOG -level 0 -msg "$dut_log_msg(WAIT_L1SYNCRECEIPTTIMEOUT)"

sleep $::ptp_ha_l1sync_timeout_d

###############################################################################
# Step 16: Observe that the DUT's L1SYNC port status P1 is in LINK_ALIVE state#
#                                                                             #
###############################################################################

STEP 16

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_LINK_ALIVE_P1_V)"

if {![ptp_ha::dut_check_l1sync_state \
              -port_num                $dut_port_1\
              -l1sync_state            $link_alive] } {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_LINK_ALIVE_P1_F)"\
                       -tc_def  $tc_def
    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 17: Send PTP SIGNALING message with L1 Sync TLV on the port P1 with    #
#          following parameters:                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = 128                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 0                                          #
#                  RCR           = 0                                          #
#                  CR            = 0                                          #
#                  ITC           = 0                                          #
#                  IRC           = 0                                          #
#                  IC            = 0                                          #
#                                                                             #
###############################################################################

STEP 17

LOG -level 0 -msg  "$dut_log_msg(L1SYNC_TX_T1)"

if {![ptp_ha::send_signal \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -tlv_type                $tlv_type(l1sync)\
           -domain_number           128\
           -tcr                     $tcr_false\
           -rcr                     $rcr_false\
           -cr                      $cr_false\
           -itc                     $itc_false\
           -irc                     $irc_false\
           -ic                      $ic_false\
           -count                   $infinity\
           -sequence_id             $sequence_id_l1sync]} {
    TEE_CLEANUP

    TC_CLEAN_AND_ABORT   -reason "$dut_log_msg(L1SYNC_TX_T1_F)"\
                         -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 18: Wait for the expiry of twice the L1SyncReceiptTimeout.             #
#                                                                             #
###############################################################################

STEP 18

LOG -level 0 -msg "$dut_log_msg(WAIT_TWICE_L1SYNCRECEIPTTIMEOUT)"

sleep $::ptp_ha_l1sync_timeout_twice

###############################################################################
# Step 19 : Verify that the DUT's L1SYNC port status P1 is in IDLE state.     #
#                                                                             #
###############################################################################

STEP 19

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_IDLE_P1_V)"

if {![ptp_ha::dut_check_l1sync_state \
              -port_num             $dut_port_1\
              -l1sync_state         $idle] } {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "PTP enabled device accepts PTP\
                                signaling message with L1 Sync TLV and invalid domain\
                                number"\
                       -tc_def  $tc_def
    return 0
}

TEE_CLEANUP

TC_CLEAN_AND_PASS -reason  "PTP enabled device does not accept PTP\
                            signaling message with L1 Sync TLV and invalid domain\
                            number"\
                  -tc_def   $tc_def

return 1

############################# END OF TEST CASE #################################
