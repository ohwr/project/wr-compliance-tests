#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_ptp-ha_pcg_002                                  #
# Test Case Version : 1.2                                                     #
# Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    #
# Module Name       : PTP-HA Configuration Group (PCG)                        #
#                                                                             #
# Title             : Domain Number                                           #
#                                                                             #
# Purpose           : To verify that a PTP enabled device supports to         #
#                     configure domain number in range 0 to 127.              #
#                                                                             #
# Reference         : P1588/D1.3, February 2018 V3.01 Clause J.5.2 Page 412   #
#                                                                             #
# Conformance Type  : SHALL                                                   #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 001                                                     #
# Test Topology     : 001                                                     #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode - one-step/two-step> |           #
#           |                     <Configure logAnnounceInterval, | P1        #
#           |                             announceReceiptTimeout> |           #
#           |                         <Configure logSyncInterval> | P1        #
#           |                  <Configure logMinDelayReqInterval> | P1        #
#           |                    <Configure Priority1, Priority2> | P1        #
#           |                                     <Enable L1SYNC> | P1        #
#           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        #
#           |                     <Disable L1SynOptParams option> | P1        #
#           |           <Enable asymmetryCorrectionPortDS.enable> | P1        #
#           | <Configure timestampCorrectionPortDS.egressLatency, | P1        #
#           |           timestampCorrectionPortDS.ingressLatency, |           #
#           |        asymmetryCorrectionPortDS.constantAsymmetry, |           #
#           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           #
#           |                                                     |           #
#           |    PTP SIGNALING with L1 Sync TLV [MSG_TYPE = 0xC,  |           #
#           |                             DN = 0,TLV_TYPE=0x8001] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                      <Configure Domain Number = 127>|           #
#           |                                                     |           #
#           |    PTP SIGNALING with L1 Sync TLV [MSG_TYPE = 0xC,  |           #
#           |                           DN = 127,TLV_TYPE=0x8001] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                      !<Configure Domain Number = -1>|           #
#           |                                                     |           #
#           |    PTP SIGNALING with L1 Sync TLV [MSG_TYPE = 0xC,  |           #
#           |                            DN = -1,TLV_TYPE=0x8001] |           #
#        T1 |                      XX-------------------------<<--| P1        #
#           |                                                     |           #
#           |                        <Configure Domain Number = 0>|           #
#           |                                                     |           #
#           |    PTP SIGNALING with L1 Sync TLV [MSG_TYPE = 0xC,  |           #
#           |                             DN = 0,TLV_TYPE=0x8001] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     MSG_TYPE  = Message Type                                                #
#     DN        = Domain Number                                               #
#     BC        = Boundary Clock                                              #
#     OC        = Ordinary Clock                                              #
#                                                                             #
# NOTE :                                                                      #
#                                                                             #
#  1. This objective is verified using the High Accuracy Delay Request-       #
#     Response Default PTP Profile                                            #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.ingressLatency,           #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part I)                                                                    #
#                                                                             #
# Step 3 : Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV  #
#          on the port P1 with following parameters:                          #
#                                                                             #
#                        PTP Header                                           #
#                            Message Type  = 0xC                              #
#                            Domain Number = 0                                #
#                        L1_SYNC TLV                                          #
#                            TLV_TYPE      = 0x8001                           #
#                                                                             #
# Step 4 : Configure domain number as 127 on Port P1 in DUT.                  #
#                                                                             #
# Step 5 : Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV  #
#          on the port P1 with following parameters:                          #
#                                                                             #
#                        PTP Header                                           #
#                            Message Type  = 0xC                              #
#                            Domain Number = 127                              #
#                        L1_SYNC TLV                                          #
#                            TLV_TYPE      = 0x8001                           #
#                                                                             #
# Step 6 : Observe that DUT does not allow to configure domain number as -1 on#
#          port P1 in DUT.                                                    #
#                                                                             #
# Step 7 : If DUT allows to configure domain number in step 6, observe that   #
#          DUT does not transmit PTP SIGNALING message with L1 Sync TLV on the#
#          port P1 with following parameters:                                 #
#                                                                             #
#                        PTP Header                                           #
#                            Message Type  = 0xC                              #
#                            Domain Number = -1                               #
#                        L1_SYNC TLV                                          #
#                            TLV_TYPE      = 0x8001                           #
#                                                                             #
# Step 8 : Configure domain number as 0 on Port P1 in DUT.                    #
#                                                                             #
# Step 9 : Verify that DUT transmits PTP SIGNALING message with L1 Sync TLV   #
#          on the port P1 with following parameters:                          #
#                                                                             #
#                        PTP Header                                           #
#                            Message Type  = 0xC                              #
#                            Domain Number = 0                                #
#                        L1_SYNC TLV                                          #
#                            TLV_TYPE      = 0x8001                           #
#                                                                             #
###############################################################################
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          May/2018      CERN          Initial                            #
# 1.1          Jun/2018      CERN          Fixed dut_port in                  #
#                                          dut_reset_domain                   #
# 1.2          Sep/2018      CERN          a) Removed destination MAC and IP  #
#                                             in send functions.              #
#                                          b) Removed timeout in recv         #
#                                             functions.                      #
#                                          c) Moved timeout calculation to    #
#                                             PTP-HA global file.             #
#                                          d) Step 7 is changed to skip       #
#                                             validation, if DUT does not     #
#                                             allow configuration in step 6.  #
#                                          e) Capture buffer is cleared after #
#                                             every configuration step.       #
#                                                                             #
###############################################################################
# Copyright (c) 2018 CERN                                                     #
###############################################################################

global dut_log_msg
global tee_log_msg
global session_id

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def   "To verify that a PTP enabled device supports to\
              configure domain number in range 0 to 127." 

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set dut_port_1                     $::dut_port_num_1
set vlan_id                        $::ptp_ha_vlan_id

set dut_ip                         $::dut_test_port_ip
set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT

set domain                         $::DEFAULT_DOMAIN
set gmid                           $::GRANDMASTER_ID
set infinity                       $::INFINITY
set tlv_type(l1sync)               $::TLV_TYPE(L1SYNC)

set sequence_id_ann                1
set sequence_id_l1sync             43692

set domain1                        127
set domain2                         -1

########################### END - INITIALIZATION ##############################

ptp_ha::display_port_details

ptp_ha::dut_Initialization

proc cleanup {} {

    ptp_ha::dut_cleanup_setup_001

    ptp_ha::dut_reset_domain \
            -port_num                $::dut_port_num_1\
            -vlan_id                 $::ptp_ha_vlan_id

}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

### (Initial Part) ###

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.ingressLatency,           #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
###############################################################################

STEP 1

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP1)"

if {![ptp_ha::dut_configure_setup_001]} {

    LOG -msg "$dut_log_msg(INIT_SETUP1_F)"

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP1_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
###############################################################################

STEP 2

LOG -level 0 -msg "$tee_log_msg(INIT)"

ptp_ha::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

    if {![PTP_HA_UNICAST_PRE_REQUISITE_CONFIGURATION\
                        -port_num       $tee_port_1\
                        -tee_mac        $tee_mac\
                        -tee_ip         $tee_ip\
                        -dut_ip         $dut_ip\
                        -vlan_id        $vlan_id\

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
                           -tc_def $tc_def

        return 0
    }

}

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_1\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def $tc_def

    return 0
}

set filter_id [lindex $id1 end]

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1


#(Part I)#  

###############################################################################
# Step 3 : Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV  #
#          on the port P1 with following parameters:                          #
#                                                                             #
#                        PTP Header                                           #
#                            Message Type  = 0xC                              #
#                            Domain Number = 0                                #
#                        L1_SYNC TLV                                          #
#                            TLV_TYPE      = 0x8001                           #
###############################################################################

STEP 3

LOG -level 0 -msg "$tee_log_msg(L1SYNC_RX_P1_O)"

  if {![ptp_ha::recv_signal \
                -port_num                $tee_port_1\
                -domain_number           $domain\
                -tlv_type                $tlv_type(l1sync)\
                -filter_id               $filter_id]} {

      TEE_CLEANUP

      TC_CLEAN_AND_ABORT -reason "$tee_log_msg(L1SYNC_RX_P1_F)"\
                         -tc_def  $tc_def

      return 0
}

###############################################################################
#                                                                             #
# Step 4 : Configure domain number as 127 on Port P1 in DUT.                  #
###############################################################################

STEP 4

LOG -level 0 -msg "$dut_log_msg(CONFIGURE_DN_127)"

  if {![ptp_ha::dut_set_domain \
                -port_num                $dut_port_1\
                -domain                  $domain1\
                -clock_mode              $::ptp_dut_clock_step\
                -filter_id               $filter_id]} {

      TEE_CLEANUP

      TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIGURE_DN_F)"\
                         -tc_def  $tc_def

      return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
#                                                                             #
# Step 5 : Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV  #
#          on the port P1 with following parameters:                          #
#                                                                             #
#                        PTP Header                                           #
#                            Message Type  = 0xC                              #
#                            Domain Number = 127                              #
#                        L1_SYNC TLV                                          #
#                            TLV_TYPE      = 0x8001                           #
###############################################################################

STEP 5

LOG -level 0 -msg "$tee_log_msg(L1SYNC_RX_P1_O)"

  if {![ptp_ha::recv_signal \
                -port_num                $tee_port_1\
                -domain_number           $domain1\
                -tlv_type                $tlv_type(l1sync)\
                -filter_id               $filter_id]} {

      TEE_CLEANUP

      TC_CLEAN_AND_ABORT -reason "$tee_log_msg(L1SYNC_RX_P1_F)"\
                         -tc_def  $tc_def

      return 0
}

###############################################################################
#                                                                             #
# Step 6 : Observe that DUT does not allow to configure domain number as -1 on#
#          port P1 in DUT.                                                    #
###############################################################################

STEP 6

LOG -level 0 -msg "$dut_log_msg(CONFIGURE_DN_1)"

if {[ptp_ha::dut_set_domain \
                -port_num                $dut_port_1\
                -domain                  $domain2\
                -clock_mode              $::ptp_dut_clock_step\
                -filter_id               $filter_id]} {

    set step6_pass 1

} else {

    set step6_pass 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
#                                                                             #
# Step 7 : If DUT allows to configure domain number in step 6, observe that   #
#          DUT does not transmit PTP SIGNALING message with L1 Sync TLV on the#
#          port P1 with following parameters:                                 #
#                                                                             #
#                        PTP Header                                           #
#                            Message Type  = 0xC                              #
#                            Domain Number = -1                               #
#                        L1_SYNC TLV                                          #
#                            TLV_TYPE      = 0x8001                           #
###############################################################################

STEP 7

LOG -level 0 -msg "$tee_log_msg(L1SYNC_RX_P1_O)"

if {$step6_pass} {

  if {[ptp_ha::recv_signal \
                -port_num                $tee_port_1\
                -domain_number           $domain2\
                -tlv_type                $tlv_type(l1sync)\
                -filter_id               $filter_id]} {

      TEE_CLEANUP

      TC_CLEAN_AND_ABORT -reason "$tee_log_msg(L1SYNC_RX_P1_F)"\
                         -tc_def  $tc_def

      return 0
 }

} else {

    LOG -level 0 -msg "$tee_log_msg(SKIP_STEP)"
}

###############################################################################
#                                                                             #
# Step 8 : Configure domain number as 0 on Port P1 in DUT.                    #
###############################################################################

STEP 8

LOG -level 0 -msg "$dut_log_msg(CONFIGURE_DN_0)"

  if {![ptp_ha::dut_set_domain \
                -port_num                $dut_port_1\
                -domain                  $domain\
                -clock_mode              $::ptp_dut_clock_step\
                -filter_id               $filter_id]} {

      TEE_CLEANUP

      TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIGURE_DN_F)"\
                         -tc_def $tc_def

      return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
#                                                                             #
# Step 9 : Verify that DUT transmits PTP SIGNALING message with L1 Sync TLV   #
#          on the port P1 with following parameters:                          #
#                                                                             #
#                        PTP Header                                           #
#                            Message Type  = 0xC                              #
#                            Domain Number = 0                                #
#                        L1_SYNC TLV                                          #
#                            TLV_TYPE      = 0x8001                           #
#                                                                             #
###############################################################################

STEP 9

LOG -level 0 -msg "$tee_log_msg(L1SYNC_RX_P1_V)"

  if {![ptp_ha::recv_signal \
                -port_num                $tee_port_1\
                -domain_number           $domain\
                -tlv_type                $tlv_type(l1sync)\
                -filter_id               $filter_id]} {

      TEE_CLEANUP

      TC_CLEAN_AND_FAIL -reason "PTP enabled device does not supports to\
                                 configure domain number in range 0 to 127"\
                         -tc_def $tc_def

      return 0
}

TEE_CLEANUP

TC_CLEAN_AND_PASS -reason  "PTP enabled device supports to configure domain\
                            number in range 0 to 127"\
                  -tc_def   $tc_def

return 1

############################### END OF TEST CASE #################################
