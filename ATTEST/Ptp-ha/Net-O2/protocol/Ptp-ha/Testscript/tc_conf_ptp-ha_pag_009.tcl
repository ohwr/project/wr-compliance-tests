#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_ptp-ha_pag_009                                  #
# Test Case Version : 1.4                                                     #
# Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    #
# Module Name       : PTP Accuracy Group (PAG)                                #
#                                                                             #
# Title             : Ingress timestamp in Delay_Req message                  #
#                                                                             #
# Purpose           : To verify that a PTP enabled device generates Ingress   #
#                     timestamp in Delay_Req (event) messages from            #
#                     timestampCorrectionPortDS.ingressLatency when using     #
#                     Delay Request-Response mechanism.                       #
#                                                                             #
# Reference         : IEEE 1588-2017 Clause 16.7.1 Page 301, Clause 7.3.4.2   #
#                     Page 68, Clause 8.2.16.2 Page 128, Clause 11.3.2        #
#                     Page 193                                                #
#                                                                             #
# Conformance Type  : SHALL                                                   #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 009                                                     #
# Test Topology     : 001                                                     #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode - one-step/two-step> |           #
#           |                     <Configure logAnnounceInterval, | P1        #
#           |                             announceReceiptTimeout> |           #
#           |                         <Configure logSyncInterval> | P1        #
#           |                  <Configure logMinDelayReqInterval> | P1        #
#           |                    <Configure Priority1, Priority2> | P1        #
#           |                                     <Enable L1SYNC> | P1        #
#           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        #
#           |                     <Disable L1SynOptParams option> | P1        #
#           |           <Enable asymmetryCorrectionPortDS.enable> | P1        #
#           | <Configure timestampCorrectionPortDS.egressLatency, | P1        #
#           |           timestampCorrectionPortDS.ingressLatency, |           #
#           |        asymmetryCorrectionPortDS.constantAsymmetry, |           #
#           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                          DN = DN1, PRI1 = X]        |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | ANNOUNCE [MSG_TYPE = 0x0B,                          |           #
#           |   DN = DN1, PRI1 = X+1]                             |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                    SYNC [MSG_TYPE = 0x00, DN = DN1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                    SYNC [MSG_TYPE = 0x00, DN = DN1, |           #
#           |                ORG_TS = S_OTS1, CF = SYNC_CF1](TS1) |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                     <If Clock Step = Two Step, then |           #
#           |          receive FOLLOW_UP and include CF = FU_CF1> |           #
#           |                                                     |           #
#           | DELAY_REQ [MSG_TYPE = 0x01, DN = DN1,               |           #
#           |  ORG_TS = D_OTS1, CF = 0](TS2)                      |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                        DELAY_RESP [MSG_TYPE = 0x09, |           #
#           |                               DN = DN1, RTS = RTS1] |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           | <Calculate meanDelay (MD1)>                         |           #
#           |                                                     |           #
#           | <Configure timestampCorrectionPortDS.ingressLatency |           #
#           |                                 value to 2^(32+16)> |           #
#           |                                                     |           #
#           |                    SYNC [MSG_TYPE = 0x00, DN = DN1, |           #
#           |                ORG_TS = S_OTS2, CF = SYNC_CF2](TS1) |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                     <If Clock Step = Two Step, then |           #
#           |          receive FOLLOW_UP and include CF = FU_CF2> |           #
#           |                                                     |           #
#           | DELAY_REQ [MSG_TYPE = 0x01, DN = DN1,               |           #
#           |  ORG_TS = D_OTS2, CF = 0](TS2)                      |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                        DELAY_RESP [MSG_TYPE = 0x09, |           #
#           |                               DN = DN1, RTS = RTS2] |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           | <Calculate meanDelay (MD2)>                         |           #
#           |                                                     |           #
#           |                       MD2 < MD1                     |           #
#           |                                                     |           #
#           | <Configure timestampCorrectionPortDS.ingressLatency |           #
#           |                                value to -2^(32+16)> |           #
#           |                                                     |           #
#           |                    SYNC [MSG_TYPE = 0x00, DN = DN1, |           #
#           |                ORG_TS = S_OTS3, CF = SYNC_CF3](TS1) |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                     <If Clock Step = Two Step, then |           #
#           |          receive FOLLOW_UP and include CF = FU_CF3> |           #
#           |                                                     |           #
#           | DELAY_REQ [MSG_TYPE = 0x01, DN = DN1,               |           #
#           |  ORG_TS = D_OTS3, CF = 0](TS2)                      |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                        DELAY_RESP [MSG_TYPE = 0x09, |           #
#           |                               DN = DN1, RTS = RTS3] |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           | <Calculate meanDelay (MD3)>                         |           #
#           |                                                     |           #
#           |                       MD3 > MD1                     |           #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     MSG_TYPE  = Message Type                                                #
#     DN        = Domain Number                                               #
#     PRT1      = priority1                                                   #
#     ORG_TS    = originTimestamp                                             #
#     CF        = correctionField                                             #
#     RTS       = receiveTimestamp                                            #
#     TS        = Timestamp                                                   #
#                                                                             #
# NOTE :                                                                      #
#                                                                             #
#  1. This objective is verified using the High Accuracy Delay Request-       #
#     Response Default PTP Profile                                            #
#                                                                             #
#  2. meanDelay is calculated with below formulae:                            #
#     * For one-step clock:                                                   #
#                                                                             #
#        [(t2 - t3) + (receiveTimestamp of Delay_Resp message -               #
#        originTimestamp of Sync message) - <correctedSyncCorrectionField> -  #
#        correctionField of Delay_Resp message]/2.                            #
#                                                                             #
#     * For two-step clock:                                                   #
#        [(t2 - t3) + (receiveTimestamp of Delay_Resp message -               #
#        preciseOriginTimestamp of Follow_Up message) -                       #
#        <correctedSyncCorrectionField> - correctionField of Follow_Up message#
#        - correctionField of Delay_Resp message]/2.                          #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency = 0, timestampCorrectionPortDS.ingressLatency = 0,   #
#          asymmetryCorrectionPortDS.constantAsymmetry = 0 and                #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient = 0.              #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x0B                                    #
#                  Domain Number    = DN1                                     #
#                  Priority1        = X                                       #
#                                                                             #
# Step 4 : Send periodic ANNOUNCE message on port T1 with following parameters#
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x0B                                    #
#                  Domain Number    = DN1                                     #
#                  Priority1        = X+1                                     #
#                                                                             #
# Step 5 : Observe that DUT transmits SYNC message on port P1 with following  #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x00                                #
#                  Domain Number        = DN1                                 #
#                                                                             #
# Step 6 : Observe that DUT transmits SYNC message on port P1 with following  #
#          parameters and store timestamp TS1.                                #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x00                                #
#                  Domain Number        = DN1                                 #
#                  Origin Timestamp     = S_OTS1                              #
#                  Correction Field     = SYNC_CF1                            #
#                                                                             #
# Step 7 : If the clock is two step, observe that DUT transmits FOLLOW_UP     #
#          message on the port P1 with following parameters.                  #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x08                                #
#                  Domain Number        = DN1                                 #
#                  Correction Field     = FU_CF1                              #
#                                                                             #
# Step 8 : Send DELAY_REQ message on port T1 with following parameters and    #
#          store timestamp TS2.                                               #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x01                                #
#                  Domain Number        = DN1                                 #
#                  Origin Timestamp     = D_OTS1                              #
#                  Correction Field     = 0                                   #
#                                                                             #
# Step 9 : Observe that the DUT transmits DELAY_RESP message on the port P1   #
#          with following parameters.                                         #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type        = 0x09                                #
#                   Domain Number       = DN1                                 #
#                   Receive Timestamp   = RTS1                                #
#                                                                             #
# Step 10: Calculate meanDelay (MD1) at TEE.                                  #
#                                                                             #
# Step 11: Configure ingressLatency on port P1 by setting ingressLatency to   #
#          4 294 967 296 ns (i.e., the value of dataset expressed in          #
#          TimeInterval asymmetryCorrectionPortDS.ingressLatency = 2^48).     #
#                                                                             #
# Step 12: Observe that DUT transmits SYNC message on port P1 with following  #
#          parameters and store timestamp TS1.                                #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x00                                #
#                  Domain Number        = DN1                                 #
#                  Origin Timestamp     = S_OTS2                              #
#                  Correction Field     = SYNC_CF2                            #
#                                                                             #
# Step 13: If the clock is two step, observe that DUT transmits FOLLOW_UP     #
#          message on the port P1 with following parameters.                  #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x08                                #
#                  Domain Number        = DN1                                 #
#                  Correction Field     = FU_CF2                              #
#                                                                             #
# Step 14: Send DELAY_REQ message on port T1 with following parameters and    #
#          store timestamp TS2.                                               #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x01                                #
#                  Domain Number        = DN1                                 #
#                  Origin Timestamp     = D_OTS2                              #
#                  Correction Field     = 0                                   #
#                                                                             #
# Step 15: Observe that the DUT transmits DELAY_RESP message on the port P1   #
#          with following parameters.                                         #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type        = 0x09                                #
#                   Domain Number       = DN1                                 #
#                   Receive Timestamp   = RTS2                                #
#                                                                             #
# Step 16: Calculate meanDelay (MD2) at TEE.                                  #
#                                                                             #
# Step 17: Observe that MD2 is lesser than MD1.                               #
#                                                                             #
# Step 18: Configure ingressLatency on port P1 by setting ingressLatency to   #
#          -4 294 967 296 ns (i.e., the value of dataset expressed in         #
#          TimeInterval asymmetryCorrectionPortDS.ingressLatency = -2^48).    #
#                                                                             #
# Step 19: Observe that DUT transmits SYNC message on port P1 with following  #
#          parameters and store timestamp TS1.                                #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x00                                #
#                  Domain Number        = DN1                                 #
#                  Origin Timestamp     = S_OTS3                              #
#                  Correction Field     = SYNC_CF3                            #
#                                                                             #
# Step 20: If the clock is two step, observe that DUT transmits FOLLOW_UP     #
#          message on the port P1 with following parameters.                  #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x08                                #
#                  Domain Number        = DN1                                 #
#                  Correction Field     = FU_CF3                              #
#                                                                             #
# Step 21: Send DELAY_REQ message on port T1 with following parameters and    #
#          store timestamp TS2.                                               #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x01                                #
#                  Domain Number        = DN1                                 #
#                  Origin Timestamp     = D_OTS3                              #
#                  Correction Field     = 0                                   #
#                                                                             #
# Step 22: Observe that the DUT transmits DELAY_RESP message on the port P1   #
#          with following parameters.                                         #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type        = 0x09                                #
#                   Domain Number       = DN1                                 #
#                   Receive Timestamp   = RTS3                                #
#                                                                             #
# Step 23: Calculate meanDelay (MD3) at TEE.                                  #
#                                                                             #
# Step 24: Verify that MD3 is greater than MD1.                               #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          May/2018      CERN          Initial                            #
# 1.1          Jun/2018      CERN          Fixed dut_port in                  #
#                                          dut_reset_egress_latency           #
# 1.2          Jul/2018      CERN          Added ptp_ha::display_port_details #
#                                          to print port details              #
# 1.3          Sep/2018      CERN          a) Removed destination MAC and IP  #
#                                             in send functions.              #
#                                          b) Removed timeout in recv         #
#                                             functions.                      #
#                                          c) Moved timeout calculation to    #
#                                             PTP-HA global file.             #
#                                          d) Re-worded header and log prints #
#                                             to improve understandability.   #
# 1.4          Nov/2018      CERN          Synchronized the timestamp between #
#                                          reception of Sync and transmission #
#                                          of Delay_Req messages.             #
#                                                                             #
###############################################################################
# Copyright (c) 2018 CERN                                                     #
###############################################################################

global dut_log_msg
global tee_log_msg
global session_id

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def  "To verify that a PTP enabled device generates Ingress\
             timestamp in Delay_Req (event) messages from\
             timestampCorrectionPortDS.ingressLatency when using\
             Delay Request-Response mechanism"

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set dut_port_1                     $::dut_port_num_1
set vlan_id                        $::ptp_ha_vlan_id
set dut_ip                         $::dut_test_port_ip
set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT

set domain                         $::DEFAULT_DOMAIN
set gmid                           $::GRANDMASTER_ID
set infinity                       $::INFINITY
set clock_step                     $::ptp_dut_clock_step

set sequence_id_ann                1

########################### END - INITIALIZATION ##############################

if {($env(TEST_DEVICE_MANAGEMENT_MODE) == "Manual")} {
    
    puts ".\n"
    puts "???????????????????????????????????????????????????????????????????????????"
    puts "???????????????????????????????????????????????????????????????????????????"
    puts "?? WARNING: Execution of this test in manual management mode may give    ??"
    puts "??          in-correct result.                                           ??"
    puts "??                                                                       ??"
    puts "?? The timestamps from the device should be taken faster and hence it is ??"
    puts "?? advised to execute this test case in automated mode.                  ??"
    puts "??                                                                       ??"
    puts "???????????????????????????????????????????????????????????????????????????"
    puts "???????????????????????????????????????????????????????????????????????????"
    puts ".\n"
}

ptp_ha::display_port_details

ptp_ha::dut_Initialization

proc cleanup {} {

    ptp_ha::dut_cleanup_setup_009

    ptp_ha::dut_reset_ingress_latency\
                    -port_num       $::dut_port_num_1
}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

### (Initial Part) ###

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency = 0, timestampCorrectionPortDS.ingressLatency = 0,   #
#          asymmetryCorrectionPortDS.constantAsymmetry = 0 and                #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient = 0.              #
#                                                                             #
###############################################################################

STEP 1

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP9)"

if {![ptp_ha::dut_configure_setup_009]} {

    LOG -msg "$dut_log_msg(INIT_SETUP9_F)"

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP9_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
###############################################################################

STEP 2

LOG -level 0 -msg "$tee_log_msg(INIT)"

ptp_ha::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

    if {![PTP_HA_UNICAST_PRE_REQUISITE_CONFIGURATION\
                        -port_num       $tee_port_1\
                        -tee_mac        $tee_mac\
                        -tee_ip         $tee_ip\
                        -dut_ip         $dut_ip\
                        -vlan_id        $vlan_id\
                        -dut_mac        tee_dest_mac\
                        -timeout        $arp_timeout]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
                           -tc_def  $tc_def

        return 0
    }

}

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_1\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def  $tc_def

    return 0
}

set filter_id [lindex $id1 end]

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

ptp_ha::enable_auto_response_to_delay_requests

#(Part 1)#

###############################################################################
#                                                                             #
# Step 3 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x0B                                    #
#                  Domain Number    = DN1                                     #
#                  Priority1        = X                                       #
###############################################################################

STEP 3

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
           -port_num                $tee_port_1\
           -domain_number           $domain\
           -filter_id               $filter_id\
           -recvd_gm_priority1      priority1]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                        -tc_def  $tc_def

    return 0
}

###############################################################################
#                                                                             #
# Step 4 : Send periodic ANNOUNCE message on port T1 with following parameters#
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x0B                                    #
#                  Domain Number    = DN1                                     #
#                  Priority1        = X+1                                     #
#                                                                             #
###############################################################################

STEP 4

set priority [expr $priority1 + 1]

LOG -level 0 -msg "$tee_log_msg(ANN_TX_T1)"

if {![ptp_ha::send_announce \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -gm_priority1            $priority\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -count                   $infinity\
           -gmid                    0x$gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANN_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 5 : Observe that DUT transmits SYNC message on port P1 with following  #
#          parameters.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x00                                #
#                  Domain Number        = DN1                                 #
###############################################################################

STEP 5

LOG -level 0 -msg "$tee_log_msg(SYNC_RX_P1_O)"

if {![ptp_ha::recv_sync \
           -port_num                $tee_port_1\
           -filter_id               $filter_id\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -gmid                    0x$gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
#                                                                             #
# Step 6 : Observe that DUT transmits SYNC message on port P1 with following  #
#          parameters and store timestamp TS1.                                #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x00                                #
#                  Domain Number        = DN1                                 #
#                  Origin Timestamp     = S_OTS1                              #
#                  Correction Field     = SYNC_CF1                            #
###############################################################################

STEP 6

LOG -level 0 -msg "$tee_log_msg(SYNC_RX_P1_O)"

if {![ptp_ha::recv_sync \
           -port_num                     $tee_port_1\
           -filter_id                    $filter_id\
           -unicast_flag                 $::unicast_flag\
           -recvd_origin_timestamp_sec   S_OTS1_sec\
           -recvd_origin_timestamp_ns    S_OTS1_ns\
           -recvd_correction_field       SYNC_CF1\
           -rx_timestamp_sec             TS1_sec\
           -rx_timestamp_ns              TS1_ns\
           -domain_number                $domain\
           -gmid                         0x$gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

set S_OTS1  [expr ($S_OTS1_sec * pow(10,9)) + $S_OTS1_ns]
set TS1     [expr ($TS1_sec * pow(10,9)) + $TS1_ns]

###############################################################################
#                                                                             #
# Step 7 : If the clock is two step, observe that DUT transmits FOLLOW_UP     #
#          message on the port P1 with following parameters.                  #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x08                                #
#                  Domain Number        = DN1                                 #
#                  Correction Field     = FU_CF1                              #
###############################################################################

if { $clock_step == "Two-Step" } {

STEP 7

LOG -level 0 -msg "$tee_log_msg(FOLLOW_UP_RX_P1_O)"

if {![ptp_ha::recv_followup \
           -port_num                           $tee_port_1\
           -filter_id                          $filter_id\
           -unicast_flag                       $::unicast_flag\
           -recvd_correction_field             FU_CF1\
           -recvd_precise_origin_timestamp_sec PTS1_sec\
           -recvd_precise_origin_timestamp_ns  PTS1_ns\
           -domain_number                      $domain\
           -gmid                               0x$gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FOLLOW_UP_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

set PTS1    [expr ($PTS1_sec * pow(10,9)) + $PTS1_ns]

}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
#                                                                             #
# Step 8 : Send DELAY_REQ message on port T1 with following parameters and    #
#          store timestamp TS2.                                               #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x01                                #
#                  Domain Number        = DN1                                 #
#                  Origin Timestamp     = D_OTS1                              #
#                  Correction Field     = 0                                   #
#                                                                             #
###############################################################################

STEP 8

LOG -level 0 -msg "$tee_log_msg(DELAY_REQ_TX)"

if {![ptp_ha::send_delay_req \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -correction_field        0\
           -count                   $infinity\
           -tx_origin_timestamp_sec TS2_sec\
           -tx_origin_timestamp_ns  TS2_ns\
           -gmid                    0x$gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(DELAY_REQ_TX_F)"\
                       -tc_def  $tc_def

    return 0
}

# Transmission of Delay_Req is assumed to happen within 400ms. Taking timestamp
# from xena is not correct due to difference in the time between Chassis and
# module.

set TS2     [expr $TS1 + 400000000]

###############################################################################
# Step 9 : Observe that the DUT transmits DELAY_RESP message on the port P1   #
#          with following parameters.                                         #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type        = 0x09                                #
#                   Domain Number       = DN1                                 #
#                   Receive Timestamp   = RTS1                                #
###############################################################################

STEP 9

LOG -level 0 -msg "$tee_log_msg(DELAY_RESP_RX_P1_O)"

if {![ptp_ha::recv_delay_resp \
           -port_num                     $tee_port_1\
           -filter_id                    $filter_id\
           -unicast_flag                 $::unicast_flag\
           -recvd_receive_timestamp_sec  RTS1_sec\
           -recvd_receive_timestamp_ns   RTS1_ns\
           -recvd_correction_field       DR_CF1\
           -domain_number                $domain\
           -gmid                         0x$gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(DELAY_RESP_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

set RTS1    [expr ($RTS1_sec * pow(10,9)) + $RTS1_ns]

###############################################################################
#                                                                             #
# Step 10: Calculate meanDelay (MD1) at TEE.                                  #
###############################################################################

STEP 10

LOG -level 0 -msg "$tee_log_msg(CALC_MD1)"

if { $clock_step == "One-Step" } {

    set TS1          [format "%.f" $TS1]
    set TS2          [format "%.f" $TS2]
    set RTS1         [format "%.f" $RTS1]
    set S_OTS1       [format "%.f" $S_OTS1]
    set SYNC_CF1     [format "%.f" $SYNC_CF1]
    set DR_CF1       [format "%.f" $DR_CF1]

    set MD1 [ expr ((($TS1 - $TS2) + ($RTS1 - $S_OTS1) - $SYNC_CF1 - $DR_CF1)/2)]

    LOG -level 3 -msg "t2       = [nano $TS1] ns"
    LOG -level 3 -msg "t3       = [nano $TS2] ns"
    LOG -level 3 -msg "RTS1     = [nano $RTS1] ns"
    LOG -level 3 -msg "S_OTS1   = [nano $S_OTS1] ns"
    LOG -level 3 -msg "SYNC_CF1 = [nano $SYNC_CF1] ns"
    LOG -level 3 -msg "DR_CF1   = [nano $DR_CF1] ns"
    LOG -level 3 -msg "-----------------------------------------"
    LOG -level 3 -msg "MD1      = [nano $MD1] ns (formula: ((t2 - t3) + (RTS1 - S_OTS1) - SYNC_CF1 - DR_CF1)/2)"

#     * For one-step clock:                                                   #
#                                                                             #
#        [(t2 - t3) + (receiveTimestamp of Delay_Resp message -               #
#        originTimestamp of Sync message) - <correctedSyncCorrectionField> -  #
#        correctionField of Delay_Resp message]/2.                            #

} else {

    set TS1          [format "%.f" $TS1]
    set TS2          [format "%.f" $TS2]
    set RTS1         [format "%.f" $RTS1]
    set PTS1         [format "%.f" $PTS1]
    set SYNC_CF1     [format "%.f" $SYNC_CF1]
    set FU_CF1       [format "%.f" $FU_CF1]
    set DR_CF1       [format "%.f" $DR_CF1]

    set MD1 [ expr ((($TS1 - $TS2) + ($RTS1 - $PTS1) - $SYNC_CF1 - $FU_CF1 - $DR_CF1)/2)]

    LOG -level 3 -msg "t2       = [nano $TS1] ns"
    LOG -level 3 -msg "t3       = [nano $TS2] ns"
    LOG -level 3 -msg "RTS1     = [nano $RTS1] ns"
    LOG -level 3 -msg "PTS1     = [nano $PTS1] ns"
    LOG -level 3 -msg "SYNC_CF1 = [nano $SYNC_CF1] ns"
    LOG -level 3 -msg "FU_CF1   = [nano $FU_CF1] ns"
    LOG -level 3 -msg "DR_CF1   = [nano $DR_CF1] ns"
    LOG -level 3 -msg "-----------------------------------------"

    LOG -level 3 -msg "MD1      = [nano $MD1] ns (formula: ((t2 - t3) + (RTS1 - PTS1) - SYNC_CF1 - FU_CF1 - DR_CF1)/2)"

#     * For two-step clock:                                                   #
#        [(t2 - t3) + (receiveTimestamp of Delay_Resp message -               #
#        preciseOriginTimestamp of Follow_Up message) -                       #
#        <correctedSyncCorrectionField> - correctionField of Follow_Up message#
#        - correctionField of Delay_Resp message]/2.                          #
}

###############################################################################
# Step 11: Configure ingressLatency on port P1 by setting ingressLatency to   #
#          4 294 967 296 ns (i.e., the value of dataset expressed in          #
#          TimeInterval asymmetryCorrectionPortDS.ingressLatency = 2^48).     #
#                                                                             #
###############################################################################

STEP 11

set latency 4294967296

LOG -level 0 -msg "$dut_log_msg(CONFIG_TXPORTDS.INGRESSLATENCY)"

if {![ptp_ha::dut_set_ingress_latency\
             -port_num       $dut_port_1\
             -latency        $latency]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_TXPORTDS.INGRESSLATENCY_F)"\
                        -tc_def  $tc_def

     return 0
}

sleep $::ptp_ha_l1sync_wait

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 12: Observe that DUT transmits SYNC message on port P1 with following  #
#          parameters and store timestamp TS1.                                #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x00                                #
#                  Domain Number        = DN1                                 #
#                  Origin Timestamp     = S_OTS2                              #
#                  Correction Field     = SYNC_CF2                            #
###############################################################################

STEP 12

LOG -level 0 -msg "$tee_log_msg(SYNC_RX_P1_O)"

if {![ptp_ha::recv_sync \
           -port_num                     $tee_port_1\
           -filter_id                    $filter_id\
           -unicast_flag                 $::unicast_flag\
           -recvd_origin_timestamp_sec   S_OTS2_sec\
           -recvd_origin_timestamp_ns    S_OTS2_ns\
           -recvd_correction_field       SYNC_CF2\
           -rx_timestamp_sec             TS1_sec\
           -rx_timestamp_ns              TS1_ns\
           -domain_number                $domain\
           -gmid                         0x$gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

set S_OTS2  [expr ($S_OTS2_sec * pow(10,9)) + $S_OTS2_ns]
set TS1     [expr ($TS1_sec * pow(10,9)) + $TS1_ns]

###############################################################################
#                                                                             #
# Step 13: If the clock is two step, observe that DUT transmits FOLLOW_UP     #
#          message on the port P1 with following parameters.                  #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x08                                #
#                  Domain Number        = DN1                                 #
#                  Correction Field     = FU_CF2                              #
###############################################################################

if { $clock_step == "Two-Step" } {

STEP 13

LOG -level 0 -msg "$tee_log_msg(FOLLOW_UP_RX_P1_O)"

if {![ptp_ha::recv_followup \
           -port_num                           $tee_port_1\
           -filter_id                          $filter_id\
           -unicast_flag                       $::unicast_flag\
           -recvd_correction_field             FU_CF2\
           -recvd_precise_origin_timestamp_sec PTS2_sec\
           -recvd_precise_origin_timestamp_ns  PTS2_ns\
           -domain_number                      $domain\
           -gmid                               0x$gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FOLLOW_UP_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

set PTS2    [expr ($PTS2_sec * pow(10,9)) + $PTS2_ns]

}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
#                                                                             #
# Step 14: Send DELAY_REQ message on port T1 with following parameters and    #
#          store timestamp TS2.                                               #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x01                                #
#                  Domain Number        = DN1                                 #
#                  Origin Timestamp     = D_OTS2                              #
#                  Correction Field     = 0                                   #
###############################################################################

STEP 14

LOG -level 0 -msg "$tee_log_msg(DELAY_REQ_TX)"

if {![ptp_ha::send_delay_req \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -correction_field        0\
           -count                   $infinity\
           -tx_origin_timestamp_sec TS2_sec\
           -tx_origin_timestamp_ns  TS2_ns\
           -gmid                    0x$gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(DELAY_REQ_TX_F)"\
                       -tc_def  $tc_def

    return 0
}

# Transmission of Delay_Req is assumed to happen within 400ms. Taking timestamp
# from xena is not correct due to difference in the time between Chassis and
# module.

set TS2     [expr $TS1 + 400000000]

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
#                                                                             #
# Step 15: Observe that the DUT transmits DELAY_RESP message on the port P1   #
#          with following parameters.                                         #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type        = 0x09                                #
#                   Domain Number       = DN1                                 #
#                   Receive Timestamp   = RTS2                                #
###############################################################################

STEP 15

LOG -level 0 -msg "$tee_log_msg(DELAY_RESP_RX_P1_O)"

if {![ptp_ha::recv_delay_resp \
           -port_num                     $tee_port_1\
           -filter_id                    $filter_id\
           -unicast_flag                 $::unicast_flag\
           -recvd_receive_timestamp_sec  RTS2_sec\
           -recvd_receive_timestamp_ns   RTS2_ns\
           -recvd_correction_field       DR_CF2\
           -domain_number                $domain\
           -gmid                         0x$gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(DELAY_RESP_RX_P1_O)"\
                       -tc_def  $tc_def

    return 0
}

set RTS2    [expr ($RTS2_sec * pow(10,9)) + $RTS2_ns]

###############################################################################
#                                                                             #
# Step 16: Calculate meanDelay (MD2) at TEE.                                  #
###############################################################################

STEP 16

LOG -level 0 -msg "$tee_log_msg(CALC_MD2)"

if { $clock_step == "One-Step" } {

    set TS1          [format "%.f" $TS1]
    set TS2          [format "%.f" $TS2]
    set RTS2         [format "%.f" $RTS2]
    set S_OTS2       [format "%.f" $S_OTS2]
    set SYNC_CF2     [format "%.f" $SYNC_CF2]
    set DR_CF2       [format "%.f" $DR_CF2]

    set MD2 [ expr ((($TS1 - $TS2) + ($RTS2 - $S_OTS2) - $SYNC_CF2 - $DR_CF2)/2)]

    LOG -level 3 -msg "t2       = [nano $TS1] ns"
    LOG -level 3 -msg "t3       = [nano $TS2] ns"
    LOG -level 3 -msg "RTS2     = [nano $RTS2] ns"
    LOG -level 3 -msg "S_OTS2   = [nano $S_OTS2] ns"
    LOG -level 3 -msg "SYNC_CF2 = [nano $SYNC_CF2] ns"
    LOG -level 3 -msg "DR_CF2   = [nano $DR_CF2] ns"
    LOG -level 3 -msg "-----------------------------------------"
    LOG -level 3 -msg "MD2      = [nano $MD2] ns (formula: ((t2 - t3) + (RTS2 - S_OTS2) - SYNC_CF2 - DR_CF2)/2)"

} else {

    set TS1          [format "%.f" $TS1]
    set TS2          [format "%.f" $TS2]
    set RTS2         [format "%.f" $RTS2]
    set PTS2         [format "%.f" $PTS2]
    set SYNC_CF2     [format "%.f" $SYNC_CF2]
    set FU_CF2       [format "%.f" $FU_CF2]
    set DR_CF2       [format "%.f" $DR_CF2]

    set MD2 [ expr ((($TS1 - $TS2) + ($RTS2 - $PTS2) - $SYNC_CF2 - $FU_CF2 - $DR_CF2)/2)]

    LOG -level 3 -msg "t2       = [nano $TS1] ns"
    LOG -level 3 -msg "t3       = [nano $TS2] ns"
    LOG -level 3 -msg "RTS2     = [nano $RTS2] ns"
    LOG -level 3 -msg "PTS2     = [nano $PTS2] ns"
    LOG -level 3 -msg "SYNC_CF2 = [nano $SYNC_CF2] ns"
    LOG -level 3 -msg "FU_CF2   = [nano $FU_CF2] ns"
    LOG -level 3 -msg "DR_CF2   = [nano $DR_CF2] ns"
    LOG -level 3 -msg "-----------------------------------------"

    LOG -level 3 -msg "MD2      = [nano $MD2] ns (formula: ((t2 - t3) + (RTS2 - PTS2) - SYNC_CF2 - FU_CF2 - DR_CF2)/2)"

}

###############################################################################
#                                                                             #
# Step 17: Observe that MD2 is lesser than MD1.                               #
###############################################################################

STEP 17

LOG -level 0 -msg "$dut_log_msg(MD2_LESSER_MD1_O)"

LOG -level 3 -msg "Value of meanDelay (MD1) = [nano $MD1] ns"
LOG -level 3 -msg "value of meanDealy (MD2) = [nano $MD2] ns"

if {!( $MD2 < $MD1 )} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(MD2_LESSER_MD1_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 18: Configure ingressLatency on port P1 by setting ingressLatency to   #
#          -4 294 967 296 ns (i.e., the value of dataset expressed in         #
#          TimeInterval asymmetryCorrectionPortDS.ingressLatency = -2^48).    #
#                                                                             #
###############################################################################

STEP 18

set latency -4294967296

LOG -level 0 -msg "$dut_log_msg(CONFIG_TXPORTDS.INGRESSLATENCY)"

if {![ptp_ha::dut_set_ingress_latency\
             -port_num       $dut_port_1\
             -latency        $latency]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_TXPORTDS.INGRESSLATENCY_F)"\
                        -tc_def  $tc_def

     return 0
}

sleep $::ptp_ha_l1sync_wait

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
#                                                                             #
# Step 19: Observe that DUT transmits SYNC message on port P1 with following  #
#          parameters and store timestamp TS1.                                #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x00                                #
#                  Domain Number        = DN1                                 #
#                  Origin Timestamp     = S_OTS3                              #
#                  Correction Field     = SYNC_CF3                            #
###############################################################################

STEP 19

LOG -level 0 -msg "$tee_log_msg(SYNC_RX_P1_O)"

if {![ptp_ha::recv_sync \
           -port_num                     $tee_port_1\
           -filter_id                    $filter_id\
           -unicast_flag                 $::unicast_flag\
           -recvd_origin_timestamp_sec   S_OTS3_sec\
           -recvd_origin_timestamp_ns    S_OTS3_ns\
           -recvd_correction_field       SYNC_CF3\
           -rx_timestamp_sec             TS1_sec\
           -rx_timestamp_ns              TS1_ns\
           -domain_number                $domain\
           -gmid                         0x$gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

set S_OTS3  [expr ($S_OTS3_sec * pow(10,9)) + $S_OTS3_ns]
set TS1     [expr ($TS1_sec * pow(10,9)) + $TS1_ns]

###############################################################################
#                                                                             #
# Step 20: If the clock is two step, observe that DUT transmits FOLLOW_UP     #
#          message on the port P1 with following parameters.                  #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x08                                #
#                  Domain Number        = DN1                                 #
#                  Correction Field     = FU_CF3                              #
###############################################################################

if { $clock_step == "Two-Step" } {

STEP 20

LOG -level 0 -msg "$tee_log_msg(FOLLOW_UP_RX_P1_O)"

if {![ptp_ha::recv_followup \
           -port_num                           $tee_port_1\
           -filter_id                          $filter_id\
           -unicast_flag                       $::unicast_flag\
           -recvd_correction_field             FU_CF3\
           -recvd_precise_origin_timestamp_sec PTS3_sec\
           -recvd_precise_origin_timestamp_ns  PTS3_ns\
           -domain_number                      $domain\
           -gmid                               0x$gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FOLLOW_UP_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}
 }

set PTS3    [expr ($PTS3_sec * pow(10,9)) + $PTS3_ns]

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
#                                                                             #
# Step 21: Send DELAY_REQ message on port T1 with following parameters and    #
#          store timestamp TS2.                                               #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type         = 0x01                                #
#                  Domain Number        = DN1                                 #
#                  Origin Timestamp     = D_OTS3                              #
#                  Correction Field     = 0                                   #
###############################################################################

STEP 21

LOG -level 0 -msg "$tee_log_msg(DELAY_REQ_TX)"

if {![ptp_ha::send_delay_req \
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -port_num                $tee_port_1\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -correction_field        0\
           -count                   $infinity\
           -tx_origin_timestamp_sec TS2_sec\
           -tx_origin_timestamp_ns  TS2_ns\
           -gmid                    0x$gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(DELAY_REQ_TX_F)"\
                       -tc_def  $tc_def

    return 0
}

# Transmission of Delay_Req is assumed to happen within 400ms. Taking timestamp
# from xena is not correct due to difference in the time between Chassis and
# module.

set TS2     [expr $TS1 + 400000000]

###############################################################################
#                                                                             #
# Step 22: Observe that the DUT transmits DELAY_RESP message on the port P1   #
#          with following parameters.                                         #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type        = 0x09                                #
#                   Domain Number       = DN1                                 #
#                   Receive Timestamp   = RTS3                                #
###############################################################################

STEP 22

LOG -level 0 -msg "$tee_log_msg(DELAY_RESP_RX_P1_O)"

if {![ptp_ha::recv_delay_resp \
           -port_num                     $tee_port_1\
           -filter_id                    $filter_id\
           -unicast_flag                 $::unicast_flag\
           -recvd_receive_timestamp_sec  RTS3_sec\
           -recvd_receive_timestamp_ns   RTS3_ns\
           -recvd_correction_field       DR_CF3\
           -domain_number                $domain\
           -gmid                         0x$gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(DELAY_RESP_RX_P1_O)"\
                       -tc_def  $tc_def

    return 0
}

set RTS3    [expr ($RTS3_sec * pow(10,9)) + $RTS3_ns]

###############################################################################
#                                                                             #
# Step 23: Calculate meanDelay (MD3) at TEE.                                  #
#                                                                             #
###############################################################################

STEP 16

LOG -level 0 -msg "$tee_log_msg(CALC_MD3)"

if { $clock_step == "One-Step" } {

    set TS1          [format "%.f" $TS1]
    set TS2          [format "%.f" $TS2]
    set RTS3         [format "%.f" $RTS3]
    set S_OTS3       [format "%.f" $S_OTS3]
    set SYNC_CF3     [format "%.f" $SYNC_CF3]
    set DR_CF3       [format "%.f" $DR_CF3]

    set MD3 [ expr ((($TS1 - $TS2) + ($RTS3 - $S_OTS3) - $SYNC_CF3 - $DR_CF3)/2)]

    LOG -level 3 -msg "t2       = [nano $TS1] ns"
    LOG -level 3 -msg "t3       = [nano $TS2] ns"
    LOG -level 3 -msg "RTS3     = [nano $RTS3] ns"
    LOG -level 3 -msg "S_OTS3   = [nano $S_OTS3] ns"
    LOG -level 3 -msg "SYNC_CF3 = [nano $SYNC_CF3] ns"
    LOG -level 3 -msg "DR_CF3   = [nano $DR_CF3] ns"
    LOG -level 3 -msg "-----------------------------------------"
    LOG -level 3 -msg "MD3      = [nano $MD3] ns (formula: ((t2 - t3) + (RTS3 - S_OTS3) - SYNC_CF3 - DR_CF3)/2)"

} else {

    set TS1          [format "%.f" $TS1]
    set TS2          [format "%.f" $TS2]
    set RTS3         [format "%.f" $RTS3]
    set PTS3         [format "%.f" $PTS3]
    set SYNC_CF3     [format "%.f" $SYNC_CF3]
    set FU_CF3       [format "%.f" $FU_CF3]
    set DR_CF3       [format "%.f" $DR_CF3]

    set MD3 [ expr ((($TS1 - $TS2) + ($RTS3 - $PTS3) - $SYNC_CF3 - $FU_CF3 - $DR_CF3)/2)]

    LOG -level 3 -msg "t2       = [nano $TS1] ns"
    LOG -level 3 -msg "t3       = [nano $TS2] ns"
    LOG -level 3 -msg "RTS3     = [nano $RTS3] ns"
    LOG -level 3 -msg "PTS3     = [nano $PTS3] ns"
    LOG -level 3 -msg "SYNC_CF3 = [nano $SYNC_CF3] ns"
    LOG -level 3 -msg "FU_CF3   = [nano $FU_CF3] ns"
    LOG -level 3 -msg "DR_CF3   = [nano $DR_CF3] ns"
    LOG -level 3 -msg "-----------------------------------------"

    LOG -level 3 -msg "MD3      = [nano $MD3] ns (formula: ((t2 - t3) + (RTS3 - PTS3) - SYNC_CF3 - FU_CF3 - DR_CF3)/2)"

}

###############################################################################
# Step 24: Verify that MD3 is greater than MD1.                               #
#                                                                             #
###############################################################################

STEP 24

LOG -level 0 -msg "$dut_log_msg(MD3_GREATER_MD1_V)"

    LOG -level 3 -msg "Value of meanDelay (MD1) = [nano $MD1] ns"
    LOG -level 3 -msg "Value of meanDelay (MD3) = [nano $MD3] ns"

if {!( $MD3 > $MD1 )} {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL -reason  "PTP enabled device does not generate Ingress\
                                timestamp in Delay_Req (event) messages from\
                                timestampCorrectionPortDS.ingressLatency when using\
                                Delay Request-Response mechanism"\
                       -tc_def  $tc_def
    return 0
}

TEE_CLEANUP

TC_CLEAN_AND_PASS -reason  "PTP enabled device generates Ingress\
                            timestamp in Delay_Req (event) messages from\
                            timestampCorrectionPortDS.ingressLatency when using\
                            Delay Request-Response mechanism"\
                  -tc_def   $tc_def

return 1

########################### END OF TEST CASE ##################################
