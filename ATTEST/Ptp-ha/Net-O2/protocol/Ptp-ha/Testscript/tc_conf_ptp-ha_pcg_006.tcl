#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_ptp-ha_pcg_006                                  #
# Test Case Version : 1.6                                                     #
# Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    #
# Module Name       : PTP-HA Configuration Group (PCG)                        #
#                                                                             #
# Title             : announceReceiptTimeout                                  #
#                                                                             #
# Purpose           : To verify that a PTP enabled device supports to         #
#                     configure announceReceiptTimeout interval to range of   #
#                     value 2 to 10.                                          #
#                                                                             #
# Reference         : P1588/D1.3, February 2018 V3.01 Clause J.5.2 Page 412   #
#                                                                             #
# Conformance Type  : SHALL                                                   #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 001                                                     #
# Test Topology     : 001                                                     #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode - one-step/two-step> |           #
#           |                     <Configure logAnnounceInterval, | P1        #
#           |                             announceReceiptTimeout> |           #
#           |                         <Configure logSyncInterval> | P1        #
#           |                  <Configure logMinDelayReqInterval> | P1        #
#           |                    <Configure Priority1, Priority2> | P1        #
#           |                                     <Enable L1SYNC> | P1        #
#           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        #
#           |                     <Disable L1SynOptParams option> | P1        #
#           |           <Enable asymmetryCorrectionPortDS.enable> | P1        #
#           | <Configure timestampCorrectionPortDS.egressLatency, | P1        #
#           |           timestampCorrectionPortDS.ingressLatency, |           #
#           |        asymmetryCorrectionPortDS.constantAsymmetry, |           #
#           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           #
#           |                                                     |           #
#           |               ANNOUNCE [MSG_TYPE = 0x0B, DN = DN1,  |           #
#           |                             ANN_INT = 1, PRIOR = Z] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |  ANNOUNCE [MSG_TYPE = 0x0B,                         |           #
#           |  DN = DN1, ANN_INT = 1, PRIOR = Z-1]                |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           | <Send Sync/ Sync & Follow-up messages               |           #
#           | based on clock step>                                |           #
#           |                                                     |           #
#           |          < Wait for 6s to complete BMCA >           |           #
#           |                                                     |           #
#           |  ANNOUNCE [MSG_TYPE = 0x0B,                         |           #
#           |  DN = DN1, ANN_INT = 1, PRIOR = Z-1]                |           #
#        T1 |---->>----XX                                         | P1        #
#           |                                                     |           #
#           |               ANNOUNCE [MSG_TYPE = 0x0B, DN = DN1,  |           #
#           |                                        ANN_INT = 1] |           #
#        T1 |                     XX--------------------------<<--| P1        #
#           |                                         (within 6s) |           #
#           |                                                     |           #
#           |               ANNOUNCE [MSG_TYPE = 0x0B, DN = DN1,  |           #
#           |                                        ANN_INT = 1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |              <Configure announceReceiptTimeout = 2> |           #
#           |                                                     |           #
#           |  ANNOUNCE [MSG_TYPE = 0x0B,                         |           #
#           |  DN = DN1, ANN_INT = 1, PRIOR = Z-1]                |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           | <Send Sync/ Sync & Follow-up messages               |           #
#           | based on clock step>                                |           #
#           |                                                     |           #
#           |          < Wait for 6s to complete BMCA >           |           #
#           |                                                     |           #
#           |  ANNOUNCE [MSG_TYPE = 0x0B,                         |           #
#           |  DN = DN1, ANN_INT = 1, PRIOR = Z-1]                |           #
#        T1 |---->>----XX                                         | P1        #
#           |                                                     |           #
#           |               ANNOUNCE [MSG_TYPE = 0x0B, DN = DN1,  |           #
#           |                                        ANN_INT = 1] |           #
#        T1 |                     XX--------------------------<<--| P1        #
#           |                                         (within 4s) |           #
#           |                                                     |           #
#           |               ANNOUNCE [MSG_TYPE = 0x0B, DN = DN1,  |           #
#           |                                        ANN_INT = 1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |             <Configure announceReceiptTimeout = 10> |           #
#           |                                                     |           #
#           |  ANNOUNCE [MSG_TYPE = 0x0B,                         |           #
#           |  DN = DN1, ANN_INT = 1, PRIOR = Z-1]                |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           | <Send Sync/ Sync & Follow-up messages               |           #
#           | based on clock step>                                |           #
#           |                                                     |           #
#           |          < Wait for 6s to complete BMCA >           |           #
#           |                                                     |           #
#           |  ANNOUNCE [MSG_TYPE = 0x0B,                         |           #
#           |  DN = DN1, ANN_INT = 1, PRIOR = Z-1]                |           #
#        T1 |---->>----XX                                         | P1        #
#           |                                                     |           #
#           |               ANNOUNCE [MSG_TYPE = 0x0B, DN = DN1,  |           #
#           |                                        ANN_INT = 1] |           #
#        T1 |                     XX--------------------------<<--| P1        #
#           |                                         within 20s) |           #
#           |                                                     |           #
#           |               ANNOUNCE [MSG_TYPE = 0x0B, DN = DN1,  |           #
#           |                                        ANN_INT = 1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |              <Configure announceReceiptTimeout = 3> |           #
#           |                                                     |           #
#           |  ANNOUNCE [MSG_TYPE = 0x0B,                         |           #
#           |  DN = DN1, ANN_INT = 1, PRIOR = Z-1]                |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           | <Send Sync/ Sync & Follow-up messages               |           #
#           | based on clock step>                                |           #
#           |                                                     |           #
#           |          < Wait for 6s to complete BMCA >           |           #
#           |                                                     |           #
#           |  ANNOUNCE [MSG_TYPE = 0x0B,                         |           #
#           |  DN = DN1, ANN_INT = 1, PRIOR = Z-1]                |           #
#        T1 |---->>----XX                                         | P1        #
#           |                                                     |           #
#           |               ANNOUNCE [MSG_TYPE = 0x0B, DN = DN1,  |           #
#           |                                        ANN_INT = 1] |           #
#        T1 |                     XX--------------------------<<--| P1        #
#           |                                          within 6s) |           #
#           |                                                     |           #
#           |               ANNOUNCE [MSG_TYPE = 0x0B, DN = DN1,  |           #
#           |                                        ANN_INT = 1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     MSG_TYPE  = Message Type                                                #
#     DN        = Domain Number                                               #
#     BC        = Boundary Clock                                              #
#     OC        = Ordinary Clock                                              #
#                                                                             #
# NOTE :                                                                      #
#                                                                             #
#  1. This objective is verified using the High Accuracy Delay Request-       #
#     Response Default PTP Profile                                            #
#                                                                             #
#  2. Timeout = (2 ^ logAnnounceInterval) * announceReceiptTimeout            #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.ingressLatency,           #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters:                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  Priority1          = Z                                     #
#                  ANN_INT            = 1                                     #
#                                                                             #
# Step 4 : Send periodic ANNOUNCE message with Priority1 value decremented    #
#          from the Priority1 value of received Announce message on port T1   #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  Priority1          = Z-1                                   #
#                  ANN_INT            = 1                                     #
#                                                                             #
# Step 5 : Send periodic SYNC message on the port P1 with following           #
#          parameters:                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x00                                  #
#                  Domain Number      = DN1                                   #
#                                                                             #
# Step 5a: If the clock is two-step clock, send periodic FOLLOW_UP message on #
#          port T1 with following parameters:                                 #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x00                                  #
#                  Domain Number      = DN1                                   #
#                                                                             #
# Step 6 : Wait for 6s for completing BMCA and stop ANNOUNCE, SYNC and        #
#          FOLLOW_UP (if two-step clock) messages to port T1.                 #
#                                                                             #
# Step 7 : Observe that DUT does not transmit ANNOUNCE message on the port P1 #
#          within 6s.                                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  ANN_INT            = 1                                     #
#                                                                             #
# Step 8 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters after 6s:                                     #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  ANN_INT            = 1                                     #
#                                                                             #
# Step 9 : Configure announceReceiptTimeout as 2 on DUT.                      #
#                                                                             #
# Step 10: Send periodic ANNOUNCE message on port T1 with following parameters#
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  Priority1          = Z-1                                   #
#                  ANN_INT            = 1                                     #
#                                                                             #
# Step 11: Send periodic SYNC message on the port P1 with following           #
#          parameters:                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x00                                  #
#                  Domain Number      = DN1                                   #
#                                                                             #
# Step 11a: If the clock is two-step clock, send periodic FOLLOW_UP message on#
#          port T1 with following parameters:                                 #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x00                                  #
#                  Domain Number      = DN1                                   #
#                                                                             #
# Step 12: Wait for 6s for completing BMCA and stop ANNOUNCE, SYNC and        #
#          FOLLOW_UP (if two-step clock) messages to port T1.                 #
#                                                                             #
# Step 13: Observe that DUT does not transmit ANNOUNCE message on the port P1 #
#          within 4s.                                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  ANN_INT            = 1                                     #
#                                                                             #
# Step 14: Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters after 4s:                                     #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  ANN_INT            = 1                                     #
#                                                                             #
# Step 15: Configure announceReceiptTimeout as 10 on DUT.                     #
#                                                                             #
# Step 16: Send periodic ANNOUNCE message on port T1 with following parameters#
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  Priority1          = Z-1                                   #
#                  ANN_INT            = 1                                     #
#                                                                             #
# Step 17: Send periodic SYNC message on the port P1 with following           #
#          parameters:                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x00                                  #
#                  Domain Number      = DN1                                   #
#                                                                             #
# Step 18a: If the clock is two-step clock, send periodic FOLLOW_UP message on#
#          port T1 with following parameters:                                 #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x00                                  #
#                  Domain Number      = DN1                                   #
#                                                                             #
# Step 19: Wait for 6s for completing BMCA and stop ANNOUNCE, SYNC and        #
#          FOLLOW_UP (if two-step clock) messages to port T1.                 #
#                                                                             #
# Step 20: Observe that DUT does not transmit ANNOUNCE message on the port P1 #
#          within 20s.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  ANN_INT            = 1                                     #
#                                                                             #
# Step 21: Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters after 20s:                                    #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  ANN_INT            = 1                                     #
#                                                                             #
# Step 22: Configure announceReceiptTimeout as 3 on DUT.                      #
#                                                                             #
# Step 23: Send periodic ANNOUNCE message on port T1 with following parameters#
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  Priority1          = Z-1                                   #
#                  ANN_INT            = 1                                     #
#                                                                             #
# Step 24: Send periodic SYNC message on the port P1 with following           #
#          parameters:                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x00                                  #
#                  Domain Number      = DN1                                   #
#                                                                             #
# Step 25a: If the clock is two-step clock, send periodic FOLLOW_UP message on#
#          port T1 with following parameters:                                 #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x00                                  #
#                  Domain Number      = DN1                                   #
#                                                                             #
# Step 26: Wait for 6s for completing BMCA and stop ANNOUNCE, SYNC and        #
#          FOLLOW_UP (if two-step clock) messages to port T1.                 #
#                                                                             #
# Step 27: Verify that DUT does not transmit ANNOUNCE message on the port P1  #
#          within 6s.                                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  ANN_INT            = 1                                     #
#                                                                             #
# Step 28: Verify that DUT transmits ANNOUNCE message on the port P1 with     #
#          following parameters after 6s:                                     #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  ANN_INT            = 1                                     #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          May/2018      CERN          Initial                            #
# 1.1          Jun/2018      CERN          Fixed dut_port in                  #
#                                          dut_reset_announce_timeout         #
# 1.2          Jul/2018      CERN          Added ptp_ha::display_port_details #
#                                          to print port details              #
# 1.3          Sep/2018      CERN          a) Removed destination MAC and IP  #
#                                             in send functions.              #
#                                          b) Removed timeout in recv         #
#                                             functions.                      #
#                                          c) Moved timeout calculation to    #
#                                             PTP-HA global file.             #
#                                          d) Capture buffer is cleared after #
#                                             every configuration step.       #
# 1.4          Oct/2018      CERN          Fixed errors in the timeout        #
#                                          calculation.                       #
# 1.5          Nov/2018      CERN          Fixed script error.                #
# 1.6          Nov/2018      CERN          Reduced 500ms in the time waiting  #
#                                          for not receiving Announce message.#
#                                                                             #
###############################################################################
# Copyright (c) 2018 CERN                                                     #
###############################################################################

global dut_log_msg
global tee_log_msg
global session_id

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def "To verify that a PTP enabled device supports to\
            configure announceReceiptTimeout interval to range of\
            value 2 to 10."

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set dut_port_1                     $::dut_port_num_1
set vlan_id                        $::ptp_ha_vlan_id

set dut_ip                         $::dut_test_port_ip
set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT

set domain                         $::DEFAULT_DOMAIN
set gmid                           $::GRANDMASTER_ID
set infinity                       $::INFINITY

set sequence_id_ann                1
set sequence_id_l1sync             43692

set ann_int                        1
set announcetimeout                0

set clock_step                     $::ptp_dut_clock_step

########################### END - INITIALIZATION ##############################

ptp_ha::display_port_details

ptp_ha::dut_Initialization

proc cleanup {} {

    ptp_ha::dut_cleanup_setup_001

    ptp_ha::dut_reset_announce_timeout \
            -port_num          $::dut_port_num_1

}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.ingressLatency,           #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
###############################################################################

STEP 1

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP1)"

if {![ptp_ha::dut_configure_setup_001]} {

    LOG -msg "$dut_log_msg(INIT_SETUP1_F)"

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP1_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
###############################################################################

STEP 2

LOG -level 0 -msg "$tee_log_msg(INIT)"

ptp_ha::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

    if {![PTP_HA_UNICAST_PRE_REQUISITE_CONFIGURATION\
                        -port_num       $tee_port_1\
                        -tee_mac        $tee_mac\
                        -tee_ip         $tee_ip\
                        -dut_ip         $dut_ip\
                        -vlan_id        $vlan_id\
                        -dut_mac        tee_dest_mac\
                        -timeout        $arp_timeout]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
                           -tc_def  $tc_def

        return 0
    }

}

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_1\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def  $tc_def

    return 0
}

set filter_id [lindex $id1 end]

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

#(Part 1)#

###############################################################################
# Step 3 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters:                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  Priority1          = Z                                     #
#                  ANN_INT            = 1                                     #
###############################################################################

STEP 3

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
           -port_num                $tee_port_1\
           -domain_number           $domain\
           -filter_id               $filter_id\
           -recvd_gm_priority1      priority1\
           -log_message_interval    $ann_int]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                        -tc_def  $tc_def

    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 4 : Send periodic ANNOUNCE message with Priority1 value decremented    #
#          from the Priority1 value of received Announce message on port T1   #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  Priority1          = Z-1                                   #
#                  ANN_INT            = 1                                     #
###############################################################################

STEP 4

set priority [expr $priority1 - 1]

LOG -level 0 -msg "$tee_log_msg(ANN_TX_T1)"

if {![ptp_ha::send_announce \
           -port_num                $tee_port_1\
           -gm_priority1            $priority\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -log_message_interval    $ann_int\
           -sequence_id             $sequence_id_ann\
           -count                   $infinity\
           -gmid                    $gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANN_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 5 : Send periodic SYNC message on the port P1 with following           #
#          parameters:                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x00                                  #
#                  Domain Number      = DN1                                   #
###############################################################################

STEP 5

LOG -level 0 -msg "$tee_log_msg(SYNC_TX_T1)"

if {![ptp_ha::send_sync \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -count                   $infinity\
           -gmid                    $gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 5a: If the clock is two-step clock, send periodic FOLLOW_UP message on #
#          port T1 with following parameters:                                 #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x00                                  #
#                  Domain Number      = DN1                                   #
###############################################################################

if { $clock_step == "Two-Step" } {

STEP 5a

LOG -level 0 -msg "$tee_log_msg(FOLLOWUP_TX_T1)"

if {![ptp_ha::send_followup \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -count                   $infinity\
           -gmid                    $gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FOLLOWUP_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}
 }

###############################################################################
#                                                                             #
# Step 6 : Wait for 6s for completing BMCA and stop ANNOUNCE, SYNC and        #
#          FOLLOW_UP (if two-step clock) messages to port T1.                 #
#                                                                             #
###############################################################################

STEP 6a

LOG -level 0 -msg "$dut_log_msg(WAIT_BMCA)"

sleep $::ptp_ha_bmca

STEP 6b

LOG -level 0 -msg "$tee_log_msg(STOP_ANNOUNCE_TX_T1)"

if {![ptp_ha::stop_announce ]} {
    
    TEE_CLEANUP
    
    TC_CLEAN_AND_ABORT   -reason "$tee_log_msg(STOP_ANNOUNCE_TX_T1_F)"\
                         -tc_def  $tc_def

    return 0
}

STEP 6c

LOG -level 0 -msg "$tee_log_msg(STOP_SYNC_TX_T1)"

if {![ptp_ha::stop_sync ]} {
    
    TEE_CLEANUP
    
    TC_CLEAN_AND_ABORT   -reason "$tee_log_msg(STOP_SYNC_TX_T1_F)"\
                         -tc_def  $tc_def

    return 0
}

if { $clock_step == "Two-Step" } {

STEP 6d

LOG -level 0 -msg "$tee_log_msg(STOP_FOLLOWUP_TX_T1)"

if {![ptp_ha::stop_followup ]} {
    
    TEE_CLEANUP
    
    TC_CLEAN_AND_ABORT   -reason "$tee_log_msg(STOP_FOLLOWUP_TX_T1_F)"\
                         -tc_def  $tc_def

    return 0
}

}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 7 : Observe that DUT does not transmit ANNOUNCE message on the port P1 #
#          within 6s.                                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  ANN_INT            = 1                                     #
###############################################################################

STEP 7

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_NRX_P1_O)"

set ptp_ha_announce_timeout             [expr $::ptp_ha_announce_timeout - 0.5]

if {[ptp_ha::recv_announce \
           -port_num                $tee_port_1\
           -domain_number           $domain\
           -reset_count             $::reset_count_off\
           -filter_id               $filter_id\
           -log_message_interval    $ann_int\
           -timeout                 $ptp_ha_announce_timeout]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANNOUNCE_NRX_P1_F)"\
                        -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 8 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters after 6s:                                     #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  ANN_INT            = 1                                     #
###############################################################################

STEP 8

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
           -port_num                $tee_port_1\
           -domain_number           $domain\
           -filter_id               $filter_id\
           -log_message_interval    $ann_int]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                        -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 9 : Configure announceReceiptTimeout as 2 on DUT.                      #
#                                                                             #
###############################################################################

STEP 9

set announce_timeout [ expr $announcetimeout + 2 ]

LOG -level 0 -msg "$dut_log_msg(CONFIG_ANN_TIMEOUT_2)"

  if {![ptp_ha::dut_set_announce_timeout \
                -port_num                $dut_port_1\
                -announce_timeout        $announce_timeout]} {

      TEE_CLEANUP

      TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_ANN_TIMEOUT_F)"\
                         -tc_def  $tc_def

      return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 10: Send periodic ANNOUNCE message on port T1 with following parameters#
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  Priority1          = Z-1                                   #
#                  ANN_INT            = 1                                     #
###############################################################################

STEP 10

set priority [expr $priority1 - 1]

LOG -level 0 -msg "$tee_log_msg(ANN_TX_T1)"

if {![ptp_ha::send_announce \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -gm_priority1            $priority\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -log_message_interval    $ann_int\
           -sequence_id             $sequence_id_ann\
           -count                   $infinity\
           -gmid                    $gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANN_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
#                                                                             #
# Step 11: Send periodic SYNC message on the port P1 with following           #
#          parameters:                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x00                                  #
#                  Domain Number      = DN1                                   #
###############################################################################

STEP 11

LOG -level 0 -msg "$tee_log_msg(SYNC_TX_T1)"

if {![ptp_ha::send_sync \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -count                   $infinity\
           -gmid                    $gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
#                                                                             #
# Step 11a: If the clock is two-step clock, send periodic FOLLOW_UP message on#
#          port T1 with following parameters:                                 #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x00                                  #
#                  Domain Number      = DN1                                   #
###############################################################################

if { $clock_step == "Two-Step" } {

STEP 11a

LOG -level 0 -msg "$tee_log_msg(FOLLOWUP_TX_T1)"

if {![ptp_ha::send_followup \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -count                   $infinity\
           -gmid                    $gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FOLLOWUP_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}
 }

###############################################################################
#                                                                             #
# Step 12: Wait for 6s for completing BMCA and stop ANNOUNCE, SYNC and        #
#          FOLLOW_UP (if two-step clock) messages to port T1.                 #
#                                                                             #
###############################################################################

STEP 12a

LOG -level 0 -msg "$dut_log_msg(WAIT_BMCA)"

sleep $::ptp_ha_bmca

STEP 12b

LOG -level 0 -msg "$tee_log_msg(STOP_ANNOUNCE_TX_T1)"

if {![ptp_ha::stop_announce ]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT   -reason "$tee_log_msg(STOP_ANNOUNCE_TX_T1_F)"\
                         -tc_def $tc_def

    return 0
}

STEP 12c

LOG -level 0 -msg "$tee_log_msg(STOP_SYNC_TX_T1)"

if {![ptp_ha::stop_sync ]} {
    
    TEE_CLEANUP
    
    TC_CLEAN_AND_ABORT   -reason "$tee_log_msg(STOP_SYNC_TX_T1_F)"\
                         -tc_def $tc_def

    return 0
}

if { $clock_step == "Two-Step" } {

STEP 12d

LOG -level 0 -msg "$tee_log_msg(STOP_FOLLOWUP_TX_T1)"

if {![ptp_ha::stop_followup ]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT   -reason "$tee_log_msg(STOP_FOLLOWUP_TX_T1_F)"\
                         -tc_def $tc_def

    return 0
}
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 13: Observe that DUT does not transmit ANNOUNCE message on the port P1 #
#          within 4s.                                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  ANN_INT            = 1                                     #
###############################################################################

STEP 13

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_NRX_P1_O)"

set ptp_ha_announce_timeout             [expr (int(pow(2,$::ptp_announce_interval)) * $announce_timeout)]
set ptp_ha_announce_timeout             [expr $ptp_ha_announce_timeout - 0.5]

if {[ptp_ha::recv_announce \
           -port_num                $tee_port_1\
           -domain_number           $domain\
           -filter_id               $filter_id\
           -log_message_interval    $ann_int\
           -timeout                 $ptp_ha_announce_timeout]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANNOUNCE_NRX_P1_F)"\
                        -tc_def  $tc_def

    return 0
}

###############################################################################
#                                                                             #
# Step 14: Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters after 4s:                                     #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  ANN_INT            = 1                                     #
###############################################################################

STEP 14

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

sleep 4

if {![ptp_ha::recv_announce \
           -port_num                $tee_port_1\
           -domain_number           $domain\
           -filter_id               $filter_id\
           -log_message_interval    $ann_int]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                        -tc_def  $tc_def

    return 0
}

###############################################################################
#                                                                             #
# Step 15: Configure announceReceiptTimeout as 10 on DUT.                     #
#                                                                             #
###############################################################################

STEP 15

set announce_timeout [ expr $announcetimeout + 10 ]

LOG -level 0 -msg "$dut_log_msg(CONFIG_ANN_TIMEOUT_10)"

  if {![ptp_ha::dut_set_announce_timeout \
                -port_num                $dut_port_1\
                -announce_timeout        $announce_timeout]} {

      TEE_CLEANUP

      TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_ANN_TIMEOUT_F)"\
                         -tc_def  $tc_def

      return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 16: Send periodic ANNOUNCE message on port T1 with following parameters#
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  Priority1          = Z-1                                   #
#                  ANN_INT            = 1                                     #
###############################################################################

STEP 16

set priority [expr $priority1 - 1]

LOG -level 0 -msg "$tee_log_msg(ANN_TX_T1)"

if {![ptp_ha::send_announce \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -gm_priority1            $priority\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -log_message_interval    $ann_int\
           -sequence_id             $sequence_id_ann\
           -count                   $infinity\
           -gmid                    $gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANN_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
#                                                                             #
# Step 17: Send periodic SYNC message on the port P1 with following           #
#          parameters:                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x00                                  #
#                  Domain Number      = DN1                                   #
###############################################################################

STEP 17

LOG -level 0 -msg "$tee_log_msg(SYNC_TX_T1)"

if {![ptp_ha::send_sync \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -count                   $infinity\
           -gmid                    $gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
#                                                                             #
# Step 18a: If the clock is two-step clock, send periodic FOLLOW_UP message on#
#          port T1 with following parameters:                                 #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x00                                  #
#                  Domain Number      = DN1                                   #
###############################################################################

if { $clock_step == "Two-Step" } {

STEP 18a

LOG -level 0 -msg "$tee_log_msg(FOLLOWUP_TX_T1)"

if {![ptp_ha::send_followup \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -count                   $infinity\
           -gmid                    $gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FOLLOWUP_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}
 }

###############################################################################
#                                                                             #
# Step 19: Wait for 6s for completing BMCA and stop ANNOUNCE, SYNC and        #
#          FOLLOW_UP (if two-step clock) messages to port T1.                 #
#                                                                             #
###############################################################################

STEP 19a

LOG -level 0 -msg "$dut_log_msg(WAIT_BMCA)"

sleep $::ptp_ha_bmca

STEP 19b

LOG -level 0 -msg "$tee_log_msg(STOP_ANNOUNCE_TX_T1)"

if {![ptp_ha::stop_announce]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT   -reason "$tee_log_msg(STOP_ANNOUNCE_TX_T1_F)"\
                         -tc_def $tc_def

    return 0
}

STEP 19c

LOG -level 0 -msg "$tee_log_msg(STOP_SYNC_TX_T1)"

if {![ptp_ha::stop_sync]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT   -reason "$tee_log_msg(STOP_SYNC_TX_T1_F)"\
                         -tc_def $tc_def

    return 0
}

if { $clock_step == "Two-Step" } {

STEP 19d

LOG -level 0 -msg "$tee_log_msg(STOP_FOLLOWUP_TX_T1)"

if {![ptp_ha::stop_followup]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT   -reason "$tee_log_msg(STOP_FOLLOWUP_TX_T1_F)"\
                         -tc_def $tc_def

    return 0
}
 }

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 20: Observe that DUT does not transmit ANNOUNCE message on the port P1 #
#          within 20s.                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  ANN_INT            = 1                                     #
###############################################################################

STEP 20

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_NRX_P1_O)"

set ptp_ha_announce_timeout             [expr (int(pow(2,$::ptp_announce_interval)) * $announce_timeout)]
set ptp_ha_announce_timeout             [expr $ptp_ha_announce_timeout - 0.5]

if {[ptp_ha::recv_announce \
           -port_num                $tee_port_1\
           -domain_number           $domain\
           -filter_id               $filter_id\
           -log_message_interval    $ann_int\
           -timeout                 $ptp_ha_announce_timeout]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANNOUNCE_NRX_P1_F)"\
                        -tc_def  $tc_def

    return 0
}

###############################################################################
#                                                                             #
# Step 21: Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters after 20s:                                    #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  ANN_INT            = 1                                     #
###############################################################################

STEP 21

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
           -port_num                $tee_port_1\
           -domain_number           $domain\
           -filter_id               $filter_id\
           -log_message_interval    $ann_int]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                        -tc_def  $tc_def

    return 0
}

###############################################################################
#                                                                             #
# Step 22: Configure announceReceiptTimeout as 3 on DUT.                      #
###############################################################################

STEP 22

set announce_timeout [ expr $announcetimeout + 3 ]

LOG -level 0 -msg "$dut_log_msg(CONFIG_ANN_TIMEOUT_3)"

  if {![ptp_ha::dut_set_announce_timeout \
                -port_num                $dut_port_1\
                -announce_timeout        $announce_timeout]} {

      TEE_CLEANUP

      TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CONFIG_ANN_TIMEOUT_F)"\
                         -tc_def  $tc_def

      return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
#                                                                             #
# Step 23: Send periodic ANNOUNCE message on port T1 with following parameters#
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  Priority1          = Z-1                                   #
#                  ANN_INT            = 1                                     #
###############################################################################

STEP 23

set priority [expr $priority1 - 1]

LOG -level 0 -msg "$tee_log_msg(ANN_TX_T1)"

if {![ptp_ha::send_announce \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -gm_priority1            $priority\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -log_message_interval    $ann_int\
           -sequence_id             $sequence_id_ann\
           -count                   $infinity\
           -gmid                    $gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANN_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
#                                                                             #
# Step 24: Send periodic SYNC message on the port P1 with following           #
#          parameters:                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x00                                  #
#                  Domain Number      = DN1                                   #
#                                                                             #
###############################################################################

STEP 24

LOG -level 0 -msg "$tee_log_msg(SYNC_TX_T1)"

if {![ptp_ha::send_sync \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -count                   $infinity\
           -gmid                    $gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 25a: If the clock is two-step clock, send periodic FOLLOW_UP message on#
#          port T1 with following parameters:                                 #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x00                                  #
#                  Domain Number      = DN1                                   #
#                                                                             #
###############################################################################

if { $clock_step == "Two-Step" } {

STEP 25a

LOG -level 0 -msg "$tee_log_msg(SYNC_TX_T1)"

if {![ptp_ha::send_followup \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -domain_number           $domain\
           -count                   $infinity\
           -gmid                    $gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}
 }

###############################################################################
# Step 26: Wait for 6s for completing BMCA and stop ANNOUNCE, SYNC and        #
#          FOLLOW_UP (if two-step clock) messages to port T1.                 #
#                                                                             #
###############################################################################

STEP 26a

LOG -level 0 -msg "$dut_log_msg(WAIT_BMCA)"

sleep $::ptp_ha_bmca

STEP 26b

LOG -level 0 -msg "$tee_log_msg(STOP_ANNOUNCE_TX_T1)"

if {![ptp_ha::stop_announce]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT   -reason "$tee_log_msg(STOP_ANNOUNCE_TX_T1_F)"\
                         -tc_def $tc_def

    return 0
}

STEP 26c

LOG -level 0 -msg "$tee_log_msg(STOP_SYNC_TX_T1)"

if {![ptp_ha::stop_sync]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT   -reason "$tee_log_msg(STOP_SYNC_TX_T1_F)"\
                         -tc_def $tc_def

    return 0
}

if { $clock_step == "Two-Step" } {

STEP 26d

LOG -level 0 -msg "$tee_log_msg(STOP_FOLLOWUP_TX_T1)"

if {![ptp_ha::stop_followup]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT   -reason "$tee_log_msg(STOP_FOLLOWUP_TX_T1_F)"\
                         -tc_def $tc_def

    return 0
}
 }

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 27: Verify that DUT does not transmit ANNOUNCE message on the port P1  #
#          within 6s.                                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  ANN_INT            = 1                                     #
###############################################################################

STEP 27

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_NRX_P1_V)"

set ptp_ha_announce_timeout             [expr (int(pow(2,$::ptp_announce_interval)) * $announce_timeout)]
set ptp_ha_announce_timeout             [expr $ptp_ha_announce_timeout - 0.5]

if {[ptp_ha::recv_announce \
           -port_num                $tee_port_1\
           -domain_number           $domain\
           -filter_id               $filter_id\
           -log_message_interval    $ann_int\
           -timeout                 $ptp_ha_announce_timeout]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANNOUNCE_NRX_P1_F)"\
                        -tc_def  $tc_def

    return 0
}

###############################################################################
#                                                                             #
# Step 28: Verify that DUT transmits ANNOUNCE message on the port P1 with     #
#          following parameters after 6s:                                     #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = DN1                                   #
#                  ANN_INT            = 1                                     #
#                                                                             #
###############################################################################

STEP 28

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_V)"

if {![ptp_ha::recv_announce \
           -port_num                $tee_port_1\
           -domain_number           $domain\
           -filter_id               $filter_id\
           -log_message_interval    $ann_int]} {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL  -reason "PTP enabled device does not support to\
                                configure announceReceiptTimeout interval to range of\
                                value 2 to 10"\
                        -tc_def $tc_def

    return 0
}

TEE_CLEANUP

TC_CLEAN_AND_PASS   -reason "PTP enabled device supports to\
                             configure announceReceiptTimeout interval to range of\
                             value 2 to 10"\
                    -tc_def  $tc_def
return 1

############################### END OF TEST CASE ###############################
