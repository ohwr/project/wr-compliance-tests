#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_ptp-ha_mfg_003                                  #
# Test Case Version : 1.3                                                     #
# Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    #
# Module Name       : Message Format Group (MFG)                              #
#                                                                             #
# Title             : L1Sync message with optParamsEnabled is set to FALSE    #
#                     - transport over IEEE 802.3/Ethernet                    #
#                                                                             #
# Purpose           : To verify that a PTP enabled device sends L1Sync        #
#                     signaling message in correct format when                #
#                     optParamsEnabled is set to FALSE and transport over     #
#                     IEEE 802.3/Ethernet.                                    #
#                                                                             #
# Reference         : P1588/D1.3, February 2018 V3.01 Clause O.6.1 Page 447,  #
#                     Clause O.6.2 Page 447, Clause O.6.4 Pages 448 and 449,  #
#                     Clause 13.12.2 Page 225                                 #
#                                                                             #
# Conformance Type  : SHALL                                                   #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 001                                                     #
# Test Topology     : 001                                                     #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode - one-step/two-step> |           #
#           |                     <Configure logAnnounceInterval, | P1        #
#           |                             announceReceiptTimeout> |           #
#           |                         <Configure logSyncInterval> | P1        #
#           |                  <Configure logMinDelayReqInterval> | P1        #
#           |                    <Configure Priority1, Priority2> | P1        #
#           |                                     <Enable L1SYNC> | P1        #
#           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        #
#           |                     <Disable L1SynOptParams option> | P1        #
#           |           <Enable asymmetryCorrectionPortDS.enable> | P1        #
#           | <Configure timestampCorrectionPortDS.egressLatency, | P1        #
#           |           timestampCorrectionPortDS.ingressLatency, |           #
#           |        asymmetryCorrectionPortDS.constantAsymmetry, |           #
#           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           #
#           |                                                     |           #
#           |     PTP SIGNALING with L1 Sync TLV [MSG_TYPE = 0x0C,|           #
#           |                                  DN = DN1, OPE = 0] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     MSG_TYPE  = Message Type                                                #
#     DN        = Domain Number                                               #
#     OPE       = Optional Parameters Enabled                                 #
#                                                                             #
# NOTE :                                                                      #
#                                                                             #
#  1. This objective is verified using the High Accuracy Delay Request-       #
#     Response Default PTP Profile                                            #
#                                                                             #
#  2. This objective is applicable only for device implementation supports    #
#     transport over IEEE 802.3/Ethernet                                      #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.ingressLatency,           #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Verify that DUT transmits L1 Sync Signaling message on the port    #
#          P1 when optParamsEnabled is set to false. Checking that the        #
#          following PTP message fields have correct information.             #
#                      Ethernet Fields                                        #
#                         1) Source MAC = Unicast MAC                         #
#                         2) Destination MAC = 01:80:C2:00:00:0E              #
#                                              (non-forwardable address)      #
#                         3) Ether Type = 0x88F7 (PTPv2 over Ethernet)        #
#                                                                             #
#                      PTP Fields                                             #
#                         4) messageType = 0xC (Signaling message) (4 bits)   #
#                         5) majorSdoId = 0x000 (4 bits)                      #
#                         6) versionPTP = 2 (4 bits)                          #
#                         7) minorVersionPTP = 1 (4 bits)                     #
#                         8) messageLength = non-zero (2 octets)              #
#                         9) domainNumber = 0 - 127 (1 octet)                 #
#                        10) minorSdoId = 0 (1 octet)                         #
#                        11) flagField = 0x0000 - 0xFFFF (2 octets)           #
#                        12) correctionField = 0 (8 octets)                   #
#                        13) messageTypeSpecific = 0 (4 octets)               #
#                        14) sourcePortIdentity = non-zero (10 octets)        #
#                        15) sequenceId = 0 - 65535 (2 octets)                #
#                        16) controlField = 05 (1 octet)                      #
#                        17) logMessageInterval = 0x7F (1 octet)              #
#                        18) targetPortIdentity = non-zero (10 octets)        #
#                                                                             #
#                      L1Sync Details                                         #
#                        19) tlvType = 0x8001 (L1_SYNC)(2 octets)             #
#                        20) length field = 2 (2 octets)                      #
#                        21) TCR = 0 or 1 (1 bit)                             #
#                        22) RCR = 0 or 1 (1 bit)                             #
#                        23) CR = 0 or 1 (1 bit)                              #
#                        24) OPE = 1 (1 bit)                                  #
#                        25) Reserved = 4 bits                                #
#                        26) ITC = 0 or 1 (1 bit)                             #
#                        27) IRC = 0 or 1 (1 bit)                             #
#                        28) IC = 0 or 1 (1 bit)                              #
#                        29) Reserved = 5 bits                                #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          May/2018      CERN          Initial                            #
# 1.1          Jul/2018      CERN          Added ptp_ha::display_port_details #
#                                          to print port details              #
# 1.2          Sep/2018      CERN          a) Removed destination MAC and IP  #
#                                             in send functions.              #
#                                          b) Removed timeout in recv         #
#                                             functions.                      #
#                                          c) Moved timeout calculation to    #
#                                             PTP-HA global file.             #
# 1.3          Oct/2018      CERN          Modified to gracefully abort the   #
#                                          test if transport type is not      #
#                                          IEEE 802.3/Ethernet.               #
#                                                                             #
###############################################################################
# Copyright (c) 2018 CERN                                                     #
###############################################################################

global dut_log_msg
global tee_log_msg
global session_id

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def    "To verify that a PTP enabled device sends L1Sync\
               signaling message in correct format when\
               optParamsEnabled is set to FALSE and transport over\
               IEEE 802.3/Ethernet"

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set dut_port_1                     $::dut_port_num_1
set vlan_id                        $::ptp_ha_vlan_id
set dut_ip                         $::dut_test_port_ip
set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT

########################### END - INITIALIZATION ##############################

if { $::ptp_trans_type == "UDP/IPv4" } {

       ERRLOG -msg "$tee_log_msg(IEEE)" 

       TC_ABORT -reason "$tee_log_msg(IEEE)"\
                -tc_def  $tc_def

  return 0

}

ptp_ha::display_port_details

ptp_ha::dut_Initialization

proc cleanup {} {

    ptp_ha::dut_cleanup_setup_001

}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

### (Initial Part) ###

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.egressLatency,            #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
###############################################################################

STEP 1

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP1)"

if {![ptp_ha::dut_configure_setup_001]} {
    
    LOG -msg "$dut_log_msg(INIT_SETUP1_F)"
    
    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP1_F)"\
                -tc_def $tc_def
    return 0
}

###############################################################################
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
###############################################################################

STEP 2

LOG -level 0 -msg "$tee_log_msg(INIT)"

ptp_ha::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip


if {$ptp_comm_model == "Unicast"} {
    
    if {![PTP_HA_UNICAST_PRE_REQUISITE_CONFIGURATION\
                    -port_num       $tee_port_1\
                    -tee_mac        $tee_mac\
                    -tee_ip         $tee_ip\
                    -dut_ip         $dut_ip\
                    -vlan_id        $vlan_id\
                    -dut_mac        tee_dest_mac\
                    -timeout        $arp_timeout]} {
        
        TEE_CLEANUP
        
        TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
                    -tc_def $tc_def
        
        return 0
    }
    
}

if {![pltLib::create_filter_ptp_ha_pkt\
                -port_num                $tee_port_1\
                -ether_type              $ptp_ethtype\
                -vlan_id                 $vlan_id\
                -ids                     id1]} {
    
    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                -tc_def  $tc_def
    
    return 0
}

set filter_id [lindex $id1 end]

ptp_ha::reset_capture_stats\
            -session_id         $::tee_session_id\
            -port_num           $tee_port_1

###############################################################################
# Step 3 : Verify that DUT transmits L1 Sync Signalling message on the port   #
#          P1 when optParamsEnabled is set to false. Checking that the        #
#          following PTP message fields have correct information.             #
#                      Ethernet Fields                                        #
#                         1) Source MAC = Unicast MAC                         #
#                         2) Destination MAC = 01:80:C2:00:00:0E              #
#                                              (non-forwardable address)      #
#                         3) Ether Type = 0x88F7 (PTPv2 over Ethernet)        #
#                                                                             #
#                      PTP Fields                                             #
#                         4) messageType = 0xC (Signaling message)  (4 bits)  #
#                         5) majorSdoId = 0x000  (4 bits)                     #
#                         6) versionPTP = 2      (4 bits)                     #
#                         7) minorVersionPTP = 1 (4 bits)                     #
#                         8) messageLength = non-zero (2 octets)              #
#                         9) domainNumber = 0 - 127  (1 octet)                #
#                        10) minorSdoId = 0 (1 octet)                         #
#                        11) flagField = 0x0000 - 0xFFFF (2 octets)           #
#                        12) correctionField = 0  (8 octets)                  #
#                        13) messageTypeSpecific = (4 octets)                 #
#                        14) sourcePortIdentity = non-zero (10 octets)        #
#                        15) sequenceId = 0 - 65535   (2 octets)              #
#                        16) controlField = 05  (1 octet)                     #
#                        17) logMessageInterval = 0x7F  (1 octet)             #
#                        18) targetPortIdentity = non-zero (10 octets)        #
#                                                                             #
#                      L1Sync Details                                         #
#                        19) tlvType = 0x8001 (L1_SYNC)(2 octets)             #
#                        20) length field = 2 (2 octets)                      #
#                        21) TCR = 0 or 1 (1 bit)                             #
#                        22) RCR = 0 or 1 (1 bit)                             #
#                        23) CR = 0 or 1 (1 bit)                              #
#                        24) OPE = 1 (1 bit)                                  #
#                        25) Reserved = 4 bits                                #
#                        26) ITC = 0 or 1 (1 bit)                             #
#                        27) IRC = 0 or 1 (1 bit)                             #
#                        28) IC = 0 or 1 (1 bit)                              #
#                        29) Reserved = 5 bits                                #
#                                                                             #
###############################################################################

STEP 3
LOG -level 0 -msg  "$dut_log_msg(L1SYNC_RX_P1_V) "

if {![ptp_ha::check_signal\
                -port_num                $tee_port_1\
                -ope                     0\
                -filter_id               $filter_id]} {
    
    TEE_CLEANUP
    
    TC_CLEAN_AND_FAIL -reason  "PTP enabled device does not send L1Sync\
                                signaling message in correct format when\
                                optParamsEnabled is set to FALSE and transport over\
                                IEEE 802.3/Ethernet."\
                -tc_def   $tc_def
    
    return 0
}

TEE_CLEANUP

TC_CLEAN_AND_PASS  -reason "PTP enabled device sends L1Sync\
                            signaling message in correct format when\
                            optParamsEnabled is set to FALSE and transport over\
                            IEEE 802.3/Ethernet."\
            -tc_def  $tc_def

return 1

########################## END OF TEST CASE ###################################

