#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_ptp-ha_opv_001                                  #
# Test Case Version : 1.1                                                     #
# Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    #
# Module Name       : PTP-HA Optional Parameters Verification (OPV)           #
#                                                                             #
# Title             : L1SyncOptParamsPortDS.timestampsCorrectedTx             #
#                                                                             #
# Purpose           : To verify that a PTP enabled device supports to enable  #
#                     L1SyncOptParamsPortDS.timestampsCorrectedTx only when   #
#                     L1SyncBasicPortDS.optParamsEnabled is enabled.          #
#                                                                             #
# Reference         : P1588/D1.3, Febraury 2018, V3.01 Clause O.8.4.2.1       #
#                     Page 453                                                #
#                                                                             #
# Conformance Type  : SHALL                                                   #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 001                                                     #
# Test Topology     : 001                                                     #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode - one-step/two-step> |           #
#           |                     <Configure logAnnounceInterval, | P1        #
#           |                             announceReceiptTimeout> |           #
#           |                         <Configure logSyncInterval> | P1        #
#           |                  <Configure logMinDelayReqInterval> | P1        #
#           |                    <Configure Priority1, Priority2> | P1        #
#           |                                     <Enable L1SYNC> | P1        #
#           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        #
#           |                     <Disable L1SynOptParams option> | P1        #
#           |           <Enable asymmetryCorrectionPortDS.enable> | P1        #
#           | <Configure timestampCorrectionPortDS.egressLatency, | P1        #
#           |           timestampCorrectionPortDS.ingressLatency, |           #
#           |        asymmetryCorrectionPortDS.constantAsymmetry, |           #
#           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           #
#           |                                                     |           #
#           |!<Enable L1SyncOptParamsPortDS.timestampsCorrectedTx>|           #
#           |                                                     |           #
#           |                      PTP SIGNALING with L1 Sync TLV |           #
#           |                [MSG_TYPE = 0x0C, DN = DN1, OPE = 0] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |         <Enable L1SyncBasicPortDS.optParamsEnabled> |           #
#           |                                                     |           #
#           | <Enable L1SyncOptParamsPortDS.timestampsCorrectedTx>|           #
#           |                                                     |           #
#           |                      PTP SIGNALING with L1 Sync TLV |           #
#           |       [MSG_TYPE = 0x0C, DN = DN1, OPE = 1, TCT = 1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     MSG_TYPE  = Message Type                                                #
#     DN        = Domain Number                                               #
#     BC        = Boundary Clock                                              #
#     OC        = Ordinary Clock                                              #
#     TCR       = txCoherentIsRequired                                        #
#     RCR       = rxCoherentIsRequired                                        #
#     CR        = congruentIsRequired                                         #
#     OPE       = optParamsEnabled                                            #
#     TCT       = timestampsCorrectedTx                                       #
#                                                                             #
# NOTE :                                                                      #
#                                                                             #
#  1. This objective is verified using the High Accuracy Delay Request-       #
#     Response Default PTP Profile                                            #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.ingressLatency,           #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Observe that the DUT does not allow to enable                      #
#          L1SyncOptParamsPortDS.timestampsCorrectedTx.                       #
#                                                                             #
# Step 4 : If DUT allows to enable L1SyncOptParamsPortDS.timestampsCorrectedTx#
#          at Step 3, observe that DUT transmits PTP SIGNALING message with   #
#          L1 Sync TLV on the port P1 with following parameters.              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0C                                       #
#                  Domain Number = DN1                                        #
#              L1SYNC TLV                                                     #
#                  OPE           = 0                                          #
#                                                                             #
# Step 5 : Enable L1SyncBasicPortDS.optParamsEnabled.                         #
#                                                                             #
# Step 6 : Enable L1SyncOptParamsPortDS.timestampsCorrectedTx.                #
#                                                                             #
# Step 7 : Verify that DUT transmits PTP SIGNALING message with extended      #
#          format of the L1_SYNC_TLV on the port P1 with following parameters.#
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0C                                       #
#                  Domain Number = DN1                                        #
#              L1SYNC TLV (with extended format)                              #
#                  OPE           = 1                                          #
#                  TCT           = 1                                          #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          Oct/2018      CERN          Initial                            #
# 1.1          Nov/2018      CERN          Replaced enabling of               #
#                                          optParamsEnabled with              #
#                                          timestampsCorrectedTx in step 3.   #
#                                                                             #
###############################################################################
# Copyright (c) 2018 CERN                                                     #
###############################################################################

global dut_log_msg
global tee_log_msg
global session_id 

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def "To verify that a PTP enabled device supports to enable\
            L1SyncOptParamsPortDS.timestampsCorrectedTx only when\
            L1SyncBasicPortDS.optParamsEnabled is enabled."

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set dut_port_1                     $::dut_port_num_1
set vlan_id                        $::ptp_ha_vlan_id
set dut_ip                         $::dut_test_port_ip
set dut_dest_ip                    $::dut_test_port_ip
set tee_dest_ip                    $::tee_test_port_ip
set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT

set domain                         $::DEFAULT_DOMAIN
set infinity                       $::INFINITY
set clock_step                     $::ptp_dut_clock_step
set sequence_id_ann                1

set ope_false             0
set ope_true              1
set tct_true              1
set tlv_type(l1sync)               $::TLV_TYPE(L1SYNC)

########################### END - INITIALIZATION ##############################

ptp_ha::display_port_details

ptp_ha::dut_Initialization

proc cleanup {} {

    ptp_ha::dut_cleanup_setup_001

}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

### (Initial Part) ###

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's port P1.                                    #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.ingressLatency,           #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
###############################################################################

STEP 1

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP1)"

if {![ptp_ha::dut_configure_setup_001]} {

    LOG -msg "$dut_log_msg(INIT_SETUP1_F)"

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP1_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
###############################################################################

STEP 2

LOG -level 0 -msg "$tee_log_msg(INIT)"

ptp_ha::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

    if {![PTP_HA_UNICAST_PRE_REQUISITE_CONFIGURATION\
                        -port_num       $tee_port_1\
                        -tee_mac        $tee_mac\
                        -tee_ip         $tee_ip\
                        -dut_ip         $dut_ip\
                        -vlan_id        $vlan_id\
                        -dut_mac        tee_dest_mac\
                        -timeout        $arp_timeout]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
                           -tc_def $tc_def

        return 0
    }

}

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_1\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def  $tc_def

    return 0
}

set filter_id [lindex $id1 end]

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

# (Part 1)  #

###############################################################################
# Step 3 : Observe that the DUT does not allow to enable                      #
#          L1SyncOptParamsPortDS.timestampsCorrectedTx.                       #
#                                                                             #
###############################################################################

STEP 3

LOG -level 0 -msg "$dut_log_msg(ENABLE_L1SYNC_TCT)"

if {[ptp_ha::dut_timestamp_corrected_tx_enable\
              -port_num        $dut_port_1]} {

     set step3_pass 1

} else {

set step3_pass 0

}

###############################################################################
# Step 4 : If DUT allows to enable L1SyncOptParamsPortDS.timestampsCorrectedTx#
#          at Step 3, observe that DUT transmits PTP SIGNALING message with   #
#          L1 Sync TLV on the port P1 with following parameters.              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0C                                       #
#                  Domain Number = DN1                                        #
#              L1SYNC TLV                                                     #
#                  OPE           = 0                                          #
#                                                                             #
###############################################################################

STEP 4

if {$step3_pass} {

LOG -level 0 -msg "$tee_log_msg(L1SYNC_RX_P1_O)"

    if {![ptp_ha::recv_signal\
             -port_num       $tee_port_1\
             -filter_id      $filter_id\
             -domain_number  $domain\
             -tlv_type       $tlv_type(l1sync)\
             -ope            $ope_false\
             -error_reason   error_reason]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT -reason "$tee_log_msg(L1SYNC_RX_P1_F) $error_reason"\
                           -tc_def  $tc_def
        return 0

    }

} else {

    LOG -level 0 -msg "$tee_log_msg(L1SYNC_RX_P1_SKIP)"

}

###############################################################################
# Step 5 : Enable L1SyncBasicPortDS.optParamsEnabled.                         #
#                                                                             #
###############################################################################

STEP 5

LOG -level 0 -msg "$dut_log_msg(ENABLE_L1SYN_OPT_PARAMS)"

if {![ptp_ha::dut_l1sync_opt_params_enable\
             -port_num      $dut_port_1]} {

     TEE_CLEANUP

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(ENABLE_L1SYN_OPT_PARAMS_F)"\
                        -tc_def  $tc_def

    return 0

}

###############################################################################
# Step 6 : Enable L1SyncOptParamsPortDS.timestampsCorrectedTx.                #
#                                                                             #
###############################################################################

STEP 6

LOG -level 0 -msg "$dut_log_msg(ENABLE_L1SYNC_TCT)"

if {![ptp_ha::dut_timestamp_corrected_tx_enable\
             -port_num     $dut_port_1]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(ENABLE_L1SYNC_TCT_F)"\
                       -tc_def  $tc_def

    return 0

}

###############################################################################
# Step 7 : Verify that DUT transmits PTP SIGNALING message with extended      #
#          format of the L1_SYNC_TLV on the port P1 with following parameters.#
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0C                                       #
#                  Domain Number = DN1                                        #
#              L1SYNC TLV (with extended format)                              #
#                  OPE           = 1                                          #
#                  TCT           = 1                                          #
#                                                                             #
###############################################################################

STEP 7

LOG -level 0 -msg "$tee_log_msg(L1SYNC_RX_P1_O)"

if {![ptp_ha::recv_signal\
           -port_num          $tee_port_1\
           -domain_number     $domain\
           -tlv_type          $tlv_type(l1sync)\
           -filter_id         $filter_id\
           -error_reason      error_reason\
           -ope               $ope_true\
           -tct               $tct_true]} {

   TEE_CLEANUP

   TC_CLEAN_AND_FAIL -reason "DUT does not transmit PTP SIGNALING message\
                              with extended format of L1_SYNC_TLV. $error_reason"\
                     -tc_def  $tc_def

   return 0

}

TEE_CLEANUP

TC_CLEAN_AND_PASS -reason "DUT supports to enable\
                           L1SyncOptParamsPortDS.timestampsCorrectedTx only when\
                           L1SyncBasicPortDS.optParamsEnabled is enabled."\
                  -tc_def  $tc_def

return 1

################################# END OF TEST CASE #####################################
