#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_ptp-ha_peg_012                                  #
# Test Case Version : 1.2                                                     #
# Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    #
# Module Name       : PTP ExternalPortConfiguration Group (PEG)               #
#                                                                             #
# Title             : Data set updation based on Announce message             #
#                     portDS.portState is in UNCALIBRATED                     #
#                                                                             #
# Purpose           : To verify that a PTP enabled device updates data set    #
#                     from most recently received Announce message when port  #
#                     state is in UNCALIBRATED state and                      #
#                     defaultDS.externalPortConfigurationEnabled is set       #
#                     to TRUE.                                                #
#                                                                             #
# Reference         : P1588/D1.3, February 2018 V3.01 Clause 17.6.5.4         #
#                     Page 356                                                #
#                                                                             #
# Conformance Type  : SHALL                                                   #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 007                                                     #
# Test Topology     : 004                                                     #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                                        <Enable PTP> | P2        #
#           |                          <Enable PTP with BC clock> |           #
#           |                    <Clock mode - one-step/two-step> |           #
#           |                     <Configure logAnnounceInterval, | P1        #
#           |                             announceReceiptTimeout> |           #
#           |                         <Configure logSyncInterval> | P1        #
#           |                  <Configure logMinDelayReqInterval> | P1        #
#           |                    <Configure Priority1, Priority2> | P1        #
#           |                                     <Enable L1SYNC> | P1        #
#           |                                     <Enable L1SYNC> | P2        #
#           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        #
#           |                     <Disable L1SynOptParams option> | P1        #
#           |           <Enable asymmetryCorrectionPortDS.enable> | P1        #
#           | <Configure timestampCorrectionPortDS.egressLatency, | P1        #
#           |           timestampCorrectionPortDS.ingressLatency, |           #
#           |        asymmetryCorrectionPortDS.constantAsymmetry, |           #
#           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           #
#           |                                            <Enable  |           #
#           |         defaultDS.externalPortConfigurationEnabled> | P1        #
#           |                                                     |           #
#           |                                          <Configure |           #
#           |       externalPortConfigurationPortDS.desiredState= |           #
#           |                                       UNCALIBRATED> |           #
#           |                                                     |           #
#           |                 <Check Port Status = UNCALIBRATED>  | P1        #
#           |                                                     |           #
#           |                                            <Enable  |           #
#           |         defaultDS.externalPortConfigurationEnabled> | P2        #
#           |                                                     |           #
#           |                <Configure.externalPortConfiguration.|           #
#           |                  desiredstate = MASTER>             | P2        #
#           |                                                     |           #
#           |                         <Check Port Status = MASTER>| P2        #
#           |                                                     |           #
#           | ANNOUNCE [MSG_TYPE = 0x0B,                          |           #
#           |  PRI=X+1, DN = DN1, PR2 = Y]                        |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                           PRI=X+1, DN = DN1, PR2=Y] |           #
#        T2 |-------------------------------------------------<<--| P2        #
#           |                                                     |           #
#           | ANNOUNCE [MSG_TYPE = 0x0B,                          |           #
#           |  PRI=X+2, DN = DN1, PR2 = Y+1]                      |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                         PRI=X+2, DN = DN1, PR2=Y+1] |           #
#        T2 |-------------------------------------------------<<--| P2        #
#           |                                                     |           #
#           | ANNOUNCE [MSG_TYPE = 0x0B,                          |           #
#           |  PRI=X+3, DN = DN1, PR2 = Y+2]                      |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                         PRI=X+3, DN = DN1, PR2=Y+2] |           #
#        T2 |-------------------------------------------------<<--| P2        #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     MSG_TYPE  = Message Type                                                #
#     DN        = Domain Number                                               #
#     PRI       = Priority                                                    #
#                                                                             #
# NOTE :                                                                      #
#                                                                             #
#  1. This objective is verified using the High Accuracy Delay Request-       #
#     Response Default PTP Profile                                            #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's ports P1 and P2.                                      #
#    ii.   Enable PTP on ports P1 and P2.                                     #
#   iii.   Enable PTP globally with device type as Boundary clock.            #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's ports P1 and P2.                            #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.egressLatency,            #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
#    xi.   Enable defaultDS.externalPortConfigurationEnabled on port P1.      #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add ports T1 and T2 at TEE.                                        #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Configure externalPortConfigurationPortDS.desiredState as          #
#          UNCALIBRATED at Port P1.                                           #
#                                                                             #
# Step 4 : Observe that the port status of P1 in DUT is in UNCALIBRATED       #
#          state.                                                             #
#                                                                             #
# Step 5 : Configure externalPortConfigurationPortDS.desiredState as MASTER   #
#          at Port P2.                                                        #
#                                                                             #
# Step 6 : Observe that the port status of P2 in DUT is in MASTER state.      #
#                                                                             #
# Step 7 : Send ANNOUNCE message on port T1 with following parameters:        #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type  = 0x0B                                      #
#                   Domain Number = DN1                                       #
#                   Priority1     = X+1                                       #
#                   Priority2     = Y                                         #
#                                                                             #
# Step 8 : Verify that DUT transmits ANNOUNCE message on port P2              #
#          with following parameters.                                         #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type  = 0x0B                                      #
#                   Domain Number = DN1                                       #
#                   Priority1     = X+1                                       #
#                   Priority2     = Y                                         #
#                                                                             #
# Step 9 : Send ANNOUNCE message on port T1 with following parameters:        #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type  = 0x0B                                      #
#                   Domain Number = DN1                                       #
#                   Priority1     = X+2                                       #
#                   Priority2     = Y+1                                       #
#                                                                             #
# Step 10: Verify that DUT transmits ANNOUNCE message on port P2              #
#          with following parameters.                                         #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type  = 0x0B                                      #
#                   Domain Number = DN1                                       #
#                   Priority1     = X+2                                       #
#                   Priority2     = Y+1                                       #
#                                                                             #
# Step 11: Send ANNOUNCE message on port T1 with following parameters:        #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type  = 0x0B                                      #
#                   Domain Number = DN1                                       #
#                   Priority1     = X+3                                       #
#                   Priority2     = Y+2                                       #
#                                                                             #
# Step 12: Verify that DUT transmits ANNOUNCE message on port P2              #
#          with following parameters.                                         #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type  = 0x0B                                      #
#                   Domain Number = DN1                                       #
#                   Priority1     = X+3                                       #
#                   Priority2     = Y+2                                       #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          May/2018      CERN          Initial                            #
# 1.1          Jul/2018      CERN          Added ptp_ha::display_port_details #
#                                          to print port details              #
# 1.2          Sep/2018      CERN          a) Removed destination MAC and IP  #
#                                             in send functions.              #
#                                          b) Removed timeout in recv         #
#                                             functions.                      #
#                                          c) Moved timeout calculation to    #
#                                             PTP-HA global file.             #
#                                          d) Correct MASTER state in steps 5 #
#                                             and 6.                          #
#                                          e) Removed unwanted last 2 steps in#
#                                             ladder diagram.                 #
#                                                                             #
###############################################################################
# Copyright (c) 2018 CERN                                                     #
###############################################################################

global dut_log_msg
global tee_log_msg
global session_id

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def "To verify that a PTP enabled device updates data set\
            from most recently received Announce message when port\
            state is in UNCALIBRATED state and\
            defaultDS.externalPortConfigurationEnabled is set\
            to TRUE."

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set tee_port_2                     $::tee_port_num_2
set tee_port_list                  "$tee_port_1 $tee_port_2"
set dut_port_1                     $::dut_port_num_1
set dut_port_2                     $::dut_port_num_2
set vlan_id_1                      $::ptp_ha_vlan_id
set vlan_id_2                      $::ptp_ha_vlan_id_2
set dut_ip                         $::dut_test_port_ip

set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT
set domain                         $::DEFAULT_DOMAIN
set infinity                       $::INFINITY
set gmid                           $::GRANDMASTER_ID
set master                         $::PTP_PORT_STATE(MASTER)
set uncalibrated                   $::PTP_PORT_STATE(UNCALIBRATED)
set sequence_id_ann                1
set priority1                      $::ptp_dut_default_priority1
set priority2                      $::ptp_dut_default_priority2

########################### END - INITIALIZATION ##############################

ptp_ha::display_port_details\
                -port_list  $tee_port_list 

ptp_ha::dut_Initialization

proc cleanup {} {

    ptp_ha::dut_cleanup_setup_007

}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

### (Initial Part) ###

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's ports P1 and P2.                                      #
#    ii.   Enable PTP on ports P1 and P2.                                     #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#    vi.   Enable L1SYNC on DUT's ports P1 and P2.                            #
#   vii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#  viii.   Disable L1SynOptParams on DUT.                                     #
#    ix.   Enable asymmetryCorrectionPortDS.enable.                           #
#     x.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.egressLatency,            #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
#    xi.   Enable defaultDS.externalPortConfigurationEnabled on port P1.      #
###############################################################################

STEP 1

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP7)"

if {![ptp_ha::dut_configure_setup_007]} {

    LOG -msg "$dut_log_msg(INIT_SETUP7_F)"

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP7_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 2 : Initialization of TEE                                              #
#     i.   Add ports T1 and T2 at TEE.                                        #
###############################################################################

STEP 2a

LOG -level 0 -msg "$tee_log_msg(INIT)"

ptp_ha::Initialization\
              -port_list      $tee_port_list

set tee_mac_1     $::TEE_MAC
set tee_ip_1      $::tee_test_port_ip

set tee_mac_2     $::TEE_MAC_2
set tee_ip_2      $::tee_test_port_ip_2

for {set i 1} {$i<=2} {incr i} {
    if {$ptp_comm_model == "Unicast"} {

        if {![PTP_HA_UNICAST_PRE_REQUISITE_CONFIGURATION\
                -port_num       [set tee_port_$i]\
                -tee_mac        [set tee_mac_$i]\
                -tee_ip         [set tee_ip_$i]\
                -dut_ip         [set dut_ip_$i]\
                -vlan_id        [set vlan_id_$i]\
                -dut_mac        tee_dest_mac_$i\
                -timeout        $arp_timeout]} {

                    TEE_CLEANUP

                    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
                                       -tc_def  $tc_def
        
                    return 0
        }

        } 

}

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_1\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id_1\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def  $tc_def

    return 0
}

set filter_id1 [lindex $id1 end]

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_2\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id_2\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def $tc_def

    return 0
}

set filter_id2 [lindex $id1 end]


if { $::ptp_ha_vlan_encap != "true" } {
    set vlan_id_2 "dont_care"
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_2

###############################################################################
# Step 3 : Configure externalPortConfigurationPortDS.desiredState as          #
#          UNCALIBRATED at Port P1.                                           #
###############################################################################

STEP 3

LOG -level 0 -msg "$dut_log_msg(CONFIGURE_UNCALIBRATED)"

if {![ptp_ha::dut_set_epc_desired_state\
         -port_num         $dut_port_1\
         -desired_state    $uncalibrated]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT   -reason "$dut_log_msg(CONFIGURE_UNCALIBRATED_F)"\
                         -tc_def  $tc_def
        return 0
}

sleep $::ptp_ha_bmca

###############################################################################
# Step 4 : Observe that the port status of P1 in DUT is in UNCALIBRATED       #
#          state.                                                             #
###############################################################################

STEP 4

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_UNCALIBRATED_P1_O)"

if {![ptp_ha::dut_check_ptp_port_state \
              -port_num             $dut_port_1\
              -port_state           $uncalibrated] } {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_UNCALIBRATED_P1_F)"\
                       -tc_def  $tc_def
    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 5 : Configure externalPortConfigurationPortDS.desiredState as MASTER   #
#          at Port P2.                                                        #
###############################################################################

STEP 5

LOG -level 0 -msg "$dut_log_msg(CONFIGURE_MASTER)"

if {![ptp_ha::dut_set_epc_desired_state\
           -port_num         $dut_port_2\
           -desired_state    $master]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT    -reason "$dut_log_msg(CONFIGURE_MASTER_F)"\
                          -tc_def  $tc_def
        return 0
}

###############################################################################
# Step 6 : Observe that the port status of P2 in DUT is in MASTER state.      #
###############################################################################

sleep $::ptp_ha_bmca

STEP 6

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_MASTER_P2_O)"

if {![ptp_ha::dut_check_ptp_port_state \
              -port_num                $dut_port_2\
              -port_state              $master] } {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_MASTER_P2_F)"\
                       -tc_def  $tc_def
    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_2

###############################################################################
# Step 7 : Send ANNOUNCE message on port T1 with following parameters:        #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type  = 0x0B                                      #
#                   Domain Number = DN1                                       #
#                   Priority1     = X+1                                       #
#                   Priority2     = Y                                         #
###############################################################################

STEP 7

set priority [expr $priority1 + 1]

LOG -level 0 -msg "$tee_log_msg(ANN_TX_T1)"

if {![ptp_ha::send_announce \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac_1\
           -gm_priority1            $priority\
           -gm_priority2            $priority2\
           -src_ip                  $tee_ip_1\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -sequence_id             $sequence_id_ann\
           -count                   $infinity]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANN_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 8 : Verify that DUT transmits ANNOUNCE message on port P2              #
#          with following parameters.                                         #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type  = 0x0B                                      #
#                   Domain Number = DN1                                       #
#                   Priority1     = X+1                                       #
#                   Priority2     = Y                                         #
###############################################################################

STEP 8

set priority [expr $priority1 + 1]

LOG -level 0 -msg "$tee_log_msg(ANN_RX_P2_V)"

if {![ptp_ha::recv_announce \
           -port_num                $tee_port_2\
           -vlan_id                 $vlan_id_2\
           -gm_priority1            $priority\
           -gm_priority2            $priority2\
           -filter_id               $filter_id2\
           -domain_number           $domain]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANN_RX_P2_F)"\
                       -tc_def  $tc_def

    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_2

###############################################################################
# Step 9 : Send ANNOUNCE message on port T1 with following parameters:        #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type  = 0x0B                                      #
#                   Domain Number = DN1                                       #
#                   Priority1     = X+2                                       #
#                   Priority2     = Y+1                                       #
###############################################################################

STEP 9

set priority [expr $priority1 + 2]
set priority3 [expr $priority2 + 1]

LOG -level 0 -msg "$tee_log_msg(ANN_TX_T1)"

if {![ptp_ha::send_announce \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac_1\
           -gm_priority1            $priority\
           -gm_priority2            $priority3\
           -src_ip                  $tee_ip_1\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -sequence_id             $sequence_id_ann\
           -count                   $infinity]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANN_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 10 : Verify that DUT transmits ANNOUNCE message on port P2             #
#          with following parameters.                                         #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type  = 0x0B                                      #
#                   Domain Number = DN1                                       #
#                   Priority1     = X+2                                       #
#                   Priority2     = Y+1                                       #
###############################################################################

STEP 10

set priority [expr $priority1 + 2]
set priority3 [expr $priority2 + 1]

LOG -level 0 -msg "$tee_log_msg(ANN_RX_P2_V)"

if {![ptp_ha::recv_announce \
           -port_num                $tee_port_2\
           -vlan_id                 $vlan_id_2\
           -gm_priority1            $priority\
           -gm_priority2            $priority3\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -filter_id               $filter_id2]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANN_RX_P2_F)"\
                       -tc_def  $tc_def

    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_2

###############################################################################
# Step 11 : Send ANNOUNCE message on port T1 with following parameters:       #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type  = 0x0B                                      #
#                   Domain Number = DN1                                       #
#                   Priority1     = X+3                                       #
#                   Priority2     = Y+2                                       #
###############################################################################

STEP 11

set priority [expr $priority1 + 3]
set priority3 [expr $priority2 + 2]

LOG -level 0 -msg "$tee_log_msg(ANN_TX_T1)"

if {![ptp_ha::send_announce \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac_1\
           -gm_priority1            $priority\
           -gm_priority2            $priority3\
           -src_ip                  $tee_ip_1\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -sequence_id             $sequence_id_ann\
           -count                   $infinity]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANN_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 12 : Verify that DUT transmits ANNOUNCE message on port P2             #
#          with following parameters.                                         #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type  = 0x0B                                      #
#                   Domain Number = DN1                                       #
#                   Priority1     = X+3                                       #
#                   Priority2     = Y+2                                       #
###############################################################################

STEP 12

set priority [expr $priority1 + 3]
set priority3 [expr $priority2 + 2]

LOG -level 0 -msg "$tee_log_msg(ANN_RX_P2_V)"

if {![ptp_ha::recv_announce \
           -port_num                $tee_port_2\
           -vlan_id                 $vlan_id_2\
           -gm_priority1            $priority\
           -gm_priority2            $priority3\
           -unicast_flag            $::unicast_flag\
           -filter_id               $filter_id2\
           -domain_number           $domain]} {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL  -reason "PTP enabled device does not update data set\
                                from most recently received Announce message when port\
                                state is in UNCALIBRATED state and\
                                defaultDS.externalPortConfigurationEnabled is set\
                                to TRUE"\
                       -tc_def  $tc_def

    return 0
}

TEE_CLEANUP

TC_CLEAN_AND_PASS -reason "PTP enabled device updates data set\
                           from most recently received Announce message when port\
                           state is in UNCALIBRATED state and\
                           defaultDS.externalPortConfigurationEnabled is set\
                           to TRUE"\
                  -tc_def  $tc_def

return 1
######################### END OF TEST CASE ####################################
