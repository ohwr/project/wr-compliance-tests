#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_ptp-ha_peg_017                                  #
# Test Case Version : 1.1                                                     #
# Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    #
# Module Name       : PTP ExternalPortConfiguration Group (PEG)               #
#                                                                             #
# Title             : portDS.portState remains in PASSIVE state               #
#                                                                             #
# Purpose           : To verify that a PTP enabled device remains in PASSIVE  #
#                     state by setting externalPortConfigurationPortDS.       #
#                     desiredState to PASSIVE, even if fault condition occur. #
#                     (This test is applicable only if Peer to Peer Delay     #
#                     mechanism is supported.)                                #
#                                                                             #
# Reference         : P1588/D1.3, February 2018 V3.01 Clause 17.6.1 Page 353, #
#                     Clause 17.6.3.2 Page 354.                               #
#                                                                             #
# Conformance Type  : SHALL                                                   #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 002                                                     #
# Test Topology     : 001                                                     #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                                        <Enable PTP> | P1        #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode - one-step/two-step> |           #
#           |                        <Set delay mechanism as P2P> |           #
#           |                     <Configure logAnnounceInterval, | P1        #
#           |                             announceReceiptTimeout> |           #
#           |                         <Configure logSyncInterval> | P1        #
#           |                  <Configure logMinDelayReqInterval> | P1        #
#           |                    <Configure Priority1, Priority2> | P1        #
#           |                                     <Enable L1SYNC> | P1        #
#           |    <Configure L1SyncInterval, L1SyncReceiptTimeout> | P1        #
#           |                     <Disable L1SynOptParams option> | P1        #
#           |           <Enable asymmetryCorrectionPortDS.enable> | P1        #
#           | <Configure timestampCorrectionPortDS.egressLatency, | P1        #
#           |           timestampCorrectionPortDS.ingressLatency, |           #
#           |        asymmetryCorrectionPortDS.constantAsymmetry, |           #
#           |   asymmetryCorrectionPortDS.scaledDelayCoefficient> |           #
#           |                                            <Disable |           #
#           |         defaultDS.externalPortConfigurationEnabled> | P1        #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                                    PRI=X, DN = DN1] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | ANNOUNCE [MSG_TYPE = 0x0B,                          |           #
#           |  PRI=X+1, DN = DN1]                                 |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                        <Check Port Status = MASTER> | P1        #
#           |                                                     |           #
#        T1 | <Enable auto responder to Pdelay_Req messages>      | P1        #
#           |                                                     |           #
#           |                                            <Enable  |           #
#           |         defaultDS.externalPortConfigurationEnabled> | P1        #
#           |                                                     |           #
#           |                       <Check Port Status = PASSIVE> | P1        #
#           |                                                     |           #
#           |               PDELAY_REQ [MSG_TYPE = 0x02, DN = DN1,|           #
#           |                         SRC_PRT_ID = E, SEQ_ID = D] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | PDELAY_RESP [SRC_MAC = SRC1,CLK_ID = CLK1,          |           #
#           | REQ_PRT_ID = E, SEQ_ID = D                          |           #
#           | MSG_TYPE = 0x03, DN = DN1]                          |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           | PDELAY_RESP [SRC_MAC = SRC2,CLK_ID = CLK2,          |           #
#           | REQ_PRT_ID = E, SEQ_ID = D                          |           #
#           | MSG_TYPE = 0x03, DN = DN1]                          |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                       <Check Port Status = PASSIVE> | P1        #
#           |                                                     |           #
#           |                                           <Disable  |           #
#           |         defaultDS.externalPortConfigurationEnabled> | P1        #
#           |                                                     |           #
#           |               PDELAY_REQ [MSG_TYPE = 0x02, DN = DN1,|           #
#           |                         SRC_PRT_ID = E, SEQ_ID = D] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           | PDELAY_RESP [SRC_MAC = SRC1,CLK_ID = CLK1,          |           #
#           | REQ_PRT_ID = E, SEQ_ID = D                          |           #
#           | MSG_TYPE = 0x03, DN = DN1]                          |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           | PDELAY_RESP [SRC_MAC = SRC2,CLK_ID = CLK2,          |           #
#           | REQ_PRT_ID = E, SEQ_ID = D                          |           #
#           | MSG_TYPE = 0x03, DN = DN1]                          |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                        <Check Port Status = FAULTY> | P1        #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     MSG_TYPE  = Message Type                                                #
#     DN        = Domain Number                                               #
#     PRI       = Priority                                                    #
#     P2P       = Peer to Peer                                                #
#     SEQ_ID    = Sequence ID                                                 #
#     SRC_MAC   = Source mac address                                          #
#     CLK_ID    = Clock Identity                                              #
#     SRC_PRT_ID= Source Port Identity                                        #
#     REQ_PRT_ID= Requesting Port Identity                                    #
#                                                                             #
# NOTE :                                                                      #
#                                                                             #
#  1. This objective is verified using the High Accuracy Peer to Peer Default #
#     PTP Profile                                                             #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure delaymechanism as Peer to peer.                          #
#    vi.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#   vii.   Enable L1SYNC on DUT's port P1.                                    #
#  viii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#    ix.   Disable L1SynOptParams on DUT.                                     #
#     x.   Enable asymmetryCorrectionPortDS.enable.                           #
#    xi.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.egressLatency,            #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
#   xii.   Disable defaultDS.externalPortConfigurationEnabled on port P1.     #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Observe that DUT transmits ANNOUNCE message on port P1 with        #
#          following parameters.                                              #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type  = 0x0B                                      #
#                   Domain Number = DN1                                       #
#                   Priority      = X                                         #
#                                                                             #
# Step 4 : Send periodic ANNOUNCE messages on port T1 with following          #
#          parameters.                                                        #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type  = 0x0B                                      #
#                   Domain Number = DN1                                       #
#                   Priority      = X+1                                       #
#                                                                             #
# Step 5 : Observe that the port status of P1 in DUT is in MASTER state.      #
#                                                                             #
# Step 6 : Enable auto responder to respond every Pdelay_Req messages received#
#          on port T1.                                                        #
#                                                                             #
# Step 7 : Enable defaultDS.externalPortConfigurationEnabled on port P1.      #
# NOTE   : Default state of externalPortConfigurationPortDS.desiredState is   #
#          PASSIVE.                                                           #
#                                                                             #
# Step 8 : Observe that the port status of P1 in DUT is in PASSIVE state.     #
#                                                                             #
# Step 9 : Observe that DUT transmits PDELAY_REQ message on the port P1 with  #
#          following parameters :                                             #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type              = 0x02                           #
#                  Domain Number             = DN1                            #
#                  Sequence ID               = D                              #
#                  Source Port Identity      = E                              #
#                                                                             #
# Step 9a: Send PDELAY_RESP to every PDELAY_REQ on the port P1 and with       #
#          following parameters:                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Source Mac               = SRC1                            #
#                  Clock ID                 = CLK1                            #
#                  Message Type             = 0x03                            #
#                  Domain Number            = DN1                             #
#                  Sequence Id              = D                               #
#                  Requesting Port Identity = E                               #
#                                                                             #
# Step 9b: Send PDELAY_RESP to every PDELAY_REQ on the port P1 and with       #
#          following parameters:                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Source Mac               = SRC2                            #
#                  Clock ID                 = CLK2                            #
#                  Message Type             = 0x03                            #
#                  Domain Number            = DN1                             #
#                  Sequence Id              = D                               #
#                  Requesting Port Identity = E                               #
#                                                                             #
# Step 10 : Verify that the port status of P1 in DUT continues to be in       #
#           PASSIVE state.                                                    #
#                                                                             #
# Step 11 : Disable defaultDS.externalPortConfigurationEnabled on port P1 in  #
#           DUT.                                                              #
#                                                                             #
# Step 12: Observe that DUT transmits PDELAY_REQ message on the port P1 with  #
#          following parameters :                                             #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type              = 0x02                           #
#                  Domain Number             = DN1                            #
#                  Sequence ID               = D                              #
#                  Source Port Identity      = E                              #
#                                                                             #
# Step 12a:Send PDELAY_RESP to every PDELAY_REQ on the port P1 and with       #
#          following parameters:                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Source Mac               = SRC1                            #
#                  Clock ID                 = CLK1                            #
#                  Message Type             = 0x03                            #
#                  Domain Number            = DN1                             #
#                  Sequence Id              = D                               #
#                  Requesting Port Identity = E                               #
#                                                                             #
# Step 12b:Send PDELAY_RESP to every PDELAY_REQ on the port P1 and with       #
#          following parameters:                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Source Mac               = SRC2                            #
#                  Clock ID                 = CLK2                            #
#                  Message Type             = 0x03                            #
#                  Domain Number            = DN1                             #
#                  Sequence Id              = D                               #
#                  Requesting Port Identity = E                               #
#                                                                             #
# Step 13 : Verify that the port status of P1 in DUT is in FAULTY state.      #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          Oct/2018      CERN          Initial                            #
# 1.1          Dec/2018      CERN          Modified steps to send Pdelay_Resp #
#                                          to every Pdelay_Req.               #
#                                                                             #
###############################################################################
# Copyright (c) 2018 CERN                                                     #
###############################################################################

global dut_log_msg
global tee_log_msg
global session_id

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def  "To verify that a PTP enabled device remains in PASSIVE\
             state by setting externalPortConfigurationPortDS\
             desiredState to PASSIVE, even if fault condition occur.\
             (This test is applicable only if Peer to Peer delay\
             mechanism is supported)."

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set dut_port_1                     $::dut_port_num_1
set vlan_id                        $::ptp_ha_vlan_id
set dut_ip                         $::dut_test_port_ip
set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT
set domain                         $::DEFAULT_DOMAIN
set infinity                       $::INFINITY
set gmid                           $::GRANDMASTER_ID
set tee_mac_1                      $::TEE_MAC_1
set tee_mac_2                      $::TEE_MAC_2

set instance_1                     7
set instance_2                     11

set sequence_id_ann                1

set master                         $::PTP_PORT_STATE(MASTER)
set slave                          $::PTP_PORT_STATE(SLAVE)
set faulty                         $::PTP_PORT_STATE(FAULTY)
set passive                        $::PTP_PORT_STATE(PASSIVE)

########################### END - INITIALIZATION ##############################

if { $::ptp_p2p_delay_mechanism != "true" } {

    TC_ABORT -reason "$tee_log_msg(P2P_DLY_MECHANISM)"\
             -tc_def $tc_def

    return 0
}

ptp_ha::display_port_details

ptp_ha::dut_Initialization

proc cleanup {} {

    ptp_ha::dut_cleanup_setup_002

}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

### (Initial Part) ###

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP on port P1.                                             #
#   iii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#    iv.   Configure clock mode as One-step/Two-step.                         #
#     v.   Configure delaymechanism as Peer to peer.                          #
#    vi.   Configure default values for Priority1, Priority2,                 #
#          logAnnounceInterval, announceReceiptTimeout, logSyncInterval and   #
#          logMinDelayReqInterval.                                            #
#   vii.   Enable L1SYNC on DUT's port P1.                                    #
#  viii.   Configure default values for L1SyncInterval and                    #
#          L1SyncReceiptTimeout.                                              #
#    ix.   Disable L1SynOptParams on DUT.                                     #
#     x.   Enable asymmetryCorrectionPortDS.enable.                           #
#    xi.   Configure default values for timestampCorrectionPortDS.            #
#          egressLatency, timestampCorrectionPortDS.egressLatency,            #
#          asymmetryCorrectionPortDS.constantAsymmetry and                    #
#          asymmetryCorrectionPortDS.scaledDelayCoefficient.                  #
#   xii.   Disable defaultDS.externalPortConfigurationEnabled on port P1.     #
###############################################################################

STEP 1

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP2)"

if {![ptp_ha::dut_configure_setup_002]} {

    LOG -msg "$dut_log_msg(INIT_SETUP2_F)"

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP2_F)"\
                       -tc_def $tc_def
    return 0
}

###############################################################################
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
###############################################################################

STEP 2

LOG -level 0 -msg "$tee_log_msg(INIT)"

ptp_ha::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

    if {![PTP_HA_UNICAST_PRE_REQUISITE_CONFIGURATION\
                        -port_num       $tee_port_1\
                        -tee_mac        $tee_mac\
                        -tee_ip         $tee_ip\
                        -dut_ip         $dut_ip\
                        -vlan_id        $vlan_id\
                        -dut_mac        tee_dest_mac\
                        -timeout        $arp_timeout]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
                           -tc_def $tc_def

        return 0
    }
}

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_1\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def $tc_def

    return 0
}

set filter_id [lindex $id1 end]

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

### (Part 1) ###

###############################################################################
# Step 3 : Observe that DUT transmits ANNOUNCE message on port P1 with        #
#          following parameters.                                              #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type  = 0x0B                                      #
#                   Domain Number = DN1                                       #
#                   Priority      = X                                         #
###############################################################################

STEP 3

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
           -port_num                $tee_port_1\
           -domain_number           $domain\
           -filter_id               $filter_id\
           -error_reason            error_reason\
           -recvd_gm_priority1      priority]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANNOUNCE_RX_P1_F) $error_reason"\
                        -tc_def $tc_def

    return 0
}

###############################################################################
# Step 4 : Send periodic ANNOUNCE messages on port T1 with following          #
#          parameters.                                                        #
#                                                                             #
#               PTP Header                                                    #
#                   Message Type  = 0x0B                                      #
#                   Domain Number = DN1                                       #
#                   Priority      = X+1                                       #
###############################################################################

STEP 4

set priority1 [expr $priority + 1] 

LOG -level 0 -msg "$tee_log_msg(ANN_TX_T1)"

if {![ptp_ha::send_announce \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -gm_priority1            $priority1\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -sequence_id             $sequence_id_ann\
           -count                   $infinity]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANN_TX_T1_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 5 : Observe that the port status of P1 in DUT is in MASTER state.      #
###############################################################################

STEP 5

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_MASTER_P1_O)"

if {![ptp_ha::dut_check_ptp_port_state \
              -port_num                $dut_port_1\
              -port_state              $master] } {
 
    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_MASTER_P1_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 6 : Enable auto responder to respond every Pdelay_Req messages received#
#          on port T1.                                                        #
#                                                                             #
###############################################################################

STEP 6

LOG -level 0 -msg "$tee_log_msg(ENABLE_RESPONDER_TO_PDELAY_REQ)"

ptp_ha::enable_auto_response_to_pdelay_requests

###############################################################################
# Step 7 : Enable defaultDS.externalPortConfigurationEnabled on DUT.          #
# NOTE   : Default state of externalPortConfigurationPortDS.desiredState is   #
#          PASSIVE.                                                           #
###############################################################################

STEP 7

LOG -level 0 -msg "$dut_log_msg(ENABLE_EXTERNAL_PORT_CONFIG)"

if {![ptp_ha::dut_external_port_config_enable\
               -port_num       $dut_port_1]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(ENABLE_EXTERNAL_PORT_CONFIG_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 8 : Observe that the port status of P1 in DUT is in PASSIVE state.     #
###############################################################################

sleep 3

STEP 8

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_PASSIVE_P1_O)"

if {![ptp_ha::dut_check_ptp_port_state \
              -port_num                $dut_port_1\
              -port_state              $passive]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_PASSIVE_P1_F)"\
                       -tc_def  $tc_def
    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 9 : Observe that DUT transmits PDELAY_REQ message on the port P1 with  #
#          following parameters :                                             #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type              = 0x02                           #
#                  Domain Number             = DN1                            #
#                  Sequence ID               = D                              #
#                  Source Port Identity      = E                              #
###############################################################################

STEP 9

LOG -level 0 -msg "$tee_log_msg(PDELAY_REQ_RX_P1_O)"

if {![ptp_ha::recv_pdelay_req \
           -port_num                   $tee_port_1\
           -domain_number              $domain\
           -error_reason               error_reason\
           -recvd_src_port_number      recvd_src_port_number\
           -recvd_src_clock_identity   recvd_src_clock_identity\
           -recvd_correction_field     PDREQ_CF\
           -recvd_sequence_id          recvd_sequence_id\
           -filter_id                  $filter_id]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(PDELAY_REQ_RX_P1_F) $error_reason"\
                        -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 10 : Verify that the port status of P1 in DUT continues to be in       #
#           PASSIVE state.                                                    #
###############################################################################

STEP 10

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_PASSIVE_P1_F)"

if {![ptp_ha::dut_check_ptp_port_state \
              -port_num                $dut_port_1\
              -port_state              $passive] } {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL  -reason "$dut_log_msg(CHECK_STATE_PASSIVE_P1_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 11 : Disable defaultDS.externalPortConfigurationEnabled on DUT.        #
###############################################################################

STEP 11

LOG -level 0 -msg "$dut_log_msg(DISABLE_EXTERNAL_PORT_CONFIG)"

if {![ptp_ha::dut_external_port_config_disable\
            -port_num         $dut_port_1]} {

     TEE_CLEANUP 

     TC_CLEAN_AND_ABORT -reason "$dut_log_msg(DISABLE_EXTERNAL_PORT_CONFIG_F)"\
                        -tc_def $tc_def
     return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 12: Observe that DUT transmits PDELAY_REQ message on the port P1 with  #
#          following parameters :                                             #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type              = 0x02                           #
#                  Domain Number             = DN1                            #
#                  Sequence ID               = D                              #
#                  Source Port Identity      = E                              #
###############################################################################

STEP 12

LOG -level 0 -msg "$tee_log_msg(PDELAY_REQ_RX_P1_O)"

if {![ptp_ha::recv_pdelay_req \
           -port_num                   $tee_port_1\
           -domain_number              $domain\
           -error_reason               error_reason\
           -recvd_src_port_number      recvd_src_port_number\
           -recvd_src_clock_identity   recvd_src_clock_identity\
           -recvd_correction_field     PDREQ_CF\
           -recvd_sequence_id          recvd_sequence_id\
           -filter_id                  $filter_id]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(PDELAY_REQ_RX_P1_F) $error_reason"\
                        -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 13 : Verify that the port status of P1 in DUT is in FAULTY state.      #
###############################################################################

vwait ::ok_to_send_13

STEP 13

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_FAULTY_P1_V)"

if {![ptp_ha::dut_check_ptp_port_state \
              -port_num                $dut_port_1\
              -port_state              $faulty] } {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL  -reason "$dut_log_msg(CHECK_STATE_FAULTY_P1_F)"\
                       -tc_def  $tc_def
    return 0
}

TEE_CLEANUP

TC_CLEAN_AND_PASS -reason "DUT remains in PASSIVE state by\
                           setting externalPortConfigurationPortDS\
                           desiredState to PASSIVE, even if fault condition occur."\
                  -tc_def  $tc_def

return 1

############################### END OF TEST CASE ##############################
