#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_ptp-ha_smg_018                                  #
# Test Case Version : 1.0                                                     #
# Component Name    : ATTEST PTP_HA CONFORMANCE TEST SUITE                    #
# Module Name       : PTP-HA State Machine Group (SMG)                        #
#                                                                             #
# Title             : L1Sync Port state changes from L1_SYNC_UP to DISABLED   #
#                     when L1Sync is disabled                                 #
#                                                                             #
# Purpose           : To verify that L1Sync port changes its state from       #
#                     L1_SYNC_UP to DISABLED when L1Sync is disabled by       #
#                     setting the data set L1SyncBasicPortDS.L1SyncEnabled    #
#                     to FALSE via configuration.                             #
#                                                                             #
# Reference         : IEEE 1588-2017 Clause O.7.2 Table 157 Page 449,         #
#                     Clause O.7.3 Figure 70 Page 450                         #
#                                                                             #
# Conformance Type  : MUST                                                    #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 001                                                     #
# Test Topology     : 001                                                     #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#             TEE                                                   DUT       #
#           [slave]                                              [master]     #
#              |                                                     |        #
#              |                       <Enable PTP with BC/OC clock> |        #
#              |                      <Clock mode one-step/two-step> |        #
#              |                                <Disable TCR,RCR,CR> |        #
#              |                     <Disable L1SynOptParams option> |        #
#              |     <Configure L1SyncInterval,L1SyncReceiptTimeout> |        #
#              |                           <Configure Priority1 - X> |        #
#              |                                        <Enable PTP> | P1     #
#              |                                     <Enable L1SYNC> | P1     #
#              |                                                     |        #
#              |                          ANNOUNCE [MSG_TYPE = 0x0B, |        #
#              |                     DN = DN1, SEQ_ID = Y, PRI1 = X] |        #
#           T1 |-------------------------------------------------<<--| P1     #
#              |                                                     |        #
#              | ANNOUNCE [MSG_TYPE = 0x0B, DN = DN1,                |        #
#              |           SEQ_ID = A, PRI1 = X+1]                   |        #
#           T1 |-->>-------------------------------------------------| P1     #
#              |                                                     |        #
#              |                   <Check L1SYNC port status - IDLE> | P1     #
#              |                                                     |        #
#              |     PTP SIGNALING with L1 Sync TLV [MSG_TYPE = 0xC, |        #
#              |                           DN = DN1,TLV_TYPE=0x8001] |        #
#           T1 |-------------------------------------------------<<--| P1     #
#              |                                                     |        #
#              | PTP SIGNALING with L1 Sync TLV [MSG_TYPE = 0xC,     |        #
#              | DN = DN1,TLV_TYPE=0x8001,TCR=1,RCR=1,CR=1           |        #
#              | ITC=0,IRC=0,IC=0]                                   |        #
#           T1 |---->>-----------------------------------------------| P1     #
#              |                                                     |        #
#              |  <Enable txCoherentIsRequired,rxCoherentIsRequired  | P1     #
#              |                               congruentIsRequired>  |        #
#              |                                                     |        #
#              |     PTP SIGNALING with L1 Sync TLV [MSG_TYPE = 0xC, |        #
#              |            DN=DN1,TLV_TYPE=0X8001,TCR=1,RCR=1,CR=1] |        #
#           T1 |-------------------------------------------------<<--| P1     #
#              |                                                     |        #
#              | PTP SIGNALING with L1 Sync TLV [MSG_TYPE = 0xC,     |        #
#              | DN=DN1,TLV_TYPE=0x8001,TCR=1,RCR=1,CR=1,ITC=0,      |        #
#              | IRC=0,IC=0]                                         |        #
#           T1 |-->>-------------------------------------------------| P1     #
#              |                                                     |        #
#              | PTP SIGNALING with L1 Sync TLV [MSG_TYPE = 0xC,     |        #
#              | DN=DN1,TLV_TYPE=0x8001,TCR=1,RCR=1,CR=1,ITC=1,      |        #
#              | IRC=1,IC=1]                                         |        #
#           T1 |-->>-------------------------------------------------| P1     #
#              |                                                     |        #
#              |                   <Wait for 60s>                    |        #
#              |                                                     |        #
#              |     PTP SIGNALING with L1 Sync TLV [MSG_TYPE = 0xC, |        #
#              |      DN=DN1,TLV_TYPE=0x8001,TCR=1,RCR=1,CR=1,ITC=1, |        #
#              |                                         IRC=1,IC=1] |        #
#           T1 |-------------------------------------------------<<--| P1     #
#              |                                                     |        #
#              |             <Check L1SYNC port status - L1_SYNC_UP> | P1     #
#              |                                                     |        #
#              |                                    <Disable L1SYNC> | P1     #
#              |                                                     |        #
#              |               <Check L1SYNC port status - DISABLED> | P1     #
#              |                                                     |        #
#              |     PTP SIGNALING with L1 Sync TLV [MSG_TYPE = 0xC, |        #
#              |                           DN = DN1,TLV_TYPE=0x8001] |        #
#           T1 |          XX-------------------------------------<<--| P1     #
#              |                                                     |        #
#                                                                             #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     MSG_TYPE  = Message Type                                                #
#     DN        = Domain Number                                               #
#     BC        = Boundary Clock                                              #
#     OC        = Ordinary Clock                                              #
#     TCR       = txCoherentIsRequired                                        #
#     RCR       = rxCoherentIsRequired                                        #
#     CR        = congruentIsRequired                                         #
#     ITC       = peerIsTxCoherent                                            #
#     IRC       = peerIsRxCoherent                                            #
#     IC        = peerIsCongruent                                             #
#                                                                             #
# NOTE :                                                                      #
#                                                                             #
#  1. This objective is verified using the High Accuracy Delay Request-       #
#     Response Default PTP Profile                                            #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#   iii.   Configure clock mode as One-step/Two-step.                         #
#    iv.   Disable txcoherentisRequired, rxcoherentisRequired,                #
#          congruentIsRequired.                                               #
#     v.   Disable L1SynOptParams on DUT.                                     #
#    vi.   Configure L1SyncInterval and L1SyncReceiptTimeout value.           #
#   vii.   Enable PTP on port P1.                                             #
#  viii.   Enable L1SYNC on DUT's port P1.                                    #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
#                  Sequence ID   = Y                                          #
#                  Priority1     = X                                          #
#                                                                             #
# Step 4 : Send periodic ANNOUNCE message with Priority1 value incremented    #
#          from the Priority1 value of received Announce message on port T1   #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
#                  Sequence ID   = A                                          #
#                  Priority1     = X+1                                        #
#                                                                             #
# Step 5 : Observe that the DUT's L1SYNC port status P1 is in IDLE state.     #
#                                                                             #
# Step 6 : Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV  #
#          on the port P1 with following parameters:                          #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                                                                             #
# Step 7 : Send PTP SIGNALING message with L1 Sync TLV on the port P1 with    #
#          following parameters:                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 1                                          #
#                  RCR           = 1                                          #
#                  CR            = 1                                          #
#                  ITC           = 0                                          #
#                  IRC           = 0                                          #
#                  IC            = 0                                          #
#                                                                             #
# Step 8 : Enable txCoherentIsRequired,rxCoherentIsRequired and               #
#          congruentIsRequired on DUT's port P1.                              #
#                                                                             #
# Step 9 : Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV  #
#          on the port P1 with following parameters:                          #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 1                                          #
#                  RCR           = 1                                          #
#                  CR            = 1                                          #
#                                                                             #
# Step 10 : Send PTP SIGNALING message with L1 Sync TLV on the port P1 with   #
#          following parameters:                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 1                                          #
#                  RCR           = 1                                          #
#                  CR            = 1                                          #
#                  ITC           = 0                                          #
#                  IRC           = 0                                          #
#                  IC            = 0                                          #
#                                                                             #
# Step 11: Send PTP SIGNALING message with L1 Sync TLV on the port P1 with    #
#          following parameters:                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 1                                          #
#                  RCR           = 1                                          #
#                  CR            = 1                                          #
#                  ITC           = 1                                          #
#                  IRC           = 1                                          #
#                  IC            = 1                                          #
#                                                                             #
# Step 12 : Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV #
#           on the port P1 after duration of 60s with following parameters:   #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 1                                          #
#                  RCR           = 1                                          #
#                  CR            = 1                                          #
#                  ITC           = 1                                          #
#                  IRC           = 1                                          #
#                  IC            = 1                                          #
#                                                                             #
# Step 13 : Observe that the DUT's L1SYNC port status P1 is in L1_SYNC_UP     #
#           state                                                             #
#                                                                             #
# Step 14 : Disable L1SYNC on DUT's port P1.                                  #
#                                                                             #
# Step 15 : Observe that the DUT's L1SYNC port status P1 is in DISABLED state.#
#                                                                             #
# Step 16 : Verify that DUT does not transmit PTP SIGNALING message with      #
#          L1 Sync TLV on Port P1.                                            #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                                                                             #
# Step 17: Send PTP SIGNALING message with L1 Sync TLV on the port P1 with    #
#          following parameters:                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                                                                             #
# Step 18: Observe that the DUT's L1SYNC port status P1 is in DISABLED state. #
#                                                                             #
# Step 19: Verify that DUT does not transmit PTP SIGNALING message with       #
#          L1 Sync TLV on Port P1.                                            #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                                                                             #
###############################################################################
#                                                                             #
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          Apr/2018      CERN             Initial                         #
#                                                                             #
###############################################################################
# Copyright (C) 2018 CERN                                                     #
###############################################################################

global dut_log_msg
global tee_log_msg
global session_id

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def  "To verify that L1Sync port changes its state from\
             L1_SYNC_UP to DISABLED when L1SYNC is disabled by\
             setting the data set L1SyncBasicPortDS.L1SyncEnabled to\
             FALSE via configuration."

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set dut_port_1                     $::dut_port_num_1
set vlan_id                        $::ptp_ha_vlan_id
set dut_ip                         $::dut_test_port_ip
set tee_ip                         $::tee_test_port_ip
set dut_dest_ip                    $::dut_test_port_ip
set tee_dest_ip                    $::tee_test_port_ip
set ptp_comm_model                 $::ptp_comm_model
set tee_mac                        $::TEE_MAC
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT
set domain                         $::DEFAULT_DOMAIN
set gmid                           $::GRANDMASTER_ID
set infinity                       $::INFINITY

set disabled                       $::PTP_L1SYNC_STATE(DISABLED)
set idle                           $::PTP_L1SYNC_STATE(IDLE)
set link_alive                     $::PTP_L1SYNC_STATE(LINK_ALIVE)
set config_match                   $::PTP_L1SYNC_STATE(CONFIG_MATCH) 
set l1_sync_up                     $::PTP_L1SYNC_STATE(L1_SYNC_UP)
set tlv_type(l1sync)               $::TLV_TYPE(L1SYNC)

set sequence_id_ann        1
set sequence_id_l1sync     43692

set tcr_true        1
set rcr_true        1
set cr_true         1
set itc_true        1
set irc_true        1
set ic_true         1

set tcr_false       0
set rcr_false       0
set cr_false        0
set itc_false       0
set irc_false       0
set ic_false        0

########################### END - INITIALIZATION ##############################

ptp_ha::display_port_details

ptp_ha::dut_Initialization

proc cleanup {} {

    ptp_ha::dut_cleanup_setup_001

}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

### (Initial Part) ###

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP globally with Device Type as Boundary/Ordinary clock.   #
#   iii.   Configure clock mode as One-step/ two-Step.                        #
#    iv.   Disable txcoherentisRequired,rxcoherentisRequired,                 #
#          congruentIsRequired                                                #
#     v.   Disable L1SynOptParams on DUT.                                     #
#    vi.   Configure L1SyncInterval and L1SyncReceiptTimeout value            #
#   vii.   Enable PTP on port P1.                                             #
#  viii.   Enable L1SYNC on DUT's port P1.                                    #
###############################################################################

STEP 1

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP1)"

if {![ptp_ha::dut_configure_setup_001]} {

    LOG -msg "$dut_log_msg(INIT_SETUP1_F)"

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP1_F)"\
                       -tc_def $tc_def
    return 0
}

###############################################################################
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
###############################################################################

STEP 2

LOG -level 0 -msg "$tee_log_msg(INIT)"

ptp_ha::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

    if {![PTP_HA_UNICAST_PRE_REQUISITE_CONFIGURATION\
                        -port_num       $tee_port_1\
                        -tee_mac        $tee_mac\
                        -tee_ip         $tee_ip\
                        -dut_ip         $dut_ip\
                        -vlan_id        $vlan_id\
                        -dut_mac        tee_dest_mac\
                        -timeout        $arp_timeout]} {


        TEE_CLEANUP

        TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
                           -tc_def  $tc_def

        return 0
    }

}

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_1\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def  $tc_def

    return 0
}

set filter_id [lindex $id1 end]

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 3 : Observe that DUT transmits ANNOUNCE message on the port P1 with    #
#          following parameters.                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
#                  Sequence ID   = Y                                          #
#                  Priority1     = X                                          #
###############################################################################

STEP 3

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
           -port_num                $tee_port_1\
           -domain_number           $domain\
           -filter_id               $filter_id\
           -recvd_gm_priority1      priority1\
           -error_reason            error_reason]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANNOUNCE_RX_P1_F) $error_reason"\
                        -tc_def  $tc_def

    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 4 : Send periodic ANNOUNCE message with Priority1 value incremented    #
#          from the Priority1 value of received Announce message on port T1   #
#          with following parameters.                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0x0B                                       #
#                  Domain Number = DN1                                        #
#                  Sequence ID   = A                                          #
#                  Priority1     = X+1                                        #
###############################################################################

STEP 4

set priority [expr $priority1 + 1]

LOG -level 0 -msg "$tee_log_msg(ANN_TX_T1)"

if {![ptp_ha::send_announce \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -gm_priority1            $priority\
           -src_ip                  $tee_ip\
           -unicast_flag            $::unicast_flag\
           -domain_number           $domain\
           -count                   $infinity\
           -sequence_id             $sequence_id_ann]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANN_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 5 : Observe that the DUT's L1SYNC port status P1 is in IDLE state.     #
###############################################################################

STEP 5

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_IDLE_P1_O)"

if {![ptp_ha::dut_check_l1sync_state \
              -port_num                $dut_port_1\
              -l1sync_state            $idle] } {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_IDLE_P1_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 6 : Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV  #
#          on the port P1 with following parameters:                          #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
###############################################################################

STEP 6

LOG -level 0 -msg  "$tee_log_msg(L1SYNC_RX_P1_O)"\

if {![ptp_ha::recv_signal \
              -port_num                $tee_port_1\
              -domain_number           $domain\
              -filter_id               $filter_id\
              -tlv_type                $tlv_type(l1sync)\
              -error_reason            error_reason]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(L1SYNC_RX_P1_F) $error_reason"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 7 : Send PTP SIGNALING message with L1 Sync TLV on the port P1 with    #
#          following parameters:                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 1                                          #
#                  RCR           = 1                                          #
#                  CR            = 1                                          #
#                  ITC           = 0                                          #
#                  IRC           = 0                                          #
#                  IC            = 0                                          #
###############################################################################

STEP 7

LOG -level 0 -msg  "$tee_log_msg(L1SYNC_TX_T1)"

if {![ptp_ha::send_signal \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -domain_number           $domain\
           -count                   $infinity\
           -tcr                     $tcr_true\
           -rcr                     $rcr_true\
           -cr                      $cr_true\
           -itc                     $itc_false\
           -irc                     $irc_false\
           -ic                      $ic_false\
           -sequence_id             $sequence_id_l1sync]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT   -reason "$tee_log_msg(L1SYNC_TX_T1_F)"\
                         -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 8 : Enable txCoherentIsRequired,rxCoherentIsRequired and               #
#          congruentIsRequired on DUT's port P1.                              #
###############################################################################

STEP 8

LOG -level 0 -msg "$dut_log_msg(ENABLE_TCR_RCR_CR)"

if {![ptp_ha::dut_tcr_rcr_cr_enable \
            -port_num       $dut_port_1]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(ENABLE_TCR_RCR_CR_F)"\
                       -tc_def $tc_def

    return 0
}

###############################################################################
# Step 9 : Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV  #
#          on the port P1 with following parameters:                          #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 1                                          #
#                  RCR           = 1                                          #
#                  CR            = 1                                          #
###############################################################################

STEP 9

LOG -level 0 -msg  "$tee_log_msg(L1SYNC_RX_P1_O)"

if {![ptp_ha::recv_signal \
              -port_num                $tee_port_1\
              -domain_number           $domain\
              -tlv_type                $tlv_type(l1sync)\
              -tcr                     $tcr_true\
              -rcr                     $rcr_true\
              -cr                      $cr_true\
              -filter_id               $filter_id\
              -error_reason            error_reason]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(L1SYNC_RX_P1_F) $error_reason"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 10 : Send PTP SIGNALING message with L1 Sync TLV on the port P1 with   #
#          following parameters:                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 1                                          #
#                  RCR           = 1                                          #
#                  CR            = 1                                          #
#                  ITC           = 0                                          #
#                  IRC           = 0                                          #
#                  IC            = 0                                          #
###############################################################################

STEP 10

LOG -level 0 -msg  "$tee_log_msg(L1SYNC_TX_T1)"

if {![ptp_ha::send_signal \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -domain_number           $domain\
           -count                   $infinity\
           -tcr                     $tcr_true\
           -rcr                     $rcr_true\
           -cr                      $cr_true\
           -itc                     $itc_false\
           -irc                     $irc_false\
           -ic                      $ic_false\
           -sequence_id             $sequence_id_l1sync]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT   -reason "$tee_log_msg(L1SYNC_TX_T1_F)"\
                         -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 11 : Send PTP SIGNALING message with L1 Sync TLV on the port P1 with   #
#          following parameters:                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 1                                          #
#                  RCR           = 1                                          #
#                  CR            = 1                                          #
#                  ITC           = 1                                          #
#                  IRC           = 1                                          #
#                  IC            = 1                                          #
###############################################################################

STEP 11

LOG -level 0 -msg  "$tee_log_msg(L1SYNC_TX_T1)"

if {![ptp_ha::send_signal \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -domain_number           $domain\
           -count                   $infinity\
           -tcr                     $tcr_true\
           -rcr                     $rcr_true\
           -cr                      $cr_true\
           -itc                     $itc_true\
           -irc                     $irc_true\
           -ic                      $ic_true\
           -count                   $infinity\
           -sequence_id             $sequence_id_l1sync]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT   -reason "$tee_log_msg(L1SYNC_TX_T1_F)"\
                         -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 12 : Observe that DUT transmits PTP SIGNALING message with L1 Sync TLV #
#          on the port P1 with following parameters:                          #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 1                                          #
#                  RCR           = 1                                          #
#                  CR            = 1                                          #
#                  ITC           = 1                                          #
#                  IRC           = 1                                          #
#                  IC            = 1                                          #
###############################################################################

STEP 12

set timeout [expr $::ptp_ha_1_l1sync_timeout + 60]

LOG -level 0 -msg  "$tee_log_msg(L1SYNC_RX_P1_O)"

if {![ptp_ha::recv_signal \
              -port_num                $tee_port_1\
              -domain_number           $domain\
              -tcr                     $tcr_true\
              -rcr                     $rcr_true\
              -tlv_type                $tlv_type(l1sync)\
              -cr                      $cr_true\
              -itc                     $itc_true\
              -irc                     $irc_true\
              -ic                      $ic_true\
              -timeout                 $timeout\
              -error_reason            error_reason\
              -filter_id               $filter_id]} {

    TEE_CLEANUP
    
    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(L1SYNC_RX_P1_F) $error_reason"\
                       -tc_def  $tc_def

    return 0
}

################################################################################
# Step 13 : Observe that the DUT's L1SYNC port status P1 is in L1_SYNC_UP state#
################################################################################

STEP 13

LOG -level 0 -msg  " $dut_log_msg(CHECK_STATE_L1_SYNC_UP_P1_O)"

sleep $::ptp_ha_l1sync_wait

if {![ptp_ha::dut_check_l1sync_state \
              -port_num                $dut_port_1\
              -l1sync_state            $l1_sync_up] } {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_L1_SYNC_UP_P1_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 14 : Disable L1SYNC on DUT's port P1.                                  #
###############################################################################

STEP 14

LOG -level 0 -msg "$dut_log_msg(DISABLE_L1SYNC_P1)"

if {![ptp_ha::dut_l1sync_disable\
           -port_num        $dut_port_1]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(DISABLE_L1SYNC_P1_F)"\
                       -tc_def $tc_def
    return 0
}

###############################################################################
# Step 15 : Observe that the DUT's L1SYNC port status P1 is in DISABLED state.#
###############################################################################

STEP 15

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_DISABLED_P1_O)"

if {![ptp_ha::dut_check_l1sync_state \
              -port_num                $dut_port_1\
              -l1sync_state            $disabled] } {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$dut_log_msg(CHECK_STATE_DISABLED_P1_F)"\
                        -tc_def  $tc_def
    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 16 : Verify that DUT does not transmit PTP SIGNALING message with L1   #
#          Sync TLV on the port P1 with following parameters:                 #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#                  L1_SYNC TLV                                                #
#                  TLV_TYPE      = 0x8001                                     #
###############################################################################

STEP 16

LOG -level 0 -msg  "$tee_log_msg(L1SYNC_NRX_P1_V)"

if {[ptp_ha::recv_signal \
           -port_num                $tee_port_1\
           -domain_number           $domain\
           -tlv_type                $tlv_type(l1sync)\
           -filter_id               $filter_id\
           -error_reason            error_reason]} {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL -reason "$tee_log_msg(L1SYNC_NRX_P1_F) $error_reason"\
                       -tc_def $tc_def

    return 0
}

###############################################################################
# Step 17 : Send PTP SIGNALING message with L1 Sync TLV on the port P1 with   #
#          following parameters:                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
###############################################################################

STEP 17

LOG -level 0 -msg  "$tee_log_msg(L1SYNC_TX_T1)"

if {![ptp_ha::send_signal \
              -port_num                $tee_port_1\
              -src_mac                 $tee_mac\
              -src_ip                  $tee_ip\
              -domain_number           $domain\
              -count                   $infinity\
              -sequence_id             $sequence_id_l1sync]} {

    TEE_CLEANUP
    
    TC_CLEAN_AND_ABORT   -reason "$tee_log_msg(L1SYNC_TX_T1_F)"\
                         -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 18 : Observe that the DUT's L1SYNC port status P1 is in DISABLED state.#
###############################################################################

STEP 18

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_DISABLED_P1_O)"\

if {![ptp_ha::dut_check_l1sync_state \
              -port_num                $dut_port_1\
              -l1sync_state            $disabled] } {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL   -reason "$dut_log_msg(CHECK_STATE_DISABLED_P1_F)"\
                        -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 19 : Verify that DUT does not transmit PTP SIGNALING message with L1   #
#          Sync TLV on the port P1 with following parameters:                 #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = DN1                                        #
#                  L1_SYNC TLV                                                #
#                  TLV_TYPE      = 0x8001                                     #
###############################################################################

STEP 19

LOG -level 0 -msg  "$tee_log_msg(L1SYNC_NRX_P1_V)"\

if {[ptp_ha::recv_signal \
           -port_num                $tee_port_1\
           -domain_number           $domain\
           -tlv_type                $tlv_type(l1sync)\
           -filter_id               $filter_id\
           -error_reason            error_reason]} {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL -reason "DUT transmits PTP SIGNALING message with L1 Sync\
                               TLV on the port P1."\
                      -tc_def  $tc_def

    return 0
}

TEE_CLEANUP

TC_CLEAN_AND_PASS -reason "L1Sync port changes its state from\
                           L1_SYNC_UP to DISABLED when L1SYNC is disabled by\
                           setting the data set L1SyncBasicPortDS.L1SyncEnabled to\
                           FALSE via configuration."\
                  -tc_def  $tc_def

return 1

########################## END OF TEST CASE ###################################
