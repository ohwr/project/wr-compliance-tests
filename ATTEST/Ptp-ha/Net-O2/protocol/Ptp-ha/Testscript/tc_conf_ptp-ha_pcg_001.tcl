#! /usr/bin/tclsh
###############################################################################
# Test Case         : tc_conf_ptp-ha_pcg_001                                  #
# Test Case Version : 1.4                                                     #
# Component Name    : ATTEST PTP-HA CONFORMANCE TEST SUITE                    #
# Module Name       : PTP-HA Configuration Group (PCG)                        #
#                                                                             #
# Title             : Default initialization values for attributes - High     #
#                     Accuracy Delay Request-Response mechanism               #
#                                                                             #
# Purpose           : To verify that a PTP enabled device stores all          #
#                     attributes with default initialization values for High  #
#                     Accuracy Delay Request-Respone mechanism. Checking that #
#                     the following attributes have correct default values.   #
#                     1) defaultDS.domainNumber = 0                           #
#                     2) portDS.logAnnounceInterval = 1                       #
#                     3) portDS.logSyncInterval = 0                           #
#                     4) portDS.logMinDelayReqInterval = 0                    #
#                     5) portDS.announceReceiptTimeout = 3                    #
#                     6) defaultDS.priority1 = 128                            #
#                     7) defaultDS.priority2 = 128                            #
#                     8) defaultDS.slaveOnly = FALSE                          #
#                     9) defaultDS.SdoId = 0x000                              #
#                     10) L1SyncBasicPortDS.L1SyncEnabled = TRUE              #
#                     11) L1SyncBasicPortDS.txCoherencyIsRequired = TRUE      #
#                     12) L1SyncBasicPortDS.rxCoherencyIsRequired = TRUE      #
#                     13) L1SyncBasicPortDS.congruencyIsRequired = TRUE       #
#                     14) L1SyncBasicPortDS.optParametersConfigured = FALSE   #
#                     15) L1SyncBasicPortDS.logL1SyncInterval = 0             #
#                     16) L1SyncBasicPortDS.L1SyncReceiptTimeout = 3          #
#                     17) defaultDS.externalPortConfigurationEnabled = FALSE  #
#                     18) timestampCorrectionPortDS.egressLatency = Default is#
#                         zero unless specified otherwise by implementation.  #
#                     19) timestampCorrectionPortDS.ingressLatency = Default  #
#                         is zero unless specified otherwise by               #
#                         implementation.                                     #
#                     20) asymmetryCorrectionPortDS.constantAsymmetry =       #
#                         Default is zero unless specified otherwise by       #
#                         implementation.                                     #
#                     21) asymmetryCorrectionPortDS.scaledDelayCoefficient =  #
#                         Default is zero unless specified otherwise by       #
#                         implementation.                                     #
#                     22) asymmetryCorrectionPortDS.enable = TRUE             #
#                     23) portDS.masterOnly = FALSE                           #
#                     Note: The default values of these attributes can be     #
#                     changed through ATTEST GUI (Go to Configuration Manager #
#                     and select desired configuration, go to                 #
#                     Protocol Options > PTP-HA > PTP-HA Attributes).         #
#                                                                             #
# Reference         : P1588/D1.3, February 2018 V3.01 Clause J.5.2 Page 412,  #
#                     Table 150 Page 413                                      #
#                                                                             #
# Conformance Type  : MUST                                                    #
#                                                                             #
###############################################################################
#                                                                             #
# Test Setup        : 004                                                     #
# Test Topology     : 001                                                     #
#                                                                             #
###############################################################################
#                                                                             #
# Ladder Diagram    :                                                         #
#                                                                             #
#          TEE                                                   DUT          #
#           |                                                     |           #
#           |                       <Enable PTP with BC/OC clock> |           #
#           |                    <Clock mode - one-step/two-step> |           #
#           |                                        <Enable PTP> | P1        #
#           |                                     <Enable L1SYNC> | P1        #
#           |                                                     |           #
#           |                   <Check portDS.masterOnly = FALSE> | P1        #
#           |                                                     |           #
#           |                 <Check defaultDS.slaveOnly = FALSE> | P1        #
#           |                                                     |           #
#           | <Check timestampCorrectionPortDS.egressLatency = 0> | P1        #
#           |                                                     |           #
#           | <Check timestampCorrectionPortDS.ingressLatency = 0>| P1        #
#           |                                                     |           #
#           |                                             <Check  | P1        #
#           |    asymmetryCorrectionPortDS.constantAsymmetry = 0> |           #
#           |                                                     |           #
#           |                                             <Check  | P1        #
#           | asymmetryCorrectionPortDS.scaledDelayCoefficient=0> |           #
#           |                                                     |           #
#           |                                              <Check | P1        #
#           | defaultDS.externalPortConfigurationEnabled = FALSE> |           #
#           |                                                     |           #
#           |                                             <Check  | P1        #
#           |            asymmetryCorrectionPortDS.enable = TRUE> |           #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                     DN = 0, PRI1 = 128, PRI2 = 128, |           #
#           |                      SDOID = 0x000, Timestamp = Ta] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                     DN = 0, PRI1 = 128, PRI2 = 128, |           #
#           |                      SDOID = 0x000, Timestamp = Tb] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                     DN = 0, PRI1 = 128, PRI2 = 128, |           #
#           |                      SDOID = 0x000, Timestamp = Tc] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |              <Check ((Tb - Ta) + (Tc - Tb))/2 = 2s> |           #
#           |                                                     |           #
#           | ANNOUNCE [MSG_TYPE = 0x0B,                          |           #
#           | DN = 0, PRI1 = 129, PRI2 = 128,                     |           #
#           | SDOID = 0x000]                                      |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           |                              SYNC [MSG_TYPE = 0x00, |           #
#           |                     DN = 0, PRI1 = 128, PRI2 = 128, |           #
#           |                      SDOID = 0x000, Timestamp = Ta] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                              SYNC [MSG_TYPE = 0x00, |           #
#           |                     DN = 0, PRI1 = 128, PRI2 = 128, |           #
#           |                      SDOID = 0x000, Timestamp = Tb] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                              SYNC [MSG_TYPE = 0x00, |           #
#           |                     DN = 0, PRI1 = 128, PRI2 = 128, |           #
#           |                      SDOID = 0x000, Timestamp = Tc] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |              <Check ((Tb - Ta) + (Tc - Tb))/2 = 1s> |           #
#           |                                                     |           #
#           |               PTP_SIGNALING message with L1SYNC TLV |           #
#           |    [MSG_TYPE = 0xC, DN = 0, PRI1 = 128, PRI2 = 128, |           #
#           |           SDOID = 0x000, TCR = 1,  RCR = 1, CR = 1, |           #
#           |                            OPE = 0, Timestamp = Ta] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |               PTP_SIGNALING message with L1SYNC TLV |           #
#           |    [MSG_TYPE = 0xC, DN = 0, PRI1 = 128, PRI2 = 128, |           #
#           |           SDOID = 0x000, TCR = 1,  RCR = 1, CR = 1, |           #
#           |                            OPE = 0, Timestamp = Tb] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |               PTP_SIGNALING message with L1SYNC TLV |           #
#           |    [MSG_TYPE = 0xC, DN = 0, PRI1 = 128, PRI2 = 128, |           #
#           |           SDOID = 0x000, TCR = 1,  RCR = 1, CR = 1, |           #
#           |                            OPE = 0, Timestamp = Tc] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |              <Check ((Tb - Ta) + (Tc - Tb))/2 = 1s> |           #
#           |                                                     |           #
#           | ANNOUNCE [MSG_TYPE = 0x0B,                          |           #
#           | DN = 0, PRI1 = 127, PRI2 = 128,                     |           #
#           | SDOID = 0x000]                                      |           #
#        T1 |-->>-------------------------------------------------| P1        #
#           |                                                     |           #
#           | <Send Sync/ Sync & Follow-up messages               |           #
#           | based on clock step>                                |           #
#           |                                                     |           #
#           |          < Wait for 6s to complete BMCA >           |           #
#           |                                                     |           #
#           |                         DELAY_REQ [MSG_TYPE = 0x01, |           #
#           |                     DN = 0, PRI1 = 128, PRI2 = 128, |           #
#           |                      SDOID = 0x000, Timestamp = Ta] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                         DELAY_REQ [MSG_TYPE = 0x01, |           #
#           |                     DN = 0, PRI1 = 128, PRI2 = 128, |           #
#           |                      SDOID = 0x000, Timestamp = Tb] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |                         DELAY_REQ [MSG_TYPE = 0x01, |           #
#           |                     DN = 0, PRI1 = 128, PRI2 = 128, |           #
#           |                      SDOID = 0x000, Timestamp = Tc] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#           |             <Check ((Tb - Ta) + (Tc - Tb))/2 <= 2s> |           #
#           |                                                     |           #
#           | PTP SIGNALING with L1 Sync TLV                      |           #
#           | [MSG_TYPE = 0xC, DN = 0,                            |           #
#           | PRI1 = 128, PRI2 = 128, SDOID = 0x000,              |           #
#           | TLV_TYPE = 0x8001, TCR = 0, RCR = 0, CR = 0]        |           #
#        T1 |---->>-----------------------------------------------| P1        #
#           |                                                     |           #
#           |             <Check L1SYNC port status - LINK_ALIVE> | P1        #
#           |                                                     |           #
#           | PTP SIGNALING with L1 Sync TLV                      |           #
#           | [MSG_TYPE = 0xC, DN = 0,                            |           #
#           | PRI1 = 128, PRI2 = 128, SDOID = 0x000,              |           #
#           | TLV_TYPE = 0x8001, TCR = 0, RCR = 0, CR = 0]        |           #
#        T1 |---->>----XX                                         | P1        #
#           |                                                     |           #
#           |                    <Wait for 3s>                    |           #
#           |                                                     |           #
#           |                   <Check L1SYNC port status - IDLE> | P1        #
#           |                                                     |           #
#           | ANNOUNCE [MSG_TYPE = 0x0B,                          |           #
#           | DN = 0, PRI1 = 127, PRI2 = 128,                     |           #
#           | SDOID = 0x000]                                      |           #
#        T1 |---->>----XX                                         | P1        #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                              DN = 0, SDOID = 0x000] |           #
#        T1 |                     XX--------------------------<<--| P1        #
#           |                                         (within 6s) |           #
#           |                                                     |           #
#           |                          ANNOUNCE [MSG_TYPE = 0x0B, |           #
#           |                              DN = 0, SDOID = 0x000] |           #
#        T1 |-------------------------------------------------<<--| P1        #
#           |                                                     |           #
#                                                                             #
# Legends           :                                                         #
#                                                                             #
#     MSG_TYPE  = Message Type                                                #
#     DN        = Domain Number                                               #
#     BC        = Boundary Clock                                              #
#     OC        = Ordinary Clock                                              #
#                                                                             #
# NOTE :                                                                      #
#                                                                             #
#  1. This objective is verified using the High Accuracy Delay                #
#     Request-Response Default PTP Profile                                    #
#                                                                             #
###############################################################################
#                                                                             #
# Procedure         :                                                         #
#                                                                             #
# (Initial Part)                                                              #
#                                                                             #
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#   iii.   Configure clock mode as One-step/Two-step.                         #
#    iv.   Enable PTP on port P1.                                             #
#     v.   Enable L1SYNC on DUT's port P1.                                    #
#                                                                             #
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
#                                                                             #
# (Part 1)                                                                    #
#                                                                             #
# Step 3 : Verify whether portDS.masterOnly is set to FALSE on port P1.       #
#                                                                             #
# Step 4 : Verify whether defaultDS.slaveOnly is set to FALSE.                #
#                                                                             #
# Step 5 : Verify whether timestampCorrectionPortDS.egressLatency = 0 on      #
#          port P1.                                                           #
#                                                                             #
# Step 6 : Verify whether timestampCorrectionPortDS.ingressLatency = 0 on     #
#          port P1.                                                           #
#                                                                             #
# Step 7 : Verify whether timestampCorrectionPortDS.constantAsymmetry = 0 on  #
#          port P1.                                                           #
#                                                                             #
# Step 8 : Verify whether timestampCorrectionPortDS.scaledDelayCoefficient = 0#
#          on port P1.                                                        #
#                                                                             #
# Step 9 : Verify whether asymmetryCorrectionPortDS.enable is set to TRUE on  #
#          port P1.                                                           #
#                                                                             #
# Step 10: Verify whether defaultDS.externalPortConfigurationEnabled is set to#
#          FALSE on port P1.                                                  #
#                                                                             #
# Step 11: Check whether that the DUT transmits three consecutive ANNOUNCE    #
#          messages on the port P1 with following parameters and store        #
#          timestamps Ta, Tb and Tc for messages respectively.                #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = 0                                     #
#                  Priority1          = 128                                   #
#                  Priority2          = 128                                   #
#                  majorSdoId         = 0                                     #
#                  minorSdoId         = 0                                     #
#                                                                             #
# Step 12: Verify whether ((Tb - Ta) + (Tc - Tb))/2 = 2s                      #
#                                                                             #
# Step 13: Send periodic ANNOUNCE message on port T1 with following           #
#          parameters:                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = 0                                     #
#                  Priority1          = 129                                   #
#                  Priority2          = 128                                   #
#                  majorSdoId         = 0                                     #
#                  minorSdoId         = 0                                     #
#                                                                             #
# Step 14: Check whether that the DUT transmits three consecutive SYNC        #
#          messages on the port P1 with following parameters and store        #
#          timestamps Ta, Tb and Tc for messages respectively.                #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x00                                  #
#                  Domain Number      = 0                                     #
#                  Priority1          = 128                                   #
#                  Priority2          = 128                                   #
#                  majorSdoId         = 0                                     #
#                  minorSdoId         = 0                                     #
#                                                                             #
# Step 15: Verify whether ((Tb - Ta) + (Tc - Tb))/2 = 1s                      #
#                                                                             #
# Step 16: Check whether that the DUT transmits three consecutive PTP         #
#          SIGNALING message with L1 Sync TLV messages on the port P1 with    #
#          following parameters and store timestamps Ta, Tb and Tc for        #
#          messages respectively.                                             #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0xC                                   #
#                  Domain Number      = DN1                                   #
#                  Priority1          = 128                                   #
#                  Priority2          = 128                                   #
#                  majorSdoId         = 0                                     #
#                  minorSdoId         = 0                                     #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE           = 0x8001                                #
#                  TCR                = 1                                     #
#                  RCR                = 1                                     #
#                  CR                 = 1                                     #
#                  OPE                = 0                                     #
#                                                                             #
# Step 17: Verify whether ((Tb - Ta) + (Tc - Tb))/2 = 1s                      #
#                                                                             #
# Step 18: Send periodic ANNOUNCE message on port T1 with following           #
#          parameters:                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = 0                                     #
#                  Priority1          = 127                                   #
#                  Priority2          = 128                                   #
#                  majorSdoId         = 0                                     #
#                  minorSdoId         = 0                                     #
#                                                                             #
# Step 19: Send periodic SYNC message on the port P1 with following           #
#           parameters:                                                       #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x00                                  #
#                  Domain Number      = 0                                     #
#                  majorSdoId         = 0                                     #
#                  minorSdoId         = 0                                     #
#                                                                             #
# Step 19a:If the clock is two-step clock, send periodic FOLLOW_UP message on #
#          port T1 with following parameters:                                 #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x08                                  #
#                  Domain Number      = 0                                     #
#                  majorSdoId         = 0                                     #
#                  minorSdoId         = 0                                     #
#                                                                             #
# Step 20: Wait for 6s for completing BMCA.                                   #
#                                                                             #
# Step 21: Check whether that the DUT transmits three consecutive DELAY_REQ   #
#          messages on the port P1 with following parameters and store        #
#          timestamps Ta, Tb and Tc for messages respectively.                #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x01                                  #
#                  Domain Number      = 0                                     #
#                  Priority1          = 128                                   #
#                  Priority2          = 128                                   #
#                  majorSdoId         = 0                                     #
#                  minorSdoId         = 0                                     #
#                                                                             #
# Step 22: Verify whether ((Tb - Ta) + (Tc - Tb))/2 <= 2s                     #
#                                                                             #
# Step 23: Send periodic PTP SIGNALING message with L1 Sync TLV on the port P1#
#          with following parameters:                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = 0                                          #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 0                                          #
#                  RCR           = 0                                          #
#                  CR            = 0                                          #
#                  ITC           = 0                                          #
#                  IRC           = 0                                          #
#                  IC            = 0                                          #
#                                                                             #
# Step 24: Observe that DUT's L1SYNC port status P1 is in LINK_ALIVE state.   #
#                                                                             #
# Step 25: Stop sending L1SYNC SIGNALLING message on the port T1.             #
#                                                                             #
# Step 26: Wait for expiry of 3s.                                             #
#                                                                             #
# Step 27: Verify that DUT's L1SYNC port status P1 is in IDLE state.          #
#                                                                             #
# Step 28: Stop ANNOUNCE, SYNC and FOLLOW_UP (if two-step clock) messages on  #
#          port T1.                                                           #
#                                                                             #
# Step 29: Observe that DUT does not transmit ANNOUNCE message on the port P1 #
#          within 6s.                                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = 0                                     #
#                  majorSdoId         = 0                                     #
#                  minorSdoId         = 0                                     #
#                                                                             #
# Step 30: Verify that DUT transmits ANNOUNCE message on the port P1 with     #
#          following parameters:                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = 0                                     #
#                  majorSdoId         = 0                                     #
#                  minorSdoId         = 0                                     #
#                                                                             #
###############################################################################
# History        Date        Author        Addition/Alteration                #
#                                                                             #
# 1.0          May/2018      CERN          Initial                            #
# 1.1          Jul/2018      CERN          Added 30% tolerance in message     #
#                                          measurements                       #
# 1.2          Sep/2018      CERN          a) Removed destination MAC and IP  #
#                                             in send functions.              #
#                                          b) Removed timeout in recv         #
#                                             functions.                      #
#                                          c) Moved timeout calculation to    #
#                                             PTP-HA global file.             #
#                                          d) Replaced configuration procedure#
#                                             with check procedure in steps 3 #
#                                             and 4.                          #
#                                          e) Changed default value of        #
#                                             transmission interval of Sync   #
#                                             messages to 1s.                 #
# 1.3          Oct/2018      CERN           Modified the acceptable range of  #
#                                           MinDelayReqInterval to 0% - 200%. #
# 1.4          Oct/2018      CERN           Updated failure condition in      #
#                                           script.                           #
#                                                                             #
###############################################################################
# Copyright (c) 2018 CERN                                                     #
###############################################################################

global dut_log_msg
global tee_log_msg
global session_id

####################### LOCAL VARIABLES ARE DECLARED HERE #####################

set tc_def "To verify that a PTP enabled device stores all\
            attributes with default initialization values for High\
            Accuracy Delay Request-Respone mechanism."

########################### START - INITIALIZATION ############################

set tee_port_1                     $::tee_port_num_1
set dut_port_1                     $::dut_port_num_1
set vlan_id                        $::ptp_ha_vlan_id
set dut_ip                         $::dut_test_port_ip
set ptp_comm_model                 $::ptp_comm_model
set ptp_ethtype                    $::PTP_ETHTYPE
set arp_timeout                    $::ARP_TIMEOUT

set domain                         $::DEFAULT_DOMAIN
set infinity                       $::INFINITY
set gmid                           $::GRANDMASTER_ID

set sequence_id_ann                1
set tlv_type(l1sync)               $::TLV_TYPE(L1SYNC)
set clock_step                     $::ptp_dut_clock_step
set idle                           $::PTP_L1SYNC_STATE(IDLE)
set link_alive                     $::PTP_L1SYNC_STATE(LINK_ALIVE)

set state               "TRUE"
set egress_latency      $::ptp_egress_latency
set ingress_latency     $::ptp_ingress_latency
set constant_asymmetry  $::ptp_constant_asymmetry
set delay_coefficient   $::ptp_delay_coefficient
set major_sdo_id        0
set minor_sdo_id        0

set tcr_true       1
set rcr_true       1
set cr_true        1
set ope_false      0

set tcr_false       0
set rcr_false       0
set cr_false        0
set itc_false       0
set irc_false       0
set ic_false        0

set value_false     0

set priority1      $::ptp_dut_default_priority1
set priority2      $::ptp_dut_default_priority2

set DUT_BOOLEAN(0)  FALSE
set DUT_BOOLEAN(1)  TRUE


########################### END - INITIALIZATION ##############################

ptp_ha::display_port_details

ptp_ha::dut_Initialization

proc cleanup {} {

    ptp_ha::dut_cleanup_setup_004

}

##############################################################################
# NOTE : Cleanup procedure must be called once both the sessions have been   #
#        opened                                                              #
##############################################################################

TC_CLEAN

### (Initial Part) ###

###############################################################################
# Step 1 : Initialization of DUT                                              #
#     i.   Enable DUT's port P1.                                              #
#    ii.   Enable PTP globally with device type as Boundary/Ordinary clock.   #
#   iii.   Configure clock mode as One-step/Two-step.                         #
#    iv.   Enable PTP on port P1.                                             #
#     v.   Enable L1SYNC on DUT's port P1.                                    #
###############################################################################

STEP 1

LOG -level 0 -msg "$dut_log_msg(INIT_SETUP4)"

if {![ptp_ha::dut_configure_setup_004]} {

    LOG -msg "$dut_log_msg(INIT_SETUP4_F)"

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(INIT_SETUP4_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 2 : Initialization of TEE                                              #
#     i.   Add port T1 at TEE.                                                #
###############################################################################

STEP 2

LOG -level 0 -msg "$tee_log_msg(INIT)"

ptp_ha::Initialization

set tee_mac       $::TEE_MAC
set tee_ip        $::tee_test_port_ip

if {$ptp_comm_model == "Unicast"} {

    if {![PTP_HA_UNICAST_PRE_REQUISITE_CONFIGURATION\
                        -port_num       $tee_port_1\
                        -tee_mac        $tee_mac\
                        -tee_ip         $tee_ip\
                        -dut_ip         $dut_ip\
                        -vlan_id        $vlan_id\
                        -dut_mac        tee_dest_mac\
                        -timeout        $arp_timeout]} {

        TEE_CLEANUP

        TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ARP_F)"\
                           -tc_def  $tc_def

        return 0
    }

}

if {![pltLib::create_filter_ptp_ha_pkt\
              -port_num                $tee_port_1\
              -ether_type              $ptp_ethtype\
              -vlan_id                 $vlan_id\
              -ids                     id1]} {

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FILTER_F)"\
                       -tc_def  $tc_def

    return 0
}

set filter_id [lindex $id1 end]

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 3 : Verify whether portDS.masterOnly is set to FALSE on port P1.       #
###############################################################################

STEP 3

LOG -level 0 -msg "$dut_log_msg(DEFAULTDS.MASTERONLY_V)"

if {![ptp_ha::dut_check_master_only\
             -port_num     $dut_port_1\
             -value        [set DUT_BOOLEAN($value_false)]]} {

     TEE_CLEANUP

     TC_CLEAN_AND_FAIL -reason "$dut_log_msg(DEFAULTDS.MASTERONLY_F)"\
                       -tc_def  $tc_def
     return 0
}

###############################################################################
# Step 4 : Verify whether defaultDS.slaveOnly is set to FALSE.                #
###############################################################################

STEP 4

LOG -level 0 -msg "$dut_log_msg(DEFAULTDS.SLAVEONLY_V)"

if {![ptp_ha::dut_check_slave_only \
             -value        [set DUT_BOOLEAN($value_false)]]} {

     TEE_CLEANUP

     TC_CLEAN_AND_FAIL -reason "$dut_log_msg(DEFAULTDS.SLAVEONLY_F)"\
                       -tc_def  $tc_def
     return 0
}

###############################################################################
# Step 5 : Verify whether timestampCorrectionPortDS.egressLatency = 0 on      #
#          port P1.                                                           #
###############################################################################

STEP 5

LOG -level 0 -msg "Check egressLatency of port P1 ($dut_port_1) is\
                   $egress_latency ns"

if {![ptp_ha::dut_check_egress_latency\
              -port_num     $dut_port_1\
              -latency      $egress_latency]} {

     TEE_CLEANUP

     TC_CLEAN_AND_FAIL -reason "egressLatency of port P1 ($dut_port_1) is not\
                                 $egress_latency ns"\
                       -tc_def  $tc_def
     return 0
}

###############################################################################
# Step 6 : Verify whether timestampCorrectionPortDS.ingressLatency = 0 on     #
#          port P1.                                                           #
###############################################################################

STEP 6

LOG -level 0 -msg "Check ingressLatency of port P1 ($dut_port_1) is\
                   $ingress_latency ns"

if {![ptp_ha::dut_check_ingress_latency\
              -port_num       $dut_port_1\
              -latency        $ingress_latency]} {

     TEE_CLEANUP

     TC_CLEAN_AND_FAIL -reason "ingressLatency of port P1 ($dut_port_1) is not\
                                $ingress_latency ns"\
                       -tc_def  $tc_def
     return 0
}

###############################################################################
# Step 7 : Verify whether timestampCorrectionPortDS.constantAsymmetry = 0 on  #
#          port P1.                                                           #
###############################################################################

STEP 7

LOG -level 0 -msg "Check constantAsymmetry of port P1 ($dut_port_1) is\
                   $constant_asymmetry ns"

if {![ptp_ha::dut_check_constant_asymmetry \
              -port_num             $dut_port_1\
              -constant_asymmetry   $constant_asymmetry]} {

     TEE_CLEANUP

     TC_CLEAN_AND_FAIL -reason "constantAsymmetry of port P1 ($dut_port_1) is not\
                                $constant_asymmetry ns"\
                       -tc_def  $tc_def
     return 0
}

###############################################################################
# Step 8 : Verify whether timestampCorrectionPortDS.scaledDelayCoefficient = 0#
#          on port P1.                                                        #
###############################################################################

STEP 8

LOG -level 0 -msg "Check scaledDelayCoefficient of port P1 ($dut_port_1) is\
                   $delay_coefficient"

if {![ptp_ha::dut_check_scaled_delay_coefficient\
              -port_num             $dut_port_1\
              -delay_coefficient    $delay_coefficient]} {

     TEE_CLEANUP

     TC_CLEAN_AND_FAIL -reason "scaledDelayCoefficient of port P1 ($dut_port_1) is not\
                                $delay_coefficient"\
                       -tc_def  $tc_def
     return 0
}

###############################################################################
# Step 9 : Verify whether asymmetryCorrectionPortDS.enable is set to TRUE on  #
#          port P1.                                                           #
###############################################################################

STEP 9

LOG -level 0 -msg "$dut_log_msg(ACP.ENABLE_V)"

if {![ptp_ha::dut_check_asymmetry_correction\
              -port_num         $dut_port_1\
              -state            $state]} {

     TEE_CLEANUP

     TC_CLEAN_AND_FAIL -reason "$dut_log_msg(ACP.ENABLE_VF)"\
                       -tc_def  $tc_def
     return 0
}

###############################################################################
# Step 10: Verify whether defaultDS.externalPortConfigurationEnabled is set to#
#          FALSE on port P1.                                                  #
###############################################################################

STEP 10

LOG -level 0 -msg "$dut_log_msg(EXTPORT_CONFIGURE_V)"

if {![ptp_ha::dut_check_external_port_config \
            -port_num       $dut_port_1\
            -state          [set DUT_BOOLEAN($value_false)]]} {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL -reason "$dut_log_msg(EXTPORT_CONFIGURE_F)"\
                      -tc_def  $tc_def
    return 0

}

###############################################################################
# Step 11: Check whether that the DUT transmits three consecutive ANNOUNCE    #
#          messages on the port P1 with following parameters and store        #
#          timestamps Ta, Tb and Tc for messages respectively.                #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = 0                                     #
#                  Priority1          = 128                                   #
#                  Priority2          = 128                                   #
#                  majorSdoId         = 0                                     #
#                  minorSdoId         = 0                                     #
###############################################################################

STEP 11a

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -gm_priority1      $priority1\
             -gm_priority2      $priority2\
             -reset_count       $::reset_count_off\
             -major_sdo_id      $major_sdo_id\
             -minor_sdo_id      $minor_sdo_id\
             -rx_timestamp_ns   Ta]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}


STEP 11b

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -gm_priority1      $priority1\
             -gm_priority2      $priority2\
             -reset_count       $::reset_count_off\
             -major_sdo_id      $major_sdo_id\
             -minor_sdo_id      $minor_sdo_id\
             -rx_timestamp_ns   Tb]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

STEP 11c

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -gm_priority1      $priority1\
             -gm_priority2      $priority2\
             -reset_count       $::reset_count_off\
             -major_sdo_id      $major_sdo_id\
             -minor_sdo_id      $minor_sdo_id\
             -rx_timestamp_ns   Tc]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 12: Verify whether ((Tb - Ta) + (Tc - Tb))/2 = 2s                      #
###############################################################################

STEP 12

set b [ expr (((($Tb-$Ta) + ($Tc-$Tb))/2)/1000000000.0) ]
set b [format %.1f $b]

LOG -level 0 -msg "$dut_log_msg(CHECK_EXPR)"
LOG -level 0 -msg "\nAnnounce Interval:"
LOG -level 0 -msg "               Expected = 2 secs (Acceptable range: 1.4s - 2.6s)"
LOG -level 0 -msg "               Observed = $b secs"

if {!($b >= 1.4 && $b <= 2.6)} {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL  -reason "$dut_log_msg(CHECK_EXPR_F)"\
                       -tc_def  $tc_def
    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 13: Send periodic ANNOUNCE message on port T1 with following           #
#          parameters:                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = 0                                     #
#                  Priority1          = 129                                   #
#                  Priority2          = 128                                   #
#                  majorSdoId         = 0                                     #
#                  minorSdoId         = 0                                     #
###############################################################################

STEP 13

set priority [ expr $priority1 + 1 ]

LOG -level 0 -msg "$tee_log_msg(ANN_TX_T1)"

if {![ptp_ha::send_announce \
      -port_num          $tee_port_1\
      -src_mac           $tee_mac\
      -src_ip            $tee_ip\
      -sequence_id       $sequence_id_ann\
      -count             $infinity\
      -domain_number     $domain\
      -gm_priority1      $priority\
      -gm_priority2      $priority2\
      -major_sdo_id      $major_sdo_id\
      -minor_sdo_id      $minor_sdo_id]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANN_TX_T1_F)"\
                        -tc_def $tc_def
    return 0
}

###############################################################################
# Step 14: Check whether that the DUT transmits three consecutive SYNC        #
#          messages on the port P1 with following parameters and store        #
#          timestamps Ta, Tb and Tc for messages respectively.                #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x00                                  #
#                  Domain Number      = 0                                     #
#                  Priority1          = 128                                   #
#                  Priority2          = 128                                   #
#                  majorSdoId         = 0                                     #
#                  minorSdoId         = 0                                     #
###############################################################################

STEP 14a

LOG -level 0 -msg "$tee_log_msg(SYNC_RX_P1_O)"

if {![ptp_ha::recv_sync \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -reset_count       $::reset_count_off\
             -gm_priority1      $priority1\
             -gm_priority2      $priority2\
             -major_sdo_id      $major_sdo_id\
             -minor_sdo_id      $minor_sdo_id\
             -rx_timestamp_ns   Ta]} {
    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

STEP 14b

LOG -level 0 -msg "$tee_log_msg(SYNC_RX_P1_O)"

if {![ptp_ha::recv_sync \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -reset_count       $::reset_count_off\
             -gm_priority1      $priority1\
             -gm_priority2      $priority2\
             -major_sdo_id      $major_sdo_id\
             -minor_sdo_id      $minor_sdo_id\
             -rx_timestamp_ns   Tb]} {
    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

STEP 14c

LOG -level 0 -msg "$tee_log_msg(SYNC_RX_P1_O)"

if {![ptp_ha::recv_sync \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -reset_count       $::reset_count_off\
             -gm_priority1      $priority1\
             -gm_priority2      $priority2\
             -major_sdo_id      $major_sdo_id\
             -minor_sdo_id      $minor_sdo_id\
             -rx_timestamp_ns   Tc]} {
    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 15: Verify whether ((Tb - Ta) + (Tc - Tb))/2 = 1s                      #
###############################################################################

STEP 15

set b [ expr (((($Tb-$Ta) + ($Tc-$Tb))/2)/1000000000.0) ]
set b [format %.1f $b]

LOG -level 0 -msg "$dut_log_msg(CHECK_EXPR)"
LOG -level 0 -msg "\nSync Interval:"
LOG -level 0 -msg "               Expected = 1 sec (Acceptable range: 0.7s - 1.3s)"
LOG -level 0 -msg "               Observed = $b secs"


if {!($b >= 0.7 && $b <= 1.3)} {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL  -reason "$dut_log_msg(CHECK_EXPR_F)"\
                       -tc_def  $tc_def
    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 16: Check whether that the DUT transmits three consecutive PTP         #
#          SIGNALING message with L1 Sync TLV messages on the port P1 with    #
#          following parameters and store timestamps Ta, Tb and Tc for        #
#          messages respectively.                                             #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0xC                                   #
#                  Domain Number      = DN1                                   #
#                  Priority1          = 128                                   #
#                  Priority2          = 128                                   #
#                  majorSdoId         = 0                                     #
#                  minorSdoId         = 0                                     #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE           = 0x8001                                #
#                  TCR                = 1                                     #
#                  RCR                = 1                                     #
#                  CR                 = 1                                     #
#                  OPE                = 0                                     #
###############################################################################

STEP 16a

LOG -level 0 -msg "$tee_log_msg(L1SYNC_RX_P1_O)"

if {![ptp_ha::recv_signal \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -gm_priority1      $priority1\
             -gm_priority2      $priority2\
             -major_sdo_id      $major_sdo_id\
             -reset_count       $::reset_count_off\
             -minor_sdo_id      $minor_sdo_id\
             -tlv_type          $tlv_type(l1sync)\
             -tcr               $tcr_true\
             -rcr               $rcr_true\
             -cr                $cr_true\
             -ope               $ope_false\
             -rx_timestamp_ns   Ta]} {
    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(L1SYNC_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

STEP 16b

LOG -level 0 -msg "$tee_log_msg(L1SYNC_RX_P1_O)"

if {![ptp_ha::recv_signal \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -gm_priority1      $priority1\
             -gm_priority2      $priority2\
             -major_sdo_id      $major_sdo_id\
             -minor_sdo_id      $minor_sdo_id\
             -tlv_type          $tlv_type(l1sync)\
             -tcr               $tcr_true\
             -rcr               $rcr_true\
             -cr                $cr_true\
             -ope               $ope_false\
             -reset_count       $::reset_count_off\
             -rx_timestamp_ns   Tb]} {
    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(L1SYNC_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

STEP 16c

LOG -level 0 -msg "$tee_log_msg(L1SYNC_RX_P1_O)"

if {![ptp_ha::recv_signal \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -gm_priority1      $priority1\
             -gm_priority2      $priority2\
             -major_sdo_id      $major_sdo_id\
             -minor_sdo_id      $minor_sdo_id\
             -tlv_type          $tlv_type(l1sync)\
             -tcr               $tcr_true\
             -rcr               $rcr_true\
             -cr                $cr_true\
             -ope               $ope_false\
             -reset_count       $::reset_count_off\
             -rx_timestamp_ns   Tc]} {
    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(L1SYNC_RX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 17: Verify whether ((Tb - Ta) + (Tc - Tb))/2 = 1s                      #
###############################################################################

STEP 17

set b [ expr (((($Tb-$Ta) + ($Tc-$Tb))/2)/1000000000.0) ]
set b [format %.1f $b]

LOG -level 0 -msg "$dut_log_msg(CHECK_EXPR)"
LOG -level 0 -msg "\nSignaling Interval:"
LOG -level 0 -msg "               Expected = 1 sec (Acceptable range: 0.7s - 1.3s)"
LOG -level 0 -msg "               Observed = $b secs"

if {!($b >= 0.7 && $b <= 1.3)} {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL  -reason "$dut_log_msg(CHECK_EXPR_F)"\
                       -tc_def  $tc_def
    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 18: Send periodic ANNOUNCE message on port T1 with following           #
#          parameters:                                                        #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = 0                                     #
#                  Priority1          = 127                                   #
#                  Priority2          = 128                                   #
#                  majorSdoId         = 0                                     #
#                  minorSdoId         = 0                                     #
###############################################################################

STEP 18

set priority [ expr $priority1 - 1 ]

LOG -level 0 -msg "$tee_log_msg(ANN_TX_T1)"

if {![ptp_ha::send_announce \
      -port_num          $tee_port_1\
      -src_mac           $tee_mac\
      -src_ip            $tee_ip\
      -sequence_id       $sequence_id_ann\
      -count             $infinity\
      -domain_number     $domain\
      -gm_priority1      $priority\
      -gm_priority2      $priority2\
      -major_sdo_id      $major_sdo_id\
      -minor_sdo_id      $minor_sdo_id\
      -gmid              $gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(ANN_TX_T1_F)"\
                        -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 19: Send periodic SYNC message on the port P1 with following           #
#           parameters:                                                       #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type    = 0x00                                     #
#                  Domain Number   = DN1                                      #
###############################################################################

STEP 19

LOG -level 0 -msg "$tee_log_msg(SYNC_TX_T1)"

if {![ptp_ha::send_sync \
           -port_num                $tee_port_1\
           -src_mac                 $tee_mac\
           -src_ip                  $tee_ip\
           -count                   $infinity\
           -domain_number           $domain\
           -major_sdo_id            $major_sdo_id\
           -minor_sdo_id            $minor_sdo_id\
           -gmid                    $gmid]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(SYNC_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 19a: If the clock is two-step clock, send periodic FOLLOW_UP message on#
#          port T1 with following parameters:                                 #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type     = 0x08                                    #
#                  Domain Number    = DN1                                     #
###############################################################################

if { $clock_step == "Two-Step" } {

STEP 19a

LOG -level 0 -msg "$tee_log_msg(FOLLOWUP_TX_T1)"

if {![ptp_ha::send_followup \
           -port_num          $tee_port_1\
           -domain_number     $domain\
           -src_mac           $tee_mac\
           -src_ip            $tee_ip\
           -count             $infinity]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(FOLLOWUP_TX_T1_F)"\
                       -tc_def  $tc_def

    return 0
}
 }

###############################################################################
# Step 20: Wait for 6s for completing BMCA.                                   #
###############################################################################

STEP 20

LOG -level 0 -msg "$dut_log_msg(WAIT_BMCA)"

sleep $::ptp_ha_bmca

###############################################################################
# Step 21: Check whether that the DUT transmits three consecutive DELAY_REQ   #
#          messages on the port P1 with following parameters and store        #
#          timestamps Ta, Tb and Tc for messages respectively.                #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x01                                  #
#                  Domain Number      = 0                                     #
#                  Priority1          = 128                                   #
#                  Priority2          = 128                                   #
#                  majorSdoId         = 0                                     #
#                  minorSdoId         = 0                                     #
###############################################################################

STEP 21a

LOG -level 0 -msg "$tee_log_msg(DELAY_REQ_RX_O)"

if {![ptp_ha::recv_delay_req \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -gm_priority1      $priority1\
             -gm_priority2      $priority2\
             -reset_count       $::reset_count_off\
             -major_sdo_id      $major_sdo_id\
             -minor_sdo_id      $minor_sdo_id\
             -rx_timestamp_ns   Ta]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(DELAY_REQ_RX_F)"\
                       -tc_def  $tc_def

    return 0
}

STEP 21b

LOG -level 0 -msg "$tee_log_msg(DELAY_REQ_RX_O)"

if {![ptp_ha::recv_delay_req \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -gm_priority1      $priority1\
             -gm_priority2      $priority2\
             -major_sdo_id      $major_sdo_id\
             -minor_sdo_id      $minor_sdo_id\
             -reset_count       $::reset_count_off\
             -rx_timestamp_ns   Tb]} {
    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(DELAY_REQ_RX_F)"\
                       -tc_def  $tc_def

    return 0
}

STEP 21c

LOG -level 0 -msg "$tee_log_msg(DELAY_REQ_RX_O)"

if {![ptp_ha::recv_delay_req \
             -port_num          $tee_port_1\
             -filter_id         $filter_id\
             -domain_number     $domain\
             -gm_priority1      $priority1\
             -gm_priority2      $priority2\
             -major_sdo_id      $major_sdo_id\
             -minor_sdo_id      $minor_sdo_id\
             -reset_count       $::reset_count_off\
             -recvd_src_port_number    src_port_number\
             -recvd_src_clock_identity src_clock_identity\
             -recvd_sequence_id        sequence_id\
             -rx_timestamp_ns   Tc]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(DELAY_REQ_RX_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 22: Verify whether ((Tb - Ta) + (Tc - Tb))/2 <= 2s                     #
###############################################################################

STEP 22

set b [ expr (((($Tb-$Ta) + ($Tc-$Tb))/2)/1000000000.0) ]
set b [format %.1f $b]

LOG -level 0 -msg "$dut_log_msg(CHECK_EXPR)"
LOG -level 0 -msg "\nminDelayRequestInterval:"
LOG -level 0 -msg "        Expected: <= 2 sec"
LOG -level 0 -msg "        Observed: $b sec"

if {!($b <= 2)} {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL  -reason "$dut_log_msg(CHECK_EXPR_F)"\
                       -tc_def  $tc_def
    return 0
}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
# Step 23: Send periodic PTP SIGNALING message with L1 Sync TLV on the port P1#
#          with following parameters:                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type  = 0xC                                        #
#                  Domain Number = 0                                          #
#              L1_SYNC TLV                                                    #
#                  TLV_TYPE      = 0x8001                                     #
#                  TCR           = 0                                          #
#                  RCR           = 0                                          #
#                  CR            = 0                                          #
#                  ITC           = 0                                          #
#                  IRC           = 0                                          #
#                  IC            = 0                                          #
###############################################################################

STEP 23

LOG -level 0 -msg "$tee_log_msg(L1SYNC_TX_T1)"

if {![ptp_ha::send_signal \
      -port_num          $tee_port_1\
      -src_mac           $tee_mac\
      -src_ip            $tee_ip\
      -sequence_id       $sequence_id_ann\
      -filter_id         $filter_id\
      -domain_number     $domain\
      -target_port_number     $src_port_number\
      -target_clock_identity  $src_clock_identity\
      -count             $infinity\
      -tlv_type          $tlv_type(l1sync)\
      -tcr               $tcr_false\
      -rcr               $rcr_false\
      -cr                $cr_false\
      -itc               $itc_false\
      -irc               $irc_false\
      -ic                $ic_false]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT  -reason "$tee_log_msg(L1SYNC_TX_T1_F)"\
                        -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 24: Observe that DUT's L1SYNC port status P1 is in LINK_ALIVE state.   #
#                                                                             #
###############################################################################

STEP 24

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_LINK_ALIVE_P1_O)"

sleep $::ptp_ha_l1sync_wait

if {![ptp_ha::dut_check_l1sync_state \
              -port_num                $dut_port_1\
              -l1sync_state            $link_alive] } {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$dut_log_msg(CHECK_STATE_LINK_ALIVE_P1_F)"\
                       -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 25: Stop sending L1SYNC SIGNALLING message on the port T1.             #
#                                                                             #
###############################################################################

STEP 25

LOG -level 0 -msg "$tee_log_msg(STOP_L1SYNC_TX_T1)"

if {![ptp_ha::stop_signal]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT   -reason "$tee_log_msg(STOP_L1SYNC_TX_T1_F)"\
                         -tc_def  $tc_def

    return 0
}

###############################################################################
# Step 26: Wait for expiry of 3s.                                             #
#                                                                             #
###############################################################################

STEP 26

LOG -level 0 -msg "$dut_log_msg(WAIT_3)"

sleep 3

###############################################################################
# Step 27: Verify that DUT's L1SYNC port status P1 is in IDLE state.          #
#                                                                             #
###############################################################################

STEP 27

LOG -level 0 -msg "$dut_log_msg(CHECK_STATE_IDLE_P1_V)"

if {![ptp_ha::dut_check_l1sync_state \
              -port_num                $dut_port_1\
              -l1sync_state            $idle] } {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL -reason "$dut_log_msg(CHECK_STATE_IDLE_P1_F)"\
                      -tc_def  $tc_def
    return 0
}

###############################################################################
# Step 28: Stop ANNOUNCE, SYNC and FOLLOW_UP (if two-step clock) messages on  #
#          port T1.                                                           #
###############################################################################

STEP 28a

LOG -level 0 -msg "$tee_log_msg(STOP_ANNOUNCE_TX_T1)"

if {![ptp_ha::stop_announce]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT   -reason "$tee_log_msg(STOP_ANNOUNCE_TX_T1_F)"\
                         -tc_def $tc_def

    return 0
}

STEP 28b

LOG -level 0 -msg "$tee_log_msg(STOP_SYNC_TX_T1)"

if {![ptp_ha::stop_sync ]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT   -reason "$tee_log_msg(STOP_SYNC_TX_T1_F)"\
                         -tc_def $tc_def

    return 0
}

if { $clock_step == "Two-Step" } {

STEP 28c

LOG -level 0 -msg "$tee_log_msg(STOP_FOLLOWUP_TX_T1)"

if {![ptp_ha::stop_followup ]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT   -reason "$tee_log_msg(STOP_FOLLOWUP_TX_T1_F)"\
                         -tc_def  $tc_def

    return 0
}

}

ptp_ha::reset_capture_stats\
        -session_id         $::tee_session_id\
        -port_num           $tee_port_1

###############################################################################
#                                                                             #
# Step 29: Observe that DUT does not transmit ANNOUNCE message on the port P1 #
#          within 6s.                                                         #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = 0                                     #
#                  majorSdoId         = 0                                     #
#                  minorSdoId         = 0                                     #
###############################################################################

STEP 29

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_NRX_P1_O)"

set timeout [expr $::ptp_ha_announce_timeout - 1]

if {[ptp_ha::recv_announce \
      -port_num          $tee_port_1\
      -filter_id         $filter_id\
      -domain_number     $domain\
      -major_sdo_id      $major_sdo_id\
      -minor_sdo_id      $minor_sdo_id\
      -timeout           $timeout]} {

    TEE_CLEANUP

    TC_CLEAN_AND_ABORT -reason "$tee_log_msg(ANNOUNCE_NRX_P1_F)"\
                       -tc_def  $tc_def

    return 0
}

###############################################################################
#                                                                             #
# Step 30: Verify that DUT transmits ANNOUNCE message on the port P1 with     #
#           following parameters                                              #
#                                                                             #
#              PTP Header                                                     #
#                  Message Type       = 0x0B                                  #
#                  Domain Number      = 0                                     #
#                  majorSdoId         = 0                                     #
#                  minorSdoId         = 0                                     #
###############################################################################

STEP 30

LOG -level 0 -msg "$tee_log_msg(ANNOUNCE_RX_P1_O)"

if {![ptp_ha::recv_announce \
      -port_num          $tee_port_1\
      -filter_id         $filter_id\
      -domain_number     $domain\
      -major_sdo_id      $major_sdo_id\
      -minor_sdo_id      $minor_sdo_id\
      -error_reason      error_reason\
      -timeout           $::ptp_ha_announce_timeout]} {

    TEE_CLEANUP

    TC_CLEAN_AND_FAIL -reason "$dut_log_msg(ANNOUNCE_RX_P1_F) $error_reason"\
                      -tc_def $tc_def

    return 0
}

TEE_CLEANUP

TC_CLEAN_AND_PASS -reason  "PTP enabled device stores all attributes with\
                            default initialization values for High Accuracy\
                            Delay Request-Respone mechanism."\
                  -tc_def   $tc_def

return 1

############################## END OF TEST CASE ##################################
